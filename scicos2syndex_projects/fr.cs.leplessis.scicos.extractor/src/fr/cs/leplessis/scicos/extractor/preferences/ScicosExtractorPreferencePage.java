package fr.cs.leplessis.scicos.extractor.preferences;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import fr.cs.leplessis.scicos.extractor.ScicosExtractorPlugin;

/**
 * This class represents a preference page that is contributed to the Preferences dialog. By subclassing
 * <samp>FieldEditorPreferencePage</samp>, we can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main
 * plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class ScicosExtractorPreferencePage extends PreferencePage implements IWorkbenchPreferencePage
{
	public ScicosExtractorPreferencePage()
	{
		super();
		setPreferenceStore(ScicosExtractorPlugin.getDefault().getPreferenceStore());
		noDefaultAndApplyButton();
	}

	@Override
	protected Control createContents(Composite parent)
	{
		Composite top = new Composite(parent, SWT.LEFT);

		// Sets the layout data for the top composite's
		// place in its parent's layout.
		GridLayout topLayout = new GridLayout();
		topLayout.numColumns = 1;
		top.setLayout(topLayout);
		// top.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label instructionLbl0 = new Label(top, SWT.NONE);
		instructionLbl0
				.setText("To use the transformation from end-to-end, you will need to add an extension into your scilab directory.");

		Label instructionLbl1 = new Label(top, SWT.NONE);
		instructionLbl1.setText("- First, download and install the Scilab 4.0 tool (http://www.scilab.org/).");

		Label instructionLbl2 = new Label(top, SWT.NONE);
		instructionLbl2
				.setText("- Then, install the Scicos extractor into the Scilab directory by pressing on the following button.");

		Button resetButton;
		resetButton = new Button(top, SWT.PUSH);
		resetButton.setText("Install the Scicos->ScicosMM tool");
		resetButton.setToolTipText("This will add a SciMeta directory inside your SciLab directory.");
		resetButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				installExtractor();
			}
		});

		Label instructionLbl5 = new Label(top, SWT.NONE);
		instructionLbl5.setText("- Now, launch Scilab to finalize the installation.\n"
				+ "  Normally, in the Scicos application, you will find a 'To Scimeta' element into the 'Object' menu.");

		return parent;
	}

	/**
	 * Extract the zip archive of scimeta extension into the SciLab directory.
	 */
	protected void installExtractor()
	{
		// Select the Scilab directory
		DirectoryDialog dialog = new DirectoryDialog(getShell(), SWT.OPEN);
		dialog.setText("Select Scilab directory...");
		String path = dialog.open();

		if (path != null)
		{
			// Extract the zip file into the selected scilab directory
			URL zipUrl = FileLocator.find(Platform.getBundle(ScicosExtractorPlugin.PLUGIN_ID), new Path(
				"zip/scimeta.zip"), null);

			try
			{
				ZipInputStream zipFileStream = new ZipInputStream(zipUrl.openStream());
				ZipEntry zipEntry = zipFileStream.getNextEntry();

				while (zipEntry != null)
				{
					// We will construct the new file but we will strip off the project
					// directory from the beginning of the path because we have already
					// created the destination project for this zip.
					File file = new File(path, zipEntry.getName());

					if (false == zipEntry.isDirectory())
					{
						/*
						 * Copy files (and make sure parent directory exist)
						 */
						File parentFile = file.getParentFile();
						if (null != parentFile && false == parentFile.exists())
						{
							parentFile.mkdirs();
						}
						OutputStream os = null;

						try
						{
							os = new FileOutputStream(file);

							byte[] buffer = new byte[102400];
							while (true)
							{
								int len = zipFileStream.read(buffer);
								if (zipFileStream.available() == 0)
									break;
								os.write(buffer, 0, len);
							}
						}
						finally
						{
							if (null != os)
							{
								os.close();
							}
						}
					}

					zipFileStream.closeEntry();
					zipEntry = zipFileStream.getNextEntry();
				}

				// Update the scilab.star file
				File sciFile = new File(path, "scilab.star");
				if (sciFile.exists())
				{
					PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(sciFile, true)));
					pw.println("\n// Add the reference to the Scimeta extractor =============================");
					pw.println("exec SCI/scimeta/dot.scilab");
					pw.close();
					ScicosExtractorPlugin.getDefault().getLog().log(
						new Status(IStatus.INFO, ScicosExtractorPlugin.PLUGIN_ID, "The '" + sciFile.getPath()
								+ "' file has been updated."));
				}
				else
				{
					MessageDialog
							.openError(
								getShell(),
								"Problem with scilab.star",
								"The file 'scilab.star' does not exist at the selected path. "
										+ "Maybe it is not the good version of Scilab (4.0) or the selected path is not correct."
										+ "If you find the file, you can add manually in it the following line:\n exec SCI/scimeta/dot.scilab");
				}

			}
			catch (IOException e)
			{
				ScicosExtractorPlugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, ScicosExtractorPlugin.PLUGIN_ID, IStatus.ERROR, e.getMessage(), e));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{}

}
