package fr.cs.leplessis.scicos.extractor;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ScicosExtractorPlugin extends AbstractUIPlugin
{
	// The plug-in ID
	public static final String	PLUGIN_ID	= "fr.cs.leplessis.scicos.extractor";

	// The shared instance
	private static ScicosExtractorPlugin	plugin;

	/**
	 * The constructor
	 */
	public ScicosExtractorPlugin()
	{}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ScicosExtractorPlugin getDefault()
	{
		return plugin;
	}

}
