/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.UserBlock#getFunctionType <em>Function Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getUserBlock()
 * @model
 * @generated
 */
public interface UserBlock extends Block {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Function Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.cs.leplessis.scicosMeta.ScsFunctionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.ScsFunctionType
	 * @see #setFunctionType(ScsFunctionType)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getUserBlock_FunctionType()
	 * @model
	 * @generated
	 */
	ScsFunctionType getFunctionType();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.UserBlock#getFunctionType <em>Function Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.ScsFunctionType
	 * @see #getFunctionType()
	 * @generated
	 */
	void setFunctionType(ScsFunctionType value);

} // UserBlock
