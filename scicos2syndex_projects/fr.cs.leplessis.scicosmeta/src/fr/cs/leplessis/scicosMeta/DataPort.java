/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.DataPort#getSize <em>Size</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.DataPort#getSizeType <em>Size Type</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.DataPort#getDataType <em>Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataPort()
 * @model
 * @generated
 */
public interface DataPort extends FlowPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(int)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataPort_Size()
	 * @model unique="false" required="true"
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.DataPort#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

	/**
	 * Returns the value of the '<em><b>Size Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.cs.leplessis.scicosMeta.SizeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.SizeType
	 * @see #setSizeType(SizeType)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataPort_SizeType()
	 * @model required="true"
	 * @generated
	 */
	SizeType getSizeType();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.DataPort#getSizeType <em>Size Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.SizeType
	 * @see #getSizeType()
	 * @generated
	 */
	void setSizeType(SizeType value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * The literals are from the enumeration {@link fr.cs.leplessis.scicosMeta.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.DataType
	 * @see #setDataType(DataType)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataPort_DataType()
	 * @model default=""
	 * @generated
	 */
	DataType getDataType();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.DataPort#getDataType <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' attribute.
	 * @see fr.cs.leplessis.scicosMeta.DataType
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataType value);

} // DataPort
