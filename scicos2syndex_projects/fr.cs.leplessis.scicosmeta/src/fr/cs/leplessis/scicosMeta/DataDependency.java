/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.DataDependency#getDst <em>Dst</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.DataDependency#getSrc <em>Src</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataDependency()
 * @model
 * @generated
 */
public interface DataDependency extends Dependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Dst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dst</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dst</em>' reference.
	 * @see #setDst(DataPort)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataDependency_Dst()
	 * @model required="true"
	 * @generated
	 */
	DataPort getDst();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.DataDependency#getDst <em>Dst</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dst</em>' reference.
	 * @see #getDst()
	 * @generated
	 */
	void setDst(DataPort value);

	/**
	 * Returns the value of the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' reference.
	 * @see #setSrc(DataPort)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataDependency_Src()
	 * @model required="true"
	 * @generated
	 */
	DataPort getSrc();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.DataDependency#getSrc <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(DataPort value);

} // DataDependency
