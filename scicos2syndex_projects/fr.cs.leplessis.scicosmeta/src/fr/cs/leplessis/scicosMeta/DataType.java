/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Data Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getDataType()
 * @model
 * @generated
 */
public enum DataType implements Enumerator {
	/**
	 * The '<em><b>Double Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_TYPE
	 * @generated
	 * @ordered
	 */
	DOUBLE_TYPE_LITERAL(0, "doubleType", "doubleType"),

	/**
	 * The '<em><b>Double Set Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_SET_TYPE
	 * @generated
	 * @ordered
	 */
	DOUBLE_SET_TYPE_LITERAL(1, "doubleSetType", "doubleSetType"),

	/**
	 * The '<em><b>String Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRING_TYPE
	 * @generated
	 * @ordered
	 */
	STRING_TYPE_LITERAL(2, "stringType", "stringType"),

	/**
	 * The '<em><b>Boolean Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_TYPE
	 * @generated
	 * @ordered
	 */
	BOOLEAN_TYPE_LITERAL(0, "booleanType", "booleanType"),

	/**
	 * The '<em><b>Integer Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTEGER_TYPE
	 * @generated
	 * @ordered
	 */
	INTEGER_TYPE_LITERAL(3, "integerType", "integerType");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * The '<em><b>Double Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Double Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_TYPE_LITERAL
	 * @model name="doubleType"
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_TYPE = 0;

	/**
	 * The '<em><b>Double Set Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Double Set Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_SET_TYPE_LITERAL
	 * @model name="doubleSetType"
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_SET_TYPE = 1;

	/**
	 * The '<em><b>String Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRING_TYPE_LITERAL
	 * @model name="stringType"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_TYPE = 2;

	/**
	 * The '<em><b>Boolean Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_TYPE_LITERAL
	 * @model name="booleanType"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_TYPE = 0;

	/**
	 * The '<em><b>Integer Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Integer Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTEGER_TYPE_LITERAL
	 * @model name="integerType"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGER_TYPE = 3;

	/**
	 * An array of all the '<em><b>Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DataType[] VALUES_ARRAY =
		new DataType[] {
			DOUBLE_TYPE_LITERAL,
			DOUBLE_SET_TYPE_LITERAL,
			STRING_TYPE_LITERAL,
			BOOLEAN_TYPE_LITERAL,
			INTEGER_TYPE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DataType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Data Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DataType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DataType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Data Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DataType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DataType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Data Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DataType get(int value) {
		switch (value) {
			case DOUBLE_TYPE: return DOUBLE_TYPE_LITERAL;
			case DOUBLE_SET_TYPE: return DOUBLE_SET_TYPE_LITERAL;
			case STRING_TYPE: return STRING_TYPE_LITERAL;
			case INTEGER_TYPE: return INTEGER_TYPE_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DataType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DataType
