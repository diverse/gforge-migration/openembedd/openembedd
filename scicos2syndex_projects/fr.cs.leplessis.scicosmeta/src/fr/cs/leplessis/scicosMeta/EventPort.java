/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getEventPort()
 * @model
 * @generated
 */
public interface EventPort extends FlowPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

} // EventPort
