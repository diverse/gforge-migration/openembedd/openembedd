/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationAbsTolerance <em>Integration Abs Tolerance</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationRelTolerance <em>Integration Rel Tolerance</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getToleranceOnTime <em>Tolerance On Time</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxIntegrationTimeInterval <em>Max Integration Time Interval</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getFinalIntegrationTime <em>Final Integration Time</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getContext <em>Context</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getRealTimeScaling <em>Real Time Scaling</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxStepSize <em>Max Step Size</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getParameterNames <em>Parameter Names</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getZVector <em>ZVector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getXtVector <em>Xt Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getXtdVector <em>Xtd Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getIparVector <em>Ipar Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getRparVector <em>Rpar Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.RootBlock#getRootScicosId <em>Root Scicos Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock()
 * @model
 * @generated
 */
public interface RootBlock extends OwnerElement, NamedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Integration Abs Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integration Abs Tolerance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integration Abs Tolerance</em>' attribute.
	 * @see #setIntegrationAbsTolerance(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_IntegrationAbsTolerance()
	 * @model
	 * @generated
	 */
	double getIntegrationAbsTolerance();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationAbsTolerance <em>Integration Abs Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integration Abs Tolerance</em>' attribute.
	 * @see #getIntegrationAbsTolerance()
	 * @generated
	 */
	void setIntegrationAbsTolerance(double value);

	/**
	 * Returns the value of the '<em><b>Integration Rel Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integration Rel Tolerance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integration Rel Tolerance</em>' attribute.
	 * @see #setIntegrationRelTolerance(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_IntegrationRelTolerance()
	 * @model
	 * @generated
	 */
	double getIntegrationRelTolerance();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationRelTolerance <em>Integration Rel Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integration Rel Tolerance</em>' attribute.
	 * @see #getIntegrationRelTolerance()
	 * @generated
	 */
	void setIntegrationRelTolerance(double value);

	/**
	 * Returns the value of the '<em><b>Tolerance On Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tolerance On Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tolerance On Time</em>' attribute.
	 * @see #setToleranceOnTime(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_ToleranceOnTime()
	 * @model
	 * @generated
	 */
	double getToleranceOnTime();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getToleranceOnTime <em>Tolerance On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tolerance On Time</em>' attribute.
	 * @see #getToleranceOnTime()
	 * @generated
	 */
	void setToleranceOnTime(double value);

	/**
	 * Returns the value of the '<em><b>Max Integration Time Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Integration Time Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Integration Time Interval</em>' attribute.
	 * @see #setMaxIntegrationTimeInterval(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_MaxIntegrationTimeInterval()
	 * @model
	 * @generated
	 */
	double getMaxIntegrationTimeInterval();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxIntegrationTimeInterval <em>Max Integration Time Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Integration Time Interval</em>' attribute.
	 * @see #getMaxIntegrationTimeInterval()
	 * @generated
	 */
	void setMaxIntegrationTimeInterval(double value);

	/**
	 * Returns the value of the '<em><b>Final Integration Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Integration Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Integration Time</em>' attribute.
	 * @see #setFinalIntegrationTime(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_FinalIntegrationTime()
	 * @model
	 * @generated
	 */
	double getFinalIntegrationTime();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getFinalIntegrationTime <em>Final Integration Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Integration Time</em>' attribute.
	 * @see #getFinalIntegrationTime()
	 * @generated
	 */
	void setFinalIntegrationTime(double value);

	/**
	 * Returns the value of the '<em><b>Context</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' attribute.
	 * @see #setContext(String)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_Context()
	 * @model
	 * @generated
	 */
	String getContext();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getContext <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' attribute.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(String value);

	/**
	 * Returns the value of the '<em><b>Real Time Scaling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Time Scaling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Time Scaling</em>' attribute.
	 * @see #setRealTimeScaling(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_RealTimeScaling()
	 * @model
	 * @generated
	 */
	double getRealTimeScaling();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getRealTimeScaling <em>Real Time Scaling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Time Scaling</em>' attribute.
	 * @see #getRealTimeScaling()
	 * @generated
	 */
	void setRealTimeScaling(double value);

	/**
	 * Returns the value of the '<em><b>Max Step Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Step Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Step Size</em>' attribute.
	 * @see #setMaxStepSize(double)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_MaxStepSize()
	 * @model
	 * @generated
	 */
	double getMaxStepSize();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxStepSize <em>Max Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Step Size</em>' attribute.
	 * @see #getMaxStepSize()
	 * @generated
	 */
	void setMaxStepSize(double value);

	/**
	 * Returns the value of the '<em><b>Parameter Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Names</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Names</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_ParameterNames()
	 * @model
	 * @generated
	 */
	EList<String> getParameterNames();

	/**
	 * Returns the value of the '<em><b>ZVector</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ZVector</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ZVector</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_ZVector()
	 * @model
	 * @generated
	 */
	EList<Double> getZVector();

	/**
	 * Returns the value of the '<em><b>Xt Vector</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xt Vector</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xt Vector</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_XtVector()
	 * @model
	 * @generated
	 */
	EList<Double> getXtVector();

	/**
	 * Returns the value of the '<em><b>Xtd Vector</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xtd Vector</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xtd Vector</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_XtdVector()
	 * @model
	 * @generated
	 */
	EList<Double> getXtdVector();

	/**
	 * Returns the value of the '<em><b>Ipar Vector</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ipar Vector</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ipar Vector</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_IparVector()
	 * @model
	 * @generated
	 */
	EList<Integer> getIparVector();

	/**
	 * Returns the value of the '<em><b>Rpar Vector</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rpar Vector</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rpar Vector</em>' attribute list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_RparVector()
	 * @model
	 * @generated
	 */
	EList<Double> getRparVector();

	/**
	 * Returns the value of the '<em><b>Root Scicos Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Scicos Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Scicos Id</em>' attribute.
	 * @see #setRootScicosId(int)
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getRootBlock_RootScicosId()
	 * @model
	 * @generated
	 */
	int getRootScicosId();

	/**
	 * Sets the value of the '{@link fr.cs.leplessis.scicosMeta.RootBlock#getRootScicosId <em>Root Scicos Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Scicos Id</em>' attribute.
	 * @see #getRootScicosId()
	 * @generated
	 */
	void setRootScicosId(int value);

} // RootBlock
