/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaFactory
 * @model kind="package"
 * @generated
 */
public interface ScicosMetaPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scicosMeta";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.cs.leplessis.scicosmeta/ScicosMeta/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sciMeta";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScicosMetaPackage eINSTANCE = fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.ScsElementImpl <em>Scs Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.ScsElementImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getScsElement()
	 * @generated
	 */
	int SCS_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Scs Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCS_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.OwnableElementImpl <em>Ownable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.OwnableElementImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getOwnableElement()
	 * @generated
	 */
	int OWNABLE_ELEMENT = 1;

	/**
	 * The number of structural features of the '<em>Ownable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNABLE_ELEMENT_FEATURE_COUNT = SCS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.OwnerElementImpl <em>Owner Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.OwnerElementImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getOwnerElement()
	 * @generated
	 */
	int OWNER_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Owned Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER_ELEMENT__OWNED_ELEMENTS = SCS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER_ELEMENT__DATA_DEPENDENCIES = SCS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER_ELEMENT__EVENT_DEPENDENCIES = SCS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Owner Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER_ELEMENT_FEATURE_COUNT = SCS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.FlowPortImpl <em>Flow Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.FlowPortImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getFlowPort()
	 * @generated
	 */
	int FLOW_PORT = 3;

	/**
	 * The number of structural features of the '<em>Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_PORT_FEATURE_COUNT = SCS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl <em>Interfaced Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getInterfacedElement()
	 * @generated
	 */
	int INTERFACED_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Output Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_ELEMENT__OUTPUT_DATA_PORTS = OWNABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS = OWNABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Input Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_ELEMENT__INPUT_EVENT_PORTS = OWNABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_ELEMENT__INPUT_DATA_PORTS = OWNABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Interfaced Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_ELEMENT_FEATURE_COUNT = OWNABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl <em>Super Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getSuperBlock()
	 * @generated
	 */
	int SUPER_BLOCK = 5;

	/**
	 * The feature id for the '<em><b>Output Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__OUTPUT_DATA_PORTS = INTERFACED_ELEMENT__OUTPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Output Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__OUTPUT_EVENT_PORTS = INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__INPUT_EVENT_PORTS = INTERFACED_ELEMENT__INPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__INPUT_DATA_PORTS = INTERFACED_ELEMENT__INPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Owned Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__OWNED_ELEMENTS = INTERFACED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__DATA_DEPENDENCIES = INTERFACED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK__EVENT_DEPENDENCIES = INTERFACED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Super Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_BLOCK_FEATURE_COUNT = INTERFACED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl <em>Root Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.RootBlockImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getRootBlock()
	 * @generated
	 */
	int ROOT_BLOCK = 6;

	/**
	 * The feature id for the '<em><b>Owned Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__OWNED_ELEMENTS = OWNER_ELEMENT__OWNED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Data Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__DATA_DEPENDENCIES = OWNER_ELEMENT__DATA_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Event Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__EVENT_DEPENDENCIES = OWNER_ELEMENT__EVENT_DEPENDENCIES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__NAME = OWNER_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integration Abs Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE = OWNER_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Integration Rel Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__INTEGRATION_REL_TOLERANCE = OWNER_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tolerance On Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__TOLERANCE_ON_TIME = OWNER_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Max Integration Time Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL = OWNER_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Final Integration Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__FINAL_INTEGRATION_TIME = OWNER_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Context</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__CONTEXT = OWNER_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Real Time Scaling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__REAL_TIME_SCALING = OWNER_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Max Step Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__MAX_STEP_SIZE = OWNER_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Parameter Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__PARAMETER_NAMES = OWNER_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>ZVector</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__ZVECTOR = OWNER_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Xt Vector</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__XT_VECTOR = OWNER_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Xtd Vector</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__XTD_VECTOR = OWNER_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Ipar Vector</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__IPAR_VECTOR = OWNER_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Rpar Vector</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__RPAR_VECTOR = OWNER_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Root Scicos Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK__ROOT_SCICOS_ID = OWNER_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The number of structural features of the '<em>Root Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_BLOCK_FEATURE_COUNT = OWNER_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.DataPortImpl <em>Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.DataPortImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataPort()
	 * @generated
	 */
	int DATA_PORT = 7;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PORT__SIZE = FLOW_PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Size Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PORT__SIZE_TYPE = FLOW_PORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PORT__DATA_TYPE = FLOW_PORT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PORT_FEATURE_COUNT = FLOW_PORT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.EventPortImpl <em>Event Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.EventPortImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getEventPort()
	 * @generated
	 */
	int EVENT_PORT = 8;

	/**
	 * The number of structural features of the '<em>Event Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PORT_FEATURE_COUNT = FLOW_PORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.DependencyImpl <em>Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.DependencyImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDependency()
	 * @generated
	 */
	int DEPENDENCY = 13;

	/**
	 * The number of structural features of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.DataDependencyImpl <em>Data Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.DataDependencyImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataDependency()
	 * @generated
	 */
	int DATA_DEPENDENCY = 9;

	/**
	 * The feature id for the '<em><b>Dst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_DEPENDENCY__DST = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_DEPENDENCY__SRC = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.EventDependencyImpl <em>Event Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.EventDependencyImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getEventDependency()
	 * @generated
	 */
	int EVENT_DEPENDENCY = 10;

	/**
	 * The feature id for the '<em><b>Dst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DEPENDENCY__DST = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DEPENDENCY__SRC = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Event Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.BlockImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 11;

	/**
	 * The feature id for the '<em><b>Output Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__OUTPUT_DATA_PORTS = INTERFACED_ELEMENT__OUTPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Output Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__OUTPUT_EVENT_PORTS = INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__INPUT_EVENT_PORTS = INTERFACED_ELEMENT__INPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__INPUT_DATA_PORTS = INTERFACED_ELEMENT__INPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__NAME = INTERFACED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Pointers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PARAMETER_POINTERS = INTERFACED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = INTERFACED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.NamedElementImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.impl.UserBlockImpl <em>User Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.impl.UserBlockImpl
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getUserBlock()
	 * @generated
	 */
	int USER_BLOCK = 14;

	/**
	 * The feature id for the '<em><b>Output Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__OUTPUT_DATA_PORTS = BLOCK__OUTPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Output Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__OUTPUT_EVENT_PORTS = BLOCK__OUTPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Event Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__INPUT_EVENT_PORTS = BLOCK__INPUT_EVENT_PORTS;

	/**
	 * The feature id for the '<em><b>Input Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__INPUT_DATA_PORTS = BLOCK__INPUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parameter Pointers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__PARAMETER_POINTERS = BLOCK__PARAMETER_POINTERS;

	/**
	 * The feature id for the '<em><b>Function Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK__FUNCTION_TYPE = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.DataType <em>Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.DataType
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 15;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.SizeType <em>Size Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.SizeType
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getSizeType()
	 * @generated
	 */
	int SIZE_TYPE = 16;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.ScsFunctionType <em>Scs Function Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.ScsFunctionType
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getScsFunctionType()
	 * @generated
	 */
	int SCS_FUNCTION_TYPE = 17;

	/**
	 * The meta object id for the '{@link fr.cs.leplessis.scicosMeta.PortDirection <em>Port Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.cs.leplessis.scicosMeta.PortDirection
	 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getPortDirection()
	 * @generated
	 */
	int PORT_DIRECTION = 18;


	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.ScsElement <em>Scs Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scs Element</em>'.
	 * @see fr.cs.leplessis.scicosMeta.ScsElement
	 * @generated
	 */
	EClass getScsElement();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.OwnableElement <em>Ownable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ownable Element</em>'.
	 * @see fr.cs.leplessis.scicosMeta.OwnableElement
	 * @generated
	 */
	EClass getOwnableElement();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.OwnerElement <em>Owner Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Owner Element</em>'.
	 * @see fr.cs.leplessis.scicosMeta.OwnerElement
	 * @generated
	 */
	EClass getOwnerElement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.OwnerElement#getOwnedElements <em>Owned Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Elements</em>'.
	 * @see fr.cs.leplessis.scicosMeta.OwnerElement#getOwnedElements()
	 * @see #getOwnerElement()
	 * @generated
	 */
	EReference getOwnerElement_OwnedElements();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.OwnerElement#getDataDependencies <em>Data Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Dependencies</em>'.
	 * @see fr.cs.leplessis.scicosMeta.OwnerElement#getDataDependencies()
	 * @see #getOwnerElement()
	 * @generated
	 */
	EReference getOwnerElement_DataDependencies();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.OwnerElement#getEventDependencies <em>Event Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Dependencies</em>'.
	 * @see fr.cs.leplessis.scicosMeta.OwnerElement#getEventDependencies()
	 * @see #getOwnerElement()
	 * @generated
	 */
	EReference getOwnerElement_EventDependencies();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.FlowPort <em>Flow Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Port</em>'.
	 * @see fr.cs.leplessis.scicosMeta.FlowPort
	 * @generated
	 */
	EClass getFlowPort();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.InterfacedElement <em>Interfaced Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interfaced Element</em>'.
	 * @see fr.cs.leplessis.scicosMeta.InterfacedElement
	 * @generated
	 */
	EClass getInterfacedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputDataPorts <em>Output Data Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Data Ports</em>'.
	 * @see fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputDataPorts()
	 * @see #getInterfacedElement()
	 * @generated
	 */
	EReference getInterfacedElement_OutputDataPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputEventPorts <em>Output Event Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Event Ports</em>'.
	 * @see fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputEventPorts()
	 * @see #getInterfacedElement()
	 * @generated
	 */
	EReference getInterfacedElement_OutputEventPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getInputEventPorts <em>Input Event Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Event Ports</em>'.
	 * @see fr.cs.leplessis.scicosMeta.InterfacedElement#getInputEventPorts()
	 * @see #getInterfacedElement()
	 * @generated
	 */
	EReference getInterfacedElement_InputEventPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getInputDataPorts <em>Input Data Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Data Ports</em>'.
	 * @see fr.cs.leplessis.scicosMeta.InterfacedElement#getInputDataPorts()
	 * @see #getInterfacedElement()
	 * @generated
	 */
	EReference getInterfacedElement_InputDataPorts();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.SuperBlock <em>Super Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Super Block</em>'.
	 * @see fr.cs.leplessis.scicosMeta.SuperBlock
	 * @generated
	 */
	EClass getSuperBlock();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.RootBlock <em>Root Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Root Block</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock
	 * @generated
	 */
	EClass getRootBlock();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationAbsTolerance <em>Integration Abs Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integration Abs Tolerance</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationAbsTolerance()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_IntegrationAbsTolerance();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationRelTolerance <em>Integration Rel Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integration Rel Tolerance</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getIntegrationRelTolerance()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_IntegrationRelTolerance();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getToleranceOnTime <em>Tolerance On Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tolerance On Time</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getToleranceOnTime()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_ToleranceOnTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxIntegrationTimeInterval <em>Max Integration Time Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Integration Time Interval</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getMaxIntegrationTimeInterval()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_MaxIntegrationTimeInterval();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getFinalIntegrationTime <em>Final Integration Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Integration Time</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getFinalIntegrationTime()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_FinalIntegrationTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getContext()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_Context();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getRealTimeScaling <em>Real Time Scaling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Time Scaling</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getRealTimeScaling()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_RealTimeScaling();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getMaxStepSize <em>Max Step Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Step Size</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getMaxStepSize()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_MaxStepSize();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getParameterNames <em>Parameter Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Parameter Names</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getParameterNames()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_ParameterNames();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getZVector <em>ZVector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>ZVector</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getZVector()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_ZVector();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getXtVector <em>Xt Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Xt Vector</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getXtVector()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_XtVector();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getXtdVector <em>Xtd Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Xtd Vector</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getXtdVector()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_XtdVector();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getIparVector <em>Ipar Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Ipar Vector</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getIparVector()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_IparVector();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.RootBlock#getRparVector <em>Rpar Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Rpar Vector</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getRparVector()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_RparVector();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.RootBlock#getRootScicosId <em>Root Scicos Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Root Scicos Id</em>'.
	 * @see fr.cs.leplessis.scicosMeta.RootBlock#getRootScicosId()
	 * @see #getRootBlock()
	 * @generated
	 */
	EAttribute getRootBlock_RootScicosId();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.DataPort <em>Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Port</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataPort
	 * @generated
	 */
	EClass getDataPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.DataPort#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataPort#getSize()
	 * @see #getDataPort()
	 * @generated
	 */
	EAttribute getDataPort_Size();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.DataPort#getSizeType <em>Size Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataPort#getSizeType()
	 * @see #getDataPort()
	 * @generated
	 */
	EAttribute getDataPort_SizeType();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.DataPort#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataPort#getDataType()
	 * @see #getDataPort()
	 * @generated
	 */
	EAttribute getDataPort_DataType();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.EventPort <em>Event Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Port</em>'.
	 * @see fr.cs.leplessis.scicosMeta.EventPort
	 * @generated
	 */
	EClass getEventPort();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.DataDependency <em>Data Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Dependency</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataDependency
	 * @generated
	 */
	EClass getDataDependency();

	/**
	 * Returns the meta object for the reference '{@link fr.cs.leplessis.scicosMeta.DataDependency#getDst <em>Dst</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dst</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataDependency#getDst()
	 * @see #getDataDependency()
	 * @generated
	 */
	EReference getDataDependency_Dst();

	/**
	 * Returns the meta object for the reference '{@link fr.cs.leplessis.scicosMeta.DataDependency#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataDependency#getSrc()
	 * @see #getDataDependency()
	 * @generated
	 */
	EReference getDataDependency_Src();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.EventDependency <em>Event Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Dependency</em>'.
	 * @see fr.cs.leplessis.scicosMeta.EventDependency
	 * @generated
	 */
	EClass getEventDependency();

	/**
	 * Returns the meta object for the reference '{@link fr.cs.leplessis.scicosMeta.EventDependency#getDst <em>Dst</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dst</em>'.
	 * @see fr.cs.leplessis.scicosMeta.EventDependency#getDst()
	 * @see #getEventDependency()
	 * @generated
	 */
	EReference getEventDependency_Dst();

	/**
	 * Returns the meta object for the reference '{@link fr.cs.leplessis.scicosMeta.EventDependency#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src</em>'.
	 * @see fr.cs.leplessis.scicosMeta.EventDependency#getSrc()
	 * @see #getEventDependency()
	 * @generated
	 */
	EReference getEventDependency_Src();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see fr.cs.leplessis.scicosMeta.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the attribute list '{@link fr.cs.leplessis.scicosMeta.Block#getParameterPointers <em>Parameter Pointers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Parameter Pointers</em>'.
	 * @see fr.cs.leplessis.scicosMeta.Block#getParameterPointers()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_ParameterPointers();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.cs.leplessis.scicosMeta.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.cs.leplessis.scicosMeta.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency</em>'.
	 * @see fr.cs.leplessis.scicosMeta.Dependency
	 * @generated
	 */
	EClass getDependency();

	/**
	 * Returns the meta object for class '{@link fr.cs.leplessis.scicosMeta.UserBlock <em>User Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Block</em>'.
	 * @see fr.cs.leplessis.scicosMeta.UserBlock
	 * @generated
	 */
	EClass getUserBlock();

	/**
	 * Returns the meta object for the attribute '{@link fr.cs.leplessis.scicosMeta.UserBlock#getFunctionType <em>Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Function Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.UserBlock#getFunctionType()
	 * @see #getUserBlock()
	 * @generated
	 */
	EAttribute getUserBlock_FunctionType();

	/**
	 * Returns the meta object for enum '{@link fr.cs.leplessis.scicosMeta.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.DataType
	 * @generated
	 */
	EEnum getDataType();

	/**
	 * Returns the meta object for enum '{@link fr.cs.leplessis.scicosMeta.SizeType <em>Size Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Size Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.SizeType
	 * @generated
	 */
	EEnum getSizeType();

	/**
	 * Returns the meta object for enum '{@link fr.cs.leplessis.scicosMeta.ScsFunctionType <em>Scs Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Scs Function Type</em>'.
	 * @see fr.cs.leplessis.scicosMeta.ScsFunctionType
	 * @generated
	 */
	EEnum getScsFunctionType();

	/**
	 * Returns the meta object for enum '{@link fr.cs.leplessis.scicosMeta.PortDirection <em>Port Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Port Direction</em>'.
	 * @see fr.cs.leplessis.scicosMeta.PortDirection
	 * @generated
	 */
	EEnum getPortDirection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScicosMetaFactory getScicosMetaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.ScsElementImpl <em>Scs Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.ScsElementImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getScsElement()
		 * @generated
		 */
		EClass SCS_ELEMENT = eINSTANCE.getScsElement();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.OwnableElementImpl <em>Ownable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.OwnableElementImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getOwnableElement()
		 * @generated
		 */
		EClass OWNABLE_ELEMENT = eINSTANCE.getOwnableElement();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.OwnerElementImpl <em>Owner Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.OwnerElementImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getOwnerElement()
		 * @generated
		 */
		EClass OWNER_ELEMENT = eINSTANCE.getOwnerElement();

		/**
		 * The meta object literal for the '<em><b>Owned Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OWNER_ELEMENT__OWNED_ELEMENTS = eINSTANCE.getOwnerElement_OwnedElements();

		/**
		 * The meta object literal for the '<em><b>Data Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OWNER_ELEMENT__DATA_DEPENDENCIES = eINSTANCE.getOwnerElement_DataDependencies();

		/**
		 * The meta object literal for the '<em><b>Event Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OWNER_ELEMENT__EVENT_DEPENDENCIES = eINSTANCE.getOwnerElement_EventDependencies();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.FlowPortImpl <em>Flow Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.FlowPortImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getFlowPort()
		 * @generated
		 */
		EClass FLOW_PORT = eINSTANCE.getFlowPort();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl <em>Interfaced Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getInterfacedElement()
		 * @generated
		 */
		EClass INTERFACED_ELEMENT = eINSTANCE.getInterfacedElement();

		/**
		 * The meta object literal for the '<em><b>Output Data Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_ELEMENT__OUTPUT_DATA_PORTS = eINSTANCE.getInterfacedElement_OutputDataPorts();

		/**
		 * The meta object literal for the '<em><b>Output Event Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS = eINSTANCE.getInterfacedElement_OutputEventPorts();

		/**
		 * The meta object literal for the '<em><b>Input Event Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_ELEMENT__INPUT_EVENT_PORTS = eINSTANCE.getInterfacedElement_InputEventPorts();

		/**
		 * The meta object literal for the '<em><b>Input Data Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_ELEMENT__INPUT_DATA_PORTS = eINSTANCE.getInterfacedElement_InputDataPorts();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl <em>Super Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getSuperBlock()
		 * @generated
		 */
		EClass SUPER_BLOCK = eINSTANCE.getSuperBlock();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl <em>Root Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.RootBlockImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getRootBlock()
		 * @generated
		 */
		EClass ROOT_BLOCK = eINSTANCE.getRootBlock();

		/**
		 * The meta object literal for the '<em><b>Integration Abs Tolerance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE = eINSTANCE.getRootBlock_IntegrationAbsTolerance();

		/**
		 * The meta object literal for the '<em><b>Integration Rel Tolerance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__INTEGRATION_REL_TOLERANCE = eINSTANCE.getRootBlock_IntegrationRelTolerance();

		/**
		 * The meta object literal for the '<em><b>Tolerance On Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__TOLERANCE_ON_TIME = eINSTANCE.getRootBlock_ToleranceOnTime();

		/**
		 * The meta object literal for the '<em><b>Max Integration Time Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL = eINSTANCE.getRootBlock_MaxIntegrationTimeInterval();

		/**
		 * The meta object literal for the '<em><b>Final Integration Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__FINAL_INTEGRATION_TIME = eINSTANCE.getRootBlock_FinalIntegrationTime();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__CONTEXT = eINSTANCE.getRootBlock_Context();

		/**
		 * The meta object literal for the '<em><b>Real Time Scaling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__REAL_TIME_SCALING = eINSTANCE.getRootBlock_RealTimeScaling();

		/**
		 * The meta object literal for the '<em><b>Max Step Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__MAX_STEP_SIZE = eINSTANCE.getRootBlock_MaxStepSize();

		/**
		 * The meta object literal for the '<em><b>Parameter Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__PARAMETER_NAMES = eINSTANCE.getRootBlock_ParameterNames();

		/**
		 * The meta object literal for the '<em><b>ZVector</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__ZVECTOR = eINSTANCE.getRootBlock_ZVector();

		/**
		 * The meta object literal for the '<em><b>Xt Vector</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__XT_VECTOR = eINSTANCE.getRootBlock_XtVector();

		/**
		 * The meta object literal for the '<em><b>Xtd Vector</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__XTD_VECTOR = eINSTANCE.getRootBlock_XtdVector();

		/**
		 * The meta object literal for the '<em><b>Ipar Vector</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__IPAR_VECTOR = eINSTANCE.getRootBlock_IparVector();

		/**
		 * The meta object literal for the '<em><b>Rpar Vector</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__RPAR_VECTOR = eINSTANCE.getRootBlock_RparVector();

		/**
		 * The meta object literal for the '<em><b>Root Scicos Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_BLOCK__ROOT_SCICOS_ID = eINSTANCE.getRootBlock_RootScicosId();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.DataPortImpl <em>Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.DataPortImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataPort()
		 * @generated
		 */
		EClass DATA_PORT = eINSTANCE.getDataPort();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_PORT__SIZE = eINSTANCE.getDataPort_Size();

		/**
		 * The meta object literal for the '<em><b>Size Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_PORT__SIZE_TYPE = eINSTANCE.getDataPort_SizeType();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_PORT__DATA_TYPE = eINSTANCE.getDataPort_DataType();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.EventPortImpl <em>Event Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.EventPortImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getEventPort()
		 * @generated
		 */
		EClass EVENT_PORT = eINSTANCE.getEventPort();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.DataDependencyImpl <em>Data Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.DataDependencyImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataDependency()
		 * @generated
		 */
		EClass DATA_DEPENDENCY = eINSTANCE.getDataDependency();

		/**
		 * The meta object literal for the '<em><b>Dst</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_DEPENDENCY__DST = eINSTANCE.getDataDependency_Dst();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_DEPENDENCY__SRC = eINSTANCE.getDataDependency_Src();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.EventDependencyImpl <em>Event Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.EventDependencyImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getEventDependency()
		 * @generated
		 */
		EClass EVENT_DEPENDENCY = eINSTANCE.getEventDependency();

		/**
		 * The meta object literal for the '<em><b>Dst</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_DEPENDENCY__DST = eINSTANCE.getEventDependency_Dst();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_DEPENDENCY__SRC = eINSTANCE.getEventDependency_Src();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.BlockImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Parameter Pointers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__PARAMETER_POINTERS = eINSTANCE.getBlock_ParameterPointers();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.NamedElementImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.DependencyImpl <em>Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.DependencyImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDependency()
		 * @generated
		 */
		EClass DEPENDENCY = eINSTANCE.getDependency();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.impl.UserBlockImpl <em>User Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.impl.UserBlockImpl
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getUserBlock()
		 * @generated
		 */
		EClass USER_BLOCK = eINSTANCE.getUserBlock();

		/**
		 * The meta object literal for the '<em><b>Function Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_BLOCK__FUNCTION_TYPE = eINSTANCE.getUserBlock_FunctionType();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.DataType <em>Data Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.DataType
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getDataType()
		 * @generated
		 */
		EEnum DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.SizeType <em>Size Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.SizeType
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getSizeType()
		 * @generated
		 */
		EEnum SIZE_TYPE = eINSTANCE.getSizeType();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.ScsFunctionType <em>Scs Function Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.ScsFunctionType
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getScsFunctionType()
		 * @generated
		 */
		EEnum SCS_FUNCTION_TYPE = eINSTANCE.getScsFunctionType();

		/**
		 * The meta object literal for the '{@link fr.cs.leplessis.scicosMeta.PortDirection <em>Port Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.cs.leplessis.scicosMeta.PortDirection
		 * @see fr.cs.leplessis.scicosMeta.impl.ScicosMetaPackageImpl#getPortDirection()
		 * @generated
		 */
		EEnum PORT_DIRECTION = eINSTANCE.getPortDirection();

	}

} //ScicosMetaPackage
