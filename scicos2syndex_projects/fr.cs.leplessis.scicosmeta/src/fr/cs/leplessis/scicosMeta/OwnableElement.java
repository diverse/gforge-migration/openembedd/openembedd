/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ownable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getOwnableElement()
 * @model abstract="true"
 * @generated
 */
public interface OwnableElement extends ScsElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

} // OwnableElement
