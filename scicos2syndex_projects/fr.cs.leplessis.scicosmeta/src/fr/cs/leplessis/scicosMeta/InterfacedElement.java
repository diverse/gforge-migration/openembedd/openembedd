/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interfaced Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputDataPorts <em>Output Data Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getOutputEventPorts <em>Output Event Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getInputEventPorts <em>Input Event Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.InterfacedElement#getInputDataPorts <em>Input Data Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getInterfacedElement()
 * @model abstract="true"
 * @generated
 */
public interface InterfacedElement extends OwnableElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Output Data Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.DataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Data Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Data Ports</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getInterfacedElement_OutputDataPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPort> getOutputDataPorts();

	/**
	 * Returns the value of the '<em><b>Output Event Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.EventPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Event Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Event Ports</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getInterfacedElement_OutputEventPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventPort> getOutputEventPorts();

	/**
	 * Returns the value of the '<em><b>Input Event Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.EventPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event Ports</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getInterfacedElement_InputEventPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventPort> getInputEventPorts();

	/**
	 * Returns the value of the '<em><b>Input Data Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.DataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Data Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Data Ports</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getInterfacedElement_InputDataPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPort> getInputDataPorts();

} // InterfacedElement
