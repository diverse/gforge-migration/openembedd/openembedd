/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owner Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.OwnerElement#getOwnedElements <em>Owned Elements</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.OwnerElement#getDataDependencies <em>Data Dependencies</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.OwnerElement#getEventDependencies <em>Event Dependencies</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getOwnerElement()
 * @model abstract="true"
 * @generated
 */
public interface OwnerElement extends ScsElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * Returns the value of the '<em><b>Owned Elements</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.OwnableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Elements</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getOwnerElement_OwnedElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<OwnableElement> getOwnedElements();

	/**
	 * Returns the value of the '<em><b>Data Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.DataDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Dependencies</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getOwnerElement_DataDependencies()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataDependency> getDataDependencies();

	/**
	 * Returns the value of the '<em><b>Event Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link fr.cs.leplessis.scicosMeta.EventDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Dependencies</em>' containment reference list.
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getOwnerElement_EventDependencies()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventDependency> getEventDependencies();

} // OwnerElement
