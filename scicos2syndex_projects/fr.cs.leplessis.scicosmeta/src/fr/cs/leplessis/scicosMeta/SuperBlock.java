/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Super Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getSuperBlock()
 * @model
 * @generated
 */
public interface SuperBlock extends InterfacedElement, OwnerElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

} // SuperBlock
