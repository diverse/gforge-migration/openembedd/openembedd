/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta.impl;

import fr.cs.leplessis.scicosMeta.DataDependency;
import fr.cs.leplessis.scicosMeta.EventDependency;
import fr.cs.leplessis.scicosMeta.OwnableElement;
import fr.cs.leplessis.scicosMeta.OwnerElement;
import fr.cs.leplessis.scicosMeta.ScicosMetaPackage;
import fr.cs.leplessis.scicosMeta.SuperBlock;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Super Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl#getOwnedElements <em>Owned Elements</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl#getDataDependencies <em>Data Dependencies</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.SuperBlockImpl#getEventDependencies <em>Event Dependencies</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SuperBlockImpl extends InterfacedElementImpl implements SuperBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * The cached value of the '{@link #getOwnedElements() <em>Owned Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<OwnableElement> ownedElements;

	/**
	 * The cached value of the '{@link #getDataDependencies() <em>Data Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<DataDependency> dataDependencies;

	/**
	 * The cached value of the '{@link #getEventDependencies() <em>Event Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<EventDependency> eventDependencies;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SuperBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScicosMetaPackage.Literals.SUPER_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OwnableElement> getOwnedElements() {
		if (ownedElements == null) {
			ownedElements = new EObjectContainmentEList<OwnableElement>(OwnableElement.class, this, ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS);
		}
		return ownedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataDependency> getDataDependencies() {
		if (dataDependencies == null) {
			dataDependencies = new EObjectContainmentEList<DataDependency>(DataDependency.class, this, ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES);
		}
		return dataDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventDependency> getEventDependencies() {
		if (eventDependencies == null) {
			eventDependencies = new EObjectContainmentEList<EventDependency>(EventDependency.class, this, ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES);
		}
		return eventDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS:
				return ((InternalEList<?>)getOwnedElements()).basicRemove(otherEnd, msgs);
			case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES:
				return ((InternalEList<?>)getDataDependencies()).basicRemove(otherEnd, msgs);
			case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES:
				return ((InternalEList<?>)getEventDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS:
				return getOwnedElements();
			case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES:
				return getDataDependencies();
			case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES:
				return getEventDependencies();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS:
				getOwnedElements().clear();
				getOwnedElements().addAll((Collection<? extends OwnableElement>)newValue);
				return;
			case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES:
				getDataDependencies().clear();
				getDataDependencies().addAll((Collection<? extends DataDependency>)newValue);
				return;
			case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES:
				getEventDependencies().clear();
				getEventDependencies().addAll((Collection<? extends EventDependency>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS:
				getOwnedElements().clear();
				return;
			case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES:
				getDataDependencies().clear();
				return;
			case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES:
				getEventDependencies().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS:
				return ownedElements != null && !ownedElements.isEmpty();
			case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES:
				return dataDependencies != null && !dataDependencies.isEmpty();
			case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES:
				return eventDependencies != null && !eventDependencies.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == OwnerElement.class) {
			switch (derivedFeatureID) {
				case ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS: return ScicosMetaPackage.OWNER_ELEMENT__OWNED_ELEMENTS;
				case ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES: return ScicosMetaPackage.OWNER_ELEMENT__DATA_DEPENDENCIES;
				case ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES: return ScicosMetaPackage.OWNER_ELEMENT__EVENT_DEPENDENCIES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == OwnerElement.class) {
			switch (baseFeatureID) {
				case ScicosMetaPackage.OWNER_ELEMENT__OWNED_ELEMENTS: return ScicosMetaPackage.SUPER_BLOCK__OWNED_ELEMENTS;
				case ScicosMetaPackage.OWNER_ELEMENT__DATA_DEPENDENCIES: return ScicosMetaPackage.SUPER_BLOCK__DATA_DEPENDENCIES;
				case ScicosMetaPackage.OWNER_ELEMENT__EVENT_DEPENDENCIES: return ScicosMetaPackage.SUPER_BLOCK__EVENT_DEPENDENCIES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //SuperBlockImpl
