/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta.impl;

import fr.cs.leplessis.scicosMeta.DataPort;
import fr.cs.leplessis.scicosMeta.EventPort;
import fr.cs.leplessis.scicosMeta.InterfacedElement;
import fr.cs.leplessis.scicosMeta.ScicosMetaPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interfaced Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl#getOutputDataPorts <em>Output Data Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl#getOutputEventPorts <em>Output Event Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl#getInputEventPorts <em>Input Event Ports</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.InterfacedElementImpl#getInputDataPorts <em>Input Data Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class InterfacedElementImpl extends OwnableElementImpl implements InterfacedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * The cached value of the '{@link #getOutputDataPorts() <em>Output Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDataPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPort> outputDataPorts;

	/**
	 * The cached value of the '{@link #getOutputEventPorts() <em>Output Event Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputEventPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<EventPort> outputEventPorts;

	/**
	 * The cached value of the '{@link #getInputEventPorts() <em>Input Event Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputEventPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<EventPort> inputEventPorts;

	/**
	 * The cached value of the '{@link #getInputDataPorts() <em>Input Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputDataPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPort> inputDataPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfacedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScicosMetaPackage.Literals.INTERFACED_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPort> getOutputDataPorts() {
		if (outputDataPorts == null) {
			outputDataPorts = new EObjectContainmentEList<DataPort>(DataPort.class, this, ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS);
		}
		return outputDataPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventPort> getOutputEventPorts() {
		if (outputEventPorts == null) {
			outputEventPorts = new EObjectContainmentEList<EventPort>(EventPort.class, this, ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS);
		}
		return outputEventPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventPort> getInputEventPorts() {
		if (inputEventPorts == null) {
			inputEventPorts = new EObjectContainmentEList<EventPort>(EventPort.class, this, ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS);
		}
		return inputEventPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPort> getInputDataPorts() {
		if (inputDataPorts == null) {
			inputDataPorts = new EObjectContainmentEList<DataPort>(DataPort.class, this, ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS);
		}
		return inputDataPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS:
				return ((InternalEList<?>)getOutputDataPorts()).basicRemove(otherEnd, msgs);
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS:
				return ((InternalEList<?>)getOutputEventPorts()).basicRemove(otherEnd, msgs);
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS:
				return ((InternalEList<?>)getInputEventPorts()).basicRemove(otherEnd, msgs);
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS:
				return ((InternalEList<?>)getInputDataPorts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS:
				return getOutputDataPorts();
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS:
				return getOutputEventPorts();
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS:
				return getInputEventPorts();
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS:
				return getInputDataPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS:
				getOutputDataPorts().clear();
				getOutputDataPorts().addAll((Collection<? extends DataPort>)newValue);
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS:
				getOutputEventPorts().clear();
				getOutputEventPorts().addAll((Collection<? extends EventPort>)newValue);
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS:
				getInputEventPorts().clear();
				getInputEventPorts().addAll((Collection<? extends EventPort>)newValue);
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS:
				getInputDataPorts().clear();
				getInputDataPorts().addAll((Collection<? extends DataPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS:
				getOutputDataPorts().clear();
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS:
				getOutputEventPorts().clear();
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS:
				getInputEventPorts().clear();
				return;
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS:
				getInputDataPorts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_DATA_PORTS:
				return outputDataPorts != null && !outputDataPorts.isEmpty();
			case ScicosMetaPackage.INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS:
				return outputEventPorts != null && !outputEventPorts.isEmpty();
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_EVENT_PORTS:
				return inputEventPorts != null && !inputEventPorts.isEmpty();
			case ScicosMetaPackage.INTERFACED_ELEMENT__INPUT_DATA_PORTS:
				return inputDataPorts != null && !inputDataPorts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InterfacedElementImpl
