/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta.impl;

import fr.cs.leplessis.scicosMeta.Block;
import fr.cs.leplessis.scicosMeta.DataDependency;
import fr.cs.leplessis.scicosMeta.DataPort;
import fr.cs.leplessis.scicosMeta.DataType;
import fr.cs.leplessis.scicosMeta.Dependency;
import fr.cs.leplessis.scicosMeta.EventDependency;
import fr.cs.leplessis.scicosMeta.EventPort;
import fr.cs.leplessis.scicosMeta.FlowPort;
import fr.cs.leplessis.scicosMeta.InterfacedElement;
import fr.cs.leplessis.scicosMeta.NamedElement;
import fr.cs.leplessis.scicosMeta.OwnableElement;
import fr.cs.leplessis.scicosMeta.OwnerElement;
import fr.cs.leplessis.scicosMeta.PortDirection;
import fr.cs.leplessis.scicosMeta.RootBlock;
import fr.cs.leplessis.scicosMeta.ScicosMetaFactory;
import fr.cs.leplessis.scicosMeta.ScicosMetaPackage;
import fr.cs.leplessis.scicosMeta.ScsElement;
import fr.cs.leplessis.scicosMeta.ScsFunctionType;
import fr.cs.leplessis.scicosMeta.SizeType;
import fr.cs.leplessis.scicosMeta.SuperBlock;
import fr.cs.leplessis.scicosMeta.UserBlock;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScicosMetaPackageImpl extends EPackageImpl implements ScicosMetaPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scsElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ownableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ownerElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flowPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfacedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass superBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rootBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sizeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum scsFunctionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portDirectionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScicosMetaPackageImpl() {
		super(eNS_URI, ScicosMetaFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScicosMetaPackage init() {
		if (isInited) return (ScicosMetaPackage)EPackage.Registry.INSTANCE.getEPackage(ScicosMetaPackage.eNS_URI);

		// Obtain or create and register package
		ScicosMetaPackageImpl theScicosMetaPackage = (ScicosMetaPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof ScicosMetaPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new ScicosMetaPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theScicosMetaPackage.createPackageContents();

		// Initialize created meta-data
		theScicosMetaPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theScicosMetaPackage.freeze();

		return theScicosMetaPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScsElement() {
		return scsElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOwnableElement() {
		return ownableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOwnerElement() {
		return ownerElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOwnerElement_OwnedElements() {
		return (EReference)ownerElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOwnerElement_DataDependencies() {
		return (EReference)ownerElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOwnerElement_EventDependencies() {
		return (EReference)ownerElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlowPort() {
		return flowPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfacedElement() {
		return interfacedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfacedElement_OutputDataPorts() {
		return (EReference)interfacedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfacedElement_OutputEventPorts() {
		return (EReference)interfacedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfacedElement_InputEventPorts() {
		return (EReference)interfacedElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfacedElement_InputDataPorts() {
		return (EReference)interfacedElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSuperBlock() {
		return superBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRootBlock() {
		return rootBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_IntegrationAbsTolerance() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_IntegrationRelTolerance() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_ToleranceOnTime() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_MaxIntegrationTimeInterval() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_FinalIntegrationTime() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_Context() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_RealTimeScaling() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_MaxStepSize() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_ParameterNames() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_ZVector() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_XtVector() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_XtdVector() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_IparVector() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_RparVector() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRootBlock_RootScicosId() {
		return (EAttribute)rootBlockEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataPort() {
		return dataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPort_Size() {
		return (EAttribute)dataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPort_SizeType() {
		return (EAttribute)dataPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPort_DataType() {
		return (EAttribute)dataPortEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventPort() {
		return eventPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataDependency() {
		return dataDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataDependency_Dst() {
		return (EReference)dataDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataDependency_Src() {
		return (EReference)dataDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventDependency() {
		return eventDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventDependency_Dst() {
		return (EReference)eventDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventDependency_Src() {
		return (EReference)eventDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlock() {
		return blockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_ParameterPointers() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserBlock() {
		return userBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserBlock_FunctionType() {
		return (EAttribute)userBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataType() {
		return dataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSizeType() {
		return sizeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getScsFunctionType() {
		return scsFunctionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortDirection() {
		return portDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScicosMetaFactory getScicosMetaFactory() {
		return (ScicosMetaFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scsElementEClass = createEClass(SCS_ELEMENT);

		ownableElementEClass = createEClass(OWNABLE_ELEMENT);

		ownerElementEClass = createEClass(OWNER_ELEMENT);
		createEReference(ownerElementEClass, OWNER_ELEMENT__OWNED_ELEMENTS);
		createEReference(ownerElementEClass, OWNER_ELEMENT__DATA_DEPENDENCIES);
		createEReference(ownerElementEClass, OWNER_ELEMENT__EVENT_DEPENDENCIES);

		flowPortEClass = createEClass(FLOW_PORT);

		interfacedElementEClass = createEClass(INTERFACED_ELEMENT);
		createEReference(interfacedElementEClass, INTERFACED_ELEMENT__OUTPUT_DATA_PORTS);
		createEReference(interfacedElementEClass, INTERFACED_ELEMENT__OUTPUT_EVENT_PORTS);
		createEReference(interfacedElementEClass, INTERFACED_ELEMENT__INPUT_EVENT_PORTS);
		createEReference(interfacedElementEClass, INTERFACED_ELEMENT__INPUT_DATA_PORTS);

		superBlockEClass = createEClass(SUPER_BLOCK);

		rootBlockEClass = createEClass(ROOT_BLOCK);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__INTEGRATION_REL_TOLERANCE);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__TOLERANCE_ON_TIME);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__FINAL_INTEGRATION_TIME);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__CONTEXT);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__REAL_TIME_SCALING);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__MAX_STEP_SIZE);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__PARAMETER_NAMES);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__ZVECTOR);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__XT_VECTOR);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__XTD_VECTOR);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__IPAR_VECTOR);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__RPAR_VECTOR);
		createEAttribute(rootBlockEClass, ROOT_BLOCK__ROOT_SCICOS_ID);

		dataPortEClass = createEClass(DATA_PORT);
		createEAttribute(dataPortEClass, DATA_PORT__SIZE);
		createEAttribute(dataPortEClass, DATA_PORT__SIZE_TYPE);
		createEAttribute(dataPortEClass, DATA_PORT__DATA_TYPE);

		eventPortEClass = createEClass(EVENT_PORT);

		dataDependencyEClass = createEClass(DATA_DEPENDENCY);
		createEReference(dataDependencyEClass, DATA_DEPENDENCY__DST);
		createEReference(dataDependencyEClass, DATA_DEPENDENCY__SRC);

		eventDependencyEClass = createEClass(EVENT_DEPENDENCY);
		createEReference(eventDependencyEClass, EVENT_DEPENDENCY__DST);
		createEReference(eventDependencyEClass, EVENT_DEPENDENCY__SRC);

		blockEClass = createEClass(BLOCK);
		createEAttribute(blockEClass, BLOCK__PARAMETER_POINTERS);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		dependencyEClass = createEClass(DEPENDENCY);

		userBlockEClass = createEClass(USER_BLOCK);
		createEAttribute(userBlockEClass, USER_BLOCK__FUNCTION_TYPE);

		// Create enums
		dataTypeEEnum = createEEnum(DATA_TYPE);
		sizeTypeEEnum = createEEnum(SIZE_TYPE);
		scsFunctionTypeEEnum = createEEnum(SCS_FUNCTION_TYPE);
		portDirectionEEnum = createEEnum(PORT_DIRECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		ownableElementEClass.getESuperTypes().add(this.getScsElement());
		ownerElementEClass.getESuperTypes().add(this.getScsElement());
		flowPortEClass.getESuperTypes().add(this.getScsElement());
		interfacedElementEClass.getESuperTypes().add(this.getOwnableElement());
		superBlockEClass.getESuperTypes().add(this.getInterfacedElement());
		superBlockEClass.getESuperTypes().add(this.getOwnerElement());
		rootBlockEClass.getESuperTypes().add(this.getOwnerElement());
		rootBlockEClass.getESuperTypes().add(this.getNamedElement());
		dataPortEClass.getESuperTypes().add(this.getFlowPort());
		eventPortEClass.getESuperTypes().add(this.getFlowPort());
		dataDependencyEClass.getESuperTypes().add(this.getDependency());
		eventDependencyEClass.getESuperTypes().add(this.getDependency());
		blockEClass.getESuperTypes().add(this.getInterfacedElement());
		blockEClass.getESuperTypes().add(this.getNamedElement());
		userBlockEClass.getESuperTypes().add(this.getBlock());

		// Initialize classes and features; add operations and parameters
		initEClass(scsElementEClass, ScsElement.class, "ScsElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ownableElementEClass, OwnableElement.class, "OwnableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ownerElementEClass, OwnerElement.class, "OwnerElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOwnerElement_OwnedElements(), this.getOwnableElement(), null, "ownedElements", null, 0, -1, OwnerElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOwnerElement_DataDependencies(), this.getDataDependency(), null, "dataDependencies", null, 0, -1, OwnerElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOwnerElement_EventDependencies(), this.getEventDependency(), null, "eventDependencies", null, 0, -1, OwnerElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(flowPortEClass, FlowPort.class, "FlowPort", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(interfacedElementEClass, InterfacedElement.class, "InterfacedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterfacedElement_OutputDataPorts(), this.getDataPort(), null, "outputDataPorts", null, 0, -1, InterfacedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfacedElement_OutputEventPorts(), this.getEventPort(), null, "outputEventPorts", null, 0, -1, InterfacedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfacedElement_InputEventPorts(), this.getEventPort(), null, "inputEventPorts", null, 0, -1, InterfacedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfacedElement_InputDataPorts(), this.getDataPort(), null, "inputDataPorts", null, 0, -1, InterfacedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(superBlockEClass, SuperBlock.class, "SuperBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rootBlockEClass, RootBlock.class, "RootBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRootBlock_IntegrationAbsTolerance(), ecorePackage.getEDouble(), "integrationAbsTolerance", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_IntegrationRelTolerance(), ecorePackage.getEDouble(), "integrationRelTolerance", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_ToleranceOnTime(), ecorePackage.getEDouble(), "toleranceOnTime", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_MaxIntegrationTimeInterval(), ecorePackage.getEDouble(), "maxIntegrationTimeInterval", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_FinalIntegrationTime(), ecorePackage.getEDouble(), "finalIntegrationTime", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_Context(), ecorePackage.getEString(), "context", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_RealTimeScaling(), ecorePackage.getEDouble(), "realTimeScaling", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_MaxStepSize(), ecorePackage.getEDouble(), "maxStepSize", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_ParameterNames(), ecorePackage.getEString(), "parameterNames", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_ZVector(), ecorePackage.getEDouble(), "zVector", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_XtVector(), ecorePackage.getEDouble(), "xtVector", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_XtdVector(), ecorePackage.getEDouble(), "xtdVector", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_IparVector(), ecorePackage.getEInt(), "iparVector", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_RparVector(), ecorePackage.getEDouble(), "rparVector", null, 0, -1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRootBlock_RootScicosId(), ecorePackage.getEInt(), "rootScicosId", null, 0, 1, RootBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataPortEClass, DataPort.class, "DataPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataPort_Size(), ecorePackage.getEInt(), "size", null, 1, 1, DataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataPort_SizeType(), this.getSizeType(), "sizeType", null, 1, 1, DataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataPort_DataType(), this.getDataType(), "dataType", "", 0, 1, DataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventPortEClass, EventPort.class, "EventPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataDependencyEClass, DataDependency.class, "DataDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataDependency_Dst(), this.getDataPort(), null, "dst", null, 1, 1, DataDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataDependency_Src(), this.getDataPort(), null, "src", null, 1, 1, DataDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventDependencyEClass, EventDependency.class, "EventDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventDependency_Dst(), this.getEventPort(), null, "dst", null, 1, 1, EventDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventDependency_Src(), this.getEventPort(), null, "src", null, 1, 1, EventDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blockEClass, Block.class, "Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlock_ParameterPointers(), ecorePackage.getEInt(), "parameterPointers", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dependencyEClass, Dependency.class, "Dependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(userBlockEClass, UserBlock.class, "UserBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserBlock_FunctionType(), this.getScsFunctionType(), "functionType", null, 0, 1, UserBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dataTypeEEnum, DataType.class, "DataType");
		addEEnumLiteral(dataTypeEEnum, DataType.DOUBLE_TYPE_LITERAL);
		addEEnumLiteral(dataTypeEEnum, DataType.DOUBLE_SET_TYPE_LITERAL);
		addEEnumLiteral(dataTypeEEnum, DataType.STRING_TYPE_LITERAL);
		addEEnumLiteral(dataTypeEEnum, DataType.BOOLEAN_TYPE_LITERAL);
		addEEnumLiteral(dataTypeEEnum, DataType.INTEGER_TYPE_LITERAL);

		initEEnum(sizeTypeEEnum, SizeType.class, "SizeType");
		addEEnumLiteral(sizeTypeEEnum, SizeType.ABSOLUTE_LITERAL);
		addEEnumLiteral(sizeTypeEEnum, SizeType.DERIVED_LITERAL);

		initEEnum(scsFunctionTypeEEnum, ScsFunctionType.class, "ScsFunctionType");
		addEEnumLiteral(scsFunctionTypeEEnum, ScsFunctionType.TYPE1_LITERAL);
		addEEnumLiteral(scsFunctionTypeEEnum, ScsFunctionType.TYPE2_LITERAL);
		addEEnumLiteral(scsFunctionTypeEEnum, ScsFunctionType.TYPE3_LITERAL);
		addEEnumLiteral(scsFunctionTypeEEnum, ScsFunctionType.TYPE4_LITERAL);

		initEEnum(portDirectionEEnum, PortDirection.class, "PortDirection");
		addEEnumLiteral(portDirectionEEnum, PortDirection.IN_LITERAL);
		addEEnumLiteral(portDirectionEEnum, PortDirection.OUT_LITERAL);

		// Create resource
		createResource(eNS_URI);
	}

} //ScicosMetaPackageImpl
