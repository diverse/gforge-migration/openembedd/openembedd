/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta.impl;

import fr.cs.leplessis.scicosMeta.NamedElement;
import fr.cs.leplessis.scicosMeta.RootBlock;
import fr.cs.leplessis.scicosMeta.ScicosMetaPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Root Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getIntegrationAbsTolerance <em>Integration Abs Tolerance</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getIntegrationRelTolerance <em>Integration Rel Tolerance</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getToleranceOnTime <em>Tolerance On Time</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getMaxIntegrationTimeInterval <em>Max Integration Time Interval</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getFinalIntegrationTime <em>Final Integration Time</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getContext <em>Context</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getRealTimeScaling <em>Real Time Scaling</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getMaxStepSize <em>Max Step Size</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getParameterNames <em>Parameter Names</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getZVector <em>ZVector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getXtVector <em>Xt Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getXtdVector <em>Xtd Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getIparVector <em>Ipar Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getRparVector <em>Rpar Vector</em>}</li>
 *   <li>{@link fr.cs.leplessis.scicosMeta.impl.RootBlockImpl#getRootScicosId <em>Root Scicos Id</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RootBlockImpl extends OwnerElementImpl implements RootBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegrationAbsTolerance() <em>Integration Abs Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrationAbsTolerance()
	 * @generated
	 * @ordered
	 */
	protected static final double INTEGRATION_ABS_TOLERANCE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getIntegrationAbsTolerance() <em>Integration Abs Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrationAbsTolerance()
	 * @generated
	 * @ordered
	 */
	protected double integrationAbsTolerance = INTEGRATION_ABS_TOLERANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegrationRelTolerance() <em>Integration Rel Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrationRelTolerance()
	 * @generated
	 * @ordered
	 */
	protected static final double INTEGRATION_REL_TOLERANCE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getIntegrationRelTolerance() <em>Integration Rel Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegrationRelTolerance()
	 * @generated
	 * @ordered
	 */
	protected double integrationRelTolerance = INTEGRATION_REL_TOLERANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getToleranceOnTime() <em>Tolerance On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToleranceOnTime()
	 * @generated
	 * @ordered
	 */
	protected static final double TOLERANCE_ON_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getToleranceOnTime() <em>Tolerance On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToleranceOnTime()
	 * @generated
	 * @ordered
	 */
	protected double toleranceOnTime = TOLERANCE_ON_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxIntegrationTimeInterval() <em>Max Integration Time Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIntegrationTimeInterval()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_INTEGRATION_TIME_INTERVAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMaxIntegrationTimeInterval() <em>Max Integration Time Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIntegrationTimeInterval()
	 * @generated
	 * @ordered
	 */
	protected double maxIntegrationTimeInterval = MAX_INTEGRATION_TIME_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalIntegrationTime() <em>Final Integration Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalIntegrationTime()
	 * @generated
	 * @ordered
	 */
	protected static final double FINAL_INTEGRATION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFinalIntegrationTime() <em>Final Integration Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalIntegrationTime()
	 * @generated
	 * @ordered
	 */
	protected double finalIntegrationTime = FINAL_INTEGRATION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContext() <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected String context = CONTEXT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRealTimeScaling() <em>Real Time Scaling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealTimeScaling()
	 * @generated
	 * @ordered
	 */
	protected static final double REAL_TIME_SCALING_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getRealTimeScaling() <em>Real Time Scaling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealTimeScaling()
	 * @generated
	 * @ordered
	 */
	protected double realTimeScaling = REAL_TIME_SCALING_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxStepSize() <em>Max Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxStepSize()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_STEP_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMaxStepSize() <em>Max Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxStepSize()
	 * @generated
	 * @ordered
	 */
	protected double maxStepSize = MAX_STEP_SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameterNames() <em>Parameter Names</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterNames()
	 * @generated
	 * @ordered
	 */
	protected EList<String> parameterNames;

	/**
	 * The cached value of the '{@link #getZVector() <em>ZVector</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZVector()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> zVector;

	/**
	 * The cached value of the '{@link #getXtVector() <em>Xt Vector</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXtVector()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> xtVector;

	/**
	 * The cached value of the '{@link #getXtdVector() <em>Xtd Vector</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXtdVector()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> xtdVector;

	/**
	 * The cached value of the '{@link #getIparVector() <em>Ipar Vector</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIparVector()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> iparVector;

	/**
	 * The cached value of the '{@link #getRparVector() <em>Rpar Vector</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRparVector()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> rparVector;

	/**
	 * The default value of the '{@link #getRootScicosId() <em>Root Scicos Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootScicosId()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOT_SCICOS_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRootScicosId() <em>Root Scicos Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootScicosId()
	 * @generated
	 * @ordered
	 */
	protected int rootScicosId = ROOT_SCICOS_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RootBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScicosMetaPackage.Literals.ROOT_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getIntegrationAbsTolerance() {
		return integrationAbsTolerance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegrationAbsTolerance(double newIntegrationAbsTolerance) {
		double oldIntegrationAbsTolerance = integrationAbsTolerance;
		integrationAbsTolerance = newIntegrationAbsTolerance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE, oldIntegrationAbsTolerance, integrationAbsTolerance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getIntegrationRelTolerance() {
		return integrationRelTolerance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegrationRelTolerance(double newIntegrationRelTolerance) {
		double oldIntegrationRelTolerance = integrationRelTolerance;
		integrationRelTolerance = newIntegrationRelTolerance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE, oldIntegrationRelTolerance, integrationRelTolerance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getToleranceOnTime() {
		return toleranceOnTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToleranceOnTime(double newToleranceOnTime) {
		double oldToleranceOnTime = toleranceOnTime;
		toleranceOnTime = newToleranceOnTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME, oldToleranceOnTime, toleranceOnTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMaxIntegrationTimeInterval() {
		return maxIntegrationTimeInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxIntegrationTimeInterval(double newMaxIntegrationTimeInterval) {
		double oldMaxIntegrationTimeInterval = maxIntegrationTimeInterval;
		maxIntegrationTimeInterval = newMaxIntegrationTimeInterval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL, oldMaxIntegrationTimeInterval, maxIntegrationTimeInterval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFinalIntegrationTime() {
		return finalIntegrationTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalIntegrationTime(double newFinalIntegrationTime) {
		double oldFinalIntegrationTime = finalIntegrationTime;
		finalIntegrationTime = newFinalIntegrationTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME, oldFinalIntegrationTime, finalIntegrationTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContext() {
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(String newContext) {
		String oldContext = context;
		context = newContext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__CONTEXT, oldContext, context));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getRealTimeScaling() {
		return realTimeScaling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealTimeScaling(double newRealTimeScaling) {
		double oldRealTimeScaling = realTimeScaling;
		realTimeScaling = newRealTimeScaling;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING, oldRealTimeScaling, realTimeScaling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMaxStepSize() {
		return maxStepSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxStepSize(double newMaxStepSize) {
		double oldMaxStepSize = maxStepSize;
		maxStepSize = newMaxStepSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE, oldMaxStepSize, maxStepSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getParameterNames() {
		if (parameterNames == null) {
			parameterNames = new EDataTypeUniqueEList<String>(String.class, this, ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES);
		}
		return parameterNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getZVector() {
		if (zVector == null) {
			zVector = new EDataTypeUniqueEList<Double>(Double.class, this, ScicosMetaPackage.ROOT_BLOCK__ZVECTOR);
		}
		return zVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getXtVector() {
		if (xtVector == null) {
			xtVector = new EDataTypeUniqueEList<Double>(Double.class, this, ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR);
		}
		return xtVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getXtdVector() {
		if (xtdVector == null) {
			xtdVector = new EDataTypeUniqueEList<Double>(Double.class, this, ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR);
		}
		return xtdVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getIparVector() {
		if (iparVector == null) {
			iparVector = new EDataTypeUniqueEList<Integer>(Integer.class, this, ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR);
		}
		return iparVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getRparVector() {
		if (rparVector == null) {
			rparVector = new EDataTypeUniqueEList<Double>(Double.class, this, ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR);
		}
		return rparVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRootScicosId() {
		return rootScicosId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootScicosId(int newRootScicosId) {
		int oldRootScicosId = rootScicosId;
		rootScicosId = newRootScicosId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID, oldRootScicosId, rootScicosId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScicosMetaPackage.ROOT_BLOCK__NAME:
				return getName();
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE:
				return new Double(getIntegrationAbsTolerance());
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE:
				return new Double(getIntegrationRelTolerance());
			case ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME:
				return new Double(getToleranceOnTime());
			case ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL:
				return new Double(getMaxIntegrationTimeInterval());
			case ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME:
				return new Double(getFinalIntegrationTime());
			case ScicosMetaPackage.ROOT_BLOCK__CONTEXT:
				return getContext();
			case ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING:
				return new Double(getRealTimeScaling());
			case ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE:
				return new Double(getMaxStepSize());
			case ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES:
				return getParameterNames();
			case ScicosMetaPackage.ROOT_BLOCK__ZVECTOR:
				return getZVector();
			case ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR:
				return getXtVector();
			case ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR:
				return getXtdVector();
			case ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR:
				return getIparVector();
			case ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR:
				return getRparVector();
			case ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID:
				return new Integer(getRootScicosId());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScicosMetaPackage.ROOT_BLOCK__NAME:
				setName((String)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE:
				setIntegrationAbsTolerance(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE:
				setIntegrationRelTolerance(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME:
				setToleranceOnTime(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL:
				setMaxIntegrationTimeInterval(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME:
				setFinalIntegrationTime(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__CONTEXT:
				setContext((String)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING:
				setRealTimeScaling(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE:
				setMaxStepSize(((Double)newValue).doubleValue());
				return;
			case ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES:
				getParameterNames().clear();
				getParameterNames().addAll((Collection<? extends String>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__ZVECTOR:
				getZVector().clear();
				getZVector().addAll((Collection<? extends Double>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR:
				getXtVector().clear();
				getXtVector().addAll((Collection<? extends Double>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR:
				getXtdVector().clear();
				getXtdVector().addAll((Collection<? extends Double>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR:
				getIparVector().clear();
				getIparVector().addAll((Collection<? extends Integer>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR:
				getRparVector().clear();
				getRparVector().addAll((Collection<? extends Double>)newValue);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID:
				setRootScicosId(((Integer)newValue).intValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.ROOT_BLOCK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE:
				setIntegrationAbsTolerance(INTEGRATION_ABS_TOLERANCE_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE:
				setIntegrationRelTolerance(INTEGRATION_REL_TOLERANCE_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME:
				setToleranceOnTime(TOLERANCE_ON_TIME_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL:
				setMaxIntegrationTimeInterval(MAX_INTEGRATION_TIME_INTERVAL_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME:
				setFinalIntegrationTime(FINAL_INTEGRATION_TIME_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__CONTEXT:
				setContext(CONTEXT_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING:
				setRealTimeScaling(REAL_TIME_SCALING_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE:
				setMaxStepSize(MAX_STEP_SIZE_EDEFAULT);
				return;
			case ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES:
				getParameterNames().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__ZVECTOR:
				getZVector().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR:
				getXtVector().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR:
				getXtdVector().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR:
				getIparVector().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR:
				getRparVector().clear();
				return;
			case ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID:
				setRootScicosId(ROOT_SCICOS_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScicosMetaPackage.ROOT_BLOCK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE:
				return integrationAbsTolerance != INTEGRATION_ABS_TOLERANCE_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE:
				return integrationRelTolerance != INTEGRATION_REL_TOLERANCE_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME:
				return toleranceOnTime != TOLERANCE_ON_TIME_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL:
				return maxIntegrationTimeInterval != MAX_INTEGRATION_TIME_INTERVAL_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME:
				return finalIntegrationTime != FINAL_INTEGRATION_TIME_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__CONTEXT:
				return CONTEXT_EDEFAULT == null ? context != null : !CONTEXT_EDEFAULT.equals(context);
			case ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING:
				return realTimeScaling != REAL_TIME_SCALING_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE:
				return maxStepSize != MAX_STEP_SIZE_EDEFAULT;
			case ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES:
				return parameterNames != null && !parameterNames.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__ZVECTOR:
				return zVector != null && !zVector.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR:
				return xtVector != null && !xtVector.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR:
				return xtdVector != null && !xtdVector.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR:
				return iparVector != null && !iparVector.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR:
				return rparVector != null && !rparVector.isEmpty();
			case ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID:
				return rootScicosId != ROOT_SCICOS_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case ScicosMetaPackage.ROOT_BLOCK__NAME: return ScicosMetaPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case ScicosMetaPackage.NAMED_ELEMENT__NAME: return ScicosMetaPackage.ROOT_BLOCK__NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", integrationAbsTolerance: ");
		result.append(integrationAbsTolerance);
		result.append(", integrationRelTolerance: ");
		result.append(integrationRelTolerance);
		result.append(", toleranceOnTime: ");
		result.append(toleranceOnTime);
		result.append(", maxIntegrationTimeInterval: ");
		result.append(maxIntegrationTimeInterval);
		result.append(", finalIntegrationTime: ");
		result.append(finalIntegrationTime);
		result.append(", context: ");
		result.append(context);
		result.append(", realTimeScaling: ");
		result.append(realTimeScaling);
		result.append(", maxStepSize: ");
		result.append(maxStepSize);
		result.append(", parameterNames: ");
		result.append(parameterNames);
		result.append(", zVector: ");
		result.append(zVector);
		result.append(", xtVector: ");
		result.append(xtVector);
		result.append(", xtdVector: ");
		result.append(xtdVector);
		result.append(", iparVector: ");
		result.append(iparVector);
		result.append(", rparVector: ");
		result.append(rparVector);
		result.append(", rootScicosId: ");
		result.append(rootScicosId);
		result.append(')');
		return result.toString();
	}

} //RootBlockImpl
