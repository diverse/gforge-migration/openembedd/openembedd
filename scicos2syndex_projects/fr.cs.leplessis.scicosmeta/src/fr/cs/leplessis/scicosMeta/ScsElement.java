/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scs Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.cs.leplessis.scicosMeta.ScicosMetaPackage#getScsElement()
 * @model abstract="true"
 * @generated
 */
public interface ScsElement extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

} // ScsElement
