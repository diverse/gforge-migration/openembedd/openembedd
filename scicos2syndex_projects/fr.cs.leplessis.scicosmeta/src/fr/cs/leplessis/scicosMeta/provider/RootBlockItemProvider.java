/**
 * The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI
 *
 * $Id$
 */
package fr.cs.leplessis.scicosMeta.provider;


import fr.cs.leplessis.scicosMeta.RootBlock;
import fr.cs.leplessis.scicosMeta.ScicosMetaPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.cs.leplessis.scicosMeta.RootBlock} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RootBlockItemProvider
	extends OwnerElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "The ScicosMeta plugin and the Scicos meta model are the sole property of CS SI";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootBlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addIntegrationAbsTolerancePropertyDescriptor(object);
			addIntegrationRelTolerancePropertyDescriptor(object);
			addToleranceOnTimePropertyDescriptor(object);
			addMaxIntegrationTimeIntervalPropertyDescriptor(object);
			addFinalIntegrationTimePropertyDescriptor(object);
			addContextPropertyDescriptor(object);
			addRealTimeScalingPropertyDescriptor(object);
			addMaxStepSizePropertyDescriptor(object);
			addParameterNamesPropertyDescriptor(object);
			addZVectorPropertyDescriptor(object);
			addXtVectorPropertyDescriptor(object);
			addXtdVectorPropertyDescriptor(object);
			addIparVectorPropertyDescriptor(object);
			addRparVectorPropertyDescriptor(object);
			addRootScicosIdPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_NamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_NamedElement_name_feature", "_UI_NamedElement_type"),
				 ScicosMetaPackage.Literals.NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Integration Abs Tolerance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegrationAbsTolerancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_integrationAbsTolerance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_integrationAbsTolerance_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Integration Rel Tolerance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegrationRelTolerancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_integrationRelTolerance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_integrationRelTolerance_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tolerance On Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToleranceOnTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_toleranceOnTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_toleranceOnTime_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__TOLERANCE_ON_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Integration Time Interval feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxIntegrationTimeIntervalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_maxIntegrationTimeInterval_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_maxIntegrationTimeInterval_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Final Integration Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFinalIntegrationTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_finalIntegrationTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_finalIntegrationTime_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__FINAL_INTEGRATION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContextPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_context_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_context_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__CONTEXT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Real Time Scaling feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRealTimeScalingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_realTimeScaling_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_realTimeScaling_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__REAL_TIME_SCALING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Step Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxStepSizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_maxStepSize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_maxStepSize_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__MAX_STEP_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parameter Names feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterNamesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_parameterNames_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_parameterNames_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__PARAMETER_NAMES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the ZVector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addZVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_zVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_zVector_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__ZVECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Xt Vector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addXtVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_xtVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_xtVector_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__XT_VECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Xtd Vector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addXtdVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_xtdVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_xtdVector_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__XTD_VECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ipar Vector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIparVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_iparVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_iparVector_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__IPAR_VECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rpar Vector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRparVectorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_rparVector_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_rparVector_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__RPAR_VECTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Root Scicos Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootScicosIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RootBlock_rootScicosId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RootBlock_rootScicosId_feature", "_UI_RootBlock_type"),
				 ScicosMetaPackage.Literals.ROOT_BLOCK__ROOT_SCICOS_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns RootBlock.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RootBlock"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RootBlock)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_RootBlock_type") :
			getString("_UI_RootBlock_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RootBlock.class)) {
			case ScicosMetaPackage.ROOT_BLOCK__NAME:
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_ABS_TOLERANCE:
			case ScicosMetaPackage.ROOT_BLOCK__INTEGRATION_REL_TOLERANCE:
			case ScicosMetaPackage.ROOT_BLOCK__TOLERANCE_ON_TIME:
			case ScicosMetaPackage.ROOT_BLOCK__MAX_INTEGRATION_TIME_INTERVAL:
			case ScicosMetaPackage.ROOT_BLOCK__FINAL_INTEGRATION_TIME:
			case ScicosMetaPackage.ROOT_BLOCK__CONTEXT:
			case ScicosMetaPackage.ROOT_BLOCK__REAL_TIME_SCALING:
			case ScicosMetaPackage.ROOT_BLOCK__MAX_STEP_SIZE:
			case ScicosMetaPackage.ROOT_BLOCK__PARAMETER_NAMES:
			case ScicosMetaPackage.ROOT_BLOCK__ZVECTOR:
			case ScicosMetaPackage.ROOT_BLOCK__XT_VECTOR:
			case ScicosMetaPackage.ROOT_BLOCK__XTD_VECTOR:
			case ScicosMetaPackage.ROOT_BLOCK__IPAR_VECTOR:
			case ScicosMetaPackage.ROOT_BLOCK__RPAR_VECTOR:
			case ScicosMetaPackage.ROOT_BLOCK__ROOT_SCICOS_ID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
