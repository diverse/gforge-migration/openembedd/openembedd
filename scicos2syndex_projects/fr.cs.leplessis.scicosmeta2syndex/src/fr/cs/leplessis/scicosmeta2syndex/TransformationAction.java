
package fr.cs.leplessis.scicosmeta2syndex;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.kermeta.interpreter.api.Interpreter;
import org.kermeta.interpreter.api.InterpreterMode;

import fr.irisa.triskell.eclipse.console.EclipseConsole;
import fr.irisa.triskell.eclipse.console.IOConsole;

public class TransformationAction implements IObjectActionDelegate {
	
	/** Path to the model file */
	private IFile scicosModelFile = null;

	/**
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// Nothing to do
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action){
		
		if (scicosModelFile != null) {
			IOConsole console = new EclipseConsole("Scicos to SynDEx model transformation");
	        
			Interpreter interpreter;
			
			try {
				IPath syndexModelPath = scicosModelFile.getFullPath().removeFileExtension().addFileExtension("syndex");
				
				
				interpreter = new Interpreter("platform:/plugin/fr.cs.leplessis.scicosmeta2syndex/transformations/scicos2syndex.kmt",
												InterpreterMode.RUN, null);
				interpreter.setStreams(console);       
				interpreter.setEntryPoint("scs2sdx::Main", "main");
				
				String[] parameters = new String[2];
				parameters[0] = "platform:/resource" + scicosModelFile.getFullPath().toString();
				IFile syndexModelFile = ResourcesPlugin.getWorkspace().getRoot().getFile(syndexModelPath);
				parameters[1] = "platform:/resource" + syndexModelFile.getFullPath().toString();
				
				interpreter.setParameters(parameters);
				
			    interpreter.launch();
				try
				{
					scicosModelFile.getParent().refreshLocal(2, null);
				}
				catch (CoreException ce)
				{
					ce.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println ("No model");
		}
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		IStructuredSelection structured = (IStructuredSelection) selection;
		Object object = structured.getFirstElement();
		if (object instanceof IAdaptable) {
			object = ((IAdaptable) object).getAdapter(IResource.class);
		}
		// Retrieves the path of the model file
		if (object != null && object instanceof IResource) {
			IResource res = (IResource) object;
			if (res instanceof IFile) {
				scicosModelFile = (IFile) res;
			} else {
				scicosModelFile = null;
			}
		}
	}
}

