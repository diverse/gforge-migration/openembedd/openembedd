package org.openembedd.testing.kermeta.editor;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	UseKermetaEditor.class
})
public class KermetaEditorSuite {

}
