package org.openembedd.testing.kermeta;

public interface Constants extends org.openembedd.testing.utils.Constants {

	public static String TEST_PROJECT_NAME = "org.kermeta.test.ordinary.project";
	public static String KERMETA_SOURCE_FOLDER_NAME = "kermeta";
	public static String KERMETA_SOURCE_EXTENSION = ".kmt";
	
	public static String SOURCE_FILE_NAME = "editorTest";
}
