package org.openembedd.testing.kermeta.editor;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.internal.ui.views.console.ProcessConsole;
import org.eclipse.swt.SWT;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openembedd.testing.kermeta.Constants;
import org.openembedd.testing.utils.ExamplesTestTool;
import org.openembedd.testing.utils.TestUtils;
import org.openembedd.tests.utils.UiTools;

import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.eclipse.finder.SWTEclipseBot;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotEclipseEditor;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.widgets.TimeoutException;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class UseKermetaEditor implements Constants {

	static TestUtils utils;
	static ExamplesTestTool tool;
	
	protected SWTEclipseBot bot = new SWTEclipseBot();
	
	@BeforeClass public static void setKermetaEnvironment() throws WidgetNotFoundException, InterruptedException {
		utils = new TestUtils();
		utils.closeWelcome();
		UiTools.cleanProject(TEST_PROJECT_NAME);
		
		new UseKermetaEditor().setEnvironment();
	}
	
	private void setEnvironment() throws WidgetNotFoundException, InterruptedException {
		// increase timeout as some Kermeta & Eclipse actions are quite long
//		System.setProperty("net.sf.swtbot.search.timeout", "5000");
		
		// we need a [ordinary) project in order to use its editor
		bot.menu(FILE_MENU_TEXT).menu(NEW_MENU_TEXT).menu("Project...").click();
		SWTBotTreeItem generalitem = bot.tree().getTreeItem("General");
		generalitem.expand();	// to unfold the sub-tree
		generalitem.getNode("Project").select();
		bot.button(NEXT_BUTTON_TEXT).click();
		bot.textWithLabel("Project name:").setText(TEST_PROJECT_NAME);
		bot.button(FINISH_BUTTON_TEXT).click();
		
		// we also need a folder for source files
		bot.menu(FILE_MENU_TEXT).menu(NEW_MENU_TEXT).menu("Folder").click();
//		bot.textWithLabel("Enter or select the parent folder:").setText(TEST_PROJECT_NAME);
		bot.textWithLabel("Folder name:").setText(KERMETA_SOURCE_FOLDER_NAME);
		bot.button(FINISH_BUTTON_TEXT).click();
	}
	
	/**
	 * any OpenEmbeDD user must be able to open the Kermeta text editor
	 * @throws WidgetNotFoundException 
	 * @throws InterruptedException 
	 * @throws TimeoutException 
	 */
	@Test public void openKermetaEditor() throws WidgetNotFoundException, InterruptedException, TimeoutException {
		bot.menu(FILE_MENU_TEXT).menu(NEW_MENU_TEXT).menu("Other...").click();
		
		// we should now be in the "New" wizards dialog and see a tree with a Kermeta item
		SWTBotTreeItem kermetaTree = bot.tree().getTreeItem("Kermeta");
		
		System.out.println("DEBUG - openKermetaEditor()");
		
		kermetaTree.expand();
		kermetaTree.getNode("New Kermeta file").select();
		bot.button(NEXT_BUTTON_TEXT).click();
		
		// we are now in the wizard for new Kermeta source files
		String filePath = TEST_PROJECT_NAME+"/"+KERMETA_SOURCE_FOLDER_NAME;
		String fileFullName = SOURCE_FILE_NAME + KERMETA_SOURCE_EXTENSION;
		bot.textWithLabel("File name:").setText(fileFullName);
		bot.textWithLabel("Enter or select the parent folder:").setText(filePath);
		bot.button(FINISH_BUTTON_TEXT).click();
		
		// we should now have a Kermeta file inside our project
		IFile sourceFile = UiTools.getWorkspaceFile(filePath, fileFullName);
		SWTBotEclipseTestCase.assertTrue(sourceFile != null);
		
		// the Kermeta editor must be opened and active
		SWTBotEclipseEditor editor = (SWTBotEclipseEditor) bot.activeEditor();
		SWTBotEclipseTestCase.assertTrue(editor.getTitle().equals(fileFullName));
	
		// a simple "Hello World!" test
		String helloText = "Hello World!";
		editor.selectLine(18);											// catch the comment
		editor.notifyKeyboardEvent(SWT.NONE, SWT.DEL);					// erase it
		editor.insertText("\t\tstdio.writeln(\"" + helloText + "\")");	// replace it with our code
		editor.save();
		
		// try to launch the Hello World
		SWTBotMenu contextMenu = editor.contextMenu("Run As");
		contextMenu.menu("1 Run as Kermeta Application").click();
		Thread.sleep(5000);												// wait for the end of the execution
		
		//////////// Kermeta 1.1.99 does not use KermetaConsole!!! /////////////////
		ProcessConsole kermetaConsole = (ProcessConsole) UiTools.getConsoleByType(ProcessConsole.class);
		
		SWTBotEclipseTestCase.assertTrue(kermetaConsole != null);
		SWTBotEclipseTestCase.assertTrue(kermetaConsole.getDocument().get().contains(helloText));
	}
	
	@AfterClass public static void cleanUpKermetaEnvironment() throws Exception {
		utils.closeAllShells();
		// we must let workspace as it was before the test
		UiTools.cleanProject(TEST_PROJECT_NAME);
	}
}
