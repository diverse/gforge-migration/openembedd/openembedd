package org.openembedd.testing.kermeta;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openembedd.testing.kermeta.editor.KermetaEditorSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	KermetaEditorSuite.class
})
public class AllTestsSuite {

}
