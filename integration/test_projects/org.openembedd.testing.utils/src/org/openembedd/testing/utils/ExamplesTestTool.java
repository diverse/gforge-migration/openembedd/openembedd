package org.openembedd.testing.utils;

//import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotEclipseConsole;
import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotEclipseEditor;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.widgets.TimeoutException;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class ExamplesTestTool extends TestUtils {

	/**
	 * open the Eclipse "New Example Wizard", find the targeted example and execute it
	 * 
	 * @param treeNodeName
	 * @param exampleNodeName
	 * @throws WidgetNotFoundException
	 */
	public void installExample(String treeNodeName, String exampleNodeName) throws WidgetNotFoundException {
		bot.menu(FILE_MENU_TEXT).menu(NEW_MENU_TEXT).menu(EXAMPLE_MENU_TEXT).click();
		
		SWTBotTree newWizardTree = bot.tree();
		
		SWTBotTreeItem categoryNode = newWizardTree.getTreeItem(treeNodeName);
		
		SWTBotTreeItem sampleNode = categoryNode.getNode(exampleNodeName);
		
		sampleNode.select();
		bot.button(FINISH_BUTTON_TEXT).click();
	}

	/**
	 * assert the project exists in the package Explorer
	 * @param projectName
	 * @throws WidgetNotFoundException
	 */
	public void isExampleInstalled(String projectName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		
		SWTBotTree workspaceTree = bot.tree();
		
		SWTBotTreeItem projectNode = workspaceTree.getTreeItem(projectName);
		
		SWTBotEclipseTestCase.assertVisible(projectNode);
	}

	/**
	 * assert the file exists in the project, within the package Explorer
	 * @param projectName
	 * @param folderName
	 * @param fileName
	 * @throws WidgetNotFoundException
	 */
	public void exampleProjectHasFile(String projectName, String folderName, String fileName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		SWTBotTreeItem node = bot.tree().getTreeItem(projectName).getNode(folderName).getNode(fileName);
		SWTBotEclipseTestCase.assertVisible(node);
	}

	/**
	 * Open the file, verify the corresponding editor is visible and then close it
	 * 
	 * @param projectName
	 * @param folderName
	 * @param fileName
	 * @throws WidgetNotFoundException
	 */
	public void exampleFileOpens(String projectName, String folderName, String fileName) throws WidgetNotFoundException {
		openExampleFile(projectName, folderName, fileName);
		
	
		SWTBotEclipseEditor editor = bot.editor(fileName);
		SWTBotEclipseTestCase.assertNotNull(editor);
		
		editor.close();
	}

	/**
	 * DO NOT USE: the EMF validation SUCCESS dialog blocks all threads, even SWTBot thread
	 * 
	 * @param projectName
	 * @param folderName
	 * @param modelName
	 * @param modelRootName
	 * @throws WidgetNotFoundException
	 * @throws TimeoutException
	 * @deprecated no functional way to test model validity
	 */
	public void modelIsValid(String projectName, String folderName, String modelName, String modelRootName) throws WidgetNotFoundException, TimeoutException {
		openExampleFile(projectName, folderName, modelName);
		
		SWTBotEclipseEditor editor = bot.editor(modelName);
		SWTBotEclipseTestCase.assertNotNull(editor);
		
		// get the model root
		SWTBotTreeItem modelTop = bot.tree().getAllItems()[0];
		SWTBotEclipseTestCase.assertNotNull("Problem with model editor - No tree found", modelTop);
		modelTop.expand();
		
		SWTBotTreeItem rootNode = modelTop.getNode(modelRootName);
		SWTBotEclipseTestCase.assertVisible(rootNode);
		
		rootNode.contextMenu(RCM_MODEL_VALIDATE_TEXT).click();
		
		//DEBUG
		bot.sleep(2000);
		SWTBotShell[] shells = bot.shells();
		for (int i = 0; i < shells.length; i++) {
		System.err.println("DEBUG ExamplesTool.modelIsValid() - shell["+i+"] = "+shells[i]);
		}
		
		// wait for the computation
//		bot.waitUntil(Conditions.aDialogAppears("Eclipse SDK"), (long)5000);
		
		if (bot.activeShell().getText().equals(VALIDATION_PROBLEM_TEXT)) {
			// close the error dialog
			bot.button(OK_BUTTON_TEXT).click();
			// send an assertion failure
			SWTBotEclipseTestCase.assertTrue("The '" + modelName + "' is not valid", false);
		}
		SWTBotEclipseTestCase.assertText(VALIDATION_SUCCESS_TEXT, bot.activeShell());
		// close the validation dialog
		bot.button(OK_BUTTON_TEXT).click();
		
		editor.close();
	}
	
	public void launchExample(String projectName, String launchFileName, String launchMenuName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		
		SWTBotTree workspaceTree = bot.tree();
		bot.sleep(2000);
		
		SWTBotTreeItem projectNode = workspaceTree.getTreeItem(projectName);
		projectNode.expand();
		
		SWTBotTreeItem launch = projectNode.getNode(launchFileName);
		
//		assertVisible(launch);
		
		// open the Eclipse Console view
		openConsoleView();
		
		launch.contextMenu(RCM_LAUNCH_RUN_TEXT).menu(launchMenuName).click();
	}
	
	/**
	 * @Deprecated we are not able to catch the Eclipse console
	 */
	public void launchAndVerify(String projectName, String launchFileName, String launchMenuName, String[] consoleExpectedTexts) throws WidgetNotFoundException {
		launchExample(projectName, launchFileName, launchMenuName);
		
		// programs first launch may imply some (compilation) work so it can take time
		bot.sleep(10000);
		
/*		SWTBotEclipseConsole console = bot.console(Constants.CONSOLE_VIEW_NAME);
		console.setFocus();
		
		for (int i = 0; i < consoleExpectedTexts.length; i++) {
			assertTextContains(consoleExpectedTexts[i], console);
		}*/
	}
	
	
	///////////////////////////////////////// Useful non public functions //////////////////////////////////////

	/**
	 * find the resource inside the Package Explorer and open it. You get the corresponding editor
	 * @param projectName
	 * @param folderName
	 * @param fileName
	 * @throws WidgetNotFoundException
	 */
	private void openExampleFile(String projectName, String folderName, String fileName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		
		SWTBotTree workspaceTree = bot.tree();
		
		SWTBotTreeItem projectNode = workspaceTree.getTreeItem(projectName);
		projectNode.expand();
		bot.sleep(2000);
		
		SWTBotTreeItem folderNode = projectNode.getNode(folderName);
		folderNode.expand();
		bot.sleep(2000);
		
		SWTBotTreeItem fileNode = folderNode.getNode(fileName);
		SWTBotEclipseTestCase.assertNotNull("'" + fileName + "' not found in "+folderName, fileNode);
		
		SWTBotEclipseTestCase.assertVisible(fileNode);
		
		// open the file
		fileNode.contextMenu(RCM_OPEN_TEXT).click();
		
		// wait for the model reflexive editor to open
		bot.sleep(1000);
	}
}
