package org.openembedd.testing.utils;

public interface Constants {

	// usual menus
	public static String FILE_MENU_TEXT = "File";
	public static String NEW_MENU_TEXT = "New";
	public static String EXAMPLE_MENU_TEXT = "Example...";
	public static String WINDOW_MENU_TEXT = "Window";
	public static String SHOW_VIEW_MENU_TEXT = "Show View";
	public static String HELP_MENU_TEXT = "Help";
	public static String SOFTWARE_MENU_TEXT = "Software Updates...";
	public static String CONSOLE_MENU_NAME = "Console";
	
	// usual views
	public static String PACKAGE_EXPLORER_VIEW_TEXT = "Package Explorer";
	
	// usual buttons
	public static String OK_BUTTON_TEXT = "OK";
	public static String YES_BUTTON_TEXT = "Yes";
	public static String NO_BUTTON_TEXT = "No";
	public static String CANCEL_BUTTON_TEXT = "Cancel";
	public static String CLOSE_BUTTON_TEXT = "Close";
	public static String FINISH_BUTTON_TEXT = "Finish";
	public static String NEXT_BUTTON_TEXT = "Next >";
	public static String PREVIOUS_BUTTON_TEXT = "< Previous";
	
	// usual right-click menu items
	public static String RCM_OPEN_TEXT = "Open";
	public static String RCM_MODEL_VALIDATE_TEXT = "Validate";
	public static String RCM_LAUNCH_RUN_TEXT = "Run As";
	
	// error dialogs
	public static String VALIDATION_PROBLEM_TEXT = "Validation Problems";
	public static String VALIDATION_SUCCESS_TEXT = "Validation Information";
	
	// some other stuff
	public static String WELCOME_VIEW_NAME = "Welcome";
	public static String CONSOLE_VIEW_NAME = "Console";
	
	public static String EMPTY_ANT_PARAMETER = "_";
}
