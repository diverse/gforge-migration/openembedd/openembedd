package org.openembedd.testing.utils;

import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.eclipse.finder.SWTEclipseBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class TestUtils implements Constants {

	protected SWTEclipseBot bot = new SWTEclipseBot();
	
	/** 
	 * close the Welcome windows which generally covers the workbench
	 */
	public void closeWelcome() {
		try {
			bot.view(WELCOME_VIEW_NAME).close();
		} catch (WidgetNotFoundException e) {
			System.err.println("WARNING: no 'Welcome' view found in the workbench!");
		}
	}
	
	/**
	 * assert the file is visible in the Package Explorer (project sub-folder)
	 * 
	 * @param projectName
	 * @param folderName
	 * @param fileName
	 * @throws WidgetNotFoundException
	 */
	public void fileIsPresent(String projectName, String folderName, String fileName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		
		SWTBotTree workspaceTree = bot.tree();
		
		SWTBotTreeItem projectNode = workspaceTree.getTreeItem(projectName);
		projectNode.expand();
		bot.sleep(1000);
		
		SWTBotTreeItem folderNode = projectNode.getNode(folderName);
		folderNode.expand();
		bot.sleep(1000);
		
		SWTBotTreeItem fileNode = folderNode.getNode(fileName);
		SWTBotEclipseTestCase.assertNotNull("'" + fileName + "' not found in "+folderName, fileNode);
		
		SWTBotEclipseTestCase.assertVisible(fileNode);
	}
	
	/**
	 * assert the file is visible in the Package Explorer (project root)
	 * 
	 * @param projectName
	 * @param fileName
	 * @throws WidgetNotFoundException
	 */
	public void fileIsPresent(String projectName, String fileName) throws WidgetNotFoundException {
		bot.view(PACKAGE_EXPLORER_VIEW_TEXT).setFocus();
		
		SWTBotTree workspaceTree = bot.tree();
		
		SWTBotTreeItem projectNode = workspaceTree.getTreeItem(projectName);
		projectNode.expand();
		bot.sleep(1000);
		
		SWTBotTreeItem fileNode = projectNode.getNode(fileName);
		SWTBotEclipseTestCase.assertNotNull("'" + fileName + "' not found in "+projectName, fileNode);
		
		SWTBotEclipseTestCase.assertVisible(fileNode);
	}
	
	/**
	 * open the Eclipse "Console" view
	 * @throws WidgetNotFoundException 
	 */
	public void openConsoleView() throws WidgetNotFoundException {
		bot.menu(WINDOW_MENU_TEXT).menu(SHOW_VIEW_MENU_TEXT).menu(CONSOLE_MENU_NAME).click();
	}

    public void closeAllShells() throws Exception {
        SWTBotShell[] shells = bot.shells();
        for (SWTBotShell shell : shells) {
            if (!isEclipseShell(shell) && !isTestThreadShell(shell)) {
            	// some dialogs may be canceled
            	try {
					bot.button(CANCEL_BUTTON_TEXT);
				} catch (Exception e) {
					// others must be forced
					shell.close();
				}
            }
        }
    }

	private boolean isEclipseShell(SWTBotShell shell) {
		return shell.getText().contains("Eclipse SDK");
	}
	private boolean isTestThreadShell(SWTBotShell shell) {
		return shell.getText().contains("Test thread");
	}
}
