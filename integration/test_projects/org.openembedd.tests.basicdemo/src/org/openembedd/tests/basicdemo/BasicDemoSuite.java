package org.openembedd.tests.basicdemo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openembedd.tests.basicdemo.changes.*;
import org.openembedd.tests.basicdemo.creation.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ExampleCreationTestSuite.class,
	ExampleChangesTestSuite.class
})
public class BasicDemoSuite { }