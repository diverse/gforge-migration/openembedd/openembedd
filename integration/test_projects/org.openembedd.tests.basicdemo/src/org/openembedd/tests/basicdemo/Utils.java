package org.openembedd.tests.basicdemo;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.progress.UIJob;
import org.openembedd.tests.utils.UiTools;

import fr.irisa.triskell.eclipse.console.KermetaConsole;

/**
 * Set of tools needed by tests on the Basic Demo.
 * Those tools can be used in setUp() & tearDown() methods or directly in test methods.
 * 
 * @author vmahe@irisa.fr
 *
 */
public class Utils {
	
	public static String PROJECT_NAME = "org.openembedd.basic.uml2cwm.demo";
	public static String PLUGIN_NAME = "org.openembedd.basic.uml2cwm.core";
	public static String WIZARD_ID = "org.openembedd.basic.uml2cwm.wizards";
	
	public static String UML_EDITOR_ID = "org.topcased.uml.modeler.UMLModeler";
	public static String UML_EDITOR_EXTENSION = ".umldi";
	public static String MODEL_NAME = "SalesRDB.uml";
	public static String DIAGRAM_NAME = "SalesRDB.umldi";
	public static String DIAGRAMS_PATH = "org.openembedd.basic.uml2cwm.demo/model/";
	
	public static String ATL_EDITOR_ID = "org.atl.eclipse.adt.ui.editor.AtlEditor";
	public static String ATL_EDITOR_EXTENSION = ".atl";
	public static String ATL_LAUNCH_FILE_NAME = "uml2cwm.launch"; 
	public static String ATL_RULES_FILE_NAME = "umlclass2cwmrelational.atl"; 
	public static String ATL_RULES_FILE_PATH = "org.openembedd.basic.uml2cwm.demo/atl/";
	public static String RELATIONAL_NAME = "SalesRDB.relational";
	public static String RELATIONAL_PATH = "org.openembedd.basic.uml2cwm.demo/model/";
	
	public static String KERMETA_EDITOR_ID = "fr.irisa.triskell.kermeta.texteditor.editors.KMTEditor";
	public static String KERMETA_EDITOR_EXTENSION = ".kmt";
	public static String KERMETA_LAUNCH_FILE_NAME = "cwmSimulator.launch"; 
	public static String KERMETA_CWM_SIMULATOR_FILE_NAME = "cwm_simulator.kmt"; 
	public static String KERMETA_CWM_SIMULATOR_FILE_PATH = "org.openembedd.basic.uml2cwm.demo/kermeta/";
	public static String SIMULATOR_PROMPT = " --> ";

	// phase 2 : modify input files then verify the demo runs (or fails if corrupted files)
	public static String MODIFIED_DIAGRAM = "/data/phase2/model/SalesRDB.umldi";
	public static String MODIFIED_MODEL = "/data/phase2/model/SalesRDB.uml";
	public static String TESTS_PLUGIN_NAME = "org.openembedd.tests.basicdemo";
	
	/**
	 * Erase the Basic Demo example project from the current workspace, if exists
	 *
	 */
	public static void cleanProject() {
		
		try {
			/* delete also the content, force deletion, no need for progress monitor */
			ResourcesPlugin.getWorkspace().getRoot().getProject(PROJECT_NAME).delete(true, true, null);
			
		} catch (Exception e) {
			System.out.println("Exception when cleaning project '"+PROJECT_NAME+"'");
			System.out.println(e.getStackTrace()[0]);
		}
	}
	
	/**
	 * Use the wizard of the Basic Demo plug-in to create an example project in the current workspace
	 * @throws Exception 
	 */
	public static void createProject() throws Exception {
			UiTools.createProjectThroughNewWizard(WIZARD_ID);
	}
	
	public static IFile getUmlDiagramFile(){
		return UiTools.getWorkspaceFile(DIAGRAMS_PATH,DIAGRAM_NAME);
	}
	
	public static IFile getRelationalFile(){
		return UiTools.getWorkspaceFile(RELATIONAL_PATH,RELATIONAL_NAME);
	}
	
	public static IFile getAtlLaunchFile(){
		return UiTools.getWorkspaceFile(PROJECT_NAME+"/",ATL_LAUNCH_FILE_NAME);
	}
	
	public static IFile getAtlRulesFile(){
		return UiTools.getWorkspaceFile(ATL_RULES_FILE_PATH,ATL_RULES_FILE_NAME);
	}
	
	public static IFile getKermetaLaunchFile(){
		return UiTools.getWorkspaceFile(PROJECT_NAME+"/",KERMETA_LAUNCH_FILE_NAME);
	}
	
	public static IFile getKermetaCwmSimulatorFile(){
		return UiTools.getWorkspaceFile(KERMETA_CWM_SIMULATOR_FILE_PATH,KERMETA_CWM_SIMULATOR_FILE_NAME);
	}
	
	// Phase 2
	
	public static void copyModifiedDiagramPhase2() throws Exception{
		UiTools.copyResource(getPluginPath()+MODIFIED_MODEL, UiTools.getWorkspacePath()+DIAGRAMS_PATH+MODEL_NAME);
		UiTools.copyResource(getPluginPath()+MODIFIED_DIAGRAM, UiTools.getWorkspacePath()+DIAGRAMS_PATH+DIAGRAM_NAME);
	}
	
	private static String getPluginPath() throws IOException {
		URL pluginUrl = Platform.getBundle(TESTS_PLUGIN_NAME).getEntry("/");
		return new File(FileLocator.toFileURL(pluginUrl).getFile()).getAbsolutePath();
	}
	
	public static void waitSimulatorEndOfWriting(IOConsole console) throws InterruptedException{
		while (! console.getDocument().get().endsWith(SIMULATOR_PROMPT))
			Thread.sleep(100);
	}
	
	// the "clearConsole()" method of InternalIOConsole fails to clean-up the text !!!
	public static void clearConsole(IOConsole console) {
		final IOConsole uiConsole = console;
		UIJob job = new UIJob("Clear console") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					uiConsole.getDocument().replace(0, uiConsole.getDocument().getLength(), "");
				} catch (BadLocationException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
		        return Status.OK_STATUS;
			}	
		};
		
		UiTools.executeUIJob(job);
	}
}
