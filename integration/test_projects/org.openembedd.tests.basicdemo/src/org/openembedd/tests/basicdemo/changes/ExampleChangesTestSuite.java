package org.openembedd.tests.basicdemo.changes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ModifyUMLDiagramTest.class,
					ModifyATLTransfoTest.class,
					ModifyKermetaSimulatorTest.class})
public class ExampleChangesTestSuite { }
