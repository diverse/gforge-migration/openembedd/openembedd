package org.openembedd.tests.basicdemo.changes;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;
import org.junit.*;
import org.openembedd.tests.basicdemo.Utils;
import org.openembedd.tests.basicdemo.creation.RunExampleTest;
import org.openembedd.tests.utils.ManualOracle;
import org.openembedd.tests.utils.ManualTest;
import org.openembedd.tests.utils.UiTools;
import org.topcased.modeler.uml.editor.UMLEditor;

//import fr.irisa.triskell.kermeta.runner.console.RunnerConsole;;

public class ModifyUMLDiagramTest {

	public static UMLEditor editor;
	public static File resultFile;
	public static ILaunch atlLaunch;
	public static ILaunch kermetaLaunch;
	
	@Before
	public void resetExampleProject() throws Exception {
		/* 
		 * clean up the workspace before testing creation of the example
		 */
		Utils.cleanProject();
		Utils.createProject();
		
		// clean up the workspace before trying to [re]launch transformation
		resultFile = new File(UiTools.getWorkspacePath() + Utils.RELATIONAL_PATH + Utils.RELATIONAL_NAME);
		resultFile.delete();
	}
	
	@Test
	@Ignore
	public void modifyUMLModel() throws Exception {
		
		// replace the Demo input diagram by a modified one
		Utils.copyModifiedDiagramPhase2();
		
		UIJob job = new UIJob("blah") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					// get the UML editor with the diagram given by the demo.
					editor = (UMLEditor)IDE.openEditor(UiTools.getPage(), Utils.getUmlDiagramFile());
//					editor.close(false);
					
				} catch (PartInitException e) { UiTools.uiException = e; }
		        return Status.OK_STATUS;
			}	
		};
		UiTools.executeUIJob(job);
		UiTools.manageUIException();
		
		Assert.assertTrue("The modified diagram should be open by the Topcased UML2 editor", editor != null);
		
		///////////// MANUAL TESTS ////////////////
		
		String testText = " - select the 'Address' class in the diagram\n"+
			" - hit the 'Delete' key.\n" +
			" - save the diagram.";
		ManualTest test = new ManualTest("", testText);
		test.doTest();
		
		String oracleText = "Open the 'SalesRDB.uml' model.\n" +
			"The model must contains an 'Address' object.";
		ManualOracle oracle = new ManualOracle(oracleText);
		
		Assert.assertTrue("The Address should not be remove from the model, only the diagram!", oracle.isSatisfied());
	}
	
//	@Test
//	/**
//	 * we replay the ATL transformation and the Kermeta simulator, with a new input file
//	 */
//	@Ignore
//	public void runKermetaSimulator() throws Exception {
//		
//		// replace the Demo input diagram by a modified one
//		Utils.copyModifiedDiagramPhase2();
//		
//		RunExampleTest run = new RunExampleTest();
//		
//		// we need the correct ".relational" file
//		run.runATLTransformation();
//		
//		// we know test the simulator on the new class "Town" added to the UML model
//		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
//		ILaunchConfiguration config = manager.getLaunchConfiguration(Utils.getKermetaLaunchFile());
//		
//		kermetaLaunch = config.launch(ILaunchManager.RUN_MODE, null);
//			
//		Assert.assertTrue("The Demo given launch of Kermeta fails", kermetaLaunch != null);
//		
//		IConsole[] consoles = ConsolePlugin.getDefault().getConsoleManager().getConsoles();
//		
//		Assert.assertTrue("There should be at least one console", consoles.length > 0);
//		
//		RunnerConsole console = null;
//		for (int i = 0; i < consoles.length; i++) {
//			if (consoles[i] instanceof fr.irisa.triskell.kermeta.runner.console.RunnerConsole)
//				console = (RunnerConsole)consoles[i];
//		}
//		
//		Assert.assertTrue("The kermeta console should be opened", console != null);
//		
//		// we must wait the simulator finishes to print on the console
//		Utils.waitSimulatorEndOfWriting(console);
//		// we should get in the console the line "1 - manage schema: SalesRDB"
//		Assert.assertTrue("The simulator must propose to manage SalesRDB",
//				console.getDocument().get().contains("1 - manage schema: SalesRDB"));
//		Utils.clearConsole(console);
//		
//		// try to go into database management ("1" choice)
//		console.getInputStream().appendData("1\n");
//		
//		// we must wait the simulator finishes to print on the console
//		Utils.waitSimulatorEndOfWriting(console);
//		Assert.assertTrue("we should get in the console the tables names",
//				console.getDocument().get().contains("Product")
//				&& console.getDocument().get().contains("Order")
//				&& console.getDocument().get().contains("Customer")
//				&& console.getDocument().get().contains("Address"));
//		
//		//TODO : write more tests
//		
//		
//		// clean closing of Kermeta console
//		console.getInputStream().appendData("exit\n");
//		kermetaLaunch.terminate(); // not enough to avoid error messages
//	}
}
