package org.openembedd.tests.basicdemo.creation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
//	ExampleFoundTest.class, 
	RunExampleTest.class
})
public class ExampleCreationTestSuite { }
