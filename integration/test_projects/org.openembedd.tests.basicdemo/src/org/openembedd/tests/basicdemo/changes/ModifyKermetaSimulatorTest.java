package org.openembedd.tests.basicdemo.changes;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;
import org.junit.*;
import org.openembedd.tests.basicdemo.Utils;
import org.openembedd.tests.utils.UiTools;

import fr.irisa.triskell.kermeta.texteditor.TexteditorPlugin;;

public class ModifyKermetaSimulatorTest {

	public static TexteditorPlugin editor;
	
	@Before
	public void resetExampleProject() throws Exception {
		/* 
		 * clean up the workspace before testing creation of the example
		 */
		Utils.cleanProject();
		Utils.createProject();
	}
	
	@Test
	@Ignore
	public void openKermetaEditor() throws Exception {
		UIJob job = new UIJob("blah") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					// get the Kermeta editor with the KMT file given by the demo.
					editor = (TexteditorPlugin)IDE.openEditor(UiTools.getPage(), Utils.getKermetaCwmSimulatorFile());
//					editor.close(false);
					
				} catch (PartInitException e) { UiTools.uiException = e; }
		        return Status.OK_STATUS;
			}	
		};
		UiTools.manageUIException();
		
		UiTools.executeUIJob(job);
		
		Assert.assertTrue("We should get the Kermeta editor", editor != null);
	}
	
	@Test
	@Ignore
	public void runKermetaSimulator() {
		;
		
		Assert.assertTrue("TODO", false);
	}
}
