package org.openembedd.testing.level3.kermeta.fsm;

import org.eclipse.core.runtime.CoreException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import org.openembedd.testing.utils.Constants;
import org.openembedd.testing.utils.ExamplesTestTool;
import org.openembedd.testing.utils.TestUtils;
import org.openembedd.tests.utils.UiTools;

//import org.eclipse.swtbot.swt.finder.widgets.SWTBotEclipseConsole;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class KermetaSamplesLaunchesTests extends ExamplesTestTool implements KermetaSamplesConstants {
	
	@BeforeClass
	public static void setLaunchesTest() throws WidgetNotFoundException {
		new TestUtils().closeWelcome();
		new ExamplesTestTool().installExample(EXAMPLE_CATEGORY_NAME, EXAMPLE_NODE_NAME);
	}
	
	@Ignore
	@Test
	public void allLaunchesArePresent() throws WidgetNotFoundException {
		fileIsPresent(SAMPLE_PROJECT_NAME, FIRST_LAUNCH_FILE_NAME);
		fileIsPresent(SAMPLE_PROJECT_NAME, SECOND_LAUNCH_FILE_NAME);
		fileIsPresent(SAMPLE_PROJECT_NAME, THIRD_LAUNCH_FILE_NAME);
		fileIsPresent(SAMPLE_PROJECT_NAME, FORTH_LAUNCH_FILE_NAME);
		fileIsPresent(SAMPLE_PROJECT_NAME, FIFTH_LAUNCH_FILE_NAME);
	}
	
	@Ignore
	@Test
	public void launchFirst() throws WidgetNotFoundException {
		launchAndVerify(SAMPLE_PROJECT_NAME, FIRST_LAUNCH_FILE_NAME, kermetaLaunchName(FIRST_LAUNCH_FILE_NAME), FIRST_LAUNCH_EXPECTED_TEXT);
	}
	
	@Ignore
	@Test
	public void launchSecond() throws WidgetNotFoundException {
		launchAndVerify(SAMPLE_PROJECT_NAME, SECOND_LAUNCH_FILE_NAME, kermetaLaunchName(SECOND_LAUNCH_FILE_NAME), SECOND_LAUNCH_EXPECTED_TEXT);
	}
	
//	@Test
//	public void launchThird() throws WidgetNotFoundException {
//		launchWithParameters(SAMPLE_PROJECT_NAME, THIRD_LAUNCH_FILE_NAME, THIRD_LAUNCH_PARAMETERS, THIRD_LAUNCH_EXPECTED_TEXT);
//	}
	
//	@Ignore
//	@Test
//	public void launchForth() throws WidgetNotFoundException {
//		launchWithParameters(SAMPLE_PROJECT_NAME, FORTH_LAUNCH_FILE_NAME, FORTH_LAUNCH_PARAMETERS, FORTH_LAUNCH_EXPECTED_TEXT);
//	}
	
	@Ignore
	@Test
	public void launchFifth() throws WidgetNotFoundException {
		launchExample(SAMPLE_PROJECT_NAME, FIFTH_LAUNCH_FILE_NAME, kermetaLaunchName(FIFTH_LAUNCH_FILE_NAME));
		
		
	}
	
//	void launchWithParameters(String projectName, String launchFileName, String[] parameters, String[] expectedTexts) throws WidgetNotFoundException {
//		launchExample(projectName, launchFileName, kermetaLaunchName(launchFileName));
//		
//		bot.sleep(10000);
//		
//		SWTBotEclipseConsole console = bot.console(Constants.CONSOLE_VIEW_NAME);
//		console.setFocus();
//		
//		assertTextContains(expectedTexts[0], console);
//		
//		for (int i = 0; i < parameters.length; i++) {
//			console.insertText(parameters[i]);
//			console.insertText("\n");	// return ('ENTER' key)
//			
//			bot.sleep(1000);
//			
//			assertTextContains(expectedTexts[i+1], console);
//		}
//	}
	
	String kermetaLaunchName(String launchFileName) {
		return "1 " + launchFileName.substring(0, launchFileName.length()-7);
	}
	
	@AfterClass
	public static void cleanUpWorkspace() {
		try {
			UiTools.getProject(SAMPLE_PROJECT_NAME).delete(true, true, null);
			UiTools.getProject(SAMPLE_ASPECT_PROJECT_NAME).delete(true, true, null);
		} catch (CoreException e) { }
	}
}
