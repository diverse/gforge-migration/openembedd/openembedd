package org.openembedd.testing.level3;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openembedd.testing.level3.kermeta.fsm.KermetaSamplesTestsSuite;
import org.openembedd.testing.level3.syndex.SyndexSamplesTestsSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	KermetaSamplesTestsSuite.class,
	SyndexSamplesTestsSuite.class
})
public class AllTestsSuite {

}
