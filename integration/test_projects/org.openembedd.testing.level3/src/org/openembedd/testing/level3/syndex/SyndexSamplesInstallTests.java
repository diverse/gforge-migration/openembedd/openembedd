package org.openembedd.testing.level3.syndex;

import org.eclipse.core.runtime.CoreException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openembedd.testing.utils.ExamplesTestTool;
import org.openembedd.testing.utils.TestUtils;
import org.openembedd.tests.utils.UiTools;

import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class SyndexSamplesInstallTests implements SyndexSamplesConstants {

	static TestUtils utils;
	static ExamplesTestTool tool;
	
	@BeforeClass
	public static void setSamplesTest() {
		utils = new TestUtils();
		tool = new ExamplesTestTool();
		
		utils.closeWelcome();
	}
	
	@Test
	public void installSyndexSamples() throws WidgetNotFoundException {
		tool.installExample(EXAMPLE_CATEGORY_NAME, EXAMPLE_NODE_NAME);
	}
	
	@Test
	public void samplesAreInstalled() throws WidgetNotFoundException {
		tool.isExampleInstalled(SAMPLE_PROJECT_NAME);
	}
	
	@AfterClass
	public static void cleanUpWorkspace() {
		try {
			UiTools.getProject(SAMPLE_PROJECT_NAME).delete(true, true, null);
		} catch (CoreException e) { }
	}
}
