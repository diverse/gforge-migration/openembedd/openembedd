package org.openembedd.testing.level3;

import org.eclipse.equinox.app.IApplicationContext;
import org.openembedd.tests.Constants;
import org.openembedd.tests.ui.UiTestApplication;

public class SamplesUiTestApplication extends UiTestApplication implements Constants {

	/* ***************************************************************
	 * Must overwrite the same method of org.openembedd.test plug-in *
	 * in order to properly set the class loader (which must be the  *
	 * one of the test bundle to avoid NoClassDefFound error).       *
	 *****************************************************************/
	public Object start(IApplicationContext context) throws Exception {
		setClassLoader(this.getClass().getClassLoader());
		return super.start(context);
	}
}
