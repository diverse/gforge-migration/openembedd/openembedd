package org.openembedd.testing.level3.syndex;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SyndexSamplesInstallTests.class,
	SyndexSamplesModelsTests.class,
	SyndexSamplesDiagramsTests.class
})
public class SyndexSamplesTestsSuite {

}
