package org.openembedd.testing.level3.kermeta.fsm;

public interface KermetaSamplesConstants {

	/////////// generic samples parameters ////////////
	
	public static String EXAMPLE_CATEGORY_NAME = "Kermeta samples";
	public static String EXAMPLE_NODE_NAME = "FSM Demo";
	public static String SAMPLE_PROJECT_NAME = "fr.irisa.triskell.kermeta.samples.fsm.demo";
	public static String SAMPLE_ASPECT_PROJECT_NAME = "fr.irisa.triskell.kermeta.samples.fsm.demoAspect";
	public static String SAMPLE_MODELS_FOLDER_NAME = "models";
	
	/////////// Kermeta samples specific parameters //////////////
	
	public static String FIRST_MODEL_FILE_NAME = "complextodet.fsm";
	public static String FIRST_MODEL_ROOT_NAME = "FSM [initial state = a]";
	public static String FIRST_DIAGRAM_FILE_NAME = "complextodet.fsmdi";
	
	public static String SECOND_MODEL_FILE_NAME = "sampletodeterminize.fsm";
	public static String SECOND_MODEL_ROOT_NAME = "FSM [initial state = s0]";
	public static String SECOND_DIAGRAM_FILE_NAME = "sampletodeterminize.fsmdi";
	
	public static String FIRST_LAUNCH_FILE_NAME = "FSM check invariants.launch";
	public static String[] FIRST_LAUNCH_EXPECTED_TEXT = {
		"call of the checkAllInvariants method",
		"call of the checkInvariants method"};
	
	public static String SECOND_LAUNCH_FILE_NAME = "FSM determinization.launch";
	public static String[] SECOND_LAUNCH_EXPECTED_TEXT = {
		"finite_state_machine {",
		"rankdir=LR;",
		"s1s0 -> s1s0 [ label=\"a\" ];",
		"transitions : [fsm::State:"};
	
	public static String THIRD_LAUNCH_FILE_NAME = "FSM loader with pre-post check.launch";
	public static String[] THIRD_LAUNCH_PARAMETERS = {"c"};
	public static String[] THIRD_LAUNCH_EXPECTED_TEXT = {
		"Current state : s1",
		"[kermeta::exceptions::ConstraintViolatedPost:"};
	
	public static String FORTH_LAUNCH_FILE_NAME = "FSM loader.launch";
	public static String[] FORTH_LAUNCH_PARAMETERS = {"c", "x", "z"};
	public static String[] FORTH_LAUNCH_EXPECTED_TEXT = {
		"Current state : s1",
		"Current state : s2",
		"Current state : s2",
		"[fsm::NoTransition:"};
	
	public static String FIFTH_LAUNCH_FILE_NAME = "FSM minimization.launch";
	public static String[] FIFTH_LAUNCH_PARAMETERS = {"complextodet.fsm", "simpler.fsm"};
	public static String[] FIFTH_LAUNCH_EXPECTED_TEXT = {
		"Enter the EMF model of the automaton to minimize:",
		"{a,a} ,",
		"Eo = Square{F-Q}^Square{F} is initialized",
		"Minimalize succeeded",
		"Equivalence classes creation succeeded : 4","Transition set creation succeeded"};

}
