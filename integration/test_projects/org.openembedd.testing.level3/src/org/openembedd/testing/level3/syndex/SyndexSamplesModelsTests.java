package org.openembedd.testing.level3.syndex;

import org.eclipse.core.runtime.CoreException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openembedd.testing.utils.ExamplesTestTool;
import org.openembedd.testing.utils.TestUtils;
import org.openembedd.tests.utils.UiTools;

import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

public class SyndexSamplesModelsTests implements SyndexSamplesConstants {
	
	static TestUtils utils;
	static ExamplesTestTool tool;
	
	@BeforeClass
	public static void setSamplesTest() throws WidgetNotFoundException {
		utils = new TestUtils();
		tool = new ExamplesTestTool();
		
		utils.closeWelcome();
		tool.installExample(EXAMPLE_CATEGORY_NAME, EXAMPLE_NODE_NAME);
	}

	@Test
	public void firstModelOpens() throws WidgetNotFoundException {
		tool.exampleFileOpens(SAMPLE_PROJECT_NAME, SAMPLE_MODELS_FOLDER_NAME, FIRST_MODEL_FILE_NAME);
	}

//	@Test
//	public void firstModelIsValid() throws WidgetNotFoundException, TimeoutException {
//		tool.modelIsValid(SAMPLE_PROJECT_NAME, SAMPLE_MODELS_FOLDER_NAME, FIRST_MODEL_FILE_NAME, FIRST_MODEL_ROOT_NAME);
//	}

	@Test
	public void secondModelOpens() throws WidgetNotFoundException {
		tool.exampleFileOpens(SAMPLE_PROJECT_NAME, SAMPLE_MODELS_FOLDER_NAME, SECOND_MODEL_FILE_NAME);
	}

//	@Test
//	public void secondModelIsValid() throws WidgetNotFoundException, TimeoutException {
//		tool.modelIsValid(SAMPLE_PROJECT_NAME, SAMPLE_MODELS_FOLDER_NAME, SECOND_MODEL_FILE_NAME, SECOND_MODEL_ROOT_NAME);
//	}
	
	@AfterClass
	public static void cleanUpWorkspace() {
		try {
			UiTools.getProject(SAMPLE_PROJECT_NAME).delete(true, true, null);
		} catch (CoreException e) { }
	}
}
