package org.openembedd.testing.level3.syndex;

public interface SyndexSamplesConstants {

	// generic samples parameters
	public static String EXAMPLE_CATEGORY_NAME = "OpenEmbeDD demo";
	public static String EXAMPLE_NODE_NAME = "SynDEx examples";
	public static String SAMPLE_PROJECT_NAME = "org.topcased.syndex.samples";
	public static String SAMPLE_MODELS_FOLDER_NAME = "Models";
	
	// SynDEx samples specific parameters
	public static String FIRST_MODEL_FILE_NAME = "[Appli]_ega2.syndex";
	public static String SECOND_MODEL_FILE_NAME = "[HW]_ega2.syndex";
	public static String FIRST_MODEL_ROOT_NAME = "Syn DEx";
	public static String SECOND_MODEL_ROOT_NAME = "Syn DEx";
	public static String FIRST_DIAGRAM_FILE_NAME = "[Appli]_ega2.syndexdi";
	public static String SECOND_DIAGRAM_FILE_NAME = "[HW]_ega2.syndexdi";
}
