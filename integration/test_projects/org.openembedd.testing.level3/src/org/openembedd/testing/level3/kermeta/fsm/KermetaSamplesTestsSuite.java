package org.openembedd.testing.level3.kermeta.fsm;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	KermetaSamplesInstallTests.class,
	KermetaSamplesModelsTests.class,
	KermetaSamplesDiagramsTests.class,
	KermetaSamplesLaunchesTests.class
})
public class KermetaSamplesTestsSuite {

}
