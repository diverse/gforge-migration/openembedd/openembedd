package org.openembedd.tests.sample;

import org.eclipse.equinox.app.IApplicationContext;
import org.openembedd.tests.ui.UiTestApplication;

public class SampleUiTestApplication extends UiTestApplication {

	/* ***************************************************************
	 * Must overwrite the same method of org.openembedd.test plug-in *
	 * in order to properly set the class loader (which must be the  *
	 * one of the test bundle to avoid NoClassDefFound error).       *
	 *****************************************************************/
	public Object start(IApplicationContext context) throws Exception {
		setClassLoader(this.getClass().getClassLoader());
		return super.start(context);
	}
}
