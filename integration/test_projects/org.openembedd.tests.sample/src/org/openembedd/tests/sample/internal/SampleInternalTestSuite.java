package org.openembedd.tests.sample.internal;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	FirstInternalTest.class})
public class SampleInternalTestSuite { }
