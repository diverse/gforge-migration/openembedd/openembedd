package org.openembedd.tests.sample.internal;

import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.core.resources.IProject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openembedd.tests.sample.Constants;
import org.openembedd.tests.sample.gui.FirstGUITest;
import org.openembedd.tests.utils.UiTools;

public class FirstInternalTest extends SWTBotEclipseTestCase implements Constants {

	@BeforeClass
	public static void setUpOnceForAllTests() throws WidgetNotFoundException {
		// any kind of initialization work to be done once time before the full sequence of tests
		
		///// as an example, use the GUI test method to initialize a java project /////
		FirstGUITest gui = new FirstGUITest();
		gui.createJavaProject();
	}
	@Before
	public void setUp() {
		// any kind of initialization work to be done before EACH TEST
	}
	
	@Test
	public void aSuccessfulTest() {
		assertTrue(true);
	}
	
	@Test
	public void aFailingTest() {
		assertTrue("The test has failed by construction", false);
	}
	
	@Test
	public void anErrorTest() {
		double x = 52/0;
		// the assertion is never tested as the previous line of code throws a DivideByZero error
		assertTrue(x != 0);
	}
	
	@Ignore
	@Test
	public void anIgnoredTest() {
		// this test is not passed until you remove the @Ignored annotation
		// (useful for tests which are under work or about unimplemented features)
		assertTrue(true);
	}
	
	@Test
	public void verifyJavaProjectExists() {
		IProject javaProject = UiTools.getProject(JAVA_PROJECT_NAME);
		assertTrue(javaProject != null);
	}
	
	@After
	public void tearDown() {
		// any kind of cleaning work to be done after EACH TEST
	}
	@AfterClass
	public static void tearDownOnceForAllTests() {
		// any kind of cleaning work to be done once time after the full sequence of tests
	}
}
