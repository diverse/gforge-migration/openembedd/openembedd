package org.openembedd.tests.sample;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openembedd.tests.sample.gui.SampleGUITestSuite;
import org.openembedd.tests.sample.internal.SampleInternalTestSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SampleInternalTestSuite.class,
	SampleGUITestSuite.class
	})
public class SampleTestSuite { }
