package org.openembedd.tests.sample.gui;

import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotView;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.openembedd.tests.sample.Constants;

public class NavigatorTest extends SWTBotEclipseTestCase implements Constants {

	@Before
	public void feedWorkspace() throws WidgetNotFoundException {
		// close the Welcome window
		bot.view("Welcome").close();

		// create a project
		FirstGUITest gui = new FirstGUITest();
		gui.createJavaProject();

		// add a class
		bot.menu("File").menu("New").menu("Class").click();
		bot.textWithLabel("Source folder:").setText(JAVA_PROJECT_NAME + "/src");
		bot.textWithLabel("Package:").setText(JAVA_PACKAGE_NAME);
		bot.textWithLabel("Name:").setText(JAVA_CLASS_NAME);
		bot.button("Finish").click();

		// we must wait for the Java editor
		bot.sleep(5000);

		// close the Java editor
		bot.activeEditor().close();
	}

	@Test
	public void howToManipulateNavigatorItems() throws WidgetNotFoundException {

		// catch the view
		SWTBotView navigator = bot.view("Package Explorer");
		navigator.setFocus();

		// find the tree
		SWTBotTree tree = bot.tree();

		// expand our Java project
		SWTBotTreeItem project = tree.getTreeItem(JAVA_PROJECT_NAME);
		project.expand();
		SWTBotTreeItem source = project.getNode("src").getNode(
				JAVA_PACKAGE_NAME);
		source.expand();

		// get our Java class
		SWTBotTreeItem javaFile = source.getNode(JAVA_CLASS_NAME + ".java");
		javaFile.doubleClick();

		// verify we have open the Java editor
		bot.sleep(10000);
		bot.activeEditor().getText().equals(JAVA_CLASS_NAME + ".java");
	}
}
