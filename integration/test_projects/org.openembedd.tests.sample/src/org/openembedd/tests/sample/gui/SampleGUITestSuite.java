package org.openembedd.tests.sample.gui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	FirstGUITest.class,
	NavigatorTest.class})
public class SampleGUITestSuite { }
