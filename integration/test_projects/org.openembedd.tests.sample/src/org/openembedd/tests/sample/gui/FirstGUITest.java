package org.openembedd.tests.sample.gui;

import org.eclipse.swtbot.eclipse.finder.SWTBotEclipseTestCase;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openembedd.tests.sample.Constants;
import org.openembedd.tests.utils.UiTools;

public class FirstGUITest extends SWTBotEclipseTestCase implements Constants {

	@Before public void prepareWorkspace() throws WidgetNotFoundException {
		// clear the workspace (if the project was created in a previous test run)
//		UiTools.cleanProject(JAVA_PROJECT_NAME);
	}
	
	/**
	 * tries to create a Java project through the NewJavaProject wizard
	 * @throws WidgetNotFoundException 
	 */
	@Test public void createJavaProject() throws WidgetNotFoundException {
		// close the Welcome window
		bot.view("Welcome").close();

		
		UiTools.switchToPerspective(UiTools.JAVA_PERSPECTIVE_ID);
		
		// detailed opening of the New/Projects dialog
		SWTBotMenu fileMenu = bot.menu("File");
		SWTBotMenu newMenu = fileMenu.menu("New");
		SWTBotMenu njpMenu = newMenu.menu("Project...");
		njpMenu.click();
		
		// select the Java/Java Project in the dialog tree
		SWTBotTree projectsTree = bot.tree();
		SWTBotTreeItem javaSubTree = projectsTree.getTreeItem("Java Project");
		javaSubTree.select();
		bot.button("Next >").click();
		
		// the New Java Project would be opened now =>
		// we can fill the project name text box and create the project
		bot.textWithLabel("Project name:").setText(JAVA_PROJECT_NAME);
		bot.button("Finish").click();
	}

	@After public void cleanUpWorkspace() throws WidgetNotFoundException {
		// clear the workspace (if the project was created in a previous test run)
//		UiTools.cleanProject(JAVA_PROJECT_NAME);
	}
}
