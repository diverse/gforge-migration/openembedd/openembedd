package org.openembedd.tests.sample;

public interface Constants {

	public static String JAVA_PROJECT_NAME = "org.openembedd.java.project.test";
	public static String JAVA_PACKAGE_NAME = "org.openembedd.tests.sample";
	public static String JAVA_CLASS_NAME = "MyJavaClass";
}
