    Ce répertoire est destiné aux scénarios de test des mises à jours entre versions de la plate-forme.
    
    La version 0.3.0 de la plate-forme reposant sur un changement de version lourd d'Eclipse et EMF, 
elle n'a pas fait l'objet d'un tel test.

    La version 0.4.0 (Japet) n'est utilisable qu'à partir d'une version 3.3.0 d'Eclipse =>
update possible seulement depuis la version 0.3.0 d'OpenEmbeDD

IMPORTANT : l'outil d'UPGRADE d'Eclipse 
		Help
		 -> software updates
		 	 -> find new software to install
		 	 	 -> search for updates of currently installed features
récupère les dernières versions depuis tous les sites updates enregistrés,
 ce qui empêche tout contrôle de la part de l'utilisateur et d'OpenEmbeDD.
 Aussi la cellule d'intégration rejette officiellement toute responsabilité quant 
 à ce mode de mise à jour  et ne fait pas de tests correspondants !!