@echo off

REM uses default java runtime
set vm=java

REM give the version for the used JVM
java -version

REM default values for os, ws and arch
set os=win32
set ws=win32
set arch=x86

REM gets targets to be executed from command line
REM targets=
REM while [ $# -gt 0 ]
REM do
REM 	targets=$targets\ $1
REM 	shift
REM done

REM  execution of the tests
echo "Launch of the platform tests..."
apache-ant-1.7.1\bin\ant -file tests.xml -logger org.apache.tools.ant.DefaultLogger
