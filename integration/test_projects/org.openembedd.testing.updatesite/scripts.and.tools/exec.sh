#!/bin/sh

# uses default java runtime
#vm=java
vm=/local/java/bin/java

# give the version for the used JVM
$vm -version

# gets targets to be executed from command line
targets=
while [ $# -gt 0 ]
do
	targets=$targets\ $1
	shift
done

# execution of the tests
echo "Launch of the platform tests..."
apache-ant-1.7.1/bin/ant -file tests.xml -logger org.apache.tools.ant.DefaultLogger
