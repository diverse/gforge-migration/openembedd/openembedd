package org.openembedd.testing.updatesite;

public class DependenciesProblemException extends Exception {

	public DependenciesProblemException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2505689720934468240L;
}
