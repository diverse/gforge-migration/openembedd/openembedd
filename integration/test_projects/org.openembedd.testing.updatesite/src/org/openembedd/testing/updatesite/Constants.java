package org.openembedd.testing.updatesite;

public interface Constants extends org.openembedd.testing.utils.Constants {

	public static String UPDATE_SITE_URL_PROPERTY = "org.openembedd.testing.updatesite.siteupdateurl";
	public static String UPDATE_SITE_LABEL_PROPERTY = "org.openembedd.testing.updatesite.siteupdatelabel";
	public static String CATEGORY_LABEL_PROPERTY = "org.openembedd.testing.updatesite.categorylabel";
	public static String FEATURE_LABEL_PROPERTY = "org.openembedd.testing.updatesite.featurelabel";
	
}
