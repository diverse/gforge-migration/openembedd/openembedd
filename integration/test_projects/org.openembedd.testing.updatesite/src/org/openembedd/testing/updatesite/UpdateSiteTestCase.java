package org.openembedd.testing.updatesite;

import static org.eclipse.swtbot.swt.finder.matchers.WidgetMatcherFactory.withLabel;

import java.io.IOException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.After;
import org.junit.Test;
//import org.openembedd.tests.utils.IOInterceptor;

import org.eclipse.swtbot.eclipse.finder.SWTEclipseBot;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.results.ArrayResult;
import org.eclipse.swtbot.swt.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTable;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.swtbot.swt.finder.widgets.TimeoutException;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;

/**
 * Tool which factorize the GUI manipulations realizing
 * OpenEmbeDD tools installation with the Update Site
 *  
 * @author vmahe
 *
 */
public class UpdateSiteTestCase implements Constants {
	
	// list of dialogs we may encounter when using Update Site manager
	private final String UPDATE_DIALOG_TEXT = "Software Updates and Add-ons";
	private final String ADD_SITE_DIALOG_TEXT = "Add Site";
	private final String MANAGE_SITES_DIALOG_TEXT = "Available Software Sites";
	private final String PROGRESS_DIALOG_TEXT = "Progress Information";
	private final String QUESTION_DIALOG_TEXT = "Question";
	private final String INSTALL_DIALOG_TEXT = "Install";
//	private final String PROBLEM_DIALOG_TEXT = "Problem Occurred";
	
	protected SWTEclipseBot bot = new SWTEclipseBot();

	@SuppressWarnings("static-access")
	@Test public void installRun() 
			throws	WidgetNotFoundException,
					TimeoutException,
					DependenciesProblemException, 
					IOException,
					FetchProblemException {
		
		// fetch the 4 parameters from the OS
		String httpID = System.getProperty(UPDATE_SITE_URL_PROPERTY);
		String labelID = System.getProperty(UPDATE_SITE_LABEL_PROPERTY);
		String categoryLabel = System.getProperty(CATEGORY_LABEL_PROPERTY);
		String featureLabel = System.getProperty(FEATURE_LABEL_PROPERTY);
		
		
		// open the update menu
		bot.menu(HELP_MENU_TEXT).menu(SOFTWARE_MENU_TEXT).click();
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");

		bot.waitUntil(Conditions.shellIsActive(UPDATE_DIALOG_TEXT), bot.DEFAULT_TIMEOUT);
		
		bot.tabItem("Available Software").activate();
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		/////////// unset the default sites, as OpenEmbeDD does not need them //////
		bot.button("Manage Sites...").click();
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		bot.waitUntil(Conditions.shellIsActive(MANAGE_SITES_DIALOG_TEXT), bot.DEFAULT_TIMEOUT);
		
		SWTBotTable table = bot.table();
		table.getTableItem("Ganymede Update Site").toggleCheck();
		table.getTableItem("The Eclipse Project Updates").toggleCheck();
		
		bot.button(OK_BUTTON_TEXT).click();
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		// we have return to the main update dialog
		bot.waitUntil(Conditions.shellIsActive(UPDATE_DIALOG_TEXT), bot.DEFAULT_TIMEOUT);
		
		
		// add the OpenEmbeDD update site we want to test
		bot.button("Add Site...").click();
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		bot.waitUntil(Conditions.waitForShell(withLabel(ADD_SITE_DIALOG_TEXT)), bot.DEFAULT_TIMEOUT);
		bot.textWithLabel("Location:").setText(httpID);
		
		bot.button(OK_BUTTON_TEXT).click();		
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		bot.waitUntil(Conditions.shellIsActive(UPDATE_DIALOG_TEXT), bot.DEFAULT_TIMEOUT);
		
		// wait for Eclipse to refresh the tree
		bot.sleep(1000);
		
		// we should now see the added site in the "Available Software" tree
		SWTBotTreeItem openembeddSubTree = bot.tree().getTreeItem(labelID);
		openembeddSubTree.expand();
		
		// wait for Eclipse to refresh the tree
		bot.sleep(2000);
		
		// select the feature we want to test
		SWTBotTreeItem categorySubTree = openembeddSubTree.getNode(categoryLabel);
		if (featureLabel.equals(EMPTY_ANT_PARAMETER)) {
			// no individual tool passed so we install all the category
			categorySubTree.select();
			categorySubTree.toggleCheck();
		} else {
			categorySubTree.expand();
			// wait for Eclipse to refresh the tree
			bot.sleep(2000);
			
			SWTBotTreeItem featureLine = categorySubTree.getNode(featureLabel);
			featureLine.select();
			featureLine.toggleCheck();
		}
		
		// we can now install the feature under test
		bot.button("Install...").click();
		
		// wait for the dependencies computation (may need a while)
		try {
			bot.waitUntil(Conditions.shellCloses(bot.shell(PROGRESS_DIALOG_TEXT)), (long) 300000);
			
			// It seems SWTBot needs some delay (given by IO call) before catching next widget.
			System.err.println("  IO sync for SWTBot");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// the Update Site manager now should list the tools to be installed
		bot.sleep(1000);		// delay for dialog activation before testing it
		try {
			if (bot.activeShell().getText().equals(QUESTION_DIALOG_TEXT)) {
				/* you get a dialog with the text:
				   The software items you selected may not be valid with
				   your current installation. Do you want to open the wizard
				   anyway to review the selections? */
				bot.button(YES_BUTTON_TEXT).click();
				
				// an "Install" windows presents you selected items
				// + a "Details" text box containing dependencies error(s)
				String errorText = bot.textWithLabel("Details").getText();
				
				// we must close windows to free up the workbench
				bot.button(CANCEL_BUTTON_TEXT);
				bot.button(CLOSE_BUTTON_TEXT);
				
				// we transmit the error to the JUnit harness
				throw new DependenciesProblemException(errorText);
			}
		} catch (WidgetNotFoundException e) { }
		
		// now a dialog displays the installation process
		bot.waitUntil(Conditions.shellIsActive(INSTALL_DIALOG_TEXT), bot.DEFAULT_TIMEOUT);
		
		// It seems SWTBot needs some delay (given by IO call) before catching next widget.
		System.err.println("  IO sync for SWTBot");
		
		bot.button(NEXT_BUTTON_TEXT).click();
		
		// we must wait the computation of licenses
		bot.sleep(10000);
		
		// license(s) dialog
		try {
			// if multiple items have license
			bot.radio("I accept the terms of the license agreements").click();
		} catch (Exception e) {
			// only one item => singular
			bot.radio("I accept the terms of the license agreement").click();
		}
		
		// we should catch the errors when fetching jars to transmit them
//		IOInterceptor.startErrCapture();
		
		bot.button(FINISH_BUTTON_TEXT).click();
		
		// wait during the download of corresponding jars (may need a while)
		bot.waitWhile(Conditions.shellIsActive(INSTALL_DIALOG_TEXT), (long)300000);
		
//		String fetchErrors = IOInterceptor.getCapturedText();
//		IOInterceptor.stopErrCapture();
		
		// just a little time for next dialog to appears
		bot.sleep(1000);		// delay for dialog activation before testing it
//		try {
//			if (bot.activeShell().getText().equals(PROBLEM_DIALOG_TEXT))
//					throw new FetchProblemException(fetchErrors);
//		} catch (WidgetNotFoundException e) { }
		
		// after some delay, a dialog appears which proposes to restart the workbench
		// but we quit and do not restart because the Ant file will re-launch it
		try {
			bot.button(NO_BUTTON_TEXT).click();
		} catch (WidgetNotFoundException e) {
			// EXIT
		}
	}

	@After
    public void closeAllShells() throws Exception {
		final Display display = bot.getDisplay();
		UIThreadRunnable.syncExec(display, new ArrayResult<Shell>() {
				public Shell[] run() {
					Shell[] shells=  display.getShells();
					if (shells.length > 0) {
				        for (int i = 0; i < shells.length; i++) {
				        	String shellName = shells[i].getText();
				            if (!shellName.contains("Eclipse") && !shellName.contains("Test thread")) {
				                shells[i].close();
				            }
				        }
					}
					return null;
				}
			});
    }
}
