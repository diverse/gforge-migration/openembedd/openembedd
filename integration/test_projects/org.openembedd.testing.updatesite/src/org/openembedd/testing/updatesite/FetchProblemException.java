package org.openembedd.testing.updatesite;

public class FetchProblemException extends Exception {

	public FetchProblemException(String fetchErrors) {
		super(fetchErrors);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2505689720934468240L;

}
