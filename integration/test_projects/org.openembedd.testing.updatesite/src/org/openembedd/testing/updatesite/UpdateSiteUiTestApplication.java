package org.openembedd.testing.updatesite;

import org.eclipse.equinox.app.IApplicationContext;
import org.openembedd.tests.ui.UiTestApplication;

public class UpdateSiteUiTestApplication extends UiTestApplication implements Constants {

	protected void processArgs(IApplicationContext context) {
		super.processArgs(context);
		
		args = (String[]) context.getArguments().get("application.args");
		if (args == null)
			args = new String[0];
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-siteupdateurl"))
				System.setProperty(UPDATE_SITE_URL_PROPERTY, args[i+1]);
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-siteupdatelabel"))
				System.setProperty(UPDATE_SITE_LABEL_PROPERTY, args[i+1]);
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-categorylabel"))
				System.setProperty(CATEGORY_LABEL_PROPERTY, args[i+1]);
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-featurelabel"))
				System.setProperty(FEATURE_LABEL_PROPERTY, args[i+1]);
		}
	}

	/* ***************************************************************
	 * Must overwrite the same method of org.openembedd.test plug-in *
	 * in order to properly set the class loader (which must be the  *
	 * one of the test bundle to avoid NoClassDefFound error).       *
	 *****************************************************************/
	public Object start(IApplicationContext context) throws Exception {
		setClassLoader(this.getClass().getClassLoader());
		return super.start(context);
	}
}
