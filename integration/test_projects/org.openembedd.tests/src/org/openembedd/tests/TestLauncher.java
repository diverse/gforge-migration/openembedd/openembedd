/* $Id: TestLauncher.java,v 1.21 2008-09-04 13:15:07 vmahe Exp $
* Project : org.openembedd.tests
* File : 	TestLauncher.java
* License : EPL
* Copyright : IRISA / INRIA / Universite de Rennes 1
* ----------------------------------------------------------------------------
* Creation date : Jul 20, 2007
* Authors : vmahe
*/
package org.openembedd.tests;

import java.util.ArrayList;
import java.util.List;

import org.openembedd.tests.TestRunner;
import org.openembedd.tests.TestWorkbenchAdvisor;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;

/**
 * Launch the workbench (in automated tests) then run the test runner.<br>
 * Configuration arguments for a run-time execution :
 * <ul><li> -testclassname {name of the tests suite}</li>
 *   <li> -testresultname {what you want (name of the plug-in is god idea)}.xml</li>
 *   <li> -testresultdirectory {/tmp (for example)}</li></ul>
 * Dependency: org.eclipse.core.runtime
 * @author vmahe@irisa.fr
 */
public class TestLauncher  implements IApplication, IExecutableExtension {
	
	public void launchTest(String[] arg0) throws Exception {

		// print a message to the console, this is useful in automatic tests to check that
		// the application was correctly started ...
		System.out.println("Launching eclipse application: " +this.getClass().getCanonicalName());
		
		/*
		 * 
		 * We must wait until the workbench has been set up.
		 * 
		 */
		while ( ! PlatformUI.isWorkbenchRunning() ) {
			System.out.println("Waiting for the workbench...");
			Thread.sleep(1000);
		}

		/*
		 * 
		 * We must wait until the plug-ins extended the startup extension are initialized.
		 * 
		 */
		int nbTryLeft = 60 * 2 ; //
		while ( ! arePluginsInitialized() && nbTryLeft > 0) {
			System.out.println("Waiting for all the startup plugins...");
			Thread.sleep(1000);	
			nbTryLeft--;
		}
		if(! arePluginsInitialized()){
			String msg = "";
//			for (String s : notInitializedPlugins){
				msg += notInitializedPlugins + ", ";
//			}
			closeWorkbench();
			System.out.println("Didn't succeed to startup the plugins : " + msg);
			throw new Error("Didn't succeed to startup the plugins : " + msg);
		}
	
		// We use the generic OpenEmbeDD test runner
		TestRunner runner = new TestRunner();
		
		// give the current loader to the (external) launcher
		runner.setClassLoader(this.getClass().getClassLoader());
		
		runner.run(arg0);

		closeWorkbench();
	}
	
	private void closeWorkbench(){
		UIJob job = new UIJob("") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				//PlatformUI.getWorkbench().getActiveWorkbenchWindow().close();
				PlatformUI.getWorkbench().close();
				return org.eclipse.core.runtime.Status.OK_STATUS;
			}	
		};
		job.schedule();
	}
	
	public Object start(IApplicationContext context) throws Exception {
		
		final String[] args = (String[]) context.getArguments().get("application.args");
		
		Runnable run = new Runnable() {
			public void run() {
				try {
					launchTest(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		Thread t = new Thread(run);
		t.start();
		
	    Display display = PlatformUI.createDisplay();
	    
	    int returnCode = PlatformUI.createAndRunWorkbench(display, getAdvisor());
	    if (returnCode == PlatformUI.RETURN_RESTART)
	    	return IApplication.EXIT_RESTART;
	    else
	    	return IApplication.EXIT_OK;
	}

	protected List<String> notInitializedPlugins = new ArrayList<String>();
	private boolean arePluginsInitialized() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();

		// Eclipse bug 55901: don't use getConfigElements directly, for pre-3.0
		// compatibility, make sure to allow both missing class
		// attribute and a missing startup element
		IExtensionPoint point = registry.getExtensionPoint(PlatformUI.PLUGIN_ID,"startup");

		final IExtension[] extensions = point.getExtensions();
		if (extensions.length == 0) {
			return true;
		}
		boolean result = true;
		notInitializedPlugins.clear();
		for (int i=0; i<extensions.length; i++) {
			IExtension extension = extensions[i];
			
			Bundle bundle = Platform.getBundle( extension.getNamespaceIdentifier() );
			
			// problem with 'org.topcased.toolkit', which is never more than "resolved" without any activator nor activation policy
			boolean initialized = (bundle.getState() == Bundle.ACTIVE)
				|| ((bundle.getState() == Bundle.RESOLVED)
						&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATOR) == null) 
						&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATIONPOLICY) == null ));
			
			if ( ! initialized ) {
				notInitializedPlugins.add(extension.getNamespaceIdentifier() + "( bundle : "+ bundle.getState() +" "+bundle.toString() +")");
				result = false;
			}
		}

		return result;
	}

	
	public void setInitializationData(IConfigurationElement config, String propertyName, Object data) throws CoreException {
		// nothing to be done here
	}
	
	/**
	 * to be overwritten in order to give another InitialWindowPerspectiveId
	 * @param advisor
	 * @return
	 */
	public TestWorkbenchAdvisor getAdvisor() {
		return new TestWorkbenchAdvisor(IDE.RESOURCE_PERSPECTIVE_ID);
	}

	public void stop() {
		// TODO Auto-generated method stub
		
	}
}
