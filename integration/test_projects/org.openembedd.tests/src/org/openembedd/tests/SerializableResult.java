package org.openembedd.tests;

import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.openembedd.tests.utils.UiTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class SerializableResult extends Result implements Constants {

	private Document resultDoc;
	private Element resultSuite;	// compatibility with Ant/JUnit standard
	private Element currentRoot;
	private Hashtable<String, Element> allTests = new Hashtable<String, Element> ();
	private Integer errors = 0;		// JUnit 4 does not distinguish between test oracle failures and program errors
	
	private class DOMListener extends RunListener {
		public DOMListener(String suiteClassName) {
			// building the DOM tree of the tests results
			try {
				resultDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
				
				/////// compatible with Ant/JUnit standard ///////
				resultSuite = resultDoc.createElement(SUITE);
				resultSuite.setAttribute(ATTR_NAME, suiteClassName);
				
				resultDoc.appendChild(resultSuite);
				currentRoot = resultSuite;
				
			} catch (ParserConfigurationException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Override
		public void testRunStarted(Description description) throws Exception {
			
			// we create the Element for the testsuite and store it
			Element currentSuiteTest = null;
			if (description.isSuite()) {
				currentSuiteTest = resultDoc.createElement(SUITE);
			}
			else if (description.isTest()) {
				currentSuiteTest = resultDoc.createElement(TEST);
			}
			else throw new Exception("ERROR : "+ description.getDisplayName() +" is not a Suite nor a Test!!");
			
			// default status value
//			currentSuiteTest.setAttribute(TEST_STATUS, ATTR_STATUS_PASSED);
			
			currentSuiteTest.setAttribute(ATTR_NAME, description.getDisplayName());
			currentSuiteTest.setAttribute(ATTR_CLASS_NAME, "");
			currentSuiteTest.setAttribute(ATTR_METHOD_NAME, "");
			
			// insert the new test in the tree
			currentRoot.appendChild(currentSuiteTest);
			allTests.put(description.getDisplayName(), currentSuiteTest);
			
			currentRoot = currentSuiteTest;
			
			errors = 0;
		}

		@Override
		public void testStarted(Description description) throws Exception {
			// we create the Element for the test and store it
			Element currentTest = createTest(description);
			
			// default status value
			currentTest.setAttribute(TEST_STATUS, ATTR_STATUS_PASSED);
			
			setTest(currentTest, description);
		}

		@Override
		public void testFailure(Failure failure) throws Exception {
			Boolean beforeTestFailure = false;
			
			Element currentTest = allTests.get(failure.getDescription().getDisplayName());
			if (currentTest == null) {
				beforeTestFailure = true;
				currentTest = createTest(failure.getDescription());
				setTest(currentTest, failure.getDescription());
			}
			Element currentFailure = null;
			
			// try to distinguish between "failures" (unsatisfied assertions) and errors
//			if (failure.getException() instanceof junit.framework.AssertionFailedError) {
			/////// 2 instances of the junit.framework.AssertionFailedError class on Windows
			if (failure.getException().getClass().getCanonicalName().equals(junit.framework.AssertionFailedError.class.getCanonicalName())) {
				// a JUnit4 "JUnit3-like" failure
				currentTest.setAttribute(TEST_STATUS, ATTR_STATUS_FAILURE);
				// create a sub-element for failure properties
				currentFailure = resultDoc.createElement(FAILURE);
				currentFailure.setAttribute(ATTR_FAILURE_MESSAGE, failure.getMessage());
				currentFailure.setAttribute(ATTR_FAILURE_TYPE,
						failure.getException().getClass().getName());
			} else {
				// a program error
				currentTest.setAttribute(TEST_STATUS, ATTR_STATUS_ERROR);
				// create a sub-element for failure properties
				currentFailure = resultDoc.createElement(ERROR);
				currentFailure.setAttribute(ATTR_ERROR_MESSAGE,
						(beforeTestFailure ? " [failure before test]" : "")
						+ failure.getMessage());
				currentFailure.setAttribute(ATTR_ERROR_TYPE,
						failure.getException().getClass().getName());
				
				errors++;
			}
			
			//TODO : filter "junit" out of the trace...
			Text trace = resultDoc.createTextNode(UiTools.getFilteredTrace(failure));
			currentFailure.appendChild(trace);
			
			currentTest.appendChild(currentFailure);
		}

		@Override
		public void testIgnored(Description description) throws Exception {
			// ignored tests are not "started" so we must add them here
			Element currentTest = resultDoc.createElement(TEST);
			
			currentTest.setAttribute(TEST_STATUS, ATTR_STATUS_IGNORED);
			
			setTest(currentTest, description);
		}

		@Override
		public void testFinished(Description description) throws Exception {
			Element currentTest = allTests.get(description.getDisplayName());
			currentTest.setAttribute(ATTR_TEST_DURATION, "0.0");
		}

		@Override
		public void testRunFinished(Result result) throws Exception {
			
			// end of the tests suite run => add figures to the document.
			resultSuite.setAttribute(ATTR_TESTS_COUNT, "" + result.getRunCount());
			resultSuite.setAttribute(ATTR_IGNORED_COUNT, "" + result.getIgnoreCount());
			resultSuite.setAttribute(ATTR_FAILURES_COUNT, "" + (result.getFailureCount()-errors));
			resultSuite.setAttribute(ATTR_ERRORS_COUNT, "" + errors);
			resultSuite.setAttribute(ATTR_TOTAL_DURATION, "" + result.getRunTime()/1000.0);
		}
	}

	public RunListener createListener(String suiteClassName) {
		return new DOMListener(suiteClassName);
	}
	
	private void setTest(Element currentTest, Description description) {
		currentTest.setAttribute(ATTR_NAME, description.getDisplayName());
		currentTest.setAttribute(ATTR_CLASS_NAME, TestHelper.getClassName(description));
		currentTest.setAttribute(ATTR_METHOD_NAME, TestHelper.getMethodName(description)); // not compatible with Ant/JUnit DTD
		
		// insert the new test in the tree
		currentRoot.appendChild(currentTest);
		allTests.put(description.getDisplayName(), currentTest);
	}
	
	private Element createTest(Description description) throws Exception {
		// we create the Element for the test and store it
		Element test = null;
		if (description.isSuite()) {
			test = resultDoc.createElement(SUITE);
		}
		else if (description.isTest()) {
			//TODO : distinguish TestClass & TestMethod
			test = resultDoc.createElement(TEST);
		}
		else throw new Exception("ERROR : "+ description.getDisplayName() +" is not a Suite nor a Test!!");
		
		return test;
	}
	
	public Document getDocument() {
		return resultDoc;
	}
	
	public void saveXML(String directory, String xmlFileName) {
		DOMUtils.saveXML(resultDoc, directory, xmlFileName);
	}
}
