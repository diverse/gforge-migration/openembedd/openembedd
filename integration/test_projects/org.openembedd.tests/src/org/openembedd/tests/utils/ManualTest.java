package org.openembedd.tests.utils;

//import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.widgets.MessageBox;
//import org.eclipse.ui.progress.UIJob;
import org.openembedd.tests.utils.ManualTestConsole.QuestionType;

/**
 * Concept of an manual test which need human intervention (non programmable test).<BR>
 * It will present the task to be done by a tester through a dialog.<BR>
 * The tester tries to do the task and then he answers DONE (if he did) or ERROR (if he can not do).<BR>
 * ERROR choice throws a specific exception, which will be traced in the global tests results.<BR>
 * 
 * @author vmahe@irisa.fr
 *
 */
public class ManualTest {

	private String setUp;
	private String test;
	
//	private static String TITLE = "Manual test: what to do?";
	
	/**
	 * 
	 * @param setUpText : the initialization task(s) to be done before the test
	 * @param testText : the manipulation(s) to be done by the user in order to test
	 */
	public ManualTest(String setUpText, String testText) {
		setUp = setUpText;
		test = testText;
	}
	
	/**
	 * create a new dialog which present SetUp and Test tasks to the tester (may throw an error)
	 * @throws ManualErrorException 
	 *
	 */
	public void doTest() throws ManualErrorException {
//		UIJob job = new UIJob("blah") {
//			@Override
//			public IStatus runInUIThread(IProgressMonitor monitor) {
				
				String msg = "SETUP:\n\n" + setUp +
							"\nTEST TO DO:\n\n" + test +
							"\n\n\t-> Have you done all the TEST task?";

//				MessageBox msgbox = new MessageBox(UiTools.getHandShell(),
//						SWT.ICON_QUESTION | SWT.YES | SWT.NO | SWT.CANCEL);
//				
//				msgbox.setText(TITLE);
//				
//				msgbox.setMessage(msg);
//				
//				int result = msgbox.open();
//				
//				if (result == SWT.NO)
//					return StatusConstants.ERROR_STATUS;
//				
//				if (result == SWT.CANCEL)
//					return StatusConstants.IGNORED_STATUS;
//				
//		        return StatusConstants.PASSED_STATUS;
//			}	
//		};
//		IStatus status = UiTools.executeUIJob(job);

				
		ManualTestConsole.setType(QuestionType.TEST);
		ManualTestConsole.setMessage(msg);
		IStatus status = ManualTestConsole.open();
				
				
		if (status == StatusConstants.ERROR_STATUS)
			throw new ManualErrorException("Error trying to do test:\n"+test);
		
		//TODO manage the IGNORED case
	}
}
