package org.openembedd.tests.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.progress.UIJob;
import org.junit.runner.notification.Failure;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;


public class UiTools {

	public static String JAVA_WIZARD_ID = "org.eclipse.jdt.ui.wizards.JavaProjectWizard";
	public static String JAVA_PERSPECTIVE_ID = "org.eclipse.jdt.ui.JavaPerspective";

	public static Exception uiException = null;

	protected static List<String> notInitializedPlugins = new ArrayList<String>();

	/**
	 * Erase the named project from the current workspace, if exists
	 */
	public static void cleanProject(String projectName) {

		try {
			// wait for the GUI to eventually inform the workspace
			// the project has been previously created
			Thread.sleep(1000);

			/* delete also the content, force deletion, no need for progress monitor */
			IProject projectToDelete = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			if (projectToDelete.exists()) {
				projectToDelete.delete(true, true, null);
			}
		} catch (Exception e) {
			System.out.println("Error when cleaning project '"+projectName+"': "
					+e.getStackTrace()[0]);
		}
	}

	/**
	 * Launch the given job.
	 * How To define an UIJob :
	 * <pre>
    final IWorkbenchWizard wizard;
    wizard = UiTools.getNewWizard(WIZARD_ID);
    UIJob job = new UIJob("blah") {
        public IStatus runInUIThread(IProgressMonitor monitor) {
            WizardDialog dialog = new WizardDialog(null, wizard);
            dialog.create();
            dialog.setBlockOnOpen(false);
            dialog.open();
            ... / work to be done / ...
            wizard.performFinish();
            dialog.close();
            return Status.OK_STATUS;
        }	
    };</pre>
	 */
	public static IStatus executeUIJob(UIJob job) {
		return executeJob(job);
	}

	public static IStatus executeJob(Job job) {
		job.setUser(true);
		job.schedule();
		try {
			// wait until the job is finished
			while ( job.getState() == Job.RUNNING || job.getState() == Job.WAITING ) {
				System.out.println("The job '"+job.getName()+ "' is "+
						(job.getState() == Job.RUNNING ? "RUNNING" : "WAITING"));
				job.join();
			}
		} catch (InterruptedException e) { 
			// we store the exception for later processing
			uiException = e;
			e.printStackTrace();
		}

		return job.getResult();
	}
	
	public static IWorkbenchWizard getNewWizard(String wizardID) {
		try {
			return PlatformUI.getWorkbench().getNewWizardRegistry().findWizard(wizardID).createWizard();
		} catch (CoreException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	//	/**
	//	 * Needed to store current perspective before change
	//	 * @return the (String) ID of the active page perspective
	//	 */
	//	public static String getCurrentPerspectiveId() {
	//		IWorkbenchWindow win = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	//		IWorkbenchPage page = win.getActivePage();
	//		IPerspectiveDescriptor desc = page.getPerspective();
	//		return desc.getId();
	//	}
	/**
	 * easy-to-use way of changing the current perspective.
	 * @param perspectiveID
	 */
	public static void switchToPerspective(String perspectiveID) {
		String currentPerspective = PlatformUI.getWorkbench().getPerspectiveRegistry().getDefaultPerspective();
		if (!currentPerspective.equals(perspectiveID)) {
			final String pID = perspectiveID;
			UIJob job = new UIJob("Switch to perspective: "+pID) {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					try {
						PlatformUI.getWorkbench().showPerspective(pID, PlatformUI.getWorkbench().getActiveWorkbenchWindow());
					} catch (WorkbenchException e) {
						e.printStackTrace();
					}
					return Status.OK_STATUS;
				}	
			};
	
			UiTools.executeUIJob(job);
		}
	}

	/**
	 * Use the given wizard to create the corresponding project in the current workspace
	 * (mainly for Example projects creation, but also for AGR test suites)
	 * @throws Exception 
	 */
	public static void createProjectThroughNewWizard(String wizardID) throws Exception {
		final IWorkbenchWizard wizard;
		wizard = getNewWizard(wizardID);
		assert( wizard != null );

//		UIJob job = new UIJob("create Project Through New Wizard") {
//			@Override
//			public IStatus runInUIThread(IProgressMonitor monitor) {
				
				System.out.println("DEBUG UiTools.createProjectThroughNewWizard() - new UIJob()");
				
				WizardDialog dialog = new WizardDialog(null, wizard);
				dialog.create();
				dialog.setBlockOnOpen(false);
				dialog.open();
				wizard.performFinish();
				dialog.close();
//				return Status.OK_STATUS;
//			}	
//		};
//
//		UiTools.executeUIJob(job);
	}

	public static IProject getProject(String name) {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(name);
	}

	public static IFolder getFolder(String name) {
		return ResourcesPlugin.getWorkspace().getRoot().getFolder(new Path(name));
	}
	/**
	 * Create a new project in the current workspace
	 * @param name : the desired name of the project
	 */
	public static void createProject(String name) {
		// it is the default project type
		try {
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(name);
			project.create(null);
			project.open(null);
		} catch (CoreException e) { e.printStackTrace(); }
	}

	public static IWorkbenchPage getPage() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getWorkbenchWindows()[0];
		return window.getActivePage();
	}

	public static IEditorDescriptor getEditorDescriptor(String fileExtension) {
		IEditorDescriptor[] editors = PlatformUI.getWorkbench().getEditorRegistry().getEditors(fileExtension);
		if (editors.length > 0)
			return editors[0];
		else
			System.out.println("Error retrieving editor for '"+fileExtension+"' files: not found in EditorsRegistry.");
		return null;
	}

	public static String getWorkspacePath() {
		return Platform.getInstanceLocation().getURL().getPath();
	}

	public static String getTestMainDirectoryPath() {
		return Platform.getInstallLocation().getURL().getPath();
	}

	/**
	 * **** Not yet implemented *****
	 * 
	 * @param fullPathName
	 * @return
	 * @deprecated never implemented
	 */
	public static boolean fileExists(String fullPathName) {
		// TODO : write the method to get the info without creating the file
		return false;
	}

	/**
	 * Enables catching the exceptions / errors which occur within UI jobs.
	 * Execute the 'manageUiException()' after the job,
	 * and it will throw up the exception stored in 'uiException' if exists. 
	 * 
	 * @throws Exception
	 */
	public static void manageUIException() throws Exception {
		if (uiException != null) {
			// we clear uiException for another use in tests
			Exception tmpException = uiException;
			uiException = null;
			// then we throw the exception registered in UI thread in current test thread
			throw tmpException;
		}
	}

	/**
	 * Clean up the "org.junit.*" reference lines from the stack trace
	 * @param failure: the failure (which can be an error) to clean
	 * @return: the clean trace
	 */
	public static String getFilteredTrace(Failure failure) {
		StackTraceElement[] stackTrace = failure.getException().getStackTrace();
		String trace = failure.getException().toString();
		for (int i = 0; i < stackTrace.length; i++) {
			if (!stackTrace[i].toString().contains("org.junit.")				// JUnit4 stack traces
					&& !stackTrace[i].toString().contains("junit.framework."))	// JUnit3 stack traces
				trace = trace + "\n\t" + stackTrace[i].toString();
		}
		return trace;
	}

	public static Shell getShell() {
		return PlatformUI.getWorkbench().getDisplay().getActiveShell();
	}

	// for hand tests, access to the launcher shell
	private static Shell handShell; 
	public static Shell getHandShell() { return handShell; }
	public static void setHandShell(Shell shell) { handShell = shell; }


	public static String getPluginPath(String pluginName) throws IOException {
		URL pluginRootUrl = Platform.getBundle(pluginName).getEntry("/");
		return new File(FileLocator.toFileURL(pluginRootUrl).getFile()).getAbsolutePath();
	}

	//	public static void refreshWorkspace() {
	//		UIJob job = new UIJob("blah") {
	//			@Override
	//			public IStatus runInUIThread(IProgressMonitor monitor) {
	//				// we need to select all the workspace before launching a refresh
	//				// (RefreshAction only acts on selected resources)
	//				StructuredSelection sel;
	//				try {
	//					sel = new StructuredSelection(ResourcesPlugin.getWorkspace().getRoot().members());
	//				} catch (CoreException e) {
	//					e.printStackTrace();
	//				} 
	//				RefreshAction action = new RefreshAction(UiTools.getShell());
	//				action.refreshAll();
	//		        return Status.OK_STATUS;
	//			}	
	//		};
	//		job.setPriority(Job.INTERACTIVE);
	//		executeUIJob(job);
	//	}

	public static IFile getWorkspaceFile(String relativePath, String fileName){
		IPath path = new Path(getWorkspacePath()+relativePath+"/"+fileName);
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(path);
		return file;
	}

	public static IFolder getWorkspaceFolder(String relativePath, String folderName){
		IPath path = new Path(getWorkspacePath()+relativePath+"/"+folderName);
		IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder(path);
		return folder;
	}

	/**
	 * ATTENTION: works only within a workspace!!!!
	 * @param originPath: String value of the full path of the file to be copied 
	 * @param destinationPath: String value of the full path where the file will be copied
	 * @throws Exception
	 */
	public static void copyFile(String originPath, String destinationPath) throws Exception{
		IFile origin = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(new Path(originPath));
		if (origin == null)
			throw new Exception("The '" + originPath + "' file does not exists!");
		origin.copy(new Path(destinationPath), true, null);
	}

	/**
	 * ATTENTION: works only within a workspace!!!!
	 * @param originPath: String value of the full path of the folder to be copied 
	 * @param destinationPath: String value of the full path where the folder will be copied
	 * @throws Exception
	 */
	public static void copyFolder(String originPath, String destinationPath) throws Exception{
		IFolder origin = ResourcesPlugin.getWorkspace().getRoot().getFolder(new Path(originPath));
		origin.copy(new Path(destinationPath), true, null);
	}

	/**
	 * Transfer a data file from the plug-in into the project
	 * @param pluginName
	 * @param projectName
	 * @param filePath: the relative path of the file (including its name), within the plug-in.
	 * The same path will be used for the copy in the project.
	 * @throws Exception
	 * @deprecated old code, with refresh problems. Use {@link #extractFileFromPlugin(String pluginName,
	 *		String projectName, String folderRelativePath, String fileName)}
	 */
	public static void copyFileFromPlugin(	String pluginName,
			String projectName,
			String filePath) throws Exception{
		String originPath = getPluginPath(pluginName) + filePath;
		String destinationPath = getWorkspacePath() + projectName + filePath;
		copyFile(originPath, destinationPath);
	}

	/**
	 * Transfer a data file from the plug-in into the project
	 * @param pluginName used to retrieve the bundle
	 * @param projectName destination in the workspace
	 * @param folderRelativePath the relative path of the file within the plug-in.
	 * The same path will be used for the copy in the project.
	 * @param fileName
	 * @throws Exception
	 */
	public static void extractFileFromPlugin(String pluginName,
			String projectName,
			String folderRelativePath,
			String fileName) throws Exception{
		String originPath = getPluginPath(pluginName) + folderRelativePath + "/" + fileName;
		IProject project = getProject(projectName);
		IFolder folder = project.getFolder(folderRelativePath);
		if (folder.exists()) {
			// use a FileStream to create the copy of origin file
			IFile destFile = folder.getFile(fileName);
			FileInputStream fileStream = new FileInputStream(originPath);
			destFile.create(fileStream, false, null);
			// create closes the file stream, so no worries.   
		}
		else {
			throw new Exception("The '"+ folderRelativePath + "' destination folder does not exist!");
		}
	}

	public static void copyFile(File source, File dest) throws IOException{
		if(!dest.exists()){
			dest.createNewFile();
		}
		InputStream in = null;
		OutputStream out = null;
		try{
			in = new FileInputStream(source);
			out = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
		}
		finally{
			in.close();
			out.close(); 
		}
	}

	/**
	 * Transfer a data folder from the plug-in into the project
	 * @param pluginName
	 * @param projectName
	 * @param filePath: the relative path of the folder (including its name), within the plug-in.
	 * The same path will be used for the copy in the project.
	 * @throws Exception
	 * @deprecated old code, with refresh problems. Use {@link #extractFolderFromPlugin(String pluginName,
	 *		String projectName, String folderPath)}
	 */
	public static void copyFolderFromPlugin(String pluginName,
			String projectName,
			String filePath) throws Exception{
		String originPath = getPluginPath(pluginName) + "/" + filePath;
		String destinationPath = getWorkspacePath() + projectName + "/" + filePath;
		IFileStore origin = FileBuffers.getFileStoreAtLocation(new Path(originPath));
		// verify the project
		if (getProject(projectName) == null)
			createProject(projectName);
		IFileStore destination = FileBuffers.getFileStoreAtLocation(new Path(destinationPath));
		origin.copy(destination, EFS.OVERWRITE, null);
	}

	/**
	 * Transfer a data folder from the plug-in into the project
	 * @param pluginName
	 * @param projectName
	 * @param filePath: the relative path of the folder (including its name), within the plug-in.
	 * The same path will be used for the copy in the project.
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public static void extractFolderFromPlugin(String pluginName,
			String projectName,
			String folderPath) throws Exception{

		IProject project = getProject(projectName);
		project.open(null);

		IFolder folder = project.getFolder(folderPath);
		folder.create(true, true, null);

		String originPath = getPluginPath(pluginName) + "/" + folderPath;
		File origin = FileBuffers.getSystemFileAtLocation(new Path(originPath));

		if (!origin.exists())
			throw new Exception("The '"+ originPath + "' origin does not exist!");
		if (!origin.isDirectory())
			throw new Exception("The '"+ originPath + "' origin is not a folder!");

		for (int i = 0; i < origin.listFiles().length; i++) {

			File originFile = origin.listFiles()[i];
			if (!originFile.isDirectory()) {
				// use a FileStream to create the copy of origin file
				IFile destFile = folder.getFile(originFile.getName());
				FileInputStream fileStream = new FileInputStream(originPath + "/" + originFile.getName());
				// create closes the file stream, so no worries.   
				destFile.create(fileStream, false, null);
			}
			else {
				// recursive call to copy sub folders
				extractFolderFromPlugin(pluginName, projectName, folderPath + "/" + originFile.getName());
			}
		}
	}

	public static void copyResource(String originPath, String destinationPath) throws Exception{
		IFileStore origin = FileBuffers.getFileStoreAtLocation(new Path(originPath));
		IFileStore destination = FileBuffers.getFileStoreAtLocation(new Path(destinationPath));
		origin.copy(destination, EFS.OVERWRITE, null);
	}

	/**
	 * For debugging purposes, a function which add a line in a fixed textual log file
	 * @param textToLog
	 */
	public static void debugLog(String lineToLog) {
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(getTestMainDirectoryPath() + "/../../DEBUG.log", true));
			dos.writeBytes(lineToLog + "\n");
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * To use when launching an application, in order to be sure all plug-ins have been set up
	 * @return true if all the bundles installed on the current Eclipse are initialized
	 */
	public static boolean arePluginsInitialized() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();

		// Eclipse bug 55901: don't use getConfigElements directly, for pre-3.0
		// compatibility, make sure to allow both missing class
		// attribute and a missing startup element
		IExtensionPoint point = registry.getExtensionPoint(PlatformUI.PLUGIN_ID,"startup");

		final IExtension[] extensions = point.getExtensions();
		if (extensions.length == 0) {
			return true;
		}

		for (int i=0; i<extensions.length; i++) {
			IExtension extension = extensions[i];

			Bundle bundle = Platform.getBundle( extension.getNamespaceIdentifier() );

			// problem with 'org.topcased.toolkit', which is never more than "resolved" without any activator nor activation policy
			boolean initialized = (bundle.getState() == Bundle.ACTIVE)
			|| ((bundle.getState() == Bundle.RESOLVED)
					&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATOR) == null) 
					&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATIONPOLICY) == null ));

			if ( ! initialized ) 
				return false;
		}

		return true;
	}

	public static void closeWorkbench() {

		UIJob job = new UIJob("") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					ResourcesPlugin.getWorkspace().save(true, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
				//PlatformUI.getWorkbench().getActiveWorkbenchWindow().close();
				PlatformUI.getWorkbench().close();
				return org.eclipse.core.runtime.Status.OK_STATUS;
			}	
		};
		job.schedule();
	}

	/**
	 * Extract the readable file within a zipped file.<P>
	 * The zipped archive must embedded an unique file.
	 * 
	 * @param fullZippedFilePath  the physical directory where the file would be found
	 * @param zippedFileName the name of the archive file
	 */
	public static File unzipFile(String fullZippedFileFolderPath, String zippedFileName) throws IOException {

		URL interpreterZipUrl = new URL("file:" + fullZippedFileFolderPath + "/" + zippedFileName);

		ZipInputStream zipFileStream = new ZipInputStream(interpreterZipUrl.openStream());
		zipFileStream.getNextEntry();

		File file = new File(zippedFileName);

		OutputStream os = null;

		try {
			os = new FileOutputStream(file);

			byte[] buffer = new byte[102400];
			while (true) {
				int len = zipFileStream.read(buffer);
				if (zipFileStream.available() == 0)
					break;
				os.write(buffer, 0, len);
			}
		} finally {
			if (null != os) {
				os.close();
			}
		}

		zipFileStream.closeEntry();

		return file;
	}

	/**
	 * Extract the readable file within a zipped file.<P>
	 * The zipped archive must embedded an unique file.
	 * 
	 * @param absoluteZippedFilePath
	 */
	public static File unzipFile(String absoluteZippedFilePath) throws IOException {
		File file = new File(absoluteZippedFilePath);
		
		String fullZippedFileFolderPath = file.getParent();
		String zippedFileName = file.getName();
		
		return unzipFile(fullZippedFileFolderPath, zippedFileName);
	}

	//	public static void displayDebugBox(String title, String textToDisplay) {
	//		MessageBox box = new MessageBox(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
	//		
	//		box.setMessage(textToDisplay);
	//		box.setText("DEBUG "+title);
	//		box.open();
	//	}
	
	public static void waitWorkbench() throws InterruptedException {
		/*
		 * 
		 * We must wait until the workbench has been set up.
		 * 
		 */
		while ( ! PlatformUI.isWorkbenchRunning() ) {
			System.out.println("Waiting for the workbench...");
			Thread.sleep(1000);
		}

		/*
		 * 
		 * We must wait until the plug-ins extended the startup extension are initialized.
		 * 
		 */
		int nbTryLeft = 60 * 2 ; //
		while ( ! arePluginsInitialized() && nbTryLeft > 0) {
			System.out.println("Waiting for the startup plugins...");
			Thread.sleep(1000);	
			nbTryLeft--;
		}
		if(! arePluginsInitialized()){
			String msg = "";
//			for (String s : notInitializedPlugins){
				msg += notInitializedPlugins + ", ";
//			}
			closeWorkbench();
			throw new Error("Didn't succeed to startup the plugins : " + msg);
		}
	}
	
	/**
	 * return the current console which has the focus in the workbench
	 */
	public static IConsole getConsoleByName(String name) {
		IConsole[] consoles = ConsolePlugin.getDefault().getConsoleManager().getConsoles();
		for (int i = 0; i < consoles.length; i++) {
			if (consoles[i].getName().equals(name))
				return consoles[i];
		}
		return null;
	}

	public static IConsole getConsoleByType(Class<?> type) {
		IConsole[] consoles = ConsolePlugin.getDefault().getConsoleManager().getConsoles();
		for (int i = 0; i < consoles.length; i++) {
			if (consoles[i].getClass().equals(type))
				return consoles[i];
		}
		return null;
	}
	
	/**
	 * list all the listeners listening event on the given widget
	 * (for debugging purposes)
	 * @param widget
	 */
	public static void debugListSWTListeners(Widget widget) {
		debugListSWTEventListeners(widget, SWT.MouseEnter);
		debugListSWTEventListeners(widget, SWT.MouseMove);
		debugListSWTEventListeners(widget, SWT.Activate);
		debugListSWTEventListeners(widget, SWT.FocusIn);
		debugListSWTEventListeners(widget, SWT.MouseDown);
		debugListSWTEventListeners(widget, SWT.MouseUp);
		debugListSWTEventListeners(widget, SWT.Selection);
		debugListSWTEventListeners(widget, SWT.MouseHover);
		debugListSWTEventListeners(widget, SWT.MouseMove);
		debugListSWTEventListeners(widget, SWT.MouseExit);
		debugListSWTEventListeners(widget, SWT.Deactivate);
		debugListSWTEventListeners(widget, SWT.FocusOut);
	}
	
	/**
	 * list all the listeners listening event type on the given widget
	 * (for debugging purposes)
	 * @param widget
	 */
	public static void debugListSWTEventListeners(Widget widget, int eventType) {
		Listener[] listeners = widget.getListeners(eventType);
		System.err.println("UiTools.DEBUG   SWT event type "+eventType+" -> "+listeners.length+" listeners ");
		for (int i = 0; i < listeners.length; i++) {
			System.err.println("UiTools.DEBUG   SWT event type "+eventType+" - listener "+listeners[i]);
		}
	}
}
