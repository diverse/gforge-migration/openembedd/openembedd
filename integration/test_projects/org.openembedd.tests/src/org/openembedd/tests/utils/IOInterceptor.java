/**
 * Class for interception of System.out and/or System.err
 */
package org.openembedd.tests.utils;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

/**
 * @author vmahe@irisa.fr
 *
 */
public class IOInterceptor {
	private static PipedInputStream pipeIn;
	private static PrintStream oldErr;
	private static PrintStream oldOut;
	
	public static void startErrCapture() {
		// save previous Err stream
		oldErr = System.err;
		pipeIn = new PipedInputStream();
		try {
			System.setErr(new PrintStream(new PipedOutputStream(pipeIn)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void stopErrCapture() throws IOException {
		// restore previous Err stream
		System.setErr(oldErr);
		pipeIn.close();
	}
	
	public static void startOutCapture() {
		// save previous Out stream
		oldOut = System.out;
		pipeIn = new PipedInputStream();
		try {
			System.setOut(new PrintStream(new PipedOutputStream(pipeIn)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void stopOutCapture() throws IOException {
		// restore previous Out stream
		System.setOut(oldOut);
		pipeIn.close();
	}
	
	public static String getCapturedText() throws IOException {
		StringBuffer buffer = new StringBuffer();
		while (pipeIn.available() > 0) {
			buffer.append((char)pipeIn.read());
		}
		return buffer.toString();
	}
}
