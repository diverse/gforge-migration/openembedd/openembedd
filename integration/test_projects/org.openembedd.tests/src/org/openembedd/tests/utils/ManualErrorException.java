package org.openembedd.tests.utils;

public class ManualErrorException extends Exception {
	private static final long serialVersionUID = 1L;

	public ManualErrorException() {
		// Auto-generated constructor stub
	}

	public ManualErrorException(String message) {
		super(message);
		// Auto-generated constructor stub
	}

	public ManualErrorException(Throwable cause) {
		super(cause);
		// Auto-generated constructor stub
	}

	public ManualErrorException(String message, Throwable cause) {
		super(message, cause);
		// Auto-generated constructor stub
	}

}
