package org.openembedd.tests.utils;

//import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.widgets.MessageBox;
//import org.eclipse.ui.progress.UIJob;
import org.openembedd.tests.utils.ManualTestConsole.QuestionType;

/**
 * Concept of an test oracle which need human decision (non computable oracle).<BR>
 * It will present the task to be done and the expected result to a tester through a dialog.<BR>
 * The tester does the task and verifies the result is the expected one, then he answers FAILED or PASSED.<BR>
 * FAILED choice throws a JUnit AssertionFailed error, which will be traced in the global tests results.
 * 
 * @author vmahe@irisa.fr
 *
 */
public class ManualOracle {
	
	private String oracle;
	
//	private static String TITLE = "Manual oracle: what to verify?";
	
	/**
	 * 
	 */
	public ManualOracle(String oracleText) {
		oracle = oracleText;
	}
	
	/**
	 * create a new dialog which present SetUp and Test tasks to the tester (may throw an error)
	 * @throws ManualErrorException 
	 *
	 */
	public Boolean isSatisfied() {
//		UIJob job = new UIJob("blah") {
//			@Override
//			public IStatus runInUIThread(IProgressMonitor monitor) {
//				MessageBox msgbox = new MessageBox(UiTools.getHandShell(),
//													SWT.ICON_QUESTION | SWT.YES | SWT.NO);
//				msgbox.setText(TITLE);
				
				String msg = "ORACLE:\n" + oracle +
							"\n\n\t-> Has the oracle beeing fulfilled?";
//				msgbox.setMessage(msg);
				
//				int result = msgbox.open();
				
//				if (result == SWT.NO)
//					return StatusConstants.FAILED_STATUS;
//				
//		        return StatusConstants.PASSED_STATUS;
//			}	
//		};
//		IStatus status = UiTools.executeUIJob(job);
		
		ManualTestConsole.setType(QuestionType.ORACLE);
		ManualTestConsole.setMessage(msg);
		IStatus status = ManualTestConsole.open();
		
		return (status == StatusConstants.PASSED_STATUS);
	}
}
