package org.openembedd.tests.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleInputStream;
import org.eclipse.ui.console.IOConsoleOutputStream;

/**
 * A console adapter for manual tests.<BR>
 * The real console could be a graphic Eclipse View.
 *  For the moment, the console is a generic text console.
 * @author vmahe@irisa.fr
 *
 */
public class ManualTestConsole {

	private static IOConsole _console;
	private static IOConsoleOutputStream out;
	private static IOConsoleInputStream in;
	private static BufferedReader bufferedReader;
	
	private static String _message;
	private static QuestionType _type;
	
	private static String ANSWER_DONE = "Done";
	private static String ANSWER_IMPOSSIBLE = "Impossible";
	private static String ANSWER_IGNORE = "Ignore";
	private static String ANSWER_FULFILLED = "Fulfilled";
	private static String ANSWER_FAILED = "Failed";
	
	public enum QuestionType {TEST, ORACLE}
	
	public static void setMessage(String message) { _message = message; }
	public static void setType(QuestionType type) { _type = type; }
	
	/**
	 * singleton pattern
	 */
	protected static IOConsole console() {
		if (_console == null){
			_console = new IOConsole("Manual tests console", null);
			
			out = _console.newOutputStream();
			in = _console.getInputStream();
			
			IOConsole consoles[] = {_console};
			ConsolePlugin.getDefault().getConsoleManager().addConsoles(consoles);
		}
		return _console;
	}
	
	/**
	 * depending on the set type, the console is open, presents the question
	 * and available answers and returns the user's answer.
	 */
	public static IStatus open() {
//		console().clearConsole();
		console().activate();
		
		try {
			out.write(_message);
		} catch (IOException e) { e.printStackTrace(); }
		
		return getAnswer(_type);
	}
	
	private static IStatus getAnswer(QuestionType type) {
		String[] possibleAnswers;
		if (type == QuestionType.TEST) {
			String data[] = {ANSWER_DONE, ANSWER_IMPOSSIBLE, ANSWER_IGNORE};
			possibleAnswers = data;
		} else {
			String data[] = {ANSWER_FULFILLED, ANSWER_FAILED};
			possibleAnswers = data;
		}
		
		String answer = ANSWER_IMPOSSIBLE;	// if default is used, it means error somewhere
		try {
			answer = askUser(possibleAnswers);
		} catch (Exception e) { e.printStackTrace(); }
		
		if (answer.equalsIgnoreCase(ANSWER_IMPOSSIBLE))
			return StatusConstants.ERROR_STATUS;
		
		if (answer.equalsIgnoreCase(ANSWER_IGNORE))
			return StatusConstants.IGNORED_STATUS;
		
		if (answer.equalsIgnoreCase(ANSWER_FAILED))
			return StatusConstants.FAILED_STATUS;
		
		return StatusConstants.PASSED_STATUS;
	}
	private static String askUser(String[] answers) throws Exception {
		
		String answer = "";
		Boolean found = false;
		while (! found) {
			presentAnswers(answers);
			
			if (bufferedReader == null)
				bufferedReader = new BufferedReader(new InputStreamReader(in));
			answer = bufferedReader.readLine();
			
			for (int i = 0; i < answers.length; i++) {
				if (answer.equalsIgnoreCase(answers[i]))
					found = true;
			}
		}
		
		return answer;
	}
	private static void presentAnswers(String[] answers) throws IOException {
		String possibleAnswers = " ( "+answers[0];
		for (int i = 1; i < answers.length; i++) {
			possibleAnswers = possibleAnswers + " / " + answers[i];
		}
		
		out.write(possibleAnswers + " )\n");
	}
}
