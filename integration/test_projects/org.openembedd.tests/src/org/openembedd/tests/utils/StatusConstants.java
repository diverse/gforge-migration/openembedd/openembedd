package org.openembedd.tests.utils;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.openembedd.tests.Constants;

public class StatusConstants {
	
	public static final int PASSED = 0;
	public static final int FAILED = 1;
	public static final int ERROR = 2;
	public static final int IGNORED = 3;

	public static final IStatus PASSED_STATUS = new Status(IStatus.OK, Constants.OPENEMBEDD_TESTS_ID, PASSED, "PASSED", null);
	public static final IStatus FAILED_STATUS = new Status(IStatus.ERROR, Constants.OPENEMBEDD_TESTS_ID, FAILED, "FAILED", null);
	public static final IStatus ERROR_STATUS = new Status(IStatus.ERROR, Constants.OPENEMBEDD_TESTS_ID, ERROR, "ERROR", null);
	public static final IStatus IGNORED_STATUS = new Status(IStatus.CANCEL, Constants.OPENEMBEDD_TESTS_ID, IGNORED, "IGNORED", null);
}
