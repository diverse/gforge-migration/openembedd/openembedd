package org.openembedd.tests;

import org.junit.runner.JUnitCore;

public class TestRunner extends JUnitCore {

	protected String testSuiteName = "";
	protected String testResultFileName = "";
	protected String testResultDirectory = ".";
	protected String interpretedTestsProject = "org.openembedd.tests";
	protected ClassLoader loader = null;

	@SuppressWarnings("unchecked")
	public Object run(Object arg0) throws Exception {
		
		processArgs(arg0);
		
		// we must load the class inside this plug-in 
		// (JUnit does not see "org.openembedd.tests.*" plug-ins)
		// but with the loader of the test plug-in
		Class[] testClassArg = new Class[1];
		testClassArg[0] = Class.forName(testSuiteName, true, loader);
	
		SerializableResult result = new SerializableResult();
		addListener(result.createListener(testSuiteName));
		
		run(testClassArg);
				
		result.saveXML(testResultDirectory, testResultFileName);
		
		// html result name is same as XML file except extension
		String htmlFileName = testResultFileName.substring(0, testResultFileName.length()-4)+".html";
		Serializable2HTML dom = new Serializable2HTML();
		dom.result2HTML(result, testResultDirectory, htmlFileName);
		
		return 0;
	}

	private void processArgs(Object arg0) {
		String[] args = (String[]) arg0;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-testclassname")
				&& args.length >= i)
				testSuiteName = args[i+1];
			
			if (args[i].equalsIgnoreCase("-testresultname")
					&& args.length >= i)
				testResultFileName = args[i+1];
			
			if (args[i].equalsIgnoreCase("-testresultdirectory")
					&& args.length >= i)
				testResultDirectory = args[i+1];
			
			if (args[i].equalsIgnoreCase("-interpretedtestsproject")
					&& args.length >= i)
				interpretedTestsProject = args[i+1];
		}
	}
	
	/**
	 * This method is used by test wrapper in order to set the correct class loader.
	 * See org.openembedd.tests.basicdemo.BasicDemoTestLauncher for example of use.
	 */
	public void setClassLoader(ClassLoader loader) {
		this.loader = loader;
	}
}
