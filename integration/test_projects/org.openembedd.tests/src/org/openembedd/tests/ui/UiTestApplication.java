/**
 * 
 */
package org.openembedd.tests.ui;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.pde.core.plugin.TargetPlatform;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.testing.ITestHarness;
import org.eclipse.ui.testing.TestableObject;
import org.openembedd.tests.TestRunner;
import org.openembedd.tests.utils.UiTools;

/**
 * An application which registers a TestHarness in the Platform
 * and then launches the Eclipse Workbench or a specified equivalent.
 * Need an IApplication (do not process @deprecated IPlatformRunnable)
 * 
 * @author vmahe
 */
public class UiTestApplication implements ITestHarness, IApplication {
	
	// default Eclipse application to be ran
	static String applicationName = "org.eclipse.ui.ide.workbench";
	static String productName = null;
	
	public static String[] args;
	
	// Eclipse test harness
	protected TestableObject uiTestableObject;
	
	/* class loader from the test plug-in (ClassNotFoundError if the
	 * org.openembedd.tests loader is used) so the test plug-in
	 * must extend the current UiTestApplication and force its loader
	 */
	public static ClassLoader loader;
	

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	/**
	 * must be overwriten in the test plug-in which uses it like below,
	 * in order to properly set the class loader (which must be the
	 * one of the test bundle to avoid NoClassDefFound error):<br>
	 * <code>
	 *		public Object start(IApplicationContext context) throws Exception {<br>
	 *			setClassLoader(this.getClass().getClassLoader());<br>
	 *			super.start(context);<br>
	 *			return null;<br>
	 *		}<br>
	 * </code>
	 */
	public Object start(IApplicationContext context) throws Exception {
		processArgs(context);
		
		IApplication application = getApplication();
		
		uiTestableObject = PlatformUI.getTestableObject();
		uiTestableObject.setTestHarness(this);
		
		System.out.println("Launching the "+( productName == null ? "application: " + applicationName : "product: " + productName));
		application.start(context);
		return null;
	}

	protected void processArgs(IApplicationContext context) {
		args = (String[]) context.getArguments().get("application.args");
		if (args == null)
			args = new String[0];
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-uiapplication"))
				applicationName = args[i+1];
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-uiproduct"))
				productName = args[i+1];
		}
		// other arguments are processed by the UiTestRunner 
	}

	private IApplication getApplication() throws Exception {
		//TODO it seems this code would not operate for a product or
		// an application which are not default ones
		IProduct product = Platform.getProduct();
		if (product != null)
			applicationName = TargetPlatform.getDefaultApplication();
		
		IExtension extension = Platform.getExtensionRegistry().getExtension(
				Platform.PI_RUNTIME,
				Platform.PT_APPLICATIONS,
				applicationName);
		
		if (extension == null)
			extension = Platform.getExtensionRegistry().getExtension(
				Platform.PI_RUNTIME,
				Platform.PT_PRODUCT,
				productName);
		
		IConfigurationElement[] elements = extension.getConfigurationElements();
		if (elements.length > 0) {
			IConfigurationElement[] runs = elements[0].getChildren("run");
			if (runs.length > 0) {
				return (IApplication) runs[0].createExecutableExtension("class");
			}
		}
		throw new Exception("Fail to get the instance of UI application '"+applicationName+"'.");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.testing.ITestHarness#runTests()
	 */
	public void runTests() {
		uiTestableObject.testingStarting();
		
		// Distinct thread (needed for SWTBot tests against modal dialogs)
		Job job = new Job("Test thread") {
			protected IStatus run(IProgressMonitor monitor) {
				TestRunner runner = new TestRunner();
				
				// give the current loader to the (external) launcher
				runner.setClassLoader(loader);

				try {
					runner.run(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
		        return Status.OK_STATUS;
			}	
		};
		UiTools.executeJob(job);
		
		// close the workbench after tests
		uiTestableObject.testingFinished();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		// TODO Auto-generated method stub

	}

	public static void setClassLoader(ClassLoader classLoader) {
		loader = classLoader;
	}
}
