package org.openembedd.tests;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

public class SampleTest {

	@Test
	public void aSampleTest() {
		assertTrue("Ca marche !", true);
	}
	
	@Test
	public void aFailureTest() {
		assertFalse("a failure test", true);
	}
	
	@Test
	public void anErrorTest() {
		assertFalse("an error test", 1/0 > 1);
	}
	
	@Test
	@Ignore
	public void anIgnoredTest() {
		assertFalse("an ignored test", true);
	}
}
