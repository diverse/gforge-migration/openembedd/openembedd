package org.openembedd.tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/** 
 * DOM2HTML
 * @author vmahe@irisa.fr
 * 
 * Transform a SerializableResult DOM tree into an HTML file
 */
public class Serializable2HTML implements Constants {

	private Document doc;
	private StringBuffer buffer;
	
	private void processResult(SerializableResult result) {
		doc = result.getDocument();
		buffer = new StringBuffer();
		// generate the HTML document
		processHead(doc.getDocumentElement());
		processRoot(doc.getDocumentElement());
		processEnd();
	}
	
	private void processHead(Element rootElement) {
		String head = "<HTML>\n" +
			"<HEAD>\n" +
			"<TITLE>\n" +
			"Testing "+ rootElement.getAttribute(ATTR_CLASS_NAME) +
			"</TITLE>\n" +
			"</HEAD>\n" +
			"<BODY>\n";
		
		buffer.append(head);
	}
	
	private void processRoot(Element element) {
		// specific generation for tree root ("All" test)
		String rootTitle = "<H2>Testing "+ 
			element.getAttribute(ATTR_NAME) +
			"</H2>\n";
		buffer.append(rootTitle);
		
		String suiteBegin = "<BR>Results:\n" +
			"<BR> - Number of effective tests = "+ element.getAttribute(ATTR_TESTS_COUNT)+"\n" +
			"<BR> - Number of failed tests = "+ element.getAttribute(ATTR_FAILURES_COUNT)+"\n" +
			"<BR> - Number of errors = "+ element.getAttribute(ATTR_ERRORS_COUNT)+"\n" +
			"<BR> - Number of ignored tests = "+ element.getAttribute(ATTR_IGNORED_COUNT)+"\n" +
			"<BR> - Total time = "+ element.getAttribute(ATTR_TOTAL_DURATION)+"\n" +
			"<BR>Test cases :";
		buffer.append(suiteBegin);
		
		// we present tests in sub lists
		String testsListBegin = "<UL>\n";
		buffer.append(testsListBegin);
		for (int i = 0; i < element.getChildNodes().getLength(); i++) {
			String listItemBegin = "<LI>\n";
			buffer.append(listItemBegin);
			
			// sub nodes
			processElement((Element)element.getChildNodes().item(i));
			
			String listItemEnd = "</LI>\n";
			buffer.append(listItemEnd);
		}
		String testsListEnd = "</UL>\n";
		buffer.append(testsListEnd);
	}

	
	private void processElement(Element element) {
		// recursive exploration of the tree
		if (element.hasChildNodes()) {
			
			boolean isSuite = element.getTagName().equalsIgnoreCase(SUITE.toLowerCase());
			if (isSuite) {
				String suiteTitle = "<B>"+ element.getAttribute(ATTR_NAME) +"</B>\n";
				buffer.append(suiteTitle);
				
				// it is a TestSuite so we need to build an itemized list
				String listBegin = "<UL>\n";
				buffer.append(listBegin);
			} else {
				processTestcaseBegin(element);
			}
			
			NodeList childs = element.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				Element child = (Element)childs.item(i);
				if (child.getTagName().equalsIgnoreCase(SUITE.toLowerCase())
						|| child.getTagName().equalsIgnoreCase(TEST.toLowerCase())) {
					//as we have Tests (or even sub TestSuites), current Element is a TestSuite
					String listItemBegin = "<LI>\n";
					buffer.append(listItemBegin);
					
					// sub nodes
					processElement(child);
					
					String listItemEnd = "</LI>\n";
					buffer.append(listItemEnd);
				} else {
					if (isSuite)
						buffer.append("A problem occured before test");
					
					// FAILURE element
					processFailure(child);
				}
				
			}
			if (isSuite) {
				// it is a TestSuite so we need to build an itemized list
				String listEnd = "</UL>\n";
				buffer.append(listEnd);
			}
		} else {
			// testcase with no childs => successful testcase
			processTestcaseBegin(element);
		}
		
		processElementEnd(element);
	}
	
	private void processEnd() {
		String end = "</BODY>";
		
		buffer.append(end);
	}
	
	private void processTestcaseBegin(Element element) {
		buffer.append(getColorFont(element.getAttribute(TEST_STATUS)));
		
		String elementBegin = "TestClass "+ element.getAttribute(ATTR_CLASS_NAME)+ " - test " +
			element.getAttribute(ATTR_METHOD_NAME) + "()";
		buffer.append(elementBegin);
		
		buffer.append(getResultText(element.getAttribute(TEST_STATUS)));
		
		buffer.append("</FONT>\n");
	}
	
	private void processElementEnd(Element element) {
		// nothing to do
	}
	
	private void processFailure(Element failure) {
		// get the trace of the failure
		Text trace = (Text)failure.getChildNodes().item(0);
		//TODO : obsolet => should use CSS styles
		buffer.append("<PRE><FONT size=\"1\">"+ trace.getTextContent() + "</FONT></PRE>\n");
	}
	
	private String getColorFont(String status) {
		String font = "";
		//TODO : obsolet => should use CSS styles
		if (status.equalsIgnoreCase(ATTR_STATUS_PASSED.toLowerCase())) {
			font = "<FONT COLOR=\"green\">";
		} else
			if (status.equalsIgnoreCase(ATTR_STATUS_FAILURE.toLowerCase())) {
				font = "<FONT COLOR=\"orange\">";
			} else
				if (status.equalsIgnoreCase(ATTR_STATUS_ERROR.toLowerCase())) {
					font = "<FONT COLOR=\"red\">";
				} else
					if (status.equalsIgnoreCase(ATTR_STATUS_IGNORED.toLowerCase())) {
						font = "<FONT COLOR=\"grey\">";
					}		return font;
	}
	
	private String getResultText(String status) {
		String result = "";
		if (status.equalsIgnoreCase(ATTR_STATUS_PASSED.toLowerCase())) {
			result = ": PASSED!";
		} else
			if (status.equalsIgnoreCase(ATTR_STATUS_FAILURE.toLowerCase())) {
				result = ": FAILED!";
			} else
				if (status.equalsIgnoreCase(ATTR_STATUS_ERROR.toLowerCase())) {
					result = ": PROGRAM ERROR!";
				} else
					if (status.equalsIgnoreCase(ATTR_STATUS_IGNORED.toLowerCase())) {
						result = ": IGNORED!";
					} else
						result =": a problem has occured before test!";
		return result;
	}
	
	public void result2HTML(SerializableResult result, String directory, String htmlFile) {
		processResult(result);
		try {
			/* Checking for directory existence       	 */
        	File folder = new File( directory );
        	if ( ! folder.exists() )
        		folder.mkdirs();
        	
			FileWriter writer = new FileWriter(directory + System.getProperty("file.separator")+htmlFile);
			writer.write(buffer.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
