package org.openembedd.tests;

import org.junit.runner.Description;

public class TestHelper {

	public static String getClassName(Description description) {
		String name = "";
		// the display name has the form : testMethodName(testClassName)
		int par = description.getDisplayName().indexOf('(');
		name = description.getDisplayName().substring(par+1, description.getDisplayName().length()-1);
		
		return name;
	}

	public static String getMethodName(Description description) {
		String name = "";
		// the display name has the form : testMethodName(testClassName)
		int par = description.getDisplayName().indexOf('(');
		name = par > 0 ? description.getDisplayName().substring(0, par) : "[no name]";
		
		return name;
	}
}
