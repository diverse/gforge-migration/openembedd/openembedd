In order to use the automated tests headless:

0 - copy all "bundles.test.scripts" file in a test directory
  (the place and name is your choice)

1 - unzip the Ant file

2 - export your test plug-in(s) unjared in a zip archive
  within the test directory
  you may setUp the zip name in test.xml
  (search for "JUnit")
  
3 - copy your Eclipse bundle in the test directory
  you may setUp the zip name in version.properties file
  (search for "bundle_name=")
  Change in test.xml the suffixes if needed

4 - add your test as a target at the end of "test.xml"
  and put its version number in version.properties

You may now launch your tests with ./exec.sh or exec.bat
depending the OS you are on