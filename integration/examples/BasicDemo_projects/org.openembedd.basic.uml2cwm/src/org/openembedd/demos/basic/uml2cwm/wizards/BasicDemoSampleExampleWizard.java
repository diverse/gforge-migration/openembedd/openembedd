package org.openembedd.demos.basic.uml2cwm.wizards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openembedd.wizards.AbstractNewExampleWizard;

public class BasicDemoSampleExampleWizard extends AbstractNewExampleWizard
{

	protected Collection<ProjectDescriptor> getProjectDescriptors()
	{
		// We need the statements example to be unzipped along with the
		// EMF library example model, edit and editor examples
		List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>(1);
		projects.add(new ProjectDescriptor("org.openembedd.basic.uml2cwm", "zip/org.openembedd.basic.uml2cwm.demo.zip",
			"org.openembedd.basic.uml2cwm.demo"));
		return projects;
	}
}
