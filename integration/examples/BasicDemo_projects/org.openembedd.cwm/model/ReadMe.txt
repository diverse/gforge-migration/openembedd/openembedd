1) Metamodel UML2 : native in Eclipse
------------------

The uml2 metamodel is always included into OpenEmbeDD project if you have selected the Topcased profil with the UML diagram during the OpenEmbeDD installation.
If this is not the case, you may begin to download the missings plugins by selecting menu 
Help->
  Softwares Updates->
    Find and install...->
, then click radio button "Search for new features to install"
, then click "Next >"
, then if necessary add a "New remote site" for OpenEmbeDD url site : "http://openembedd.org/update"
and complete your OpenEmbeDD configuration by clicking on button "Finish"

2) Metamodel CWM :  result file from Rose->Ecore
------------------

At the origin, the cwm metamodel is described with Rose model format, which we transformed into ecore format by importing with EMF wizard.
Have a look on the file "CWM - problems with Rose model.txt" which joins to the package, to see the warnings and mistakes which appeared during the transformation Rose->Ecore.
So, we can't assure that the file CWM.ecore is the right ecore file for the cwm metamodel, however it is very interesting for a basic demo.


Note: Manage the virtual memory
-------------------------------

To avoid getting an error message such "PermGen space", we advise to add argument for the vm, like below :

-vmargs -Xmx512M -XX:PermSize=64M -XX:MaxPermSize=128M

You can write in the shortcut according to your platform (for example here under Windows XP) :
C:\eclipse_3.2.0_developpement\eclipse.exe -clean -vmargs -Xmx512M -XX:PermSize=64M -XX:MaxPermSize=128M