/**
 * INRIA/IRISA
 *
 * $Id: Extent.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.objectmodel.instance;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Each instance of Extent owns a collection of instances and is used to link such collections to their structural and behavioral definitions in CWM Resource packages. Because Extent is a subclass of package, it owns member instances via the
 * ElementOwnership associaton.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.objectmodel.instance.InstancePackage#getExtent()
 * @model
 * @generated
 */
public interface Extent extends orgomg.cwm.objectmodel.core.Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Extent
