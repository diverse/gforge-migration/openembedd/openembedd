/**
 * INRIA/IRISA
 *
 * $Id: Element.java,v 1.1 2008-04-01 09:35:21 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An element is an atomic constituent of a model. In the metamodel, an Element is the top metaclass in the metaclass hierarchy. Element is an abstract metaclass.
 * 
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.objectmodel.core.CorePackage#getElement()
 * @model abstract="true"
 * @generated
 */
public interface Element extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Element
