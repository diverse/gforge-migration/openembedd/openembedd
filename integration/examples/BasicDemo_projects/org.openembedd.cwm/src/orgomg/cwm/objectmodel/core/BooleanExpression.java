/**
 * INRIA/IRISA
 *
 * $Id: BooleanExpression.java,v 1.1 2008-04-01 09:35:21 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * In the metamodel BooleanExpression defines a statement which will evaluate to an instance of Boolean when it is evaluated.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.objectmodel.core.CorePackage#getBooleanExpression()
 * @model
 * @generated
 */
public interface BooleanExpression extends Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // BooleanExpression
