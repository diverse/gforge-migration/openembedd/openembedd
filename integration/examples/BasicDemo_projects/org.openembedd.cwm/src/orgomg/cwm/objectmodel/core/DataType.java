/**
 * INRIA/IRISA
 *
 * $Id: DataType.java,v 1.1 2008-04-01 09:35:21 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.instance.DataSlot;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A data type is a type whose values have no identity (i.e., they are pure values). Data types include primitive built-in types (such as integer and string) as well as definable enumeration types.
 * 
 * In the metamodel, a DataType defines a special kind of Classifier in which operations are all pure functions (i.e., they can return data values but they cannot change data values, because they have no identity). For example, an "add" operation on a number with another number as an argument yields a third number as a result; the target and argument are unchanged.
 * 
 * A DataType is a special kind of Classifier whose instances are primitive values, not objects. For example, integers and strings are usually treated as primitive values. A primitive value does not have an identity, so two occurrences of the same value cannot be differentiated. Usually, DataTypes are used for specification of the type of an attribute or parameter.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.objectmodel.core.DataType#getDataSlot <em>Data Slot</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.objectmodel.core.CorePackage#getDataType()
 * @model
 * @generated
 */
public interface DataType extends Classifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Data Slot</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.objectmodel.instance.DataSlot}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.objectmodel.instance.DataSlot#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Identifies the DataSlot instances for which the DataType instance is the type of information stored in the DataSlot::dataValue attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Data Slot</em>' reference list.
	 * @see orgomg.cwm.objectmodel.core.CorePackage#getDataType_DataSlot()
	 * @see orgomg.cwm.objectmodel.instance.DataSlot#getDataType
	 * @model opposite="dataType"
	 * @generated
	 */
	EList<DataSlot> getDataSlot();

} // DataType
