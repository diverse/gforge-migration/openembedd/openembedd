/**
 * INRIA/IRISA
 *
 * $Id: NamespaceItemProvider.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import orgomg.cwm.analysis.businessnomenclature.BusinessnomenclatureFactory;

import orgomg.cwm.analysis.datamining.approximation.ApproximationFactory;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesFactory;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportanceFactory;

import orgomg.cwm.analysis.datamining.classification.ClassificationFactory;

import orgomg.cwm.analysis.datamining.clustering.ClusteringFactory;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointFactory;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataFactory;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelFactory;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskFactory;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFactory;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationFactory;

import orgomg.cwm.analysis.olap.OlapFactory;

import orgomg.cwm.analysis.transformation.TransformationFactory;

import orgomg.cwm.foundation.businessinformation.BusinessinformationFactory;

import orgomg.cwm.foundation.datatypes.DatatypesFactory;

import orgomg.cwm.foundation.keysindexes.KeysindexesFactory;

import orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentFactory;

import orgomg.cwm.foundation.typemapping.TypemappingFactory;

import orgomg.cwm.management.warehouseoperation.WarehouseoperationFactory;

import orgomg.cwm.management.warehouseprocess.WarehouseprocessFactory;

import orgomg.cwm.management.warehouseprocess.events.EventsFactory;

import orgomg.cwm.objectmodel.behavioral.BehavioralFactory;

import orgomg.cwm.objectmodel.core.CoreFactory;
import orgomg.cwm.objectmodel.core.CorePackage;
import orgomg.cwm.objectmodel.core.Namespace;

import orgomg.cwm.objectmodel.instance.InstanceFactory;

import orgomg.cwm.objectmodel.relationships.RelationshipsFactory;

import orgomg.cwm.resource.multidimensional.MultidimensionalFactory;

import orgomg.cwm.resource.record.RecordFactory;

import orgomg.cwm.resource.relational.RelationalFactory;

import orgomg.cwm.resource.xml.XmlFactory;

/**
 * This is the item provider adapter for a {@link orgomg.cwm.objectmodel.core.Namespace} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NamespaceItemProvider
	extends ModelElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamespaceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Namespace)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Namespace_type") :
			getString("_UI_Namespace_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Namespace.class)) {
			case CorePackage.NAMESPACE__OWNED_ELEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createPackage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createSubsystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createModel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createDependency()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createStereotype()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createArgument()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createMethod()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BehavioralFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationshipsFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationshipsFactory.eINSTANCE.createAssociationEnd()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationshipsFactory.eINSTANCE.createGeneralization()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InstanceFactory.eINSTANCE.createSlot()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InstanceFactory.eINSTANCE.createDataSlot()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InstanceFactory.eINSTANCE.createDataValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InstanceFactory.eINSTANCE.createExtent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InstanceFactory.eINSTANCE.createObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createResponsibleParty()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createTelephone()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createEmail()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createLocation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createContact()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createDescription()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createDocument()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessinformationFactory.eINSTANCE.createResourceLocator()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 DatatypesFactory.eINSTANCE.createEnumeration()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 DatatypesFactory.eINSTANCE.createEnumerationLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 DatatypesFactory.eINSTANCE.createTypeAlias()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 DatatypesFactory.eINSTANCE.createUnion()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 DatatypesFactory.eINSTANCE.createUnionMember()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 KeysindexesFactory.eINSTANCE.createUniqueKey()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 KeysindexesFactory.eINSTANCE.createIndex()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 KeysindexesFactory.eINSTANCE.createKeyRelationship()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 KeysindexesFactory.eINSTANCE.createIndexedFeature()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createSite()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createMachine()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createSoftwareSystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createDeployedComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createDeployedSoftwareSystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createDataManager()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createDataProvider()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createProviderConnection()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SoftwaredeploymentFactory.eINSTANCE.createPackageUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TypemappingFactory.eINSTANCE.createTypeMapping()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TypemappingFactory.eINSTANCE.createTypeSystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createCatalog()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSchema()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createColumnSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createNamedColumnSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createTable()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createView()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createQueryColumnSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLDistinctType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLSimpleType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLStructuredType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createColumn()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createProcedure()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createTrigger()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLIndex()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createUniqueConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createForeignKey()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLIndexColumn()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createPrimaryKey()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createRow()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createColumnValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createCheckConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createRowSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RelationalFactory.eINSTANCE.createSQLParameter()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createField()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createRecordDef()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createFixedOffsetField()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createRecordFile()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createFieldValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createRecord()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createRecordSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 RecordFactory.eINSTANCE.createGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createDimension()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createDimensionedObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createMember()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createMemberSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createMemberValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MultidimensionalFactory.eINSTANCE.createSchema()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createSchema()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createElementType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createContent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createElementContent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createMixedContent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createElementTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createElement()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 XmlFactory.eINSTANCE.createDocument()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createDataObjectSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationStep()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationActivity()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createPrecedenceConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationUse()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createTransformationTree()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createClassifierMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createFeatureMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createStepPrecedence()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 TransformationFactory.eINSTANCE.createClassifierFeatureMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createContentMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createCube()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createCubeDeployment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createCubeDimensionAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createCubeRegion()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createDeploymentGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createDimension()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createDimensionDeployment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createHierarchyLevelAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createLevelBasedHierarchy()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createMemberSelectionGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createMemberSelection()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createSchema()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createValueBasedHierarchy()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createLevel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createCodedLevel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createMeasure()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createStructureMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 OlapFactory.eINSTANCE.createHierarchyMemberSelectionGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createAttributeAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createAttributeAssignmentSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createAttributeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createAttributeUsageSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoricalAttributeProperties()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createDirectAttributeAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createLogicalAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createLogicalData()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createNumericalAttributeProperties()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createOrdinalAttributeProperties()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createPhysicalData()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createPivotAttributeAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createReversePivotAttributeAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createSetAttributeAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategory()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMap()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMapObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMapObjectEntry()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMapTable()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMatrix()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMatrixEntry()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMatrixObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryMatrixTable()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningdataFactory.eINSTANCE.createCategoryTaxonomy()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningmodelFactory.eINSTANCE.createMiningModel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningmodelFactory.eINSTANCE.createModelSignature()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningmodelFactory.eINSTANCE.createSignatureAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createApplyOutputItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createApplyProbabilityItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createApplyRuleIdItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createApplyScoreItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createApplySourceItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createMiningApplyOutput()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createMiningApplyTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createMiningBuildTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 MiningtaskFactory.eINSTANCE.createMiningTransformation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EntrypointFactory.eINSTANCE.createAuxiliaryObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EntrypointFactory.eINSTANCE.createCatalog()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EntrypointFactory.eINSTANCE.createSchema()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClusteringFactory.eINSTANCE.createClusteringAttributeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClusteringFactory.eINSTANCE.createClusteringFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 AssociationrulesFactory.eINSTANCE.createFrequentItemSetFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 AssociationrulesFactory.eINSTANCE.createAssociationRulesFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 AssociationrulesFactory.eINSTANCE.createSequenceFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SupervisedFactory.eINSTANCE.createLiftAnalysisPoint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SupervisedFactory.eINSTANCE.createLiftAnalysis()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SupervisedFactory.eINSTANCE.createMiningTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SupervisedFactory.eINSTANCE.createSupervisedFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 SupervisedFactory.eINSTANCE.createMiningTestResult()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createApplyTargetValueItem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createClassificationAttributeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createClassificationFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createClassificationTestResult()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createClassificationTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createPriorProbabilities()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ClassificationFactory.eINSTANCE.createPriorProbabilitiesEntry()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ApproximationFactory.eINSTANCE.createApproximationFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ApproximationFactory.eINSTANCE.createApproximationTestResult()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 ApproximationFactory.eINSTANCE.createApproximationTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 AttributeimportanceFactory.eINSTANCE.createAttributeImportanceSettings()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InformationvisualizationFactory.eINSTANCE.createRenderedObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InformationvisualizationFactory.eINSTANCE.createRenderedObjectSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InformationvisualizationFactory.eINSTANCE.createRendering()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 InformationvisualizationFactory.eINSTANCE.createXSLRendering()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createVocabularyElement()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createNomenclature()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createTaxonomy()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createGlossary()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createBusinessDomain()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createConcept()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 BusinessnomenclatureFactory.eINSTANCE.createTerm()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseprocessFactory.eINSTANCE.createWarehouseStep()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseprocessFactory.eINSTANCE.createProcessPackage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseprocessFactory.eINSTANCE.createWarehouseActivity()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createPointInTimeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createCustomCalendarEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createCustomCalendar()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createCalendarDate()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createIntervalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createExternalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createInternalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createCascadeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createRetryEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 EventsFactory.eINSTANCE.createRecurringPointInTimeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseoperationFactory.eINSTANCE.createMeasurement()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseoperationFactory.eINSTANCE.createChangeRequest()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseoperationFactory.eINSTANCE.createTransformationExecution()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseoperationFactory.eINSTANCE.createActivityExecution()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 WarehouseoperationFactory.eINSTANCE.createStepExecution()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CWMEditPlugin.INSTANCE;
	}

}
