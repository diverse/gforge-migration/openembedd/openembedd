/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElementValidator.java,v 1.1 2008-04-01 09:35:23 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.businessnomenclature.VocabularyElement;

import orgomg.cwm.analysis.informationvisualization.RenderedObject;

import orgomg.cwm.analysis.transformation.DataObjectSet;

import orgomg.cwm.foundation.businessinformation.Description;
import orgomg.cwm.foundation.businessinformation.Document;
import orgomg.cwm.foundation.businessinformation.ResponsibleParty;

import orgomg.cwm.foundation.expressions.ElementNode;

import orgomg.cwm.management.warehouseoperation.ChangeRequest;
import orgomg.cwm.management.warehouseoperation.Measurement;

import orgomg.cwm.objectmodel.core.Constraint;
import orgomg.cwm.objectmodel.core.Dependency;
import orgomg.cwm.objectmodel.core.Namespace;
import orgomg.cwm.objectmodel.core.Stereotype;
import orgomg.cwm.objectmodel.core.TaggedValue;
import orgomg.cwm.objectmodel.core.VisibilityKind;

/**
 * A sample validator interface for {@link orgomg.cwm.objectmodel.core.ModelElement}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ModelElementValidator {
	boolean validate();

	boolean validateName(String value);
	boolean validateVisibility(VisibilityKind value);
	boolean validateClientDependency(EList<Dependency> value);
	boolean validateSupplierDependency(EList<Dependency> value);
	boolean validateConstraint(EList<Constraint> value);
	boolean validateNamespace(Namespace value);
	boolean validateImporter(EList<orgomg.cwm.objectmodel.core.Package> value);
	boolean validateStereotype(Stereotype value);
	boolean validateTaggedValue(EList<TaggedValue> value);
	boolean validateDocument(EList<Document> value);
	boolean validateDescription(EList<Description> value);
	boolean validateResponsibleParty(EList<ResponsibleParty> value);
	boolean validateElementNode(EList<ElementNode> value);
	boolean validateSet(EList<DataObjectSet> value);
	boolean validateRenderedObject(EList<RenderedObject> value);
	boolean validateVocabularyElement(EList<VocabularyElement> value);
	boolean validateMeasurement(EList<Measurement> value);
	boolean validateChangeRequest(EList<ChangeRequest> value);
}
