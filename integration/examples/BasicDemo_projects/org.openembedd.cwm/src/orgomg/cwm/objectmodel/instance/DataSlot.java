/**
 * INRIA/IRISA
 *
 * $Id: DataSlot.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.objectmodel.instance;

import orgomg.cwm.objectmodel.core.DataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Slot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Slot which is used to hold a data value where there is no need to manage the value as an element in its own right (in which case a DataValue would be used) - for example it is a one-off string value or a number. The dataValue (and dataType where set) must be consistent with the type of the DataSlot's feature (Attribute) and must obey any constraints on the full descriptor of the Attribute's DataType (including both explicit constraints and built-in constraints such as multiplicity).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.objectmodel.instance.DataSlot#getDataValue <em>Data Value</em>}</li>
 *   <li>{@link orgomg.cwm.objectmodel.instance.DataSlot#getDataType <em>Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.objectmodel.instance.InstancePackage#getDataSlot()
 * @model
 * @generated
 */
public interface DataSlot extends Slot {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Data Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The value for the slot expressed as a string.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Data Value</em>' attribute.
	 * @see #setDataValue(String)
	 * @see orgomg.cwm.objectmodel.instance.InstancePackage#getDataSlot_DataValue()
	 * @model dataType="orgomg.cwm.objectmodel.core.String"
	 * @generated
	 */
	String getDataValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.objectmodel.instance.DataSlot#getDataValue <em>Data Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Value</em>' attribute.
	 * @see #getDataValue()
	 * @generated
	 */
	void setDataValue(String value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.objectmodel.core.DataType#getDataSlot <em>Data Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Identifies the DataType instance representing the type of the information stored in the dataValue attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Data Type</em>' reference.
	 * @see #setDataType(DataType)
	 * @see orgomg.cwm.objectmodel.instance.InstancePackage#getDataSlot_DataType()
	 * @see orgomg.cwm.objectmodel.core.DataType#getDataSlot
	 * @model opposite="dataSlot"
	 * @generated
	 */
	DataType getDataType();

	/**
	 * Sets the value of the '{@link orgomg.cwm.objectmodel.instance.DataSlot#getDataType <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' reference.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataType value);

} // DataSlot
