/**
 * INRIA/IRISA
 *
 * $Id: ObjectmodelResourceFactoryImpl.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.objectmodel.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource Factory</b> associated with the package.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.objectmodel.util.ObjectmodelResourceImpl
 * @generated
 */
public class ObjectmodelResourceFactoryImpl extends ResourceFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates an instance of the resource factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectmodelResourceFactoryImpl() {
		super();
	}

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource createResource(URI uri) {
		Resource result = new ObjectmodelResourceImpl(uri);
		return result;
	}

} //ObjectmodelResourceFactoryImpl
