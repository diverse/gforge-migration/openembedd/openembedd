/**
 * INRIA/IRISA
 *
 * $Id: ExtentImpl.java,v 1.1 2008-04-01 09:35:43 vmahe Exp $
 */
package orgomg.cwm.objectmodel.instance.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.core.impl.PackageImpl;

import orgomg.cwm.objectmodel.instance.Extent;
import orgomg.cwm.objectmodel.instance.InstancePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ExtentImpl extends PackageImpl implements Extent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstancePackage.Literals.EXTENT;
	}

} //ExtentImpl
