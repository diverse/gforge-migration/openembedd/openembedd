/**
 * INRIA/IRISA
 *
 * $Id: ProcedureExpressionImpl.java,v 1.1 2008-04-01 09:35:40 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.core.CorePackage;
import orgomg.cwm.objectmodel.core.ProcedureExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ProcedureExpressionImpl extends ExpressionImpl implements ProcedureExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.PROCEDURE_EXPRESSION;
	}

} //ProcedureExpressionImpl
