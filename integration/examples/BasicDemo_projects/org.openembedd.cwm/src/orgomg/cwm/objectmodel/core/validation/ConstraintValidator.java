/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstraintValidator.java,v 1.1 2008-04-01 09:35:23 vmahe Exp $
 */
package orgomg.cwm.objectmodel.core.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.BooleanExpression;
import orgomg.cwm.objectmodel.core.ModelElement;
import orgomg.cwm.objectmodel.core.Stereotype;

/**
 * A sample validator interface for {@link orgomg.cwm.objectmodel.core.Constraint}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ConstraintValidator {
	boolean validate();

	boolean validateBody(BooleanExpression value);
	boolean validateConstrainedElement(EList<ModelElement> value);
	boolean validateConstrainedStereotype(Stereotype value);
}
