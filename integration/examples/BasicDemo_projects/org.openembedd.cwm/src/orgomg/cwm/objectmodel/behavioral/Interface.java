/**
 * INRIA/IRISA
 *
 * $Id: Interface.java,v 1.1 2008-04-01 09:35:37 vmahe Exp $
 */
package orgomg.cwm.objectmodel.behavioral;

import orgomg.cwm.objectmodel.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Interface is a named set of operations that specify the behavior of an element.
 * 
 * In the metamodel, an Interface contains a set of Operations that together define a service offered by a Classifier realizing the Interface. A Classifier may offer several services, which means that it may realize several Interfaces, and several Classifiers may realize the same Interface.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.objectmodel.behavioral.BehavioralPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends Classifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Interface
