/**
 * INRIA/IRISA
 *
 * $Id: MiningdataPackage.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import orgomg.cwm.objectmodel.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package defines the objects that describe the input data, the way the input data is to be treated, and the mapping between the input data and internal representation for which mining algorithms can understand.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataFactory
 * @model kind="package"
 * @generated
 */
public interface MiningdataPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "miningdata";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/miningcore/miningdata.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.miningcore.miningdata";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MiningdataPackage eINSTANCE = orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl <em>Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeAssignment()
	 * @generated
	 */
	int ATTRIBUTE_ASSIGNMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Order Id Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentSetImpl <em>Attribute Assignment Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentSetImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeAssignmentSet()
	 * @generated
	 */
	int ATTRIBUTE_ASSIGNMENT_SET = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Auxiliary Object</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Assignment Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_ASSIGNMENT_SET_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl <em>Attribute Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeUsage()
	 * @generated
	 */
	int ATTRIBUTE_USAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__VISIBILITY = CorePackage.FEATURE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__CLIENT_DEPENDENCY = CorePackage.FEATURE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__SUPPLIER_DEPENDENCY = CorePackage.FEATURE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__CONSTRAINT = CorePackage.FEATURE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__NAMESPACE = CorePackage.FEATURE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__IMPORTER = CorePackage.FEATURE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__STEREOTYPE = CorePackage.FEATURE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__TAGGED_VALUE = CorePackage.FEATURE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__DOCUMENT = CorePackage.FEATURE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__RESPONSIBLE_PARTY = CorePackage.FEATURE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__ELEMENT_NODE = CorePackage.FEATURE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__SET = CorePackage.FEATURE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__RENDERED_OBJECT = CorePackage.FEATURE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__VOCABULARY_ELEMENT = CorePackage.FEATURE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__MEASUREMENT = CorePackage.FEATURE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__CHANGE_REQUEST = CorePackage.FEATURE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__OWNER_SCOPE = CorePackage.FEATURE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__OWNER = CorePackage.FEATURE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__FEATURE_NODE = CorePackage.FEATURE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__FEATURE_MAP = CorePackage.FEATURE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__CF_MAP = CorePackage.FEATURE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__USAGE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__WEIGHT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Suppress Discretization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Suppress Normalization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE__ATTRIBUTE = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Attribute Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageSetImpl <em>Attribute Usage Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageSetImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeUsageSet()
	 * @generated
	 */
	int ATTRIBUTE_USAGE_SET = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__NAME = CorePackage.CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__VISIBILITY = CorePackage.CLASS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__CLIENT_DEPENDENCY = CorePackage.CLASS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__SUPPLIER_DEPENDENCY = CorePackage.CLASS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__CONSTRAINT = CorePackage.CLASS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__NAMESPACE = CorePackage.CLASS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__IMPORTER = CorePackage.CLASS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__STEREOTYPE = CorePackage.CLASS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__TAGGED_VALUE = CorePackage.CLASS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__DOCUMENT = CorePackage.CLASS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__DESCRIPTION = CorePackage.CLASS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__RESPONSIBLE_PARTY = CorePackage.CLASS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__ELEMENT_NODE = CorePackage.CLASS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__SET = CorePackage.CLASS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__RENDERED_OBJECT = CorePackage.CLASS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__VOCABULARY_ELEMENT = CorePackage.CLASS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__MEASUREMENT = CorePackage.CLASS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__CHANGE_REQUEST = CorePackage.CLASS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__OWNED_ELEMENT = CorePackage.CLASS__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__IS_ABSTRACT = CorePackage.CLASS__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__FEATURE = CorePackage.CLASS__FEATURE;

	/**
	 * The feature id for the '<em><b>Structural Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__STRUCTURAL_FEATURE = CorePackage.CLASS__STRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__PARAMETER = CorePackage.CLASS__PARAMETER;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__GENERALIZATION = CorePackage.CLASS__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__SPECIALIZATION = CorePackage.CLASS__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__INSTANCE = CorePackage.CLASS__INSTANCE;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__ALIAS = CorePackage.CLASS__ALIAS;

	/**
	 * The feature id for the '<em><b>Expression Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__EXPRESSION_NODE = CorePackage.CLASS__EXPRESSION_NODE;

	/**
	 * The feature id for the '<em><b>Mapping From</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__MAPPING_FROM = CorePackage.CLASS__MAPPING_FROM;

	/**
	 * The feature id for the '<em><b>Mapping To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__MAPPING_TO = CorePackage.CLASS__MAPPING_TO;

	/**
	 * The feature id for the '<em><b>Classifier Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__CLASSIFIER_MAP = CorePackage.CLASS__CLASSIFIER_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__CF_MAP = CorePackage.CLASS__CF_MAP;

	/**
	 * The feature id for the '<em><b>Index</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__INDEX = CorePackage.CLASS__INDEX;

	/**
	 * The feature id for the '<em><b>Settings</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET__SETTINGS = CorePackage.CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Usage Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_USAGE_SET_FEATURE_COUNT = CorePackage.CLASS_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl <em>Categorical Attribute Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoricalAttributeProperties()
	 * @generated
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Taxonomy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Category</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Categorical Attribute Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORICAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.DirectAttributeAssignmentImpl <em>Direct Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.DirectAttributeAssignmentImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getDirectAttributeAssignment()
	 * @generated
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__NAME = ATTRIBUTE_ASSIGNMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__VISIBILITY = ATTRIBUTE_ASSIGNMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__CONSTRAINT = ATTRIBUTE_ASSIGNMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__NAMESPACE = ATTRIBUTE_ASSIGNMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__IMPORTER = ATTRIBUTE_ASSIGNMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__STEREOTYPE = ATTRIBUTE_ASSIGNMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE = ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__DOCUMENT = ATTRIBUTE_ASSIGNMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__DESCRIPTION = ATTRIBUTE_ASSIGNMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY = ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE = ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__SET = ATTRIBUTE_ASSIGNMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT = ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT = ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__MEASUREMENT = ATTRIBUTE_ASSIGNMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST = ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Order Id Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Direct Attribute Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECT_ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl <em>Mining Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getMiningAttribute()
	 * @generated
	 */
	int MINING_ATTRIBUTE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__NAME = CorePackage.ATTRIBUTE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__VISIBILITY = CorePackage.ATTRIBUTE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__CLIENT_DEPENDENCY = CorePackage.ATTRIBUTE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__SUPPLIER_DEPENDENCY = CorePackage.ATTRIBUTE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__CONSTRAINT = CorePackage.ATTRIBUTE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__NAMESPACE = CorePackage.ATTRIBUTE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__IMPORTER = CorePackage.ATTRIBUTE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__STEREOTYPE = CorePackage.ATTRIBUTE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__TAGGED_VALUE = CorePackage.ATTRIBUTE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__DOCUMENT = CorePackage.ATTRIBUTE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__DESCRIPTION = CorePackage.ATTRIBUTE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__RESPONSIBLE_PARTY = CorePackage.ATTRIBUTE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__ELEMENT_NODE = CorePackage.ATTRIBUTE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__SET = CorePackage.ATTRIBUTE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__RENDERED_OBJECT = CorePackage.ATTRIBUTE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__VOCABULARY_ELEMENT = CorePackage.ATTRIBUTE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__MEASUREMENT = CorePackage.ATTRIBUTE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__CHANGE_REQUEST = CorePackage.ATTRIBUTE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__OWNER_SCOPE = CorePackage.ATTRIBUTE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__OWNER = CorePackage.ATTRIBUTE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__FEATURE_NODE = CorePackage.ATTRIBUTE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__FEATURE_MAP = CorePackage.ATTRIBUTE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__CF_MAP = CorePackage.ATTRIBUTE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__CHANGEABILITY = CorePackage.ATTRIBUTE__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__MULTIPLICITY = CorePackage.ATTRIBUTE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__ORDERING = CorePackage.ATTRIBUTE__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__TARGET_SCOPE = CorePackage.ATTRIBUTE__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__TYPE = CorePackage.ATTRIBUTE__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__SLOT = CorePackage.ATTRIBUTE__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__DISCRIMINATED_UNION = CorePackage.ATTRIBUTE__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__INDEXED_FEATURE = CorePackage.ATTRIBUTE__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__KEY_RELATIONSHIP = CorePackage.ATTRIBUTE__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__UNIQUE_KEY = CorePackage.ATTRIBUTE__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__INITIAL_VALUE = CorePackage.ATTRIBUTE__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__DISPLAY_NAME = CorePackage.ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE__ATTRIBUTE_TYPE = CorePackage.ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Mining Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_ATTRIBUTE_FEATURE_COUNT = CorePackage.ATTRIBUTE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalAttributeImpl <em>Logical Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalAttributeImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getLogicalAttribute()
	 * @generated
	 */
	int LOGICAL_ATTRIBUTE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__NAME = MINING_ATTRIBUTE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__VISIBILITY = MINING_ATTRIBUTE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CLIENT_DEPENDENCY = MINING_ATTRIBUTE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__SUPPLIER_DEPENDENCY = MINING_ATTRIBUTE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CONSTRAINT = MINING_ATTRIBUTE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__NAMESPACE = MINING_ATTRIBUTE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__IMPORTER = MINING_ATTRIBUTE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__STEREOTYPE = MINING_ATTRIBUTE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__TAGGED_VALUE = MINING_ATTRIBUTE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__DOCUMENT = MINING_ATTRIBUTE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__DESCRIPTION = MINING_ATTRIBUTE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__RESPONSIBLE_PARTY = MINING_ATTRIBUTE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__ELEMENT_NODE = MINING_ATTRIBUTE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__SET = MINING_ATTRIBUTE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__RENDERED_OBJECT = MINING_ATTRIBUTE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__VOCABULARY_ELEMENT = MINING_ATTRIBUTE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__MEASUREMENT = MINING_ATTRIBUTE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CHANGE_REQUEST = MINING_ATTRIBUTE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__OWNER_SCOPE = MINING_ATTRIBUTE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__OWNER = MINING_ATTRIBUTE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__FEATURE_NODE = MINING_ATTRIBUTE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__FEATURE_MAP = MINING_ATTRIBUTE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CF_MAP = MINING_ATTRIBUTE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CHANGEABILITY = MINING_ATTRIBUTE__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__MULTIPLICITY = MINING_ATTRIBUTE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__ORDERING = MINING_ATTRIBUTE__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__TARGET_SCOPE = MINING_ATTRIBUTE__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__TYPE = MINING_ATTRIBUTE__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__SLOT = MINING_ATTRIBUTE__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__DISCRIMINATED_UNION = MINING_ATTRIBUTE__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__INDEXED_FEATURE = MINING_ATTRIBUTE__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__KEY_RELATIONSHIP = MINING_ATTRIBUTE__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__UNIQUE_KEY = MINING_ATTRIBUTE__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__INITIAL_VALUE = MINING_ATTRIBUTE__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__DISPLAY_NAME = MINING_ATTRIBUTE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__ATTRIBUTE_TYPE = MINING_ATTRIBUTE__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Is Set Valued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__IS_SET_VALUED = MINING_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Categorical Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__CATEGORICAL_PROPERTIES = MINING_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Numerical Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE__NUMERICAL_PROPERTIES = MINING_ATTRIBUTE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Logical Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_ATTRIBUTE_FEATURE_COUNT = MINING_ATTRIBUTE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalDataImpl <em>Logical Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalDataImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getLogicalData()
	 * @generated
	 */
	int LOGICAL_DATA = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__NAME = CorePackage.CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__VISIBILITY = CorePackage.CLASS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__CLIENT_DEPENDENCY = CorePackage.CLASS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__SUPPLIER_DEPENDENCY = CorePackage.CLASS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__CONSTRAINT = CorePackage.CLASS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__NAMESPACE = CorePackage.CLASS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__IMPORTER = CorePackage.CLASS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__STEREOTYPE = CorePackage.CLASS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__TAGGED_VALUE = CorePackage.CLASS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__DOCUMENT = CorePackage.CLASS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__DESCRIPTION = CorePackage.CLASS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__RESPONSIBLE_PARTY = CorePackage.CLASS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__ELEMENT_NODE = CorePackage.CLASS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__SET = CorePackage.CLASS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__RENDERED_OBJECT = CorePackage.CLASS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__VOCABULARY_ELEMENT = CorePackage.CLASS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__MEASUREMENT = CorePackage.CLASS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__CHANGE_REQUEST = CorePackage.CLASS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__OWNED_ELEMENT = CorePackage.CLASS__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__IS_ABSTRACT = CorePackage.CLASS__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__FEATURE = CorePackage.CLASS__FEATURE;

	/**
	 * The feature id for the '<em><b>Structural Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__STRUCTURAL_FEATURE = CorePackage.CLASS__STRUCTURAL_FEATURE;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__PARAMETER = CorePackage.CLASS__PARAMETER;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__GENERALIZATION = CorePackage.CLASS__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__SPECIALIZATION = CorePackage.CLASS__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__INSTANCE = CorePackage.CLASS__INSTANCE;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__ALIAS = CorePackage.CLASS__ALIAS;

	/**
	 * The feature id for the '<em><b>Expression Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__EXPRESSION_NODE = CorePackage.CLASS__EXPRESSION_NODE;

	/**
	 * The feature id for the '<em><b>Mapping From</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__MAPPING_FROM = CorePackage.CLASS__MAPPING_FROM;

	/**
	 * The feature id for the '<em><b>Mapping To</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__MAPPING_TO = CorePackage.CLASS__MAPPING_TO;

	/**
	 * The feature id for the '<em><b>Classifier Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__CLASSIFIER_MAP = CorePackage.CLASS__CLASSIFIER_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__CF_MAP = CorePackage.CLASS__CF_MAP;

	/**
	 * The feature id for the '<em><b>Index</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__INDEX = CorePackage.CLASS__INDEX;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA__SCHEMA = CorePackage.CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Logical Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_DATA_FEATURE_COUNT = CorePackage.CLASS_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl <em>Numerical Attribute Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getNumericalAttributeProperties()
	 * @generated
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Discrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Cyclic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Anchor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Cycle Begin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Cycle End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Discrete Step Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Numerical Attribute Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERICAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl <em>Ordinal Attribute Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getOrdinalAttributeProperties()
	 * @generated
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__NAME = CATEGORICAL_ATTRIBUTE_PROPERTIES__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__VISIBILITY = CATEGORICAL_ATTRIBUTE_PROPERTIES__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__CLIENT_DEPENDENCY = CATEGORICAL_ATTRIBUTE_PROPERTIES__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__SUPPLIER_DEPENDENCY = CATEGORICAL_ATTRIBUTE_PROPERTIES__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__CONSTRAINT = CATEGORICAL_ATTRIBUTE_PROPERTIES__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__NAMESPACE = CATEGORICAL_ATTRIBUTE_PROPERTIES__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__IMPORTER = CATEGORICAL_ATTRIBUTE_PROPERTIES__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__STEREOTYPE = CATEGORICAL_ATTRIBUTE_PROPERTIES__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__TAGGED_VALUE = CATEGORICAL_ATTRIBUTE_PROPERTIES__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__DOCUMENT = CATEGORICAL_ATTRIBUTE_PROPERTIES__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__DESCRIPTION = CATEGORICAL_ATTRIBUTE_PROPERTIES__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__RESPONSIBLE_PARTY = CATEGORICAL_ATTRIBUTE_PROPERTIES__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__ELEMENT_NODE = CATEGORICAL_ATTRIBUTE_PROPERTIES__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__SET = CATEGORICAL_ATTRIBUTE_PROPERTIES__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__RENDERED_OBJECT = CATEGORICAL_ATTRIBUTE_PROPERTIES__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__VOCABULARY_ELEMENT = CATEGORICAL_ATTRIBUTE_PROPERTIES__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__MEASUREMENT = CATEGORICAL_ATTRIBUTE_PROPERTIES__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__CHANGE_REQUEST = CATEGORICAL_ATTRIBUTE_PROPERTIES__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE = CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Taxonomy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__TAXONOMY = CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY;

	/**
	 * The feature id for the '<em><b>Category</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__CATEGORY = CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY;

	/**
	 * The feature id for the '<em><b>Order Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE = CATEGORICAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Cyclic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC = CATEGORICAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ordinal Attribute Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDINAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT = CATEGORICAL_ATTRIBUTE_PROPERTIES_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PhysicalDataImpl <em>Physical Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PhysicalDataImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getPhysicalData()
	 * @generated
	 */
	int PHYSICAL_DATA = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA__SOURCE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_DATA_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PivotAttributeAssignmentImpl <em>Pivot Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PivotAttributeAssignmentImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getPivotAttributeAssignment()
	 * @generated
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__NAME = ATTRIBUTE_ASSIGNMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__VISIBILITY = ATTRIBUTE_ASSIGNMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__CONSTRAINT = ATTRIBUTE_ASSIGNMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__NAMESPACE = ATTRIBUTE_ASSIGNMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__IMPORTER = ATTRIBUTE_ASSIGNMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__STEREOTYPE = ATTRIBUTE_ASSIGNMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE = ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__DOCUMENT = ATTRIBUTE_ASSIGNMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__DESCRIPTION = ATTRIBUTE_ASSIGNMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY = ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE = ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__SET = ATTRIBUTE_ASSIGNMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT = ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT = ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__MEASUREMENT = ATTRIBUTE_ASSIGNMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST = ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Order Id Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Set Id Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__NAME_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Pivot Attribute Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIVOT_ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl <em>Reverse Pivot Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getReversePivotAttributeAssignment()
	 * @generated
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__NAME = ATTRIBUTE_ASSIGNMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VISIBILITY = ATTRIBUTE_ASSIGNMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__CONSTRAINT = ATTRIBUTE_ASSIGNMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__NAMESPACE = ATTRIBUTE_ASSIGNMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__IMPORTER = ATTRIBUTE_ASSIGNMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__STEREOTYPE = ATTRIBUTE_ASSIGNMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE = ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__DOCUMENT = ATTRIBUTE_ASSIGNMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__DESCRIPTION = ATTRIBUTE_ASSIGNMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY = ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE = ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SET = ATTRIBUTE_ASSIGNMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT = ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT = ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__MEASUREMENT = ATTRIBUTE_ASSIGNMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST = ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Order Id Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Attribute Selection Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Selection Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Selector Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Reverse Pivot Attribute Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl <em>Set Attribute Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getSetAttributeAssignment()
	 * @generated
	 */
	int SET_ATTRIBUTE_ASSIGNMENT = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__NAME = ATTRIBUTE_ASSIGNMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__VISIBILITY = ATTRIBUTE_ASSIGNMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY = ATTRIBUTE_ASSIGNMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__CONSTRAINT = ATTRIBUTE_ASSIGNMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__NAMESPACE = ATTRIBUTE_ASSIGNMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__IMPORTER = ATTRIBUTE_ASSIGNMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__STEREOTYPE = ATTRIBUTE_ASSIGNMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE = ATTRIBUTE_ASSIGNMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__DOCUMENT = ATTRIBUTE_ASSIGNMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__DESCRIPTION = ATTRIBUTE_ASSIGNMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY = ATTRIBUTE_ASSIGNMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE = ATTRIBUTE_ASSIGNMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__SET = ATTRIBUTE_ASSIGNMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT = ATTRIBUTE_ASSIGNMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT = ATTRIBUTE_ASSIGNMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__MEASUREMENT = ATTRIBUTE_ASSIGNMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST = ATTRIBUTE_ASSIGNMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Logical Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Order Id Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Set Id Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Member Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Set Attribute Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT = ATTRIBUTE_ASSIGNMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl <em>Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategory()
	 * @generated
	 */
	int CATEGORY = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__VALUE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Null Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__IS_NULL_CATEGORY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__DISPLAY_NAME = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__PROPERTY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Prior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__PRIOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Categorical Properties</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CATEGORICAL_PROPERTIES = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl <em>Category Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMap()
	 * @generated
	 */
	int CATEGORY_MAP = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Is Multi Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__IS_MULTI_LEVEL = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Item Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__IS_ITEM_MAP = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Taxonomy</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP__TAXONOMY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Category Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectImpl <em>Category Map Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapObject()
	 * @generated
	 */
	int CATEGORY_MAP_OBJECT = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__NAME = CATEGORY_MAP__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__VISIBILITY = CATEGORY_MAP__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__CLIENT_DEPENDENCY = CATEGORY_MAP__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__SUPPLIER_DEPENDENCY = CATEGORY_MAP__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__CONSTRAINT = CATEGORY_MAP__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__NAMESPACE = CATEGORY_MAP__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__IMPORTER = CATEGORY_MAP__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__STEREOTYPE = CATEGORY_MAP__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__TAGGED_VALUE = CATEGORY_MAP__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__DOCUMENT = CATEGORY_MAP__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__DESCRIPTION = CATEGORY_MAP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__RESPONSIBLE_PARTY = CATEGORY_MAP__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__ELEMENT_NODE = CATEGORY_MAP__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__SET = CATEGORY_MAP__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__RENDERED_OBJECT = CATEGORY_MAP__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__VOCABULARY_ELEMENT = CATEGORY_MAP__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__MEASUREMENT = CATEGORY_MAP__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__CHANGE_REQUEST = CATEGORY_MAP__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Is Multi Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__IS_MULTI_LEVEL = CATEGORY_MAP__IS_MULTI_LEVEL;

	/**
	 * The feature id for the '<em><b>Is Item Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__IS_ITEM_MAP = CATEGORY_MAP__IS_ITEM_MAP;

	/**
	 * The feature id for the '<em><b>Taxonomy</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__TAXONOMY = CATEGORY_MAP__TAXONOMY;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT__ENTRY = CATEGORY_MAP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Category Map Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_FEATURE_COUNT = CATEGORY_MAP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl <em>Category Map Object Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapObjectEntry()
	 * @generated
	 */
	int CATEGORY_MAP_OBJECT_ENTRY = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Graph Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Map Object</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Child</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__CHILD = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY__PARENT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Category Map Object Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_OBJECT_ENTRY_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl <em>Category Map Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapTable()
	 * @generated
	 */
	int CATEGORY_MAP_TABLE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__NAME = CATEGORY_MAP__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__VISIBILITY = CATEGORY_MAP__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__CLIENT_DEPENDENCY = CATEGORY_MAP__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__SUPPLIER_DEPENDENCY = CATEGORY_MAP__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__CONSTRAINT = CATEGORY_MAP__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__NAMESPACE = CATEGORY_MAP__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__IMPORTER = CATEGORY_MAP__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__STEREOTYPE = CATEGORY_MAP__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__TAGGED_VALUE = CATEGORY_MAP__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__DOCUMENT = CATEGORY_MAP__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__DESCRIPTION = CATEGORY_MAP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__RESPONSIBLE_PARTY = CATEGORY_MAP__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__ELEMENT_NODE = CATEGORY_MAP__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__SET = CATEGORY_MAP__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__RENDERED_OBJECT = CATEGORY_MAP__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__VOCABULARY_ELEMENT = CATEGORY_MAP__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__MEASUREMENT = CATEGORY_MAP__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__CHANGE_REQUEST = CATEGORY_MAP__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Is Multi Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__IS_MULTI_LEVEL = CATEGORY_MAP__IS_MULTI_LEVEL;

	/**
	 * The feature id for the '<em><b>Is Item Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__IS_ITEM_MAP = CATEGORY_MAP__IS_ITEM_MAP;

	/**
	 * The feature id for the '<em><b>Taxonomy</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__TAXONOMY = CATEGORY_MAP__TAXONOMY;

	/**
	 * The feature id for the '<em><b>Child Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE = CATEGORY_MAP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Table</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__TABLE = CATEGORY_MAP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Graph Id Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE = CATEGORY_MAP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE = CATEGORY_MAP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Category Map Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MAP_TABLE_FEATURE_COUNT = CATEGORY_MAP_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl <em>Category Matrix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrix()
	 * @generated
	 */
	int CATEGORY_MATRIX = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__DIAGONAL_DEFAULT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Off Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__KIND = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__CATEGORY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__SCHEMA = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Test Result</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX__TEST_RESULT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Category Matrix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl <em>Category Matrix Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixEntry()
	 * @generated
	 */
	int CATEGORY_MATRIX_ENTRY = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__VALUE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Column Index</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__COLUMN_INDEX = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Category Matrix</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Row Index</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY__ROW_INDEX = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Category Matrix Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_ENTRY_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixObjectImpl <em>Category Matrix Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixObjectImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixObject()
	 * @generated
	 */
	int CATEGORY_MATRIX_OBJECT = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__NAME = CATEGORY_MATRIX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__VISIBILITY = CATEGORY_MATRIX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__CLIENT_DEPENDENCY = CATEGORY_MATRIX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__SUPPLIER_DEPENDENCY = CATEGORY_MATRIX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__CONSTRAINT = CATEGORY_MATRIX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__NAMESPACE = CATEGORY_MATRIX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__IMPORTER = CATEGORY_MATRIX__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__STEREOTYPE = CATEGORY_MATRIX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__TAGGED_VALUE = CATEGORY_MATRIX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__DOCUMENT = CATEGORY_MATRIX__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__DESCRIPTION = CATEGORY_MATRIX__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__RESPONSIBLE_PARTY = CATEGORY_MATRIX__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__ELEMENT_NODE = CATEGORY_MATRIX__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__SET = CATEGORY_MATRIX__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__RENDERED_OBJECT = CATEGORY_MATRIX__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__VOCABULARY_ELEMENT = CATEGORY_MATRIX__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__MEASUREMENT = CATEGORY_MATRIX__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__CHANGE_REQUEST = CATEGORY_MATRIX__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__DIAGONAL_DEFAULT = CATEGORY_MATRIX__DIAGONAL_DEFAULT;

	/**
	 * The feature id for the '<em><b>Off Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__OFF_DIAGONAL_DEFAULT = CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__KIND = CATEGORY_MATRIX__KIND;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__CATEGORY = CATEGORY_MATRIX__CATEGORY;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__SCHEMA = CATEGORY_MATRIX__SCHEMA;

	/**
	 * The feature id for the '<em><b>Test Result</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__TEST_RESULT = CATEGORY_MATRIX__TEST_RESULT;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT__ENTRY = CATEGORY_MATRIX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Category Matrix Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_OBJECT_FEATURE_COUNT = CATEGORY_MATRIX_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl <em>Category Matrix Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixTable()
	 * @generated
	 */
	int CATEGORY_MATRIX_TABLE = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__NAME = CATEGORY_MATRIX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__VISIBILITY = CATEGORY_MATRIX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__CLIENT_DEPENDENCY = CATEGORY_MATRIX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__SUPPLIER_DEPENDENCY = CATEGORY_MATRIX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__CONSTRAINT = CATEGORY_MATRIX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__NAMESPACE = CATEGORY_MATRIX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__IMPORTER = CATEGORY_MATRIX__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__STEREOTYPE = CATEGORY_MATRIX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__TAGGED_VALUE = CATEGORY_MATRIX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__DOCUMENT = CATEGORY_MATRIX__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__DESCRIPTION = CATEGORY_MATRIX__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__RESPONSIBLE_PARTY = CATEGORY_MATRIX__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__ELEMENT_NODE = CATEGORY_MATRIX__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__SET = CATEGORY_MATRIX__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__RENDERED_OBJECT = CATEGORY_MATRIX__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__VOCABULARY_ELEMENT = CATEGORY_MATRIX__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__MEASUREMENT = CATEGORY_MATRIX__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__CHANGE_REQUEST = CATEGORY_MATRIX__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__DIAGONAL_DEFAULT = CATEGORY_MATRIX__DIAGONAL_DEFAULT;

	/**
	 * The feature id for the '<em><b>Off Diagonal Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__OFF_DIAGONAL_DEFAULT = CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__KIND = CATEGORY_MATRIX__KIND;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__CATEGORY = CATEGORY_MATRIX__CATEGORY;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__SCHEMA = CATEGORY_MATRIX__SCHEMA;

	/**
	 * The feature id for the '<em><b>Test Result</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__TEST_RESULT = CATEGORY_MATRIX__TEST_RESULT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__SOURCE = CATEGORY_MATRIX_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Column Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE = CATEGORY_MATRIX_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Row Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE = CATEGORY_MATRIX_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE = CATEGORY_MATRIX_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Category Matrix Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_MATRIX_TABLE_FEATURE_COUNT = CATEGORY_MATRIX_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl <em>Category Taxonomy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryTaxonomy()
	 * @generated
	 */
	int CATEGORY_TAXONOMY = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Category Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__CATEGORY_MAP = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__ROOT_CATEGORY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY__SCHEMA = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Category Taxonomy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TAXONOMY_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction <em>Attribute Selection Function</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeSelectionFunction()
	 * @generated
	 */
	int ATTRIBUTE_SELECTION_FUNCTION = 25;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType <em>Attribute Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeType()
	 * @generated
	 */
	int ATTRIBUTE_TYPE = 26;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType <em>Order Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getOrderType()
	 * @generated
	 */
	int ORDER_TYPE = 27;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption <em>Usage Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getUsageOption()
	 * @generated
	 */
	int USAGE_OPTION = 28;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction <em>Value Selection Function</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getValueSelectionFunction()
	 * @generated
	 */
	int VALUE_SELECTION_FUNCTION = 29;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty <em>Category Property</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryProperty()
	 * @generated
	 */
	int CATEGORY_PROPERTY = 30;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty <em>Matrix Property</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getMatrixProperty()
	 * @generated
	 */
	int MATRIX_PROPERTY = 31;

	/**
	 * The meta object id for the '<em>Double</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getDouble()
	 * @generated
	 */
	int DOUBLE = 32;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment <em>Attribute Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment
	 * @generated
	 */
	EClass getAttributeAssignment();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getLogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Logical Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getLogicalAttribute()
	 * @see #getAttributeAssignment()
	 * @generated
	 */
	EReference getAttributeAssignment_LogicalAttribute();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getOrderIdAttribute <em>Order Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Order Id Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getOrderIdAttribute()
	 * @see #getAttributeAssignment()
	 * @generated
	 */
	EReference getAttributeAssignment_OrderIdAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet <em>Attribute Assignment Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Assignment Set</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet
	 * @generated
	 */
	EClass getAttributeAssignmentSet();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject <em>Auxiliary Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Auxiliary Object</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject()
	 * @see #getAttributeAssignmentSet()
	 * @generated
	 */
	EReference getAttributeAssignmentSet_AuxiliaryObject();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage <em>Attribute Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Usage</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage
	 * @generated
	 */
	EClass getAttributeUsage();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getUsage <em>Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Usage</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getUsage()
	 * @see #getAttributeUsage()
	 * @generated
	 */
	EAttribute getAttributeUsage_Usage();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getWeight()
	 * @see #getAttributeUsage()
	 * @generated
	 */
	EAttribute getAttributeUsage_Weight();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressDiscretization <em>Suppress Discretization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suppress Discretization</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressDiscretization()
	 * @see #getAttributeUsage()
	 * @generated
	 */
	EAttribute getAttributeUsage_SuppressDiscretization();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressNormalization <em>Suppress Normalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suppress Normalization</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressNormalization()
	 * @see #getAttributeUsage()
	 * @generated
	 */
	EAttribute getAttributeUsage_SuppressNormalization();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getAttribute()
	 * @see #getAttributeUsage()
	 * @generated
	 */
	EReference getAttributeUsage_Attribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet <em>Attribute Usage Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Usage Set</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet
	 * @generated
	 */
	EClass getAttributeUsageSet();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings()
	 * @see #getAttributeUsageSet()
	 * @generated
	 */
	EReference getAttributeUsageSet_Settings();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties <em>Categorical Attribute Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Categorical Attribute Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties
	 * @generated
	 */
	EClass getCategoricalAttributeProperties();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Logical Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute()
	 * @see #getCategoricalAttributeProperties()
	 * @generated
	 */
	EReference getCategoricalAttributeProperties_LogicalAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getTaxonomy <em>Taxonomy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Taxonomy</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getTaxonomy()
	 * @see #getCategoricalAttributeProperties()
	 * @generated
	 */
	EReference getCategoricalAttributeProperties_Taxonomy();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getCategory()
	 * @see #getCategoricalAttributeProperties()
	 * @generated
	 */
	EReference getCategoricalAttributeProperties_Category();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.DirectAttributeAssignment <em>Direct Attribute Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Direct Attribute Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.DirectAttributeAssignment
	 * @generated
	 */
	EClass getDirectAttributeAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.DirectAttributeAssignment#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.DirectAttributeAssignment#getAttribute()
	 * @see #getDirectAttributeAssignment()
	 * @generated
	 */
	EReference getDirectAttributeAssignment_Attribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute
	 * @generated
	 */
	EClass getLogicalAttribute();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#isIsSetValued <em>Is Set Valued</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Set Valued</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#isIsSetValued()
	 * @see #getLogicalAttribute()
	 * @generated
	 */
	EAttribute getLogicalAttribute_IsSetValued();

	/**
	 * Returns the meta object for the containment reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties <em>Categorical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Categorical Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties()
	 * @see #getLogicalAttribute()
	 * @generated
	 */
	EReference getLogicalAttribute_CategoricalProperties();

	/**
	 * Returns the meta object for the containment reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties <em>Numerical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Numerical Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties()
	 * @see #getLogicalAttribute()
	 * @generated
	 */
	EReference getLogicalAttribute_NumericalProperties();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData <em>Logical Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Data</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData
	 * @generated
	 */
	EClass getLogicalData();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Schema</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema()
	 * @see #getLogicalData()
	 * @generated
	 */
	EReference getLogicalData_Schema();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute <em>Mining Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute
	 * @generated
	 */
	EClass getMiningAttribute();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getDisplayName <em>Display Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Name</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getDisplayName()
	 * @see #getMiningAttribute()
	 * @generated
	 */
	EAttribute getMiningAttribute_DisplayName();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Type</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getAttributeType()
	 * @see #getMiningAttribute()
	 * @generated
	 */
	EAttribute getMiningAttribute_AttributeType();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties <em>Numerical Attribute Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Numerical Attribute Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties
	 * @generated
	 */
	EClass getNumericalAttributeProperties();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLowerBound <em>Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Bound</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLowerBound()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_LowerBound();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getUpperBound <em>Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getUpperBound()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_UpperBound();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsDiscrete <em>Is Discrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Discrete</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsDiscrete()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_IsDiscrete();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Cyclic</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsCyclic()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_IsCyclic();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getAnchor <em>Anchor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Anchor</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getAnchor()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_Anchor();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleBegin <em>Cycle Begin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cycle Begin</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleBegin()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_CycleBegin();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleEnd <em>Cycle End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cycle End</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleEnd()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_CycleEnd();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getDiscreteStepSize <em>Discrete Step Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discrete Step Size</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getDiscreteStepSize()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EAttribute getNumericalAttributeProperties_DiscreteStepSize();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Logical Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute()
	 * @see #getNumericalAttributeProperties()
	 * @generated
	 */
	EReference getNumericalAttributeProperties_LogicalAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties <em>Ordinal Attribute Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ordinal Attribute Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties
	 * @generated
	 */
	EClass getOrdinalAttributeProperties();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#getOrderType <em>Order Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order Type</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#getOrderType()
	 * @see #getOrdinalAttributeProperties()
	 * @generated
	 */
	EAttribute getOrdinalAttributeProperties_OrderType();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Cyclic</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#isIsCyclic()
	 * @see #getOrdinalAttributeProperties()
	 * @generated
	 */
	EAttribute getOrdinalAttributeProperties_IsCyclic();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData <em>Physical Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Data</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData
	 * @generated
	 */
	EClass getPhysicalData();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData#getSource()
	 * @see #getPhysicalData()
	 * @generated
	 */
	EReference getPhysicalData_Source();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment <em>Pivot Attribute Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pivot Attribute Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment
	 * @generated
	 */
	EClass getPivotAttributeAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Set Id Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getSetIdAttribute()
	 * @see #getPivotAttributeAssignment()
	 * @generated
	 */
	EReference getPivotAttributeAssignment_SetIdAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getNameAttribute <em>Name Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getNameAttribute()
	 * @see #getPivotAttributeAssignment()
	 * @generated
	 */
	EReference getPivotAttributeAssignment_NameAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getValueAttribute <em>Value Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getValueAttribute()
	 * @see #getPivotAttributeAssignment()
	 * @generated
	 */
	EReference getPivotAttributeAssignment_ValueAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment <em>Reverse Pivot Attribute Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reverse Pivot Attribute Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment
	 * @generated
	 */
	EClass getReversePivotAttributeAssignment();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getAttributeSelectionFunction <em>Attribute Selection Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Selection Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getAttributeSelectionFunction()
	 * @see #getReversePivotAttributeAssignment()
	 * @generated
	 */
	EAttribute getReversePivotAttributeAssignment_AttributeSelectionFunction();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getValueSelectionFunction <em>Value Selection Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Selection Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getValueSelectionFunction()
	 * @see #getReversePivotAttributeAssignment()
	 * @generated
	 */
	EAttribute getReversePivotAttributeAssignment_ValueSelectionFunction();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getSelectorAttribute <em>Selector Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Selector Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getSelectorAttribute()
	 * @see #getReversePivotAttributeAssignment()
	 * @generated
	 */
	EReference getReversePivotAttributeAssignment_SelectorAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment <em>Set Attribute Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Attribute Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment
	 * @generated
	 */
	EClass getSetAttributeAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Set Id Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getSetIdAttribute()
	 * @see #getSetAttributeAssignment()
	 * @generated
	 */
	EReference getSetAttributeAssignment_SetIdAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getMemberAttribute <em>Member Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Member Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getMemberAttribute()
	 * @see #getSetAttributeAssignment()
	 * @generated
	 */
	EReference getSetAttributeAssignment_MemberAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category
	 * @generated
	 */
	EClass getCategory();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getValue()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_Value();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#isIsNullCategory <em>Is Null Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Null Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#isIsNullCategory()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_IsNullCategory();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getDisplayName <em>Display Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Name</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getDisplayName()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_DisplayName();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getProperty()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_Property();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getPrior <em>Prior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prior</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getPrior()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_Prior();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties <em>Categorical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Categorical Properties</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_CategoricalProperties();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap <em>Category Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Map</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap
	 * @generated
	 */
	EClass getCategoryMap();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsMultiLevel <em>Is Multi Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Multi Level</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsMultiLevel()
	 * @see #getCategoryMap()
	 * @generated
	 */
	EAttribute getCategoryMap_IsMultiLevel();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsItemMap <em>Is Item Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Item Map</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsItemMap()
	 * @see #getCategoryMap()
	 * @generated
	 */
	EAttribute getCategoryMap_IsItemMap();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy <em>Taxonomy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Taxonomy</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy()
	 * @see #getCategoryMap()
	 * @generated
	 */
	EReference getCategoryMap_Taxonomy();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject <em>Category Map Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Map Object</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject
	 * @generated
	 */
	EClass getCategoryMapObject();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entry</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject#getEntry()
	 * @see #getCategoryMapObject()
	 * @generated
	 */
	EReference getCategoryMapObject_Entry();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry <em>Category Map Object Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Map Object Entry</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry
	 * @generated
	 */
	EClass getCategoryMapObjectEntry();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getGraphId <em>Graph Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Graph Id</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getGraphId()
	 * @see #getCategoryMapObjectEntry()
	 * @generated
	 */
	EAttribute getCategoryMapObjectEntry_GraphId();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject <em>Map Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Map Object</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject()
	 * @see #getCategoryMapObjectEntry()
	 * @generated
	 */
	EReference getCategoryMapObjectEntry_MapObject();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Child</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getChild()
	 * @see #getCategoryMapObjectEntry()
	 * @generated
	 */
	EReference getCategoryMapObjectEntry_Child();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Parent</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getParent()
	 * @see #getCategoryMapObjectEntry()
	 * @generated
	 */
	EReference getCategoryMapObjectEntry_Parent();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable <em>Category Map Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Map Table</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable
	 * @generated
	 */
	EClass getCategoryMapTable();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getChildAttribute <em>Child Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Child Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getChildAttribute()
	 * @see #getCategoryMapTable()
	 * @generated
	 */
	EReference getCategoryMapTable_ChildAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Table</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getTable()
	 * @see #getCategoryMapTable()
	 * @generated
	 */
	EReference getCategoryMapTable_Table();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getGraphIdAttribute <em>Graph Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph Id Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getGraphIdAttribute()
	 * @see #getCategoryMapTable()
	 * @generated
	 */
	EReference getCategoryMapTable_GraphIdAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getParentAttribute <em>Parent Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable#getParentAttribute()
	 * @see #getCategoryMapTable()
	 * @generated
	 */
	EReference getCategoryMapTable_ParentAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix <em>Category Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Matrix</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix
	 * @generated
	 */
	EClass getCategoryMatrix();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getDiagonalDefault <em>Diagonal Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diagonal Default</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getDiagonalDefault()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EAttribute getCategoryMatrix_DiagonalDefault();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getOffDiagonalDefault <em>Off Diagonal Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Off Diagonal Default</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getOffDiagonalDefault()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EAttribute getCategoryMatrix_OffDiagonalDefault();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getKind()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EAttribute getCategoryMatrix_Kind();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getCategory()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EReference getCategoryMatrix_Category();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Schema</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EReference getCategoryMatrix_Schema();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Test Result</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult()
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	EReference getCategoryMatrix_TestResult();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry <em>Category Matrix Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Matrix Entry</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry
	 * @generated
	 */
	EClass getCategoryMatrixEntry();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getValue()
	 * @see #getCategoryMatrixEntry()
	 * @generated
	 */
	EAttribute getCategoryMatrixEntry_Value();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getColumnIndex <em>Column Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column Index</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getColumnIndex()
	 * @see #getCategoryMatrixEntry()
	 * @generated
	 */
	EReference getCategoryMatrixEntry_ColumnIndex();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix <em>Category Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Category Matrix</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix()
	 * @see #getCategoryMatrixEntry()
	 * @generated
	 */
	EReference getCategoryMatrixEntry_CategoryMatrix();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getRowIndex <em>Row Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Row Index</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getRowIndex()
	 * @see #getCategoryMatrixEntry()
	 * @generated
	 */
	EReference getCategoryMatrixEntry_RowIndex();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject <em>Category Matrix Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Matrix Object</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject
	 * @generated
	 */
	EClass getCategoryMatrixObject();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entry</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject#getEntry()
	 * @see #getCategoryMatrixObject()
	 * @generated
	 */
	EReference getCategoryMatrixObject_Entry();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable <em>Category Matrix Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Matrix Table</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable
	 * @generated
	 */
	EClass getCategoryMatrixTable();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getSource()
	 * @see #getCategoryMatrixTable()
	 * @generated
	 */
	EReference getCategoryMatrixTable_Source();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getColumnAttribute <em>Column Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getColumnAttribute()
	 * @see #getCategoryMatrixTable()
	 * @generated
	 */
	EReference getCategoryMatrixTable_ColumnAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getRowAttribute <em>Row Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Row Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getRowAttribute()
	 * @see #getCategoryMatrixTable()
	 * @generated
	 */
	EReference getCategoryMatrixTable_RowAttribute();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getValueAttribute <em>Value Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Attribute</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getValueAttribute()
	 * @see #getCategoryMatrixTable()
	 * @generated
	 */
	EReference getCategoryMatrixTable_ValueAttribute();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy <em>Category Taxonomy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Taxonomy</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy
	 * @generated
	 */
	EClass getCategoryTaxonomy();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getCategoryMap <em>Category Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Category Map</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getCategoryMap()
	 * @see #getCategoryTaxonomy()
	 * @generated
	 */
	EReference getCategoryTaxonomy_CategoryMap();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getRootCategory <em>Root Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Root Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getRootCategory()
	 * @see #getCategoryTaxonomy()
	 * @generated
	 */
	EReference getCategoryTaxonomy_RootCategory();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Schema</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema()
	 * @see #getCategoryTaxonomy()
	 * @generated
	 */
	EReference getCategoryTaxonomy_Schema();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction <em>Attribute Selection Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attribute Selection Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction
	 * @generated
	 */
	EEnum getAttributeSelectionFunction();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attribute Type</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType
	 * @generated
	 */
	EEnum getAttributeType();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType <em>Order Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Order Type</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType
	 * @generated
	 */
	EEnum getOrderType();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption <em>Usage Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Usage Option</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @generated
	 */
	EEnum getUsageOption();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction <em>Value Selection Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Value Selection Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction
	 * @generated
	 */
	EEnum getValueSelectionFunction();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty <em>Category Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Category Property</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty
	 * @generated
	 */
	EEnum getCategoryProperty();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty <em>Matrix Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Matrix Property</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty
	 * @generated
	 */
	EEnum getMatrixProperty();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Double</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getDouble();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MiningdataFactory getMiningdataFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl <em>Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeAssignment()
		 * @generated
		 */
		EClass ATTRIBUTE_ASSIGNMENT = eINSTANCE.getAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>Logical Attribute</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE = eINSTANCE.getAttributeAssignment_LogicalAttribute();

		/**
		 * The meta object literal for the '<em><b>Order Id Attribute</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE = eINSTANCE.getAttributeAssignment_OrderIdAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentSetImpl <em>Attribute Assignment Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentSetImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeAssignmentSet()
		 * @generated
		 */
		EClass ATTRIBUTE_ASSIGNMENT_SET = eINSTANCE.getAttributeAssignmentSet();

		/**
		 * The meta object literal for the '<em><b>Auxiliary Object</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT = eINSTANCE.getAttributeAssignmentSet_AuxiliaryObject();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl <em>Attribute Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeUsage()
		 * @generated
		 */
		EClass ATTRIBUTE_USAGE = eINSTANCE.getAttributeUsage();

		/**
		 * The meta object literal for the '<em><b>Usage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_USAGE__USAGE = eINSTANCE.getAttributeUsage_Usage();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_USAGE__WEIGHT = eINSTANCE.getAttributeUsage_Weight();

		/**
		 * The meta object literal for the '<em><b>Suppress Discretization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION = eINSTANCE.getAttributeUsage_SuppressDiscretization();

		/**
		 * The meta object literal for the '<em><b>Suppress Normalization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION = eINSTANCE.getAttributeUsage_SuppressNormalization();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_USAGE__ATTRIBUTE = eINSTANCE.getAttributeUsage_Attribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageSetImpl <em>Attribute Usage Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageSetImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeUsageSet()
		 * @generated
		 */
		EClass ATTRIBUTE_USAGE_SET = eINSTANCE.getAttributeUsageSet();

		/**
		 * The meta object literal for the '<em><b>Settings</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_USAGE_SET__SETTINGS = eINSTANCE.getAttributeUsageSet_Settings();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl <em>Categorical Attribute Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoricalAttributeProperties()
		 * @generated
		 */
		EClass CATEGORICAL_ATTRIBUTE_PROPERTIES = eINSTANCE.getCategoricalAttributeProperties();

		/**
		 * The meta object literal for the '<em><b>Logical Attribute</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE = eINSTANCE.getCategoricalAttributeProperties_LogicalAttribute();

		/**
		 * The meta object literal for the '<em><b>Taxonomy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY = eINSTANCE.getCategoricalAttributeProperties_Taxonomy();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY = eINSTANCE.getCategoricalAttributeProperties_Category();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.DirectAttributeAssignmentImpl <em>Direct Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.DirectAttributeAssignmentImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getDirectAttributeAssignment()
		 * @generated
		 */
		EClass DIRECT_ATTRIBUTE_ASSIGNMENT = eINSTANCE.getDirectAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE = eINSTANCE.getDirectAttributeAssignment_Attribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalAttributeImpl <em>Logical Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalAttributeImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getLogicalAttribute()
		 * @generated
		 */
		EClass LOGICAL_ATTRIBUTE = eINSTANCE.getLogicalAttribute();

		/**
		 * The meta object literal for the '<em><b>Is Set Valued</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_ATTRIBUTE__IS_SET_VALUED = eINSTANCE.getLogicalAttribute_IsSetValued();

		/**
		 * The meta object literal for the '<em><b>Categorical Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_ATTRIBUTE__CATEGORICAL_PROPERTIES = eINSTANCE.getLogicalAttribute_CategoricalProperties();

		/**
		 * The meta object literal for the '<em><b>Numerical Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_ATTRIBUTE__NUMERICAL_PROPERTIES = eINSTANCE.getLogicalAttribute_NumericalProperties();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalDataImpl <em>Logical Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.LogicalDataImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getLogicalData()
		 * @generated
		 */
		EClass LOGICAL_DATA = eINSTANCE.getLogicalData();

		/**
		 * The meta object literal for the '<em><b>Schema</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_DATA__SCHEMA = eINSTANCE.getLogicalData_Schema();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl <em>Mining Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getMiningAttribute()
		 * @generated
		 */
		EClass MINING_ATTRIBUTE = eINSTANCE.getMiningAttribute();

		/**
		 * The meta object literal for the '<em><b>Display Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MINING_ATTRIBUTE__DISPLAY_NAME = eINSTANCE.getMiningAttribute_DisplayName();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MINING_ATTRIBUTE__ATTRIBUTE_TYPE = eINSTANCE.getMiningAttribute_AttributeType();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl <em>Numerical Attribute Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getNumericalAttributeProperties()
		 * @generated
		 */
		EClass NUMERICAL_ATTRIBUTE_PROPERTIES = eINSTANCE.getNumericalAttributeProperties();

		/**
		 * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND = eINSTANCE.getNumericalAttributeProperties_LowerBound();

		/**
		 * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND = eINSTANCE.getNumericalAttributeProperties_UpperBound();

		/**
		 * The meta object literal for the '<em><b>Is Discrete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE = eINSTANCE.getNumericalAttributeProperties_IsDiscrete();

		/**
		 * The meta object literal for the '<em><b>Is Cyclic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC = eINSTANCE.getNumericalAttributeProperties_IsCyclic();

		/**
		 * The meta object literal for the '<em><b>Anchor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR = eINSTANCE.getNumericalAttributeProperties_Anchor();

		/**
		 * The meta object literal for the '<em><b>Cycle Begin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN = eINSTANCE.getNumericalAttributeProperties_CycleBegin();

		/**
		 * The meta object literal for the '<em><b>Cycle End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END = eINSTANCE.getNumericalAttributeProperties_CycleEnd();

		/**
		 * The meta object literal for the '<em><b>Discrete Step Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE = eINSTANCE.getNumericalAttributeProperties_DiscreteStepSize();

		/**
		 * The meta object literal for the '<em><b>Logical Attribute</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE = eINSTANCE.getNumericalAttributeProperties_LogicalAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl <em>Ordinal Attribute Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getOrdinalAttributeProperties()
		 * @generated
		 */
		EClass ORDINAL_ATTRIBUTE_PROPERTIES = eINSTANCE.getOrdinalAttributeProperties();

		/**
		 * The meta object literal for the '<em><b>Order Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE = eINSTANCE.getOrdinalAttributeProperties_OrderType();

		/**
		 * The meta object literal for the '<em><b>Is Cyclic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC = eINSTANCE.getOrdinalAttributeProperties_IsCyclic();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PhysicalDataImpl <em>Physical Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PhysicalDataImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getPhysicalData()
		 * @generated
		 */
		EClass PHYSICAL_DATA = eINSTANCE.getPhysicalData();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_DATA__SOURCE = eINSTANCE.getPhysicalData_Source();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PivotAttributeAssignmentImpl <em>Pivot Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.PivotAttributeAssignmentImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getPivotAttributeAssignment()
		 * @generated
		 */
		EClass PIVOT_ATTRIBUTE_ASSIGNMENT = eINSTANCE.getPivotAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>Set Id Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIVOT_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE = eINSTANCE.getPivotAttributeAssignment_SetIdAttribute();

		/**
		 * The meta object literal for the '<em><b>Name Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIVOT_ATTRIBUTE_ASSIGNMENT__NAME_ATTRIBUTE = eINSTANCE.getPivotAttributeAssignment_NameAttribute();

		/**
		 * The meta object literal for the '<em><b>Value Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_ATTRIBUTE = eINSTANCE.getPivotAttributeAssignment_ValueAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl <em>Reverse Pivot Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getReversePivotAttributeAssignment()
		 * @generated
		 */
		EClass REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT = eINSTANCE.getReversePivotAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>Attribute Selection Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION = eINSTANCE.getReversePivotAttributeAssignment_AttributeSelectionFunction();

		/**
		 * The meta object literal for the '<em><b>Value Selection Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION = eINSTANCE.getReversePivotAttributeAssignment_ValueSelectionFunction();

		/**
		 * The meta object literal for the '<em><b>Selector Attribute</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE = eINSTANCE.getReversePivotAttributeAssignment_SelectorAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl <em>Set Attribute Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getSetAttributeAssignment()
		 * @generated
		 */
		EClass SET_ATTRIBUTE_ASSIGNMENT = eINSTANCE.getSetAttributeAssignment();

		/**
		 * The meta object literal for the '<em><b>Set Id Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE = eINSTANCE.getSetAttributeAssignment_SetIdAttribute();

		/**
		 * The meta object literal for the '<em><b>Member Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE = eINSTANCE.getSetAttributeAssignment_MemberAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl <em>Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategory()
		 * @generated
		 */
		EClass CATEGORY = eINSTANCE.getCategory();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__VALUE = eINSTANCE.getCategory_Value();

		/**
		 * The meta object literal for the '<em><b>Is Null Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__IS_NULL_CATEGORY = eINSTANCE.getCategory_IsNullCategory();

		/**
		 * The meta object literal for the '<em><b>Display Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__DISPLAY_NAME = eINSTANCE.getCategory_DisplayName();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__PROPERTY = eINSTANCE.getCategory_Property();

		/**
		 * The meta object literal for the '<em><b>Prior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__PRIOR = eINSTANCE.getCategory_Prior();

		/**
		 * The meta object literal for the '<em><b>Categorical Properties</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__CATEGORICAL_PROPERTIES = eINSTANCE.getCategory_CategoricalProperties();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl <em>Category Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMap()
		 * @generated
		 */
		EClass CATEGORY_MAP = eINSTANCE.getCategoryMap();

		/**
		 * The meta object literal for the '<em><b>Is Multi Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MAP__IS_MULTI_LEVEL = eINSTANCE.getCategoryMap_IsMultiLevel();

		/**
		 * The meta object literal for the '<em><b>Is Item Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MAP__IS_ITEM_MAP = eINSTANCE.getCategoryMap_IsItemMap();

		/**
		 * The meta object literal for the '<em><b>Taxonomy</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP__TAXONOMY = eINSTANCE.getCategoryMap_Taxonomy();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectImpl <em>Category Map Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapObject()
		 * @generated
		 */
		EClass CATEGORY_MAP_OBJECT = eINSTANCE.getCategoryMapObject();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_OBJECT__ENTRY = eINSTANCE.getCategoryMapObject_Entry();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl <em>Category Map Object Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapObjectEntry()
		 * @generated
		 */
		EClass CATEGORY_MAP_OBJECT_ENTRY = eINSTANCE.getCategoryMapObjectEntry();

		/**
		 * The meta object literal for the '<em><b>Graph Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID = eINSTANCE.getCategoryMapObjectEntry_GraphId();

		/**
		 * The meta object literal for the '<em><b>Map Object</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT = eINSTANCE.getCategoryMapObjectEntry_MapObject();

		/**
		 * The meta object literal for the '<em><b>Child</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_OBJECT_ENTRY__CHILD = eINSTANCE.getCategoryMapObjectEntry_Child();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_OBJECT_ENTRY__PARENT = eINSTANCE.getCategoryMapObjectEntry_Parent();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl <em>Category Map Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMapTable()
		 * @generated
		 */
		EClass CATEGORY_MAP_TABLE = eINSTANCE.getCategoryMapTable();

		/**
		 * The meta object literal for the '<em><b>Child Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE = eINSTANCE.getCategoryMapTable_ChildAttribute();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_TABLE__TABLE = eINSTANCE.getCategoryMapTable_Table();

		/**
		 * The meta object literal for the '<em><b>Graph Id Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE = eINSTANCE.getCategoryMapTable_GraphIdAttribute();

		/**
		 * The meta object literal for the '<em><b>Parent Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE = eINSTANCE.getCategoryMapTable_ParentAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl <em>Category Matrix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrix()
		 * @generated
		 */
		EClass CATEGORY_MATRIX = eINSTANCE.getCategoryMatrix();

		/**
		 * The meta object literal for the '<em><b>Diagonal Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MATRIX__DIAGONAL_DEFAULT = eINSTANCE.getCategoryMatrix_DiagonalDefault();

		/**
		 * The meta object literal for the '<em><b>Off Diagonal Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT = eINSTANCE.getCategoryMatrix_OffDiagonalDefault();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MATRIX__KIND = eINSTANCE.getCategoryMatrix_Kind();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX__CATEGORY = eINSTANCE.getCategoryMatrix_Category();

		/**
		 * The meta object literal for the '<em><b>Schema</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX__SCHEMA = eINSTANCE.getCategoryMatrix_Schema();

		/**
		 * The meta object literal for the '<em><b>Test Result</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX__TEST_RESULT = eINSTANCE.getCategoryMatrix_TestResult();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl <em>Category Matrix Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixEntry()
		 * @generated
		 */
		EClass CATEGORY_MATRIX_ENTRY = eINSTANCE.getCategoryMatrixEntry();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_MATRIX_ENTRY__VALUE = eINSTANCE.getCategoryMatrixEntry_Value();

		/**
		 * The meta object literal for the '<em><b>Column Index</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_ENTRY__COLUMN_INDEX = eINSTANCE.getCategoryMatrixEntry_ColumnIndex();

		/**
		 * The meta object literal for the '<em><b>Category Matrix</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX = eINSTANCE.getCategoryMatrixEntry_CategoryMatrix();

		/**
		 * The meta object literal for the '<em><b>Row Index</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_ENTRY__ROW_INDEX = eINSTANCE.getCategoryMatrixEntry_RowIndex();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixObjectImpl <em>Category Matrix Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixObjectImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixObject()
		 * @generated
		 */
		EClass CATEGORY_MATRIX_OBJECT = eINSTANCE.getCategoryMatrixObject();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_OBJECT__ENTRY = eINSTANCE.getCategoryMatrixObject_Entry();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl <em>Category Matrix Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryMatrixTable()
		 * @generated
		 */
		EClass CATEGORY_MATRIX_TABLE = eINSTANCE.getCategoryMatrixTable();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_TABLE__SOURCE = eINSTANCE.getCategoryMatrixTable_Source();

		/**
		 * The meta object literal for the '<em><b>Column Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE = eINSTANCE.getCategoryMatrixTable_ColumnAttribute();

		/**
		 * The meta object literal for the '<em><b>Row Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE = eINSTANCE.getCategoryMatrixTable_RowAttribute();

		/**
		 * The meta object literal for the '<em><b>Value Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE = eINSTANCE.getCategoryMatrixTable_ValueAttribute();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl <em>Category Taxonomy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryTaxonomy()
		 * @generated
		 */
		EClass CATEGORY_TAXONOMY = eINSTANCE.getCategoryTaxonomy();

		/**
		 * The meta object literal for the '<em><b>Category Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_TAXONOMY__CATEGORY_MAP = eINSTANCE.getCategoryTaxonomy_CategoryMap();

		/**
		 * The meta object literal for the '<em><b>Root Category</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_TAXONOMY__ROOT_CATEGORY = eINSTANCE.getCategoryTaxonomy_RootCategory();

		/**
		 * The meta object literal for the '<em><b>Schema</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_TAXONOMY__SCHEMA = eINSTANCE.getCategoryTaxonomy_Schema();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction <em>Attribute Selection Function</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeSelectionFunction()
		 * @generated
		 */
		EEnum ATTRIBUTE_SELECTION_FUNCTION = eINSTANCE.getAttributeSelectionFunction();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType <em>Attribute Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getAttributeType()
		 * @generated
		 */
		EEnum ATTRIBUTE_TYPE = eINSTANCE.getAttributeType();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType <em>Order Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getOrderType()
		 * @generated
		 */
		EEnum ORDER_TYPE = eINSTANCE.getOrderType();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption <em>Usage Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getUsageOption()
		 * @generated
		 */
		EEnum USAGE_OPTION = eINSTANCE.getUsageOption();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction <em>Value Selection Function</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getValueSelectionFunction()
		 * @generated
		 */
		EEnum VALUE_SELECTION_FUNCTION = eINSTANCE.getValueSelectionFunction();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty <em>Category Property</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getCategoryProperty()
		 * @generated
		 */
		EEnum CATEGORY_PROPERTY = eINSTANCE.getCategoryProperty();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty <em>Matrix Property</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getMatrixProperty()
		 * @generated
		 */
		EEnum MATRIX_PROPERTY = eINSTANCE.getMatrixProperty();

		/**
		 * The meta object literal for the '<em>Double</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl#getDouble()
		 * @generated
		 */
		EDataType DOUBLE = eINSTANCE.getDouble();

	}

} //MiningdataPackage
