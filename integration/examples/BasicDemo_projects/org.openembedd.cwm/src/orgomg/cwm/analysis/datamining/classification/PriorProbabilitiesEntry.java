/**
 * INRIA/IRISA
 *
 * $Id: PriorProbabilitiesEntry.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prior Probabilities Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the probability of a category in the original data, i.e. before performing biased sampling to enrich individual values.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriorProbability <em>Prior Probability</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors <em>Priors</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getTargetValue <em>Target Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilitiesEntry()
 * @model
 * @generated
 */
public interface PriorProbabilitiesEntry extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Prior Probability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This represents the probability of the targetValue in the original data.
	 * The value must be in [0,1]
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Prior Probability</em>' attribute.
	 * @see #setPriorProbability(String)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilitiesEntry_PriorProbability()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getPriorProbability();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriorProbability <em>Prior Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prior Probability</em>' attribute.
	 * @see #getPriorProbability()
	 * @generated
	 */
	void setPriorProbability(String value);

	/**
	 * Returns the value of the '<em><b>Priors</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getPrior <em>Prior</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priors</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priors</em>' container reference.
	 * @see #setPriors(PriorProbabilities)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilitiesEntry_Priors()
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getPrior
	 * @model opposite="prior" required="true"
	 * @generated
	 */
	PriorProbabilities getPriors();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors <em>Priors</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priors</em>' container reference.
	 * @see #getPriors()
	 * @generated
	 */
	void setPriors(PriorProbabilities value);

	/**
	 * Returns the value of the '<em><b>Target Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Value</em>' reference.
	 * @see #setTargetValue(Category)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilitiesEntry_TargetValue()
	 * @model required="true"
	 * @generated
	 */
	Category getTargetValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getTargetValue <em>Target Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Value</em>' reference.
	 * @see #getTargetValue()
	 * @generated
	 */
	void setTargetValue(Category value);

} // PriorProbabilitiesEntry
