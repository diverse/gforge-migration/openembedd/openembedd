/**
 * <copyright>
 * </copyright>
 *
 * $Id: DataObjectSetValidator.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.transformation.Transformation;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.transformation.DataObjectSet}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface DataObjectSetValidator {
	boolean validate();

	boolean validateSourceTransformation(EList<Transformation> value);
	boolean validateTargetTransformation(EList<Transformation> value);
	boolean validateElement(EList<ModelElement> value);
}
