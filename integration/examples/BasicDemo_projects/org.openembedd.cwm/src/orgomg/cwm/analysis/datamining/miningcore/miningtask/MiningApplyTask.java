/**
 * INRIA/IRISA
 *
 * $Id: MiningApplyTask.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Apply Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This describes a task that computes the result of an application of a data mining model to (new) data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputOption <em>Output Option</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputAssignment <em>Output Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getApplyOutput <em>Apply Output</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyTask()
 * @model
 * @generated
 */
public interface MiningApplyTask extends MiningTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Output Option</b></em>' attribute.
	 * The default value is <code>"createNew"</code>.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies how the apply output is created. The default is "createNew", which means the output is stored in a new file/table.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Output Option</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption
	 * @see #setOutputOption(ApplyOutputOption)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyTask_OutputOption()
	 * @model default="createNew"
	 * @generated
	 */
	ApplyOutputOption getOutputOption();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputOption <em>Output Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Option</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption
	 * @see #getOutputOption()
	 * @generated
	 */
	void setOutputOption(ApplyOutputOption value);

	/**
	 * Returns the value of the '<em><b>Output Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Assignment</em>' reference.
	 * @see #setOutputAssignment(AttributeAssignmentSet)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyTask_OutputAssignment()
	 * @model required="true"
	 * @generated
	 */
	AttributeAssignmentSet getOutputAssignment();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputAssignment <em>Output Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Assignment</em>' reference.
	 * @see #getOutputAssignment()
	 * @generated
	 */
	void setOutputAssignment(AttributeAssignmentSet value);

	/**
	 * Returns the value of the '<em><b>Apply Output</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apply Output</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Apply Output</em>' reference.
	 * @see #setApplyOutput(MiningApplyOutput)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyTask_ApplyOutput()
	 * @model required="true"
	 * @generated
	 */
	MiningApplyOutput getApplyOutput();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getApplyOutput <em>Apply Output</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Apply Output</em>' reference.
	 * @see #getApplyOutput()
	 * @generated
	 */
	void setApplyOutput(MiningApplyOutput value);

} // MiningApplyTask
