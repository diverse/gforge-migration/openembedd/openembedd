/**
 * INRIA/IRISA
 *
 * $Id: ApplyContentItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Content Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is an abstract class that describes an item to appear in the output based on the rank of the prediction.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem#getTopNthIndex <em>Top Nth Index</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyContentItem()
 * @model abstract="true"
 * @generated
 */
public interface ApplyContentItem extends ApplyOutputItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Top Nth Index</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates the rank of the prediction whose associated values (score, probability, and rule id) to appear in the output are specified by the subclasses.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Top Nth Index</em>' attribute.
	 * @see #setTopNthIndex(long)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyContentItem_TopNthIndex()
	 * @model default="1" dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getTopNthIndex();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem#getTopNthIndex <em>Top Nth Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Top Nth Index</em>' attribute.
	 * @see #getTopNthIndex()
	 * @generated
	 */
	void setTopNthIndex(long value);

} // ApplyContentItem
