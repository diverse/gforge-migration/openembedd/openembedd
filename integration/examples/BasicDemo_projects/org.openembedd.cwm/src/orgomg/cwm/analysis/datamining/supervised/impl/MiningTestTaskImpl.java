/**
 * INRIA/IRISA
 *
 * $Id: MiningTestTaskImpl.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl;

import orgomg.cwm.analysis.datamining.supervised.MiningTestTask;
import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Test Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.MiningTestTaskImpl#isComputeLift <em>Compute Lift</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.MiningTestTaskImpl#getPositiveTargetCategory <em>Positive Target Category</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MiningTestTaskImpl extends MiningTaskImpl implements MiningTestTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #isComputeLift() <em>Compute Lift</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isComputeLift()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMPUTE_LIFT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isComputeLift() <em>Compute Lift</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isComputeLift()
	 * @generated
	 * @ordered
	 */
	protected boolean computeLift = COMPUTE_LIFT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPositiveTargetCategory() <em>Positive Target Category</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositiveTargetCategory()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> positiveTargetCategory;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningTestTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SupervisedPackage.Literals.MINING_TEST_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isComputeLift() {
		return computeLift;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComputeLift(boolean newComputeLift) {
		boolean oldComputeLift = computeLift;
		computeLift = newComputeLift;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT, oldComputeLift, computeLift));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getPositiveTargetCategory() {
		if (positiveTargetCategory == null) {
			positiveTargetCategory = new EObjectResolvingEList<Category>(Category.class, this, SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY);
		}
		return positiveTargetCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT:
				return isComputeLift() ? Boolean.TRUE : Boolean.FALSE;
			case SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY:
				return getPositiveTargetCategory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT:
				setComputeLift(((Boolean)newValue).booleanValue());
				return;
			case SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY:
				getPositiveTargetCategory().clear();
				getPositiveTargetCategory().addAll((Collection<? extends Category>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT:
				setComputeLift(COMPUTE_LIFT_EDEFAULT);
				return;
			case SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY:
				getPositiveTargetCategory().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT:
				return computeLift != COMPUTE_LIFT_EDEFAULT;
			case SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY:
				return positiveTargetCategory != null && !positiveTargetCategory.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (computeLift: ");
		result.append(computeLift);
		result.append(')');
		return result.toString();
	}

} //MiningTestTaskImpl
