/**
 * INRIA/IRISA
 *
 * $Id: MiningTestResultImpl.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.impl.MiningResultImpl;

import orgomg.cwm.analysis.datamining.supervised.LiftAnalysis;
import orgomg.cwm.analysis.datamining.supervised.MiningTestResult;
import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Test Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.MiningTestResultImpl#getNumberOfTestRecords <em>Number Of Test Records</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.MiningTestResultImpl#getLiftAnalysis <em>Lift Analysis</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MiningTestResultImpl extends MiningResultImpl implements MiningTestResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getNumberOfTestRecords() <em>Number Of Test Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfTestRecords()
	 * @generated
	 * @ordered
	 */
	protected static final long NUMBER_OF_TEST_RECORDS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getNumberOfTestRecords() <em>Number Of Test Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfTestRecords()
	 * @generated
	 * @ordered
	 */
	protected long numberOfTestRecords = NUMBER_OF_TEST_RECORDS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLiftAnalysis() <em>Lift Analysis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiftAnalysis()
	 * @generated
	 * @ordered
	 */
	protected LiftAnalysis liftAnalysis;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningTestResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SupervisedPackage.Literals.MINING_TEST_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getNumberOfTestRecords() {
		return numberOfTestRecords;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberOfTestRecords(long newNumberOfTestRecords) {
		long oldNumberOfTestRecords = numberOfTestRecords;
		numberOfTestRecords = newNumberOfTestRecords;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS, oldNumberOfTestRecords, numberOfTestRecords));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiftAnalysis getLiftAnalysis() {
		return liftAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLiftAnalysis(LiftAnalysis newLiftAnalysis, NotificationChain msgs) {
		LiftAnalysis oldLiftAnalysis = liftAnalysis;
		liftAnalysis = newLiftAnalysis;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS, oldLiftAnalysis, newLiftAnalysis);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiftAnalysis(LiftAnalysis newLiftAnalysis) {
		if (newLiftAnalysis != liftAnalysis) {
			NotificationChain msgs = null;
			if (liftAnalysis != null)
				msgs = ((InternalEObject)liftAnalysis).eInverseRemove(this, SupervisedPackage.LIFT_ANALYSIS__TEST_RESULT, LiftAnalysis.class, msgs);
			if (newLiftAnalysis != null)
				msgs = ((InternalEObject)newLiftAnalysis).eInverseAdd(this, SupervisedPackage.LIFT_ANALYSIS__TEST_RESULT, LiftAnalysis.class, msgs);
			msgs = basicSetLiftAnalysis(newLiftAnalysis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS, newLiftAnalysis, newLiftAnalysis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				if (liftAnalysis != null)
					msgs = ((InternalEObject)liftAnalysis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS, null, msgs);
				return basicSetLiftAnalysis((LiftAnalysis)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				return basicSetLiftAnalysis(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS:
				return new Long(getNumberOfTestRecords());
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				return getLiftAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS:
				setNumberOfTestRecords(((Long)newValue).longValue());
				return;
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				setLiftAnalysis((LiftAnalysis)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS:
				setNumberOfTestRecords(NUMBER_OF_TEST_RECORDS_EDEFAULT);
				return;
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				setLiftAnalysis((LiftAnalysis)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS:
				return numberOfTestRecords != NUMBER_OF_TEST_RECORDS_EDEFAULT;
			case SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS:
				return liftAnalysis != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numberOfTestRecords: ");
		result.append(numberOfTestRecords);
		result.append(')');
		return result.toString();
	}

} //MiningTestResultImpl
