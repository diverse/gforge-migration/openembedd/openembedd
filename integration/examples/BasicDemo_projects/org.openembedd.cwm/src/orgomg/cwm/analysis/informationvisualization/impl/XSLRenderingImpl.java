/**
 * INRIA/IRISA
 *
 * $Id: XSLRenderingImpl.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.analysis.informationvisualization.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage;
import orgomg.cwm.analysis.informationvisualization.XSLRendering;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XSL Rendering</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XSLRenderingImpl extends RenderingImpl implements XSLRendering {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XSLRenderingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InformationvisualizationPackage.Literals.XSL_RENDERING;
	}

} //XSLRenderingImpl
