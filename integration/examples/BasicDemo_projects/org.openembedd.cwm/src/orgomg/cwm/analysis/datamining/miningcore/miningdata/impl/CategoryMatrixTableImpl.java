/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixTableImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Matrix Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl#getSource <em>Source</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl#getColumnAttribute <em>Column Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl#getRowAttribute <em>Row Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixTableImpl#getValueAttribute <em>Value Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMatrixTableImpl extends CategoryMatrixImpl implements CategoryMatrixTable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected EList<orgomg.cwm.objectmodel.core.Class> source;

	/**
	 * The cached value of the '{@link #getColumnAttribute() <em>Column Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumnAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute columnAttribute;

	/**
	 * The cached value of the '{@link #getRowAttribute() <em>Row Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute rowAttribute;

	/**
	 * The cached value of the '{@link #getValueAttribute() <em>Value Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute valueAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMatrixTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MATRIX_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<orgomg.cwm.objectmodel.core.Class> getSource() {
		if (source == null) {
			source = new EObjectResolvingEList<orgomg.cwm.objectmodel.core.Class>(orgomg.cwm.objectmodel.core.Class.class, this, MiningdataPackage.CATEGORY_MATRIX_TABLE__SOURCE);
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getColumnAttribute() {
		if (columnAttribute != null && columnAttribute.eIsProxy()) {
			InternalEObject oldColumnAttribute = (InternalEObject)columnAttribute;
			columnAttribute = (Attribute)eResolveProxy(oldColumnAttribute);
			if (columnAttribute != oldColumnAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE, oldColumnAttribute, columnAttribute));
			}
		}
		return columnAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetColumnAttribute() {
		return columnAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColumnAttribute(Attribute newColumnAttribute) {
		Attribute oldColumnAttribute = columnAttribute;
		columnAttribute = newColumnAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE, oldColumnAttribute, columnAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getRowAttribute() {
		if (rowAttribute != null && rowAttribute.eIsProxy()) {
			InternalEObject oldRowAttribute = (InternalEObject)rowAttribute;
			rowAttribute = (Attribute)eResolveProxy(oldRowAttribute);
			if (rowAttribute != oldRowAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE, oldRowAttribute, rowAttribute));
			}
		}
		return rowAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetRowAttribute() {
		return rowAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRowAttribute(Attribute newRowAttribute) {
		Attribute oldRowAttribute = rowAttribute;
		rowAttribute = newRowAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE, oldRowAttribute, rowAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getValueAttribute() {
		if (valueAttribute != null && valueAttribute.eIsProxy()) {
			InternalEObject oldValueAttribute = (InternalEObject)valueAttribute;
			valueAttribute = (Attribute)eResolveProxy(oldValueAttribute);
			if (valueAttribute != oldValueAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE, oldValueAttribute, valueAttribute));
			}
		}
		return valueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetValueAttribute() {
		return valueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueAttribute(Attribute newValueAttribute) {
		Attribute oldValueAttribute = valueAttribute;
		valueAttribute = newValueAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE, oldValueAttribute, valueAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__SOURCE:
				return getSource();
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE:
				if (resolve) return getColumnAttribute();
				return basicGetColumnAttribute();
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE:
				if (resolve) return getRowAttribute();
				return basicGetRowAttribute();
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE:
				if (resolve) return getValueAttribute();
				return basicGetValueAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends orgomg.cwm.objectmodel.core.Class>)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE:
				setColumnAttribute((Attribute)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE:
				setRowAttribute((Attribute)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE:
				setValueAttribute((Attribute)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__SOURCE:
				getSource().clear();
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE:
				setColumnAttribute((Attribute)null);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE:
				setRowAttribute((Attribute)null);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE:
				setValueAttribute((Attribute)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__SOURCE:
				return source != null && !source.isEmpty();
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE:
				return columnAttribute != null;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE:
				return rowAttribute != null;
			case MiningdataPackage.CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE:
				return valueAttribute != null;
		}
		return super.eIsSet(featureID);
	}

} //CategoryMatrixTableImpl
