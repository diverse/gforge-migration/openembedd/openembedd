/**
 * INRIA/IRISA
 *
 * $Id: MiningModel.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

import orgomg.cwm.objectmodel.core.Attribute;
import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A MiningModel holds the metadata of the result of a mining (training) run.
 * This information is sufficient to determine whether a model can be applied to given data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getFunction <em>Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getAlgorithmName <em>Algorithm Name</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getKeyValue <em>Key Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSettings <em>Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelSignature <em>Model Signature</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelLocation <em>Model Location</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getKeyAttribute <em>Key Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel()
 * @model
 * @generated
 */
public interface MiningModel extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Function</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningFunction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Data mining function (as opposed to algorithm), e.g. classification or clustering.
	 * The following function names are predefined:
	 * attributeImportance
	 * associationRules
	 * classification
	 * regression
	 * clustering
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningFunction
	 * @see #setFunction(MiningFunction)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_Function()
	 * @model
	 * @generated
	 */
	MiningFunction getFunction();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getFunction <em>Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningFunction
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(MiningFunction value);

	/**
	 * Returns the value of the '<em><b>Algorithm Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specific implementation of the data mining function, e.g. CART decision tree or SOM clustering.
	 * 
	 * The following algorithm names are predefined (their functions in parentheses):
	 * decisionTree (classification, regression)
	 * neuralNetwork (classification, regression)
	 * naiveBayes (classification)
	 * selfOrganizingMap (clusteirng)
	 * kMeans (clustering)
	 * competitiveLearning
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Algorithm Name</em>' attribute.
	 * @see #setAlgorithmName(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_AlgorithmName()
	 * @model dataType="orgomg.cwm.objectmodel.core.String"
	 * @generated
	 */
	String getAlgorithmName();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getAlgorithmName <em>Algorithm Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Algorithm Name</em>' attribute.
	 * @see #getAlgorithmName()
	 * @generated
	 */
	void setAlgorithmName(String value);

	/**
	 * Returns the value of the '<em><b>Key Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This optinoally represents the key value when the model is to be located.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Key Value</em>' attribute.
	 * @see #setKeyValue(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_KeyValue()
	 * @model dataType="orgomg.cwm.objectmodel.core.Any"
	 * @generated
	 */
	String getKeyValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getKeyValue <em>Key Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Value</em>' attribute.
	 * @see #getKeyValue()
	 * @generated
	 */
	void setKeyValue(String value);

	/**
	 * Returns the value of the '<em><b>Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Settings</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Settings</em>' reference.
	 * @see #setSettings(MiningFunctionSettings)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_Settings()
	 * @model
	 * @generated
	 */
	MiningFunctionSettings getSettings();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSettings <em>Settings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Settings</em>' reference.
	 * @see #getSettings()
	 * @generated
	 */
	void setSettings(MiningFunctionSettings value);

	/**
	 * Returns the value of the '<em><b>Model Signature</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.ModelSignature#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Signature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Signature</em>' containment reference.
	 * @see #setModelSignature(ModelSignature)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_ModelSignature()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.ModelSignature#getModel
	 * @model opposite="model" containment="true"
	 * @generated
	 */
	ModelSignature getModelSignature();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelSignature <em>Model Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Signature</em>' containment reference.
	 * @see #getModelSignature()
	 * @generated
	 */
	void setModelSignature(ModelSignature value);

	/**
	 * Returns the value of the '<em><b>Model Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Location</em>' reference.
	 * @see #setModelLocation(orgomg.cwm.objectmodel.core.Class)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_ModelLocation()
	 * @model required="true"
	 * @generated
	 */
	orgomg.cwm.objectmodel.core.Class getModelLocation();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelLocation <em>Model Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Location</em>' reference.
	 * @see #getModelLocation()
	 * @generated
	 */
	void setModelLocation(orgomg.cwm.objectmodel.core.Class value);

	/**
	 * Returns the value of the '<em><b>Key Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Attribute</em>' reference.
	 * @see #setKeyAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_KeyAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getKeyAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getKeyAttribute <em>Key Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Attribute</em>' reference.
	 * @see #getKeyAttribute()
	 * @generated
	 */
	void setKeyAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningModel <em>Mining Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningModel_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningModel
	 * @model opposite="miningModel" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

} // MiningModel
