/**
 * INRIA/IRISA
 *
 * $Id: FrequentItemSetFunctionSettings.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Frequent Item Set Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is a subclass of MiningFunctionSettings that specifies the parameters specific to frequent itemset algorithms.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMinimumSupport <em>Minimum Support</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMaximumSetSize <em>Maximum Set Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getExclusion <em>Exclusion</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getFrequentItemSetFunctionSettings()
 * @model
 * @generated
 */
public interface FrequentItemSetFunctionSettings extends MiningFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Minimum Support</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the minimum support of each frequent itemset to be found.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Minimum Support</em>' attribute.
	 * @see #setMinimumSupport(String)
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getFrequentItemSetFunctionSettings_MinimumSupport()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getMinimumSupport();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMinimumSupport <em>Minimum Support</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum Support</em>' attribute.
	 * @see #getMinimumSupport()
	 * @generated
	 */
	void setMinimumSupport(String value);

	/**
	 * Returns the value of the '<em><b>Maximum Set Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the maximum number of items to be included in any frequent itemset to be found.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Maximum Set Size</em>' attribute.
	 * @see #setMaximumSetSize(long)
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getFrequentItemSetFunctionSettings_MaximumSetSize()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getMaximumSetSize();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMaximumSetSize <em>Maximum Set Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Set Size</em>' attribute.
	 * @see #getMaximumSetSize()
	 * @generated
	 */
	void setMaximumSetSize(long value);

	/**
	 * Returns the value of the '<em><b>Exclusion</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclusion</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclusion</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getFrequentItemSetFunctionSettings_Exclusion()
	 * @model
	 * @generated
	 */
	EList<Category> getExclusion();

} // FrequentItemSetFunctionSettings
