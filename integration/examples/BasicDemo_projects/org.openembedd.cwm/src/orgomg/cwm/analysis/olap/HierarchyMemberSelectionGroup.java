/**
 * INRIA/IRISA
 *
 * $Id: HierarchyMemberSelectionGroup.java,v 1.1 2008-04-01 09:35:29 vmahe Exp $
 */
package orgomg.cwm.analysis.olap;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hierarchy Member Selection Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This subtype of MemberSelectionGroup allows users to specify that a particular cube region is determined by hierarchy. This allows the description of data to vary by hierarchy and, therefore, provides the ability to model multiple measure values per hierarchy
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.olap.HierarchyMemberSelectionGroup#getHierarchy <em>Hierarchy</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.olap.OlapPackage#getHierarchyMemberSelectionGroup()
 * @model
 * @generated
 */
public interface HierarchyMemberSelectionGroup extends MemberSelectionGroup {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Hierarchy</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.olap.Hierarchy}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.olap.Hierarchy#getHierarchyMemberSelectionGroup <em>Hierarchy Member Selection Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The Hierarchies referenced by one or more HierarchyMemberSelectionGroups.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Hierarchy</em>' reference list.
	 * @see orgomg.cwm.analysis.olap.OlapPackage#getHierarchyMemberSelectionGroup_Hierarchy()
	 * @see orgomg.cwm.analysis.olap.Hierarchy#getHierarchyMemberSelectionGroup
	 * @model opposite="hierarchyMemberSelectionGroup" required="true"
	 * @generated
	 */
	EList<Hierarchy> getHierarchy();

} // HierarchyMemberSelectionGroup
