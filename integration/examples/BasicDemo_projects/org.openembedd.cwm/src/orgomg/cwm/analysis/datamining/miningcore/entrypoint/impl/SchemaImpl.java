/**
 * INRIA/IRISA
 *
 * $Id: SchemaImpl.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Catalog;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;
import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel;
import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult;
import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningresultPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.objectmodel.core.impl.NamespaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getLogicalData <em>Logical Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getCategoryMatrix <em>Category Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getAuxObjects <em>Aux Objects</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getTaxonomy <em>Taxonomy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getMiningFunctionSettings <em>Mining Function Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getMiningModel <em>Mining Model</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getMiningTask <em>Mining Task</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.SchemaImpl#getResult <em>Result</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SchemaImpl extends NamespaceImpl implements Schema {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getLogicalData() <em>Logical Data</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalData()
	 * @generated
	 * @ordered
	 */
	protected EList<LogicalData> logicalData;

	/**
	 * The cached value of the '{@link #getCategoryMatrix() <em>Category Matrix</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategoryMatrix()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryMatrix> categoryMatrix;

	/**
	 * The cached value of the '{@link #getAuxObjects() <em>Aux Objects</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuxObjects()
	 * @generated
	 * @ordered
	 */
	protected AuxiliaryObject auxObjects;

	/**
	 * The cached value of the '{@link #getTaxonomy() <em>Taxonomy</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaxonomy()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryTaxonomy> taxonomy;

	/**
	 * The cached value of the '{@link #getMiningFunctionSettings() <em>Mining Function Settings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMiningFunctionSettings()
	 * @generated
	 * @ordered
	 */
	protected EList<MiningFunctionSettings> miningFunctionSettings;

	/**
	 * The cached value of the '{@link #getMiningModel() <em>Mining Model</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMiningModel()
	 * @generated
	 * @ordered
	 */
	protected EList<MiningModel> miningModel;

	/**
	 * The cached value of the '{@link #getMiningTask() <em>Mining Task</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMiningTask()
	 * @generated
	 * @ordered
	 */
	protected EList<MiningTask> miningTask;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected EList<MiningResult> result;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchemaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EntrypointPackage.Literals.SCHEMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Catalog getCatalog() {
		if (eContainerFeatureID != EntrypointPackage.SCHEMA__CATALOG) return null;
		return (Catalog)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatalog(Catalog newCatalog, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCatalog, EntrypointPackage.SCHEMA__CATALOG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCatalog(Catalog newCatalog) {
		if (newCatalog != eInternalContainer() || (eContainerFeatureID != EntrypointPackage.SCHEMA__CATALOG && newCatalog != null)) {
			if (EcoreUtil.isAncestor(this, newCatalog))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCatalog != null)
				msgs = ((InternalEObject)newCatalog).eInverseAdd(this, EntrypointPackage.CATALOG__SCHEMA, Catalog.class, msgs);
			msgs = basicSetCatalog(newCatalog, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntrypointPackage.SCHEMA__CATALOG, newCatalog, newCatalog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogicalData> getLogicalData() {
		if (logicalData == null) {
			logicalData = new EObjectContainmentWithInverseEList<LogicalData>(LogicalData.class, this, EntrypointPackage.SCHEMA__LOGICAL_DATA, MiningdataPackage.LOGICAL_DATA__SCHEMA);
		}
		return logicalData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CategoryMatrix> getCategoryMatrix() {
		if (categoryMatrix == null) {
			categoryMatrix = new EObjectContainmentWithInverseEList<CategoryMatrix>(CategoryMatrix.class, this, EntrypointPackage.SCHEMA__CATEGORY_MATRIX, MiningdataPackage.CATEGORY_MATRIX__SCHEMA);
		}
		return categoryMatrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuxiliaryObject getAuxObjects() {
		return auxObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuxObjects(AuxiliaryObject newAuxObjects, NotificationChain msgs) {
		AuxiliaryObject oldAuxObjects = auxObjects;
		auxObjects = newAuxObjects;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EntrypointPackage.SCHEMA__AUX_OBJECTS, oldAuxObjects, newAuxObjects);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuxObjects(AuxiliaryObject newAuxObjects) {
		if (newAuxObjects != auxObjects) {
			NotificationChain msgs = null;
			if (auxObjects != null)
				msgs = ((InternalEObject)auxObjects).eInverseRemove(this, EntrypointPackage.AUXILIARY_OBJECT__SCHEMA, AuxiliaryObject.class, msgs);
			if (newAuxObjects != null)
				msgs = ((InternalEObject)newAuxObjects).eInverseAdd(this, EntrypointPackage.AUXILIARY_OBJECT__SCHEMA, AuxiliaryObject.class, msgs);
			msgs = basicSetAuxObjects(newAuxObjects, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntrypointPackage.SCHEMA__AUX_OBJECTS, newAuxObjects, newAuxObjects));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CategoryTaxonomy> getTaxonomy() {
		if (taxonomy == null) {
			taxonomy = new EObjectContainmentWithInverseEList<CategoryTaxonomy>(CategoryTaxonomy.class, this, EntrypointPackage.SCHEMA__TAXONOMY, MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA);
		}
		return taxonomy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MiningFunctionSettings> getMiningFunctionSettings() {
		if (miningFunctionSettings == null) {
			miningFunctionSettings = new EObjectContainmentWithInverseEList<MiningFunctionSettings>(MiningFunctionSettings.class, this, EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA);
		}
		return miningFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MiningModel> getMiningModel() {
		if (miningModel == null) {
			miningModel = new EObjectContainmentWithInverseEList<MiningModel>(MiningModel.class, this, EntrypointPackage.SCHEMA__MINING_MODEL, MiningmodelPackage.MINING_MODEL__SCHEMA);
		}
		return miningModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MiningTask> getMiningTask() {
		if (miningTask == null) {
			miningTask = new EObjectContainmentWithInverseEList<MiningTask>(MiningTask.class, this, EntrypointPackage.SCHEMA__MINING_TASK, MiningtaskPackage.MINING_TASK__SCHEMA);
		}
		return miningTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MiningResult> getResult() {
		if (result == null) {
			result = new EObjectContainmentWithInverseEList<MiningResult>(MiningResult.class, this, EntrypointPackage.SCHEMA__RESULT, MiningresultPackage.MINING_RESULT__SCHEMA);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCatalog((Catalog)otherEnd, msgs);
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLogicalData()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCategoryMatrix()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				if (auxObjects != null)
					msgs = ((InternalEObject)auxObjects).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EntrypointPackage.SCHEMA__AUX_OBJECTS, null, msgs);
				return basicSetAuxObjects((AuxiliaryObject)otherEnd, msgs);
			case EntrypointPackage.SCHEMA__TAXONOMY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTaxonomy()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMiningFunctionSettings()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMiningModel()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_TASK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMiningTask()).basicAdd(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__RESULT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResult()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				return basicSetCatalog(null, msgs);
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				return ((InternalEList<?>)getLogicalData()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				return ((InternalEList<?>)getCategoryMatrix()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				return basicSetAuxObjects(null, msgs);
			case EntrypointPackage.SCHEMA__TAXONOMY:
				return ((InternalEList<?>)getTaxonomy()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				return ((InternalEList<?>)getMiningFunctionSettings()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				return ((InternalEList<?>)getMiningModel()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__MINING_TASK:
				return ((InternalEList<?>)getMiningTask()).basicRemove(otherEnd, msgs);
			case EntrypointPackage.SCHEMA__RESULT:
				return ((InternalEList<?>)getResult()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.CATALOG__SCHEMA, Catalog.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				return getCatalog();
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				return getLogicalData();
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				return getCategoryMatrix();
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				return getAuxObjects();
			case EntrypointPackage.SCHEMA__TAXONOMY:
				return getTaxonomy();
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				return getMiningFunctionSettings();
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				return getMiningModel();
			case EntrypointPackage.SCHEMA__MINING_TASK:
				return getMiningTask();
			case EntrypointPackage.SCHEMA__RESULT:
				return getResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				setCatalog((Catalog)newValue);
				return;
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				getLogicalData().clear();
				getLogicalData().addAll((Collection<? extends LogicalData>)newValue);
				return;
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				getCategoryMatrix().clear();
				getCategoryMatrix().addAll((Collection<? extends CategoryMatrix>)newValue);
				return;
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				setAuxObjects((AuxiliaryObject)newValue);
				return;
			case EntrypointPackage.SCHEMA__TAXONOMY:
				getTaxonomy().clear();
				getTaxonomy().addAll((Collection<? extends CategoryTaxonomy>)newValue);
				return;
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				getMiningFunctionSettings().clear();
				getMiningFunctionSettings().addAll((Collection<? extends MiningFunctionSettings>)newValue);
				return;
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				getMiningModel().clear();
				getMiningModel().addAll((Collection<? extends MiningModel>)newValue);
				return;
			case EntrypointPackage.SCHEMA__MINING_TASK:
				getMiningTask().clear();
				getMiningTask().addAll((Collection<? extends MiningTask>)newValue);
				return;
			case EntrypointPackage.SCHEMA__RESULT:
				getResult().clear();
				getResult().addAll((Collection<? extends MiningResult>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				setCatalog((Catalog)null);
				return;
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				getLogicalData().clear();
				return;
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				getCategoryMatrix().clear();
				return;
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				setAuxObjects((AuxiliaryObject)null);
				return;
			case EntrypointPackage.SCHEMA__TAXONOMY:
				getTaxonomy().clear();
				return;
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				getMiningFunctionSettings().clear();
				return;
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				getMiningModel().clear();
				return;
			case EntrypointPackage.SCHEMA__MINING_TASK:
				getMiningTask().clear();
				return;
			case EntrypointPackage.SCHEMA__RESULT:
				getResult().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EntrypointPackage.SCHEMA__CATALOG:
				return getCatalog() != null;
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
				return logicalData != null && !logicalData.isEmpty();
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
				return categoryMatrix != null && !categoryMatrix.isEmpty();
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
				return auxObjects != null;
			case EntrypointPackage.SCHEMA__TAXONOMY:
				return taxonomy != null && !taxonomy.isEmpty();
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
				return miningFunctionSettings != null && !miningFunctionSettings.isEmpty();
			case EntrypointPackage.SCHEMA__MINING_MODEL:
				return miningModel != null && !miningModel.isEmpty();
			case EntrypointPackage.SCHEMA__MINING_TASK:
				return miningTask != null && !miningTask.isEmpty();
			case EntrypointPackage.SCHEMA__RESULT:
				return result != null && !result.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SchemaImpl
