/**
 * INRIA/IRISA
 *
 * $Id: AttributeAssignmentSetImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Assignment Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentSetImpl#getAuxiliaryObject <em>Auxiliary Object</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeAssignmentSetImpl extends ModelElementImpl implements AttributeAssignmentSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeAssignmentSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.ATTRIBUTE_ASSIGNMENT_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuxiliaryObject getAuxiliaryObject() {
		if (eContainerFeatureID != MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT) return null;
		return (AuxiliaryObject)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuxiliaryObject(AuxiliaryObject newAuxiliaryObject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAuxiliaryObject, MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuxiliaryObject(AuxiliaryObject newAuxiliaryObject) {
		if (newAuxiliaryObject != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT && newAuxiliaryObject != null)) {
			if (EcoreUtil.isAncestor(this, newAuxiliaryObject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAuxiliaryObject != null)
				msgs = ((InternalEObject)newAuxiliaryObject).eInverseAdd(this, EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET, AuxiliaryObject.class, msgs);
			msgs = basicSetAuxiliaryObject(newAuxiliaryObject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT, newAuxiliaryObject, newAuxiliaryObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAuxiliaryObject((AuxiliaryObject)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				return basicSetAuxiliaryObject(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET, AuxiliaryObject.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				return getAuxiliaryObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				setAuxiliaryObject((AuxiliaryObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				setAuxiliaryObject((AuxiliaryObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT:
				return getAuxiliaryObject() != null;
		}
		return super.eIsSet(featureID);
	}

} //AttributeAssignmentSetImpl
