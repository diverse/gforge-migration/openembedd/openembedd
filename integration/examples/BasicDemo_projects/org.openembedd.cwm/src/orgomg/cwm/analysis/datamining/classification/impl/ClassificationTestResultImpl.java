/**
 * INRIA/IRISA
 *
 * $Id: ClassificationTestResultImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;
import orgomg.cwm.analysis.datamining.classification.ClassificationTestResult;
import orgomg.cwm.analysis.datamining.classification.ClassificationTestTask;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.MiningTestResultImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl#getConfusionMatrix <em>Confusion Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl#getTestTask <em>Test Task</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassificationTestResultImpl extends MiningTestResultImpl implements ClassificationTestResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected static final String ACCURACY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected String accuracy = ACCURACY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConfusionMatrix() <em>Confusion Matrix</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfusionMatrix()
	 * @generated
	 * @ordered
	 */
	protected CategoryMatrix confusionMatrix;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassificationTestResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassificationPackage.Literals.CLASSIFICATION_TEST_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAccuracy() {
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccuracy(String newAccuracy) {
		String oldAccuracy = accuracy;
		accuracy = newAccuracy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.CLASSIFICATION_TEST_RESULT__ACCURACY, oldAccuracy, accuracy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrix getConfusionMatrix() {
		return confusionMatrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfusionMatrix(CategoryMatrix newConfusionMatrix, NotificationChain msgs) {
		CategoryMatrix oldConfusionMatrix = confusionMatrix;
		confusionMatrix = newConfusionMatrix;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX, oldConfusionMatrix, newConfusionMatrix);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfusionMatrix(CategoryMatrix newConfusionMatrix) {
		if (newConfusionMatrix != confusionMatrix) {
			NotificationChain msgs = null;
			if (confusionMatrix != null)
				msgs = ((InternalEObject)confusionMatrix).eInverseRemove(this, MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT, CategoryMatrix.class, msgs);
			if (newConfusionMatrix != null)
				msgs = ((InternalEObject)newConfusionMatrix).eInverseAdd(this, MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT, CategoryMatrix.class, msgs);
			msgs = basicSetConfusionMatrix(newConfusionMatrix, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX, newConfusionMatrix, newConfusionMatrix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassificationTestTask getTestTask() {
		if (eContainerFeatureID != ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK) return null;
		return (ClassificationTestTask)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestTask(ClassificationTestTask newTestTask, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTestTask, ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestTask(ClassificationTestTask newTestTask) {
		if (newTestTask != eInternalContainer() || (eContainerFeatureID != ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK && newTestTask != null)) {
			if (EcoreUtil.isAncestor(this, newTestTask))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTestTask != null)
				msgs = ((InternalEObject)newTestTask).eInverseAdd(this, ClassificationPackage.CLASSIFICATION_TEST_TASK__TEST_RESULT, ClassificationTestTask.class, msgs);
			msgs = basicSetTestTask(newTestTask, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK, newTestTask, newTestTask));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				if (confusionMatrix != null)
					msgs = ((InternalEObject)confusionMatrix).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX, null, msgs);
				return basicSetConfusionMatrix((CategoryMatrix)otherEnd, msgs);
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTestTask((ClassificationTestTask)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				return basicSetConfusionMatrix(null, msgs);
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				return basicSetTestTask(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				return eInternalContainer().eInverseRemove(this, ClassificationPackage.CLASSIFICATION_TEST_TASK__TEST_RESULT, ClassificationTestTask.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__ACCURACY:
				return getAccuracy();
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				return getConfusionMatrix();
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				return getTestTask();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__ACCURACY:
				setAccuracy((String)newValue);
				return;
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				setConfusionMatrix((CategoryMatrix)newValue);
				return;
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				setTestTask((ClassificationTestTask)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__ACCURACY:
				setAccuracy(ACCURACY_EDEFAULT);
				return;
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				setConfusionMatrix((CategoryMatrix)null);
				return;
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				setTestTask((ClassificationTestTask)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__ACCURACY:
				return ACCURACY_EDEFAULT == null ? accuracy != null : !ACCURACY_EDEFAULT.equals(accuracy);
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX:
				return confusionMatrix != null;
			case ClassificationPackage.CLASSIFICATION_TEST_RESULT__TEST_TASK:
				return getTestTask() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (accuracy: ");
		result.append(accuracy);
		result.append(')');
		return result.toString();
	}

} //ClassificationTestResultImpl
