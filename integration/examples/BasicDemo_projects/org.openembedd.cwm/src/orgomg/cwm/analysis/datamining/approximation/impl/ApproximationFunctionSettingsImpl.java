/**
 * INRIA/IRISA
 *
 * $Id: ApproximationFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.approximation.ApproximationFunctionSettings;
import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedFunctionSettingsImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationFunctionSettingsImpl#getToleratedError <em>Tolerated Error</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApproximationFunctionSettingsImpl extends SupervisedFunctionSettingsImpl implements ApproximationFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getToleratedError() <em>Tolerated Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToleratedError()
	 * @generated
	 * @ordered
	 */
	protected static final String TOLERATED_ERROR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToleratedError() <em>Tolerated Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToleratedError()
	 * @generated
	 * @ordered
	 */
	protected String toleratedError = TOLERATED_ERROR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApproximationFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApproximationPackage.Literals.APPROXIMATION_FUNCTION_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToleratedError() {
		return toleratedError;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToleratedError(String newToleratedError) {
		String oldToleratedError = toleratedError;
		toleratedError = newToleratedError;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS__TOLERATED_ERROR, oldToleratedError, toleratedError));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS__TOLERATED_ERROR:
				return getToleratedError();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS__TOLERATED_ERROR:
				setToleratedError((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS__TOLERATED_ERROR:
				setToleratedError(TOLERATED_ERROR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS__TOLERATED_ERROR:
				return TOLERATED_ERROR_EDEFAULT == null ? toleratedError != null : !TOLERATED_ERROR_EDEFAULT.equals(toleratedError);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (toleratedError: ");
		result.append(toleratedError);
		result.append(')');
		return result.toString();
	}

} //ApproximationFunctionSettingsImpl
