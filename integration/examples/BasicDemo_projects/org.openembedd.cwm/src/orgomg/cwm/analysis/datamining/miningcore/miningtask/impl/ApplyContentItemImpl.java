/**
 * INRIA/IRISA
 *
 * $Id: ApplyContentItemImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apply Content Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyContentItemImpl#getTopNthIndex <em>Top Nth Index</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ApplyContentItemImpl extends ApplyOutputItemImpl implements ApplyContentItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getTopNthIndex() <em>Top Nth Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopNthIndex()
	 * @generated
	 * @ordered
	 */
	protected static final long TOP_NTH_INDEX_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getTopNthIndex() <em>Top Nth Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopNthIndex()
	 * @generated
	 * @ordered
	 */
	protected long topNthIndex = TOP_NTH_INDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplyContentItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.APPLY_CONTENT_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getTopNthIndex() {
		return topNthIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopNthIndex(long newTopNthIndex) {
		long oldTopNthIndex = topNthIndex;
		topNthIndex = newTopNthIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.APPLY_CONTENT_ITEM__TOP_NTH_INDEX, oldTopNthIndex, topNthIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_CONTENT_ITEM__TOP_NTH_INDEX:
				return new Long(getTopNthIndex());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_CONTENT_ITEM__TOP_NTH_INDEX:
				setTopNthIndex(((Long)newValue).longValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_CONTENT_ITEM__TOP_NTH_INDEX:
				setTopNthIndex(TOP_NTH_INDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_CONTENT_ITEM__TOP_NTH_INDEX:
				return topNthIndex != TOP_NTH_INDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (topNthIndex: ");
		result.append(topNthIndex);
		result.append(')');
		return result.toString();
	}

} //ApplyContentItemImpl
