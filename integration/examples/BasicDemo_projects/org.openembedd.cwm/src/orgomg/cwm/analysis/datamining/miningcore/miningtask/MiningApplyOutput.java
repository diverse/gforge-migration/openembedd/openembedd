/**
 * INRIA/IRISA
 *
 * $Id: MiningApplyOutput.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Apply Output</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This describes the ouput Specification for a MiningApplyTask.
 * It contains a set of attributes (represented as ApplyOutputItem objects) holding the output information. These attributes can hold the score or other computed information, or else be copied from input columns for reference.
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput#getItem <em>Item</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyOutput()
 * @model
 * @generated
 */
public interface MiningApplyOutput extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Item</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput <em>Apply Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningApplyOutput_Item()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput
	 * @model opposite="applyOutput" containment="true" required="true"
	 * @generated
	 */
	EList<ApplyOutputItem> getItem();

} // MiningApplyOutput
