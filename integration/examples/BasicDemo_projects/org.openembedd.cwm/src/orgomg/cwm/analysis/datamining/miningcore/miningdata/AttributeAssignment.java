/**
 * INRIA/IRISA
 *
 * $Id: AttributeAssignment.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.Attribute;
import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This object provides a mapping between a mining attribute (logical data) and a set of attributes in the input data (physical data).
 * logicalAttribute is the mining attribute being mapped by this object.
 * orderIdAttribute is used when ordering of attributes is required. In some cases, ordering of attributes is important (as in sequence analysis). In other cases, a sequence of an attribute is favored over having a set-valued attributes.
 * 
 * AttributeAssignment can be reused among several tasks, but a MiningAttribute can be refered to by an AttributeAssignment within a task.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getLogicalAttribute <em>Logical Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment#getOrderIdAttribute <em>Order Id Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeAssignment()
 * @model
 * @generated
 */
public interface AttributeAssignment extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Logical Attribute</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Attribute</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Attribute</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeAssignment_LogicalAttribute()
	 * @model required="true"
	 * @generated
	 */
	EList<MiningAttribute> getLogicalAttribute();

	/**
	 * Returns the value of the '<em><b>Order Id Attribute</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.objectmodel.core.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order Id Attribute</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order Id Attribute</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeAssignment_OrderIdAttribute()
	 * @model
	 * @generated
	 */
	EList<Attribute> getOrderIdAttribute();

} // AttributeAssignment
