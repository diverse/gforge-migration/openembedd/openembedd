/**
 * INRIA/IRISA
 *
 * $Id: MiningdataPackageImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import orgomg.cwm.analysis.businessnomenclature.BusinessnomenclaturePackage;

import orgomg.cwm.analysis.businessnomenclature.impl.BusinessnomenclaturePackageImpl;

import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;

import orgomg.cwm.analysis.datamining.approximation.impl.ApproximationPackageImpl;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;

import orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage;

import orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;

import orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl;

import orgomg.cwm.analysis.datamining.clustering.ClusteringPackage;

import orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.EntrypointPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.DirectAttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataFactory;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningfunctionsettingsPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.MiningmodelPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningresultPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.impl.MiningresultPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedPackageImpl;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage;

import orgomg.cwm.analysis.informationvisualization.impl.InformationvisualizationPackageImpl;

import orgomg.cwm.analysis.olap.OlapPackage;

import orgomg.cwm.analysis.olap.impl.OlapPackageImpl;

import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.analysis.transformation.impl.TransformationPackageImpl;

import orgomg.cwm.foundation.businessinformation.BusinessinformationPackage;

import orgomg.cwm.foundation.businessinformation.impl.BusinessinformationPackageImpl;

import orgomg.cwm.foundation.datatypes.DatatypesPackage;

import orgomg.cwm.foundation.datatypes.impl.DatatypesPackageImpl;

import orgomg.cwm.foundation.expressions.ExpressionsPackage;

import orgomg.cwm.foundation.expressions.impl.ExpressionsPackageImpl;

import orgomg.cwm.foundation.keysindexes.KeysindexesPackage;

import orgomg.cwm.foundation.keysindexes.impl.KeysindexesPackageImpl;

import orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage;

import orgomg.cwm.foundation.softwaredeployment.impl.SoftwaredeploymentPackageImpl;

import orgomg.cwm.foundation.typemapping.TypemappingPackage;

import orgomg.cwm.foundation.typemapping.impl.TypemappingPackageImpl;

import orgomg.cwm.management.warehouseoperation.WarehouseoperationPackage;

import orgomg.cwm.management.warehouseoperation.impl.WarehouseoperationPackageImpl;

import orgomg.cwm.management.warehouseprocess.WarehouseprocessPackage;

import orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage;

import orgomg.cwm.management.warehouseprocess.datatype.impl.DatatypePackageImpl;

import orgomg.cwm.management.warehouseprocess.events.EventsPackage;

import orgomg.cwm.management.warehouseprocess.events.impl.EventsPackageImpl;

import orgomg.cwm.management.warehouseprocess.impl.WarehouseprocessPackageImpl;

import orgomg.cwm.objectmodel.behavioral.BehavioralPackage;

import orgomg.cwm.objectmodel.behavioral.impl.BehavioralPackageImpl;

import orgomg.cwm.objectmodel.core.CorePackage;

import orgomg.cwm.objectmodel.core.impl.CorePackageImpl;

import orgomg.cwm.objectmodel.instance.InstancePackage;

import orgomg.cwm.objectmodel.instance.impl.InstancePackageImpl;

import orgomg.cwm.objectmodel.relationships.RelationshipsPackage;

import orgomg.cwm.objectmodel.relationships.impl.RelationshipsPackageImpl;

import orgomg.cwm.resource.multidimensional.MultidimensionalPackage;

import orgomg.cwm.resource.multidimensional.impl.MultidimensionalPackageImpl;

import orgomg.cwm.resource.record.RecordPackage;

import orgomg.cwm.resource.record.impl.RecordPackageImpl;

import orgomg.cwm.resource.relational.RelationalPackage;

import orgomg.cwm.resource.relational.enumerations.EnumerationsPackage;

import orgomg.cwm.resource.relational.enumerations.impl.EnumerationsPackageImpl;

import orgomg.cwm.resource.relational.impl.RelationalPackageImpl;

import orgomg.cwm.resource.xml.XmlPackage;

import orgomg.cwm.resource.xml.impl.XmlPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiningdataPackageImpl extends EPackageImpl implements MiningdataPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeAssignmentSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeUsageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeUsageSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoricalAttributePropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass directAttributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numericalAttributePropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ordinalAttributePropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pivotAttributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reversePivotAttributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setAttributeAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMapObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMapObjectEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMapTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMatrixEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMatrixEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMatrixObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryMatrixTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryTaxonomyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attributeSelectionFunctionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attributeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum orderTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum usageOptionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum valueSelectionFunctionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum categoryPropertyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum matrixPropertyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MiningdataPackageImpl() {
		super(eNS_URI, MiningdataFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MiningdataPackage init() {
		if (isInited) return (MiningdataPackage)EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI);

		// Obtain or create and register package
		MiningdataPackageImpl theMiningdataPackage = (MiningdataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof MiningdataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new MiningdataPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		BehavioralPackageImpl theBehavioralPackage = (BehavioralPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) instanceof BehavioralPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) : BehavioralPackage.eINSTANCE);
		RelationshipsPackageImpl theRelationshipsPackage = (RelationshipsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) instanceof RelationshipsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) : RelationshipsPackage.eINSTANCE);
		InstancePackageImpl theInstancePackage = (InstancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) instanceof InstancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) : InstancePackage.eINSTANCE);
		BusinessinformationPackageImpl theBusinessinformationPackage = (BusinessinformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) instanceof BusinessinformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) : BusinessinformationPackage.eINSTANCE);
		DatatypesPackageImpl theDatatypesPackage = (DatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) instanceof DatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) : DatatypesPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		KeysindexesPackageImpl theKeysindexesPackage = (KeysindexesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) instanceof KeysindexesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) : KeysindexesPackage.eINSTANCE);
		SoftwaredeploymentPackageImpl theSoftwaredeploymentPackage = (SoftwaredeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) instanceof SoftwaredeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) : SoftwaredeploymentPackage.eINSTANCE);
		TypemappingPackageImpl theTypemappingPackage = (TypemappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) instanceof TypemappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) : TypemappingPackage.eINSTANCE);
		RelationalPackageImpl theRelationalPackage = (RelationalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) instanceof RelationalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) : RelationalPackage.eINSTANCE);
		EnumerationsPackageImpl theEnumerationsPackage = (EnumerationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) instanceof EnumerationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) : EnumerationsPackage.eINSTANCE);
		RecordPackageImpl theRecordPackage = (RecordPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) instanceof RecordPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) : RecordPackage.eINSTANCE);
		MultidimensionalPackageImpl theMultidimensionalPackage = (MultidimensionalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) instanceof MultidimensionalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) : MultidimensionalPackage.eINSTANCE);
		XmlPackageImpl theXmlPackage = (XmlPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) instanceof XmlPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) : XmlPackage.eINSTANCE);
		TransformationPackageImpl theTransformationPackage = (TransformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) instanceof TransformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) : TransformationPackage.eINSTANCE);
		OlapPackageImpl theOlapPackage = (OlapPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) instanceof OlapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) : OlapPackage.eINSTANCE);
		MiningfunctionsettingsPackageImpl theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) instanceof MiningfunctionsettingsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) : MiningfunctionsettingsPackage.eINSTANCE);
		MiningmodelPackageImpl theMiningmodelPackage = (MiningmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) instanceof MiningmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) : MiningmodelPackage.eINSTANCE);
		MiningresultPackageImpl theMiningresultPackage = (MiningresultPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) instanceof MiningresultPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) : MiningresultPackage.eINSTANCE);
		MiningtaskPackageImpl theMiningtaskPackage = (MiningtaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) instanceof MiningtaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) : MiningtaskPackage.eINSTANCE);
		EntrypointPackageImpl theEntrypointPackage = (EntrypointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) instanceof EntrypointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) : EntrypointPackage.eINSTANCE);
		ClusteringPackageImpl theClusteringPackage = (ClusteringPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) instanceof ClusteringPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) : ClusteringPackage.eINSTANCE);
		AssociationrulesPackageImpl theAssociationrulesPackage = (AssociationrulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) instanceof AssociationrulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) : AssociationrulesPackage.eINSTANCE);
		SupervisedPackageImpl theSupervisedPackage = (SupervisedPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) instanceof SupervisedPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) : SupervisedPackage.eINSTANCE);
		ClassificationPackageImpl theClassificationPackage = (ClassificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) instanceof ClassificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) : ClassificationPackage.eINSTANCE);
		ApproximationPackageImpl theApproximationPackage = (ApproximationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) instanceof ApproximationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) : ApproximationPackage.eINSTANCE);
		AttributeimportancePackageImpl theAttributeimportancePackage = (AttributeimportancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) instanceof AttributeimportancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) : AttributeimportancePackage.eINSTANCE);
		InformationvisualizationPackageImpl theInformationvisualizationPackage = (InformationvisualizationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) instanceof InformationvisualizationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) : InformationvisualizationPackage.eINSTANCE);
		BusinessnomenclaturePackageImpl theBusinessnomenclaturePackage = (BusinessnomenclaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) instanceof BusinessnomenclaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) : BusinessnomenclaturePackage.eINSTANCE);
		WarehouseprocessPackageImpl theWarehouseprocessPackage = (WarehouseprocessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) instanceof WarehouseprocessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) : WarehouseprocessPackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		EventsPackageImpl theEventsPackage = (EventsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) instanceof EventsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) : EventsPackage.eINSTANCE);
		WarehouseoperationPackageImpl theWarehouseoperationPackage = (WarehouseoperationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) instanceof WarehouseoperationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) : WarehouseoperationPackage.eINSTANCE);

		// Create package meta-data objects
		theMiningdataPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theBehavioralPackage.createPackageContents();
		theRelationshipsPackage.createPackageContents();
		theInstancePackage.createPackageContents();
		theBusinessinformationPackage.createPackageContents();
		theDatatypesPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theKeysindexesPackage.createPackageContents();
		theSoftwaredeploymentPackage.createPackageContents();
		theTypemappingPackage.createPackageContents();
		theRelationalPackage.createPackageContents();
		theEnumerationsPackage.createPackageContents();
		theRecordPackage.createPackageContents();
		theMultidimensionalPackage.createPackageContents();
		theXmlPackage.createPackageContents();
		theTransformationPackage.createPackageContents();
		theOlapPackage.createPackageContents();
		theMiningfunctionsettingsPackage.createPackageContents();
		theMiningmodelPackage.createPackageContents();
		theMiningresultPackage.createPackageContents();
		theMiningtaskPackage.createPackageContents();
		theEntrypointPackage.createPackageContents();
		theClusteringPackage.createPackageContents();
		theAssociationrulesPackage.createPackageContents();
		theSupervisedPackage.createPackageContents();
		theClassificationPackage.createPackageContents();
		theApproximationPackage.createPackageContents();
		theAttributeimportancePackage.createPackageContents();
		theInformationvisualizationPackage.createPackageContents();
		theBusinessnomenclaturePackage.createPackageContents();
		theWarehouseprocessPackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theEventsPackage.createPackageContents();
		theWarehouseoperationPackage.createPackageContents();

		// Initialize created meta-data
		theMiningdataPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theBehavioralPackage.initializePackageContents();
		theRelationshipsPackage.initializePackageContents();
		theInstancePackage.initializePackageContents();
		theBusinessinformationPackage.initializePackageContents();
		theDatatypesPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theKeysindexesPackage.initializePackageContents();
		theSoftwaredeploymentPackage.initializePackageContents();
		theTypemappingPackage.initializePackageContents();
		theRelationalPackage.initializePackageContents();
		theEnumerationsPackage.initializePackageContents();
		theRecordPackage.initializePackageContents();
		theMultidimensionalPackage.initializePackageContents();
		theXmlPackage.initializePackageContents();
		theTransformationPackage.initializePackageContents();
		theOlapPackage.initializePackageContents();
		theMiningfunctionsettingsPackage.initializePackageContents();
		theMiningmodelPackage.initializePackageContents();
		theMiningresultPackage.initializePackageContents();
		theMiningtaskPackage.initializePackageContents();
		theEntrypointPackage.initializePackageContents();
		theClusteringPackage.initializePackageContents();
		theAssociationrulesPackage.initializePackageContents();
		theSupervisedPackage.initializePackageContents();
		theClassificationPackage.initializePackageContents();
		theApproximationPackage.initializePackageContents();
		theAttributeimportancePackage.initializePackageContents();
		theInformationvisualizationPackage.initializePackageContents();
		theBusinessnomenclaturePackage.initializePackageContents();
		theWarehouseprocessPackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theEventsPackage.initializePackageContents();
		theWarehouseoperationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMiningdataPackage.freeze();

		return theMiningdataPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeAssignment() {
		return attributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignment_LogicalAttribute() {
		return (EReference)attributeAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignment_OrderIdAttribute() {
		return (EReference)attributeAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeAssignmentSet() {
		return attributeAssignmentSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeAssignmentSet_AuxiliaryObject() {
		return (EReference)attributeAssignmentSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeUsage() {
		return attributeUsageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeUsage_Usage() {
		return (EAttribute)attributeUsageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeUsage_Weight() {
		return (EAttribute)attributeUsageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeUsage_SuppressDiscretization() {
		return (EAttribute)attributeUsageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeUsage_SuppressNormalization() {
		return (EAttribute)attributeUsageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeUsage_Attribute() {
		return (EReference)attributeUsageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeUsageSet() {
		return attributeUsageSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeUsageSet_Settings() {
		return (EReference)attributeUsageSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoricalAttributeProperties() {
		return categoricalAttributePropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoricalAttributeProperties_LogicalAttribute() {
		return (EReference)categoricalAttributePropertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoricalAttributeProperties_Taxonomy() {
		return (EReference)categoricalAttributePropertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoricalAttributeProperties_Category() {
		return (EReference)categoricalAttributePropertiesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirectAttributeAssignment() {
		return directAttributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirectAttributeAssignment_Attribute() {
		return (EReference)directAttributeAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogicalAttribute() {
		return logicalAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogicalAttribute_IsSetValued() {
		return (EAttribute)logicalAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalAttribute_CategoricalProperties() {
		return (EReference)logicalAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalAttribute_NumericalProperties() {
		return (EReference)logicalAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogicalData() {
		return logicalDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalData_Schema() {
		return (EReference)logicalDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningAttribute() {
		return miningAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMiningAttribute_DisplayName() {
		return (EAttribute)miningAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMiningAttribute_AttributeType() {
		return (EAttribute)miningAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumericalAttributeProperties() {
		return numericalAttributePropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_LowerBound() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_UpperBound() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_IsDiscrete() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_IsCyclic() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_Anchor() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_CycleBegin() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_CycleEnd() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericalAttributeProperties_DiscreteStepSize() {
		return (EAttribute)numericalAttributePropertiesEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNumericalAttributeProperties_LogicalAttribute() {
		return (EReference)numericalAttributePropertiesEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrdinalAttributeProperties() {
		return ordinalAttributePropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOrdinalAttributeProperties_OrderType() {
		return (EAttribute)ordinalAttributePropertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOrdinalAttributeProperties_IsCyclic() {
		return (EAttribute)ordinalAttributePropertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalData() {
		return physicalDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalData_Source() {
		return (EReference)physicalDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPivotAttributeAssignment() {
		return pivotAttributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPivotAttributeAssignment_SetIdAttribute() {
		return (EReference)pivotAttributeAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPivotAttributeAssignment_NameAttribute() {
		return (EReference)pivotAttributeAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPivotAttributeAssignment_ValueAttribute() {
		return (EReference)pivotAttributeAssignmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReversePivotAttributeAssignment() {
		return reversePivotAttributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReversePivotAttributeAssignment_AttributeSelectionFunction() {
		return (EAttribute)reversePivotAttributeAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReversePivotAttributeAssignment_ValueSelectionFunction() {
		return (EAttribute)reversePivotAttributeAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReversePivotAttributeAssignment_SelectorAttribute() {
		return (EReference)reversePivotAttributeAssignmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetAttributeAssignment() {
		return setAttributeAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetAttributeAssignment_SetIdAttribute() {
		return (EReference)setAttributeAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetAttributeAssignment_MemberAttribute() {
		return (EReference)setAttributeAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategory() {
		return categoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategory_Value() {
		return (EAttribute)categoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategory_IsNullCategory() {
		return (EAttribute)categoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategory_DisplayName() {
		return (EAttribute)categoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategory_Property() {
		return (EAttribute)categoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategory_Prior() {
		return (EAttribute)categoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategory_CategoricalProperties() {
		return (EReference)categoryEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMap() {
		return categoryMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMap_IsMultiLevel() {
		return (EAttribute)categoryMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMap_IsItemMap() {
		return (EAttribute)categoryMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMap_Taxonomy() {
		return (EReference)categoryMapEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMapObject() {
		return categoryMapObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapObject_Entry() {
		return (EReference)categoryMapObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMapObjectEntry() {
		return categoryMapObjectEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMapObjectEntry_GraphId() {
		return (EAttribute)categoryMapObjectEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapObjectEntry_MapObject() {
		return (EReference)categoryMapObjectEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapObjectEntry_Child() {
		return (EReference)categoryMapObjectEntryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapObjectEntry_Parent() {
		return (EReference)categoryMapObjectEntryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMapTable() {
		return categoryMapTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapTable_ChildAttribute() {
		return (EReference)categoryMapTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapTable_Table() {
		return (EReference)categoryMapTableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapTable_GraphIdAttribute() {
		return (EReference)categoryMapTableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMapTable_ParentAttribute() {
		return (EReference)categoryMapTableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMatrix() {
		return categoryMatrixEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMatrix_DiagonalDefault() {
		return (EAttribute)categoryMatrixEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMatrix_OffDiagonalDefault() {
		return (EAttribute)categoryMatrixEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMatrix_Kind() {
		return (EAttribute)categoryMatrixEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrix_Category() {
		return (EReference)categoryMatrixEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrix_Schema() {
		return (EReference)categoryMatrixEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrix_TestResult() {
		return (EReference)categoryMatrixEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMatrixEntry() {
		return categoryMatrixEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCategoryMatrixEntry_Value() {
		return (EAttribute)categoryMatrixEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixEntry_ColumnIndex() {
		return (EReference)categoryMatrixEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixEntry_CategoryMatrix() {
		return (EReference)categoryMatrixEntryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixEntry_RowIndex() {
		return (EReference)categoryMatrixEntryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMatrixObject() {
		return categoryMatrixObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixObject_Entry() {
		return (EReference)categoryMatrixObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryMatrixTable() {
		return categoryMatrixTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixTable_Source() {
		return (EReference)categoryMatrixTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixTable_ColumnAttribute() {
		return (EReference)categoryMatrixTableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixTable_RowAttribute() {
		return (EReference)categoryMatrixTableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryMatrixTable_ValueAttribute() {
		return (EReference)categoryMatrixTableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategoryTaxonomy() {
		return categoryTaxonomyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryTaxonomy_CategoryMap() {
		return (EReference)categoryTaxonomyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryTaxonomy_RootCategory() {
		return (EReference)categoryTaxonomyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategoryTaxonomy_Schema() {
		return (EReference)categoryTaxonomyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttributeSelectionFunction() {
		return attributeSelectionFunctionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttributeType() {
		return attributeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOrderType() {
		return orderTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUsageOption() {
		return usageOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getValueSelectionFunction() {
		return valueSelectionFunctionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCategoryProperty() {
		return categoryPropertyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMatrixProperty() {
		return matrixPropertyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDouble() {
		return doubleEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningdataFactory getMiningdataFactory() {
		return (MiningdataFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		attributeAssignmentEClass = createEClass(ATTRIBUTE_ASSIGNMENT);
		createEReference(attributeAssignmentEClass, ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE);
		createEReference(attributeAssignmentEClass, ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE);

		attributeAssignmentSetEClass = createEClass(ATTRIBUTE_ASSIGNMENT_SET);
		createEReference(attributeAssignmentSetEClass, ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT);

		attributeUsageEClass = createEClass(ATTRIBUTE_USAGE);
		createEAttribute(attributeUsageEClass, ATTRIBUTE_USAGE__USAGE);
		createEAttribute(attributeUsageEClass, ATTRIBUTE_USAGE__WEIGHT);
		createEAttribute(attributeUsageEClass, ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION);
		createEAttribute(attributeUsageEClass, ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION);
		createEReference(attributeUsageEClass, ATTRIBUTE_USAGE__ATTRIBUTE);

		attributeUsageSetEClass = createEClass(ATTRIBUTE_USAGE_SET);
		createEReference(attributeUsageSetEClass, ATTRIBUTE_USAGE_SET__SETTINGS);

		categoricalAttributePropertiesEClass = createEClass(CATEGORICAL_ATTRIBUTE_PROPERTIES);
		createEReference(categoricalAttributePropertiesEClass, CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE);
		createEReference(categoricalAttributePropertiesEClass, CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY);
		createEReference(categoricalAttributePropertiesEClass, CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY);

		directAttributeAssignmentEClass = createEClass(DIRECT_ATTRIBUTE_ASSIGNMENT);
		createEReference(directAttributeAssignmentEClass, DIRECT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE);

		logicalAttributeEClass = createEClass(LOGICAL_ATTRIBUTE);
		createEAttribute(logicalAttributeEClass, LOGICAL_ATTRIBUTE__IS_SET_VALUED);
		createEReference(logicalAttributeEClass, LOGICAL_ATTRIBUTE__CATEGORICAL_PROPERTIES);
		createEReference(logicalAttributeEClass, LOGICAL_ATTRIBUTE__NUMERICAL_PROPERTIES);

		logicalDataEClass = createEClass(LOGICAL_DATA);
		createEReference(logicalDataEClass, LOGICAL_DATA__SCHEMA);

		miningAttributeEClass = createEClass(MINING_ATTRIBUTE);
		createEAttribute(miningAttributeEClass, MINING_ATTRIBUTE__DISPLAY_NAME);
		createEAttribute(miningAttributeEClass, MINING_ATTRIBUTE__ATTRIBUTE_TYPE);

		numericalAttributePropertiesEClass = createEClass(NUMERICAL_ATTRIBUTE_PROPERTIES);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END);
		createEAttribute(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE);
		createEReference(numericalAttributePropertiesEClass, NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE);

		ordinalAttributePropertiesEClass = createEClass(ORDINAL_ATTRIBUTE_PROPERTIES);
		createEAttribute(ordinalAttributePropertiesEClass, ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE);
		createEAttribute(ordinalAttributePropertiesEClass, ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC);

		physicalDataEClass = createEClass(PHYSICAL_DATA);
		createEReference(physicalDataEClass, PHYSICAL_DATA__SOURCE);

		pivotAttributeAssignmentEClass = createEClass(PIVOT_ATTRIBUTE_ASSIGNMENT);
		createEReference(pivotAttributeAssignmentEClass, PIVOT_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE);
		createEReference(pivotAttributeAssignmentEClass, PIVOT_ATTRIBUTE_ASSIGNMENT__NAME_ATTRIBUTE);
		createEReference(pivotAttributeAssignmentEClass, PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_ATTRIBUTE);

		reversePivotAttributeAssignmentEClass = createEClass(REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT);
		createEAttribute(reversePivotAttributeAssignmentEClass, REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION);
		createEAttribute(reversePivotAttributeAssignmentEClass, REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION);
		createEReference(reversePivotAttributeAssignmentEClass, REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE);

		setAttributeAssignmentEClass = createEClass(SET_ATTRIBUTE_ASSIGNMENT);
		createEReference(setAttributeAssignmentEClass, SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE);
		createEReference(setAttributeAssignmentEClass, SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE);

		categoryEClass = createEClass(CATEGORY);
		createEAttribute(categoryEClass, CATEGORY__VALUE);
		createEAttribute(categoryEClass, CATEGORY__IS_NULL_CATEGORY);
		createEAttribute(categoryEClass, CATEGORY__DISPLAY_NAME);
		createEAttribute(categoryEClass, CATEGORY__PROPERTY);
		createEAttribute(categoryEClass, CATEGORY__PRIOR);
		createEReference(categoryEClass, CATEGORY__CATEGORICAL_PROPERTIES);

		categoryMapEClass = createEClass(CATEGORY_MAP);
		createEAttribute(categoryMapEClass, CATEGORY_MAP__IS_MULTI_LEVEL);
		createEAttribute(categoryMapEClass, CATEGORY_MAP__IS_ITEM_MAP);
		createEReference(categoryMapEClass, CATEGORY_MAP__TAXONOMY);

		categoryMapObjectEClass = createEClass(CATEGORY_MAP_OBJECT);
		createEReference(categoryMapObjectEClass, CATEGORY_MAP_OBJECT__ENTRY);

		categoryMapObjectEntryEClass = createEClass(CATEGORY_MAP_OBJECT_ENTRY);
		createEAttribute(categoryMapObjectEntryEClass, CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID);
		createEReference(categoryMapObjectEntryEClass, CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT);
		createEReference(categoryMapObjectEntryEClass, CATEGORY_MAP_OBJECT_ENTRY__CHILD);
		createEReference(categoryMapObjectEntryEClass, CATEGORY_MAP_OBJECT_ENTRY__PARENT);

		categoryMapTableEClass = createEClass(CATEGORY_MAP_TABLE);
		createEReference(categoryMapTableEClass, CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE);
		createEReference(categoryMapTableEClass, CATEGORY_MAP_TABLE__TABLE);
		createEReference(categoryMapTableEClass, CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE);
		createEReference(categoryMapTableEClass, CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE);

		categoryMatrixEClass = createEClass(CATEGORY_MATRIX);
		createEAttribute(categoryMatrixEClass, CATEGORY_MATRIX__DIAGONAL_DEFAULT);
		createEAttribute(categoryMatrixEClass, CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT);
		createEAttribute(categoryMatrixEClass, CATEGORY_MATRIX__KIND);
		createEReference(categoryMatrixEClass, CATEGORY_MATRIX__CATEGORY);
		createEReference(categoryMatrixEClass, CATEGORY_MATRIX__SCHEMA);
		createEReference(categoryMatrixEClass, CATEGORY_MATRIX__TEST_RESULT);

		categoryMatrixEntryEClass = createEClass(CATEGORY_MATRIX_ENTRY);
		createEAttribute(categoryMatrixEntryEClass, CATEGORY_MATRIX_ENTRY__VALUE);
		createEReference(categoryMatrixEntryEClass, CATEGORY_MATRIX_ENTRY__COLUMN_INDEX);
		createEReference(categoryMatrixEntryEClass, CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX);
		createEReference(categoryMatrixEntryEClass, CATEGORY_MATRIX_ENTRY__ROW_INDEX);

		categoryMatrixObjectEClass = createEClass(CATEGORY_MATRIX_OBJECT);
		createEReference(categoryMatrixObjectEClass, CATEGORY_MATRIX_OBJECT__ENTRY);

		categoryMatrixTableEClass = createEClass(CATEGORY_MATRIX_TABLE);
		createEReference(categoryMatrixTableEClass, CATEGORY_MATRIX_TABLE__SOURCE);
		createEReference(categoryMatrixTableEClass, CATEGORY_MATRIX_TABLE__COLUMN_ATTRIBUTE);
		createEReference(categoryMatrixTableEClass, CATEGORY_MATRIX_TABLE__ROW_ATTRIBUTE);
		createEReference(categoryMatrixTableEClass, CATEGORY_MATRIX_TABLE__VALUE_ATTRIBUTE);

		categoryTaxonomyEClass = createEClass(CATEGORY_TAXONOMY);
		createEReference(categoryTaxonomyEClass, CATEGORY_TAXONOMY__CATEGORY_MAP);
		createEReference(categoryTaxonomyEClass, CATEGORY_TAXONOMY__ROOT_CATEGORY);
		createEReference(categoryTaxonomyEClass, CATEGORY_TAXONOMY__SCHEMA);

		// Create enums
		attributeSelectionFunctionEEnum = createEEnum(ATTRIBUTE_SELECTION_FUNCTION);
		attributeTypeEEnum = createEEnum(ATTRIBUTE_TYPE);
		orderTypeEEnum = createEEnum(ORDER_TYPE);
		usageOptionEEnum = createEEnum(USAGE_OPTION);
		valueSelectionFunctionEEnum = createEEnum(VALUE_SELECTION_FUNCTION);
		categoryPropertyEEnum = createEEnum(CATEGORY_PROPERTY);
		matrixPropertyEEnum = createEEnum(MATRIX_PROPERTY);

		// Create data types
		doubleEDataType = createEDataType(DOUBLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		EntrypointPackage theEntrypointPackage = (EntrypointPackage)EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI);
		MiningfunctionsettingsPackage theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackage)EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI);
		ClassificationPackage theClassificationPackage = (ClassificationPackage)EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		attributeAssignmentEClass.getESuperTypes().add(theCorePackage.getModelElement());
		attributeAssignmentSetEClass.getESuperTypes().add(theCorePackage.getModelElement());
		attributeUsageEClass.getESuperTypes().add(theCorePackage.getFeature());
		attributeUsageSetEClass.getESuperTypes().add(theCorePackage.getClass_());
		categoricalAttributePropertiesEClass.getESuperTypes().add(theCorePackage.getModelElement());
		directAttributeAssignmentEClass.getESuperTypes().add(this.getAttributeAssignment());
		logicalAttributeEClass.getESuperTypes().add(this.getMiningAttribute());
		logicalDataEClass.getESuperTypes().add(theCorePackage.getClass_());
		miningAttributeEClass.getESuperTypes().add(theCorePackage.getAttribute());
		numericalAttributePropertiesEClass.getESuperTypes().add(theCorePackage.getModelElement());
		ordinalAttributePropertiesEClass.getESuperTypes().add(this.getCategoricalAttributeProperties());
		physicalDataEClass.getESuperTypes().add(theCorePackage.getModelElement());
		pivotAttributeAssignmentEClass.getESuperTypes().add(this.getAttributeAssignment());
		reversePivotAttributeAssignmentEClass.getESuperTypes().add(this.getAttributeAssignment());
		setAttributeAssignmentEClass.getESuperTypes().add(this.getAttributeAssignment());
		categoryEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMapEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMapObjectEClass.getESuperTypes().add(this.getCategoryMap());
		categoryMapObjectEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMapObjectEntryEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMapTableEClass.getESuperTypes().add(this.getCategoryMap());
		categoryMapTableEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMatrixEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMatrixEntryEClass.getESuperTypes().add(theCorePackage.getModelElement());
		categoryMatrixObjectEClass.getESuperTypes().add(this.getCategoryMatrix());
		categoryMatrixTableEClass.getESuperTypes().add(this.getCategoryMatrix());
		categoryTaxonomyEClass.getESuperTypes().add(theCorePackage.getModelElement());

		// Initialize classes and features; add operations and parameters
		initEClass(attributeAssignmentEClass, AttributeAssignment.class, "AttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeAssignment_LogicalAttribute(), this.getMiningAttribute(), null, "logicalAttribute", null, 1, -1, AttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttributeAssignment_OrderIdAttribute(), theCorePackage.getAttribute(), null, "orderIdAttribute", null, 0, -1, AttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeAssignmentSetEClass, AttributeAssignmentSet.class, "AttributeAssignmentSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeAssignmentSet_AuxiliaryObject(), theEntrypointPackage.getAuxiliaryObject(), theEntrypointPackage.getAuxiliaryObject_AttributeAssignmentSet(), "auxiliaryObject", null, 1, 1, AttributeAssignmentSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeUsageEClass, AttributeUsage.class, "AttributeUsage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttributeUsage_Usage(), this.getUsageOption(), "usage", "active", 0, 1, AttributeUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttributeUsage_Weight(), this.getDouble(), "weight", "1.0", 0, 1, AttributeUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttributeUsage_SuppressDiscretization(), theCorePackage.getBoolean(), "suppressDiscretization", "false", 0, 1, AttributeUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttributeUsage_SuppressNormalization(), theCorePackage.getBoolean(), "suppressNormalization", "false", 0, 1, AttributeUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttributeUsage_Attribute(), this.getLogicalAttribute(), null, "attribute", null, 1, -1, AttributeUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeUsageSetEClass, AttributeUsageSet.class, "AttributeUsageSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeUsageSet_Settings(), theMiningfunctionsettingsPackage.getMiningFunctionSettings(), theMiningfunctionsettingsPackage.getMiningFunctionSettings_AttributeUsageSet(), "settings", null, 1, 1, AttributeUsageSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoricalAttributePropertiesEClass, CategoricalAttributeProperties.class, "CategoricalAttributeProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoricalAttributeProperties_LogicalAttribute(), this.getLogicalAttribute(), this.getLogicalAttribute_CategoricalProperties(), "logicalAttribute", null, 1, 1, CategoricalAttributeProperties.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoricalAttributeProperties_Taxonomy(), this.getCategoryTaxonomy(), null, "taxonomy", null, 0, 1, CategoricalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoricalAttributeProperties_Category(), this.getCategory(), this.getCategory_CategoricalProperties(), "category", null, 0, -1, CategoricalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(directAttributeAssignmentEClass, DirectAttributeAssignment.class, "DirectAttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirectAttributeAssignment_Attribute(), theCorePackage.getAttribute(), null, "attribute", null, 1, 1, DirectAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(logicalAttributeEClass, LogicalAttribute.class, "LogicalAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLogicalAttribute_IsSetValued(), theCorePackage.getBoolean(), "isSetValued", "false", 0, 1, LogicalAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLogicalAttribute_CategoricalProperties(), this.getCategoricalAttributeProperties(), this.getCategoricalAttributeProperties_LogicalAttribute(), "categoricalProperties", null, 0, 1, LogicalAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLogicalAttribute_NumericalProperties(), this.getNumericalAttributeProperties(), this.getNumericalAttributeProperties_LogicalAttribute(), "numericalProperties", null, 0, 1, LogicalAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(logicalDataEClass, LogicalData.class, "LogicalData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLogicalData_Schema(), theEntrypointPackage.getSchema(), theEntrypointPackage.getSchema_LogicalData(), "schema", null, 1, 1, LogicalData.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miningAttributeEClass, MiningAttribute.class, "MiningAttribute", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMiningAttribute_DisplayName(), theCorePackage.getString(), "displayName", null, 0, 1, MiningAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMiningAttribute_AttributeType(), this.getAttributeType(), "attributeType", null, 0, 1, MiningAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(numericalAttributePropertiesEClass, NumericalAttributeProperties.class, "NumericalAttributeProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNumericalAttributeProperties_LowerBound(), this.getDouble(), "lowerBound", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_UpperBound(), this.getDouble(), "upperBound", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_IsDiscrete(), theCorePackage.getBoolean(), "isDiscrete", "false", 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_IsCyclic(), theCorePackage.getBoolean(), "isCyclic", "false", 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_Anchor(), this.getDouble(), "anchor", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_CycleBegin(), this.getDouble(), "cycleBegin", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_CycleEnd(), this.getDouble(), "cycleEnd", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumericalAttributeProperties_DiscreteStepSize(), this.getDouble(), "discreteStepSize", null, 0, 1, NumericalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNumericalAttributeProperties_LogicalAttribute(), this.getLogicalAttribute(), this.getLogicalAttribute_NumericalProperties(), "logicalAttribute", null, 1, 1, NumericalAttributeProperties.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ordinalAttributePropertiesEClass, OrdinalAttributeProperties.class, "OrdinalAttributeProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOrdinalAttributeProperties_OrderType(), this.getOrderType(), "orderType", null, 0, 1, OrdinalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOrdinalAttributeProperties_IsCyclic(), theCorePackage.getBoolean(), "isCyclic", "false", 0, 1, OrdinalAttributeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalDataEClass, PhysicalData.class, "PhysicalData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalData_Source(), theCorePackage.getClass_(), null, "source", null, 1, 1, PhysicalData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pivotAttributeAssignmentEClass, PivotAttributeAssignment.class, "PivotAttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPivotAttributeAssignment_SetIdAttribute(), theCorePackage.getAttribute(), null, "setIdAttribute", null, 1, 1, PivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPivotAttributeAssignment_NameAttribute(), theCorePackage.getAttribute(), null, "nameAttribute", null, 1, 1, PivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPivotAttributeAssignment_ValueAttribute(), theCorePackage.getAttribute(), null, "valueAttribute", null, 1, 1, PivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reversePivotAttributeAssignmentEClass, ReversePivotAttributeAssignment.class, "ReversePivotAttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReversePivotAttributeAssignment_AttributeSelectionFunction(), this.getAttributeSelectionFunction(), "attributeSelectionFunction", null, 0, 1, ReversePivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReversePivotAttributeAssignment_ValueSelectionFunction(), this.getValueSelectionFunction(), "valueSelectionFunction", null, 0, 1, ReversePivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReversePivotAttributeAssignment_SelectorAttribute(), theCorePackage.getAttribute(), null, "selectorAttribute", null, 1, -1, ReversePivotAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(setAttributeAssignmentEClass, SetAttributeAssignment.class, "SetAttributeAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSetAttributeAssignment_SetIdAttribute(), theCorePackage.getAttribute(), null, "setIdAttribute", null, 1, 1, SetAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSetAttributeAssignment_MemberAttribute(), theCorePackage.getAttribute(), null, "memberAttribute", null, 1, 1, SetAttributeAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryEClass, Category.class, "Category", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategory_Value(), theCorePackage.getAny(), "value", null, 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategory_IsNullCategory(), theCorePackage.getBoolean(), "isNullCategory", "false", 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategory_DisplayName(), theCorePackage.getString(), "displayName", null, 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategory_Property(), this.getCategoryProperty(), "property", "valid", 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategory_Prior(), this.getDouble(), "prior", null, 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategory_CategoricalProperties(), this.getCategoricalAttributeProperties(), this.getCategoricalAttributeProperties_Category(), "categoricalProperties", null, 1, 1, Category.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMapEClass, CategoryMap.class, "CategoryMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryMap_IsMultiLevel(), theCorePackage.getBoolean(), "isMultiLevel", "false", 0, 1, CategoryMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryMap_IsItemMap(), theCorePackage.getBoolean(), "isItemMap", "false", 0, 1, CategoryMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMap_Taxonomy(), this.getCategoryTaxonomy(), this.getCategoryTaxonomy_CategoryMap(), "taxonomy", null, 0, 1, CategoryMap.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMapObjectEClass, CategoryMapObject.class, "CategoryMapObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryMapObject_Entry(), this.getCategoryMapObjectEntry(), this.getCategoryMapObjectEntry_MapObject(), "entry", null, 0, -1, CategoryMapObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMapObjectEntryEClass, CategoryMapObjectEntry.class, "CategoryMapObjectEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryMapObjectEntry_GraphId(), theCorePackage.getAny(), "graphId", null, 0, 1, CategoryMapObjectEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapObjectEntry_MapObject(), this.getCategoryMapObject(), this.getCategoryMapObject_Entry(), "mapObject", null, 1, 1, CategoryMapObjectEntry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapObjectEntry_Child(), this.getCategory(), null, "child", null, 1, 1, CategoryMapObjectEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapObjectEntry_Parent(), this.getCategory(), null, "parent", null, 1, -1, CategoryMapObjectEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMapTableEClass, CategoryMapTable.class, "CategoryMapTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryMapTable_ChildAttribute(), theCorePackage.getAttribute(), null, "childAttribute", null, 1, 1, CategoryMapTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapTable_Table(), theCorePackage.getClass_(), null, "table", null, 1, 1, CategoryMapTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapTable_GraphIdAttribute(), theCorePackage.getAttribute(), null, "graphIdAttribute", null, 0, 1, CategoryMapTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMapTable_ParentAttribute(), theCorePackage.getAttribute(), null, "parentAttribute", null, 1, 1, CategoryMapTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMatrixEClass, CategoryMatrix.class, "CategoryMatrix", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryMatrix_DiagonalDefault(), this.getDouble(), "diagonalDefault", "1.0", 0, 1, CategoryMatrix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryMatrix_OffDiagonalDefault(), this.getDouble(), "offDiagonalDefault", "0.0", 0, 1, CategoryMatrix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryMatrix_Kind(), this.getMatrixProperty(), "kind", "MatrixProperty.any", 0, 1, CategoryMatrix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrix_Category(), this.getCategory(), null, "category", null, 2, -1, CategoryMatrix.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrix_Schema(), theEntrypointPackage.getSchema(), theEntrypointPackage.getSchema_CategoryMatrix(), "schema", null, 1, 1, CategoryMatrix.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrix_TestResult(), theClassificationPackage.getClassificationTestResult(), theClassificationPackage.getClassificationTestResult_ConfusionMatrix(), "testResult", null, 1, 1, CategoryMatrix.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMatrixEntryEClass, CategoryMatrixEntry.class, "CategoryMatrixEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryMatrixEntry_Value(), this.getDouble(), "value", null, 0, 1, CategoryMatrixEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixEntry_ColumnIndex(), this.getCategory(), null, "columnIndex", null, 1, 1, CategoryMatrixEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixEntry_CategoryMatrix(), this.getCategoryMatrixObject(), this.getCategoryMatrixObject_Entry(), "categoryMatrix", null, 1, 1, CategoryMatrixEntry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixEntry_RowIndex(), this.getCategory(), null, "rowIndex", null, 1, 1, CategoryMatrixEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMatrixObjectEClass, CategoryMatrixObject.class, "CategoryMatrixObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryMatrixObject_Entry(), this.getCategoryMatrixEntry(), this.getCategoryMatrixEntry_CategoryMatrix(), "entry", null, 0, -1, CategoryMatrixObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryMatrixTableEClass, CategoryMatrixTable.class, "CategoryMatrixTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryMatrixTable_Source(), theCorePackage.getClass_(), null, "source", null, 0, -1, CategoryMatrixTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixTable_ColumnAttribute(), theCorePackage.getAttribute(), null, "columnAttribute", null, 1, 1, CategoryMatrixTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixTable_RowAttribute(), theCorePackage.getAttribute(), null, "rowAttribute", null, 1, 1, CategoryMatrixTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryMatrixTable_ValueAttribute(), theCorePackage.getAttribute(), null, "valueAttribute", null, 1, 1, CategoryMatrixTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryTaxonomyEClass, CategoryTaxonomy.class, "CategoryTaxonomy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryTaxonomy_CategoryMap(), this.getCategoryMap(), this.getCategoryMap_Taxonomy(), "categoryMap", null, 0, -1, CategoryTaxonomy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryTaxonomy_RootCategory(), this.getCategory(), null, "rootCategory", null, 0, -1, CategoryTaxonomy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryTaxonomy_Schema(), theEntrypointPackage.getSchema(), theEntrypointPackage.getSchema_Taxonomy(), "schema", null, 1, 1, CategoryTaxonomy.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(attributeSelectionFunctionEEnum, AttributeSelectionFunction.class, "AttributeSelectionFunction");
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_NOT_NULL_LITERAL);
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_NULL_LITERAL);
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_ONE_LITERAL);
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_ZERO_LITERAL);
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_TRUE_LITERAL);
		addEEnumLiteral(attributeSelectionFunctionEEnum, AttributeSelectionFunction.IS_FALSE_LITERAL);

		initEEnum(attributeTypeEEnum, AttributeType.class, "AttributeType");
		addEEnumLiteral(attributeTypeEEnum, AttributeType.CATEGORICAL_LITERAL);
		addEEnumLiteral(attributeTypeEEnum, AttributeType.ORDINAL_LITERAL);
		addEEnumLiteral(attributeTypeEEnum, AttributeType.NUMERICAL_LITERAL);
		addEEnumLiteral(attributeTypeEEnum, AttributeType.NOT_SPECIFIED_LITERAL);

		initEEnum(orderTypeEEnum, OrderType.class, "OrderType");
		addEEnumLiteral(orderTypeEEnum, OrderType.AS_IS_LITERAL);
		addEEnumLiteral(orderTypeEEnum, OrderType.ALPHABETICAL_LITERAL);
		addEEnumLiteral(orderTypeEEnum, OrderType.NUMERIC_LITERAL);
		addEEnumLiteral(orderTypeEEnum, OrderType.DATE_LITERAL);

		initEEnum(usageOptionEEnum, UsageOption.class, "UsageOption");
		addEEnumLiteral(usageOptionEEnum, UsageOption.ACTIVE_LITERAL);
		addEEnumLiteral(usageOptionEEnum, UsageOption.SUPPLEMENTARY_LITERAL);
		addEEnumLiteral(usageOptionEEnum, UsageOption.TARGET_LITERAL);

		initEEnum(valueSelectionFunctionEEnum, ValueSelectionFunction.class, "ValueSelectionFunction");
		addEEnumLiteral(valueSelectionFunctionEEnum, ValueSelectionFunction.VSF_VALUE_LITERAL);
		addEEnumLiteral(valueSelectionFunctionEEnum, ValueSelectionFunction.VSF_ATTRIBUTE_LITERAL);

		initEEnum(categoryPropertyEEnum, CategoryProperty.class, "CategoryProperty");
		addEEnumLiteral(categoryPropertyEEnum, CategoryProperty.VALID_LITERAL);
		addEEnumLiteral(categoryPropertyEEnum, CategoryProperty.INVALID_LITERAL);
		addEEnumLiteral(categoryPropertyEEnum, CategoryProperty.MISSING_LITERAL);

		initEEnum(matrixPropertyEEnum, MatrixProperty.class, "MatrixProperty");
		addEEnumLiteral(matrixPropertyEEnum, MatrixProperty.MP_SYMMETRIC_LITERAL);
		addEEnumLiteral(matrixPropertyEEnum, MatrixProperty.MP_DIAGONAL_LITERAL);
		addEEnumLiteral(matrixPropertyEEnum, MatrixProperty.MP_ANY_LITERAL);

		// Initialize data types
		initEDataType(doubleEDataType, String.class, "Double", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //MiningdataPackageImpl
