/**
 * INRIA/IRISA
 *
 * $Id: OrdinalAttributeProperties.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ordinal Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An OrdinalAttributeProperties object is used to describe properties of the ordinal attribute.  Ordinal attributes can use the "ordered" constraint on the MiningCategory class to use the "asIs" OrderType. The "asIs" allows the list ordering to imply a "less than" relationship between categories N and N+1. In addition, ordinals may be cyclic , e.g., days of the week.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#getOrderType <em>Order Type</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getOrdinalAttributeProperties()
 * @model
 * @generated
 */
public interface OrdinalAttributeProperties extends CategoricalAttributeProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Order Type</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * OrderType indicates how the sequence of categories should be interpreted as ordinal (potentially mapped to integers). 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Order Type</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType
	 * @see #setOrderType(OrderType)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getOrdinalAttributeProperties_OrderType()
	 * @model
	 * @generated
	 */
	OrderType getOrderType();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#getOrderType <em>Order Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order Type</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType
	 * @see #getOrderType()
	 * @generated
	 */
	void setOrderType(OrderType value);

	/**
	 * Returns the value of the '<em><b>Is Cyclic</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates whether the values of the attributes are cyclic, i.e., the next value of the ending value is the starting value.
	 * The default is false.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Cyclic</em>' attribute.
	 * @see #setIsCyclic(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getOrdinalAttributeProperties_IsCyclic()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsCyclic();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Cyclic</em>' attribute.
	 * @see #isIsCyclic()
	 * @generated
	 */
	void setIsCyclic(boolean value);

} // OrdinalAttributeProperties
