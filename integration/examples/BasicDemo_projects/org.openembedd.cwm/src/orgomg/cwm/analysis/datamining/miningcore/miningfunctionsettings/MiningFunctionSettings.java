/**
 * INRIA/IRISA
 *
 * $Id: MiningFunctionSettings.java,v 1.1 2008-04-01 09:35:51 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A MiningFunctionSettings object captures the high level specification input for building a data mining model. The intent of mining function settings, is to allow a user to specify the type of result desired without having to specify a particular algorithm. 
 * 
 * Although mining function settings allow for the specification of algorithm, if this is omitted, the underlying data mining system is responsible for selecting the algorithm based on basic user-provided parameters.
 * 
 * Subclasses throw exceptions if invalid algorithm-function pairs are supplied.
 * 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getDesiredExecutionTimeInMinutes <em>Desired Execution Time In Minutes</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAlgorithmSettings <em>Algorithm Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAttributeUsageSet <em>Attribute Usage Set</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getLogicalData <em>Logical Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings()
 * @model abstract="true"
 * @generated
 */
public interface MiningFunctionSettings extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The desiredExecutionTime attribute indicates the maximum execution time (in minutes) allowed for model building.  If NULL, the algrotihm determines for how long the model will build.
	 * 
	 * This is to serve as a hint to the algorithm to adjust model building to meet the time constraint. Vendor implementations may support this to varying degrees, e.g., terminate model build if exceeds this limit, intelligently adjust algorithm parameters to meet this constraint, or dynamically distribute or parallelize the operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Desired Execution Time In Minutes</em>' attribute.
	 * @see #setDesiredExecutionTimeInMinutes(long)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings_DesiredExecutionTimeInMinutes()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getDesiredExecutionTimeInMinutes();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getDesiredExecutionTimeInMinutes <em>Desired Execution Time In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Desired Execution Time In Minutes</em>' attribute.
	 * @see #getDesiredExecutionTimeInMinutes()
	 * @generated
	 */
	void setDesiredExecutionTimeInMinutes(long value);

	/**
	 * Returns the value of the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Algorithm Settings</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Algorithm Settings</em>' reference.
	 * @see #setAlgorithmSettings(MiningAlgorithmSettings)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings_AlgorithmSettings()
	 * @model
	 * @generated
	 */
	MiningAlgorithmSettings getAlgorithmSettings();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAlgorithmSettings <em>Algorithm Settings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Algorithm Settings</em>' reference.
	 * @see #getAlgorithmSettings()
	 * @generated
	 */
	void setAlgorithmSettings(MiningAlgorithmSettings value);

	/**
	 * Returns the value of the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Usage Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Usage Set</em>' containment reference.
	 * @see #setAttributeUsageSet(AttributeUsageSet)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings_AttributeUsageSet()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings
	 * @model opposite="settings" containment="true"
	 * @generated
	 */
	AttributeUsageSet getAttributeUsageSet();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAttributeUsageSet <em>Attribute Usage Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Usage Set</em>' containment reference.
	 * @see #getAttributeUsageSet()
	 * @generated
	 */
	void setAttributeUsageSet(AttributeUsageSet value);

	/**
	 * Returns the value of the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Data</em>' reference.
	 * @see #setLogicalData(LogicalData)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings_LogicalData()
	 * @model required="true"
	 * @generated
	 */
	LogicalData getLogicalData();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getLogicalData <em>Logical Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical Data</em>' reference.
	 * @see #getLogicalData()
	 * @generated
	 */
	void setLogicalData(LogicalData value);

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningFunctionSettings <em>Mining Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningFunctionSettings_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningFunctionSettings
	 * @model opposite="miningFunctionSettings" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

} // MiningFunctionSettings
