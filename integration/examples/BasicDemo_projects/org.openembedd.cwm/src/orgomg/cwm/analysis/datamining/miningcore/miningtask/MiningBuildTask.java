/**
 * INRIA/IRISA
 *
 * $Id: MiningBuildTask.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Build Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This describes a task that builds a mining model, sometimes also called training task.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getResultModel <em>Result Model</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getMiningSettings <em>Mining Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationAssignment <em>Validation Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getSettingsAssignment <em>Settings Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationData <em>Validation Data</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask()
 * @model
 * @generated
 */
public interface MiningBuildTask extends MiningTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Result Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Model</em>' reference.
	 * @see #setResultModel(MiningModel)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask_ResultModel()
	 * @model
	 * @generated
	 */
	MiningModel getResultModel();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getResultModel <em>Result Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Model</em>' reference.
	 * @see #getResultModel()
	 * @generated
	 */
	void setResultModel(MiningModel value);

	/**
	 * Returns the value of the '<em><b>Mining Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mining Settings</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mining Settings</em>' reference.
	 * @see #setMiningSettings(MiningFunctionSettings)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask_MiningSettings()
	 * @model required="true"
	 * @generated
	 */
	MiningFunctionSettings getMiningSettings();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getMiningSettings <em>Mining Settings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mining Settings</em>' reference.
	 * @see #getMiningSettings()
	 * @generated
	 */
	void setMiningSettings(MiningFunctionSettings value);

	/**
	 * Returns the value of the '<em><b>Validation Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Assignment</em>' reference.
	 * @see #setValidationAssignment(AttributeAssignmentSet)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask_ValidationAssignment()
	 * @model
	 * @generated
	 */
	AttributeAssignmentSet getValidationAssignment();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationAssignment <em>Validation Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Assignment</em>' reference.
	 * @see #getValidationAssignment()
	 * @generated
	 */
	void setValidationAssignment(AttributeAssignmentSet value);

	/**
	 * Returns the value of the '<em><b>Settings Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Settings Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Settings Assignment</em>' reference.
	 * @see #setSettingsAssignment(AttributeAssignmentSet)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask_SettingsAssignment()
	 * @model
	 * @generated
	 */
	AttributeAssignmentSet getSettingsAssignment();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getSettingsAssignment <em>Settings Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Settings Assignment</em>' reference.
	 * @see #getSettingsAssignment()
	 * @generated
	 */
	void setSettingsAssignment(AttributeAssignmentSet value);

	/**
	 * Returns the value of the '<em><b>Validation Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Data</em>' reference.
	 * @see #setValidationData(PhysicalData)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningBuildTask_ValidationData()
	 * @model
	 * @generated
	 */
	PhysicalData getValidationData();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationData <em>Validation Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validation Data</em>' reference.
	 * @see #getValidationData()
	 * @generated
	 */
	void setValidationData(PhysicalData value);

} // MiningBuildTask
