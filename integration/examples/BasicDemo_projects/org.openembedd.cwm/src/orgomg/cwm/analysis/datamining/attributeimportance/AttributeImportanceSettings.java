/**
 * INRIA/IRISA
 *
 * $Id: AttributeImportanceSettings.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.attributeimportance;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Importance Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is a subclass of MiningFunctionSettings that supports features unique to attribute importance identification, as known as feature selection.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#getMaximumResultSize <em>Maximum Result Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#isReturnTop <em>Return Top</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage#getAttributeImportanceSettings()
 * @model
 * @generated
 */
public interface AttributeImportanceSettings extends SupervisedFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Maximum Result Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The attribute maximumResultSize indicates to return the top N most important attributes. It may return fewer.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Maximum Result Size</em>' attribute.
	 * @see #setMaximumResultSize(long)
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage#getAttributeImportanceSettings_MaximumResultSize()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getMaximumResultSize();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#getMaximumResultSize <em>Maximum Result Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Result Size</em>' attribute.
	 * @see #getMaximumResultSize()
	 * @generated
	 */
	void setMaximumResultSize(long value);

	/**
	 * Returns the value of the '<em><b>Return Top</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If true, returns the most important attributes. If false, the least important.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Return Top</em>' attribute.
	 * @see #setReturnTop(boolean)
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage#getAttributeImportanceSettings_ReturnTop()
	 * @model default="true" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isReturnTop();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#isReturnTop <em>Return Top</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Top</em>' attribute.
	 * @see #isReturnTop()
	 * @generated
	 */
	void setReturnTop(boolean value);

} // AttributeImportanceSettings
