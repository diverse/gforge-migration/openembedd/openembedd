/**
 * INRIA/IRISA
 *
 * $Id: ApplyScoreItemImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyScoreItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apply Score Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ApplyScoreItemImpl extends ApplyContentItemImpl implements ApplyScoreItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplyScoreItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.APPLY_SCORE_ITEM;
	}

} //ApplyScoreItemImpl
