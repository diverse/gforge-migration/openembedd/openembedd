/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixObject.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Matrix Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The object representation of CategoryMatrix. Each object references a set of CategoryMatrixEntry objects.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixObject()
 * @model
 * @generated
 */
public interface CategoryMatrixObject extends CategoryMatrix {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix <em>Category Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixObject_Entry()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix
	 * @model opposite="categoryMatrix" containment="true"
	 * @generated
	 */
	EList<CategoryMatrixEntry> getEntry();

} // CategoryMatrixObject
