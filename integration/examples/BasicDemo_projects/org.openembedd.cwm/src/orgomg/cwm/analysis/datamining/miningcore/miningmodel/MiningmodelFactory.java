/**
 * INRIA/IRISA
 *
 * $Id: MiningmodelFactory.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage
 * @generated
 */
public interface MiningmodelFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MiningmodelFactory eINSTANCE = orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.MiningmodelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Mining Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mining Model</em>'.
	 * @generated
	 */
	MiningModel createMiningModel();

	/**
	 * Returns a new object of class '<em>Model Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Signature</em>'.
	 * @generated
	 */
	ModelSignature createModelSignature();

	/**
	 * Returns a new object of class '<em>Signature Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signature Attribute</em>'.
	 * @generated
	 */
	SignatureAttribute createSignatureAttribute();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MiningmodelPackage getMiningmodelPackage();

} //MiningmodelFactory
