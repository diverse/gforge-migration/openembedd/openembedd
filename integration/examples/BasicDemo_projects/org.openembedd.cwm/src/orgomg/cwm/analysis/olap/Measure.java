/**
 * INRIA/IRISA
 *
 * $Id: Measure.java,v 1.1 2008-04-01 09:35:30 vmahe Exp $
 */
package orgomg.cwm.analysis.olap;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Measure is a subclass of Attribute representing Dimension Measures (e.g., Sales, Quantity, Weight).  Synonym: Variable.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.olap.OlapPackage#getMeasure()
 * @model
 * @generated
 */
public interface Measure extends Attribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Measure
