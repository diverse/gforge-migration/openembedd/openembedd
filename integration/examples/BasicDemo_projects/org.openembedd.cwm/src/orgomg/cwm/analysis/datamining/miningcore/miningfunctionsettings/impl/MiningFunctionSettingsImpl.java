/**
 * INRIA/IRISA
 *
 * $Id: MiningFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningAlgorithmSettings;
import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;
import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl#getDesiredExecutionTimeInMinutes <em>Desired Execution Time In Minutes</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl#getAlgorithmSettings <em>Algorithm Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl#getAttributeUsageSet <em>Attribute Usage Set</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl#getLogicalData <em>Logical Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class MiningFunctionSettingsImpl extends ModelElementImpl implements MiningFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getDesiredExecutionTimeInMinutes() <em>Desired Execution Time In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesiredExecutionTimeInMinutes()
	 * @generated
	 * @ordered
	 */
	protected static final long DESIRED_EXECUTION_TIME_IN_MINUTES_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDesiredExecutionTimeInMinutes() <em>Desired Execution Time In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesiredExecutionTimeInMinutes()
	 * @generated
	 * @ordered
	 */
	protected long desiredExecutionTimeInMinutes = DESIRED_EXECUTION_TIME_IN_MINUTES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAlgorithmSettings() <em>Algorithm Settings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlgorithmSettings()
	 * @generated
	 * @ordered
	 */
	protected MiningAlgorithmSettings algorithmSettings;

	/**
	 * The cached value of the '{@link #getAttributeUsageSet() <em>Attribute Usage Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeUsageSet()
	 * @generated
	 * @ordered
	 */
	protected AttributeUsageSet attributeUsageSet;

	/**
	 * The cached value of the '{@link #getLogicalData() <em>Logical Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalData()
	 * @generated
	 * @ordered
	 */
	protected LogicalData logicalData;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningfunctionsettingsPackage.Literals.MINING_FUNCTION_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDesiredExecutionTimeInMinutes() {
		return desiredExecutionTimeInMinutes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDesiredExecutionTimeInMinutes(long newDesiredExecutionTimeInMinutes) {
		long oldDesiredExecutionTimeInMinutes = desiredExecutionTimeInMinutes;
		desiredExecutionTimeInMinutes = newDesiredExecutionTimeInMinutes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES, oldDesiredExecutionTimeInMinutes, desiredExecutionTimeInMinutes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningAlgorithmSettings getAlgorithmSettings() {
		if (algorithmSettings != null && algorithmSettings.eIsProxy()) {
			InternalEObject oldAlgorithmSettings = (InternalEObject)algorithmSettings;
			algorithmSettings = (MiningAlgorithmSettings)eResolveProxy(oldAlgorithmSettings);
			if (algorithmSettings != oldAlgorithmSettings) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS, oldAlgorithmSettings, algorithmSettings));
			}
		}
		return algorithmSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningAlgorithmSettings basicGetAlgorithmSettings() {
		return algorithmSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlgorithmSettings(MiningAlgorithmSettings newAlgorithmSettings) {
		MiningAlgorithmSettings oldAlgorithmSettings = algorithmSettings;
		algorithmSettings = newAlgorithmSettings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS, oldAlgorithmSettings, algorithmSettings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeUsageSet getAttributeUsageSet() {
		return attributeUsageSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeUsageSet(AttributeUsageSet newAttributeUsageSet, NotificationChain msgs) {
		AttributeUsageSet oldAttributeUsageSet = attributeUsageSet;
		attributeUsageSet = newAttributeUsageSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET, oldAttributeUsageSet, newAttributeUsageSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeUsageSet(AttributeUsageSet newAttributeUsageSet) {
		if (newAttributeUsageSet != attributeUsageSet) {
			NotificationChain msgs = null;
			if (attributeUsageSet != null)
				msgs = ((InternalEObject)attributeUsageSet).eInverseRemove(this, MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS, AttributeUsageSet.class, msgs);
			if (newAttributeUsageSet != null)
				msgs = ((InternalEObject)newAttributeUsageSet).eInverseAdd(this, MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS, AttributeUsageSet.class, msgs);
			msgs = basicSetAttributeUsageSet(newAttributeUsageSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET, newAttributeUsageSet, newAttributeUsageSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalData getLogicalData() {
		if (logicalData != null && logicalData.eIsProxy()) {
			InternalEObject oldLogicalData = (InternalEObject)logicalData;
			logicalData = (LogicalData)eResolveProxy(oldLogicalData);
			if (logicalData != oldLogicalData) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA, oldLogicalData, logicalData));
			}
		}
		return logicalData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalData basicGetLogicalData() {
		return logicalData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalData(LogicalData newLogicalData) {
		LogicalData oldLogicalData = logicalData;
		logicalData = newLogicalData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA, oldLogicalData, logicalData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema getSchema() {
		if (eContainerFeatureID != MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA) return null;
		return (Schema)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchema(Schema newSchema, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSchema, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchema(Schema newSchema) {
		if (newSchema != eInternalContainer() || (eContainerFeatureID != MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA && newSchema != null)) {
			if (EcoreUtil.isAncestor(this, newSchema))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSchema != null)
				msgs = ((InternalEObject)newSchema).eInverseAdd(this, EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS, Schema.class, msgs);
			msgs = basicSetSchema(newSchema, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA, newSchema, newSchema));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				if (attributeUsageSet != null)
					msgs = ((InternalEObject)attributeUsageSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET, null, msgs);
				return basicSetAttributeUsageSet((AttributeUsageSet)otherEnd, msgs);
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSchema((Schema)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				return basicSetAttributeUsageSet(null, msgs);
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				return basicSetSchema(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS, Schema.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES:
				return new Long(getDesiredExecutionTimeInMinutes());
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS:
				if (resolve) return getAlgorithmSettings();
				return basicGetAlgorithmSettings();
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				return getAttributeUsageSet();
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA:
				if (resolve) return getLogicalData();
				return basicGetLogicalData();
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				return getSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES:
				setDesiredExecutionTimeInMinutes(((Long)newValue).longValue());
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS:
				setAlgorithmSettings((MiningAlgorithmSettings)newValue);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				setAttributeUsageSet((AttributeUsageSet)newValue);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA:
				setLogicalData((LogicalData)newValue);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				setSchema((Schema)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES:
				setDesiredExecutionTimeInMinutes(DESIRED_EXECUTION_TIME_IN_MINUTES_EDEFAULT);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS:
				setAlgorithmSettings((MiningAlgorithmSettings)null);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				setAttributeUsageSet((AttributeUsageSet)null);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA:
				setLogicalData((LogicalData)null);
				return;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				setSchema((Schema)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES:
				return desiredExecutionTimeInMinutes != DESIRED_EXECUTION_TIME_IN_MINUTES_EDEFAULT;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS:
				return algorithmSettings != null;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET:
				return attributeUsageSet != null;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA:
				return logicalData != null;
			case MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA:
				return getSchema() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (desiredExecutionTimeInMinutes: ");
		result.append(desiredExecutionTimeInMinutes);
		result.append(')');
		return result.toString();
	}

} //MiningFunctionSettingsImpl
