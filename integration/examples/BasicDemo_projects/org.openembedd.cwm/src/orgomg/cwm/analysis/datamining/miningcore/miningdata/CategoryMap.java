/**
 * INRIA/IRISA
 *
 * $Id: CategoryMap.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the common superclass of CategoryMapObject and CategoryMapTable supporting the CategorizationGraph class.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsMultiLevel <em>Is Multi Level</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsItemMap <em>Is Item Map</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy <em>Taxonomy</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMap()
 * @model
 * @generated
 */
public interface CategoryMap extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Is Multi Level</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates that this table or object represents multiple levels of the categorization graph, if true.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Multi Level</em>' attribute.
	 * @see #setIsMultiLevel(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMap_IsMultiLevel()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsMultiLevel();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsMultiLevel <em>Is Multi Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Multi Level</em>' attribute.
	 * @see #isIsMultiLevel()
	 * @generated
	 */
	void setIsMultiLevel(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Item Map</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates that this is a grouping of items to categories, if true.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Item Map</em>' attribute.
	 * @see #setIsItemMap(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMap_IsItemMap()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsItemMap();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#isIsItemMap <em>Is Item Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Item Map</em>' attribute.
	 * @see #isIsItemMap()
	 * @generated
	 */
	void setIsItemMap(boolean value);

	/**
	 * Returns the value of the '<em><b>Taxonomy</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getCategoryMap <em>Category Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Taxonomy</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Taxonomy</em>' container reference.
	 * @see #setTaxonomy(CategoryTaxonomy)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMap_Taxonomy()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getCategoryMap
	 * @model opposite="categoryMap"
	 * @generated
	 */
	CategoryTaxonomy getTaxonomy();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy <em>Taxonomy</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Taxonomy</em>' container reference.
	 * @see #getTaxonomy()
	 * @generated
	 */
	void setTaxonomy(CategoryTaxonomy value);

} // CategoryMap
