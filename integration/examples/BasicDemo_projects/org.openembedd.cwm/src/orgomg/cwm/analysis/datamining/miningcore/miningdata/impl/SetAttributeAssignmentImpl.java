/**
 * INRIA/IRISA
 *
 * $Id: SetAttributeAssignmentImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl#getSetIdAttribute <em>Set Id Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.SetAttributeAssignmentImpl#getMemberAttribute <em>Member Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SetAttributeAssignmentImpl extends AttributeAssignmentImpl implements SetAttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getSetIdAttribute() <em>Set Id Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute setIdAttribute;

	/**
	 * The cached value of the '{@link #getMemberAttribute() <em>Member Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemberAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute memberAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetAttributeAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.SET_ATTRIBUTE_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getSetIdAttribute() {
		if (setIdAttribute != null && setIdAttribute.eIsProxy()) {
			InternalEObject oldSetIdAttribute = (InternalEObject)setIdAttribute;
			setIdAttribute = (Attribute)eResolveProxy(oldSetIdAttribute);
			if (setIdAttribute != oldSetIdAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE, oldSetIdAttribute, setIdAttribute));
			}
		}
		return setIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetSetIdAttribute() {
		return setIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSetIdAttribute(Attribute newSetIdAttribute) {
		Attribute oldSetIdAttribute = setIdAttribute;
		setIdAttribute = newSetIdAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE, oldSetIdAttribute, setIdAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getMemberAttribute() {
		if (memberAttribute != null && memberAttribute.eIsProxy()) {
			InternalEObject oldMemberAttribute = (InternalEObject)memberAttribute;
			memberAttribute = (Attribute)eResolveProxy(oldMemberAttribute);
			if (memberAttribute != oldMemberAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE, oldMemberAttribute, memberAttribute));
			}
		}
		return memberAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetMemberAttribute() {
		return memberAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemberAttribute(Attribute newMemberAttribute) {
		Attribute oldMemberAttribute = memberAttribute;
		memberAttribute = newMemberAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE, oldMemberAttribute, memberAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE:
				if (resolve) return getSetIdAttribute();
				return basicGetSetIdAttribute();
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE:
				if (resolve) return getMemberAttribute();
				return basicGetMemberAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE:
				setSetIdAttribute((Attribute)newValue);
				return;
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE:
				setMemberAttribute((Attribute)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE:
				setSetIdAttribute((Attribute)null);
				return;
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE:
				setMemberAttribute((Attribute)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__SET_ID_ATTRIBUTE:
				return setIdAttribute != null;
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT__MEMBER_ATTRIBUTE:
				return memberAttribute != null;
		}
		return super.eIsSet(featureID);
	}

} //SetAttributeAssignmentImpl
