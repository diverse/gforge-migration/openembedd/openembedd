/**
 * INRIA/IRISA
 *
 * $Id: AssociationRulesFunctionSettings.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Rules Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An AssociationRulesFunctionSettings is a subclass of FrequentItemSetFunctionSettings that supports features that are unique to association rules algorithms.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMinimumConfidence <em>Minimum Confidence</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMaximumRuleLength <em>Maximum Rule Length</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getAssociationRulesFunctionSettings()
 * @model
 * @generated
 */
public interface AssociationRulesFunctionSettings extends FrequentItemSetFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Minimum Confidence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the minimum confidence value of each association rule to be found.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Minimum Confidence</em>' attribute.
	 * @see #setMinimumConfidence(String)
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getAssociationRulesFunctionSettings_MinimumConfidence()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getMinimumConfidence();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMinimumConfidence <em>Minimum Confidence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum Confidence</em>' attribute.
	 * @see #getMinimumConfidence()
	 * @generated
	 */
	void setMinimumConfidence(String value);

	/**
	 * Returns the value of the '<em><b>Maximum Rule Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is the maximum length of the antecedent and consequent item set sizes.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Maximum Rule Length</em>' attribute.
	 * @see #setMaximumRuleLength(long)
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getAssociationRulesFunctionSettings_MaximumRuleLength()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getMaximumRuleLength();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMaximumRuleLength <em>Maximum Rule Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Rule Length</em>' attribute.
	 * @see #getMaximumRuleLength()
	 * @generated
	 */
	void setMaximumRuleLength(long value);

} // AssociationRulesFunctionSettings
