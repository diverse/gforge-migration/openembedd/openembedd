/**
 * INRIA/IRISA
 *
 * $Id: ValueSelectionFunction.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Value Selection Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Given an attribute that satisfies the specified attribute selection function, this enumeration determines whether to select value of the attribute or the attribute itself.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getValueSelectionFunction()
 * @model
 * @generated
 */
public enum ValueSelectionFunction implements Enumerator {
	/**
	 * The '<em><b>Vsf value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VSF_VALUE
	 * @generated
	 * @ordered
	 */
	VSF_VALUE_LITERAL(0, "vsf_value", "vsf_value"),

	/**
	 * The '<em><b>Vsf attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VSF_ATTRIBUTE
	 * @generated
	 * @ordered
	 */
	VSF_ATTRIBUTE_LITERAL(1, "vsf_attribute", "vsf_attribute");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Vsf value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows the "value" of the given selector attribute to be chosen in the logical attribute when the value of the attribute meets the AttributeSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #VSF_VALUE_LITERAL
	 * @model name="vsf_value"
	 * @generated
	 * @ordered
	 */
	public static final int VSF_VALUE = 0;

	/**
	 * The '<em><b>Vsf attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Vsf attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VSF_ATTRIBUTE_LITERAL
	 * @model name="vsf_attribute"
	 * @generated
	 * @ordered
	 */
	public static final int VSF_ATTRIBUTE = 1;

	/**
	 * An array of all the '<em><b>Value Selection Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ValueSelectionFunction[] VALUES_ARRAY =
		new ValueSelectionFunction[] {
			VSF_VALUE_LITERAL,
			VSF_ATTRIBUTE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Value Selection Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ValueSelectionFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Value Selection Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueSelectionFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ValueSelectionFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Value Selection Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueSelectionFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ValueSelectionFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Value Selection Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueSelectionFunction get(int value) {
		switch (value) {
			case VSF_VALUE: return VSF_VALUE_LITERAL;
			case VSF_ATTRIBUTE: return VSF_ATTRIBUTE_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ValueSelectionFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ValueSelectionFunction
