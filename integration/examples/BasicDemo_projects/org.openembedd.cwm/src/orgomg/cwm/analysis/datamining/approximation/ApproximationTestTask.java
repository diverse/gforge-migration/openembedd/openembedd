/**
 * INRIA/IRISA
 *
 * $Id: ApproximationTestTask.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation;

import orgomg.cwm.analysis.datamining.supervised.MiningTestTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a task to check the quality of a regression model. A comparison of mean predicted values and mean actual values can be done and a number of numerical error measures can be computed.
 * NULL values mean that the model did not compute the value.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestTask#getTestResult <em>Test Result</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestTask()
 * @model
 * @generated
 */
public interface ApproximationTestTask extends MiningTestTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Test Result</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getTestTask <em>Test Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Result</em>' containment reference.
	 * @see #setTestResult(ApproximationTestResult)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestTask_TestResult()
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getTestTask
	 * @model opposite="testTask" containment="true" required="true"
	 * @generated
	 */
	ApproximationTestResult getTestResult();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestTask#getTestResult <em>Test Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Result</em>' containment reference.
	 * @see #getTestResult()
	 * @generated
	 */
	void setTestResult(ApproximationTestResult value);

} // ApproximationTestTask
