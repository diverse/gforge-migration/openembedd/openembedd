/**
 * INRIA/IRISA
 *
 * $Id: CategoricalAttributePropertiesImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Categorical Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl#getLogicalAttribute <em>Logical Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl#getTaxonomy <em>Taxonomy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoricalAttributePropertiesImpl#getCategory <em>Category</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoricalAttributePropertiesImpl extends ModelElementImpl implements CategoricalAttributeProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getTaxonomy() <em>Taxonomy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaxonomy()
	 * @generated
	 * @ordered
	 */
	protected CategoryTaxonomy taxonomy;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> category;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoricalAttributePropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORICAL_ATTRIBUTE_PROPERTIES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalAttribute getLogicalAttribute() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE) return null;
		return (LogicalAttribute)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLogicalAttribute(LogicalAttribute newLogicalAttribute, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLogicalAttribute, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalAttribute(LogicalAttribute newLogicalAttribute) {
		if (newLogicalAttribute != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE && newLogicalAttribute != null)) {
			if (EcoreUtil.isAncestor(this, newLogicalAttribute))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLogicalAttribute != null)
				msgs = ((InternalEObject)newLogicalAttribute).eInverseAdd(this, MiningdataPackage.LOGICAL_ATTRIBUTE__CATEGORICAL_PROPERTIES, LogicalAttribute.class, msgs);
			msgs = basicSetLogicalAttribute(newLogicalAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE, newLogicalAttribute, newLogicalAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryTaxonomy getTaxonomy() {
		if (taxonomy != null && taxonomy.eIsProxy()) {
			InternalEObject oldTaxonomy = (InternalEObject)taxonomy;
			taxonomy = (CategoryTaxonomy)eResolveProxy(oldTaxonomy);
			if (taxonomy != oldTaxonomy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY, oldTaxonomy, taxonomy));
			}
		}
		return taxonomy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryTaxonomy basicGetTaxonomy() {
		return taxonomy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaxonomy(CategoryTaxonomy newTaxonomy) {
		CategoryTaxonomy oldTaxonomy = taxonomy;
		taxonomy = newTaxonomy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY, oldTaxonomy, taxonomy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getCategory() {
		if (category == null) {
			category = new EObjectContainmentWithInverseEList<Category>(Category.class, this, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY, MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES);
		}
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLogicalAttribute((LogicalAttribute)otherEnd, msgs);
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCategory()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return basicSetLogicalAttribute(null, msgs);
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				return ((InternalEList<?>)getCategory()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.LOGICAL_ATTRIBUTE__CATEGORICAL_PROPERTIES, LogicalAttribute.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return getLogicalAttribute();
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY:
				if (resolve) return getTaxonomy();
				return basicGetTaxonomy();
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				return getCategory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				setLogicalAttribute((LogicalAttribute)newValue);
				return;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY:
				setTaxonomy((CategoryTaxonomy)newValue);
				return;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				getCategory().clear();
				getCategory().addAll((Collection<? extends Category>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				setLogicalAttribute((LogicalAttribute)null);
				return;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY:
				setTaxonomy((CategoryTaxonomy)null);
				return;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				getCategory().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return getLogicalAttribute() != null;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__TAXONOMY:
				return taxonomy != null;
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY:
				return category != null && !category.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CategoricalAttributePropertiesImpl
