/**
 * INRIA/IRISA
 *
 * $Id: ClusteringFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:46 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.clustering.AggregationFunction;
import orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings;
import orgomg.cwm.analysis.datamining.clustering.ClusteringPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl#getMaxNumberOfClusters <em>Max Number Of Clusters</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl#getMinClusterSize <em>Min Cluster Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl#getAggregationFunction <em>Aggregation Function</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClusteringFunctionSettingsImpl extends MiningFunctionSettingsImpl implements ClusteringFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getMaxNumberOfClusters() <em>Max Number Of Clusters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxNumberOfClusters()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_NUMBER_OF_CLUSTERS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaxNumberOfClusters() <em>Max Number Of Clusters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxNumberOfClusters()
	 * @generated
	 * @ordered
	 */
	protected long maxNumberOfClusters = MAX_NUMBER_OF_CLUSTERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinClusterSize() <em>Min Cluster Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinClusterSize()
	 * @generated
	 * @ordered
	 */
	protected static final long MIN_CLUSTER_SIZE_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getMinClusterSize() <em>Min Cluster Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinClusterSize()
	 * @generated
	 * @ordered
	 */
	protected long minClusterSize = MIN_CLUSTER_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAggregationFunction() <em>Aggregation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregationFunction()
	 * @generated
	 * @ordered
	 */
	protected static final AggregationFunction AGGREGATION_FUNCTION_EDEFAULT = AggregationFunction.EUCLIDEAN_LITERAL;

	/**
	 * The cached value of the '{@link #getAggregationFunction() <em>Aggregation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregationFunction()
	 * @generated
	 * @ordered
	 */
	protected AggregationFunction aggregationFunction = AGGREGATION_FUNCTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClusteringFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClusteringPackage.Literals.CLUSTERING_FUNCTION_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaxNumberOfClusters() {
		return maxNumberOfClusters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxNumberOfClusters(long newMaxNumberOfClusters) {
		long oldMaxNumberOfClusters = maxNumberOfClusters;
		maxNumberOfClusters = newMaxNumberOfClusters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS, oldMaxNumberOfClusters, maxNumberOfClusters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMinClusterSize() {
		return minClusterSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinClusterSize(long newMinClusterSize) {
		long oldMinClusterSize = minClusterSize;
		minClusterSize = newMinClusterSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE, oldMinClusterSize, minClusterSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregationFunction getAggregationFunction() {
		return aggregationFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAggregationFunction(AggregationFunction newAggregationFunction) {
		AggregationFunction oldAggregationFunction = aggregationFunction;
		aggregationFunction = newAggregationFunction == null ? AGGREGATION_FUNCTION_EDEFAULT : newAggregationFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION, oldAggregationFunction, aggregationFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS:
				return new Long(getMaxNumberOfClusters());
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE:
				return new Long(getMinClusterSize());
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION:
				return getAggregationFunction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS:
				setMaxNumberOfClusters(((Long)newValue).longValue());
				return;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE:
				setMinClusterSize(((Long)newValue).longValue());
				return;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION:
				setAggregationFunction((AggregationFunction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS:
				setMaxNumberOfClusters(MAX_NUMBER_OF_CLUSTERS_EDEFAULT);
				return;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE:
				setMinClusterSize(MIN_CLUSTER_SIZE_EDEFAULT);
				return;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION:
				setAggregationFunction(AGGREGATION_FUNCTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS:
				return maxNumberOfClusters != MAX_NUMBER_OF_CLUSTERS_EDEFAULT;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE:
				return minClusterSize != MIN_CLUSTER_SIZE_EDEFAULT;
			case ClusteringPackage.CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION:
				return aggregationFunction != AGGREGATION_FUNCTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (maxNumberOfClusters: ");
		result.append(maxNumberOfClusters);
		result.append(", minClusterSize: ");
		result.append(minClusterSize);
		result.append(", aggregationFunction: ");
		result.append(aggregationFunction);
		result.append(')');
		return result.toString();
	}

} //ClusteringFunctionSettingsImpl
