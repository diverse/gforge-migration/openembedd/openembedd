/**
 * INRIA/IRISA
 *
 * $Id: ClassificationTestResult.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;

import orgomg.cwm.analysis.datamining.supervised.MiningTestResult;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the result of a test task applied to a classification model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix <em>Confusion Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getTestTask <em>Test Task</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationTestResult()
 * @model
 * @generated
 */
public interface ClassificationTestResult extends MiningTestResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Accuracy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If state is successful, the absolute number or the  percentage (between 0 and 100) of correct predictions on the inputData (test data). NULL in other states.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Accuracy</em>' attribute.
	 * @see #setAccuracy(String)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationTestResult_Accuracy()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getAccuracy();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getAccuracy <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accuracy</em>' attribute.
	 * @see #getAccuracy()
	 * @generated
	 */
	void setAccuracy(String value);

	/**
	 * Returns the value of the '<em><b>Confusion Matrix</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confusion Matrix</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confusion Matrix</em>' containment reference.
	 * @see #setConfusionMatrix(CategoryMatrix)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationTestResult_ConfusionMatrix()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult
	 * @model opposite="testResult" containment="true"
	 * @generated
	 */
	CategoryMatrix getConfusionMatrix();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix <em>Confusion Matrix</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confusion Matrix</em>' containment reference.
	 * @see #getConfusionMatrix()
	 * @generated
	 */
	void setConfusionMatrix(CategoryMatrix value);

	/**
	 * Returns the value of the '<em><b>Test Task</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestTask#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Task</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Task</em>' container reference.
	 * @see #setTestTask(ClassificationTestTask)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationTestResult_TestTask()
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestTask#getTestResult
	 * @model opposite="testResult" required="true"
	 * @generated
	 */
	ClassificationTestTask getTestTask();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getTestTask <em>Test Task</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Task</em>' container reference.
	 * @see #getTestTask()
	 * @generated
	 */
	void setTestTask(ClassificationTestTask value);

} // ClassificationTestResult
