/**
 * INRIA/IRISA
 *
 * $Id: LogicalData.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A LogicalData object contains the set of MiningAttributes that describe the logical nature of the data used as input to data mining. The MiningAttributes within a LogicalData object are uniquely named.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalData()
 * @model
 * @generated
 */
public interface LogicalData extends orgomg.cwm.objectmodel.core.Class {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getLogicalData <em>Logical Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalData_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getLogicalData
	 * @model opposite="logicalData" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

} // LogicalData
