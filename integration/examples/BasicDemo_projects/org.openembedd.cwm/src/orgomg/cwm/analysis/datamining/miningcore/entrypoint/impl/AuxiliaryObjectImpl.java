/**
 * INRIA/IRISA
 *
 * $Id: AuxiliaryObjectImpl.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Auxiliary Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.AuxiliaryObjectImpl#getSchema <em>Schema</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.AuxiliaryObjectImpl#getAttributeAssignmentSet <em>Attribute Assignment Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AuxiliaryObjectImpl extends ModelElementImpl implements AuxiliaryObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getAttributeAssignmentSet() <em>Attribute Assignment Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeAssignmentSet()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeAssignmentSet> attributeAssignmentSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AuxiliaryObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EntrypointPackage.Literals.AUXILIARY_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema getSchema() {
		if (eContainerFeatureID != EntrypointPackage.AUXILIARY_OBJECT__SCHEMA) return null;
		return (Schema)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchema(Schema newSchema, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSchema, EntrypointPackage.AUXILIARY_OBJECT__SCHEMA, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchema(Schema newSchema) {
		if (newSchema != eInternalContainer() || (eContainerFeatureID != EntrypointPackage.AUXILIARY_OBJECT__SCHEMA && newSchema != null)) {
			if (EcoreUtil.isAncestor(this, newSchema))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSchema != null)
				msgs = ((InternalEObject)newSchema).eInverseAdd(this, EntrypointPackage.SCHEMA__AUX_OBJECTS, Schema.class, msgs);
			msgs = basicSetSchema(newSchema, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EntrypointPackage.AUXILIARY_OBJECT__SCHEMA, newSchema, newSchema));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeAssignmentSet> getAttributeAssignmentSet() {
		if (attributeAssignmentSet == null) {
			attributeAssignmentSet = new EObjectContainmentWithInverseEList<AttributeAssignmentSet>(AttributeAssignmentSet.class, this, EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET, MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET__AUXILIARY_OBJECT);
		}
		return attributeAssignmentSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSchema((Schema)otherEnd, msgs);
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributeAssignmentSet()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				return basicSetSchema(null, msgs);
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				return ((InternalEList<?>)getAttributeAssignmentSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.SCHEMA__AUX_OBJECTS, Schema.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				return getSchema();
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				return getAttributeAssignmentSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				setSchema((Schema)newValue);
				return;
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				getAttributeAssignmentSet().clear();
				getAttributeAssignmentSet().addAll((Collection<? extends AttributeAssignmentSet>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				setSchema((Schema)null);
				return;
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				getAttributeAssignmentSet().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EntrypointPackage.AUXILIARY_OBJECT__SCHEMA:
				return getSchema() != null;
			case EntrypointPackage.AUXILIARY_OBJECT__ATTRIBUTE_ASSIGNMENT_SET:
				return attributeAssignmentSet != null && !attributeAssignmentSet.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AuxiliaryObjectImpl
