/**
 * INRIA/IRISA
 *
 * $Id: AggregationFunction.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Aggregation Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Per ClusteringModel, there is one aggregation function: depending on the attribute type. The aggregated value is optimal if it is 0 (for distance measure) or greater values indicate optimal fit (for simiarity measure).
 * In the definition of functions, Wi is the field weight in the attribute, and Xi and Yi are values.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getAggregationFunction()
 * @model
 * @generated
 */
public enum AggregationFunction implements Enumerator {
	/**
	 * The '<em><b>Euclidean</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EUCLIDEAN
	 * @generated
	 * @ordered
	 */
	EUCLIDEAN_LITERAL(0, "euclidean", "euclidean"),

	/**
	 * The '<em><b>Squared Euclidean</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SQUARED_EUCLIDEAN
	 * @generated
	 * @ordered
	 */
	SQUARED_EUCLIDEAN_LITERAL(1, "squaredEuclidean", "squaredEuclidean"),

	/**
	 * The '<em><b>Chebychev</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHEBYCHEV
	 * @generated
	 * @ordered
	 */
	CHEBYCHEV_LITERAL(2, "chebychev", "chebychev"),

	/**
	 * The '<em><b>City Block</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CITY_BLOCK
	 * @generated
	 * @ordered
	 */
	CITY_BLOCK_LITERAL(3, "cityBlock", "cityBlock"),

	/**
	 * The '<em><b>Minkovski</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINKOVSKI
	 * @generated
	 * @ordered
	 */
	MINKOVSKI_LITERAL(4, "minkovski", "minkovski"),

	/**
	 * The '<em><b>Simple Matching</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIMPLE_MATCHING
	 * @generated
	 * @ordered
	 */
	SIMPLE_MATCHING_LITERAL(5, "simpleMatching", "simpleMatching"),

	/**
	 * The '<em><b>Jaccard</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #JACCARD
	 * @generated
	 * @ordered
	 */
	JACCARD_LITERAL(6, "jaccard", "jaccard"),

	/**
	 * The '<em><b>Tanimoto</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TANIMOTO
	 * @generated
	 * @ordered
	 */
	TANIMOTO_LITERAL(7, "tanimoto", "tanimoto"),

	/**
	 * The '<em><b>Binary Similarity</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BINARY_SIMILARITY
	 * @generated
	 * @ordered
	 */
	BINARY_SIMILARITY_LITERAL(8, "binarySimilarity", "binarySimilarity");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Euclidean</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = distance
	 * D = (sum(Wi * c(Xi,Yi)^2))^(1/2)
	 * <!-- end-model-doc -->
	 * @see #EUCLIDEAN_LITERAL
	 * @model name="euclidean"
	 * @generated
	 * @ordered
	 */
	public static final int EUCLIDEAN = 0;

	/**
	 * The '<em><b>Squared Euclidean</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind - distance (aka squared)
	 * D = sum(Wi * c(Xi, Yi)^2)
	 * 
	 * Euclidean and squared Euclidean are essentially equivalent because only the minimum or maximum of values is needed in order to assign a cluster.
	 * <!-- end-model-doc -->
	 * @see #SQUARED_EUCLIDEAN_LITERAL
	 * @model name="squaredEuclidean"
	 * @generated
	 * @ordered
	 */
	public static final int SQUARED_EUCLIDEAN = 1;

	/**
	 * The '<em><b>Chebychev</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = distance (aka maximum)
	 * D = max(Wi * c(Xi, Yi)) over all i
	 * <!-- end-model-doc -->
	 * @see #CHEBYCHEV_LITERAL
	 * @model name="chebychev"
	 * @generated
	 * @ordered
	 */
	public static final int CHEBYCHEV = 2;

	/**
	 * The '<em><b>City Block</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = distance (aka sum)
	 * D = sum(Wi * c(Xi, Yi))
	 * <!-- end-model-doc -->
	 * @see #CITY_BLOCK_LITERAL
	 * @model name="cityBlock"
	 * @generated
	 * @ordered
	 */
	public static final int CITY_BLOCK = 3;

	/**
	 * The '<em><b>Minkovski</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = distance (p is a parameter > 0)
	 * D = (sum |Xi - Yi|^p)^(1/p)
	 * <!-- end-model-doc -->
	 * @see #MINKOVSKI_LITERAL
	 * @model name="minkovski"
	 * @generated
	 * @ordered
	 */
	public static final int MINKOVSKI = 4;

	/**
	 * The '<em><b>Simple Matching</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = similarity (min=0, max=1)
	 * D = (a11 + a00) / (a11 + a10 + a01 + a00)
	 * 
	 * For binary or categorical data, let two individuals compare their values for each attribute, and
	 * a11 = number of times where Xi=1 and Yi=1
	 * a10 = number of times where Xi=1 and Yi=0
	 * a01 = number of times where Xi=0 and Yi=1
	 * a00 = number of times where Xi=0 and Yi=0
	 * <!-- end-model-doc -->
	 * @see #SIMPLE_MATCHING_LITERAL
	 * @model name="simpleMatching"
	 * @generated
	 * @ordered
	 */
	public static final int SIMPLE_MATCHING = 5;

	/**
	 * The '<em><b>Jaccard</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = similarity
	 * D = a11 / (a11 + a10 + a01)
	 * <!-- end-model-doc -->
	 * @see #JACCARD_LITERAL
	 * @model name="jaccard"
	 * @generated
	 * @ordered
	 */
	public static final int JACCARD = 6;

	/**
	 * The '<em><b>Tanimoto</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kiind = similarity (min=0, max=1)
	 * D = (a11 + a00) / (a11 + 2*(a10+a01) + a00)
	 * <!-- end-model-doc -->
	 * @see #TANIMOTO_LITERAL
	 * @model name="tanimoto"
	 * @generated
	 * @ordered
	 */
	public static final int TANIMOTO = 7;

	/**
	 * The '<em><b>Binary Similarity</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * kind = similarity (min=0, max=1, and c.. and d.. are parameters > 0)
	 * D = (c11*a11 + c10*a10 + c01*a01 + c00*a00) / (d11*a11 + d10*a10 + d01*a01 + d00*a00)
	 * <!-- end-model-doc -->
	 * @see #BINARY_SIMILARITY_LITERAL
	 * @model name="binarySimilarity"
	 * @generated
	 * @ordered
	 */
	public static final int BINARY_SIMILARITY = 8;

	/**
	 * An array of all the '<em><b>Aggregation Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AggregationFunction[] VALUES_ARRAY =
		new AggregationFunction[] {
			EUCLIDEAN_LITERAL,
			SQUARED_EUCLIDEAN_LITERAL,
			CHEBYCHEV_LITERAL,
			CITY_BLOCK_LITERAL,
			MINKOVSKI_LITERAL,
			SIMPLE_MATCHING_LITERAL,
			JACCARD_LITERAL,
			TANIMOTO_LITERAL,
			BINARY_SIMILARITY_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Aggregation Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AggregationFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Aggregation Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AggregationFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AggregationFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Aggregation Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AggregationFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AggregationFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Aggregation Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AggregationFunction get(int value) {
		switch (value) {
			case EUCLIDEAN: return EUCLIDEAN_LITERAL;
			case SQUARED_EUCLIDEAN: return SQUARED_EUCLIDEAN_LITERAL;
			case CHEBYCHEV: return CHEBYCHEV_LITERAL;
			case CITY_BLOCK: return CITY_BLOCK_LITERAL;
			case MINKOVSKI: return MINKOVSKI_LITERAL;
			case SIMPLE_MATCHING: return SIMPLE_MATCHING_LITERAL;
			case JACCARD: return JACCARD_LITERAL;
			case TANIMOTO: return TANIMOTO_LITERAL;
			case BINARY_SIMILARITY: return BINARY_SIMILARITY_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AggregationFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AggregationFunction
