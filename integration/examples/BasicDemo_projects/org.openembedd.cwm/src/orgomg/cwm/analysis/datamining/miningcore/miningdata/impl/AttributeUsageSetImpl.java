/**
 * INRIA/IRISA
 *
 * $Id: AttributeUsageSetImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;
import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.objectmodel.core.impl.ClassImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Usage Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageSetImpl#getSettings <em>Settings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeUsageSetImpl extends ClassImpl implements AttributeUsageSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeUsageSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.ATTRIBUTE_USAGE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningFunctionSettings getSettings() {
		if (eContainerFeatureID != MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS) return null;
		return (MiningFunctionSettings)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSettings(MiningFunctionSettings newSettings, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSettings, MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSettings(MiningFunctionSettings newSettings) {
		if (newSettings != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS && newSettings != null)) {
			if (EcoreUtil.isAncestor(this, newSettings))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSettings != null)
				msgs = ((InternalEObject)newSettings).eInverseAdd(this, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET, MiningFunctionSettings.class, msgs);
			msgs = basicSetSettings(newSettings, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS, newSettings, newSettings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSettings((MiningFunctionSettings)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				return basicSetSettings(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				return eInternalContainer().eInverseRemove(this, MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET, MiningFunctionSettings.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				return getSettings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				setSettings((MiningFunctionSettings)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				setSettings((MiningFunctionSettings)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE_SET__SETTINGS:
				return getSettings() != null;
		}
		return super.eIsSet(featureID);
	}

} //AttributeUsageSetImpl
