/**
 * INRIA/IRISA
 *
 * $Id: MiningFunction.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Mining Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getMiningFunction()
 * @model
 * @generated
 */
public enum MiningFunction implements Enumerator {
	/**
	 * The '<em><b>Classification</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLASSIFICATION
	 * @generated
	 * @ordered
	 */
	CLASSIFICATION_LITERAL(0, "classification", "classification"),

	/**
	 * The '<em><b>Regression</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REGRESSION
	 * @generated
	 * @ordered
	 */
	REGRESSION_LITERAL(1, "regression", "regression"),

	/**
	 * The '<em><b>Clustering</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLUSTERING
	 * @generated
	 * @ordered
	 */
	CLUSTERING_LITERAL(2, "clustering", "clustering"),

	/**
	 * The '<em><b>Association Rules</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSOCIATION_RULES
	 * @generated
	 * @ordered
	 */
	ASSOCIATION_RULES_LITERAL(3, "associationRules", "associationRules"),

	/**
	 * The '<em><b>Attribute Importance</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATTRIBUTE_IMPORTANCE
	 * @generated
	 * @ordered
	 */
	ATTRIBUTE_IMPORTANCE_LITERAL(4, "attributeImportance", "attributeImportance"),

	/**
	 * The '<em><b>Sequence Analysis</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SEQUENCE_ANALYSIS
	 * @generated
	 * @ordered
	 */
	SEQUENCE_ANALYSIS_LITERAL(5, "sequenceAnalysis", "sequenceAnalysis");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Classification</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algorithm that is used for classification of one or more specified target attributes.
	 * <!-- end-model-doc -->
	 * @see #CLASSIFICATION_LITERAL
	 * @model name="classification"
	 * @generated
	 * @ordered
	 */
	public static final int CLASSIFICATION = 0;

	/**
	 * The '<em><b>Regression</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algorithm that is used for regressing a value of any given attribute.
	 * <!-- end-model-doc -->
	 * @see #REGRESSION_LITERAL
	 * @model name="regression"
	 * @generated
	 * @ordered
	 */
	public static final int REGRESSION = 1;

	/**
	 * The '<em><b>Clustering</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algorithm that is used for finding clusters of a given data set.
	 * <!-- end-model-doc -->
	 * @see #CLUSTERING_LITERAL
	 * @model name="clustering"
	 * @generated
	 * @ordered
	 */
	public static final int CLUSTERING = 2;

	/**
	 * The '<em><b>Association Rules</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algorithm that is used for finding associations between items in a given data set.
	 * <!-- end-model-doc -->
	 * @see #ASSOCIATION_RULES_LITERAL
	 * @model name="associationRules"
	 * @generated
	 * @ordered
	 */
	public static final int ASSOCIATION_RULES = 3;

	/**
	 * The '<em><b>Attribute Importance</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algorithm that is used for finding a subset of attributes in a given data set based on certain importance criteria.
	 * This kind of function is also known as feature selection in the data mining literature, which is broader semantically than attribute importance.
	 * <!-- end-model-doc -->
	 * @see #ATTRIBUTE_IMPORTANCE_LITERAL
	 * @model name="attributeImportance"
	 * @generated
	 * @ordered
	 */
	public static final int ATTRIBUTE_IMPORTANCE = 4;

	/**
	 * The '<em><b>Sequence Analysis</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates any algoithm that is used for finding and anayzing sequences in a given data set.
	 * <!-- end-model-doc -->
	 * @see #SEQUENCE_ANALYSIS_LITERAL
	 * @model name="sequenceAnalysis"
	 * @generated
	 * @ordered
	 */
	public static final int SEQUENCE_ANALYSIS = 5;

	/**
	 * An array of all the '<em><b>Mining Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MiningFunction[] VALUES_ARRAY =
		new MiningFunction[] {
			CLASSIFICATION_LITERAL,
			REGRESSION_LITERAL,
			CLUSTERING_LITERAL,
			ASSOCIATION_RULES_LITERAL,
			ATTRIBUTE_IMPORTANCE_LITERAL,
			SEQUENCE_ANALYSIS_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Mining Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MiningFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Mining Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MiningFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mining Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MiningFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mining Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningFunction get(int value) {
		switch (value) {
			case CLASSIFICATION: return CLASSIFICATION_LITERAL;
			case REGRESSION: return REGRESSION_LITERAL;
			case CLUSTERING: return CLUSTERING_LITERAL;
			case ASSOCIATION_RULES: return ASSOCIATION_RULES_LITERAL;
			case ATTRIBUTE_IMPORTANCE: return ATTRIBUTE_IMPORTANCE_LITERAL;
			case SEQUENCE_ANALYSIS: return SEQUENCE_ANALYSIS_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MiningFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MiningFunction
