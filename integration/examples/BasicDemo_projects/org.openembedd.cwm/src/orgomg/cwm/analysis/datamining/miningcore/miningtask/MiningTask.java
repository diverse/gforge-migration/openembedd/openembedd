/**
 * INRIA/IRISA
 *
 * $Id: MiningTask.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is an abstract class that describes an executable data mining task operating on data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputModel <em>Input Model</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputData <em>Input Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getModelAssignment <em>Model Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningTask()
 * @model abstract="true"
 * @generated
 */
public interface MiningTask extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Input Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Model</em>' reference.
	 * @see #setInputModel(MiningModel)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningTask_InputModel()
	 * @model
	 * @generated
	 */
	MiningModel getInputModel();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputModel <em>Input Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Model</em>' reference.
	 * @see #getInputModel()
	 * @generated
	 */
	void setInputModel(MiningModel value);

	/**
	 * Returns the value of the '<em><b>Input Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Data</em>' reference.
	 * @see #setInputData(PhysicalData)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningTask_InputData()
	 * @model required="true"
	 * @generated
	 */
	PhysicalData getInputData();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputData <em>Input Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Data</em>' reference.
	 * @see #getInputData()
	 * @generated
	 */
	void setInputData(PhysicalData value);

	/**
	 * Returns the value of the '<em><b>Model Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Assignment</em>' reference.
	 * @see #setModelAssignment(AttributeAssignmentSet)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningTask_ModelAssignment()
	 * @model
	 * @generated
	 */
	AttributeAssignmentSet getModelAssignment();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getModelAssignment <em>Model Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Assignment</em>' reference.
	 * @see #getModelAssignment()
	 * @generated
	 */
	void setModelAssignment(AttributeAssignmentSet value);

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningTask <em>Mining Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getMiningTask_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningTask
	 * @model opposite="miningTask" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

} // MiningTask
