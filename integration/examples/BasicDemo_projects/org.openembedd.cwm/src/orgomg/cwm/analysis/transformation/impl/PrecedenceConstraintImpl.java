/**
 * INRIA/IRISA
 *
 * $Id: PrecedenceConstraintImpl.java,v 1.1 2008-04-01 09:35:37 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.transformation.PrecedenceConstraint;
import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.objectmodel.core.impl.ConstraintImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Precedence Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PrecedenceConstraintImpl extends ConstraintImpl implements PrecedenceConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrecedenceConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformationPackage.Literals.PRECEDENCE_CONSTRAINT;
	}

} //PrecedenceConstraintImpl
