/**
 * INRIA/IRISA
 *
 * $Id: SchemaItemProvider.java,v 1.1 2008-04-01 09:35:42 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import orgomg.cwm.analysis.datamining.approximation.ApproximationFactory;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesFactory;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportanceFactory;

import orgomg.cwm.analysis.datamining.classification.ClassificationFactory;

import orgomg.cwm.analysis.datamining.clustering.ClusteringFactory;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointFactory;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataFactory;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelFactory;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskFactory;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFactory;

import orgomg.cwm.objectmodel.core.CorePackage;

import orgomg.cwm.objectmodel.core.provider.CWMEditPlugin;
import orgomg.cwm.objectmodel.core.provider.NamespaceItemProvider;

/**
 * This is the item provider adapter for a {@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SchemaItemProvider
	extends NamespaceItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchemaItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__LOGICAL_DATA);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__CATEGORY_MATRIX);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__AUX_OBJECTS);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__TAXONOMY);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__MINING_MODEL);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__MINING_TASK);
			childrenFeatures.add(EntrypointPackage.Literals.SCHEMA__RESULT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Schema.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Schema"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Schema)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Schema_type") :
			getString("_UI_Schema_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Schema.class)) {
			case EntrypointPackage.SCHEMA__LOGICAL_DATA:
			case EntrypointPackage.SCHEMA__CATEGORY_MATRIX:
			case EntrypointPackage.SCHEMA__AUX_OBJECTS:
			case EntrypointPackage.SCHEMA__TAXONOMY:
			case EntrypointPackage.SCHEMA__MINING_FUNCTION_SETTINGS:
			case EntrypointPackage.SCHEMA__MINING_MODEL:
			case EntrypointPackage.SCHEMA__MINING_TASK:
			case EntrypointPackage.SCHEMA__RESULT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__LOGICAL_DATA,
				 MiningdataFactory.eINSTANCE.createLogicalData()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__CATEGORY_MATRIX,
				 MiningdataFactory.eINSTANCE.createCategoryMatrix()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__CATEGORY_MATRIX,
				 MiningdataFactory.eINSTANCE.createCategoryMatrixObject()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__CATEGORY_MATRIX,
				 MiningdataFactory.eINSTANCE.createCategoryMatrixTable()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__AUX_OBJECTS,
				 EntrypointFactory.eINSTANCE.createAuxiliaryObject()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__TAXONOMY,
				 MiningdataFactory.eINSTANCE.createCategoryTaxonomy()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 ClusteringFactory.eINSTANCE.createClusteringFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 AssociationrulesFactory.eINSTANCE.createFrequentItemSetFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 AssociationrulesFactory.eINSTANCE.createAssociationRulesFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 AssociationrulesFactory.eINSTANCE.createSequenceFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 SupervisedFactory.eINSTANCE.createSupervisedFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 ClassificationFactory.eINSTANCE.createClassificationFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 ApproximationFactory.eINSTANCE.createApproximationFunctionSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS,
				 AttributeimportanceFactory.eINSTANCE.createAttributeImportanceSettings()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_MODEL,
				 MiningmodelFactory.eINSTANCE.createMiningModel()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_TASK,
				 MiningtaskFactory.eINSTANCE.createMiningApplyTask()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_TASK,
				 MiningtaskFactory.eINSTANCE.createMiningBuildTask()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_TASK,
				 SupervisedFactory.eINSTANCE.createMiningTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_TASK,
				 ClassificationFactory.eINSTANCE.createClassificationTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__MINING_TASK,
				 ApproximationFactory.eINSTANCE.createApproximationTestTask()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__RESULT,
				 SupervisedFactory.eINSTANCE.createMiningTestResult()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__RESULT,
				 ClassificationFactory.eINSTANCE.createClassificationTestResult()));

		newChildDescriptors.add
			(createChildParameter
				(EntrypointPackage.Literals.SCHEMA__RESULT,
				 ApproximationFactory.eINSTANCE.createApproximationTestResult()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CorePackage.Literals.NAMESPACE__OWNED_ELEMENT ||
			childFeature == EntrypointPackage.Literals.SCHEMA__AUX_OBJECTS ||
			childFeature == EntrypointPackage.Literals.SCHEMA__LOGICAL_DATA ||
			childFeature == EntrypointPackage.Literals.SCHEMA__CATEGORY_MATRIX ||
			childFeature == EntrypointPackage.Literals.SCHEMA__TAXONOMY ||
			childFeature == EntrypointPackage.Literals.SCHEMA__MINING_MODEL ||
			childFeature == EntrypointPackage.Literals.SCHEMA__MINING_TASK ||
			childFeature == EntrypointPackage.Literals.SCHEMA__MINING_FUNCTION_SETTINGS ||
			childFeature == EntrypointPackage.Literals.SCHEMA__RESULT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CWMEditPlugin.INSTANCE;
	}

}
