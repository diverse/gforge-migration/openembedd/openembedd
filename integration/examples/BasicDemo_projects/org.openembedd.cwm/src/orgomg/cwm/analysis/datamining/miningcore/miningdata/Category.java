/**
 * INRIA/IRISA
 *
 * $Id: Category.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a discrete value. A collection of Category instances defines the values that may or may not be annotated with a mining attribute.
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getValue <em>Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#isIsNullCategory <em>Is Null Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getDisplayName <em>Display Name</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getProperty <em>Property</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getPrior <em>Prior</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties <em>Categorical Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory()
 * @model
 * @generated
 */
public interface Category extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Constraint on value: Datatype must define equality operator.
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_Value()
	 * @model dataType="orgomg.cwm.objectmodel.core.Any"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Is Null Category</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The optional attribute isNullCategory is set to true if the Category provided is the NULL category. 
	 * 
	 * If true, this invalidates any specification in the value attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Null Category</em>' attribute.
	 * @see #setIsNullCategory(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_IsNullCategory()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsNullCategory();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#isIsNullCategory <em>Is Null Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Null Category</em>' attribute.
	 * @see #isIsNullCategory()
	 * @generated
	 */
	void setIsNullCategory(boolean value);

	/**
	 * Returns the value of the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The displayName is a string which may be used by applications to refer to that category.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Display Name</em>' attribute.
	 * @see #setDisplayName(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_DisplayName()
	 * @model dataType="orgomg.cwm.objectmodel.core.String"
	 * @generated
	 */
	String getDisplayName();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getDisplayName <em>Display Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Name</em>' attribute.
	 * @see #getDisplayName()
	 * @generated
	 */
	void setDisplayName(String value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' attribute.
	 * The default value is <code>"valid"</code>.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This accepts a value of the enumeration identifying the role of this Category instance.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Property</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty
	 * @see #setProperty(CategoryProperty)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_Property()
	 * @model default="valid"
	 * @generated
	 */
	CategoryProperty getProperty();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getProperty <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(CategoryProperty value);

	/**
	 * Returns the value of the '<em><b>Prior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This contains the prior probability associated with this Category, if any.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Prior</em>' attribute.
	 * @see #setPrior(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_Prior()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getPrior();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getPrior <em>Prior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prior</em>' attribute.
	 * @see #getPrior()
	 * @generated
	 */
	void setPrior(String value);

	/**
	 * Returns the value of the '<em><b>Categorical Properties</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Categorical Properties</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Categorical Properties</em>' container reference.
	 * @see #setCategoricalProperties(CategoricalAttributeProperties)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategory_CategoricalProperties()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getCategory
	 * @model opposite="category" required="true"
	 * @generated
	 */
	CategoricalAttributeProperties getCategoricalProperties();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties <em>Categorical Properties</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Categorical Properties</em>' container reference.
	 * @see #getCategoricalProperties()
	 * @generated
	 */
	void setCategoricalProperties(CategoricalAttributeProperties value);

} // Category
