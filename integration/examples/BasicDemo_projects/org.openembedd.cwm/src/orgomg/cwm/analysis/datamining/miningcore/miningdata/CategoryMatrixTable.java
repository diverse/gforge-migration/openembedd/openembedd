/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixTable.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Matrix Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Table with three columns holding the definition of a category matrix.
 * Tabular representation of a CategoryMatrix. A category matrix consists of exactly one table. The table has three row, column and value of the entry.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getSource <em>Source</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getColumnAttribute <em>Column Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getRowAttribute <em>Row Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getValueAttribute <em>Value Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixTable()
 * @model
 * @generated
 */
public interface CategoryMatrixTable extends CategoryMatrix {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.objectmodel.core.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixTable_Source()
	 * @model
	 * @generated
	 */
	EList<orgomg.cwm.objectmodel.core.Class> getSource();

	/**
	 * Returns the value of the '<em><b>Column Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column Attribute</em>' reference.
	 * @see #setColumnAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixTable_ColumnAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getColumnAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getColumnAttribute <em>Column Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column Attribute</em>' reference.
	 * @see #getColumnAttribute()
	 * @generated
	 */
	void setColumnAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Row Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row Attribute</em>' reference.
	 * @see #setRowAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixTable_RowAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getRowAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getRowAttribute <em>Row Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row Attribute</em>' reference.
	 * @see #getRowAttribute()
	 * @generated
	 */
	void setRowAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' reference.
	 * @see #setValueAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixTable_ValueAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getValueAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixTable#getValueAttribute <em>Value Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Attribute</em>' reference.
	 * @see #getValueAttribute()
	 * @generated
	 */
	void setValueAttribute(Attribute value);

} // CategoryMatrixTable
