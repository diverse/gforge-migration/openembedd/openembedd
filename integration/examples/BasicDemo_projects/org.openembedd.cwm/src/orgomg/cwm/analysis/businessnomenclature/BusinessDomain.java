/**
 * INRIA/IRISA
 *
 * $Id: BusinessDomain.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.analysis.businessnomenclature;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a business domain.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.businessnomenclature.BusinessnomenclaturePackage#getBusinessDomain()
 * @model
 * @generated
 */
public interface BusinessDomain extends orgomg.cwm.objectmodel.core.Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // BusinessDomain
