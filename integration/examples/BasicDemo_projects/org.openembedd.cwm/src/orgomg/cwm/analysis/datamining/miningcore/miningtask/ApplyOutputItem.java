/**
 * INRIA/IRISA
 *
 * $Id: ApplyOutputItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Output Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This object describes an entity of apply output. It is usually stored in a destination attribute. The destination attribute is specified by an AttributeAssignment object.
 * If Transactional output, then ApplyOutputItem does not have a destination attribute. Instead, the (name) entry for the attrNameAttribute is held in 'name' inherited from ModelElement
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput <em>Apply Output</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyOutputItem()
 * @model
 * @generated
 */
public interface ApplyOutputItem extends MiningAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Apply Output</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apply Output</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Apply Output</em>' container reference.
	 * @see #setApplyOutput(MiningApplyOutput)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyOutputItem_ApplyOutput()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput#getItem
	 * @model opposite="item" required="true"
	 * @generated
	 */
	MiningApplyOutput getApplyOutput();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput <em>Apply Output</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Apply Output</em>' container reference.
	 * @see #getApplyOutput()
	 * @generated
	 */
	void setApplyOutput(MiningApplyOutput value);

} // ApplyOutputItem
