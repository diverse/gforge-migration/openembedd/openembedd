/**
 * INRIA/IRISA
 *
 * $Id: MiningfunctionsettingsFactoryImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiningfunctionsettingsFactoryImpl extends EFactoryImpl implements MiningfunctionsettingsFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningfunctionsettingsFactory init() {
		try {
			MiningfunctionsettingsFactory theMiningfunctionsettingsFactory = (MiningfunctionsettingsFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/miningcore/miningfunctionsettings.ecore"); 
			if (theMiningfunctionsettingsFactory != null) {
				return theMiningfunctionsettingsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MiningfunctionsettingsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningfunctionsettingsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningfunctionsettingsPackage getMiningfunctionsettingsPackage() {
		return (MiningfunctionsettingsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MiningfunctionsettingsPackage getPackage() {
		return MiningfunctionsettingsPackage.eINSTANCE;
	}

} //MiningfunctionsettingsFactoryImpl
