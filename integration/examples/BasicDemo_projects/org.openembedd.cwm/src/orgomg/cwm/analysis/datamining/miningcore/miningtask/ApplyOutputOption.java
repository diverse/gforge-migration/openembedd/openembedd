/**
 * INRIA/IRISA
 *
 * $Id: ApplyOutputOption.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Apply Output Option</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This enumeration specifies how apply output is created.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyOutputOption()
 * @model
 * @generated
 */
public enum ApplyOutputOption implements Enumerator {
	/**
	 * The '<em><b>Append To Existing</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPEND_TO_EXISTING
	 * @generated
	 * @ordered
	 */
	APPEND_TO_EXISTING_LITERAL(0, "appendToExisting", "appendToExisting"),

	/**
	 * The '<em><b>Create New</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CREATE_NEW
	 * @generated
	 * @ordered
	 */
	CREATE_NEW_LITERAL(1, "createNew", "createNew");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Append To Existing</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates that the apply output is appended to an existing file/table (presumably, another apply output).
	 * <!-- end-model-doc -->
	 * @see #APPEND_TO_EXISTING_LITERAL
	 * @model name="appendToExisting"
	 * @generated
	 * @ordered
	 */
	public static final int APPEND_TO_EXISTING = 0;

	/**
	 * The '<em><b>Create New</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates that the apply output is to be stored in a new file/table.
	 * <!-- end-model-doc -->
	 * @see #CREATE_NEW_LITERAL
	 * @model name="createNew"
	 * @generated
	 * @ordered
	 */
	public static final int CREATE_NEW = 1;

	/**
	 * An array of all the '<em><b>Apply Output Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ApplyOutputOption[] VALUES_ARRAY =
		new ApplyOutputOption[] {
			APPEND_TO_EXISTING_LITERAL,
			CREATE_NEW_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Apply Output Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ApplyOutputOption> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Apply Output Option</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApplyOutputOption get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ApplyOutputOption result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Apply Output Option</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApplyOutputOption getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ApplyOutputOption result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Apply Output Option</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApplyOutputOption get(int value) {
		switch (value) {
			case APPEND_TO_EXISTING: return APPEND_TO_EXISTING_LITERAL;
			case CREATE_NEW: return CREATE_NEW_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ApplyOutputOption(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ApplyOutputOption
