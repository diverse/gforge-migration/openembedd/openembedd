/**
 * <copyright>
 * </copyright>
 *
 * $Id: CategoricalAttributePropertiesValidator.java,v 1.1 2008-04-01 09:35:21 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface CategoricalAttributePropertiesValidator {
	boolean validate();

	boolean validateLogicalAttribute(LogicalAttribute value);
	boolean validateTaxonomy(CategoryTaxonomy value);
	boolean validateCategory(EList<Category> value);
}
