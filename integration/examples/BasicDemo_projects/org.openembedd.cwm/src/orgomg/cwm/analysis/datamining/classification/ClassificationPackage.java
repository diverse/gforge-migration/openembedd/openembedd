/**
 * INRIA/IRISA
 *
 * $Id: ClassificationPackage.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.objectmodel.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains the metamodel that represents classification function, models, and settings.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationFactory
 * @model kind="package"
 * @generated
 */
public interface ClassificationPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "classification";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/classification.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.classification";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassificationPackage eINSTANCE = orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ApplyTargetValueItemImpl <em>Apply Target Value Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ApplyTargetValueItemImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getApplyTargetValueItem()
	 * @generated
	 */
	int APPLY_TARGET_VALUE_ITEM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__NAME = MiningtaskPackage.APPLY_OUTPUT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__VISIBILITY = MiningtaskPackage.APPLY_OUTPUT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__CLIENT_DEPENDENCY = MiningtaskPackage.APPLY_OUTPUT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__SUPPLIER_DEPENDENCY = MiningtaskPackage.APPLY_OUTPUT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__CONSTRAINT = MiningtaskPackage.APPLY_OUTPUT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__NAMESPACE = MiningtaskPackage.APPLY_OUTPUT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__IMPORTER = MiningtaskPackage.APPLY_OUTPUT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__STEREOTYPE = MiningtaskPackage.APPLY_OUTPUT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__TAGGED_VALUE = MiningtaskPackage.APPLY_OUTPUT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__DOCUMENT = MiningtaskPackage.APPLY_OUTPUT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__DESCRIPTION = MiningtaskPackage.APPLY_OUTPUT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__RESPONSIBLE_PARTY = MiningtaskPackage.APPLY_OUTPUT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__ELEMENT_NODE = MiningtaskPackage.APPLY_OUTPUT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__SET = MiningtaskPackage.APPLY_OUTPUT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__RENDERED_OBJECT = MiningtaskPackage.APPLY_OUTPUT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__VOCABULARY_ELEMENT = MiningtaskPackage.APPLY_OUTPUT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__MEASUREMENT = MiningtaskPackage.APPLY_OUTPUT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__CHANGE_REQUEST = MiningtaskPackage.APPLY_OUTPUT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__OWNER_SCOPE = MiningtaskPackage.APPLY_OUTPUT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__OWNER = MiningtaskPackage.APPLY_OUTPUT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__FEATURE_NODE = MiningtaskPackage.APPLY_OUTPUT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__FEATURE_MAP = MiningtaskPackage.APPLY_OUTPUT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__CF_MAP = MiningtaskPackage.APPLY_OUTPUT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__CHANGEABILITY = MiningtaskPackage.APPLY_OUTPUT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__MULTIPLICITY = MiningtaskPackage.APPLY_OUTPUT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__ORDERING = MiningtaskPackage.APPLY_OUTPUT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__TARGET_SCOPE = MiningtaskPackage.APPLY_OUTPUT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__TYPE = MiningtaskPackage.APPLY_OUTPUT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__SLOT = MiningtaskPackage.APPLY_OUTPUT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__DISCRIMINATED_UNION = MiningtaskPackage.APPLY_OUTPUT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__INDEXED_FEATURE = MiningtaskPackage.APPLY_OUTPUT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__KEY_RELATIONSHIP = MiningtaskPackage.APPLY_OUTPUT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__UNIQUE_KEY = MiningtaskPackage.APPLY_OUTPUT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__INITIAL_VALUE = MiningtaskPackage.APPLY_OUTPUT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__DISPLAY_NAME = MiningtaskPackage.APPLY_OUTPUT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__ATTRIBUTE_TYPE = MiningtaskPackage.APPLY_OUTPUT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__APPLY_OUTPUT = MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT;

	/**
	 * The feature id for the '<em><b>Target Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM__TARGET_VALUE = MiningtaskPackage.APPLY_OUTPUT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Apply Target Value Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TARGET_VALUE_ITEM_FEATURE_COUNT = MiningtaskPackage.APPLY_OUTPUT_ITEM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationAttributeUsageImpl <em>Attribute Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationAttributeUsageImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationAttributeUsage()
	 * @generated
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__NAME = MiningdataPackage.ATTRIBUTE_USAGE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__VISIBILITY = MiningdataPackage.ATTRIBUTE_USAGE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__CLIENT_DEPENDENCY = MiningdataPackage.ATTRIBUTE_USAGE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__SUPPLIER_DEPENDENCY = MiningdataPackage.ATTRIBUTE_USAGE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__CONSTRAINT = MiningdataPackage.ATTRIBUTE_USAGE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__NAMESPACE = MiningdataPackage.ATTRIBUTE_USAGE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__IMPORTER = MiningdataPackage.ATTRIBUTE_USAGE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__STEREOTYPE = MiningdataPackage.ATTRIBUTE_USAGE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__TAGGED_VALUE = MiningdataPackage.ATTRIBUTE_USAGE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__DOCUMENT = MiningdataPackage.ATTRIBUTE_USAGE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__DESCRIPTION = MiningdataPackage.ATTRIBUTE_USAGE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__RESPONSIBLE_PARTY = MiningdataPackage.ATTRIBUTE_USAGE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__ELEMENT_NODE = MiningdataPackage.ATTRIBUTE_USAGE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__SET = MiningdataPackage.ATTRIBUTE_USAGE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__RENDERED_OBJECT = MiningdataPackage.ATTRIBUTE_USAGE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__VOCABULARY_ELEMENT = MiningdataPackage.ATTRIBUTE_USAGE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__MEASUREMENT = MiningdataPackage.ATTRIBUTE_USAGE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__CHANGE_REQUEST = MiningdataPackage.ATTRIBUTE_USAGE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__OWNER_SCOPE = MiningdataPackage.ATTRIBUTE_USAGE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__OWNER = MiningdataPackage.ATTRIBUTE_USAGE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__FEATURE_NODE = MiningdataPackage.ATTRIBUTE_USAGE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__FEATURE_MAP = MiningdataPackage.ATTRIBUTE_USAGE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__CF_MAP = MiningdataPackage.ATTRIBUTE_USAGE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__USAGE = MiningdataPackage.ATTRIBUTE_USAGE__USAGE;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__WEIGHT = MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT;

	/**
	 * The feature id for the '<em><b>Suppress Discretization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION = MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION;

	/**
	 * The feature id for the '<em><b>Suppress Normalization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION = MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__ATTRIBUTE = MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Positive Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__POSITIVE_CATEGORY = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Priors</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE__PRIORS = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_ATTRIBUTE_USAGE_FEATURE_COUNT = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationFunctionSettingsImpl <em>Function Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationFunctionSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationFunctionSettings()
	 * @generated
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__NAME = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__VISIBILITY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__CLIENT_DEPENDENCY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__CONSTRAINT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__NAMESPACE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__IMPORTER = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__STEREOTYPE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__TAGGED_VALUE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__DOCUMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__DESCRIPTION = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__RESPONSIBLE_PARTY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__ELEMENT_NODE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__SET = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__RENDERED_OBJECT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__VOCABULARY_ELEMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__MEASUREMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__CHANGE_REQUEST = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__ALGORITHM_SETTINGS = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__LOGICAL_DATA = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__SCHEMA = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Cost Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS__COST_MATRIX = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Function Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_FUNCTION_SETTINGS_FEATURE_COUNT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl <em>Test Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationTestResult()
	 * @generated
	 */
	int CLASSIFICATION_TEST_RESULT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__NAME = SupervisedPackage.MINING_TEST_RESULT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__VISIBILITY = SupervisedPackage.MINING_TEST_RESULT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__CLIENT_DEPENDENCY = SupervisedPackage.MINING_TEST_RESULT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__SUPPLIER_DEPENDENCY = SupervisedPackage.MINING_TEST_RESULT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__CONSTRAINT = SupervisedPackage.MINING_TEST_RESULT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__NAMESPACE = SupervisedPackage.MINING_TEST_RESULT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__IMPORTER = SupervisedPackage.MINING_TEST_RESULT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__STEREOTYPE = SupervisedPackage.MINING_TEST_RESULT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__TAGGED_VALUE = SupervisedPackage.MINING_TEST_RESULT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__DOCUMENT = SupervisedPackage.MINING_TEST_RESULT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__DESCRIPTION = SupervisedPackage.MINING_TEST_RESULT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__RESPONSIBLE_PARTY = SupervisedPackage.MINING_TEST_RESULT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__ELEMENT_NODE = SupervisedPackage.MINING_TEST_RESULT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__SET = SupervisedPackage.MINING_TEST_RESULT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__RENDERED_OBJECT = SupervisedPackage.MINING_TEST_RESULT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__VOCABULARY_ELEMENT = SupervisedPackage.MINING_TEST_RESULT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__MEASUREMENT = SupervisedPackage.MINING_TEST_RESULT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__CHANGE_REQUEST = SupervisedPackage.MINING_TEST_RESULT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__SCHEMA = SupervisedPackage.MINING_TEST_RESULT__SCHEMA;

	/**
	 * The feature id for the '<em><b>Number Of Test Records</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__NUMBER_OF_TEST_RECORDS = SupervisedPackage.MINING_TEST_RESULT__NUMBER_OF_TEST_RECORDS;

	/**
	 * The feature id for the '<em><b>Lift Analysis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__LIFT_ANALYSIS = SupervisedPackage.MINING_TEST_RESULT__LIFT_ANALYSIS;

	/**
	 * The feature id for the '<em><b>Accuracy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__ACCURACY = SupervisedPackage.MINING_TEST_RESULT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Confusion Matrix</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX = SupervisedPackage.MINING_TEST_RESULT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Test Task</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT__TEST_TASK = SupervisedPackage.MINING_TEST_RESULT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_RESULT_FEATURE_COUNT = SupervisedPackage.MINING_TEST_RESULT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestTaskImpl <em>Test Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestTaskImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationTestTask()
	 * @generated
	 */
	int CLASSIFICATION_TEST_TASK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__NAME = SupervisedPackage.MINING_TEST_TASK__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__VISIBILITY = SupervisedPackage.MINING_TEST_TASK__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__CLIENT_DEPENDENCY = SupervisedPackage.MINING_TEST_TASK__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__SUPPLIER_DEPENDENCY = SupervisedPackage.MINING_TEST_TASK__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__CONSTRAINT = SupervisedPackage.MINING_TEST_TASK__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__NAMESPACE = SupervisedPackage.MINING_TEST_TASK__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__IMPORTER = SupervisedPackage.MINING_TEST_TASK__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__STEREOTYPE = SupervisedPackage.MINING_TEST_TASK__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__TAGGED_VALUE = SupervisedPackage.MINING_TEST_TASK__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__DOCUMENT = SupervisedPackage.MINING_TEST_TASK__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__DESCRIPTION = SupervisedPackage.MINING_TEST_TASK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__RESPONSIBLE_PARTY = SupervisedPackage.MINING_TEST_TASK__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__ELEMENT_NODE = SupervisedPackage.MINING_TEST_TASK__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__SET = SupervisedPackage.MINING_TEST_TASK__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__RENDERED_OBJECT = SupervisedPackage.MINING_TEST_TASK__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__VOCABULARY_ELEMENT = SupervisedPackage.MINING_TEST_TASK__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__MEASUREMENT = SupervisedPackage.MINING_TEST_TASK__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__CHANGE_REQUEST = SupervisedPackage.MINING_TEST_TASK__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Input Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__INPUT_MODEL = SupervisedPackage.MINING_TEST_TASK__INPUT_MODEL;

	/**
	 * The feature id for the '<em><b>Input Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__INPUT_DATA = SupervisedPackage.MINING_TEST_TASK__INPUT_DATA;

	/**
	 * The feature id for the '<em><b>Model Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__MODEL_ASSIGNMENT = SupervisedPackage.MINING_TEST_TASK__MODEL_ASSIGNMENT;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__SCHEMA = SupervisedPackage.MINING_TEST_TASK__SCHEMA;

	/**
	 * The feature id for the '<em><b>Compute Lift</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__COMPUTE_LIFT = SupervisedPackage.MINING_TEST_TASK__COMPUTE_LIFT;

	/**
	 * The feature id for the '<em><b>Positive Target Category</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__POSITIVE_TARGET_CATEGORY = SupervisedPackage.MINING_TEST_TASK__POSITIVE_TARGET_CATEGORY;

	/**
	 * The feature id for the '<em><b>Test Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK__TEST_RESULT = SupervisedPackage.MINING_TEST_TASK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Test Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFICATION_TEST_TASK_FEATURE_COUNT = SupervisedPackage.MINING_TEST_TASK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl <em>Prior Probabilities</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getPriorProbabilities()
	 * @generated
	 */
	int PRIOR_PROBABILITIES = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Usage</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__USAGE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Prior</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES__PRIOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Prior Probabilities</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl <em>Prior Probabilities Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl
	 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getPriorProbabilitiesEntry()
	 * @generated
	 */
	int PRIOR_PROBABILITIES_ENTRY = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Prior Probability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Priors</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__PRIORS = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Prior Probabilities Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIOR_PROBABILITIES_ENTRY_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem <em>Apply Target Value Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Target Value Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem
	 * @generated
	 */
	EClass getApplyTargetValueItem();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem#getTargetValue <em>Target Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Value</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem#getTargetValue()
	 * @see #getApplyTargetValueItem()
	 * @generated
	 */
	EReference getApplyTargetValueItem_TargetValue();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage <em>Attribute Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Usage</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage
	 * @generated
	 */
	EClass getClassificationAttributeUsage();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPositiveCategory <em>Positive Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Positive Category</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPositiveCategory()
	 * @see #getClassificationAttributeUsage()
	 * @generated
	 */
	EReference getClassificationAttributeUsage_PositiveCategory();

	/**
	 * Returns the meta object for the containment reference '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors <em>Priors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Priors</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors()
	 * @see #getClassificationAttributeUsage()
	 * @generated
	 */
	EReference getClassificationAttributeUsage_Priors();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings <em>Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings
	 * @generated
	 */
	EClass getClassificationFunctionSettings();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings#getCostMatrix <em>Cost Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Cost Matrix</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings#getCostMatrix()
	 * @see #getClassificationFunctionSettings()
	 * @generated
	 */
	EReference getClassificationFunctionSettings_CostMatrix();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Result</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult
	 * @generated
	 */
	EClass getClassificationTestResult();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getAccuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accuracy</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getAccuracy()
	 * @see #getClassificationTestResult()
	 * @generated
	 */
	EAttribute getClassificationTestResult_Accuracy();

	/**
	 * Returns the meta object for the containment reference '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix <em>Confusion Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Confusion Matrix</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix()
	 * @see #getClassificationTestResult()
	 * @generated
	 */
	EReference getClassificationTestResult_ConfusionMatrix();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getTestTask <em>Test Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Test Task</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getTestTask()
	 * @see #getClassificationTestResult()
	 * @generated
	 */
	EReference getClassificationTestResult_TestTask();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestTask <em>Test Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Task</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestTask
	 * @generated
	 */
	EClass getClassificationTestTask();

	/**
	 * Returns the meta object for the containment reference '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestTask#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Result</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestTask#getTestResult()
	 * @see #getClassificationTestTask()
	 * @generated
	 */
	EReference getClassificationTestTask_TestResult();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities <em>Prior Probabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prior Probabilities</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities
	 * @generated
	 */
	EClass getPriorProbabilities();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage <em>Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Usage</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage()
	 * @see #getPriorProbabilities()
	 * @generated
	 */
	EReference getPriorProbabilities_Usage();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getPrior <em>Prior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Prior</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getPrior()
	 * @see #getPriorProbabilities()
	 * @generated
	 */
	EReference getPriorProbabilities_Prior();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry <em>Prior Probabilities Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prior Probabilities Entry</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry
	 * @generated
	 */
	EClass getPriorProbabilitiesEntry();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriorProbability <em>Prior Probability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prior Probability</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriorProbability()
	 * @see #getPriorProbabilitiesEntry()
	 * @generated
	 */
	EAttribute getPriorProbabilitiesEntry_PriorProbability();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors <em>Priors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Priors</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors()
	 * @see #getPriorProbabilitiesEntry()
	 * @generated
	 */
	EReference getPriorProbabilitiesEntry_Priors();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getTargetValue <em>Target Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Value</em>'.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getTargetValue()
	 * @see #getPriorProbabilitiesEntry()
	 * @generated
	 */
	EReference getPriorProbabilitiesEntry_TargetValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassificationFactory getClassificationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ApplyTargetValueItemImpl <em>Apply Target Value Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ApplyTargetValueItemImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getApplyTargetValueItem()
		 * @generated
		 */
		EClass APPLY_TARGET_VALUE_ITEM = eINSTANCE.getApplyTargetValueItem();

		/**
		 * The meta object literal for the '<em><b>Target Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLY_TARGET_VALUE_ITEM__TARGET_VALUE = eINSTANCE.getApplyTargetValueItem_TargetValue();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationAttributeUsageImpl <em>Attribute Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationAttributeUsageImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationAttributeUsage()
		 * @generated
		 */
		EClass CLASSIFICATION_ATTRIBUTE_USAGE = eINSTANCE.getClassificationAttributeUsage();

		/**
		 * The meta object literal for the '<em><b>Positive Category</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_ATTRIBUTE_USAGE__POSITIVE_CATEGORY = eINSTANCE.getClassificationAttributeUsage_PositiveCategory();

		/**
		 * The meta object literal for the '<em><b>Priors</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_ATTRIBUTE_USAGE__PRIORS = eINSTANCE.getClassificationAttributeUsage_Priors();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationFunctionSettingsImpl <em>Function Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationFunctionSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationFunctionSettings()
		 * @generated
		 */
		EClass CLASSIFICATION_FUNCTION_SETTINGS = eINSTANCE.getClassificationFunctionSettings();

		/**
		 * The meta object literal for the '<em><b>Cost Matrix</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_FUNCTION_SETTINGS__COST_MATRIX = eINSTANCE.getClassificationFunctionSettings_CostMatrix();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl <em>Test Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestResultImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationTestResult()
		 * @generated
		 */
		EClass CLASSIFICATION_TEST_RESULT = eINSTANCE.getClassificationTestResult();

		/**
		 * The meta object literal for the '<em><b>Accuracy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASSIFICATION_TEST_RESULT__ACCURACY = eINSTANCE.getClassificationTestResult_Accuracy();

		/**
		 * The meta object literal for the '<em><b>Confusion Matrix</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX = eINSTANCE.getClassificationTestResult_ConfusionMatrix();

		/**
		 * The meta object literal for the '<em><b>Test Task</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_TEST_RESULT__TEST_TASK = eINSTANCE.getClassificationTestResult_TestTask();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestTaskImpl <em>Test Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationTestTaskImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getClassificationTestTask()
		 * @generated
		 */
		EClass CLASSIFICATION_TEST_TASK = eINSTANCE.getClassificationTestTask();

		/**
		 * The meta object literal for the '<em><b>Test Result</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFICATION_TEST_TASK__TEST_RESULT = eINSTANCE.getClassificationTestTask_TestResult();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl <em>Prior Probabilities</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getPriorProbabilities()
		 * @generated
		 */
		EClass PRIOR_PROBABILITIES = eINSTANCE.getPriorProbabilities();

		/**
		 * The meta object literal for the '<em><b>Usage</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIOR_PROBABILITIES__USAGE = eINSTANCE.getPriorProbabilities_Usage();

		/**
		 * The meta object literal for the '<em><b>Prior</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIOR_PROBABILITIES__PRIOR = eINSTANCE.getPriorProbabilities_Prior();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl <em>Prior Probabilities Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl
		 * @see orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl#getPriorProbabilitiesEntry()
		 * @generated
		 */
		EClass PRIOR_PROBABILITIES_ENTRY = eINSTANCE.getPriorProbabilitiesEntry();

		/**
		 * The meta object literal for the '<em><b>Prior Probability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY = eINSTANCE.getPriorProbabilitiesEntry_PriorProbability();

		/**
		 * The meta object literal for the '<em><b>Priors</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIOR_PROBABILITIES_ENTRY__PRIORS = eINSTANCE.getPriorProbabilitiesEntry_Priors();

		/**
		 * The meta object literal for the '<em><b>Target Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE = eINSTANCE.getPriorProbabilitiesEntry_TargetValue();

	}

} //ClassificationPackage
