/**
 * INRIA/IRISA
 *
 * $Id: SupervisedFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings;
import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SupervisedFunctionSettingsImpl extends MiningFunctionSettingsImpl implements SupervisedFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SupervisedFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SupervisedPackage.Literals.SUPERVISED_FUNCTION_SETTINGS;
	}

} //SupervisedFunctionSettingsImpl
