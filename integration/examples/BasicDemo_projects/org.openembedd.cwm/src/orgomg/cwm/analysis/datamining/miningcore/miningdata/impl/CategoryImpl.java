/**
 * INRIA/IRISA
 *
 * $Id: CategoryImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryProperty;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#getValue <em>Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#isIsNullCategory <em>Is Null Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#getDisplayName <em>Display Name</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#getPrior <em>Prior</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryImpl#getCategoricalProperties <em>Categorical Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryImpl extends ModelElementImpl implements Category {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsNullCategory() <em>Is Null Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsNullCategory()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_NULL_CATEGORY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsNullCategory() <em>Is Null Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsNullCategory()
	 * @generated
	 * @ordered
	 */
	protected boolean isNullCategory = IS_NULL_CATEGORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getDisplayName() <em>Display Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayName()
	 * @generated
	 * @ordered
	 */
	protected static final String DISPLAY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDisplayName() <em>Display Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplayName()
	 * @generated
	 * @ordered
	 */
	protected String displayName = DISPLAY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getProperty() <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected static final CategoryProperty PROPERTY_EDEFAULT = CategoryProperty.VALID_LITERAL;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected CategoryProperty property = PROPERTY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrior() <em>Prior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrior()
	 * @generated
	 * @ordered
	 */
	protected static final String PRIOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrior() <em>Prior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrior()
	 * @generated
	 * @ordered
	 */
	protected String prior = PRIOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsNullCategory() {
		return isNullCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsNullCategory(boolean newIsNullCategory) {
		boolean oldIsNullCategory = isNullCategory;
		isNullCategory = newIsNullCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__IS_NULL_CATEGORY, oldIsNullCategory, isNullCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayName(String newDisplayName) {
		String oldDisplayName = displayName;
		displayName = newDisplayName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__DISPLAY_NAME, oldDisplayName, displayName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryProperty getProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(CategoryProperty newProperty) {
		CategoryProperty oldProperty = property;
		property = newProperty == null ? PROPERTY_EDEFAULT : newProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__PROPERTY, oldProperty, property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrior() {
		return prior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrior(String newPrior) {
		String oldPrior = prior;
		prior = newPrior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__PRIOR, oldPrior, prior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoricalAttributeProperties getCategoricalProperties() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES) return null;
		return (CategoricalAttributeProperties)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCategoricalProperties(CategoricalAttributeProperties newCategoricalProperties, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCategoricalProperties, MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategoricalProperties(CategoricalAttributeProperties newCategoricalProperties) {
		if (newCategoricalProperties != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES && newCategoricalProperties != null)) {
			if (EcoreUtil.isAncestor(this, newCategoricalProperties))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCategoricalProperties != null)
				msgs = ((InternalEObject)newCategoricalProperties).eInverseAdd(this, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY, CategoricalAttributeProperties.class, msgs);
			msgs = basicSetCategoricalProperties(newCategoricalProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES, newCategoricalProperties, newCategoricalProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCategoricalProperties((CategoricalAttributeProperties)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				return basicSetCategoricalProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES__CATEGORY, CategoricalAttributeProperties.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__VALUE:
				return getValue();
			case MiningdataPackage.CATEGORY__IS_NULL_CATEGORY:
				return isIsNullCategory() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.CATEGORY__DISPLAY_NAME:
				return getDisplayName();
			case MiningdataPackage.CATEGORY__PROPERTY:
				return getProperty();
			case MiningdataPackage.CATEGORY__PRIOR:
				return getPrior();
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				return getCategoricalProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__VALUE:
				setValue((String)newValue);
				return;
			case MiningdataPackage.CATEGORY__IS_NULL_CATEGORY:
				setIsNullCategory(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.CATEGORY__DISPLAY_NAME:
				setDisplayName((String)newValue);
				return;
			case MiningdataPackage.CATEGORY__PROPERTY:
				setProperty((CategoryProperty)newValue);
				return;
			case MiningdataPackage.CATEGORY__PRIOR:
				setPrior((String)newValue);
				return;
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				setCategoricalProperties((CategoricalAttributeProperties)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY__IS_NULL_CATEGORY:
				setIsNullCategory(IS_NULL_CATEGORY_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY__DISPLAY_NAME:
				setDisplayName(DISPLAY_NAME_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY__PROPERTY:
				setProperty(PROPERTY_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY__PRIOR:
				setPrior(PRIOR_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				setCategoricalProperties((CategoricalAttributeProperties)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case MiningdataPackage.CATEGORY__IS_NULL_CATEGORY:
				return isNullCategory != IS_NULL_CATEGORY_EDEFAULT;
			case MiningdataPackage.CATEGORY__DISPLAY_NAME:
				return DISPLAY_NAME_EDEFAULT == null ? displayName != null : !DISPLAY_NAME_EDEFAULT.equals(displayName);
			case MiningdataPackage.CATEGORY__PROPERTY:
				return property != PROPERTY_EDEFAULT;
			case MiningdataPackage.CATEGORY__PRIOR:
				return PRIOR_EDEFAULT == null ? prior != null : !PRIOR_EDEFAULT.equals(prior);
			case MiningdataPackage.CATEGORY__CATEGORICAL_PROPERTIES:
				return getCategoricalProperties() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", isNullCategory: ");
		result.append(isNullCategory);
		result.append(", displayName: ");
		result.append(displayName);
		result.append(", property: ");
		result.append(property);
		result.append(", prior: ");
		result.append(prior);
		result.append(')');
		return result.toString();
	}

} //CategoryImpl
