/**
 * INRIA/IRISA
 *
 * $Id: MiningtaskPackageImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import orgomg.cwm.analysis.businessnomenclature.BusinessnomenclaturePackage;

import orgomg.cwm.analysis.businessnomenclature.impl.BusinessnomenclaturePackageImpl;

import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;

import orgomg.cwm.analysis.datamining.approximation.impl.ApproximationPackageImpl;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;

import orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage;

import orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;

import orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl;

import orgomg.cwm.analysis.datamining.clustering.ClusteringPackage;

import orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.EntrypointPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningfunctionsettingsPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.MiningmodelPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningresultPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.impl.MiningresultPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyProbabilityItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyRuleIdItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyScoreItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplySourceItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTransformation;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskFactory;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedPackageImpl;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage;

import orgomg.cwm.analysis.informationvisualization.impl.InformationvisualizationPackageImpl;

import orgomg.cwm.analysis.olap.OlapPackage;

import orgomg.cwm.analysis.olap.impl.OlapPackageImpl;

import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.analysis.transformation.impl.TransformationPackageImpl;

import orgomg.cwm.foundation.businessinformation.BusinessinformationPackage;

import orgomg.cwm.foundation.businessinformation.impl.BusinessinformationPackageImpl;

import orgomg.cwm.foundation.datatypes.DatatypesPackage;

import orgomg.cwm.foundation.datatypes.impl.DatatypesPackageImpl;

import orgomg.cwm.foundation.expressions.ExpressionsPackage;

import orgomg.cwm.foundation.expressions.impl.ExpressionsPackageImpl;

import orgomg.cwm.foundation.keysindexes.KeysindexesPackage;

import orgomg.cwm.foundation.keysindexes.impl.KeysindexesPackageImpl;

import orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage;

import orgomg.cwm.foundation.softwaredeployment.impl.SoftwaredeploymentPackageImpl;

import orgomg.cwm.foundation.typemapping.TypemappingPackage;

import orgomg.cwm.foundation.typemapping.impl.TypemappingPackageImpl;

import orgomg.cwm.management.warehouseoperation.WarehouseoperationPackage;

import orgomg.cwm.management.warehouseoperation.impl.WarehouseoperationPackageImpl;

import orgomg.cwm.management.warehouseprocess.WarehouseprocessPackage;

import orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage;

import orgomg.cwm.management.warehouseprocess.datatype.impl.DatatypePackageImpl;

import orgomg.cwm.management.warehouseprocess.events.EventsPackage;

import orgomg.cwm.management.warehouseprocess.events.impl.EventsPackageImpl;

import orgomg.cwm.management.warehouseprocess.impl.WarehouseprocessPackageImpl;

import orgomg.cwm.objectmodel.behavioral.BehavioralPackage;

import orgomg.cwm.objectmodel.behavioral.impl.BehavioralPackageImpl;

import orgomg.cwm.objectmodel.core.CorePackage;

import orgomg.cwm.objectmodel.core.impl.CorePackageImpl;

import orgomg.cwm.objectmodel.instance.InstancePackage;

import orgomg.cwm.objectmodel.instance.impl.InstancePackageImpl;

import orgomg.cwm.objectmodel.relationships.RelationshipsPackage;

import orgomg.cwm.objectmodel.relationships.impl.RelationshipsPackageImpl;

import orgomg.cwm.resource.multidimensional.MultidimensionalPackage;

import orgomg.cwm.resource.multidimensional.impl.MultidimensionalPackageImpl;

import orgomg.cwm.resource.record.RecordPackage;

import orgomg.cwm.resource.record.impl.RecordPackageImpl;

import orgomg.cwm.resource.relational.RelationalPackage;

import orgomg.cwm.resource.relational.enumerations.EnumerationsPackage;

import orgomg.cwm.resource.relational.enumerations.impl.EnumerationsPackageImpl;

import orgomg.cwm.resource.relational.impl.RelationalPackageImpl;

import orgomg.cwm.resource.xml.XmlPackage;

import orgomg.cwm.resource.xml.impl.XmlPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiningtaskPackageImpl extends EPackageImpl implements MiningtaskPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applyContentItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applyOutputItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applyProbabilityItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applyRuleIdItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applyScoreItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applySourceItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningApplyOutputEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningApplyTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningBuildTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miningTransformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum applyOutputOptionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MiningtaskPackageImpl() {
		super(eNS_URI, MiningtaskFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MiningtaskPackage init() {
		if (isInited) return (MiningtaskPackage)EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI);

		// Obtain or create and register package
		MiningtaskPackageImpl theMiningtaskPackage = (MiningtaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof MiningtaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new MiningtaskPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		BehavioralPackageImpl theBehavioralPackage = (BehavioralPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) instanceof BehavioralPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) : BehavioralPackage.eINSTANCE);
		RelationshipsPackageImpl theRelationshipsPackage = (RelationshipsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) instanceof RelationshipsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) : RelationshipsPackage.eINSTANCE);
		InstancePackageImpl theInstancePackage = (InstancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) instanceof InstancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) : InstancePackage.eINSTANCE);
		BusinessinformationPackageImpl theBusinessinformationPackage = (BusinessinformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) instanceof BusinessinformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) : BusinessinformationPackage.eINSTANCE);
		DatatypesPackageImpl theDatatypesPackage = (DatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) instanceof DatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) : DatatypesPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		KeysindexesPackageImpl theKeysindexesPackage = (KeysindexesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) instanceof KeysindexesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) : KeysindexesPackage.eINSTANCE);
		SoftwaredeploymentPackageImpl theSoftwaredeploymentPackage = (SoftwaredeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) instanceof SoftwaredeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) : SoftwaredeploymentPackage.eINSTANCE);
		TypemappingPackageImpl theTypemappingPackage = (TypemappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) instanceof TypemappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) : TypemappingPackage.eINSTANCE);
		RelationalPackageImpl theRelationalPackage = (RelationalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) instanceof RelationalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) : RelationalPackage.eINSTANCE);
		EnumerationsPackageImpl theEnumerationsPackage = (EnumerationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) instanceof EnumerationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) : EnumerationsPackage.eINSTANCE);
		RecordPackageImpl theRecordPackage = (RecordPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) instanceof RecordPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) : RecordPackage.eINSTANCE);
		MultidimensionalPackageImpl theMultidimensionalPackage = (MultidimensionalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) instanceof MultidimensionalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) : MultidimensionalPackage.eINSTANCE);
		XmlPackageImpl theXmlPackage = (XmlPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) instanceof XmlPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) : XmlPackage.eINSTANCE);
		TransformationPackageImpl theTransformationPackage = (TransformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) instanceof TransformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) : TransformationPackage.eINSTANCE);
		OlapPackageImpl theOlapPackage = (OlapPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) instanceof OlapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) : OlapPackage.eINSTANCE);
		MiningdataPackageImpl theMiningdataPackage = (MiningdataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) instanceof MiningdataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) : MiningdataPackage.eINSTANCE);
		MiningfunctionsettingsPackageImpl theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) instanceof MiningfunctionsettingsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) : MiningfunctionsettingsPackage.eINSTANCE);
		MiningmodelPackageImpl theMiningmodelPackage = (MiningmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) instanceof MiningmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) : MiningmodelPackage.eINSTANCE);
		MiningresultPackageImpl theMiningresultPackage = (MiningresultPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) instanceof MiningresultPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) : MiningresultPackage.eINSTANCE);
		EntrypointPackageImpl theEntrypointPackage = (EntrypointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) instanceof EntrypointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) : EntrypointPackage.eINSTANCE);
		ClusteringPackageImpl theClusteringPackage = (ClusteringPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) instanceof ClusteringPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) : ClusteringPackage.eINSTANCE);
		AssociationrulesPackageImpl theAssociationrulesPackage = (AssociationrulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) instanceof AssociationrulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) : AssociationrulesPackage.eINSTANCE);
		SupervisedPackageImpl theSupervisedPackage = (SupervisedPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) instanceof SupervisedPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) : SupervisedPackage.eINSTANCE);
		ClassificationPackageImpl theClassificationPackage = (ClassificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) instanceof ClassificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) : ClassificationPackage.eINSTANCE);
		ApproximationPackageImpl theApproximationPackage = (ApproximationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) instanceof ApproximationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) : ApproximationPackage.eINSTANCE);
		AttributeimportancePackageImpl theAttributeimportancePackage = (AttributeimportancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) instanceof AttributeimportancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) : AttributeimportancePackage.eINSTANCE);
		InformationvisualizationPackageImpl theInformationvisualizationPackage = (InformationvisualizationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) instanceof InformationvisualizationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) : InformationvisualizationPackage.eINSTANCE);
		BusinessnomenclaturePackageImpl theBusinessnomenclaturePackage = (BusinessnomenclaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) instanceof BusinessnomenclaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) : BusinessnomenclaturePackage.eINSTANCE);
		WarehouseprocessPackageImpl theWarehouseprocessPackage = (WarehouseprocessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) instanceof WarehouseprocessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) : WarehouseprocessPackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		EventsPackageImpl theEventsPackage = (EventsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) instanceof EventsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) : EventsPackage.eINSTANCE);
		WarehouseoperationPackageImpl theWarehouseoperationPackage = (WarehouseoperationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) instanceof WarehouseoperationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) : WarehouseoperationPackage.eINSTANCE);

		// Create package meta-data objects
		theMiningtaskPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theBehavioralPackage.createPackageContents();
		theRelationshipsPackage.createPackageContents();
		theInstancePackage.createPackageContents();
		theBusinessinformationPackage.createPackageContents();
		theDatatypesPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theKeysindexesPackage.createPackageContents();
		theSoftwaredeploymentPackage.createPackageContents();
		theTypemappingPackage.createPackageContents();
		theRelationalPackage.createPackageContents();
		theEnumerationsPackage.createPackageContents();
		theRecordPackage.createPackageContents();
		theMultidimensionalPackage.createPackageContents();
		theXmlPackage.createPackageContents();
		theTransformationPackage.createPackageContents();
		theOlapPackage.createPackageContents();
		theMiningdataPackage.createPackageContents();
		theMiningfunctionsettingsPackage.createPackageContents();
		theMiningmodelPackage.createPackageContents();
		theMiningresultPackage.createPackageContents();
		theEntrypointPackage.createPackageContents();
		theClusteringPackage.createPackageContents();
		theAssociationrulesPackage.createPackageContents();
		theSupervisedPackage.createPackageContents();
		theClassificationPackage.createPackageContents();
		theApproximationPackage.createPackageContents();
		theAttributeimportancePackage.createPackageContents();
		theInformationvisualizationPackage.createPackageContents();
		theBusinessnomenclaturePackage.createPackageContents();
		theWarehouseprocessPackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theEventsPackage.createPackageContents();
		theWarehouseoperationPackage.createPackageContents();

		// Initialize created meta-data
		theMiningtaskPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theBehavioralPackage.initializePackageContents();
		theRelationshipsPackage.initializePackageContents();
		theInstancePackage.initializePackageContents();
		theBusinessinformationPackage.initializePackageContents();
		theDatatypesPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theKeysindexesPackage.initializePackageContents();
		theSoftwaredeploymentPackage.initializePackageContents();
		theTypemappingPackage.initializePackageContents();
		theRelationalPackage.initializePackageContents();
		theEnumerationsPackage.initializePackageContents();
		theRecordPackage.initializePackageContents();
		theMultidimensionalPackage.initializePackageContents();
		theXmlPackage.initializePackageContents();
		theTransformationPackage.initializePackageContents();
		theOlapPackage.initializePackageContents();
		theMiningdataPackage.initializePackageContents();
		theMiningfunctionsettingsPackage.initializePackageContents();
		theMiningmodelPackage.initializePackageContents();
		theMiningresultPackage.initializePackageContents();
		theEntrypointPackage.initializePackageContents();
		theClusteringPackage.initializePackageContents();
		theAssociationrulesPackage.initializePackageContents();
		theSupervisedPackage.initializePackageContents();
		theClassificationPackage.initializePackageContents();
		theApproximationPackage.initializePackageContents();
		theAttributeimportancePackage.initializePackageContents();
		theInformationvisualizationPackage.initializePackageContents();
		theBusinessnomenclaturePackage.initializePackageContents();
		theWarehouseprocessPackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theEventsPackage.initializePackageContents();
		theWarehouseoperationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMiningtaskPackage.freeze();

		return theMiningtaskPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplyContentItem() {
		return applyContentItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApplyContentItem_TopNthIndex() {
		return (EAttribute)applyContentItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplyOutputItem() {
		return applyOutputItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplyOutputItem_ApplyOutput() {
		return (EReference)applyOutputItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplyProbabilityItem() {
		return applyProbabilityItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplyRuleIdItem() {
		return applyRuleIdItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplyScoreItem() {
		return applyScoreItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplySourceItem() {
		return applySourceItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningApplyOutput() {
		return miningApplyOutputEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningApplyOutput_Item() {
		return (EReference)miningApplyOutputEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningApplyTask() {
		return miningApplyTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMiningApplyTask_OutputOption() {
		return (EAttribute)miningApplyTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningApplyTask_OutputAssignment() {
		return (EReference)miningApplyTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningApplyTask_ApplyOutput() {
		return (EReference)miningApplyTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningBuildTask() {
		return miningBuildTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningBuildTask_ResultModel() {
		return (EReference)miningBuildTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningBuildTask_MiningSettings() {
		return (EReference)miningBuildTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningBuildTask_ValidationAssignment() {
		return (EReference)miningBuildTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningBuildTask_SettingsAssignment() {
		return (EReference)miningBuildTaskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningBuildTask_ValidationData() {
		return (EReference)miningBuildTaskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningTask() {
		return miningTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningTask_InputModel() {
		return (EReference)miningTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningTask_InputData() {
		return (EReference)miningTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningTask_ModelAssignment() {
		return (EReference)miningTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningTask_Schema() {
		return (EReference)miningTaskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiningTransformation() {
		return miningTransformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiningTransformation_Procedure() {
		return (EReference)miningTransformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getApplyOutputOption() {
		return applyOutputOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningtaskFactory getMiningtaskFactory() {
		return (MiningtaskFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		applyContentItemEClass = createEClass(APPLY_CONTENT_ITEM);
		createEAttribute(applyContentItemEClass, APPLY_CONTENT_ITEM__TOP_NTH_INDEX);

		applyOutputItemEClass = createEClass(APPLY_OUTPUT_ITEM);
		createEReference(applyOutputItemEClass, APPLY_OUTPUT_ITEM__APPLY_OUTPUT);

		applyProbabilityItemEClass = createEClass(APPLY_PROBABILITY_ITEM);

		applyRuleIdItemEClass = createEClass(APPLY_RULE_ID_ITEM);

		applyScoreItemEClass = createEClass(APPLY_SCORE_ITEM);

		applySourceItemEClass = createEClass(APPLY_SOURCE_ITEM);

		miningApplyOutputEClass = createEClass(MINING_APPLY_OUTPUT);
		createEReference(miningApplyOutputEClass, MINING_APPLY_OUTPUT__ITEM);

		miningApplyTaskEClass = createEClass(MINING_APPLY_TASK);
		createEAttribute(miningApplyTaskEClass, MINING_APPLY_TASK__OUTPUT_OPTION);
		createEReference(miningApplyTaskEClass, MINING_APPLY_TASK__OUTPUT_ASSIGNMENT);
		createEReference(miningApplyTaskEClass, MINING_APPLY_TASK__APPLY_OUTPUT);

		miningBuildTaskEClass = createEClass(MINING_BUILD_TASK);
		createEReference(miningBuildTaskEClass, MINING_BUILD_TASK__RESULT_MODEL);
		createEReference(miningBuildTaskEClass, MINING_BUILD_TASK__MINING_SETTINGS);
		createEReference(miningBuildTaskEClass, MINING_BUILD_TASK__VALIDATION_ASSIGNMENT);
		createEReference(miningBuildTaskEClass, MINING_BUILD_TASK__SETTINGS_ASSIGNMENT);
		createEReference(miningBuildTaskEClass, MINING_BUILD_TASK__VALIDATION_DATA);

		miningTaskEClass = createEClass(MINING_TASK);
		createEReference(miningTaskEClass, MINING_TASK__INPUT_MODEL);
		createEReference(miningTaskEClass, MINING_TASK__INPUT_DATA);
		createEReference(miningTaskEClass, MINING_TASK__MODEL_ASSIGNMENT);
		createEReference(miningTaskEClass, MINING_TASK__SCHEMA);

		miningTransformationEClass = createEClass(MINING_TRANSFORMATION);
		createEReference(miningTransformationEClass, MINING_TRANSFORMATION__PROCEDURE);

		// Create enums
		applyOutputOptionEEnum = createEEnum(APPLY_OUTPUT_OPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		MiningdataPackage theMiningdataPackage = (MiningdataPackage)EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI);
		MiningmodelPackage theMiningmodelPackage = (MiningmodelPackage)EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI);
		MiningfunctionsettingsPackage theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackage)EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI);
		EntrypointPackage theEntrypointPackage = (EntrypointPackage)EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI);
		TransformationPackage theTransformationPackage = (TransformationPackage)EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		applyContentItemEClass.getESuperTypes().add(this.getApplyOutputItem());
		applyOutputItemEClass.getESuperTypes().add(theMiningdataPackage.getMiningAttribute());
		applyProbabilityItemEClass.getESuperTypes().add(this.getApplyContentItem());
		applyRuleIdItemEClass.getESuperTypes().add(this.getApplyContentItem());
		applyScoreItemEClass.getESuperTypes().add(this.getApplyContentItem());
		applySourceItemEClass.getESuperTypes().add(this.getApplyOutputItem());
		miningApplyOutputEClass.getESuperTypes().add(theCorePackage.getModelElement());
		miningApplyTaskEClass.getESuperTypes().add(this.getMiningTask());
		miningBuildTaskEClass.getESuperTypes().add(this.getMiningTask());
		miningTaskEClass.getESuperTypes().add(theCorePackage.getModelElement());
		miningTransformationEClass.getESuperTypes().add(theTransformationPackage.getTransformation());

		// Initialize classes and features; add operations and parameters
		initEClass(applyContentItemEClass, ApplyContentItem.class, "ApplyContentItem", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getApplyContentItem_TopNthIndex(), theCorePackage.getInteger(), "topNthIndex", "1", 0, 1, ApplyContentItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applyOutputItemEClass, ApplyOutputItem.class, "ApplyOutputItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApplyOutputItem_ApplyOutput(), this.getMiningApplyOutput(), this.getMiningApplyOutput_Item(), "applyOutput", null, 1, 1, ApplyOutputItem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applyProbabilityItemEClass, ApplyProbabilityItem.class, "ApplyProbabilityItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(applyRuleIdItemEClass, ApplyRuleIdItem.class, "ApplyRuleIdItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(applyScoreItemEClass, ApplyScoreItem.class, "ApplyScoreItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(applySourceItemEClass, ApplySourceItem.class, "ApplySourceItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(miningApplyOutputEClass, MiningApplyOutput.class, "MiningApplyOutput", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMiningApplyOutput_Item(), this.getApplyOutputItem(), this.getApplyOutputItem_ApplyOutput(), "item", null, 1, -1, MiningApplyOutput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miningApplyTaskEClass, MiningApplyTask.class, "MiningApplyTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMiningApplyTask_OutputOption(), this.getApplyOutputOption(), "outputOption", "createNew", 0, 1, MiningApplyTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningApplyTask_OutputAssignment(), theMiningdataPackage.getAttributeAssignmentSet(), null, "outputAssignment", null, 1, 1, MiningApplyTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningApplyTask_ApplyOutput(), this.getMiningApplyOutput(), null, "applyOutput", null, 1, 1, MiningApplyTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miningBuildTaskEClass, MiningBuildTask.class, "MiningBuildTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMiningBuildTask_ResultModel(), theMiningmodelPackage.getMiningModel(), null, "resultModel", null, 0, 1, MiningBuildTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningBuildTask_MiningSettings(), theMiningfunctionsettingsPackage.getMiningFunctionSettings(), null, "miningSettings", null, 1, 1, MiningBuildTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningBuildTask_ValidationAssignment(), theMiningdataPackage.getAttributeAssignmentSet(), null, "validationAssignment", null, 0, 1, MiningBuildTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningBuildTask_SettingsAssignment(), theMiningdataPackage.getAttributeAssignmentSet(), null, "settingsAssignment", null, 0, 1, MiningBuildTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningBuildTask_ValidationData(), theMiningdataPackage.getPhysicalData(), null, "validationData", null, 0, 1, MiningBuildTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miningTaskEClass, MiningTask.class, "MiningTask", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMiningTask_InputModel(), theMiningmodelPackage.getMiningModel(), null, "inputModel", null, 0, 1, MiningTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningTask_InputData(), theMiningdataPackage.getPhysicalData(), null, "inputData", null, 1, 1, MiningTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningTask_ModelAssignment(), theMiningdataPackage.getAttributeAssignmentSet(), null, "modelAssignment", null, 0, 1, MiningTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMiningTask_Schema(), theEntrypointPackage.getSchema(), theEntrypointPackage.getSchema_MiningTask(), "schema", null, 1, 1, MiningTask.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miningTransformationEClass, MiningTransformation.class, "MiningTransformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMiningTransformation_Procedure(), this.getMiningTask(), null, "procedure", null, 1, 1, MiningTransformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(applyOutputOptionEEnum, ApplyOutputOption.class, "ApplyOutputOption");
		addEEnumLiteral(applyOutputOptionEEnum, ApplyOutputOption.APPEND_TO_EXISTING_LITERAL);
		addEEnumLiteral(applyOutputOptionEEnum, ApplyOutputOption.CREATE_NEW_LITERAL);

		// Create resource
		createResource(eNS_URI);
	}

} //MiningtaskPackageImpl
