/**
 * INRIA/IRISA
 *
 * $Id: ReversePivotAttributeAssignmentImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reverse Pivot Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl#getAttributeSelectionFunction <em>Attribute Selection Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl#getValueSelectionFunction <em>Value Selection Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.ReversePivotAttributeAssignmentImpl#getSelectorAttribute <em>Selector Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReversePivotAttributeAssignmentImpl extends AttributeAssignmentImpl implements ReversePivotAttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getAttributeSelectionFunction() <em>Attribute Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeSelectionFunction()
	 * @generated
	 * @ordered
	 */
	protected static final AttributeSelectionFunction ATTRIBUTE_SELECTION_FUNCTION_EDEFAULT = AttributeSelectionFunction.IS_NOT_NULL_LITERAL;

	/**
	 * The cached value of the '{@link #getAttributeSelectionFunction() <em>Attribute Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeSelectionFunction()
	 * @generated
	 * @ordered
	 */
	protected AttributeSelectionFunction attributeSelectionFunction = ATTRIBUTE_SELECTION_FUNCTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueSelectionFunction() <em>Value Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueSelectionFunction()
	 * @generated
	 * @ordered
	 */
	protected static final ValueSelectionFunction VALUE_SELECTION_FUNCTION_EDEFAULT = ValueSelectionFunction.VSF_VALUE_LITERAL;

	/**
	 * The cached value of the '{@link #getValueSelectionFunction() <em>Value Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueSelectionFunction()
	 * @generated
	 * @ordered
	 */
	protected ValueSelectionFunction valueSelectionFunction = VALUE_SELECTION_FUNCTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSelectorAttribute() <em>Selector Attribute</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectorAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> selectorAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReversePivotAttributeAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeSelectionFunction getAttributeSelectionFunction() {
		return attributeSelectionFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeSelectionFunction(AttributeSelectionFunction newAttributeSelectionFunction) {
		AttributeSelectionFunction oldAttributeSelectionFunction = attributeSelectionFunction;
		attributeSelectionFunction = newAttributeSelectionFunction == null ? ATTRIBUTE_SELECTION_FUNCTION_EDEFAULT : newAttributeSelectionFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION, oldAttributeSelectionFunction, attributeSelectionFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueSelectionFunction getValueSelectionFunction() {
		return valueSelectionFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueSelectionFunction(ValueSelectionFunction newValueSelectionFunction) {
		ValueSelectionFunction oldValueSelectionFunction = valueSelectionFunction;
		valueSelectionFunction = newValueSelectionFunction == null ? VALUE_SELECTION_FUNCTION_EDEFAULT : newValueSelectionFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION, oldValueSelectionFunction, valueSelectionFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getSelectorAttribute() {
		if (selectorAttribute == null) {
			selectorAttribute = new EObjectResolvingEList<Attribute>(Attribute.class, this, MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE);
		}
		return selectorAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION:
				return getAttributeSelectionFunction();
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION:
				return getValueSelectionFunction();
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE:
				return getSelectorAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION:
				setAttributeSelectionFunction((AttributeSelectionFunction)newValue);
				return;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION:
				setValueSelectionFunction((ValueSelectionFunction)newValue);
				return;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE:
				getSelectorAttribute().clear();
				getSelectorAttribute().addAll((Collection<? extends Attribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION:
				setAttributeSelectionFunction(ATTRIBUTE_SELECTION_FUNCTION_EDEFAULT);
				return;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION:
				setValueSelectionFunction(VALUE_SELECTION_FUNCTION_EDEFAULT);
				return;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE:
				getSelectorAttribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__ATTRIBUTE_SELECTION_FUNCTION:
				return attributeSelectionFunction != ATTRIBUTE_SELECTION_FUNCTION_EDEFAULT;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__VALUE_SELECTION_FUNCTION:
				return valueSelectionFunction != VALUE_SELECTION_FUNCTION_EDEFAULT;
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT__SELECTOR_ATTRIBUTE:
				return selectorAttribute != null && !selectorAttribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (attributeSelectionFunction: ");
		result.append(attributeSelectionFunction);
		result.append(", valueSelectionFunction: ");
		result.append(valueSelectionFunction);
		result.append(')');
		return result.toString();
	}

} //ReversePivotAttributeAssignmentImpl
