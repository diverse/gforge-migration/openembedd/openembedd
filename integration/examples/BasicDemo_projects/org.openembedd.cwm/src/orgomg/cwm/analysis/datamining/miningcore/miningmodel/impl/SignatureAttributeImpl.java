/**
 * INRIA/IRISA
 *
 * $Id: SignatureAttributeImpl.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningmodel.SignatureAttribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signature Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.SignatureAttributeImpl#getUsageOption <em>Usage Option</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SignatureAttributeImpl extends MiningAttributeImpl implements SignatureAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getUsageOption() <em>Usage Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsageOption()
	 * @generated
	 * @ordered
	 */
	protected static final UsageOption USAGE_OPTION_EDEFAULT = UsageOption.ACTIVE_LITERAL;

	/**
	 * The cached value of the '{@link #getUsageOption() <em>Usage Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsageOption()
	 * @generated
	 * @ordered
	 */
	protected UsageOption usageOption = USAGE_OPTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignatureAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningmodelPackage.Literals.SIGNATURE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsageOption getUsageOption() {
		return usageOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsageOption(UsageOption newUsageOption) {
		UsageOption oldUsageOption = usageOption;
		usageOption = newUsageOption == null ? USAGE_OPTION_EDEFAULT : newUsageOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningmodelPackage.SIGNATURE_ATTRIBUTE__USAGE_OPTION, oldUsageOption, usageOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningmodelPackage.SIGNATURE_ATTRIBUTE__USAGE_OPTION:
				return getUsageOption();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningmodelPackage.SIGNATURE_ATTRIBUTE__USAGE_OPTION:
				setUsageOption((UsageOption)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningmodelPackage.SIGNATURE_ATTRIBUTE__USAGE_OPTION:
				setUsageOption(USAGE_OPTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningmodelPackage.SIGNATURE_ATTRIBUTE__USAGE_OPTION:
				return usageOption != USAGE_OPTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (usageOption: ");
		result.append(usageOption);
		result.append(')');
		return result.toString();
	}

} //SignatureAttributeImpl
