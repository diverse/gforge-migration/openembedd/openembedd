/**
 * INRIA/IRISA
 *
 * $Id: AssociationrulesFactory.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage
 * @generated
 */
public interface AssociationrulesFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssociationrulesFactory eINSTANCE = orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Association Rules Function Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association Rules Function Settings</em>'.
	 * @generated
	 */
	AssociationRulesFunctionSettings createAssociationRulesFunctionSettings();

	/**
	 * Returns a new object of class '<em>Frequent Item Set Function Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Frequent Item Set Function Settings</em>'.
	 * @generated
	 */
	FrequentItemSetFunctionSettings createFrequentItemSetFunctionSettings();

	/**
	 * Returns a new object of class '<em>Sequence Function Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Function Settings</em>'.
	 * @generated
	 */
	SequenceFunctionSettings createSequenceFunctionSettings();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AssociationrulesPackage getAssociationrulesPackage();

} //AssociationrulesFactory
