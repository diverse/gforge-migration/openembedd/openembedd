/**
 * INRIA/IRISA
 *
 * $Id: ClassificationAdapterFactory.java,v 1.1 2008-04-01 09:35:54 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import orgomg.cwm.analysis.datamining.classification.*;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;

import orgomg.cwm.analysis.datamining.supervised.MiningTestResult;
import orgomg.cwm.analysis.datamining.supervised.MiningTestTask;
import orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings;

import orgomg.cwm.objectmodel.core.Attribute;
import orgomg.cwm.objectmodel.core.Element;
import orgomg.cwm.objectmodel.core.Feature;
import orgomg.cwm.objectmodel.core.ModelElement;
import orgomg.cwm.objectmodel.core.StructuralFeature;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage
 * @generated
 */
public class ClassificationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClassificationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassificationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ClassificationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch the delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassificationSwitch<Adapter> modelSwitch =
		new ClassificationSwitch<Adapter>() {
			@Override
			public Adapter caseApplyTargetValueItem(ApplyTargetValueItem object) {
				return createApplyTargetValueItemAdapter();
			}
			@Override
			public Adapter caseClassificationAttributeUsage(ClassificationAttributeUsage object) {
				return createClassificationAttributeUsageAdapter();
			}
			@Override
			public Adapter caseClassificationFunctionSettings(ClassificationFunctionSettings object) {
				return createClassificationFunctionSettingsAdapter();
			}
			@Override
			public Adapter caseClassificationTestResult(ClassificationTestResult object) {
				return createClassificationTestResultAdapter();
			}
			@Override
			public Adapter caseClassificationTestTask(ClassificationTestTask object) {
				return createClassificationTestTaskAdapter();
			}
			@Override
			public Adapter casePriorProbabilities(PriorProbabilities object) {
				return createPriorProbabilitiesAdapter();
			}
			@Override
			public Adapter casePriorProbabilitiesEntry(PriorProbabilitiesEntry object) {
				return createPriorProbabilitiesEntryAdapter();
			}
			@Override
			public Adapter caseElement(Element object) {
				return createElementAdapter();
			}
			@Override
			public Adapter caseModelElement(ModelElement object) {
				return createModelElementAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseStructuralFeature(StructuralFeature object) {
				return createStructuralFeatureAdapter();
			}
			@Override
			public Adapter caseAttribute(Attribute object) {
				return createAttributeAdapter();
			}
			@Override
			public Adapter caseMiningAttribute(MiningAttribute object) {
				return createMiningAttributeAdapter();
			}
			@Override
			public Adapter caseApplyOutputItem(ApplyOutputItem object) {
				return createApplyOutputItemAdapter();
			}
			@Override
			public Adapter caseAttributeUsage(AttributeUsage object) {
				return createAttributeUsageAdapter();
			}
			@Override
			public Adapter caseMiningFunctionSettings(MiningFunctionSettings object) {
				return createMiningFunctionSettingsAdapter();
			}
			@Override
			public Adapter caseSupervisedFunctionSettings(SupervisedFunctionSettings object) {
				return createSupervisedFunctionSettingsAdapter();
			}
			@Override
			public Adapter caseMiningResult(MiningResult object) {
				return createMiningResultAdapter();
			}
			@Override
			public Adapter caseMiningTestResult(MiningTestResult object) {
				return createMiningTestResultAdapter();
			}
			@Override
			public Adapter caseMiningTask(MiningTask object) {
				return createMiningTaskAdapter();
			}
			@Override
			public Adapter caseMiningTestTask(MiningTestTask object) {
				return createMiningTestTaskAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem <em>Apply Target Value Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem
	 * @generated
	 */
	public Adapter createApplyTargetValueItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage <em>Attribute Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage
	 * @generated
	 */
	public Adapter createClassificationAttributeUsageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings <em>Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings
	 * @generated
	 */
	public Adapter createClassificationFunctionSettingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult
	 * @generated
	 */
	public Adapter createClassificationTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestTask <em>Test Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestTask
	 * @generated
	 */
	public Adapter createClassificationTestTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities <em>Prior Probabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities
	 * @generated
	 */
	public Adapter createPriorProbabilitiesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry <em>Prior Probabilities Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry
	 * @generated
	 */
	public Adapter createPriorProbabilitiesEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.objectmodel.core.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.objectmodel.core.Element
	 * @generated
	 */
	public Adapter createElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.objectmodel.core.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.objectmodel.core.ModelElement
	 * @generated
	 */
	public Adapter createModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.objectmodel.core.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.objectmodel.core.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.objectmodel.core.StructuralFeature <em>Structural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.objectmodel.core.StructuralFeature
	 * @generated
	 */
	public Adapter createStructuralFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.objectmodel.core.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.objectmodel.core.Attribute
	 * @generated
	 */
	public Adapter createAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute <em>Mining Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute
	 * @generated
	 */
	public Adapter createMiningAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem <em>Apply Output Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem
	 * @generated
	 */
	public Adapter createApplyOutputItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage <em>Attribute Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage
	 * @generated
	 */
	public Adapter createAttributeUsageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings <em>Mining Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings
	 * @generated
	 */
	public Adapter createMiningFunctionSettingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings <em>Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings
	 * @generated
	 */
	public Adapter createSupervisedFunctionSettingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult <em>Mining Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult
	 * @generated
	 */
	public Adapter createMiningResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult <em>Mining Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.supervised.MiningTestResult
	 * @generated
	 */
	public Adapter createMiningTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask <em>Mining Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask
	 * @generated
	 */
	public Adapter createMiningTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestTask <em>Mining Test Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see orgomg.cwm.analysis.datamining.supervised.MiningTestTask
	 * @generated
	 */
	public Adapter createMiningTestTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ClassificationAdapterFactory
