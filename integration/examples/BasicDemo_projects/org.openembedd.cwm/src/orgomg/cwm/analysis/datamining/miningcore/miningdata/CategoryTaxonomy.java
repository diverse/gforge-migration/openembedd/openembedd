/**
 * INRIA/IRISA
 *
 * $Id: CategoryTaxonomy.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Taxonomy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A CategorizationGraph supports the specification of taxonomy or category hierarchy as required by data mining in the form of a directed acyclic graph. It enables two representations: 1) explicit specification of the graph through the referenced node class, and 2) referencing MiningData with specific attributes (columns) that store the data in tabular form.
 * 
 * A CategorizationGraph can contain multiple "root" nodes, in a sense being a single representation for several possibly strict hierarchies. 
 * 
 * Constraint to have either the MiningData instances (CategoryMapTable) OR the java object instances (CategoryMap).
 * 
 * Note: This is a "graph" as opposed to a "hierarchy" because it can have multiple roots, i.e. hierarchies in the same object.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getCategoryMap <em>Category Map</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getRootCategory <em>Root Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryTaxonomy()
 * @model
 * @generated
 */
public interface CategoryTaxonomy extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Category Map</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy <em>Taxonomy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category Map</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryTaxonomy_CategoryMap()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap#getTaxonomy
	 * @model opposite="taxonomy" containment="true"
	 * @generated
	 */
	EList<CategoryMap> getCategoryMap();

	/**
	 * Returns the value of the '<em><b>Root Category</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Category</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Category</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryTaxonomy_RootCategory()
	 * @model
	 * @generated
	 */
	EList<Category> getRootCategory();

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getTaxonomy <em>Taxonomy</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryTaxonomy_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getTaxonomy
	 * @model opposite="taxonomy" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

} // CategoryTaxonomy
