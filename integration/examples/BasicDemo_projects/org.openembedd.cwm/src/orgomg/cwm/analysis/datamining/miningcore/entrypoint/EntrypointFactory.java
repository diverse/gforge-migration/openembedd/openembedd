/**
 * INRIA/IRISA
 *
 * $Id: EntrypointFactory.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage
 * @generated
 */
public interface EntrypointFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EntrypointFactory eINSTANCE = orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.EntrypointFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Auxiliary Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Auxiliary Object</em>'.
	 * @generated
	 */
	AuxiliaryObject createAuxiliaryObject();

	/**
	 * Returns a new object of class '<em>Catalog</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Catalog</em>'.
	 * @generated
	 */
	Catalog createCatalog();

	/**
	 * Returns a new object of class '<em>Schema</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Schema</em>'.
	 * @generated
	 */
	Schema createSchema();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EntrypointPackage getEntrypointPackage();

} //EntrypointFactory
