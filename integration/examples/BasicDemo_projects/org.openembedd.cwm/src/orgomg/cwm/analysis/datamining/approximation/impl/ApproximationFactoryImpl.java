/**
 * INRIA/IRISA
 *
 * $Id: ApproximationFactoryImpl.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.approximation.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApproximationFactoryImpl extends EFactoryImpl implements ApproximationFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApproximationFactory init() {
		try {
			ApproximationFactory theApproximationFactory = (ApproximationFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/approximation.ecore"); 
			if (theApproximationFactory != null) {
				return theApproximationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApproximationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApproximationPackage.APPROXIMATION_FUNCTION_SETTINGS: return createApproximationFunctionSettings();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT: return createApproximationTestResult();
			case ApproximationPackage.APPROXIMATION_TEST_TASK: return createApproximationTestTask();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationFunctionSettings createApproximationFunctionSettings() {
		ApproximationFunctionSettingsImpl approximationFunctionSettings = new ApproximationFunctionSettingsImpl();
		return approximationFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationTestResult createApproximationTestResult() {
		ApproximationTestResultImpl approximationTestResult = new ApproximationTestResultImpl();
		return approximationTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationTestTask createApproximationTestTask() {
		ApproximationTestTaskImpl approximationTestTask = new ApproximationTestTaskImpl();
		return approximationTestTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationPackage getApproximationPackage() {
		return (ApproximationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApproximationPackage getPackage() {
		return ApproximationPackage.eINSTANCE;
	}

} //ApproximationFactoryImpl
