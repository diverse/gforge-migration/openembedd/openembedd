/**
 * INRIA/IRISA
 *
 * $Id: MiningdataFactoryImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiningdataFactoryImpl extends EFactoryImpl implements MiningdataFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningdataFactory init() {
		try {
			MiningdataFactory theMiningdataFactory = (MiningdataFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/miningcore/miningdata.ecore"); 
			if (theMiningdataFactory != null) {
				return theMiningdataFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MiningdataFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningdataFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT: return createAttributeAssignment();
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT_SET: return createAttributeAssignmentSet();
			case MiningdataPackage.ATTRIBUTE_USAGE: return createAttributeUsage();
			case MiningdataPackage.ATTRIBUTE_USAGE_SET: return createAttributeUsageSet();
			case MiningdataPackage.CATEGORICAL_ATTRIBUTE_PROPERTIES: return createCategoricalAttributeProperties();
			case MiningdataPackage.DIRECT_ATTRIBUTE_ASSIGNMENT: return createDirectAttributeAssignment();
			case MiningdataPackage.LOGICAL_ATTRIBUTE: return createLogicalAttribute();
			case MiningdataPackage.LOGICAL_DATA: return createLogicalData();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES: return createNumericalAttributeProperties();
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES: return createOrdinalAttributeProperties();
			case MiningdataPackage.PHYSICAL_DATA: return createPhysicalData();
			case MiningdataPackage.PIVOT_ATTRIBUTE_ASSIGNMENT: return createPivotAttributeAssignment();
			case MiningdataPackage.REVERSE_PIVOT_ATTRIBUTE_ASSIGNMENT: return createReversePivotAttributeAssignment();
			case MiningdataPackage.SET_ATTRIBUTE_ASSIGNMENT: return createSetAttributeAssignment();
			case MiningdataPackage.CATEGORY: return createCategory();
			case MiningdataPackage.CATEGORY_MAP: return createCategoryMap();
			case MiningdataPackage.CATEGORY_MAP_OBJECT: return createCategoryMapObject();
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY: return createCategoryMapObjectEntry();
			case MiningdataPackage.CATEGORY_MAP_TABLE: return createCategoryMapTable();
			case MiningdataPackage.CATEGORY_MATRIX: return createCategoryMatrix();
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY: return createCategoryMatrixEntry();
			case MiningdataPackage.CATEGORY_MATRIX_OBJECT: return createCategoryMatrixObject();
			case MiningdataPackage.CATEGORY_MATRIX_TABLE: return createCategoryMatrixTable();
			case MiningdataPackage.CATEGORY_TAXONOMY: return createCategoryTaxonomy();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MiningdataPackage.ATTRIBUTE_SELECTION_FUNCTION:
				return createAttributeSelectionFunctionFromString(eDataType, initialValue);
			case MiningdataPackage.ATTRIBUTE_TYPE:
				return createAttributeTypeFromString(eDataType, initialValue);
			case MiningdataPackage.ORDER_TYPE:
				return createOrderTypeFromString(eDataType, initialValue);
			case MiningdataPackage.USAGE_OPTION:
				return createUsageOptionFromString(eDataType, initialValue);
			case MiningdataPackage.VALUE_SELECTION_FUNCTION:
				return createValueSelectionFunctionFromString(eDataType, initialValue);
			case MiningdataPackage.CATEGORY_PROPERTY:
				return createCategoryPropertyFromString(eDataType, initialValue);
			case MiningdataPackage.MATRIX_PROPERTY:
				return createMatrixPropertyFromString(eDataType, initialValue);
			case MiningdataPackage.DOUBLE:
				return createDoubleFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MiningdataPackage.ATTRIBUTE_SELECTION_FUNCTION:
				return convertAttributeSelectionFunctionToString(eDataType, instanceValue);
			case MiningdataPackage.ATTRIBUTE_TYPE:
				return convertAttributeTypeToString(eDataType, instanceValue);
			case MiningdataPackage.ORDER_TYPE:
				return convertOrderTypeToString(eDataType, instanceValue);
			case MiningdataPackage.USAGE_OPTION:
				return convertUsageOptionToString(eDataType, instanceValue);
			case MiningdataPackage.VALUE_SELECTION_FUNCTION:
				return convertValueSelectionFunctionToString(eDataType, instanceValue);
			case MiningdataPackage.CATEGORY_PROPERTY:
				return convertCategoryPropertyToString(eDataType, instanceValue);
			case MiningdataPackage.MATRIX_PROPERTY:
				return convertMatrixPropertyToString(eDataType, instanceValue);
			case MiningdataPackage.DOUBLE:
				return convertDoubleToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignment createAttributeAssignment() {
		AttributeAssignmentImpl attributeAssignment = new AttributeAssignmentImpl();
		return attributeAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignmentSet createAttributeAssignmentSet() {
		AttributeAssignmentSetImpl attributeAssignmentSet = new AttributeAssignmentSetImpl();
		return attributeAssignmentSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeUsage createAttributeUsage() {
		AttributeUsageImpl attributeUsage = new AttributeUsageImpl();
		return attributeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeUsageSet createAttributeUsageSet() {
		AttributeUsageSetImpl attributeUsageSet = new AttributeUsageSetImpl();
		return attributeUsageSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoricalAttributeProperties createCategoricalAttributeProperties() {
		CategoricalAttributePropertiesImpl categoricalAttributeProperties = new CategoricalAttributePropertiesImpl();
		return categoricalAttributeProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectAttributeAssignment createDirectAttributeAssignment() {
		DirectAttributeAssignmentImpl directAttributeAssignment = new DirectAttributeAssignmentImpl();
		return directAttributeAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalAttribute createLogicalAttribute() {
		LogicalAttributeImpl logicalAttribute = new LogicalAttributeImpl();
		return logicalAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalData createLogicalData() {
		LogicalDataImpl logicalData = new LogicalDataImpl();
		return logicalData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumericalAttributeProperties createNumericalAttributeProperties() {
		NumericalAttributePropertiesImpl numericalAttributeProperties = new NumericalAttributePropertiesImpl();
		return numericalAttributeProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinalAttributeProperties createOrdinalAttributeProperties() {
		OrdinalAttributePropertiesImpl ordinalAttributeProperties = new OrdinalAttributePropertiesImpl();
		return ordinalAttributeProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalData createPhysicalData() {
		PhysicalDataImpl physicalData = new PhysicalDataImpl();
		return physicalData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PivotAttributeAssignment createPivotAttributeAssignment() {
		PivotAttributeAssignmentImpl pivotAttributeAssignment = new PivotAttributeAssignmentImpl();
		return pivotAttributeAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReversePivotAttributeAssignment createReversePivotAttributeAssignment() {
		ReversePivotAttributeAssignmentImpl reversePivotAttributeAssignment = new ReversePivotAttributeAssignmentImpl();
		return reversePivotAttributeAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetAttributeAssignment createSetAttributeAssignment() {
		SetAttributeAssignmentImpl setAttributeAssignment = new SetAttributeAssignmentImpl();
		return setAttributeAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category createCategory() {
		CategoryImpl category = new CategoryImpl();
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMap createCategoryMap() {
		CategoryMapImpl categoryMap = new CategoryMapImpl();
		return categoryMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMapObject createCategoryMapObject() {
		CategoryMapObjectImpl categoryMapObject = new CategoryMapObjectImpl();
		return categoryMapObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMapObjectEntry createCategoryMapObjectEntry() {
		CategoryMapObjectEntryImpl categoryMapObjectEntry = new CategoryMapObjectEntryImpl();
		return categoryMapObjectEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMapTable createCategoryMapTable() {
		CategoryMapTableImpl categoryMapTable = new CategoryMapTableImpl();
		return categoryMapTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrix createCategoryMatrix() {
		CategoryMatrixImpl categoryMatrix = new CategoryMatrixImpl();
		return categoryMatrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrixEntry createCategoryMatrixEntry() {
		CategoryMatrixEntryImpl categoryMatrixEntry = new CategoryMatrixEntryImpl();
		return categoryMatrixEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrixObject createCategoryMatrixObject() {
		CategoryMatrixObjectImpl categoryMatrixObject = new CategoryMatrixObjectImpl();
		return categoryMatrixObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrixTable createCategoryMatrixTable() {
		CategoryMatrixTableImpl categoryMatrixTable = new CategoryMatrixTableImpl();
		return categoryMatrixTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryTaxonomy createCategoryTaxonomy() {
		CategoryTaxonomyImpl categoryTaxonomy = new CategoryTaxonomyImpl();
		return categoryTaxonomy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeSelectionFunction createAttributeSelectionFunctionFromString(EDataType eDataType, String initialValue) {
		AttributeSelectionFunction result = AttributeSelectionFunction.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttributeSelectionFunctionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeType createAttributeTypeFromString(EDataType eDataType, String initialValue) {
		AttributeType result = AttributeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttributeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderType createOrderTypeFromString(EDataType eDataType, String initialValue) {
		OrderType result = OrderType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOrderTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsageOption createUsageOptionFromString(EDataType eDataType, String initialValue) {
		UsageOption result = UsageOption.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUsageOptionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueSelectionFunction createValueSelectionFunctionFromString(EDataType eDataType, String initialValue) {
		ValueSelectionFunction result = ValueSelectionFunction.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValueSelectionFunctionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryProperty createCategoryPropertyFromString(EDataType eDataType, String initialValue) {
		CategoryProperty result = CategoryProperty.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCategoryPropertyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatrixProperty createMatrixPropertyFromString(EDataType eDataType, String initialValue) {
		MatrixProperty result = MatrixProperty.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMatrixPropertyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createDoubleFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDoubleToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningdataPackage getMiningdataPackage() {
		return (MiningdataPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MiningdataPackage getPackage() {
		return MiningdataPackage.eINSTANCE;
	}

} //MiningdataFactoryImpl
