/**
 * INRIA/IRISA
 *
 * $Id: ApproximationTestResult.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation;

import orgomg.cwm.analysis.datamining.supervised.MiningTestResult;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the result of a test task applied to an approximation model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanPredictedValue <em>Mean Predicted Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanActualValue <em>Mean Actual Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanAbsoluteError <em>Mean Absolute Error</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getRmsError <em>Rms Error</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getRSquared <em>RSquared</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getTestTask <em>Test Task</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult()
 * @model
 * @generated
 */
public interface ApproximationTestResult extends MiningTestResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Mean Predicted Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Mean of the predicted values for test data. NULL if not computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Mean Predicted Value</em>' attribute.
	 * @see #setMeanPredictedValue(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_MeanPredictedValue()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getMeanPredictedValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanPredictedValue <em>Mean Predicted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mean Predicted Value</em>' attribute.
	 * @see #getMeanPredictedValue()
	 * @generated
	 */
	void setMeanPredictedValue(String value);

	/**
	 * Returns the value of the '<em><b>Mean Actual Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Mean of the actual values in the target attribute for test data. NULL if not computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Mean Actual Value</em>' attribute.
	 * @see #setMeanActualValue(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_MeanActualValue()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getMeanActualValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanActualValue <em>Mean Actual Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mean Actual Value</em>' attribute.
	 * @see #getMeanActualValue()
	 * @generated
	 */
	void setMeanActualValue(String value);

	/**
	 * Returns the value of the '<em><b>Mean Absolute Error</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Mean of the absolute values of the prediction error on the test data.. NULL if not computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Mean Absolute Error</em>' attribute.
	 * @see #setMeanAbsoluteError(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_MeanAbsoluteError()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getMeanAbsoluteError();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getMeanAbsoluteError <em>Mean Absolute Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mean Absolute Error</em>' attribute.
	 * @see #getMeanAbsoluteError()
	 * @generated
	 */
	void setMeanAbsoluteError(String value);

	/**
	 * Returns the value of the '<em><b>Rms Error</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Root of the mean squared errors on the test data. NULL if not computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Rms Error</em>' attribute.
	 * @see #setRmsError(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_RmsError()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getRmsError();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getRmsError <em>Rms Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rms Error</em>' attribute.
	 * @see #getRmsError()
	 * @generated
	 */
	void setRmsError(String value);

	/**
	 * Returns the value of the '<em><b>RSquared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The squared Pearson correlation coefficient computed on the test data. NULL if not computed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>RSquared</em>' attribute.
	 * @see #setRSquared(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_RSquared()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getRSquared();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getRSquared <em>RSquared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RSquared</em>' attribute.
	 * @see #getRSquared()
	 * @generated
	 */
	void setRSquared(String value);

	/**
	 * Returns the value of the '<em><b>Test Task</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestTask#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Task</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Task</em>' container reference.
	 * @see #setTestTask(ApproximationTestTask)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationTestResult_TestTask()
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationTestTask#getTestResult
	 * @model opposite="testResult" required="true"
	 * @generated
	 */
	ApproximationTestTask getTestTask();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult#getTestTask <em>Test Task</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Task</em>' container reference.
	 * @see #getTestTask()
	 * @generated
	 */
	void setTestTask(ApproximationTestTask value);

} // ApproximationTestResult
