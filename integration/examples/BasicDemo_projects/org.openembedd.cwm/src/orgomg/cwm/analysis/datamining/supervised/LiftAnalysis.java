/**
 * INRIA/IRISA
 *
 * $Id: LiftAnalysis.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lift Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the result of lift computation applied to a supervised model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTargetAttributeName <em>Target Attribute Name</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTestResult <em>Test Result</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getPoint <em>Point</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getPositiveTargetCategory <em>Positive Target Category</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysis()
 * @model
 * @generated
 */
public interface LiftAnalysis extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Target Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This represents the name of the target attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Target Attribute Name</em>' attribute.
	 * @see #setTargetAttributeName(String)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysis_TargetAttributeName()
	 * @model dataType="orgomg.cwm.objectmodel.core.String"
	 * @generated
	 */
	String getTargetAttributeName();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTargetAttributeName <em>Target Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Attribute Name</em>' attribute.
	 * @see #getTargetAttributeName()
	 * @generated
	 */
	void setTargetAttributeName(String value);

	/**
	 * Returns the value of the '<em><b>Test Result</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getLiftAnalysis <em>Lift Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Result</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Result</em>' container reference.
	 * @see #setTestResult(MiningTestResult)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysis_TestResult()
	 * @see orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getLiftAnalysis
	 * @model opposite="liftAnalysis" required="true"
	 * @generated
	 */
	MiningTestResult getTestResult();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTestResult <em>Test Result</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Result</em>' container reference.
	 * @see #getTestResult()
	 * @generated
	 */
	void setTestResult(MiningTestResult value);

	/**
	 * Returns the value of the '<em><b>Point</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getLiftAnalysis <em>Lift Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Point</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Point</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysis_Point()
	 * @see orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getLiftAnalysis
	 * @model opposite="liftAnalysis" containment="true" required="true"
	 * @generated
	 */
	EList<LiftAnalysisPoint> getPoint();

	/**
	 * Returns the value of the '<em><b>Positive Target Category</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positive Target Category</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positive Target Category</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysis_PositiveTargetCategory()
	 * @model required="true"
	 * @generated
	 */
	EList<Category> getPositiveTargetCategory();

} // LiftAnalysis
