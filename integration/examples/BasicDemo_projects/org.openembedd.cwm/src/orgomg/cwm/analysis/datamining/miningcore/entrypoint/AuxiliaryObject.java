/**
 * INRIA/IRISA
 *
 * $Id: AuxiliaryObject.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Auxiliary Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This contains ancillary objects that must be part of a schema.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getSchema <em>Schema</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getAttributeAssignmentSet <em>Attribute Assignment Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getAuxiliaryObject()
 * @model
 * @generated
 */
public interface AuxiliaryObject extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getAuxObjects <em>Aux Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getAuxiliaryObject_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getAuxObjects
	 * @model opposite="auxObjects" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

	/**
	 * Returns the value of the '<em><b>Attribute Assignment Set</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject <em>Auxiliary Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Assignment Set</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Assignment Set</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getAuxiliaryObject_AttributeAssignmentSet()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject
	 * @model opposite="auxiliaryObject" containment="true"
	 * @generated
	 */
	EList<AttributeAssignmentSet> getAttributeAssignmentSet();

} // AuxiliaryObject
