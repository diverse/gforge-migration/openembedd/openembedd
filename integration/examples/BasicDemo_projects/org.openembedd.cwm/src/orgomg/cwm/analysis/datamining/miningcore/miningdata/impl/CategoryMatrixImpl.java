/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;
import orgomg.cwm.analysis.datamining.classification.ClassificationTestResult;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Matrix</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getDiagonalDefault <em>Diagonal Default</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getOffDiagonalDefault <em>Off Diagonal Default</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getSchema <em>Schema</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixImpl#getTestResult <em>Test Result</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMatrixImpl extends ModelElementImpl implements CategoryMatrix {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getDiagonalDefault() <em>Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagonalDefault()
	 * @generated
	 * @ordered
	 */
	protected static final String DIAGONAL_DEFAULT_EDEFAULT = "1.0";

	/**
	 * The cached value of the '{@link #getDiagonalDefault() <em>Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagonalDefault()
	 * @generated
	 * @ordered
	 */
	protected String diagonalDefault = DIAGONAL_DEFAULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOffDiagonalDefault() <em>Off Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffDiagonalDefault()
	 * @generated
	 * @ordered
	 */
	protected static final String OFF_DIAGONAL_DEFAULT_EDEFAULT = "0.0";

	/**
	 * The cached value of the '{@link #getOffDiagonalDefault() <em>Off Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffDiagonalDefault()
	 * @generated
	 * @ordered
	 */
	protected String offDiagonalDefault = OFF_DIAGONAL_DEFAULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final MatrixProperty KIND_EDEFAULT = MatrixProperty.MP_SYMMETRIC_LITERAL;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected MatrixProperty kind = KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> category;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMatrixImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MATRIX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDiagonalDefault() {
		return diagonalDefault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagonalDefault(String newDiagonalDefault) {
		String oldDiagonalDefault = diagonalDefault;
		diagonalDefault = newDiagonalDefault;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX__DIAGONAL_DEFAULT, oldDiagonalDefault, diagonalDefault));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOffDiagonalDefault() {
		return offDiagonalDefault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffDiagonalDefault(String newOffDiagonalDefault) {
		String oldOffDiagonalDefault = offDiagonalDefault;
		offDiagonalDefault = newOffDiagonalDefault;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT, oldOffDiagonalDefault, offDiagonalDefault));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatrixProperty getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(MatrixProperty newKind) {
		MatrixProperty oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getCategory() {
		if (category == null) {
			category = new EObjectResolvingEList<Category>(Category.class, this, MiningdataPackage.CATEGORY_MATRIX__CATEGORY);
		}
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema getSchema() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX__SCHEMA) return null;
		return (Schema)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchema(Schema newSchema, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSchema, MiningdataPackage.CATEGORY_MATRIX__SCHEMA, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchema(Schema newSchema) {
		if (newSchema != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX__SCHEMA && newSchema != null)) {
			if (EcoreUtil.isAncestor(this, newSchema))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSchema != null)
				msgs = ((InternalEObject)newSchema).eInverseAdd(this, EntrypointPackage.SCHEMA__CATEGORY_MATRIX, Schema.class, msgs);
			msgs = basicSetSchema(newSchema, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX__SCHEMA, newSchema, newSchema));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassificationTestResult getTestResult() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT) return null;
		return (ClassificationTestResult)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestResult(ClassificationTestResult newTestResult, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTestResult, MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestResult(ClassificationTestResult newTestResult) {
		if (newTestResult != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT && newTestResult != null)) {
			if (EcoreUtil.isAncestor(this, newTestResult))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTestResult != null)
				msgs = ((InternalEObject)newTestResult).eInverseAdd(this, ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX, ClassificationTestResult.class, msgs);
			msgs = basicSetTestResult(newTestResult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT, newTestResult, newTestResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSchema((Schema)otherEnd, msgs);
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTestResult((ClassificationTestResult)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				return basicSetSchema(null, msgs);
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				return basicSetTestResult(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.SCHEMA__CATEGORY_MATRIX, Schema.class, msgs);
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				return eInternalContainer().eInverseRemove(this, ClassificationPackage.CLASSIFICATION_TEST_RESULT__CONFUSION_MATRIX, ClassificationTestResult.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__DIAGONAL_DEFAULT:
				return getDiagonalDefault();
			case MiningdataPackage.CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT:
				return getOffDiagonalDefault();
			case MiningdataPackage.CATEGORY_MATRIX__KIND:
				return getKind();
			case MiningdataPackage.CATEGORY_MATRIX__CATEGORY:
				return getCategory();
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				return getSchema();
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				return getTestResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__DIAGONAL_DEFAULT:
				setDiagonalDefault((String)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT:
				setOffDiagonalDefault((String)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__KIND:
				setKind((MatrixProperty)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__CATEGORY:
				getCategory().clear();
				getCategory().addAll((Collection<? extends Category>)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				setSchema((Schema)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				setTestResult((ClassificationTestResult)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__DIAGONAL_DEFAULT:
				setDiagonalDefault(DIAGONAL_DEFAULT_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT:
				setOffDiagonalDefault(OFF_DIAGONAL_DEFAULT_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__CATEGORY:
				getCategory().clear();
				return;
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				setSchema((Schema)null);
				return;
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				setTestResult((ClassificationTestResult)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX__DIAGONAL_DEFAULT:
				return DIAGONAL_DEFAULT_EDEFAULT == null ? diagonalDefault != null : !DIAGONAL_DEFAULT_EDEFAULT.equals(diagonalDefault);
			case MiningdataPackage.CATEGORY_MATRIX__OFF_DIAGONAL_DEFAULT:
				return OFF_DIAGONAL_DEFAULT_EDEFAULT == null ? offDiagonalDefault != null : !OFF_DIAGONAL_DEFAULT_EDEFAULT.equals(offDiagonalDefault);
			case MiningdataPackage.CATEGORY_MATRIX__KIND:
				return kind != KIND_EDEFAULT;
			case MiningdataPackage.CATEGORY_MATRIX__CATEGORY:
				return category != null && !category.isEmpty();
			case MiningdataPackage.CATEGORY_MATRIX__SCHEMA:
				return getSchema() != null;
			case MiningdataPackage.CATEGORY_MATRIX__TEST_RESULT:
				return getTestResult() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (diagonalDefault: ");
		result.append(diagonalDefault);
		result.append(", offDiagonalDefault: ");
		result.append(offDiagonalDefault);
		result.append(", kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} //CategoryMatrixImpl
