/**
 * INRIA/IRISA
 *
 * $Id: LogicalAttribute.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A MiningAttribute object is a logical concept that describes a domain of data to be used as input to data mining operations. Mining attributes are typically either categorical, ordinal, or numerical. As such, a mining attribute references additional metadata that characterizes the attribute as either categorical, e.g., a list of the categories; or numerical, e.g., the bounds of the data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#isIsSetValued <em>Is Set Valued</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties <em>Categorical Properties</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties <em>Numerical Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalAttribute()
 * @model
 * @generated
 */
public interface LogicalAttribute extends MiningAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Is Set Valued</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates that the values of the attribute being specified here are sets, if true.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Set Valued</em>' attribute.
	 * @see #setIsSetValued(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalAttribute_IsSetValued()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsSetValued();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#isIsSetValued <em>Is Set Valued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Set Valued</em>' attribute.
	 * @see #isIsSetValued()
	 * @generated
	 */
	void setIsSetValued(boolean value);

	/**
	 * Returns the value of the '<em><b>Categorical Properties</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Categorical Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Categorical Properties</em>' containment reference.
	 * @see #setCategoricalProperties(CategoricalAttributeProperties)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalAttribute_CategoricalProperties()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute
	 * @model opposite="logicalAttribute" containment="true"
	 * @generated
	 */
	CategoricalAttributeProperties getCategoricalProperties();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties <em>Categorical Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Categorical Properties</em>' containment reference.
	 * @see #getCategoricalProperties()
	 * @generated
	 */
	void setCategoricalProperties(CategoricalAttributeProperties value);

	/**
	 * Returns the value of the '<em><b>Numerical Properties</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Numerical Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numerical Properties</em>' containment reference.
	 * @see #setNumericalProperties(NumericalAttributeProperties)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getLogicalAttribute_NumericalProperties()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute
	 * @model opposite="logicalAttribute" containment="true"
	 * @generated
	 */
	NumericalAttributeProperties getNumericalProperties();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties <em>Numerical Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Numerical Properties</em>' containment reference.
	 * @see #getNumericalProperties()
	 * @generated
	 */
	void setNumericalProperties(NumericalAttributeProperties value);

} // LogicalAttribute
