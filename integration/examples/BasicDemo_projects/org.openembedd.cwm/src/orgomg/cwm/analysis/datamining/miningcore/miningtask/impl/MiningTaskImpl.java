/**
 * INRIA/IRISA
 *
 * $Id: MiningTaskImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.PhysicalData;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl#getInputModel <em>Input Model</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl#getInputData <em>Input Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl#getModelAssignment <em>Model Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class MiningTaskImpl extends ModelElementImpl implements MiningTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getInputModel() <em>Input Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputModel()
	 * @generated
	 * @ordered
	 */
	protected MiningModel inputModel;

	/**
	 * The cached value of the '{@link #getInputData() <em>Input Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputData()
	 * @generated
	 * @ordered
	 */
	protected PhysicalData inputData;

	/**
	 * The cached value of the '{@link #getModelAssignment() <em>Model Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelAssignment()
	 * @generated
	 * @ordered
	 */
	protected AttributeAssignmentSet modelAssignment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.MINING_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningModel getInputModel() {
		if (inputModel != null && inputModel.eIsProxy()) {
			InternalEObject oldInputModel = (InternalEObject)inputModel;
			inputModel = (MiningModel)eResolveProxy(oldInputModel);
			if (inputModel != oldInputModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningtaskPackage.MINING_TASK__INPUT_MODEL, oldInputModel, inputModel));
			}
		}
		return inputModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningModel basicGetInputModel() {
		return inputModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputModel(MiningModel newInputModel) {
		MiningModel oldInputModel = inputModel;
		inputModel = newInputModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_TASK__INPUT_MODEL, oldInputModel, inputModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalData getInputData() {
		if (inputData != null && inputData.eIsProxy()) {
			InternalEObject oldInputData = (InternalEObject)inputData;
			inputData = (PhysicalData)eResolveProxy(oldInputData);
			if (inputData != oldInputData) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningtaskPackage.MINING_TASK__INPUT_DATA, oldInputData, inputData));
			}
		}
		return inputData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalData basicGetInputData() {
		return inputData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputData(PhysicalData newInputData) {
		PhysicalData oldInputData = inputData;
		inputData = newInputData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_TASK__INPUT_DATA, oldInputData, inputData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignmentSet getModelAssignment() {
		if (modelAssignment != null && modelAssignment.eIsProxy()) {
			InternalEObject oldModelAssignment = (InternalEObject)modelAssignment;
			modelAssignment = (AttributeAssignmentSet)eResolveProxy(oldModelAssignment);
			if (modelAssignment != oldModelAssignment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT, oldModelAssignment, modelAssignment));
			}
		}
		return modelAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignmentSet basicGetModelAssignment() {
		return modelAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelAssignment(AttributeAssignmentSet newModelAssignment) {
		AttributeAssignmentSet oldModelAssignment = modelAssignment;
		modelAssignment = newModelAssignment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT, oldModelAssignment, modelAssignment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema getSchema() {
		if (eContainerFeatureID != MiningtaskPackage.MINING_TASK__SCHEMA) return null;
		return (Schema)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchema(Schema newSchema, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSchema, MiningtaskPackage.MINING_TASK__SCHEMA, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchema(Schema newSchema) {
		if (newSchema != eInternalContainer() || (eContainerFeatureID != MiningtaskPackage.MINING_TASK__SCHEMA && newSchema != null)) {
			if (EcoreUtil.isAncestor(this, newSchema))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSchema != null)
				msgs = ((InternalEObject)newSchema).eInverseAdd(this, EntrypointPackage.SCHEMA__MINING_TASK, Schema.class, msgs);
			msgs = basicSetSchema(newSchema, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_TASK__SCHEMA, newSchema, newSchema));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSchema((Schema)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				return basicSetSchema(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.SCHEMA__MINING_TASK, Schema.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__INPUT_MODEL:
				if (resolve) return getInputModel();
				return basicGetInputModel();
			case MiningtaskPackage.MINING_TASK__INPUT_DATA:
				if (resolve) return getInputData();
				return basicGetInputData();
			case MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT:
				if (resolve) return getModelAssignment();
				return basicGetModelAssignment();
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				return getSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__INPUT_MODEL:
				setInputModel((MiningModel)newValue);
				return;
			case MiningtaskPackage.MINING_TASK__INPUT_DATA:
				setInputData((PhysicalData)newValue);
				return;
			case MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT:
				setModelAssignment((AttributeAssignmentSet)newValue);
				return;
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				setSchema((Schema)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__INPUT_MODEL:
				setInputModel((MiningModel)null);
				return;
			case MiningtaskPackage.MINING_TASK__INPUT_DATA:
				setInputData((PhysicalData)null);
				return;
			case MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT:
				setModelAssignment((AttributeAssignmentSet)null);
				return;
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				setSchema((Schema)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.MINING_TASK__INPUT_MODEL:
				return inputModel != null;
			case MiningtaskPackage.MINING_TASK__INPUT_DATA:
				return inputData != null;
			case MiningtaskPackage.MINING_TASK__MODEL_ASSIGNMENT:
				return modelAssignment != null;
			case MiningtaskPackage.MINING_TASK__SCHEMA:
				return getSchema() != null;
		}
		return super.eIsSet(featureID);
	}

} //MiningTaskImpl
