/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrix.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.classification.ClassificationTestResult;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Matrix</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A CategoryMatrix assigns numeric values to pairs of categories.
 * It is either represented as a set of CategoryMatrixEntries or as a table.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getDiagonalDefault <em>Diagonal Default</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getOffDiagonalDefault <em>Off Diagonal Default</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getKind <em>Kind</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getCategory <em>Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema <em>Schema</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult <em>Test Result</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix()
 * @model
 * @generated
 */
public interface CategoryMatrix extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Diagonal Default</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If a matrix cell in the diagonal is not specified use this value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Diagonal Default</em>' attribute.
	 * @see #setDiagonalDefault(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_DiagonalDefault()
	 * @model default="1.0" dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getDiagonalDefault();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getDiagonalDefault <em>Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagonal Default</em>' attribute.
	 * @see #getDiagonalDefault()
	 * @generated
	 */
	void setDiagonalDefault(String value);

	/**
	 * Returns the value of the '<em><b>Off Diagonal Default</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If a matrix cell not in the diagonal is not specified use this value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Off Diagonal Default</em>' attribute.
	 * @see #setOffDiagonalDefault(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_OffDiagonalDefault()
	 * @model default="0.0" dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getOffDiagonalDefault();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getOffDiagonalDefault <em>Off Diagonal Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Off Diagonal Default</em>' attribute.
	 * @see #getOffDiagonalDefault()
	 * @generated
	 */
	void setOffDiagonalDefault(String value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The default value is <code>"MatrixProperty.any"</code>.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Type of matrix, 'diagonal', 'symmetric', or 'any'.
	 * If diagonal, then all values outside the diagonal are 0.
	 * If symmetric then value(i,j)=value(j,i).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty
	 * @see #setKind(MatrixProperty)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_Kind()
	 * @model default="MatrixProperty.any"
	 * @generated
	 */
	MatrixProperty getKind();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MatrixProperty
	 * @see #getKind()
	 * @generated
	 */
	void setKind(MatrixProperty value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_Category()
	 * @model lower="2"
	 * @generated
	 */
	EList<Category> getCategory();

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCategoryMatrix <em>Category Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' container reference.
	 * @see #setSchema(Schema)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCategoryMatrix
	 * @model opposite="categoryMatrix" required="true"
	 * @generated
	 */
	Schema getSchema();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema <em>Schema</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema</em>' container reference.
	 * @see #getSchema()
	 * @generated
	 */
	void setSchema(Schema value);

	/**
	 * Returns the value of the '<em><b>Test Result</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix <em>Confusion Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Result</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Result</em>' container reference.
	 * @see #setTestResult(ClassificationTestResult)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrix_TestResult()
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationTestResult#getConfusionMatrix
	 * @model opposite="confusionMatrix" required="true"
	 * @generated
	 */
	ClassificationTestResult getTestResult();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getTestResult <em>Test Result</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Result</em>' container reference.
	 * @see #getTestResult()
	 * @generated
	 */
	void setTestResult(ClassificationTestResult value);

} // CategoryMatrix
