/**
 * INRIA/IRISA
 *
 * $Id: ClusteringAttributeUsage.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Usage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ClusteringAttributeUsage is a subclass of AttributeUsage to support attribute usages that are specific to clustering algorithms. 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getAttributeComparisonFunction <em>Attribute Comparison Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getSimilarityScale <em>Similarity Scale</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getComparisonMatrix <em>Comparison Matrix</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringAttributeUsage()
 * @model
 * @generated
 */
public interface ClusteringAttributeUsage extends AttributeUsage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Attribute Comparison Function</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * When two records are compared then either the distance or the similarity is of interest. In both cases the measures can be computed by a combination of an 'inner' function and an 'outer' function. The inner function compares two single field values and the outer function computes an aggregation over all fields
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attribute Comparison Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction
	 * @see #setAttributeComparisonFunction(AttributeComparisonFunction)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringAttributeUsage_AttributeComparisonFunction()
	 * @model
	 * @generated
	 */
	AttributeComparisonFunction getAttributeComparisonFunction();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getAttributeComparisonFunction <em>Attribute Comparison Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Comparison Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction
	 * @see #getAttributeComparisonFunction()
	 * @generated
	 */
	void setAttributeComparisonFunction(AttributeComparisonFunction value);

	/**
	 * Returns the value of the '<em><b>Similarity Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If the attributeComparisonFunction admits a value, then similarityScale is that value. Only valid for numerical attributes. NULL otherwise.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Similarity Scale</em>' attribute.
	 * @see #setSimilarityScale(String)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringAttributeUsage_SimilarityScale()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getSimilarityScale();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getSimilarityScale <em>Similarity Scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Similarity Scale</em>' attribute.
	 * @see #getSimilarityScale()
	 * @generated
	 */
	void setSimilarityScale(String value);

	/**
	 * Returns the value of the '<em><b>Comparison Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Matrix</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison Matrix</em>' reference.
	 * @see #setComparisonMatrix(CategoryMatrix)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringAttributeUsage_ComparisonMatrix()
	 * @model
	 * @generated
	 */
	CategoryMatrix getComparisonMatrix();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getComparisonMatrix <em>Comparison Matrix</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison Matrix</em>' reference.
	 * @see #getComparisonMatrix()
	 * @generated
	 */
	void setComparisonMatrix(CategoryMatrix value);

} // ClusteringAttributeUsage
