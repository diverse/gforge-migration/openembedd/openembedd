/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierMapValidator.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.transformation.ClassifierFeatureMap;
import orgomg.cwm.analysis.transformation.FeatureMap;

import orgomg.cwm.objectmodel.core.Classifier;
import orgomg.cwm.objectmodel.core.ProcedureExpression;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.transformation.ClassifierMap}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ClassifierMapValidator {
	boolean validate();

	boolean validateFunction(ProcedureExpression value);
	boolean validateFunctionDescription(String value);
	boolean validateFeatureMap(EList<FeatureMap> value);
	boolean validateCfMap(EList<ClassifierFeatureMap> value);
	boolean validateSource(EList<Classifier> value);
}
