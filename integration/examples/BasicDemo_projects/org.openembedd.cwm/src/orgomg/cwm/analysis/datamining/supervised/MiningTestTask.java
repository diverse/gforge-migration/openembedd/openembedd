/**
 * INRIA/IRISA
 *
 * $Id: MiningTestTask.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Test Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a mining task that is used to check some aspect of the quality of a classification or regression model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.MiningTestTask#isComputeLift <em>Compute Lift</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.MiningTestTask#getPositiveTargetCategory <em>Positive Target Category</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestTask()
 * @model
 * @generated
 */
public interface MiningTestTask extends MiningTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Compute Lift</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates to perform lift computation as part of test task, if true. The default is false.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Compute Lift</em>' attribute.
	 * @see #setComputeLift(boolean)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestTask_ComputeLift()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isComputeLift();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestTask#isComputeLift <em>Compute Lift</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compute Lift</em>' attribute.
	 * @see #isComputeLift()
	 * @generated
	 */
	void setComputeLift(boolean value);

	/**
	 * Returns the value of the '<em><b>Positive Target Category</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positive Target Category</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positive Target Category</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestTask_PositiveTargetCategory()
	 * @model required="true"
	 * @generated
	 */
	EList<Category> getPositiveTargetCategory();

} // MiningTestTask
