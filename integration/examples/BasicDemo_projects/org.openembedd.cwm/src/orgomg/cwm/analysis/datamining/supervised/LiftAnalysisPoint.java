/**
 * INRIA/IRISA
 *
 * $Id: LiftAnalysisPoint.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lift Analysis Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the lift result for the quantile of the input data specified in this object.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getSubsetOfRecords <em>Subset Of Records</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getAggregateTarget <em>Aggregate Target</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getLiftAnalysis <em>Lift Analysis</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysisPoint()
 * @model
 * @generated
 */
public interface LiftAnalysisPoint extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Subset Of Records</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The number of records for which the lift (sum of target predictions or actual target values) is given.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Subset Of Records</em>' attribute.
	 * @see #setSubsetOfRecords(long)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysisPoint_SubsetOfRecords()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getSubsetOfRecords();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getSubsetOfRecords <em>Subset Of Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subset Of Records</em>' attribute.
	 * @see #getSubsetOfRecords()
	 * @generated
	 */
	void setSubsetOfRecords(long value);

	/**
	 * Returns the value of the '<em><b>Aggregate Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The lift, i.e. the sum of actual positive targets for classification or the sum of the actual values for regression, for the specified subset of records.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Aggregate Target</em>' attribute.
	 * @see #setAggregateTarget(String)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysisPoint_AggregateTarget()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getAggregateTarget();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getAggregateTarget <em>Aggregate Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aggregate Target</em>' attribute.
	 * @see #getAggregateTarget()
	 * @generated
	 */
	void setAggregateTarget(String value);

	/**
	 * Returns the value of the '<em><b>Lift Analysis</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lift Analysis</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lift Analysis</em>' container reference.
	 * @see #setLiftAnalysis(LiftAnalysis)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getLiftAnalysisPoint_LiftAnalysis()
	 * @see orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getPoint
	 * @model opposite="point" required="true"
	 * @generated
	 */
	LiftAnalysis getLiftAnalysis();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint#getLiftAnalysis <em>Lift Analysis</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lift Analysis</em>' container reference.
	 * @see #getLiftAnalysis()
	 * @generated
	 */
	void setLiftAnalysis(LiftAnalysis value);

} // LiftAnalysisPoint
