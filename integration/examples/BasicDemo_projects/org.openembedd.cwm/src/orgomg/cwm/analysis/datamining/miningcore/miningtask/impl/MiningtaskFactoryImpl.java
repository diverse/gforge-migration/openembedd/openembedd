/**
 * INRIA/IRISA
 *
 * $Id: MiningtaskFactoryImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiningtaskFactoryImpl extends EFactoryImpl implements MiningtaskFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MiningtaskFactory init() {
		try {
			MiningtaskFactory theMiningtaskFactory = (MiningtaskFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/miningcore/miningtask.ecore"); 
			if (theMiningtaskFactory != null) {
				return theMiningtaskFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MiningtaskFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningtaskFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM: return createApplyOutputItem();
			case MiningtaskPackage.APPLY_PROBABILITY_ITEM: return createApplyProbabilityItem();
			case MiningtaskPackage.APPLY_RULE_ID_ITEM: return createApplyRuleIdItem();
			case MiningtaskPackage.APPLY_SCORE_ITEM: return createApplyScoreItem();
			case MiningtaskPackage.APPLY_SOURCE_ITEM: return createApplySourceItem();
			case MiningtaskPackage.MINING_APPLY_OUTPUT: return createMiningApplyOutput();
			case MiningtaskPackage.MINING_APPLY_TASK: return createMiningApplyTask();
			case MiningtaskPackage.MINING_BUILD_TASK: return createMiningBuildTask();
			case MiningtaskPackage.MINING_TRANSFORMATION: return createMiningTransformation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MiningtaskPackage.APPLY_OUTPUT_OPTION:
				return createApplyOutputOptionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MiningtaskPackage.APPLY_OUTPUT_OPTION:
				return convertApplyOutputOptionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyOutputItem createApplyOutputItem() {
		ApplyOutputItemImpl applyOutputItem = new ApplyOutputItemImpl();
		return applyOutputItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyProbabilityItem createApplyProbabilityItem() {
		ApplyProbabilityItemImpl applyProbabilityItem = new ApplyProbabilityItemImpl();
		return applyProbabilityItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyRuleIdItem createApplyRuleIdItem() {
		ApplyRuleIdItemImpl applyRuleIdItem = new ApplyRuleIdItemImpl();
		return applyRuleIdItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyScoreItem createApplyScoreItem() {
		ApplyScoreItemImpl applyScoreItem = new ApplyScoreItemImpl();
		return applyScoreItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplySourceItem createApplySourceItem() {
		ApplySourceItemImpl applySourceItem = new ApplySourceItemImpl();
		return applySourceItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningApplyOutput createMiningApplyOutput() {
		MiningApplyOutputImpl miningApplyOutput = new MiningApplyOutputImpl();
		return miningApplyOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningApplyTask createMiningApplyTask() {
		MiningApplyTaskImpl miningApplyTask = new MiningApplyTaskImpl();
		return miningApplyTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningBuildTask createMiningBuildTask() {
		MiningBuildTaskImpl miningBuildTask = new MiningBuildTaskImpl();
		return miningBuildTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningTransformation createMiningTransformation() {
		MiningTransformationImpl miningTransformation = new MiningTransformationImpl();
		return miningTransformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyOutputOption createApplyOutputOptionFromString(EDataType eDataType, String initialValue) {
		ApplyOutputOption result = ApplyOutputOption.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertApplyOutputOptionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningtaskPackage getMiningtaskPackage() {
		return (MiningtaskPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MiningtaskPackage getPackage() {
		return MiningtaskPackage.eINSTANCE;
	}

} //MiningtaskFactoryImpl
