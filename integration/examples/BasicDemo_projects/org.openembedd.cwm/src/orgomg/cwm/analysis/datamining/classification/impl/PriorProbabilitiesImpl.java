/**
 * INRIA/IRISA
 *
 * $Id: PriorProbabilitiesImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage;
import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;
import orgomg.cwm.analysis.datamining.classification.PriorProbabilities;
import orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prior Probabilities</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl#getUsage <em>Usage</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesImpl#getPrior <em>Prior</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PriorProbabilitiesImpl extends ModelElementImpl implements PriorProbabilities {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getPrior() <em>Prior</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrior()
	 * @generated
	 * @ordered
	 */
	protected EList<PriorProbabilitiesEntry> prior;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorProbabilitiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassificationPackage.Literals.PRIOR_PROBABILITIES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassificationAttributeUsage getUsage() {
		if (eContainerFeatureID != ClassificationPackage.PRIOR_PROBABILITIES__USAGE) return null;
		return (ClassificationAttributeUsage)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUsage(ClassificationAttributeUsage newUsage, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newUsage, ClassificationPackage.PRIOR_PROBABILITIES__USAGE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsage(ClassificationAttributeUsage newUsage) {
		if (newUsage != eInternalContainer() || (eContainerFeatureID != ClassificationPackage.PRIOR_PROBABILITIES__USAGE && newUsage != null)) {
			if (EcoreUtil.isAncestor(this, newUsage))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newUsage != null)
				msgs = ((InternalEObject)newUsage).eInverseAdd(this, ClassificationPackage.CLASSIFICATION_ATTRIBUTE_USAGE__PRIORS, ClassificationAttributeUsage.class, msgs);
			msgs = basicSetUsage(newUsage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.PRIOR_PROBABILITIES__USAGE, newUsage, newUsage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorProbabilitiesEntry> getPrior() {
		if (prior == null) {
			prior = new EObjectContainmentWithInverseEList<PriorProbabilitiesEntry>(PriorProbabilitiesEntry.class, this, ClassificationPackage.PRIOR_PROBABILITIES__PRIOR, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS);
		}
		return prior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetUsage((ClassificationAttributeUsage)otherEnd, msgs);
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPrior()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				return basicSetUsage(null, msgs);
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				return ((InternalEList<?>)getPrior()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				return eInternalContainer().eInverseRemove(this, ClassificationPackage.CLASSIFICATION_ATTRIBUTE_USAGE__PRIORS, ClassificationAttributeUsage.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				return getUsage();
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				return getPrior();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				setUsage((ClassificationAttributeUsage)newValue);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				getPrior().clear();
				getPrior().addAll((Collection<? extends PriorProbabilitiesEntry>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				setUsage((ClassificationAttributeUsage)null);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				getPrior().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES__USAGE:
				return getUsage() != null;
			case ClassificationPackage.PRIOR_PROBABILITIES__PRIOR:
				return prior != null && !prior.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PriorProbabilitiesImpl
