/**
 * INRIA/IRISA
 *
 * $Id: ApproximationTestResultImpl.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;
import orgomg.cwm.analysis.datamining.approximation.ApproximationTestResult;
import orgomg.cwm.analysis.datamining.approximation.ApproximationTestTask;

import orgomg.cwm.analysis.datamining.supervised.impl.MiningTestResultImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getMeanPredictedValue <em>Mean Predicted Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getMeanActualValue <em>Mean Actual Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getMeanAbsoluteError <em>Mean Absolute Error</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getRmsError <em>Rms Error</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getRSquared <em>RSquared</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.impl.ApproximationTestResultImpl#getTestTask <em>Test Task</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApproximationTestResultImpl extends MiningTestResultImpl implements ApproximationTestResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getMeanPredictedValue() <em>Mean Predicted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanPredictedValue()
	 * @generated
	 * @ordered
	 */
	protected static final String MEAN_PREDICTED_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMeanPredictedValue() <em>Mean Predicted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanPredictedValue()
	 * @generated
	 * @ordered
	 */
	protected String meanPredictedValue = MEAN_PREDICTED_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeanActualValue() <em>Mean Actual Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanActualValue()
	 * @generated
	 * @ordered
	 */
	protected static final String MEAN_ACTUAL_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMeanActualValue() <em>Mean Actual Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanActualValue()
	 * @generated
	 * @ordered
	 */
	protected String meanActualValue = MEAN_ACTUAL_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMeanAbsoluteError() <em>Mean Absolute Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanAbsoluteError()
	 * @generated
	 * @ordered
	 */
	protected static final String MEAN_ABSOLUTE_ERROR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMeanAbsoluteError() <em>Mean Absolute Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeanAbsoluteError()
	 * @generated
	 * @ordered
	 */
	protected String meanAbsoluteError = MEAN_ABSOLUTE_ERROR_EDEFAULT;

	/**
	 * The default value of the '{@link #getRmsError() <em>Rms Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRmsError()
	 * @generated
	 * @ordered
	 */
	protected static final String RMS_ERROR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRmsError() <em>Rms Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRmsError()
	 * @generated
	 * @ordered
	 */
	protected String rmsError = RMS_ERROR_EDEFAULT;

	/**
	 * The default value of the '{@link #getRSquared() <em>RSquared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRSquared()
	 * @generated
	 * @ordered
	 */
	protected static final String RSQUARED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRSquared() <em>RSquared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRSquared()
	 * @generated
	 * @ordered
	 */
	protected String rSquared = RSQUARED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApproximationTestResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApproximationPackage.Literals.APPROXIMATION_TEST_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMeanPredictedValue() {
		return meanPredictedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeanPredictedValue(String newMeanPredictedValue) {
		String oldMeanPredictedValue = meanPredictedValue;
		meanPredictedValue = newMeanPredictedValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_PREDICTED_VALUE, oldMeanPredictedValue, meanPredictedValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMeanActualValue() {
		return meanActualValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeanActualValue(String newMeanActualValue) {
		String oldMeanActualValue = meanActualValue;
		meanActualValue = newMeanActualValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ACTUAL_VALUE, oldMeanActualValue, meanActualValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMeanAbsoluteError() {
		return meanAbsoluteError;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeanAbsoluteError(String newMeanAbsoluteError) {
		String oldMeanAbsoluteError = meanAbsoluteError;
		meanAbsoluteError = newMeanAbsoluteError;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ABSOLUTE_ERROR, oldMeanAbsoluteError, meanAbsoluteError));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRmsError() {
		return rmsError;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRmsError(String newRmsError) {
		String oldRmsError = rmsError;
		rmsError = newRmsError;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__RMS_ERROR, oldRmsError, rmsError));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRSquared() {
		return rSquared;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRSquared(String newRSquared) {
		String oldRSquared = rSquared;
		rSquared = newRSquared;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__RSQUARED, oldRSquared, rSquared));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApproximationTestTask getTestTask() {
		if (eContainerFeatureID != ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK) return null;
		return (ApproximationTestTask)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestTask(ApproximationTestTask newTestTask, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTestTask, ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestTask(ApproximationTestTask newTestTask) {
		if (newTestTask != eInternalContainer() || (eContainerFeatureID != ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK && newTestTask != null)) {
			if (EcoreUtil.isAncestor(this, newTestTask))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTestTask != null)
				msgs = ((InternalEObject)newTestTask).eInverseAdd(this, ApproximationPackage.APPROXIMATION_TEST_TASK__TEST_RESULT, ApproximationTestTask.class, msgs);
			msgs = basicSetTestTask(newTestTask, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK, newTestTask, newTestTask));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTestTask((ApproximationTestTask)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				return basicSetTestTask(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				return eInternalContainer().eInverseRemove(this, ApproximationPackage.APPROXIMATION_TEST_TASK__TEST_RESULT, ApproximationTestTask.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_PREDICTED_VALUE:
				return getMeanPredictedValue();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ACTUAL_VALUE:
				return getMeanActualValue();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ABSOLUTE_ERROR:
				return getMeanAbsoluteError();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RMS_ERROR:
				return getRmsError();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RSQUARED:
				return getRSquared();
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				return getTestTask();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_PREDICTED_VALUE:
				setMeanPredictedValue((String)newValue);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ACTUAL_VALUE:
				setMeanActualValue((String)newValue);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ABSOLUTE_ERROR:
				setMeanAbsoluteError((String)newValue);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RMS_ERROR:
				setRmsError((String)newValue);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RSQUARED:
				setRSquared((String)newValue);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				setTestTask((ApproximationTestTask)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_PREDICTED_VALUE:
				setMeanPredictedValue(MEAN_PREDICTED_VALUE_EDEFAULT);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ACTUAL_VALUE:
				setMeanActualValue(MEAN_ACTUAL_VALUE_EDEFAULT);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ABSOLUTE_ERROR:
				setMeanAbsoluteError(MEAN_ABSOLUTE_ERROR_EDEFAULT);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RMS_ERROR:
				setRmsError(RMS_ERROR_EDEFAULT);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RSQUARED:
				setRSquared(RSQUARED_EDEFAULT);
				return;
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				setTestTask((ApproximationTestTask)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_PREDICTED_VALUE:
				return MEAN_PREDICTED_VALUE_EDEFAULT == null ? meanPredictedValue != null : !MEAN_PREDICTED_VALUE_EDEFAULT.equals(meanPredictedValue);
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ACTUAL_VALUE:
				return MEAN_ACTUAL_VALUE_EDEFAULT == null ? meanActualValue != null : !MEAN_ACTUAL_VALUE_EDEFAULT.equals(meanActualValue);
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__MEAN_ABSOLUTE_ERROR:
				return MEAN_ABSOLUTE_ERROR_EDEFAULT == null ? meanAbsoluteError != null : !MEAN_ABSOLUTE_ERROR_EDEFAULT.equals(meanAbsoluteError);
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RMS_ERROR:
				return RMS_ERROR_EDEFAULT == null ? rmsError != null : !RMS_ERROR_EDEFAULT.equals(rmsError);
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__RSQUARED:
				return RSQUARED_EDEFAULT == null ? rSquared != null : !RSQUARED_EDEFAULT.equals(rSquared);
			case ApproximationPackage.APPROXIMATION_TEST_RESULT__TEST_TASK:
				return getTestTask() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (meanPredictedValue: ");
		result.append(meanPredictedValue);
		result.append(", meanActualValue: ");
		result.append(meanActualValue);
		result.append(", meanAbsoluteError: ");
		result.append(meanAbsoluteError);
		result.append(", rmsError: ");
		result.append(rmsError);
		result.append(", rSquared: ");
		result.append(rSquared);
		result.append(')');
		return result.toString();
	}

} //ApproximationTestResultImpl
