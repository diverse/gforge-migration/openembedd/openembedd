/**
 * INRIA/IRISA
 *
 * $Id: FrequentItemSetFunctionSettingsItemProvider.java,v 1.1 2008-04-01 09:35:54 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;
import orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.provider.MiningFunctionSettingsItemProvider;

import orgomg.cwm.objectmodel.core.provider.CWMEditPlugin;

/**
 * This is the item provider adapter for a {@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FrequentItemSetFunctionSettingsItemProvider
	extends MiningFunctionSettingsItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrequentItemSetFunctionSettingsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMinimumSupportPropertyDescriptor(object);
			addMaximumSetSizePropertyDescriptor(object);
			addExclusionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Minimum Support feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinimumSupportPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FrequentItemSetFunctionSettings_minimumSupport_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FrequentItemSetFunctionSettings_minimumSupport_feature", "_UI_FrequentItemSetFunctionSettings_type"),
				 AssociationrulesPackage.Literals.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Maximum Set Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaximumSetSizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FrequentItemSetFunctionSettings_maximumSetSize_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FrequentItemSetFunctionSettings_maximumSetSize_feature", "_UI_FrequentItemSetFunctionSettings_type"),
				 AssociationrulesPackage.Literals.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Exclusion feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExclusionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FrequentItemSetFunctionSettings_exclusion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FrequentItemSetFunctionSettings_exclusion_feature", "_UI_FrequentItemSetFunctionSettings_type"),
				 AssociationrulesPackage.Literals.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns FrequentItemSetFunctionSettings.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FrequentItemSetFunctionSettings"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FrequentItemSetFunctionSettings)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_FrequentItemSetFunctionSettings_type") :
			getString("_UI_FrequentItemSetFunctionSettings_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FrequentItemSetFunctionSettings.class)) {
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT:
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CWMEditPlugin.INSTANCE;
	}

}
