/**
 * INRIA/IRISA
 *
 * $Id: ApplyTargetValueItem.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Target Value Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This indicates that the probability value of the given target value is to appear in the output.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem#getTargetValue <em>Target Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getApplyTargetValueItem()
 * @model
 * @generated
 */
public interface ApplyTargetValueItem extends ApplyOutputItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Target Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Value</em>' reference.
	 * @see #setTargetValue(Category)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getApplyTargetValueItem_TargetValue()
	 * @model required="true"
	 * @generated
	 */
	Category getTargetValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ApplyTargetValueItem#getTargetValue <em>Target Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Value</em>' reference.
	 * @see #getTargetValue()
	 * @generated
	 */
	void setTargetValue(Category value);

} // ApplyTargetValueItem
