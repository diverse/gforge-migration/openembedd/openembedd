/**
 * INRIA/IRISA
 *
 * $Id: ApplyProbabilityItemImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyProbabilityItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apply Probability Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ApplyProbabilityItemImpl extends ApplyContentItemImpl implements ApplyProbabilityItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplyProbabilityItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.APPLY_PROBABILITY_ITEM;
	}

} //ApplyProbabilityItemImpl
