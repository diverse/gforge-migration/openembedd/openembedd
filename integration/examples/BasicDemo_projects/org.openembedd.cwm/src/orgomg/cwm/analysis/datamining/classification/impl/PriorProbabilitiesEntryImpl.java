/**
 * INRIA/IRISA
 *
 * $Id: PriorProbabilitiesEntryImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;
import orgomg.cwm.analysis.datamining.classification.PriorProbabilities;
import orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prior Probabilities Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl#getPriorProbability <em>Prior Probability</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl#getPriors <em>Priors</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.impl.PriorProbabilitiesEntryImpl#getTargetValue <em>Target Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PriorProbabilitiesEntryImpl extends ModelElementImpl implements PriorProbabilitiesEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getPriorProbability() <em>Prior Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorProbability()
	 * @generated
	 * @ordered
	 */
	protected static final String PRIOR_PROBABILITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPriorProbability() <em>Prior Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorProbability()
	 * @generated
	 * @ordered
	 */
	protected String priorProbability = PRIOR_PROBABILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTargetValue() <em>Target Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetValue()
	 * @generated
	 * @ordered
	 */
	protected Category targetValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorProbabilitiesEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassificationPackage.Literals.PRIOR_PROBABILITIES_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPriorProbability() {
		return priorProbability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorProbability(String newPriorProbability) {
		String oldPriorProbability = priorProbability;
		priorProbability = newPriorProbability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY, oldPriorProbability, priorProbability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorProbabilities getPriors() {
		if (eContainerFeatureID != ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS) return null;
		return (PriorProbabilities)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriors(PriorProbabilities newPriors, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newPriors, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriors(PriorProbabilities newPriors) {
		if (newPriors != eInternalContainer() || (eContainerFeatureID != ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS && newPriors != null)) {
			if (EcoreUtil.isAncestor(this, newPriors))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPriors != null)
				msgs = ((InternalEObject)newPriors).eInverseAdd(this, ClassificationPackage.PRIOR_PROBABILITIES__PRIOR, PriorProbabilities.class, msgs);
			msgs = basicSetPriors(newPriors, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS, newPriors, newPriors));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getTargetValue() {
		if (targetValue != null && targetValue.eIsProxy()) {
			InternalEObject oldTargetValue = (InternalEObject)targetValue;
			targetValue = (Category)eResolveProxy(oldTargetValue);
			if (targetValue != oldTargetValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE, oldTargetValue, targetValue));
			}
		}
		return targetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category basicGetTargetValue() {
		return targetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetValue(Category newTargetValue) {
		Category oldTargetValue = targetValue;
		targetValue = newTargetValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE, oldTargetValue, targetValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetPriors((PriorProbabilities)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				return basicSetPriors(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				return eInternalContainer().eInverseRemove(this, ClassificationPackage.PRIOR_PROBABILITIES__PRIOR, PriorProbabilities.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY:
				return getPriorProbability();
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				return getPriors();
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE:
				if (resolve) return getTargetValue();
				return basicGetTargetValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY:
				setPriorProbability((String)newValue);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				setPriors((PriorProbabilities)newValue);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE:
				setTargetValue((Category)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY:
				setPriorProbability(PRIOR_PROBABILITY_EDEFAULT);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				setPriors((PriorProbabilities)null);
				return;
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE:
				setTargetValue((Category)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIOR_PROBABILITY:
				return PRIOR_PROBABILITY_EDEFAULT == null ? priorProbability != null : !PRIOR_PROBABILITY_EDEFAULT.equals(priorProbability);
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__PRIORS:
				return getPriors() != null;
			case ClassificationPackage.PRIOR_PROBABILITIES_ENTRY__TARGET_VALUE:
				return targetValue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (priorProbability: ");
		result.append(priorProbability);
		result.append(')');
		return result.toString();
	}

} //PriorProbabilitiesEntryImpl
