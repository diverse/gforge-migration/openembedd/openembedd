/**
 * INRIA/IRISA
 *
 * $Id: SupervisedFunctionSettings.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A SupervisedFunctionSettings is a subclass of MiningFunctionSettings that supports features that are unique and shared by  supervised functions, e.g., classification and approximation, as well as algorithms, e.g., decision trees and neural networks.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getSupervisedFunctionSettings()
 * @model
 * @generated
 */
public interface SupervisedFunctionSettings extends MiningFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // SupervisedFunctionSettings
