/**
 * INRIA/IRISA
 *
 * $Id: AttributeUsageSet.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Usage Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An AttributeUsageSet object contains a collection of AttributeUsage objects. This specifes how MinigAttributes are to be used or manipulated by a model. The specification may contain at most one AttributeUsage object for each MiningAttribute in the LogicalDataSpecification. The default usage is active for an attribute if no entry for a MiningAttribute is present.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings <em>Settings</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsageSet()
 * @model
 * @generated
 */
public interface AttributeUsageSet extends orgomg.cwm.objectmodel.core.Class {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Settings</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAttributeUsageSet <em>Attribute Usage Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Settings</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Settings</em>' container reference.
	 * @see #setSettings(MiningFunctionSettings)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsageSet_Settings()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getAttributeUsageSet
	 * @model opposite="attributeUsageSet" required="true"
	 * @generated
	 */
	MiningFunctionSettings getSettings();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet#getSettings <em>Settings</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Settings</em>' container reference.
	 * @see #getSettings()
	 * @generated
	 */
	void setSettings(MiningFunctionSettings value);

} // AttributeUsageSet
