/**
 * INRIA/IRISA
 *
 * $Id: ApplyScoreItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Score Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This indicates that the score (target value) of the prediction (whose rank is specified here) should appear in the output.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyScoreItem()
 * @model
 * @generated
 */
public interface ApplyScoreItem extends ApplyContentItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ApplyScoreItem
