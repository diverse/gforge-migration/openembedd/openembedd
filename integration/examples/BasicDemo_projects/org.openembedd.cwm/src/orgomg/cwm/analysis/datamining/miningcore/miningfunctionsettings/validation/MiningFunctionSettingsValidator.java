/**
 * <copyright>
 * </copyright>
 *
 * $Id: MiningFunctionSettingsValidator.java,v 1.1 2008-04-01 09:35:29 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.validation;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsageSet;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningAlgorithmSettings;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface MiningFunctionSettingsValidator {
	boolean validate();

	boolean validateDesiredExecutionTimeInMinutes(long value);
	boolean validateAlgorithmSettings(MiningAlgorithmSettings value);
	boolean validateAttributeUsageSet(AttributeUsageSet value);
	boolean validateLogicalData(LogicalData value);
	boolean validateSchema(Schema value);
}
