/**
 * INRIA/IRISA
 *
 * $Id: AttributeAssignmentImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignment;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.Attribute;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl#getLogicalAttribute <em>Logical Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeAssignmentImpl#getOrderIdAttribute <em>Order Id Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeAssignmentImpl extends ModelElementImpl implements AttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getLogicalAttribute() <em>Logical Attribute</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<MiningAttribute> logicalAttribute;

	/**
	 * The cached value of the '{@link #getOrderIdAttribute() <em>Order Id Attribute</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> orderIdAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.ATTRIBUTE_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MiningAttribute> getLogicalAttribute() {
		if (logicalAttribute == null) {
			logicalAttribute = new EObjectResolvingEList<MiningAttribute>(MiningAttribute.class, this, MiningdataPackage.ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE);
		}
		return logicalAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getOrderIdAttribute() {
		if (orderIdAttribute == null) {
			orderIdAttribute = new EObjectResolvingEList<Attribute>(Attribute.class, this, MiningdataPackage.ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE);
		}
		return orderIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE:
				return getLogicalAttribute();
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE:
				return getOrderIdAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE:
				getLogicalAttribute().clear();
				getLogicalAttribute().addAll((Collection<? extends MiningAttribute>)newValue);
				return;
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE:
				getOrderIdAttribute().clear();
				getOrderIdAttribute().addAll((Collection<? extends Attribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE:
				getLogicalAttribute().clear();
				return;
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE:
				getOrderIdAttribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__LOGICAL_ATTRIBUTE:
				return logicalAttribute != null && !logicalAttribute.isEmpty();
			case MiningdataPackage.ATTRIBUTE_ASSIGNMENT__ORDER_ID_ATTRIBUTE:
				return orderIdAttribute != null && !orderIdAttribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AttributeAssignmentImpl
