/**
 * INRIA/IRISA
 *
 * $Id: ReversePivotAttributeAssignment.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reverse Pivot Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Constraint: The logicalAttribute must be set valued.
 * This object is used when the input data is in tabular (2-D) form.
 * 
 * The sets are represented by enumerating their elements based on the selection functions.
 * For example, if the attribute selection function is "isOne" and the value selection function is "attribute", the we get:
 * A B C D E F
 * 1 0 0 1 0 0 = {A, D}
 * 0 0 0 0 0 1 = {F}
 * 0 0 0 0 0 0 = {}
 * 
 * Each of the input attributes (A, B, C, D, E, and F) is a selector attribute in this object. It works best for a small number of members known a priori.
 * 
 * In some cases, when the potential number of values is large, but it is also known that the set sizes are all small, e.g., less than 6, then we get the following:
 * A    B    C    D    F
 * X    Y   NULL NULL NULL = {X, Y}
 * Z    NULL NULL NULL NULL = {Z}
 * NULL NULL NULL NULL NULL = {}
 * 
 * In the above example, the attribute selection function is "isNotNull" and the value selection function is "value".
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getAttributeSelectionFunction <em>Attribute Selection Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getValueSelectionFunction <em>Value Selection Function</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getSelectorAttribute <em>Selector Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getReversePivotAttributeAssignment()
 * @model
 * @generated
 */
public interface ReversePivotAttributeAssignment extends AttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Attribute Selection Function</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This describes how the selector attributes are selected based on their values.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attribute Selection Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction
	 * @see #setAttributeSelectionFunction(AttributeSelectionFunction)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getReversePivotAttributeAssignment_AttributeSelectionFunction()
	 * @model
	 * @generated
	 */
	AttributeSelectionFunction getAttributeSelectionFunction();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getAttributeSelectionFunction <em>Attribute Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Selection Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeSelectionFunction
	 * @see #getAttributeSelectionFunction()
	 * @generated
	 */
	void setAttributeSelectionFunction(AttributeSelectionFunction value);

	/**
	 * Returns the value of the '<em><b>Value Selection Function</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This describes whether the value or the name of a selector attribute to appear in the destination logical attribute when the selector attribute satisfies the specified AttributeSelectionFunction.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Value Selection Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction
	 * @see #setValueSelectionFunction(ValueSelectionFunction)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getReversePivotAttributeAssignment_ValueSelectionFunction()
	 * @model
	 * @generated
	 */
	ValueSelectionFunction getValueSelectionFunction();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.ReversePivotAttributeAssignment#getValueSelectionFunction <em>Value Selection Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Selection Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.ValueSelectionFunction
	 * @see #getValueSelectionFunction()
	 * @generated
	 */
	void setValueSelectionFunction(ValueSelectionFunction value);

	/**
	 * Returns the value of the '<em><b>Selector Attribute</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.objectmodel.core.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selector Attribute</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selector Attribute</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getReversePivotAttributeAssignment_SelectorAttribute()
	 * @model required="true"
	 * @generated
	 */
	EList<Attribute> getSelectorAttribute();

} // ReversePivotAttributeAssignment
