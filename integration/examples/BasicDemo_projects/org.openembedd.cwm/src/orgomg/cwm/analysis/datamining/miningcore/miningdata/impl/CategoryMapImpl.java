/**
 * INRIA/IRISA
 *
 * $Id: CategoryMapImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Map</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl#isIsMultiLevel <em>Is Multi Level</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl#isIsItemMap <em>Is Item Map</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapImpl#getTaxonomy <em>Taxonomy</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMapImpl extends ModelElementImpl implements CategoryMap {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #isIsMultiLevel() <em>Is Multi Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMultiLevel()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_MULTI_LEVEL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsMultiLevel() <em>Is Multi Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMultiLevel()
	 * @generated
	 * @ordered
	 */
	protected boolean isMultiLevel = IS_MULTI_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsItemMap() <em>Is Item Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsItemMap()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ITEM_MAP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsItemMap() <em>Is Item Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsItemMap()
	 * @generated
	 * @ordered
	 */
	protected boolean isItemMap = IS_ITEM_MAP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMapImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MAP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsMultiLevel() {
		return isMultiLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsMultiLevel(boolean newIsMultiLevel) {
		boolean oldIsMultiLevel = isMultiLevel;
		isMultiLevel = newIsMultiLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP__IS_MULTI_LEVEL, oldIsMultiLevel, isMultiLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsItemMap() {
		return isItemMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsItemMap(boolean newIsItemMap) {
		boolean oldIsItemMap = isItemMap;
		isItemMap = newIsItemMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP__IS_ITEM_MAP, oldIsItemMap, isItemMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryTaxonomy getTaxonomy() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_MAP__TAXONOMY) return null;
		return (CategoryTaxonomy)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaxonomy(CategoryTaxonomy newTaxonomy, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTaxonomy, MiningdataPackage.CATEGORY_MAP__TAXONOMY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaxonomy(CategoryTaxonomy newTaxonomy) {
		if (newTaxonomy != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_MAP__TAXONOMY && newTaxonomy != null)) {
			if (EcoreUtil.isAncestor(this, newTaxonomy))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTaxonomy != null)
				msgs = ((InternalEObject)newTaxonomy).eInverseAdd(this, MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP, CategoryTaxonomy.class, msgs);
			msgs = basicSetTaxonomy(newTaxonomy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP__TAXONOMY, newTaxonomy, newTaxonomy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTaxonomy((CategoryTaxonomy)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				return basicSetTaxonomy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP, CategoryTaxonomy.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__IS_MULTI_LEVEL:
				return isIsMultiLevel() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.CATEGORY_MAP__IS_ITEM_MAP:
				return isIsItemMap() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				return getTaxonomy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__IS_MULTI_LEVEL:
				setIsMultiLevel(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.CATEGORY_MAP__IS_ITEM_MAP:
				setIsItemMap(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				setTaxonomy((CategoryTaxonomy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__IS_MULTI_LEVEL:
				setIsMultiLevel(IS_MULTI_LEVEL_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MAP__IS_ITEM_MAP:
				setIsItemMap(IS_ITEM_MAP_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				setTaxonomy((CategoryTaxonomy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP__IS_MULTI_LEVEL:
				return isMultiLevel != IS_MULTI_LEVEL_EDEFAULT;
			case MiningdataPackage.CATEGORY_MAP__IS_ITEM_MAP:
				return isItemMap != IS_ITEM_MAP_EDEFAULT;
			case MiningdataPackage.CATEGORY_MAP__TAXONOMY:
				return getTaxonomy() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isMultiLevel: ");
		result.append(isMultiLevel);
		result.append(", isItemMap: ");
		result.append(isItemMap);
		result.append(')');
		return result.toString();
	}

} //CategoryMapImpl
