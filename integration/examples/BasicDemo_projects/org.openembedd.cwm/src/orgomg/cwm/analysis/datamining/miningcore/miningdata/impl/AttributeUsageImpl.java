/**
 * INRIA/IRISA
 *
 * $Id: AttributeUsageImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption;

import orgomg.cwm.objectmodel.core.impl.FeatureImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Usage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl#getUsage <em>Usage</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl#getWeight <em>Weight</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl#isSuppressDiscretization <em>Suppress Discretization</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl#isSuppressNormalization <em>Suppress Normalization</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.AttributeUsageImpl#getAttribute <em>Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeUsageImpl extends FeatureImpl implements AttributeUsage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getUsage() <em>Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsage()
	 * @generated
	 * @ordered
	 */
	protected static final UsageOption USAGE_EDEFAULT = UsageOption.ACTIVE_LITERAL;

	/**
	 * The cached value of the '{@link #getUsage() <em>Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsage()
	 * @generated
	 * @ordered
	 */
	protected UsageOption usage = USAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected static final String WEIGHT_EDEFAULT = "1.0";

	/**
	 * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected String weight = WEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #isSuppressDiscretization() <em>Suppress Discretization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressDiscretization()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SUPPRESS_DISCRETIZATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSuppressDiscretization() <em>Suppress Discretization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressDiscretization()
	 * @generated
	 * @ordered
	 */
	protected boolean suppressDiscretization = SUPPRESS_DISCRETIZATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isSuppressNormalization() <em>Suppress Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressNormalization()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SUPPRESS_NORMALIZATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSuppressNormalization() <em>Suppress Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressNormalization()
	 * @generated
	 * @ordered
	 */
	protected boolean suppressNormalization = SUPPRESS_NORMALIZATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttribute() <em>Attribute</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<LogicalAttribute> attribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeUsageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.ATTRIBUTE_USAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsageOption getUsage() {
		return usage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsage(UsageOption newUsage) {
		UsageOption oldUsage = usage;
		usage = newUsage == null ? USAGE_EDEFAULT : newUsage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_USAGE__USAGE, oldUsage, usage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeight(String newWeight) {
		String oldWeight = weight;
		weight = newWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT, oldWeight, weight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSuppressDiscretization() {
		return suppressDiscretization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressDiscretization(boolean newSuppressDiscretization) {
		boolean oldSuppressDiscretization = suppressDiscretization;
		suppressDiscretization = newSuppressDiscretization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION, oldSuppressDiscretization, suppressDiscretization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSuppressNormalization() {
		return suppressNormalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressNormalization(boolean newSuppressNormalization) {
		boolean oldSuppressNormalization = suppressNormalization;
		suppressNormalization = newSuppressNormalization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION, oldSuppressNormalization, suppressNormalization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogicalAttribute> getAttribute() {
		if (attribute == null) {
			attribute = new EObjectResolvingEList<LogicalAttribute>(LogicalAttribute.class, this, MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE);
		}
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE__USAGE:
				return getUsage();
			case MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT:
				return getWeight();
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION:
				return isSuppressDiscretization() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION:
				return isSuppressNormalization() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE:
				return getAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE__USAGE:
				setUsage((UsageOption)newValue);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT:
				setWeight((String)newValue);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION:
				setSuppressDiscretization(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION:
				setSuppressNormalization(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE:
				getAttribute().clear();
				getAttribute().addAll((Collection<? extends LogicalAttribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE__USAGE:
				setUsage(USAGE_EDEFAULT);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT:
				setWeight(WEIGHT_EDEFAULT);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION:
				setSuppressDiscretization(SUPPRESS_DISCRETIZATION_EDEFAULT);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION:
				setSuppressNormalization(SUPPRESS_NORMALIZATION_EDEFAULT);
				return;
			case MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE:
				getAttribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ATTRIBUTE_USAGE__USAGE:
				return usage != USAGE_EDEFAULT;
			case MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT:
				return WEIGHT_EDEFAULT == null ? weight != null : !WEIGHT_EDEFAULT.equals(weight);
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION:
				return suppressDiscretization != SUPPRESS_DISCRETIZATION_EDEFAULT;
			case MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION:
				return suppressNormalization != SUPPRESS_NORMALIZATION_EDEFAULT;
			case MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE:
				return attribute != null && !attribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (usage: ");
		result.append(usage);
		result.append(", weight: ");
		result.append(weight);
		result.append(", suppressDiscretization: ");
		result.append(suppressDiscretization);
		result.append(", suppressNormalization: ");
		result.append(suppressNormalization);
		result.append(')');
		return result.toString();
	}

} //AttributeUsageImpl
