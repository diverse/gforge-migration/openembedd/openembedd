/**
 * INRIA/IRISA
 *
 * $Id: SupervisedFactoryImpl.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.supervised.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SupervisedFactoryImpl extends EFactoryImpl implements SupervisedFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SupervisedFactory init() {
		try {
			SupervisedFactory theSupervisedFactory = (SupervisedFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/supervised.ecore"); 
			if (theSupervisedFactory != null) {
				return theSupervisedFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SupervisedFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupervisedFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT: return createLiftAnalysisPoint();
			case SupervisedPackage.LIFT_ANALYSIS: return createLiftAnalysis();
			case SupervisedPackage.MINING_TEST_TASK: return createMiningTestTask();
			case SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS: return createSupervisedFunctionSettings();
			case SupervisedPackage.MINING_TEST_RESULT: return createMiningTestResult();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiftAnalysisPoint createLiftAnalysisPoint() {
		LiftAnalysisPointImpl liftAnalysisPoint = new LiftAnalysisPointImpl();
		return liftAnalysisPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiftAnalysis createLiftAnalysis() {
		LiftAnalysisImpl liftAnalysis = new LiftAnalysisImpl();
		return liftAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningTestTask createMiningTestTask() {
		MiningTestTaskImpl miningTestTask = new MiningTestTaskImpl();
		return miningTestTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupervisedFunctionSettings createSupervisedFunctionSettings() {
		SupervisedFunctionSettingsImpl supervisedFunctionSettings = new SupervisedFunctionSettingsImpl();
		return supervisedFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningTestResult createMiningTestResult() {
		MiningTestResultImpl miningTestResult = new MiningTestResultImpl();
		return miningTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupervisedPackage getSupervisedPackage() {
		return (SupervisedPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SupervisedPackage getPackage() {
		return SupervisedPackage.eINSTANCE;
	}

} //SupervisedFactoryImpl
