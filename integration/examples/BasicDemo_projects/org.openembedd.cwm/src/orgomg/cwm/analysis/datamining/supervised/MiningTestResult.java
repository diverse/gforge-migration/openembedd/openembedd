/**
 * INRIA/IRISA
 *
 * $Id: MiningTestResult.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Test Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the result of a test task applied to a supervised model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getNumberOfTestRecords <em>Number Of Test Records</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getLiftAnalysis <em>Lift Analysis</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestResult()
 * @model
 * @generated
 */
public interface MiningTestResult extends MiningResult {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Number Of Test Records</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This represents the number of records applied to the test task.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Number Of Test Records</em>' attribute.
	 * @see #setNumberOfTestRecords(long)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestResult_NumberOfTestRecords()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getNumberOfTestRecords();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getNumberOfTestRecords <em>Number Of Test Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Test Records</em>' attribute.
	 * @see #getNumberOfTestRecords()
	 * @generated
	 */
	void setNumberOfTestRecords(long value);

	/**
	 * Returns the value of the '<em><b>Lift Analysis</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTestResult <em>Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lift Analysis</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lift Analysis</em>' containment reference.
	 * @see #setLiftAnalysis(LiftAnalysis)
	 * @see orgomg.cwm.analysis.datamining.supervised.SupervisedPackage#getMiningTestResult_LiftAnalysis()
	 * @see orgomg.cwm.analysis.datamining.supervised.LiftAnalysis#getTestResult
	 * @model opposite="testResult" containment="true"
	 * @generated
	 */
	LiftAnalysis getLiftAnalysis();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.supervised.MiningTestResult#getLiftAnalysis <em>Lift Analysis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lift Analysis</em>' containment reference.
	 * @see #getLiftAnalysis()
	 * @generated
	 */
	void setLiftAnalysis(LiftAnalysis value);

} // MiningTestResult
