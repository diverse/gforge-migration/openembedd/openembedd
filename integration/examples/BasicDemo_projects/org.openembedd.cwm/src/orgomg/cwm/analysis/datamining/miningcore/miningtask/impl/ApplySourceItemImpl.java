/**
 * INRIA/IRISA
 *
 * $Id: ApplySourceItemImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplySourceItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apply Source Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ApplySourceItemImpl extends ApplyOutputItemImpl implements ApplySourceItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplySourceItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.APPLY_SOURCE_ITEM;
	}

} //ApplySourceItemImpl
