/**
 * INRIA/IRISA
 *
 * $Id: ApplyProbabilityItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Probability Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This indicates that the probability value of the prediction (whose rank is specified here) should appear in the output.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyProbabilityItem()
 * @model
 * @generated
 */
public interface ApplyProbabilityItem extends ApplyContentItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ApplyProbabilityItem
