/**
 * INRIA/IRISA
 *
 * $Id: ApplySourceItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Source Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This indicates that the source attribute specified here appears in the output as well.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplySourceItem()
 * @model
 * @generated
 */
public interface ApplySourceItem extends ApplyOutputItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ApplySourceItem
