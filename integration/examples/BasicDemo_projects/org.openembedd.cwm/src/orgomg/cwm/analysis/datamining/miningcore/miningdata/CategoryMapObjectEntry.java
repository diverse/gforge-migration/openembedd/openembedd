/**
 * INRIA/IRISA
 *
 * $Id: CategoryMapObjectEntry.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Map Object Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Object representation of an edge in the categorization graph. This is analogous to a record in the CategoryMapTable. Each entry consists of child, parent, level and graphId attributes. If isItemMap is TRUE then the child attribute corresponds to item values.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getGraphId <em>Graph Id</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject <em>Map Object</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getChild <em>Child</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObjectEntry()
 * @model
 * @generated
 */
public interface CategoryMapObjectEntry extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Graph Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The graphId attribute identifies the graph to which this entry belongs and enables representing multiple categorization graphs in the same table. 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Graph Id</em>' attribute.
	 * @see #setGraphId(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObjectEntry_GraphId()
	 * @model dataType="orgomg.cwm.objectmodel.core.Any"
	 * @generated
	 */
	String getGraphId();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getGraphId <em>Graph Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph Id</em>' attribute.
	 * @see #getGraphId()
	 * @generated
	 */
	void setGraphId(String value);

	/**
	 * Returns the value of the '<em><b>Map Object</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Object</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Object</em>' container reference.
	 * @see #setMapObject(CategoryMapObject)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObjectEntry_MapObject()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject#getEntry
	 * @model opposite="entry" required="true"
	 * @generated
	 */
	CategoryMapObject getMapObject();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject <em>Map Object</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Map Object</em>' container reference.
	 * @see #getMapObject()
	 * @generated
	 */
	void setMapObject(CategoryMapObject value);

	/**
	 * Returns the value of the '<em><b>Child</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' reference.
	 * @see #setChild(Category)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObjectEntry_Child()
	 * @model required="true"
	 * @generated
	 */
	Category getChild();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getChild <em>Child</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child</em>' reference.
	 * @see #getChild()
	 * @generated
	 */
	void setChild(Category value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObjectEntry_Parent()
	 * @model required="true"
	 * @generated
	 */
	EList<Category> getParent();

} // CategoryMapObjectEntry
