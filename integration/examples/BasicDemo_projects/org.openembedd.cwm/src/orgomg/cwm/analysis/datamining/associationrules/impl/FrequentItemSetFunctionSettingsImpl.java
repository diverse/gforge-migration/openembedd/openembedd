/**
 * INRIA/IRISA
 *
 * $Id: FrequentItemSetFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:54 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;
import orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningFunctionSettingsImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Frequent Item Set Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl#getMinimumSupport <em>Minimum Support</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl#getMaximumSetSize <em>Maximum Set Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl#getExclusion <em>Exclusion</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FrequentItemSetFunctionSettingsImpl extends MiningFunctionSettingsImpl implements FrequentItemSetFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getMinimumSupport() <em>Minimum Support</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumSupport()
	 * @generated
	 * @ordered
	 */
	protected static final String MINIMUM_SUPPORT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinimumSupport() <em>Minimum Support</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumSupport()
	 * @generated
	 * @ordered
	 */
	protected String minimumSupport = MINIMUM_SUPPORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaximumSetSize() <em>Maximum Set Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumSetSize()
	 * @generated
	 * @ordered
	 */
	protected static final long MAXIMUM_SET_SIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaximumSetSize() <em>Maximum Set Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumSetSize()
	 * @generated
	 * @ordered
	 */
	protected long maximumSetSize = MAXIMUM_SET_SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExclusion() <em>Exclusion</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExclusion()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> exclusion;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FrequentItemSetFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssociationrulesPackage.Literals.FREQUENT_ITEM_SET_FUNCTION_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMinimumSupport() {
		return minimumSupport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinimumSupport(String newMinimumSupport) {
		String oldMinimumSupport = minimumSupport;
		minimumSupport = newMinimumSupport;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT, oldMinimumSupport, minimumSupport));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaximumSetSize() {
		return maximumSetSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumSetSize(long newMaximumSetSize) {
		long oldMaximumSetSize = maximumSetSize;
		maximumSetSize = newMaximumSetSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE, oldMaximumSetSize, maximumSetSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getExclusion() {
		if (exclusion == null) {
			exclusion = new EObjectResolvingEList<Category>(Category.class, this, AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION);
		}
		return exclusion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT:
				return getMinimumSupport();
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE:
				return new Long(getMaximumSetSize());
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION:
				return getExclusion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT:
				setMinimumSupport((String)newValue);
				return;
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE:
				setMaximumSetSize(((Long)newValue).longValue());
				return;
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION:
				getExclusion().clear();
				getExclusion().addAll((Collection<? extends Category>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT:
				setMinimumSupport(MINIMUM_SUPPORT_EDEFAULT);
				return;
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE:
				setMaximumSetSize(MAXIMUM_SET_SIZE_EDEFAULT);
				return;
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION:
				getExclusion().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT:
				return MINIMUM_SUPPORT_EDEFAULT == null ? minimumSupport != null : !MINIMUM_SUPPORT_EDEFAULT.equals(minimumSupport);
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE:
				return maximumSetSize != MAXIMUM_SET_SIZE_EDEFAULT;
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION:
				return exclusion != null && !exclusion.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minimumSupport: ");
		result.append(minimumSupport);
		result.append(", maximumSetSize: ");
		result.append(maximumSetSize);
		result.append(')');
		return result.toString();
	}

} //FrequentItemSetFunctionSettingsImpl
