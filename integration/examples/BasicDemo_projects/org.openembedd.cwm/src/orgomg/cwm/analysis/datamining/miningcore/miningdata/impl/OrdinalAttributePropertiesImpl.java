/**
 * INRIA/IRISA
 *
 * $Id: OrdinalAttributePropertiesImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.OrderType;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.OrdinalAttributeProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ordinal Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl#getOrderType <em>Order Type</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.OrdinalAttributePropertiesImpl#isIsCyclic <em>Is Cyclic</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OrdinalAttributePropertiesImpl extends CategoricalAttributePropertiesImpl implements OrdinalAttributeProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getOrderType() <em>Order Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderType()
	 * @generated
	 * @ordered
	 */
	protected static final OrderType ORDER_TYPE_EDEFAULT = OrderType.AS_IS_LITERAL;

	/**
	 * The cached value of the '{@link #getOrderType() <em>Order Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderType()
	 * @generated
	 * @ordered
	 */
	protected OrderType orderType = ORDER_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsCyclic() <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCyclic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CYCLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsCyclic() <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCyclic()
	 * @generated
	 * @ordered
	 */
	protected boolean isCyclic = IS_CYCLIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrdinalAttributePropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.ORDINAL_ATTRIBUTE_PROPERTIES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderType getOrderType() {
		return orderType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrderType(OrderType newOrderType) {
		OrderType oldOrderType = orderType;
		orderType = newOrderType == null ? ORDER_TYPE_EDEFAULT : newOrderType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE, oldOrderType, orderType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsCyclic() {
		return isCyclic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsCyclic(boolean newIsCyclic) {
		boolean oldIsCyclic = isCyclic;
		isCyclic = newIsCyclic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC, oldIsCyclic, isCyclic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE:
				return getOrderType();
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				return isIsCyclic() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE:
				setOrderType((OrderType)newValue);
				return;
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				setIsCyclic(((Boolean)newValue).booleanValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE:
				setOrderType(ORDER_TYPE_EDEFAULT);
				return;
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				setIsCyclic(IS_CYCLIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__ORDER_TYPE:
				return orderType != ORDER_TYPE_EDEFAULT;
			case MiningdataPackage.ORDINAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				return isCyclic != IS_CYCLIC_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (orderType: ");
		result.append(orderType);
		result.append(", isCyclic: ");
		result.append(isCyclic);
		result.append(')');
		return result.toString();
	}

} //OrdinalAttributePropertiesImpl
