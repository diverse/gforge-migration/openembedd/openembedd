/**
 * INRIA/IRISA
 *
 * $Id: UsageOption.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Usage Option</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * UsageOption specifies how a MiningAttribute is used for mining operations. 
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getUsageOption()
 * @model
 * @generated
 */
public enum UsageOption implements Enumerator {
	/**
	 * The '<em><b>Active</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACTIVE
	 * @generated
	 * @ordered
	 */
	ACTIVE_LITERAL(0, "active", "active"),

	/**
	 * The '<em><b>Supplementary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUPPLEMENTARY
	 * @generated
	 * @ordered
	 */
	SUPPLEMENTARY_LITERAL(1, "supplementary", "supplementary"),

	/**
	 * The '<em><b>Target</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TARGET
	 * @generated
	 * @ordered
	 */
	TARGET_LITERAL(2, "target", "target");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Active</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The active option indicates that the attribute is used for for the mining function. This is the default for a mining attribute.
	 * <!-- end-model-doc -->
	 * @see #ACTIVE_LITERAL
	 * @model name="active"
	 * @generated
	 * @ordered
	 */
	public static final int ACTIVE = 0;

	/**
	 * The '<em><b>Supplementary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The supplementary option indicates that the attribute is not used for the mining function. It can be used to provide additional information used, e.g., for visualization or providing key attributes on apply output.
	 * <!-- end-model-doc -->
	 * @see #SUPPLEMENTARY_LITERAL
	 * @model name="supplementary"
	 * @generated
	 * @ordered
	 */
	public static final int SUPPLEMENTARY = 1;

	/**
	 * The '<em><b>Target</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The target option indicates that
	 * the attribute is active and used as a target for supervised model. This option is used for supervised models only. For unsupervised models a target option is the same as an the active option.
	 * <!-- end-model-doc -->
	 * @see #TARGET_LITERAL
	 * @model name="target"
	 * @generated
	 * @ordered
	 */
	public static final int TARGET = 2;

	/**
	 * An array of all the '<em><b>Usage Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final UsageOption[] VALUES_ARRAY =
		new UsageOption[] {
			ACTIVE_LITERAL,
			SUPPLEMENTARY_LITERAL,
			TARGET_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Usage Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<UsageOption> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Usage Option</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UsageOption get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UsageOption result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Usage Option</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UsageOption getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UsageOption result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Usage Option</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UsageOption get(int value) {
		switch (value) {
			case ACTIVE: return ACTIVE_LITERAL;
			case SUPPLEMENTARY: return SUPPLEMENTARY_LITERAL;
			case TARGET: return TARGET_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private UsageOption(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //UsageOption
