/**
 * INRIA/IRISA
 *
 * $Id: ModelSignature.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The model signature is a description of the input needed to apply a data mining model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.ModelSignature#getModel <em>Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getModelSignature()
 * @model
 * @generated
 */
public interface ModelSignature extends orgomg.cwm.objectmodel.core.Class {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelSignature <em>Model Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' container reference.
	 * @see #setModel(MiningModel)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getModelSignature_Model()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getModelSignature
	 * @model opposite="modelSignature" required="true"
	 * @generated
	 */
	MiningModel getModel();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.ModelSignature#getModel <em>Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' container reference.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(MiningModel value);

} // ModelSignature
