/**
 * INRIA/IRISA
 *
 * $Id: AttributeUsage.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Usage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An AttributeUsage object specifies how a MinigAttribute is to be used for mining operations.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getUsage <em>Usage</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getWeight <em>Weight</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressDiscretization <em>Suppress Discretization</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressNormalization <em>Suppress Normalization</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getAttribute <em>Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage()
 * @model
 * @generated
 */
public interface AttributeUsage extends Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Usage</b></em>' attribute.
	 * The default value is <code>"active"</code>.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The usage attribute  indicates if the MiningAttribute should be used by the model or not, according to the details specified in the UsageOption enumeration class.
	 * The default is "active".
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Usage</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @see #setUsage(UsageOption)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage_Usage()
	 * @model default="active"
	 * @generated
	 */
	UsageOption getUsage();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getUsage <em>Usage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usage</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @see #getUsage()
	 * @generated
	 */
	void setUsage(UsageOption value);

	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The attribute weight indicates the weight the algorithm should assign to an attribute. The default is 1.0, indicating no effect. The particular vendor defines what effect a given weight greater or less than one has on an attribute for a particular algorithm.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage_Weight()
	 * @model default="1.0" dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getWeight();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(String value);

	/**
	 * Returns the value of the '<em><b>Suppress Discretization</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This suppresses discretization to be performed on the attribute being specified, if true.
	 * The default is false.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Suppress Discretization</em>' attribute.
	 * @see #setSuppressDiscretization(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage_SuppressDiscretization()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isSuppressDiscretization();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressDiscretization <em>Suppress Discretization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Discretization</em>' attribute.
	 * @see #isSuppressDiscretization()
	 * @generated
	 */
	void setSuppressDiscretization(boolean value);

	/**
	 * Returns the value of the '<em><b>Suppress Normalization</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Normalization</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Normalization</em>' attribute.
	 * @see #setSuppressNormalization(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage_SuppressNormalization()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isSuppressNormalization();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage#isSuppressNormalization <em>Suppress Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Normalization</em>' attribute.
	 * @see #isSuppressNormalization()
	 * @generated
	 */
	void setSuppressNormalization(boolean value);

	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeUsage_Attribute()
	 * @model required="true"
	 * @generated
	 */
	EList<LogicalAttribute> getAttribute();

} // AttributeUsage
