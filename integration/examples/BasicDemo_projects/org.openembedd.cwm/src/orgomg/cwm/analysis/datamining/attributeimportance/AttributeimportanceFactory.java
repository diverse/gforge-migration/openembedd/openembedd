/**
 * INRIA/IRISA
 *
 * $Id: AttributeimportanceFactory.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.attributeimportance;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage
 * @generated
 */
public interface AttributeimportanceFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AttributeimportanceFactory eINSTANCE = orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportanceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Attribute Importance Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Importance Settings</em>'.
	 * @generated
	 */
	AttributeImportanceSettings createAttributeImportanceSettings();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AttributeimportancePackage getAttributeimportancePackage();

} //AttributeimportanceFactory
