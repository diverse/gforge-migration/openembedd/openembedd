/**
 * INRIA/IRISA
 *
 * $Id: MiningtaskPackage.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.objectmodel.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package defines the objects that are related to mining tasks. A MiningTask object represents a specific mining operation to be performed on a given data.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskFactory
 * @model kind="package"
 * @generated
 */
public interface MiningtaskPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "miningtask";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/miningcore/miningtask.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.miningcore.miningtask";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MiningtaskPackage eINSTANCE = orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyOutputItemImpl <em>Apply Output Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyOutputItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyOutputItem()
	 * @generated
	 */
	int APPLY_OUTPUT_ITEM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__NAME = MiningdataPackage.MINING_ATTRIBUTE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__VISIBILITY = MiningdataPackage.MINING_ATTRIBUTE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__CLIENT_DEPENDENCY = MiningdataPackage.MINING_ATTRIBUTE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__SUPPLIER_DEPENDENCY = MiningdataPackage.MINING_ATTRIBUTE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__CONSTRAINT = MiningdataPackage.MINING_ATTRIBUTE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__NAMESPACE = MiningdataPackage.MINING_ATTRIBUTE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__IMPORTER = MiningdataPackage.MINING_ATTRIBUTE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__STEREOTYPE = MiningdataPackage.MINING_ATTRIBUTE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__TAGGED_VALUE = MiningdataPackage.MINING_ATTRIBUTE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__DOCUMENT = MiningdataPackage.MINING_ATTRIBUTE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__DESCRIPTION = MiningdataPackage.MINING_ATTRIBUTE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__RESPONSIBLE_PARTY = MiningdataPackage.MINING_ATTRIBUTE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__ELEMENT_NODE = MiningdataPackage.MINING_ATTRIBUTE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__SET = MiningdataPackage.MINING_ATTRIBUTE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__RENDERED_OBJECT = MiningdataPackage.MINING_ATTRIBUTE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__VOCABULARY_ELEMENT = MiningdataPackage.MINING_ATTRIBUTE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__MEASUREMENT = MiningdataPackage.MINING_ATTRIBUTE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__CHANGE_REQUEST = MiningdataPackage.MINING_ATTRIBUTE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__OWNER_SCOPE = MiningdataPackage.MINING_ATTRIBUTE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__OWNER = MiningdataPackage.MINING_ATTRIBUTE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__FEATURE_NODE = MiningdataPackage.MINING_ATTRIBUTE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__FEATURE_MAP = MiningdataPackage.MINING_ATTRIBUTE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__CF_MAP = MiningdataPackage.MINING_ATTRIBUTE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__CHANGEABILITY = MiningdataPackage.MINING_ATTRIBUTE__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__MULTIPLICITY = MiningdataPackage.MINING_ATTRIBUTE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__ORDERING = MiningdataPackage.MINING_ATTRIBUTE__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__TARGET_SCOPE = MiningdataPackage.MINING_ATTRIBUTE__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__TYPE = MiningdataPackage.MINING_ATTRIBUTE__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__SLOT = MiningdataPackage.MINING_ATTRIBUTE__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__DISCRIMINATED_UNION = MiningdataPackage.MINING_ATTRIBUTE__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__INDEXED_FEATURE = MiningdataPackage.MINING_ATTRIBUTE__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__KEY_RELATIONSHIP = MiningdataPackage.MINING_ATTRIBUTE__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__UNIQUE_KEY = MiningdataPackage.MINING_ATTRIBUTE__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__INITIAL_VALUE = MiningdataPackage.MINING_ATTRIBUTE__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__DISPLAY_NAME = MiningdataPackage.MINING_ATTRIBUTE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__ATTRIBUTE_TYPE = MiningdataPackage.MINING_ATTRIBUTE__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM__APPLY_OUTPUT = MiningdataPackage.MINING_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Apply Output Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_OUTPUT_ITEM_FEATURE_COUNT = MiningdataPackage.MINING_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyContentItemImpl <em>Apply Content Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyContentItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyContentItem()
	 * @generated
	 */
	int APPLY_CONTENT_ITEM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__NAME = APPLY_OUTPUT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__VISIBILITY = APPLY_OUTPUT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__CLIENT_DEPENDENCY = APPLY_OUTPUT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__SUPPLIER_DEPENDENCY = APPLY_OUTPUT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__CONSTRAINT = APPLY_OUTPUT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__NAMESPACE = APPLY_OUTPUT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__IMPORTER = APPLY_OUTPUT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__STEREOTYPE = APPLY_OUTPUT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__TAGGED_VALUE = APPLY_OUTPUT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__DOCUMENT = APPLY_OUTPUT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__DESCRIPTION = APPLY_OUTPUT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__RESPONSIBLE_PARTY = APPLY_OUTPUT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__ELEMENT_NODE = APPLY_OUTPUT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__SET = APPLY_OUTPUT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__RENDERED_OBJECT = APPLY_OUTPUT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__VOCABULARY_ELEMENT = APPLY_OUTPUT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__MEASUREMENT = APPLY_OUTPUT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__CHANGE_REQUEST = APPLY_OUTPUT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__OWNER_SCOPE = APPLY_OUTPUT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__OWNER = APPLY_OUTPUT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__FEATURE_NODE = APPLY_OUTPUT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__FEATURE_MAP = APPLY_OUTPUT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__CF_MAP = APPLY_OUTPUT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__CHANGEABILITY = APPLY_OUTPUT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__MULTIPLICITY = APPLY_OUTPUT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__ORDERING = APPLY_OUTPUT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__TARGET_SCOPE = APPLY_OUTPUT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__TYPE = APPLY_OUTPUT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__SLOT = APPLY_OUTPUT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__DISCRIMINATED_UNION = APPLY_OUTPUT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__INDEXED_FEATURE = APPLY_OUTPUT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__KEY_RELATIONSHIP = APPLY_OUTPUT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__UNIQUE_KEY = APPLY_OUTPUT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__INITIAL_VALUE = APPLY_OUTPUT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__DISPLAY_NAME = APPLY_OUTPUT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__ATTRIBUTE_TYPE = APPLY_OUTPUT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__APPLY_OUTPUT = APPLY_OUTPUT_ITEM__APPLY_OUTPUT;

	/**
	 * The feature id for the '<em><b>Top Nth Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM__TOP_NTH_INDEX = APPLY_OUTPUT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Apply Content Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_CONTENT_ITEM_FEATURE_COUNT = APPLY_OUTPUT_ITEM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyProbabilityItemImpl <em>Apply Probability Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyProbabilityItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyProbabilityItem()
	 * @generated
	 */
	int APPLY_PROBABILITY_ITEM = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__NAME = APPLY_CONTENT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__VISIBILITY = APPLY_CONTENT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__CLIENT_DEPENDENCY = APPLY_CONTENT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__SUPPLIER_DEPENDENCY = APPLY_CONTENT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__CONSTRAINT = APPLY_CONTENT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__NAMESPACE = APPLY_CONTENT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__IMPORTER = APPLY_CONTENT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__STEREOTYPE = APPLY_CONTENT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__TAGGED_VALUE = APPLY_CONTENT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__DOCUMENT = APPLY_CONTENT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__DESCRIPTION = APPLY_CONTENT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__RESPONSIBLE_PARTY = APPLY_CONTENT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__ELEMENT_NODE = APPLY_CONTENT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__SET = APPLY_CONTENT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__RENDERED_OBJECT = APPLY_CONTENT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__VOCABULARY_ELEMENT = APPLY_CONTENT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__MEASUREMENT = APPLY_CONTENT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__CHANGE_REQUEST = APPLY_CONTENT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__OWNER_SCOPE = APPLY_CONTENT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__OWNER = APPLY_CONTENT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__FEATURE_NODE = APPLY_CONTENT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__FEATURE_MAP = APPLY_CONTENT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__CF_MAP = APPLY_CONTENT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__CHANGEABILITY = APPLY_CONTENT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__MULTIPLICITY = APPLY_CONTENT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__ORDERING = APPLY_CONTENT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__TARGET_SCOPE = APPLY_CONTENT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__TYPE = APPLY_CONTENT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__SLOT = APPLY_CONTENT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__DISCRIMINATED_UNION = APPLY_CONTENT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__INDEXED_FEATURE = APPLY_CONTENT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__KEY_RELATIONSHIP = APPLY_CONTENT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__UNIQUE_KEY = APPLY_CONTENT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__INITIAL_VALUE = APPLY_CONTENT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__DISPLAY_NAME = APPLY_CONTENT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__ATTRIBUTE_TYPE = APPLY_CONTENT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__APPLY_OUTPUT = APPLY_CONTENT_ITEM__APPLY_OUTPUT;

	/**
	 * The feature id for the '<em><b>Top Nth Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM__TOP_NTH_INDEX = APPLY_CONTENT_ITEM__TOP_NTH_INDEX;

	/**
	 * The number of structural features of the '<em>Apply Probability Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_PROBABILITY_ITEM_FEATURE_COUNT = APPLY_CONTENT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyRuleIdItemImpl <em>Apply Rule Id Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyRuleIdItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyRuleIdItem()
	 * @generated
	 */
	int APPLY_RULE_ID_ITEM = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__NAME = APPLY_CONTENT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__VISIBILITY = APPLY_CONTENT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__CLIENT_DEPENDENCY = APPLY_CONTENT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__SUPPLIER_DEPENDENCY = APPLY_CONTENT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__CONSTRAINT = APPLY_CONTENT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__NAMESPACE = APPLY_CONTENT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__IMPORTER = APPLY_CONTENT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__STEREOTYPE = APPLY_CONTENT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__TAGGED_VALUE = APPLY_CONTENT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__DOCUMENT = APPLY_CONTENT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__DESCRIPTION = APPLY_CONTENT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__RESPONSIBLE_PARTY = APPLY_CONTENT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__ELEMENT_NODE = APPLY_CONTENT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__SET = APPLY_CONTENT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__RENDERED_OBJECT = APPLY_CONTENT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__VOCABULARY_ELEMENT = APPLY_CONTENT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__MEASUREMENT = APPLY_CONTENT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__CHANGE_REQUEST = APPLY_CONTENT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__OWNER_SCOPE = APPLY_CONTENT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__OWNER = APPLY_CONTENT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__FEATURE_NODE = APPLY_CONTENT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__FEATURE_MAP = APPLY_CONTENT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__CF_MAP = APPLY_CONTENT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__CHANGEABILITY = APPLY_CONTENT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__MULTIPLICITY = APPLY_CONTENT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__ORDERING = APPLY_CONTENT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__TARGET_SCOPE = APPLY_CONTENT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__TYPE = APPLY_CONTENT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__SLOT = APPLY_CONTENT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__DISCRIMINATED_UNION = APPLY_CONTENT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__INDEXED_FEATURE = APPLY_CONTENT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__KEY_RELATIONSHIP = APPLY_CONTENT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__UNIQUE_KEY = APPLY_CONTENT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__INITIAL_VALUE = APPLY_CONTENT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__DISPLAY_NAME = APPLY_CONTENT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__ATTRIBUTE_TYPE = APPLY_CONTENT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__APPLY_OUTPUT = APPLY_CONTENT_ITEM__APPLY_OUTPUT;

	/**
	 * The feature id for the '<em><b>Top Nth Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM__TOP_NTH_INDEX = APPLY_CONTENT_ITEM__TOP_NTH_INDEX;

	/**
	 * The number of structural features of the '<em>Apply Rule Id Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_RULE_ID_ITEM_FEATURE_COUNT = APPLY_CONTENT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyScoreItemImpl <em>Apply Score Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyScoreItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyScoreItem()
	 * @generated
	 */
	int APPLY_SCORE_ITEM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__NAME = APPLY_CONTENT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__VISIBILITY = APPLY_CONTENT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__CLIENT_DEPENDENCY = APPLY_CONTENT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__SUPPLIER_DEPENDENCY = APPLY_CONTENT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__CONSTRAINT = APPLY_CONTENT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__NAMESPACE = APPLY_CONTENT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__IMPORTER = APPLY_CONTENT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__STEREOTYPE = APPLY_CONTENT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__TAGGED_VALUE = APPLY_CONTENT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__DOCUMENT = APPLY_CONTENT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__DESCRIPTION = APPLY_CONTENT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__RESPONSIBLE_PARTY = APPLY_CONTENT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__ELEMENT_NODE = APPLY_CONTENT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__SET = APPLY_CONTENT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__RENDERED_OBJECT = APPLY_CONTENT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__VOCABULARY_ELEMENT = APPLY_CONTENT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__MEASUREMENT = APPLY_CONTENT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__CHANGE_REQUEST = APPLY_CONTENT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__OWNER_SCOPE = APPLY_CONTENT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__OWNER = APPLY_CONTENT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__FEATURE_NODE = APPLY_CONTENT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__FEATURE_MAP = APPLY_CONTENT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__CF_MAP = APPLY_CONTENT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__CHANGEABILITY = APPLY_CONTENT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__MULTIPLICITY = APPLY_CONTENT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__ORDERING = APPLY_CONTENT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__TARGET_SCOPE = APPLY_CONTENT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__TYPE = APPLY_CONTENT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__SLOT = APPLY_CONTENT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__DISCRIMINATED_UNION = APPLY_CONTENT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__INDEXED_FEATURE = APPLY_CONTENT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__KEY_RELATIONSHIP = APPLY_CONTENT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__UNIQUE_KEY = APPLY_CONTENT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__INITIAL_VALUE = APPLY_CONTENT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__DISPLAY_NAME = APPLY_CONTENT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__ATTRIBUTE_TYPE = APPLY_CONTENT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__APPLY_OUTPUT = APPLY_CONTENT_ITEM__APPLY_OUTPUT;

	/**
	 * The feature id for the '<em><b>Top Nth Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM__TOP_NTH_INDEX = APPLY_CONTENT_ITEM__TOP_NTH_INDEX;

	/**
	 * The number of structural features of the '<em>Apply Score Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SCORE_ITEM_FEATURE_COUNT = APPLY_CONTENT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplySourceItemImpl <em>Apply Source Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplySourceItemImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplySourceItem()
	 * @generated
	 */
	int APPLY_SOURCE_ITEM = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__NAME = APPLY_OUTPUT_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__VISIBILITY = APPLY_OUTPUT_ITEM__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__CLIENT_DEPENDENCY = APPLY_OUTPUT_ITEM__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__SUPPLIER_DEPENDENCY = APPLY_OUTPUT_ITEM__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__CONSTRAINT = APPLY_OUTPUT_ITEM__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__NAMESPACE = APPLY_OUTPUT_ITEM__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__IMPORTER = APPLY_OUTPUT_ITEM__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__STEREOTYPE = APPLY_OUTPUT_ITEM__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__TAGGED_VALUE = APPLY_OUTPUT_ITEM__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__DOCUMENT = APPLY_OUTPUT_ITEM__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__DESCRIPTION = APPLY_OUTPUT_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__RESPONSIBLE_PARTY = APPLY_OUTPUT_ITEM__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__ELEMENT_NODE = APPLY_OUTPUT_ITEM__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__SET = APPLY_OUTPUT_ITEM__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__RENDERED_OBJECT = APPLY_OUTPUT_ITEM__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__VOCABULARY_ELEMENT = APPLY_OUTPUT_ITEM__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__MEASUREMENT = APPLY_OUTPUT_ITEM__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__CHANGE_REQUEST = APPLY_OUTPUT_ITEM__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__OWNER_SCOPE = APPLY_OUTPUT_ITEM__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__OWNER = APPLY_OUTPUT_ITEM__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__FEATURE_NODE = APPLY_OUTPUT_ITEM__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__FEATURE_MAP = APPLY_OUTPUT_ITEM__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__CF_MAP = APPLY_OUTPUT_ITEM__CF_MAP;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__CHANGEABILITY = APPLY_OUTPUT_ITEM__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__MULTIPLICITY = APPLY_OUTPUT_ITEM__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__ORDERING = APPLY_OUTPUT_ITEM__ORDERING;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__TARGET_SCOPE = APPLY_OUTPUT_ITEM__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__TYPE = APPLY_OUTPUT_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__SLOT = APPLY_OUTPUT_ITEM__SLOT;

	/**
	 * The feature id for the '<em><b>Discriminated Union</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__DISCRIMINATED_UNION = APPLY_OUTPUT_ITEM__DISCRIMINATED_UNION;

	/**
	 * The feature id for the '<em><b>Indexed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__INDEXED_FEATURE = APPLY_OUTPUT_ITEM__INDEXED_FEATURE;

	/**
	 * The feature id for the '<em><b>Key Relationship</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__KEY_RELATIONSHIP = APPLY_OUTPUT_ITEM__KEY_RELATIONSHIP;

	/**
	 * The feature id for the '<em><b>Unique Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__UNIQUE_KEY = APPLY_OUTPUT_ITEM__UNIQUE_KEY;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__INITIAL_VALUE = APPLY_OUTPUT_ITEM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__DISPLAY_NAME = APPLY_OUTPUT_ITEM__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__ATTRIBUTE_TYPE = APPLY_OUTPUT_ITEM__ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM__APPLY_OUTPUT = APPLY_OUTPUT_ITEM__APPLY_OUTPUT;

	/**
	 * The number of structural features of the '<em>Apply Source Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_SOURCE_ITEM_FEATURE_COUNT = APPLY_OUTPUT_ITEM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyOutputImpl <em>Mining Apply Output</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyOutputImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningApplyOutput()
	 * @generated
	 */
	int MINING_APPLY_OUTPUT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Item</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT__ITEM = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mining Apply Output</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_OUTPUT_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl <em>Mining Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningTask()
	 * @generated
	 */
	int MINING_TASK = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__IMPORTER = CorePackage.MODEL_ELEMENT__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__DOCUMENT = CorePackage.MODEL_ELEMENT__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__DESCRIPTION = CorePackage.MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__RESPONSIBLE_PARTY = CorePackage.MODEL_ELEMENT__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__ELEMENT_NODE = CorePackage.MODEL_ELEMENT__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__SET = CorePackage.MODEL_ELEMENT__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__RENDERED_OBJECT = CorePackage.MODEL_ELEMENT__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__VOCABULARY_ELEMENT = CorePackage.MODEL_ELEMENT__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__MEASUREMENT = CorePackage.MODEL_ELEMENT__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__CHANGE_REQUEST = CorePackage.MODEL_ELEMENT__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Input Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__INPUT_MODEL = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__INPUT_DATA = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Model Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__MODEL_ASSIGNMENT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK__SCHEMA = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Mining Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TASK_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl <em>Mining Apply Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningApplyTask()
	 * @generated
	 */
	int MINING_APPLY_TASK = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__NAME = MINING_TASK__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__VISIBILITY = MINING_TASK__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__CLIENT_DEPENDENCY = MINING_TASK__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__SUPPLIER_DEPENDENCY = MINING_TASK__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__CONSTRAINT = MINING_TASK__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__NAMESPACE = MINING_TASK__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__IMPORTER = MINING_TASK__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__STEREOTYPE = MINING_TASK__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__TAGGED_VALUE = MINING_TASK__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__DOCUMENT = MINING_TASK__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__DESCRIPTION = MINING_TASK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__RESPONSIBLE_PARTY = MINING_TASK__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__ELEMENT_NODE = MINING_TASK__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__SET = MINING_TASK__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__RENDERED_OBJECT = MINING_TASK__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__VOCABULARY_ELEMENT = MINING_TASK__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__MEASUREMENT = MINING_TASK__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__CHANGE_REQUEST = MINING_TASK__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Input Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__INPUT_MODEL = MINING_TASK__INPUT_MODEL;

	/**
	 * The feature id for the '<em><b>Input Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__INPUT_DATA = MINING_TASK__INPUT_DATA;

	/**
	 * The feature id for the '<em><b>Model Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__MODEL_ASSIGNMENT = MINING_TASK__MODEL_ASSIGNMENT;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__SCHEMA = MINING_TASK__SCHEMA;

	/**
	 * The feature id for the '<em><b>Output Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__OUTPUT_OPTION = MINING_TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__OUTPUT_ASSIGNMENT = MINING_TASK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Apply Output</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK__APPLY_OUTPUT = MINING_TASK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Mining Apply Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_APPLY_TASK_FEATURE_COUNT = MINING_TASK_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningBuildTaskImpl <em>Mining Build Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningBuildTaskImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningBuildTask()
	 * @generated
	 */
	int MINING_BUILD_TASK = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__NAME = MINING_TASK__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__VISIBILITY = MINING_TASK__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__CLIENT_DEPENDENCY = MINING_TASK__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__SUPPLIER_DEPENDENCY = MINING_TASK__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__CONSTRAINT = MINING_TASK__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__NAMESPACE = MINING_TASK__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__IMPORTER = MINING_TASK__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__STEREOTYPE = MINING_TASK__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__TAGGED_VALUE = MINING_TASK__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__DOCUMENT = MINING_TASK__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__DESCRIPTION = MINING_TASK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__RESPONSIBLE_PARTY = MINING_TASK__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__ELEMENT_NODE = MINING_TASK__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__SET = MINING_TASK__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__RENDERED_OBJECT = MINING_TASK__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__VOCABULARY_ELEMENT = MINING_TASK__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__MEASUREMENT = MINING_TASK__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__CHANGE_REQUEST = MINING_TASK__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Input Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__INPUT_MODEL = MINING_TASK__INPUT_MODEL;

	/**
	 * The feature id for the '<em><b>Input Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__INPUT_DATA = MINING_TASK__INPUT_DATA;

	/**
	 * The feature id for the '<em><b>Model Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__MODEL_ASSIGNMENT = MINING_TASK__MODEL_ASSIGNMENT;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__SCHEMA = MINING_TASK__SCHEMA;

	/**
	 * The feature id for the '<em><b>Result Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__RESULT_MODEL = MINING_TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mining Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__MINING_SETTINGS = MINING_TASK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Validation Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__VALIDATION_ASSIGNMENT = MINING_TASK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Settings Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__SETTINGS_ASSIGNMENT = MINING_TASK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Validation Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK__VALIDATION_DATA = MINING_TASK_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Mining Build Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_BUILD_TASK_FEATURE_COUNT = MINING_TASK_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTransformationImpl <em>Mining Transformation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTransformationImpl
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningTransformation()
	 * @generated
	 */
	int MINING_TRANSFORMATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__NAME = TransformationPackage.TRANSFORMATION__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__VISIBILITY = TransformationPackage.TRANSFORMATION__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__CLIENT_DEPENDENCY = TransformationPackage.TRANSFORMATION__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__SUPPLIER_DEPENDENCY = TransformationPackage.TRANSFORMATION__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__CONSTRAINT = TransformationPackage.TRANSFORMATION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__NAMESPACE = TransformationPackage.TRANSFORMATION__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__IMPORTER = TransformationPackage.TRANSFORMATION__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__STEREOTYPE = TransformationPackage.TRANSFORMATION__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__TAGGED_VALUE = TransformationPackage.TRANSFORMATION__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__DOCUMENT = TransformationPackage.TRANSFORMATION__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__DESCRIPTION = TransformationPackage.TRANSFORMATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__RESPONSIBLE_PARTY = TransformationPackage.TRANSFORMATION__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__ELEMENT_NODE = TransformationPackage.TRANSFORMATION__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__SET = TransformationPackage.TRANSFORMATION__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__RENDERED_OBJECT = TransformationPackage.TRANSFORMATION__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__VOCABULARY_ELEMENT = TransformationPackage.TRANSFORMATION__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__MEASUREMENT = TransformationPackage.TRANSFORMATION__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__CHANGE_REQUEST = TransformationPackage.TRANSFORMATION__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__OWNED_ELEMENT = TransformationPackage.TRANSFORMATION__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__FUNCTION = TransformationPackage.TRANSFORMATION__FUNCTION;

	/**
	 * The feature id for the '<em><b>Function Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__FUNCTION_DESCRIPTION = TransformationPackage.TRANSFORMATION__FUNCTION_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Is Primary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__IS_PRIMARY = TransformationPackage.TRANSFORMATION__IS_PRIMARY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__SOURCE = TransformationPackage.TRANSFORMATION__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__TARGET = TransformationPackage.TRANSFORMATION__TARGET;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__TASK = TransformationPackage.TRANSFORMATION__TASK;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION__PROCEDURE = TransformationPackage.TRANSFORMATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mining Transformation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINING_TRANSFORMATION_FEATURE_COUNT = TransformationPackage.TRANSFORMATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption <em>Apply Output Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyOutputOption()
	 * @generated
	 */
	int APPLY_OUTPUT_OPTION = 11;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem <em>Apply Content Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Content Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem
	 * @generated
	 */
	EClass getApplyContentItem();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem#getTopNthIndex <em>Top Nth Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Top Nth Index</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyContentItem#getTopNthIndex()
	 * @see #getApplyContentItem()
	 * @generated
	 */
	EAttribute getApplyContentItem_TopNthIndex();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem <em>Apply Output Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Output Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem
	 * @generated
	 */
	EClass getApplyOutputItem();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput <em>Apply Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Apply Output</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem#getApplyOutput()
	 * @see #getApplyOutputItem()
	 * @generated
	 */
	EReference getApplyOutputItem_ApplyOutput();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyProbabilityItem <em>Apply Probability Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Probability Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyProbabilityItem
	 * @generated
	 */
	EClass getApplyProbabilityItem();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyRuleIdItem <em>Apply Rule Id Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Rule Id Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyRuleIdItem
	 * @generated
	 */
	EClass getApplyRuleIdItem();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyScoreItem <em>Apply Score Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Score Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyScoreItem
	 * @generated
	 */
	EClass getApplyScoreItem();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplySourceItem <em>Apply Source Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Source Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplySourceItem
	 * @generated
	 */
	EClass getApplySourceItem();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput <em>Mining Apply Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Apply Output</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput
	 * @generated
	 */
	EClass getMiningApplyOutput();

	/**
	 * Returns the meta object for the containment reference list '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Item</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput#getItem()
	 * @see #getMiningApplyOutput()
	 * @generated
	 */
	EReference getMiningApplyOutput_Item();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask <em>Mining Apply Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Apply Task</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask
	 * @generated
	 */
	EClass getMiningApplyTask();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputOption <em>Output Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Option</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputOption()
	 * @see #getMiningApplyTask()
	 * @generated
	 */
	EAttribute getMiningApplyTask_OutputOption();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputAssignment <em>Output Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getOutputAssignment()
	 * @see #getMiningApplyTask()
	 * @generated
	 */
	EReference getMiningApplyTask_OutputAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getApplyOutput <em>Apply Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Apply Output</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask#getApplyOutput()
	 * @see #getMiningApplyTask()
	 * @generated
	 */
	EReference getMiningApplyTask_ApplyOutput();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask <em>Mining Build Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Build Task</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask
	 * @generated
	 */
	EClass getMiningBuildTask();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getResultModel <em>Result Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Result Model</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getResultModel()
	 * @see #getMiningBuildTask()
	 * @generated
	 */
	EReference getMiningBuildTask_ResultModel();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getMiningSettings <em>Mining Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mining Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getMiningSettings()
	 * @see #getMiningBuildTask()
	 * @generated
	 */
	EReference getMiningBuildTask_MiningSettings();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationAssignment <em>Validation Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Validation Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationAssignment()
	 * @see #getMiningBuildTask()
	 * @generated
	 */
	EReference getMiningBuildTask_ValidationAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getSettingsAssignment <em>Settings Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Settings Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getSettingsAssignment()
	 * @see #getMiningBuildTask()
	 * @generated
	 */
	EReference getMiningBuildTask_SettingsAssignment();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationData <em>Validation Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Validation Data</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningBuildTask#getValidationData()
	 * @see #getMiningBuildTask()
	 * @generated
	 */
	EReference getMiningBuildTask_ValidationData();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask <em>Mining Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Task</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask
	 * @generated
	 */
	EClass getMiningTask();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputModel <em>Input Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Model</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputModel()
	 * @see #getMiningTask()
	 * @generated
	 */
	EReference getMiningTask_InputModel();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputData <em>Input Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Data</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getInputData()
	 * @see #getMiningTask()
	 * @generated
	 */
	EReference getMiningTask_InputData();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getModelAssignment <em>Model Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model Assignment</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getModelAssignment()
	 * @see #getMiningTask()
	 * @generated
	 */
	EReference getMiningTask_ModelAssignment();

	/**
	 * Returns the meta object for the container reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Schema</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema()
	 * @see #getMiningTask()
	 * @generated
	 */
	EReference getMiningTask_Schema();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTransformation <em>Mining Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mining Transformation</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTransformation
	 * @generated
	 */
	EClass getMiningTransformation();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTransformation#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Procedure</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTransformation#getProcedure()
	 * @see #getMiningTransformation()
	 * @generated
	 */
	EReference getMiningTransformation_Procedure();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption <em>Apply Output Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Apply Output Option</em>'.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption
	 * @generated
	 */
	EEnum getApplyOutputOption();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MiningtaskFactory getMiningtaskFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyContentItemImpl <em>Apply Content Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyContentItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyContentItem()
		 * @generated
		 */
		EClass APPLY_CONTENT_ITEM = eINSTANCE.getApplyContentItem();

		/**
		 * The meta object literal for the '<em><b>Top Nth Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLY_CONTENT_ITEM__TOP_NTH_INDEX = eINSTANCE.getApplyContentItem_TopNthIndex();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyOutputItemImpl <em>Apply Output Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyOutputItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyOutputItem()
		 * @generated
		 */
		EClass APPLY_OUTPUT_ITEM = eINSTANCE.getApplyOutputItem();

		/**
		 * The meta object literal for the '<em><b>Apply Output</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLY_OUTPUT_ITEM__APPLY_OUTPUT = eINSTANCE.getApplyOutputItem_ApplyOutput();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyProbabilityItemImpl <em>Apply Probability Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyProbabilityItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyProbabilityItem()
		 * @generated
		 */
		EClass APPLY_PROBABILITY_ITEM = eINSTANCE.getApplyProbabilityItem();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyRuleIdItemImpl <em>Apply Rule Id Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyRuleIdItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyRuleIdItem()
		 * @generated
		 */
		EClass APPLY_RULE_ID_ITEM = eINSTANCE.getApplyRuleIdItem();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyScoreItemImpl <em>Apply Score Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyScoreItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyScoreItem()
		 * @generated
		 */
		EClass APPLY_SCORE_ITEM = eINSTANCE.getApplyScoreItem();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplySourceItemImpl <em>Apply Source Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplySourceItemImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplySourceItem()
		 * @generated
		 */
		EClass APPLY_SOURCE_ITEM = eINSTANCE.getApplySourceItem();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyOutputImpl <em>Mining Apply Output</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyOutputImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningApplyOutput()
		 * @generated
		 */
		EClass MINING_APPLY_OUTPUT = eINSTANCE.getMiningApplyOutput();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_APPLY_OUTPUT__ITEM = eINSTANCE.getMiningApplyOutput_Item();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl <em>Mining Apply Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningApplyTask()
		 * @generated
		 */
		EClass MINING_APPLY_TASK = eINSTANCE.getMiningApplyTask();

		/**
		 * The meta object literal for the '<em><b>Output Option</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MINING_APPLY_TASK__OUTPUT_OPTION = eINSTANCE.getMiningApplyTask_OutputOption();

		/**
		 * The meta object literal for the '<em><b>Output Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_APPLY_TASK__OUTPUT_ASSIGNMENT = eINSTANCE.getMiningApplyTask_OutputAssignment();

		/**
		 * The meta object literal for the '<em><b>Apply Output</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_APPLY_TASK__APPLY_OUTPUT = eINSTANCE.getMiningApplyTask_ApplyOutput();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningBuildTaskImpl <em>Mining Build Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningBuildTaskImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningBuildTask()
		 * @generated
		 */
		EClass MINING_BUILD_TASK = eINSTANCE.getMiningBuildTask();

		/**
		 * The meta object literal for the '<em><b>Result Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_BUILD_TASK__RESULT_MODEL = eINSTANCE.getMiningBuildTask_ResultModel();

		/**
		 * The meta object literal for the '<em><b>Mining Settings</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_BUILD_TASK__MINING_SETTINGS = eINSTANCE.getMiningBuildTask_MiningSettings();

		/**
		 * The meta object literal for the '<em><b>Validation Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_BUILD_TASK__VALIDATION_ASSIGNMENT = eINSTANCE.getMiningBuildTask_ValidationAssignment();

		/**
		 * The meta object literal for the '<em><b>Settings Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_BUILD_TASK__SETTINGS_ASSIGNMENT = eINSTANCE.getMiningBuildTask_SettingsAssignment();

		/**
		 * The meta object literal for the '<em><b>Validation Data</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_BUILD_TASK__VALIDATION_DATA = eINSTANCE.getMiningBuildTask_ValidationData();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl <em>Mining Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTaskImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningTask()
		 * @generated
		 */
		EClass MINING_TASK = eINSTANCE.getMiningTask();

		/**
		 * The meta object literal for the '<em><b>Input Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_TASK__INPUT_MODEL = eINSTANCE.getMiningTask_InputModel();

		/**
		 * The meta object literal for the '<em><b>Input Data</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_TASK__INPUT_DATA = eINSTANCE.getMiningTask_InputData();

		/**
		 * The meta object literal for the '<em><b>Model Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_TASK__MODEL_ASSIGNMENT = eINSTANCE.getMiningTask_ModelAssignment();

		/**
		 * The meta object literal for the '<em><b>Schema</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_TASK__SCHEMA = eINSTANCE.getMiningTask_Schema();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTransformationImpl <em>Mining Transformation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningTransformationImpl
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getMiningTransformation()
		 * @generated
		 */
		EClass MINING_TRANSFORMATION = eINSTANCE.getMiningTransformation();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINING_TRANSFORMATION__PROCEDURE = eINSTANCE.getMiningTransformation_Procedure();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption <em>Apply Output Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption
		 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl#getApplyOutputOption()
		 * @generated
		 */
		EEnum APPLY_OUTPUT_OPTION = eINSTANCE.getApplyOutputOption();

	}

} //MiningtaskPackage
