/**
 * INRIA/IRISA
 *
 * $Id: ApproximationFunctionSettings.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An ApproximationFunctionSettings is a subclass of SupervisedFunctionSettings that supports features that are unique to approximate numerical values. 
 * 
 * This originally was named "RegressionFunctionSettings", however, this implies a restriction on the type of algorithms that can be applied to solve the problem of approximation, or curve fitting.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.approximation.ApproximationFunctionSettings#getToleratedError <em>Tolerated Error</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationFunctionSettings()
 * @model
 * @generated
 */
public interface ApproximationFunctionSettings extends SupervisedFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Tolerated Error</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The toleratedError is defined in terms of R-squared
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Tolerated Error</em>' attribute.
	 * @see #setToleratedError(String)
	 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage#getApproximationFunctionSettings_ToleratedError()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getToleratedError();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.approximation.ApproximationFunctionSettings#getToleratedError <em>Tolerated Error</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tolerated Error</em>' attribute.
	 * @see #getToleratedError()
	 * @generated
	 */
	void setToleratedError(String value);

} // ApproximationFunctionSettings
