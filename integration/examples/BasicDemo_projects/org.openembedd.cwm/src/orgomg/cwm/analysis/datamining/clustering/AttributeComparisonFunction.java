/**
 * INRIA/IRISA
 *
 * $Id: AttributeComparisonFunction.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Attribute Comparison Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * When two records are compared, then either the distance or similarity is of interest. In both cases the measures can be compouted by a combination of an "inner" function and an "outer" function. The inner function compares two single field values and the outer function computes an aggregation over all fields.
 * Each field has a comparison function, this is either defined as a default in ClusteringModel or it can be defined per attribute.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getAttributeComparisonFunction()
 * @model
 * @generated
 */
public enum AttributeComparisonFunction implements Enumerator {
	/**
	 * The '<em><b>Abs Diff</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABS_DIFF
	 * @generated
	 * @ordered
	 */
	ABS_DIFF_LITERAL(0, "absDiff", "absDiff"),

	/**
	 * The '<em><b>Gauss Sim</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GAUSS_SIM
	 * @generated
	 * @ordered
	 */
	GAUSS_SIM_LITERAL(1, "gaussSim", "gaussSim"),

	/**
	 * The '<em><b>Delta</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELTA
	 * @generated
	 * @ordered
	 */
	DELTA_LITERAL(2, "delta", "delta"),

	/**
	 * The '<em><b>Equal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUAL
	 * @generated
	 * @ordered
	 */
	EQUAL_LITERAL(3, "equal", "equal"),

	/**
	 * The '<em><b>Table</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TABLE
	 * @generated
	 * @ordered
	 */
	TABLE_LITERAL(4, "table", "table");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Abs Diff</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Absolute difference: c(x,y) = |x-y|
	 * <!-- end-model-doc -->
	 * @see #ABS_DIFF_LITERAL
	 * @model name="absDiff"
	 * @generated
	 * @ordered
	 */
	public static final int ABS_DIFF = 0;

	/**
	 * The '<em><b>Gauss Sim</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Gaussian similarity
	 * c(x,y) = exp(-ln(2)*z*z/(s*s))
	 * where z=x-y, and s is the value of attribute similarityValue (required in this case) in the attribute.
	 * <!-- end-model-doc -->
	 * @see #GAUSS_SIM_LITERAL
	 * @model name="gaussSim"
	 * @generated
	 * @ordered
	 */
	public static final int GAUSS_SIM = 1;

	/**
	 * The '<em><b>Delta</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * c(x,y) = 0 if x=y, 1 else
	 * <!-- end-model-doc -->
	 * @see #DELTA_LITERAL
	 * @model name="delta"
	 * @generated
	 * @ordered
	 */
	public static final int DELTA = 2;

	/**
	 * The '<em><b>Equal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * c(x,y) = 1 if x=y, 0 else
	 * <!-- end-model-doc -->
	 * @see #EQUAL_LITERAL
	 * @model name="equal"
	 * @generated
	 * @ordered
	 */
	public static final int EQUAL = 3;

	/**
	 * The '<em><b>Table</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * c(x,y) = look up in similarity matrix
	 * <!-- end-model-doc -->
	 * @see #TABLE_LITERAL
	 * @model name="table"
	 * @generated
	 * @ordered
	 */
	public static final int TABLE = 4;

	/**
	 * An array of all the '<em><b>Attribute Comparison Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AttributeComparisonFunction[] VALUES_ARRAY =
		new AttributeComparisonFunction[] {
			ABS_DIFF_LITERAL,
			GAUSS_SIM_LITERAL,
			DELTA_LITERAL,
			EQUAL_LITERAL,
			TABLE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Attribute Comparison Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AttributeComparisonFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Attribute Comparison Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeComparisonFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeComparisonFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Comparison Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeComparisonFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeComparisonFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Comparison Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeComparisonFunction get(int value) {
		switch (value) {
			case ABS_DIFF: return ABS_DIFF_LITERAL;
			case GAUSS_SIM: return GAUSS_SIM_LITERAL;
			case DELTA: return DELTA_LITERAL;
			case EQUAL: return EQUAL_LITERAL;
			case TABLE: return TABLE_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AttributeComparisonFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AttributeComparisonFunction
