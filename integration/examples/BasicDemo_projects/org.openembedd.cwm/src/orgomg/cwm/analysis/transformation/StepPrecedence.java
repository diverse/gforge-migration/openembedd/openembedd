/**
 * INRIA/IRISA
 *
 * $Id: StepPrecedence.java,v 1.1 2008-04-01 09:35:51 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation;

import orgomg.cwm.objectmodel.core.Dependency;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step Precedence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is used to define explicit order-of-execution relationships among TransformationSteps. It may be used independent of or in conjunction with PrecedenceConstraint
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.transformation.TransformationPackage#getStepPrecedence()
 * @model
 * @generated
 */
public interface StepPrecedence extends Dependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // StepPrecedence
