/**
 * INRIA/IRISA
 *
 * $Id: EntrypointFactoryImpl.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EntrypointFactoryImpl extends EFactoryImpl implements EntrypointFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EntrypointFactory init() {
		try {
			EntrypointFactory theEntrypointFactory = (EntrypointFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/miningcore/entrypoint.ecore"); 
			if (theEntrypointFactory != null) {
				return theEntrypointFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EntrypointFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntrypointFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EntrypointPackage.AUXILIARY_OBJECT: return createAuxiliaryObject();
			case EntrypointPackage.CATALOG: return createCatalog();
			case EntrypointPackage.SCHEMA: return createSchema();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuxiliaryObject createAuxiliaryObject() {
		AuxiliaryObjectImpl auxiliaryObject = new AuxiliaryObjectImpl();
		return auxiliaryObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Catalog createCatalog() {
		CatalogImpl catalog = new CatalogImpl();
		return catalog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema createSchema() {
		SchemaImpl schema = new SchemaImpl();
		return schema;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntrypointPackage getEntrypointPackage() {
		return (EntrypointPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EntrypointPackage getPackage() {
		return EntrypointPackage.eINSTANCE;
	}

} //EntrypointFactoryImpl
