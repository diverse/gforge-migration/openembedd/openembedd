/**
 * <copyright>
 * </copyright>
 *
 * $Id: PriorProbabilitiesValidator.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage;
import orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface PriorProbabilitiesValidator {
	boolean validate();

	boolean validateUsage(ClassificationAttributeUsage value);
	boolean validatePrior(EList<PriorProbabilitiesEntry> value);
}
