/**
 * INRIA/IRISA
 *
 * $Id: NumericalAttributePropertiesImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Numerical Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getUpperBound <em>Upper Bound</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#isIsDiscrete <em>Is Discrete</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#isIsCyclic <em>Is Cyclic</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getAnchor <em>Anchor</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getCycleBegin <em>Cycle Begin</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getCycleEnd <em>Cycle End</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getDiscreteStepSize <em>Discrete Step Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.NumericalAttributePropertiesImpl#getLogicalAttribute <em>Logical Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NumericalAttributePropertiesImpl extends ModelElementImpl implements NumericalAttributeProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getLowerBound() <em>Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerBound()
	 * @generated
	 * @ordered
	 */
	protected static final String LOWER_BOUND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLowerBound() <em>Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerBound()
	 * @generated
	 * @ordered
	 */
	protected String lowerBound = LOWER_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperBound() <em>Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperBound()
	 * @generated
	 * @ordered
	 */
	protected static final String UPPER_BOUND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpperBound() <em>Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperBound()
	 * @generated
	 * @ordered
	 */
	protected String upperBound = UPPER_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsDiscrete() <em>Is Discrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsDiscrete()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_DISCRETE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsDiscrete() <em>Is Discrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsDiscrete()
	 * @generated
	 * @ordered
	 */
	protected boolean isDiscrete = IS_DISCRETE_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsCyclic() <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCyclic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CYCLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsCyclic() <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCyclic()
	 * @generated
	 * @ordered
	 */
	protected boolean isCyclic = IS_CYCLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getAnchor() <em>Anchor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnchor()
	 * @generated
	 * @ordered
	 */
	protected static final String ANCHOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAnchor() <em>Anchor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnchor()
	 * @generated
	 * @ordered
	 */
	protected String anchor = ANCHOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getCycleBegin() <em>Cycle Begin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycleBegin()
	 * @generated
	 * @ordered
	 */
	protected static final String CYCLE_BEGIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCycleBegin() <em>Cycle Begin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycleBegin()
	 * @generated
	 * @ordered
	 */
	protected String cycleBegin = CYCLE_BEGIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getCycleEnd() <em>Cycle End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycleEnd()
	 * @generated
	 * @ordered
	 */
	protected static final String CYCLE_END_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCycleEnd() <em>Cycle End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycleEnd()
	 * @generated
	 * @ordered
	 */
	protected String cycleEnd = CYCLE_END_EDEFAULT;

	/**
	 * The default value of the '{@link #getDiscreteStepSize() <em>Discrete Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteStepSize()
	 * @generated
	 * @ordered
	 */
	protected static final String DISCRETE_STEP_SIZE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDiscreteStepSize() <em>Discrete Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteStepSize()
	 * @generated
	 * @ordered
	 */
	protected String discreteStepSize = DISCRETE_STEP_SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NumericalAttributePropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.NUMERICAL_ATTRIBUTE_PROPERTIES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLowerBound() {
		return lowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerBound(String newLowerBound) {
		String oldLowerBound = lowerBound;
		lowerBound = newLowerBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND, oldLowerBound, lowerBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUpperBound() {
		return upperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperBound(String newUpperBound) {
		String oldUpperBound = upperBound;
		upperBound = newUpperBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND, oldUpperBound, upperBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsDiscrete() {
		return isDiscrete;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsDiscrete(boolean newIsDiscrete) {
		boolean oldIsDiscrete = isDiscrete;
		isDiscrete = newIsDiscrete;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE, oldIsDiscrete, isDiscrete));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsCyclic() {
		return isCyclic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsCyclic(boolean newIsCyclic) {
		boolean oldIsCyclic = isCyclic;
		isCyclic = newIsCyclic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC, oldIsCyclic, isCyclic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAnchor() {
		return anchor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnchor(String newAnchor) {
		String oldAnchor = anchor;
		anchor = newAnchor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR, oldAnchor, anchor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCycleBegin() {
		return cycleBegin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCycleBegin(String newCycleBegin) {
		String oldCycleBegin = cycleBegin;
		cycleBegin = newCycleBegin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN, oldCycleBegin, cycleBegin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCycleEnd() {
		return cycleEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCycleEnd(String newCycleEnd) {
		String oldCycleEnd = cycleEnd;
		cycleEnd = newCycleEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END, oldCycleEnd, cycleEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDiscreteStepSize() {
		return discreteStepSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteStepSize(String newDiscreteStepSize) {
		String oldDiscreteStepSize = discreteStepSize;
		discreteStepSize = newDiscreteStepSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE, oldDiscreteStepSize, discreteStepSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalAttribute getLogicalAttribute() {
		if (eContainerFeatureID != MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE) return null;
		return (LogicalAttribute)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLogicalAttribute(LogicalAttribute newLogicalAttribute, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLogicalAttribute, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalAttribute(LogicalAttribute newLogicalAttribute) {
		if (newLogicalAttribute != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE && newLogicalAttribute != null)) {
			if (EcoreUtil.isAncestor(this, newLogicalAttribute))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLogicalAttribute != null)
				msgs = ((InternalEObject)newLogicalAttribute).eInverseAdd(this, MiningdataPackage.LOGICAL_ATTRIBUTE__NUMERICAL_PROPERTIES, LogicalAttribute.class, msgs);
			msgs = basicSetLogicalAttribute(newLogicalAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE, newLogicalAttribute, newLogicalAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLogicalAttribute((LogicalAttribute)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return basicSetLogicalAttribute(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.LOGICAL_ATTRIBUTE__NUMERICAL_PROPERTIES, LogicalAttribute.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND:
				return getLowerBound();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND:
				return getUpperBound();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE:
				return isIsDiscrete() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				return isIsCyclic() ? Boolean.TRUE : Boolean.FALSE;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR:
				return getAnchor();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN:
				return getCycleBegin();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END:
				return getCycleEnd();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE:
				return getDiscreteStepSize();
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return getLogicalAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND:
				setLowerBound((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND:
				setUpperBound((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE:
				setIsDiscrete(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				setIsCyclic(((Boolean)newValue).booleanValue());
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR:
				setAnchor((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN:
				setCycleBegin((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END:
				setCycleEnd((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE:
				setDiscreteStepSize((String)newValue);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				setLogicalAttribute((LogicalAttribute)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND:
				setLowerBound(LOWER_BOUND_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND:
				setUpperBound(UPPER_BOUND_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE:
				setIsDiscrete(IS_DISCRETE_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				setIsCyclic(IS_CYCLIC_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR:
				setAnchor(ANCHOR_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN:
				setCycleBegin(CYCLE_BEGIN_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END:
				setCycleEnd(CYCLE_END_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE:
				setDiscreteStepSize(DISCRETE_STEP_SIZE_EDEFAULT);
				return;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				setLogicalAttribute((LogicalAttribute)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOWER_BOUND:
				return LOWER_BOUND_EDEFAULT == null ? lowerBound != null : !LOWER_BOUND_EDEFAULT.equals(lowerBound);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__UPPER_BOUND:
				return UPPER_BOUND_EDEFAULT == null ? upperBound != null : !UPPER_BOUND_EDEFAULT.equals(upperBound);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_DISCRETE:
				return isDiscrete != IS_DISCRETE_EDEFAULT;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__IS_CYCLIC:
				return isCyclic != IS_CYCLIC_EDEFAULT;
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__ANCHOR:
				return ANCHOR_EDEFAULT == null ? anchor != null : !ANCHOR_EDEFAULT.equals(anchor);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_BEGIN:
				return CYCLE_BEGIN_EDEFAULT == null ? cycleBegin != null : !CYCLE_BEGIN_EDEFAULT.equals(cycleBegin);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__CYCLE_END:
				return CYCLE_END_EDEFAULT == null ? cycleEnd != null : !CYCLE_END_EDEFAULT.equals(cycleEnd);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__DISCRETE_STEP_SIZE:
				return DISCRETE_STEP_SIZE_EDEFAULT == null ? discreteStepSize != null : !DISCRETE_STEP_SIZE_EDEFAULT.equals(discreteStepSize);
			case MiningdataPackage.NUMERICAL_ATTRIBUTE_PROPERTIES__LOGICAL_ATTRIBUTE:
				return getLogicalAttribute() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lowerBound: ");
		result.append(lowerBound);
		result.append(", upperBound: ");
		result.append(upperBound);
		result.append(", isDiscrete: ");
		result.append(isDiscrete);
		result.append(", isCyclic: ");
		result.append(isCyclic);
		result.append(", anchor: ");
		result.append(anchor);
		result.append(", cycleBegin: ");
		result.append(cycleBegin);
		result.append(", cycleEnd: ");
		result.append(cycleEnd);
		result.append(", discreteStepSize: ");
		result.append(discreteStepSize);
		result.append(')');
		return result.toString();
	}

} //NumericalAttributePropertiesImpl
