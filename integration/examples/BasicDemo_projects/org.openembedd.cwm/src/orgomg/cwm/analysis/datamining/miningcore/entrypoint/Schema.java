/**
 * INRIA/IRISA
 *
 * $Id: Schema.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask;

import orgomg.cwm.objectmodel.core.Namespace;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Container for all data mining top level objects.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getLogicalData <em>Logical Data</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCategoryMatrix <em>Category Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getAuxObjects <em>Aux Objects</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getTaxonomy <em>Taxonomy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningFunctionSettings <em>Mining Function Settings</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningModel <em>Mining Model</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getMiningTask <em>Mining Task</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getResult <em>Result</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema()
 * @model
 * @generated
 */
public interface Schema extends Namespace {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Catalog</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Catalog#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Catalog</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catalog</em>' container reference.
	 * @see #setCatalog(Catalog)
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_Catalog()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Catalog#getSchema
	 * @model opposite="schema" required="true"
	 * @generated
	 */
	Catalog getCatalog();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCatalog <em>Catalog</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catalog</em>' container reference.
	 * @see #getCatalog()
	 * @generated
	 */
	void setCatalog(Catalog value);

	/**
	 * Returns the value of the '<em><b>Logical Data</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Data</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_LogicalData()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalData#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<LogicalData> getLogicalData();

	/**
	 * Returns the value of the '<em><b>Category Matrix</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category Matrix</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category Matrix</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_CategoryMatrix()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<CategoryMatrix> getCategoryMatrix();

	/**
	 * Returns the value of the '<em><b>Aux Objects</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aux Objects</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aux Objects</em>' containment reference.
	 * @see #setAuxObjects(AuxiliaryObject)
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_AuxObjects()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	AuxiliaryObject getAuxObjects();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getAuxObjects <em>Aux Objects</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aux Objects</em>' containment reference.
	 * @see #getAuxObjects()
	 * @generated
	 */
	void setAuxObjects(AuxiliaryObject value);

	/**
	 * Returns the value of the '<em><b>Taxonomy</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Taxonomy</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Taxonomy</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_Taxonomy()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<CategoryTaxonomy> getTaxonomy();

	/**
	 * Returns the value of the '<em><b>Mining Function Settings</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mining Function Settings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mining Function Settings</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_MiningFunctionSettings()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<MiningFunctionSettings> getMiningFunctionSettings();

	/**
	 * Returns the value of the '<em><b>Mining Model</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mining Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mining Model</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_MiningModel()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningModel#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<MiningModel> getMiningModel();

	/**
	 * Returns the value of the '<em><b>Mining Task</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mining Task</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mining Task</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_MiningTask()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningTask#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<MiningTask> getMiningTask();

	/**
	 * Returns the value of the '<em><b>Result</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult#getSchema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getSchema_Result()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningResult#getSchema
	 * @model opposite="schema" containment="true"
	 * @generated
	 */
	EList<MiningResult> getResult();

} // Schema
