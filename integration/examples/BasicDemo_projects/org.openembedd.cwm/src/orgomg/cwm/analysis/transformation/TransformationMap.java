/**
 * INRIA/IRISA
 *
 * $Id: TransformationMap.java,v 1.1 2008-04-01 09:35:51 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a specialized Transformation which consists of a group of ClassifierMaps.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.transformation.TransformationPackage#getTransformationMap()
 * @model
 * @generated
 */
public interface TransformationMap extends Transformation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // TransformationMap
