/**
 * INRIA/IRISA
 *
 * $Id: XSLRendering.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.analysis.informationvisualization;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XSL Rendering</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * XSLRendering represents a useful subclass of Rendering based on XSL (i.e., this subclass' formula might contain a procedure that uses XSL to create an HTML document).
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage#getXSLRendering()
 * @model
 * @generated
 */
public interface XSLRendering extends Rendering {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // XSLRendering
