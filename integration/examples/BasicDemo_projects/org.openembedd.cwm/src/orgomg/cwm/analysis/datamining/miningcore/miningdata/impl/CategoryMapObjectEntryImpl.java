/**
 * INRIA/IRISA
 *
 * $Id: CategoryMapObjectEntryImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Map Object Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl#getGraphId <em>Graph Id</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl#getMapObject <em>Map Object</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl#getChild <em>Child</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapObjectEntryImpl#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMapObjectEntryImpl extends ModelElementImpl implements CategoryMapObjectEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getGraphId() <em>Graph Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphId()
	 * @generated
	 * @ordered
	 */
	protected static final String GRAPH_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGraphId() <em>Graph Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphId()
	 * @generated
	 * @ordered
	 */
	protected String graphId = GRAPH_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChild() <em>Child</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChild()
	 * @generated
	 * @ordered
	 */
	protected Category child;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> parent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMapObjectEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MAP_OBJECT_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGraphId() {
		return graphId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphId(String newGraphId) {
		String oldGraphId = graphId;
		graphId = newGraphId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID, oldGraphId, graphId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMapObject getMapObject() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT) return null;
		return (CategoryMapObject)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMapObject(CategoryMapObject newMapObject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMapObject, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMapObject(CategoryMapObject newMapObject) {
		if (newMapObject != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT && newMapObject != null)) {
			if (EcoreUtil.isAncestor(this, newMapObject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMapObject != null)
				msgs = ((InternalEObject)newMapObject).eInverseAdd(this, MiningdataPackage.CATEGORY_MAP_OBJECT__ENTRY, CategoryMapObject.class, msgs);
			msgs = basicSetMapObject(newMapObject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT, newMapObject, newMapObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getChild() {
		if (child != null && child.eIsProxy()) {
			InternalEObject oldChild = (InternalEObject)child;
			child = (Category)eResolveProxy(oldChild);
			if (child != oldChild) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD, oldChild, child));
			}
		}
		return child;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category basicGetChild() {
		return child;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChild(Category newChild) {
		Category oldChild = child;
		child = newChild;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD, oldChild, child));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getParent() {
		if (parent == null) {
			parent = new EObjectResolvingEList<Category>(Category.class, this, MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__PARENT);
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMapObject((CategoryMapObject)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				return basicSetMapObject(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.CATEGORY_MAP_OBJECT__ENTRY, CategoryMapObject.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID:
				return getGraphId();
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				return getMapObject();
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD:
				if (resolve) return getChild();
				return basicGetChild();
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__PARENT:
				return getParent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID:
				setGraphId((String)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				setMapObject((CategoryMapObject)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD:
				setChild((Category)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__PARENT:
				getParent().clear();
				getParent().addAll((Collection<? extends Category>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID:
				setGraphId(GRAPH_ID_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				setMapObject((CategoryMapObject)null);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD:
				setChild((Category)null);
				return;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__PARENT:
				getParent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__GRAPH_ID:
				return GRAPH_ID_EDEFAULT == null ? graphId != null : !GRAPH_ID_EDEFAULT.equals(graphId);
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__MAP_OBJECT:
				return getMapObject() != null;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__CHILD:
				return child != null;
			case MiningdataPackage.CATEGORY_MAP_OBJECT_ENTRY__PARENT:
				return parent != null && !parent.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (graphId: ");
		result.append(graphId);
		result.append(')');
		return result.toString();
	}

} //CategoryMapObjectEntryImpl
