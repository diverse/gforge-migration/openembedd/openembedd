/**
 * INRIA/IRISA
 *
 * $Id: AttributeimportancePackage.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.attributeimportance;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains the metamodel that represents the constructs for attribute importance model.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportanceFactory
 * @model kind="package"
 * @generated
 */
public interface AttributeimportancePackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "attributeimportance";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/attributeimportance.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.attributeimportance";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AttributeimportancePackage eINSTANCE = orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl <em>Attribute Importance Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl#getAttributeImportanceSettings()
	 * @generated
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__NAME = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__VISIBILITY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__CLIENT_DEPENDENCY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__SUPPLIER_DEPENDENCY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__CONSTRAINT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__NAMESPACE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__IMPORTER = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__STEREOTYPE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__TAGGED_VALUE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__DOCUMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__DESCRIPTION = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__RESPONSIBLE_PARTY = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__ELEMENT_NODE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__SET = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__RENDERED_OBJECT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__VOCABULARY_ELEMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__MEASUREMENT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__CHANGE_REQUEST = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__ALGORITHM_SETTINGS = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__ATTRIBUTE_USAGE_SET = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__LOGICAL_DATA = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__SCHEMA = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Maximum Result Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Return Top</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Importance Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_IMPORTANCE_SETTINGS_FEATURE_COUNT = SupervisedPackage.SUPERVISED_FUNCTION_SETTINGS_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings <em>Attribute Importance Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Importance Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings
	 * @generated
	 */
	EClass getAttributeImportanceSettings();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#getMaximumResultSize <em>Maximum Result Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Result Size</em>'.
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#getMaximumResultSize()
	 * @see #getAttributeImportanceSettings()
	 * @generated
	 */
	EAttribute getAttributeImportanceSettings_MaximumResultSize();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#isReturnTop <em>Return Top</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Top</em>'.
	 * @see orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings#isReturnTop()
	 * @see #getAttributeImportanceSettings()
	 * @generated
	 */
	EAttribute getAttributeImportanceSettings_ReturnTop();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AttributeimportanceFactory getAttributeimportanceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl <em>Attribute Importance Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl#getAttributeImportanceSettings()
		 * @generated
		 */
		EClass ATTRIBUTE_IMPORTANCE_SETTINGS = eINSTANCE.getAttributeImportanceSettings();

		/**
		 * The meta object literal for the '<em><b>Maximum Result Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE = eINSTANCE.getAttributeImportanceSettings_MaximumResultSize();

		/**
		 * The meta object literal for the '<em><b>Return Top</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP = eINSTANCE.getAttributeImportanceSettings_ReturnTop();

	}

} //AttributeimportancePackage
