/**
 * INRIA/IRISA
 *
 * $Id: AttributeImportanceSettingsImpl.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.attributeimportance.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeImportanceSettings;
import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedFunctionSettingsImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Importance Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl#getMaximumResultSize <em>Maximum Result Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeImportanceSettingsImpl#isReturnTop <em>Return Top</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeImportanceSettingsImpl extends SupervisedFunctionSettingsImpl implements AttributeImportanceSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getMaximumResultSize() <em>Maximum Result Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumResultSize()
	 * @generated
	 * @ordered
	 */
	protected static final long MAXIMUM_RESULT_SIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaximumResultSize() <em>Maximum Result Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumResultSize()
	 * @generated
	 * @ordered
	 */
	protected long maximumResultSize = MAXIMUM_RESULT_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #isReturnTop() <em>Return Top</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReturnTop()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RETURN_TOP_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isReturnTop() <em>Return Top</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReturnTop()
	 * @generated
	 * @ordered
	 */
	protected boolean returnTop = RETURN_TOP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeImportanceSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AttributeimportancePackage.Literals.ATTRIBUTE_IMPORTANCE_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaximumResultSize() {
		return maximumResultSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumResultSize(long newMaximumResultSize) {
		long oldMaximumResultSize = maximumResultSize;
		maximumResultSize = newMaximumResultSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE, oldMaximumResultSize, maximumResultSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isReturnTop() {
		return returnTop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnTop(boolean newReturnTop) {
		boolean oldReturnTop = returnTop;
		returnTop = newReturnTop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP, oldReturnTop, returnTop));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE:
				return new Long(getMaximumResultSize());
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP:
				return isReturnTop() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE:
				setMaximumResultSize(((Long)newValue).longValue());
				return;
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP:
				setReturnTop(((Boolean)newValue).booleanValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE:
				setMaximumResultSize(MAXIMUM_RESULT_SIZE_EDEFAULT);
				return;
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP:
				setReturnTop(RETURN_TOP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__MAXIMUM_RESULT_SIZE:
				return maximumResultSize != MAXIMUM_RESULT_SIZE_EDEFAULT;
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS__RETURN_TOP:
				return returnTop != RETURN_TOP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (maximumResultSize: ");
		result.append(maximumResultSize);
		result.append(", returnTop: ");
		result.append(returnTop);
		result.append(')');
		return result.toString();
	}

} //AttributeImportanceSettingsImpl
