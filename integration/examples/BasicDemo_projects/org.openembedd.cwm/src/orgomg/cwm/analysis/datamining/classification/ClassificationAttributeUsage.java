/**
 * INRIA/IRISA
 *
 * $Id: ClassificationAttributeUsage.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeUsage;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Usage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * As a subclass of AttributeUsage, CategoricalAttributeUsage provides additional specification for categorical attributes only, in particular, a set of the positive categories. 
 * 
 * Positive categories are provided here since different models may choose different postive categories, hence it is not a characteristic of the data.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPositiveCategory <em>Positive Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors <em>Priors</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationAttributeUsage()
 * @model
 * @generated
 */
public interface ClassificationAttributeUsage extends AttributeUsage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Positive Category</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positive Category</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positive Category</em>' reference list.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationAttributeUsage_PositiveCategory()
	 * @model required="true"
	 * @generated
	 */
	EList<Category> getPositiveCategory();

	/**
	 * Returns the value of the '<em><b>Priors</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage <em>Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priors</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priors</em>' containment reference.
	 * @see #setPriors(PriorProbabilities)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationAttributeUsage_Priors()
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage
	 * @model opposite="usage" containment="true"
	 * @generated
	 */
	PriorProbabilities getPriors();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors <em>Priors</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priors</em>' containment reference.
	 * @see #getPriors()
	 * @generated
	 */
	void setPriors(PriorProbabilities value);

} // ClassificationAttributeUsage
