/**
 * INRIA/IRISA
 *
 * $Id: CategoricalAttributeProperties.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Categorical Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A CategoricalAttributeProperties object is used to describe properties of a categorical attribute. It lists the specific categories that are recognized in the attribute, as well as a taxonomy, or CategorizationGraph, that organizes the categories into a hierarchy.
 * 
 * This metadata may or may not be used by the underlying algorithm. It may be leveraged to determine if data being supplied as input to a mining operation is sufficiently similar to the data used to build the model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getTaxonomy <em>Taxonomy</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getCategory <em>Category</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoricalAttributeProperties()
 * @model
 * @generated
 */
public interface CategoricalAttributeProperties extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Logical Attribute</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties <em>Categorical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Attribute</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Attribute</em>' container reference.
	 * @see #setLogicalAttribute(LogicalAttribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoricalAttributeProperties_LogicalAttribute()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getCategoricalProperties
	 * @model opposite="categoricalProperties" required="true"
	 * @generated
	 */
	LogicalAttribute getLogicalAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical Attribute</em>' container reference.
	 * @see #getLogicalAttribute()
	 * @generated
	 */
	void setLogicalAttribute(LogicalAttribute value);

	/**
	 * Returns the value of the '<em><b>Taxonomy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Taxonomy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Taxonomy</em>' reference.
	 * @see #setTaxonomy(CategoryTaxonomy)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoricalAttributeProperties_Taxonomy()
	 * @model
	 * @generated
	 */
	CategoryTaxonomy getTaxonomy();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoricalAttributeProperties#getTaxonomy <em>Taxonomy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Taxonomy</em>' reference.
	 * @see #getTaxonomy()
	 * @generated
	 */
	void setTaxonomy(CategoryTaxonomy value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties <em>Categorical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoricalAttributeProperties_Category()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.Category#getCategoricalProperties
	 * @model opposite="categoricalProperties" containment="true"
	 * @generated
	 */
	EList<Category> getCategory();

} // CategoricalAttributeProperties
