/**
 * INRIA/IRISA
 *
 * $Id: ClassificationFunctionSettings.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrix;

import orgomg.cwm.analysis.datamining.supervised.SupervisedFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ClassificationFunctionSettings object is a subclass of SupervisedFunctionSettings that supports features unique to the classification mining function and corresponding algorithms, specifically costMatrix.
 * 
 * The costMatrix must be associated with the target MiningAttribute.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings#getCostMatrix <em>Cost Matrix</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationFunctionSettings()
 * @model
 * @generated
 */
public interface ClassificationFunctionSettings extends SupervisedFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Cost Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost Matrix</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost Matrix</em>' reference.
	 * @see #setCostMatrix(CategoryMatrix)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getClassificationFunctionSettings_CostMatrix()
	 * @model
	 * @generated
	 */
	CategoryMatrix getCostMatrix();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.ClassificationFunctionSettings#getCostMatrix <em>Cost Matrix</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost Matrix</em>' reference.
	 * @see #getCostMatrix()
	 * @generated
	 */
	void setCostMatrix(CategoryMatrix value);

} // ClassificationFunctionSettings
