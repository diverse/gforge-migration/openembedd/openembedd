/**
 * INRIA/IRISA
 *
 * $Id: ClusteringFunctionSettings.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningFunctionSettings;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ClusteringFunctionSettings object is a subclass of MiningFunctionSettings that supports features unique to clustering algorithms, such as: self-organizing map and k-means.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMaxNumberOfClusters <em>Max Number Of Clusters</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMinClusterSize <em>Min Cluster Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getAggregationFunction <em>Aggregation Function</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringFunctionSettings()
 * @model
 * @generated
 */
public interface ClusteringFunctionSettings extends MiningFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Max Number Of Clusters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The maxNumberOfClusters attribute specifies the maximum number of clusters the clustering algorithm should generate.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Max Number Of Clusters</em>' attribute.
	 * @see #setMaxNumberOfClusters(long)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringFunctionSettings_MaxNumberOfClusters()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getMaxNumberOfClusters();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMaxNumberOfClusters <em>Max Number Of Clusters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Number Of Clusters</em>' attribute.
	 * @see #getMaxNumberOfClusters()
	 * @generated
	 */
	void setMaxNumberOfClusters(long value);

	/**
	 * Returns the value of the '<em><b>Min Cluster Size</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The minClusterSize attribute specifies the minimum number of records (cases) that must be present in a cluster to establish a cluster.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Min Cluster Size</em>' attribute.
	 * @see #setMinClusterSize(long)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringFunctionSettings_MinClusterSize()
	 * @model default="1" dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getMinClusterSize();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMinClusterSize <em>Min Cluster Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Cluster Size</em>' attribute.
	 * @see #getMinClusterSize()
	 * @generated
	 */
	void setMinClusterSize(long value);

	/**
	 * Returns the value of the '<em><b>Aggregation Function</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.clustering.AggregationFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aggregation Function</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aggregation Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.clustering.AggregationFunction
	 * @see #setAggregationFunction(AggregationFunction)
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringFunctionSettings_AggregationFunction()
	 * @model
	 * @generated
	 */
	AggregationFunction getAggregationFunction();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getAggregationFunction <em>Aggregation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aggregation Function</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.clustering.AggregationFunction
	 * @see #getAggregationFunction()
	 * @generated
	 */
	void setAggregationFunction(AggregationFunction value);

} // ClusteringFunctionSettings
