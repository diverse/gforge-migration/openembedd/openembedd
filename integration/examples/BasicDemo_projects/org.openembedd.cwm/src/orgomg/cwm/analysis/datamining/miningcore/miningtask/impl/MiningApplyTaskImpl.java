/**
 * INRIA/IRISA
 *
 * $Id: MiningApplyTaskImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputOption;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyTask;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Apply Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl#getOutputOption <em>Output Option</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl#getOutputAssignment <em>Output Assignment</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningApplyTaskImpl#getApplyOutput <em>Apply Output</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MiningApplyTaskImpl extends MiningTaskImpl implements MiningApplyTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getOutputOption() <em>Output Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputOption()
	 * @generated
	 * @ordered
	 */
	protected static final ApplyOutputOption OUTPUT_OPTION_EDEFAULT = ApplyOutputOption.CREATE_NEW_LITERAL;

	/**
	 * The cached value of the '{@link #getOutputOption() <em>Output Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputOption()
	 * @generated
	 * @ordered
	 */
	protected ApplyOutputOption outputOption = OUTPUT_OPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOutputAssignment() <em>Output Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputAssignment()
	 * @generated
	 * @ordered
	 */
	protected AttributeAssignmentSet outputAssignment;

	/**
	 * The cached value of the '{@link #getApplyOutput() <em>Apply Output</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplyOutput()
	 * @generated
	 * @ordered
	 */
	protected MiningApplyOutput applyOutput;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningApplyTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.MINING_APPLY_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplyOutputOption getOutputOption() {
		return outputOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputOption(ApplyOutputOption newOutputOption) {
		ApplyOutputOption oldOutputOption = outputOption;
		outputOption = newOutputOption == null ? OUTPUT_OPTION_EDEFAULT : newOutputOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_OPTION, oldOutputOption, outputOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignmentSet getOutputAssignment() {
		if (outputAssignment != null && outputAssignment.eIsProxy()) {
			InternalEObject oldOutputAssignment = (InternalEObject)outputAssignment;
			outputAssignment = (AttributeAssignmentSet)eResolveProxy(oldOutputAssignment);
			if (outputAssignment != oldOutputAssignment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT, oldOutputAssignment, outputAssignment));
			}
		}
		return outputAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeAssignmentSet basicGetOutputAssignment() {
		return outputAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputAssignment(AttributeAssignmentSet newOutputAssignment) {
		AttributeAssignmentSet oldOutputAssignment = outputAssignment;
		outputAssignment = newOutputAssignment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT, oldOutputAssignment, outputAssignment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningApplyOutput getApplyOutput() {
		if (applyOutput != null && applyOutput.eIsProxy()) {
			InternalEObject oldApplyOutput = (InternalEObject)applyOutput;
			applyOutput = (MiningApplyOutput)eResolveProxy(oldApplyOutput);
			if (applyOutput != oldApplyOutput) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT, oldApplyOutput, applyOutput));
			}
		}
		return applyOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningApplyOutput basicGetApplyOutput() {
		return applyOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplyOutput(MiningApplyOutput newApplyOutput) {
		MiningApplyOutput oldApplyOutput = applyOutput;
		applyOutput = newApplyOutput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT, oldApplyOutput, applyOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_OPTION:
				return getOutputOption();
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT:
				if (resolve) return getOutputAssignment();
				return basicGetOutputAssignment();
			case MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT:
				if (resolve) return getApplyOutput();
				return basicGetApplyOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_OPTION:
				setOutputOption((ApplyOutputOption)newValue);
				return;
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT:
				setOutputAssignment((AttributeAssignmentSet)newValue);
				return;
			case MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT:
				setApplyOutput((MiningApplyOutput)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_OPTION:
				setOutputOption(OUTPUT_OPTION_EDEFAULT);
				return;
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT:
				setOutputAssignment((AttributeAssignmentSet)null);
				return;
			case MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT:
				setApplyOutput((MiningApplyOutput)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_OPTION:
				return outputOption != OUTPUT_OPTION_EDEFAULT;
			case MiningtaskPackage.MINING_APPLY_TASK__OUTPUT_ASSIGNMENT:
				return outputAssignment != null;
			case MiningtaskPackage.MINING_APPLY_TASK__APPLY_OUTPUT:
				return applyOutput != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (outputOption: ");
		result.append(outputOption);
		result.append(')');
		return result.toString();
	}

} //MiningApplyTaskImpl
