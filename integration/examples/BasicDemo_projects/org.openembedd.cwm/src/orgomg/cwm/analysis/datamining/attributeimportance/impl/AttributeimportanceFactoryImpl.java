/**
 * INRIA/IRISA
 *
 * $Id: AttributeimportanceFactoryImpl.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.attributeimportance.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.attributeimportance.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeimportanceFactoryImpl extends EFactoryImpl implements AttributeimportanceFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeimportanceFactory init() {
		try {
			AttributeimportanceFactory theAttributeimportanceFactory = (AttributeimportanceFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/attributeimportance.ecore"); 
			if (theAttributeimportanceFactory != null) {
				return theAttributeimportanceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AttributeimportanceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeimportanceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AttributeimportancePackage.ATTRIBUTE_IMPORTANCE_SETTINGS: return createAttributeImportanceSettings();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeImportanceSettings createAttributeImportanceSettings() {
		AttributeImportanceSettingsImpl attributeImportanceSettings = new AttributeImportanceSettingsImpl();
		return attributeImportanceSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeimportancePackage getAttributeimportancePackage() {
		return (AttributeimportancePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AttributeimportancePackage getPackage() {
		return AttributeimportancePackage.eINSTANCE;
	}

} //AttributeimportanceFactoryImpl
