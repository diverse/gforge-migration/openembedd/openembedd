/**
 * <copyright>
 * </copyright>
 *
 * $Id: TransformationTaskValidator.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.transformation.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.analysis.transformation.Transformation;
import orgomg.cwm.analysis.transformation.TransformationStep;
import orgomg.cwm.analysis.transformation.TransformationTask;

/**
 * A sample validator interface for {@link orgomg.cwm.analysis.transformation.TransformationTask}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface TransformationTaskValidator {
	boolean validate();

	boolean validateStep(EList<TransformationStep> value);
	boolean validateOriginalTask(EList<TransformationTask> value);
	boolean validateInverseTask(EList<TransformationTask> value);
	boolean validateTransformation(EList<Transformation> value);
}
