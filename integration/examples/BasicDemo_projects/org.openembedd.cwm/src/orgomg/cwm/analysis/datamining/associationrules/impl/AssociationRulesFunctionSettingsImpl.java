/**
 * INRIA/IRISA
 *
 * $Id: AssociationRulesFunctionSettingsImpl.java,v 1.1 2008-04-01 09:35:54 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings;
import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Rules Function Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl#getMinimumConfidence <em>Minimum Confidence</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl#getMaximumRuleLength <em>Maximum Rule Length</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationRulesFunctionSettingsImpl extends FrequentItemSetFunctionSettingsImpl implements AssociationRulesFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getMinimumConfidence() <em>Minimum Confidence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumConfidence()
	 * @generated
	 * @ordered
	 */
	protected static final String MINIMUM_CONFIDENCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinimumConfidence() <em>Minimum Confidence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumConfidence()
	 * @generated
	 * @ordered
	 */
	protected String minimumConfidence = MINIMUM_CONFIDENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaximumRuleLength() <em>Maximum Rule Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumRuleLength()
	 * @generated
	 * @ordered
	 */
	protected static final long MAXIMUM_RULE_LENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMaximumRuleLength() <em>Maximum Rule Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumRuleLength()
	 * @generated
	 * @ordered
	 */
	protected long maximumRuleLength = MAXIMUM_RULE_LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationRulesFunctionSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssociationrulesPackage.Literals.ASSOCIATION_RULES_FUNCTION_SETTINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMinimumConfidence() {
		return minimumConfidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinimumConfidence(String newMinimumConfidence) {
		String oldMinimumConfidence = minimumConfidence;
		minimumConfidence = newMinimumConfidence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE, oldMinimumConfidence, minimumConfidence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaximumRuleLength() {
		return maximumRuleLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumRuleLength(long newMaximumRuleLength) {
		long oldMaximumRuleLength = maximumRuleLength;
		maximumRuleLength = newMaximumRuleLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH, oldMaximumRuleLength, maximumRuleLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE:
				return getMinimumConfidence();
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH:
				return new Long(getMaximumRuleLength());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE:
				setMinimumConfidence((String)newValue);
				return;
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH:
				setMaximumRuleLength(((Long)newValue).longValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE:
				setMinimumConfidence(MINIMUM_CONFIDENCE_EDEFAULT);
				return;
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH:
				setMaximumRuleLength(MAXIMUM_RULE_LENGTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE:
				return MINIMUM_CONFIDENCE_EDEFAULT == null ? minimumConfidence != null : !MINIMUM_CONFIDENCE_EDEFAULT.equals(minimumConfidence);
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH:
				return maximumRuleLength != MAXIMUM_RULE_LENGTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minimumConfidence: ");
		result.append(minimumConfidence);
		result.append(", maximumRuleLength: ");
		result.append(maximumRuleLength);
		result.append(')');
		return result.toString();
	}

} //AssociationRulesFunctionSettingsImpl
