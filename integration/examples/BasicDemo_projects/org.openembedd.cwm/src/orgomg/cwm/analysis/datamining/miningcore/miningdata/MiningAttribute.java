/**
 * INRIA/IRISA
 *
 * $Id: MiningAttribute.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is an abstract class that describes the generic attribute to be used in mining operations.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getDisplayName <em>Display Name</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getAttributeType <em>Attribute Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getMiningAttribute()
 * @model abstract="true"
 * @generated
 */
public interface MiningAttribute extends Attribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The optional displayName of an attribute indicates a name that an application may use as a substitute for the actual MiningAttribute name, which may be cryptic.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Display Name</em>' attribute.
	 * @see #setDisplayName(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getMiningAttribute_DisplayName()
	 * @model dataType="orgomg.cwm.objectmodel.core.String"
	 * @generated
	 */
	String getDisplayName();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getDisplayName <em>Display Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Name</em>' attribute.
	 * @see #getDisplayName()
	 * @generated
	 */
	void setDisplayName(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The attribute type indicates if the attribute is categorical, ordinal,  numerical, or not specified. 
	 * 
	 * If either categoricalProperties or numericalProperties are specified, a constraint exists to ensure the attributeType value is consistent with these attributes.
	 * 
	 * This attribute allows a MiningAttribute to be identified with a particular type even if no additional properties are specified.
	 * 
	 * If ordinal, then the OrdinalAttributeProperties must be specified to indicate the ordering of the categories.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attribute Type</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType
	 * @see #setAttributeType(AttributeType)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getMiningAttribute_AttributeType()
	 * @model
	 * @generated
	 */
	AttributeType getAttributeType();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute#getAttributeType <em>Attribute Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeType
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(AttributeType value);

} // MiningAttribute
