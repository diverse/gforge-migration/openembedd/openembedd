/**
 * INRIA/IRISA
 *
 * $Id: AssociationrulesPackage.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains the metamodel that represents the constructs for frequent itemset, association rules and sequence algorithms.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesFactory
 * @model kind="package"
 * @generated
 */
public interface AssociationrulesPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "associationrules";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/associationrules.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.associationrules";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssociationrulesPackage eINSTANCE = orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl <em>Frequent Item Set Function Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getFrequentItemSetFunctionSettings()
	 * @generated
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAME = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VISIBILITY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CLIENT_DEPENDENCY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CONSTRAINT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAMESPACE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__IMPORTER = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__STEREOTYPE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__TAGGED_VALUE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DOCUMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESCRIPTION = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RESPONSIBLE_PARTY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ELEMENT_NODE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SET = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RENDERED_OBJECT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VOCABULARY_ELEMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MEASUREMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CHANGE_REQUEST = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ALGORITHM_SETTINGS = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__LOGICAL_DATA = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SCHEMA = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Minimum Support</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Maximum Set Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Exclusion</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Frequent Item Set Function Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl <em>Association Rules Function Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getAssociationRulesFunctionSettings()
	 * @generated
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__NAME = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__VISIBILITY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__CLIENT_DEPENDENCY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__CONSTRAINT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__NAMESPACE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__IMPORTER = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__STEREOTYPE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__TAGGED_VALUE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__DOCUMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__DESCRIPTION = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__RESPONSIBLE_PARTY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__ELEMENT_NODE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__SET = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__RENDERED_OBJECT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__VOCABULARY_ELEMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__MEASUREMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__CHANGE_REQUEST = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__ALGORITHM_SETTINGS = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__LOGICAL_DATA = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__SCHEMA = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Minimum Support</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_SUPPORT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT;

	/**
	 * The feature id for the '<em><b>Maximum Set Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE;

	/**
	 * The feature id for the '<em><b>Exclusion</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__EXCLUSION = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION;

	/**
	 * The feature id for the '<em><b>Minimum Confidence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Maximum Rule Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH = FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Association Rules Function Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_RULES_FUNCTION_SETTINGS_FEATURE_COUNT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.SequenceFunctionSettingsImpl <em>Sequence Function Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.SequenceFunctionSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getSequenceFunctionSettings()
	 * @generated
	 */
	int SEQUENCE_FUNCTION_SETTINGS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__NAME = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__VISIBILITY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__CLIENT_DEPENDENCY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__CONSTRAINT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__NAMESPACE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__IMPORTER = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__STEREOTYPE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__TAGGED_VALUE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__DOCUMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__DESCRIPTION = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__RESPONSIBLE_PARTY = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__ELEMENT_NODE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__SET = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__RENDERED_OBJECT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__VOCABULARY_ELEMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__MEASUREMENT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__CHANGE_REQUEST = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__ALGORITHM_SETTINGS = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__LOGICAL_DATA = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__SCHEMA = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Minimum Support</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__MINIMUM_SUPPORT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT;

	/**
	 * The feature id for the '<em><b>Maximum Set Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE;

	/**
	 * The feature id for the '<em><b>Exclusion</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__EXCLUSION = FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION;

	/**
	 * The feature id for the '<em><b>Window Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS__WINDOW_SIZE = FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequence Function Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FUNCTION_SETTINGS_FEATURE_COUNT = FREQUENT_ITEM_SET_FUNCTION_SETTINGS_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings <em>Association Rules Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association Rules Function Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings
	 * @generated
	 */
	EClass getAssociationRulesFunctionSettings();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMinimumConfidence <em>Minimum Confidence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum Confidence</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMinimumConfidence()
	 * @see #getAssociationRulesFunctionSettings()
	 * @generated
	 */
	EAttribute getAssociationRulesFunctionSettings_MinimumConfidence();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMaximumRuleLength <em>Maximum Rule Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Rule Length</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationRulesFunctionSettings#getMaximumRuleLength()
	 * @see #getAssociationRulesFunctionSettings()
	 * @generated
	 */
	EAttribute getAssociationRulesFunctionSettings_MaximumRuleLength();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings <em>Frequent Item Set Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Frequent Item Set Function Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings
	 * @generated
	 */
	EClass getFrequentItemSetFunctionSettings();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMinimumSupport <em>Minimum Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum Support</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMinimumSupport()
	 * @see #getFrequentItemSetFunctionSettings()
	 * @generated
	 */
	EAttribute getFrequentItemSetFunctionSettings_MinimumSupport();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMaximumSetSize <em>Maximum Set Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Set Size</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getMaximumSetSize()
	 * @see #getFrequentItemSetFunctionSettings()
	 * @generated
	 */
	EAttribute getFrequentItemSetFunctionSettings_MaximumSetSize();

	/**
	 * Returns the meta object for the reference list '{@link orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getExclusion <em>Exclusion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exclusion</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.FrequentItemSetFunctionSettings#getExclusion()
	 * @see #getFrequentItemSetFunctionSettings()
	 * @generated
	 */
	EReference getFrequentItemSetFunctionSettings_Exclusion();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings <em>Sequence Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Function Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings
	 * @generated
	 */
	EClass getSequenceFunctionSettings();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings#getWindowSize <em>Window Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Size</em>'.
	 * @see orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings#getWindowSize()
	 * @see #getSequenceFunctionSettings()
	 * @generated
	 */
	EAttribute getSequenceFunctionSettings_WindowSize();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AssociationrulesFactory getAssociationrulesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl <em>Association Rules Function Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationRulesFunctionSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getAssociationRulesFunctionSettings()
		 * @generated
		 */
		EClass ASSOCIATION_RULES_FUNCTION_SETTINGS = eINSTANCE.getAssociationRulesFunctionSettings();

		/**
		 * The meta object literal for the '<em><b>Minimum Confidence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSOCIATION_RULES_FUNCTION_SETTINGS__MINIMUM_CONFIDENCE = eINSTANCE.getAssociationRulesFunctionSettings_MinimumConfidence();

		/**
		 * The meta object literal for the '<em><b>Maximum Rule Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSOCIATION_RULES_FUNCTION_SETTINGS__MAXIMUM_RULE_LENGTH = eINSTANCE.getAssociationRulesFunctionSettings_MaximumRuleLength();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl <em>Frequent Item Set Function Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.FrequentItemSetFunctionSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getFrequentItemSetFunctionSettings()
		 * @generated
		 */
		EClass FREQUENT_ITEM_SET_FUNCTION_SETTINGS = eINSTANCE.getFrequentItemSetFunctionSettings();

		/**
		 * The meta object literal for the '<em><b>Minimum Support</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MINIMUM_SUPPORT = eINSTANCE.getFrequentItemSetFunctionSettings_MinimumSupport();

		/**
		 * The meta object literal for the '<em><b>Maximum Set Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREQUENT_ITEM_SET_FUNCTION_SETTINGS__MAXIMUM_SET_SIZE = eINSTANCE.getFrequentItemSetFunctionSettings_MaximumSetSize();

		/**
		 * The meta object literal for the '<em><b>Exclusion</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FREQUENT_ITEM_SET_FUNCTION_SETTINGS__EXCLUSION = eINSTANCE.getFrequentItemSetFunctionSettings_Exclusion();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.associationrules.impl.SequenceFunctionSettingsImpl <em>Sequence Function Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.SequenceFunctionSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl#getSequenceFunctionSettings()
		 * @generated
		 */
		EClass SEQUENCE_FUNCTION_SETTINGS = eINSTANCE.getSequenceFunctionSettings();

		/**
		 * The meta object literal for the '<em><b>Window Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEQUENCE_FUNCTION_SETTINGS__WINDOW_SIZE = eINSTANCE.getSequenceFunctionSettings_WindowSize();

	}

} //AssociationrulesPackage
