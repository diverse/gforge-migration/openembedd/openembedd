/**
 * INRIA/IRISA
 *
 * $Id: ClusteringPackage.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains the metamodel that represents clustering function, models, and settings.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringFactory
 * @model kind="package"
 * @generated
 */
public interface ClusteringPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "clustering";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///org/omg/cwm/analysis/datamining/clustering.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.omg.cwm.analysis.datamining.clustering";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClusteringPackage eINSTANCE = orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl.init();

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringAttributeUsageImpl <em>Attribute Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringAttributeUsageImpl
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringAttributeUsage()
	 * @generated
	 */
	int CLUSTERING_ATTRIBUTE_USAGE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__NAME = MiningdataPackage.ATTRIBUTE_USAGE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__VISIBILITY = MiningdataPackage.ATTRIBUTE_USAGE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__CLIENT_DEPENDENCY = MiningdataPackage.ATTRIBUTE_USAGE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__SUPPLIER_DEPENDENCY = MiningdataPackage.ATTRIBUTE_USAGE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__CONSTRAINT = MiningdataPackage.ATTRIBUTE_USAGE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__NAMESPACE = MiningdataPackage.ATTRIBUTE_USAGE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__IMPORTER = MiningdataPackage.ATTRIBUTE_USAGE__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__STEREOTYPE = MiningdataPackage.ATTRIBUTE_USAGE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__TAGGED_VALUE = MiningdataPackage.ATTRIBUTE_USAGE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__DOCUMENT = MiningdataPackage.ATTRIBUTE_USAGE__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__DESCRIPTION = MiningdataPackage.ATTRIBUTE_USAGE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__RESPONSIBLE_PARTY = MiningdataPackage.ATTRIBUTE_USAGE__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__ELEMENT_NODE = MiningdataPackage.ATTRIBUTE_USAGE__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__SET = MiningdataPackage.ATTRIBUTE_USAGE__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__RENDERED_OBJECT = MiningdataPackage.ATTRIBUTE_USAGE__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__VOCABULARY_ELEMENT = MiningdataPackage.ATTRIBUTE_USAGE__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__MEASUREMENT = MiningdataPackage.ATTRIBUTE_USAGE__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__CHANGE_REQUEST = MiningdataPackage.ATTRIBUTE_USAGE__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Owner Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__OWNER_SCOPE = MiningdataPackage.ATTRIBUTE_USAGE__OWNER_SCOPE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__OWNER = MiningdataPackage.ATTRIBUTE_USAGE__OWNER;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__FEATURE_NODE = MiningdataPackage.ATTRIBUTE_USAGE__FEATURE_NODE;

	/**
	 * The feature id for the '<em><b>Feature Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__FEATURE_MAP = MiningdataPackage.ATTRIBUTE_USAGE__FEATURE_MAP;

	/**
	 * The feature id for the '<em><b>Cf Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__CF_MAP = MiningdataPackage.ATTRIBUTE_USAGE__CF_MAP;

	/**
	 * The feature id for the '<em><b>Usage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__USAGE = MiningdataPackage.ATTRIBUTE_USAGE__USAGE;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__WEIGHT = MiningdataPackage.ATTRIBUTE_USAGE__WEIGHT;

	/**
	 * The feature id for the '<em><b>Suppress Discretization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION = MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_DISCRETIZATION;

	/**
	 * The feature id for the '<em><b>Suppress Normalization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION = MiningdataPackage.ATTRIBUTE_USAGE__SUPPRESS_NORMALIZATION;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__ATTRIBUTE = MiningdataPackage.ATTRIBUTE_USAGE__ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Attribute Comparison Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__ATTRIBUTE_COMPARISON_FUNCTION = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Similarity Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__SIMILARITY_SCALE = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comparison Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE__COMPARISON_MATRIX = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Attribute Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_ATTRIBUTE_USAGE_FEATURE_COUNT = MiningdataPackage.ATTRIBUTE_USAGE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl <em>Function Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringFunctionSettings()
	 * @generated
	 */
	int CLUSTERING_FUNCTION_SETTINGS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__NAME = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__VISIBILITY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__CLIENT_DEPENDENCY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__CONSTRAINT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__NAMESPACE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Importer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__IMPORTER = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__IMPORTER;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__STEREOTYPE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__TAGGED_VALUE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Document</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__DOCUMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DOCUMENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__DESCRIPTION = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Responsible Party</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__RESPONSIBLE_PARTY = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__RESPONSIBLE_PARTY;

	/**
	 * The feature id for the '<em><b>Element Node</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__ELEMENT_NODE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ELEMENT_NODE;

	/**
	 * The feature id for the '<em><b>Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__SET = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SET;

	/**
	 * The feature id for the '<em><b>Rendered Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__RENDERED_OBJECT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__RENDERED_OBJECT;

	/**
	 * The feature id for the '<em><b>Vocabulary Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__VOCABULARY_ELEMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__VOCABULARY_ELEMENT;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__MEASUREMENT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__MEASUREMENT;

	/**
	 * The feature id for the '<em><b>Change Request</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__CHANGE_REQUEST = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__CHANGE_REQUEST;

	/**
	 * The feature id for the '<em><b>Desired Execution Time In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__DESIRED_EXECUTION_TIME_IN_MINUTES;

	/**
	 * The feature id for the '<em><b>Algorithm Settings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ALGORITHM_SETTINGS;

	/**
	 * The feature id for the '<em><b>Attribute Usage Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__ATTRIBUTE_USAGE_SET;

	/**
	 * The feature id for the '<em><b>Logical Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__LOGICAL_DATA = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__LOGICAL_DATA;

	/**
	 * The feature id for the '<em><b>Schema</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__SCHEMA = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS__SCHEMA;

	/**
	 * The feature id for the '<em><b>Max Number Of Clusters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Cluster Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Aggregation Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Function Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTERING_FUNCTION_SETTINGS_FEATURE_COUNT = MiningfunctionsettingsPackage.MINING_FUNCTION_SETTINGS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.clustering.AggregationFunction <em>Aggregation Function</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.clustering.AggregationFunction
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getAggregationFunction()
	 * @generated
	 */
	int AGGREGATION_FUNCTION = 2;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction <em>Attribute Comparison Function</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getAttributeComparisonFunction()
	 * @generated
	 */
	int ATTRIBUTE_COMPARISON_FUNCTION = 3;

	/**
	 * The meta object id for the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis <em>Model CBasis</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis
	 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringModelCBasis()
	 * @generated
	 */
	int CLUSTERING_MODEL_CBASIS = 4;


	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage <em>Attribute Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Usage</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage
	 * @generated
	 */
	EClass getClusteringAttributeUsage();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getAttributeComparisonFunction <em>Attribute Comparison Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Comparison Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getAttributeComparisonFunction()
	 * @see #getClusteringAttributeUsage()
	 * @generated
	 */
	EAttribute getClusteringAttributeUsage_AttributeComparisonFunction();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getSimilarityScale <em>Similarity Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Similarity Scale</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getSimilarityScale()
	 * @see #getClusteringAttributeUsage()
	 * @generated
	 */
	EAttribute getClusteringAttributeUsage_SimilarityScale();

	/**
	 * Returns the meta object for the reference '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getComparisonMatrix <em>Comparison Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Comparison Matrix</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringAttributeUsage#getComparisonMatrix()
	 * @see #getClusteringAttributeUsage()
	 * @generated
	 */
	EReference getClusteringAttributeUsage_ComparisonMatrix();

	/**
	 * Returns the meta object for class '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings <em>Function Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Settings</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings
	 * @generated
	 */
	EClass getClusteringFunctionSettings();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMaxNumberOfClusters <em>Max Number Of Clusters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Number Of Clusters</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMaxNumberOfClusters()
	 * @see #getClusteringFunctionSettings()
	 * @generated
	 */
	EAttribute getClusteringFunctionSettings_MaxNumberOfClusters();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMinClusterSize <em>Min Cluster Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Cluster Size</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getMinClusterSize()
	 * @see #getClusteringFunctionSettings()
	 * @generated
	 */
	EAttribute getClusteringFunctionSettings_MinClusterSize();

	/**
	 * Returns the meta object for the attribute '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getAggregationFunction <em>Aggregation Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aggregation Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringFunctionSettings#getAggregationFunction()
	 * @see #getClusteringFunctionSettings()
	 * @generated
	 */
	EAttribute getClusteringFunctionSettings_AggregationFunction();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.clustering.AggregationFunction <em>Aggregation Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Aggregation Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.AggregationFunction
	 * @generated
	 */
	EEnum getAggregationFunction();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction <em>Attribute Comparison Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attribute Comparison Function</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction
	 * @generated
	 */
	EEnum getAttributeComparisonFunction();

	/**
	 * Returns the meta object for enum '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis <em>Model CBasis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Model CBasis</em>'.
	 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis
	 * @generated
	 */
	EEnum getClusteringModelCBasis();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClusteringFactory getClusteringFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringAttributeUsageImpl <em>Attribute Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringAttributeUsageImpl
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringAttributeUsage()
		 * @generated
		 */
		EClass CLUSTERING_ATTRIBUTE_USAGE = eINSTANCE.getClusteringAttributeUsage();

		/**
		 * The meta object literal for the '<em><b>Attribute Comparison Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTERING_ATTRIBUTE_USAGE__ATTRIBUTE_COMPARISON_FUNCTION = eINSTANCE.getClusteringAttributeUsage_AttributeComparisonFunction();

		/**
		 * The meta object literal for the '<em><b>Similarity Scale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTERING_ATTRIBUTE_USAGE__SIMILARITY_SCALE = eINSTANCE.getClusteringAttributeUsage_SimilarityScale();

		/**
		 * The meta object literal for the '<em><b>Comparison Matrix</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLUSTERING_ATTRIBUTE_USAGE__COMPARISON_MATRIX = eINSTANCE.getClusteringAttributeUsage_ComparisonMatrix();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl <em>Function Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFunctionSettingsImpl
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringFunctionSettings()
		 * @generated
		 */
		EClass CLUSTERING_FUNCTION_SETTINGS = eINSTANCE.getClusteringFunctionSettings();

		/**
		 * The meta object literal for the '<em><b>Max Number Of Clusters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTERING_FUNCTION_SETTINGS__MAX_NUMBER_OF_CLUSTERS = eINSTANCE.getClusteringFunctionSettings_MaxNumberOfClusters();

		/**
		 * The meta object literal for the '<em><b>Min Cluster Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTERING_FUNCTION_SETTINGS__MIN_CLUSTER_SIZE = eINSTANCE.getClusteringFunctionSettings_MinClusterSize();

		/**
		 * The meta object literal for the '<em><b>Aggregation Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTERING_FUNCTION_SETTINGS__AGGREGATION_FUNCTION = eINSTANCE.getClusteringFunctionSettings_AggregationFunction();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.clustering.AggregationFunction <em>Aggregation Function</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.clustering.AggregationFunction
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getAggregationFunction()
		 * @generated
		 */
		EEnum AGGREGATION_FUNCTION = eINSTANCE.getAggregationFunction();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction <em>Attribute Comparison Function</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.clustering.AttributeComparisonFunction
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getAttributeComparisonFunction()
		 * @generated
		 */
		EEnum ATTRIBUTE_COMPARISON_FUNCTION = eINSTANCE.getAttributeComparisonFunction();

		/**
		 * The meta object literal for the '{@link orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis <em>Model CBasis</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringModelCBasis
		 * @see orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl#getClusteringModelCBasis()
		 * @generated
		 */
		EEnum CLUSTERING_MODEL_CBASIS = eINSTANCE.getClusteringModelCBasis();

	}

} //ClusteringPackage
