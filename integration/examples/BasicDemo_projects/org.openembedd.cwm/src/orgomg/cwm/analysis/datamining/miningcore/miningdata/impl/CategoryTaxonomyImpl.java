/**
 * INRIA/IRISA
 *
 * $Id: CategoryTaxonomyImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;
import orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMap;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryTaxonomy;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Taxonomy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl#getCategoryMap <em>Category Map</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl#getRootCategory <em>Root Category</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryTaxonomyImpl#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryTaxonomyImpl extends ModelElementImpl implements CategoryTaxonomy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getCategoryMap() <em>Category Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategoryMap()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryMap> categoryMap;

	/**
	 * The cached value of the '{@link #getRootCategory() <em>Root Category</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootCategory()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> rootCategory;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryTaxonomyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_TAXONOMY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CategoryMap> getCategoryMap() {
		if (categoryMap == null) {
			categoryMap = new EObjectContainmentWithInverseEList<CategoryMap>(CategoryMap.class, this, MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP, MiningdataPackage.CATEGORY_MAP__TAXONOMY);
		}
		return categoryMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getRootCategory() {
		if (rootCategory == null) {
			rootCategory = new EObjectResolvingEList<Category>(Category.class, this, MiningdataPackage.CATEGORY_TAXONOMY__ROOT_CATEGORY);
		}
		return rootCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema getSchema() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA) return null;
		return (Schema)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchema(Schema newSchema, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSchema, MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchema(Schema newSchema) {
		if (newSchema != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA && newSchema != null)) {
			if (EcoreUtil.isAncestor(this, newSchema))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSchema != null)
				msgs = ((InternalEObject)newSchema).eInverseAdd(this, EntrypointPackage.SCHEMA__TAXONOMY, Schema.class, msgs);
			msgs = basicSetSchema(newSchema, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA, newSchema, newSchema));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCategoryMap()).basicAdd(otherEnd, msgs);
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSchema((Schema)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				return ((InternalEList<?>)getCategoryMap()).basicRemove(otherEnd, msgs);
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				return basicSetSchema(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				return eInternalContainer().eInverseRemove(this, EntrypointPackage.SCHEMA__TAXONOMY, Schema.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				return getCategoryMap();
			case MiningdataPackage.CATEGORY_TAXONOMY__ROOT_CATEGORY:
				return getRootCategory();
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				return getSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				getCategoryMap().clear();
				getCategoryMap().addAll((Collection<? extends CategoryMap>)newValue);
				return;
			case MiningdataPackage.CATEGORY_TAXONOMY__ROOT_CATEGORY:
				getRootCategory().clear();
				getRootCategory().addAll((Collection<? extends Category>)newValue);
				return;
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				setSchema((Schema)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				getCategoryMap().clear();
				return;
			case MiningdataPackage.CATEGORY_TAXONOMY__ROOT_CATEGORY:
				getRootCategory().clear();
				return;
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				setSchema((Schema)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_TAXONOMY__CATEGORY_MAP:
				return categoryMap != null && !categoryMap.isEmpty();
			case MiningdataPackage.CATEGORY_TAXONOMY__ROOT_CATEGORY:
				return rootCategory != null && !rootCategory.isEmpty();
			case MiningdataPackage.CATEGORY_TAXONOMY__SCHEMA:
				return getSchema() != null;
		}
		return super.eIsSet(featureID);
	}

} //CategoryTaxonomyImpl
