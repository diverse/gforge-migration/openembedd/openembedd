/**
 * INRIA/IRISA
 *
 * $Id: Catalog.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.entrypoint;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Catalog</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This object is the top level entry point for CWM Data Mining package. It contains a set of schema.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Catalog#getSchema <em>Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getCatalog()
 * @model
 * @generated
 */
public interface Catalog extends orgomg.cwm.objectmodel.core.Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Schema</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCatalog <em>Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage#getCatalog_Schema()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.Schema#getCatalog
	 * @model opposite="catalog" containment="true"
	 * @generated
	 */
	EList<Schema> getSchema();

} // Catalog
