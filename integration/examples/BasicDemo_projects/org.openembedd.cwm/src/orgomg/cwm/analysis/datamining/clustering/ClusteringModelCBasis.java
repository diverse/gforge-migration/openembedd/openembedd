/**
 * INRIA/IRISA
 *
 * $Id: ClusteringModelCBasis.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Model CBasis</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage#getClusteringModelCBasis()
 * @model
 * @generated
 */
public enum ClusteringModelCBasis implements Enumerator {
	/**
	 * The '<em><b>Center Based</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CENTER_BASED
	 * @generated
	 * @ordered
	 */
	CENTER_BASED_LITERAL(0, "centerBased", "centerBased"),

	/**
	 * The '<em><b>Probability Based</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROBABILITY_BASED
	 * @generated
	 * @ordered
	 */
	PROBABILITY_BASED_LITERAL(1, "probabilityBased", "probabilityBased"),

	/**
	 * The '<em><b>Partition Based</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARTITION_BASED
	 * @generated
	 * @ordered
	 */
	PARTITION_BASED_LITERAL(2, "partitionBased", "partitionBased"),

	/**
	 * The '<em><b>Aggregation Based</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AGGREGATION_BASED
	 * @generated
	 * @ordered
	 */
	AGGREGATION_BASED_LITERAL(3, "aggregationBased", "aggregationBased");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Center Based</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This value indicates the clustering algorithm being specified this is based on center of the cluster.
	 * <!-- end-model-doc -->
	 * @see #CENTER_BASED_LITERAL
	 * @model name="centerBased"
	 * @generated
	 * @ordered
	 */
	public static final int CENTER_BASED = 0;

	/**
	 * The '<em><b>Probability Based</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This value indicates the clustering algorithm being specified this is based on probability.
	 * <!-- end-model-doc -->
	 * @see #PROBABILITY_BASED_LITERAL
	 * @model name="probabilityBased"
	 * @generated
	 * @ordered
	 */
	public static final int PROBABILITY_BASED = 1;

	/**
	 * The '<em><b>Partition Based</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This value indicates the clustering algorithm being specified this is based on partitions of the input data.
	 * <!-- end-model-doc -->
	 * @see #PARTITION_BASED_LITERAL
	 * @model name="partitionBased"
	 * @generated
	 * @ordered
	 */
	public static final int PARTITION_BASED = 2;

	/**
	 * The '<em><b>Aggregation Based</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This value indicates the clustering algorithm being specified this is based on aggregated values of the input data.
	 * <!-- end-model-doc -->
	 * @see #AGGREGATION_BASED_LITERAL
	 * @model name="aggregationBased"
	 * @generated
	 * @ordered
	 */
	public static final int AGGREGATION_BASED = 3;

	/**
	 * An array of all the '<em><b>Model CBasis</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ClusteringModelCBasis[] VALUES_ARRAY =
		new ClusteringModelCBasis[] {
			CENTER_BASED_LITERAL,
			PROBABILITY_BASED_LITERAL,
			PARTITION_BASED_LITERAL,
			AGGREGATION_BASED_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Model CBasis</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ClusteringModelCBasis> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Model CBasis</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClusteringModelCBasis get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ClusteringModelCBasis result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Model CBasis</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClusteringModelCBasis getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ClusteringModelCBasis result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Model CBasis</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClusteringModelCBasis get(int value) {
		switch (value) {
			case CENTER_BASED: return CENTER_BASED_LITERAL;
			case PROBABILITY_BASED: return PROBABILITY_BASED_LITERAL;
			case PARTITION_BASED: return PARTITION_BASED_LITERAL;
			case AGGREGATION_BASED: return AGGREGATION_BASED_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ClusteringModelCBasis(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ClusteringModelCBasis
