/**
 * INRIA/IRISA
 *
 * $Id: ApproximationFactory.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.approximation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.approximation.ApproximationPackage
 * @generated
 */
public interface ApproximationFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApproximationFactory eINSTANCE = orgomg.cwm.analysis.datamining.approximation.impl.ApproximationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Function Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Settings</em>'.
	 * @generated
	 */
	ApproximationFunctionSettings createApproximationFunctionSettings();

	/**
	 * Returns a new object of class '<em>Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Result</em>'.
	 * @generated
	 */
	ApproximationTestResult createApproximationTestResult();

	/**
	 * Returns a new object of class '<em>Test Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Task</em>'.
	 * @generated
	 */
	ApproximationTestTask createApproximationTestTask();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApproximationPackage getApproximationPackage();

} //ApproximationFactory
