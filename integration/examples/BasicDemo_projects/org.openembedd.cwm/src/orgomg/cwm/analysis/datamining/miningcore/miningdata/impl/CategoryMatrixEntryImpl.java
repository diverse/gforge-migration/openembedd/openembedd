/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixEntryImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.Category;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Matrix Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl#getValue <em>Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl#getColumnIndex <em>Column Index</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl#getCategoryMatrix <em>Category Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMatrixEntryImpl#getRowIndex <em>Row Index</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMatrixEntryImpl extends ModelElementImpl implements CategoryMatrixEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumnIndex() <em>Column Index</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumnIndex()
	 * @generated
	 * @ordered
	 */
	protected Category columnIndex;

	/**
	 * The cached value of the '{@link #getRowIndex() <em>Row Index</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowIndex()
	 * @generated
	 * @ordered
	 */
	protected Category rowIndex;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMatrixEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MATRIX_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_ENTRY__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getColumnIndex() {
		if (columnIndex != null && columnIndex.eIsProxy()) {
			InternalEObject oldColumnIndex = (InternalEObject)columnIndex;
			columnIndex = (Category)eResolveProxy(oldColumnIndex);
			if (columnIndex != oldColumnIndex) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX, oldColumnIndex, columnIndex));
			}
		}
		return columnIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category basicGetColumnIndex() {
		return columnIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColumnIndex(Category newColumnIndex) {
		Category oldColumnIndex = columnIndex;
		columnIndex = newColumnIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX, oldColumnIndex, columnIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CategoryMatrixObject getCategoryMatrix() {
		if (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX) return null;
		return (CategoryMatrixObject)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCategoryMatrix(CategoryMatrixObject newCategoryMatrix, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCategoryMatrix, MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategoryMatrix(CategoryMatrixObject newCategoryMatrix) {
		if (newCategoryMatrix != eInternalContainer() || (eContainerFeatureID != MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX && newCategoryMatrix != null)) {
			if (EcoreUtil.isAncestor(this, newCategoryMatrix))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCategoryMatrix != null)
				msgs = ((InternalEObject)newCategoryMatrix).eInverseAdd(this, MiningdataPackage.CATEGORY_MATRIX_OBJECT__ENTRY, CategoryMatrixObject.class, msgs);
			msgs = basicSetCategoryMatrix(newCategoryMatrix, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX, newCategoryMatrix, newCategoryMatrix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getRowIndex() {
		if (rowIndex != null && rowIndex.eIsProxy()) {
			InternalEObject oldRowIndex = (InternalEObject)rowIndex;
			rowIndex = (Category)eResolveProxy(oldRowIndex);
			if (rowIndex != oldRowIndex) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX, oldRowIndex, rowIndex));
			}
		}
		return rowIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category basicGetRowIndex() {
		return rowIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRowIndex(Category newRowIndex) {
		Category oldRowIndex = rowIndex;
		rowIndex = newRowIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX, oldRowIndex, rowIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCategoryMatrix((CategoryMatrixObject)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				return basicSetCategoryMatrix(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				return eInternalContainer().eInverseRemove(this, MiningdataPackage.CATEGORY_MATRIX_OBJECT__ENTRY, CategoryMatrixObject.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__VALUE:
				return getValue();
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX:
				if (resolve) return getColumnIndex();
				return basicGetColumnIndex();
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				return getCategoryMatrix();
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX:
				if (resolve) return getRowIndex();
				return basicGetRowIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__VALUE:
				setValue((String)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX:
				setColumnIndex((Category)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				setCategoryMatrix((CategoryMatrixObject)newValue);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX:
				setRowIndex((Category)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX:
				setColumnIndex((Category)null);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				setCategoryMatrix((CategoryMatrixObject)null);
				return;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX:
				setRowIndex((Category)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__COLUMN_INDEX:
				return columnIndex != null;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__CATEGORY_MATRIX:
				return getCategoryMatrix() != null;
			case MiningdataPackage.CATEGORY_MATRIX_ENTRY__ROW_INDEX:
				return rowIndex != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //CategoryMatrixEntryImpl
