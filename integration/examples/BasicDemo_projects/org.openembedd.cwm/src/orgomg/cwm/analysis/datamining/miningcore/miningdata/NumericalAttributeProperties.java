/**
 * INRIA/IRISA
 *
 * $Id: NumericalAttributeProperties.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numerical Attribute Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A NumericalAttributeProperties object is used to describe properties of the numerical attribute.
 * This metadata may or may not be used by the underlying algorithm. It may be leveraged to determine if data being supplied as input to a mining operation is sufficiently similar to the data used to build the model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getUpperBound <em>Upper Bound</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsDiscrete <em>Is Discrete</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getAnchor <em>Anchor</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleBegin <em>Cycle Begin</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleEnd <em>Cycle End</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getDiscreteStepSize <em>Discrete Step Size</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties()
 * @model
 * @generated
 */
public interface NumericalAttributeProperties extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates the lower bound (the smallest) of the values in the attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Lower Bound</em>' attribute.
	 * @see #setLowerBound(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_LowerBound()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getLowerBound();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLowerBound <em>Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Bound</em>' attribute.
	 * @see #getLowerBound()
	 * @generated
	 */
	void setLowerBound(String value);

	/**
	 * Returns the value of the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates the upper bound (the largest) of the values in the attribute.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Upper Bound</em>' attribute.
	 * @see #setUpperBound(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_UpperBound()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getUpperBound();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getUpperBound <em>Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Bound</em>' attribute.
	 * @see #getUpperBound()
	 * @generated
	 */
	void setUpperBound(String value);

	/**
	 * Returns the value of the '<em><b>Is Discrete</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates whether the values are discrete.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Discrete</em>' attribute.
	 * @see #setIsDiscrete(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_IsDiscrete()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsDiscrete();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsDiscrete <em>Is Discrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Discrete</em>' attribute.
	 * @see #isIsDiscrete()
	 * @generated
	 */
	void setIsDiscrete(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Cyclic</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This indicates whether the values of the attributes are cyclic, i.e., the next value of the ending value is the starting value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Cyclic</em>' attribute.
	 * @see #setIsCyclic(boolean)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_IsCyclic()
	 * @model default="false" dataType="orgomg.cwm.objectmodel.core.Boolean"
	 * @generated
	 */
	boolean isIsCyclic();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#isIsCyclic <em>Is Cyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Cyclic</em>' attribute.
	 * @see #isIsCyclic()
	 * @generated
	 */
	void setIsCyclic(boolean value);

	/**
	 * Returns the value of the '<em><b>Anchor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the value of the anchor.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Anchor</em>' attribute.
	 * @see #setAnchor(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_Anchor()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getAnchor();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getAnchor <em>Anchor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anchor</em>' attribute.
	 * @see #getAnchor()
	 * @generated
	 */
	void setAnchor(String value);

	/**
	 * Returns the value of the '<em><b>Cycle Begin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the starting value of the cycle.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Cycle Begin</em>' attribute.
	 * @see #setCycleBegin(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_CycleBegin()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getCycleBegin();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleBegin <em>Cycle Begin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cycle Begin</em>' attribute.
	 * @see #getCycleBegin()
	 * @generated
	 */
	void setCycleBegin(String value);

	/**
	 * Returns the value of the '<em><b>Cycle End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the ending value of the cycle.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Cycle End</em>' attribute.
	 * @see #setCycleEnd(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_CycleEnd()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getCycleEnd();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getCycleEnd <em>Cycle End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cycle End</em>' attribute.
	 * @see #getCycleEnd()
	 * @generated
	 */
	void setCycleEnd(String value);

	/**
	 * Returns the value of the '<em><b>Discrete Step Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the interval value between two adjacent discrete values when the attribute is discrete.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Discrete Step Size</em>' attribute.
	 * @see #setDiscreteStepSize(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_DiscreteStepSize()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getDiscreteStepSize();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getDiscreteStepSize <em>Discrete Step Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Step Size</em>' attribute.
	 * @see #getDiscreteStepSize()
	 * @generated
	 */
	void setDiscreteStepSize(String value);

	/**
	 * Returns the value of the '<em><b>Logical Attribute</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties <em>Numerical Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Attribute</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Attribute</em>' container reference.
	 * @see #setLogicalAttribute(LogicalAttribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getNumericalAttributeProperties_LogicalAttribute()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.LogicalAttribute#getNumericalProperties
	 * @model opposite="numericalProperties" required="true"
	 * @generated
	 */
	LogicalAttribute getLogicalAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.NumericalAttributeProperties#getLogicalAttribute <em>Logical Attribute</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical Attribute</em>' container reference.
	 * @see #getLogicalAttribute()
	 * @generated
	 */
	void setLogicalAttribute(LogicalAttribute value);

} // NumericalAttributeProperties
