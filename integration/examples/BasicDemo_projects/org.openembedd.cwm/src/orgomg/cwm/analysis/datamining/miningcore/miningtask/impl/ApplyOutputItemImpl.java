/**
 * INRIA/IRISA
 *
 * $Id: ApplyOutputItemImpl.java,v 1.1 2008-04-01 09:35:45 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningAttributeImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.ApplyOutputItem;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningApplyOutput;
import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apply Output Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.ApplyOutputItemImpl#getApplyOutput <em>Apply Output</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApplyOutputItemImpl extends MiningAttributeImpl implements ApplyOutputItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplyOutputItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningtaskPackage.Literals.APPLY_OUTPUT_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiningApplyOutput getApplyOutput() {
		if (eContainerFeatureID != MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT) return null;
		return (MiningApplyOutput)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplyOutput(MiningApplyOutput newApplyOutput, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newApplyOutput, MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplyOutput(MiningApplyOutput newApplyOutput) {
		if (newApplyOutput != eInternalContainer() || (eContainerFeatureID != MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT && newApplyOutput != null)) {
			if (EcoreUtil.isAncestor(this, newApplyOutput))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newApplyOutput != null)
				msgs = ((InternalEObject)newApplyOutput).eInverseAdd(this, MiningtaskPackage.MINING_APPLY_OUTPUT__ITEM, MiningApplyOutput.class, msgs);
			msgs = basicSetApplyOutput(newApplyOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT, newApplyOutput, newApplyOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetApplyOutput((MiningApplyOutput)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				return basicSetApplyOutput(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				return eInternalContainer().eInverseRemove(this, MiningtaskPackage.MINING_APPLY_OUTPUT__ITEM, MiningApplyOutput.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				return getApplyOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				setApplyOutput((MiningApplyOutput)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				setApplyOutput((MiningApplyOutput)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningtaskPackage.APPLY_OUTPUT_ITEM__APPLY_OUTPUT:
				return getApplyOutput() != null;
		}
		return super.eIsSet(featureID);
	}

} //ApplyOutputItemImpl
