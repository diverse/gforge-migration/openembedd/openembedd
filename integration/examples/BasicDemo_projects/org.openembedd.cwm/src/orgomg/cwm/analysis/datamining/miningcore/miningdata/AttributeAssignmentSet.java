/**
 * INRIA/IRISA
 *
 * $Id: AttributeAssignmentSet.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Assignment Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This object contains a set of AttributeAssignment objects and completes attribute assignment for a mining operation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject <em>Auxiliary Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeAssignmentSet()
 * @model
 * @generated
 */
public interface AttributeAssignmentSet extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Auxiliary Object</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getAttributeAssignmentSet <em>Attribute Assignment Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auxiliary Object</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auxiliary Object</em>' container reference.
	 * @see #setAuxiliaryObject(AuxiliaryObject)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeAssignmentSet_AuxiliaryObject()
	 * @see orgomg.cwm.analysis.datamining.miningcore.entrypoint.AuxiliaryObject#getAttributeAssignmentSet
	 * @model opposite="attributeAssignmentSet" required="true"
	 * @generated
	 */
	AuxiliaryObject getAuxiliaryObject();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.AttributeAssignmentSet#getAuxiliaryObject <em>Auxiliary Object</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auxiliary Object</em>' container reference.
	 * @see #getAuxiliaryObject()
	 * @generated
	 */
	void setAuxiliaryObject(AuxiliaryObject value);

} // AttributeAssignmentSet
