/**
 * INRIA/IRISA
 *
 * $Id: ClusteringFactory.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.clustering;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.analysis.datamining.clustering.ClusteringPackage
 * @generated
 */
public interface ClusteringFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClusteringFactory eINSTANCE = orgomg.cwm.analysis.datamining.clustering.impl.ClusteringFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Attribute Usage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Usage</em>'.
	 * @generated
	 */
	ClusteringAttributeUsage createClusteringAttributeUsage();

	/**
	 * Returns a new object of class '<em>Function Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Settings</em>'.
	 * @generated
	 */
	ClusteringFunctionSettings createClusteringFunctionSettings();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClusteringPackage getClusteringPackage();

} //ClusteringFactory
