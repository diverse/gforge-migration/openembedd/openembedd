/**
 * INRIA/IRISA
 *
 * $Id: MiningAlgorithmSettings.java,v 1.1 2008-04-01 09:35:51 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mining Algorithm Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A mining algorithm settings object captures the parameters associated with a particular algorithm. It allows a knowledgeable user to fine tune algorithm parameters. Generally, not all parameters must be specified, however, those specified are taken into account by the underlying data mining system. 
 * Separating mining algorithm from mining function provides a natural and convenient separation for those users experienced with data mining algorithms and those only familiar with mining functions.
 * 
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage#getMiningAlgorithmSettings()
 * @model abstract="true"
 * @generated
 */
public interface MiningAlgorithmSettings extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // MiningAlgorithmSettings
