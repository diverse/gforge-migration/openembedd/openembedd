/**
 * INRIA/IRISA
 *
 * $Id: SetAttributeAssignment.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Constraint: The logicalAttribute must be set valued.
 * This object  provides a mapping between a set-valued mining attribute and a set of attributes in the physical data.
 * setIdAttribute is the set identifier of the set being mapped.
 * memberAttribute represents a set of attributes being mapped to the set-valued mining attribute.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getMemberAttribute <em>Member Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getSetAttributeAssignment()
 * @model
 * @generated
 */
public interface SetAttributeAssignment extends AttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Set Id Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Id Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Id Attribute</em>' reference.
	 * @see #setSetIdAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getSetAttributeAssignment_SetIdAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getSetIdAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Set Id Attribute</em>' reference.
	 * @see #getSetIdAttribute()
	 * @generated
	 */
	void setSetIdAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Member Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Member Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Member Attribute</em>' reference.
	 * @see #setMemberAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getSetAttributeAssignment_MemberAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getMemberAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.SetAttributeAssignment#getMemberAttribute <em>Member Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Member Attribute</em>' reference.
	 * @see #getMemberAttribute()
	 * @generated
	 */
	void setMemberAttribute(Attribute value);

} // SetAttributeAssignment
