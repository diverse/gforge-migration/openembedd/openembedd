/**
 * INRIA/IRISA
 *
 * $Id: CategoryMapTableImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapTable;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Map Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl#getChildAttribute <em>Child Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl#getTable <em>Table</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl#getGraphIdAttribute <em>Graph Id Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.CategoryMapTableImpl#getParentAttribute <em>Parent Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryMapTableImpl extends CategoryMapImpl implements CategoryMapTable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The cached value of the '{@link #getChildAttribute() <em>Child Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute childAttribute;

	/**
	 * The cached value of the '{@link #getTable() <em>Table</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected orgomg.cwm.objectmodel.core.Class table;

	/**
	 * The cached value of the '{@link #getGraphIdAttribute() <em>Graph Id Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute graphIdAttribute;

	/**
	 * The cached value of the '{@link #getParentAttribute() <em>Parent Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute parentAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryMapTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningdataPackage.Literals.CATEGORY_MAP_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getChildAttribute() {
		if (childAttribute != null && childAttribute.eIsProxy()) {
			InternalEObject oldChildAttribute = (InternalEObject)childAttribute;
			childAttribute = (Attribute)eResolveProxy(oldChildAttribute);
			if (childAttribute != oldChildAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE, oldChildAttribute, childAttribute));
			}
		}
		return childAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetChildAttribute() {
		return childAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChildAttribute(Attribute newChildAttribute) {
		Attribute oldChildAttribute = childAttribute;
		childAttribute = newChildAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE, oldChildAttribute, childAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public orgomg.cwm.objectmodel.core.Class getTable() {
		if (table != null && table.eIsProxy()) {
			InternalEObject oldTable = (InternalEObject)table;
			table = (orgomg.cwm.objectmodel.core.Class)eResolveProxy(oldTable);
			if (table != oldTable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MAP_TABLE__TABLE, oldTable, table));
			}
		}
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public orgomg.cwm.objectmodel.core.Class basicGetTable() {
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTable(orgomg.cwm.objectmodel.core.Class newTable) {
		orgomg.cwm.objectmodel.core.Class oldTable = table;
		table = newTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_TABLE__TABLE, oldTable, table));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getGraphIdAttribute() {
		if (graphIdAttribute != null && graphIdAttribute.eIsProxy()) {
			InternalEObject oldGraphIdAttribute = (InternalEObject)graphIdAttribute;
			graphIdAttribute = (Attribute)eResolveProxy(oldGraphIdAttribute);
			if (graphIdAttribute != oldGraphIdAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE, oldGraphIdAttribute, graphIdAttribute));
			}
		}
		return graphIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetGraphIdAttribute() {
		return graphIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphIdAttribute(Attribute newGraphIdAttribute) {
		Attribute oldGraphIdAttribute = graphIdAttribute;
		graphIdAttribute = newGraphIdAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE, oldGraphIdAttribute, graphIdAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getParentAttribute() {
		if (parentAttribute != null && parentAttribute.eIsProxy()) {
			InternalEObject oldParentAttribute = (InternalEObject)parentAttribute;
			parentAttribute = (Attribute)eResolveProxy(oldParentAttribute);
			if (parentAttribute != oldParentAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE, oldParentAttribute, parentAttribute));
			}
		}
		return parentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetParentAttribute() {
		return parentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAttribute(Attribute newParentAttribute) {
		Attribute oldParentAttribute = parentAttribute;
		parentAttribute = newParentAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE, oldParentAttribute, parentAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE:
				if (resolve) return getChildAttribute();
				return basicGetChildAttribute();
			case MiningdataPackage.CATEGORY_MAP_TABLE__TABLE:
				if (resolve) return getTable();
				return basicGetTable();
			case MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE:
				if (resolve) return getGraphIdAttribute();
				return basicGetGraphIdAttribute();
			case MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE:
				if (resolve) return getParentAttribute();
				return basicGetParentAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE:
				setChildAttribute((Attribute)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__TABLE:
				setTable((orgomg.cwm.objectmodel.core.Class)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE:
				setGraphIdAttribute((Attribute)newValue);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE:
				setParentAttribute((Attribute)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE:
				setChildAttribute((Attribute)null);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__TABLE:
				setTable((orgomg.cwm.objectmodel.core.Class)null);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE:
				setGraphIdAttribute((Attribute)null);
				return;
			case MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE:
				setParentAttribute((Attribute)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiningdataPackage.CATEGORY_MAP_TABLE__CHILD_ATTRIBUTE:
				return childAttribute != null;
			case MiningdataPackage.CATEGORY_MAP_TABLE__TABLE:
				return table != null;
			case MiningdataPackage.CATEGORY_MAP_TABLE__GRAPH_ID_ATTRIBUTE:
				return graphIdAttribute != null;
			case MiningdataPackage.CATEGORY_MAP_TABLE__PARENT_ATTRIBUTE:
				return parentAttribute != null;
		}
		return super.eIsSet(featureID);
	}

} //CategoryMapTableImpl
