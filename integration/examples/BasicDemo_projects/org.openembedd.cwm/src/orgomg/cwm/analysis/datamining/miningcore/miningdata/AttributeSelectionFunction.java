/**
 * INRIA/IRISA
 *
 * $Id: AttributeSelectionFunction.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Attribute Selection Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This enumeration determines which attriute in the given data to select and map it to a set-valued logical attribute.
 * The attributes that satisfy the given condition in this enumeration get selected.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getAttributeSelectionFunction()
 * @model
 * @generated
 */
public enum AttributeSelectionFunction implements Enumerator {
	/**
	 * The '<em><b>Is Not Null</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_NOT_NULL
	 * @generated
	 * @ordered
	 */
	IS_NOT_NULL_LITERAL(0, "isNotNull", "isNotNull"),

	/**
	 * The '<em><b>Is Null</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_NULL
	 * @generated
	 * @ordered
	 */
	IS_NULL_LITERAL(1, "isNull", "isNull"),

	/**
	 * The '<em><b>Is One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ONE
	 * @generated
	 * @ordered
	 */
	IS_ONE_LITERAL(2, "isOne", "isOne"),

	/**
	 * The '<em><b>Is Zero</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ZERO
	 * @generated
	 * @ordered
	 */
	IS_ZERO_LITERAL(3, "isZero", "isZero"),

	/**
	 * The '<em><b>Is True</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_TRUE
	 * @generated
	 * @ordered
	 */
	IS_TRUE_LITERAL(4, "isTrue", "isTrue"),

	/**
	 * The '<em><b>Is False</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_FALSE
	 * @generated
	 * @ordered
	 */
	IS_FALSE_LITERAL(5, "isFalse", "isFalse");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Is Not Null</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is not null. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_NOT_NULL_LITERAL
	 * @model name="isNotNull"
	 * @generated
	 * @ordered
	 */
	public static final int IS_NOT_NULL = 0;

	/**
	 * The '<em><b>Is Null</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is null. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_NULL_LITERAL
	 * @model name="isNull"
	 * @generated
	 * @ordered
	 */
	public static final int IS_NULL = 1;

	/**
	 * The '<em><b>Is One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is one. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_ONE_LITERAL
	 * @model name="isOne"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ONE = 2;

	/**
	 * The '<em><b>Is Zero</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is zero. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_ZERO_LITERAL
	 * @model name="isZero"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ZERO = 3;

	/**
	 * The '<em><b>Is True</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is true. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_TRUE_LITERAL
	 * @model name="isTrue"
	 * @generated
	 * @ordered
	 */
	public static final int IS_TRUE = 4;

	/**
	 * The '<em><b>Is False</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This allows a selector attribute to be chosen when the value of the attribute is false. Whether the value or the attribute name is selected is based on ValueSelectionFunction.
	 * <!-- end-model-doc -->
	 * @see #IS_FALSE_LITERAL
	 * @model name="isFalse"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FALSE = 5;

	/**
	 * An array of all the '<em><b>Attribute Selection Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AttributeSelectionFunction[] VALUES_ARRAY =
		new AttributeSelectionFunction[] {
			IS_NOT_NULL_LITERAL,
			IS_NULL_LITERAL,
			IS_ONE_LITERAL,
			IS_ZERO_LITERAL,
			IS_TRUE_LITERAL,
			IS_FALSE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Attribute Selection Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AttributeSelectionFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Attribute Selection Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeSelectionFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeSelectionFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Selection Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeSelectionFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeSelectionFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Selection Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeSelectionFunction get(int value) {
		switch (value) {
			case IS_NOT_NULL: return IS_NOT_NULL_LITERAL;
			case IS_NULL: return IS_NULL_LITERAL;
			case IS_ONE: return IS_ONE_LITERAL;
			case IS_ZERO: return IS_ZERO_LITERAL;
			case IS_TRUE: return IS_TRUE_LITERAL;
			case IS_FALSE: return IS_FALSE_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AttributeSelectionFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AttributeSelectionFunction
