/**
 * INRIA/IRISA
 *
 * $Id: SequenceFunctionSettings.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence Function Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A SequenceFunctionSettings is a subclass of FrequentItemSetFunctionSettings that supports features that are unique to sequence rules algorithms.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings#getWindowSize <em>Window Size</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getSequenceFunctionSettings()
 * @model
 * @generated
 */
public interface SequenceFunctionSettings extends FrequentItemSetFunctionSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Window Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This specifies the size of the window to be considered when executing sequence algorithm in terms of the number of items.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Window Size</em>' attribute.
	 * @see #setWindowSize(long)
	 * @see orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage#getSequenceFunctionSettings_WindowSize()
	 * @model dataType="orgomg.cwm.objectmodel.core.Integer"
	 * @generated
	 */
	long getWindowSize();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.associationrules.SequenceFunctionSettings#getWindowSize <em>Window Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Window Size</em>' attribute.
	 * @see #getWindowSize()
	 * @generated
	 */
	void setWindowSize(long value);

} // SequenceFunctionSettings
