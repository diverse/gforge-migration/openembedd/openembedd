/**
 * INRIA/IRISA
 *
 * $Id: CategoryMapObject.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Map Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The object representation of categorization graph. Each object references a set of CategoryMapObjectEntries
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObject#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObject()
 * @model
 * @generated
 */
public interface CategoryMapObject extends CategoryMap, ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject <em>Map Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMapObject_Entry()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMapObjectEntry#getMapObject
	 * @model opposite="mapObject" containment="true"
	 * @generated
	 */
	EList<CategoryMapObjectEntry> getEntry();

} // CategoryMapObject
