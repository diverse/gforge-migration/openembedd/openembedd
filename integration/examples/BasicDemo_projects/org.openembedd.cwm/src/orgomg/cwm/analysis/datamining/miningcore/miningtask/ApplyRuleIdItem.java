/**
 * INRIA/IRISA
 *
 * $Id: ApplyRuleIdItem.java,v 1.1 2008-04-01 09:35:53 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningtask;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apply Rule Id Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This indicates that the rule ID of the prediction (whose rank is specified here) should appear in the output. This applies only to the models with rule IDs (such as decision tree models).
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage#getApplyRuleIdItem()
 * @model
 * @generated
 */
public interface ApplyRuleIdItem extends ApplyContentItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ApplyRuleIdItem
