/**
 * INRIA/IRISA
 *
 * $Id: PriorProbabilities.java,v 1.1 2008-04-01 09:35:41 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.classification;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prior Probabilities</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a set of prior probabilities of the categories in a mining attribute. Mostly applies to a target MiningAttribute  used for classification.
 * The sum of the probabilities in all priorsEntries must not exceed 1.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage <em>Usage</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getPrior <em>Prior</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilities()
 * @model
 * @generated
 */
public interface PriorProbabilities extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Usage</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors <em>Priors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usage</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usage</em>' container reference.
	 * @see #setUsage(ClassificationAttributeUsage)
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilities_Usage()
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationAttributeUsage#getPriors
	 * @model opposite="priors" required="true"
	 * @generated
	 */
	ClassificationAttributeUsage getUsage();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilities#getUsage <em>Usage</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usage</em>' container reference.
	 * @see #getUsage()
	 * @generated
	 */
	void setUsage(ClassificationAttributeUsage value);

	/**
	 * Returns the value of the '<em><b>Prior</b></em>' containment reference list.
	 * The list contents are of type {@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors <em>Priors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prior</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prior</em>' containment reference list.
	 * @see orgomg.cwm.analysis.datamining.classification.ClassificationPackage#getPriorProbabilities_Prior()
	 * @see orgomg.cwm.analysis.datamining.classification.PriorProbabilitiesEntry#getPriors
	 * @model opposite="priors" containment="true"
	 * @generated
	 */
	EList<PriorProbabilitiesEntry> getPrior();

} // PriorProbabilities
