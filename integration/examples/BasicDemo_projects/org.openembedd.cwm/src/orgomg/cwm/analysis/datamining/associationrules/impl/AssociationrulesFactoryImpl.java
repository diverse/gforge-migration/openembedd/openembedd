/**
 * INRIA/IRISA
 *
 * $Id: AssociationrulesFactoryImpl.java,v 1.1 2008-04-01 09:35:54 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.associationrules.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import orgomg.cwm.analysis.datamining.associationrules.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssociationrulesFactoryImpl extends EFactoryImpl implements AssociationrulesFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AssociationrulesFactory init() {
		try {
			AssociationrulesFactory theAssociationrulesFactory = (AssociationrulesFactory)EPackage.Registry.INSTANCE.getEFactory("http:///org/omg/cwm/analysis/datamining/associationrules.ecore"); 
			if (theAssociationrulesFactory != null) {
				return theAssociationrulesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AssociationrulesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationrulesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AssociationrulesPackage.ASSOCIATION_RULES_FUNCTION_SETTINGS: return createAssociationRulesFunctionSettings();
			case AssociationrulesPackage.FREQUENT_ITEM_SET_FUNCTION_SETTINGS: return createFrequentItemSetFunctionSettings();
			case AssociationrulesPackage.SEQUENCE_FUNCTION_SETTINGS: return createSequenceFunctionSettings();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationRulesFunctionSettings createAssociationRulesFunctionSettings() {
		AssociationRulesFunctionSettingsImpl associationRulesFunctionSettings = new AssociationRulesFunctionSettingsImpl();
		return associationRulesFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FrequentItemSetFunctionSettings createFrequentItemSetFunctionSettings() {
		FrequentItemSetFunctionSettingsImpl frequentItemSetFunctionSettings = new FrequentItemSetFunctionSettingsImpl();
		return frequentItemSetFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceFunctionSettings createSequenceFunctionSettings() {
		SequenceFunctionSettingsImpl sequenceFunctionSettings = new SequenceFunctionSettingsImpl();
		return sequenceFunctionSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationrulesPackage getAssociationrulesPackage() {
		return (AssociationrulesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AssociationrulesPackage getPackage() {
		return AssociationrulesPackage.eINSTANCE;
	}

} //AssociationrulesFactoryImpl
