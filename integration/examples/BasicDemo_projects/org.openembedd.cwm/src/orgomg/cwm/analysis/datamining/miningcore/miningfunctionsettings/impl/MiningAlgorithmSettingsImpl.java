/**
 * INRIA/IRISA
 *
 * $Id: MiningAlgorithmSettingsImpl.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningAlgorithmSettings;
import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mining Algorithm Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class MiningAlgorithmSettingsImpl extends ModelElementImpl implements MiningAlgorithmSettings {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiningAlgorithmSettingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiningfunctionsettingsPackage.Literals.MINING_ALGORITHM_SETTINGS;
	}

} //MiningAlgorithmSettingsImpl
