/**
 * INRIA/IRISA
 *
 * $Id: MatrixProperty.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Matrix Property</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This specifies the matrix property that describes the shape of the matrix.
 * <!-- end-model-doc -->
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getMatrixProperty()
 * @model
 * @generated
 */
public enum MatrixProperty implements Enumerator {
	/**
	 * The '<em><b>Mp symmetric</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MP_SYMMETRIC
	 * @generated
	 * @ordered
	 */
	MP_SYMMETRIC_LITERAL(0, "mp_symmetric", "mp_symmetric"),

	/**
	 * The '<em><b>Mp diagonal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MP_DIAGONAL
	 * @generated
	 * @ordered
	 */
	MP_DIAGONAL_LITERAL(1, "mp_diagonal", "mp_diagonal"),

	/**
	 * The '<em><b>Mp any</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MP_ANY
	 * @generated
	 * @ordered
	 */
	MP_ANY_LITERAL(2, "mp_any", "mp_any");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Mp symmetric</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The property that the matrix values for a rowId and a columnID are identical to those with exchanged id's.
	 * <!-- end-model-doc -->
	 * @see #MP_SYMMETRIC_LITERAL
	 * @model name="mp_symmetric"
	 * @generated
	 * @ordered
	 */
	public static final int MP_SYMMETRIC = 0;

	/**
	 * The '<em><b>Mp diagonal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The property that all matrix values where rowID is different from ColumnID are 0.0
	 * <!-- end-model-doc -->
	 * @see #MP_DIAGONAL_LITERAL
	 * @model name="mp_diagonal"
	 * @generated
	 * @ordered
	 */
	public static final int MP_DIAGONAL = 1;

	/**
	 * The '<em><b>Mp any</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * An arbitrary matrix, not necessarily diagonal or symmetric.
	 * <!-- end-model-doc -->
	 * @see #MP_ANY_LITERAL
	 * @model name="mp_any"
	 * @generated
	 * @ordered
	 */
	public static final int MP_ANY = 2;

	/**
	 * An array of all the '<em><b>Matrix Property</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MatrixProperty[] VALUES_ARRAY =
		new MatrixProperty[] {
			MP_SYMMETRIC_LITERAL,
			MP_DIAGONAL_LITERAL,
			MP_ANY_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Matrix Property</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MatrixProperty> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Matrix Property</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MatrixProperty get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MatrixProperty result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Matrix Property</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MatrixProperty getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MatrixProperty result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Matrix Property</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MatrixProperty get(int value) {
		switch (value) {
			case MP_SYMMETRIC: return MP_SYMMETRIC_LITERAL;
			case MP_DIAGONAL: return MP_DIAGONAL_LITERAL;
			case MP_ANY: return MP_ANY_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MatrixProperty(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MatrixProperty
