/**
 * INRIA/IRISA
 *
 * $Id: CategoryMatrixEntry.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Matrix Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Defines the value of a single cell in a CategoryMatrix.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getValue <em>Value</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getColumnIndex <em>Column Index</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix <em>Category Matrix</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getRowIndex <em>Row Index</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixEntry()
 * @model
 * @generated
 */
public interface CategoryMatrixEntry extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Value of a cell. It overwrites any default value in CategoryMatrix.
	 * For Cost matrix, value is intended to be a double. For ConfusionMatrix, the value can be either a "count" which is an integer value, or a "percentage" which is a double value. This is up to the implementation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixEntry_Value()
	 * @model dataType="orgomg.cwm.analysis.datamining.miningcore.miningdata.Double"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Column Index</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column Index</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column Index</em>' reference.
	 * @see #setColumnIndex(Category)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixEntry_ColumnIndex()
	 * @model required="true"
	 * @generated
	 */
	Category getColumnIndex();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getColumnIndex <em>Column Index</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column Index</em>' reference.
	 * @see #getColumnIndex()
	 * @generated
	 */
	void setColumnIndex(Category value);

	/**
	 * Returns the value of the '<em><b>Category Matrix</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category Matrix</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category Matrix</em>' container reference.
	 * @see #setCategoryMatrix(CategoryMatrixObject)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixEntry_CategoryMatrix()
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixObject#getEntry
	 * @model opposite="entry" required="true"
	 * @generated
	 */
	CategoryMatrixObject getCategoryMatrix();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getCategoryMatrix <em>Category Matrix</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category Matrix</em>' container reference.
	 * @see #getCategoryMatrix()
	 * @generated
	 */
	void setCategoryMatrix(CategoryMatrixObject value);

	/**
	 * Returns the value of the '<em><b>Row Index</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row Index</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row Index</em>' reference.
	 * @see #setRowIndex(Category)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getCategoryMatrixEntry_RowIndex()
	 * @model required="true"
	 * @generated
	 */
	Category getRowIndex();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.CategoryMatrixEntry#getRowIndex <em>Row Index</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row Index</em>' reference.
	 * @see #getRowIndex()
	 * @generated
	 */
	void setRowIndex(Category value);

} // CategoryMatrixEntry
