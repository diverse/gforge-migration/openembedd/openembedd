/**
 * INRIA/IRISA
 *
 * $Id: SignatureAttribute.java,v 1.1 2008-04-01 09:35:33 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningmodel;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningAttribute;
import orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A SignatureAttribute object describes the input expected to a model. This is automatically produced as part of the model. It indicates not only the basic Attribute properties, but also how outliers and missing values were handled for model build. This is potentially duplicate information from the MiningFunctionSettings, but must be provided since MiningFunctionSettings are optional.
 * 
 * If an attribute was normalized or discretized automatically by the Data Mining System, the specific details are provided in the SignatureAttribute object. The user is not expected to use this information to preprocess the data in any way. The Data Mining System uses this information to automatically preprocess data, if required.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.SignatureAttribute#getUsageOption <em>Usage Option</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getSignatureAttribute()
 * @model
 * @generated
 */
public interface SignatureAttribute extends MiningAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Usage Option</b></em>' attribute.
	 * The literals are from the enumeration {@link orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The usage intended for this attribute. A model signature consists only of 'active' and 'supplemental' attributes. 'Inactive' attributes are filtered out as they do not contribute to the model.
	 * 
	 * Note that 'supplemental' attributes do not contribute to model apply.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Usage Option</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @see #setUsageOption(UsageOption)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage#getSignatureAttribute_UsageOption()
	 * @model
	 * @generated
	 */
	UsageOption getUsageOption();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningmodel.SignatureAttribute#getUsageOption <em>Usage Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usage Option</em>' attribute.
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.UsageOption
	 * @see #getUsageOption()
	 * @generated
	 */
	void setUsageOption(UsageOption value);

} // SignatureAttribute
