/**
 * INRIA/IRISA
 *
 * $Id: PivotAttributeAssignment.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.miningcore.miningdata;

import orgomg.cwm.objectmodel.core.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pivot Attribute Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This object provides a mapping where the input data is in transactional format. Each of the logical attributes ocurring in a pivoted table is mapped to three physical columns, presumably the same ones every time. If the data types don't match, the value column may be different in that case.
 * Question: Seems that we need to have more than one logical attribute associated with this assignment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getNameAttribute <em>Name Attribute</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getValueAttribute <em>Value Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getPivotAttributeAssignment()
 * @model
 * @generated
 */
public interface PivotAttributeAssignment extends AttributeAssignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Set Id Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Id Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Id Attribute</em>' reference.
	 * @see #setSetIdAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getPivotAttributeAssignment_SetIdAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getSetIdAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getSetIdAttribute <em>Set Id Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Set Id Attribute</em>' reference.
	 * @see #getSetIdAttribute()
	 * @generated
	 */
	void setSetIdAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Name Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Attribute</em>' reference.
	 * @see #setNameAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getPivotAttributeAssignment_NameAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getNameAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getNameAttribute <em>Name Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Attribute</em>' reference.
	 * @see #getNameAttribute()
	 * @generated
	 */
	void setNameAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' reference.
	 * @see #setValueAttribute(Attribute)
	 * @see orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage#getPivotAttributeAssignment_ValueAttribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getValueAttribute();

	/**
	 * Sets the value of the '{@link orgomg.cwm.analysis.datamining.miningcore.miningdata.PivotAttributeAssignment#getValueAttribute <em>Value Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Attribute</em>' reference.
	 * @see #getValueAttribute()
	 * @generated
	 */
	void setValueAttribute(Attribute value);

} // PivotAttributeAssignment
