/**
 * INRIA/IRISA
 *
 * $Id: LiftAnalysisPointImpl.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.analysis.datamining.supervised.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import orgomg.cwm.analysis.datamining.supervised.LiftAnalysis;
import orgomg.cwm.analysis.datamining.supervised.LiftAnalysisPoint;
import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.objectmodel.core.impl.ModelElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lift Analysis Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.LiftAnalysisPointImpl#getSubsetOfRecords <em>Subset Of Records</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.LiftAnalysisPointImpl#getAggregateTarget <em>Aggregate Target</em>}</li>
 *   <li>{@link orgomg.cwm.analysis.datamining.supervised.impl.LiftAnalysisPointImpl#getLiftAnalysis <em>Lift Analysis</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LiftAnalysisPointImpl extends ModelElementImpl implements LiftAnalysisPoint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The default value of the '{@link #getSubsetOfRecords() <em>Subset Of Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsetOfRecords()
	 * @generated
	 * @ordered
	 */
	protected static final long SUBSET_OF_RECORDS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSubsetOfRecords() <em>Subset Of Records</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsetOfRecords()
	 * @generated
	 * @ordered
	 */
	protected long subsetOfRecords = SUBSET_OF_RECORDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getAggregateTarget() <em>Aggregate Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregateTarget()
	 * @generated
	 * @ordered
	 */
	protected static final String AGGREGATE_TARGET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAggregateTarget() <em>Aggregate Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregateTarget()
	 * @generated
	 * @ordered
	 */
	protected String aggregateTarget = AGGREGATE_TARGET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LiftAnalysisPointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SupervisedPackage.Literals.LIFT_ANALYSIS_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSubsetOfRecords() {
		return subsetOfRecords;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubsetOfRecords(long newSubsetOfRecords) {
		long oldSubsetOfRecords = subsetOfRecords;
		subsetOfRecords = newSubsetOfRecords;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.LIFT_ANALYSIS_POINT__SUBSET_OF_RECORDS, oldSubsetOfRecords, subsetOfRecords));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAggregateTarget() {
		return aggregateTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAggregateTarget(String newAggregateTarget) {
		String oldAggregateTarget = aggregateTarget;
		aggregateTarget = newAggregateTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.LIFT_ANALYSIS_POINT__AGGREGATE_TARGET, oldAggregateTarget, aggregateTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiftAnalysis getLiftAnalysis() {
		if (eContainerFeatureID != SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS) return null;
		return (LiftAnalysis)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLiftAnalysis(LiftAnalysis newLiftAnalysis, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLiftAnalysis, SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiftAnalysis(LiftAnalysis newLiftAnalysis) {
		if (newLiftAnalysis != eInternalContainer() || (eContainerFeatureID != SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS && newLiftAnalysis != null)) {
			if (EcoreUtil.isAncestor(this, newLiftAnalysis))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLiftAnalysis != null)
				msgs = ((InternalEObject)newLiftAnalysis).eInverseAdd(this, SupervisedPackage.LIFT_ANALYSIS__POINT, LiftAnalysis.class, msgs);
			msgs = basicSetLiftAnalysis(newLiftAnalysis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS, newLiftAnalysis, newLiftAnalysis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLiftAnalysis((LiftAnalysis)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				return basicSetLiftAnalysis(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				return eInternalContainer().eInverseRemove(this, SupervisedPackage.LIFT_ANALYSIS__POINT, LiftAnalysis.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__SUBSET_OF_RECORDS:
				return new Long(getSubsetOfRecords());
			case SupervisedPackage.LIFT_ANALYSIS_POINT__AGGREGATE_TARGET:
				return getAggregateTarget();
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				return getLiftAnalysis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__SUBSET_OF_RECORDS:
				setSubsetOfRecords(((Long)newValue).longValue());
				return;
			case SupervisedPackage.LIFT_ANALYSIS_POINT__AGGREGATE_TARGET:
				setAggregateTarget((String)newValue);
				return;
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				setLiftAnalysis((LiftAnalysis)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__SUBSET_OF_RECORDS:
				setSubsetOfRecords(SUBSET_OF_RECORDS_EDEFAULT);
				return;
			case SupervisedPackage.LIFT_ANALYSIS_POINT__AGGREGATE_TARGET:
				setAggregateTarget(AGGREGATE_TARGET_EDEFAULT);
				return;
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				setLiftAnalysis((LiftAnalysis)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SupervisedPackage.LIFT_ANALYSIS_POINT__SUBSET_OF_RECORDS:
				return subsetOfRecords != SUBSET_OF_RECORDS_EDEFAULT;
			case SupervisedPackage.LIFT_ANALYSIS_POINT__AGGREGATE_TARGET:
				return AGGREGATE_TARGET_EDEFAULT == null ? aggregateTarget != null : !AGGREGATE_TARGET_EDEFAULT.equals(aggregateTarget);
			case SupervisedPackage.LIFT_ANALYSIS_POINT__LIFT_ANALYSIS:
				return getLiftAnalysis() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (subsetOfRecords: ");
		result.append(subsetOfRecords);
		result.append(", aggregateTarget: ");
		result.append(aggregateTarget);
		result.append(')');
		return result.toString();
	}

} //LiftAnalysisPointImpl
