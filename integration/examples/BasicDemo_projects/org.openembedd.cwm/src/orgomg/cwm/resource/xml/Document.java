/**
 * INRIA/IRISA
 *
 * $Id: Document.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.resource.xml;

import orgomg.cwm.objectmodel.instance.Extent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents an XML document, which is a collection of XML Elements.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.xml.XmlPackage#getDocument()
 * @model
 * @generated
 */
public interface Document extends Extent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Document
