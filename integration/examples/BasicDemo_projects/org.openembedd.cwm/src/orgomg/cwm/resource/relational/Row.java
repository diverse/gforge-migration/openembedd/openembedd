/**
 * INRIA/IRISA
 *
 * $Id: Row.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An instance of a ColumnSet.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getRow()
 * @model
 * @generated
 */
public interface Row extends orgomg.cwm.objectmodel.instance.Object {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Row
