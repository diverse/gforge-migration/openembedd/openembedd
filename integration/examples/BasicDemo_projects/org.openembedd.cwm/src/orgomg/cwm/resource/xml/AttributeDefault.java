/**
 * INRIA/IRISA
 *
 * $Id: AttributeDefault.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.resource.xml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Attribute Default</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * required, implied, default, fixed
 * <!-- end-model-doc -->
 * @see orgomg.cwm.resource.xml.XmlPackage#getAttributeDefault()
 * @model
 * @generated
 */
public enum AttributeDefault implements Enumerator {
	/**
	 * The '<em><b>Xml required</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XML_REQUIRED
	 * @generated
	 * @ordered
	 */
	XML_REQUIRED_LITERAL(0, "xml_required", "xml_required"),

	/**
	 * The '<em><b>Xml implied</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XML_IMPLIED
	 * @generated
	 * @ordered
	 */
	XML_IMPLIED_LITERAL(1, "xml_implied", "xml_implied"),

	/**
	 * The '<em><b>Xml default</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XML_DEFAULT
	 * @generated
	 * @ordered
	 */
	XML_DEFAULT_LITERAL(2, "xml_default", "xml_default"),

	/**
	 * The '<em><b>Xml fixed</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XML_FIXED
	 * @generated
	 * @ordered
	 */
	XML_FIXED_LITERAL(3, "xml_fixed", "xml_fixed");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Xml required</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Xml required</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XML_REQUIRED_LITERAL
	 * @model name="xml_required"
	 * @generated
	 * @ordered
	 */
	public static final int XML_REQUIRED = 0;

	/**
	 * The '<em><b>Xml implied</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Xml implied</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XML_IMPLIED_LITERAL
	 * @model name="xml_implied"
	 * @generated
	 * @ordered
	 */
	public static final int XML_IMPLIED = 1;

	/**
	 * The '<em><b>Xml default</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Xml default</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XML_DEFAULT_LITERAL
	 * @model name="xml_default"
	 * @generated
	 * @ordered
	 */
	public static final int XML_DEFAULT = 2;

	/**
	 * The '<em><b>Xml fixed</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Xml fixed</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XML_FIXED_LITERAL
	 * @model name="xml_fixed"
	 * @generated
	 * @ordered
	 */
	public static final int XML_FIXED = 3;

	/**
	 * An array of all the '<em><b>Attribute Default</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AttributeDefault[] VALUES_ARRAY =
		new AttributeDefault[] {
			XML_REQUIRED_LITERAL,
			XML_IMPLIED_LITERAL,
			XML_DEFAULT_LITERAL,
			XML_FIXED_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Attribute Default</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AttributeDefault> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Attribute Default</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeDefault get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeDefault result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Default</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeDefault getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AttributeDefault result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Attribute Default</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AttributeDefault get(int value) {
		switch (value) {
			case XML_REQUIRED: return XML_REQUIRED_LITERAL;
			case XML_IMPLIED: return XML_IMPLIED_LITERAL;
			case XML_DEFAULT: return XML_DEFAULT_LITERAL;
			case XML_FIXED: return XML_FIXED_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AttributeDefault(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AttributeDefault
