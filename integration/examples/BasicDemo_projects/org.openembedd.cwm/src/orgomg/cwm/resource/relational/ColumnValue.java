/**
 * INRIA/IRISA
 *
 * $Id: ColumnValue.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;

import orgomg.cwm.objectmodel.instance.DataValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The value in a column instance.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getColumnValue()
 * @model
 * @generated
 */
public interface ColumnValue extends DataValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ColumnValue
