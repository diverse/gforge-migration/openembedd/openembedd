/**
 * <copyright>
 * </copyright>
 *
 * $Id: NamedColumnSetValidator.java,v 1.1 2008-04-01 09:35:22 vmahe Exp $
 */
package orgomg.cwm.resource.relational.validation;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.resource.relational.Column;
import orgomg.cwm.resource.relational.SQLStructuredType;
import orgomg.cwm.resource.relational.Trigger;

/**
 * A sample validator interface for {@link orgomg.cwm.resource.relational.NamedColumnSet}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface NamedColumnSetValidator {
	boolean validate();

	boolean validateUsingTrigger(EList<Trigger> value);
	boolean validateType(SQLStructuredType value);
	boolean validateOptionScopeColumn(EList<Column> value);
}
