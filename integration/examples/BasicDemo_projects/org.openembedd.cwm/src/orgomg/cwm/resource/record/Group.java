/**
 * INRIA/IRISA
 *
 * $Id: Group.java,v 1.1 2008-04-01 09:35:23 vmahe Exp $
 */
package orgomg.cwm.resource.record;

import orgomg.cwm.objectmodel.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Group is a structured data type and is used to collect together Field instances within a Record. Groups can be used in RecordDef instances as shown in the foregoing example.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.record.RecordPackage#getGroup()
 * @model
 * @generated
 */
public interface Group extends Classifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Group
