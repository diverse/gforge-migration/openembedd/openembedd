/**
 * INRIA/IRISA
 *
 * $Id: Element.java,v 1.1 2008-04-01 09:35:44 vmahe Exp $
 */
package orgomg.cwm.resource.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents an instance of an ElementType.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.xml.XmlPackage#getElement()
 * @model
 * @generated
 */
public interface Element extends orgomg.cwm.objectmodel.instance.Object {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Element
