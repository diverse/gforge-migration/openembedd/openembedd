/**
 * INRIA/IRISA
 *
 * $Id: RecordImpl.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.resource.record.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.instance.impl.ObjectImpl;

import orgomg.cwm.resource.record.Record;
import orgomg.cwm.resource.record.RecordPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Record</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class RecordImpl extends ObjectImpl implements Record {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RecordPackage.Literals.RECORD;
	}

} //RecordImpl
