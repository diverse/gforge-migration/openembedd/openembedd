/**
 * <copyright>
 * </copyright>
 *
 * $Id: ElementTypeValidator.java,v 1.1 2008-04-01 09:35:26 vmahe Exp $
 */
package orgomg.cwm.resource.xml.validation;

import orgomg.cwm.resource.xml.Content;

/**
 * A sample validator interface for {@link orgomg.cwm.resource.xml.ElementType}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ElementTypeValidator {
	boolean validate();

	boolean validateContent(Content value);
}
