/**
 * INRIA/IRISA
 *
 * $Id: RecordSetImpl.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.resource.record.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.instance.impl.ExtentImpl;

import orgomg.cwm.resource.record.RecordPackage;
import orgomg.cwm.resource.record.RecordSet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class RecordSetImpl extends ExtentImpl implements RecordSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecordSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RecordPackage.Literals.RECORD_SET;
	}

} //RecordSetImpl
