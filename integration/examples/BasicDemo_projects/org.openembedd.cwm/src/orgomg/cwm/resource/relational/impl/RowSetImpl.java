/**
 * INRIA/IRISA
 *
 * $Id: RowSetImpl.java,v 1.1 2008-04-01 09:35:25 vmahe Exp $
 */
package orgomg.cwm.resource.relational.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.instance.impl.ExtentImpl;

import orgomg.cwm.resource.relational.RelationalPackage;
import orgomg.cwm.resource.relational.RowSet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Row Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class RowSetImpl extends ExtentImpl implements RowSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RowSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RelationalPackage.Literals.ROW_SET;
	}

} //RowSetImpl
