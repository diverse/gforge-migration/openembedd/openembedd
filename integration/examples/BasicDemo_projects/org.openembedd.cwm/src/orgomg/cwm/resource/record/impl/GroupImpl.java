/**
 * INRIA/IRISA
 *
 * $Id: GroupImpl.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.resource.record.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.core.impl.ClassifierImpl;

import orgomg.cwm.resource.record.Group;
import orgomg.cwm.resource.record.RecordPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class GroupImpl extends ClassifierImpl implements Group {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RecordPackage.Literals.GROUP;
	}

} //GroupImpl
