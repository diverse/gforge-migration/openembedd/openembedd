/**
 * INRIA/IRISA
 *
 * $Id: EnumerationsPackageImpl.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.resource.relational.enumerations.impl;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import orgomg.cwm.analysis.businessnomenclature.BusinessnomenclaturePackage;

import orgomg.cwm.analysis.businessnomenclature.impl.BusinessnomenclaturePackageImpl;

import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;

import orgomg.cwm.analysis.datamining.approximation.impl.ApproximationPackageImpl;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;

import orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage;

import orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;

import orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl;

import orgomg.cwm.analysis.datamining.clustering.ClusteringPackage;

import orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.EntrypointPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningfunctionsettingsPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.MiningmodelPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningresultPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.impl.MiningresultPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedPackageImpl;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage;

import orgomg.cwm.analysis.informationvisualization.impl.InformationvisualizationPackageImpl;

import orgomg.cwm.analysis.olap.OlapPackage;

import orgomg.cwm.analysis.olap.impl.OlapPackageImpl;

import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.analysis.transformation.impl.TransformationPackageImpl;

import orgomg.cwm.foundation.businessinformation.BusinessinformationPackage;

import orgomg.cwm.foundation.businessinformation.impl.BusinessinformationPackageImpl;

import orgomg.cwm.foundation.datatypes.DatatypesPackage;

import orgomg.cwm.foundation.datatypes.impl.DatatypesPackageImpl;

import orgomg.cwm.foundation.expressions.ExpressionsPackage;

import orgomg.cwm.foundation.expressions.impl.ExpressionsPackageImpl;

import orgomg.cwm.foundation.keysindexes.KeysindexesPackage;

import orgomg.cwm.foundation.keysindexes.impl.KeysindexesPackageImpl;

import orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage;

import orgomg.cwm.foundation.softwaredeployment.impl.SoftwaredeploymentPackageImpl;

import orgomg.cwm.foundation.typemapping.TypemappingPackage;

import orgomg.cwm.foundation.typemapping.impl.TypemappingPackageImpl;

import orgomg.cwm.management.warehouseoperation.WarehouseoperationPackage;

import orgomg.cwm.management.warehouseoperation.impl.WarehouseoperationPackageImpl;

import orgomg.cwm.management.warehouseprocess.WarehouseprocessPackage;

import orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage;

import orgomg.cwm.management.warehouseprocess.datatype.impl.DatatypePackageImpl;

import orgomg.cwm.management.warehouseprocess.events.EventsPackage;

import orgomg.cwm.management.warehouseprocess.events.impl.EventsPackageImpl;

import orgomg.cwm.management.warehouseprocess.impl.WarehouseprocessPackageImpl;

import orgomg.cwm.objectmodel.behavioral.BehavioralPackage;

import orgomg.cwm.objectmodel.behavioral.impl.BehavioralPackageImpl;

import orgomg.cwm.objectmodel.core.CorePackage;

import orgomg.cwm.objectmodel.core.impl.CorePackageImpl;

import orgomg.cwm.objectmodel.instance.InstancePackage;

import orgomg.cwm.objectmodel.instance.impl.InstancePackageImpl;

import orgomg.cwm.objectmodel.relationships.RelationshipsPackage;

import orgomg.cwm.objectmodel.relationships.impl.RelationshipsPackageImpl;

import orgomg.cwm.resource.multidimensional.MultidimensionalPackage;

import orgomg.cwm.resource.multidimensional.impl.MultidimensionalPackageImpl;

import orgomg.cwm.resource.record.RecordPackage;

import orgomg.cwm.resource.record.impl.RecordPackageImpl;

import orgomg.cwm.resource.relational.RelationalPackage;

import orgomg.cwm.resource.relational.enumerations.ActionOrientationType;
import orgomg.cwm.resource.relational.enumerations.ConditionTimingType;
import orgomg.cwm.resource.relational.enumerations.DeferrabilityType;
import orgomg.cwm.resource.relational.enumerations.EnumerationsFactory;
import orgomg.cwm.resource.relational.enumerations.EnumerationsPackage;
import orgomg.cwm.resource.relational.enumerations.EventManipulationType;
import orgomg.cwm.resource.relational.enumerations.NullableType;
import orgomg.cwm.resource.relational.enumerations.ProcedureType;
import orgomg.cwm.resource.relational.enumerations.ReferentialRuleType;

import orgomg.cwm.resource.relational.impl.RelationalPackageImpl;

import orgomg.cwm.resource.xml.XmlPackage;

import orgomg.cwm.resource.xml.impl.XmlPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EnumerationsPackageImpl extends EPackageImpl implements EnumerationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum actionOrientationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum conditionTimingTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum deferrabilityTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eventManipulationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum nullableTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum procedureTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum referentialRuleTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see orgomg.cwm.resource.relational.enumerations.EnumerationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EnumerationsPackageImpl() {
		super(eNS_URI, EnumerationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EnumerationsPackage init() {
		if (isInited) return (EnumerationsPackage)EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI);

		// Obtain or create and register package
		EnumerationsPackageImpl theEnumerationsPackage = (EnumerationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof EnumerationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new EnumerationsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		BehavioralPackageImpl theBehavioralPackage = (BehavioralPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) instanceof BehavioralPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) : BehavioralPackage.eINSTANCE);
		RelationshipsPackageImpl theRelationshipsPackage = (RelationshipsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) instanceof RelationshipsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) : RelationshipsPackage.eINSTANCE);
		InstancePackageImpl theInstancePackage = (InstancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) instanceof InstancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) : InstancePackage.eINSTANCE);
		BusinessinformationPackageImpl theBusinessinformationPackage = (BusinessinformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) instanceof BusinessinformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) : BusinessinformationPackage.eINSTANCE);
		DatatypesPackageImpl theDatatypesPackage = (DatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) instanceof DatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) : DatatypesPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		KeysindexesPackageImpl theKeysindexesPackage = (KeysindexesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) instanceof KeysindexesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) : KeysindexesPackage.eINSTANCE);
		SoftwaredeploymentPackageImpl theSoftwaredeploymentPackage = (SoftwaredeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) instanceof SoftwaredeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) : SoftwaredeploymentPackage.eINSTANCE);
		TypemappingPackageImpl theTypemappingPackage = (TypemappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) instanceof TypemappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) : TypemappingPackage.eINSTANCE);
		RelationalPackageImpl theRelationalPackage = (RelationalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) instanceof RelationalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) : RelationalPackage.eINSTANCE);
		RecordPackageImpl theRecordPackage = (RecordPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) instanceof RecordPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) : RecordPackage.eINSTANCE);
		MultidimensionalPackageImpl theMultidimensionalPackage = (MultidimensionalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) instanceof MultidimensionalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) : MultidimensionalPackage.eINSTANCE);
		XmlPackageImpl theXmlPackage = (XmlPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) instanceof XmlPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) : XmlPackage.eINSTANCE);
		TransformationPackageImpl theTransformationPackage = (TransformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) instanceof TransformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) : TransformationPackage.eINSTANCE);
		OlapPackageImpl theOlapPackage = (OlapPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) instanceof OlapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) : OlapPackage.eINSTANCE);
		MiningdataPackageImpl theMiningdataPackage = (MiningdataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) instanceof MiningdataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) : MiningdataPackage.eINSTANCE);
		MiningfunctionsettingsPackageImpl theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) instanceof MiningfunctionsettingsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) : MiningfunctionsettingsPackage.eINSTANCE);
		MiningmodelPackageImpl theMiningmodelPackage = (MiningmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) instanceof MiningmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) : MiningmodelPackage.eINSTANCE);
		MiningresultPackageImpl theMiningresultPackage = (MiningresultPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) instanceof MiningresultPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) : MiningresultPackage.eINSTANCE);
		MiningtaskPackageImpl theMiningtaskPackage = (MiningtaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) instanceof MiningtaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) : MiningtaskPackage.eINSTANCE);
		EntrypointPackageImpl theEntrypointPackage = (EntrypointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) instanceof EntrypointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) : EntrypointPackage.eINSTANCE);
		ClusteringPackageImpl theClusteringPackage = (ClusteringPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) instanceof ClusteringPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) : ClusteringPackage.eINSTANCE);
		AssociationrulesPackageImpl theAssociationrulesPackage = (AssociationrulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) instanceof AssociationrulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) : AssociationrulesPackage.eINSTANCE);
		SupervisedPackageImpl theSupervisedPackage = (SupervisedPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) instanceof SupervisedPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) : SupervisedPackage.eINSTANCE);
		ClassificationPackageImpl theClassificationPackage = (ClassificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) instanceof ClassificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) : ClassificationPackage.eINSTANCE);
		ApproximationPackageImpl theApproximationPackage = (ApproximationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) instanceof ApproximationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) : ApproximationPackage.eINSTANCE);
		AttributeimportancePackageImpl theAttributeimportancePackage = (AttributeimportancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) instanceof AttributeimportancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) : AttributeimportancePackage.eINSTANCE);
		InformationvisualizationPackageImpl theInformationvisualizationPackage = (InformationvisualizationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) instanceof InformationvisualizationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) : InformationvisualizationPackage.eINSTANCE);
		BusinessnomenclaturePackageImpl theBusinessnomenclaturePackage = (BusinessnomenclaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) instanceof BusinessnomenclaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) : BusinessnomenclaturePackage.eINSTANCE);
		WarehouseprocessPackageImpl theWarehouseprocessPackage = (WarehouseprocessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) instanceof WarehouseprocessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) : WarehouseprocessPackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		EventsPackageImpl theEventsPackage = (EventsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) instanceof EventsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI) : EventsPackage.eINSTANCE);
		WarehouseoperationPackageImpl theWarehouseoperationPackage = (WarehouseoperationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) instanceof WarehouseoperationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) : WarehouseoperationPackage.eINSTANCE);

		// Create package meta-data objects
		theEnumerationsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theBehavioralPackage.createPackageContents();
		theRelationshipsPackage.createPackageContents();
		theInstancePackage.createPackageContents();
		theBusinessinformationPackage.createPackageContents();
		theDatatypesPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theKeysindexesPackage.createPackageContents();
		theSoftwaredeploymentPackage.createPackageContents();
		theTypemappingPackage.createPackageContents();
		theRelationalPackage.createPackageContents();
		theRecordPackage.createPackageContents();
		theMultidimensionalPackage.createPackageContents();
		theXmlPackage.createPackageContents();
		theTransformationPackage.createPackageContents();
		theOlapPackage.createPackageContents();
		theMiningdataPackage.createPackageContents();
		theMiningfunctionsettingsPackage.createPackageContents();
		theMiningmodelPackage.createPackageContents();
		theMiningresultPackage.createPackageContents();
		theMiningtaskPackage.createPackageContents();
		theEntrypointPackage.createPackageContents();
		theClusteringPackage.createPackageContents();
		theAssociationrulesPackage.createPackageContents();
		theSupervisedPackage.createPackageContents();
		theClassificationPackage.createPackageContents();
		theApproximationPackage.createPackageContents();
		theAttributeimportancePackage.createPackageContents();
		theInformationvisualizationPackage.createPackageContents();
		theBusinessnomenclaturePackage.createPackageContents();
		theWarehouseprocessPackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theEventsPackage.createPackageContents();
		theWarehouseoperationPackage.createPackageContents();

		// Initialize created meta-data
		theEnumerationsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theBehavioralPackage.initializePackageContents();
		theRelationshipsPackage.initializePackageContents();
		theInstancePackage.initializePackageContents();
		theBusinessinformationPackage.initializePackageContents();
		theDatatypesPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theKeysindexesPackage.initializePackageContents();
		theSoftwaredeploymentPackage.initializePackageContents();
		theTypemappingPackage.initializePackageContents();
		theRelationalPackage.initializePackageContents();
		theRecordPackage.initializePackageContents();
		theMultidimensionalPackage.initializePackageContents();
		theXmlPackage.initializePackageContents();
		theTransformationPackage.initializePackageContents();
		theOlapPackage.initializePackageContents();
		theMiningdataPackage.initializePackageContents();
		theMiningfunctionsettingsPackage.initializePackageContents();
		theMiningmodelPackage.initializePackageContents();
		theMiningresultPackage.initializePackageContents();
		theMiningtaskPackage.initializePackageContents();
		theEntrypointPackage.initializePackageContents();
		theClusteringPackage.initializePackageContents();
		theAssociationrulesPackage.initializePackageContents();
		theSupervisedPackage.initializePackageContents();
		theClassificationPackage.initializePackageContents();
		theApproximationPackage.initializePackageContents();
		theAttributeimportancePackage.initializePackageContents();
		theInformationvisualizationPackage.initializePackageContents();
		theBusinessnomenclaturePackage.initializePackageContents();
		theWarehouseprocessPackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theEventsPackage.initializePackageContents();
		theWarehouseoperationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEnumerationsPackage.freeze();

		return theEnumerationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActionOrientationType() {
		return actionOrientationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConditionTimingType() {
		return conditionTimingTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDeferrabilityType() {
		return deferrabilityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEventManipulationType() {
		return eventManipulationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNullableType() {
		return nullableTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getProcedureType() {
		return procedureTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReferentialRuleType() {
		return referentialRuleTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationsFactory getEnumerationsFactory() {
		return (EnumerationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create enums
		actionOrientationTypeEEnum = createEEnum(ACTION_ORIENTATION_TYPE);
		conditionTimingTypeEEnum = createEEnum(CONDITION_TIMING_TYPE);
		deferrabilityTypeEEnum = createEEnum(DEFERRABILITY_TYPE);
		eventManipulationTypeEEnum = createEEnum(EVENT_MANIPULATION_TYPE);
		nullableTypeEEnum = createEEnum(NULLABLE_TYPE);
		procedureTypeEEnum = createEEnum(PROCEDURE_TYPE);
		referentialRuleTypeEEnum = createEEnum(REFERENTIAL_RULE_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Initialize enums and add enum literals
		initEEnum(actionOrientationTypeEEnum, ActionOrientationType.class, "ActionOrientationType");
		addEEnumLiteral(actionOrientationTypeEEnum, ActionOrientationType.ROW_LITERAL);
		addEEnumLiteral(actionOrientationTypeEEnum, ActionOrientationType.STATEMENT_LITERAL);

		initEEnum(conditionTimingTypeEEnum, ConditionTimingType.class, "ConditionTimingType");
		addEEnumLiteral(conditionTimingTypeEEnum, ConditionTimingType.BEFORE_LITERAL);
		addEEnumLiteral(conditionTimingTypeEEnum, ConditionTimingType.AFTER_LITERAL);

		initEEnum(deferrabilityTypeEEnum, DeferrabilityType.class, "DeferrabilityType");
		addEEnumLiteral(deferrabilityTypeEEnum, DeferrabilityType.INITIALLY_DEFERRED_LITERAL);
		addEEnumLiteral(deferrabilityTypeEEnum, DeferrabilityType.INITIALLY_IMMEDIATE_LITERAL);
		addEEnumLiteral(deferrabilityTypeEEnum, DeferrabilityType.NOT_DEFERRABLE_LITERAL);

		initEEnum(eventManipulationTypeEEnum, EventManipulationType.class, "EventManipulationType");
		addEEnumLiteral(eventManipulationTypeEEnum, EventManipulationType.INSERT_LITERAL);
		addEEnumLiteral(eventManipulationTypeEEnum, EventManipulationType.DELETE_LITERAL);
		addEEnumLiteral(eventManipulationTypeEEnum, EventManipulationType.UPDATE_LITERAL);

		initEEnum(nullableTypeEEnum, NullableType.class, "NullableType");
		addEEnumLiteral(nullableTypeEEnum, NullableType.COLUMN_NO_NULLS_LITERAL);
		addEEnumLiteral(nullableTypeEEnum, NullableType.COLUMN_NULLABLE_LITERAL);
		addEEnumLiteral(nullableTypeEEnum, NullableType.COLUMN_NULLABLE_UNKNOWN_LITERAL);

		initEEnum(procedureTypeEEnum, ProcedureType.class, "ProcedureType");
		addEEnumLiteral(procedureTypeEEnum, ProcedureType.PROCEDURE_LITERAL);
		addEEnumLiteral(procedureTypeEEnum, ProcedureType.FUNCTION_LITERAL);

		initEEnum(referentialRuleTypeEEnum, ReferentialRuleType.class, "ReferentialRuleType");
		addEEnumLiteral(referentialRuleTypeEEnum, ReferentialRuleType.IMPORTED_KEY_NO_ACTION_LITERAL);
		addEEnumLiteral(referentialRuleTypeEEnum, ReferentialRuleType.IMPORTED_KEY_CASCADE_LITERAL);
		addEEnumLiteral(referentialRuleTypeEEnum, ReferentialRuleType.IMPORTED_KEY_SET_NULL_LITERAL);
		addEEnumLiteral(referentialRuleTypeEEnum, ReferentialRuleType.IMPORTED_KEY_RESTRICT_LITERAL);
		addEEnumLiteral(referentialRuleTypeEEnum, ReferentialRuleType.IMPORTED_KEY_SET_DEFAULT_LITERAL);
	}

} //EnumerationsPackageImpl
