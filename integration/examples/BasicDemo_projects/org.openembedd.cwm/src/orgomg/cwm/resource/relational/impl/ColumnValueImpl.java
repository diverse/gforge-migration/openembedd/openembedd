/**
 * INRIA/IRISA
 *
 * $Id: ColumnValueImpl.java,v 1.1 2008-04-01 09:35:24 vmahe Exp $
 */
package orgomg.cwm.resource.relational.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.instance.impl.DataValueImpl;

import orgomg.cwm.resource.relational.ColumnValue;
import orgomg.cwm.resource.relational.RelationalPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Column Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ColumnValueImpl extends DataValueImpl implements ColumnValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ColumnValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RelationalPackage.Literals.COLUMN_VALUE;
	}

} //ColumnValueImpl
