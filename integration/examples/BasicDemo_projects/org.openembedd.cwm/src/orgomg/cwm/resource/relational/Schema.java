/**
 * INRIA/IRISA
 *
 * $Id: Schema.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A schema is a named collection of tables
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getSchema()
 * @model
 * @generated
 */
public interface Schema extends orgomg.cwm.objectmodel.core.Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Schema
