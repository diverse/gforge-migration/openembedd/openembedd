/**
 * INRIA/IRISA
 *
 * $Id: EnumerationsFactory.java,v 1.1 2008-04-01 09:35:35 vmahe Exp $
 */
package orgomg.cwm.resource.relational.enumerations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.resource.relational.enumerations.EnumerationsPackage
 * @generated
 */
public interface EnumerationsFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EnumerationsFactory eINSTANCE = orgomg.cwm.resource.relational.enumerations.impl.EnumerationsFactoryImpl.init();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EnumerationsPackage getEnumerationsPackage();

} //EnumerationsFactory
