/**
 * INRIA/IRISA
 *
 * $Id: FieldValue.java,v 1.1 2008-04-01 09:35:23 vmahe Exp $
 */
package orgomg.cwm.resource.record;

import orgomg.cwm.objectmodel.instance.DataValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The value currently held in a Field instance.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.record.RecordPackage#getFieldValue()
 * @model
 * @generated
 */
public interface FieldValue extends DataValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // FieldValue
