/**
 * INRIA/IRISA
 *
 * $Id: SQLParameter.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;

import orgomg.cwm.objectmodel.behavioral.Parameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Parameters of stored procedures.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getSQLParameter()
 * @model
 * @generated
 */
public interface SQLParameter extends Parameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // SQLParameter
