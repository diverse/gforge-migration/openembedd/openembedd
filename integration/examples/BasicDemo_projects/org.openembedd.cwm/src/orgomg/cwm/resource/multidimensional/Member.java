/**
 * INRIA/IRISA
 *
 * $Id: Member.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.resource.multidimensional;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Member represents a member of a Dimension.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.multidimensional.MultidimensionalPackage#getMember()
 * @model
 * @generated
 */
public interface Member extends orgomg.cwm.objectmodel.instance.Object {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Member
