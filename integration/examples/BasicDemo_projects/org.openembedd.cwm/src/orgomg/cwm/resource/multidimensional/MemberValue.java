/**
 * INRIA/IRISA
 *
 * $Id: MemberValue.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.resource.multidimensional;

import orgomg.cwm.objectmodel.instance.DataValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Member Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * MemberValue represents an instance value of a Member.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.multidimensional.MultidimensionalPackage#getMemberValue()
 * @model
 * @generated
 */
public interface MemberValue extends DataValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // MemberValue
