/**
 * INRIA/IRISA
 *
 * $Id: PrimaryKey.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primary Key</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * There is only one UniqueConstraint of type PrimaryKey per Table. It is implemented specifically by each RDBMS.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getPrimaryKey()
 * @model
 * @generated
 */
public interface PrimaryKey extends UniqueConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // PrimaryKey
