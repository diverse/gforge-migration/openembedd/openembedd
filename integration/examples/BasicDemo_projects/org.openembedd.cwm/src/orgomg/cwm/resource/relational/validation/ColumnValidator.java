/**
 * <copyright>
 * </copyright>
 *
 * $Id: ColumnValidator.java,v 1.1 2008-04-01 09:35:22 vmahe Exp $
 */
package orgomg.cwm.resource.relational.validation;

import orgomg.cwm.resource.relational.NamedColumnSet;
import orgomg.cwm.resource.relational.SQLStructuredType;

import orgomg.cwm.resource.relational.enumerations.NullableType;

/**
 * A sample validator interface for {@link orgomg.cwm.resource.relational.Column}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ColumnValidator {
	boolean validate();

	boolean validatePrecision(long value);
	boolean validateScale(long value);
	boolean validateIsNullable(NullableType value);
	boolean validateLength(long value);
	boolean validateCollationName(String value);
	boolean validateCharacterSetName(String value);
	boolean validateReferencedTableType(SQLStructuredType value);
	boolean validateOptionScopeColumnSet(NamedColumnSet value);
}
