/**
 * INRIA/IRISA
 *
 * $Id: RowSet.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;

import orgomg.cwm.objectmodel.instance.Extent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Row Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Each instance of RowSet owns a collection of Row instances. The inherited association between Namespace (a superclass of Package) and ModelElement is used to contain Instances.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getRowSet()
 * @model
 * @generated
 */
public interface RowSet extends Extent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // RowSet
