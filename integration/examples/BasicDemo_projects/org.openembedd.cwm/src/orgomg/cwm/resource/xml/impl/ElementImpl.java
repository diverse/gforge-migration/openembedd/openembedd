/**
 * INRIA/IRISA
 *
 * $Id: ElementImpl.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.xml.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.objectmodel.instance.impl.ObjectImpl;

import orgomg.cwm.resource.xml.Element;
import orgomg.cwm.resource.xml.XmlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ElementImpl extends ObjectImpl implements Element {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XmlPackage.Literals.ELEMENT;
	}

} //ElementImpl
