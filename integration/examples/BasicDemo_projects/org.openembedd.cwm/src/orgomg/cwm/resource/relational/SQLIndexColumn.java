/**
 * INRIA/IRISA
 *
 * $Id: SQLIndexColumn.java,v 1.1 2008-04-01 09:35:19 vmahe Exp $
 */
package orgomg.cwm.resource.relational;

import orgomg.cwm.foundation.keysindexes.IndexedFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Index Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Associates an index with its columns.
 * 
 * This is really an association (link) class. It is associated with one index and one column.
 * 
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.relational.RelationalPackage#getSQLIndexColumn()
 * @model
 * @generated
 */
public interface SQLIndexColumn extends IndexedFeature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // SQLIndexColumn
