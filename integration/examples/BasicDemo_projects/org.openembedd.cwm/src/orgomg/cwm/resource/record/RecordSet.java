/**
 * INRIA/IRISA
 *
 * $Id: RecordSet.java,v 1.1 2008-04-01 09:35:24 vmahe Exp $
 */
package orgomg.cwm.resource.record;

import orgomg.cwm.objectmodel.instance.Extent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A RecordSet represents a collection of Record instances.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.record.RecordPackage#getRecordSet()
 * @model
 * @generated
 */
public interface RecordSet extends Extent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // RecordSet
