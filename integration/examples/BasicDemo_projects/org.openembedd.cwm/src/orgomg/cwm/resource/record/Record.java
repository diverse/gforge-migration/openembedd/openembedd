/**
 * INRIA/IRISA
 *
 * $Id: Record.java,v 1.1 2008-04-01 09:35:23 vmahe Exp $
 */
package orgomg.cwm.resource.record;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Record, a subclass of Object, represents a single data record. Each Record is described by a RecordDef instance found via the Object’s InstanceClassifier association.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.resource.record.RecordPackage#getRecord()
 * @model
 * @generated
 */
public interface Record extends orgomg.cwm.objectmodel.instance.Object {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // Record
