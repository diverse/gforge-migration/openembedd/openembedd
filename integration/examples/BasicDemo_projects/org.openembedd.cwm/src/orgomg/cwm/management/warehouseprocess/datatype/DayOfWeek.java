/**
 * INRIA/IRISA
 *
 * $Id: DayOfWeek.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.datatype;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Day Of Week</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, workingDay, nonworkingDay
 * <!-- end-model-doc -->
 * @see orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage#getDayOfWeek()
 * @model
 * @generated
 */
public enum DayOfWeek implements Enumerator {
	/**
	 * The '<em><b>Monday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MONDAY
	 * @generated
	 * @ordered
	 */
	MONDAY_LITERAL(0, "monday", "monday"),

	/**
	 * The '<em><b>Tuesday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TUESDAY
	 * @generated
	 * @ordered
	 */
	TUESDAY_LITERAL(1, "tuesday", "tuesday"),

	/**
	 * The '<em><b>Wednesday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WEDNESDAY
	 * @generated
	 * @ordered
	 */
	WEDNESDAY_LITERAL(2, "wednesday", "wednesday"),

	/**
	 * The '<em><b>Thursday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THURSDAY
	 * @generated
	 * @ordered
	 */
	THURSDAY_LITERAL(3, "thursday", "thursday"),

	/**
	 * The '<em><b>Friday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FRIDAY
	 * @generated
	 * @ordered
	 */
	FRIDAY_LITERAL(4, "friday", "friday"),

	/**
	 * The '<em><b>Saturday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SATURDAY
	 * @generated
	 * @ordered
	 */
	SATURDAY_LITERAL(5, "saturday", "saturday"),

	/**
	 * The '<em><b>Sunday</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUNDAY
	 * @generated
	 * @ordered
	 */
	SUNDAY_LITERAL(6, "sunday", "sunday"),

	/**
	 * The '<em><b>Working Day</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WORKING_DAY
	 * @generated
	 * @ordered
	 */
	WORKING_DAY_LITERAL(7, "workingDay", "workingDay"),

	/**
	 * The '<em><b>Nonworking Day</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONWORKING_DAY
	 * @generated
	 * @ordered
	 */
	NONWORKING_DAY_LITERAL(8, "nonworkingDay", "nonworkingDay");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Monday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Monday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MONDAY_LITERAL
	 * @model name="monday"
	 * @generated
	 * @ordered
	 */
	public static final int MONDAY = 0;

	/**
	 * The '<em><b>Tuesday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Tuesday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TUESDAY_LITERAL
	 * @model name="tuesday"
	 * @generated
	 * @ordered
	 */
	public static final int TUESDAY = 1;

	/**
	 * The '<em><b>Wednesday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wednesday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WEDNESDAY_LITERAL
	 * @model name="wednesday"
	 * @generated
	 * @ordered
	 */
	public static final int WEDNESDAY = 2;

	/**
	 * The '<em><b>Thursday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Thursday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THURSDAY_LITERAL
	 * @model name="thursday"
	 * @generated
	 * @ordered
	 */
	public static final int THURSDAY = 3;

	/**
	 * The '<em><b>Friday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Friday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FRIDAY_LITERAL
	 * @model name="friday"
	 * @generated
	 * @ordered
	 */
	public static final int FRIDAY = 4;

	/**
	 * The '<em><b>Saturday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Saturday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SATURDAY_LITERAL
	 * @model name="saturday"
	 * @generated
	 * @ordered
	 */
	public static final int SATURDAY = 5;

	/**
	 * The '<em><b>Sunday</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sunday</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUNDAY_LITERAL
	 * @model name="sunday"
	 * @generated
	 * @ordered
	 */
	public static final int SUNDAY = 6;

	/**
	 * The '<em><b>Working Day</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Working Day</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WORKING_DAY_LITERAL
	 * @model name="workingDay"
	 * @generated
	 * @ordered
	 */
	public static final int WORKING_DAY = 7;

	/**
	 * The '<em><b>Nonworking Day</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nonworking Day</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NONWORKING_DAY_LITERAL
	 * @model name="nonworkingDay"
	 * @generated
	 * @ordered
	 */
	public static final int NONWORKING_DAY = 8;

	/**
	 * An array of all the '<em><b>Day Of Week</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DayOfWeek[] VALUES_ARRAY =
		new DayOfWeek[] {
			MONDAY_LITERAL,
			TUESDAY_LITERAL,
			WEDNESDAY_LITERAL,
			THURSDAY_LITERAL,
			FRIDAY_LITERAL,
			SATURDAY_LITERAL,
			SUNDAY_LITERAL,
			WORKING_DAY_LITERAL,
			NONWORKING_DAY_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Day Of Week</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DayOfWeek> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Day Of Week</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DayOfWeek get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DayOfWeek result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Day Of Week</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DayOfWeek getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DayOfWeek result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Day Of Week</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DayOfWeek get(int value) {
		switch (value) {
			case MONDAY: return MONDAY_LITERAL;
			case TUESDAY: return TUESDAY_LITERAL;
			case WEDNESDAY: return WEDNESDAY_LITERAL;
			case THURSDAY: return THURSDAY_LITERAL;
			case FRIDAY: return FRIDAY_LITERAL;
			case SATURDAY: return SATURDAY_LITERAL;
			case SUNDAY: return SUNDAY_LITERAL;
			case WORKING_DAY: return WORKING_DAY_LITERAL;
			case NONWORKING_DAY: return NONWORKING_DAY_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DayOfWeek(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DayOfWeek
