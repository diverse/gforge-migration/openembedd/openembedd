/**
 * INRIA/IRISA
 *
 * $Id: ProcessPackage.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A group of related WarehouseActivities.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.management.warehouseprocess.WarehouseprocessPackage#getProcessPackage()
 * @model
 * @generated
 */
public interface ProcessPackage extends orgomg.cwm.objectmodel.core.Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ProcessPackage
