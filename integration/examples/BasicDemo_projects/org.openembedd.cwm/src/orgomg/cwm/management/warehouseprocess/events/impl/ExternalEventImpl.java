/**
 * INRIA/IRISA
 *
 * $Id: ExternalEventImpl.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.events.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.management.warehouseprocess.events.EventsPackage;
import orgomg.cwm.management.warehouseprocess.events.ExternalEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ExternalEventImpl extends WarehouseEventImpl implements ExternalEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.EXTERNAL_EVENT;
	}

} //ExternalEventImpl
