/**
 * INRIA/IRISA
 *
 * $Id: PointInTimeEventImpl.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.events.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.management.warehouseprocess.events.EventsPackage;
import orgomg.cwm.management.warehouseprocess.events.PointInTimeEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point In Time Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PointInTimeEventImpl extends ScheduleEventImpl implements PointInTimeEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointInTimeEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.POINT_IN_TIME_EVENT;
	}

} //PointInTimeEventImpl
