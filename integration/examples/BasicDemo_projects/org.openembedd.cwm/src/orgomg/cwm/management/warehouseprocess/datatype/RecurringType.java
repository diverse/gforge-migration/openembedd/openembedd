/**
 * INRIA/IRISA
 *
 * $Id: RecurringType.java,v 1.1 2008-04-01 09:35:49 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.datatype;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Recurring Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * everyYear, everyMonth, everyWeek, everyDay, everyHour, everyMinute
 * <!-- end-model-doc -->
 * @see orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage#getRecurringType()
 * @model
 * @generated
 */
public enum RecurringType implements Enumerator {
	/**
	 * The '<em><b>Every Year</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_YEAR
	 * @generated
	 * @ordered
	 */
	EVERY_YEAR_LITERAL(0, "everyYear", "everyYear"),

	/**
	 * The '<em><b>Every Month</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_MONTH
	 * @generated
	 * @ordered
	 */
	EVERY_MONTH_LITERAL(1, "everyMonth", "everyMonth"),

	/**
	 * The '<em><b>Every Week</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_WEEK
	 * @generated
	 * @ordered
	 */
	EVERY_WEEK_LITERAL(2, "everyWeek", "everyWeek"),

	/**
	 * The '<em><b>Every Day</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_DAY
	 * @generated
	 * @ordered
	 */
	EVERY_DAY_LITERAL(3, "everyDay", "everyDay"),

	/**
	 * The '<em><b>Every Hour</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_HOUR
	 * @generated
	 * @ordered
	 */
	EVERY_HOUR_LITERAL(4, "everyHour", "everyHour"),

	/**
	 * The '<em><b>Every Minute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVERY_MINUTE
	 * @generated
	 * @ordered
	 */
	EVERY_MINUTE_LITERAL(5, "everyMinute", "everyMinute");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * The '<em><b>Every Year</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Year</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_YEAR_LITERAL
	 * @model name="everyYear"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_YEAR = 0;

	/**
	 * The '<em><b>Every Month</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Month</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_MONTH_LITERAL
	 * @model name="everyMonth"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_MONTH = 1;

	/**
	 * The '<em><b>Every Week</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Week</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_WEEK_LITERAL
	 * @model name="everyWeek"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_WEEK = 2;

	/**
	 * The '<em><b>Every Day</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Day</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_DAY_LITERAL
	 * @model name="everyDay"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_DAY = 3;

	/**
	 * The '<em><b>Every Hour</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Hour</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_HOUR_LITERAL
	 * @model name="everyHour"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_HOUR = 4;

	/**
	 * The '<em><b>Every Minute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Every Minute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVERY_MINUTE_LITERAL
	 * @model name="everyMinute"
	 * @generated
	 * @ordered
	 */
	public static final int EVERY_MINUTE = 5;

	/**
	 * An array of all the '<em><b>Recurring Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final RecurringType[] VALUES_ARRAY =
		new RecurringType[] {
			EVERY_YEAR_LITERAL,
			EVERY_MONTH_LITERAL,
			EVERY_WEEK_LITERAL,
			EVERY_DAY_LITERAL,
			EVERY_HOUR_LITERAL,
			EVERY_MINUTE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Recurring Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<RecurringType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Recurring Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RecurringType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RecurringType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Recurring Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RecurringType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RecurringType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Recurring Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RecurringType get(int value) {
		switch (value) {
			case EVERY_YEAR: return EVERY_YEAR_LITERAL;
			case EVERY_MONTH: return EVERY_MONTH_LITERAL;
			case EVERY_WEEK: return EVERY_WEEK_LITERAL;
			case EVERY_DAY: return EVERY_DAY_LITERAL;
			case EVERY_HOUR: return EVERY_HOUR_LITERAL;
			case EVERY_MINUTE: return EVERY_MINUTE_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private RecurringType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //RecurringType
