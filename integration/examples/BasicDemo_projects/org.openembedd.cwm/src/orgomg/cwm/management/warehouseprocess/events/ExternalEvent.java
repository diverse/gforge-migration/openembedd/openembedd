/**
 * INRIA/IRISA
 *
 * $Id: ExternalEvent.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An ExternalEvent allows the description of the triggering of a WarehouseProcess by a task which is not described in the model. This is merely a place holder. The actual behavior and the connection with the external trigger is left to the implementation of
 * the scheduler.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.management.warehouseprocess.events.EventsPackage#getExternalEvent()
 * @model
 * @generated
 */
public interface ExternalEvent extends WarehouseEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ExternalEvent
