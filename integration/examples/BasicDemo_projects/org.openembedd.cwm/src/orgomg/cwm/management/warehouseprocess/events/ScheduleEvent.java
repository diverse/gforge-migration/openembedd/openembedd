/**
 * INRIA/IRISA
 *
 * $Id: ScheduleEvent.java,v 1.1 2008-04-01 09:35:38 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A ScheduleEvent is an abstract class which covers all the clock based events.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.management.warehouseprocess.events.EventsPackage#getScheduleEvent()
 * @model abstract="true"
 * @generated
 */
public interface ScheduleEvent extends WarehouseEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // ScheduleEvent
