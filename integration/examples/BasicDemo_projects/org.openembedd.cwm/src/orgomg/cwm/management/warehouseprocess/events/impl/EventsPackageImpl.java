/**
 * INRIA/IRISA
 *
 * $Id: EventsPackageImpl.java,v 1.1 2008-04-01 09:35:20 vmahe Exp $
 */
package orgomg.cwm.management.warehouseprocess.events.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import orgomg.cwm.analysis.businessnomenclature.BusinessnomenclaturePackage;

import orgomg.cwm.analysis.businessnomenclature.impl.BusinessnomenclaturePackageImpl;

import orgomg.cwm.analysis.datamining.approximation.ApproximationPackage;

import orgomg.cwm.analysis.datamining.approximation.impl.ApproximationPackageImpl;

import orgomg.cwm.analysis.datamining.associationrules.AssociationrulesPackage;

import orgomg.cwm.analysis.datamining.associationrules.impl.AssociationrulesPackageImpl;

import orgomg.cwm.analysis.datamining.attributeimportance.AttributeimportancePackage;

import orgomg.cwm.analysis.datamining.attributeimportance.impl.AttributeimportancePackageImpl;

import orgomg.cwm.analysis.datamining.classification.ClassificationPackage;

import orgomg.cwm.analysis.datamining.classification.impl.ClassificationPackageImpl;

import orgomg.cwm.analysis.datamining.clustering.ClusteringPackage;

import orgomg.cwm.analysis.datamining.clustering.impl.ClusteringPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.EntrypointPackage;

import orgomg.cwm.analysis.datamining.miningcore.entrypoint.impl.EntrypointPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.MiningdataPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningdata.impl.MiningdataPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.MiningfunctionsettingsPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningfunctionsettings.impl.MiningfunctionsettingsPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.MiningmodelPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningmodel.impl.MiningmodelPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.MiningresultPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningresult.impl.MiningresultPackageImpl;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.MiningtaskPackage;

import orgomg.cwm.analysis.datamining.miningcore.miningtask.impl.MiningtaskPackageImpl;

import orgomg.cwm.analysis.datamining.supervised.SupervisedPackage;

import orgomg.cwm.analysis.datamining.supervised.impl.SupervisedPackageImpl;

import orgomg.cwm.analysis.informationvisualization.InformationvisualizationPackage;

import orgomg.cwm.analysis.informationvisualization.impl.InformationvisualizationPackageImpl;

import orgomg.cwm.analysis.olap.OlapPackage;

import orgomg.cwm.analysis.olap.impl.OlapPackageImpl;

import orgomg.cwm.analysis.transformation.TransformationPackage;

import orgomg.cwm.analysis.transformation.impl.TransformationPackageImpl;

import orgomg.cwm.foundation.businessinformation.BusinessinformationPackage;

import orgomg.cwm.foundation.businessinformation.impl.BusinessinformationPackageImpl;

import orgomg.cwm.foundation.datatypes.DatatypesPackage;

import orgomg.cwm.foundation.datatypes.impl.DatatypesPackageImpl;

import orgomg.cwm.foundation.expressions.ExpressionsPackage;

import orgomg.cwm.foundation.expressions.impl.ExpressionsPackageImpl;

import orgomg.cwm.foundation.keysindexes.KeysindexesPackage;

import orgomg.cwm.foundation.keysindexes.impl.KeysindexesPackageImpl;

import orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage;

import orgomg.cwm.foundation.softwaredeployment.impl.SoftwaredeploymentPackageImpl;

import orgomg.cwm.foundation.typemapping.TypemappingPackage;

import orgomg.cwm.foundation.typemapping.impl.TypemappingPackageImpl;

import orgomg.cwm.management.warehouseoperation.WarehouseoperationPackage;

import orgomg.cwm.management.warehouseoperation.impl.WarehouseoperationPackageImpl;

import orgomg.cwm.management.warehouseprocess.WarehouseprocessPackage;

import orgomg.cwm.management.warehouseprocess.datatype.DatatypePackage;

import orgomg.cwm.management.warehouseprocess.datatype.impl.DatatypePackageImpl;

import orgomg.cwm.management.warehouseprocess.events.CalendarDate;
import orgomg.cwm.management.warehouseprocess.events.CascadeEvent;
import orgomg.cwm.management.warehouseprocess.events.CustomCalendar;
import orgomg.cwm.management.warehouseprocess.events.CustomCalendarEvent;
import orgomg.cwm.management.warehouseprocess.events.EventsFactory;
import orgomg.cwm.management.warehouseprocess.events.EventsPackage;
import orgomg.cwm.management.warehouseprocess.events.ExternalEvent;
import orgomg.cwm.management.warehouseprocess.events.InternalEvent;
import orgomg.cwm.management.warehouseprocess.events.IntervalEvent;
import orgomg.cwm.management.warehouseprocess.events.PointInTimeEvent;
import orgomg.cwm.management.warehouseprocess.events.RecurringPointInTimeEvent;
import orgomg.cwm.management.warehouseprocess.events.RetryEvent;
import orgomg.cwm.management.warehouseprocess.events.ScheduleEvent;
import orgomg.cwm.management.warehouseprocess.events.WarehouseEvent;

import orgomg.cwm.management.warehouseprocess.impl.WarehouseprocessPackageImpl;

import orgomg.cwm.objectmodel.behavioral.BehavioralPackage;

import orgomg.cwm.objectmodel.behavioral.impl.BehavioralPackageImpl;

import orgomg.cwm.objectmodel.core.CorePackage;

import orgomg.cwm.objectmodel.core.impl.CorePackageImpl;

import orgomg.cwm.objectmodel.instance.InstancePackage;

import orgomg.cwm.objectmodel.instance.impl.InstancePackageImpl;

import orgomg.cwm.objectmodel.relationships.RelationshipsPackage;

import orgomg.cwm.objectmodel.relationships.impl.RelationshipsPackageImpl;

import orgomg.cwm.resource.multidimensional.MultidimensionalPackage;

import orgomg.cwm.resource.multidimensional.impl.MultidimensionalPackageImpl;

import orgomg.cwm.resource.record.RecordPackage;

import orgomg.cwm.resource.record.impl.RecordPackageImpl;

import orgomg.cwm.resource.relational.RelationalPackage;

import orgomg.cwm.resource.relational.enumerations.EnumerationsPackage;

import orgomg.cwm.resource.relational.enumerations.impl.EnumerationsPackageImpl;

import orgomg.cwm.resource.relational.impl.RelationalPackageImpl;

import orgomg.cwm.resource.xml.XmlPackage;

import orgomg.cwm.resource.xml.impl.XmlPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EventsPackageImpl extends EPackageImpl implements EventsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass warehouseEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointInTimeEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customCalendarEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customCalendarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass calendarDateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intervalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cascadeEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass retryEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recurringPointInTimeEventEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see orgomg.cwm.management.warehouseprocess.events.EventsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EventsPackageImpl() {
		super(eNS_URI, EventsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EventsPackage init() {
		if (isInited) return (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Obtain or create and register package
		EventsPackageImpl theEventsPackage = (EventsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof EventsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new EventsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		BehavioralPackageImpl theBehavioralPackage = (BehavioralPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) instanceof BehavioralPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI) : BehavioralPackage.eINSTANCE);
		RelationshipsPackageImpl theRelationshipsPackage = (RelationshipsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) instanceof RelationshipsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationshipsPackage.eNS_URI) : RelationshipsPackage.eINSTANCE);
		InstancePackageImpl theInstancePackage = (InstancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) instanceof InstancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI) : InstancePackage.eINSTANCE);
		BusinessinformationPackageImpl theBusinessinformationPackage = (BusinessinformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) instanceof BusinessinformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessinformationPackage.eNS_URI) : BusinessinformationPackage.eINSTANCE);
		DatatypesPackageImpl theDatatypesPackage = (DatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) instanceof DatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) : DatatypesPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		KeysindexesPackageImpl theKeysindexesPackage = (KeysindexesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) instanceof KeysindexesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(KeysindexesPackage.eNS_URI) : KeysindexesPackage.eINSTANCE);
		SoftwaredeploymentPackageImpl theSoftwaredeploymentPackage = (SoftwaredeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) instanceof SoftwaredeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SoftwaredeploymentPackage.eNS_URI) : SoftwaredeploymentPackage.eINSTANCE);
		TypemappingPackageImpl theTypemappingPackage = (TypemappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) instanceof TypemappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypemappingPackage.eNS_URI) : TypemappingPackage.eINSTANCE);
		RelationalPackageImpl theRelationalPackage = (RelationalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) instanceof RelationalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RelationalPackage.eNS_URI) : RelationalPackage.eINSTANCE);
		EnumerationsPackageImpl theEnumerationsPackage = (EnumerationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) instanceof EnumerationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EnumerationsPackage.eNS_URI) : EnumerationsPackage.eINSTANCE);
		RecordPackageImpl theRecordPackage = (RecordPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) instanceof RecordPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RecordPackage.eNS_URI) : RecordPackage.eINSTANCE);
		MultidimensionalPackageImpl theMultidimensionalPackage = (MultidimensionalPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) instanceof MultidimensionalPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MultidimensionalPackage.eNS_URI) : MultidimensionalPackage.eINSTANCE);
		XmlPackageImpl theXmlPackage = (XmlPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) instanceof XmlPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmlPackage.eNS_URI) : XmlPackage.eINSTANCE);
		TransformationPackageImpl theTransformationPackage = (TransformationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) instanceof TransformationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TransformationPackage.eNS_URI) : TransformationPackage.eINSTANCE);
		OlapPackageImpl theOlapPackage = (OlapPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) instanceof OlapPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OlapPackage.eNS_URI) : OlapPackage.eINSTANCE);
		MiningdataPackageImpl theMiningdataPackage = (MiningdataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) instanceof MiningdataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningdataPackage.eNS_URI) : MiningdataPackage.eINSTANCE);
		MiningfunctionsettingsPackageImpl theMiningfunctionsettingsPackage = (MiningfunctionsettingsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) instanceof MiningfunctionsettingsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningfunctionsettingsPackage.eNS_URI) : MiningfunctionsettingsPackage.eINSTANCE);
		MiningmodelPackageImpl theMiningmodelPackage = (MiningmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) instanceof MiningmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningmodelPackage.eNS_URI) : MiningmodelPackage.eINSTANCE);
		MiningresultPackageImpl theMiningresultPackage = (MiningresultPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) instanceof MiningresultPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningresultPackage.eNS_URI) : MiningresultPackage.eINSTANCE);
		MiningtaskPackageImpl theMiningtaskPackage = (MiningtaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) instanceof MiningtaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MiningtaskPackage.eNS_URI) : MiningtaskPackage.eINSTANCE);
		EntrypointPackageImpl theEntrypointPackage = (EntrypointPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) instanceof EntrypointPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntrypointPackage.eNS_URI) : EntrypointPackage.eINSTANCE);
		ClusteringPackageImpl theClusteringPackage = (ClusteringPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) instanceof ClusteringPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI) : ClusteringPackage.eINSTANCE);
		AssociationrulesPackageImpl theAssociationrulesPackage = (AssociationrulesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) instanceof AssociationrulesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AssociationrulesPackage.eNS_URI) : AssociationrulesPackage.eINSTANCE);
		SupervisedPackageImpl theSupervisedPackage = (SupervisedPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) instanceof SupervisedPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SupervisedPackage.eNS_URI) : SupervisedPackage.eINSTANCE);
		ClassificationPackageImpl theClassificationPackage = (ClassificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) instanceof ClassificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassificationPackage.eNS_URI) : ClassificationPackage.eINSTANCE);
		ApproximationPackageImpl theApproximationPackage = (ApproximationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) instanceof ApproximationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ApproximationPackage.eNS_URI) : ApproximationPackage.eINSTANCE);
		AttributeimportancePackageImpl theAttributeimportancePackage = (AttributeimportancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) instanceof AttributeimportancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AttributeimportancePackage.eNS_URI) : AttributeimportancePackage.eINSTANCE);
		InformationvisualizationPackageImpl theInformationvisualizationPackage = (InformationvisualizationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) instanceof InformationvisualizationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationvisualizationPackage.eNS_URI) : InformationvisualizationPackage.eINSTANCE);
		BusinessnomenclaturePackageImpl theBusinessnomenclaturePackage = (BusinessnomenclaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) instanceof BusinessnomenclaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BusinessnomenclaturePackage.eNS_URI) : BusinessnomenclaturePackage.eINSTANCE);
		WarehouseprocessPackageImpl theWarehouseprocessPackage = (WarehouseprocessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) instanceof WarehouseprocessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI) : WarehouseprocessPackage.eINSTANCE);
		DatatypePackageImpl theDatatypePackage = (DatatypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) instanceof DatatypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI) : DatatypePackage.eINSTANCE);
		WarehouseoperationPackageImpl theWarehouseoperationPackage = (WarehouseoperationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) instanceof WarehouseoperationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WarehouseoperationPackage.eNS_URI) : WarehouseoperationPackage.eINSTANCE);

		// Create package meta-data objects
		theEventsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theBehavioralPackage.createPackageContents();
		theRelationshipsPackage.createPackageContents();
		theInstancePackage.createPackageContents();
		theBusinessinformationPackage.createPackageContents();
		theDatatypesPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theKeysindexesPackage.createPackageContents();
		theSoftwaredeploymentPackage.createPackageContents();
		theTypemappingPackage.createPackageContents();
		theRelationalPackage.createPackageContents();
		theEnumerationsPackage.createPackageContents();
		theRecordPackage.createPackageContents();
		theMultidimensionalPackage.createPackageContents();
		theXmlPackage.createPackageContents();
		theTransformationPackage.createPackageContents();
		theOlapPackage.createPackageContents();
		theMiningdataPackage.createPackageContents();
		theMiningfunctionsettingsPackage.createPackageContents();
		theMiningmodelPackage.createPackageContents();
		theMiningresultPackage.createPackageContents();
		theMiningtaskPackage.createPackageContents();
		theEntrypointPackage.createPackageContents();
		theClusteringPackage.createPackageContents();
		theAssociationrulesPackage.createPackageContents();
		theSupervisedPackage.createPackageContents();
		theClassificationPackage.createPackageContents();
		theApproximationPackage.createPackageContents();
		theAttributeimportancePackage.createPackageContents();
		theInformationvisualizationPackage.createPackageContents();
		theBusinessnomenclaturePackage.createPackageContents();
		theWarehouseprocessPackage.createPackageContents();
		theDatatypePackage.createPackageContents();
		theWarehouseoperationPackage.createPackageContents();

		// Initialize created meta-data
		theEventsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theBehavioralPackage.initializePackageContents();
		theRelationshipsPackage.initializePackageContents();
		theInstancePackage.initializePackageContents();
		theBusinessinformationPackage.initializePackageContents();
		theDatatypesPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theKeysindexesPackage.initializePackageContents();
		theSoftwaredeploymentPackage.initializePackageContents();
		theTypemappingPackage.initializePackageContents();
		theRelationalPackage.initializePackageContents();
		theEnumerationsPackage.initializePackageContents();
		theRecordPackage.initializePackageContents();
		theMultidimensionalPackage.initializePackageContents();
		theXmlPackage.initializePackageContents();
		theTransformationPackage.initializePackageContents();
		theOlapPackage.initializePackageContents();
		theMiningdataPackage.initializePackageContents();
		theMiningfunctionsettingsPackage.initializePackageContents();
		theMiningmodelPackage.initializePackageContents();
		theMiningresultPackage.initializePackageContents();
		theMiningtaskPackage.initializePackageContents();
		theEntrypointPackage.initializePackageContents();
		theClusteringPackage.initializePackageContents();
		theAssociationrulesPackage.initializePackageContents();
		theSupervisedPackage.initializePackageContents();
		theClassificationPackage.initializePackageContents();
		theApproximationPackage.initializePackageContents();
		theAttributeimportancePackage.initializePackageContents();
		theInformationvisualizationPackage.initializePackageContents();
		theBusinessnomenclaturePackage.initializePackageContents();
		theWarehouseprocessPackage.initializePackageContents();
		theDatatypePackage.initializePackageContents();
		theWarehouseoperationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEventsPackage.freeze();

		return theEventsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWarehouseEvent() {
		return warehouseEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWarehouseEvent_WarehouseProcess() {
		return (EReference)warehouseEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduleEvent() {
		return scheduleEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointInTimeEvent() {
		return pointInTimeEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomCalendarEvent() {
		return customCalendarEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomCalendarEvent_CustomCalendar() {
		return (EReference)customCalendarEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomCalendar() {
		return customCalendarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomCalendar_CustomCalendarEvent() {
		return (EReference)customCalendarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCalendarDate() {
		return calendarDateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCalendarDate_SpecificDate() {
		return (EAttribute)calendarDateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntervalEvent() {
		return intervalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntervalEvent_Duration() {
		return (EAttribute)intervalEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalEvent() {
		return externalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInternalEvent() {
		return internalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInternalEvent_TriggeringWP() {
		return (EReference)internalEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInternalEvent_Condition() {
		return (EReference)internalEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCascadeEvent() {
		return cascadeEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCascadeEvent_WaitRule() {
		return (EAttribute)cascadeEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRetryEvent() {
		return retryEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRetryEvent_WaitDuration() {
		return (EAttribute)retryEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRetryEvent_MaxCount() {
		return (EAttribute)retryEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecurringPointInTimeEvent() {
		return recurringPointInTimeEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_RecurringType() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_FrequencyFactor() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_Month() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_DayOfMonth() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_DayOfWeek() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_Hour() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_Minute() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecurringPointInTimeEvent_Second() {
		return (EAttribute)recurringPointInTimeEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventsFactory getEventsFactory() {
		return (EventsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		warehouseEventEClass = createEClass(WAREHOUSE_EVENT);
		createEReference(warehouseEventEClass, WAREHOUSE_EVENT__WAREHOUSE_PROCESS);

		scheduleEventEClass = createEClass(SCHEDULE_EVENT);

		pointInTimeEventEClass = createEClass(POINT_IN_TIME_EVENT);

		customCalendarEventEClass = createEClass(CUSTOM_CALENDAR_EVENT);
		createEReference(customCalendarEventEClass, CUSTOM_CALENDAR_EVENT__CUSTOM_CALENDAR);

		customCalendarEClass = createEClass(CUSTOM_CALENDAR);
		createEReference(customCalendarEClass, CUSTOM_CALENDAR__CUSTOM_CALENDAR_EVENT);

		calendarDateEClass = createEClass(CALENDAR_DATE);
		createEAttribute(calendarDateEClass, CALENDAR_DATE__SPECIFIC_DATE);

		intervalEventEClass = createEClass(INTERVAL_EVENT);
		createEAttribute(intervalEventEClass, INTERVAL_EVENT__DURATION);

		externalEventEClass = createEClass(EXTERNAL_EVENT);

		internalEventEClass = createEClass(INTERNAL_EVENT);
		createEReference(internalEventEClass, INTERNAL_EVENT__TRIGGERING_WP);
		createEReference(internalEventEClass, INTERNAL_EVENT__CONDITION);

		cascadeEventEClass = createEClass(CASCADE_EVENT);
		createEAttribute(cascadeEventEClass, CASCADE_EVENT__WAIT_RULE);

		retryEventEClass = createEClass(RETRY_EVENT);
		createEAttribute(retryEventEClass, RETRY_EVENT__WAIT_DURATION);
		createEAttribute(retryEventEClass, RETRY_EVENT__MAX_COUNT);

		recurringPointInTimeEventEClass = createEClass(RECURRING_POINT_IN_TIME_EVENT);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__RECURRING_TYPE);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__FREQUENCY_FACTOR);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__MONTH);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__DAY_OF_MONTH);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__DAY_OF_WEEK);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__HOUR);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__MINUTE);
		createEAttribute(recurringPointInTimeEventEClass, RECURRING_POINT_IN_TIME_EVENT__SECOND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BehavioralPackage theBehavioralPackage = (BehavioralPackage)EPackage.Registry.INSTANCE.getEPackage(BehavioralPackage.eNS_URI);
		WarehouseprocessPackage theWarehouseprocessPackage = (WarehouseprocessPackage)EPackage.Registry.INSTANCE.getEPackage(WarehouseprocessPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		DatatypePackage theDatatypePackage = (DatatypePackage)EPackage.Registry.INSTANCE.getEPackage(DatatypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		warehouseEventEClass.getESuperTypes().add(theBehavioralPackage.getEvent());
		scheduleEventEClass.getESuperTypes().add(this.getWarehouseEvent());
		pointInTimeEventEClass.getESuperTypes().add(this.getScheduleEvent());
		customCalendarEventEClass.getESuperTypes().add(this.getPointInTimeEvent());
		customCalendarEClass.getESuperTypes().add(theCorePackage.getPackage());
		calendarDateEClass.getESuperTypes().add(theCorePackage.getModelElement());
		intervalEventEClass.getESuperTypes().add(this.getScheduleEvent());
		externalEventEClass.getESuperTypes().add(this.getWarehouseEvent());
		internalEventEClass.getESuperTypes().add(this.getWarehouseEvent());
		cascadeEventEClass.getESuperTypes().add(this.getInternalEvent());
		retryEventEClass.getESuperTypes().add(this.getInternalEvent());
		recurringPointInTimeEventEClass.getESuperTypes().add(this.getPointInTimeEvent());

		// Initialize classes and features; add operations and parameters
		initEClass(warehouseEventEClass, WarehouseEvent.class, "WarehouseEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWarehouseEvent_WarehouseProcess(), theWarehouseprocessPackage.getWarehouseProcess(), theWarehouseprocessPackage.getWarehouseProcess_WarehouseEvent(), "warehouseProcess", null, 1, 1, WarehouseEvent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleEventEClass, ScheduleEvent.class, "ScheduleEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pointInTimeEventEClass, PointInTimeEvent.class, "PointInTimeEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customCalendarEventEClass, CustomCalendarEvent.class, "CustomCalendarEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomCalendarEvent_CustomCalendar(), this.getCustomCalendar(), this.getCustomCalendar_CustomCalendarEvent(), "customCalendar", null, 1, 1, CustomCalendarEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(customCalendarEClass, CustomCalendar.class, "CustomCalendar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomCalendar_CustomCalendarEvent(), this.getCustomCalendarEvent(), this.getCustomCalendarEvent_CustomCalendar(), "customCalendarEvent", null, 0, -1, CustomCalendar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(calendarDateEClass, CalendarDate.class, "CalendarDate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCalendarDate_SpecificDate(), theCorePackage.getTime(), "specificDate", null, 0, 1, CalendarDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intervalEventEClass, IntervalEvent.class, "IntervalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntervalEvent_Duration(), theCorePackage.getFloat(), "duration", null, 0, 1, IntervalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalEventEClass, ExternalEvent.class, "ExternalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(internalEventEClass, InternalEvent.class, "InternalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalEvent_TriggeringWP(), theWarehouseprocessPackage.getWarehouseProcess(), theWarehouseprocessPackage.getWarehouseProcess_InternalEvent(), "triggeringWP", null, 1, -1, InternalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalEvent_Condition(), theCorePackage.getBooleanExpression(), null, "condition", null, 0, 1, InternalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cascadeEventEClass, CascadeEvent.class, "CascadeEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCascadeEvent_WaitRule(), theDatatypePackage.getWaitRuleType(), "waitRule", null, 0, 1, CascadeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(retryEventEClass, RetryEvent.class, "RetryEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRetryEvent_WaitDuration(), theCorePackage.getFloat(), "waitDuration", null, 0, 1, RetryEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRetryEvent_MaxCount(), theCorePackage.getInteger(), "maxCount", null, 0, 1, RetryEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(recurringPointInTimeEventEClass, RecurringPointInTimeEvent.class, "RecurringPointInTimeEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRecurringPointInTimeEvent_RecurringType(), theDatatypePackage.getRecurringType(), "recurringType", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_FrequencyFactor(), theCorePackage.getInteger(), "frequencyFactor", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_Month(), theCorePackage.getInteger(), "month", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_DayOfMonth(), theCorePackage.getInteger(), "dayOfMonth", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_DayOfWeek(), theDatatypePackage.getDayOfWeek(), "dayOfWeek", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_Hour(), theCorePackage.getInteger(), "hour", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_Minute(), theCorePackage.getInteger(), "minute", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecurringPointInTimeEvent_Second(), theCorePackage.getInteger(), "second", null, 0, 1, RecurringPointInTimeEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //EventsPackageImpl
