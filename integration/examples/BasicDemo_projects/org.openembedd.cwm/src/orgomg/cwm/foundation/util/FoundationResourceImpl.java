/**
 * INRIA/IRISA
 *
 * $Id: FoundationResourceImpl.java,v 1.1 2008-04-01 09:35:21 vmahe Exp $
 */
package orgomg.cwm.foundation.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see orgomg.cwm.foundation.util.FoundationResourceFactoryImpl
 * @generated
 */
public class FoundationResourceImpl extends XMIResourceImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public FoundationResourceImpl(URI uri) {
		super(uri);
	}

} //FoundationResourceImpl
