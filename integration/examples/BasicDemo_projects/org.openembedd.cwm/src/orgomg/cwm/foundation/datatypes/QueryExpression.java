/**
 * INRIA/IRISA
 *
 * $Id: QueryExpression.java,v 1.1 2008-04-01 09:35:32 vmahe Exp $
 */
package orgomg.cwm.foundation.datatypes;

import orgomg.cwm.objectmodel.core.ProcedureExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * QueryExpression instances contain query statements in language-dependent form.
 * <!-- end-model-doc -->
 *
 *
 * @see orgomg.cwm.foundation.datatypes.DatatypesPackage#getQueryExpression()
 * @model
 * @generated
 */
public interface QueryExpression extends ProcedureExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

} // QueryExpression
