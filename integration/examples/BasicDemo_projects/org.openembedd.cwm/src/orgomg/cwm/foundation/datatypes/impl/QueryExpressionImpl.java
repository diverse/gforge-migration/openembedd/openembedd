/**
 * INRIA/IRISA
 *
 * $Id: QueryExpressionImpl.java,v 1.1 2008-04-01 09:35:50 vmahe Exp $
 */
package orgomg.cwm.foundation.datatypes.impl;

import org.eclipse.emf.ecore.EClass;

import orgomg.cwm.foundation.datatypes.DatatypesPackage;
import orgomg.cwm.foundation.datatypes.QueryExpression;

import orgomg.cwm.objectmodel.core.impl.ProcedureExpressionImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class QueryExpressionImpl extends ProcedureExpressionImpl implements QueryExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueryExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatatypesPackage.Literals.QUERY_EXPRESSION;
	}

} //QueryExpressionImpl
