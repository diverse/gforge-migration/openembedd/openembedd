/**
 * INRIA/IRISA
 *
 * $Id: Component.java,v 1.1 2008-04-01 09:35:39 vmahe Exp $
 */
package orgomg.cwm.foundation.softwaredeployment;

import org.eclipse.emf.common.util.EList;

import orgomg.cwm.objectmodel.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Component represents a physical piece of implementation of a system, including software code (source, binary or executable) or equivalents such as scripts or command files. A Component is a subtype of Classifier, and so may have its own Features, such as Attributes and Operations.
 * 
 * Deployment of a Component on a specific Machine is represented as a DeployedComponent.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link orgomg.cwm.foundation.softwaredeployment.Component#getDesignPackage <em>Design Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends Classifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "INRIA/IRISA";

	/**
	 * Returns the value of the '<em><b>Design Package</b></em>' reference list.
	 * The list contents are of type {@link orgomg.cwm.objectmodel.core.Package}.
	 * It is bidirectional and its opposite is '{@link orgomg.cwm.objectmodel.core.Package#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Identifies the Package instance containing the Component's design.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Design Package</em>' reference list.
	 * @see orgomg.cwm.foundation.softwaredeployment.SoftwaredeploymentPackage#getComponent_DesignPackage()
	 * @see orgomg.cwm.objectmodel.core.Package#getComponent
	 * @model opposite="component"
	 * @generated
	 */
	EList<orgomg.cwm.objectmodel.core.Package> getDesignPackage();

} // Component
