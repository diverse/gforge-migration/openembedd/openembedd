/**
 * INRIA/IRISA
 *
 * $Id: CWMResourceImpl.java,v 1.1 2008-04-01 09:35:52 vmahe Exp $
 */
package orgomg.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see orgomg.util.CWMResourceFactoryImpl
 * @generated
 */
public class CWMResourceImpl extends XMIResourceImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "INRIA/IRISA";

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public CWMResourceImpl(URI uri) {
		super(uri);
	}

} //CWMResourceImpl
