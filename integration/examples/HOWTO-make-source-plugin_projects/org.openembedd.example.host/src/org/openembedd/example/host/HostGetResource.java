package org.openembedd.example.host;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;

import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.osgi.framework.Bundle;
/**
 * @author openembedd
 * Example of use fragment like patch
 */
public class HostGetResource implements IApplication {

	private static String hostPluginID = "org.openembedd.example.host";
	private static String pathResource = "/resource/resource_example.txt"; 
	
	private File resource = null; 

	public Object start(IApplicationContext context) throws Exception {
		
		/**
		 * resource fragment
		 */
		Bundle host = Platform.getBundle(hostPluginID);
		URL url = host.getResource(pathResource);
		
		if (url != null) {
			/** resource exists **/
			getResourceFile(url.openStream());		//way to get File
			getResourceObject(url.openStream());	//way to get String object
			}
		else {
			/** resource doesn't exist **/
			System.out.println("no resource file");
		}
		
		
		/**
		 * native fragment
		 */
		//NativeClass nc = new NativeClass (); 
		  
		return null;
	}
	
	
	/**
	 * return the resource file or null if it doesn't exist 
	 */
	private File getResourceFile(InputStream in) throws Exception {
		if (in != null && in instanceof FileInputStream) {
			resource = new File(tempFolder() + "resource.txt");
			if (resource.exists()) {
				resource.delete();
			}
			if (resource.createNewFile()) {
				FileInputStream infile = (FileInputStream)in;
				FileOutputStream outfile = new FileOutputStream(resource);
				byte[] read = new byte[128];
				int len = 128;
				while ((len = infile.read(read)) > 0) {
					outfile.write(read, 0, len);
				}
				outfile.flush();
				outfile.close();
				infile.close();

				StringBuffer text = new StringBuffer();
				FileReader filereader = new FileReader(resource);
				char[] chread = new char[1];
				while ((len = filereader.read(chread)) > 0)
					text.append(chread);
				System.out.println("content of resource file = " + text.toString());
				
				return resource;
			}
		}
		return null;
	}
	
	
	/**
	 * return the resource object or null if it doesn't exist 
	 * @throws Exception 
	 */
	private String getResourceObject(InputStream in) throws Exception {
		if (in != null && in instanceof FileInputStream) {
			StringBuffer text = new StringBuffer();
			FileInputStream infile = (FileInputStream)in;
			if (infile.available() > 0) {
				byte[] read = new byte[1];
				int len = 0;
				ByteBuffer.allocate(1);
				while ((len = infile.getChannel().read(ByteBuffer.wrap(read))) > 0) {
					text.append(new String(read, 0, 1));
				}
				System.out.println("resource String          = " + text.toString());
			}
		}
		return null;
	}

	
	public void stop() {
		return;
	}

	public static String tempFolder() {
		String tempPath = "";
		if (System.getProperties().getProperty("os.name").trim().toLowerCase().indexOf("windows") >= 0)
			tempPath = System.getProperty("java.io.tmpdir");
		else if (System.getProperties().getProperty("os.name").trim().toLowerCase().indexOf("linux") >= 0)
			tempPath = System.getProperties().getProperty("user.home");
		else
			tempPath = "";
		return tempPath;
	}
};
