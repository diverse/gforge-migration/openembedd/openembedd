/**
 * <copyright> </copyright>
 * 
 * $Id: Plus.java,v 1.1 2008-05-28 12:47:10 vmahe Exp $
 */
package org.openembedd.logo.csm.logoCSM;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Plus</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see org.openembedd.logo.csm.logoCSM.LogoCSMPackage#getPlus()
 * @model
 * @generated
 */
public interface Plus extends BinaryExp
{} // Plus
