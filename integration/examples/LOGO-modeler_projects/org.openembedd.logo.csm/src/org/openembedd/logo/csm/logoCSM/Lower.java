/**
 * <copyright> </copyright>
 * 
 * $Id: Lower.java,v 1.1 2008-05-28 12:47:10 vmahe Exp $
 */
package org.openembedd.logo.csm.logoCSM;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Lower</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see org.openembedd.logo.csm.logoCSM.LogoCSMPackage#getLower()
 * @model
 * @generated
 */
public interface Lower extends BinaryExp
{} // Lower
