/**
 * <copyright> </copyright>
 * 
 * $Id: PenDown.java,v 1.1 2008-05-28 12:47:10 vmahe Exp $
 */
package org.openembedd.logo.csm.logoCSM;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Pen Down</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see org.openembedd.logo.csm.logoCSM.LogoCSMPackage#getPenDown()
 * @model
 * @generated
 */
public interface PenDown extends Primitive
{} // PenDown
