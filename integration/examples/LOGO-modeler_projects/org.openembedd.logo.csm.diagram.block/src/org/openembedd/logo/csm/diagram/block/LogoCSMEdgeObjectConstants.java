/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block;

/**
 * An interface defining EdgeObject Constants.<br>
 * 
 * @generated
 */
public interface LogoCSMEdgeObjectConstants
{}
