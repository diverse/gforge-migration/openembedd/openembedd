/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

import org.openembedd.logo.csm.diagram.block.LogoCSMImageRegistry;

/**
 * @generated
 */
public class ClearFigure extends org.eclipse.draw2d.ImageFigure
{
	/**
	 * Constructor
	 * 
	 * @generated NOT
	 */
	public ClearFigure()
	{
		super(LogoCSMImageRegistry.getImageDescriptor("CLEAR_HUGE").createImage());
	}

}
