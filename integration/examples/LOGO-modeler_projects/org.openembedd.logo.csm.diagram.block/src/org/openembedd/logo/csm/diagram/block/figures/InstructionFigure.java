/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

/**
 * @generated
 */
public class InstructionFigure extends org.eclipse.draw2d.Figure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public InstructionFigure()
	{
		super();
	}

}
