/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block;

/**
 * A Set of properties that are used for the graphical objects that are not associated with a model object. Each name is
 * used as the typeInfo attribute in the DI file.
 * 
 * @generated
 */
public interface LogoCSMSimpleObjectConstants
{
	/**
	 * The name that identify the <i>Sequence</i> SimpleObject.
	 * 
	 * @generated
	 */
	String	SIMPLE_OBJECT_SEQUENCE	= "Sequence";
}
