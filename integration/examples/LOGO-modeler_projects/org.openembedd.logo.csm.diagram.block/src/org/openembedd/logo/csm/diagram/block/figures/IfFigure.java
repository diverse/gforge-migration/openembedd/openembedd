/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

/**
 * @generated
 */
public class IfFigure extends org.topcased.draw2d.figures.ContainerWithInnerLabel
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public IfFigure()
	{
		super();
	}

}
