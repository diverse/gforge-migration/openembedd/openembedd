/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

import org.topcased.modeler.figures.DiagramFigure;

/**
 * The figure to display a Block Diagram.
 * 
 * @generated
 */
public class LogoCSMDiagramFigure extends DiagramFigure
{

}
