/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

import org.openembedd.logo.csm.diagram.block.LogoCSMImageRegistry;

/**
 * @generated
 */
public class LeftFigure extends org.topcased.draw2d.figures.ImageWithLabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated NOT
	 */
	public LeftFigure()
	{
		super(LogoCSMImageRegistry.getImageDescriptor("LEFT_HUGE").createImage());
	}

}
