/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface LogoCSMDiagramPreferenceConstants
{
	/**
	 * The key used to install a <i>Back Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	BACK_DEFAULT_BACKGROUND_COLOR			= "Back Default Background Color";

	/**
	 * The key used to install a <i>Back Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	BACK_DEFAULT_FOREGROUND_COLOR			= "Back Default Foreground Color";

	/**
	 * The key used to install a <i>Back Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	BACK_DEFAULT_FONT						= "Back Default Font";

	/**
	 * The key used to install a <i>Forward Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	FORWARD_DEFAULT_BACKGROUND_COLOR		= "Forward Default Background Color";

	/**
	 * The key used to install a <i>Forward Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	FORWARD_DEFAULT_FOREGROUND_COLOR		= "Forward Default Foreground Color";

	/**
	 * The key used to install a <i>Forward Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	FORWARD_DEFAULT_FONT					= "Forward Default Font";

	/**
	 * The key used to install a <i>Left Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	LEFT_DEFAULT_BACKGROUND_COLOR			= "Left Default Background Color";

	/**
	 * The key used to install a <i>Left Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	LEFT_DEFAULT_FOREGROUND_COLOR			= "Left Default Foreground Color";

	/**
	 * The key used to install a <i>Left Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	LEFT_DEFAULT_FONT						= "Left Default Font";

	/**
	 * The key used to install a <i>Right Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	RIGHT_DEFAULT_BACKGROUND_COLOR			= "Right Default Background Color";

	/**
	 * The key used to install a <i>Right Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	RIGHT_DEFAULT_FOREGROUND_COLOR			= "Right Default Foreground Color";

	/**
	 * The key used to install a <i>Right Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	RIGHT_DEFAULT_FONT						= "Right Default Font";

	/**
	 * The key used to install a <i>PenDown Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	PENDOWN_DEFAULT_BACKGROUND_COLOR		= "PenDown Default Background Color";

	/**
	 * The key used to install a <i>PenDown Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	PENDOWN_DEFAULT_FOREGROUND_COLOR		= "PenDown Default Foreground Color";

	/**
	 * The key used to install a <i>PenDown Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	PENDOWN_DEFAULT_FONT					= "PenDown Default Font";

	/**
	 * The key used to install a <i>PenUp Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	PENUP_DEFAULT_BACKGROUND_COLOR			= "PenUp Default Background Color";

	/**
	 * The key used to install a <i>PenUp Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	PENUP_DEFAULT_FOREGROUND_COLOR			= "PenUp Default Foreground Color";

	/**
	 * The key used to install a <i>PenUp Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	PENUP_DEFAULT_FONT						= "PenUp Default Font";

	/**
	 * The key used to install a <i>Clear Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	CLEAR_DEFAULT_BACKGROUND_COLOR			= "Clear Default Background Color";

	/**
	 * The key used to install a <i>Clear Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	CLEAR_DEFAULT_FOREGROUND_COLOR			= "Clear Default Foreground Color";

	/**
	 * The key used to install a <i>Clear Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	CLEAR_DEFAULT_FONT						= "Clear Default Font";

	/**
	 * The key used to install a <i>Block Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	BLOCK_DEFAULT_BACKGROUND_COLOR			= "Block Default Background Color";

	/**
	 * The key used to install a <i>Block Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	BLOCK_DEFAULT_FOREGROUND_COLOR			= "Block Default Foreground Color";

	/**
	 * The key used to install a <i>Block Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	BLOCK_DEFAULT_FONT						= "Block Default Font";

	/**
	 * The key used to install a <i>If Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	IF_DEFAULT_BACKGROUND_COLOR				= "If Default Background Color";

	/**
	 * The key used to install a <i>If Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	IF_DEFAULT_FOREGROUND_COLOR				= "If Default Foreground Color";

	/**
	 * The key used to install a <i>If Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	IF_DEFAULT_FONT							= "If Default Font";

	/**
	 * The key used to install a <i>Repeat Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	REPEAT_DEFAULT_BACKGROUND_COLOR			= "Repeat Default Background Color";

	/**
	 * The key used to install a <i>Repeat Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	REPEAT_DEFAULT_FOREGROUND_COLOR			= "Repeat Default Foreground Color";

	/**
	 * The key used to install a <i>Repeat Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	REPEAT_DEFAULT_FONT						= "Repeat Default Font";

	/**
	 * The key used to install a <i>Sequence Edge Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	SEQUENCE_EDGE_DEFAULT_FONT				= "Sequence Edge Default Font";

	/**
	 * The key used to install a <i>Sequence Edge Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	SEQUENCE_EDGE_DEFAULT_FOREGROUND_COLOR	= "Sequence Edge Default Foreground Color";

	/**
	 * The key used to install a <i>Sequence Edge Default Router</i> Preference.
	 * 
	 * @generated
	 */
	String	SEQUENCE_EDGE_DEFAULT_ROUTER			= "Sequence Edge Default Router";

	/**
	 * The key used to install a <i>Constant Default Background Color</i> Preference.
	 * 
	 * @generated
	 */
	String	CONSTANT_DEFAULT_BACKGROUND_COLOR		= "Constant Default Background Color";

	/**
	 * The key used to install a <i>Constant Default Foreground Color</i> Preference.
	 * 
	 * @generated
	 */
	String	CONSTANT_DEFAULT_FOREGROUND_COLOR		= "Constant Default Foreground Color";

	/**
	 * The key used to install a <i>Constant Default Font</i> Preference.
	 * 
	 * @generated
	 */
	String	CONSTANT_DEFAULT_FONT					= "Constant Default Font";

}
