/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 * 
 * @generated
 */
public interface LogoCSMEditPolicyConstants
{

	/**
	 * The key used to install an <i>Instruction</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INSTRUCTION_EDITPOLICY	= "Instruction EditPolicy";

	/**
	 * The key used to install an <i>Back</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	BACK_EDITPOLICY			= "Back EditPolicy";

	/**
	 * The key used to install an <i>Forward</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	FORWARD_EDITPOLICY		= "Forward EditPolicy";

	/**
	 * The key used to install an <i>Left</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	LEFT_EDITPOLICY			= "Left EditPolicy";

	/**
	 * The key used to install an <i>Right</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	RIGHT_EDITPOLICY		= "Right EditPolicy";

	/**
	 * The key used to install an <i>PenDown</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PENDOWN_EDITPOLICY		= "PenDown EditPolicy";

	/**
	 * The key used to install an <i>PenUp</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PENUP_EDITPOLICY		= "PenUp EditPolicy";

	/**
	 * The key used to install an <i>Clear</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CLEAR_EDITPOLICY		= "Clear EditPolicy";

	/**
	 * The key used to install an <i>Block</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	BLOCK_EDITPOLICY		= "Block EditPolicy";

	/**
	 * The key used to install an <i>If</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	IF_EDITPOLICY			= "If EditPolicy";

	/**
	 * The key used to install an <i>Repeat</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	REPEAT_EDITPOLICY		= "Repeat EditPolicy";

	/**
	 * The key used to install an <i>Sequence</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	SEQUENCE_EDITPOLICY		= "Sequence EditPolicy";

}
