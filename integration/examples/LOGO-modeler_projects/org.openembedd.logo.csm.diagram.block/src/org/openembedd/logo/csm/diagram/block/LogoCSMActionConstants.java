/***********************************************************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 **********************************************************************************************************************/
package org.openembedd.logo.csm.diagram.block;

/**
 * Identifies new Action constants
 */
public interface LogoCSMActionConstants
{
	/**
	 * Edit the comment of an identifier
	 */
	String	EDIT_EXPRESSION	= "EditExpression";
}
