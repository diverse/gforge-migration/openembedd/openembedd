/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

/**
 * @generated
 */
public class BlockFigure extends org.topcased.draw2d.figures.ContainerFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public BlockFigure()
	{
		super();
	}

}
