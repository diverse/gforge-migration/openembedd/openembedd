/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block.figures;

import org.openembedd.logo.csm.diagram.block.LogoCSMImageRegistry;

/**
 * @generated
 */
public class RightFigure extends org.topcased.draw2d.figures.ImageWithLabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated NOT
	 */
	public RightFigure()
	{
		super(LogoCSMImageRegistry.getImageDescriptor("RIGHT_HUGE").createImage());
	}

}
