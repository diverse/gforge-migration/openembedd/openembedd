/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.diagram.block;

/**
 * @generated
 */
public class EditPart2ModelAdapterFactory extends org.topcased.modeler.editor.EditPart2ModelAdapterFactory
{

	/**
	 * Constructor
	 * 
	 * @param adaptableClass
	 * @param adapterType
	 * @generated
	 */
	public EditPart2ModelAdapterFactory(Class adaptableClass, Class adapterType)
	{
		super(adaptableClass, adapterType);
	}

}
