/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.modeler.actions;

import org.topcased.modeler.actions.ModelerActionBarContributor;

/**
 * Generated Actions
 * 
 * @generated
 */
public class LogoCSMEditorActionBarContributor extends ModelerActionBarContributor
{
	// TODO defined customized actions
}
