/*******************************************************************************
 * Copyright : INRIA OpenEmbeDD - integration team This plug-in is under the terms of the EPL License.
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 ******************************************************************************/
package org.openembedd.logo.csm.modeler.providers;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.openembedd.logo.csm.logoCSM.util.LogoCSMAdapterFactory;
import org.topcased.modeler.providers.ILabelFeatureProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers. The adapters generated by this
 * factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}. The adapters
 * also support Eclipse property sheets. Note that most of the adapters are shared among multiple instances.
 * 
 * @generated
 */
public class LogoCSMModelerProviderAdapterFactory extends LogoCSMAdapterFactory implements ComposeableAdapterFactory,
		IChangeNotifier, IDisposable
{
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * 
	 * @generated
	 */
	private ComposedAdapterFactory			parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * 
	 * @generated
	 */
	private IChangeNotifier					changeNotifier	= new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * 
	 * @generated
	 */
	private Collection						supportedTypes	= new ArrayList();

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Instruction} instances.
	 * 
	 * @generated
	 */
	private InstructionModelerProvider		instructionModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Primitive} instances.
	 * 
	 * @generated
	 */
	private PrimitiveModelerProvider		primitiveModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Back} instances.
	 * 
	 * @generated
	 */
	private BackModelerProvider				backModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Forward} instances.
	 * 
	 * @generated
	 */
	private ForwardModelerProvider			forwardModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Left} instances.
	 * 
	 * @generated
	 */
	private LeftModelerProvider				leftModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Right} instances.
	 * 
	 * @generated
	 */
	private RightModelerProvider			rightModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.PenDown} instances.
	 * 
	 * @generated
	 */
	private PenDownModelerProvider			pendownModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.PenUp} instances.
	 * 
	 * @generated
	 */
	private PenUpModelerProvider			penupModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Clear} instances.
	 * 
	 * @generated
	 */
	private ClearModelerProvider			clearModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Expression} instances.
	 * 
	 * @generated
	 */
	private ExpressionModelerProvider		expressionModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.BinaryExp} instances.
	 * 
	 * @generated
	 */
	private BinaryExpModelerProvider		binaryexpModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Constant} instances.
	 * 
	 * @generated
	 */
	private ConstantModelerProvider			constantModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.ProcCall} instances.
	 * 
	 * @generated
	 */
	private ProcCallModelerProvider			proccallModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.ProcDeclaration}
	 * instances.
	 * 
	 * @generated
	 */
	private ProcDeclarationModelerProvider	procdeclarationModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Block} instances.
	 * 
	 * @generated
	 */
	private BlockModelerProvider			blockModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.If} instances.
	 * 
	 * @generated
	 */
	private IfModelerProvider				ifModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.ControlStructure}
	 * instances.
	 * 
	 * @generated
	 */
	private ControlStructureModelerProvider	controlstructureModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Repeat} instances.
	 * 
	 * @generated
	 */
	private RepeatModelerProvider			repeatModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.While} instances.
	 * 
	 * @generated
	 */
	private WhileModelerProvider			whileModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Parameter} instances.
	 * 
	 * @generated
	 */
	private ParameterModelerProvider		parameterModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.ParameterCall} instances.
	 * 
	 * @generated
	 */
	private ParameterCallModelerProvider	parametercallModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Plus} instances.
	 * 
	 * @generated
	 */
	private PlusModelerProvider				plusModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Minus} instances.
	 * 
	 * @generated
	 */
	private MinusModelerProvider			minusModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Mult} instances.
	 * 
	 * @generated
	 */
	private MultModelerProvider				multModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Div} instances.
	 * 
	 * @generated
	 */
	private DivModelerProvider				divModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Equals} instances.
	 * 
	 * @generated
	 */
	private EqualsModelerProvider			equalsModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Greater} instances.
	 * 
	 * @generated
	 */
	private GreaterModelerProvider			greaterModelerProvider;

	/**
	 * This keeps track of the one adapter used for all {@link org.openembedd.logo.csm.logoCSM.Lower} instances.
	 * 
	 * @generated
	 */
	private LowerModelerProvider			lowerModelerProvider;

	/**
	 * This constructs an instance.
	 * 
	 * @generated
	 */
	public LogoCSMModelerProviderAdapterFactory()
	{
		supportedTypes.add(ILabelFeatureProvider.class);
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * 
	 * @return the root AdapterFactory
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory()
	{
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * 
	 * @param parentAdapterFactory
	 *        the new parent adapter factory
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory)
	{
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * @param type
	 *        the type to test
	 * @return true if supported
	 * 
	 * @generated
	 */
	public boolean isFactoryForType(Object type)
	{
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * 
	 * @param notifier
	 *        the notifier
	 * @param type
	 *        the object to adapt
	 * @return the Adapter the created adatper
	 * @generated
	 */
	public Adapter adapt(Notifier notifier, Object type)
	{
		return super.adapt(notifier, this);
	}

	/**
	 * @param object
	 *        the object to adapt
	 * @param type
	 *        the type to adapt
	 * @return the adapted Object
	 * @generated
	 */
	public Object adapt(Object object, Object type)
	{
		if (isFactoryForType(type))
		{
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class) || (((Class) type).isInstance(adapter)))
			{
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * 
	 * @param notifyChangedListener
	 *        the listener to add
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener)
	{
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * 
	 * @param notifyChangedListener
	 *        the listener to remove
	 * 
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener)
	{
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * 
	 * @param notification
	 *        the notification to fire
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification)
	{
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null)
		{
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Instruction}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createInstructionAdapter()
	{
		if (instructionModelerProvider == null)
		{
			instructionModelerProvider = new InstructionModelerProvider(this);
		}

		return instructionModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Primitive}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createPrimitiveAdapter()
	{
		if (primitiveModelerProvider == null)
		{
			primitiveModelerProvider = new PrimitiveModelerProvider(this);
		}

		return primitiveModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Back}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createBackAdapter()
	{
		if (backModelerProvider == null)
		{
			backModelerProvider = new BackModelerProvider(this);
		}

		return backModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Forward}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createForwardAdapter()
	{
		if (forwardModelerProvider == null)
		{
			forwardModelerProvider = new ForwardModelerProvider(this);
		}

		return forwardModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Left}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createLeftAdapter()
	{
		if (leftModelerProvider == null)
		{
			leftModelerProvider = new LeftModelerProvider(this);
		}

		return leftModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Right}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createRightAdapter()
	{
		if (rightModelerProvider == null)
		{
			rightModelerProvider = new RightModelerProvider(this);
		}

		return rightModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.PenDown}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createPenDownAdapter()
	{
		if (pendownModelerProvider == null)
		{
			pendownModelerProvider = new PenDownModelerProvider(this);
		}

		return pendownModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.PenUp}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createPenUpAdapter()
	{
		if (penupModelerProvider == null)
		{
			penupModelerProvider = new PenUpModelerProvider(this);
		}

		return penupModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Clear}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createClearAdapter()
	{
		if (clearModelerProvider == null)
		{
			clearModelerProvider = new ClearModelerProvider(this);
		}

		return clearModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Expression}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createExpressionAdapter()
	{
		if (expressionModelerProvider == null)
		{
			expressionModelerProvider = new ExpressionModelerProvider(this);
		}

		return expressionModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.BinaryExp}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createBinaryExpAdapter()
	{
		if (binaryexpModelerProvider == null)
		{
			binaryexpModelerProvider = new BinaryExpModelerProvider(this);
		}

		return binaryexpModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Constant}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createConstantAdapter()
	{
		if (constantModelerProvider == null)
		{
			constantModelerProvider = new ConstantModelerProvider(this);
		}

		return constantModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.ProcCall}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createProcCallAdapter()
	{
		if (proccallModelerProvider == null)
		{
			proccallModelerProvider = new ProcCallModelerProvider(this);
		}

		return proccallModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.ProcDeclaration}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createProcDeclarationAdapter()
	{
		if (procdeclarationModelerProvider == null)
		{
			procdeclarationModelerProvider = new ProcDeclarationModelerProvider(this);
		}

		return procdeclarationModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Block}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createBlockAdapter()
	{
		if (blockModelerProvider == null)
		{
			blockModelerProvider = new BlockModelerProvider(this);
		}

		return blockModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.If}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createIfAdapter()
	{
		if (ifModelerProvider == null)
		{
			ifModelerProvider = new IfModelerProvider(this);
		}

		return ifModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.ControlStructure}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createControlStructureAdapter()
	{
		if (controlstructureModelerProvider == null)
		{
			controlstructureModelerProvider = new ControlStructureModelerProvider(this);
		}

		return controlstructureModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Repeat}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createRepeatAdapter()
	{
		if (repeatModelerProvider == null)
		{
			repeatModelerProvider = new RepeatModelerProvider(this);
		}

		return repeatModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.While}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createWhileAdapter()
	{
		if (whileModelerProvider == null)
		{
			whileModelerProvider = new WhileModelerProvider(this);
		}

		return whileModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Parameter}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createParameterAdapter()
	{
		if (parameterModelerProvider == null)
		{
			parameterModelerProvider = new ParameterModelerProvider(this);
		}

		return parameterModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.ParameterCall}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createParameterCallAdapter()
	{
		if (parametercallModelerProvider == null)
		{
			parametercallModelerProvider = new ParameterCallModelerProvider(this);
		}

		return parametercallModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Plus}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createPlusAdapter()
	{
		if (plusModelerProvider == null)
		{
			plusModelerProvider = new PlusModelerProvider(this);
		}

		return plusModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Minus}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createMinusAdapter()
	{
		if (minusModelerProvider == null)
		{
			minusModelerProvider = new MinusModelerProvider(this);
		}

		return minusModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Mult}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createMultAdapter()
	{
		if (multModelerProvider == null)
		{
			multModelerProvider = new MultModelerProvider(this);
		}

		return multModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Div}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createDivAdapter()
	{
		if (divModelerProvider == null)
		{
			divModelerProvider = new DivModelerProvider(this);
		}

		return divModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Equals}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createEqualsAdapter()
	{
		if (equalsModelerProvider == null)
		{
			equalsModelerProvider = new EqualsModelerProvider(this);
		}

		return equalsModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Greater}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createGreaterAdapter()
	{
		if (greaterModelerProvider == null)
		{
			greaterModelerProvider = new GreaterModelerProvider(this);
		}

		return greaterModelerProvider;
	}

	/**
	 * This creates an adapter for a {@link org.openembedd.logo.csm.logoCSM.Lower}.
	 * 
	 * @return the Adapter
	 * @generated
	 */
	public Adapter createLowerAdapter()
	{
		if (lowerModelerProvider == null)
		{
			lowerModelerProvider = new LowerModelerProvider(this);
		}

		return lowerModelerProvider;
	}

	/**
	 * This disposes all of the item providers created by this factory.
	 * 
	 * @generated
	 */
	public void dispose()
	{
		if (instructionModelerProvider != null)
		{
			instructionModelerProvider.dispose();
		}
		if (primitiveModelerProvider != null)
		{
			primitiveModelerProvider.dispose();
		}
		if (backModelerProvider != null)
		{
			backModelerProvider.dispose();
		}
		if (forwardModelerProvider != null)
		{
			forwardModelerProvider.dispose();
		}
		if (leftModelerProvider != null)
		{
			leftModelerProvider.dispose();
		}
		if (rightModelerProvider != null)
		{
			rightModelerProvider.dispose();
		}
		if (pendownModelerProvider != null)
		{
			pendownModelerProvider.dispose();
		}
		if (penupModelerProvider != null)
		{
			penupModelerProvider.dispose();
		}
		if (clearModelerProvider != null)
		{
			clearModelerProvider.dispose();
		}
		if (expressionModelerProvider != null)
		{
			expressionModelerProvider.dispose();
		}
		if (binaryexpModelerProvider != null)
		{
			binaryexpModelerProvider.dispose();
		}
		if (constantModelerProvider != null)
		{
			constantModelerProvider.dispose();
		}
		if (proccallModelerProvider != null)
		{
			proccallModelerProvider.dispose();
		}
		if (procdeclarationModelerProvider != null)
		{
			procdeclarationModelerProvider.dispose();
		}
		if (blockModelerProvider != null)
		{
			blockModelerProvider.dispose();
		}
		if (ifModelerProvider != null)
		{
			ifModelerProvider.dispose();
		}
		if (controlstructureModelerProvider != null)
		{
			controlstructureModelerProvider.dispose();
		}
		if (repeatModelerProvider != null)
		{
			repeatModelerProvider.dispose();
		}
		if (whileModelerProvider != null)
		{
			whileModelerProvider.dispose();
		}
		if (parameterModelerProvider != null)
		{
			parameterModelerProvider.dispose();
		}
		if (parametercallModelerProvider != null)
		{
			parametercallModelerProvider.dispose();
		}
		if (plusModelerProvider != null)
		{
			plusModelerProvider.dispose();
		}
		if (minusModelerProvider != null)
		{
			minusModelerProvider.dispose();
		}
		if (multModelerProvider != null)
		{
			multModelerProvider.dispose();
		}
		if (divModelerProvider != null)
		{
			divModelerProvider.dispose();
		}
		if (equalsModelerProvider != null)
		{
			equalsModelerProvider.dispose();
		}
		if (greaterModelerProvider != null)
		{
			greaterModelerProvider.dispose();
		}
		if (lowerModelerProvider != null)
		{
			lowerModelerProvider.dispose();
		}
	}

}
