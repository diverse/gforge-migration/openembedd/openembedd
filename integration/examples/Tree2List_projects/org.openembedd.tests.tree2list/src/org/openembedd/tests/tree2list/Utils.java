package org.openembedd.tests.tree2list;

import org.openembedd.tests.utils.UiTools;

/**
 * Set of tools needed by tests on the Basic Demo.
 * Those tools can be used in setUp() & tearDown() methods or directly in test methods.
 * 
 * @author vmahe@irisa.fr
 *
 */
public class Utils {
	
	public static String PROJECT_NAME = "org.openembedd.tests.temp";
	public static String PLUGIN_NAME = "org.openembedd.tests.tree2list";

	public static String LIST_EDITOR_ID = "MMElementList.presentation.MMElementListEditorID";
	public static String TREE_EDITOR_ID = "MMTree.presentation.MMTreeEditorID";
	
	public static void loadData() {
		try {
			UiTools.copyFolderFromPlugin(PLUGIN_NAME, PROJECT_NAME, "/data");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
