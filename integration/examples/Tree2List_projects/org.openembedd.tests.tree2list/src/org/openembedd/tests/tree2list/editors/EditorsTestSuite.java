package org.openembedd.tests.tree2list.editors;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TreeEditorTestSuite.class, ListEditorTestSuite.class})
public class EditorsTestSuite { }
