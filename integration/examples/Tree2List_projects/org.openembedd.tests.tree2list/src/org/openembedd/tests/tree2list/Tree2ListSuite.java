package org.openembedd.tests.tree2list;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openembedd.tests.tree2list.editors.EditorsTestSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({EditorsTestSuite.class})
public class Tree2ListSuite { }
