package org.openembedd.tests.tree2list.editors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;
import org.junit.*;
import org.openembedd.tests.tree2list.Utils;
import org.openembedd.tests.utils.UiTools;

public class TreeEditorTestSuite {
	
	public static IEditorPart editor;
	public static IEditorInput editorInput;
	
	@BeforeClass
	public static void resetExampleProject() {
		/* 
		 * clean up the workspace before testing creation of the example
		 */
		UiTools.cleanProject(Utils.PROJECT_NAME);
		UiTools.createProject(Utils.PROJECT_NAME);
		Utils.loadData();
		// erase the main property use to control opening of editor
		editorInput = null;
	}
	
	@Test
	public void openEditor() throws Exception {
		UIJob job = new UIJob("blah") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					IFile model = UiTools.getWorkspaceFile(Utils.PROJECT_NAME+"/data/", "simpleModel.mmtree");
					if (model.isAccessible()) {
						// get the ElementList editor with the given model.
						editor = IDE.openEditor(UiTools.getPage(), model);
						editorInput = editor.getEditorInput();
					}
				} catch (PartInitException e) { UiTools.uiException = e; }
		        return Status.OK_STATUS;
			}	
		};
		UiTools.executeUIJob(job);
		
		UiTools.manageUIException();
		
		Assert.assertTrue("The MMTree model is not accessible", editor != null);
		Assert.assertTrue("We should get the MMTree editor opened", editorInput.exists());
	}
}
