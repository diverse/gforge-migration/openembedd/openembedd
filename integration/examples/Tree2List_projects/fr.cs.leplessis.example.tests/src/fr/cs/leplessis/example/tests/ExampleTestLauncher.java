/* $Id: ExampleTestLauncher.java,v 1.2 2007-08-23 13:11:35 vmahe Exp $
 * Creation date: August 23, 2007
 * License: Eclipse Public License
 * Copyright: INRIA 2007
 * Authors: vmahe@irisa.fr
 */

/**
 * Integration level test for fr.cs.leplessis.example
 */
package fr.cs.leplessis.example.tests;

import org.openembedd.tests.TestLauncher;
import org.openembedd.tests.TestWorkbenchAdvisor;

public class ExampleTestLauncher  extends TestLauncher {
	public TestWorkbenchAdvisor getAdvisor() {
		// Eclipse default Java Perspective
		return new TestWorkbenchAdvisor("org.eclipse.jdt.ui.JavaPerspective");
	}
}