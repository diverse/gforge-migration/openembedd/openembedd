/* $Id: Utils.java,v 1.1 2007-09-28 09:05:40 vmahe Exp $
 * Creation date: August 23, 2007
 * License: Eclipse Public License
 * Copyright: INRIA 2007
 * Authors: vmahe@irisa.fr
 */

/**
 * Integration level test for fr.cs.leplessis.example
 */
package fr.cs.leplessis.example.tests;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class Utils {

	public static Resource getResource(String uri) {
    	ResourceSet resource_set = new ResourceSetImpl();
    	Resource resource = resource_set.getResource(URI.createFileURI(uri), true);
    	try {
			resource.load(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resource;
	}
}
