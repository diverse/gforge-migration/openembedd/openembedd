/* $Id: TestValidModels.java,v 1.4 2007-09-28 09:05:40 vmahe Exp $
 * Creation date: August 23, 2007
 * License: Eclipse Public License
 * Copyright: INRIA 2007
 * Authors: vmahe@irisa.fr
 */

/**
 * Integration level test for fr.cs.leplessis.example
 */
package fr.cs.leplessis.example.tests.models;

import org.eclipse.emf.ecore.resource.Resource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openembedd.tests.utils.UiTools;
import org.openembedd.tests.model.ModelTools;
import org.openembedd.tests.model.NonValidModelException;

import fr.cs.leplessis.example.tests.Utils;

public class TestValidModels {

	@BeforeClass
	/* We create the data directory once for all the tests */
	public static void setUpTestsData() {
		UiTools.cleanProject("temp");
		UiTools.createProject("temp");
		try {
			UiTools.copyFolderFromPlugin("fr.cs.leplessis.example.tests", "temp", "/data");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUpTest() {
		
	}
	
	@After
	public void tearDownTest() {
		
	}
	
	@Test
	/* We just verify that the 'data' directory has been created */
	public void aFirstTest() {
		Resource res = Utils.getResource(UiTools.getWorkspacePath()+ "temp/data/simpleCompleteTree.xmi");
		Assert.assertTrue("The test data directory must exists", res != null);
	}
	
	@Test
	/* we verify that the example basic model is still considered as valid */
	public void validateSimpleModel() {
		Resource res = Utils.getResource(UiTools.getWorkspacePath()+ "temp/data/simpleCompleteTree.xmi");
		Assert.assertTrue("The model is not valid", ModelTools.validModel(res));
	}
	
	@Test (expected = NonValidModelException.class)
	/*
	 *  The validation should throws a NonValidModelException exception
	 *  because the 'badRootTree' model is not a valid one
	 *  (documentation says: "The root element should be a Node.")
	 */
	public void validateModelWithRootError() throws NonValidModelException {
		Resource res = Utils.getResource(UiTools.getWorkspacePath()+ "temp/data/badRoot.mmtree");
		ModelTools.validModelWithException(res);
	}
}
