/* $Id: IntegrationTestsSuite.java,v 1.2 2007-08-23 13:11:35 vmahe Exp $
 * Creation date: August 23, 2007
 * License: Eclipse Public License
 * Copyright: INRIA 2007
 * Authors: vmahe@irisa.fr
 */

/**
 * Integration level test for fr.cs.leplessis.example
 */
package fr.cs.leplessis.example.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import fr.cs.leplessis.example.tests.models.ModelsTestsSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ModelsTestsSuite.class})
public class IntegrationTestsSuite { }
