/**
 * <copyright>
 * </copyright>
 *
 * $Id: Node.java,v 1.2 2007-08-21 09:33:09 vmahe Exp $
 */
package MMTree;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link MMTree.Node#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see MMTree.MMTreePackage#getNode()
 * @model
 * @generated
 */
public interface Node extends TreeElement {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link MMTree.TreeElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see MMTree.MMTreePackage#getNode_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<TreeElement> getChildren();

} // Node
