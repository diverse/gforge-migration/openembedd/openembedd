/**
 * <copyright>
 * </copyright>
 *
 * $Id: Leaf.java,v 1.2 2007-08-21 09:33:09 vmahe Exp $
 */
package MMTree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link MMTree.Leaf#getSize <em>Size</em>}</li>
 * </ul>
 * </p>
 *
 * @see MMTree.MMTreePackage#getLeaf()
 * @model
 * @generated
 */
public interface Leaf extends TreeElement {
	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * The literals are from the enumeration {@link MMTree.LeafSize}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see MMTree.LeafSize
	 * @see #setSize(LeafSize)
	 * @see MMTree.MMTreePackage#getLeaf_Size()
	 * @model
	 * @generated
	 */
	LeafSize getSize();

	/**
	 * Sets the value of the '{@link MMTree.Leaf#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see MMTree.LeafSize
	 * @see #getSize()
	 * @generated
	 */
	void setSize(LeafSize value);

} // Leaf
