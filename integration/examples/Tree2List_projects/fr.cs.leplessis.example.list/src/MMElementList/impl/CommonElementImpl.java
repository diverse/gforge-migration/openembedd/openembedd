/**
 * <copyright>
 * </copyright>
 *
 * $Id: CommonElementImpl.java,v 1.2 2007-08-21 09:33:08 vmahe Exp $
 */
package MMElementList.impl;

import MMElementList.CommonElement;
import MMElementList.MMElementListPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Common Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class CommonElementImpl extends AbstractElementImpl implements CommonElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommonElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MMElementListPackage.Literals.COMMON_ELEMENT;
	}

} //CommonElementImpl
