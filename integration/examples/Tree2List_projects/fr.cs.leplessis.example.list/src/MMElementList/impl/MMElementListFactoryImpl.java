/**
 * <copyright>
 * </copyright>
 *
 * $Id: MMElementListFactoryImpl.java,v 1.2 2007-08-21 09:33:08 vmahe Exp $
 */
package MMElementList.impl;

import MMElementList.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MMElementListFactoryImpl extends EFactoryImpl implements MMElementListFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MMElementListFactory init() {
		try {
			MMElementListFactory theMMElementListFactory = (MMElementListFactory)EPackage.Registry.INSTANCE.getEFactory("platform:/resource/fr.cs.leplessis.example.list/model/MMElementList.ecore"); 
			if (theMMElementListFactory != null) {
				return theMMElementListFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MMElementListFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMElementListFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MMElementListPackage.ROOT_ELEMENT: return createRootElement();
			case MMElementListPackage.COMMON_ELEMENT: return createCommonElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootElement createRootElement() {
		RootElementImpl rootElement = new RootElementImpl();
		return rootElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonElement createCommonElement() {
		CommonElementImpl commonElement = new CommonElementImpl();
		return commonElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MMElementListPackage getMMElementListPackage() {
		return (MMElementListPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MMElementListPackage getPackage() {
		return MMElementListPackage.eINSTANCE;
	}

} //MMElementListFactoryImpl
