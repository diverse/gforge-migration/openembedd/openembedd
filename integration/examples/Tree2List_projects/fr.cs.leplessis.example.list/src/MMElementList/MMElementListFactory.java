/**
 * <copyright>
 * </copyright>
 *
 * $Id: MMElementListFactory.java,v 1.2 2007-08-21 09:33:08 vmahe Exp $
 */
package MMElementList;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see MMElementList.MMElementListPackage
 * @generated
 */
public interface MMElementListFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MMElementListFactory eINSTANCE = MMElementList.impl.MMElementListFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Root Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Root Element</em>'.
	 * @generated
	 */
	RootElement createRootElement();

	/**
	 * Returns a new object of class '<em>Common Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Common Element</em>'.
	 * @generated
	 */
	CommonElement createCommonElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MMElementListPackage getMMElementListPackage();

} //MMElementListFactory
