/**
 * <copyright>
 * </copyright>
 *
 * $Id: CommonElement.java,v 1.2 2007-08-21 09:33:08 vmahe Exp $
 */
package MMElementList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Common Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see MMElementList.MMElementListPackage#getCommonElement()
 * @model
 * @generated
 */
public interface CommonElement extends AbstractElement {
} // CommonElement
