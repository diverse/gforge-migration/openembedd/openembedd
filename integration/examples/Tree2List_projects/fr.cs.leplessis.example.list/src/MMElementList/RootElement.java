/**
 * <copyright>
 * </copyright>
 *
 * $Id: RootElement.java,v 1.2 2007-08-21 09:33:08 vmahe Exp $
 */
package MMElementList;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link MMElementList.RootElement#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see MMElementList.MMElementListPackage#getRootElement()
 * @model
 * @generated
 */
public interface RootElement extends AbstractElement {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link MMElementList.CommonElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see MMElementList.MMElementListPackage#getRootElement_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommonElement> getElements();

} // RootElement
