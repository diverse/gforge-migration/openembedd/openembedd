/* $Id: leaf_size_test_case.kmt,v 1.2 2007-09-26 13:27:19 vmahe Exp $
 * Creation date: August 23, 2007
 * License: Eclipse Public License
 * Copyright: INRIA 2007
 * Authors: vmahe@irisa.fr
 */

/**
 * Unit level test for fr.cs.leplessis.example.tree2list plug-in
 */
package tree2list::tests;

require kermeta
require "../transformations/tree2list.kmt"

using kermeta::standard
using tree2list

class LeafSizeTestCase inherits kermeta::kunit::TestCase
{
	attribute tl : Tree2List
	
	method setUp() is do
		// we use a new tree2list for each test
		tl := Tree2List.new
	end
	
	method tearDown() is do
		// We don't need to tearDown anything in this test case.
	end
    
    // test on the "big" leaf size
    operation testBigLeaf() is do
    	var leaf : MMTree::Leaf init MMTree::Leaf.new
    	leaf.size := MMTree::LeafSize.big
    	
    	var node : MMTree::Node init MMTree::Node.new
    	node.children.add(leaf)
    	tl.initialize(node)
    	tl.computeRootElement
    	
    	assertTrueWithMsg(tl.bigSizedElements.size == 1,
    		"Big leaves should go in bigSizedElements set")
    end
    
    // test on the "medium" leaf size
    operation testMediumLeaf() is do
    	var leaf : MMTree::Leaf init MMTree::Leaf.new
    	leaf.size := MMTree::LeafSize.medium
    	
    	var node : MMTree::Node init MMTree::Node.new
    	node.children.add(leaf)
    	tl.initialize(node)
    	tl.computeRootElement
    	
    	assertTrueWithMsg(tl.mediumSizedElements.size == 1,
    		"Medium leaves should go in mediumSizedElements set")
    end
    
    // test on the "small" leaf size
    operation testSmallLeaf() is do
    	var leaf : MMTree::Leaf init MMTree::Leaf.new
    	leaf.size := MMTree::LeafSize.small
    	
    	var node : MMTree::Node init MMTree::Node.new
    	node.children.add(leaf)
    	tl.initialize(node)
    	tl.computeRootElement
    	
    	assertTrueWithMsg(tl.smallSizedElements.size == 1,
    		"Small leaves should go in smallSizedElements set")
    end
}