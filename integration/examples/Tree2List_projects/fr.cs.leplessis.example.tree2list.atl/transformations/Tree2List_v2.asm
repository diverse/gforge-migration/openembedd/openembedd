<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm name="0">
	<cp>
		<constant value="Tree2List"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J;"/>
		<constant value="getLeavesInOrder"/>
		<constant value="OMMMTree!Leaf;"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="0"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="Node"/>
		<constant value="MMTree"/>
		<constant value="__initgetLeavesInOrder"/>
		<constant value="J.registerHelperAttribute(SS):V"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="MMMTree!Node;"/>
		<constant value="Sequence"/>
		<constant value="J.getAllChildren():J"/>
		<constant value="1"/>
		<constant value="Leaf"/>
		<constant value="J.oclIsTypeOf(J):J"/>
		<constant value="B.not():B"/>
		<constant value="16"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="2"/>
		<constant value="size"/>
		<constant value="EnumLiteral"/>
		<constant value="big"/>
		<constant value="name"/>
		<constant value="J.=(J):J"/>
		<constant value="37"/>
		<constant value="medium"/>
		<constant value="57"/>
		<constant value="J.union(J):J"/>
		<constant value="small"/>
		<constant value="78"/>
		<constant value="36:17-36:21"/>
		<constant value="36:17-36:39"/>
		<constant value="36:60-36:69"/>
		<constant value="36:82-36:93"/>
		<constant value="36:60-36:94"/>
		<constant value="36:17-36:95"/>
		<constant value="35:13-36:95"/>
		<constant value="38:17-38:27"/>
		<constant value="38:43-38:47"/>
		<constant value="38:43-38:52"/>
		<constant value="38:55-38:59"/>
		<constant value="38:43-38:59"/>
		<constant value="38:17-38:60"/>
		<constant value="39:25-39:35"/>
		<constant value="39:51-39:55"/>
		<constant value="39:51-39:60"/>
		<constant value="39:63-39:70"/>
		<constant value="39:51-39:70"/>
		<constant value="39:25-39:71"/>
		<constant value="38:17-39:72"/>
		<constant value="40:25-40:35"/>
		<constant value="40:51-40:55"/>
		<constant value="40:51-40:60"/>
		<constant value="40:63-40:69"/>
		<constant value="40:51-40:69"/>
		<constant value="40:25-40:70"/>
		<constant value="38:17-40:71"/>
		<constant value="35:9-40:71"/>
		<constant value="currChild"/>
		<constant value="leaf"/>
		<constant value="leavesList"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchrootNode():V"/>
		<constant value="A.__matchLeaf2CommonElement():V"/>
		<constant value="__matchrootNode"/>
		<constant value="aTree"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="CJ.union(CJ):CJ"/>
		<constant value="J.isRootObject():J"/>
		<constant value="38"/>
		<constant value="TransientLink"/>
		<constant value="rootNode"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="rt"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="lstRt"/>
		<constant value="RootElement"/>
		<constant value="MMElementList"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink(NTransientLink;):V"/>
		<constant value="47:35-47:37"/>
		<constant value="47:35-47:52"/>
		<constant value="49:25-49:50"/>
		<constant value="__matchLeaf2CommonElement"/>
		<constant value="Leaf2CommonElement"/>
		<constant value="s"/>
		<constant value="t"/>
		<constant value="CommonElement"/>
		<constant value="62:21-62:48"/>
		<constant value="__resolve__"/>
		<constant value="J"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="__exec__"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyrootNode(NTransientLink;):V"/>
		<constant value="A.__applyLeaf2CommonElement(NTransientLink;):V"/>
		<constant value="isRootObject"/>
		<constant value="MMMTree!TreeElement;"/>
		<constant value="J.refImmediateComposite():J"/>
		<constant value="19:9-19:13"/>
		<constant value="19:9-19:37"/>
		<constant value="19:9-19:54"/>
		<constant value="getAllChildren"/>
		<constant value="OrderedSet"/>
		<constant value="children"/>
		<constant value="J.append(J):J"/>
		<constant value="22"/>
		<constant value="24:17-24:29"/>
		<constant value="23:41-24:29"/>
		<constant value="23:9-23:13"/>
		<constant value="23:9-23:22"/>
		<constant value="25:20-25:25"/>
		<constant value="25:38-25:49"/>
		<constant value="25:20-25:50"/>
		<constant value="27:22-27:30"/>
		<constant value="27:38-27:43"/>
		<constant value="27:22-27:44"/>
		<constant value="26:22-26:30"/>
		<constant value="26:37-26:42"/>
		<constant value="26:37-26:60"/>
		<constant value="26:22-26:61"/>
		<constant value="25:17-28:22"/>
		<constant value="23:9-29:18"/>
		<constant value="child"/>
		<constant value="elements"/>
		<constant value="__applyrootNode"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="50:33-50:35"/>
		<constant value="50:33-50:40"/>
		<constant value="50:25-50:40"/>
		<constant value="51:37-51:39"/>
		<constant value="51:37-51:56"/>
		<constant value="51:25-51:56"/>
		<constant value="link"/>
		<constant value="__applyLeaf2CommonElement"/>
		<constant value="63:33-63:34"/>
		<constant value="63:33-63:39"/>
		<constant value="63:25-63:39"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<field name="5" type="6"/>
	<operation name="7">
		<context type="8"/>
		<parameters>
		</parameters>
		<code>
			<load arg="9"/>
			<push arg="10"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<call arg="13"/>
			<dup/>
			<push arg="14"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="15"/>
			<call arg="13"/>
			<call arg="16"/>
			<set arg="3"/>
			<load arg="9"/>
			<push arg="17"/>
			<push arg="11"/>
			<new/>
			<set arg="1"/>
			<push arg="18"/>
			<push arg="19"/>
			<findme/>
			<push arg="5"/>
			<push arg="20"/>
			<call arg="21"/>
			<load arg="9"/>
			<call arg="22"/>
			<load arg="9"/>
			<call arg="23"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="24" begin="0" end="30"/>
		</localvariabletable>
	</operation>
	<operation name="20">
		<context type="25"/>
		<parameters>
		</parameters>
		<code>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<load arg="9"/>
			<call arg="27"/>
			<iterate/>
			<store arg="28"/>
			<load arg="28"/>
			<push arg="29"/>
			<push arg="19"/>
			<findme/>
			<call arg="30"/>
			<call arg="31"/>
			<if arg="32"/>
			<load arg="28"/>
			<call arg="33"/>
			<enditerate/>
			<store arg="28"/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<load arg="28"/>
			<iterate/>
			<store arg="34"/>
			<load arg="34"/>
			<get arg="35"/>
			<push arg="36"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="37"/>
			<set arg="38"/>
			<call arg="39"/>
			<call arg="31"/>
			<if arg="40"/>
			<load arg="34"/>
			<call arg="33"/>
			<enditerate/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<load arg="28"/>
			<iterate/>
			<store arg="34"/>
			<load arg="34"/>
			<get arg="35"/>
			<push arg="36"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="41"/>
			<set arg="38"/>
			<call arg="39"/>
			<call arg="31"/>
			<if arg="42"/>
			<load arg="34"/>
			<call arg="33"/>
			<enditerate/>
			<call arg="43"/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<load arg="28"/>
			<iterate/>
			<store arg="34"/>
			<load arg="34"/>
			<get arg="35"/>
			<push arg="36"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="44"/>
			<set arg="38"/>
			<call arg="39"/>
			<call arg="31"/>
			<if arg="45"/>
			<load arg="34"/>
			<call arg="33"/>
			<enditerate/>
			<call arg="43"/>
		</code>
		<linenumbertable>
			<lne id="46" begin="3" end="3"/>
			<lne id="47" begin="3" end="4"/>
			<lne id="48" begin="7" end="7"/>
			<lne id="49" begin="8" end="10"/>
			<lne id="50" begin="7" end="11"/>
			<lne id="51" begin="0" end="16"/>
			<lne id="52" begin="0" end="16"/>
			<lne id="53" begin="21" end="21"/>
			<lne id="54" begin="24" end="24"/>
			<lne id="55" begin="24" end="25"/>
			<lne id="56" begin="26" end="31"/>
			<lne id="57" begin="24" end="32"/>
			<lne id="58" begin="18" end="37"/>
			<lne id="59" begin="41" end="41"/>
			<lne id="60" begin="44" end="44"/>
			<lne id="61" begin="44" end="45"/>
			<lne id="62" begin="46" end="51"/>
			<lne id="63" begin="44" end="52"/>
			<lne id="64" begin="38" end="57"/>
			<lne id="65" begin="18" end="58"/>
			<lne id="66" begin="62" end="62"/>
			<lne id="67" begin="65" end="65"/>
			<lne id="68" begin="65" end="66"/>
			<lne id="69" begin="67" end="72"/>
			<lne id="70" begin="65" end="73"/>
			<lne id="71" begin="59" end="78"/>
			<lne id="72" begin="18" end="79"/>
			<lne id="73" begin="0" end="79"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="74" begin="6" end="15"/>
			<lve slot="2" name="75" begin="23" end="36"/>
			<lve slot="2" name="75" begin="43" end="56"/>
			<lve slot="2" name="75" begin="64" end="77"/>
			<lve slot="1" name="76" begin="17" end="79"/>
			<lve slot="0" name="24" begin="0" end="79"/>
		</localvariabletable>
	</operation>
	<operation name="77">
		<context type="8"/>
		<parameters>
		</parameters>
		<code>
			<load arg="9"/>
			<call arg="78"/>
			<load arg="9"/>
			<call arg="79"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="24" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="80">
		<context type="8"/>
		<parameters>
		</parameters>
		<code>
			<push arg="18"/>
			<push arg="19"/>
			<findme/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<swap/>
			<dup_x1/>
			<push arg="81"/>
			<call arg="82"/>
			<call arg="83"/>
			<swap/>
			<pop/>
			<iterate/>
			<store arg="28"/>
			<load arg="28"/>
			<call arg="84"/>
			<call arg="31"/>
			<if arg="85"/>
			<load arg="9"/>
			<get arg="1"/>
			<push arg="86"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="87"/>
			<call arg="88"/>
			<dup/>
			<push arg="89"/>
			<load arg="28"/>
			<call arg="90"/>
			<dup/>
			<push arg="91"/>
			<push arg="92"/>
			<push arg="93"/>
			<new/>
			<call arg="94"/>
			<call arg="95"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="96" begin="15" end="15"/>
			<lne id="97" begin="15" end="16"/>
			<lne id="98" begin="33" end="35"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="89" begin="14" end="37"/>
			<lve slot="0" name="24" begin="0" end="38"/>
		</localvariabletable>
	</operation>
	<operation name="99">
		<context type="8"/>
		<parameters>
		</parameters>
		<code>
			<push arg="29"/>
			<push arg="19"/>
			<findme/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<swap/>
			<dup_x1/>
			<push arg="81"/>
			<call arg="82"/>
			<call arg="83"/>
			<swap/>
			<pop/>
			<iterate/>
			<store arg="28"/>
			<pusht/>
			<call arg="31"/>
			<if arg="40"/>
			<load arg="9"/>
			<get arg="1"/>
			<push arg="86"/>
			<push arg="11"/>
			<new/>
			<dup/>
			<push arg="100"/>
			<call arg="88"/>
			<dup/>
			<push arg="101"/>
			<load arg="28"/>
			<call arg="90"/>
			<dup/>
			<push arg="102"/>
			<push arg="103"/>
			<push arg="93"/>
			<new/>
			<call arg="94"/>
			<call arg="95"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="104" begin="32" end="34"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="101" begin="14" end="36"/>
			<lve slot="0" name="24" begin="0" end="37"/>
		</localvariabletable>
	</operation>
	<operation name="105">
		<context type="8"/>
		<parameters>
			<parameter name="28" type="106"/>
		</parameters>
		<code>
			<load arg="28"/>
			<load arg="9"/>
			<get arg="3"/>
			<call arg="107"/>
			<if arg="108"/>
			<load arg="9"/>
			<get arg="1"/>
			<load arg="28"/>
			<call arg="109"/>
			<dup/>
			<call arg="110"/>
			<if arg="111"/>
			<load arg="28"/>
			<call arg="112"/>
			<goto arg="113"/>
			<pop/>
			<load arg="28"/>
			<goto arg="114"/>
			<push arg="26"/>
			<push arg="11"/>
			<new/>
			<load arg="28"/>
			<iterate/>
			<store arg="34"/>
			<load arg="9"/>
			<load arg="34"/>
			<call arg="115"/>
			<call arg="116"/>
			<enditerate/>
			<call arg="117"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="118" begin="23" end="27"/>
			<lve slot="0" name="24" begin="0" end="29"/>
			<lve slot="1" name="119" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="120">
		<context type="8"/>
		<parameters>
			<parameter name="28" type="106"/>
			<parameter name="34" type="121"/>
		</parameters>
		<code>
			<load arg="9"/>
			<get arg="1"/>
			<load arg="28"/>
			<call arg="109"/>
			<load arg="28"/>
			<load arg="34"/>
			<call arg="122"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="24" begin="0" end="6"/>
			<lve slot="1" name="119" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="123">
		<context type="8"/>
		<parameters>
		</parameters>
		<code>
			<load arg="9"/>
			<get arg="1"/>
			<push arg="87"/>
			<call arg="124"/>
			<iterate/>
			<store arg="28"/>
			<load arg="9"/>
			<load arg="28"/>
			<call arg="125"/>
			<enditerate/>
			<load arg="9"/>
			<get arg="1"/>
			<push arg="100"/>
			<call arg="124"/>
			<iterate/>
			<store arg="28"/>
			<load arg="9"/>
			<load arg="28"/>
			<call arg="126"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="118" begin="5" end="8"/>
			<lve slot="1" name="118" begin="15" end="18"/>
			<lve slot="0" name="24" begin="0" end="19"/>
		</localvariabletable>
	</operation>
	<operation name="127">
		<context type="128"/>
		<parameters>
		</parameters>
		<code>
			<load arg="9"/>
			<call arg="129"/>
			<call arg="110"/>
		</code>
		<linenumbertable>
			<lne id="130" begin="0" end="0"/>
			<lne id="131" begin="0" end="1"/>
			<lne id="132" begin="0" end="2"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="24" begin="0" end="2"/>
		</localvariabletable>
	</operation>
	<operation name="133">
		<context type="25"/>
		<parameters>
		</parameters>
		<code>
			<push arg="134"/>
			<push arg="11"/>
			<new/>
			<store arg="28"/>
			<load arg="9"/>
			<get arg="135"/>
			<iterate/>
			<store arg="34"/>
			<load arg="34"/>
			<push arg="18"/>
			<push arg="19"/>
			<findme/>
			<call arg="30"/>
			<if arg="108"/>
			<load arg="28"/>
			<load arg="34"/>
			<call arg="136"/>
			<goto arg="137"/>
			<load arg="28"/>
			<load arg="34"/>
			<call arg="27"/>
			<call arg="43"/>
			<store arg="28"/>
			<enditerate/>
			<load arg="28"/>
		</code>
		<linenumbertable>
			<lne id="138" begin="0" end="2"/>
			<lne id="139" begin="0" end="2"/>
			<lne id="140" begin="4" end="4"/>
			<lne id="141" begin="4" end="5"/>
			<lne id="142" begin="8" end="8"/>
			<lne id="143" begin="9" end="11"/>
			<lne id="144" begin="8" end="12"/>
			<lne id="145" begin="14" end="14"/>
			<lne id="146" begin="15" end="15"/>
			<lne id="147" begin="14" end="16"/>
			<lne id="148" begin="18" end="18"/>
			<lne id="149" begin="19" end="19"/>
			<lne id="150" begin="19" end="20"/>
			<lne id="151" begin="18" end="21"/>
			<lne id="152" begin="8" end="21"/>
			<lne id="153" begin="0" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="154" begin="7" end="22"/>
			<lve slot="1" name="155" begin="3" end="24"/>
			<lve slot="0" name="24" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="156">
		<context type="8"/>
		<parameters>
			<parameter name="28" type="157"/>
		</parameters>
		<code>
			<load arg="28"/>
			<push arg="89"/>
			<call arg="158"/>
			<store arg="34"/>
			<load arg="28"/>
			<push arg="91"/>
			<call arg="159"/>
			<store arg="160"/>
			<load arg="160"/>
			<dup/>
			<load arg="9"/>
			<load arg="34"/>
			<get arg="38"/>
			<call arg="115"/>
			<set arg="38"/>
			<dup/>
			<load arg="9"/>
			<load arg="34"/>
			<get arg="5"/>
			<call arg="115"/>
			<set arg="155"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="161" begin="11" end="11"/>
			<lne id="162" begin="11" end="12"/>
			<lne id="163" begin="9" end="14"/>
			<lne id="164" begin="17" end="17"/>
			<lne id="165" begin="17" end="18"/>
			<lne id="166" begin="15" end="20"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="89" begin="3" end="21"/>
			<lve slot="3" name="91" begin="7" end="21"/>
			<lve slot="0" name="24" begin="0" end="21"/>
			<lve slot="1" name="167" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="168">
		<context type="8"/>
		<parameters>
			<parameter name="28" type="157"/>
		</parameters>
		<code>
			<load arg="28"/>
			<push arg="101"/>
			<call arg="158"/>
			<store arg="34"/>
			<load arg="28"/>
			<push arg="102"/>
			<call arg="159"/>
			<store arg="160"/>
			<load arg="160"/>
			<dup/>
			<load arg="9"/>
			<load arg="34"/>
			<get arg="38"/>
			<call arg="115"/>
			<set arg="38"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="169" begin="11" end="11"/>
			<lne id="170" begin="11" end="12"/>
			<lne id="171" begin="9" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="101" begin="3" end="15"/>
			<lve slot="3" name="102" begin="7" end="15"/>
			<lve slot="0" name="24" begin="0" end="15"/>
			<lve slot="1" name="167" begin="0" end="15"/>
		</localvariabletable>
	</operation>
</asm>
