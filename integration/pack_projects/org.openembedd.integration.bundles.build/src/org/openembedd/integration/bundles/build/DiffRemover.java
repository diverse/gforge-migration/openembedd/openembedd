package org.openembedd.integration.bundles.build;

import java.io.File;
import java.net.MalformedURLException;

public class DiffRemover
{

	private static String	originalDirPath;
	private static File		originalDir;
	private static String	enhancedDirPath;

	public static Object main(String[] args) throws Exception
	{

		processArgs(args);

		originalDir = new File(originalDirPath);

		// launch the recursive removal
		differentialRemove(originalDir);

		return null;
	}

	private static void processArgs(Object arg0) throws MalformedURLException
	{
		String[] args = (String[]) arg0;

		for (int i = 0; i < args.length; i++)
		{
			if (args[i].equalsIgnoreCase("-originaldir") && args.length >= i)
				originalDirPath = args[i + 1];
			if (args[i].equalsIgnoreCase("-enhanceddir") && args.length >= i)
				enhancedDirPath = args[i + 1];
		}
	}

	/**
	 * Recursive browsing of current directory with on-the-fly removing.
	 * 
	 * @param currentOriginalDir
	 *        : directory which appears in the original tree which we want to remove from enhanced tree
	 * @throws Exception
	 */
	private static void differentialRemove(File currentOriginalDir) throws Exception
	{
		if (!currentOriginalDir.isDirectory())
			throw new Exception("The '" + currentOriginalDir.getAbsolutePath() + "' is not a directory.");

		File files[] = currentOriginalDir.listFiles();

		for (int i = 0; i < files.length; i++)
		{
			if (files[i].isDirectory())
				// recursive exploration
				differentialRemove(files[i]);
			else
			{
				removeMirrorFile(files[i]);
			}
		}

		// if the directory (in enhanced dir) is now empty, erase it
		File currentEnhancedDir = new File(enhancedDirPath + relativePath(currentOriginalDir));
		if(!currentEnhancedDir.exists())
			return;
		if (currentEnhancedDir.listFiles().length == 0)
		{
			boolean deleted = currentEnhancedDir.delete();
			if (!deleted)
				System.out.println("WARNING: Fail to remove the '" + currentEnhancedDir.getName() + "' directory!");
		}
	}
	/*
	 * take a file from original dir and remove the corresponding file in enhanced dir
	 */
	private static void removeMirrorFile(File referenceFile) throws Exception
	{
		// get the relative path in original dir
		// and remove the file name
		int filePathLength = relativePath(referenceFile).length() - referenceFile.getName().length();
		String fileRelativePath = relativePath(referenceFile).substring(0, filePathLength);

		// get the corresponding dir in enhanced dir
		File relativeDir = new File(enhancedDirPath + fileRelativePath);

		File fileToRemove = find(relativeDir, referenceFile.getName());

		if (fileToRemove != null)
		{
			boolean deleted = fileToRemove.delete();
			if (!deleted)
				System.out.println("WARNING: Fail to remove the '" + fileToRemove.getName() + "' file!");

		}
	}

	/**
	 * get the relative path in original dir => remove the original dir path
	 */
	private static String relativePath(File referenceFile)
	{
		return referenceFile.getAbsolutePath().substring(originalDir.getAbsolutePath().length());
	}

	private static File find(File dir, String name) throws Exception
	{
		if (!dir.exists())
			return null;
		if (!dir.isDirectory())
			throw new Exception("The '" + dir.getAbsolutePath() + "' resource is not a directory!");
		File list[] = dir.listFiles();
		for (int i = 0; i < list.length; i++)
		{
			if (list[i].getName().equals(name))
				return list[i];
		}
		return null;
	}
}
