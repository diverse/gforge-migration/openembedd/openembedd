package org.openembedd.integration.bundles.build;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

/**
 * An empty action, in order to restart (and close) the workbench after the install
 * 
 * @author vmahe@irisa.fr
 * 
 */
public class Restart implements IApplication, IExecutableExtension
{

	public Object start(IApplicationContext context) throws Exception
	{
		System.out.println("Restart of the workbench to complete installation!");
		return null;
	}

	public void stop()
	{

	}

	public void setInitializationData(IConfigurationElement config, String propertyName, Object data)
			throws CoreException
	{
	// TODO Auto-generated method stub

	}

}
