package org.openembedd.integration.bundles.build;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.update.configuration.ILocalSite;
import org.eclipse.update.core.ICategory;
import org.eclipse.update.core.IFeature;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.ISite;
import org.eclipse.update.core.ISiteFeatureReference;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.standalone.InstallCommand;

/**
 * Class which get the all plugins from a given update site
 * 
 * @author vmahe@irisa.fr, Christian Brunette
 * 
 */
public class Updater implements IApplication, IExecutableExtension
{
	private String	updateSiteURL;
	private boolean	fullUpdate	= false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception
	{
		final String[] args = (String[]) context.getArguments().get("application.args");
		processArgs(args);
		System.out.println("Trying to install OpenEmbeDD plug-ins from update site: " + updateSiteURL);

		install();

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop()
	{}

	/**
	 * @param args
	 * @throws MalformedURLException
	 */
	private void processArgs(String[] args) throws MalformedURLException
	{
		for (int i = 0; i < args.length; i++)
		{

			if (args[i].equalsIgnoreCase("-updatesiteurl") && args.length >= i)
				updateSiteURL = args[i + 1];

			// optional parameter to take all features from all categories
			if (args[i].equalsIgnoreCase("-fullupdate"))
				fullUpdate = true;
		}
	}

	/**
	 * method which launch update wizard to install all update site features
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public boolean install() throws Exception
	{
		String problemsLogger = "";
		URL siteURL = null;
		try
		{
			siteURL = new URL(updateSiteURL);
		}
		catch (MalformedURLException e)
		{
			// explicit stack trace => nothing more to do
			e.printStackTrace();
		}
		ISite site = null;
		try
		{
			site = SiteManager.getSite(siteURL, new NullProgressMonitor());
		}
		catch (CoreException e)
		{
			problemsLogger
					.concat("\n------------------------------------------------------------------------------------------------------------------------------------------\n"
							+ "A PROBLEM OCCURS CONNECTING SITE " + updateSiteURL);
			problemsLogger.concat("\n         -->  " + e.getStackTrace()[0]);
		}
		ISiteFeatureReference siteFeatures[] = site.getFeatureReferences();

		boolean allFeaturesInstalled = false;
		int countdown = siteFeatures.length; // avoid infinite loop if unsatisfied dependencies
		int passNum = 0;

		while (!allFeaturesInstalled)
		{
			// need to know if one feature is not installed (because unsolved dependencies)
			allFeaturesInstalled = Boolean.TRUE;

			passNum += 1;
			System.out.println(">>>>>>>>> Pass = " + passNum + " <<<<<<<<<<");

			for (int i = 0; i < siteFeatures.length; i++)
			{
				// we install only the features we want in the bundle, except for Full Bundle
				if (isForBundle(siteFeatures[i]))
				{
					IFeature feature = null;
					try
					{
						feature = siteFeatures[i].getFeature(new NullProgressMonitor());
					}
					catch (CoreException e)
					{
						problemsLogger
								.concat("\n------------------------------------------------------------------------------------------------------------------------------------------\n"
										+ "A ''CoreException'' OCCURS WHEN GETTING INFORMATION ON FEATURE "
										+ siteFeatures[i].getVersionedIdentifier().getIdentifier());
						problemsLogger.concat("\n         -->  " + e.getStackTrace()[0]);
					}
					InstallCommand command;
					try
					{
						// TODO remplacer l'index par la variable......
						command = new InstallCommand(siteFeatures[i].getVersionedIdentifier().getIdentifier(),
							siteFeatures[i].getVersionedIdentifier().getVersion().toString(), updateSiteURL, Platform
									.getInstallLocation().getURL().getPath(), "false");
						command.run();
					}
					catch (Exception e)
					{
						problemsLogger
								.concat(" _n------------------------------------------------------------------------------------------------------------------------------------------\n"
										+ "A PROBLEM OCCURS DURING DOWNLOAD OF FEATURE "
										+ feature.getLabel()
										+ " PLUG-INS");
						problemsLogger.concat("\n         -->  " + e.getStackTrace()[0]);
					}
					// escape this test if a problem occured fetching a feature
					if (!isConfigured(feature) && feature != null)
					{
						allFeaturesInstalled = false;
						System.out.println(" ****** the feature << " + feature.getLabel()
								+ " >> has not been yet installed!");
					}
				}
			}
			// we only do loops for a quarter of total features number, as non problematic installs run in 3 or 4
			// passes.
			countdown -= 4;
			if (countdown <= 0)
			{
				System.out.println("=====================WARNING=========================\n"
						+ " The installation process met unsatisfied dependencies or problems!"
						+ "\n======================================================");
				System.out.println(problemsLogger);
				return false;
			}
		}
		System.out.println(">>>>> All features have been successfully installed <<<<<");
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org.eclipse.core.runtime.IConfigurationElement
	 * , java.lang.String, java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config, String propertyName, Object data)
			throws CoreException
	{
	// nothing to be done here
	}

	/**
	 * @param feature
	 * @return
	 * @throws CoreException
	 */
	private boolean isConfigured(IFeature feature) throws CoreException
	{
		if (feature == null)
			return false;
		ILocalSite localSite = SiteManager.getLocalSite();
		IFeatureReference configuredFeatures[] = localSite.getCurrentConfiguration().getConfiguredSites()[0]
				.getConfiguredFeatures();
		boolean result = false;
		for (int i = 0; i < configuredFeatures.length; i++)
		{
			if (configuredFeatures[i].getName().equals(feature.getLabel()))
			{
				result = true;
			}
		}
		return result;
	}

	/**
	 * @param reference
	 * @return
	 */
	private boolean isForBundle(ISiteFeatureReference reference)
	{
		if (fullUpdate) // force the result in case of desired full bundle
			return true;

		ICategory categories[] = reference.getCategories();
		for (int i = 0; i < categories.length; i++)
		{
			if (isOnTheExtraList(categories[i]))
				return false;
		}

		return true; // default answer
	}

	/**
	 * List of categories which must not be in an OpenEmbeDD bundle
	 * 
	 * @param category
	 * @return
	 */
	private boolean isOnTheExtraList(ICategory category)
	{
		if (category.getName().equalsIgnoreCase("Sources"))
			return true;
		if (category.getName().equalsIgnoreCase("Xtras"))
			return true;
		return false;
	}
}
