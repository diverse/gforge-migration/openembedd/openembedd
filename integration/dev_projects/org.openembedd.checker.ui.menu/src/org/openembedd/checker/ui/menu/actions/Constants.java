package org.openembedd.checker.ui.menu.actions;

import java.net.URL;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;
/**
 * @author openembedd
 */
public final class Constants {
	//plugin ID
	public static final String pluginID = "org.openembedd.ui.menu.actions";
	//others	
	public static final String nameIDViewChecking = "org.openembedd.checking.views.DownloadWebFileView";	
	public static final String slash = "/";
	public static final String configPathFileLoad = "platform:" + slash + "plugin" + slash;
	public static final String jarFileSuffix = ".jar";
	public static final String versionSep_ = "_";
	public static final String defaultDevicePath = Platform.getInstallLocation().getURL().getPath();
	
	//plugin
	private static final Bundle pluginBundle = Platform.getBundle(Constants.pluginID);
	private static final URL pluginURL = Platform.getBundle(Constants.pluginID).getEntry("/");
	
	//log
	private static final ILog checkingLog = Platform.getLog(Constants.getPluginBundle());
	
	public final static URL getPlugin() {
		return pluginURL;
	}
	public final static Bundle getPluginBundle() {
		return pluginBundle;
	}

	public final static void log(String message, Throwable exception ) {
		if (message == null || message.equals("")) message = "error";
		Status status = new Status(IStatus.ERROR, pluginID, 0, message, exception);
		Constants.checkingLog.log(status);
	}
}
