package org.openembedd.checker.action;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Shell;
import org.openembedd.checker.view.ButtonCellEditor.DownloadAction;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class DownloadJob extends Job {

	public enum etat { LOCKED, UNLOCKED };
	public etat lock = etat.LOCKED;  
	private DownloadAction[] downloadAction;
	private int lastDownloadActionRun = -1;
	private Shell shell;
	
	public DownloadJob(String name, Shell shell, DownloadAction[] downloadAction) {
		super(name);
		this.shell = shell;
		this.downloadAction = downloadAction;
	}

	public int getLastDownloadActionRun() {
		return lastDownloadActionRun;
	}
	
	public DownloadAction[] getDownloadAction() {
		return downloadAction;
	}
	
	public Shell getShell() {
		return shell;
	}
	
	public IStatus run(IProgressMonitor monitor) {
		try {
			monitor.beginTask(getName(), downloadAction.length + 1);
			for (int i = 0; i < downloadAction.length; i++) {
				monitor.setTaskName(downloadAction[i].getText() + " ...");
				monitor.worked(i + 1);
				Thread.sleep(100);
				if (monitor.isCanceled()) { 
					lock = etat.UNLOCKED;
					monitor.done();
					return Status.CANCEL_STATUS; 
					}
				downloadAction[i].run();
				lastDownloadActionRun = i + 1;
			}
		} catch (Exception e) {
			DownloadConstants.log("Error when trying to run download job " + getName(), e);
		}
		monitor.worked(downloadAction.length + 1);
		monitor.done();
		lock = etat.UNLOCKED;
		return Status.OK_STATUS;
	}
}
