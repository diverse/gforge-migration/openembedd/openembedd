package org.openembedd.checker.action;


/** $Id: CompatibilityChecker.java,v 1.2 2007-07-10 15:42:41 ffillion Exp $
 * Project   : Kermeta 
 * File      : CompatibilityChecker.java
 * License   : EPL
 * Copyright : IRISA / INRIA / Universite de Rennes 1
 * ----------------------------------------------------------------------------
 * Creation date : 20 nov. 06
 * Authors       : dvojtise <dvojtise.irisa.fr>
 * ----------------------------------------------------------------------------
 * Modified date : 08 june 07
 * Authors       : openembedd
 * what          : use for openembedd checker, getter methods for major, medium and minor component
 * ----------------------------------------------------------------------------   
 */


public abstract class CompatibilityChecker {

	/**
	 * Run all the verifications supported by this checker 
	 * note: launched from the start method of the plugin => must not fail !
	 */	
	public void check(){
		try{
			checkJavaVersion();
		}
		catch(Exception e){
			//launched from the start method of the plugin => must not fail !
			DownloadConstants.log("check java version failed", e);
			e.printStackTrace(System.err);
		}
	}

	/** 
	 * Checks that the current version of java is >= 1.5 
	 * message goes to both log4j and the errorlog view
	 */
	private void checkJavaVersion() {
		String javaVersion = System.getProperty("java.version");
		String[] versionsNumbers = javaVersion.split("\\.");
		Integer major = new Integer(versionsNumbers[0]);
		Integer medium = new Integer(versionsNumbers[1]);
		if(!(major>=1 && medium >=5)){
			/*
			KermetaPlugin.getDefault().getLog().log(new Status(Status.ERROR, "fr.irisa.triskell.kermeta",
                    Status.OK, 
                    "Your java version ("+javaVersion+") doesn't match the minimum required (java 5)", 
                    null));
			KermetaPlugin.getLogger().error("Your java version ("+javaVersion+") doesn't match the minimum required (java 5)");
			*/
			//adapted to checker logger code
			Exception e = new Exception("Your java version (" + javaVersion + ") doesn't match the minimum required (java 5)");
			DownloadConstants.log("error : check java version", e);
		}
	}
	
	/**
	 * method which return the actual jvm version of Eclipse instance
	 */
	static public String getVersionJavaPlatform() {
		return System.getProperty("java.version");
	}
	
	/**
	 * method which return the actual jvm major version of Eclipse instance
	 */
	static public String getMajorVersionJavaPlatform() {
		return getMajorVersionJava(getVersionJavaPlatform());
	}
	
	/**
	 * method which return a jvm major version
	 */
	static public String getMajorVersionJava(String version) {
		if (version != null) {
			String[] versionsNumbers = version.split("\\.");
			if (versionsNumbers != null && versionsNumbers.length > 0) {
				String major = versionsNumbers[0];
				return major;
			}
		}
		return null;
	}
	
	/**
	 * method which return the actual jvm medium version of Eclipse instance
	 */
	static public String getMediumVersionJavaPlatform() {
		return getMediumVersionJava(getVersionJavaPlatform());
	}
	
	/**
	 * method which return a jvm medium version
	 */
	static public String getMediumVersionJava(String version) {
		if (version != null) {
			String[] versionsNumbers = version.split("\\.");
			if (versionsNumbers != null && versionsNumbers.length > 1) {
				String medium = versionsNumbers[1];
				return medium;
			}
		}
		return null;
	}
	
	/**
	 * method which return the actual jvm minor version of Eclipse instance
	 */
	static public String getMinorVersionJavaPlatform() {
		return getMinorVersionJava(getVersionJavaPlatform());
	}
	
	/**
	 * method which return a jvm minor version
	 */
	static public String getMinorVersionJava(String version) {
		if (version != null) {
			String[] versionsNumbers = version.split("\\.");
			if (versionsNumbers != null && versionsNumbers.length > 2) {
				String minor = versionsNumbers[2];
				return minor;
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(CompatibilityChecker.getVersionJavaPlatform());
		System.out.println(CompatibilityChecker.getMajorVersionJavaPlatform());
		System.out.println(CompatibilityChecker.getMediumVersionJavaPlatform());
		System.out.println(CompatibilityChecker.getMinorVersionJavaPlatform());
	}
}

