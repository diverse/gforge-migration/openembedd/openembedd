package org.openembedd.checker.action;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public final class DownloadConstants {
	//plugin ID
	public static final String pluginCheckingID = "org.openembedd.checker";
	
	//images
	public static final String OE16x16 = "icon_openembedd16x16";
	public static final String OE32x32 = "icon_openembedd32x32";
	public static final String libraryType = "icon_libraryType";
	public static final String pluginType = "icon_pluginType";
	public static final String featureType = "icon_featureType";
	public static final String fileType = "icon_fileType";
	public static final String signedNo = "icon_signedNo";
	public static final String signedYes = "icon_signedYes";
	public static final String completeStatus = "icon_completeStatus";
	public static final String signedUnknown = "icon_signedUnknown";
	public static final String infoTsk = "icon_infoTsk";
	public static final String errorTsk = "icon_errorTsk";
	public static final String warningTsk = "icon_warnTsk";
	public static final String installHandler = "icon_InstallHandler";
	public static final String addObj = "icon_AddObj";
	public static final String eclipseIcon = "icon_Eclipse";
	
	//others
	public static final String slash = "/";
	public static final String configPathSave = "configuration" + slash + pluginCheckingID;
	public static final String configDirectorySaveModel = "model";
	public static final String configPathSaveModel = configPathSave + slash + configDirectorySaveModel;
	public static final String configDirectorySaveSettings = ".settings";
	public static final String configPathSaveSettings = configPathSave + slash + configDirectorySaveSettings;
	public static final String configSuffix = ".modelconfig";
	public static final String configFile = "openembedd";
	public static final String configSettings = "configuration.settings";
	public static final String configPathFileLoadOE = pluginCheckingID + slash + "model" + slash + configFile + configSuffix;
	public static final String configPathFileLoad = "platform:" + slash + "plugin" + slash;
	public static final String siteNameUpdate = "OpenEmbeDD"; 
	public static final String siteUpdateDefault = "http://openembedd.org/update";
	public static final String CTabItem_NONE = "None";
	public static final String CTabItem_DEFAULT_LABEL = "       ";
	public static final String jarFileSuffix = ".jar";
	public static final String versionSep_ = "_";
	public static final String labelStateRequired = "required";
	public static final String labelStateLessRecent = "less recent";
	public static final String labelStateMoreRecent = "more recent";
	public static final String labelStateBadJVM = "old jvm version";
	public static final String codeURLSpaceChar = "%20";
	public static final String defaultDevicePath = Platform.getInstallLocation().getURL().getPath();
	public static final String[] lsWebLicenseExt = {"htm", "html", "mht"};
	
	//messages and titles for dialog and wizard
	public static final String message_PROFIL_OK = "current configuration is OK";
	public static final String message_PROFIL_RESOURCE_ERROR = "libraries or resources are missing";
	public static final String message_PROFIL_JVM_ERROR = "too old version of Java Virtual Machine";
	public static final String message_PROFIL_WARNING = "libraries or resources installed have different versions";
	public static final String message_NO_MODEL = "no configuration model";
	public static final String message_doublet_state_installed = "Doublet for the State item with the ID 'installed'";
	public static final String message_doublet_state_uninstalled = "Doublet for the State item with the ID 'not installed'";
	public static final String message_doublet_action_install = "Doublet for the Action item with the ID 'install'";
	public static final String message_doublet_action_uninstall = "Doublet for the Action item with the ID 'uninstall'";
	public static final String title_doublet_error = "Doublet error in model";
	
	//titles dialog
	public static final String dialog_title_configuration = "Configuration"; 
	
	//names job for loading model
	public static final String progress_loading_configuration = "Configuration";
	
	//titles menu
	public static final String menu_title_choice_configuration = "Choose configuration";
	public static final String item_default_label_configuration = "<unknown>";
	
	//EMF model root
	public static final String model_root_configuration = "Configuration"; 
	
	//keys
	public static final String settingsName = "settings_configuration";
	public static final String keyCurrentConfiguration = "current_configuration";
	public static final String keyProfil = "profil";
	
	//EMF model types
	public static final String type_FEATURE = "Feature";
	public static final String type_PLUGIN = "Plugin";
	public static final String type_LIBRARY = "Library";
	public static final String type_EXE = "Exe";
	public static final String type_NONE = "None";
	
	//EMF model states
	public static final String state_INSTALLED = "installed";
	public static final String state_INSTALLED_DESC = "the resource is installed";
	public static final String state_NOT_INSTALLED = "not installed";
	public static final String state_NOT_INSTALLED_DESC = "the resource is not installed, press button to download it";
	
	//EMF model actions
	public static final String action_INSTALL = "install";
	public static final String action_INSTALL_DESC = "install the missing resource";
	public static final String action_javaClassDownloadAction = "DownloadAction";
	public static final String action_UNINSTALL = "uninstall";
	public static final String action_UNINSTALL_DESC = "uninstall the resource selected";

	//event command label
	public static final String event_COMMAND_DELETE = "Delete"; 
	
	//plugin
	private static final Bundle pluginBundle = Platform.getBundle(DownloadConstants.pluginCheckingID);
	private static final URL pluginURL = Platform.getBundle(DownloadConstants.pluginCheckingID).getEntry("/");
	
	//extension point
	private static final String extensionPointIDModelConfiguration = pluginCheckingID + "." + "configuration"; 
	public static final String extensionContributorName = "pluginID";
	public static final String extensionRegistryContributorName = "fragmentID";
	public static final String extensionID = "id";
	public static final String extensionLabel = "label";
	public static final String extensionModel = "model";
	public static final String extensionIcon = "icon";
	public static final String extensionLicensePath = "license_path";
	
	//version of jvm
	public static final String jvm = CompatibilityChecker.getVersionJavaPlatform();
	
	//log
	private static final ILog checkingLog = Platform.getLog(DownloadConstants.getPluginBundle());
	
	public static final ImageDescriptor getIconAddObjDescriptor() {
		try {
			return ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/add_feature.gif"));
		} catch (MalformedURLException e) {
			return null;
		}
	}
	public static final ImageDescriptor getIconViewConfigurationFile() {
		try {
			return ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/view_configuration_file.gif"));
		} catch (MalformedURLException e) {
			return null;
		}
	}
	public static final ImageDescriptor getIconCongigDescriptor() {
		try {
			return ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/configure.gif"));
		} catch (MalformedURLException e) {
			return null;
		}
	}
	public static final ImageDescriptor getIconBlank() {
		try {
			return ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/blank.gif"));
		} catch (MalformedURLException e) {
			return null;
		}
	}
	public final static URL getPlugin() {
		return pluginURL;
	}
	public final static Bundle getPluginBundle() {
		return pluginBundle;
	}
	public final static String getExtensionPointIDModelConfiguration() {
		return extensionPointIDModelConfiguration;
	}
	
	public final static void log(String message, Throwable exception ) {
		if (message == null || message.equals("")) message = "error";
		Status status = new Status(IStatus.ERROR, pluginCheckingID, 0, message, exception);
		DownloadConstants.checkingLog.log(status);
	}
	
	//basic features definition
	public static final String[] arrayBasicEclipseFeaturesID = {"org.eclipse.cvs", "org.eclipse.jdt", "org.eclipse.pde", "org.eclipse.platform", "org.eclipse.rcp", "org.eclipse.sdk"};
}