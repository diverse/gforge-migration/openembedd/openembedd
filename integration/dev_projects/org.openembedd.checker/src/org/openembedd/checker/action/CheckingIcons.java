package org.openembedd.checker.action;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class CheckingIcons {

	/**
	 * This class is a singleton.
	 */
	static private CheckingIcons instance = null;
	
	/**
	 * This is the icons registry : a name corresponds to an image.
	 */
	private Hashtable<String, Image> icons = new Hashtable<String, Image>();
	
	/**
	 * A private constructor which just call an initialize method. 
	 *
	 */
	private CheckingIcons() {
		initialize();
	}
	
	/**
	 * Loads some images whose name come from DownloadConstants class.
	 *
	 */
	private void initialize() {
		try {
			URL pluginURL = DownloadConstants.getPlugin();
			//load all the images here and put them in the Hashtable  
			Image icon_openembedd16x16 = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons/OpenEmbeDD16x16.png")).createImage();
			icons.put(DownloadConstants.OE16x16, icon_openembedd16x16);
			Image icon_openembedd32x32 = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons/OpenEmbeDD32x32.png")).createImage();
			icons.put(DownloadConstants.OE32x32, icon_openembedd32x32);
			Image icon_LibraryType = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/contents_view.gif")).createImage();
			icons.put(DownloadConstants.libraryType, icon_LibraryType);
			Image icon_PluginType = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/plugin_obj.gif")).createImage();
			icons.put(DownloadConstants.pluginType, icon_PluginType);
			Image icon_FeatureType = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/feature_obj.gif")).createImage();
			icons.put(DownloadConstants.featureType, icon_FeatureType);
			Image icon_FileType = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/file_obj.gif")).createImage();
			icons.put(DownloadConstants.fileType, icon_FileType);
			Image icon_SignedNo = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/signed_no.gif")).createImage();
			icons.put(DownloadConstants.signedNo, icon_SignedNo);
			Image icon_SignedYes = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/signed_yes.gif")).createImage();
			icons.put(DownloadConstants.signedYes, icon_SignedYes);
			Image icon_SignedUnknown = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/signed_unknown.gif")).createImage();
			icons.put(DownloadConstants.signedUnknown, icon_SignedUnknown);
			Image icon_InfoTsk = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/info_tsk.gif")).createImage();
			icons.put(DownloadConstants.infoTsk, icon_InfoTsk);
			Image icon_CompleteStatus = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/complete_status.gif")).createImage();
			icons.put(DownloadConstants.completeStatus, icon_CompleteStatus);
			Image icon_ErrorTsk = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/error_tsk.gif")).createImage();
			icons.put(DownloadConstants.errorTsk, icon_ErrorTsk);
			Image icon_WarningTsk = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/warn_tsk.gif")).createImage();
			icons.put(DownloadConstants.warningTsk, icon_WarningTsk);
			Image icon_InstallHandler = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons_checker/install-handler.gif")).createImage();
			icons.put(DownloadConstants.installHandler, icon_InstallHandler);
			Image icon_AddObj = DownloadConstants.getIconAddObjDescriptor().createImage();
			icons.put(DownloadConstants.addObj, icon_AddObj);
			Image icon_Eclipse = ImageDescriptor.createFromURL(new URL(pluginURL, "/icons/sample.gif")).createImage();
			icons.put(DownloadConstants.eclipseIcon, icon_Eclipse);

		} catch (MalformedURLException e) { 
			DownloadConstants.log("Error when initializing icons", e);
		}	
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	static public Image get(String path) {
		if ( instance == null )
			instance = new CheckingIcons();
		return (Image)instance.icons.get(path);
	}
}
