package org.openembedd.checker.action;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CommandActionDelegate;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.configuration.IInstallConfiguration;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.IImport;
import org.eclipse.update.core.IIncludedFeatureReference;
import org.eclipse.update.core.InstallMonitor;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.model.FeatureModel;
import org.eclipse.update.core.model.FeatureModelFactory;
import org.eclipse.update.core.model.ImportModel;
import org.eclipse.update.core.model.IncludedFeatureReferenceModel;
import org.eclipse.update.core.model.PluginEntryModel;
import org.eclipse.update.standalone.ListFeaturesCommand;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.Feature;
import org.openembedd.checker.modelConfig.State;
import org.openembedd.checker.modelConfig.modelConfigFactory;
import org.openembedd.checker.modelConfig.impl.FeatureImpl;
import org.openembedd.checker.modelConfig.provider.modelConfigEditPlugin;
import org.osgi.framework.Version;
/********************************************************************************************
 * Class ComputeAction (use class type Command)
 ******************************************************************************************** 
 * INRIA/IRISA  10/07/2007
 * @author openembedd
 * Copyright(c) 2007 OpenEmbeDD project 
 ********************************************************************************************
 * Action :
 * ComputeAction allows to compute the configuration model with required features.
 * After checking dependencies of features which part of the building model, this class 
 * adds required elements to finalize the model. After the user can remove or change these 
 * elements if necessary. Only the selected features are checked, all if the configuration 
 * root is selected and none if another elements are selected like "Action", "State", etc.
 * The features in work in the workspace are considered too.
 * Note: this dark code will be re-organized later with a better design, because the API is 
 * not still stable, it will almost certainly be broken (repeatedly) as the API evolves 
 * like specified in the API reference.
 ********************************************************************************************/
public class ComputeAction extends StaticSelectionCommandAction implements /*IEditorActionDelegate,*/ ISelectionChangedListener {
	/**
	 * This records the editing domain of the current editor or viewer.  For global
	 * popups, we try to determine the editing domain from the selected
	 * objects themselves.
	 */
	protected EditingDomain editingDomain;
	protected Command command;
	protected ISelectionProvider selectionProvider;
	protected List<Object> selectedObjects;
	protected Object descriptor;

	private List<org.eclipse.update.core.Feature> listFeaturesSiteLocal;
	private List<org.eclipse.update.core.Feature> listFeaturesProject;

	/**
	 * This constructs an instance for a command to be executed via
	 * workbenchPart's editing domain.
	 * @since 2.1.0
	 */
	public ComputeAction(IWorkbenchPart workbenchPart, ISelection selection) {
		super();
		initMenuLabel();
		initListFeaturesSiteLocal();
		if (workbenchPart instanceof IEditingDomainProvider) {
			editingDomain = ((IEditingDomainProvider)workbenchPart).getEditingDomain();
		}
		configureAction(selection);
	}

	public ComputeAction(IEditorPart editorPart, ISelection selection) {
		this((IWorkbenchPart)editorPart, selection);
	}

	/**
	 * This constructs an instance without a specified workbenchPart.
	 */
	public ComputeAction() {
		super();
		initMenuLabel();
		initListFeaturesSiteLocal();
	}

	/**
	 * Setting the menu title and icon
	 */
	private void initMenuLabel() {
		setText(modelConfigEditPlugin.INSTANCE.getString("_UI_ComputeAction_menu_item"));
		setImageDescriptor(DownloadConstants.getIconAddObjDescriptor());
	}

	/**
	 * Initialize the features list from the local site. //local site for the beginning 
	 * The Eclipse basic features are excluded.
	 */
	private void initListFeaturesSiteLocal() {
		listFeaturesSiteLocal = new ArrayList<org.eclipse.update.core.Feature>();
		List<String> listBasicEclipseFeaturesID = new ArrayList<String>();
		for (String s : DownloadConstants.arrayBasicEclipseFeaturesID) listBasicEclipseFeaturesID.add(s);
		try {
			//local features installed
			ListFeaturesCommand liste = new ListFeaturesCommand(null);
			IInstallConfiguration config = liste.getConfiguration();
			IConfiguredSite configSite[] = config.getConfiguredSites();
			for (int i = 0; i < configSite.length; i++) {
				IConfiguredSite site = configSite[i];
				IFeatureReference[] featureref = site.getConfiguredFeatures();
				for (int j = 0; j < featureref.length; j++) {
					IFeatureReference reference = featureref[j];
					org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
					String featureID = feature.getFeatureIdentifier();
					//basic features are excluded
					if (!listBasicEclipseFeaturesID.contains(featureID)) {
						listFeaturesSiteLocal.add(feature);
					}
				}
			}
		} catch (Exception e) {
			DownloadConstants.log("Error when checking the local features ", e);
		}
		return;
	}

	/**
	 * Update the features list with the features of the local site. 
	 * The Eclipse basic features are excluded.
	 */
	@SuppressWarnings("restriction")
	private void updateListFeaturesSiteLocalWithProject() {
		if (listFeaturesSiteLocal == null) listFeaturesSiteLocal = new ArrayList<org.eclipse.update.core.Feature>();
		if (listFeaturesProject == null) listFeaturesProject = new ArrayList<org.eclipse.update.core.Feature>();
		listFeaturesSiteLocal.removeAll(listFeaturesProject);
		listFeaturesProject.clear();
		//features projects
		IWorkspaceRoot wroot = ResourcesPlugin.getWorkspace().getRoot();
		IProject[] projects = wroot.getProjects();
		try {
			for (int i = 0; i < projects.length; i++) {
				IProject project = projects[i];
				if (project.isOpen() && project.getDescription().hasNature(org.eclipse.pde.internal.core.natures.PDE.FEATURE_NATURE)) {
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					URL url = project.findMember(org.eclipse.update.core.Feature.FEATURE_XML).getLocation().toFile().toURL();
					org.eclipse.update.core.Feature feature = new org.eclipse.update.core.Feature();
					FeatureModelFactory featuremodelfactory = new FeatureModelFactory();
					if (url != null) {
						FeatureModel model = featuremodelfactory.parseFeature(url.openStream());
						if (feature != null && model != null) {
							feature.setFeatureIdentifier(model.getFeatureIdentifier());
							feature.setFeatureVersion(model.getFeatureVersion());
							feature.setDiscoverySiteEntryModels(model.getDiscoverySiteEntryModels());
							feature.setImportModels(model.getImportModels());
							feature.setInstallHandlerModel(model.getInstallHandlerModel());
							feature.setPluginEntryModels(model.getPluginEntryModels());
							feature.setNonPluginEntryModels(model.getNonPluginEntryModels());
							feature.setUpdateSiteEntryModel(model.getUpdateSiteEntryModel());
							feature.setLicenseModel(model.getLicenseModel());
							feature.setCopyrightModel(model.getCopyrightModel());
							for(int fi = 0; fi < model.getFeatureIncluded().length; fi++) {
								model.getFeatureIncluded()[fi].setSite(SiteManager.getSite(project.getWorkspace().getRoot().getLocationURI().toURL(), null));
								feature.addIncludedFeatureReferenceModel((IncludedFeatureReferenceModel)model.getFeatureIncluded()[fi]);
							}
							listFeaturesSiteLocal.add(feature);
							listFeaturesProject.add(feature);
						}
					}
				}
			}
		} catch (Exception e) {
			DownloadConstants.log("Error when checking the feature projects", e);
		}
		return;
	}
	
	/**
	 * This creates the command "Compute"
	 */
	@SuppressWarnings("unchecked")
	protected Command createActionCommand(EditingDomain editingDomain, Collection<?> collection) {
		//Note : the parameter "collection" is not used here because selectedObjects contains the useful elements
		if (editingDomain != null && selectedObjects!= null && selectedObjects.size() > 0) {
			//refresh projects
			updateListFeaturesSiteLocalWithProject();

			//commands stack
			CompoundCommand computeCommand = new CompoundCommand();

			//initialization
			List<Object>[] selectedObjectsSorted = getSortedSelectedObjects();
			EReferenceImpl feature = (EReferenceImpl) modelConfigFactory.eINSTANCE.getmodelConfigPackage().getConfiguration_OwnedResource();

			for (int i = 0; i < selectedObjectsSorted.length; i++) {
				if (selectedObjectsSorted[i].size() == 0) continue;
				Object element = selectedObjectsSorted[i].get(0);
				Configuration owner = null;
				if (element instanceof Feature)
					owner = ((Feature)element).getOwner();
				if (element instanceof Configuration)
					owner = (Configuration)element;
				if (owner == null) continue;
				//selected features
				List<Feature> listSelectedFeature = new ArrayList<Feature>();
				if (selectedObjectsSorted[i].contains(owner)) {
					for (Iterator<?> iterator = owner.getOwnedResource().iterator(); iterator.hasNext();) {
						Object resource = (Object) iterator.next();
						if (resource instanceof Feature) {
							listSelectedFeature.add((Feature)resource);
						}
					}
				}
				else {
					listSelectedFeature.addAll((Collection<? extends Feature>) selectedObjectsSorted[i]);
				}
				//initial list 
				Hashtable<Object, String> featureLinkSiteUpdate = new Hashtable<Object, String>(); 
				List<org.eclipse.update.core.Feature> listRequiredFeature = getListInitialRequiredFeature(listSelectedFeature, featureLinkSiteUpdate);

				//creation of command
				if (listRequiredFeature != null && listRequiredFeature.size() > 0) {
					try {
						listRequiredFeature = getListRequiredFeature(listRequiredFeature, featureLinkSiteUpdate);
						if (listRequiredFeature == null) continue;
						AddCommand addObjCommand = null;
						try {
							for (Iterator<org.eclipse.update.core.Feature> iterator = listRequiredFeature.iterator(); iterator.hasNext();) {
								org.eclipse.update.core.Feature featureRequired = (org.eclipse.update.core.Feature) iterator.next();
								//check if the feature exists already in the model
								if (!isFeaturePartOfModel(featureRequired, owner)) {
									//initialize attributes
									String nameID = featureRequired.getFeatureIdentifier();
									String version = featureRequired.getFeatureVersion();
									String longName = nameID + DownloadConstants.versionSep_ + version;
									String nameWebFile = longName + DownloadConstants.jarFileSuffix;
									Version suffixOfName = new Version(version);
									String name = featureRequired.getFeatureIdentifier() + (suffixOfName == null ? "" : " " + suffixOfName.getMajor() + "." + suffixOfName.getMinor() + "." + suffixOfName.getMicro());
									String siteURL = featureLinkSiteUpdate.get(featureRequired);  
									Feature value = modelConfigFactory.eINSTANCE.createFeature();
									value.setNameID(nameID);
									value.setName(name);
									value.setNameWebFile(nameWebFile);
									value.setLongName(longName);
									value.setVersion(version);
									value.setIsLocalFile(false);
									value.setIsLocalLicense(false);
									value.setIsVisible(true);
									value.setJavaVMVersion(DownloadConstants.jvm);
									value.setSiteURL(siteURL);
									State stateNotInstalled = getStateModel(owner, DownloadConstants.state_NOT_INSTALLED);
									if (stateNotInstalled != null) {
										addObjCommand = (AddCommand) AddCommand.create(editingDomain, stateNotInstalled, (EReferenceImpl) modelConfigFactory.eINSTANCE.getmodelConfigPackage().getState_Resource(), (FeatureImpl)value);
										if (addObjCommand != null) computeCommand.append(addObjCommand);
									}
									addObjCommand = (AddCommand) AddCommand.create(editingDomain, owner, feature, (FeatureImpl)value);
									if (addObjCommand != null) computeCommand.append(addObjCommand);
								}
							}
						} catch (Exception e) {
							DownloadConstants.log("Error to create compute command", e);
						}
					} catch (CoreException e) {
						DownloadConstants.log("Error checking features", e);
					}
				}
			}
			return computeCommand;
		}
		return UnexecutableCommand.INSTANCE;
	}

	/**
	 * Method returns true if the feature is already in the model of configuration
	 * The matching of version is equal
	 */
	protected boolean isFeaturePartOfModel(org.eclipse.update.core.Feature feature, Configuration configuration) {
		String nameID = feature.getFeatureIdentifier();
		String version = feature.getFeatureVersion();
		for (Iterator<?> iterator = configuration.getOwnedResource().iterator(); iterator.hasNext();) {
			Object resource = (Object) iterator.next();
			if (resource instanceof Feature) {
				Feature featureModel = (Feature)resource;
				if (featureModel.getNameID()  != null && featureModel.getNameID().equals(nameID) &&
					featureModel.getVersion() != null && featureModel.getVersion().equals(version)
					)
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Method to get the initial State of a Feature resource 
	 */
	protected State getStateModel(Configuration configuration, String nameID) {
		if (configuration != null && nameID != null && !nameID.trim().equals("")) {
			for (Iterator<?> iterator = configuration.getOwnedState().iterator(); iterator.hasNext();) {
				State state = (State) iterator.next();
				if (state.getNameID() != null && state.getNameID().equals(nameID)) {
					return state;
				}
			}
		}
		return null;
	}
	
	/**
	 * Method which initializes the required features list with Eclipse object org.eclipse.update.core.Feature 
	 */
	protected List<org.eclipse.update.core.Feature> getListInitialRequiredFeature(List<Feature> listSelectedFeature, Hashtable<Object, String> featureLinkSiteUpdate) {
		List<org.eclipse.update.core.Feature> listRequiredFeature = null;
		if (featureLinkSiteUpdate == null) featureLinkSiteUpdate = new Hashtable<Object, String>();
		if (listSelectedFeature != null && listSelectedFeature.size() > 0 && listFeaturesSiteLocal != null && listFeaturesSiteLocal.size() > 0) {
			listRequiredFeature = new ArrayList<org.eclipse.update.core.Feature>();
			for (Iterator<Feature> iterator = listSelectedFeature.iterator(); iterator.hasNext();) {
				Feature selectedFeature = (Feature) iterator.next();
				for (Iterator<org.eclipse.update.core.Feature> iterFeatureObj = listFeaturesSiteLocal.iterator(); iterFeatureObj.hasNext();) {
					org.eclipse.update.core.Feature featureObj = (org.eclipse.update.core.Feature) iterFeatureObj.next();
					if (selectedFeature.getNameID() != null && selectedFeature.getNameID().equals(featureObj.getFeatureIdentifier())) {
						if (selectedFeature.getVersion() != null && !selectedFeature.getVersion().trim().equals("") && !selectedFeature.getVersion().trim().equals("0.0.0")) {
							Version versionFeatureObj = new Version(featureObj.getFeatureVersion());
							Version versionFeatureModel = new Version(selectedFeature.getVersion());
							if (versionFeatureObj.compareTo(versionFeatureModel) >= 0) {
								listRequiredFeature.add(featureObj);
								String siteUpdateURL = (selectedFeature.getSiteURL() != null && !selectedFeature.getSiteURL().trim().equals("")) ? selectedFeature.getSiteURL() : (featureObj.getUpdateSiteEntryModel() != null) ? featureObj.getUpdateSiteEntryModel().getURLString() : "";
								if (siteUpdateURL != null) try {new URL(siteUpdateURL);} catch (MalformedURLException e) {siteUpdateURL = (featureObj.getUpdateSiteEntryModel() != null) ? featureObj.getUpdateSiteEntryModel().getURLString() : "";}
								featureLinkSiteUpdate.put(featureObj, siteUpdateURL);
								break;
							}
						}
						else {
							listRequiredFeature.add(featureObj);
							break;
						}
					}
				}
			}
		}
		return listRequiredFeature;
	}
	
	/**
	 * Method which add all the required features to the list.
	 * Note : this is the simplest algorithm : "first found are taken",
	 *        but can be to optimize thereafter with an holistic algorithm
	 * @throws CoreException 
	 */
	@SuppressWarnings("deprecation")
	protected List<org.eclipse.update.core.Feature> getListRequiredFeature(List<org.eclipse.update.core.Feature> listInitialRequiredFeature, Hashtable<Object, String> featureLinkSiteUpdate) throws CoreException {
		List<org.eclipse.update.core.Feature> listRequiredFeature = listInitialRequiredFeature;
		if (featureLinkSiteUpdate == null) featureLinkSiteUpdate = new Hashtable<Object, String>();
		if (listRequiredFeature != null && listRequiredFeature.size() > 0 && listFeaturesSiteLocal != null && listFeaturesSiteLocal.size() > 0) {
			List<org.eclipse.update.core.Feature> listTempRequiredFeature = new ArrayList<org.eclipse.update.core.Feature>();
			Hashtable<org.eclipse.update.core.Feature, Boolean> checkFeatureResolved = new Hashtable<org.eclipse.update.core.Feature, Boolean>();
			for (Iterator<org.eclipse.update.core.Feature> iterator = listRequiredFeature.iterator(); iterator.hasNext();) {
				org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
				checkFeatureResolved.put(feature, Boolean.FALSE);
				if (featureLinkSiteUpdate.get(feature) == null) {
					String siteUpdateURL = (feature.getUpdateSiteEntryModel() != null) ? feature.getUpdateSiteEntryModel().getURLString() : "";
					featureLinkSiteUpdate.put(feature, siteUpdateURL);
				}
			}
			boolean noFeatureToCheck = true;
			//main loop of checking
			while(noFeatureToCheck) {
				noFeatureToCheck = false;
				for (Iterator<org.eclipse.update.core.Feature> iterator = listRequiredFeature.iterator(); iterator.hasNext();) {
					org.eclipse.update.core.Feature featureRequired = (org.eclipse.update.core.Feature) iterator.next();
					if (!checkFeatureResolved.get(featureRequired).booleanValue()) {
						String siteUpdateURL = featureLinkSiteUpdate.get(featureRequired);
						//check required elements 
						ImportModel iimportElts[] = featureRequired.getImportModels();
						for (int i = 0; i < iimportElts.length; i++) {
							org.eclipse.update.core.Feature firstFeature = getFirstFeatureForImport(iimportElts[i]);
							if (firstFeature != null && !listRequiredFeature.contains(firstFeature) && !listTempRequiredFeature.contains(firstFeature)) {
								listTempRequiredFeature.add(firstFeature);
								if (featureLinkSiteUpdate.get(firstFeature) == null) {
									featureLinkSiteUpdate.put(firstFeature, siteUpdateURL);
								}
							}
						}
						/** 
						 * here you can disable the check of included features
						 */
						//check included features
						if (featureRequired instanceof org.eclipse.update.core.Feature) {
							if (featureRequired.getIncludedFeatureReferences().length > 0) {
								IIncludedFeatureReference iincludedFeatures[] = featureRequired.getIncludedFeatureReferences();
								for (int i = 0; i < iincludedFeatures.length; i++) {
									IIncludedFeatureReference iincludedFeature = iincludedFeatures[i];
									org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature)iincludedFeature.getFeature(null);
									if (feature != null && !listRequiredFeature.contains(feature) && !listTempRequiredFeature.contains(feature)) {
										listTempRequiredFeature.add(feature);
										if (featureLinkSiteUpdate.get(feature) == null) {
											featureLinkSiteUpdate.put(feature, siteUpdateURL);
										}
									}
								}
							}
							else if (featureRequired.getFeatureIncluded().length > 0) {
								IIncludedFeatureReference iincludedFeatures[] = featureRequired.getFeatureIncluded();
								for (int i = 0; i < iincludedFeatures.length; i++) {
									IIncludedFeatureReference iincludedFeature = iincludedFeatures[i];
									org.eclipse.update.core.Feature feature = getFeatureInLocalList(iincludedFeature.getVersionedIdentifier().getIdentifier(), iincludedFeature.getVersionedIdentifier().getVersion().toString(), false);
									if (feature != null && !listRequiredFeature.contains(feature) && !listTempRequiredFeature.contains(feature)) {
										listTempRequiredFeature.add(feature);
										if (featureLinkSiteUpdate.get(feature) == null) {
											featureLinkSiteUpdate.put(feature, siteUpdateURL);
										}
									}
								}
							}
						}
						/**/
						//mark feature checked
						checkFeatureResolved.put(featureRequired, Boolean.TRUE);
					}
				}
				//erase buffer
				if (listTempRequiredFeature.size() > 0) {
					noFeatureToCheck = true;
					listRequiredFeature.addAll(listTempRequiredFeature);
					for (Iterator<org.eclipse.update.core.Feature> iterator = listTempRequiredFeature.iterator(); iterator.hasNext();) {
						org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
						checkFeatureResolved.put(feature, Boolean.FALSE);
					}
					listTempRequiredFeature.clear();
				}
			}
		}
		return listRequiredFeature;
	}
	
	/**
	 * Method which returns the feature object from the local list
	 */
	protected org.eclipse.update.core.Feature getFeatureInLocalList(String featureIdentifier, String version, boolean ignoreversion) {
		if (listFeaturesSiteLocal != null && listFeaturesSiteLocal.size() > 0) {
			for (Iterator<org.eclipse.update.core.Feature> iterator = listFeaturesSiteLocal.iterator(); iterator.hasNext();) {
				org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
				if (feature.getFeatureIdentifier().equals(featureIdentifier) && (feature.getFeatureVersion().equals(version) || ignoreversion))
					return feature;
			}
		}
		return null;
	}
	
	/**
	 * Simple algorithm of "first found first taken"
	 * @param iimport
	 * @return
	 */
	@SuppressWarnings("deprecation")
	protected org.eclipse.update.core.Feature getFirstFeatureForImport(ImportModel iimport) {
		if (iimport == null) {
			return null;
		}
		int kind = iimport.isFeatureImport() ? IImport.KIND_FEATURE : IImport.KIND_PLUGIN;
		switch (kind) {
		case IImport.KIND_FEATURE:
			for (Iterator<org.eclipse.update.core.Feature> iterator = listFeaturesSiteLocal.iterator(); iterator.hasNext();) {
				org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
				if (isImportInFeature(iimport, feature)) {
					return feature;
				}
			}
			break;
		case IImport.KIND_PLUGIN:
			//first time (optimization) : small improvement to recover the most probable feature by checking if there is one of the same na
			for (Iterator<org.eclipse.update.core.Feature> iterator = listFeaturesSiteLocal.iterator(); iterator.hasNext();) {
				org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
				if (feature.getFeatureIdentifier().equals(iimport.getIdentifier()) && isImportInFeature(iimport, feature)) {
					return feature;
				}
			}
			//second time : choose first feature deploying the plugin
			for (Iterator<org.eclipse.update.core.Feature> iterator = listFeaturesSiteLocal.iterator(); iterator.hasNext();) {
				org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iterator.next();
				if (isImportInFeature(iimport, feature)) {
					return feature;
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns true if the import (feature or plugin) is deploying by the feature 
	 * @param iimport
	 * @return
	 */
	protected boolean isImportInFeature(ImportModel iimport, org.eclipse.update.core.Feature feature) {
		if (iimport == null || feature == null) {
			return false;
		}
		int kind = iimport.isFeatureImport() ? IImport.KIND_FEATURE : IImport.KIND_PLUGIN;
		switch (kind) {
		case IImport.KIND_FEATURE:
			if (feature.getFeatureIdentifier().equals(iimport.getIdentifier())) {
				if (feature.getFeatureVersion().equals("0.0.0"))
					return true;
				//TODO use feature rule matching
				String versionIdImport = iimport.getVersion();
				if (versionIdImport != null && !versionIdImport.trim().equals("")) {
					Version versionFeature = new Version(feature.getFeatureVersion());
					Version versionImport = new Version(versionIdImport.toString());
					if (versionFeature.compareTo(versionImport) >= 0) {
						return true;
					}
				}
				else {
					return true;
				}
			}			
			break;
		case IImport.KIND_PLUGIN:
			PluginEntryModel ipluginEntries[] = feature.getPluginEntryModels();
			for (int i = 0; i < ipluginEntries.length; i++) {
				PluginEntryModel pluginEntry = ipluginEntries[i]; 
				if (pluginEntry.getPluginIdentifier().equals(iimport.getIdentifier())) {
					String versionIdImport = iimport.getVersion();
					if (versionIdImport != null && !versionIdImport.trim().equals("")) {
						Version versionFeature = new Version(feature.getFeatureVersion());
						Version versionImport = new Version(versionIdImport.toString());
						if (versionFeature.compareTo(versionImport) >= 0) {
							return true;
						}
					}
					else {
						return true;
					}
				}
			}			
			break;
		}
		return false;
	}
	
	/**
	 * This extracts the objects from selection, invokes createActionCommand
	 * to create the command, and then configures the action based on the
	 * result.
	 */
	@SuppressWarnings("unchecked")
	public void configureAction(ISelection selection) {
		// only handle structured selections
		if (!(selection instanceof IStructuredSelection)) {
			disable();
		}
		else {
			// convert the selection to a collection of the selected objects
			IStructuredSelection sselection = (IStructuredSelection) selection;
			Collection collection = new ArrayList(sselection.size());
			for (Iterator i = sselection.iterator(); i.hasNext(); ) {
				collection.add(i.next());
			}

			// if the editing domain wasn't given by the workbench part, try to get
			// it from the selection
			for (Iterator i = collection.iterator();
			editingDomain == null && i.hasNext(); ) {
				Object o = i.next();
				editingDomain = AdapterFactoryEditingDomain.getEditingDomainFor(o);
			}

			// if we found an editing domain, create command
			if (editingDomain != null) {
				command = createActionCommand(editingDomain, collection);
				setEnabled(command.canExecute());
			}

			// give up if we couldn't create the command; otherwise, use a
			// CommandActionDelegate to set the action's text, tooltip, icon,
			// etc. or just use the default icon
			if (command == null || command == UnexecutableCommand.INSTANCE) {
				disable();
			}
			else if (!(command instanceof CommandActionDelegate)) {
				if (getDefaultImageDescriptor() != null) {
					setImageDescriptor(getDefaultImageDescriptor());
				}
			}
			else {
				CommandActionDelegate commandActionDelegate =
					(CommandActionDelegate) command;

				ImageDescriptor imageDescriptor =
					objectToImageDescriptor(commandActionDelegate.getImage());
				if (imageDescriptor != null) {
					setImageDescriptor(imageDescriptor);
				}
				else if (getDefaultImageDescriptor() != null) {
					setImageDescriptor(getDefaultImageDescriptor());
				}

				if (commandActionDelegate.getText() != null) {
					setText(commandActionDelegate.getText());
				}

				if (commandActionDelegate.getDescription() != null) {
					setDescription(commandActionDelegate.getDescription());
				}

				if (commandActionDelegate.getToolTipText() != null) {
					setToolTipText(commandActionDelegate.getToolTipText());
				}
			}
		}
	}

	/**
	 * This can be overridden to provide the image descriptor used when the
	 * command does not provide one.  This implementation simply returns null.
	 */
	protected ImageDescriptor getDefaultImageDescriptor() {
		return null;
	}

	/**
	 * This gets invoked when the selection is inappropriate or the command
	 * cannot be created.  It puts the action in the correct state for such
	 * error conditions.  This implementation disables the action and sets its
	 * icon to the default.
	 */
	protected void disable() {
		setEnabled(false);
		if (getDefaultImageDescriptor() != null) {
			setImageDescriptor(getDefaultImageDescriptor());
		}
	}

	/**
	 * This executes the command.
	 */
	public void run() {
		// this guard is for extra security, but should not be necessary
		if (editingDomain != null && command != null) {
			// use up the command
			editingDomain.getCommandStack().execute(command);
		}
	}

	/**
	 * If necessary, this converts any image representation into an image
	 * descriptor.
	 */
	protected ImageDescriptor objectToImageDescriptor(Object object) {
		return ExtendedImageRegistry.getInstance().getImageDescriptor(object);
	}

	/**
	 * Each time the selection changed  
	 */
	@SuppressWarnings("unchecked")
	public void selectionChanged(SelectionChangedEvent event) {
		selectionProvider = event.getSelectionProvider();
		ISelection selection = event.getSelection();
		if (!(selection instanceof IStructuredSelection)) {
			disable();
		}
		else {
			// convert the selection to a collection of the selected objects
			IStructuredSelection sselection = (IStructuredSelection) selection;
			if (updateSelection(sselection)) {
				Collection collection = new ArrayList(sselection.size());
				for (Iterator i = sselection.iterator(); i.hasNext(); ) {
					collection.add(i.next());
				}
				if (editingDomain != null && selectedObjects!= null && selectedObjects.size() > 0) {
					command = createActionCommand(editingDomain, collection);
					setEnabled(command.canExecute());
				}				
			}
			else {
				disable();
			}
		}
	}

	/**
	 * @param selection
	 * @return
	 */
	public boolean updateSelection(IStructuredSelection selection) {
		boolean eObjectElement = true;
		selectedObjects = new ArrayList<Object>();
		for (Iterator<?> objects = selection.iterator(); objects.hasNext(); ) {
			Object element = AdapterFactoryEditingDomain.unwrap(objects.next());
			if (element instanceof EObject) {
				if (element instanceof Configuration || element instanceof Feature /*|| element instanceof Plugin*/) {
					selectedObjects.add(element);	
				}
			}
			else {
				eObjectElement = false;
				break;
			}
		}
		return selectedObjects.size() > 0 && eObjectElement;
	}

	/**
	 * Method to sort the selected elements collection to manage a multi-configuration model
	 */
	@SuppressWarnings("unchecked")
	public List<Object>[] getSortedSelectedObjects() {
		if (editingDomain != null && selectedObjects!= null && selectedObjects.size() > 0) {
			//manage several configuration
			List<Configuration> owners = new ArrayList<Configuration>();
			for (Iterator<Resource> i = editingDomain.getResourceSet().getResources().iterator(); i.hasNext(); ) {
				Resource resource = (Resource)i.next();
				for (Iterator<EObject> iter = resource.getContents().iterator(); iter.hasNext();) {
					Object element = (Object) iter.next();
					if (element instanceof Configuration) {
						Configuration configuration = (Configuration)element;
						if (!owners.contains(configuration)) owners.add(configuration);
					}
					if (element instanceof Feature) {
						Configuration configuration = (Configuration)((Feature)element).getOwner();
						if (!owners.contains(configuration)) owners.add(configuration);
					}
				}
			}
			//owners known : sorting selected elements
			if (owners.size() > 0) {
				int iowner = 0;
				List<Object>[] selectedObjectsSorted = new ArrayList[owners.size()];
				for (Iterator<Configuration> iterowner = owners.iterator(); iterowner.hasNext();) {
					Configuration owner = (Configuration) iterowner.next();
					selectedObjectsSorted[iowner] = new ArrayList<Object>();
					for (Iterator<Object> iterelement = selectedObjects.iterator(); iterelement.hasNext();) {
						Object element = (Object) iterelement.next();
						if (element instanceof Feature && ((Feature)element).getOwner() != null) {
							if (((Feature)element).getOwner().equals(owner)) {
								selectedObjectsSorted[iowner].add(element);
							}
						}
						if (element instanceof Configuration) {
							if (((Configuration)element).equals(owner)) {
								selectedObjectsSorted[iowner].add(element);
							}
						}
					}
					iowner++;
				}
				return selectedObjectsSorted;
			}
		}
		return null;
	}
	

	/**
	 * @since 2.1.0
	 */
	public void setActiveWorkbenchPart(IWorkbenchPart workbenchPart) {
		if (workbenchPart instanceof IEditingDomainProvider) {
			editingDomain = ((IEditingDomainProvider)workbenchPart).getEditingDomain();
		}
	}
};