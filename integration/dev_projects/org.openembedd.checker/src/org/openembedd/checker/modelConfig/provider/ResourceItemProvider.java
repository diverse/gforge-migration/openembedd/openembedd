/**
 * <copyright>
 * </copyright>
 *
 * $Id: ResourceItemProvider.java,v 1.5 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.openembedd.checker.modelConfig.Resource;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * This is the item provider adapter for a {@link org.openembedd.checker.modelConfig.Resource} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ResourceItemProvider
	extends ModelElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCategoryPropertyDescriptor(object);
			addProfilPropertyDescriptor(object);
			addStatePropertyDescriptor(object);
			addNameWebFilePropertyDescriptor(object);
			addSiteURLPropertyDescriptor(object);
			addEntryPropertyDescriptor(object);
			addIsLocalFilePropertyDescriptor(object);
			addTargetNameFilePropertyDescriptor(object);
			addTargetPathFilePropertyDescriptor(object);
			addIsLocalLicensePropertyDescriptor(object);
			addLicenseNamePropertyDescriptor(object);
			addLicenseURLPropertyDescriptor(object);
			addLicensePathFilePropertyDescriptor(object);
			addGroupPropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addRequiredPropertyDescriptor(object);
			addRequirePropertyDescriptor(object);
			addIsVisiblePropertyDescriptor(object);
			addIncludesInPropertyDescriptor(object);
			addJavaVMVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Category feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCategoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_category_feature"),
				 getString("_UI_Resource_category_description"),
				 modelConfigPackage.Literals.RESOURCE__CATEGORY,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_bindingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Profil feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProfilPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_profil_feature"),
				 getString("_UI_Resource_profil_description"),
				 modelConfigPackage.Literals.RESOURCE__PROFIL,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_bindingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the State feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_state_feature"),
				 getString("_UI_Resource_state_description"),
				 modelConfigPackage.Literals.RESOURCE__STATE,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_statePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Name Web File feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNameWebFilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_nameWebFile_feature"),
				 getString("_UI_Resource_nameWebFile_description"),
				 modelConfigPackage.Literals.RESOURCE__NAME_WEB_FILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_webPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Site URL feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSiteURLPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_siteURL_feature"),
				 getString("_UI_Resource_siteURL_description"),
				 modelConfigPackage.Literals.RESOURCE__SITE_URL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_webPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Entry feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEntryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_entry_feature"),
				 getString("_UI_Resource_entry_description"),
				 modelConfigPackage.Literals.RESOURCE__ENTRY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_webPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Local File feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsLocalFilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_isLocalFile_feature"),
				 getString("_UI_Resource_isLocalFile_description"),
				 modelConfigPackage.Literals.RESOURCE__IS_LOCAL_FILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Name File feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetNameFilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_targetNameFile_feature"),
				 getString("_UI_Resource_targetNameFile_description"),
				 modelConfigPackage.Literals.RESOURCE__TARGET_NAME_FILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Path File feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPathFilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_targetPathFile_feature"),
				 getString("_UI_Resource_targetPathFile_description"),
				 modelConfigPackage.Literals.RESOURCE__TARGET_PATH_FILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Local License feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsLocalLicensePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_isLocalLicense_feature"),
				 getString("_UI_Resource_isLocalLicense_description"),
				 modelConfigPackage.Literals.RESOURCE__IS_LOCAL_LICENSE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the License Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLicenseNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_licenseName_feature"),
				 getString("_UI_Resource_licenseName_description"),
				 modelConfigPackage.Literals.RESOURCE__LICENSE_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the License URL feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLicenseURLPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_licenseURL_feature"),
				 getString("_UI_Resource_licenseURL_description"),
				 modelConfigPackage.Literals.RESOURCE__LICENSE_URL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_webPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the License Path File feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLicensePathFilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_licensePathFile_feature"),
				 getString("_UI_Resource_licensePathFile_description"),
				 modelConfigPackage.Literals.RESOURCE__LICENSE_PATH_FILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Group feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGroupPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_group_feature"),
				 getString("_UI_Resource_group_description"),
				 modelConfigPackage.Literals.RESOURCE__GROUP,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_bindingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_description_feature"),
				 getString("_UI_Resource_description_description"),
				 modelConfigPackage.Literals.RESOURCE__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Required feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequiredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_required_feature"),
				 getString("_UI_Resource_required_description"),
				 modelConfigPackage.Literals.RESOURCE__REQUIRED,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_linkingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Require feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequirePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_require_feature"),
				 getString("_UI_Resource_require_description"),
				 modelConfigPackage.Literals.RESOURCE__REQUIRE,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_linkingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Visible feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_isVisible_feature"),
				 getString("_UI_Resource_isVisible_description"),
				 modelConfigPackage.Literals.RESOURCE__IS_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Includes In feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncludesInPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_includesIn_feature"),
				 getString("_UI_Resource_includesIn_description"),
				 modelConfigPackage.Literals.RESOURCE__INCLUDES_IN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_webPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Java VM Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addJavaVMVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Resource_javaVMVersion_feature"),
				 getString("_UI_Resource_javaVMVersion_description"),
				 modelConfigPackage.Literals.RESOURCE__JAVA_VM_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_commonPropertyCategory"),
				 null));
	}

	/**
	 * This returns Resource.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getImage(Object object) {
		//return overlayImage(object, getResourceLocator().getImage("full/obj16/Resource"));
		return overlayImage(object, getResourceLocator().getImage("../icons_checker/resource_persp"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Resource)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Resource_type") :
			getString("_UI_Resource_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Resource.class)) {
			case modelConfigPackage.RESOURCE__NAME_WEB_FILE:
			case modelConfigPackage.RESOURCE__SITE_URL:
			case modelConfigPackage.RESOURCE__ENTRY:
			case modelConfigPackage.RESOURCE__IS_LOCAL_FILE:
			case modelConfigPackage.RESOURCE__TARGET_NAME_FILE:
			case modelConfigPackage.RESOURCE__TARGET_PATH_FILE:
			case modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE:
			case modelConfigPackage.RESOURCE__LICENSE_NAME:
			case modelConfigPackage.RESOURCE__LICENSE_URL:
			case modelConfigPackage.RESOURCE__LICENSE_PATH_FILE:
			case modelConfigPackage.RESOURCE__DESCRIPTION:
			case modelConfigPackage.RESOURCE__IS_VISIBLE:
			case modelConfigPackage.RESOURCE__INCLUDES_IN:
			case modelConfigPackage.RESOURCE__JAVA_VM_VERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return modelConfigEditPlugin.INSTANCE;
	}

}
