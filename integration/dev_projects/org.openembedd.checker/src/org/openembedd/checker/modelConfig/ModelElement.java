/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElement.java,v 1.3 2007-07-10 15:42:34 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.ModelElement#getName <em>Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.ModelElement#getNameID <em>Name ID</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getModelElement()
 * @model
 * @generated
 */
public interface ModelElement extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getModelElement_Name()
	 * @model dataType="org.openembedd.checking.modelConfig.String" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.ModelElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name ID</em>' attribute.
	 * @see #setNameID(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getModelElement_NameID()
	 * @model dataType="org.openembedd.checking.modelConfig.String" required="true"
	 * @generated
	 */
	String getNameID();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.ModelElement#getNameID <em>Name ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name ID</em>' attribute.
	 * @see #getNameID()
	 * @generated
	 */
	void setNameID(String value);

} // ModelElement