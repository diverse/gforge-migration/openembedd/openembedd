/**
 * <copyright>
 * </copyright>
 *
 * $Id: FeatureImpl.java,v 1.11 2007-12-20 17:50:29 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Dictionary;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ui.PlatformUI;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.configuration.IInstallConfiguration;
import org.eclipse.update.configuration.ILocalSite;
import org.eclipse.update.core.IFeature;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.IImport;
import org.eclipse.update.core.IIncludedFeatureReference;
import org.eclipse.update.core.IPluginEntry;
import org.eclipse.update.core.ISite;
import org.eclipse.update.core.ISiteFeatureReference;
import org.eclipse.update.core.Import;
import org.eclipse.update.core.InstallMonitor;
import org.eclipse.update.core.PluginEntry;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.VersionedIdentifier;
import org.eclipse.update.internal.configurator.UpdateURLDecoder;
import org.eclipse.update.internal.search.SiteSearchCategory;
import org.eclipse.update.search.BackLevelFilter;
import org.eclipse.update.search.UpdateSearchRequest;
import org.eclipse.update.search.UpdateSearchScope;
import org.eclipse.update.standalone.ListFeaturesCommand;
import org.eclipse.update.ui.UpdateJob;
import org.eclipse.update.ui.UpdateManagerUI;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.modelConfig.Feature;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.FeatureImpl#getLongName <em>Long Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.FeatureImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.FeatureImpl#getToDecompress <em>To Decompress</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FeatureImpl extends ResourceImpl implements Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The default value of the '{@link #getLongName() <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongName()
	 * @generated
	 * @ordered
	 */
	protected static final String LONG_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLongName() <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongName()
	 * @generated
	 * @ordered
	 */
	protected String longName = LONG_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getToDecompress() <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDecompress()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_DECOMPRESS_EDEFAULT= Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getToDecompress() <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDecompress()
	 * @generated
	 * @ordered
	 */
	protected Boolean toDecompress = TO_DECOMPRESS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongName(String newLongName) {
		String oldLongName = longName;
		longName = newLongName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.FEATURE__LONG_NAME, oldLongName, longName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.FEATURE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToDecompress() {
		return toDecompress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToDecompress(Boolean newToDecompress) {
		Boolean oldToDecompress = toDecompress;
		toDecompress = newToDecompress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.FEATURE__TO_DECOMPRESS, oldToDecompress, toDecompress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case modelConfigPackage.FEATURE__LONG_NAME:
			return getLongName();
		case modelConfigPackage.FEATURE__VERSION:
			return getVersion();
		case modelConfigPackage.FEATURE__TO_DECOMPRESS:
			return getToDecompress();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case modelConfigPackage.FEATURE__LONG_NAME:
			setLongName((String)newValue);
			return;
		case modelConfigPackage.FEATURE__VERSION:
			setVersion((String)newValue);
			return;
		case modelConfigPackage.FEATURE__TO_DECOMPRESS:
			setToDecompress((Boolean)newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case modelConfigPackage.FEATURE__LONG_NAME:
			setLongName(LONG_NAME_EDEFAULT);
			return;
		case modelConfigPackage.FEATURE__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case modelConfigPackage.FEATURE__TO_DECOMPRESS:
			setToDecompress(TO_DECOMPRESS_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case modelConfigPackage.FEATURE__LONG_NAME:
			return LONG_NAME_EDEFAULT == null ? longName != null : !LONG_NAME_EDEFAULT.equals(longName);
		case modelConfigPackage.FEATURE__VERSION:
			return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		case modelConfigPackage.FEATURE__TO_DECOMPRESS:
			return TO_DECOMPRESS_EDEFAULT == null ? toDecompress != null : !TO_DECOMPRESS_EDEFAULT.equals(toDecompress);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (longName: ");
		result.append(longName);
		result.append(", version: ");
		result.append(version);
		result.append(", toDecompress: ");
		result.append(toDecompress);
		result.append(')');
		return result.toString();
	}

	/**
	 * method which launch update wizard to install the missing feature
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 * @generated NOT
	 */
	@SuppressWarnings({ "restriction", "deprecation" })
	public Boolean install() {
		String featureId, version, fromSite;
		featureId = getNameID();
		version = getVersion();

		if (version == null || version.equals("")) { 
			return false; 
		}
		fromSite = getSiteURL();

		if (fromSite == null || fromSite.equals("")) { 
			fromSite = DownloadConstants.siteUpdateDefault;
		}
		URL siteURL = null;

		try {
			siteURL =  new URL(UpdateURLDecoder.decode(fromSite, "UTF-8"));
		} catch (MalformedURLException e2) {
			DownloadConstants.log("Error when to access to the feature's site of" + featureId + version + ". URL malformed : " + fromSite +".", e2);
		} catch (UnsupportedEncodingException e) {
			DownloadConstants.log("Error when to access to the feature's site of" + featureId + version + ". Unsupported encoding for : " + fromSite +".", e);
		}

		if (siteURL != null) {
			try {
				ISite site = SiteManager.getSite(siteURL, true, (IProgressMonitor)(new NullProgressMonitor()));
				ISiteFeatureReference iSiteFeatureRef[] = site.getFeatureReferences();
				org.eclipse.update.core.Feature feature = null;
				for (int i = 0; i < iSiteFeatureRef.length; i++) {
					ISiteFeatureReference siteFeature = iSiteFeatureRef[i];
					int compare = (new Version(siteFeature.getVersionedIdentifier().getVersion().toString())).compareTo((new Version(version)));
					if (siteFeature.getVersionedIdentifier().getIdentifier().equals(featureId) && compare >= 0/*siteFeature.getVersionedIdentifier().getVersion().toString().equals(version)*/) {
						feature = (org.eclipse.update.core.Feature) siteFeature.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
						IFeature[] ifeatures = new IFeature[1];  
						ifeatures[0] = (IFeature) feature;
						SiteSearchCategory category = new SiteSearchCategory(true);
						UpdateSearchScope scope = new UpdateSearchScope();
						scope.addSearchSite(DownloadConstants.siteNameUpdate, siteURL, null);
						UpdateSearchRequest uSearchRequest = new UpdateSearchRequest(category, scope);
						uSearchRequest.addFilter(new BackLevelFilter());
						UpdateJob ujob = new UpdateJob("Update " + feature.getLabel(), uSearchRequest);
						UpdateManagerUI.openInstaller(PlatformUI.getWorkbench().getDisplay().getActiveShell(), ujob);
						//TODO : pre-select the feature in the treeViewer and make the action "select required" automatically
						return true;
					}
				}
			} catch (Exception e) {
				DownloadConstants.log("Error when installing to the feature " + featureId + version + ".", e);
			}
		}
		return false;
	}

	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist() {
		return exist(false);
	}

	/**
	 * method which return true if the resource exists with another version in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean existIgnoreVersion() {
		return exist(true);
	}

	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist(Boolean ignoreVersion) {
		Boolean exist = false;
		if (ignoreVersion == null) ignoreVersion = Boolean.FALSE;
		try {
			//ListFeaturesCommand liste = new ListFeaturesCommand(null);
			//ISite site = SiteManager.getSite(siteURL, true, (IProgressMonitor)(new NullProgressMonitor()));
			//IInstallConfiguration config = liste.getConfiguration();
			ILocalSite localSite = SiteManager.getLocalSite();
			IInstallConfiguration config = localSite.getCurrentConfiguration();
			IConfiguredSite configSite[] = config.getConfiguredSites();
			for (int i = 0; i < configSite.length; i++) {
				IConfiguredSite site = configSite[i];
				IFeatureReference[] featureref = site.getConfiguredFeatures();
				for (int j = 0; j < featureref.length; j++) {
					IFeatureReference reference = featureref[j];
					String refid = reference.getVersionedIdentifier().getIdentifier();
					String refversion = reference.getVersionedIdentifier().getVersion().toString();
					boolean matchingVersion = ignoreVersion.booleanValue() ? true : refversion.equals(getVersion());
					if (refid.equals(getNameID()) && matchingVersion) {
						org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
						IImport iimportElts[] = feature.getRawImports();
						IPluginEntry ipluginEntries[] = feature.getPluginEntries();
						IIncludedFeatureReference iincludedFeature[] = feature.getRawIncludedFeatureReferences();
						exist = isImportInstalled(iimportElts).booleanValue();
						if (!exist) return false;
						exist = isPluginEntriesInstalled(ipluginEntries).booleanValue();
						if (!exist) return false;
						exist = isIncludedFeatureInstalled(iincludedFeature).booleanValue();
						if (!exist) return false;
						return true;
					}
				}
			}
		} catch (Exception e) {
			DownloadConstants.log("Error when checking if the feature " + getNameID() + getVersion() + " exists.", e);
		}
		return exist;
	}

	/**
	 * method which return the version of the feature which is installed, null else 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("deprecation")
	public Version versionInstalled() {
		boolean existIgnoreVersion = existIgnoreVersion();
		boolean exist = exist();
		if (existIgnoreVersion && !exist) {
			try {
				ListFeaturesCommand liste = new ListFeaturesCommand(null);
				IInstallConfiguration config = liste.getConfiguration();
				IConfiguredSite configSite[] = config.getConfiguredSites();
				for (int i = 0; i < configSite.length; i++) {
					IConfiguredSite site = configSite[i];
					IFeatureReference[] featureref = site.getConfiguredFeatures();
					for (int j = 0; j < featureref.length; j++) {
						IFeatureReference reference = featureref[j];
						String refid = reference.getVersionedIdentifier().getIdentifier();
						String refversion = reference.getVersionedIdentifier().getVersion().toString();
						//org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
						if (refid.equals(getNameID())) {
							return new Version(refversion); 
						}
					}
				}
			} catch (Exception e) {
				DownloadConstants.log("Error when checking the version installed of the feature " + getNameID(), e);
			}
		}
		if (exist) {
			return getVersion() == null ? null : new Version(getVersion());
		}
		return null;
	}

	/**
	 * method which return true if the plugin is installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean isPluginInstalled(String pluginID, String version, Boolean ignoreVersion) {
		Bundle bundle = null;
		try {
			//TODO use of : bundle = Platform.getBundle(plugin, version);
			bundle = Platform.getBundle(pluginID);
			if (bundle != null) {
				if (version == null || version.equals("0.0.0") || ignoreVersion.booleanValue()) {
					return true;
				}
				else {
					Dictionary<?, ?> header = bundle.getHeaders();
					String currentversion = (header == null) ? null : (String)header.get("Bundle-Version");
					return currentversion == null ? true : currentversion.trim().equals(version);
				}
			}
		} catch (Exception e) {
			DownloadConstants.log("Error when checking if the plugin " + pluginID + version + " exists.", e);
		}
		return false;
	}

	/**
	 * method which return true if the feature is installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("deprecation")
	public Boolean isFeatureInstalled(IImport feature) {
		ListFeaturesCommand liste = null;
		try {
			liste = new ListFeaturesCommand(null);
		} catch (Exception e) {
			DownloadConstants.log("Error when checking if the feature " + getNameID() + getVersion() + " exists.", e);
		}
		if (liste != null) {
			IInstallConfiguration config = liste.getConfiguration();
			IConfiguredSite configSite[] = config.getConfiguredSites();
			for (int i = 0; i < configSite.length; i++) {
				IConfiguredSite site = configSite[i];
				IFeatureReference[] featureref = site.getConfiguredFeatures();
				for (int j = 0; j < featureref.length; j++) {
					IFeatureReference reference = featureref[j];
					//org.eclipse.update.core.Feature featureSite;
					try {
						String refid = reference.getVersionedIdentifier().getIdentifier();
						VersionedIdentifier refversionidentifier = reference.getVersionedIdentifier();
						//featureSite = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
						if (refid != null && refversionidentifier != null) {
							String featureID = feature.getVersionedIdentifier().getIdentifier();
							if (refid.equals(featureID)) {
								if (version.equals("0.0.0"))
									return true;
								int rule = (feature.getRule() != IImport.RULE_NONE) ? feature.getRule() : IImport.RULE_COMPATIBLE;
								switch (rule) {
								case IImport.RULE_PERFECT: 
									if (refversionidentifier.getVersion().isPerfect(feature.getVersionedIdentifier().getVersion()))
										return true;
									break;
								case IImport.RULE_EQUIVALENT:
									if (refversionidentifier.getVersion().isEquivalentTo(feature.getVersionedIdentifier().getVersion()))
										return true;
									break;
								case IImport.RULE_COMPATIBLE:
									if (refversionidentifier.getVersion().isCompatibleWith(feature.getVersionedIdentifier().getVersion()))
										return true;
									break;
								case IImport.RULE_GREATER_OR_EQUAL:
									if (refversionidentifier.getVersion().isGreaterOrEqualTo(feature.getVersionedIdentifier().getVersion()))
										return true;
									break;
								}
							}
						}
					} catch (CoreException e) {
						DownloadConstants.log("Error when checking if the feature " + getNameID() + getVersion() + " exists.", e);
					}
				}
			}
		}
		return false;
	}

	/**
	 * method which return true if the included feature (children) is installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("deprecation")
	public Boolean isFeatureChildrenInstalled(IFeature feature) {
		ListFeaturesCommand liste = null;
		try {
			liste = new ListFeaturesCommand(null);
		} catch (Exception e) {
			DownloadConstants.log("Error when checking if the feature children " + feature.getVersionedIdentifier().getIdentifier() + feature.getVersionedIdentifier().getVersion().toString() + " exists.", e);
		}
		if (liste != null) {
			IInstallConfiguration config = liste.getConfiguration();
			IConfiguredSite configSite[] = config.getConfiguredSites();
			for (int i = 0; i < configSite.length; i++) {
				IConfiguredSite site = configSite[i];
				IFeatureReference[] featureref = site.getConfiguredFeatures();
				for (int j = 0; j < featureref.length; j++) {
					IFeatureReference reference = featureref[j];
					//org.eclipse.update.core.Feature featureSite;
					try {
						String refid = reference.getVersionedIdentifier().getIdentifier();
						VersionedIdentifier refversionidentifier = reference.getVersionedIdentifier();
						//featureSite = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
						if (refid != null && refversionidentifier != null) {
							String featureID = feature.getVersionedIdentifier().getIdentifier();
							if (refid.equals(featureID)) {
								if (version.equals("0.0.0"))
									return true;
								//we choose to compare with "GreaterOrEqual"
								if (refversionidentifier.getVersion().isGreaterOrEqualTo(feature.getVersionedIdentifier().getVersion()))
									return true;
							}
						}
					} catch (CoreException e) {
						DownloadConstants.log("Error when checking if the feature children " + feature.getVersionedIdentifier().getIdentifier() + feature.getVersionedIdentifier().getVersion().toString() + " exists.", e);
					}
				}
			}
		}
		return false;
	}

	/**
	 * method which return true if the import resources are installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean isImportInstalled(IImport iimportElts[]) {
		boolean installed = false;
		//if empty return true
		if (iimportElts.length == 0) return true;
		for (int i = 0; i < iimportElts.length; i++) {
			Import importElt = (Import)iimportElts[i];
			//feature required
			if (importElt.getKind() == IImport.KIND_FEATURE) {
				installed = isFeatureInstalled(importElt).booleanValue();
				if (!installed) return false;
			}
			//plugin required
			else if (importElt.getKind() == IImport.KIND_PLUGIN) {
				installed = isPluginInstalled(importElt.getIdentifier(), importElt.getVersion(), true).booleanValue();
				if (!installed) return false;
			}
		}
		return installed;
	}

	/**
	 * method which return true if the plugins entries are installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean isPluginEntriesInstalled(IPluginEntry ipluginEntries[]) {
		boolean installed = false;
		//if empty return true
		if (ipluginEntries.length == 0) 
			return true;
		for (int i = 0; i < ipluginEntries.length; i++) {
			PluginEntry entry = (PluginEntry)ipluginEntries[i];
			installed = isPluginInstalled(entry.getPluginIdentifier(), entry.getPluginVersion(), true).booleanValue();
			if (!installed) return false;
		}
		return installed;
	}

	/**
	 * method which return true if the included features are installed, false else
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean isIncludedFeatureInstalled(IIncludedFeatureReference iincludedFeature[]) {
		boolean installed = true;
		//if empty return true
		if (iincludedFeature.length == 0) return true;
		for (int i = 0; i < iincludedFeature.length; i++) {
			IIncludedFeatureReference iifeature = iincludedFeature[i];
			if (!iifeature.isOptional()) {
				try {
					org.eclipse.update.core.Feature feature = (org.eclipse.update.core.Feature) iifeature.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
					installed = isFeatureChildrenInstalled((IFeature)feature).booleanValue();
					if (!installed) return false;
				} catch (CoreException e) {
					DownloadConstants.log("Error when checking if the feature children " + iifeature.getName() + " exists.", e);
				}
			}
		}
		return installed;
	}
};