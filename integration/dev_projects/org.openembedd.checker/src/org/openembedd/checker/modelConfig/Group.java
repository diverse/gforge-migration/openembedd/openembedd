/**
 * <copyright>
 * </copyright>
 *
 * $Id: Group.java,v 1.2 2007-07-10 15:42:33 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Group#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Group#getDescription <em>Description</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getGroup()
 * @model
 * @generated
 */
public interface Group extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedGroup <em>Owned Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getGroup_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedGroup
	 * @model opposite="ownedGroup" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Group#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getGroup_Description()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Group#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // Group