/**
 * <copyright>
 * </copyright>
 *
 * $Id: Category.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Category#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Category#getParent <em>Parent</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Category#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getCategory()
 * @model
 * @generated
 */
public interface Category extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedCategory <em>Owned Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getCategory_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedCategory
	 * @model opposite="ownedCategory" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Category#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Category#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(Category)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getCategory_Parent()
	 * @see org.openembedd.checker.modelConfig.Category#getChildren
	 * @model opposite="children"
	 * @generated
	 */
	Category getParent();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Category#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Category value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Category}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Category#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getCategory_Children()
	 * @see org.openembedd.checker.modelConfig.Category#getParent
	 * @model opposite="parent"
	 * @generated
	 */
	EList<Category> getChildren();

} // Category