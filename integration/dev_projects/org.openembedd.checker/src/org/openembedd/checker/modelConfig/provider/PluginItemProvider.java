/**
 * <copyright>
 * </copyright>
 *
 * $Id: PluginItemProvider.java,v 1.5 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.provider;


import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.List;

import org.eclipse.core.internal.runtime.InternalPlatform;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.update.internal.configurator.BundleManifest;
import org.eclipse.update.internal.configurator.VersionedIdentifier;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.check.library.VersionUtils;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

/**
 * This is the item provider adapter for a {@link org.openembedd.checker.modelConfig.Plugin} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PluginItemProvider
	extends ResourceItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLongNamePropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
			addApplicationsPropertyDescriptor(object);
			addPackagePropertyDescriptor(object);
			addToDecompressPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Long Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addLongNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Plugin_longName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Plugin_longName_feature", "_UI_Plugin_type"),
				 modelConfigPackage.Literals.PLUGIN__LONG_NAME,
				 true,
				 false,
				 true,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_pluginPropertyCategory"),
				 null) {
					@SuppressWarnings("restriction")
					protected Collection<String> getComboBoxObjects(Object object)
					{
						List<String> listChoicePlugin = new ArrayList<String>(); 
						if (object instanceof Plugin) {
							//item null
							listChoicePlugin.add(null);
							@SuppressWarnings("unused")
							Plugin plugin = (Plugin)object;
							try {
								//plugin installed
								Bundle[] bundles = InternalPlatform.getDefault().getBundleContext().getBundles();
								for (int k = 0; k < bundles.length; k++) {
									Bundle bundle = bundles[k];
									Dictionary<?, ?> header = bundle.getHeaders();
									String version = (header == null) ? null : (String)header.get("Bundle-Version");
									String pluginLongName = (version == null) ? bundle.getSymbolicName() : bundle.getSymbolicName() + DownloadConstants.versionSep_ + version; 
									if (!listChoicePlugin.contains(pluginLongName)) {
										listChoicePlugin.add(pluginLongName);
										}
									}
								//plugin projects
								IWorkspaceRoot wroot = ResourcesPlugin.getWorkspace().getRoot();
								IProject[] projects = wroot.getProjects();
								for (int i = 0; i < projects.length; i++) {
									IProject project = projects[i];
									if (project.isOpen() && project.getDescription().hasNature(org.eclipse.pde.internal.core.natures.PDE.PLUGIN_NATURE)) {
										project.refreshLocal(IResource.DEPTH_INFINITE, null);
										URL url = project.findMember(org.eclipse.update.internal.configurator.IConfigurationConstants.META_MANIFEST_MF).getLocation().toFile().toURL();
										BundleManifest bundle = new BundleManifest(url.openStream(), url.toExternalForm());
										String pluginLongName = (bundle.getPluginEntry().getPluginIdentifier() == null ? "" : bundle.getPluginEntry().getPluginIdentifier()) + DownloadConstants.versionSep_ + (bundle.getPluginEntry().getPluginVersion() == null ? "0.0.0" : bundle.getPluginEntry().getPluginVersion()) ;
										if (!listChoicePlugin.contains(pluginLongName)) {
											listChoicePlugin.add(pluginLongName);
											}
										}
									}
								} catch (Exception e) {
									DownloadConstants.log("Error when loading plugins list in the long name property", e);
									return listChoicePlugin;
							}
						}
						return listChoicePlugin;
					}
				});
		}

	/**
	 * Override of the createSetCommand
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected Command createSetCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		if (owner instanceof Plugin && feature == modelConfigPackage.eINSTANCE.getPlugin_LongName()) {
			return new SetPluginLongNameCommand(domain, owner, feature, value);
		}
		return super.createSetCommand(domain, owner, feature, value);
	}

	/**
	 * Class SetPluginLongNameCommand to automatically set the others properties
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	class SetPluginLongNameCommand extends CompoundCommand {
		protected EObject owner;
		protected Object value;
		protected EditingDomain domain;
		protected EStructuralFeature feature;
		protected Bundle objPlugin;
		
		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public SetPluginLongNameCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
			super(0);
			this.domain = domain;
			this.owner = owner;
			this.feature = feature;
			this.value = value;
			this.objPlugin = null;
			if (owner instanceof Plugin && feature == modelConfigPackage.eINSTANCE.getPlugin_LongName()) {
				Plugin pluginSet = (Plugin)owner;
				String pluginLongName = pluginSet.getLongName();
				if (pluginLongName != null) {
					VersionedIdentifier versionedIdentifier = VersionUtils.getLongName2VersionedIdentifier(pluginLongName);
					String pluginID = versionedIdentifier.getIdentifier();
					String version = versionedIdentifier.getVersion().toString();
					this.objPlugin = getInternalObjetPlugin(pluginID, version);
				}
			}
			append(new SetCommand(domain, owner, feature, value));
		}
		
		/**
		 * Override of execute
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		@Override
		public void execute() {
			if (owner instanceof Plugin && feature == modelConfigPackage.eINSTANCE.getPlugin_LongName()) {
				Plugin pluginSet = (Plugin)owner;
				Bundle oldObjPlugin = objPlugin;
				String oldPluginLongName = pluginSet.getLongName(); 
				super.execute();
				String pluginLongName = (String)value;
				if (pluginLongName != null && !pluginLongName.trim().equals("")) {
					if (!pluginLongName.equals(oldPluginLongName)) {
						VersionedIdentifier versionedIdentifier = VersionUtils.getLongName2VersionedIdentifier(pluginLongName);
						String pluginID = versionedIdentifier.getIdentifier();
						String version = versionedIdentifier.getVersion().toString();
						version = version == null ? "0.0.0" : version;
						Version suffixOfName = versionedIdentifier.getVersion();
						String name = pluginID == null ? null : version == null ? pluginID : pluginID + " " + suffixOfName.getMajor() + "." + suffixOfName.getMinor() + "." + suffixOfName.getMicro();
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getModelElement_NameID(), pluginID));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getModelElement_Name(), name));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getPlugin_Version(), version));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getResource_NameWebFile(), pluginLongName + DownloadConstants.jarFileSuffix));
					}
				}
			}
			else {
				super.execute();
			}
		}
		
		/**
		 * method getInternalObjetPlugin to get the real plugin object
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public Bundle getInternalObjetPlugin(String pluginID, String version) {
			if (pluginID!= null && version != null && !pluginID.trim().equals("") && !version.equals("")) {
				try {
					Bundle[] bundles = InternalPlatform.getDefault().getBundleContext().getBundles();
					for (int k = 0; k < bundles.length; k++) {
						Bundle bundle = bundles[k];
						Dictionary<?, ?> header = bundle.getHeaders();
						String versionBundle = (header == null) ? "" : (String)header.get("Bundle-Version");
						if (bundle.getSymbolicName().equals(pluginID) && versionBundle.equals(version)) {
								return bundle;
							}
						}
					} catch (Exception e) {
					return null; 
				}
			}
			return null;
		}
		
		/**
		 * Class utility getLongName2PluginID
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public String getLongName2PluginID(String pluginLongName) {
			if (pluginLongName != null && !pluginLongName.equals("")) {
				int indexOf_ = pluginLongName.indexOf(DownloadConstants.versionSep_);
				String pluginID = indexOf_ > 0 ? pluginLongName.substring(0, indexOf_) : pluginLongName;
				return pluginID;	
			}
			return null;
		}
		
		/**
		 * Class utility getLongName2PluginVersion
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public String getLongName2PluginVersion(String pluginLongName) {
			if (pluginLongName!= null && !pluginLongName.equals("")) {
				int indexOf_ = pluginLongName.indexOf(DownloadConstants.versionSep_);
				String version = indexOf_ > 0 ? pluginLongName.substring(indexOf_ + 1, pluginLongName.length()) : "";
				return version;	
			}
			return null;
		}
	}
	
	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Plugin_version_feature"),
				 getString("_UI_Plugin_version_description"),
				 modelConfigPackage.Literals.PLUGIN__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_pluginPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Applications feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addApplicationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Plugin_applications_feature"),
				 getString("_UI_Plugin_applications_description"),
				 modelConfigPackage.Literals.PLUGIN__APPLICATIONS,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_pluginPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPackagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Plugin_package_feature"),
				 getString("_UI_Plugin_package_description"),
				 modelConfigPackage.Literals.PLUGIN__PACKAGE,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_bindingPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the To Decompress feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToDecompressPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Plugin_toDecompress_feature"),
				 getString("_UI_Plugin_toDecompress_description"),
				 modelConfigPackage.Literals.PLUGIN__TO_DECOMPRESS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_pluginPropertyCategory"),
				 null));
	}

	/**
	 * This returns Plugin.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getImage(Object object) {
		//return overlayImage(object, getResourceLocator().getImage("full/obj16/Plugin"));
		return overlayImage(object, getResourceLocator().getImage("../icons_checker/plugin_obj"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Plugin)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Plugin_type") :
			getString("_UI_Plugin_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Plugin.class)) {
			case modelConfigPackage.PLUGIN__LONG_NAME:
			case modelConfigPackage.PLUGIN__VERSION:
			case modelConfigPackage.PLUGIN__TO_DECOMPRESS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return modelConfigEditPlugin.INSTANCE;
	}

}
