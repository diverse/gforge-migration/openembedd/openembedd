/**
 * <copyright>
 * </copyright>
 *
 * $Id: Exe.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Exe#getTypeOS <em>Type OS</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Exe#getLinkFile <em>Link File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Exe#getPlugins <em>Plugins</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getExe()
 * @model
 * @generated
 */
public interface Exe extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Type OS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type OS</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type OS</em>' attribute.
	 * @see #setTypeOS(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getExe_TypeOS()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getTypeOS();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Exe#getTypeOS <em>Type OS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type OS</em>' attribute.
	 * @see #getTypeOS()
	 * @generated
	 */
	void setTypeOS(String value);

	/**
	 * Returns the value of the '<em><b>Link File</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.File}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.File#getLinkExe <em>Link Exe</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link File</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link File</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getExe_LinkFile()
	 * @see org.openembedd.checker.modelConfig.File#getLinkExe
	 * @model opposite="linkExe"
	 * @generated
	 */
	EList<File> getLinkFile();

	/**
	 * Returns the value of the '<em><b>Plugins</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Plugin}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Plugin#getApplications <em>Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plugins</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugins</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getExe_Plugins()
	 * @see org.openembedd.checker.modelConfig.Plugin#getApplications
	 * @model opposite="applications"
	 * @generated
	 */
	EList<Plugin> getPlugins();

} // Exe