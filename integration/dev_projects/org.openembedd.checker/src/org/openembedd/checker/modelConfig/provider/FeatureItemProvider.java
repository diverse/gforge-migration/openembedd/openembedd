/**
 * <copyright>
 * </copyright>
 *
 * $Id: FeatureItemProvider.java,v 1.5 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.provider;


import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.update.configuration.IConfiguredSite;
import org.eclipse.update.configuration.IInstallConfiguration;
import org.eclipse.update.configuration.ILocalSite;
import org.eclipse.update.core.IFeatureReference;
import org.eclipse.update.core.InstallMonitor;
import org.eclipse.update.core.SiteManager;
import org.eclipse.update.core.model.FeatureModel;
import org.eclipse.update.core.model.FeatureModelFactory;
import org.eclipse.update.internal.configurator.VersionedIdentifier;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.check.library.VersionUtils;
import org.openembedd.checker.modelConfig.Feature;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Version;

/**
 * This is the item provider adapter for a {@link org.openembedd.checker.modelConfig.Feature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureItemProvider
	extends ResourceItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLongNamePropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
			addToDecompressPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addLongNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
		(new ItemPropertyDescriptor(
			((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
			 getResourceLocator(),
			 getString("_UI_Feature_longName_feature"),
			 getString("_UI_PropertyDescriptor_description", "_UI_Feature_longName_feature", "_UI_Feature_type"),
			 modelConfigPackage.Literals.FEATURE__LONG_NAME,
			 true,
			 false,
			 true,
			 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			 getString("_UI_featurePropertyCategory"),
			 null) {
				protected Collection<String> getComboBoxObjects(Object object)
				{
					List<String> listChoiceFeature = new ArrayList<String>(); 
					if (object instanceof Feature) {
						//item null
						listChoiceFeature.add(null);
						@SuppressWarnings("unused")
						Feature feature = (Feature)object;
						try {
							//feature installed
							ILocalSite localSite = SiteManager.getLocalSite();
							IInstallConfiguration config = localSite.getCurrentConfiguration();
							IConfiguredSite configSite[] = config.getConfiguredSites();
							for (int i = 0; i < configSite.length; i++) {
								IConfiguredSite site = configSite[i];
								IFeatureReference[] featureref = site.getConfiguredFeatures();
								for (int j = 0; j < featureref.length; j++) {
									IFeatureReference reference = featureref[j];
									org.eclipse.update.core.Feature localFeature = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
									String featureLongName = localFeature.getFeatureIdentifier() + DownloadConstants.versionSep_ + (localFeature.getFeatureVersion() == null ? "0.0.0" : localFeature.getFeatureVersion());
									if (!listChoiceFeature.contains(featureLongName)) {
										listChoiceFeature.add(featureLongName);
									}
								}
							}
							//feature projects
							IWorkspaceRoot wroot = ResourcesPlugin.getWorkspace().getRoot();
							IProject[] projects = wroot.getProjects();
							for (int i = 0; i < projects.length; i++) {
								IProject project = projects[i];
								if (project.isOpen() && project.getDescription().hasNature(org.eclipse.pde.internal.core.natures.PDE.FEATURE_NATURE)) {
									project.refreshLocal(IResource.DEPTH_INFINITE, null);
									URL url = project.findMember(org.eclipse.update.core.Feature.FEATURE_XML).getLocation().toFile().toURL();
									FeatureModelFactory featuremodelfactory = new FeatureModelFactory();
									if (url != null) {
										FeatureModel model = featuremodelfactory.parseFeature(url.openStream());
										listChoiceFeature.add((model.getFeatureIdentifier() == null ? "" : model.getFeatureIdentifier()) + DownloadConstants.versionSep_ + (model.getFeatureVersion() == null ? "0.0.0" : model.getFeatureVersion()));
									}
								}
							}
						} catch (Exception e) {
							DownloadConstants.log("Error when loading features list in the long name property", e);
							return listChoiceFeature; 
						}
					}
					return listChoiceFeature;
				}
			});
		
	}

	/**
	 * Override of the createSetCommand
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected Command createSetCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		if (owner instanceof Feature && feature == modelConfigPackage.eINSTANCE.getFeature_LongName()) {
			return new SetFeatureLongNameCommand(domain, owner, feature, value);
		}
		return super.createSetCommand(domain, owner, feature, value);
	}

	/**
	 * Class SetFeatureNameIDCommand to automatically set the others properties
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	class SetFeatureLongNameCommand extends CompoundCommand {
		protected EObject owner;
		protected Object value;
		protected EditingDomain domain;
		protected EStructuralFeature feature;
		protected org.eclipse.update.core.Feature objFeature;
		
		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public SetFeatureLongNameCommand(EditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
			super(0);
			this.domain = domain;
			this.owner = owner;
			this.feature = feature;
			this.value = value;
			this.objFeature = null;
			if (owner instanceof Feature && feature == modelConfigPackage.eINSTANCE.getFeature_LongName()) {
				Feature featureSet = (Feature)owner;
				String featureLongName = featureSet.getLongName();
				if (featureLongName != null) {
					VersionedIdentifier versionedIdentifier = VersionUtils.getLongName2VersionedIdentifier(featureLongName);
					String featureID = versionedIdentifier.getIdentifier();
					String version = versionedIdentifier.getVersion().toString();
					this.objFeature = getInternalObjetFeature(featureID, version);
				}
			}
			append(new SetCommand(domain, owner, feature, value));
		}
		
		/**
		 * Override of execute
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		@Override
		public void execute() {
			if (owner instanceof Feature && feature == modelConfigPackage.eINSTANCE.getFeature_LongName()) {
				Feature featureSet = (Feature)owner;
				org.eclipse.update.core.Feature oldObjFeature = objFeature;
				String oldFeatureLongName = featureSet.getLongName(); 
				super.execute();
				String featureLongName = (String)value;
				if (featureLongName != null && !featureLongName.trim().equals("")) {
					if (!featureLongName.equals(oldFeatureLongName)) {
						VersionedIdentifier versionedIdentifier = VersionUtils.getLongName2VersionedIdentifier(featureLongName);
						String featureID = versionedIdentifier.getIdentifier();
						String version = versionedIdentifier.getVersion().toString();
						version = version == null ? "0.0.0" : version;
						Version suffixOfName = versionedIdentifier.getVersion();
						String name = featureID == null ? null : version == null ? featureID : featureID + " " + suffixOfName.getMajor() + "." + suffixOfName.getMinor() + "." + suffixOfName.getMicro();
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getModelElement_NameID(), featureID));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getModelElement_Name(), name));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getFeature_Version(), version));
						appendAndExecute(new SetCommand(domain, owner, modelConfigPackage.eINSTANCE.getResource_NameWebFile(), featureLongName + DownloadConstants.jarFileSuffix));
					}
				}
			}
			else {
				super.execute();
			}
		}
		
		/**
		 * method getInternalObjetFeature to get the real feature object
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated NOT
		 */
		public org.eclipse.update.core.Feature getInternalObjetFeature(String featureID, String version) {
			if (featureID!= null && version != null && !featureID.trim().equals("") && !version.equals("")) {
				try {
					ILocalSite localSite = SiteManager.getLocalSite();
					IInstallConfiguration config = localSite.getCurrentConfiguration();
					IConfiguredSite configSite[] = config.getConfiguredSites();
					for (int i = 0; i < configSite.length; i++) {
						IConfiguredSite site = configSite[i];
						IFeatureReference[] featureref = site.getConfiguredFeatures();
						for (int j = 0; j < featureref.length; j++) {
							IFeatureReference reference = featureref[j];
							org.eclipse.update.core.Feature localFeature = (org.eclipse.update.core.Feature)reference.getFeature((IProgressMonitor) new InstallMonitor(new NullProgressMonitor()));
							if (localFeature.getFeatureIdentifier().equals(featureID)) {
								if (version.equals("0.0.0"))
									return localFeature;
								if (localFeature.getFeatureVersion().equals(version))
									return localFeature;
							}
						}
					}
				} catch (Exception e) {
					return null; 
				}
			}
			return null;
		}
	}
	
	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Feature_version_feature"),
				 getString("_UI_Feature_version_description"),
				 modelConfigPackage.Literals.FEATURE__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_featurePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the To Decompress feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToDecompressPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Feature_toDecompress_feature"),
				 getString("_UI_Feature_toDecompress_description"),
				 modelConfigPackage.Literals.FEATURE__TO_DECOMPRESS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_featurePropertyCategory"),
				 null));
	}

	/**
	 * This returns Feature.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getImage(Object object) {
		//return overlayImage(object, getResourceLocator().getImage("full/obj16/Feature"));
		return overlayImage(object, getResourceLocator().getImage("../icons_checker/feature_obj"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Feature)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Feature_type") :
			getString("_UI_Feature_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Feature.class)) {
			case modelConfigPackage.FEATURE__LONG_NAME:
			case modelConfigPackage.FEATURE__VERSION:
			case modelConfigPackage.FEATURE__TO_DECOMPRESS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return modelConfigEditPlugin.INSTANCE;
	}

}
