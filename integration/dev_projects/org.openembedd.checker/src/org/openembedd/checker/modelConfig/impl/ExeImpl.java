/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExeImpl.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.File;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ExeImpl#getTypeOS <em>Type OS</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ExeImpl#getLinkFile <em>Link File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ExeImpl#getPlugins <em>Plugins</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExeImpl extends ResourceImpl implements Exe {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The default value of the '{@link #getTypeOS() <em>Type OS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOS()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_OS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeOS() <em>Type OS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOS()
	 * @generated
	 * @ordered
	 */
	protected String typeOS = TYPE_OS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLinkFile() <em>Link File</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkFile()
	 * @generated
	 * @ordered
	 */
	protected EList<File> linkFile;

	/**
	 * The cached value of the '{@link #getPlugins() <em>Plugins</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugins()
	 * @generated
	 * @ordered
	 */
	protected EList<Plugin> plugins;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.EXE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeOS() {
		return typeOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeOS(String newTypeOS) {
		String oldTypeOS = typeOS;
		typeOS = newTypeOS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.EXE__TYPE_OS, oldTypeOS, typeOS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<File> getLinkFile() {
		if (linkFile == null) {
			linkFile = new EObjectWithInverseResolvingEList<File>(File.class, this, modelConfigPackage.EXE__LINK_FILE, modelConfigPackage.FILE__LINK_EXE);
		}
		return linkFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Plugin> getPlugins() {
		if (plugins == null) {
			plugins = new EObjectWithInverseResolvingEList.ManyInverse<Plugin>(Plugin.class, this, modelConfigPackage.EXE__PLUGINS, modelConfigPackage.PLUGIN__APPLICATIONS);
		}
		return plugins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.EXE__LINK_FILE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLinkFile()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.EXE__PLUGINS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlugins()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.EXE__LINK_FILE:
				return ((InternalEList<?>)getLinkFile()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.EXE__PLUGINS:
				return ((InternalEList<?>)getPlugins()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.EXE__TYPE_OS:
				return getTypeOS();
			case modelConfigPackage.EXE__LINK_FILE:
				return getLinkFile();
			case modelConfigPackage.EXE__PLUGINS:
				return getPlugins();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.EXE__TYPE_OS:
				setTypeOS((String)newValue);
				return;
			case modelConfigPackage.EXE__LINK_FILE:
				getLinkFile().clear();
				getLinkFile().addAll((Collection<? extends File>)newValue);
				return;
			case modelConfigPackage.EXE__PLUGINS:
				getPlugins().clear();
				getPlugins().addAll((Collection<? extends Plugin>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.EXE__TYPE_OS:
				setTypeOS(TYPE_OS_EDEFAULT);
				return;
			case modelConfigPackage.EXE__LINK_FILE:
				getLinkFile().clear();
				return;
			case modelConfigPackage.EXE__PLUGINS:
				getPlugins().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.EXE__TYPE_OS:
				return TYPE_OS_EDEFAULT == null ? typeOS != null : !TYPE_OS_EDEFAULT.equals(typeOS);
			case modelConfigPackage.EXE__LINK_FILE:
				return linkFile != null && !linkFile.isEmpty();
			case modelConfigPackage.EXE__PLUGINS:
				return plugins != null && !plugins.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typeOS: ");
		result.append(typeOS);
		result.append(')');
		return result.toString();
	}

} //ExeImpl