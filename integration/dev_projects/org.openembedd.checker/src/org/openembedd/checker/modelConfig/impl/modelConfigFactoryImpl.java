/**
 * <copyright>
 * </copyright>
 *
 * $Id: modelConfigFactoryImpl.java,v 1.5 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.openembedd.checker.modelConfig.Action;
import org.openembedd.checker.modelConfig.Category;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.EnumKindOfInclusion;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.Feature;
import org.openembedd.checker.modelConfig.File;
import org.openembedd.checker.modelConfig.Group;
import org.openembedd.checker.modelConfig.Library;
import org.openembedd.checker.modelConfig.ModelElement;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.modelConfig.Resource;
import org.openembedd.checker.modelConfig.State;
import org.openembedd.checker.modelConfig.modelConfigFactory;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class modelConfigFactoryImpl extends EFactoryImpl implements modelConfigFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static modelConfigFactory init() {
		try {
			modelConfigFactory themodelConfigFactory = (modelConfigFactory)EPackage.Registry.INSTANCE.getEFactory("http:///openembedd.org/model/modelConfig"); 
			if (themodelConfigFactory != null) {
				return themodelConfigFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new modelConfigFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelConfigFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case modelConfigPackage.RESOURCE: return createResource();
			case modelConfigPackage.CATEGORY: return createCategory();
			case modelConfigPackage.MODEL_ELEMENT: return createModelElement();
			case modelConfigPackage.PROFIL: return createProfil();
			case modelConfigPackage.STATE: return createState();
			case modelConfigPackage.ACTION: return createAction();
			case modelConfigPackage.FILE: return createFile();
			case modelConfigPackage.PLUGIN: return createPlugin();
			case modelConfigPackage.EXE: return createExe();
			case modelConfigPackage.CONFIGURATION: return createConfiguration();
			case modelConfigPackage.LIBRARY: return createLibrary();
			case modelConfigPackage.PACKAGE: return createPackage();
			case modelConfigPackage.GROUP: return createGroup();
			case modelConfigPackage.FEATURE: return createFeature();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case modelConfigPackage.ENUM_KIND_OF_INCLUSION:
				return createEnumKindOfInclusionFromString(eDataType, initialValue);
			case modelConfigPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case modelConfigPackage.INTEGER:
				return createIntegerFromString(eDataType, initialValue);
			case modelConfigPackage.BOOLEAN:
				return createBooleanFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case modelConfigPackage.ENUM_KIND_OF_INCLUSION:
				return convertEnumKindOfInclusionToString(eDataType, instanceValue);
			case modelConfigPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case modelConfigPackage.INTEGER:
				return convertIntegerToString(eDataType, instanceValue);
			case modelConfigPackage.BOOLEAN:
				return convertBooleanToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource createResource() {
		ResourceImpl resource = new ResourceImpl();
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category createCategory() {
		CategoryImpl category = new CategoryImpl();
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement createModelElement() {
		ModelElementImpl modelElement = new ModelElementImpl();
		return modelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Profil createProfil() {
		ProfilImpl profil = new ProfilImpl();
		return profil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public File createFile() {
		FileImpl file = new FileImpl();
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Plugin createPlugin() {
		PluginImpl plugin = new PluginImpl();
		return plugin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exe createExe() {
		ExeImpl exe = new ExeImpl();
		return exe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration createConfiguration() {
		ConfigurationImpl configuration = new ConfigurationImpl();
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Library createLibrary() {
		LibraryImpl library = new LibraryImpl();
		return library;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.openembedd.checker.modelConfig.Package createPackage() {
		PackageImpl package_ = new PackageImpl();
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group createGroup() {
		GroupImpl group = new GroupImpl();
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumKindOfInclusion createEnumKindOfInclusionFromString(EDataType eDataType, String initialValue) {
		EnumKindOfInclusion result = EnumKindOfInclusion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEnumKindOfInclusionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createIntegerFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntegerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean createBooleanFromString(EDataType eDataType, String initialValue) {
		return (Boolean)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelConfigPackage getmodelConfigPackage() {
		return (modelConfigPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static modelConfigPackage getPackage() {
		return modelConfigPackage.eINSTANCE;
	}

} //modelConfigFactoryImpl
