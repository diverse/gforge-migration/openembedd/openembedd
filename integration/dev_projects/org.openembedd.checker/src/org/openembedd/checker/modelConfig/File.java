/**
 * <copyright>
 * </copyright>
 *
 * $Id: File.java,v 1.2 2007-07-10 15:42:34 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.File#getLinkExe <em>Link Exe</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFile()
 * @model
 * @generated
 */
public interface File extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Link Exe</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Exe#getLinkFile <em>Link File</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Exe</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Exe</em>' reference.
	 * @see #setLinkExe(Exe)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFile_LinkExe()
	 * @see org.openembedd.checker.modelConfig.Exe#getLinkFile
	 * @model opposite="linkFile"
	 * @generated
	 */
	Exe getLinkExe();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.File#getLinkExe <em>Link Exe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Exe</em>' reference.
	 * @see #getLinkExe()
	 * @generated
	 */
	void setLinkExe(Exe value);

} // File