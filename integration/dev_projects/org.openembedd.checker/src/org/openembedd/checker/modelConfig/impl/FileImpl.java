/**
 * <copyright>
 * </copyright>
 *
 * $Id: FileImpl.java,v 1.3 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.File;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.FileImpl#getLinkExe <em>Link Exe</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FileImpl extends ResourceImpl implements File {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The cached value of the '{@link #getLinkExe() <em>Link Exe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkExe()
	 * @generated
	 * @ordered
	 */
	protected Exe linkExe;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.FILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exe getLinkExe() {
		if (linkExe != null && linkExe.eIsProxy()) {
			InternalEObject oldLinkExe = (InternalEObject)linkExe;
			linkExe = (Exe)eResolveProxy(oldLinkExe);
			if (linkExe != oldLinkExe) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.FILE__LINK_EXE, oldLinkExe, linkExe));
			}
		}
		return linkExe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exe basicGetLinkExe() {
		return linkExe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkExe(Exe newLinkExe, NotificationChain msgs) {
		Exe oldLinkExe = linkExe;
		linkExe = newLinkExe;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, modelConfigPackage.FILE__LINK_EXE, oldLinkExe, newLinkExe);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkExe(Exe newLinkExe) {
		if (newLinkExe != linkExe) {
			NotificationChain msgs = null;
			if (linkExe != null)
				msgs = ((InternalEObject)linkExe).eInverseRemove(this, modelConfigPackage.EXE__LINK_FILE, Exe.class, msgs);
			if (newLinkExe != null)
				msgs = ((InternalEObject)newLinkExe).eInverseAdd(this, modelConfigPackage.EXE__LINK_FILE, Exe.class, msgs);
			msgs = basicSetLinkExe(newLinkExe, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.FILE__LINK_EXE, newLinkExe, newLinkExe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				if (linkExe != null)
					msgs = ((InternalEObject)linkExe).eInverseRemove(this, modelConfigPackage.EXE__LINK_FILE, Exe.class, msgs);
				return basicSetLinkExe((Exe)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				return basicSetLinkExe(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				if (resolve) return getLinkExe();
				return basicGetLinkExe();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				setLinkExe((Exe)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				setLinkExe((Exe)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.FILE__LINK_EXE:
				return linkExe != null;
		}
		return super.eIsSet(featureID);
	}

} //FileImpl