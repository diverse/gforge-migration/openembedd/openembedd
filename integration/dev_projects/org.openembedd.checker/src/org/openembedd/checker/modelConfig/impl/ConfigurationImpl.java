/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConfigurationImpl.java,v 1.7 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openembedd.checker.modelConfig.Action;
import org.openembedd.checker.modelConfig.Category;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.Group;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.modelConfig.Resource;
import org.openembedd.checker.modelConfig.State;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedState <em>Owned State</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedAction <em>Owned Action</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedProfil <em>Owned Profil</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedResource <em>Owned Resource</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedCategory <em>Owned Category</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedPackage <em>Owned Package</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getOwnedGroup <em>Owned Group</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl#getConfigVersion <em>Config Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConfigurationImpl extends ModelElementImpl implements Configuration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The cached value of the '{@link #getOwnedState() <em>Owned State</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedState()
	 * @generated
	 * @ordered
	 */
	protected EList<State> ownedState;

	/**
	 * The cached value of the '{@link #getOwnedAction() <em>Owned Action</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedAction()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> ownedAction;

	/**
	 * The cached value of the '{@link #getOwnedProfil() <em>Owned Profil</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedProfil()
	 * @generated
	 * @ordered
	 */
	protected EList<Profil> ownedProfil;

	/**
	 * The cached value of the '{@link #getOwnedResource() <em>Owned Resource</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedResource()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> ownedResource;

	/**
	 * The cached value of the '{@link #getOwnedCategory() <em>Owned Category</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedCategory()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> ownedCategory;

	/**
	 * The cached value of the '{@link #getOwnedPackage() <em>Owned Package</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<org.openembedd.checker.modelConfig.Package> ownedPackage;

	/**
	 * The cached value of the '{@link #getOwnedGroup() <em>Owned Group</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedGroup()
	 * @generated
	 * @ordered
	 */
	protected EList<Group> ownedGroup;

	/**
	 * The default value of the '{@link #getConfigVersion() <em>Config Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIG_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfigVersion() <em>Config Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigVersion()
	 * @generated
	 * @ordered
	 */
	protected String configVersion = CONFIG_VERSION_EDEFAULT;

	/**
	 * the plugin contributor 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * @ordered
	 */
	protected String pluginContributorNameID = null;
	
	/**
	 * the path of license contributor 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * @ordered
	 */
	protected String pathLicenseContributor = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getOwnedState() {
		if (ownedState == null) {
			ownedState = new EObjectContainmentWithInverseEList<State>(State.class, this, modelConfigPackage.CONFIGURATION__OWNED_STATE, modelConfigPackage.STATE__OWNER);
		}
		return ownedState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getOwnedAction() {
		if (ownedAction == null) {
			ownedAction = new EObjectContainmentWithInverseEList<Action>(Action.class, this, modelConfigPackage.CONFIGURATION__OWNED_ACTION, modelConfigPackage.ACTION__OWNER);
		}
		return ownedAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Profil> getOwnedProfil() {
		if (ownedProfil == null) {
			ownedProfil = new EObjectContainmentWithInverseEList<Profil>(Profil.class, this, modelConfigPackage.CONFIGURATION__OWNED_PROFIL, modelConfigPackage.PROFIL__OWNER);
		}
		return ownedProfil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getOwnedResource() {
		if (ownedResource == null) {
			ownedResource = new EObjectContainmentWithInverseEList<Resource>(Resource.class, this, modelConfigPackage.CONFIGURATION__OWNED_RESOURCE, modelConfigPackage.RESOURCE__OWNER);
		}
		return ownedResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getOwnedCategory() {
		if (ownedCategory == null) {
			ownedCategory = new EObjectContainmentWithInverseEList<Category>(Category.class, this, modelConfigPackage.CONFIGURATION__OWNED_CATEGORY, modelConfigPackage.CATEGORY__OWNER);
		}
		return ownedCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.openembedd.checker.modelConfig.Package> getOwnedPackage() {
		if (ownedPackage == null) {
			ownedPackage = new EObjectContainmentWithInverseEList<org.openembedd.checker.modelConfig.Package>(org.openembedd.checker.modelConfig.Package.class, this, modelConfigPackage.CONFIGURATION__OWNED_PACKAGE, modelConfigPackage.PACKAGE__OWNER);
		}
		return ownedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Group> getOwnedGroup() {
		if (ownedGroup == null) {
			ownedGroup = new EObjectContainmentWithInverseEList<Group>(Group.class, this, modelConfigPackage.CONFIGURATION__OWNED_GROUP, modelConfigPackage.GROUP__OWNER);
		}
		return ownedGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfigVersion() {
		return configVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfigVersion(String newConfigVersion) {
		String oldConfigVersion = configVersion;
		configVersion = newConfigVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.CONFIGURATION__CONFIG_VERSION, oldConfigVersion, configVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedState()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedAction()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedProfil()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedResource()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedCategory()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedPackage()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedGroup()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				return ((InternalEList<?>)getOwnedState()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				return ((InternalEList<?>)getOwnedAction()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				return ((InternalEList<?>)getOwnedProfil()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				return ((InternalEList<?>)getOwnedResource()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				return ((InternalEList<?>)getOwnedCategory()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				return ((InternalEList<?>)getOwnedPackage()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				return ((InternalEList<?>)getOwnedGroup()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				return getOwnedState();
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				return getOwnedAction();
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				return getOwnedProfil();
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				return getOwnedResource();
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				return getOwnedCategory();
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				return getOwnedPackage();
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				return getOwnedGroup();
			case modelConfigPackage.CONFIGURATION__CONFIG_VERSION:
				return getConfigVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				getOwnedState().clear();
				getOwnedState().addAll((Collection<? extends State>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				getOwnedAction().clear();
				getOwnedAction().addAll((Collection<? extends Action>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				getOwnedProfil().clear();
				getOwnedProfil().addAll((Collection<? extends Profil>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				getOwnedResource().clear();
				getOwnedResource().addAll((Collection<? extends Resource>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				getOwnedCategory().clear();
				getOwnedCategory().addAll((Collection<? extends Category>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				getOwnedPackage().clear();
				getOwnedPackage().addAll((Collection<? extends org.openembedd.checker.modelConfig.Package>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				getOwnedGroup().clear();
				getOwnedGroup().addAll((Collection<? extends Group>)newValue);
				return;
			case modelConfigPackage.CONFIGURATION__CONFIG_VERSION:
				setConfigVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				getOwnedState().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				getOwnedAction().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				getOwnedProfil().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				getOwnedResource().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				getOwnedCategory().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				getOwnedPackage().clear();
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				getOwnedGroup().clear();
				return;
			case modelConfigPackage.CONFIGURATION__CONFIG_VERSION:
				setConfigVersion(CONFIG_VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * return the name ID of the plugin contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public String getPluginContributorNameID() {
		return pluginContributorNameID;
		
	}
	
	/**
	 * return the path of the license contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public String getPathLicenseContributor() {
		return pathLicenseContributor;
		
	}
	
	/**
	 * set the name ID of the plugin contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public void setPluginContributorNameID(String plugin) {
		pluginContributorNameID = plugin;
	}
	
	/**
	 * set the path of the license contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	public void setPathLicenseContributor(String path) {
		pathLicenseContributor = path; 
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
				return ownedState != null && !ownedState.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
				return ownedAction != null && !ownedAction.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
				return ownedProfil != null && !ownedProfil.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
				return ownedResource != null && !ownedResource.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
				return ownedCategory != null && !ownedCategory.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
				return ownedPackage != null && !ownedPackage.isEmpty();
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				return ownedGroup != null && !ownedGroup.isEmpty();
			case modelConfigPackage.CONFIGURATION__CONFIG_VERSION:
				return CONFIG_VERSION_EDEFAULT == null ? configVersion != null : !CONFIG_VERSION_EDEFAULT.equals(configVersion);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (configVersion: ");
		result.append(configVersion);
		result.append(')');
		return result.toString();
	}

} //ConfigurationImpl