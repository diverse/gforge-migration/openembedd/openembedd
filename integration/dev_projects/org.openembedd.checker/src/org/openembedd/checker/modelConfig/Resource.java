/**
 * <copyright>
 * </copyright>
 *
 * $Id: Resource.java,v 1.9 2007-10-08 07:44:59 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;
import org.osgi.framework.Version;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getCategory <em>Category</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getProfil <em>Profil</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getState <em>State</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getNameWebFile <em>Name Web File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getSiteURL <em>Site URL</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getEntry <em>Entry</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getIsLocalFile <em>Is Local File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getTargetNameFile <em>Target Name File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getTargetPathFile <em>Target Path File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getIsLocalLicense <em>Is Local License</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getLicenseName <em>License Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getLicenseURL <em>License URL</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getLicensePathFile <em>License Path File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getGroup <em>Group</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getRequired <em>Required</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getRequire <em>Require</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getIsVisible <em>Is Visible</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getIncludesIn <em>Includes In</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Resource#getJavaVMVersion <em>Java VM Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedResource <em>Owned Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedResource
	 * @model opposite="ownedResource" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' reference.
	 * @see #setCategory(Category)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Category()
	 * @model
	 * @generated
	 */
	Category getCategory();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getCategory <em>Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' reference.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(Category value);

	/**
	 * Returns the value of the '<em><b>Profil</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Profil}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Profil#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profil</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profil</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Profil()
	 * @see org.openembedd.checker.modelConfig.Profil#getResource
	 * @model opposite="resource"
	 * @generated
	 */
	EList<Profil> getProfil();

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.State#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_State()
	 * @see org.openembedd.checker.modelConfig.State#getResource
	 * @model opposite="resource" required="true"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Name Web File</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Web File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Web File</em>' attribute.
	 * @see #setNameWebFile(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_NameWebFile()
	 * @model default="" dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getNameWebFile();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getNameWebFile <em>Name Web File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Web File</em>' attribute.
	 * @see #getNameWebFile()
	 * @generated
	 */
	void setNameWebFile(String value);

	/**
	 * Returns the value of the '<em><b>Site URL</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Site URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Site URL</em>' attribute.
	 * @see #setSiteURL(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_SiteURL()
	 * @model default="" dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getSiteURL();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getSiteURL <em>Site URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Site URL</em>' attribute.
	 * @see #getSiteURL()
	 * @generated
	 */
	void setSiteURL(String value);

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' attribute.
	 * @see #setEntry(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Entry()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getEntry();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getEntry <em>Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry</em>' attribute.
	 * @see #getEntry()
	 * @generated
	 */
	void setEntry(String value);

	/**
	 * Returns the value of the '<em><b>Is Local File</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Local File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Local File</em>' attribute.
	 * @see #setIsLocalFile(Boolean)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_IsLocalFile()
	 * @model default="false" dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean getIsLocalFile();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getIsLocalFile <em>Is Local File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Local File</em>' attribute.
	 * @see #getIsLocalFile()
	 * @generated
	 */
	void setIsLocalFile(Boolean value);

	/**
	 * Returns the value of the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Name File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Name File</em>' attribute.
	 * @see #setTargetNameFile(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_TargetNameFile()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getTargetNameFile();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getTargetNameFile <em>Target Name File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Name File</em>' attribute.
	 * @see #getTargetNameFile()
	 * @generated
	 */
	void setTargetNameFile(String value);

	/**
	 * Returns the value of the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Path File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Path File</em>' attribute.
	 * @see #setTargetPathFile(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_TargetPathFile()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getTargetPathFile();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getTargetPathFile <em>Target Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Path File</em>' attribute.
	 * @see #getTargetPathFile()
	 * @generated
	 */
	void setTargetPathFile(String value);

	/**
	 * Returns the value of the '<em><b>Is Local License</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Local License</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Local License</em>' attribute.
	 * @see #setIsLocalLicense(Boolean)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_IsLocalLicense()
	 * @model default="true" dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean getIsLocalLicense();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getIsLocalLicense <em>Is Local License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Local License</em>' attribute.
	 * @see #getIsLocalLicense()
	 * @generated
	 */
	void setIsLocalLicense(Boolean value);

	/**
	 * Returns the value of the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>License Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License Name</em>' attribute.
	 * @see #setLicenseName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_LicenseName()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getLicenseName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getLicenseName <em>License Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License Name</em>' attribute.
	 * @see #getLicenseName()
	 * @generated
	 */
	void setLicenseName(String value);

	/**
	 * Returns the value of the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>License URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License URL</em>' attribute.
	 * @see #setLicenseURL(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_LicenseURL()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getLicenseURL();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getLicenseURL <em>License URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License URL</em>' attribute.
	 * @see #getLicenseURL()
	 * @generated
	 */
	void setLicenseURL(String value);

	/**
	 * Returns the value of the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>License Path File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License Path File</em>' attribute.
	 * @see #setLicensePathFile(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_LicensePathFile()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getLicensePathFile();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getLicensePathFile <em>License Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License Path File</em>' attribute.
	 * @see #getLicensePathFile()
	 * @generated
	 */
	void setLicensePathFile(String value);

	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(Group)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Group()
	 * @model
	 * @generated
	 */
	Group getGroup();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(Group value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * The default value is <code>"description of the resource"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Description()
	 * @model default="description of the resource" dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Required</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Resource}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Resource#getRequire <em>Require</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Required()
	 * @see org.openembedd.checker.modelConfig.Resource#getRequire
	 * @model opposite="require"
	 * @generated
	 */
	EList<Resource> getRequired();

	/**
	 * Returns the value of the '<em><b>Require</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Resource}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Resource#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Require</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Require</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_Require()
	 * @see org.openembedd.checker.modelConfig.Resource#getRequired
	 * @model opposite="required"
	 * @generated
	 */
	EList<Resource> getRequire();

	/**
	 * Returns the value of the '<em><b>Is Visible</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Visible</em>' attribute.
	 * @see #setIsVisible(Boolean)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_IsVisible()
	 * @model default="true" dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean getIsVisible();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getIsVisible <em>Is Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Visible</em>' attribute.
	 * @see #getIsVisible()
	 * @generated
	 */
	void setIsVisible(Boolean value);

	/**
	 * Returns the value of the '<em><b>Includes In</b></em>' attribute.
	 * The default value is <code>"None"</code>.
	 * The literals are from the enumeration {@link org.openembedd.checker.modelConfig.EnumKindOfInclusion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Includes In</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes In</em>' attribute.
	 * @see org.openembedd.checker.modelConfig.EnumKindOfInclusion
	 * @see #setIncludesIn(EnumKindOfInclusion)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_IncludesIn()
	 * @model default="None"
	 * @generated
	 */
	EnumKindOfInclusion getIncludesIn();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getIncludesIn <em>Includes In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Includes In</em>' attribute.
	 * @see org.openembedd.checker.modelConfig.EnumKindOfInclusion
	 * @see #getIncludesIn()
	 * @generated
	 */
	void setIncludesIn(EnumKindOfInclusion value);

	/**
	 * Returns the value of the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java VM Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java VM Version</em>' attribute.
	 * @see #setJavaVMVersion(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getResource_JavaVMVersion()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getJavaVMVersion();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Resource#getJavaVMVersion <em>Java VM Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Java VM Version</em>' attribute.
	 * @see #getJavaVMVersion()
	 * @generated
	 */
	void setJavaVMVersion(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean exist();

	/**
	 * method which return true if the resource exists with another version in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	Boolean existIgnoreVersion();
	
	/**
	 * method which return the version of the resource which is installed, null else 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	Version versionInstalled();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.openembedd.checking.modelConfig.String" required="true"
	 * @generated
	 */
	String getLicense();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 * @generated NOT
	 */
	String getURLLicense();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 * @generated NOT
	 */
	void setErrorTarget(boolean newErrorTarget);
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 * @generated NOT
	 */
	Boolean getErrorTarget();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc --> 
	 * @generated NOT
	 */
	Boolean isJavaVersionGreaterTo(String javaVMVersion);
	
} // Resource