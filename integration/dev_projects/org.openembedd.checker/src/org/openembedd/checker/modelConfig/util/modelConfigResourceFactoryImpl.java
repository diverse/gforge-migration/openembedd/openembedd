/**
 * <copyright>
 * </copyright>
 *
 * $Id: modelConfigResourceFactoryImpl.java,v 1.3 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource Factory</b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.openembedd.checker.modelConfig.util.modelConfigResourceImpl
 * @generated
 */
public class modelConfigResourceFactoryImpl extends ResourceFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * Creates an instance of the resource factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelConfigResourceFactoryImpl() {
		super();
	}

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource createResource(URI uri) {
		Resource result = new modelConfigResourceImpl(uri);
		return result;
	}

} //modelConfigResourceFactoryImpl
