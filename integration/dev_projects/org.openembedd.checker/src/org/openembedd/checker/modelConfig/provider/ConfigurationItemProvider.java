/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConfigurationItemProvider.java,v 1.5 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.modelConfigFactory;
import org.openembedd.checker.modelConfig.modelConfigPackage;

/**
 * This is the item provider adapter for a {@link org.openembedd.checker.modelConfig.Configuration} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigurationItemProvider
	extends ModelElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addConfigVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Config Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConfigVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Configuration_configVersion_feature"),
				 getString("_UI_Configuration_configVersion_description"),
				 modelConfigPackage.Literals.CONFIGURATION__CONFIG_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_rootPropertyCategory"),
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_STATE);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_ACTION);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_PROFIL);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_CATEGORY);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_PACKAGE);
			childrenFeatures.add(modelConfigPackage.Literals.CONFIGURATION__OWNED_GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Configuration.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getImage(Object object) {
		//return overlayImage(object, getResourceLocator().getImage("full/obj16/Configuration"));
		return overlayImage(object, getResourceLocator().getImage("../icons_checker/plugin_config_obj"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Configuration)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Configuration_type") :
			getString("_UI_Configuration_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Configuration.class)) {
			case modelConfigPackage.CONFIGURATION__CONFIG_VERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case modelConfigPackage.CONFIGURATION__OWNED_STATE:
			case modelConfigPackage.CONFIGURATION__OWNED_ACTION:
			case modelConfigPackage.CONFIGURATION__OWNED_PROFIL:
			case modelConfigPackage.CONFIGURATION__OWNED_RESOURCE:
			case modelConfigPackage.CONFIGURATION__OWNED_CATEGORY:
			case modelConfigPackage.CONFIGURATION__OWNED_PACKAGE:
			case modelConfigPackage.CONFIGURATION__OWNED_GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_STATE,
				 modelConfigFactory.eINSTANCE.createState()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_ACTION,
				 modelConfigFactory.eINSTANCE.createAction()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_PROFIL,
				 modelConfigFactory.eINSTANCE.createProfil()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createResource()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createFile()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createPlugin()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createExe()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createLibrary()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_RESOURCE,
				 modelConfigFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_CATEGORY,
				 modelConfigFactory.eINSTANCE.createCategory()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_PACKAGE,
				 modelConfigFactory.eINSTANCE.createPackage()));

		newChildDescriptors.add
			(createChildParameter
				(modelConfigPackage.Literals.CONFIGURATION__OWNED_GROUP,
				 modelConfigFactory.eINSTANCE.createGroup()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return modelConfigEditPlugin.INSTANCE;
	}

}
