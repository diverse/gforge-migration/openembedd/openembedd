/**
 * <copyright>
 * </copyright>
 *
 * $Id: LibraryImpl.java,v 1.6 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.Library;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Bundle;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.LibraryImpl#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.LibraryImpl#getExe <em>Exe</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.LibraryImpl#getGroupName <em>Group Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.LibraryImpl#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LibraryImpl extends FileImpl implements Library {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The cached value of the '{@link #getPlugin() <em>Plugin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugin()
	 * @generated
	 * @ordered
	 */
	protected Plugin plugin;

	/**
	 * The cached value of the '{@link #getExe() <em>Exe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExe()
	 * @generated
	 * @ordered
	 */
	protected Exe exe;

	/**
	 * The default value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected static final String GROUP_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected String groupName = GROUP_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibraryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.LIBRARY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Plugin getPlugin() {
		if (plugin != null && plugin.eIsProxy()) {
			InternalEObject oldPlugin = (InternalEObject)plugin;
			plugin = (Plugin)eResolveProxy(oldPlugin);
			if (plugin != oldPlugin) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.LIBRARY__PLUGIN, oldPlugin, plugin));
			}
		}
		return plugin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Plugin basicGetPlugin() {
		return plugin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlugin(Plugin newPlugin) {
		Plugin oldPlugin = plugin;
		plugin = newPlugin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.LIBRARY__PLUGIN, oldPlugin, plugin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exe getExe() {
		if (exe != null && exe.eIsProxy()) {
			InternalEObject oldExe = (InternalEObject)exe;
			exe = (Exe)eResolveProxy(oldExe);
			if (exe != oldExe) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.LIBRARY__EXE, oldExe, exe));
			}
		}
		return exe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exe basicGetExe() {
		return exe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExe(Exe newExe) {
		Exe oldExe = exe;
		exe = newExe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.LIBRARY__EXE, oldExe, exe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupName(String newGroupName) {
		String oldGroupName = groupName;
		groupName = newGroupName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.LIBRARY__GROUP_NAME, oldGroupName, groupName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.LIBRARY__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.LIBRARY__PLUGIN:
				if (resolve) return getPlugin();
				return basicGetPlugin();
			case modelConfigPackage.LIBRARY__EXE:
				if (resolve) return getExe();
				return basicGetExe();
			case modelConfigPackage.LIBRARY__GROUP_NAME:
				return getGroupName();
			case modelConfigPackage.LIBRARY__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.LIBRARY__PLUGIN:
				setPlugin((Plugin)newValue);
				return;
			case modelConfigPackage.LIBRARY__EXE:
				setExe((Exe)newValue);
				return;
			case modelConfigPackage.LIBRARY__GROUP_NAME:
				setGroupName((String)newValue);
				return;
			case modelConfigPackage.LIBRARY__VERSION:
				setVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.LIBRARY__PLUGIN:
				setPlugin((Plugin)null);
				return;
			case modelConfigPackage.LIBRARY__EXE:
				setExe((Exe)null);
				return;
			case modelConfigPackage.LIBRARY__GROUP_NAME:
				setGroupName(GROUP_NAME_EDEFAULT);
				return;
			case modelConfigPackage.LIBRARY__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.LIBRARY__PLUGIN:
				return plugin != null;
			case modelConfigPackage.LIBRARY__EXE:
				return exe != null;
			case modelConfigPackage.LIBRARY__GROUP_NAME:
				return GROUP_NAME_EDEFAULT == null ? groupName != null : !GROUP_NAME_EDEFAULT.equals(groupName);
			case modelConfigPackage.LIBRARY__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (groupName: ");
		result.append(groupName);
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist() {
		Boolean exist = false;
		URL urlTarget = null;
		String sTargetPathFile = getTargetPathFile();
		String sTargetNameFile = getTargetNameFile(); 

		if (sTargetPathFile == null || sTargetNameFile == null) {
			setErrorTarget(true);
			return false;
		}
		
		Path p_targetPathFile = new Path(sTargetPathFile);
		
		//Exe assExe = getExe();
		Plugin assPlugin = getPlugin();
		//check the target if an association with a plugin exists
		if (assPlugin != null) {
			try {
				Bundle bundle = Platform.getBundle(assPlugin.getNameID());
				if (bundle != null) {
					urlTarget = FileLocator.find(bundle, p_targetPathFile, null);
				}
			} catch (Exception e) {
				setErrorTarget(true);
			}
			if (urlTarget != null) {
				try {
					Path ptarget = new Path(FileLocator.resolve(urlTarget).getPath());
					ptarget = (Path) ptarget.append(sTargetNameFile);
					exist = ptarget.makeAbsolute().toFile().exists();
				} catch (IOException e) {
					setErrorTarget(true);
				}
			}
		}
		//check the target if there isn't any link
		else {
			try {
				if (p_targetPathFile.getDevice() == null) {
					Path device = new Path(DownloadConstants.defaultDevicePath);
					p_targetPathFile = (Path)device.append((IPath)p_targetPathFile);
				}
				IPath ptarget = p_targetPathFile.append(sTargetNameFile).makeAbsolute();
				exist = ptarget.toFile().exists();
			} catch (Exception e) {
				setErrorTarget(true);
			}
		}
		return exist;
	}
	
} //LibraryImpl