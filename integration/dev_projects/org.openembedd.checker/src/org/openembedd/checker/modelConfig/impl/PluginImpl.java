/**
 * <copyright>
 * </copyright>
 *
 * $Id: PluginImpl.java,v 1.9 2007-12-20 17:50:29 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.net.URL;
import java.util.Collection;
import java.util.Dictionary;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.PluginImpl#getLongName <em>Long Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.PluginImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.PluginImpl#getApplications <em>Applications</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.PluginImpl#getPackage <em>Package</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.PluginImpl#getToDecompress <em>To Decompress</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PluginImpl extends ResourceImpl implements Plugin {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The default value of the '{@link #getLongName() <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongName()
	 * @generated
	 * @ordered
	 */
	protected static final String LONG_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLongName() <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongName()
	 * @generated
	 * @ordered
	 */
	protected String longName = LONG_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getApplications() <em>Applications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplications()
	 * @generated
	 * @ordered
	 */
	protected EList<Exe> applications;

	/**
	 * The cached value of the '{@link #getPackage() <em>Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackage()
	 * @generated
	 * @ordered
	 */
	protected org.openembedd.checker.modelConfig.Package package_;

	/**
	 * The default value of the '{@link #getToDecompress() <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDecompress()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_DECOMPRESS_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getToDecompress() <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDecompress()
	 * @generated
	 * @ordered
	 */
	protected Boolean toDecompress = TO_DECOMPRESS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.PLUGIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongName(String newLongName) {
		String oldLongName = longName;
		longName = newLongName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.PLUGIN__LONG_NAME, oldLongName, longName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.PLUGIN__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Exe> getApplications() {
		if (applications == null) {
			applications = new EObjectWithInverseResolvingEList.ManyInverse<Exe>(Exe.class, this, modelConfigPackage.PLUGIN__APPLICATIONS, modelConfigPackage.EXE__PLUGINS);
		}
		return applications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.openembedd.checker.modelConfig.Package getPackage() {
		if (package_ != null && package_.eIsProxy()) {
			InternalEObject oldPackage = (InternalEObject)package_;
			package_ = (org.openembedd.checker.modelConfig.Package)eResolveProxy(oldPackage);
			if (package_ != oldPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.PLUGIN__PACKAGE, oldPackage, package_));
			}
		}
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.openembedd.checker.modelConfig.Package basicGetPackage() {
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackage(org.openembedd.checker.modelConfig.Package newPackage) {
		org.openembedd.checker.modelConfig.Package oldPackage = package_;
		package_ = newPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.PLUGIN__PACKAGE, oldPackage, package_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToDecompress() {
		return toDecompress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToDecompress(Boolean newToDecompress) {
		Boolean oldToDecompress = toDecompress;
		toDecompress = newToDecompress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.PLUGIN__TO_DECOMPRESS, oldToDecompress, toDecompress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getApplications()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				return ((InternalEList<?>)getApplications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__LONG_NAME:
				return getLongName();
			case modelConfigPackage.PLUGIN__VERSION:
				return getVersion();
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				return getApplications();
			case modelConfigPackage.PLUGIN__PACKAGE:
				if (resolve) return getPackage();
				return basicGetPackage();
			case modelConfigPackage.PLUGIN__TO_DECOMPRESS:
				return getToDecompress();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__LONG_NAME:
				setLongName((String)newValue);
				return;
			case modelConfigPackage.PLUGIN__VERSION:
				setVersion((String)newValue);
				return;
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				getApplications().clear();
				getApplications().addAll((Collection<? extends Exe>)newValue);
				return;
			case modelConfigPackage.PLUGIN__PACKAGE:
				setPackage((org.openembedd.checker.modelConfig.Package)newValue);
				return;
			case modelConfigPackage.PLUGIN__TO_DECOMPRESS:
				setToDecompress((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__LONG_NAME:
				setLongName(LONG_NAME_EDEFAULT);
				return;
			case modelConfigPackage.PLUGIN__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				getApplications().clear();
				return;
			case modelConfigPackage.PLUGIN__PACKAGE:
				setPackage((org.openembedd.checker.modelConfig.Package)null);
				return;
			case modelConfigPackage.PLUGIN__TO_DECOMPRESS:
				setToDecompress(TO_DECOMPRESS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.PLUGIN__LONG_NAME:
				return LONG_NAME_EDEFAULT == null ? longName != null : !LONG_NAME_EDEFAULT.equals(longName);
			case modelConfigPackage.PLUGIN__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case modelConfigPackage.PLUGIN__APPLICATIONS:
				return applications != null && !applications.isEmpty();
			case modelConfigPackage.PLUGIN__PACKAGE:
				return package_ != null;
			case modelConfigPackage.PLUGIN__TO_DECOMPRESS:
				return TO_DECOMPRESS_EDEFAULT == null ? toDecompress != null : !TO_DECOMPRESS_EDEFAULT.equals(toDecompress);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (longName: ");
		result.append(longName);
		result.append(", version: ");
		result.append(version);
		result.append(", toDecompress: ");
		result.append(toDecompress);
		result.append(')');
		return result.toString();
	}

	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist() {
		return exist(false);
	}
	
	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist(Boolean ignoreVersion) {
		if (ignoreVersion == null) ignoreVersion = Boolean.FALSE;
		Boolean exist = false;
		@SuppressWarnings("unused")
		URL urlTarget = null;
		Bundle bundle = null;
		try {
			bundle = Platform.getBundle(getNameID());
			if (bundle != null) {
				Dictionary<?, ?> header = bundle.getHeaders();
				String version = (header == null) ? "" : (String)header.get("Bundle-Version");
				exist = (ignoreVersion.booleanValue()) ? true : (version.trim().equals("") ? true : getVersion().equals(version));
			}
		} catch (Exception e) {
			DownloadConstants.log("Exception access to bundle " + getNameID(), e);
		}

		return exist;
	}
	
	/**
	 * method which return true if the resource exists with another version in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean existIgnoreVersion() {
		return exist(true);
	}
	
	/**
	 * method which try to install the plugin
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Boolean install() {
		if (!exist()) {
			
		}
		return false;
	}

	/**
	 * method which return the version of the plugin which is installed, null else 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Version versionInstalled() {
		boolean existIgnoreVersion = existIgnoreVersion();
		boolean exist = exist();
		if (existIgnoreVersion && !exist) {
			@SuppressWarnings("unused")
			URL urlTarget = null;
			Bundle bundle = null;
			try {
				bundle = Platform.getBundle(getNameID());
				if (bundle != null) {
					Dictionary<?, ?> header = bundle.getHeaders();
					String version = (header == null) ? null : (String)header.get("Bundle-Version");
					Version pluginVersion = version == null ? null : new Version(version);  
					return pluginVersion;
				}
			} catch (Exception e) {
				DownloadConstants.log("Error when checking the version installed of the plugin " + getNameID(), e);
			}
		}
		if (exist) {
			return getVersion() == null ? null : new Version(getVersion());
		}
		return null;
	}
} //PluginImpl