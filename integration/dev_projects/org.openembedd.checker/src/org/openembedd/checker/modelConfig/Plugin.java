/**
 * <copyright>
 * </copyright>
 *
 * $Id: Plugin.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Plugin#getLongName <em>Long Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Plugin#getVersion <em>Version</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Plugin#getApplications <em>Applications</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Plugin#getPackage <em>Package</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Plugin#getToDecompress <em>To Decompress</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin()
 * @model
 * @generated
 */
public interface Plugin extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Name</em>' attribute.
	 * @see #setLongName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin_LongName()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getLongName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Plugin#getLongName <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Name</em>' attribute.
	 * @see #getLongName()
	 * @generated
	 */
	void setLongName(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin_Version()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Plugin#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Applications</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Exe}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Exe#getPlugins <em>Plugins</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applications</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applications</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin_Applications()
	 * @see org.openembedd.checker.modelConfig.Exe#getPlugins
	 * @model opposite="plugins"
	 * @generated
	 */
	EList<Exe> getApplications();

	/**
	 * Returns the value of the '<em><b>Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package</em>' reference.
	 * @see #setPackage(org.openembedd.checker.modelConfig.Package)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin_Package()
	 * @model
	 * @generated
	 */
	org.openembedd.checker.modelConfig.Package getPackage();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Plugin#getPackage <em>Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package</em>' reference.
	 * @see #getPackage()
	 * @generated
	 */
	void setPackage(org.openembedd.checker.modelConfig.Package value);

	/**
	 * Returns the value of the '<em><b>To Decompress</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Decompress</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Decompress</em>' attribute.
	 * @see #setToDecompress(Boolean)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getPlugin_ToDecompress()
	 * @model default="false" dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean getToDecompress();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Plugin#getToDecompress <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Decompress</em>' attribute.
	 * @see #getToDecompress()
	 * @generated
	 */
	void setToDecompress(Boolean value);

} // Plugin