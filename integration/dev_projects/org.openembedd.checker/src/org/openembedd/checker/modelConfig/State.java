/**
 * <copyright>
 * </copyright>
 *
 * $Id: State.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.State#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.State#getResource <em>Resource</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.State#getDefaultAction <em>Default Action</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.State#getImage <em>Image</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.State#getDescription <em>Description</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState()
 * @model
 * @generated
 */
public interface State extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedState <em>Owned State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedState
	 * @model opposite="ownedState" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.State#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Resource}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Resource#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState_Resource()
	 * @see org.openembedd.checker.modelConfig.Resource#getState
	 * @model opposite="state"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * Returns the value of the '<em><b>Default Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Action</em>' reference.
	 * @see #setDefaultAction(Action)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState_DefaultAction()
	 * @model
	 * @generated
	 */
	Action getDefaultAction();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.State#getDefaultAction <em>Default Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Action</em>' reference.
	 * @see #getDefaultAction()
	 * @generated
	 */
	void setDefaultAction(Action value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState_Image()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.State#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getState_Description()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.State#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // State