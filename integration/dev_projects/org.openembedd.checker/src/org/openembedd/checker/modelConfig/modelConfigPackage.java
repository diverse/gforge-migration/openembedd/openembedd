/**
 * <copyright>
 * </copyright>
 *
 * $Id: modelConfigPackage.java,v 1.6 2007-07-10 15:42:33 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;


/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.openembedd.checker.modelConfig.modelConfigFactory
 * @model kind="package"
 * @generated
 */
public interface modelConfigPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "modelConfig";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///openembedd.org/model/modelConfig";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelConfig";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	modelConfigPackage eINSTANCE = org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ModelElementImpl <em>Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ModelElementImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getModelElement()
	 * @generated
	 */
	int MODEL_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__NAME_ID = 1;

	/**
	 * The number of structural features of the '<em>Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ResourceImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__CATEGORY = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__PROFIL = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__STATE = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME_WEB_FILE = MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__SITE_URL = MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__ENTRY = MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__IS_LOCAL_FILE = MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__TARGET_NAME_FILE = MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__TARGET_PATH_FILE = MODEL_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__IS_LOCAL_LICENSE = MODEL_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LICENSE_NAME = MODEL_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LICENSE_URL = MODEL_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LICENSE_PATH_FILE = MODEL_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__GROUP = MODEL_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__DESCRIPTION = MODEL_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__REQUIRED = MODEL_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__REQUIRE = MODEL_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__IS_VISIBLE = MODEL_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__INCLUDES_IN = MODEL_ELEMENT_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__JAVA_VM_VERSION = MODEL_ELEMENT_FEATURE_COUNT + 20;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 21;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.CategoryImpl <em>Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.CategoryImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getCategory()
	 * @generated
	 */
	int CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__PARENT = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CHILDREN = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ProfilImpl <em>Profil</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ProfilImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getProfil()
	 * @generated
	 */
	int PROFIL = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFIL__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFIL__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFIL__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFIL__RESOURCE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Profil</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFIL_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ActionImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 5;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.FileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.FileImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getFile()
	 * @generated
	 */
	int FILE = 6;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.PluginImpl <em>Plugin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.PluginImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getPlugin()
	 * @generated
	 */
	int PLUGIN = 7;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ExeImpl <em>Exe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ExeImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getExe()
	 * @generated
	 */
	int EXE = 8;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.ConfigurationImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 9;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.LibraryImpl <em>Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.LibraryImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getLibrary()
	 * @generated
	 */
	int LIBRARY = 10;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.PackageImpl <em>Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.PackageImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getPackage()
	 * @generated
	 */
	int PACKAGE = 11;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.GroupImpl <em>Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.GroupImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getGroup()
	 * @generated
	 */
	int GROUP = 12;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.StateImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getState()
	 * @generated
	 */
	int STATE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__RESOURCE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DEFAULT_ACTION = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__IMAGE = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DESCRIPTION = MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESCRIPTION = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Java Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__JAVA_CLASS_NAME = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__NAME_ID = RESOURCE__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__OWNER = RESOURCE__OWNER;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__CATEGORY = RESOURCE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__PROFIL = RESOURCE__PROFIL;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__STATE = RESOURCE__STATE;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__NAME_WEB_FILE = RESOURCE__NAME_WEB_FILE;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__SITE_URL = RESOURCE__SITE_URL;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__ENTRY = RESOURCE__ENTRY;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__IS_LOCAL_FILE = RESOURCE__IS_LOCAL_FILE;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__TARGET_NAME_FILE = RESOURCE__TARGET_NAME_FILE;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__TARGET_PATH_FILE = RESOURCE__TARGET_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__IS_LOCAL_LICENSE = RESOURCE__IS_LOCAL_LICENSE;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__LICENSE_NAME = RESOURCE__LICENSE_NAME;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__LICENSE_URL = RESOURCE__LICENSE_URL;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__LICENSE_PATH_FILE = RESOURCE__LICENSE_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__GROUP = RESOURCE__GROUP;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__DESCRIPTION = RESOURCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__REQUIRED = RESOURCE__REQUIRED;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__REQUIRE = RESOURCE__REQUIRE;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__IS_VISIBLE = RESOURCE__IS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__INCLUDES_IN = RESOURCE__INCLUDES_IN;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__JAVA_VM_VERSION = RESOURCE__JAVA_VM_VERSION;

	/**
	 * The feature id for the '<em><b>Link Exe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__LINK_EXE = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__NAME_ID = RESOURCE__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__OWNER = RESOURCE__OWNER;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__CATEGORY = RESOURCE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__PROFIL = RESOURCE__PROFIL;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__STATE = RESOURCE__STATE;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__NAME_WEB_FILE = RESOURCE__NAME_WEB_FILE;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__SITE_URL = RESOURCE__SITE_URL;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__ENTRY = RESOURCE__ENTRY;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__IS_LOCAL_FILE = RESOURCE__IS_LOCAL_FILE;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__TARGET_NAME_FILE = RESOURCE__TARGET_NAME_FILE;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__TARGET_PATH_FILE = RESOURCE__TARGET_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__IS_LOCAL_LICENSE = RESOURCE__IS_LOCAL_LICENSE;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__LICENSE_NAME = RESOURCE__LICENSE_NAME;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__LICENSE_URL = RESOURCE__LICENSE_URL;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__LICENSE_PATH_FILE = RESOURCE__LICENSE_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__GROUP = RESOURCE__GROUP;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__DESCRIPTION = RESOURCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__REQUIRED = RESOURCE__REQUIRED;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__REQUIRE = RESOURCE__REQUIRE;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__IS_VISIBLE = RESOURCE__IS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__INCLUDES_IN = RESOURCE__INCLUDES_IN;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__JAVA_VM_VERSION = RESOURCE__JAVA_VM_VERSION;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__LONG_NAME = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__VERSION = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Applications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__APPLICATIONS = RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__PACKAGE = RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To Decompress</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__TO_DECOMPRESS = RESOURCE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Plugin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__NAME_ID = RESOURCE__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__OWNER = RESOURCE__OWNER;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__CATEGORY = RESOURCE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__PROFIL = RESOURCE__PROFIL;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__STATE = RESOURCE__STATE;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__NAME_WEB_FILE = RESOURCE__NAME_WEB_FILE;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__SITE_URL = RESOURCE__SITE_URL;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__ENTRY = RESOURCE__ENTRY;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__IS_LOCAL_FILE = RESOURCE__IS_LOCAL_FILE;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__TARGET_NAME_FILE = RESOURCE__TARGET_NAME_FILE;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__TARGET_PATH_FILE = RESOURCE__TARGET_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__IS_LOCAL_LICENSE = RESOURCE__IS_LOCAL_LICENSE;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__LICENSE_NAME = RESOURCE__LICENSE_NAME;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__LICENSE_URL = RESOURCE__LICENSE_URL;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__LICENSE_PATH_FILE = RESOURCE__LICENSE_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__GROUP = RESOURCE__GROUP;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__DESCRIPTION = RESOURCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__REQUIRED = RESOURCE__REQUIRED;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__REQUIRE = RESOURCE__REQUIRE;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__IS_VISIBLE = RESOURCE__IS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__INCLUDES_IN = RESOURCE__INCLUDES_IN;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__JAVA_VM_VERSION = RESOURCE__JAVA_VM_VERSION;

	/**
	 * The feature id for the '<em><b>Type OS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__TYPE_OS = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Link File</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__LINK_FILE = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Plugins</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE__PLUGINS = RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Exe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owned State</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_STATE = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owned Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_ACTION = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Owned Profil</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_PROFIL = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_RESOURCE = MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Owned Category</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_CATEGORY = MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Owned Package</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_PACKAGE = MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__OWNED_GROUP = MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Config Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__CONFIG_VERSION = MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__NAME = FILE__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__NAME_ID = FILE__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__OWNER = FILE__OWNER;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__CATEGORY = FILE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__PROFIL = FILE__PROFIL;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__STATE = FILE__STATE;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__NAME_WEB_FILE = FILE__NAME_WEB_FILE;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__SITE_URL = FILE__SITE_URL;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__ENTRY = FILE__ENTRY;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__IS_LOCAL_FILE = FILE__IS_LOCAL_FILE;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__TARGET_NAME_FILE = FILE__TARGET_NAME_FILE;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__TARGET_PATH_FILE = FILE__TARGET_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__IS_LOCAL_LICENSE = FILE__IS_LOCAL_LICENSE;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__LICENSE_NAME = FILE__LICENSE_NAME;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__LICENSE_URL = FILE__LICENSE_URL;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__LICENSE_PATH_FILE = FILE__LICENSE_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__GROUP = FILE__GROUP;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__DESCRIPTION = FILE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__REQUIRED = FILE__REQUIRED;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__REQUIRE = FILE__REQUIRE;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__IS_VISIBLE = FILE__IS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__INCLUDES_IN = FILE__INCLUDES_IN;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__JAVA_VM_VERSION = FILE__JAVA_VM_VERSION;

	/**
	 * The feature id for the '<em><b>Link Exe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__LINK_EXE = FILE__LINK_EXE;

	/**
	 * The feature id for the '<em><b>Plugin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__PLUGIN = FILE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__EXE = FILE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__GROUP_NAME = FILE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY__VERSION = FILE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_FEATURE_COUNT = FILE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__NAME = MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__NAME_ID = MODEL_ELEMENT__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__OWNER = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__DESCRIPTION = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.impl.FeatureImpl
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Name ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME_ID = RESOURCE__NAME_ID;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__OWNER = RESOURCE__OWNER;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__CATEGORY = RESOURCE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Profil</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PROFIL = RESOURCE__PROFIL;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__STATE = RESOURCE__STATE;

	/**
	 * The feature id for the '<em><b>Name Web File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME_WEB_FILE = RESOURCE__NAME_WEB_FILE;

	/**
	 * The feature id for the '<em><b>Site URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__SITE_URL = RESOURCE__SITE_URL;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__ENTRY = RESOURCE__ENTRY;

	/**
	 * The feature id for the '<em><b>Is Local File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IS_LOCAL_FILE = RESOURCE__IS_LOCAL_FILE;

	/**
	 * The feature id for the '<em><b>Target Name File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__TARGET_NAME_FILE = RESOURCE__TARGET_NAME_FILE;

	/**
	 * The feature id for the '<em><b>Target Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__TARGET_PATH_FILE = RESOURCE__TARGET_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Is Local License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IS_LOCAL_LICENSE = RESOURCE__IS_LOCAL_LICENSE;

	/**
	 * The feature id for the '<em><b>License Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LICENSE_NAME = RESOURCE__LICENSE_NAME;

	/**
	 * The feature id for the '<em><b>License URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LICENSE_URL = RESOURCE__LICENSE_URL;

	/**
	 * The feature id for the '<em><b>License Path File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LICENSE_PATH_FILE = RESOURCE__LICENSE_PATH_FILE;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__GROUP = RESOURCE__GROUP;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__DESCRIPTION = RESOURCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__REQUIRED = RESOURCE__REQUIRED;

	/**
	 * The feature id for the '<em><b>Require</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__REQUIRE = RESOURCE__REQUIRE;

	/**
	 * The feature id for the '<em><b>Is Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IS_VISIBLE = RESOURCE__IS_VISIBLE;

	/**
	 * The feature id for the '<em><b>Includes In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__INCLUDES_IN = RESOURCE__INCLUDES_IN;

	/**
	 * The feature id for the '<em><b>Java VM Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__JAVA_VM_VERSION = RESOURCE__JAVA_VM_VERSION;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LONG_NAME = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__VERSION = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To Decompress</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__TO_DECOMPRESS = RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.openembedd.checker.modelConfig.EnumKindOfInclusion <em>Enum Kind Of Inclusion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.openembedd.checker.modelConfig.EnumKindOfInclusion
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getEnumKindOfInclusion()
	 * @generated
	 */
	int ENUM_KIND_OF_INCLUSION = 14;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getString()
	 * @generated
	 */
	int STRING = 15;

	/**
	 * The meta object id for the '<em>Integer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Integer
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getInteger()
	 * @generated
	 */
	int INTEGER = 16;

	/**
	 * The meta object id for the '<em>Boolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Boolean
	 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getBoolean()
	 * @generated
	 */
	int BOOLEAN = 17;


	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Resource#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getOwner()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Owner();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Resource#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Category</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getCategory()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Category();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Resource#getProfil <em>Profil</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Profil</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getProfil()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Profil();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Resource#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getState()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_State();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getNameWebFile <em>Name Web File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Web File</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getNameWebFile()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_NameWebFile();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getSiteURL <em>Site URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Site URL</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getSiteURL()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_SiteURL();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entry</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getEntry()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_Entry();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getIsLocalFile <em>Is Local File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Local File</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getIsLocalFile()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_IsLocalFile();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getTargetNameFile <em>Target Name File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Name File</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getTargetNameFile()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_TargetNameFile();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getTargetPathFile <em>Target Path File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Path File</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getTargetPathFile()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_TargetPathFile();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getIsLocalLicense <em>Is Local License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Local License</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getIsLocalLicense()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_IsLocalLicense();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getLicenseName <em>License Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>License Name</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getLicenseName()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_LicenseName();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getLicenseURL <em>License URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>License URL</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getLicenseURL()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_LicenseURL();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getLicensePathFile <em>License Path File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>License Path File</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getLicensePathFile()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_LicensePathFile();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Resource#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getGroup()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Group();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getDescription()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_Description();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Resource#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getRequired()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Required();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Resource#getRequire <em>Require</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Require</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getRequire()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Require();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getIsVisible <em>Is Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Visible</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getIsVisible()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_IsVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getIncludesIn <em>Includes In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Includes In</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getIncludesIn()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_IncludesIn();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Resource#getJavaVMVersion <em>Java VM Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Java VM Version</em>'.
	 * @see org.openembedd.checker.modelConfig.Resource#getJavaVMVersion()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_JavaVMVersion();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Category <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category</em>'.
	 * @see org.openembedd.checker.modelConfig.Category
	 * @generated
	 */
	EClass getCategory();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Category#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Category#getOwner()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Owner();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Category#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see org.openembedd.checker.modelConfig.Category#getParent()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Parent();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Category#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see org.openembedd.checker.modelConfig.Category#getChildren()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Children();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Element</em>'.
	 * @see org.openembedd.checker.modelConfig.ModelElement
	 * @generated
	 */
	EClass getModelElement();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.ModelElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.openembedd.checker.modelConfig.ModelElement#getName()
	 * @see #getModelElement()
	 * @generated
	 */
	EAttribute getModelElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.ModelElement#getNameID <em>Name ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name ID</em>'.
	 * @see org.openembedd.checker.modelConfig.ModelElement#getNameID()
	 * @see #getModelElement()
	 * @generated
	 */
	EAttribute getModelElement_NameID();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Profil <em>Profil</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Profil</em>'.
	 * @see org.openembedd.checker.modelConfig.Profil
	 * @generated
	 */
	EClass getProfil();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Profil#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Profil#getOwner()
	 * @see #getProfil()
	 * @generated
	 */
	EReference getProfil_Owner();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Profil#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Resource</em>'.
	 * @see org.openembedd.checker.modelConfig.Profil#getResource()
	 * @see #getProfil()
	 * @generated
	 */
	EReference getProfil_Resource();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see org.openembedd.checker.modelConfig.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Action#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Action#getOwner()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Owner();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Action#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.openembedd.checker.modelConfig.Action#getDescription()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Action#getJavaClassName <em>Java Class Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Java Class Name</em>'.
	 * @see org.openembedd.checker.modelConfig.Action#getJavaClassName()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_JavaClassName();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.File <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see org.openembedd.checker.modelConfig.File
	 * @generated
	 */
	EClass getFile();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.File#getLinkExe <em>Link Exe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Link Exe</em>'.
	 * @see org.openembedd.checker.modelConfig.File#getLinkExe()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_LinkExe();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Plugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin
	 * @generated
	 */
	EClass getPlugin();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Plugin#getLongName <em>Long Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Name</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin#getLongName()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_LongName();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Plugin#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin#getVersion()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Version();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Plugin#getApplications <em>Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Applications</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin#getApplications()
	 * @see #getPlugin()
	 * @generated
	 */
	EReference getPlugin_Applications();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Plugin#getPackage <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Package</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin#getPackage()
	 * @see #getPlugin()
	 * @generated
	 */
	EReference getPlugin_Package();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Plugin#getToDecompress <em>To Decompress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Decompress</em>'.
	 * @see org.openembedd.checker.modelConfig.Plugin#getToDecompress()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_ToDecompress();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Exe <em>Exe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exe</em>'.
	 * @see org.openembedd.checker.modelConfig.Exe
	 * @generated
	 */
	EClass getExe();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Exe#getTypeOS <em>Type OS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type OS</em>'.
	 * @see org.openembedd.checker.modelConfig.Exe#getTypeOS()
	 * @see #getExe()
	 * @generated
	 */
	EAttribute getExe_TypeOS();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Exe#getLinkFile <em>Link File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Link File</em>'.
	 * @see org.openembedd.checker.modelConfig.Exe#getLinkFile()
	 * @see #getExe()
	 * @generated
	 */
	EReference getExe_LinkFile();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.Exe#getPlugins <em>Plugins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Plugins</em>'.
	 * @see org.openembedd.checker.modelConfig.Exe#getPlugins()
	 * @see #getExe()
	 * @generated
	 */
	EReference getExe_Plugins();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedState <em>Owned State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned State</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedState()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedState();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedAction <em>Owned Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Action</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedAction()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedAction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedProfil <em>Owned Profil</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Profil</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedProfil()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedProfil();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedResource <em>Owned Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Resource</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedResource()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedResource();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedCategory <em>Owned Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Category</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedCategory()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedCategory();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedPackage <em>Owned Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Package</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedPackage()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedGroup <em>Owned Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Group</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedGroup()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_OwnedGroup();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Configuration#getConfigVersion <em>Config Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Config Version</em>'.
	 * @see org.openembedd.checker.modelConfig.Configuration#getConfigVersion()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_ConfigVersion();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Library <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library</em>'.
	 * @see org.openembedd.checker.modelConfig.Library
	 * @generated
	 */
	EClass getLibrary();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Library#getPlugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Plugin</em>'.
	 * @see org.openembedd.checker.modelConfig.Library#getPlugin()
	 * @see #getLibrary()
	 * @generated
	 */
	EReference getLibrary_Plugin();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.Library#getExe <em>Exe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Exe</em>'.
	 * @see org.openembedd.checker.modelConfig.Library#getExe()
	 * @see #getLibrary()
	 * @generated
	 */
	EReference getLibrary_Exe();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Library#getGroupName <em>Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Name</em>'.
	 * @see org.openembedd.checker.modelConfig.Library#getGroupName()
	 * @see #getLibrary()
	 * @generated
	 */
	EAttribute getLibrary_GroupName();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Library#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.openembedd.checker.modelConfig.Library#getVersion()
	 * @see #getLibrary()
	 * @generated
	 */
	EAttribute getLibrary_Version();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Package <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package</em>'.
	 * @see org.openembedd.checker.modelConfig.Package
	 * @generated
	 */
	EClass getPackage();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Package#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Package#getOwner()
	 * @see #getPackage()
	 * @generated
	 */
	EReference getPackage_Owner();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Group <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group</em>'.
	 * @see org.openembedd.checker.modelConfig.Group
	 * @generated
	 */
	EClass getGroup();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.Group#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.Group#getOwner()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Owner();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Group#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.openembedd.checker.modelConfig.Group#getDescription()
	 * @see #getGroup()
	 * @generated
	 */
	EAttribute getGroup_Description();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.openembedd.checker.modelConfig.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the container reference '{@link org.openembedd.checker.modelConfig.State#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see org.openembedd.checker.modelConfig.State#getOwner()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Owner();

	/**
	 * Returns the meta object for the reference list '{@link org.openembedd.checker.modelConfig.State#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Resource</em>'.
	 * @see org.openembedd.checker.modelConfig.State#getResource()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Resource();

	/**
	 * Returns the meta object for the reference '{@link org.openembedd.checker.modelConfig.State#getDefaultAction <em>Default Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Action</em>'.
	 * @see org.openembedd.checker.modelConfig.State#getDefaultAction()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_DefaultAction();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.State#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.openembedd.checker.modelConfig.State#getImage()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.State#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.openembedd.checker.modelConfig.State#getDescription()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Description();

	/**
	 * Returns the meta object for class '{@link org.openembedd.checker.modelConfig.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see org.openembedd.checker.modelConfig.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Feature#getLongName <em>Long Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Name</em>'.
	 * @see org.openembedd.checker.modelConfig.Feature#getLongName()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_LongName();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Feature#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.openembedd.checker.modelConfig.Feature#getVersion()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.openembedd.checker.modelConfig.Feature#getToDecompress <em>To Decompress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Decompress</em>'.
	 * @see org.openembedd.checker.modelConfig.Feature#getToDecompress()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_ToDecompress();

	/**
	 * Returns the meta object for enum '{@link org.openembedd.checker.modelConfig.EnumKindOfInclusion <em>Enum Kind Of Inclusion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Enum Kind Of Inclusion</em>'.
	 * @see org.openembedd.checker.modelConfig.EnumKindOfInclusion
	 * @generated
	 */
	EEnum getEnumKindOfInclusion();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '{@link java.lang.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Integer</em>'.
	 * @see java.lang.Integer
	 * @model instanceClass="java.lang.Integer"
	 * @generated
	 */
	EDataType getInteger();

	/**
	 * Returns the meta object for data type '{@link java.lang.Boolean <em>Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Boolean</em>'.
	 * @see java.lang.Boolean
	 * @model instanceClass="java.lang.Boolean"
	 * @generated
	 */
	EDataType getBoolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	modelConfigFactory getmodelConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals  {
		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ResourceImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__OWNER = eINSTANCE.getResource_Owner();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__CATEGORY = eINSTANCE.getResource_Category();

		/**
		 * The meta object literal for the '<em><b>Profil</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__PROFIL = eINSTANCE.getResource_Profil();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__STATE = eINSTANCE.getResource_State();

		/**
		 * The meta object literal for the '<em><b>Name Web File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__NAME_WEB_FILE = eINSTANCE.getResource_NameWebFile();

		/**
		 * The meta object literal for the '<em><b>Site URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__SITE_URL = eINSTANCE.getResource_SiteURL();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__ENTRY = eINSTANCE.getResource_Entry();

		/**
		 * The meta object literal for the '<em><b>Is Local File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__IS_LOCAL_FILE = eINSTANCE.getResource_IsLocalFile();

		/**
		 * The meta object literal for the '<em><b>Target Name File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__TARGET_NAME_FILE = eINSTANCE.getResource_TargetNameFile();

		/**
		 * The meta object literal for the '<em><b>Target Path File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__TARGET_PATH_FILE = eINSTANCE.getResource_TargetPathFile();

		/**
		 * The meta object literal for the '<em><b>Is Local License</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__IS_LOCAL_LICENSE = eINSTANCE.getResource_IsLocalLicense();

		/**
		 * The meta object literal for the '<em><b>License Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__LICENSE_NAME = eINSTANCE.getResource_LicenseName();

		/**
		 * The meta object literal for the '<em><b>License URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__LICENSE_URL = eINSTANCE.getResource_LicenseURL();

		/**
		 * The meta object literal for the '<em><b>License Path File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__LICENSE_PATH_FILE = eINSTANCE.getResource_LicensePathFile();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__GROUP = eINSTANCE.getResource_Group();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__DESCRIPTION = eINSTANCE.getResource_Description();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__REQUIRED = eINSTANCE.getResource_Required();

		/**
		 * The meta object literal for the '<em><b>Require</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__REQUIRE = eINSTANCE.getResource_Require();

		/**
		 * The meta object literal for the '<em><b>Is Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__IS_VISIBLE = eINSTANCE.getResource_IsVisible();

		/**
		 * The meta object literal for the '<em><b>Includes In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__INCLUDES_IN = eINSTANCE.getResource_IncludesIn();

		/**
		 * The meta object literal for the '<em><b>Java VM Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__JAVA_VM_VERSION = eINSTANCE.getResource_JavaVMVersion();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.CategoryImpl <em>Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.CategoryImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getCategory()
		 * @generated
		 */
		EClass CATEGORY = eINSTANCE.getCategory();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__OWNER = eINSTANCE.getCategory_Owner();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__PARENT = eINSTANCE.getCategory_Parent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__CHILDREN = eINSTANCE.getCategory_Children();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ModelElementImpl <em>Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ModelElementImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getModelElement()
		 * @generated
		 */
		EClass MODEL_ELEMENT = eINSTANCE.getModelElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_ELEMENT__NAME = eINSTANCE.getModelElement_Name();

		/**
		 * The meta object literal for the '<em><b>Name ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_ELEMENT__NAME_ID = eINSTANCE.getModelElement_NameID();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ProfilImpl <em>Profil</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ProfilImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getProfil()
		 * @generated
		 */
		EClass PROFIL = eINSTANCE.getProfil();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROFIL__OWNER = eINSTANCE.getProfil_Owner();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROFIL__RESOURCE = eINSTANCE.getProfil_Resource();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ActionImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__OWNER = eINSTANCE.getAction_Owner();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__DESCRIPTION = eINSTANCE.getAction_Description();

		/**
		 * The meta object literal for the '<em><b>Java Class Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__JAVA_CLASS_NAME = eINSTANCE.getAction_JavaClassName();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.FileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.FileImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getFile()
		 * @generated
		 */
		EClass FILE = eINSTANCE.getFile();

		/**
		 * The meta object literal for the '<em><b>Link Exe</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__LINK_EXE = eINSTANCE.getFile_LinkExe();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.PluginImpl <em>Plugin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.PluginImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getPlugin()
		 * @generated
		 */
		EClass PLUGIN = eINSTANCE.getPlugin();

		/**
		 * The meta object literal for the '<em><b>Long Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__LONG_NAME = eINSTANCE.getPlugin_LongName();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__VERSION = eINSTANCE.getPlugin_Version();

		/**
		 * The meta object literal for the '<em><b>Applications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLUGIN__APPLICATIONS = eINSTANCE.getPlugin_Applications();

		/**
		 * The meta object literal for the '<em><b>Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLUGIN__PACKAGE = eINSTANCE.getPlugin_Package();

		/**
		 * The meta object literal for the '<em><b>To Decompress</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__TO_DECOMPRESS = eINSTANCE.getPlugin_ToDecompress();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ExeImpl <em>Exe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ExeImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getExe()
		 * @generated
		 */
		EClass EXE = eINSTANCE.getExe();

		/**
		 * The meta object literal for the '<em><b>Type OS</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXE__TYPE_OS = eINSTANCE.getExe_TypeOS();

		/**
		 * The meta object literal for the '<em><b>Link File</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXE__LINK_FILE = eINSTANCE.getExe_LinkFile();

		/**
		 * The meta object literal for the '<em><b>Plugins</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXE__PLUGINS = eINSTANCE.getExe_Plugins();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.ConfigurationImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Owned State</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_STATE = eINSTANCE.getConfiguration_OwnedState();

		/**
		 * The meta object literal for the '<em><b>Owned Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_ACTION = eINSTANCE.getConfiguration_OwnedAction();

		/**
		 * The meta object literal for the '<em><b>Owned Profil</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_PROFIL = eINSTANCE.getConfiguration_OwnedProfil();

		/**
		 * The meta object literal for the '<em><b>Owned Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_RESOURCE = eINSTANCE.getConfiguration_OwnedResource();

		/**
		 * The meta object literal for the '<em><b>Owned Category</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_CATEGORY = eINSTANCE.getConfiguration_OwnedCategory();

		/**
		 * The meta object literal for the '<em><b>Owned Package</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_PACKAGE = eINSTANCE.getConfiguration_OwnedPackage();

		/**
		 * The meta object literal for the '<em><b>Owned Group</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__OWNED_GROUP = eINSTANCE.getConfiguration_OwnedGroup();

		/**
		 * The meta object literal for the '<em><b>Config Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__CONFIG_VERSION = eINSTANCE.getConfiguration_ConfigVersion();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.LibraryImpl <em>Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.LibraryImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getLibrary()
		 * @generated
		 */
		EClass LIBRARY = eINSTANCE.getLibrary();

		/**
		 * The meta object literal for the '<em><b>Plugin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY__PLUGIN = eINSTANCE.getLibrary_Plugin();

		/**
		 * The meta object literal for the '<em><b>Exe</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY__EXE = eINSTANCE.getLibrary_Exe();

		/**
		 * The meta object literal for the '<em><b>Group Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIBRARY__GROUP_NAME = eINSTANCE.getLibrary_GroupName();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIBRARY__VERSION = eINSTANCE.getLibrary_Version();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.PackageImpl <em>Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.PackageImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getPackage()
		 * @generated
		 */
		EClass PACKAGE = eINSTANCE.getPackage();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE__OWNER = eINSTANCE.getPackage_Owner();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.GroupImpl <em>Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.GroupImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getGroup()
		 * @generated
		 */
		EClass GROUP = eINSTANCE.getGroup();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__OWNER = eINSTANCE.getGroup_Owner();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP__DESCRIPTION = eINSTANCE.getGroup_Description();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.StateImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__OWNER = eINSTANCE.getState_Owner();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__RESOURCE = eINSTANCE.getState_Resource();

		/**
		 * The meta object literal for the '<em><b>Default Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__DEFAULT_ACTION = eINSTANCE.getState_DefaultAction();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__IMAGE = eINSTANCE.getState_Image();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__DESCRIPTION = eINSTANCE.getState_Description();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.impl.FeatureImpl
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '<em><b>Long Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__LONG_NAME = eINSTANCE.getFeature_LongName();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__VERSION = eINSTANCE.getFeature_Version();

		/**
		 * The meta object literal for the '<em><b>To Decompress</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__TO_DECOMPRESS = eINSTANCE.getFeature_ToDecompress();

		/**
		 * The meta object literal for the '{@link org.openembedd.checker.modelConfig.EnumKindOfInclusion <em>Enum Kind Of Inclusion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.openembedd.checker.modelConfig.EnumKindOfInclusion
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getEnumKindOfInclusion()
		 * @generated
		 */
		EEnum ENUM_KIND_OF_INCLUSION = eINSTANCE.getEnumKindOfInclusion();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>Integer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Integer
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getInteger()
		 * @generated
		 */
		EDataType INTEGER = eINSTANCE.getInteger();

		/**
		 * The meta object literal for the '<em>Boolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Boolean
		 * @see org.openembedd.checker.modelConfig.impl.modelConfigPackageImpl#getBoolean()
		 * @generated
		 */
		EDataType BOOLEAN = eINSTANCE.getBoolean();

	}

} //modelConfigPackage
