/**
 * <copyright>
 * </copyright>
 *
 * $Id: Library.java,v 1.2 2007-07-10 15:42:34 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Library#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Library#getExe <em>Exe</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Library#getGroupName <em>Group Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Library#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getLibrary()
 * @model
 * @generated
 */
public interface Library extends File {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Plugin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plugin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin</em>' reference.
	 * @see #setPlugin(Plugin)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getLibrary_Plugin()
	 * @model
	 * @generated
	 */
	Plugin getPlugin();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Library#getPlugin <em>Plugin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plugin</em>' reference.
	 * @see #getPlugin()
	 * @generated
	 */
	void setPlugin(Plugin value);

	/**
	 * Returns the value of the '<em><b>Exe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exe</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exe</em>' reference.
	 * @see #setExe(Exe)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getLibrary_Exe()
	 * @model
	 * @generated
	 */
	Exe getExe();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Library#getExe <em>Exe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exe</em>' reference.
	 * @see #getExe()
	 * @generated
	 */
	void setExe(Exe value);

	/**
	 * Returns the value of the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Name</em>' attribute.
	 * @see #setGroupName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getLibrary_GroupName()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getGroupName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Library#getGroupName <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Name</em>' attribute.
	 * @see #getGroupName()
	 * @generated
	 */
	void setGroupName(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getLibrary_Version()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Library#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

} // Library