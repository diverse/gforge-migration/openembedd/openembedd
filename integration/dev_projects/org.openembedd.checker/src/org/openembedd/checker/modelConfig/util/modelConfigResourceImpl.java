/**
 * <copyright>
 * </copyright>
 *
 * $Id: modelConfigResourceImpl.java,v 1.2 2007-07-10 15:42:42 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.openembedd.checker.modelConfig.util.modelConfigResourceFactoryImpl
 * @generated
 */
public class modelConfigResourceImpl extends XMIResourceImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public modelConfigResourceImpl(URI uri) {
		super(uri);
	}

} //modelConfigResourceImpl
