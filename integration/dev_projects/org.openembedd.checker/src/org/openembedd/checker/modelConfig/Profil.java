/**
 * <copyright>
 * </copyright>
 *
 * $Id: Profil.java,v 1.4 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Profil</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Profil#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Profil#getResource <em>Resource</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getProfil()
 * @model
 * @generated
 */
public interface Profil extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedProfil <em>Owned Profil</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getProfil_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedProfil
	 * @model opposite="ownedProfil" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Profil#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Resource}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Resource#getProfil <em>Profil</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getProfil_Resource()
	 * @see org.openembedd.checker.modelConfig.Resource#getProfil
	 * @model opposite="profil"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	State processState();

} // Profil