/**
 * <copyright>
 * </copyright>
 *
 * $Id: ResourceImpl.java,v 1.13 2007-12-20 17:50:29 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.openembedd.checker.action.CompatibilityChecker;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.check.library.DownloadWebFile;
import org.openembedd.checker.modelConfig.Category;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.EnumKindOfInclusion;
import org.openembedd.checker.modelConfig.Group;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.modelConfig.Resource;
import org.openembedd.checker.modelConfig.State;
import org.openembedd.checker.modelConfig.modelConfigPackage;
import org.osgi.framework.Version;
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getProfil <em>Profil</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getState <em>State</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getNameWebFile <em>Name Web File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getSiteURL <em>Site URL</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getIsLocalFile <em>Is Local File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getTargetNameFile <em>Target Name File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getTargetPathFile <em>Target Path File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getIsLocalLicense <em>Is Local License</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getLicenseName <em>License Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getLicenseURL <em>License URL</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getLicensePathFile <em>License Path File</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getRequired <em>Required</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getRequire <em>Require</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getIsVisible <em>Is Visible</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getIncludesIn <em>Includes In</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.impl.ResourceImpl#getJavaVMVersion <em>Java VM Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ResourceImpl extends ModelElementImpl implements Resource {
	/**
	 * true when the last download fails with an error on the target
	 * @generated NOT
	 */
	protected Boolean errorTarget = false;
	
	/**
	 * true when the last download fails with an error on the source
	 * @generated NOT
	 */
	protected Boolean errorSource = false;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "OpenEmbeDD";

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected Category category;

	/**
	 * The cached value of the '{@link #getProfil() <em>Profil</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfil()
	 * @generated
	 * @ordered
	 */
	protected EList<Profil> profil;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state;

	/**
	 * The default value of the '{@link #getNameWebFile() <em>Name Web File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameWebFile()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_WEB_FILE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getNameWebFile() <em>Name Web File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameWebFile()
	 * @generated
	 * @ordered
	 */
	protected String nameWebFile = NAME_WEB_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSiteURL() <em>Site URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSiteURL()
	 * @generated
	 * @ordered
	 */
	protected static final String SITE_URL_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getSiteURL() <em>Site URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSiteURL()
	 * @generated
	 * @ordered
	 */
	protected String siteURL = SITE_URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getEntry() <em>Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntry()
	 * @generated
	 * @ordered
	 */
	protected static final String ENTRY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEntry() <em>Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntry()
	 * @generated
	 * @ordered
	 */
	protected String entry = ENTRY_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsLocalFile() <em>Is Local File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsLocalFile()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_LOCAL_FILE_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getIsLocalFile() <em>Is Local File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsLocalFile()
	 * @generated
	 * @ordered
	 */
	protected Boolean isLocalFile = IS_LOCAL_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetNameFile() <em>Target Name File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetNameFile()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_NAME_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetNameFile() <em>Target Name File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetNameFile()
	 * @generated
	 * @ordered
	 */
	protected String targetNameFile = TARGET_NAME_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetPathFile() <em>Target Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPathFile()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_PATH_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetPathFile() <em>Target Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPathFile()
	 * @generated
	 * @ordered
	 */
	protected String targetPathFile = TARGET_PATH_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsLocalLicense() <em>Is Local License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsLocalLicense()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_LOCAL_LICENSE_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getIsLocalLicense() <em>Is Local License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsLocalLicense()
	 * @generated
	 * @ordered
	 */
	protected Boolean isLocalLicense = IS_LOCAL_LICENSE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLicenseName() <em>License Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicenseName()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicenseName() <em>License Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicenseName()
	 * @generated
	 * @ordered
	 */
	protected String licenseName = LICENSE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLicenseURL() <em>License URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicenseURL()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicenseURL() <em>License URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicenseURL()
	 * @generated
	 * @ordered
	 */
	protected String licenseURL = LICENSE_URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLicensePathFile() <em>License Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicensePathFile()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_PATH_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicensePathFile() <em>License Path File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicensePathFile()
	 * @generated
	 * @ordered
	 */
	protected String licensePathFile = LICENSE_PATH_FILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected Group group;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = "description of the resource";

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRequired() <em>Required</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequired()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> required;

	/**
	 * The cached value of the '{@link #getRequire() <em>Require</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequire()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> require;

	/**
	 * The default value of the '{@link #getIsVisible() <em>Is Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsVisible()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_VISIBLE_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getIsVisible() <em>Is Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsVisible()
	 * @generated
	 * @ordered
	 */
	protected Boolean isVisible = IS_VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getIncludesIn() <em>Includes In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludesIn()
	 * @generated
	 * @ordered
	 */
	protected static final EnumKindOfInclusion INCLUDES_IN_EDEFAULT = EnumKindOfInclusion.NONE_LITERAL;

	/**
	 * The cached value of the '{@link #getIncludesIn() <em>Includes In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludesIn()
	 * @generated
	 * @ordered
	 */
	protected EnumKindOfInclusion includesIn = INCLUDES_IN_EDEFAULT;

	/**
	 * The default value of the '{@link #getJavaVMVersion() <em>Java VM Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavaVMVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String JAVA_VM_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJavaVMVersion() <em>Java VM Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavaVMVersion()
	 * @generated
	 * @ordered
	 */
	protected String javaVMVersion = JAVA_VM_VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return modelConfigPackage.Literals.RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getOwner() {
		if (eContainerFeatureID != modelConfigPackage.RESOURCE__OWNER) return null;
		return (Configuration)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(Configuration newOwner, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOwner, modelConfigPackage.RESOURCE__OWNER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(Configuration newOwner) {
		if (newOwner != eInternalContainer() || (eContainerFeatureID != modelConfigPackage.RESOURCE__OWNER && newOwner != null)) {
			if (EcoreUtil.isAncestor(this, newOwner))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, modelConfigPackage.CONFIGURATION__OWNED_RESOURCE, Configuration.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getCategory() {
		if (category != null && category.eIsProxy()) {
			InternalEObject oldCategory = (InternalEObject)category;
			category = (Category)eResolveProxy(oldCategory);
			if (category != oldCategory) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.RESOURCE__CATEGORY, oldCategory, category));
			}
		}
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category basicGetCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(Category newCategory) {
		Category oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Profil> getProfil() {
		if (profil == null) {
			profil = new EObjectWithInverseResolvingEList.ManyInverse<Profil>(Profil.class, this, modelConfigPackage.RESOURCE__PROFIL, modelConfigPackage.PROFIL__RESOURCE);
		}
		return profil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject)state;
			state = (State)eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.RESOURCE__STATE, oldState, state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetState(State newState, NotificationChain msgs) {
		State oldState = state;
		state = newState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__STATE, oldState, newState);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		if (newState != state) {
			NotificationChain msgs = null;
			if (state != null)
				msgs = ((InternalEObject)state).eInverseRemove(this, modelConfigPackage.STATE__RESOURCE, State.class, msgs);
			if (newState != null)
				msgs = ((InternalEObject)newState).eInverseAdd(this, modelConfigPackage.STATE__RESOURCE, State.class, msgs);
			msgs = basicSetState(newState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__STATE, newState, newState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNameWebFile() {
		return nameWebFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameWebFile(String newNameWebFile) {
		String oldNameWebFile = nameWebFile;
		nameWebFile = newNameWebFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__NAME_WEB_FILE, oldNameWebFile, nameWebFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSiteURL() {
		return siteURL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSiteURL(String newSiteURL) {
		String oldSiteURL = siteURL;
		siteURL = newSiteURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__SITE_URL, oldSiteURL, siteURL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEntry() {
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntry(String newEntry) {
		String oldEntry = entry;
		entry = newEntry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__ENTRY, oldEntry, entry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsLocalFile() {
		return isLocalFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLocalFile(Boolean newIsLocalFile) {
		Boolean oldIsLocalFile = isLocalFile;
		isLocalFile = newIsLocalFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__IS_LOCAL_FILE, oldIsLocalFile, isLocalFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetNameFile() {
		return targetNameFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetNameFile(String newTargetNameFile) {
		String oldTargetNameFile = targetNameFile;
		targetNameFile = newTargetNameFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__TARGET_NAME_FILE, oldTargetNameFile, targetNameFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetPathFile() {
		return targetPathFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetPathFile(String newTargetPathFile) {
		String oldTargetPathFile = targetPathFile;
		targetPathFile = newTargetPathFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__TARGET_PATH_FILE, oldTargetPathFile, targetPathFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsLocalLicense() {
		return isLocalLicense;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLocalLicense(Boolean newIsLocalLicense) {
		Boolean oldIsLocalLicense = isLocalLicense;
		isLocalLicense = newIsLocalLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE, oldIsLocalLicense, isLocalLicense));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLicenseName() {
		return licenseName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLicenseName(String newLicenseName) {
		String oldLicenseName = licenseName;
		licenseName = newLicenseName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__LICENSE_NAME, oldLicenseName, licenseName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLicenseURL() {
		return licenseURL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLicenseURL(String newLicenseURL) {
		String oldLicenseURL = licenseURL;
		licenseURL = newLicenseURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__LICENSE_URL, oldLicenseURL, licenseURL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLicensePathFile() {
		return licensePathFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLicensePathFile(String newLicensePathFile) {
		String oldLicensePathFile = licensePathFile;
		licensePathFile = newLicensePathFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__LICENSE_PATH_FILE, oldLicensePathFile, licensePathFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group getGroup() {
		if (group != null && group.eIsProxy()) {
			InternalEObject oldGroup = (InternalEObject)group;
			group = (Group)eResolveProxy(oldGroup);
			if (group != oldGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, modelConfigPackage.RESOURCE__GROUP, oldGroup, group));
			}
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group basicGetGroup() {
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(Group newGroup) {
		Group oldGroup = group;
		group = newGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__GROUP, oldGroup, group));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getRequired() {
		if (required == null) {
			required = new EObjectWithInverseResolvingEList.ManyInverse<Resource>(Resource.class, this, modelConfigPackage.RESOURCE__REQUIRED, modelConfigPackage.RESOURCE__REQUIRE);
		}
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getRequire() {
		if (require == null) {
			require = new EObjectWithInverseResolvingEList.ManyInverse<Resource>(Resource.class, this, modelConfigPackage.RESOURCE__REQUIRE, modelConfigPackage.RESOURCE__REQUIRED);
		}
		return require;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsVisible() {
		return isVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsVisible(Boolean newIsVisible) {
		Boolean oldIsVisible = isVisible;
		isVisible = newIsVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__IS_VISIBLE, oldIsVisible, isVisible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumKindOfInclusion getIncludesIn() {
		return includesIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncludesIn(EnumKindOfInclusion newIncludesIn) {
		EnumKindOfInclusion oldIncludesIn = includesIn;
		includesIn = newIncludesIn == null ? INCLUDES_IN_EDEFAULT : newIncludesIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__INCLUDES_IN, oldIncludesIn, includesIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJavaVMVersion() {
		return javaVMVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavaVMVersion(String newJavaVMVersion) {
		String oldJavaVMVersion = javaVMVersion;
		javaVMVersion = newJavaVMVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, modelConfigPackage.RESOURCE__JAVA_VM_VERSION, oldJavaVMVersion, javaVMVersion));
	}

	/**
	 * method which return true if the resource already exists in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean exist() {
		//TODO
		return false;
	}

	/**
	 * method which return true if the resource exists with another version in the user environment
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean existIgnoreVersion() {
		return exist();
	}
	
	/**
	 * method which return the version of the resource which is installed, null else 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Version versionInstalled() {
		//TODO : File, Library, Exe (and other ones if necessary)
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getLicense() {
		String license = "";
		if (getIsLocalLicense().booleanValue()) {
			//license local got by plugin resource
			try {
				Path pathLicenseInResource = new Path(getLicensePathFile());
				Path pathLicenseNameInResource = new Path(getLicenseName());
				Path pathLicenseContributor = null;
				String pathL = getOwner().getPathLicenseContributor();
				if (pathL != null && !pathL.equals("")) 
					pathLicenseContributor = new Path(pathL);
				if ((pathLicenseInResource != null || pathLicenseContributor != null) && pathLicenseNameInResource != null) {
					Path pathR = null, pathC = null;
					if (pathLicenseInResource != null ) 
						pathR = (Path) pathLicenseInResource.append(pathLicenseNameInResource);
					if (pathLicenseContributor != null)
						pathC = (Path) pathLicenseContributor.append(pathLicenseNameInResource);
					URL context = Platform.getBundle(getOwner().getPluginContributorNameID()).getEntry("/");
					URL urlR = null, urlC = null;
					if (context != null) { 
						if (pathR != null)
							urlR = new URL(context, pathR.toPortableString());
						if (pathC != null)
							urlC = new URL(context, pathC.toPortableString());
					}
					if (urlR != null || urlC != null) {
						URL urlResolved = null;
						try {
							if (urlR != null)
								urlResolved = FileLocator.toFileURL(urlR);
						} catch (IOException e) {
							try {
								if (urlC != null)
									urlResolved = FileLocator.toFileURL(urlC);
							} catch (IOException e2) {
								license = "Error : local license not found\n";
								DownloadConstants.log("Error : local license not found", e2);
							}
						}
						if (urlResolved != null) {
							File licenseFile = new File(urlResolved.getPath().toString());
							if (licenseFile != null && licenseFile.exists() && licenseFile.isFile()) {
								FileReader  input = new FileReader(licenseFile);
								BufferedReader buffer = new BufferedReader(input);
								String line;
								StringBuffer content = new StringBuffer();
								while((line = buffer.readLine()) != null) {
									content.append(line);
								}
								buffer.close();
								input.close();
								license = content.toString();
							}
							else {
								license = "Error : local license not found\n";
							}
						}
						else {
							license = "Error : local license not found\n";
						}
					}
				}
			} catch (Exception e) {
				license = "Error : local license not found\n";
				DownloadConstants.log("Error : local license not found" + getLicenseName(), e);
			}
		}
		else {
			//license got by a web URL
			String url = getLicenseURL();
			StringBuffer content = new StringBuffer();
			String unique = DownloadWebFile.unique();
			unique = unique == null ? "tmp_license" : unique;
			String temp = DownloadWebFile.getTempPath();
			File targetTempPathFile = new File(DownloadWebFile.cutSepEnd(temp) + DownloadConstants.slash + unique);  
			String targetTempNameFile = null; 
			boolean fileReceived = false;
			try {
				DownloadWebFile downloadWebFile = new DownloadWebFile();
				URL source = new URL(url);
				if (source != null && targetTempPathFile != null) {
					String webNameFile = null;
					if (source.getFile() != null) {
						Path tempPath = new Path(source.getFile());
						String extension = tempPath.getFileExtension();
						if (extension != null) {
							String segments[] = tempPath.segments(); 
							webNameFile = segments[segments.length - 1]; 
						}
					}
					if (source.getFile() == null) {
						webNameFile = getLicenseName();
					}
					if (webNameFile == null || webNameFile.trim().equals("")) {
						webNameFile = getLicenseName();
					}
					if (webNameFile != null) {
						targetTempNameFile = webNameFile;
						downloadWebFile.setNames(url, webNameFile, targetTempPathFile.getPath(), targetTempNameFile);
						boolean accept = downloadWebFile.accept();
						boolean connected = accept ? downloadWebFile.connect() : false;
						if (connected) {
							fileReceived = downloadWebFile.getWebFile();
						}
					}
				}
				downloadWebFile.disconnect();
				downloadWebFile.clear();
			} catch (InterruptedException e) {
				content.append("Error : download license => see log error view\n");
				DownloadConstants.log("Error : interruption license download", e);
			} catch (Exception e) {
				content.append("Error : download license => see log error view\n");
				if (e instanceof java.net.MalformedURLException) {
					DownloadConstants.log("Error : malformed url for license", e);
				}
				else if (e instanceof java.io.FileNotFoundException) {
					DownloadConstants.log("Error : file name license is missing.", e);
				}
				else if (e instanceof java.net.UnknownHostException) {
					DownloadConstants.log("Error : unknown server for the web site of license", e);
				}
				else {
					DownloadConstants.log("Error : failure download license", e);
				}
			}
			boolean noLicense = true;
			if (fileReceived) {
				Path licenseDownloaded = new Path(targetTempPathFile.getPath());
				try {
					File licenseFile = new File(licenseDownloaded.toPortableString() + DownloadConstants.slash + targetTempNameFile);
					if (licenseFile != null && licenseFile.exists() && licenseFile.isFile()) {
						FileReader  input = new FileReader(licenseFile);
						BufferedReader buffer = new BufferedReader(input);
						String line;
						while((line = buffer.readLine()) != null) {
							content.append(line);
						}
						buffer.close();
						input.close();
						if (content.length() > 0) noLicense = false;
						licenseFile.delete();
						licenseFile.getParentFile().delete();
					}
				} catch (Exception e) {
					content.append("Error : download license => see log error view\n");
					DownloadConstants.log("Error : failure download license", e);
				}
			}
			if (noLicense && content.length() == 0) content.append("no license\n");

			license = content.toString();
		}
		return license;
	}

	/**
	 * @generated NOT
	 */
	public String getURLLicense() {
		if (!getIsLocalLicense().booleanValue()) {
			String url = getLicenseURL() == null ? null : getLicenseURL().trim().equals("") ? null : getLicenseURL();
			if (url != null) {
				try {
					URL source = new URL(url);
					if (source != null) {
						String webNameFile = null;
						if (source.getFile() != null) {
							Path tempPath = new Path(source.getFile());
							String extension = tempPath.getFileExtension();
							if (extension != null) {
								return source.toExternalForm();
							}
						}
						webNameFile = getLicenseName();
						if (webNameFile != null || !webNameFile.trim().equals("")) {
							Path site = new Path(source.toExternalForm());
							Path file = new Path(webNameFile);
							return site.append(file).toPortableString();
						}
						return source.toExternalForm();
					}
				} catch (Exception e) {
					DownloadConstants.log("Error : failure url license", e);
				}
			}
		}
		return null;
	}
	
	/**
	 * @generated NOT
	 */
	public Boolean getErrorTarget() {
		return errorTarget;
	}
	/**
	 * @generated NOT
	 */
	public void setErrorTarget(boolean newErrorTarget) {
		errorTarget = newErrorTarget;
	} 
	/**
	 * @generated NOT
	 */
	public Boolean getErrorSource() {
		return errorSource;
	}
	/**
	 * @generated NOT
	 */
	public void setErrorSource(boolean newErrorSource) {
		errorSource = newErrorSource;
	} 
	/**
	 * method to know if the jvm version of the required resource is greater than the platform jvm version
	 * @generated NOT
	 */
	public Boolean isJavaVersionGreaterTo(String javaVMVersion) {
		if (javaVMVersion == null) javaVMVersion = CompatibilityChecker.getVersionJavaPlatform();  
		if (javaVMVersion != null && getJavaVMVersion() != null && !getJavaVMVersion().trim().equals("")) {
			try {
				String majorJVM = CompatibilityChecker.getMajorVersionJava(javaVMVersion);
				String mediumJVM = CompatibilityChecker.getMediumVersionJava(javaVMVersion);
				String majorJVMResource = CompatibilityChecker.getMajorVersionJava(getJavaVMVersion());
				String mediumJVMResource = CompatibilityChecker.getMediumVersionJava(getJavaVMVersion());
				return !(
						((new Integer(majorJVM)) >= (new Integer(majorJVMResource)))
						&&
						((new Integer(mediumJVM)) >= (new Integer(mediumJVMResource)))
						);
			} catch (Exception e) {
				DownloadConstants.log("error compareTo current jvm to version of resource " + getNameID() + " with " + getJavaVMVersion(), e);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOwner((Configuration)otherEnd, msgs);
			case modelConfigPackage.RESOURCE__PROFIL:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getProfil()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.RESOURCE__STATE:
				if (state != null)
					msgs = ((InternalEObject)state).eInverseRemove(this, modelConfigPackage.STATE__RESOURCE, State.class, msgs);
				return basicSetState((State)otherEnd, msgs);
			case modelConfigPackage.RESOURCE__REQUIRED:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRequired()).basicAdd(otherEnd, msgs);
			case modelConfigPackage.RESOURCE__REQUIRE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRequire()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean install() {
		return false;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				return basicSetOwner(null, msgs);
			case modelConfigPackage.RESOURCE__PROFIL:
				return ((InternalEList<?>)getProfil()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.RESOURCE__STATE:
				return basicSetState(null, msgs);
			case modelConfigPackage.RESOURCE__REQUIRED:
				return ((InternalEList<?>)getRequired()).basicRemove(otherEnd, msgs);
			case modelConfigPackage.RESOURCE__REQUIRE:
				return ((InternalEList<?>)getRequire()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				return eInternalContainer().eInverseRemove(this, modelConfigPackage.CONFIGURATION__OWNED_RESOURCE, Configuration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				return getOwner();
			case modelConfigPackage.RESOURCE__CATEGORY:
				if (resolve) return getCategory();
				return basicGetCategory();
			case modelConfigPackage.RESOURCE__PROFIL:
				return getProfil();
			case modelConfigPackage.RESOURCE__STATE:
				if (resolve) return getState();
				return basicGetState();
			case modelConfigPackage.RESOURCE__NAME_WEB_FILE:
				return getNameWebFile();
			case modelConfigPackage.RESOURCE__SITE_URL:
				return getSiteURL();
			case modelConfigPackage.RESOURCE__ENTRY:
				return getEntry();
			case modelConfigPackage.RESOURCE__IS_LOCAL_FILE:
				return getIsLocalFile();
			case modelConfigPackage.RESOURCE__TARGET_NAME_FILE:
				return getTargetNameFile();
			case modelConfigPackage.RESOURCE__TARGET_PATH_FILE:
				return getTargetPathFile();
			case modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE:
				return getIsLocalLicense();
			case modelConfigPackage.RESOURCE__LICENSE_NAME:
				return getLicenseName();
			case modelConfigPackage.RESOURCE__LICENSE_URL:
				return getLicenseURL();
			case modelConfigPackage.RESOURCE__LICENSE_PATH_FILE:
				return getLicensePathFile();
			case modelConfigPackage.RESOURCE__GROUP:
				if (resolve) return getGroup();
				return basicGetGroup();
			case modelConfigPackage.RESOURCE__DESCRIPTION:
				return getDescription();
			case modelConfigPackage.RESOURCE__REQUIRED:
				return getRequired();
			case modelConfigPackage.RESOURCE__REQUIRE:
				return getRequire();
			case modelConfigPackage.RESOURCE__IS_VISIBLE:
				return getIsVisible();
			case modelConfigPackage.RESOURCE__INCLUDES_IN:
				return getIncludesIn();
			case modelConfigPackage.RESOURCE__JAVA_VM_VERSION:
				return getJavaVMVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
		@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				setOwner((Configuration)newValue);
				return;
			case modelConfigPackage.RESOURCE__CATEGORY:
				setCategory((Category)newValue);
				return;
			case modelConfigPackage.RESOURCE__PROFIL:
				getProfil().clear();
				getProfil().addAll((Collection<? extends Profil>)newValue);
				return;
			case modelConfigPackage.RESOURCE__STATE:
				setState((State)newValue);
				return;
			case modelConfigPackage.RESOURCE__NAME_WEB_FILE:
				setNameWebFile((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__SITE_URL:
				setSiteURL((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__ENTRY:
				setEntry((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__IS_LOCAL_FILE:
				setIsLocalFile((Boolean)newValue);
				return;
			case modelConfigPackage.RESOURCE__TARGET_NAME_FILE:
				setTargetNameFile((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__TARGET_PATH_FILE:
				setTargetPathFile((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE:
				setIsLocalLicense((Boolean)newValue);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_NAME:
				setLicenseName((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_URL:
				setLicenseURL((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_PATH_FILE:
				setLicensePathFile((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__GROUP:
				setGroup((Group)newValue);
				return;
			case modelConfigPackage.RESOURCE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case modelConfigPackage.RESOURCE__REQUIRED:
				getRequired().clear();
				getRequired().addAll((Collection<? extends Resource>)newValue);
				return;
			case modelConfigPackage.RESOURCE__REQUIRE:
				getRequire().clear();
				getRequire().addAll((Collection<? extends Resource>)newValue);
				return;
			case modelConfigPackage.RESOURCE__IS_VISIBLE:
				setIsVisible((Boolean)newValue);
				return;
			case modelConfigPackage.RESOURCE__INCLUDES_IN:
				setIncludesIn((EnumKindOfInclusion)newValue);
				return;
			case modelConfigPackage.RESOURCE__JAVA_VM_VERSION:
				setJavaVMVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				setOwner((Configuration)null);
				return;
			case modelConfigPackage.RESOURCE__CATEGORY:
				setCategory((Category)null);
				return;
			case modelConfigPackage.RESOURCE__PROFIL:
				getProfil().clear();
				return;
			case modelConfigPackage.RESOURCE__STATE:
				setState((State)null);
				return;
			case modelConfigPackage.RESOURCE__NAME_WEB_FILE:
				setNameWebFile(NAME_WEB_FILE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__SITE_URL:
				setSiteURL(SITE_URL_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__ENTRY:
				setEntry(ENTRY_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__IS_LOCAL_FILE:
				setIsLocalFile(IS_LOCAL_FILE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__TARGET_NAME_FILE:
				setTargetNameFile(TARGET_NAME_FILE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__TARGET_PATH_FILE:
				setTargetPathFile(TARGET_PATH_FILE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE:
				setIsLocalLicense(IS_LOCAL_LICENSE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_NAME:
				setLicenseName(LICENSE_NAME_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_URL:
				setLicenseURL(LICENSE_URL_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__LICENSE_PATH_FILE:
				setLicensePathFile(LICENSE_PATH_FILE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__GROUP:
				setGroup((Group)null);
				return;
			case modelConfigPackage.RESOURCE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__REQUIRED:
				getRequired().clear();
				return;
			case modelConfigPackage.RESOURCE__REQUIRE:
				getRequire().clear();
				return;
			case modelConfigPackage.RESOURCE__IS_VISIBLE:
				setIsVisible(IS_VISIBLE_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__INCLUDES_IN:
				setIncludesIn(INCLUDES_IN_EDEFAULT);
				return;
			case modelConfigPackage.RESOURCE__JAVA_VM_VERSION:
				setJavaVMVersion(JAVA_VM_VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case modelConfigPackage.RESOURCE__OWNER:
				return getOwner() != null;
			case modelConfigPackage.RESOURCE__CATEGORY:
				return category != null;
			case modelConfigPackage.RESOURCE__PROFIL:
				return profil != null && !profil.isEmpty();
			case modelConfigPackage.RESOURCE__STATE:
				return state != null;
			case modelConfigPackage.RESOURCE__NAME_WEB_FILE:
				return NAME_WEB_FILE_EDEFAULT == null ? nameWebFile != null : !NAME_WEB_FILE_EDEFAULT.equals(nameWebFile);
			case modelConfigPackage.RESOURCE__SITE_URL:
				return SITE_URL_EDEFAULT == null ? siteURL != null : !SITE_URL_EDEFAULT.equals(siteURL);
			case modelConfigPackage.RESOURCE__ENTRY:
				return ENTRY_EDEFAULT == null ? entry != null : !ENTRY_EDEFAULT.equals(entry);
			case modelConfigPackage.RESOURCE__IS_LOCAL_FILE:
				return IS_LOCAL_FILE_EDEFAULT == null ? isLocalFile != null : !IS_LOCAL_FILE_EDEFAULT.equals(isLocalFile);
			case modelConfigPackage.RESOURCE__TARGET_NAME_FILE:
				return TARGET_NAME_FILE_EDEFAULT == null ? targetNameFile != null : !TARGET_NAME_FILE_EDEFAULT.equals(targetNameFile);
			case modelConfigPackage.RESOURCE__TARGET_PATH_FILE:
				return TARGET_PATH_FILE_EDEFAULT == null ? targetPathFile != null : !TARGET_PATH_FILE_EDEFAULT.equals(targetPathFile);
			case modelConfigPackage.RESOURCE__IS_LOCAL_LICENSE:
				return IS_LOCAL_LICENSE_EDEFAULT == null ? isLocalLicense != null : !IS_LOCAL_LICENSE_EDEFAULT.equals(isLocalLicense);
			case modelConfigPackage.RESOURCE__LICENSE_NAME:
				return LICENSE_NAME_EDEFAULT == null ? licenseName != null : !LICENSE_NAME_EDEFAULT.equals(licenseName);
			case modelConfigPackage.RESOURCE__LICENSE_URL:
				return LICENSE_URL_EDEFAULT == null ? licenseURL != null : !LICENSE_URL_EDEFAULT.equals(licenseURL);
			case modelConfigPackage.RESOURCE__LICENSE_PATH_FILE:
				return LICENSE_PATH_FILE_EDEFAULT == null ? licensePathFile != null : !LICENSE_PATH_FILE_EDEFAULT.equals(licensePathFile);
			case modelConfigPackage.RESOURCE__GROUP:
				return group != null;
			case modelConfigPackage.RESOURCE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case modelConfigPackage.RESOURCE__REQUIRED:
				return required != null && !required.isEmpty();
			case modelConfigPackage.RESOURCE__REQUIRE:
				return require != null && !require.isEmpty();
			case modelConfigPackage.RESOURCE__IS_VISIBLE:
				return IS_VISIBLE_EDEFAULT == null ? isVisible != null : !IS_VISIBLE_EDEFAULT.equals(isVisible);
			case modelConfigPackage.RESOURCE__INCLUDES_IN:
				return includesIn != INCLUDES_IN_EDEFAULT;
			case modelConfigPackage.RESOURCE__JAVA_VM_VERSION:
				return JAVA_VM_VERSION_EDEFAULT == null ? javaVMVersion != null : !JAVA_VM_VERSION_EDEFAULT.equals(javaVMVersion);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nameWebFile: ");
		result.append(nameWebFile);
		result.append(", siteURL: ");
		result.append(siteURL);
		result.append(", entry: ");
		result.append(entry);
		result.append(", isLocalFile: ");
		result.append(isLocalFile);
		result.append(", targetNameFile: ");
		result.append(targetNameFile);
		result.append(", targetPathFile: ");
		result.append(targetPathFile);
		result.append(", isLocalLicense: ");
		result.append(isLocalLicense);
		result.append(", licenseName: ");
		result.append(licenseName);
		result.append(", licenseURL: ");
		result.append(licenseURL);
		result.append(", licensePathFile: ");
		result.append(licensePathFile);
		result.append(", description: ");
		result.append(description);
		result.append(", isVisible: ");
		result.append(isVisible);
		result.append(", includesIn: ");
		result.append(includesIn);
		result.append(", javaVMVersion: ");
		result.append(javaVMVersion);
		result.append(')');
		return result.toString();
	}

} //ResourceImpl