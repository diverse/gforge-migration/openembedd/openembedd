/**
 * <copyright>
 * </copyright>
 *
 * $Id: Action.java,v 1.2 2007-07-10 15:42:34 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Action#getOwner <em>Owner</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Action#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Action#getJavaClassName <em>Java Class Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Configuration#getOwnedAction <em>Owned Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Configuration)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getAction_Owner()
	 * @see org.openembedd.checker.modelConfig.Configuration#getOwnedAction
	 * @model opposite="ownedAction" required="true" transient="false"
	 * @generated
	 */
	Configuration getOwner();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Action#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Configuration value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getAction_Description()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Action#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Java Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java Class Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java Class Name</em>' attribute.
	 * @see #setJavaClassName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getAction_JavaClassName()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getJavaClassName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Action#getJavaClassName <em>Java Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Java Class Name</em>' attribute.
	 * @see #getJavaClassName()
	 * @generated
	 */
	void setJavaClassName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean enabled();

} // Action