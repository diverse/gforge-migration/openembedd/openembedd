/**
 * <copyright>
 * </copyright>
 *
 * $Id: Configuration.java,v 1.6 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedState <em>Owned State</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedAction <em>Owned Action</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedProfil <em>Owned Profil</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedResource <em>Owned Resource</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedCategory <em>Owned Category</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedPackage <em>Owned Package</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getOwnedGroup <em>Owned Group</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Configuration#getConfigVersion <em>Config Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends ModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Owned State</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.State}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.State#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned State</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned State</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedState()
	 * @see org.openembedd.checker.modelConfig.State#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<State> getOwnedState();

	/**
	 * Returns the value of the '<em><b>Owned Action</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Action}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Action#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Action</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedAction()
	 * @see org.openembedd.checker.modelConfig.Action#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Action> getOwnedAction();

	/**
	 * Returns the value of the '<em><b>Owned Profil</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Profil}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Profil#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Profil</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Profil</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedProfil()
	 * @see org.openembedd.checker.modelConfig.Profil#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Profil> getOwnedProfil();

	/**
	 * Returns the value of the '<em><b>Owned Resource</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Resource}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Resource#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Resource</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedResource()
	 * @see org.openembedd.checker.modelConfig.Resource#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Resource> getOwnedResource();

	/**
	 * Returns the value of the '<em><b>Owned Category</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Category}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Category#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Category</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Category</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedCategory()
	 * @see org.openembedd.checker.modelConfig.Category#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Category> getOwnedCategory();

	/**
	 * Returns the value of the '<em><b>Owned Package</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Package}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Package#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Package</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Package</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedPackage()
	 * @see org.openembedd.checker.modelConfig.Package#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<org.openembedd.checker.modelConfig.Package> getOwnedPackage();

	/**
	 * Returns the value of the '<em><b>Owned Group</b></em>' containment reference list.
	 * The list contents are of type {@link org.openembedd.checker.modelConfig.Group}.
	 * It is bidirectional and its opposite is '{@link org.openembedd.checker.modelConfig.Group#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Group</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Group</em>' containment reference list.
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_OwnedGroup()
	 * @see org.openembedd.checker.modelConfig.Group#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Group> getOwnedGroup();

	/**
	 * Returns the value of the '<em><b>Config Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Config Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Config Version</em>' attribute.
	 * @see #setConfigVersion(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getConfiguration_ConfigVersion()
	 * @model dataType="org.openembedd.checking.modelConfig.String" required="true"
	 * @generated
	 */
	String getConfigVersion();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Configuration#getConfigVersion <em>Config Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Config Version</em>' attribute.
	 * @see #getConfigVersion()
	 * @generated
	 */
	void setConfigVersion(String value);

	/**
	 * return the name ID of the plugin contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	String getPluginContributorNameID();
	
	/**
	 * return the path of the license contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	String getPathLicenseContributor();
	
	/**
	 * set the name ID of the plugin contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	void setPluginContributorNameID(String plugin);
	
	/**
	 * set the path of the license contributor
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	void setPathLicenseContributor(String path);
	
} // Configuration