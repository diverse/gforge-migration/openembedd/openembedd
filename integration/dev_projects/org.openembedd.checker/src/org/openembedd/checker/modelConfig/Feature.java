/**
 * <copyright>
 * </copyright>
 *
 * $Id: Feature.java,v 1.4 2007-07-10 15:42:34 ffillion Exp $
 */
package org.openembedd.checker.modelConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openembedd.checker.modelConfig.Feature#getLongName <em>Long Name</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Feature#getVersion <em>Version</em>}</li>
 *   <li>{@link org.openembedd.checker.modelConfig.Feature#getToDecompress <em>To Decompress</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFeature()
 * @model
 * @generated
 */
public interface Feature extends Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "OpenEmbeDD";

	/**
	 * Returns the value of the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Name</em>' attribute.
	 * @see #setLongName(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFeature_LongName()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getLongName();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Feature#getLongName <em>Long Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Name</em>' attribute.
	 * @see #getLongName()
	 * @generated
	 */
	void setLongName(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFeature_Version()
	 * @model dataType="org.openembedd.checking.modelConfig.String"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Feature#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>To Decompress</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Decompress</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Decompress</em>' attribute.
	 * @see #setToDecompress(Boolean)
	 * @see org.openembedd.checker.modelConfig.modelConfigPackage#getFeature_ToDecompress()
	 * @model default="false" dataType="org.openembedd.checking.modelConfig.Boolean" required="true"
	 * @generated
	 */
	Boolean getToDecompress();

	/**
	 * Sets the value of the '{@link org.openembedd.checker.modelConfig.Feature#getToDecompress <em>To Decompress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Decompress</em>' attribute.
	 * @see #getToDecompress()
	 * @generated
	 */
	void setToDecompress(Boolean value);

	/**
	 * method to install the feature
	 * @generated NOT
	 */
	Boolean install();
	
} // Feature