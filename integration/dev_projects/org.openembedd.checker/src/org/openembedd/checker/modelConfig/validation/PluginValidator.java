/**
 * <copyright>
 * </copyright>
 *
 * $Id: PluginValidator.java,v 1.2 2007-08-04 15:27:05 ffillion Exp $
 */
package org.openembedd.checker.modelConfig.validation;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link org.openembedd.checker.modelConfig.Plugin}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface PluginValidator {
	boolean validate();

	boolean validateLongName(String value);
	boolean validateVersion(String value);
	boolean validateApplications(EList<?> value);
	boolean validatePackage(org.openembedd.checker.modelConfig.Package value);
	boolean validateToDecompress(Boolean value);
}
