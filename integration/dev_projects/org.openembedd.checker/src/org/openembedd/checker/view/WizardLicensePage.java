package org.openembedd.checker.view;
/**
 * Copyright � 2007 The Eclipse Foundation. All Rights Reserved
 * Eclipse Public License - v 1.0
 * Internal API
 * ----------------------------------------------------------------------------
 * Modified date : september 2007
 * Authors       : openembedd
 * what          : adapt to checker
 * ----------------------------------------------------------------------------   
 */
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.openembedd.checker.view.ButtonCellEditor.DownloadAction;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;

public class WizardLicensePage extends WizardPage {
	private Text text;
	private Browser browser;
	private Table table;
	private Button acceptButton;
	private Button declineButton;
	private DownloadAction[] downloadAction;
	private DownloadAction[] oldDownloadAction;
	private boolean multiLicenseMode = false;
	private StackLayout stacklayout;
	private Composite contentLicense, pageText, pageBrowser;

	public WizardLicensePage(boolean multiLicenseMode, DownloadAction[] downloadAction) {
		super("License");
		this.multiLicenseMode = multiLicenseMode;
		setTitle("Resource License");
		setPageComplete(false);
		setDescription("Resources have license agreements that you need to accept before proceeding the installation.");
		this.downloadAction = downloadAction;
	}

	public void createControl(Composite parent) {
		Composite client = new Composite(parent, SWT.NULL);
		client.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout();
		client.setLayout(layout);
		if (multiLicenseMode) {
			layout.numColumns = 3;
			layout.makeColumnsEqualWidth = true;

			table = new Table(client, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

			table.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					if (e.item != null) {
						Object data = e.item.getData();
						ConfigTableElement element = (data == null) ? null :(ConfigTableElement)data;
						text.setText((element == null | !element.getResourceIsLocalLicense()) ? "no license" : element.getResourceTextLicense()); //$NON-NLS-1$
						browser.setText((element == null  | element.getResourceIsLocalLicense()) ? "no license" : element.getResourceURLLicense() == null ? "Error : failure url license" : element.getResourceURLLicense());
						if (element != null && !element.getResourceIsLocalLicense() && element.getResourceURLLicense() != null) {
							browser.setUrl(element.getResourceURLLicense());	
						}
						//switch
						stacklayout.topControl = (element == null) ? pageText : element.getResourceIsLocalLicense() ? pageText : pageBrowser;
						contentLicense.layout();
					}
				}
			});
			GridData gd = new GridData(GridData.FILL_BOTH);
			table.setLayoutData(gd);
		}

		if (contentLicense == null)
			contentLicense = new Composite(client, SWT.WRAP | SWT.BORDER);
		GridData gd = new GridData(GridData.FILL_BOTH);
		if (multiLicenseMode)
			gd.horizontalSpan = 2;
		contentLicense.setLayoutData(gd);		

		//composite of shared pages
		if (stacklayout == null ) 
			stacklayout = new StackLayout();
		contentLicense.setLayout(stacklayout);

		//page text
		if (pageText == null ) 
			pageText = new Composite(contentLicense, SWT.WRAP);
		pageText.setLayout(new FillLayout());

		text =
			new Text(
					pageText,
					SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.WRAP | SWT.READ_ONLY);
		text.setBackground(text.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));

		//page browser
		if (pageBrowser == null ) 
			pageBrowser = new Composite(contentLicense, SWT.WRAP);
		pageBrowser.setLayout(new FillLayout());

		browser =
			new Browser(
					pageBrowser,
					SWT.WRAP | SWT.READ_ONLY);
		browser.setBackground(browser.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));

		Composite buttonContainer = new Composite(client, SWT.NULL);
		gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		if (multiLicenseMode)
			gd.horizontalSpan = 3;
		buttonContainer.setLayout(new GridLayout());
		buttonContainer.setLayoutData(gd);

		acceptButton = new Button(buttonContainer, SWT.RADIO);
		acceptButton.setText(multiLicenseMode ? "I accept the terms in the license agreements" : "I accept the terms in the license agreement");
		acceptButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setPageComplete(acceptButton.getSelection());
			}
		});
		declineButton = new Button(buttonContainer, SWT.RADIO);
		declineButton.setText(multiLicenseMode ? "I do not accept the terms in the license agreements" : "I do not accept the terms in the license agreement");
		declineButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setPageComplete(acceptButton.getSelection());
			}
		});
		setControl(client);

		Dialog.applyDialogFont(parent);
	}

	public void setVisible(boolean visible) { // TO DO: Optimize out the case where a feature does not have a license?

		boolean jobsChanged = didJobsChange(downloadAction);
		declineButton.setSelection(!jobsChanged && declineButton.getSelection());
		acceptButton.setSelection(!jobsChanged && acceptButton.getSelection());

		if (downloadAction.length == 1) {
			acceptButton.setText("I accept the terms in the license agreement");
			declineButton.setText("I do not accept the terms in the license agreement");
		} else if (downloadAction.length > 1) {
			acceptButton.setText("I accept the terms in the license agreements");
			declineButton.setText("I do not accept the terms in the license agreements");
		}

		if (visible) {
			if (multiLicenseMode) {
				TableItem item;
				for (int i = 0; i < downloadAction.length; i++) {
					ConfigTableElement element = downloadAction[i].getConfigTableElement();
					item = new TableItem(table, SWT.NONE);
					String label =  element.getTextTypeResource() + "  " + element.getResourceTargetNameFile() + "  " ;
					item.setText(label);
					item.setImage(element.getImageTypeResource());
					// Question: Can this ever be null? What is the runtime cost?
					item.setData(element);
				}

				table.setSelection(0);
			}
			showLicenseText();
		} else {
			if (multiLicenseMode) {
				TableItem items[] = table.getItems();
				for (int i = items.length - 1; i >= 0; i--) {
					table.getItem(i).dispose();
				}
			}
		}
		super.setVisible(visible);
		oldDownloadAction = downloadAction;
	}

	private void showLicenseText() {
		if (!multiLicenseMode) {
			ConfigTableElement element = downloadAction[0].getConfigTableElement();
			text.setText((element == null | !element.getResourceIsLocalLicense()) ? "no license" : element.getResourceTextLicense()); //$NON-NLS-1$
			browser.setText((element == null  | element.getResourceIsLocalLicense()) ? "no license" : element.getResourceURLLicense() == null ? "Error : failure url license" : element.getResourceURLLicense());
			if (element != null && !element.getResourceIsLocalLicense() && element.getResourceURLLicense() != null) {
				browser.setUrl(element.getResourceURLLicense());	
			}
			//switch
			stacklayout.topControl = (element == null) ? pageText : element.getResourceIsLocalLicense() ? pageText : pageBrowser;
			contentLicense.layout();
			return;
		}
		TableItem[] selectedItems = table.getSelection();
		if (selectedItems.length == 0) {
			text.setText(""); //$NON-NLS-1$
			browser.setText(""); //$NON-NLS-1$
		} else {
			Object data = selectedItems[0].getData();
			ConfigTableElement element = (data == null) ? null :(ConfigTableElement)data;
			text.setText((element == null || !element.getResourceIsLocalLicense()) ? "no license" : element.getResourceTextLicense()); //$NON-NLS-1$
			browser.setText((element == null || element.getResourceIsLocalLicense()) ? "no license" : element.getResourceURLLicense() == null ? "Error : failure url license" : element.getResourceURLLicense());
			if (element != null && !element.getResourceIsLocalLicense() && element.getResourceURLLicense() != null) {
				browser.setUrl(element.getResourceURLLicense());	
			}
			//switch
			stacklayout.topControl = (element == null) ? pageText : element.getResourceIsLocalLicense() ? pageText : pageBrowser;
			contentLicense.layout();
		}
	}

	private boolean didJobsChange(DownloadAction[] downloadAction){

		if ( (downloadAction == null) || (oldDownloadAction == null) || (downloadAction.length == 0) || (oldDownloadAction.length == 0) )
			return true;

		boolean foundIt = false;

		for ( int i = 0; i < downloadAction.length; i++) {
			foundIt = false;
			for ( int j = 0; j < oldDownloadAction.length; j++) {
				if (downloadAction[i].getConfigTableElement().getResourceNameID().equals(oldDownloadAction[j].getConfigTableElement().getResourceNameID()) ) {
					foundIt = true;
					break;
				}
			}
			if (!foundIt) {
				return true;
			}
		}
		return false;
	}

}
