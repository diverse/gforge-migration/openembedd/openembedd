package org.openembedd.checker.view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.openembedd.checker.action.CheckingIcons;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.modelConfig.Action;
import org.openembedd.checker.modelConfig.Category;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.EnumKindOfInclusion;
import org.openembedd.checker.modelConfig.Exe;
import org.openembedd.checker.modelConfig.Feature;
import org.openembedd.checker.modelConfig.File;
import org.openembedd.checker.modelConfig.Library;
import org.openembedd.checker.modelConfig.Plugin;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.modelConfig.Resource;
import org.openembedd.checker.modelConfig.State;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class ResourceContentProvider implements IStructuredContentProvider {

	/** ConfigTableElement is the content element for tableviewer of configuration **/
	/**
	 * 
	 * Note : use of openembedd configuration model (mapping)
	 *
	 */
	public class ConfigTableElement {
		//model element
		private State state = null;
		private Resource resource = null;
		private Action action = null;
		//behavior
		private boolean cellCanModify;

		//methods
		public ConfigTableElement(Resource resource) {
			if (resource != null)  {
				this.resource = resource;
				this.state = resource.getState();
				this.action = this.state == null ? null : resource.getState().getDefaultAction();
				this.cellCanModify = this.action == null ? false : this.action.enabled().booleanValue();
			}
		}

		//label information
		public Image getImageTypeResource() {
			if (resource != null) {
				if (resource instanceof Library) {
					return CheckingIcons.get(DownloadConstants.libraryType);
				}
				else if (resource instanceof Plugin) {
					return CheckingIcons.get(DownloadConstants.pluginType);
				}
				else if (resource instanceof Exe) {
					return CheckingIcons.get(DownloadConstants.fileType);	
				}
				else if (resource instanceof File) {
					return CheckingIcons.get(DownloadConstants.fileType);
				}
				else if (resource instanceof Feature) {
					return CheckingIcons.get(DownloadConstants.featureType);
				}
				else {
					return CheckingIcons.get(DownloadConstants.fileType);
				}
			}
			return null;
		}

		public Image getImageState() {
			boolean exist = getResourceExist();
			boolean existIgnoreVersion =  getResourceExistIgnoreVersion();
			boolean isRequired = getResourceIsRequired();
			if (getResourceIsJavaVersionGreaterToPlatform().booleanValue()) {
				//jvm version of platform < jvm version resource 
				return CheckingIcons.get(DownloadConstants.errorTsk);
			}
			else if (isRequired && !existIgnoreVersion) {
				//no installed version resource
				return CheckingIcons.get(DownloadConstants.errorTsk);
			}
			else if (isRequired && existIgnoreVersion) {
				//one version is installed
				Version versionInstalled = getResourceVersionInstalled();
				if (versionInstalled != null) {
					Version versionRequired = getResourceVersion();
					if (versionInstalled.compareTo(versionRequired) > 0) {
						//more recent version installed
						return CheckingIcons.get(DownloadConstants.warningTsk);
					}
					else if (versionInstalled.compareTo(versionRequired) < 0) {
						//less recent version installed
						return CheckingIcons.get(DownloadConstants.errorTsk);
					}
					else {
						//perfect matching : the resource is correctly installed
						return CheckingIcons.get(DownloadConstants.completeStatus);
					}
				}
				else {
					return CheckingIcons.get(DownloadConstants.warningTsk);
				}
			}
			else if (!exist) {
				//resource not installed but no required
				return CheckingIcons.get(DownloadConstants.infoTsk);
			}
			else {
				//resource installed
				return CheckingIcons.get(DownloadConstants.completeStatus);
			}
		}

		public String getTextState() {
			String text = "";
			if (state != null) {
				text = state.getName();
			}
			return text;
		}

		public String getTextAction() {
			String text = "";
			if (action != null) {
				text = action.getName();
			}
			return text;
		}

		public String getTextResource() {
			String text = "";
			if (resource != null) {
				text = resource.getName();
			}
			return text;
		}

		public String getTextDescriptionResource() {
			String desc = "";
			if (resource != null) {
				desc = resource.getDescription();
				if (desc != null)
					if (desc.trim().equals(""))
						desc = null;
			}
			return desc == null ? "no description" : desc;
		}

		public String getTextDescriptionAction() {
			String desc = "";
			if (action != null) {
				desc = action.getDescription();
			}
			return desc == null ? "" : desc;
		}

		public String getTextTypeResource() {
			String text = "";
			if (resource != null) {
				if (resource instanceof Library)
					text = DownloadConstants.type_LIBRARY;
				else if (resource instanceof Plugin)
					text = DownloadConstants.type_PLUGIN;
				else if (resource instanceof Exe)
					text = DownloadConstants.type_EXE;
				else if (resource instanceof Feature)
					text = DownloadConstants.type_FEATURE;
				else
					text = DownloadConstants.type_NONE;
			}
			return text;
		}

		//resource information
		public String getResourceNameID() {
			return resource == null ? null : resource.getNameID();
		}

		public String getResourceNameWebFile() {
			if (resource!= null && resource instanceof Plugin) {
				Plugin plugin = (Plugin)resource;
				String nameweb = plugin.getNameWebFile();
				if (nameweb == null || nameweb.trim().equals("")) {
					try {
						nameweb = plugin.getNameID() + DownloadConstants.versionSep_ + plugin.getVersion() + DownloadConstants.jarFileSuffix;
						return nameweb;
					} catch (Exception e) {
						DownloadConstants.log("Error getting name web plugin.", e);
						return null;
					}
				}
			}
			return resource == null ? null : resource.getNameWebFile();
		}

		public boolean getResourceIsLocalFile() {
			return resource == null ? false : resource.getIsLocalFile().booleanValue();
		}

		public String getResourceSiteURL() {
			return resource == null ? null : resource.getSiteURL();
		}

		public String getResourceEntry() {
			return resource == null ? null : resource.getEntry();
		}

		public String getResourceTargetNameFile() {
			if (resource!= null && resource instanceof Plugin) {
				Plugin plugin = (Plugin)resource;
				String target = plugin.getTargetNameFile();
				if (target == null || target.trim().equals("")) {
					return getResourceNameWebFile();
				}
			}
			return resource == null ? null : resource.getTargetNameFile();
		}

		/**
		 * if Library : return plugin name ID if target Library is a plugin and null false
		 * if not Library : return null
		 */
		public String getNamePluginTargetForResourceLibrary() {
			if (resource != null && resource instanceof Library) {
				Library library = (Library)resource;
				return (library.getPlugin() == null) ? null : library.getPlugin().getNameID();
			}
			return null;
		}

		//TODO : integrate this method into elments class (Resource, library, etc...)
		public String getResourceTargetPathFile() {
			String target = null;
			String sTargetPathFile;
			Path p_targetPathFile; 

			if (resource != null) {
				sTargetPathFile = resource.getTargetPathFile();
				if (sTargetPathFile == null && !(resource instanceof Plugin || resource instanceof Feature)) {
					setResourceErrorTarget(true);
					return null;				
				}
				p_targetPathFile = new Path(sTargetPathFile);
				//library
				if (resource instanceof Library) {
					Plugin pluginTarget = ((Library)resource).getPlugin();
					URL urlTarget = null;
					if (pluginTarget != null) {
						Bundle bundle = null;
						try {
							bundle = Platform.getBundle(pluginTarget.getNameID());
							if (bundle != null) {
								URL bundleURL = bundle.getEntry("/") == null ? null : FileLocator.resolve(bundle.getEntry("/").toURI().toURL());
								java.io.File file = bundleURL == null ? null : new java.io.File(bundleURL.getFile());
								if (file != null && !file.isDirectory()) {
									setResourceErrorTarget(true);
									return null;
								}
								urlTarget = FileLocator.find(bundle, p_targetPathFile, null);
							}
						} catch (Exception e) {
							setResourceErrorTarget(true);
						}
						if (urlTarget != null) {
							try {
								Path ptarget = new Path(FileLocator.resolve(urlTarget).getPath());
								target = ptarget.makeAbsolute().toPortableString();
							} catch (IOException e) {
								setResourceErrorTarget(true);
							}
						}
						else if (bundle != null) {
							//we can create the path if the plugin exists
							urlTarget = FileLocator.find(bundle, new Path(""), null);
							try {
								Path ptarget = new Path(FileLocator.resolve(urlTarget).getPath());
								ptarget = (Path) ptarget.append(p_targetPathFile);
								target = ptarget.makeAbsolute().toPortableString();
							} catch (IOException e) {
								setResourceErrorTarget(true);
							}
						}
					}
					//check the target if there isn't any link
					else {
						try {
							if (p_targetPathFile.getDevice() == null) {
								Path device = new Path(DownloadConstants.defaultDevicePath);
								p_targetPathFile = (Path)device.append((IPath)p_targetPathFile);
							}
							target = p_targetPathFile.makeAbsolute().toPortableString();
						} catch (Exception e) {
							setResourceErrorTarget(true);
						}
					}
				}
				//plugin
				if (resource instanceof Plugin) {
					@SuppressWarnings("unused")
					Plugin plugin = (Plugin)resource;
					if (p_targetPathFile == null || p_targetPathFile.isEmpty()) {
						p_targetPathFile = new Path("plugins");
					}
					if (p_targetPathFile.getDevice() == null) {
						Path device = new Path(DownloadConstants.defaultDevicePath);
						p_targetPathFile = (Path)device.append((IPath)p_targetPathFile);
					}
					target = p_targetPathFile == null ? null : p_targetPathFile.makeAbsolute().toPortableString(); 
				}
				//feature - Note : features are installed by another way
				if (resource instanceof Feature) {
					@SuppressWarnings("unused")
					Feature feature = (Feature)resource;
				}
				//exe (natif or portable)
				if (resource instanceof Exe) {
					@SuppressWarnings("unused")
					Exe feature = (Exe)resource;
					//TODO
				}
				//file
				if (resource instanceof File) {
					@SuppressWarnings("unused")
					File feature = (File)resource;
					//TODO
				}
			}
			return target;
		}

		public boolean getResourceIsLocalLicense() {
			return resource == null ? false : resource.getIsLocalLicense().booleanValue();
		}

		public String getResourceLicenseURL() {
			return resource == null ? "" : resource.getLicenseURL();
		}

		public String getResourceLicensePathFile() {
			return resource == null ? "" : resource.getLicensePathFile();
		}

		public String getResourceTextLicense() {
			return resource == null ? "" : resource.getLicense();
		}

		public String getResourceURLLicense() {
			return resource == null ? null : resource.getURLLicense();
		}

		public EnumKindOfInclusion getResourceIncludesIn() {
			return resource == null ? EnumKindOfInclusion.NONE_LITERAL : resource.getIncludesIn();
		}

		public boolean getResourceExist() {
			return resource == null ? false : resource.exist().booleanValue();
		}

		public boolean getResourceExistIgnoreVersion() {
			return resource == null ? false : resource.existIgnoreVersion().booleanValue();
		}

		public Version getResourceVersionInstalled() {
			return resource == null ? null : resource.versionInstalled() == null ? null : resource.versionInstalled(); 
		}

		public Version getResourceVersion() {
			if (resource != null) {
				if (resource instanceof Plugin) {
					Plugin plugin = (Plugin)resource;
					return plugin.getVersion() == null ? null : new Version(plugin.getVersion()); 
				}
				if (resource instanceof Feature) {
					Feature feature = (Feature)resource;
					return feature.getVersion() == null ? null : new Version(feature.getVersion());
				}
			}
			return null;
		}

		public String getResourceTextVersion() {
			if (resource != null) {
				if (resource instanceof Plugin) {
					Plugin plugin = (Plugin)resource;
					return plugin.getVersion() == null ? "none" : plugin.getVersion(); 
				}
				if (resource instanceof Feature) {
					Feature feature = (Feature)resource;
					return feature.getVersion() == null ? "none" : feature.getVersion();
				}
			}
			return "none";
		}

		public String getResourceJavaJVMVersion() {
			return resource == null ? null : resource.getJavaVMVersion() == null ? null : resource.getJavaVMVersion(); 
		}

		public Boolean getResourceIsJavaVersionGreaterToPlatform() {
			if (resource != null && resource.getJavaVMVersion() != null) {
				return resource.isJavaVersionGreaterTo(DownloadConstants.jvm);
			}
			return Boolean.FALSE;
		}

		public void setResourceErrorTarget(boolean error) {
			if (resource != null) resource.setErrorTarget(error);
		}

		public boolean installFeature() {
			if (resource instanceof Feature) {
				Feature feature = (Feature) resource;
				return (feature == null) ? false : feature.install().booleanValue();
			}
			return false;
		}

		public void update() {
			//update the state's resource
			if (resource != null) {
				//resource doesn't exist
				if (!getResourceExist()) {
					if (!state.getNameID().equals(DownloadConstants.state_NOT_INSTALLED)) {
						//update state
						for (Iterator<?> iter = resource.getOwner().getOwnedState().iterator(); iter.hasNext();) {
							State eState = (State) iter.next();
							if (eState.getNameID().equals(DownloadConstants.state_NOT_INSTALLED)) {
								state = eState;
								//update model
								resource.setState(state);
								action = state.getDefaultAction();
								cellCanModify = action == null ? false : action.enabled().booleanValue();
								break;
							}
						} 
					}
				} 
				//resource exists
				else {
					if (!state.getNameID().equals(DownloadConstants.state_INSTALLED)) {
						//update state
						for (Iterator<?> iter = resource.getOwner().getOwnedState().iterator(); iter.hasNext();) {
							State eState = (State) iter.next();
							if (eState.getNameID().equals(DownloadConstants.state_INSTALLED)) {
								state = eState;
								//update model
								resource.setState(state);
								action = state.getDefaultAction();
								cellCanModify = action == null ? false : action.enabled().booleanValue();
								break;
							}
						}
					}
				}
			}
		}

		public boolean getResourceIsRequired() {
			boolean resourceRequired = false;
			if (resource != null) {
				if (!getResourceExist()) {
					//TODO : iterate on the require resource, if one exist then the resource is required
					//...re-examine
					//...
					//library (ex : )
					//case : one library for a plugin (ex : antlr for org.atl.eclipse.engin)
//					if (resource instanceof Library) {
//					Library library = (Library)resource;
//					Plugin plugin = library.getPlugin();
//					if (plugin != null) {
//					//if exists plugin need library
//					//!!!Notice: here we ignore the version number of the host plugin
//					if (plugin.existIgnoreVersion().booleanValue())
//					resourceRequired = true;
//					}
					//temporary rule: a library is always required even if the plugin target doesn't exist
//					resourceRequired = true;
//					}
					//plugin (ex : )
//					if (resource instanceof Plugin) {
//					Plugin plugin = (Plugin)resource;
//					resourceRequired = true;
//					}
					//feature (ex : )
//					if (resource instanceof Feature) {
//					Feature feature = (Feature)resource;
//					resourceRequired = true;
//					}
					
					//Note : currently a resource missing is seen as required !! 
					resourceRequired = true;
				}
			}
			return resourceRequired;
		}

		public List<String> getResourceProfilNameIDList() {
			if (resource == null)
				return null;
			if (resource.getProfil() == null)
				return null;

			List<String> profilList = new ArrayList<String>();
			for (Iterator<?> iter = resource.getProfil().iterator(); iter.hasNext();) {
				Profil profil = (Profil) iter.next();
				profilList.add(profil.getNameID());
			}
			return profilList.size() == 0 ? null : profilList;
		}

		public List<Profil> getResourceProfilList() {
			if (resource == null)
				return null;
			if (resource.getProfil() == null)
				return null;

			List<Profil> profilList = new ArrayList<Profil>();
			for (Iterator<?> iter = resource.getProfil().iterator(); iter.hasNext();) {
				Profil profil = (Profil) iter.next();
				profilList.add(profil);
			}
			return profilList.size() == 0 ? null : profilList;
		}

		public List<String> getResourceKeyProfilIDList() {
			if (resource == null)
				return null;
			if (resource.getProfil() == null)
				return null;

			List<String> profilList = new ArrayList<String>();
			for (Iterator<?> iter = resource.getProfil().iterator(); iter.hasNext();) {
				Profil profil = (Profil) iter.next();
				profilList.add(profil.getOwner().getNameID() + profil.getNameID());
			}
			return profilList.size() == 0 ? null : profilList;
		}

		public String getResourceOwnerID() {
			return resource == null ? null : resource.getOwner().getNameID();
		}

		public Configuration getResourceOwner() {
			return resource == null ? null : resource.getOwner();
		}

		public boolean getResourceIsVisible() {
			return resource == null ? false : resource.getIsVisible().booleanValue();
		}

		public Category getResourceCategory() {
			if (resource != null) {
				return resource.getCategory();
			}
			return null;
		}

		public boolean getResourceErrorTarget() {
			return resource == null ? false : resource.getErrorTarget().booleanValue();
		}

		//action information
		public boolean enabledAction() {
			return action == null ? false : cellCanModify;
		}

		public void setEnabledAction(boolean enabled) {
			if (action != null)	
				if (action.enabled().booleanValue()) cellCanModify = enabled;
		}
	};

	/**
	 * return elements array for the tableviewer
	 */
	public Object[] getElements(Object rootElement) {
		if (rootElement instanceof Object[]) {
			return (Object[]) rootElement;
		}

		if (rootElement instanceof List && rootElement!= null) {
			List<?> list = (List<?>) rootElement;
			List<ConfigTableElement> elements = new ArrayList<ConfigTableElement>();
			for (Iterator<?> iter_c = list.iterator(); iter_c.hasNext();) {
				Object model = iter_c.next();
				if (model != null && model instanceof Configuration) {
					Configuration configuration = (Configuration) model;
					for (Iterator<?> iter_e = configuration.getOwnedResource().iterator(); iter_e.hasNext();) {
						Resource resource = (Resource) iter_e.next();
						ConfigTableElement element = new ConfigTableElement(resource);
						element.update();
						elements.add(element);
					}
				} 
			}
			return ((elements == null) ? new Object[0] : elements.toArray());
		}

		return new Object[0];
	}

	public void dispose() {
		// TODO Auto-generated method stub
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}

};
