package org.openembedd.checker.view;
/**
 * Copyright � 2007 The Eclipse Foundation. All Rights Reserved
 * Eclipse Public License - v 1.0
 * Internal API
 * ----------------------------------------------------------------------------
 * Modified date : mars 2007
 * Authors       : openembedd
 * what          : adapt to checker
 * ----------------------------------------------------------------------------   
 */
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.openembedd.checker.view.ButtonCellEditor.DownloadAction;

public class WizardLicense extends Wizard {
	private WizardLicensePage wizardLicensePage;
	private DownloadAction[] downloadAction;

	public WizardLicense(DownloadAction[] downloadAction) {
		super();
		this.downloadAction = downloadAction;
	}

	@Override
	public void addPages() {
		// TODO Auto-generated method stub
		wizardLicensePage = new WizardLicensePage(true, downloadAction);
		addPage(wizardLicensePage);
		//setDialogSettings();
		//setDefaultPageImageDescriptor(ImageDescriptor.createFromImage(CheckingIcons.get(DownloadConstants.OE32x32)));
		setForcePreviousAndNextButtons(true);
		//setNeedsProgressMonitor(true);
		setWindowTitle("Install");
		setHelpAvailable(false);
	}

	@Override
	public boolean canFinish() {
		// TODO Auto-generated method stub
		return super.canFinish();
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		// TODO Auto-generated method stub
		return super.getNextPage(page);
	}

	@Override
	public boolean performCancel() {
		// TODO Auto-generated method stub
		return super.performCancel();
	}

	@Override
	public boolean performFinish() {
		// TODO Auto-generated method stub
		return true;
	}

}
