package org.openembedd.checker.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.update.internal.ui.UpdateUI;
import org.openembedd.checker.action.CheckingIcons;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.action.DownloadJob;
import org.openembedd.checker.check.library.DownloadWebFile;
import org.openembedd.checker.modelConfig.EnumKindOfInclusion;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;

/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class ButtonCellEditor extends DialogCellEditor {

	private TableViewer tableviewer;
	private boolean verbose = false;

	public class DownloadAction extends Action {
		private ConfigTableElement configElement;
		private boolean success; 
		private List<String> message;

		public DownloadAction(ConfigTableElement configElement) {
			super("Download " + configElement.getTextResource());
			this.configElement = configElement;
			this.success = false;
			this.message = new ArrayList<String>();
		}

		/**
		 * check if connection parameters are valid
		 * @return
		 */
		public boolean checkValidParameter(String url, String nameWebFile, String targetPathFile, String targetFile) {
			if (url == null) {
				message.add("Error downloading resource, no specified URL for the web site.");
				return false;
			}
			if (url != null && url.trim().equals("")) {
				message.add("Error downloading resource, no specified URL for the web site.");
				return false;
			}
			if (nameWebFile == null) {
				message.add("Error downloading resource, no specified web file.");
				return false;
			}
			if (nameWebFile != null && nameWebFile.trim().equals("")) {
				message.add("Error downloading resource, no specified web file.");
				return false;
			}
			if (targetPathFile == null) {
				message.add("Error downloading resource, no specified target path or target path is read-only.");
				return false;
			}
			if (targetPathFile != null && targetPathFile.trim().equals("")) {
				message.add("Error downloading resource, no specified target path or target path is read-only.");
				return false;
			}
			if (targetFile == null) {
				message.add("Error downloading resource, no specified target file.");
				return false;
			}
			if (targetFile != null && targetFile.trim().equals("")) {
				message.add("Error downloading resource, no specified target file.");
				return false;
			}
			return true;
		}

		public void run() {
			boolean fileReceived = false;
			boolean connected = false;
			boolean accept = false;
			boolean checkValidParameter = false;
			try {
				if (!configElement.getResourceIsLocalFile())
				{
					String url = configElement.getResourceSiteURL();
					String nameWebFile = configElement.getResourceNameWebFile();
					String targetPathFile = configElement.getResourceTargetPathFile();
					String targetFile = configElement.getResourceTargetNameFile();
					checkValidParameter = checkValidParameter(url, nameWebFile, targetPathFile, targetFile);
					if (checkValidParameter) {
						DownloadWebFile downloadWebFile = new DownloadWebFile();
						downloadWebFile.setNames(url, nameWebFile, targetPathFile, targetFile);
						accept = downloadWebFile.accept();
						connected = accept ? downloadWebFile.connect() : false;
						if (connected) {
							if (configElement.getResourceIncludesIn().equals(EnumKindOfInclusion.NONE_LITERAL))
								//simple file
								fileReceived = downloadWebFile.getWebFile();
							else if (configElement.getResourceIncludesIn().equals(EnumKindOfInclusion.IN_ZIP_LITERAL)) {
								//file inside ZIP
								String entry = configElement.getResourceEntry();
								if (entry != null && !entry.trim().equals("")) downloadWebFile.setEntryName(entry);
								fileReceived = downloadWebFile.getWebFileFromZip();
							}
							else if (configElement.getResourceIncludesIn().equals(EnumKindOfInclusion.IN_JAR_LITERAL)) {
								//file inside JAR
								fileReceived = downloadWebFile.getWebFileFromJar();
							}
						}
					}
				}
				else {
					//TODO : file is local
				}
			} catch (InterruptedException e) {
				DownloadConstants.log("Interruption error of the action of download " + getText(), e);
			} catch (Exception e) {
				if (e instanceof java.net.MalformedURLException) {
					message.add("Error : malformed url for the web site among the download parameters.");
					DownloadConstants.log("Error : malformed url for the web site among the download parameters.", e);
				}
				else if (e instanceof java.io.FileNotFoundException) {
					message.add("Error : the " + configElement.getTextTypeResource() + " was not found on the server " + configElement.getResourceSiteURL() + ".");
					DownloadConstants.log("Error : the " + configElement.getTextTypeResource() + " was not found on the server " + configElement.getResourceSiteURL() + ".", e);
				}
				else if (e instanceof java.net.UnknownHostException) {
					message.add("Error : unknown server for the web site among the download parameters.");
					DownloadConstants.log("Error : unknown server for the web site among the download parameters.", e);
				}
				else {
					message.add("Error : see the log error view.");
					DownloadConstants.log("Error during the download", e);
				}
			}

			if (fileReceived) {
				//success
				success = true;
			}
			else {
				//TODO
				//failure
				if (checkValidParameter) {
					if (!accept) {
						//connection failed
						message.add("Some of your parameters are not accepted. Check that the target is not in read only.");
					}
					else if (!connected) {
						//connection failed
						message.add("Connection failed to the server.");
					}
					else {
						//download failed
						message.add("Resource download is failed.");
					}
				}
			}
		}

		public ConfigTableElement getConfigTableElement() {
			return configElement;
		}

		public boolean isSuccess() {
			return success;
		}

		public List<String> getMessageError() {
			return message;
		}
	};

	protected ButtonCellEditor(Composite parent, int style) {
		super(parent, style);
	}

	public ButtonCellEditor(TableViewer parent) {
		this(parent.getTable(), SWT.NONE);
		this.tableviewer = parent;
	}

	/**
	 * Creates the button for this cell editor under the given parent control.
	 * <p>
	 * The default implementation of this framework method creates the button 
	 * display on the right hand side of the dialog cell editor. Subclasses
	 * may extend or reimplement.
	 * </p>
	 *
	 * @param parent the parent control
	 * @return the new button control
	 */
	@Override
	public Button createButton(Composite parent) {
		Button result = new Button(parent, SWT.DOWN);
		result.setText("  ...  "); //$NON-NLS-1$
		return result;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		Object element = getValue();
		Shell shell = cellEditorWindow.getParent().getShell();
		if (element instanceof ConfigTableElement) {
			ConfigTableElement configElement = (ConfigTableElement) element;
			if (configElement.getTextTypeResource().equals(DownloadConstants.type_FEATURE)) {
				boolean installed = configElement.installFeature();
				if (!installed) {
					MessageDialog.openInformation(
							shell,
							DownloadConstants.dialog_title_configuration,
							"The feature " + configElement.getTextResource() + " could not be installed.\n" + "This feature cannot be got from the specified remote site " + configElement.getResourceSiteURL() + ".");
				}
			}
			else {
				if (configElement.enabledAction()) {
					if (configElement.getTextTypeResource().equals(DownloadConstants.type_LIBRARY)) {
						String targetPluginName = configElement.getNamePluginTargetForResourceLibrary();
						//case where library must go in a plugin which is not yet installed
						if (targetPluginName != null && configElement.getResourceTargetPathFile() == null) {
							MessageDialog.openInformation(
									shell,
									DownloadConstants.dialog_title_configuration,
									"The target plugin " + targetPluginName + " of library " + configElement.getTextResource() + " must be installed yet before this action.");
							tableviewer.refresh(configElement);
							ISelection selection = new StructuredSelection(configElement);
							tableviewer.setSelection(selection);
							return null;
						}
					}
					//download action list
					DownloadAction[] downloadAction = new DownloadAction[] {new DownloadAction(configElement)}; 
					DownloadJob djob = new DownloadJob("Download missing resource", shell, downloadAction);
					addWakeUpActionButton(djob, null);

					WizardLicense wizard = new WizardLicense(downloadAction);
					WizardDialog dialog = new WizardDialog(shell, wizard);
					Window.setDefaultImage(CheckingIcons.get(DownloadConstants.eclipseIcon));
					dialog.setPageSize(300, 300);
					dialog.create();
					int resultWizard = dialog.open();
					if (resultWizard == WizardDialog.OK) {
						IProgressService service = PlatformUI.getWorkbench().getProgressService();
						service.showInDialog(shell, djob);
						djob.setPriority(Job.INTERACTIVE);
						djob.schedule();
						//block repetitive action
						configElement.setEnabledAction(false);
						tableviewer.refresh(configElement);
					}
					ISelection selection = new StructuredSelection(configElement);
					tableviewer.setSelection(selection);
				}
			}
		}
		if (verbose) System.out.println("action");
		return null;
	}

	private void addWakeUpActionButton(final DownloadJob djob, final Action wakeUpAction) {

		djob.addJobChangeListener(new IJobChangeListener() {
			public void aboutToRun(IJobChangeEvent event) {
				if (verbose) System.out.println("aboutToRun");
			}

			public void awake(IJobChangeEvent event) {
				if (verbose) System.out.println("awake");
			}

			public void done(final IJobChangeEvent event) {
				djob.getShell().getDisplay().asyncExec (
						new Runnable() { 
							@SuppressWarnings("restriction")
							public void run(){
								//DownloadAction list
								DownloadAction[] downloadAction = djob.getDownloadAction();

								//wake up
								if (wakeUpAction != null) {
									//...TODO
									wakeUpAction.run();
									//...TODO
								}
								else {
									if (event.getResult().equals(Status.OK_STATUS)) {
										boolean needRestart = false;
										if (verbose) System.out.println("OK_STATUS");
										if (verbose) System.out.println(djob.getLastDownloadActionRun());
										List<String> message = new ArrayList<String>();
										for (int i = 0; i < downloadAction.length; i++) {
											if (downloadAction[i].isSuccess()) {
												needRestart = true;
												downloadAction[i].getConfigTableElement().update();
												tableviewer.refresh(downloadAction[i].getConfigTableElement());//shunt, no control
											}
											else {
												downloadAction[i].getConfigTableElement().setEnabledAction(true);
												String error = "Failure : download resource." + downloadAction[i].getConfigTableElement().getTextResource(); 
												message.add(error);
												message.addAll(downloadAction[i].getMessageError());
											}
										}
										if (!message.isEmpty()) {
											String messageError = "";
											for (String string : message) {
												messageError += string + "\n";
											}
											MessageDialog.openInformation(
													djob.getShell(),
													DownloadConstants.dialog_title_configuration,
													messageError);
										}
										ISelection selection = new StructuredSelection(downloadAction[0].getConfigTableElement());
										tableviewer.setSelection(selection);
										if (needRestart) UpdateUI.requestRestart(needRestart);
									}
									else if (event.getResult().equals(Status.CANCEL_STATUS)) {
										//cancel previous action
										if (verbose) System.out.println("CANCEL_STATUS");
										if (djob.getLastDownloadActionRun() < downloadAction.length) {
											//we have to cancel the actions which are before the last finished
											if (djob.getLastDownloadActionRun() > 0) {
												//...TODO
												//...TODO
											}
											//re-active buttons
											for (int i = 0; i < downloadAction.length; i++) {
												downloadAction[i].getConfigTableElement().setEnabledAction(true);
											}
											if (verbose) System.out.println(djob.getLastDownloadActionRun());
										}
									}
									else {
										if (verbose) System.out.println("? event.getResult() = " + event.getResult().toString());
									}
								}
							}
						});
				if (verbose) System.out.println("done call back");
			}

			public void running(IJobChangeEvent event) {
				if (verbose) System.out.println("running");
			}

			public void scheduled(IJobChangeEvent event) {
				if (verbose) System.out.println("scheduled");
			}

			public void sleeping(IJobChangeEvent event) {
				if (verbose) System.out.println("sleeping");
			}
		});
	}
};
