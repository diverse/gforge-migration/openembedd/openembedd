package org.openembedd.checker.view;

import java.util.List;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class ResourceFilter extends ViewerFilter {

	/**
	 * folder
	 */
	private CTabFolder pFolder;
	private CTabFolder cFolder;

	/**
	 * @param folder
	 */
	public ResourceFilter(CTabFolder pFolder, CTabFolder cFolder) {
		super();
		this.pFolder = pFolder;
		this.cFolder = cFolder;
	}

	@Override
	/**
	 *  TODO: to optimize
	 */
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof ConfigTableElement && element!= null) {
			if (cFolder == null && pFolder != null) {
				ConfigTableElement configElement = (ConfigTableElement)element;
				if (configElement.getResourceIsVisible()) {
					List<Profil> pList = configElement.getResourceProfilList();
					if (pList == null)
						return true;
					CTabItem item = (CTabItem)pFolder.getSelection();
					if (item != null && pList.contains((Profil)item.getData("key"))) {
						return true;
					}
				}
			}
			else if (cFolder != null && pFolder != null){
				ConfigTableElement configElement = (ConfigTableElement)element;
				if (configElement.getResourceIsVisible()) {
					CTabItem itemC = (CTabItem)cFolder.getSelection();
					Configuration configuration = itemC == null ? null : (Configuration)itemC.getData("key");
					if (configuration != null && !configuration.equals(configElement.getResourceOwner()))
						return false;
					List<Profil> pList = configElement.getResourceProfilList();
					if (pList == null)
						return true;
					CTabItem itemP = (CTabItem)pFolder.getSelection();
					if (itemP != null && pList.contains((Profil)itemP.getData("key"))) {
						return true;
					}
				}
			}
		}
		return false;
	}
};
