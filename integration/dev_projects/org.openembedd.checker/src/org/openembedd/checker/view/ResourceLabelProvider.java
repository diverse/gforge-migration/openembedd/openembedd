package org.openembedd.checker.view;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Control;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;
import org.osgi.framework.Version;
/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class ResourceLabelProvider implements ITableLabelProvider {

	public Image getColumnImage(Object element, int columnIndex) {
		if (element instanceof ConfigTableElement && element!= null) {
			ConfigTableElement configElement = (ConfigTableElement)element;
			switch (columnIndex) {
			case 0 : return configElement.getImageTypeResource();
			case 1 : return configElement.getImageState();
			case 2 : break;
			case 3 : break;
			case 4 : break;
			case 5 : break;
			default : return null;
			}
		}
		return null;
	}

	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof ConfigTableElement && element!= null) {
			ConfigTableElement configElement = (ConfigTableElement)element;
			switch (columnIndex) {
			case 0 : return configElement.getTextTypeResource();
			case 1 : break;
			case 2 : //bad version for the jvm
				if (configElement.getResourceIsJavaVersionGreaterToPlatform()) {
					return DownloadConstants.labelStateBadJVM;
				}
				//one version installed
				if (configElement.getResourceExistIgnoreVersion()) {
					Version versionInstalled = configElement.getResourceVersionInstalled();
					if (versionInstalled != null) { 
						Version versionRequired = configElement.getResourceVersion();
						if (versionRequired != null) {
							if (versionInstalled.compareTo(versionRequired) > 0) {
								//more recent version installed
								return DownloadConstants.labelStateMoreRecent;
							}
							if (versionInstalled.compareTo(versionRequired) < 0) {
								//less recent version installed
								return DownloadConstants.labelStateLessRecent;
							}
							//Note: case == 0 cannot be possible because the resource doesn't exist 
						}
					}
				}
				//no version installed yet
				return configElement.getResourceIsRequired() ? DownloadConstants.labelStateRequired : configElement.getTextState();
			case 3 : return configElement.getTextResource();
			case 4 : return configElement.getTextDescriptionAction();
			case 5 : break;
			default : return "";
			}
		}
		return null;
	}

	public Control getColumnControl(Object element, int columnIndex) {
		return null;
	}

	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub		
	}

	public void dispose() {
		// TODO Auto-generated method stub		
	}

	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
	}

}
