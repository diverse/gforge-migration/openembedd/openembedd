package org.openembedd.checker.view;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.spi.RegistryContributor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPluginContribution;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;
import org.openembedd.checker.action.CheckingIcons;
import org.openembedd.checker.action.DownloadConstants;
import org.openembedd.checker.modelConfig.Configuration;
import org.openembedd.checker.modelConfig.Profil;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;
import org.osgi.framework.Version;
/*********************************************************************************************
 * This view shows all configuration files declared in the Eclipse platform.
 * Each of these files are created from a template EMF (> = 2.3) and are stored in plugins
 * or fragments. A model can contain several profils for a product (ex. developer, user, 
 * designer, architect, etc.)
 * To activate the view it is necessary to declare extension point with a configuration model. 
 *********************************************************************************************
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 *--------------------------------------------------------------------------------------------
 * Last revised : december 2007 
 **********************************************************************************************/
public class DownloadWebFileView extends ViewPart implements IViewPart{
	private static IWorkbenchWindow window;
	public static final String nameIDView = "org.openembedd.checking.views.DownloadWebFileView";
	//widgets
	private TableViewer tableviewer;
	private Table table;
	private Label labelI, labelT, labelCurrentI, labelCurrentT;
	private Composite compProfilTabFolder, compConfigTabFolder;
	private SelectionListener selectionListenerProfilTabFolder = null;
	private CTabFolder profilFolder, configFolder = null;
	private Text textDescription;
	private ConfigurationComboBarAction configurationPlatform;
	private ConfigurationFactoryExtension currentConfiguration;
	private DialogSettings settings = null;
	//model
	private java.util.List<Configuration> oeConfig;
	//debug
	private boolean verbose = false;

	/**
	 * constructor
	 */
	public DownloadWebFileView() {
		super();
		oeConfig = new ArrayList<Configuration>();
	}

	/**
	 * method to load the model of configuration
	 */
	private Configuration[] loadModelTableContent(String pathModelResource) {
		List<Configuration> model = new ArrayList<Configuration>();
		if (pathModelResource != null && pathModelResource.endsWith(DownloadConstants.configSuffix)) {
			try {
				ResourceSet resourceSet = new ResourceSetImpl();
				URI fileURI = URI.createURI(DownloadConstants.configPathFileLoad + pathModelResource, true);
				org.eclipse.emf.ecore.resource.Resource model_resource = resourceSet.getResource(fileURI, true);
				//model = new ArrayList<Configuration>();
				for (Iterator<EObject> iterator = model_resource.getContents().iterator(); iterator.hasNext();) {
					EObject eObject = (EObject) iterator.next();
					if (eObject instanceof Configuration) {
						Configuration configuration = (Configuration) eObject;
						if(configuration.getNameID() != null && !configuration.getNameID().trim().equals("")) 
							model.add(configuration);
					}
				}
			} catch (Exception e) {
				DownloadConstants.log("Error loading configuration model.", e);
			}
		}
		return (Configuration[])model.toArray(new Configuration[model.size()]);
	}

	/**
	 * Method which returns the configuration location (+ segment added optionally)
	 */
	Path getPathConfiguration(Path segment) {
		try {
			boolean exist = true;
			Location location = Platform.getInstallLocation();
			String path = location.getURL().getPath();
			Path pathName = new Path(path);
			pathName = (Path) pathName.append(DownloadConstants.configPathSave);
			if (!pathName.toFile().exists()) {
				try {
					exist = pathName.toFile().mkdirs();
				} catch (Exception e) {
					DownloadConstants.log("Error creating directory of configuration.", e);
				}
			}
			//add segment (optional)
			if (exist && segment != null) {
				pathName = (Path) pathName.append(segment);
				if (!pathName.toFile().exists()) {
					try {
						exist = pathName.toFile().mkdirs();
					} catch (Exception e) {
						DownloadConstants.log("Error creating segment " + segment.toOSString() +" inside directory of configuration.", e);
					}
				}
			}
			if (exist) {
				return pathName;
			}
		} catch (Exception e) {
			DownloadConstants.log("Error getting configuration path.", e);
		}
		return null;
	}

	/**
	 * method to save the model of configuration in order to have a "photo" of installations
	 */
	private void saveTableContent() {
		if (oeConfig != null) {
			try {
				Path pathName = getPathConfiguration(new Path(DownloadConstants.configDirectorySaveModel));
				if (pathName != null) {
					ResourceSet resourceSet = new ResourceSetImpl();
					pathName = (Path) pathName.append(DownloadConstants.configFile + DownloadConstants.configSuffix);
					URI fileURI = URI.createFileURI(pathName.toPortableString());
					org.eclipse.emf.ecore.resource.Resource model_resource = resourceSet.createResource(fileURI);
					for (Iterator<Configuration> iter = oeConfig.iterator(); iter.hasNext();) {
						Configuration configuration = (Configuration) iter.next();
						model_resource.getContents().add(configuration);	
					}
					try {
						model_resource.save(null);
					} catch (Exception e) {
						DownloadConstants.log("Error saving configuration model.", e);
					}
				}
			} catch (Exception e) {
				DownloadConstants.log("Error saving configuration model.", e);
			}
		}
	}

	/**
	 * Method which updates profil tabfolder when the configuration changes
	 */
	protected void updateProfilTabFolder(final boolean restore) {
		clearItemsProfil();
		if (oeConfig.size() <= 1) {
			java.util.List<Profil> listModelProfilUsed = oeConfig.size() == 0 ? null : getModelProfilUsed(oeConfig.get(0));
			if (listModelProfilUsed != null) {
				List<String> listProfilNameID = new ArrayList<String>();
				for (Iterator<Profil> iterator = listModelProfilUsed.iterator(); iterator.hasNext();) {
					Profil profil = (Profil) iterator.next();
					createItemProfil(profil.getName(), profil);
					listProfilNameID.add(profil.getNameID());
				}
				Profil profil = null;
				if (restore) {
					IDialogSettings section = settings == null ? null : settings.getSection(DownloadConstants.keyCurrentConfiguration); 
					if (section != null) {
						String pSettingsNameID = section.get(DownloadConstants.keyProfil);
						if (pSettingsNameID == null || pSettingsNameID.trim().equals("") || !listProfilNameID.contains(pSettingsNameID)) {
							profil = listModelProfilUsed.get(0);
						}
						else {
							profil = listModelProfilUsed.get(listProfilNameID.indexOf(pSettingsNameID));
						}
					}
				}
				else {
					profil = listModelProfilUsed.get(0);
				}
				selectItemProfilTabFolder(profil);
			}
			else {
				createItemProfil(DownloadConstants.CTabItem_DEFAULT_LABEL, DownloadConstants.CTabItem_NONE);
				selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
			}
			tableviewer.refresh();
			computeProfil();
			return;
		}
	}

	/**
	 * Method which creates tabfolder to manage profil
	 * @param parent
	 */
	protected void createTabFolder(Composite parent) {
		//configuration
		if (configFolder != null)
			configFolder.dispose();
		if (oeConfig.size() > 1) {
			compConfigTabFolder = new Composite(parent, SWT.WRAP);
			GridData compConfigTabFoldergriddata = new GridData();
			compConfigTabFoldergriddata.horizontalAlignment = GridData.FILL;
			compConfigTabFoldergriddata.verticalAlignment = GridData.FILL;
			compConfigTabFoldergriddata.horizontalSpan = 4;
			compConfigTabFoldergriddata.grabExcessHorizontalSpace = true;
			compConfigTabFoldergriddata.grabExcessVerticalSpace = false;
			compConfigTabFolder.setLayoutData(compConfigTabFoldergriddata);
			compConfigTabFolder.setLayout(new FillLayout(SWT.HORIZONTAL));
			configFolder = new CTabFolder(compConfigTabFolder, SWT.BORDER | /*SWT.WRAP | */SWT.MULTI);
			compConfigTabFoldergriddata.heightHint = configFolder.getTabHeight();
			configFolder.setSimple(false);
			configFolder.setMinimized(true);
			java.util.List<Configuration> listModelConfigUsed = getModelConfigurationUsed();
			if (listModelConfigUsed != null) {
				for (Iterator<Configuration> iter = listModelConfigUsed.iterator(); iter.hasNext();) {
					Configuration configuration = (Configuration) iter.next();
					CTabItem item = new CTabItem(configFolder, SWT.NONE);
					String text = configuration.getName().toLowerCase().indexOf("configuration") > 0 ? configuration.getName() : "Configuration" + " " + configuration.getName() ;
					item.setText(text);
					item.setData("key", configuration);
				}
			}
			else {
				CTabItem item = new CTabItem(configFolder, SWT.NONE);
				item.setText(DownloadConstants.CTabItem_DEFAULT_LABEL);
				item.setData(DownloadConstants.CTabItem_NONE);
			}

			//initialization
			if (listModelConfigUsed != null) {
				Configuration configuration = listModelConfigUsed.get(0);
				selectItemConfigTabFolder(configuration);
			}
			else {
				selectItemConfigTabFolder(DownloadConstants.CTabItem_NONE);
			}

			configFolder.addSelectionListener(new SelectionListener() {

				public void widgetDefaultSelected(SelectionEvent e) {
					java.util.List<Configuration> listModelConfigUsed = getModelConfigurationUsed();
					if (listModelConfigUsed != null) {
						Configuration configuration = listModelConfigUsed.get(0);
						selectItemConfigTabFolder(configuration);
					}
					else {
						selectItemConfigTabFolder(DownloadConstants.CTabItem_NONE);
					}
				}

				public void widgetSelected(SelectionEvent e) {
					CTabFolder tabfolder = (CTabFolder)e.getSource();
					CTabItem item = (CTabItem)tabfolder.getSelection();
					Configuration configuration = (Configuration)item.getData("key");
					clearItemsProfil();
					java.util.List<Profil> listModelProfilUsed = getModelProfilUsed(configuration);
					if (listModelProfilUsed != null) {
						for (Iterator<Profil> iterator = listModelProfilUsed.iterator(); iterator.hasNext();) {
							Profil profil = (Profil) iterator.next();
							createItemProfil(profil.getName(), profil);
						}
						Profil profil = listModelProfilUsed.get(0);
						selectItemProfilTabFolder(profil);
					}
					else {
						createItemProfil(DownloadConstants.CTabItem_DEFAULT_LABEL, DownloadConstants.CTabItem_NONE);
						selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
					}
					tableviewer.refresh();
					computeProfil();
				}
			});
		}

		//profil
		Composite compProfilTabFolder = new Composite(parent, SWT.WRAP);
		GridData compProfilTabFoldergriddata = new GridData();
		compProfilTabFoldergriddata.horizontalAlignment = GridData.FILL;
		compProfilTabFoldergriddata.verticalAlignment = GridData.FILL;
		compProfilTabFoldergriddata.horizontalSpan = 4;
		compProfilTabFoldergriddata.grabExcessHorizontalSpace = true;
		compProfilTabFoldergriddata.grabExcessVerticalSpace = false;
		compProfilTabFolder.setLayoutData(compProfilTabFoldergriddata);
		compProfilTabFolder.setLayout(new FillLayout(SWT.HORIZONTAL));			
		profilFolder = new CTabFolder(compProfilTabFolder, SWT.BORDER | /*SWT.WRAP | */SWT.MULTI);
		compProfilTabFoldergriddata.heightHint = profilFolder.getTabHeight();
		profilFolder.setSimple(false);
		profilFolder.setMinimized(true);
		java.util.List<Configuration> listModelConfigUsed = getModelConfigurationUsed();
		if (oeConfig.size() > 1 && listModelConfigUsed != null && listModelConfigUsed.size() > 1) {
			Configuration configuration = listModelConfigUsed.get(0);
			java.util.List<Profil> listModelProfilUsed = getModelProfilUsed(configuration);
			if (listModelProfilUsed != null && listModelProfilUsed.size() > 0) {
				for (Iterator<Profil> iter = listModelProfilUsed.iterator(); iter.hasNext();) {
					Profil profil = (Profil) iter.next();
					createItemProfil(profil.getName(), profil);
				}	
			}
			else {
				createItemProfil(DownloadConstants.CTabItem_DEFAULT_LABEL, DownloadConstants.CTabItem_NONE);
			}

			//initialization
			if (listModelProfilUsed != null) {
				Profil profil = listModelProfilUsed.get(0);
				selectItemProfilTabFolder(profil);
			}
			else {
				selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
			}
		}
		else {
			java.util.List<Profil> listModelProfilUsed = getModelProfilUsed(null);
			if (listModelProfilUsed != null) {
				for (Iterator<Profil> iter = listModelProfilUsed.iterator(); iter.hasNext();) {
					Profil profil = (Profil) iter.next();
					createItemProfil(profil.getName(), profil);
				}
			}
			else {
				createItemProfil(DownloadConstants.CTabItem_DEFAULT_LABEL, DownloadConstants.CTabItem_NONE); 
			}

			//initialization
			if (listModelProfilUsed != null) {
				Profil profil = listModelProfilUsed.get(0);
				selectItemProfilTabFolder(profil);
			}
			else {
				selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
			}
		}
		profilFolder.addSelectionListener(createSelectionListenerProfilTabFolder());
	}

	//extension
	class ConfigurationFactoryExtension implements IPluginContribution {
		private IConfigurationElement fconfig;
		private String pluginID, fragmentID = null;
		private String id, label;
		private String icon;
		private String model;
		private String licensePath;  

		public ConfigurationFactoryExtension(IConfigurationElement config) {
			fconfig = config;
			pluginID = config.getContributor().getName();
			if (fconfig.getContributor()!= null && (fconfig.getContributor() instanceof RegistryContributor)) {
				RegistryContributor contributor = (RegistryContributor) fconfig.getContributor(); 
				fragmentID = contributor.getActualName().equals(contributor.getName()) ? null : contributor.getActualName();
			}
			id = fconfig.getAttribute(DownloadConstants.extensionID);
			label = fconfig.getAttribute(DownloadConstants.extensionLabel);
			icon = fconfig.getAttribute(DownloadConstants.extensionIcon);
			model = fconfig.getAttribute(DownloadConstants.extensionModel);
			licensePath = fconfig.getAttribute(DownloadConstants.extensionLicensePath);
		}

		public Configuration[] getConfigurations() {
			Configuration[] configurations = null;
			if (model != null && !model.equals("")) 
				configurations = loadModelTableContent(((fragmentID != null) ? fragmentID : pluginID) + DownloadConstants.slash + model);
			if (configurations != null && configurations.length > 0) {
				for (int k = 0; k < configurations.length; k++) {
					configurations[k].setPluginContributorNameID((fragmentID != null) ? fragmentID : pluginID);
					if (licensePath != null && !licensePath.equals(""))
						configurations[k].setPathLicenseContributor(licensePath);
				}
			}
			return configurations == null ? (Configuration[])(new Configuration[0]) : configurations;
		}

		public String getLocalId() {
			return id;
		}

		public String getPluginId() {
			return pluginID;
		}

		public String getFragmentId() {
			return fragmentID;
		}

		public String getLabel() {
			return label;
		}

		public String getModel() {
			return model;
		}

		public String getIcon() {
			return icon;
		}

		public String getLicensePath() {
			return licensePath;
		}

		public ImageDescriptor getImage() {
			if (icon != null && !icon.equals("")) {
				try {
					URL url = Platform.getBundle(((fragmentID != null) ? fragmentID : pluginID)).getEntry("/");
					ImageDescriptor image = (url == null) ? null : ImageDescriptor.createFromURL(new URL(url, icon));
					return image == null || image.getImageData() == null ? null : image.getImageData().height > 16 ? null : image;
				} catch (MalformedURLException e) {
					return null;
				}
			}
			return null;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj != null && obj instanceof ConfigurationFactoryExtension) {
				ConfigurationFactoryExtension cfe = (ConfigurationFactoryExtension)obj;
				return this.fconfig.equals(cfe.fconfig);
			}
			return super.equals(obj);
		}
	};

	//choice configuration
	class ConfigurationComboBarAction extends Action implements IMenuCreator {
		private Menu fMenu;
		private List<ConfigurationFactoryExtension> fFactoryExtensions;
		public static final String labelDefault = DownloadConstants.item_default_label_configuration;
		public ImageDescriptor imageDefault = DownloadConstants.getIconCongigDescriptor();

		public ConfigurationComboBarAction() {
			init();
			setText(DownloadConstants.menu_title_choice_configuration); 
			setToolTipText(DownloadConstants.menu_title_choice_configuration);  
			setImageDescriptor(DownloadConstants.getIconViewConfigurationFile());
			setMenuCreator(this);
		}

		public boolean isConfigurationExtensionExist(ConfigurationFactoryExtension fconfig) {
			if (fFactoryExtensions == null || fFactoryExtensions.size() == 0 || fconfig == null)
				return false;
			else
				return fFactoryExtensions.contains(fconfig);
		}

		private void init() {
			fFactoryExtensions = new ArrayList<ConfigurationFactoryExtension>();
			IExtensionRegistry registry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = registry.getExtensionPoint(DownloadConstants.getExtensionPointIDModelConfiguration());
			if (extensionPoint != null) {
				IExtension[] extensions = extensionPoint.getExtensions();
				for (int i = 0; i < extensions.length; i++) {
					IConfigurationElement[] elements = extensions[i].getConfigurationElements();
					for (int j = 0; j < elements.length; j++) {
						ConfigurationFactoryExtension fconfig =  new ConfigurationFactoryExtension(elements[j]);
						fFactoryExtensions.add(fconfig);
					}
				}
			}
			setEnabled(fFactoryExtensions.size() > 0);
		}

		public void dispose() {
			fFactoryExtensions.clear();
			fFactoryExtensions = null;
		}

		public Menu getMenu(Control parent) {
			if (fMenu != null) {
				fMenu.dispose();
			}
			fMenu= new Menu(parent);
			for (Iterator<ConfigurationFactoryExtension> iterator = fFactoryExtensions.iterator(); iterator.hasNext();) {
				ConfigurationFactoryExtension fconfig = (ConfigurationFactoryExtension) iterator.next();
				ConfigurationFactoryAction action = new ConfigurationFactoryAction(parent, fconfig.getLabel() == null ? labelDefault : fconfig.getLabel(), fconfig.getImage() == null ? imageDefault : fconfig.getImage(), fconfig);
				action.setChecked(fconfig.equals(currentConfiguration));
				addActionToMenu(fMenu, action);					
			}
			return fMenu;
		}

		private void addActionToMenu(Menu parent, Action action) {
			ActionContributionItem item = new ActionContributionItem(action);
			item.fill(parent, -1);
		}

		public Menu getMenu(Menu parent) {
			return null;
		}

		class ConfigurationFactoryAction extends Action {
			private ConfigurationFactoryExtension fconfig;
			private Control parent;

			public ConfigurationFactoryAction(Control parent, String label, ImageDescriptor image, ConfigurationFactoryExtension extension) {
				super(label, AS_RADIO_BUTTON);
				setText(label);
				if (image != null) {
					setImageDescriptor(image);
				}
				fconfig = extension;
				this.parent = parent;
			}

			@Override
			public void run() {
				if (isChecked()) {
					(new Action() {
						@Override
						public void run() {
							setConfiguration(fconfig, false);
						}
					}).run();
				}
			}

			@Override
			public void setToolTipText(String toolTipText) {
				super.setToolTipText(toolTipText);
			}
		};
	};

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		final Composite comp = parent;
		//layout
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		gridLayout.makeColumnsEqualWidth = true;
		comp.setLayout(gridLayout);

		//label
		Group labelsP = new Group(parent, parent.getStyle() | SWT.WRAP);
		GridData compLabelsGridData = new GridData();
		compLabelsGridData.horizontalAlignment = GridData.FILL;
		compLabelsGridData.verticalAlignment = GridData.FILL;
		compLabelsGridData.horizontalSpan = 3;
		compLabelsGridData.grabExcessHorizontalSpace = true;
		compLabelsGridData.grabExcessVerticalSpace = false;
		labelsP.setLayoutData(compLabelsGridData);
		GridLayout labelsLayout = new GridLayout();
		labelsLayout.numColumns = 40;
		labelsLayout.makeColumnsEqualWidth = true;
		labelsP.setLayout(labelsLayout);

		labelI = new Label(labelsP, SWT.LEFT);
		labelT = new Label(labelsP, SWT.LEFT);
		GridData labelIgriddata = new GridData();
		labelIgriddata.horizontalAlignment = GridData.FILL;
		labelIgriddata.horizontalSpan = 2;
		labelIgriddata.grabExcessHorizontalSpace = true;
		labelI.setLayoutData(labelIgriddata);
		labelT.setText("");
		GridData labelTgriddata = new GridData();
		labelTgriddata.horizontalAlignment = GridData.FILL;
		labelTgriddata.horizontalSpan = 38;
		labelTgriddata.grabExcessHorizontalSpace = true;
		labelT.setLayoutData(labelTgriddata);

		Group labelsC = new Group(parent, parent.getStyle() | SWT.WRAP);
		GridData compConfigGridData = new GridData();
		compConfigGridData.horizontalAlignment = GridData.FILL;
		compConfigGridData.verticalAlignment = GridData.FILL;
		compConfigGridData.horizontalSpan = 1;
		compConfigGridData.grabExcessHorizontalSpace = true;
		compConfigGridData.grabExcessVerticalSpace = false;
		labelsC.setLayoutData(compConfigGridData);
		GridLayout labelsConfigLayout = new GridLayout();
		labelsConfigLayout.numColumns = 20;
		labelsConfigLayout.makeColumnsEqualWidth = true;
		labelsC.setLayout(labelsConfigLayout);

		labelCurrentI = new Label(labelsC, SWT.LEFT);
		labelCurrentT = new Label(labelsC, SWT.LEFT);

		GridData labelCurrentIgriddata = new GridData();
		labelCurrentIgriddata.horizontalAlignment = GridData.FILL;
		labelCurrentIgriddata.horizontalSpan = 2;
		labelCurrentIgriddata.grabExcessHorizontalSpace = true;
		labelCurrentI.setLayoutData(labelCurrentIgriddata);
		labelCurrentT.setText("");
		GridData labelCurrentTgriddata = new GridData();
		labelCurrentTgriddata.horizontalAlignment = GridData.FILL;
		labelCurrentTgriddata.horizontalSpan = 18;
		labelCurrentTgriddata.grabExcessHorizontalSpace = true;
		labelCurrentT.setLayoutData(labelCurrentTgriddata);

		Font font = new Font(labelCurrentT.getFont().getDevice(), "Arial", 9, SWT.NORMAL);
		if (font != null) {
			labelCurrentT.setFont(font);
		}

		labelsC.pack();labelsC.redraw();
		labelsP.pack();labelsP.redraw();

		//toolbar
		createToolbar();

		//tabfolder
		createTabFolder(comp);

		//tableviewer
		tableviewer = new TableViewer(comp, SWT.BORDER | SWT.H_SCROLL | SWT.FULL_SELECTION);
		GridData tablegriddata = new GridData();
		tablegriddata.horizontalAlignment = GridData.FILL;
		tablegriddata.verticalAlignment = GridData.FILL;
		tablegriddata.horizontalSpan = 3;
		tablegriddata.grabExcessHorizontalSpace = true;
		tablegriddata.grabExcessVerticalSpace = true;
		tableviewer.getControl().setLayoutData(tablegriddata);

		//table
		table = tableviewer.getTable();
		table.setHeaderVisible (true);
		table.setLinesVisible(true);
		TableColumn tc0 = new TableColumn (table, SWT.LEFT);tc0.setMoveable(false);
		TableColumn tc1 = new TableColumn (table, SWT.CENTER);tc1.setMoveable(false);
		TableColumn tc2 = new TableColumn (table, SWT.NONE);tc2.setMoveable(false);
		TableColumn tc3 = new TableColumn (table, SWT.NONE);tc3.setMoveable(false);
		TableColumn tc4 = new TableColumn (table, SWT.NONE);tc4.setMoveable(false);
		TableColumn buttonColumn = new TableColumn (table, SWT.NONE);buttonColumn.setMoveable(false);
		//hooked end column
		TableColumn tcEnd = new TableColumn (table, SWT.NULL);tc4.setMoveable(false);
		tcEnd.setWidth(1);tcEnd.setResizable(true);

		table.getColumn(0).setText ("Type");
		table.getColumn(0).setWidth(65);
		table.getColumn(1).setText ("");
		table.getColumn(1).setWidth(20);
		table.getColumn(2).setText ("State desc.");
		table.getColumn(2).setWidth(100);
		table.getColumn(3).setText ("Resource");
		table.getColumn(3).setWidth(250);
		table.getColumn(4).setText ("Action");
		table.getColumn(4).setWidth(230);
		table.getColumn(5).setImage(CheckingIcons.get(DownloadConstants.installHandler));
		table.getColumn(5).setAlignment(SWT.CENTER);
		table.getColumn(5).setText ("...");

		//provider
		tableviewer.setLabelProvider(new ResourceLabelProvider());
		tableviewer.setContentProvider(new ResourceContentProvider());
		tableviewer.addFilter(new ResourceFilter(profilFolder, configFolder));
		tableviewer.setSorter(new ResourceSorter());
		tableviewer.setInput(oeConfig);

		//computation
		//computeProfil();

		//cellmodifier
		CellEditor textDescStateEditor = new TextCellEditor(table);
		CellEditor textDescResourceEditor = new TextCellEditor(table);
		CellEditor textDescActionEditor = new TextCellEditor(table);
		CellEditor buttonEditor = new ButtonCellEditor(tableviewer);

		//column fixed
		table.getColumn(5).setWidth(buttonEditor.getLayoutData().minimumWidth);
		table.getColumn(5).setResizable(false);
		tableviewer.setCellEditors(
				new CellEditor[] {
						null,
						null,
						textDescStateEditor,
						textDescResourceEditor,
						textDescActionEditor,
						buttonEditor});
		tableviewer.setColumnProperties(
				new String[] {"imgType", "imgState", "descState", "descResource", "descAction", "button"});
		tableviewer.setCellModifier(
				new ICellModifier() {

					public boolean canModify(Object element, String property) {
						if (element instanceof ConfigTableElement) {
							ConfigTableElement tableElement = (ConfigTableElement) element;
							return property.equalsIgnoreCase("button") ? tableElement.enabledAction() : false;
						}
						else
							return false;
					}

					public Object getValue(Object element, String property) {
						if (element instanceof ConfigTableElement) {
							ConfigTableElement tableElement = (ConfigTableElement) element;
							if (property.equalsIgnoreCase("button"))
								return tableElement;
						}
						return null;
					}

					public void modify(Object element, String property, Object value) {
						if (element instanceof ConfigTableElement) {
							@SuppressWarnings("unused")
							ConfigTableElement tableElement = (ConfigTableElement) element;
							if (property.equalsIgnoreCase("button"))
								return;
						}
					}
				});

		tableviewer.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection itemSelected = (IStructuredSelection)event.getSelection();
				textDescription.setText("");
				if (itemSelected.getFirstElement() instanceof ConfigTableElement) {
					ConfigTableElement tableElement = (ConfigTableElement)itemSelected.getFirstElement();
					textDescription.append(tableElement.getTextResource() + " : " + "\n");
					textDescription.append(tableElement.getTextDescriptionResource() + "\n");
					textDescription.append("Version : " + (tableElement.getResourceVersion() == null ? tableElement.getResourceTextVersion() : tableElement.getResourceVersion().toString() ) + "\n");
					textDescription.append("" + "\n");
					textDescription.append("State : " + "\n");
					textDescription.append(tableElement.getTextState() +"\n");

					//jvm too old version
					if (tableElement.getResourceIsJavaVersionGreaterToPlatform()) {
						String javaJVMVersionRequired = tableElement.getResourceJavaJVMVersion();
						textDescription.append("" + "\n");
						textDescription.append("Error : " + "\n");
						textDescription.append("the jvm (java virtual machine) of the platform is a too old version for this " + tableElement.getTextTypeResource() + "\n");
						textDescription.append("current jvm = " + DownloadConstants.jvm  + "  required jvm = " + javaJVMVersionRequired +  "\n");
					}
					//target error
					if (tableElement.getResourceErrorTarget()) {
						textDescription.append("" + "\n");
						textDescription.append("Error : " + "\n");
						if (tableElement.getTextTypeResource().equals(DownloadConstants.type_LIBRARY)) {
							textDescription.append("no target specified for the " + tableElement.getTextTypeResource() + "\n");
							textDescription.append("or\n");
							textDescription.append("target in read-only for the " + tableElement.getTextTypeResource() + " (example : plugin in .jar)\n");
						}
						else
							textDescription.append("no target specified for the " + tableElement.getTextTypeResource() + "\n");
					}
					//resource with different version
					if (!tableElement.getResourceExist() && tableElement.getResourceExistIgnoreVersion()) {
						Version versionInstalled = tableElement.getResourceVersionInstalled();
						Version versionRequired = tableElement.getResourceVersion();
						if (versionInstalled != null && versionRequired != null) {
							textDescription.append("" + "\n");
							if (versionInstalled.compareTo(versionRequired) > 0) {
								textDescription.append("Warning : " + "\n");
								textDescription.append("a " + tableElement.getTextTypeResource() + " is installed with a more recent version = " + tableElement.getResourceVersionInstalled());	
							}
							if (versionInstalled.compareTo(versionRequired) < 0) {
								textDescription.append("Error : " + "\n");
								textDescription.append("a " + tableElement.getTextTypeResource() + " is installed with a less recent version = " + tableElement.getResourceVersionInstalled());
							}	
						}  
					}
				}
			} 

		});

		//description
		textDescription = new Text(comp, SWT.MULTI | SWT.WRAP | SWT.READ_ONLY);
		GridData textgriddata = new GridData();
		textgriddata.horizontalAlignment = GridData.FILL;
		textgriddata.verticalAlignment = GridData.FILL;
		textgriddata.horizontalSpan = 1;
		textgriddata.grabExcessHorizontalSpace = true;
		textgriddata.grabExcessVerticalSpace = true;
		textDescription.setLayoutData(textgriddata);
		textDescription.setBackground(comp.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		//init configuration
		if (currentConfiguration != null && configurationPlatform.isConfigurationExtensionExist(currentConfiguration)) {
			(new Action() {
				@Override
				public void run() {
					setConfiguration(currentConfiguration, true);
				}
			}).run();
		}
	}

	/**
	 * This method is called by createPartControl and allows
	 * to create the toolbar of the viewer and initialize it.
	 */
	private void createToolbar() {
		IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
		tbm.add(new Separator("profilGroup"));
		tbm.add(new Separator("choiceGroup"));
		configurationPlatform = new ConfigurationComboBarAction();
		tbm.add(configurationPlatform);
		tbm.add(new Separator("otherGroup"));
		tbm.add(new OtherComboBarAction());
		tbm.add(new Separator("otherGroup"));
	}

	//sample
	class OtherComboBarAction extends Action implements IMenuCreator {
		private Menu fMenu;
		private List<?> fList;

		public OtherComboBarAction() {
			init();
			setText(null); 
			setToolTipText(null);  
			setImageDescriptor(DownloadConstants.getIconBlank());
			setMenuCreator(this);
		}

		private void init() {
			fList = new ArrayList<Object>();
			setEnabled(fList.size() > 0);
		}

		public void dispose() {
			fList.clear();
			fList = null;
		}

		public Menu getMenu(Control parent) {
			if (fMenu != null) {
				fMenu.dispose();
			}
			String labelDefault = DownloadConstants.item_default_label_configuration;
			fMenu= new Menu(parent);
			fMenu.setVisible(false);
			return fMenu;
		}

		private void addActionToMenu(Menu parent, Action action) {
			ActionContributionItem item= new ActionContributionItem(action);
			item.fill(parent, -1);
		}

		public Menu getMenu(Menu parent) {
			return null;
		}

		class OtherFactoryAction extends Action {

			public OtherFactoryAction(String label, ImageDescriptor image) {
				super(label, AS_RADIO_BUTTON);
				setText(label);
				if (image != null) {
					setImageDescriptor(image);
				}
			}

			@Override
			public void run() {
			}

			@Override
			public void setToolTipText(String toolTipText) {
				super.setToolTipText(toolTipText);
			}
		}
	};

	/**
	 * This is a lazy thread which allows us to set a configuration
	 * @param fconfig
	 */
	public final void setConfiguration(final ConfigurationFactoryExtension fconfig, final boolean restore) {
		final UIJob job = new UIJob(DownloadConstants.progress_loading_configuration) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					Configuration[] configurations = fconfig.getConfigurations();
					monitor.beginTask("Loading configuration", 1 + configurations.length + 1 + 1 + 1 + 1 + 1);
					oeConfig.clear();
					Thread.sleep(10);
					monitor.worked(1);
					for (int i = 0; i < configurations.length; i++) {
						oeConfig.add(configurations[i]);
						Thread.sleep(10);
						monitor.worked(1);
					}
					tableviewer.setInput(oeConfig);
					Thread.sleep(10);
					monitor.worked(1);
					currentConfiguration = fconfig;
					Thread.sleep(10);
					monitor.worked(1);
					labelCurrentT.setText(fconfig.getLabel());labelCurrentT.redraw();
					Thread.sleep(10);
					labelCurrentI.setImage(fconfig.getImage() == null ? DownloadConstants.getIconCongigDescriptor().createImage() : fconfig.getImage().createImage());labelCurrentI.redraw();
					Thread.sleep(10);
					monitor.worked(1);
					//save view states
					saveSettingsConfiguration(DownloadConstants.keyCurrentConfiguration);
					Thread.sleep(10);
					monitor.worked(1);
					updateProfilTabFolder(restore);
					Thread.sleep(10);
					monitor.worked(1);
					Thread.sleep(10);
				} catch (Exception e) {
					return new Status(IStatus.ERROR, DownloadConstants.pluginCheckingID, e.getMessage());
				}
				finally {
					monitor.done();									
				}
				return new Status(IStatus.OK, DownloadConstants.pluginCheckingID, null);
			}
		};
		job.setPriority(Job.BUILD);
		try {
			window.run(true, false, new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor) {
					job.schedule();
				}
			});
		} catch (InvocationTargetException e) {
			DownloadConstants.log("Error loading configuration model.", e);
		} catch (InterruptedException e) {
			DownloadConstants.log("Error loading configuration model.", e);
		}
		return;
	}

	/**
	 * Clear all the items of Profil tabfolder
	 */
	private void clearItemsProfil() {
		profilFolder.removeSelectionListener(selectionListenerProfilTabFolder);
		CTabItem[] items = profilFolder.getItems();
		for (int i = 0; i < items.length; i++) {
			items[i].setData(null);
			items[i].setData("key", null);
			items[i].dispose();
			items[i] = null;
		}
		profilFolder.addSelectionListener(createSelectionListenerProfilTabFolder());
	}

	/**
	 * Add the selection listener on Profil tabfolder
	 */
	private SelectionListener createSelectionListenerProfilTabFolder() {
		selectionListenerProfilTabFolder = new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				java.util.List<Profil> listProfil = getModelProfilUsed(null);
				if (listProfil != null) {
					Profil profil = listProfil.get(0);
					selectItemProfilTabFolder(profil);
				}
				else {
					selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
				}
			}

			public void widgetSelected(SelectionEvent e) {
				CTabFolder tabfolder = (CTabFolder)e.getSource();
				CTabItem item = (CTabItem)tabfolder.getSelection();
				if (verbose) System.out.println(item.getText());
				tableviewer.refresh();
				computeProfil();
				saveSettingsConfiguration(DownloadConstants.keyProfil);
			}
		};
		return selectionListenerProfilTabFolder;
	}

	/**
	 * Add an item to Profil tabfolder
	 */
	private void createItemProfil(String name, String key) {
		if (profilFolder != null) {
			CTabItem item = new CTabItem(profilFolder, SWT.NONE);
			item.setText(name);
			item.setData(key);
		}
	}

	/**
	 * Add an item to Profil tabfolder
	 */
	private void createItemProfil(String name, Profil key) {
		if (profilFolder != null) {
			CTabItem item = new CTabItem(profilFolder, SWT.NONE);
			item.setText(name);
			item.setData("key", key);
		}
	}

	/**
	 * Compute profil(s)
	 */
	private boolean computeProfil() {
		Configuration configuration = getConfigItemFolderSelected();
		Profil profil = getProfilItemFolderSelected();
		ResourceContentProvider content = (ResourceContentProvider) tableviewer.getContentProvider();
		Object[] contents = content.getElements(tableviewer.getInput());
		//Object[] contents = tableviewer.getFilters()[0].filter(tableviewer, oeConfig, ((ResourceContentProvider)tableviewer.getContentProvider()).getElements(oeConfig));//optimization
		if (contents.length > 0) {
			boolean warn = false;
			boolean badJVM = false;
			boolean error = false;
			boolean simpleElementGroup = false;
			boolean sameOwner = false;
			for (int i = 0; i < contents.length; i++) {
				ConfigTableElement element = (ConfigTableElement)contents[i];
				java.util.List<Profil> list = element.getResourceProfilList();
				simpleElementGroup = (list == null && configuration == null) ? true : false;
				sameOwner = configuration == null ? false : configuration.equals(element.getResourceOwner());
				//outside | inside selected configuration without profil | inside selected configuration->profil 
				if (simpleElementGroup || (list == null && sameOwner) || (list != null && list.contains(profil))) {
					boolean exist = element.getResourceExist();
					boolean existIgnoreVersion = element.getResourceExistIgnoreVersion();
					boolean required = element.getResourceIsRequired();
					if (required && !existIgnoreVersion) 
						error = true; 
					if (required && existIgnoreVersion && !exist) {
						Version versionInstalled = element.getResourceVersionInstalled();
						if (versionInstalled != null) {
							Version versionRequired = element.getResourceVersion();
							if (versionInstalled.compareTo(versionRequired) > 0) {
								//more recent version
								warn = true;
							}
							else if (versionInstalled.compareTo(versionRequired) < 0) {
								//less recent version
								error = true;
							}
						}
						else
							//default : a different version is at least a warning
							warn = true;
					}
					if (element.getResourceIsJavaVersionGreaterToPlatform()) badJVM = true;
					if (error && badJVM) break;
				}
			}
			String text = "";
			if (error) text += DownloadConstants.message_PROFIL_RESOURCE_ERROR;
			if (badJVM) text += (text.equals("") ? "" : ", ") + DownloadConstants.message_PROFIL_JVM_ERROR;
			if (error | badJVM) { 
				labelI.setImage(CheckingIcons.get(DownloadConstants.errorTsk));
				labelT.setText(text);
				labelI.redraw();
				return false;
			}
			labelI.setImage(CheckingIcons.get(warn ? DownloadConstants.warningTsk : DownloadConstants.completeStatus));
			labelT.setText(warn ? DownloadConstants.message_PROFIL_WARNING : DownloadConstants.message_PROFIL_OK);
			labelI.redraw();
		}
		else {
			labelT.setText(DownloadConstants.message_NO_MODEL);
			labelI.setImage(null);
			labelI.redraw();
		}
		return true;
	}

	/**
	 * Method which gets the key profil item profilFolder selected
	 */
	private Profil getProfilItemFolderSelected() {
		if (profilFolder != null) {
			CTabItem item = (CTabItem)profilFolder.getSelection();
			return item == null ? null : item.getData("key") instanceof Profil ? (Profil)item.getData("key") : null;
		}
		return null;
	}

	/**
	 * Method which gets the key configuration item configFolder selected
	 */
	private Configuration getConfigItemFolderSelected() {
		if (configFolder != null) {
			CTabItem item = (CTabItem)configFolder.getSelection();
			return item == null ? null : item.getData("key") instanceof Configuration ? (Configuration)item.getData("key") : null;
		}
		return null;
	}

	/**
	 * Method which returns the profil list used
	 */
	private java.util.List<Profil> getModelProfilUsed(Configuration configuration) {
		java.util.List<Profil> profilUsed = null;
		if (oeConfig != null) {
			java.util.List<Configuration> listConfiguration = null;
			if (configuration == null) {
				listConfiguration = oeConfig; 
			}
			else {
				listConfiguration = new ArrayList<Configuration>();
				listConfiguration.add(configuration);
			}
			for (Iterator<Configuration> iter_c = listConfiguration.iterator(); iter_c.hasNext();) {
				Configuration config = (Configuration) iter_c.next();
				java.util.List<Profil> ownedProfil = (java.util.List<Profil>)config.getOwnedProfil();
				for (Iterator<Profil> iter_p = ownedProfil.iterator(); iter_p.hasNext();) {
					Profil profil = (Profil) iter_p.next();
					if (profil.getResource() != null) {
						if (!profil.getResource().isEmpty()) {
							if (profilUsed == null) profilUsed = new java.util.ArrayList<Profil>();
							profilUsed.add(profil);
						}
					}
				}
			}
		}
		return profilUsed;
	}

	/**
	 * Method which returns the configuration list used
	 */
	private java.util.List<Configuration> getModelConfigurationUsed() {
		java.util.List<Configuration> configUsed = null;
		if (oeConfig != null) {
			for (Iterator<Configuration> iter_c = oeConfig.iterator(); iter_c.hasNext();) {
				Configuration configuration = (Configuration) iter_c.next();
				if (configuration != null && !configuration.getNameID().trim().equals("")) {
					if (configUsed == null) configUsed = new java.util.ArrayList<Configuration>();
					configUsed.add(configuration);
				}
			}
		}
		return configUsed;
	}

	/**
	 * Method which selects the first TabItem of Profil with the key string parameter
	 */
	private void selectItemProfilTabFolder(String key) {
		if (key != null && !key.equals("")) {
			for (int iter = 0; iter < profilFolder.getItems().length; iter++) {
				CTabItem item = (CTabItem) profilFolder.getItems()[iter];
				String data = (String)item.getData();
				if (!item.isDisposed() && key.equals(data)) {
					profilFolder.setSelection(item);
					return;
				}
			}
		}
	}

	/**
	 * Method which selects the first TabItem of Configuration with the key string parameter
	 */
	private void selectItemConfigTabFolder(String key) {
		if (key != null && !key.equals("")) {
			for (int iter = 0; iter < configFolder.getItems().length; iter++) {
				CTabItem item = (CTabItem) configFolder.getItems()[iter];
				String data = (String)item.getData();
				if (key.equals(data)) {
					configFolder.setSelection(item);
					return;
				}
			}
		}
	}

	/**
	 * Method which selects the first TabItem of Profil with the key profil parameter
	 */
	@SuppressWarnings("unused")
	private void selectItemProfilTabFolder(Profil key) {
		if (key != null) {
			for (int iter = 0; iter < profilFolder.getItems().length; iter++) {
				CTabItem item = (CTabItem) profilFolder.getItems()[iter];
				Object object = item.getData("key");
				Profil profil = object == null ? null : object instanceof Profil ? (Profil)object : null;
				if (profil != null && key.equals(profil)) {
					profilFolder.setSelection(item);
					return;
				}
			}
			selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
		}
		else {
			selectItemProfilTabFolder(DownloadConstants.CTabItem_NONE);
		}
	}
	/**
	 * Method which returns the first configuration with the nameID as parameter
	 * @param nameID
	 */
	@SuppressWarnings("unused")
	private Configuration getConfiguration(String nameID) {
		if (oeConfig != null && oeConfig.size() > 0 && nameID != null && !nameID.trim().equals("")) {
			for (Iterator<Configuration> iterator = oeConfig.iterator(); iterator.hasNext();) {
				Configuration configuration = (Configuration) iterator.next();
				if (configuration.getNameID() != null && configuration.getNameID().equals(nameID))
					return configuration;
			}
		}
		return null;
	}

	/**
	 * Method which gets the key string (config.nameID + profil.nameID) item profilFolder selected
	 */
	@SuppressWarnings("unused")
	private String getProfilKeyItemFolderSelected() {
		if (profilFolder != null) {
			CTabItem item = (CTabItem)profilFolder.getSelection();
			return item == null ? null : item.getData() instanceof String ? (String)item.getData() : null;
		}
		return null;
	}

	/**
	 * Method which gets a key item profilFolder selected
	 */
	@SuppressWarnings("unused")
	private Object getProfilItemFolderSelected(String key) {
		if (profilFolder != null) {
			CTabItem item = (CTabItem)profilFolder.getSelection();
			return item == null ? null : item.getData(key);
		}
		return null;
	}

	/**
	 * Method which gets the key string (nameID) item configFolder selected
	 */
	@SuppressWarnings("unused")
	private String getConfigIDItemFolderSelected() {
		if (configFolder != null) {
			CTabItem item = (CTabItem)configFolder.getSelection();
			return item == null ? null : item.getData() instanceof String ? (String)item.getData() : null;
		}
		return null;
	}

	/**
	 * Method which gets a key item configFolder selected
	 */
	@SuppressWarnings("unused")
	private Object getConfigItemFolderSelected(String key) {
		if (configFolder != null) {
			CTabItem item = (CTabItem)configFolder.getSelection();
			return item == null ? null : item.getData(key);
		}
		return null;
	}

	/**
	 * Method which selects the first TabItem of Configuration with the key configuration parameter
	 */
	@SuppressWarnings("unused")
	private void selectItemConfigTabFolder(Configuration key) {
		if (key != null) {
			for (int iter = 0; iter < configFolder.getItems().length; iter++) {
				CTabItem item = (CTabItem) configFolder.getItems()[iter];
				Object object = item.getData("key");
				Configuration configuration = object == null ? null : object instanceof Configuration ? (Configuration)object : null;
				if (configuration != null && key.equals(configuration)) {
					configFolder.setSelection(item);
					return;
				}
			}
			selectItemConfigTabFolder(DownloadConstants.CTabItem_NONE);
		}
		else {
			selectItemConfigTabFolder(DownloadConstants.CTabItem_NONE);
		}
	}

	/**
	 * Display a message in a dialog box
	 * @param message
	 */
	@SuppressWarnings("unused")
	private void showMessage(String title, String message) {
		MessageDialog.openInformation(
				window.getShell(),
				title,
				message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		tableviewer.getControl().setFocus();
	}

	/**
	 * Method init
	 */
	public void init(IViewSite site) {
		try {
			super.init(site);
			window = window == null ? window = site.getWorkbenchWindow() : window;
		} catch (PartInitException e) {
			DownloadConstants.log("Error during the initialization of the checking view.", e);
			return;
		}
	}

	/**
	 * Method init + memento
	 */
	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		currentConfiguration = null;
		try {
			Path pathName = getPathConfiguration(new Path(DownloadConstants.configDirectorySaveSettings));
			if (pathName != null) {
				pathName = (Path) pathName.append(DownloadConstants.configSettings);
				if (settings == null) {
					settings = new DialogSettings(DownloadConstants.settingsName); 
				}
				if (settings.getSections().length == 0) {

					settings.addNewSection(DownloadConstants.keyCurrentConfiguration);
				}
				if (pathName.toFile().exists()) {
					settings.load(pathName.toPortableString());
				}
			}
		} catch (Exception e) {
			DownloadConstants.log("Error init configuration settings.", e);
			return;
		}
		IDialogSettings section = settings == null ? null : settings.getSection(DownloadConstants.keyCurrentConfiguration); 
		if (section != null) {
			String pluginID = section.get(DownloadConstants.extensionContributorName);
			pluginID = pluginID == null ? null : pluginID.equals("") ? null : pluginID; 
			String fragmentID = section.get(DownloadConstants.extensionRegistryContributorName);
			fragmentID = fragmentID == null ? null : fragmentID.equals("") ? null : fragmentID;
			String id = section.get(DownloadConstants.extensionID);
			id = id == null ? null : id.equals("") ? null : id;
			String label = section.get(DownloadConstants.extensionLabel);
			label = label == null ? null : label.equals("") ? null : label;
			String model = section.get(DownloadConstants.extensionModel);
			model = model == null ? null : model.equals("") ? null : model;
			String icon = section.get(DownloadConstants.extensionIcon);
			icon = icon == null ? null : icon.equals("") ? null : icon;
			String licensePath = section.get(DownloadConstants.extensionLicensePath);
			licensePath = licensePath == null ? null : licensePath.equals("") ? null : licensePath;
			IExtensionRegistry registry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = registry.getExtensionPoint(DownloadConstants.getExtensionPointIDModelConfiguration());
			if (extensionPoint != null) {
				IExtension[] extensions = extensionPoint.getExtensions();
				for (int i = 0; i < extensions.length; i++) {
					IConfigurationElement[] elements = extensions[i].getConfigurationElements();
					for (int j = 0; j < elements.length; j++) {
						IConfigurationElement element = elements[j];  
						String epluginID = element.getContributor().getName();
						String eid = element.getAttribute(DownloadConstants.extensionID);
						String elabel = element.getAttribute(DownloadConstants.extensionLabel);
						String emodel = element.getAttribute(DownloadConstants.extensionModel);
						String eicon = element.getAttribute(DownloadConstants.extensionIcon);
						String elicensePath = element.getAttribute(DownloadConstants.extensionLicensePath);
						if (pluginID == null || !pluginID.equals(epluginID))
							continue;
						if (fragmentID != null && 
								element.getContributor() instanceof RegistryContributor &&
								!fragmentID.equals(((RegistryContributor)element.getContributor()).getActualName()))
							continue;
						if (eid == null || elabel == null || emodel == null)
							continue;
						if (!id.equals(eid))
							continue;
						if (!label.equals(elabel))
							continue;
						if (!model.equals(emodel))
							continue;
						if (icon != null && eicon != null && !icon.equals(eicon))
							continue;
						if (licensePath != null && elicensePath != null && !licensePath.equals(elicensePath))
							continue;
						currentConfiguration = new ConfigurationFactoryExtension(element);
						return;
					}
				}
			}
		}
	}

	/**
	 *  Method save view states
	 */
	public void saveSettingsConfiguration(String key) {
		if (tableviewer != null && currentConfiguration != null) {
			if (settings == null) {
				settings = new DialogSettings(DownloadConstants.settingsName); 
			}
			if (settings.getSections().length == 0) {
				settings.addNewSection(DownloadConstants.keyCurrentConfiguration);
			}
			IDialogSettings section = settings == null ? null : settings.getSection(DownloadConstants.keyCurrentConfiguration); 
			if (section != null) {
				if (key.equals(DownloadConstants.keyCurrentConfiguration)) {
					section.put(DownloadConstants.extensionContributorName, currentConfiguration.getPluginId());
					section.put(DownloadConstants.extensionRegistryContributorName, currentConfiguration.getFragmentId());
					section.put(DownloadConstants.extensionID, currentConfiguration.getLocalId());
					section.put(DownloadConstants.extensionLabel, currentConfiguration.getLabel());
					section.put(DownloadConstants.extensionModel, currentConfiguration.getModel());
					section.put(DownloadConstants.extensionIcon, currentConfiguration.getIcon());
					section.put(DownloadConstants.extensionLicensePath, currentConfiguration.getLicensePath());
				}
				if (key.equals(DownloadConstants.keyProfil)) {
					section.put(DownloadConstants.keyProfil, getProfilItemFolderSelected() == null ? null : getProfilItemFolderSelected().getNameID());
				}
				try {
					Path pathName = getPathConfiguration(new Path(DownloadConstants.configDirectorySaveSettings));
					if (pathName != null) {
						pathName = (Path) pathName.append(DownloadConstants.configSettings);
						settings.save(pathName.toPortableString());
					}
				} catch (Exception e) {
					DownloadConstants.log("Error saving configuration settings.", e);
				}
			}
		}
	}

	/**
	 * Method dispose
	 */
	@Override
	public void dispose() {
		//save all configurations
		saveTableContent();
		super.dispose();
	}
}; 