package org.openembedd.checker.view;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.openembedd.checker.modelConfig.Category;
import org.openembedd.checker.view.ResourceContentProvider.ConfigTableElement;

/**
 * INRIA/IRISA 
 * Copyright(c) 2007 OpenEmbeDD project
 * @author openembedd
 */
public class ResourceSorter extends ViewerSorter {

	@Override
	public int category(Object element) {
		if (element instanceof ConfigTableElement && element!= null) {
			ConfigTableElement configElement = (ConfigTableElement)element;
			Category category = configElement.getResourceCategory();
			if (category != null) {
				return category.getNameID().compareTo(" ");
			}
		}
		return 0;
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		int cat1 = category(e1);
		int cat2 = category(e2);

		if (cat1 != cat2) {
			return cat1 - cat2;
		}

		String name1 = null;
		String name2 = null;

		if (e1 instanceof ConfigTableElement && e2 instanceof ConfigTableElement) {
			ConfigTableElement configTableElement1 = (ConfigTableElement)e1;
			ConfigTableElement configTableElement2 = (ConfigTableElement)e2;
			name1 = configTableElement1.getTextResource();
			name2 = configTableElement2.getTextResource();
		}
		else {
			name1 = e1.toString();
			name2 = e2.toString();
		}
		if (name1 == null) {
			name1 = "";//$NON-NLS-1$
		}
		if (name2 == null) {
			name2 = "";//$NON-NLS-1$
		}

		// use the comparator to compare the strings
		return getComparator().compare(name1, name2);
	}

};
