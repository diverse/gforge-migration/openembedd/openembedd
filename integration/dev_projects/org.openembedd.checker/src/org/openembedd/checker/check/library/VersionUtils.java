package org.openembedd.checker.check.library;

import org.osgi.framework.Version;

public final class VersionUtils {

	/**
	 * Class utility getLongName2VersionedIdentifier
	 * INRIA/IRISA 
	 * Copyright(c) 2007 OpenEmbeDD project
	 * @author openembedd 
	 * 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	public static final org.eclipse.update.internal.configurator.VersionedIdentifier getLongName2VersionedIdentifier(String featureLongName) {
		if (featureLongName!= null && !featureLongName.equals("")) {
			boolean terminate = false;
			String name = featureLongName, rest = "";
			int indexOf_ = name.indexOf("_");
			name = indexOf_ > 0 ? featureLongName.substring(0, indexOf_) : featureLongName;
			rest = indexOf_ > 0 ? featureLongName.substring(indexOf_ + 1, featureLongName.length()) : "";
			while (!terminate && !rest.equals(""))
				try {new Version(rest); terminate = true;} 
			catch (IllegalArgumentException e) {
				indexOf_ = indexOf_ + 1 + rest.indexOf("_");
				name = indexOf_ > 0 ? featureLongName.substring(0, indexOf_) : featureLongName;
				rest = indexOf_ > 0 ? featureLongName.substring(indexOf_ + 1, featureLongName.length()) : "";
			}
			return new org.eclipse.update.internal.configurator.VersionedIdentifier(name, rest);	
		}
		return null;
	}

};
