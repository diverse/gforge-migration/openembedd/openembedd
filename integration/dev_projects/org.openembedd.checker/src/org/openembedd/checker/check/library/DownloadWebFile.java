package org.openembedd.checker.check.library;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/*********************************************************************************************
 * INRIA/IRISA 
 * Copyright(c) 2006 - 2007 OpenEmbeDD project
 * @author openembedd
 ********************************************************************************************* 
 * DownloadWebFile is a class which allows to dowload additionnals libraries from the web
 * with http protocol and add them to complete directories /lib plugin and another ones.
 * In a first stage we create the URLConnection object from the URL path in entry,
 * in a second stage we download the source file and store it in a temporary folder from which
 * we get and copy the file into the target directory
 *--------------------------------------------------------------------------------------------
 * Last revised : august 2007 
 **********************************************************************************************/
public class DownloadWebFile {

	/**
	 * URL and path variables
	 **/
	private String nameURL;						//URL location (ex: "http://archive.eclipse.org/eclipse")
	private String nameWebFile;					//name web file (ex: "downloads/drops/R-3.1.1-200509290840/eclipse-SDK-3.1.1-linux-gtk.tar.gz")
	private String nameFileTarget;				//name of the final file stored
	private String pathFileTarget; 				//path (relative or absolute) of the final file stored
	private URL fileURL;						//URL of the file location
	private URLConnection connectionURL;		//net connection for URL

	/**
	 * entry variables 
	 */
	private String entryName;  					//optionnal, used to find an entry in a zip or jar file (ex: "/eclipse/plugins/org.eclipse.emf.ecore.sdo.edit_2.3.0.v200612211251.jar")

	/**
	 * synchronized progress variables
	 */
	@SuppressWarnings("unused")
	private boolean inProgress = false;
	@SuppressWarnings("unused")
	private int progress = 0;

	/**
	 * stored variables
	 */

	/**
	 * other variables
	 */
	private static enum protocol {HTTP, FTP, FILE};
	private protocol typeConnection = protocol.HTTP;
	private static String[] lsZip = {".jar", ".gz", ".zip", ".tar.gz", ".tar", ".war", ".bz2", ".ice", ".rar"};
	private boolean customizeWayToURL;		//set true to enter in the 

	/**
	 * debug variables
	 */
	private boolean debug = false;			//set true to get message in the console
	private boolean verbose = false;		//set true to get message in the console

	/**
	 * constants
	 */
	private final static int LENGTH_BUFFER = 512 * 1024;
	private final static int LENGTH_SMALL_BUFFER = 128;
	/**
	 * the constructor
	 *
	 */
	public DownloadWebFile() {
		this.nameURL = "";		
		this.nameWebFile = "";
		this.pathFileTarget = "";
		this.nameFileTarget = "";
		this.entryName = "";
		this.fileURL = null;
		this.connectionURL = null;
		customizeWayToURL = true;
	}

	/**
	 * 
	 * @param nameURL
	 * @param nameWebFile
	 */
	public void setNames(String nameURL, String nameWebFile){
		setNameURL(nameURL);		
		setNameWebFile(nameWebFile);
	}

	/**
	 * 
	 * @param nameURL
	 * @param nameWebFile
	 * @param pathFileTarget
	 */
	public void setNames(String nameURL, String nameWebFile, String pathFileTarget){
		setNames(nameURL, nameWebFile);		
		setPathFileTarget(pathFileTarget);
	}

	/**
	 * 
	 * @param nameURL
	 * @param nameWebFile
	 * @param pathFileTarget
	 * @param nameFileTarget
	 */
	public void setNames(String nameURL, String nameWebFile, String pathFileTarget, String nameFileTarget){
		setNames(nameURL, nameWebFile, pathFileTarget);		
		setNameFileTarget(nameFileTarget);
	}

	/**
	 * @param nameURL
	 */
	public void setNameURL (String nameURL) {
		this.nameURL = 	nameURL;
		if (connectionURL != null) disconnect();
	}

	/**
	 * @param nameWebFile
	 */	
	public void setNameWebFile (String nameWebFile) {
		this.nameWebFile = 	nameWebFile;		
		if (connectionURL != null) disconnect();
	}

	/**
	 * @param entry
	 */
	public void setEntryName(String entry) {
		if (entry != null) {
			entryName = entry;
		}
	}

	/**
	 * @param pathFileTarget
	 */
	public void setPathFileTarget (String pathFileTarget) {
		this.pathFileTarget = 	pathFileTarget;		
	}

	/**
	 * @param nameFileTarget
	 */
	public void setNameFileTarget (String nameFileTarget) {
		this.nameFileTarget = 	nameFileTarget;		
	}

	/**
	 * Method to set the boolean customizeWayToURL
	 * customizeWayToURL = true  => method wayToURL is used
	 * customizeWayToURL = false => method wayToURL is not used
	 * @param customize
	 */
	@SuppressWarnings("unused")
	private void setCustomizeWayToURL (boolean customize) {
		this.customizeWayToURL = customize;
	}

	/**
	 * Note : merge path, this isn't an exhaustive algorithm, but only a method which combine 2 path in entry, 
	 * where the first one is the file's path (absolute) and the second one is the name's file (relative)
	 * @param spath
	 * @param sfile
	 * @return
	 */
	private static String composePathFile (String spath, String sfile) {

		String spathfile = "";
		String end = "", begin  = "", common = "", rest = "", nameFile = "";

		begin  = spath;
		end = sfile;

		if ( begin.endsWith(File.separator ) ) begin = begin.substring(0, begin.lastIndexOf(File.separator));
		if ( end.startsWith(File.separator) ) end = end.substring(end.indexOf(File.separator) + 1, end.length());
		if ( end.indexOf(File.separator) == -1 )	{ 
			if ( begin.endsWith(File.separator + end) ) end = "";
		}
		else {
			if ( begin.endsWith(File.separator + end) ) end = "";
			else {
				nameFile = end.substring(end.lastIndexOf(File.separator) + 1, end.length());
				end = end.substring(0, end.lastIndexOf(File.separator));
				common = end; rest = "";
				while ( !begin.endsWith(File.separator + common) && common.lastIndexOf(File.separator) != -1 ) {
					rest = common.substring(common.lastIndexOf(File.separator), common.length()) + rest;
					common = common.substring(0, common.lastIndexOf(File.separator));
				}
				if ( !begin.endsWith(File.separator + common) ) rest = common + rest;
				if ( !rest.equals("") ) 
					end = rest + File.separator + nameFile;
				else 
					end = nameFile;
			}
		}
		spathfile = begin ;
		if ( !end.equalsIgnoreCase("") ) spathfile += File.separator + end;

		return spathfile;
	}

	/**
	 * 
	 * Method to create the target directory
	 */
	private boolean mkdirTarget() {

		String pathTarget = composePathFile(pathFileTarget, nameFileTarget);
		if (pathTarget.lastIndexOf(File.separator) > 0) { pathTarget = pathTarget.substring(0, pathTarget.lastIndexOf(File.separator)); }
		File directory = new File(pathTarget);
		directory = directory.getAbsoluteFile();
		if (!directory.exists()) {
			return directory.mkdirs();
		}

		return true;
	}

	/**
	 * 
	 * @return true if all required parametres are setting, false else
	 */
	public boolean accept() {

		if (nameURL == null || nameWebFile == null || entryName == null)
			return false;
		if (pathFileTarget == null || nameFileTarget == null)
			return false;

		//empty URL
		if (nameURL.trim().equalsIgnoreCase("")) {
			if (verbose) System.out.println("URL name is a empty string");
			return false;
		}
		//URL not http
		if (nameURL.indexOf("http://") == -1 && ( typeConnection == protocol.HTTP ) ) {
			if (verbose) System.out.println("URL protocol is not a HTTP protocol : " + nameURL);
			return false;
		}
		//target on read-only
		String pathTarget = composePathFile(pathFileTarget, nameFileTarget);
		if (pathTarget.lastIndexOf(File.separator) > 0) { pathTarget = pathTarget.substring(0, pathTarget.lastIndexOf(File.separator)); }
		File tempfile = new File(pathTarget);
		tempfile = tempfile.getAbsoluteFile();
		if (!tempfile.exists()) {
			//place on the first parent directory which exists
			while (!tempfile.exists() && !tempfile.getParentFile().exists()) tempfile = tempfile.getParentFile();
			//verify permission to create directory in the root parent
			if (!tempfile.mkdirs()) {
				if (verbose) System.out.println("impossible to create the target file : system file target is maybe on read-only");
				return false;
			}
			else {
				//cancel mkdir
				tempfile.delete();
			}
		}

		return true;
	}


	/**
	 * Clear the url
	 *
	 */ 
	public void clear() {
		disconnect();
		fileURL = null;
		setNames("", "", "", "");
		System.gc();
	}

	/**
	 * Disconnect the url
	 *
	 */
	public void disconnect() {
		connectionURL = null;
		inProgress = false;
		progress = 0;
		System.gc();
	}

	/**
	 * Method to extract the entry from the url or nameWebFile
	 * this use come from the factorization
	 * @return the entry
	 */
	private String extractEntry() {

		String entry = "", url = "", slash = "/";
		boolean zipInEnd = false, zipIn = false;
		int indzip = 0;

		if (fileURL == null || entryName == null) 
			return null;

		url = fileURL.toString();
		for (String s : lsZip) { if (url.endsWith(s)) { zipInEnd = true; break; } }
		for (String s : lsZip) { if ((indzip = url.lastIndexOf(s)) > 0) { zipIn = true; break; } }

		/////TODO maybe code has to re-examine TODO//////

		if (!entryName.trim().equals(""))
			entry = entryName;		
		//a good thing will be to add the part of nameWebFile which is not in url
		else {
			//else we choose to take the last chunk of url after a .zip or .jar, ...
			if (zipInEnd)
				if (!url.endsWith(nameWebFile)) {
					for (String s : lsZip) { if ((indzip = nameWebFile.lastIndexOf(s)) > 0) { break; } }
					entry = nameWebFile.substring(nameWebFile.indexOf(slash, indzip) + 1);
				}
			if (!zipInEnd && zipIn)
				entry = url.substring(url.indexOf(slash, indzip) + 1);	
		}
		if (entry.startsWith(slash)) entry = entry.substring(1, entry.length());
		if (entry.endsWith(slash)) entry = entry.substring(0, entry.length() - 1);

		/////TODO maybe code has to re-examine TODO//////

		return entry;
	}

	/**
	 * 
	 * Return a unique numeric string
	 */
	public static String unique() {

		Calendar cal = Calendar.getInstance();
		String unique = "";
		try {
			Thread.sleep(100);
			unique = String.valueOf(cal.get(Calendar.YEAR)) + 
			String.valueOf(cal.get(Calendar.MONTH)) + 
			String.valueOf(cal.get(Calendar.HOUR_OF_DAY)) +
			String.valueOf(cal.get(Calendar.MINUTE)) +
			String.valueOf(cal.get(Calendar.SECOND)) +
			String.valueOf(cal.get(Calendar.MILLISECOND));
			return unique;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Copy a file
	 * 
	 * @param src
	 * @param dest
	 */
	private static boolean cpFile(File src, File dest) throws IOException {

		if (src == null || dest == null)
			return false;

		if (!src.exists()) throw 
		new IOException("File not found '" + src.getAbsolutePath() + "'");

		if (!dest.getParentFile().exists() || !dest.getParentFile().canWrite()) throw
		new IOException("Target directory not exist or is on read-only'" + dest.getParentFile().getAbsolutePath() + "'");

		if (dest.exists() && !dest.canWrite()) throw
		new IOException("Target file already exist and is on read-only'" + dest.getAbsolutePath() + "'");

		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(src));

		byte[] read = new byte[LENGTH_SMALL_BUFFER];
		int len = LENGTH_SMALL_BUFFER;
		while ((len = in.read(read)) > 0)
			out.write(read, 0, len);
		out.flush();
		out.close();
		in.close();

		return true;
	}

	/**
	 * Method to delete a temporary file
	 * @param tempfile
	 * @return
	 */
	private static boolean deleteTempFile(File tempfile) {
		boolean deleted = false;
		if (tempfile == null)
			return false;

		//delete temp file with its directory
		if (tempfile.exists() && tempfile.isFile())
			deleted = tempfile.delete();
		if (tempfile.getParentFile().exists() && tempfile.getParentFile().isDirectory())
			deleted = tempfile.getParentFile().delete();
		return deleted;
	}

	/**
	 * Method statis return the path of user temporary file
	 */
	public static String getTempPath() {
		String tempPath = "";
		if (System.getProperties().getProperty("os.name").trim().toLowerCase().indexOf("windows") >= 0)
			tempPath = System.getProperty("java.io.tmpdir");
		else if (System.getProperties().getProperty("os.name").trim().toLowerCase().indexOf("linux") >= 0)
			tempPath = System.getProperties().getProperty("user.home");
		else
			tempPath = "";
		return tempPath;
	}

	/**
	 * Method to download a file from a jar
	 * 
	 * TODO : multiple target file in different path
	 * TODO : extract and improve code control
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean getWebFileFromJar() throws Exception {

		boolean getFileComplete = false;
		String jarNameEntry = "", tempFile = "";
		int lenTotal_ = 0;
		File outputFile = null;
		FileOutputStream output = null;
		JarInputStream inj = null;
		String directoryOSTemp = "";

		//test connection
		if (connectionURL == null) throw new Exception("no connection for the URL");

		//set up the entry for the .jar file
		jarNameEntry = extractEntry();
		if ( jarNameEntry == null | jarNameEntry.trim().equalsIgnoreCase("") ) {
			throw new Exception("no solve entry for the jar file");
		}
		//temp file
		if ((tempFile = unique()) == null) {
			throw new Exception("impossible to get a unique number");
		}
		//temp directory
		directoryOSTemp = getTempPath();

		try { if (!(new File(directoryOSTemp + tempFile).mkdirs())) throw new Exception("impossible to create temp directory"); }
		catch (Exception e) {
			throw e;
		}
		tempFile = directoryOSTemp + tempFile + File.separator + nameFileTarget;

		//get connection
		HttpURLConnection connectionHttpHost = (HttpURLConnection) connectionURL;
		if (verbose) System.out.println("------------------>" + connectionHttpHost.getContentType());
		//copy the file in the final directory
		try {
			//output
			outputFile = new File(tempFile);
			output = new FileOutputStream(outputFile);

			//input
			inj = new JarInputStream(((URLConnection)connectionHttpHost).getInputStream());

			int len_ = 0;
			byte buffer_[] = new byte[LENGTH_BUFFER];

			//read and check
			Manifest mf = inj.getManifest();
			if ((mf != null) && jarNameEntry.endsWith("MANIFEST.MF")) {
				getFileComplete = true;
				mf.write(output);
			}
			else {
				JarEntry enj;
				while ((enj = inj.getNextJarEntry()) != null  && !getFileComplete)  {
					if (verbose) System.out.println(enj.getName());
					if (!enj.isDirectory()) {
						/////TODO < manage the multiple case > TODO/////
						if (enj.getName().endsWith(jarNameEntry)) {
							getFileComplete = true;
							while ((len_ = inj.read(buffer_)) != -1) {
								output.write(buffer_, 0, len_);
								if (verbose) System.out.print(enj.toString() + "     ");
								if (verbose) System.out.println(len_);
								lenTotal_ += len_;
							}
						}
						/////TODO < manage the multiple case > TODO/////
					}
				}	
			}
			if (verbose) System.out.println("total = " + lenTotal_);
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			//close stream
			if (output != null) { output.flush(); output.close(); }
			if (inj != null) { inj.closeEntry(); inj.close(); }

			//copy temp to target
			File temp = new File(tempFile);
			if (getFileComplete) {
				File file = new File(composePathFile(pathFileTarget, nameFileTarget));
				try { if (mkdirTarget()) getFileComplete = cpFile(temp, file); } 
				catch (Exception e) { throw e; }
			}

			//delete temp file
			deleteTempFile(temp);
		}

		if (verbose) System.out.println("end of getWebFileFromJar");
		return getFileComplete;
	}

	/**
	 * Method to download a file from a zip
	 * 
	 * TODO : multiple target file in different path
	 * TODO : extract and improve code control
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean getWebFileFromZip() throws Exception {

		boolean getFileComplete = false;
		String zipNameEntry = "", tempFile = "";
		int lenTotal_ = 0;
		File outputFile = null;
		FileOutputStream output = null;
		ZipInputStream inz = null;
		String directoryOSTemp = "";

		//test connection
		if (connectionURL == null) throw new Exception("no connection for the URL");

		//set up the entry for the .jar file
		zipNameEntry = extractEntry();
		if ( zipNameEntry == null | zipNameEntry.trim().equalsIgnoreCase("") ) {
			throw new Exception("no solve entry for the zip file");
		}
		//temp file
		if ((tempFile = unique()) == null) {
			throw new Exception("impossible to get a unique number");
		}
		//temp directory
		directoryOSTemp = getTempPath();
		
		try { if (!(new File(directoryOSTemp + tempFile).mkdirs())) throw new Exception("impossible to create temp directory"); }
		catch (Exception e) {
			throw e;
		}
		tempFile = directoryOSTemp + tempFile + File.separator + nameFileTarget;

		//get connection
		HttpURLConnection connectionHttpHost = (HttpURLConnection) connectionURL;
		if (verbose) System.out.println("------------------>" + connectionHttpHost.getContentType());
		//copy the file in the final directory
		try {
			//ouput
			outputFile = new File(tempFile);
			output = new FileOutputStream(outputFile);

			//input
			inz = new ZipInputStream(((URLConnection)connectionHttpHost).getInputStream());

			int len_ = 0;
			byte buffer_[] = new byte[LENGTH_BUFFER];

			ZipEntry enz;

			//read and check
			while ((enz = inz.getNextEntry()) != null && !getFileComplete)  {
				if (verbose) System.out.println(enz.getName());
				if (!enz.isDirectory()) {
					/////TODO < manage the multiple case > TODO/////
					if (enz.getName().endsWith(zipNameEntry)) {
						getFileComplete = true;
						while ((len_ = inz.read(buffer_)) != -1) {
							output.write(buffer_, 0, len_);
							if (verbose) System.out.print(enz.toString() + "     ");
							if (verbose) System.out.println(len_);
							lenTotal_ += len_;
						}
					}
					/////TODO < manage the multiple case > TODO/////
				}
			}
			if (verbose) System.out.println("total = " + lenTotal_);
		}
		catch (Exception e) {
			throw e;
		}
		finally {

			//close stream
			if (output != null) { output.flush(); output.close(); }
			if (inz != null) { inz.closeEntry(); inz.close();}

			//copy temp to target
			File temp = new File(tempFile);
			if (getFileComplete) {
				File file = new File(composePathFile(pathFileTarget, nameFileTarget));
				try { if (mkdirTarget()) getFileComplete = cpFile(temp, file); } 
				catch (Exception e) { throw e; }
			}

			//delete temp file
			deleteTempFile(temp);
		}

		if (verbose) System.out.println("end of getWebFileFromZip");
		return getFileComplete;
	}

	/**
	 * Method to download a simple file
	 * 
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean getWebFile() throws Exception {

		boolean getFileComplete = false;
		String tempFile = "";
		int lenTotal = 0;
		File outputFile = null;
		FileOutputStream output = null;
		InputStream input = null;
		BufferedInputStream inputs = null;
		String directoryOSTemp = "";

		//test connection
		if (connectionURL == null) throw new Exception("no connection for the URL");

		//temp directory
		directoryOSTemp = getTempPath();

		//temp file
		if ((tempFile = unique()) == null) {
			throw new Exception("impossible to get a unique number");
		}
		try { if (!(new File(directoryOSTemp + tempFile).mkdirs())) throw new Exception("impossible to create temp directory"); }
		catch (Exception e) {
			throw e;
		}

		tempFile = directoryOSTemp + tempFile + File.separator + nameFileTarget;

		//get connection
		HttpURLConnection connectionHttpHost = (HttpURLConnection) connectionURL;

		//copy the file in the final directory
		try {
			/*********************************************************
			 *this code run right to download .jar, .tar.gz, .zip, ...
			 * Note: not running for file in zip
			 *
			/********************************************************/
			//output
			outputFile = new File(tempFile);
			output = new FileOutputStream(outputFile);

			//input
			input = connectionHttpHost.getInputStream();
			inputs = new BufferedInputStream(input);

			byte buffer[] = new byte[LENGTH_BUFFER];
			int len = 0;

			//read
			try {
				while((len = inputs.read(buffer)) != -1) {
					if (debug) System.out.println(len);
					lenTotal += len;
					output.write(buffer, 0, len);
				}
				if (lenTotal > 0) getFileComplete = true;
				if (debug) System.out.println("total = " + lenTotal);
			}
			catch (Exception e) {
				throw e;
			}
			finally {

			}
			if (debug) System.out.println("total = " + lenTotal);
			/********************************************************/  
		}
		catch (Exception e) {
			throw e;
		}
		finally {

			//close stream
			if (output != null) { output.flush(); output.close(); }
			if (inputs != null) { inputs.close(); }
			if (input != null) { input.close(); }

			//copy temp to target
			File temp = new File(tempFile);
			if (getFileComplete) {
				File file = new File(composePathFile(pathFileTarget, nameFileTarget));
				try { if (mkdirTarget()) getFileComplete = cpFile(temp, file); } 
				catch (Exception e) { deleteTempFile(temp); throw e; }
			}

			//delete temp file
			deleteTempFile(temp);
		}

		if (verbose) System.out.println("end of getWebFile");
		return getFileComplete;
	}

	/**
	 * Method to manage the three names : url, file and entry by using factorization
	 * 
	 * to add customization for the way to URL
	 * @return
	 */
	private String waytoURL(String sURL, String sFile, String sEntry) {

		boolean zipInEndPathURL = false, zipInPathURL = false, zipInEndPathFile = false, zipInPathFile = false;
		String swayToURL = "", slash = "/";
		int indzipFile = 0, indzipPath = 0;

		//verify if we have to find one file in an another one compressed
		for (String s : lsZip) { if (sURL.endsWith(s)) { zipInEndPathURL = true; break; } }
		for (String s : lsZip) { if ((indzipPath = sURL.indexOf(s)) > 0) { zipInPathURL = true; break; } }
		for (String s : lsZip) { if (sFile.endsWith(s)) { zipInEndPathFile = true; break; } }
		for (String s : lsZip) { if ((indzipFile = sFile.indexOf(s)) > 0) { zipInPathFile = true; break; } }

		//case where the entry is stored in the file => we extract it
		if (zipInPathFile && !zipInEndPathFile && sFile.indexOf(slash, indzipFile) >= 0) {
			sEntry = sEntry.trim().equals("") ? sFile.substring(sFile.indexOf(slash, indzipFile) + 1) : sFile.substring(sFile.indexOf(slash, indzipFile) + 1) + slash + sEntry;
			sFile = sFile.substring(0, sFile.indexOf(slash, indzipFile));
		}
		if (zipInPathURL && !zipInEndPathURL && sURL.indexOf(slash, indzipPath) >= 0) {
			sFile = sFile.trim().equals("") ? sURL.substring(sURL.indexOf(slash, indzipPath) + 1) : sURL.substring(sURL.indexOf(slash, indzipPath) + 1) + slash + sFile;
			sURL = sURL.substring(0, sURL.indexOf(slash, indzipPath));
		}

		//TODO: here you can code your own rule for way to URL 
		//case file or entry in .zip
		if (zipInEndPathURL) {
			swayToURL = sURL.endsWith(".jar") ? "jar:" + sURL + "!/" : sURL;  
		}
		else if (!zipInPathURL && zipInEndPathFile && !sEntry.equals("")) {
			swayToURL = sFile.endsWith(".jar") ? "jar:" + sURL + slash + sFile + "!/" : sURL + slash + sFile;
		}
		else if ((zipInPathURL && !sFile.trim().equals("") && !sURL.endsWith(".jar"))) {
			swayToURL = sURL + slash + sFile;
		}
		//case file or entry in .jar
		else if ((zipInPathURL && sURL.endsWith(".jar") && !sFile.trim().equals(""))) {
			swayToURL = "jar:" + sURL + slash + sFile + "!/";
		}
		//simple case file without entry
		else if (  zipInPathURL || 
				(!zipInPathURL && !zipInPathFile) ||
				(!zipInPathURL && zipInPathFile && !sEntry.trim().equals(""))) {
			if (!sFile.trim().equals(""))
				swayToURL = sURL + slash + sFile;
			else
				swayToURL = sURL;
		}
		else {
			swayToURL = sURL + slash + sFile;
		}

		//TODO: here you can code your own rule for way to URL 

		return swayToURL;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static String cutSepEdges(String s) {
		if (s != null) {
			String slash = "/", backslash = "\\", rc = s;
			if (rc.startsWith(slash) || rc.startsWith(backslash)) rc = rc.substring(1, rc.length());
			if (rc.endsWith(slash) || rc.endsWith(backslash)) rc = rc.substring(0, rc.length() - 1);
			return rc;
		}
		return null;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static String cutSepEnd(String s) {
		if (s != null) {
			String slash = "/", backslash = "\\", rc = s;
			if (rc.endsWith(slash) || rc.endsWith(backslash)) rc = rc.substring(0, rc.length() - 1);
			return rc;
		}
		return null;
	}

	
	/**
	 * Method connect
	 * 
	 * @return true if connected false else
	 * @throws
	 */
	public boolean connect() throws Exception {

		boolean connected = true;
		String sURL = "", sFile = "", sEntry = "", slash = "/";

//		//basic control
//		if (!accept()) {
//		return false;
//		}

		//URL format
		sURL = nameURL.substring(nameURL.indexOf("http://"), nameURL.length());
		sURL.replaceAll("!/", "");
		sFile = nameWebFile;
		sEntry = entryName;
		//retrieve the slash "/"
		sURL = cutSepEnd(sURL);
//		if (sURL.endsWith(slash)) sURL = sURL.substring(0, sURL.length() - 1);
//		if (sFile.startsWith(slash)) sFile = sFile.substring(1, sFile.length());
//		if (sFile.endsWith(slash)) sFile = sFile.substring(0, sFile.length() - 1);
		sFile = cutSepEdges(sFile);
//		if (sEntry.startsWith(slash)) sEntry = sEntry.substring(1, sEntry.length());
//		if (sEntry.endsWith(slash)) sEntry = sEntry.substring(0, sEntry.length() - 1);
		sEntry = cutSepEdges(sEntry);
		//factorization
		if (!sEntry.trim().equals("")) {
			if (sFile.endsWith(sEntry)) sFile = sFile.substring(0, sFile.indexOf(sEntry));
			sFile = cutSepEdges(sFile);
		}
		if (!sFile.trim().equals("")) {
			if (sURL.endsWith(sFile)) sURL = sURL.substring(0, sURL.indexOf(sFile));
			sURL = cutSepEnd(sURL);
		}

		//TODO : call wayToURL() method to customize
		//if you want customize more the way to URL
		String swayToURL = customizeWayToURL ? waytoURL(sURL, sFile, sEntry) : sURL + slash + sFile;
		//TODO : call wayToURL() method to customize

		//way to URL object
		try { 
			//case file or entry in .zip
			if (swayToURL != null && !swayToURL.trim().equals("")) {
				fileURL = new URL(swayToURL.replaceAll(" ", "%20"));
			}
			//all other case not managed
			else {
				MalformedURLException e = new MalformedURLException("URL : case not managed " + nameURL + "; "  + nameWebFile);
				connected = false;
				throw e;
			}
		} catch (MalformedURLException e) {
			connected = false;
			throw e;
		}

		if (debug) connConsoleDebug(fileURL);

		if (connected) {
			//try connection
			try {
				connectionURL = (URLConnection) fileURL.openConnection();
				if (verbose) System.out.println("connection ok ? " + (connectionURL != null));
				if (verbose) System.out.println("");
				connectionURL.connect();
				if (verbose) System.out.println("------------------>" + connectionURL.getContentType());
			} catch (Exception e) {
				connected = false;	
				throw e;
			}
		}

		return connected;
	}

	/**
	 * main method of tests
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		boolean b = true;
		System.out.println("------------------");
		DownloadWebFile monDownloadWebFile = new DownloadWebFile();
		monDownloadWebFile.setPathFileTarget("C:\\Temp\\EMF");
		//monDownloadWebFile.setPathFileTarget("C:\\eclipse_3.2.1 experimental\\plugins\\org.atl.eclipse.engine_1.0.7\\lib");
		monDownloadWebFile.setNameFileTarget("license");//antlr-2.7.5.zip //emf-sdo-xsd-SDK-2.3.0M4.zip //
		//monDownloadWebFile.setNames("http://www.antlr.org/download", "antlr-2.7.5.jar");
		//monDownloadWebFile.setNames("http://www.netbeans.org/download/dev/", "mdr-standalone.zip");
		monDownloadWebFile.setNames("http://www.eclipse.org/legal/epl", "notice.php");
		//monDownloadWebFile.setNames("http://www.antlr.org:80/download", "antlr-2.7.5.zip");
		//monDownloadWebFile.setNames("http://discus/lib/1", "antlr-2.7.5.jar");

		try {
			//REM: il faut cr�er une methode waytoURL() qui gere la plupart des cas x3
			//monDownloadWebFile.entryName = "mof.jar";//antlr-2.7.5.msi//ActionElement.class
			//monDownloadWebFile.setNames("http://www.antlr.org/download/", "test");//antlr-2.7.5.zip
			//monDownloadWebFile.setNames("http://ftp.ntua.gr/mirror/eclipse/modeling/emf/emf/downloads/drops/2.3.0/S200612211251/", "emf-sdo-xsd-SDK-2.3.0M4.zip");
			//monDownloadWebFile.setNames("http://discus/lib/1/", "org.eclipse.emf.codegen.ecore_2.2.0.v200606271057.zip/Item.gif");////antlr-2.7.5.jar
			//monDownloadWebFile.setNames("http://ftp.ntua.gr/mirror/eclipse/modeling/emf/emf/downloads/drops/2.3.0/S200612211251/emf-sdo-runtime-2.3.0M4.zip", "/eclipse/plugins/org.eclipse.emf.ecore.sdo.edit_2.3.0.v200612211251.jar");////antlr-2.7.5.jar
			//monDownloadWebFile.setCustomizeWayToURL(true);
			b = monDownloadWebFile.connect();
			if (b) System.out.println(monDownloadWebFile.getWebFile());
			//if (b) System.out.println(monDownloadWebFile.getWebFileFromZip());
			//if (b) System.out.println(monDownloadWebFile.getWebFileFromJar());
		}
		//example of management of the exception
		catch (Exception ev) {
			// TODO Auto-generated catch block
			if (ev instanceof java.net.MalformedURLException)
				System.out.println("message and treatment for a malformed url");
			else if (ev instanceof java.io.FileNotFoundException)
				System.out.println("message and treatment if the file is missing");
			else if (ev instanceof java.net.UnknownHostException)
				System.out.println("message and treatment for unknown server");
			else {
				//System.out.println("!! exception not managed !!");
				ev.printStackTrace();
			}
		}
	}

	/**
	 * Method to debug
	 * 
	 * @param conn
	 */
	private void connConsoleDebug(Object conn) {

		String typeClass = conn.getClass().toString();
		if ( typeClass.equalsIgnoreCase("class java.net.URL") ) {
			System.out.println(((URL) conn).getFile());
			System.out.println(((URL) conn).getAuthority());
			System.out.println(((URL) conn).getHost());
			System.out.println(((URL) conn).getPath());
			System.out.println(conn.toString());
			System.out.println(((URL) conn).getPort());
			System.out.println(((URL) conn).getProtocol());
		}
		else {
			System.out.println("object autre que JarURLConnection, HttpURLConnection, ZipURLConnection");
		}
	}

};