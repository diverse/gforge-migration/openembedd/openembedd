package org.openembedd.checker.check.library;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.openembedd.checker.action.DownloadConstants;
import org.osgi.framework.Bundle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * TODO
 *  think to finalize by making a interface and listener to integrate this 
 *  plugin in Eclipse
 * TODO
 */
/**
 * 
 * @author openembedd
 *
 */
public class RequiredLibrary {

	public class WebLibrary {
		private String nameID;
		private String nameWebFile;
		private String siteURL;
		private String entry;
		private Path licensePathFile;
		private String pluginID;
		private String targetNameFile;
		private Path targetPathFile;
		private boolean errorTarget;
		private boolean errorSource;
		
		public WebLibrary() {
			this.nameID = null;
			this.nameWebFile = null;
			this.siteURL = null;
			this.entry = null;
			this.licensePathFile = null;
			this.pluginID = null;
			this.targetNameFile = null;
			this.targetPathFile = null;
			this.errorTarget = false;
			this.errorSource = false;
		}
		public void setNameID(String nameID) {
			this.nameID = nameID;
		}
		public void setNameWebFile(String nameWebFile) {
			this.nameWebFile = nameWebFile;
		}
		public void setSiteURL(String siteURL) {
			this.siteURL = siteURL;
		}
		public void setEntry(String entry) {
			this.entry = entry;
		}
		public void setLicensePathFile(String slicensePathFile) {
			this.licensePathFile = new Path(slicensePathFile);
		}
		public void setPluginID(String pluginID) {
			this.pluginID = pluginID;
		}
		public void setTargetNameFile(String targetNameFile) {
			this.targetNameFile = targetNameFile;
		}
		public void setTargetPathFile(String stargetPathFile) {
			this.targetPathFile = new Path(stargetPathFile);
		}
		public boolean errorTarget() {
			return errorTarget;
		}
		public boolean errorSource() {
			return errorSource;
		}
		
		/**
		 * method to check if the library is missing from the target
		 *
		 */
		public boolean isMissing() {
			boolean missing = false;
			URL urlTarget = null;
			
			//check the target
			if (pluginID != null) {
				try {
					Bundle bundle = Platform.getBundle(pluginID);
					urlTarget = FileLocator.find(bundle, targetPathFile, null);
				} catch (Exception e) {
					this.errorTarget = true;
				}
				if (urlTarget != null) {
					try {
						Path ptarget = new Path(FileLocator.resolve(urlTarget).getPath());
						ptarget.append(targetNameFile);
						missing = ptarget.makeAbsolute().toFile().exists();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						this.errorTarget = true;
					}
				}
				else 
					this.errorTarget = true;
			}
			else {
				try {
				IPath ptarget = targetPathFile.append(targetNameFile).makeAbsolute();
				missing = ptarget.toFile().exists();
				} catch (Exception e) {
					this.errorTarget = true;
				}
			}
			return missing;
		}
	};
	
	//private URL urlPlugin = ;

	private Path pathLibrary = new Path("openembedd_config.xml"); //local xml file in the plugin
	private List<WebLibrary> libraries;
	private Hashtable<String, Type> lib; //TODO use interface
	
	
	/**
	 * the constructor
	 *
	 */
	public RequiredLibrary() {
		libraries = new ArrayList<WebLibrary>();
		init();
	}
	
	/**
	 * 
	 */
	
	public void init() {
		try {
			loadFromXML();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * method to load required libraries from the file library.xml
	 *
	 */
	private void loadFromXML() throws Exception {
		
		try {
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbfactory.newDocumentBuilder();
			Bundle bundle = Platform.getBundle(DownloadConstants.pluginCheckingID);
			//TODO search in the bundle callback
			URL urlLibrary = bundle.getEntry("openembedd_config.xml");
			if (urlLibrary != null) {
				urlLibrary = FileLocator.resolve(urlLibrary);
				Path pathLibrary = new Path(urlLibrary.getPath());
				
				File fLibrary = pathLibrary.toFile();
				if (!fLibrary.exists()) throw new Exception("file doesn't exist : library.xml");
				try {
					Document docLibrary = db.parse(fLibrary);
					docLibrary.getElementsByTagName("library").item(0).getAttributes().item(0).getNodeValue();//"antlr"
					docLibrary.getElementsByTagName("webFile").item(0).getAttributes().getNamedItem("nameWeb").getNodeValue();//"antlr-2.7.5.jar"
					docLibrary.getChildNodes().item(0).getChildNodes().item(3).getNodeName();//"pluginConfig"
					docLibrary.getChildNodes().item(0).getChildNodes().item(3).getChildNodes().item(1).getNodeName();//"level"
					docLibrary.getChildNodes().item(0).getChildNodes().item(3).getChildNodes().item(1).getChildNodes().item(1).getChildNodes().item(1).getTextContent();//"org.eclipse.emf.codegen.ecore.ui_2.2.0.v200606271057.jar"
					docLibrary.getChildNodes().item(0).getChildNodes().item(3).getChildNodes().item(1).getChildNodes().item(1).getChildNodes().item(3).getTextContent();//is equal to
					docLibrary.getChildNodes().item(0).getChildNodes().item(3).getChildNodes().item(1).getChildNodes().item(1).getChildNodes().item(1).getNextSibling().getNextSibling().getTextContent();
					NodeList libNode = docLibrary.getElementsByTagName("library");
					for (int i = 0; i < libNode.getLength(); i++) {
						Node currentLibNode = libNode.item(i);
						if (currentLibNode.getAttributes().getLength() > 0) {
							WebLibrary wl = new WebLibrary();
							libraries.add(wl);
							String nameID = currentLibNode.getAttributes().item(0).getNodeValue();
							wl.setNameID(nameID);
							NodeList infoLib = currentLibNode.getChildNodes();
							for (int j = 0; j < infoLib.getLength(); j++) {
								Node curentLibInfoNode = infoLib.item(j);
								if (!curentLibInfoNode.getNodeName().equalsIgnoreCase("#text")) {
									if (curentLibInfoNode.getNodeName().equals("webFile")) {
										String nameWeb = curentLibInfoNode.getAttributes().getNamedItem("nameWeb").getNodeValue();
										String siteURL = curentLibInfoNode.getAttributes().getNamedItem("siteURL").getNodeValue();
										String entry = curentLibInfoNode.getAttributes().getNamedItem("entry").getNodeValue();
										wl.setNameWebFile((nameWeb == null ? "" : nameWeb));
										wl.setSiteURL((siteURL == null ? "" : siteURL));
										wl.setEntry((entry == null ? "" : entry));
									}
									if (curentLibInfoNode.getNodeName().equals("license")) {
										String licensePathFile = curentLibInfoNode.getAttributes().getNamedItem("licensePathFile").getNodeValue();
										wl.setLicensePathFile((licensePathFile == null ? "" : licensePathFile));
									}
									if (curentLibInfoNode.getNodeName().equals("plugin")) {
										String pluginID = curentLibInfoNode.getAttributes().getNamedItem("pluginID").getNodeValue();
										wl.setPluginID((pluginID == null ? "" : pluginID));
									}
									if (curentLibInfoNode.getNodeName().equals("target")) {
										String targetName = curentLibInfoNode.getAttributes().getNamedItem("targetName").getNodeValue();
										String targetPath = curentLibInfoNode.getAttributes().getNamedItem("targetPath").getNodeValue();
										wl.setTargetNameFile((targetName == null ? "" : targetName));
										wl.setTargetPathFile((targetPath == null ? "" : targetPath));
									}
									
								}
							}
						}
					}
				} catch (Exception e) { throw e; }
			}
			//TODO search in the bundle callback
			//File flibrary = urlLibraryXML.;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		
		printDebug(libraries.get(0));
	}
	
	/**
	 * 
	 */
	public boolean isEmpty() {
		
		if (libraries == null) 
			return true;
		if (libraries.isEmpty())
			return true;
		return false;
	}
	
	/**
	 * @param args
	 */
	public static void main() {
		// TODO Auto-generated method stub
		RequiredLibrary rltest = new RequiredLibrary();
		System.out.println(rltest.libraries.get(0).isMissing());
		
	}

	public void printDebug(WebLibrary wl){
		System.out.println("nameID          = " + wl.nameID);
		System.out.println("nameWebFile     = " + wl.nameWebFile);
		System.out.println("siteURL         = " + wl.siteURL);
		System.out.println("entry           = " + wl.entry);
		System.out.println("licensePathFile = " + wl.licensePathFile);
		System.out.println("pluginID        = " + wl.pluginID);
		System.out.println("targetNameFile  = " + wl.targetNameFile);
		System.out.println("targetPathFile  = " + wl.targetPathFile);
	}
}
