/**

This project contains the checker of configuration developed by OpenEmbeDD team.
The source consists of two parts: 
- editor EMF of configuration model
- Eclipse viewer (a jFace component)
Editor provides the factory of models, a easy way to build configuration models for expert developers.
Viewer provides the analyser of configuration models for common users.

---------------------------------------------------------------------------------------------------------

At the beginning the goal of checker was to check if the current Eclipse platform could be in conformity with
a configuration model.
In a second time it appeared convenient to be able to complete the installation on-the-fly.
This last point requires a lot of  precaution in order to keep the stability of the platform.
 
The configuration metamodel may evolve with the time in order to enlarge the possibilities.

**/