/**
 * Copyright : IRISA / INRIA Rennes Bretagne Atlantique - OpenEmbeDD integration team
 * 
 * This plug-in is under the terms of the EPL License. http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 */
package org.openembedd.launchexec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * This class allows to execute native command and write the standard output and
 * error output into a Console.
 */
public class ExecCommand
{
	/** The console for these applications */
	private Console		console;
	/** Array represented each argument of the command line */
	private String[]	command;
	/** Indicate if the console has been created locally */
	private boolean		consoleIsLocal;

	/**
	 * Constructor which creates a new Console.
	 * 
	 * @param consoleName
	 *            the name of the console
	 * @param cmd
	 *            Array represented each argument of the native command line
	 */
	public ExecCommand(String consoleName, String[] cmd)
	{
		this.console = new Console(consoleName);
		this.consoleIsLocal = true;
		this.command = cmd;
	}

	/**
	 * Constructor which uses an already existing console.
	 * 
	 * @param console
	 *            the existing console
	 * @param cmd
	 *            Array represented each argument of the native command line
	 */
	public ExecCommand(Console console, String[] cmd)
	{
		this.console = console;
		this.consoleIsLocal = false;
		this.command = cmd;
	}

	/**
	 * Execute the string command given in the constructor. If the string is
	 * empty or null, nothing is executed. The standard output and the error
	 * output are written in the console if it exists.
	 */
	public void execute()
	{
		if (command == null || command.length == 0 || "".equals(command[0]))
		{
			Plugin.getDefault().getLog().log(new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "No command to execute."));
			return;
		}

		try
		{
			final Process process = Runtime.getRuntime().exec(command);

			if (console != null)
			{
				console.printInfo("Execution of the command: ");
				for (int i = 0; i < command.length; i++)
					console.printInfo(command[i] + " ");
				console.printInfo("\n");

				new Thread()
				{
					public void run()
					{
						try
						{
							BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
							String line;
							try
							{
								while ((line = reader.readLine()) != null)
								{
									console.println(line);
								}
							}
							finally
							{
								reader.close();
							}
						}
						catch (IOException ioex)
						{
							Plugin.getDefault().getLog().log(
									new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Error to access to the output stream",
											ioex));
						}
					}
				}.start();

				new Thread()
				{
					public void run()
					{
						try
						{
							BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
							String line;
							try
							{
								while ((line = reader.readLine()) != null)
								{
									console.printlnError(line);
								}
							}
							finally
							{
								reader.close();
							}
						}
						catch (IOException ioex)
						{
							Plugin.getDefault().getLog().log(
									new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Error to access to the error stream",
											ioex));
						}
					}
				}.start();

			}
			// Wait until the end of the command execution
			if (process.waitFor() != 0)
				Plugin.getDefault().getLog().log(
						new Status(IStatus.ERROR, Plugin.PLUGIN_ID,
								"The execution of the command does not finish normally."));
		}
		catch (IOException ioex)
		{
			Plugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, Plugin.PLUGIN_ID,
							"An Input/output Exception has been thrown during the execution of the command.", ioex));
		}
		catch (InterruptedException iex)
		{
			Plugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "The execution of the process has been interrupted.",
							iex));
		}

		// If the console has been created locally, all resources attached to it
		// are released.
		if (consoleIsLocal && console != null)
			console.dispose();
	}
}
