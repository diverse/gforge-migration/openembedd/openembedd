/**
 * Copyright : IRISA / INRIA Rennes Bretagne Atlantique - OpenEmbeDD integration team
 * 
 * This plug-in is under the terms of the EPL License. http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 */
package org.openembedd.launchexec;

import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleOutputStream;

/**
 * Utility classes to manage a console 
 */
public class Console
{
	/** Constant for red color */
	public final static Color		RED_COLOR		= new Color(Display.getCurrent(), 255, 0, 0);
	/** Constant for black color */
	public final static Color		BLACK_COLOR		= new Color(Display.getCurrent(), 0, 0, 0);
	/** Constant for orange color */
	public final static Color		ORANGE_COLOR	= new Color(Display.getCurrent(), 255, 128, 0);
	/** Constant for green color */
	public final static Color		GREEN_COLOR		= new Color(Display.getCurrent(), 0, 128, 0);

	/**
	 * The IOConsole instance.
	 */
	private IOConsole				console			= null;

	/**
	 * The stream on which to write messages
	 */
	private IOConsoleOutputStream	outputStream	= null;

	/**
	 * This basic constructor instantiates an IOConsole object, add the console instance to the eclipse console
	 * registry, and display it.
	 * 
	 * @param title
	 *        the title for the console
	 */
	public Console(String title)
	{
		console = new IOConsole(title, null);
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { console });
		ConsolePlugin.getDefault().getConsoleManager().showConsoleView(console);
		console.activate();
	}

	/**
	 * This is a lazy initialization.
	 * 
	 * @return the current output stream of the console
	 */
	private IOConsoleOutputStream getOutputStream()
	{
		if (outputStream == null)
			outputStream = console.newOutputStream();
		return outputStream;
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the message in the output
	 * stream. The message is written in the color defined by the <code>color</code> parameter.
	 * 
	 * @param color
	 *        the color used to write the message
	 * @param message
	 *        the message to display in the console
	 */
	public synchronized void print(Color color, String message)
	{
		if (message == null)
			return;
		if (color != null)
			changeColor(color);

		IOConsoleOutputStream stream = getOutputStream();
		try
		{
			stream.write(message);
		}
		catch (IOException exception)
		{
			Plugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Problem when writing in the console output stream.",
					exception));
		}
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the message in the output
	 * stream using the black color.
	 * 
	 * @param message
	 *        the message to display in the console
	 */
	public synchronized void print(String message)
	{
		this.print(BLACK_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the error message in the
	 * output stream using the red color.
	 * 
	 * @param message
	 *        the error message to display
	 */
	public synchronized void printError(String message)
	{
		this.print(RED_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the warning message in the
	 * output stream using the orange color.
	 * 
	 * @param message
	 *        the warning message to display
	 */
	public synchronized void printWarning(String message)
	{
		this.print(ORANGE_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the information message in
	 * the output stream using the green color.
	 * 
	 * @param message
	 *        the information message to display
	 */
	public synchronized void printInfo(String message)
	{
		this.print(GREEN_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the message in the output
	 * stream and add a new line at the end. The message is written in the color defined by the <code>color</code>
	 * parameter.
	 * 
	 * @param color
	 *        the color used to write the message
	 * @param message
	 *        the message to display in the console
	 */
	public synchronized void println(Color color, String message)
	{
		if (message == null)
			return;
		if (color != null)
			changeColor(color);

		IOConsoleOutputStream stream = getOutputStream();
		try
		{
			stream.write(message);
			stream.write("\n");
		}
		catch (IOException exception)
		{
			Plugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Problem when writing in the console output stream.",
					exception));
		}
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the message in the output
	 * stream and add a new line at the end using the black color.
	 * 
	 * @param message
	 *        the message to display in the console
	 */
	public synchronized void println(String message)
	{
		this.println(BLACK_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the error message in the
	 * output stream and add a new line at the end using the red color.
	 * 
	 * @param message
	 *        the error message to display
	 */
	public synchronized void printlnError(String message)
	{
		this.println(RED_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the warning message in the
	 * output stream and add a new line at the end using the orange color.
	 * 
	 * @param message
	 *        the warning message to display
	 */
	public synchronized void printlnWarning(String message)
	{
		this.println(ORANGE_COLOR, message);
	}

	/**
	 * This method creates an output stream from the console if it does not exist and write the information message in
	 * the output stream and add a new line at the end using the green color.
	 * 
	 * @param message
	 *        the information message to display
	 */
	public synchronized void printlnInfo(String message)
	{
		this.println(GREEN_COLOR, message);
	}

	/**
	 * Change the color for the following messages. If the stream already uses the same color, it does nothing. If the
	 * color is different from the previous one, the output stream is reset and a new one is created using the new color
	 * This is because a simple change of current stream color, change the color for all messages, even previous ones.
	 * 
	 * @param color
	 *        the new color to use to display next messages
	 */
	private void changeColor(Color color)
	{
		Color previousColor = getOutputStream().getColor();
		if (!color.equals(previousColor))
		{
			// need to change to another stream for the new color
			outputStream = null; // reset the stream
			getOutputStream().setColor(color);
		}
	}

	/**
	 * Clear all messages in the console
	 */
	public synchronized void clear()
	{
		console.clearConsole();
	}

	/**
	 * Closes this output stream and releases any system resources associated with this stream.
	 */
	public synchronized void dispose()
	{
		if (outputStream != null)
		{
			try
			{
				outputStream.close();
			}
			catch (IOException exception)
			{
				Plugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Problem when closing the console output stream.",
						exception));
			}
		}
	}
}
