package org.eclipse.swtbot.eclipse.recorder.ui;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.ide.IDE;

public class RecorderAdvisor extends WorkbenchAdvisor {
	
	private String perspectiveId;
	
	public RecorderAdvisor(String initialWindowPerspectiveId) {
		perspectiveId = initialWindowPerspectiveId;
	}
	
	@Override
	public String getInitialWindowPerspectiveId() {
		if (perspectiveId == null)
			perspectiveId = IDE.RESOURCE_PERSPECTIVE_ID;
		return perspectiveId;
	}

	public IAdaptable getDefaultPageInput() {
		return ResourcesPlugin.getWorkspace().getRoot();
	}
}
