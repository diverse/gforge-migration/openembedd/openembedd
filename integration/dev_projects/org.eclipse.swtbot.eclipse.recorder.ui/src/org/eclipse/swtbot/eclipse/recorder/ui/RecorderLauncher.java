package org.eclipse.swtbot.eclipse.recorder.ui;

import java.util.ArrayList;
import java.util.List;

import net.sf.swtbot.recorder.ui.SWTBotRecorderUI;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.pde.core.plugin.TargetPlatform;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * Launch the workbench then run the SWTBot recorder.<br>
 * @author vmahe@irisa.fr
 */
public class RecorderLauncher implements IApplication {

	private static String JAVA_PERSPECTIVE_ID = "org.eclipse.jdt.ui.JavaPerspective";

	public void recorderRun(String[] arg0) throws Exception {
		/*
		 * We must wait until the workbench has been set up.
		 */
		while ( ! PlatformUI.isWorkbenchRunning() ) {
			System.out.println("Waiting for the workbench...");
			Thread.sleep(1000);
		}

		/*
		 * We must wait until the plug-ins extended the startup extension are initialized.
		 */
		int nbTryLeft = 60 * 2 ; //
		while ( ! arePluginsInitialized() && nbTryLeft > 0) {
			System.out.println("Waiting for the startup plugins...");
			Thread.sleep(1000);	
			nbTryLeft--;
		}
		if(! arePluginsInitialized()){
			String msg = "";
			msg += notInitializedPlugins + ", ";
			throw new Error("Didn't succeed to startup the plugins : " + msg);
		}
		
		SWTBotRecorderUI runner = new SWTBotRecorderUI();
		runner.initialize();
	}

	public Object start(IApplicationContext context) throws Exception {

		final String[] args = (String[]) context.getArguments().get("application.args");

		Runnable runner = new Runnable() {
			public void run() {
				try {
					recorderRun(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		Thread t = new Thread(runner);
		t.start();

//		Display display = PlatformUI.createDisplay();
//		int returnCode = PlatformUI.createAndRunWorkbench(display, new RecorderAdvisor(null));
//		if (returnCode == PlatformUI.RETURN_RESTART)
//			return IApplication.EXIT_RESTART;
//		else
//			return IApplication.EXIT_OK;
		
		IApplication application = getApplication();
		return application.start(context);
	}

	private IApplication getApplication() throws Exception {
		String applicationName = TargetPlatform.getDefaultApplication();
		
		IExtension extension = Platform.getExtensionRegistry().getExtension(
				Platform.PI_RUNTIME,
				Platform.PT_APPLICATIONS,
				applicationName);
		
		IConfigurationElement[] elements = extension.getConfigurationElements();
		if (elements.length > 0) {
			IConfigurationElement[] runs = elements[0].getChildren("run");
			if (runs.length > 0) {
				return (IApplication) runs[0].createExecutableExtension("class");
			}
		}
		return null;
	}

	protected List<String> notInitializedPlugins = new ArrayList<String>();
	private boolean arePluginsInitialized() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();

		// Eclipse bug 55901: don't use getConfigElements directly, for pre-3.0
		// compatibility, make sure to allow both missing class
		// attribute and a missing startup element
		IExtensionPoint point = registry.getExtensionPoint(PlatformUI.PLUGIN_ID,"startup");

		final IExtension[] extensions = point.getExtensions();
		if (extensions.length == 0) {
			return true;
		}
		boolean result = true;
		notInitializedPlugins.clear();
		for (int i=0; i<extensions.length; i++) {
			IExtension extension = extensions[i];

			Bundle bundle = Platform.getBundle( extension.getNamespaceIdentifier() );

			// problem with 'org.topcased.toolkit', which is never more than "resolved" without any activator nor activation policy
			boolean initialized = (bundle.getState() == Bundle.ACTIVE)
			|| ((bundle.getState() == Bundle.RESOLVED)
					&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATOR) == null) 
					&& (bundle.getHeaders().get(Constants.BUNDLE_ACTIVATIONPOLICY) == null ));

			if ( ! initialized ) {
				notInitializedPlugins.add(extension.getNamespaceIdentifier() + "( bundle : "+ bundle.getState() +" "+bundle.toString() +")");
				result = false;
			}
		}

		return result;
	}

	public void stop() {
		// TODO Auto-generated method stub

	}
}
