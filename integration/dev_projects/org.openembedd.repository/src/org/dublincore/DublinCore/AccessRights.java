/**
 * <copyright>
 * </copyright>
 *
 * $Id: AccessRights.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Rights</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getAccessRights()
 * @model
 * @generated
 */
public interface AccessRights extends RightsRefinement {
} // AccessRights
