/**
 * <copyright>
 * </copyright>
 *
 * $Id: MetamodelName.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Metamodel Name</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.dublincore.DublinCore.DublinCorePackage#getMetamodelName()
 * @model
 * @generated
 */
public enum MetamodelName implements Enumerator {
	/**
	 * The '<em><b>UML2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UML2_VALUE
	 * @generated
	 * @ordered
	 */
	UML2(0, "UML2", "UML2"),

	/**
	 * The '<em><b>Marte</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MARTE_VALUE
	 * @generated
	 * @ordered
	 */
	MARTE(1, "Marte", "Marte"),

	/**
	 * The '<em><b>Fiacre</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIACRE_VALUE
	 * @generated
	 * @ordered
	 */
	FIACRE(2, "Fiacre", "Fiacre"),

	/**
	 * The '<em><b>Gaspard</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GASPARD_VALUE
	 * @generated
	 * @ordered
	 */
	GASPARD(3, "Gaspard", "Gaspard"),

	/**
	 * The '<em><b>Syn DEx</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYN_DEX_VALUE
	 * @generated
	 * @ordered
	 */
	SYN_DEX(0, "SynDEx", "SynDEx"),

	/**
	 * The '<em><b>SME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SME_VALUE
	 * @generated
	 * @ordered
	 */
	SME(0, "SME", "SME"),

	/**
	 * The '<em><b>AADL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AADL_VALUE
	 * @generated
	 * @ordered
	 */
	AADL(4, "AADL", "AADL"),

	/**
	 * The '<em><b>Sys ML</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYS_ML_VALUE
	 * @generated
	 * @ordered
	 */
	SYS_ML(0, "SysML", "SysML"),

	/**
	 * The '<em><b>Ecore</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ECORE_VALUE
	 * @generated
	 * @ordered
	 */
	ECORE(5, "Ecore", "Ecore"),

	/**
	 * The '<em><b>SAM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAM_VALUE
	 * @generated
	 * @ordered
	 */
	SAM(6, "SAM", "SAM"),

	/**
	 * The '<em><b>SDL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SDL_VALUE
	 * @generated
	 * @ordered
	 */
	SDL(7, "SDL", "SDL"),

	/**
	 * The '<em><b>OCL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OCL_VALUE
	 * @generated
	 * @ordered
	 */
	OCL(0, "OCL", "OCL"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(0, "Other", "Other"), /**
	 * The '<em><b>Kermeta</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KERMETA_VALUE
	 * @generated
	 * @ordered
	 */
	KERMETA(0, "Kermeta", "Kermeta"), /**
	 * The '<em><b>KM3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KM3_VALUE
	 * @generated
	 * @ordered
	 */
	KM3(8, "KM3", "KM3"), /**
	 * The '<em><b>CWM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CWM_VALUE
	 * @generated
	 * @ordered
	 */
	CWM(0, "CWM", "CWM");

	/**
	 * The '<em><b>UML2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UML2</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UML2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UML2_VALUE = 0;

	/**
	 * The '<em><b>Marte</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Marte</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MARTE
	 * @model name="Marte"
	 * @generated
	 * @ordered
	 */
	public static final int MARTE_VALUE = 1;

	/**
	 * The '<em><b>Fiacre</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Fiacre</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIACRE
	 * @model name="Fiacre"
	 * @generated
	 * @ordered
	 */
	public static final int FIACRE_VALUE = 2;

	/**
	 * The '<em><b>Gaspard</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Gaspard</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GASPARD
	 * @model name="Gaspard"
	 * @generated
	 * @ordered
	 */
	public static final int GASPARD_VALUE = 3;

	/**
	 * The '<em><b>Syn DEx</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Syn DEx</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYN_DEX
	 * @model name="SynDEx"
	 * @generated
	 * @ordered
	 */
	public static final int SYN_DEX_VALUE = 0;

	/**
	 * The '<em><b>SME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SME_VALUE = 0;

	/**
	 * The '<em><b>AADL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AADL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AADL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AADL_VALUE = 4;

	/**
	 * The '<em><b>Sys ML</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sys ML</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYS_ML
	 * @model name="SysML"
	 * @generated
	 * @ordered
	 */
	public static final int SYS_ML_VALUE = 0;

	/**
	 * The '<em><b>Ecore</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ecore</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ECORE
	 * @model name="Ecore"
	 * @generated
	 * @ordered
	 */
	public static final int ECORE_VALUE = 5;

	/**
	 * The '<em><b>SAM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAM_VALUE = 6;

	/**
	 * The '<em><b>SDL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SDL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SDL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SDL_VALUE = 7;

	/**
	 * The '<em><b>OCL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OCL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OCL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OCL_VALUE = 0;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 0;

	/**
	 * The '<em><b>Kermeta</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Kermeta</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KERMETA
	 * @model name="Kermeta"
	 * @generated
	 * @ordered
	 */
	public static final int KERMETA_VALUE = 0;

	/**
	 * The '<em><b>KM3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KM3</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KM3
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KM3_VALUE = 8;

	/**
	 * The '<em><b>CWM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CWM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CWM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CWM_VALUE = 0;

	/**
	 * An array of all the '<em><b>Metamodel Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MetamodelName[] VALUES_ARRAY =
		new MetamodelName[] {
			UML2,
			MARTE,
			FIACRE,
			GASPARD,
			SYN_DEX,
			SME,
			AADL,
			SYS_ML,
			ECORE,
			SAM,
			SDL,
			OCL,
			OTHER,
			KERMETA,
			KM3,
			CWM,
		};

	/**
	 * A public read-only list of all the '<em><b>Metamodel Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MetamodelName> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Metamodel Name</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetamodelName get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MetamodelName result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Metamodel Name</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetamodelName getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MetamodelName result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Metamodel Name</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetamodelName get(int value) {
		switch (value) {
			case UML2_VALUE: return UML2;
			case MARTE_VALUE: return MARTE;
			case FIACRE_VALUE: return FIACRE;
			case GASPARD_VALUE: return GASPARD;
			case AADL_VALUE: return AADL;
			case ECORE_VALUE: return ECORE;
			case SAM_VALUE: return SAM;
			case SDL_VALUE: return SDL;
			case KM3_VALUE: return KM3;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MetamodelName(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MetamodelName
