/**
 * <copyright>
 * </copyright>
 *
 * $Id: Metadata.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metadata</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getContributor <em>Contributor</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getCoverage <em>Coverage</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getCreator <em>Creator</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getDate <em>Date</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getDescription <em>Description</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getFormat <em>Format</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getRights <em>Rights</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getSource <em>Source</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getSubject <em>Subject</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getTitle <em>Title</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getType <em>Type</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getAudience <em>Audience</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Metadata#getRightsHolder <em>Rights Holder</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata()
 * @model
 * @generated
 */
public interface Metadata extends EObject {
	/**
	 * Returns the value of the '<em><b>Contributor</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Contributor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contributor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributor</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Contributor()
	 * @model containment="true"
	 * @generated
	 */
	EList<Contributor> getContributor();

	/**
	 * Returns the value of the '<em><b>Coverage</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Coverage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coverage</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coverage</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Coverage()
	 * @model containment="true"
	 * @generated
	 */
	EList<Coverage> getCoverage();

	/**
	 * Returns the value of the '<em><b>Creator</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Creator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creator</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Creator()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Creator> getCreator();

	/**
	 * Returns the value of the '<em><b>Date</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Date}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Date()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Date> getDate();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Description}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Description()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Description> getDescription();

	/**
	 * Returns the value of the '<em><b>Format</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Format}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Format()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Format> getFormat();

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Identifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Identifier()
	 * @model containment="true"
	 * @generated
	 */
	EList<Identifier> getIdentifier();

	/**
	 * Returns the value of the '<em><b>Language</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Language}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Language()
	 * @model containment="true"
	 * @generated
	 */
	EList<Language> getLanguage();

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Publisher}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Publisher()
	 * @model containment="true"
	 * @generated
	 */
	EList<Publisher> getPublisher();

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Relation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Relation()
	 * @model containment="true"
	 * @generated
	 */
	EList<Relation> getRelation();

	/**
	 * Returns the value of the '<em><b>Rights</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Rights}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rights</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rights</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Rights()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Rights> getRights();

	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Source}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Source()
	 * @model containment="true"
	 * @generated
	 */
	EList<Source> getSource();

	/**
	 * Returns the value of the '<em><b>Subject</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Subject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Subject()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Subject> getSubject();

	/**
	 * Returns the value of the '<em><b>Title</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Title}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Title()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Title> getTitle();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Type()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Type> getType();

	/**
	 * Returns the value of the '<em><b>Audience</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Audience}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Audience</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Audience</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Audience()
	 * @model containment="true"
	 * @generated
	 */
	EList<Audience> getAudience();

	/**
	 * Returns the value of the '<em><b>Provenance</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.Provenance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provenance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provenance</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_Provenance()
	 * @model containment="true"
	 * @generated
	 */
	EList<Provenance> getProvenance();

	/**
	 * Returns the value of the '<em><b>Rights Holder</b></em>' containment reference list.
	 * The list contents are of type {@link org.dublincore.DublinCore.RightsHolder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rights Holder</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rights Holder</em>' containment reference list.
	 * @see org.dublincore.DublinCore.DublinCorePackage#getMetadata_RightsHolder()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<RightsHolder> getRightsHolder();

} // Metadata
