/**
 * <copyright>
 * </copyright>
 *
 * $Id: Creator.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Creator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Creator#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getCreator()
 * @model
 * @generated
 */
public interface Creator extends Element {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getCreator_Value()
	 * @model required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Creator#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // Creator
