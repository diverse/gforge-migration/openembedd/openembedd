/**
 * <copyright>
 * </copyright>
 *
 * $Id: DateAccepted.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date Accepted</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getDateAccepted()
 * @model
 * @generated
 */
public interface DateAccepted extends DateRefinement {
} // DateAccepted
