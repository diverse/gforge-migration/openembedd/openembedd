/**
 * <copyright>
 * </copyright>
 *
 * $Id: DublinCoreFactoryImpl.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import org.dublincore.DublinCore.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DublinCoreFactoryImpl extends EFactoryImpl implements DublinCoreFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DublinCoreFactory init() {
		try {
			DublinCoreFactory theDublinCoreFactory = (DublinCoreFactory)EPackage.Registry.INSTANCE.getEFactory("http://dublincore.org"); 
			if (theDublinCoreFactory != null) {
				return theDublinCoreFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DublinCoreFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DublinCoreFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DublinCorePackage.METADATA: return createMetadata();
			case DublinCorePackage.CONTRIBUTOR: return createContributor();
			case DublinCorePackage.COVERAGE: return createCoverage();
			case DublinCorePackage.CREATOR: return createCreator();
			case DublinCorePackage.DATE: return createDate();
			case DublinCorePackage.DESCRIPTION: return createDescription();
			case DublinCorePackage.FORMAT: return createFormat();
			case DublinCorePackage.IDENTIFIER: return createIdentifier();
			case DublinCorePackage.LANGUAGE: return createLanguage();
			case DublinCorePackage.PUBLISHER: return createPublisher();
			case DublinCorePackage.RELATION: return createRelation();
			case DublinCorePackage.RIGHTS: return createRights();
			case DublinCorePackage.SOURCE: return createSource();
			case DublinCorePackage.SUBJECT: return createSubject();
			case DublinCorePackage.TITLE: return createTitle();
			case DublinCorePackage.TYPE: return createType();
			case DublinCorePackage.DATASET: return createDataset();
			case DublinCorePackage.ALTERNATIVE: return createAlternative();
			case DublinCorePackage.ABSTRACT: return createAbstract();
			case DublinCorePackage.TABLE_OF_CONTENTS: return createTableOfContents();
			case DublinCorePackage.AVAILABLE: return createAvailable();
			case DublinCorePackage.CREATED: return createCreated();
			case DublinCorePackage.DATE_ACCEPTED: return createDateAccepted();
			case DublinCorePackage.DATE_COPYRIGHTED: return createDateCopyrighted();
			case DublinCorePackage.DATE_SUBMITTED: return createDateSubmitted();
			case DublinCorePackage.ISSUED: return createIssued();
			case DublinCorePackage.MODIFIED: return createModified();
			case DublinCorePackage.IS_PART_OF: return createIsPartOf();
			case DublinCorePackage.IS_VERSION_OF: return createIsVersionOf();
			case DublinCorePackage.IS_FORMAT_OF: return createIsFormatOf();
			case DublinCorePackage.HAS_FORMAT: return createHasFormat();
			case DublinCorePackage.IS_REFERENCED_BY: return createIsReferencedBy();
			case DublinCorePackage.REFERENCES: return createReferences();
			case DublinCorePackage.IS_BASIS_FOR: return createIsBasisFor();
			case DublinCorePackage.IS_BASED_ON: return createIsBasedOn();
			case DublinCorePackage.REQUIRES: return createRequires();
			case DublinCorePackage.ACCESS_RIGHTS: return createAccessRights();
			case DublinCorePackage.LICENSE: return createLicense();
			case DublinCorePackage.AUDIENCE: return createAudience();
			case DublinCorePackage.BIBLIOGRAPHIC_CITATION: return createBibliographicCitation();
			case DublinCorePackage.PROVENANCE: return createProvenance();
			case DublinCorePackage.RIGHTS_HOLDER: return createRightsHolder();
			case DublinCorePackage.MEDIATOR: return createMediator();
			case DublinCorePackage.EDUCATION_LEVEL: return createEducationLevel();
			case DublinCorePackage.EXTERNAL_STORAGE: return createExternalStorage();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DublinCorePackage.METAMODEL_NAME:
				return createMetamodelNameFromString(eDataType, initialValue);
			case DublinCorePackage.FORMAT_NATURE:
				return createFormatNatureFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DublinCorePackage.METAMODEL_NAME:
				return convertMetamodelNameToString(eDataType, instanceValue);
			case DublinCorePackage.FORMAT_NATURE:
				return convertFormatNatureToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Metadata createMetadata() {
		MetadataImpl metadata = new MetadataImpl();
		return metadata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Contributor createContributor() {
		ContributorImpl contributor = new ContributorImpl();
		return contributor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coverage createCoverage() {
		CoverageImpl coverage = new CoverageImpl();
		return coverage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Creator createCreator() {
		CreatorImpl creator = new CreatorImpl();
		return creator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date createDate() {
		DateImpl date = new DateImpl();
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Description createDescription() {
		DescriptionImpl description = new DescriptionImpl();
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Format createFormat() {
		FormatImpl format = new FormatImpl();
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier createIdentifier() {
		IdentifierImpl identifier = new IdentifierImpl();
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Language createLanguage() {
		LanguageImpl language = new LanguageImpl();
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Publisher createPublisher() {
		PublisherImpl publisher = new PublisherImpl();
		return publisher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relation createRelation() {
		RelationImpl relation = new RelationImpl();
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rights createRights() {
		RightsImpl rights = new RightsImpl();
		return rights;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Source createSource() {
		SourceImpl source = new SourceImpl();
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Subject createSubject() {
		SubjectImpl subject = new SubjectImpl();
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Title createTitle() {
		TitleImpl title = new TitleImpl();
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type createType() {
		TypeImpl type = new TypeImpl();
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dataset createDataset() {
		DatasetImpl dataset = new DatasetImpl();
		return dataset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alternative createAlternative() {
		AlternativeImpl alternative = new AlternativeImpl();
		return alternative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Abstract createAbstract() {
		AbstractImpl abstract_ = new AbstractImpl();
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableOfContents createTableOfContents() {
		TableOfContentsImpl tableOfContents = new TableOfContentsImpl();
		return tableOfContents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Available createAvailable() {
		AvailableImpl available = new AvailableImpl();
		return available;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Created createCreated() {
		CreatedImpl created = new CreatedImpl();
		return created;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateAccepted createDateAccepted() {
		DateAcceptedImpl dateAccepted = new DateAcceptedImpl();
		return dateAccepted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateCopyrighted createDateCopyrighted() {
		DateCopyrightedImpl dateCopyrighted = new DateCopyrightedImpl();
		return dateCopyrighted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateSubmitted createDateSubmitted() {
		DateSubmittedImpl dateSubmitted = new DateSubmittedImpl();
		return dateSubmitted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Issued createIssued() {
		IssuedImpl issued = new IssuedImpl();
		return issued;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modified createModified() {
		ModifiedImpl modified = new ModifiedImpl();
		return modified;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPartOf createIsPartOf() {
		IsPartOfImpl isPartOf = new IsPartOfImpl();
		return isPartOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsVersionOf createIsVersionOf() {
		IsVersionOfImpl isVersionOf = new IsVersionOfImpl();
		return isVersionOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsFormatOf createIsFormatOf() {
		IsFormatOfImpl isFormatOf = new IsFormatOfImpl();
		return isFormatOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasFormat createHasFormat() {
		HasFormatImpl hasFormat = new HasFormatImpl();
		return hasFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsReferencedBy createIsReferencedBy() {
		IsReferencedByImpl isReferencedBy = new IsReferencedByImpl();
		return isReferencedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public References createReferences() {
		ReferencesImpl references = new ReferencesImpl();
		return references;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsBasisFor createIsBasisFor() {
		IsBasisForImpl isBasisFor = new IsBasisForImpl();
		return isBasisFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsBasedOn createIsBasedOn() {
		IsBasedOnImpl isBasedOn = new IsBasedOnImpl();
		return isBasedOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requires createRequires() {
		RequiresImpl requires = new RequiresImpl();
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessRights createAccessRights() {
		AccessRightsImpl accessRights = new AccessRightsImpl();
		return accessRights;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public License createLicense() {
		LicenseImpl license = new LicenseImpl();
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Audience createAudience() {
		AudienceImpl audience = new AudienceImpl();
		return audience;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BibliographicCitation createBibliographicCitation() {
		BibliographicCitationImpl bibliographicCitation = new BibliographicCitationImpl();
		return bibliographicCitation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provenance createProvenance() {
		ProvenanceImpl provenance = new ProvenanceImpl();
		return provenance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RightsHolder createRightsHolder() {
		RightsHolderImpl rightsHolder = new RightsHolderImpl();
		return rightsHolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mediator createMediator() {
		MediatorImpl mediator = new MediatorImpl();
		return mediator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EducationLevel createEducationLevel() {
		EducationLevelImpl educationLevel = new EducationLevelImpl();
		return educationLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalStorage createExternalStorage() {
		ExternalStorageImpl externalStorage = new ExternalStorageImpl();
		return externalStorage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodelName createMetamodelNameFromString(EDataType eDataType, String initialValue) {
		MetamodelName result = MetamodelName.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMetamodelNameToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormatNature createFormatNatureFromString(EDataType eDataType, String initialValue) {
		FormatNature result = FormatNature.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFormatNatureToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DublinCorePackage getDublinCorePackage() {
		return (DublinCorePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DublinCorePackage getPackage() {
		return DublinCorePackage.eINSTANCE;
	}

} //DublinCoreFactoryImpl
