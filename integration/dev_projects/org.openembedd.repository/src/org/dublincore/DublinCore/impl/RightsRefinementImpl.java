/**
 * <copyright>
 * </copyright>
 *
 * $Id: RightsRefinementImpl.java,v 1.1 2008-01-21 15:40:17 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import org.dublincore.DublinCore.DublinCorePackage;
import org.dublincore.DublinCore.RightsRefinement;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rights Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class RightsRefinementImpl extends EObjectImpl implements RightsRefinement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RightsRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DublinCorePackage.Literals.RIGHTS_REFINEMENT;
	}

} //RightsRefinementImpl
