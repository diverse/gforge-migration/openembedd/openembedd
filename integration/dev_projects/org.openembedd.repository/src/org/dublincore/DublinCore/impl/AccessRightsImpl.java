/**
 * <copyright>
 * </copyright>
 *
 * $Id: AccessRightsImpl.java,v 1.1 2008-01-21 15:40:17 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import org.dublincore.DublinCore.AccessRights;
import org.dublincore.DublinCore.DublinCorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Rights</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AccessRightsImpl extends RightsRefinementImpl implements AccessRights {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessRightsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DublinCorePackage.Literals.ACCESS_RIGHTS;
	}

} //AccessRightsImpl
