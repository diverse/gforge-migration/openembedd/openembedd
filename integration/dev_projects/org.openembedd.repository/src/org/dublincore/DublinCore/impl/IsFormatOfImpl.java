/**
 * <copyright>
 * </copyright>
 *
 * $Id: IsFormatOfImpl.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import org.dublincore.DublinCore.DublinCorePackage;
import org.dublincore.DublinCore.IsFormatOf;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Format Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class IsFormatOfImpl extends RelationRefinementImpl implements IsFormatOf {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsFormatOfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DublinCorePackage.Literals.IS_FORMAT_OF;
	}

} //IsFormatOfImpl
