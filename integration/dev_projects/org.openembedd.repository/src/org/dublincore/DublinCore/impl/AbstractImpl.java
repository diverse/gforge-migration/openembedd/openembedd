/**
 * <copyright>
 * </copyright>
 *
 * $Id: AbstractImpl.java,v 1.1 2008-01-21 15:40:17 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import org.dublincore.DublinCore.Abstract;
import org.dublincore.DublinCore.DublinCorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AbstractImpl extends DescriptionRefinementImpl implements Abstract {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DublinCorePackage.Literals.ABSTRACT;
	}

} //AbstractImpl
