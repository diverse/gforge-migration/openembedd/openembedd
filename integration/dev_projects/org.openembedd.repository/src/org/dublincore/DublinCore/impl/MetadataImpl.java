/**
 * <copyright>
 * </copyright>
 *
 * $Id: MetadataImpl.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore.impl;

import java.util.Collection;

import org.dublincore.DublinCore.Audience;
import org.dublincore.DublinCore.Contributor;
import org.dublincore.DublinCore.Coverage;
import org.dublincore.DublinCore.Creator;
import org.dublincore.DublinCore.Date;
import org.dublincore.DublinCore.Description;
import org.dublincore.DublinCore.DublinCorePackage;
import org.dublincore.DublinCore.Format;
import org.dublincore.DublinCore.Identifier;
import org.dublincore.DublinCore.Language;
import org.dublincore.DublinCore.Metadata;
import org.dublincore.DublinCore.Provenance;
import org.dublincore.DublinCore.Publisher;
import org.dublincore.DublinCore.Relation;
import org.dublincore.DublinCore.Rights;
import org.dublincore.DublinCore.RightsHolder;
import org.dublincore.DublinCore.Source;
import org.dublincore.DublinCore.Subject;
import org.dublincore.DublinCore.Title;
import org.dublincore.DublinCore.Type;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metadata</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getContributor <em>Contributor</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getCoverage <em>Coverage</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getCreator <em>Creator</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getDate <em>Date</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getFormat <em>Format</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getRights <em>Rights</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getAudience <em>Audience</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.impl.MetadataImpl#getRightsHolder <em>Rights Holder</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MetadataImpl extends EObjectImpl implements Metadata {
	/**
	 * The cached value of the '{@link #getContributor() <em>Contributor</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributor()
	 * @generated
	 * @ordered
	 */
	protected EList<Contributor> contributor;

	/**
	 * The cached value of the '{@link #getCoverage() <em>Coverage</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoverage()
	 * @generated
	 * @ordered
	 */
	protected EList<Coverage> coverage;

	/**
	 * The cached value of the '{@link #getCreator() <em>Creator</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreator()
	 * @generated
	 * @ordered
	 */
	protected EList<Creator> creator;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected EList<Date> date;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<Description> description;

	/**
	 * The cached value of the '{@link #getFormat() <em>Format</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected EList<Format> format;

	/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected EList<Identifier> identifier;

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected EList<Language> language;

	/**
	 * The cached value of the '{@link #getPublisher() <em>Publisher</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected EList<Publisher> publisher;

	/**
	 * The cached value of the '{@link #getRelation() <em>Relation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected EList<Relation> relation;

	/**
	 * The cached value of the '{@link #getRights() <em>Rights</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRights()
	 * @generated
	 * @ordered
	 */
	protected EList<Rights> rights;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected EList<Source> source;

	/**
	 * The cached value of the '{@link #getSubject() <em>Subject</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubject()
	 * @generated
	 * @ordered
	 */
	protected EList<Subject> subject;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected EList<Title> title;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> type;

	/**
	 * The cached value of the '{@link #getAudience() <em>Audience</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAudience()
	 * @generated
	 * @ordered
	 */
	protected EList<Audience> audience;

	/**
	 * The cached value of the '{@link #getProvenance() <em>Provenance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvenance()
	 * @generated
	 * @ordered
	 */
	protected EList<Provenance> provenance;

	/**
	 * The cached value of the '{@link #getRightsHolder() <em>Rights Holder</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightsHolder()
	 * @generated
	 * @ordered
	 */
	protected EList<RightsHolder> rightsHolder;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetadataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DublinCorePackage.Literals.METADATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Contributor> getContributor() {
		if (contributor == null) {
			contributor = new EObjectContainmentEList<Contributor>(Contributor.class, this, DublinCorePackage.METADATA__CONTRIBUTOR);
		}
		return contributor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Coverage> getCoverage() {
		if (coverage == null) {
			coverage = new EObjectContainmentEList<Coverage>(Coverage.class, this, DublinCorePackage.METADATA__COVERAGE);
		}
		return coverage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Creator> getCreator() {
		if (creator == null) {
			creator = new EObjectContainmentEList<Creator>(Creator.class, this, DublinCorePackage.METADATA__CREATOR);
		}
		return creator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Date> getDate() {
		if (date == null) {
			date = new EObjectContainmentEList<Date>(Date.class, this, DublinCorePackage.METADATA__DATE);
		}
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Description> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<Description>(Description.class, this, DublinCorePackage.METADATA__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Format> getFormat() {
		if (format == null) {
			format = new EObjectContainmentEList<Format>(Format.class, this, DublinCorePackage.METADATA__FORMAT);
		}
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Identifier> getIdentifier() {
		if (identifier == null) {
			identifier = new EObjectContainmentEList<Identifier>(Identifier.class, this, DublinCorePackage.METADATA__IDENTIFIER);
		}
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Language> getLanguage() {
		if (language == null) {
			language = new EObjectContainmentEList<Language>(Language.class, this, DublinCorePackage.METADATA__LANGUAGE);
		}
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Publisher> getPublisher() {
		if (publisher == null) {
			publisher = new EObjectContainmentEList<Publisher>(Publisher.class, this, DublinCorePackage.METADATA__PUBLISHER);
		}
		return publisher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Relation> getRelation() {
		if (relation == null) {
			relation = new EObjectContainmentEList<Relation>(Relation.class, this, DublinCorePackage.METADATA__RELATION);
		}
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Rights> getRights() {
		if (rights == null) {
			rights = new EObjectContainmentEList<Rights>(Rights.class, this, DublinCorePackage.METADATA__RIGHTS);
		}
		return rights;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Source> getSource() {
		if (source == null) {
			source = new EObjectContainmentEList<Source>(Source.class, this, DublinCorePackage.METADATA__SOURCE);
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Subject> getSubject() {
		if (subject == null) {
			subject = new EObjectContainmentEList<Subject>(Subject.class, this, DublinCorePackage.METADATA__SUBJECT);
		}
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Title> getTitle() {
		if (title == null) {
			title = new EObjectContainmentEList<Title>(Title.class, this, DublinCorePackage.METADATA__TITLE);
		}
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getType() {
		if (type == null) {
			type = new EObjectContainmentEList<Type>(Type.class, this, DublinCorePackage.METADATA__TYPE);
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Audience> getAudience() {
		if (audience == null) {
			audience = new EObjectContainmentEList<Audience>(Audience.class, this, DublinCorePackage.METADATA__AUDIENCE);
		}
		return audience;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Provenance> getProvenance() {
		if (provenance == null) {
			provenance = new EObjectContainmentEList<Provenance>(Provenance.class, this, DublinCorePackage.METADATA__PROVENANCE);
		}
		return provenance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RightsHolder> getRightsHolder() {
		if (rightsHolder == null) {
			rightsHolder = new EObjectContainmentEList<RightsHolder>(RightsHolder.class, this, DublinCorePackage.METADATA__RIGHTS_HOLDER);
		}
		return rightsHolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
				return ((InternalEList<?>)getContributor()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__COVERAGE:
				return ((InternalEList<?>)getCoverage()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__CREATOR:
				return ((InternalEList<?>)getCreator()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__DATE:
				return ((InternalEList<?>)getDate()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__FORMAT:
				return ((InternalEList<?>)getFormat()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__IDENTIFIER:
				return ((InternalEList<?>)getIdentifier()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__LANGUAGE:
				return ((InternalEList<?>)getLanguage()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__PUBLISHER:
				return ((InternalEList<?>)getPublisher()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__RELATION:
				return ((InternalEList<?>)getRelation()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__RIGHTS:
				return ((InternalEList<?>)getRights()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__SOURCE:
				return ((InternalEList<?>)getSource()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__SUBJECT:
				return ((InternalEList<?>)getSubject()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__TITLE:
				return ((InternalEList<?>)getTitle()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__TYPE:
				return ((InternalEList<?>)getType()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__AUDIENCE:
				return ((InternalEList<?>)getAudience()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__PROVENANCE:
				return ((InternalEList<?>)getProvenance()).basicRemove(otherEnd, msgs);
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				return ((InternalEList<?>)getRightsHolder()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
				return getContributor();
			case DublinCorePackage.METADATA__COVERAGE:
				return getCoverage();
			case DublinCorePackage.METADATA__CREATOR:
				return getCreator();
			case DublinCorePackage.METADATA__DATE:
				return getDate();
			case DublinCorePackage.METADATA__DESCRIPTION:
				return getDescription();
			case DublinCorePackage.METADATA__FORMAT:
				return getFormat();
			case DublinCorePackage.METADATA__IDENTIFIER:
				return getIdentifier();
			case DublinCorePackage.METADATA__LANGUAGE:
				return getLanguage();
			case DublinCorePackage.METADATA__PUBLISHER:
				return getPublisher();
			case DublinCorePackage.METADATA__RELATION:
				return getRelation();
			case DublinCorePackage.METADATA__RIGHTS:
				return getRights();
			case DublinCorePackage.METADATA__SOURCE:
				return getSource();
			case DublinCorePackage.METADATA__SUBJECT:
				return getSubject();
			case DublinCorePackage.METADATA__TITLE:
				return getTitle();
			case DublinCorePackage.METADATA__TYPE:
				return getType();
			case DublinCorePackage.METADATA__AUDIENCE:
				return getAudience();
			case DublinCorePackage.METADATA__PROVENANCE:
				return getProvenance();
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				return getRightsHolder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
				getContributor().clear();
				getContributor().addAll((Collection<? extends Contributor>)newValue);
				return;
			case DublinCorePackage.METADATA__COVERAGE:
				getCoverage().clear();
				getCoverage().addAll((Collection<? extends Coverage>)newValue);
				return;
			case DublinCorePackage.METADATA__CREATOR:
				getCreator().clear();
				getCreator().addAll((Collection<? extends Creator>)newValue);
				return;
			case DublinCorePackage.METADATA__DATE:
				getDate().clear();
				getDate().addAll((Collection<? extends Date>)newValue);
				return;
			case DublinCorePackage.METADATA__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends Description>)newValue);
				return;
			case DublinCorePackage.METADATA__FORMAT:
				getFormat().clear();
				getFormat().addAll((Collection<? extends Format>)newValue);
				return;
			case DublinCorePackage.METADATA__IDENTIFIER:
				getIdentifier().clear();
				getIdentifier().addAll((Collection<? extends Identifier>)newValue);
				return;
			case DublinCorePackage.METADATA__LANGUAGE:
				getLanguage().clear();
				getLanguage().addAll((Collection<? extends Language>)newValue);
				return;
			case DublinCorePackage.METADATA__PUBLISHER:
				getPublisher().clear();
				getPublisher().addAll((Collection<? extends Publisher>)newValue);
				return;
			case DublinCorePackage.METADATA__RELATION:
				getRelation().clear();
				getRelation().addAll((Collection<? extends Relation>)newValue);
				return;
			case DublinCorePackage.METADATA__RIGHTS:
				getRights().clear();
				getRights().addAll((Collection<? extends Rights>)newValue);
				return;
			case DublinCorePackage.METADATA__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends Source>)newValue);
				return;
			case DublinCorePackage.METADATA__SUBJECT:
				getSubject().clear();
				getSubject().addAll((Collection<? extends Subject>)newValue);
				return;
			case DublinCorePackage.METADATA__TITLE:
				getTitle().clear();
				getTitle().addAll((Collection<? extends Title>)newValue);
				return;
			case DublinCorePackage.METADATA__TYPE:
				getType().clear();
				getType().addAll((Collection<? extends Type>)newValue);
				return;
			case DublinCorePackage.METADATA__AUDIENCE:
				getAudience().clear();
				getAudience().addAll((Collection<? extends Audience>)newValue);
				return;
			case DublinCorePackage.METADATA__PROVENANCE:
				getProvenance().clear();
				getProvenance().addAll((Collection<? extends Provenance>)newValue);
				return;
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				getRightsHolder().clear();
				getRightsHolder().addAll((Collection<? extends RightsHolder>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
				getContributor().clear();
				return;
			case DublinCorePackage.METADATA__COVERAGE:
				getCoverage().clear();
				return;
			case DublinCorePackage.METADATA__CREATOR:
				getCreator().clear();
				return;
			case DublinCorePackage.METADATA__DATE:
				getDate().clear();
				return;
			case DublinCorePackage.METADATA__DESCRIPTION:
				getDescription().clear();
				return;
			case DublinCorePackage.METADATA__FORMAT:
				getFormat().clear();
				return;
			case DublinCorePackage.METADATA__IDENTIFIER:
				getIdentifier().clear();
				return;
			case DublinCorePackage.METADATA__LANGUAGE:
				getLanguage().clear();
				return;
			case DublinCorePackage.METADATA__PUBLISHER:
				getPublisher().clear();
				return;
			case DublinCorePackage.METADATA__RELATION:
				getRelation().clear();
				return;
			case DublinCorePackage.METADATA__RIGHTS:
				getRights().clear();
				return;
			case DublinCorePackage.METADATA__SOURCE:
				getSource().clear();
				return;
			case DublinCorePackage.METADATA__SUBJECT:
				getSubject().clear();
				return;
			case DublinCorePackage.METADATA__TITLE:
				getTitle().clear();
				return;
			case DublinCorePackage.METADATA__TYPE:
				getType().clear();
				return;
			case DublinCorePackage.METADATA__AUDIENCE:
				getAudience().clear();
				return;
			case DublinCorePackage.METADATA__PROVENANCE:
				getProvenance().clear();
				return;
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				getRightsHolder().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
				return contributor != null && !contributor.isEmpty();
			case DublinCorePackage.METADATA__COVERAGE:
				return coverage != null && !coverage.isEmpty();
			case DublinCorePackage.METADATA__CREATOR:
				return creator != null && !creator.isEmpty();
			case DublinCorePackage.METADATA__DATE:
				return date != null && !date.isEmpty();
			case DublinCorePackage.METADATA__DESCRIPTION:
				return description != null && !description.isEmpty();
			case DublinCorePackage.METADATA__FORMAT:
				return format != null && !format.isEmpty();
			case DublinCorePackage.METADATA__IDENTIFIER:
				return identifier != null && !identifier.isEmpty();
			case DublinCorePackage.METADATA__LANGUAGE:
				return language != null && !language.isEmpty();
			case DublinCorePackage.METADATA__PUBLISHER:
				return publisher != null && !publisher.isEmpty();
			case DublinCorePackage.METADATA__RELATION:
				return relation != null && !relation.isEmpty();
			case DublinCorePackage.METADATA__RIGHTS:
				return rights != null && !rights.isEmpty();
			case DublinCorePackage.METADATA__SOURCE:
				return source != null && !source.isEmpty();
			case DublinCorePackage.METADATA__SUBJECT:
				return subject != null && !subject.isEmpty();
			case DublinCorePackage.METADATA__TITLE:
				return title != null && !title.isEmpty();
			case DublinCorePackage.METADATA__TYPE:
				return type != null && !type.isEmpty();
			case DublinCorePackage.METADATA__AUDIENCE:
				return audience != null && !audience.isEmpty();
			case DublinCorePackage.METADATA__PROVENANCE:
				return provenance != null && !provenance.isEmpty();
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				return rightsHolder != null && !rightsHolder.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MetadataImpl
