/**
 * <copyright>
 * </copyright>
 *
 * $Id: DublinCorePackage.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.dublincore.DublinCore.DublinCoreFactory
 * @model kind="package"
 * @generated
 */
public interface DublinCorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "DublinCore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://dublincore.org";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.dublincore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DublinCorePackage eINSTANCE = org.dublincore.DublinCore.impl.DublinCorePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.MetadataImpl <em>Metadata</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.MetadataImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMetadata()
	 * @generated
	 */
	int METADATA = 0;

	/**
	 * The feature id for the '<em><b>Contributor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__CONTRIBUTOR = 0;

	/**
	 * The feature id for the '<em><b>Coverage</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__COVERAGE = 1;

	/**
	 * The feature id for the '<em><b>Creator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__CREATOR = 2;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__DATE = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Format</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__FORMAT = 5;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__IDENTIFIER = 6;

	/**
	 * The feature id for the '<em><b>Language</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__LANGUAGE = 7;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__PUBLISHER = 8;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__RELATION = 9;

	/**
	 * The feature id for the '<em><b>Rights</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__RIGHTS = 10;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__SOURCE = 11;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__SUBJECT = 12;

	/**
	 * The feature id for the '<em><b>Title</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__TITLE = 13;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__TYPE = 14;

	/**
	 * The feature id for the '<em><b>Audience</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__AUDIENCE = 15;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__PROVENANCE = 16;

	/**
	 * The feature id for the '<em><b>Rights Holder</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA__RIGHTS_HOLDER = 17;

	/**
	 * The number of structural features of the '<em>Metadata</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METADATA_FEATURE_COUNT = 18;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ElementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 17;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ContributorImpl <em>Contributor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ContributorImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getContributor()
	 * @generated
	 */
	int CONTRIBUTOR = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTOR__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Contributor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTOR_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.CoverageImpl <em>Coverage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.CoverageImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCoverage()
	 * @generated
	 */
	int COVERAGE = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COVERAGE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Coverage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COVERAGE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.CreatorImpl <em>Creator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.CreatorImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCreator()
	 * @generated
	 */
	int CREATOR = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATOR__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATOR_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DateImpl <em>Date</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DateImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDate()
	 * @generated
	 */
	int DATE = 4;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE__DATE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Date</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DescriptionImpl <em>Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DescriptionImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDescription()
	 * @generated
	 */
	int DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.FormatImpl <em>Format</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.FormatImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getFormat()
	 * @generated
	 */
	int FORMAT = 6;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAT__NATURE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAT__EXTENSION = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IdentifierImpl <em>Identifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IdentifierImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIdentifier()
	 * @generated
	 */
	int IDENTIFIER = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__ID = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.LanguageImpl <em>Language</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.LanguageImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getLanguage()
	 * @generated
	 */
	int LANGUAGE = 8;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANGUAGE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Language</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANGUAGE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.PublisherImpl <em>Publisher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.PublisherImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getPublisher()
	 * @generated
	 */
	int PUBLISHER = 9;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHER__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Publisher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHER_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RelationImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 10;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__URI = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RightsImpl <em>Rights</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RightsImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRights()
	 * @generated
	 */
	int RIGHTS = 11;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS__TEXT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS__URI = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS__QUALIFIER = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Rights</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.SourceImpl <em>Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.SourceImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getSource()
	 * @generated
	 */
	int SOURCE = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.SubjectImpl <em>Subject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.SubjectImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getSubject()
	 * @generated
	 */
	int SUBJECT = 13;

	/**
	 * The feature id for the '<em><b>Keywords</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT__KEYWORDS = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Subject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.TitleImpl <em>Title</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.TitleImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getTitle()
	 * @generated
	 */
	int TITLE = 14;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Title</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.TypeImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getType()
	 * @generated
	 */
	int TYPE = 15;

	/**
	 * The feature id for the '<em><b>Diagram Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__DIAGRAM_TYPE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Metamodel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__METAMODEL = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Metamodel Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__METAMODEL_VERSION = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DatasetImpl <em>Dataset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DatasetImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDataset()
	 * @generated
	 */
	int DATASET = 16;

	/**
	 * The number of structural features of the '<em>Dataset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATASET_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DateRefinementImpl <em>Date Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DateRefinementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateRefinement()
	 * @generated
	 */
	int DATE_REFINEMENT = 18;

	/**
	 * The number of structural features of the '<em>Date Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_REFINEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DescriptionRefinementImpl <em>Description Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DescriptionRefinementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDescriptionRefinement()
	 * @generated
	 */
	int DESCRIPTION_REFINEMENT = 19;

	/**
	 * The number of structural features of the '<em>Description Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_REFINEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RelationRefinementImpl <em>Relation Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RelationRefinementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRelationRefinement()
	 * @generated
	 */
	int RELATION_REFINEMENT = 20;

	/**
	 * The number of structural features of the '<em>Relation Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_REFINEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RightsRefinementImpl <em>Rights Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RightsRefinementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRightsRefinement()
	 * @generated
	 */
	int RIGHTS_REFINEMENT = 21;

	/**
	 * The number of structural features of the '<em>Rights Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS_REFINEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AlternativeImpl <em>Alternative</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AlternativeImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAlternative()
	 * @generated
	 */
	int ALTERNATIVE = 22;

	/**
	 * The number of structural features of the '<em>Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERNATIVE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AbstractImpl <em>Abstract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AbstractImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAbstract()
	 * @generated
	 */
	int ABSTRACT = 23;

	/**
	 * The number of structural features of the '<em>Abstract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE_COUNT = DESCRIPTION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.TableOfContentsImpl <em>Table Of Contents</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.TableOfContentsImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getTableOfContents()
	 * @generated
	 */
	int TABLE_OF_CONTENTS = 24;

	/**
	 * The number of structural features of the '<em>Table Of Contents</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OF_CONTENTS_FEATURE_COUNT = DESCRIPTION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AvailableImpl <em>Available</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AvailableImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAvailable()
	 * @generated
	 */
	int AVAILABLE = 25;

	/**
	 * The number of structural features of the '<em>Available</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABLE_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.CreatedImpl <em>Created</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.CreatedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCreated()
	 * @generated
	 */
	int CREATED = 26;

	/**
	 * The number of structural features of the '<em>Created</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DateAcceptedImpl <em>Date Accepted</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DateAcceptedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateAccepted()
	 * @generated
	 */
	int DATE_ACCEPTED = 27;

	/**
	 * The number of structural features of the '<em>Date Accepted</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_ACCEPTED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DateCopyrightedImpl <em>Date Copyrighted</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DateCopyrightedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateCopyrighted()
	 * @generated
	 */
	int DATE_COPYRIGHTED = 28;

	/**
	 * The number of structural features of the '<em>Date Copyrighted</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_COPYRIGHTED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.DateSubmittedImpl <em>Date Submitted</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.DateSubmittedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateSubmitted()
	 * @generated
	 */
	int DATE_SUBMITTED = 29;

	/**
	 * The number of structural features of the '<em>Date Submitted</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_SUBMITTED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IssuedImpl <em>Issued</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IssuedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIssued()
	 * @generated
	 */
	int ISSUED = 30;

	/**
	 * The number of structural features of the '<em>Issued</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISSUED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ModifiedImpl <em>Modified</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ModifiedImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getModified()
	 * @generated
	 */
	int MODIFIED = 31;

	/**
	 * The number of structural features of the '<em>Modified</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODIFIED_FEATURE_COUNT = DATE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsPartOfImpl <em>Is Part Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsPartOfImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsPartOf()
	 * @generated
	 */
	int IS_PART_OF = 32;

	/**
	 * The number of structural features of the '<em>Is Part Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_PART_OF_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsVersionOfImpl <em>Is Version Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsVersionOfImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsVersionOf()
	 * @generated
	 */
	int IS_VERSION_OF = 33;

	/**
	 * The number of structural features of the '<em>Is Version Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_VERSION_OF_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsFormatOfImpl <em>Is Format Of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsFormatOfImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsFormatOf()
	 * @generated
	 */
	int IS_FORMAT_OF = 34;

	/**
	 * The number of structural features of the '<em>Is Format Of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_FORMAT_OF_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.HasFormatImpl <em>Has Format</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.HasFormatImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getHasFormat()
	 * @generated
	 */
	int HAS_FORMAT = 35;

	/**
	 * The number of structural features of the '<em>Has Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_FORMAT_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsReferencedByImpl <em>Is Referenced By</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsReferencedByImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsReferencedBy()
	 * @generated
	 */
	int IS_REFERENCED_BY = 36;

	/**
	 * The number of structural features of the '<em>Is Referenced By</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_REFERENCED_BY_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ReferencesImpl <em>References</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ReferencesImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getReferences()
	 * @generated
	 */
	int REFERENCES = 37;

	/**
	 * The number of structural features of the '<em>References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsBasisForImpl <em>Is Basis For</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsBasisForImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsBasisFor()
	 * @generated
	 */
	int IS_BASIS_FOR = 38;

	/**
	 * The number of structural features of the '<em>Is Basis For</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_BASIS_FOR_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.IsBasedOnImpl <em>Is Based On</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.IsBasedOnImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsBasedOn()
	 * @generated
	 */
	int IS_BASED_ON = 39;

	/**
	 * The number of structural features of the '<em>Is Based On</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_BASED_ON_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RequiresImpl <em>Requires</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RequiresImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRequires()
	 * @generated
	 */
	int REQUIRES = 40;

	/**
	 * The number of structural features of the '<em>Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AccessRightsImpl <em>Access Rights</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AccessRightsImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAccessRights()
	 * @generated
	 */
	int ACCESS_RIGHTS = 41;

	/**
	 * The number of structural features of the '<em>Access Rights</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RIGHTS_FEATURE_COUNT = RIGHTS_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.LicenseImpl <em>License</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.LicenseImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getLicense()
	 * @generated
	 */
	int LICENSE = 42;

	/**
	 * The number of structural features of the '<em>License</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LICENSE_FEATURE_COUNT = RIGHTS_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AudienceImpl <em>Audience</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AudienceImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAudience()
	 * @generated
	 */
	int AUDIENCE = 43;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIENCE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIENCE__QUALIFIER = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Audience</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIENCE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.BibliographicCitationImpl <em>Bibliographic Citation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.BibliographicCitationImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getBibliographicCitation()
	 * @generated
	 */
	int BIBLIOGRAPHIC_CITATION = 44;

	/**
	 * The number of structural features of the '<em>Bibliographic Citation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIBLIOGRAPHIC_CITATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ProvenanceImpl <em>Provenance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ProvenanceImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getProvenance()
	 * @generated
	 */
	int PROVENANCE = 45;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Provenance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.RightsHolderImpl <em>Rights Holder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.RightsHolderImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRightsHolder()
	 * @generated
	 */
	int RIGHTS_HOLDER = 46;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS_HOLDER__VALUE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Rights Holder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RIGHTS_HOLDER_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.AudienceRefinementImpl <em>Audience Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.AudienceRefinementImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAudienceRefinement()
	 * @generated
	 */
	int AUDIENCE_REFINEMENT = 47;

	/**
	 * The number of structural features of the '<em>Audience Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIENCE_REFINEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.MediatorImpl <em>Mediator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.MediatorImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMediator()
	 * @generated
	 */
	int MEDIATOR = 48;

	/**
	 * The number of structural features of the '<em>Mediator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIATOR_FEATURE_COUNT = AUDIENCE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.EducationLevelImpl <em>Education Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.EducationLevelImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getEducationLevel()
	 * @generated
	 */
	int EDUCATION_LEVEL = 49;

	/**
	 * The number of structural features of the '<em>Education Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDUCATION_LEVEL_FEATURE_COUNT = AUDIENCE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.impl.ExternalStorageImpl <em>External Storage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.impl.ExternalStorageImpl
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getExternalStorage()
	 * @generated
	 */
	int EXTERNAL_STORAGE = 50;

	/**
	 * The number of structural features of the '<em>External Storage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_STORAGE_FEATURE_COUNT = RELATION_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.MetamodelName <em>Metamodel Name</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.MetamodelName
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMetamodelName()
	 * @generated
	 */
	int METAMODEL_NAME = 51;

	/**
	 * The meta object id for the '{@link org.dublincore.DublinCore.FormatNature <em>Format Nature</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.dublincore.DublinCore.FormatNature
	 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getFormatNature()
	 * @generated
	 */
	int FORMAT_NATURE = 52;


	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Metadata <em>Metadata</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metadata</em>'.
	 * @see org.dublincore.DublinCore.Metadata
	 * @generated
	 */
	EClass getMetadata();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getContributor <em>Contributor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contributor</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getContributor()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Contributor();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getCoverage <em>Coverage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Coverage</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getCoverage()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Coverage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getCreator <em>Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Creator</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getCreator()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Creator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Date</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getDate()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Date();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Description</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getDescription()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Format</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getFormat()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Format();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Identifier</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getIdentifier()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Identifier();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Language</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getLanguage()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Language();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Publisher</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getPublisher()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Publisher();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getRelation()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Relation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getRights <em>Rights</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rights</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getRights()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Rights();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Source</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getSource()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Source();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subject</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getSubject()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Subject();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Title</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getTitle()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Title();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Type</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getType()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getAudience <em>Audience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Audience</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getAudience()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Audience();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getProvenance <em>Provenance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provenance</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getProvenance()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_Provenance();

	/**
	 * Returns the meta object for the containment reference list '{@link org.dublincore.DublinCore.Metadata#getRightsHolder <em>Rights Holder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rights Holder</em>'.
	 * @see org.dublincore.DublinCore.Metadata#getRightsHolder()
	 * @see #getMetadata()
	 * @generated
	 */
	EReference getMetadata_RightsHolder();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Contributor <em>Contributor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contributor</em>'.
	 * @see org.dublincore.DublinCore.Contributor
	 * @generated
	 */
	EClass getContributor();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Contributor#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Contributor#getValue()
	 * @see #getContributor()
	 * @generated
	 */
	EAttribute getContributor_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Coverage <em>Coverage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coverage</em>'.
	 * @see org.dublincore.DublinCore.Coverage
	 * @generated
	 */
	EClass getCoverage();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Coverage#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Coverage#getValue()
	 * @see #getCoverage()
	 * @generated
	 */
	EAttribute getCoverage_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Creator <em>Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Creator</em>'.
	 * @see org.dublincore.DublinCore.Creator
	 * @generated
	 */
	EClass getCreator();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Creator#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Creator#getValue()
	 * @see #getCreator()
	 * @generated
	 */
	EAttribute getCreator_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Date <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date</em>'.
	 * @see org.dublincore.DublinCore.Date
	 * @generated
	 */
	EClass getDate();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Date#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see org.dublincore.DublinCore.Date#getDate()
	 * @see #getDate()
	 * @generated
	 */
	EAttribute getDate_Date();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Date#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Date#getQualifier()
	 * @see #getDate()
	 * @generated
	 */
	EReference getDate_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Description <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Description</em>'.
	 * @see org.dublincore.DublinCore.Description
	 * @generated
	 */
	EClass getDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Description#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Description#getValue()
	 * @see #getDescription()
	 * @generated
	 */
	EAttribute getDescription_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Description#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Description#getQualifier()
	 * @see #getDescription()
	 * @generated
	 */
	EReference getDescription_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Format <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Format</em>'.
	 * @see org.dublincore.DublinCore.Format
	 * @generated
	 */
	EClass getFormat();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Format#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature</em>'.
	 * @see org.dublincore.DublinCore.Format#getNature()
	 * @see #getFormat()
	 * @generated
	 */
	EAttribute getFormat_Nature();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Format#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extension</em>'.
	 * @see org.dublincore.DublinCore.Format#getExtension()
	 * @see #getFormat()
	 * @generated
	 */
	EAttribute getFormat_Extension();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifier</em>'.
	 * @see org.dublincore.DublinCore.Identifier
	 * @generated
	 */
	EClass getIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Identifier#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.dublincore.DublinCore.Identifier#getId()
	 * @see #getIdentifier()
	 * @generated
	 */
	EAttribute getIdentifier_Id();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Identifier#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Identifier#getQualifier()
	 * @see #getIdentifier()
	 * @generated
	 */
	EReference getIdentifier_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Language <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Language</em>'.
	 * @see org.dublincore.DublinCore.Language
	 * @generated
	 */
	EClass getLanguage();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Language#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Language#getValue()
	 * @see #getLanguage()
	 * @generated
	 */
	EAttribute getLanguage_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Publisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Publisher</em>'.
	 * @see org.dublincore.DublinCore.Publisher
	 * @generated
	 */
	EClass getPublisher();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Publisher#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Publisher#getValue()
	 * @see #getPublisher()
	 * @generated
	 */
	EAttribute getPublisher_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see org.dublincore.DublinCore.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Relation#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.dublincore.DublinCore.Relation#getUri()
	 * @see #getRelation()
	 * @generated
	 */
	EAttribute getRelation_Uri();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Relation#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Relation#getQualifier()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Rights <em>Rights</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rights</em>'.
	 * @see org.dublincore.DublinCore.Rights
	 * @generated
	 */
	EClass getRights();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Rights#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.dublincore.DublinCore.Rights#getText()
	 * @see #getRights()
	 * @generated
	 */
	EAttribute getRights_Text();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Rights#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.dublincore.DublinCore.Rights#getUri()
	 * @see #getRights()
	 * @generated
	 */
	EAttribute getRights_Uri();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Rights#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Rights#getQualifier()
	 * @see #getRights()
	 * @generated
	 */
	EReference getRights_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Source <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source</em>'.
	 * @see org.dublincore.DublinCore.Source
	 * @generated
	 */
	EClass getSource();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Source#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Source#getValue()
	 * @see #getSource()
	 * @generated
	 */
	EAttribute getSource_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Subject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subject</em>'.
	 * @see org.dublincore.DublinCore.Subject
	 * @generated
	 */
	EClass getSubject();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Subject#getKeywords <em>Keywords</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Keywords</em>'.
	 * @see org.dublincore.DublinCore.Subject#getKeywords()
	 * @see #getSubject()
	 * @generated
	 */
	EAttribute getSubject_Keywords();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Title <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Title</em>'.
	 * @see org.dublincore.DublinCore.Title
	 * @generated
	 */
	EClass getTitle();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Title#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Title#getValue()
	 * @see #getTitle()
	 * @generated
	 */
	EAttribute getTitle_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Title#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Title#getQualifier()
	 * @see #getTitle()
	 * @generated
	 */
	EReference getTitle_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see org.dublincore.DublinCore.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Type#getDiagramType <em>Diagram Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diagram Type</em>'.
	 * @see org.dublincore.DublinCore.Type#getDiagramType()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_DiagramType();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Type#getMetamodel <em>Metamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel</em>'.
	 * @see org.dublincore.DublinCore.Type#getMetamodel()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Metamodel();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Type#getMetamodelVersion <em>Metamodel Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel Version</em>'.
	 * @see org.dublincore.DublinCore.Type#getMetamodelVersion()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_MetamodelVersion();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Dataset <em>Dataset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dataset</em>'.
	 * @see org.dublincore.DublinCore.Dataset
	 * @generated
	 */
	EClass getDataset();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see org.dublincore.DublinCore.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.DateRefinement <em>Date Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Refinement</em>'.
	 * @see org.dublincore.DublinCore.DateRefinement
	 * @generated
	 */
	EClass getDateRefinement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.DescriptionRefinement <em>Description Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Description Refinement</em>'.
	 * @see org.dublincore.DublinCore.DescriptionRefinement
	 * @generated
	 */
	EClass getDescriptionRefinement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.RelationRefinement <em>Relation Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation Refinement</em>'.
	 * @see org.dublincore.DublinCore.RelationRefinement
	 * @generated
	 */
	EClass getRelationRefinement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.RightsRefinement <em>Rights Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rights Refinement</em>'.
	 * @see org.dublincore.DublinCore.RightsRefinement
	 * @generated
	 */
	EClass getRightsRefinement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Alternative <em>Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alternative</em>'.
	 * @see org.dublincore.DublinCore.Alternative
	 * @generated
	 */
	EClass getAlternative();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Abstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract</em>'.
	 * @see org.dublincore.DublinCore.Abstract
	 * @generated
	 */
	EClass getAbstract();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.TableOfContents <em>Table Of Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Of Contents</em>'.
	 * @see org.dublincore.DublinCore.TableOfContents
	 * @generated
	 */
	EClass getTableOfContents();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Available <em>Available</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Available</em>'.
	 * @see org.dublincore.DublinCore.Available
	 * @generated
	 */
	EClass getAvailable();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Created <em>Created</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Created</em>'.
	 * @see org.dublincore.DublinCore.Created
	 * @generated
	 */
	EClass getCreated();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.DateAccepted <em>Date Accepted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Accepted</em>'.
	 * @see org.dublincore.DublinCore.DateAccepted
	 * @generated
	 */
	EClass getDateAccepted();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.DateCopyrighted <em>Date Copyrighted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Copyrighted</em>'.
	 * @see org.dublincore.DublinCore.DateCopyrighted
	 * @generated
	 */
	EClass getDateCopyrighted();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.DateSubmitted <em>Date Submitted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Submitted</em>'.
	 * @see org.dublincore.DublinCore.DateSubmitted
	 * @generated
	 */
	EClass getDateSubmitted();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Issued <em>Issued</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Issued</em>'.
	 * @see org.dublincore.DublinCore.Issued
	 * @generated
	 */
	EClass getIssued();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Modified <em>Modified</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modified</em>'.
	 * @see org.dublincore.DublinCore.Modified
	 * @generated
	 */
	EClass getModified();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsPartOf <em>Is Part Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Part Of</em>'.
	 * @see org.dublincore.DublinCore.IsPartOf
	 * @generated
	 */
	EClass getIsPartOf();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsVersionOf <em>Is Version Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Version Of</em>'.
	 * @see org.dublincore.DublinCore.IsVersionOf
	 * @generated
	 */
	EClass getIsVersionOf();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsFormatOf <em>Is Format Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Format Of</em>'.
	 * @see org.dublincore.DublinCore.IsFormatOf
	 * @generated
	 */
	EClass getIsFormatOf();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.HasFormat <em>Has Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Format</em>'.
	 * @see org.dublincore.DublinCore.HasFormat
	 * @generated
	 */
	EClass getHasFormat();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsReferencedBy <em>Is Referenced By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Referenced By</em>'.
	 * @see org.dublincore.DublinCore.IsReferencedBy
	 * @generated
	 */
	EClass getIsReferencedBy();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.References <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>References</em>'.
	 * @see org.dublincore.DublinCore.References
	 * @generated
	 */
	EClass getReferences();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsBasisFor <em>Is Basis For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Basis For</em>'.
	 * @see org.dublincore.DublinCore.IsBasisFor
	 * @generated
	 */
	EClass getIsBasisFor();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.IsBasedOn <em>Is Based On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Based On</em>'.
	 * @see org.dublincore.DublinCore.IsBasedOn
	 * @generated
	 */
	EClass getIsBasedOn();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Requires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requires</em>'.
	 * @see org.dublincore.DublinCore.Requires
	 * @generated
	 */
	EClass getRequires();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.AccessRights <em>Access Rights</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Rights</em>'.
	 * @see org.dublincore.DublinCore.AccessRights
	 * @generated
	 */
	EClass getAccessRights();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.License <em>License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>License</em>'.
	 * @see org.dublincore.DublinCore.License
	 * @generated
	 */
	EClass getLicense();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Audience <em>Audience</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Audience</em>'.
	 * @see org.dublincore.DublinCore.Audience
	 * @generated
	 */
	EClass getAudience();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Audience#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Audience#getValue()
	 * @see #getAudience()
	 * @generated
	 */
	EAttribute getAudience_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.dublincore.DublinCore.Audience#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see org.dublincore.DublinCore.Audience#getQualifier()
	 * @see #getAudience()
	 * @generated
	 */
	EReference getAudience_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.BibliographicCitation <em>Bibliographic Citation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bibliographic Citation</em>'.
	 * @see org.dublincore.DublinCore.BibliographicCitation
	 * @generated
	 */
	EClass getBibliographicCitation();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Provenance <em>Provenance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provenance</em>'.
	 * @see org.dublincore.DublinCore.Provenance
	 * @generated
	 */
	EClass getProvenance();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.Provenance#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.Provenance#getValue()
	 * @see #getProvenance()
	 * @generated
	 */
	EAttribute getProvenance_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.RightsHolder <em>Rights Holder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rights Holder</em>'.
	 * @see org.dublincore.DublinCore.RightsHolder
	 * @generated
	 */
	EClass getRightsHolder();

	/**
	 * Returns the meta object for the attribute '{@link org.dublincore.DublinCore.RightsHolder#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.dublincore.DublinCore.RightsHolder#getValue()
	 * @see #getRightsHolder()
	 * @generated
	 */
	EAttribute getRightsHolder_Value();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.AudienceRefinement <em>Audience Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Audience Refinement</em>'.
	 * @see org.dublincore.DublinCore.AudienceRefinement
	 * @generated
	 */
	EClass getAudienceRefinement();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.Mediator <em>Mediator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mediator</em>'.
	 * @see org.dublincore.DublinCore.Mediator
	 * @generated
	 */
	EClass getMediator();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.EducationLevel <em>Education Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Education Level</em>'.
	 * @see org.dublincore.DublinCore.EducationLevel
	 * @generated
	 */
	EClass getEducationLevel();

	/**
	 * Returns the meta object for class '{@link org.dublincore.DublinCore.ExternalStorage <em>External Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Storage</em>'.
	 * @see org.dublincore.DublinCore.ExternalStorage
	 * @generated
	 */
	EClass getExternalStorage();

	/**
	 * Returns the meta object for enum '{@link org.dublincore.DublinCore.MetamodelName <em>Metamodel Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Metamodel Name</em>'.
	 * @see org.dublincore.DublinCore.MetamodelName
	 * @generated
	 */
	EEnum getMetamodelName();

	/**
	 * Returns the meta object for enum '{@link org.dublincore.DublinCore.FormatNature <em>Format Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Format Nature</em>'.
	 * @see org.dublincore.DublinCore.FormatNature
	 * @generated
	 */
	EEnum getFormatNature();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DublinCoreFactory getDublinCoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.MetadataImpl <em>Metadata</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.MetadataImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMetadata()
		 * @generated
		 */
		EClass METADATA = eINSTANCE.getMetadata();

		/**
		 * The meta object literal for the '<em><b>Contributor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__CONTRIBUTOR = eINSTANCE.getMetadata_Contributor();

		/**
		 * The meta object literal for the '<em><b>Coverage</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__COVERAGE = eINSTANCE.getMetadata_Coverage();

		/**
		 * The meta object literal for the '<em><b>Creator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__CREATOR = eINSTANCE.getMetadata_Creator();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__DATE = eINSTANCE.getMetadata_Date();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__DESCRIPTION = eINSTANCE.getMetadata_Description();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__FORMAT = eINSTANCE.getMetadata_Format();

		/**
		 * The meta object literal for the '<em><b>Identifier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__IDENTIFIER = eINSTANCE.getMetadata_Identifier();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__LANGUAGE = eINSTANCE.getMetadata_Language();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__PUBLISHER = eINSTANCE.getMetadata_Publisher();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__RELATION = eINSTANCE.getMetadata_Relation();

		/**
		 * The meta object literal for the '<em><b>Rights</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__RIGHTS = eINSTANCE.getMetadata_Rights();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__SOURCE = eINSTANCE.getMetadata_Source();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__SUBJECT = eINSTANCE.getMetadata_Subject();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__TITLE = eINSTANCE.getMetadata_Title();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__TYPE = eINSTANCE.getMetadata_Type();

		/**
		 * The meta object literal for the '<em><b>Audience</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__AUDIENCE = eINSTANCE.getMetadata_Audience();

		/**
		 * The meta object literal for the '<em><b>Provenance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__PROVENANCE = eINSTANCE.getMetadata_Provenance();

		/**
		 * The meta object literal for the '<em><b>Rights Holder</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METADATA__RIGHTS_HOLDER = eINSTANCE.getMetadata_RightsHolder();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ContributorImpl <em>Contributor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ContributorImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getContributor()
		 * @generated
		 */
		EClass CONTRIBUTOR = eINSTANCE.getContributor();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTRIBUTOR__VALUE = eINSTANCE.getContributor_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.CoverageImpl <em>Coverage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.CoverageImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCoverage()
		 * @generated
		 */
		EClass COVERAGE = eINSTANCE.getCoverage();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COVERAGE__VALUE = eINSTANCE.getCoverage_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.CreatorImpl <em>Creator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.CreatorImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCreator()
		 * @generated
		 */
		EClass CREATOR = eINSTANCE.getCreator();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATOR__VALUE = eINSTANCE.getCreator_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DateImpl <em>Date</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DateImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDate()
		 * @generated
		 */
		EClass DATE = eINSTANCE.getDate();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATE__DATE = eINSTANCE.getDate_Date();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATE__QUALIFIER = eINSTANCE.getDate_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DescriptionImpl <em>Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DescriptionImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDescription()
		 * @generated
		 */
		EClass DESCRIPTION = eINSTANCE.getDescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIPTION__VALUE = eINSTANCE.getDescription_Value();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESCRIPTION__QUALIFIER = eINSTANCE.getDescription_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.FormatImpl <em>Format</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.FormatImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getFormat()
		 * @generated
		 */
		EClass FORMAT = eINSTANCE.getFormat();

		/**
		 * The meta object literal for the '<em><b>Nature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORMAT__NATURE = eINSTANCE.getFormat_Nature();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORMAT__EXTENSION = eINSTANCE.getFormat_Extension();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IdentifierImpl <em>Identifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IdentifierImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIdentifier()
		 * @generated
		 */
		EClass IDENTIFIER = eINSTANCE.getIdentifier();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER__ID = eINSTANCE.getIdentifier_Id();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIER__QUALIFIER = eINSTANCE.getIdentifier_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.LanguageImpl <em>Language</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.LanguageImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getLanguage()
		 * @generated
		 */
		EClass LANGUAGE = eINSTANCE.getLanguage();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANGUAGE__VALUE = eINSTANCE.getLanguage_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.PublisherImpl <em>Publisher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.PublisherImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getPublisher()
		 * @generated
		 */
		EClass PUBLISHER = eINSTANCE.getPublisher();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHER__VALUE = eINSTANCE.getPublisher_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RelationImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION__URI = eINSTANCE.getRelation_Uri();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__QUALIFIER = eINSTANCE.getRelation_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RightsImpl <em>Rights</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RightsImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRights()
		 * @generated
		 */
		EClass RIGHTS = eINSTANCE.getRights();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RIGHTS__TEXT = eINSTANCE.getRights_Text();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RIGHTS__URI = eINSTANCE.getRights_Uri();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RIGHTS__QUALIFIER = eINSTANCE.getRights_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.SourceImpl <em>Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.SourceImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getSource()
		 * @generated
		 */
		EClass SOURCE = eINSTANCE.getSource();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOURCE__VALUE = eINSTANCE.getSource_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.SubjectImpl <em>Subject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.SubjectImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getSubject()
		 * @generated
		 */
		EClass SUBJECT = eINSTANCE.getSubject();

		/**
		 * The meta object literal for the '<em><b>Keywords</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBJECT__KEYWORDS = eINSTANCE.getSubject_Keywords();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.TitleImpl <em>Title</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.TitleImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getTitle()
		 * @generated
		 */
		EClass TITLE = eINSTANCE.getTitle();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TITLE__VALUE = eINSTANCE.getTitle_Value();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TITLE__QUALIFIER = eINSTANCE.getTitle_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.TypeImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '<em><b>Diagram Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__DIAGRAM_TYPE = eINSTANCE.getType_DiagramType();

		/**
		 * The meta object literal for the '<em><b>Metamodel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__METAMODEL = eINSTANCE.getType_Metamodel();

		/**
		 * The meta object literal for the '<em><b>Metamodel Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__METAMODEL_VERSION = eINSTANCE.getType_MetamodelVersion();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DatasetImpl <em>Dataset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DatasetImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDataset()
		 * @generated
		 */
		EClass DATASET = eINSTANCE.getDataset();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ElementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DateRefinementImpl <em>Date Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DateRefinementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateRefinement()
		 * @generated
		 */
		EClass DATE_REFINEMENT = eINSTANCE.getDateRefinement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DescriptionRefinementImpl <em>Description Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DescriptionRefinementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDescriptionRefinement()
		 * @generated
		 */
		EClass DESCRIPTION_REFINEMENT = eINSTANCE.getDescriptionRefinement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RelationRefinementImpl <em>Relation Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RelationRefinementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRelationRefinement()
		 * @generated
		 */
		EClass RELATION_REFINEMENT = eINSTANCE.getRelationRefinement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RightsRefinementImpl <em>Rights Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RightsRefinementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRightsRefinement()
		 * @generated
		 */
		EClass RIGHTS_REFINEMENT = eINSTANCE.getRightsRefinement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AlternativeImpl <em>Alternative</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AlternativeImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAlternative()
		 * @generated
		 */
		EClass ALTERNATIVE = eINSTANCE.getAlternative();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AbstractImpl <em>Abstract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AbstractImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAbstract()
		 * @generated
		 */
		EClass ABSTRACT = eINSTANCE.getAbstract();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.TableOfContentsImpl <em>Table Of Contents</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.TableOfContentsImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getTableOfContents()
		 * @generated
		 */
		EClass TABLE_OF_CONTENTS = eINSTANCE.getTableOfContents();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AvailableImpl <em>Available</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AvailableImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAvailable()
		 * @generated
		 */
		EClass AVAILABLE = eINSTANCE.getAvailable();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.CreatedImpl <em>Created</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.CreatedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getCreated()
		 * @generated
		 */
		EClass CREATED = eINSTANCE.getCreated();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DateAcceptedImpl <em>Date Accepted</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DateAcceptedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateAccepted()
		 * @generated
		 */
		EClass DATE_ACCEPTED = eINSTANCE.getDateAccepted();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DateCopyrightedImpl <em>Date Copyrighted</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DateCopyrightedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateCopyrighted()
		 * @generated
		 */
		EClass DATE_COPYRIGHTED = eINSTANCE.getDateCopyrighted();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.DateSubmittedImpl <em>Date Submitted</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.DateSubmittedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getDateSubmitted()
		 * @generated
		 */
		EClass DATE_SUBMITTED = eINSTANCE.getDateSubmitted();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IssuedImpl <em>Issued</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IssuedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIssued()
		 * @generated
		 */
		EClass ISSUED = eINSTANCE.getIssued();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ModifiedImpl <em>Modified</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ModifiedImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getModified()
		 * @generated
		 */
		EClass MODIFIED = eINSTANCE.getModified();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsPartOfImpl <em>Is Part Of</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsPartOfImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsPartOf()
		 * @generated
		 */
		EClass IS_PART_OF = eINSTANCE.getIsPartOf();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsVersionOfImpl <em>Is Version Of</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsVersionOfImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsVersionOf()
		 * @generated
		 */
		EClass IS_VERSION_OF = eINSTANCE.getIsVersionOf();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsFormatOfImpl <em>Is Format Of</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsFormatOfImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsFormatOf()
		 * @generated
		 */
		EClass IS_FORMAT_OF = eINSTANCE.getIsFormatOf();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.HasFormatImpl <em>Has Format</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.HasFormatImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getHasFormat()
		 * @generated
		 */
		EClass HAS_FORMAT = eINSTANCE.getHasFormat();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsReferencedByImpl <em>Is Referenced By</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsReferencedByImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsReferencedBy()
		 * @generated
		 */
		EClass IS_REFERENCED_BY = eINSTANCE.getIsReferencedBy();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ReferencesImpl <em>References</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ReferencesImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getReferences()
		 * @generated
		 */
		EClass REFERENCES = eINSTANCE.getReferences();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsBasisForImpl <em>Is Basis For</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsBasisForImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsBasisFor()
		 * @generated
		 */
		EClass IS_BASIS_FOR = eINSTANCE.getIsBasisFor();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.IsBasedOnImpl <em>Is Based On</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.IsBasedOnImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getIsBasedOn()
		 * @generated
		 */
		EClass IS_BASED_ON = eINSTANCE.getIsBasedOn();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RequiresImpl <em>Requires</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RequiresImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRequires()
		 * @generated
		 */
		EClass REQUIRES = eINSTANCE.getRequires();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AccessRightsImpl <em>Access Rights</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AccessRightsImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAccessRights()
		 * @generated
		 */
		EClass ACCESS_RIGHTS = eINSTANCE.getAccessRights();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.LicenseImpl <em>License</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.LicenseImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getLicense()
		 * @generated
		 */
		EClass LICENSE = eINSTANCE.getLicense();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AudienceImpl <em>Audience</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AudienceImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAudience()
		 * @generated
		 */
		EClass AUDIENCE = eINSTANCE.getAudience();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUDIENCE__VALUE = eINSTANCE.getAudience_Value();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUDIENCE__QUALIFIER = eINSTANCE.getAudience_Qualifier();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.BibliographicCitationImpl <em>Bibliographic Citation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.BibliographicCitationImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getBibliographicCitation()
		 * @generated
		 */
		EClass BIBLIOGRAPHIC_CITATION = eINSTANCE.getBibliographicCitation();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ProvenanceImpl <em>Provenance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ProvenanceImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getProvenance()
		 * @generated
		 */
		EClass PROVENANCE = eINSTANCE.getProvenance();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVENANCE__VALUE = eINSTANCE.getProvenance_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.RightsHolderImpl <em>Rights Holder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.RightsHolderImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getRightsHolder()
		 * @generated
		 */
		EClass RIGHTS_HOLDER = eINSTANCE.getRightsHolder();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RIGHTS_HOLDER__VALUE = eINSTANCE.getRightsHolder_Value();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.AudienceRefinementImpl <em>Audience Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.AudienceRefinementImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getAudienceRefinement()
		 * @generated
		 */
		EClass AUDIENCE_REFINEMENT = eINSTANCE.getAudienceRefinement();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.MediatorImpl <em>Mediator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.MediatorImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMediator()
		 * @generated
		 */
		EClass MEDIATOR = eINSTANCE.getMediator();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.EducationLevelImpl <em>Education Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.EducationLevelImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getEducationLevel()
		 * @generated
		 */
		EClass EDUCATION_LEVEL = eINSTANCE.getEducationLevel();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.impl.ExternalStorageImpl <em>External Storage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.impl.ExternalStorageImpl
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getExternalStorage()
		 * @generated
		 */
		EClass EXTERNAL_STORAGE = eINSTANCE.getExternalStorage();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.MetamodelName <em>Metamodel Name</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.MetamodelName
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getMetamodelName()
		 * @generated
		 */
		EEnum METAMODEL_NAME = eINSTANCE.getMetamodelName();

		/**
		 * The meta object literal for the '{@link org.dublincore.DublinCore.FormatNature <em>Format Nature</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.dublincore.DublinCore.FormatNature
		 * @see org.dublincore.DublinCore.impl.DublinCorePackageImpl#getFormatNature()
		 * @generated
		 */
		EEnum FORMAT_NATURE = eINSTANCE.getFormatNature();

	}

} //DublinCorePackage
