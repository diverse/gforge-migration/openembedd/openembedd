/**
 * <copyright>
 * </copyright>
 *
 * $Id: Date.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Date#getDate <em>Date</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Date#getQualifier <em>Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getDate()
 * @model
 * @generated
 */
public interface Date extends Element {
	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(java.util.Date)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getDate_Date()
	 * @model required="true"
	 * @generated
	 */
	java.util.Date getDate();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Date#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(java.util.Date value);

	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference.
	 * @see #setQualifier(DateRefinement)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getDate_Qualifier()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DateRefinement getQualifier();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Date#getQualifier <em>Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier</em>' containment reference.
	 * @see #getQualifier()
	 * @generated
	 */
	void setQualifier(DateRefinement value);

} // Date
