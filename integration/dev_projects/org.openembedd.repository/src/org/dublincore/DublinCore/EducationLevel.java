/**
 * <copyright>
 * </copyright>
 *
 * $Id: EducationLevel.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Education Level</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getEducationLevel()
 * @model
 * @generated
 */
public interface EducationLevel extends AudienceRefinement {
} // EducationLevel
