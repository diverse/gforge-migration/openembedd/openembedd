/**
 * <copyright>
 * </copyright>
 *
 * $Id: BibliographicCitation.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bibliographic Citation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getBibliographicCitation()
 * @model
 * @generated
 */
public interface BibliographicCitation extends EObject {
} // BibliographicCitation
