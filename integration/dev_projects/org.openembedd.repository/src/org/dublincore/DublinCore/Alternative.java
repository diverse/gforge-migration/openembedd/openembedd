/**
 * <copyright>
 * </copyright>
 *
 * $Id: Alternative.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alternative</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getAlternative()
 * @model
 * @generated
 */
public interface Alternative extends EObject {
} // Alternative
