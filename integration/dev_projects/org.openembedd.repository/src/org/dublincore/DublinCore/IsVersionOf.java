/**
 * <copyright>
 * </copyright>
 *
 * $Id: IsVersionOf.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Version Of</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getIsVersionOf()
 * @model
 * @generated
 */
public interface IsVersionOf extends RelationRefinement {
} // IsVersionOf
