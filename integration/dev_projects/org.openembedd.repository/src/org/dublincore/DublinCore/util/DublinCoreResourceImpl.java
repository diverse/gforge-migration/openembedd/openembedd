/**
 * <copyright>
 * </copyright>
 *
 * $Id: DublinCoreResourceImpl.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.dublincore.DublinCore.util.DublinCoreResourceFactoryImpl
 * @generated
 */
public class DublinCoreResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public DublinCoreResourceImpl(URI uri) {
		super(uri);
	}

} //DublinCoreResourceImpl
