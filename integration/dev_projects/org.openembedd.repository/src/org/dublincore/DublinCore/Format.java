/**
 * <copyright>
 * </copyright>
 *
 * $Id: Format.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Format</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Format#getNature <em>Nature</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Format#getExtension <em>Extension</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getFormat()
 * @model
 * @generated
 */
public interface Format extends Element {
	/**
	 * Returns the value of the '<em><b>Nature</b></em>' attribute.
	 * The literals are from the enumeration {@link org.dublincore.DublinCore.FormatNature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature</em>' attribute.
	 * @see org.dublincore.DublinCore.FormatNature
	 * @see #setNature(FormatNature)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getFormat_Nature()
	 * @model required="true"
	 * @generated
	 */
	FormatNature getNature();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Format#getNature <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' attribute.
	 * @see org.dublincore.DublinCore.FormatNature
	 * @see #getNature()
	 * @generated
	 */
	void setNature(FormatNature value);

	/**
	 * Returns the value of the '<em><b>Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension</em>' attribute.
	 * @see #setExtension(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getFormat_Extension()
	 * @model
	 * @generated
	 */
	String getExtension();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Format#getExtension <em>Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension</em>' attribute.
	 * @see #getExtension()
	 * @generated
	 */
	void setExtension(String value);

} // Format
