/**
 * <copyright>
 * </copyright>
 *
 * $Id: Identifier.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Identifier#getId <em>Id</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Identifier#getQualifier <em>Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getIdentifier()
 * @model
 * @generated
 */
public interface Identifier extends Element {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getIdentifier_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Identifier#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference.
	 * @see #setQualifier(BibliographicCitation)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getIdentifier_Qualifier()
	 * @model containment="true"
	 * @generated
	 */
	BibliographicCitation getQualifier();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Identifier#getQualifier <em>Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier</em>' containment reference.
	 * @see #getQualifier()
	 * @generated
	 */
	void setQualifier(BibliographicCitation value);

} // Identifier
