/**
 * <copyright>
 * </copyright>
 *
 * $Id: MetadataItemProvider.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore.provider;


import java.util.Collection;
import java.util.List;

import org.dublincore.DublinCore.DublinCoreFactory;
import org.dublincore.DublinCore.DublinCorePackage;
import org.dublincore.DublinCore.Metadata;

import org.dublincore.provider.DublinCoreEditPlugin;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link org.dublincore.DublinCore.Metadata} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MetadataItemProvider
	extends ItemProviderAdapter
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetadataItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__CONTRIBUTOR);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__COVERAGE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__CREATOR);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__DATE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__DESCRIPTION);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__FORMAT);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__IDENTIFIER);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__LANGUAGE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__PUBLISHER);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__RELATION);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__RIGHTS);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__SOURCE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__SUBJECT);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__TITLE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__TYPE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__AUDIENCE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__PROVENANCE);
			childrenFeatures.add(DublinCorePackage.Literals.METADATA__RIGHTS_HOLDER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Metadata.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Metadata"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Metadata_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Metadata.class)) {
			case DublinCorePackage.METADATA__CONTRIBUTOR:
			case DublinCorePackage.METADATA__COVERAGE:
			case DublinCorePackage.METADATA__CREATOR:
			case DublinCorePackage.METADATA__DATE:
			case DublinCorePackage.METADATA__DESCRIPTION:
			case DublinCorePackage.METADATA__FORMAT:
			case DublinCorePackage.METADATA__IDENTIFIER:
			case DublinCorePackage.METADATA__LANGUAGE:
			case DublinCorePackage.METADATA__PUBLISHER:
			case DublinCorePackage.METADATA__RELATION:
			case DublinCorePackage.METADATA__RIGHTS:
			case DublinCorePackage.METADATA__SOURCE:
			case DublinCorePackage.METADATA__SUBJECT:
			case DublinCorePackage.METADATA__TITLE:
			case DublinCorePackage.METADATA__TYPE:
			case DublinCorePackage.METADATA__AUDIENCE:
			case DublinCorePackage.METADATA__PROVENANCE:
			case DublinCorePackage.METADATA__RIGHTS_HOLDER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__CONTRIBUTOR,
				 DublinCoreFactory.eINSTANCE.createContributor()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__COVERAGE,
				 DublinCoreFactory.eINSTANCE.createCoverage()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__CREATOR,
				 DublinCoreFactory.eINSTANCE.createCreator()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__DATE,
				 DublinCoreFactory.eINSTANCE.createDate()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__DESCRIPTION,
				 DublinCoreFactory.eINSTANCE.createDescription()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__FORMAT,
				 DublinCoreFactory.eINSTANCE.createFormat()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__IDENTIFIER,
				 DublinCoreFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__LANGUAGE,
				 DublinCoreFactory.eINSTANCE.createLanguage()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__PUBLISHER,
				 DublinCoreFactory.eINSTANCE.createPublisher()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__RELATION,
				 DublinCoreFactory.eINSTANCE.createRelation()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__RIGHTS,
				 DublinCoreFactory.eINSTANCE.createRights()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__SOURCE,
				 DublinCoreFactory.eINSTANCE.createSource()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__SUBJECT,
				 DublinCoreFactory.eINSTANCE.createSubject()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__TITLE,
				 DublinCoreFactory.eINSTANCE.createTitle()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__TYPE,
				 DublinCoreFactory.eINSTANCE.createType()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__AUDIENCE,
				 DublinCoreFactory.eINSTANCE.createAudience()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__PROVENANCE,
				 DublinCoreFactory.eINSTANCE.createProvenance()));

		newChildDescriptors.add
			(createChildParameter
				(DublinCorePackage.Literals.METADATA__RIGHTS_HOLDER,
				 DublinCoreFactory.eINSTANCE.createRightsHolder()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DublinCoreEditPlugin.INSTANCE;
	}

}
