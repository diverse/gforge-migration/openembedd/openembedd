/**
 * <copyright>
 * </copyright>
 *
 * $Id: Type.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Type#getDiagramType <em>Diagram Type</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Type#getMetamodel <em>Metamodel</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Type#getMetamodelVersion <em>Metamodel Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getType()
 * @model
 * @generated
 */
public interface Type extends Element {
	/**
	 * Returns the value of the '<em><b>Diagram Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Type</em>' attribute.
	 * @see #setDiagramType(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getType_DiagramType()
	 * @model required="true"
	 * @generated
	 */
	String getDiagramType();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Type#getDiagramType <em>Diagram Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram Type</em>' attribute.
	 * @see #getDiagramType()
	 * @generated
	 */
	void setDiagramType(String value);

	/**
	 * Returns the value of the '<em><b>Metamodel</b></em>' attribute.
	 * The literals are from the enumeration {@link org.dublincore.DublinCore.MetamodelName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel</em>' attribute.
	 * @see org.dublincore.DublinCore.MetamodelName
	 * @see #setMetamodel(MetamodelName)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getType_Metamodel()
	 * @model required="true"
	 * @generated
	 */
	MetamodelName getMetamodel();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Type#getMetamodel <em>Metamodel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metamodel</em>' attribute.
	 * @see org.dublincore.DublinCore.MetamodelName
	 * @see #getMetamodel()
	 * @generated
	 */
	void setMetamodel(MetamodelName value);

	/**
	 * Returns the value of the '<em><b>Metamodel Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel Version</em>' attribute.
	 * @see #setMetamodelVersion(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getType_MetamodelVersion()
	 * @model required="true"
	 * @generated
	 */
	String getMetamodelVersion();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Type#getMetamodelVersion <em>Metamodel Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metamodel Version</em>' attribute.
	 * @see #getMetamodelVersion()
	 * @generated
	 */
	void setMetamodelVersion(String value);

} // Type
