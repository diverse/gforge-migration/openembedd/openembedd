/**
 * <copyright>
 * </copyright>
 *
 * $Id: AvailableValidator.java,v 1.1 2008-01-21 15:40:17 vmahe Exp $
 */
package org.dublincore.DublinCore.validation;


/**
 * A sample validator interface for {@link org.dublincore.DublinCore.Available}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface AvailableValidator {
	boolean validate();

}
