/**
 * <copyright>
 * </copyright>
 *
 * $Id: Relation.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.dublincore.DublinCore.Relation#getUri <em>Uri</em>}</li>
 *   <li>{@link org.dublincore.DublinCore.Relation#getQualifier <em>Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getRelation()
 * @model
 * @generated
 */
public interface Relation extends Element {
	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getRelation_Uri()
	 * @model required="true"
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Relation#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference.
	 * @see #setQualifier(RelationRefinement)
	 * @see org.dublincore.DublinCore.DublinCorePackage#getRelation_Qualifier()
	 * @model containment="true"
	 * @generated
	 */
	RelationRefinement getQualifier();

	/**
	 * Sets the value of the '{@link org.dublincore.DublinCore.Relation#getQualifier <em>Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier</em>' containment reference.
	 * @see #getQualifier()
	 * @generated
	 */
	void setQualifier(RelationRefinement value);

} // Relation
