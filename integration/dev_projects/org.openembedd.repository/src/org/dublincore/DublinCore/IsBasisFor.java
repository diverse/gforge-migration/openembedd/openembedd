/**
 * <copyright>
 * </copyright>
 *
 * $Id: IsBasisFor.java,v 1.1 2008-01-21 15:40:18 vmahe Exp $
 */
package org.dublincore.DublinCore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Basis For</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.dublincore.DublinCore.DublinCorePackage#getIsBasisFor()
 * @model
 * @generated
 */
public interface IsBasisFor extends RelationRefinement {
} // IsBasisFor
