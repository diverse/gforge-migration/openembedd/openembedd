/**
 * Copyright : IRISA / INRIA Rennes Bretagne Atlantique - OpenEmbeDD integration team
 * 
 * This plug-in is under the terms of the EPL License. http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette
 */
package org.openembedd.flac.popup.actions;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.openembedd.launchexec.Console;
import org.openembedd.launchexec.ExecCommand;
import org.openembedd.launchexec.Plugin;

/**
 * This Action calls the win32 native flac executable on each file selected. flac translates the input Fiacre file
 * <input>.fcr into a semantically equivalent ouput Lotos file <output>.lotos. Additional files <output>.f and
 * <output>.t are also generated, which contain the code for 'external' operations.
 * 
 * The gate parameters of the generated Lotos specification (i.e., the visible gates of the specification) are the
 * formal gate parameters of the main Fiacre component/process. It is guaranteed that these gate parameters keep the
 * name used in the Fiacre program, only prefixed by the "FIACRE_" string.
 */
public class ExecuteAction implements IObjectActionDelegate
{
	/** The file selection */
	private StructuredSelection	selection;

	/**
	 * Constructor
	 */
	public ExecuteAction()
	{
		super();
		this.selection = null;
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action)
	{
		Console console = new Console("Flac console");

		for (Iterator<?> it = selection.iterator(); it.hasNext();)
		{
			Object obj = (Object) it.next();
			if (obj instanceof IFile)
			{
				IFile file = (IFile) obj;
				String[] cmd = new String[3];

				try
				{
					URL entry = Plugin.getDefault().getBundle().getResource("/flac.exe");
					if (entry != null)
					{
						// Localize the file on the file system
						// Get the absolute path to the resource
						String dir = FileLocator.toFileURL(entry).getPath();

						cmd[0] = new File(dir).getAbsolutePath();
						// Add verbose mode
						cmd[1] = "-v";
						// Add the path to the fiacre file
						cmd[2] = file.getLocation().toOSString();
						
						// Execute the command and print the result in the console
						ExecCommand command = new ExecCommand(console, cmd);
						command.execute();

						// Refresh the directory
						try
						{
							file.getParent().refreshLocal(1, null);
						}
						catch (CoreException ce)
						{
							Plugin.getDefault().getLog().log(
								new Status(IStatus.ERROR, Plugin.PLUGIN_ID, 0, "Refreshing error.", ce));
						}
					}
					else
						Plugin.getDefault().getLog().log(
							new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Cannot find the flac native utilities."));
				}
				catch (IOException ioe)
				{
					Plugin.getDefault().getLog().log(
						new Status(IStatus.ERROR, Plugin.PLUGIN_ID, 0,
							"An I/O Exception has been thrown during the search of the executable.", ioe));
				}
			}
		}

		// Close the console output stream
		console.dispose();
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		if (selection instanceof StructuredSelection)
		{
			this.selection = (StructuredSelection) selection;
		}
	}

}
