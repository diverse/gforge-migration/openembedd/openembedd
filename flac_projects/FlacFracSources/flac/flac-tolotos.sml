(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module contains the implementation of the algorithm for translating
 * FIACRE specifications into LOTOS, as proposed by F. Lang in <REF>.
 *)

signature TOLOTOS = sig
    (** Group: Exported declarations *)

    (**
     * Function: ToLOTOS.translate
     *
     * Translates a FIACRE specification into its LOTOS equivalent.
     * Prints warnings on the error output.
     *
     * Parameters:
     *   fs - FIACRE specification to translate
     *
     * Returns:
     *   a triple (l, f, t) where l is the LOTOS equivalent of the passed FIACRE
     *   specification, and f (respectively t) is the content of the associated
     *   ".f file" (respectively ".t file")
     *
     * *Raises*:
     *   - <BYE> after printing an error message if translation fails
     *)
    val translate : Fiacre -> Lotos.specification * (string list) * (string list)
end

structure ToLOTOS :> TOLOTOS = struct
    (** Group: Internal declarations *)

    open Lotos

    open Helper

    (**
     * Function: ToLOTOS.containsSharedVariable
     *
     * Tests whether a FIACRE declaration is either a process or component
     * declaration that declares some of its parameters as shared.
     *
     * Parameters:
     *   decl - FIACRE declaration to check
     *
     * Returns:
     *   true if the passed declaration is a process or component one that
     *   contains at least one parameter marked as shared
     *)
    fun containsSharedVariable decl =
        let
            val params = case decl of
                             ProcessDecl p => #4 p
                           | ComponentDecl c => #4 c
                           | _ => []
            val args = map #2 (List.concat (map #1 params))
        in
            List.exists (fn (ValArg _) => false | (RefArg _) => true) args
        end

    fun translate (Program (decls, mainDecl)) =
        (verbose "::Translating to Lotos\n";
         Ident.reset ();
         let
             val fFile = emptyFile ()
             val tFile = emptyFile ()
             val types = ref (StringMap.empty : Type StringMap.map)
             val typeDecls = List.mapPartial (Types.translateType fFile tFile types) decls
             val shared = List.exists containsSharedVariable
                                      ((case mainDecl of SOME m => [m] | NONE => []) @ decls)
             val channels = List.foldl (fn ((ChannelDecl (_, id, ch)), acc) =>
                                           StringMap.insert (acc,
                                                             id,
                                                             (case ch of
                                                                  NamedChannel n =>
                                                                  Option.valOf (StringMap.find (acc, n))
                                                                | ProfileChannel p => p))
                                         | (_, acc) => acc)
                                       StringMap.empty
                                       decls
             val signatures = List.foldl (fn ((id, sign), acc) => StringMap.insert (acc, id, sign))
                                         StringMap.empty
                                         (List.mapPartial Processes.getSignature decls)
             val typeProcesses = ref StringSet.empty
             val infos = { types = (!types), channels = channels, signatures = signatures }
             val processDecls = List.mapPartial (Processes.translateProcessOrComponent shared infos typeProcesses) decls
             val typeProcessesDecl = map Processes.generateTypeProcess (StringSet.listItems (!typeProcesses))
             val mainName = case mainDecl of
                                SOME (Main (_, id)) => id
                              | _ => assertion InvalidMain
             val mainID = Ident.make mainName
             val mainElement = List.find (fn (ProcessDecl p) => (#2 p) = mainName
                                           | (ComponentDecl c) => (#2 c) = mainName
                                           | _ => false)
                                         decls
             val mainParams = case mainElement of
                                  SOME (ProcessDecl p) => #4 p
                                | SOME (ComponentDecl c) => #4 c
                                | _ => assertion InvalidMain
             val visibleGates = map (Ident.make o #2)
                                    (List.concat (map #1 (case mainElement of
                                                              SOME (ProcessDecl p) => #3 p
                                                            | SOME (ComponentDecl c) => #3 c
                                                            | _ => assertion InvalidMain)))
             val lockGates = [lockID, unlockID]
             val res = { name = fiacreID,
                         gates = visibleGates,
                         parameters = [],
                         func = NoExit,
                         libraries = [xbooleanID, naturalID, integerID],
                         types = extendedInteger :: typeDecls,
                         body = if mainParams = [] then
                                    (if shared then
                                         Hide (lockGates,
                                               (Parallel ((CallProcess (mainID, (visibleGates @ lockGates), [])),
                                                          lockGates,
                                                          (CallProcess (controllerID, lockGates, [])))))
                                     else
                                         (CallProcess (mainID, visibleGates, [])))
                                else
                                  (warning "main" "process/component should have no data parameter";
                                   Stop),
                         blocks = map ProcessBlock
                                      ((if shared then [controllerProcess] else []) @ processDecls @ typeProcessesDecl) }
         in
             verbose "::Done\n";
             (res,
              (fileToStringList fFile fFileHeader),
              (fileToStringList tFile tFileHeader))
         end)
        handle (e as (Assertion a)) =>
               (ErrSrcPrint.print ("Assertion error in LOTOS translation: " ^ (assertionToString a) ^ "\n");
                app (fn s => ErrSrcPrint.print ("    " ^ s ^ "\n")) (MLton.Exn.history e);
                raise BYE)
             | BYE => raise BYE
             | e =>
               (ErrSrcPrint.print ("Internal error in LOTOS translation: " ^ (exnMessage e) ^ "\n");
                app (fn s => ErrSrcPrint.print ("    " ^ s ^ "\n")) (MLton.Exn.history e);
                raise BYE)

end
