(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(* output options *)
datatype output = NoOutput | Fiacre | LOTOS
val OUTPUT = ref NoOutput  (* output option, default just parses *)
val PRELOTOS = ref false   (* fcr output suboption *)

(* mode flags *)
val VERBOSE = ref false
val DETAILS = ref false  (* for debugging specializations, with flags -g *)
val MAXIMIZE = ref true  (* debug option for type instances of primitives *)
val TYPECHECK = ref true (* typechecks or not *)
val TRANSF = ref false   (* if -F, prints result of pre-processing rather than fiacre sources *)

(* error handling *)
exception BYE  (* signals termination *)
fun error m = (ErrPrint.print m; ErrPrint.print "\n"; ErrPrint.flush (); raise BYE)

(* banner *)
val BANNER = "Flac version " ^ version ^ release ^ " -- "
             ^ date ^ " -- FERIA/SVF & INRIA/VASY\n"


(* command line parser *)
fun command args =
    let
        open OutPrint
	fun usage () =
	    (print "usage: flac [-h | -help]\n";
	     print "            [-q | -v] [-o errfile]\n";
	     print "            [infile]\n")
	fun flacerror m =
	    (print "flac: "; print m; print "\n"; usage(); raise BYE)
	fun help () =
	    (print BANNER;
	     usage();
	     print "FLAGS        WHAT                                    DEFAULT\n";
	     print "-h | -help   this mode                               \n";
             print "-version     prints the version and exits\n";
	     print "error format:                                        \n";
	     print "-q            quiet                                  -q\n";
	     print "-v            verbose                                -q\n";
	     print "-o errfile    save errors in errfile (stderr if -)   stderr\n";
	     print "files:                                               \n";
	     print "infile.fcr    input file (stdin if -)                stdin\n";
	     print "-output pre   use pre as prefix for output files     infile\n";
	     flush ();
	     OS.Process.exit OS.Process.success)
	val FILES = ref 0
	fun concat l = String.concat (map (fn x => " " ^ x) l)
	fun parse l =
            case l of
		[] => ()
	      | "-help" :: _ => help()
	      | "-h" :: _    => help()
              | "-version" :: _ => (print version; print "\n"; flush (); OS.Process.exit OS.Process.success)
	      | "-q" :: t    => (VERBOSE := false; parse t)
	      | "-v" :: t    => (VERBOSE := true; parse t)
	      | "-o" :: file :: t => (if String.size file > 1
                                     andalso "-" = String.substring(file,0,1) then
				      flacerror ("bad command line, skipping " ^ file
                                                 ^ " ... " ^ concat t)
				      else
                                          ErrPrint.name := file;
				      parse t)
              | "-output" :: file :: t => (OutPrint.name := (file ^ ".lotos"); parse t)
(* private *) | "-f" :: t    => (OUTPUT := Fiacre; parse t)
(* private *) | "-fcr" :: t  => (OUTPUT := Fiacre; parse t)
(* private *) | "-F" :: t    => (OUTPUT := Fiacre; TRANSF := true; parse t)
(* private *) | "-G" :: t    => (OUTPUT := Fiacre; DETAILS := true; parse t)
(* private *) | "-fl" :: t   => (OUTPUT := Fiacre; PRELOTOS := true; DETAILS := true; parse t)
	      | file :: t    =>
                if String.size file > 1 andalso "-" = String.substring(file,0,1) then
		    flacerror ("bad command line, skipping " ^ file ^ " ... " ^ concat t)
	        else
                    (FILES := 1 + !FILES;
                     case !FILES of
			 1 => if file = "-" then
                                  parse t
                              else
                                  (Input.name := file;
                                   if (!OutPrint.name) = "" then
                                       OutPrint.name := (OS.Path.base (OS.Path.mkCanonical file)) ^ ".lotos"
                                   else
                                       ();
                                   parse t)
		       | _ => flacerror ("bad command line, skipping " ^ file ^ " ... " ^ concat t))
    in
        parse args
    end
