(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

structure Lotos :> LOTOS = struct
    (** Group: Internal declarations *)

    (**
     * Function: Lotos.listToString
     *
     * Converts a list into a string.
     *
     * Parameters:
     *   prefix - prefix of returned string
     *   sep - separator between elements
     *   f - function to convert each element to a string
     *   l - list to convert
     *   suffix - suffix of returned string
     *
     * Returns:
     *   an empty string if the passed list is empty, a string with
     *   given prefix and suffix whose "middle" is the result of
     *   <Utils.concatMap> sep f l otherwise
     *)
    fun listToString _ _ _ [] _ = ""
      | listToString prefix sep f l suffix =
        prefix ^ (concatMap sep f l) ^ suffix

    (**
     * Function: Lotos.varInDom
     *
     * Converts identifiers to a string.
     *
     * Parameters:
     *   (x, dom) - where x is an identifier and dom an identifier list
     *
     * Returns:
     *   a string of the form "x in dom" where elements in dom are comma-separated
     *)
    fun varInDom (x, dom) =
        (Ident.toString x) ^ " in " ^ (concatMap ", " Ident.toString dom)

    (**
     * Function: Lotos.elemsInDom
     *
     * Converts identifiers to a string.
     *
     * Parameters:
     *   (ids, dom) - where ids is an identifier list and dom an identifier
     *
     * Returns:
     *   a string of the form "ids : dom" where elements in ids are comma-separated
     *)
    fun elemsInDom (ids, dom) =
        (concatMap ", " Ident.toString ids) ^ " : " ^ (Ident.toString dom)


    (* Special comments *)

    datatype sortComment = ImplementedBy of string
                         | ImplementedByIteratedBy of string * string * string

    datatype operationComment = Constructor of string
                              | AnonConstructor
                              | ImplementedByExternal of string

    fun sortCommentToString (ImplementedBy s) =
        " (*! implementedby " ^ s ^ " *)"
      | sortCommentToString (ImplementedByIteratedBy (s1, s2, s3)) =
        " (*! implementedby " ^ s1 ^ " iteratedby " ^ s2 ^ " and " ^ s3 ^ " *)"

    fun operationCommentToString (Constructor s) =
        " (*! implementedby " ^ s ^ " constructor *)"
      | operationCommentToString AnonConstructor =
        " (*! constructor *)"
      | operationCommentToString (ImplementedByExternal s) =
        " (*! implementedby " ^ s ^ " external *)"


    (* Operations *)

    type operation = { names : (Ident.t * (operationComment option)) list,
                       parameters : Ident.t list,
                       result : Ident.t }

    fun operationToString (x : operation) =
        let
            fun ident (n, NONE) = Ident.toString n
              | ident (n, (SOME c)) =
                Ident.toString n ^ (operationCommentToString c)
            val names = concatMap ", " ident (#names x)
            val parameters =
                if List.null (#parameters x) then
                    ""
                else
                    " " ^ (concatMap ", " Ident.toString (#parameters x))
            val result = Ident.toString (#result x)
        in
            names ^ " :" ^ parameters ^ " -> " ^ result
        end


    (* Equations *)

    datatype value = Const of string
                   | Var of Ident.t
                   | Prefix of Ident.t * (value list)
                   | Infix of value * Ident.t * value
                   | Of of value * Ident.t

    type simpleEq = value * value

    datatype premEq = Simple of simpleEq
                    | Value of value

    type mediumEq = premEq list * simpleEq

    type forAll = (Ident.t list * Ident.t) list

    type complexEq = Ident.t * forAll * (mediumEq list)

    fun valueToString x =
        let
            fun evts _ (Const c) = c
              | evts _ (Var id) = Ident.toString id
              | evts _ (Prefix (id, [])) = Ident.toString id
              | evts _ (Prefix (id, vals)) =
                (Ident.toString id) ^ (surround (concatMap ", " (evts false) vals))
              | evts p (Infix (v1, id, v2)) =
                (if p then surround else identity)
                    ((evts true v1)
                     ^ " " ^ (Ident.toString id)
                     ^ " " ^ (evts true v2))
              | evts p (Of (v, id)) =
                (if p then surround else identity)
                    ((evts true v) ^ " of " ^ (Ident.toString id))
        in
            evts false x
        end

    fun simpleEqToString (x : simpleEq) =
        (valueToString (#1 x)) ^ " = " ^ (valueToString (#2 x)) ^ ";"

    fun premEqToString (Simple s) = (valueToString (#1 s)) ^ " = " ^ (valueToString (#2 s))
      | premEqToString (Value v) = valueToString v

    fun mediumEqToString ([], s) = simpleEqToString s
      | mediumEqToString (prems, s) =
        (concatMap ", " premEqToString prems) ^ " => " ^ (simpleEqToString s)

    fun forAllToString [] = ""
      | forAllToString l =
        "forall "
        ^ (concatMap
               ", "
               (fn (ids, id) =>
                   (concatMap ", " Ident.toString ids)
                   ^ " : " ^ (Ident.toString id))
               l)

    fun complexEqToString sep ((id, forall, meqs) : complexEq) =
        "   ofsort " ^ (Ident.toString id)
        ^ (if List.null forall then
               ""
           else
               " " ^ (forAllToString forall))
        ^ (if List.null meqs then
               ""
           else
               sep ^ (concatMap sep mediumEqToString meqs))


    (* Types *)

    type typeDeclaration = { name : Ident.t,
                             parents : Ident.t list,
                             formalsorts : Ident.t list,
                             formaloperations : operation list,
                             formalequations : forAll * (complexEq list),
                             sorts : (Ident.t * (sortComment option)) list,
                             operations : operation list,
                             equations : forAll * (complexEq list) }

    type typeInstance = { name : Ident.t,
                          formal_name : Ident.t,
                          parameter_types : Ident.t list,
                          sort_names : (Ident.t * Ident.t) list,
                          operation_names : (Ident.t * Ident.t) list }

    type typeAlias = { name : Ident.t,
                       formal_name : Ident.t,
                       sort_names : (Ident.t * Ident.t) list,
                       operation_names : (Ident.t * Ident.t) list }

    datatype typeDef = TypeDeclaration of typeDeclaration
                     | TypeInstance of typeInstance
                     | TypeAlias of typeAlias

    fun typeDeclarationToString tab (x : typeDeclaration) =
        let
            fun equationsToString _ _ (_, []) = ""
              | equationsToString prefix tab (fa, eqs) =
                prefix
                ^ (if List.null fa then
                       " "
                   else
                       " " ^ (forAllToString fa) ^ "\n" ^ tab ^ "     ")
                ^ (listToString ""
                                ("\n" ^ tab ^ "     ")
                                (complexEqToString (("\n" ^ tab ^ "               ")))
                                eqs
                                "\n")
        in
            tab ^ "type " ^ (Ident.toString (#name x)) ^ " is"
            ^ (listToString " " ", " Ident.toString (#parents x) "") ^ "\n"
            ^ (listToString (tab ^ "   formalsorts ")
                            ", "
                            Ident.toString
                            (#formalsorts x)
                            "\n")
            ^ (listToString (tab ^ "   formalopns ")
                            ("\n              " ^ tab)
                            operationToString
                            (#formaloperations x)
                            "\n")
            ^ (equationsToString (tab ^ "   formaleqns")
                                 tab
                                 (#formalequations x))
            ^ (listToString (tab ^ "   sorts ")
                            ", "
                            (fn (n, NONE) => Ident.toString n
                              | (n, (SOME c)) =>
                                (Ident.toString n) ^ (sortCommentToString c))
                            (#sorts x)
                            "\n")
            ^ (listToString (tab ^ "   opns ")
                            ("\n        " ^ tab)
                            operationToString
                            (#operations x)
                            "\n")
            ^ (equationsToString (tab ^ "   eqns")
                                 tab
                                 (#equations x))
            ^ tab ^ "endtype\n"
        end

    (**
     * Function: Lotos.typeTemplate
     *
     * Factorizes string conversion for type instances and type aliases.
     *
     * Parameters:
     *   tab - tabulation
     *   name - type name
     *   formal_name - name of instanciated/aliased type
     *   references - keyword for type creation
     *   types - referenced types
     *   decl - keyword for type introduction
     *   sorts - aliases for sorts
     *   ops - aliases for operations
     *
     * Returns:
     *   a string corresponding to the type declaration
     *)
    fun typeTemplate tab name formal_name references types decl sorts ops =
        let
            fun for (s1, s2) =
                (Ident.toString s1) ^ " for " ^ (Ident.toString s2)
        in
            tab ^ "type " ^ (Ident.toString name)
            ^ " is " ^ (Ident.toString formal_name)
            ^ references ^ (concatMap ", " Ident.toString types) ^ decl ^ "\n"
            ^ (listToString (tab ^ "   sortnames ")
                            ("\n             " ^ tab)
                            for
                            sorts
                            "\n")
            ^ (listToString (tab ^ "   opnnames ")
                            ("\n            " ^ tab)
                            for
                            ops
                            "\n")
            ^ tab ^ "endtype\n"
        end

    fun typeInstanceToString tab (x : typeInstance) =
        typeTemplate tab
                     (#name x)
                     (#formal_name x)
                     " actualizedby "
                     (#parameter_types x)
                     " using"
                     (#sort_names x)
                     (#operation_names x)

    fun typeAliasToString tab (x : typeAlias) =
        typeTemplate tab
                     (#name x)
                     (#formal_name x)
                     " renamedby"
                     []
                     ""
                     (#sort_names x)
                     (#operation_names x)

    fun typeDefToString tab (TypeDeclaration x) = typeDeclarationToString tab x
      | typeDefToString tab (TypeInstance x) = typeInstanceToString tab x
      | typeDefToString tab (TypeAlias x) = typeAliasToString tab x


    (* Behaviours *)

    type gate = Ident.t

    datatype offer = Send of value
                   | Receive of (Ident.t list) * Ident.t

    datatype condition = SimpleCondition of value
                       | EqualCondition of value * value

    datatype parallelOperator = General of gate list | Async | Sync

    datatype exitValue = SimpleExitValue of value | AnyExitValue of Ident.t

    datatype behaviour =
             Stop
           | Rendezvous of gate * (offer list) * (condition option) * behaviour
           | Choice of behaviour * behaviour
           | GateChoice of (gate * (gate list)) list * behaviour
           | Parallel of behaviour * (gate list) * behaviour
           | ParallelAsync of behaviour * behaviour
           | ParallelSync of behaviour * behaviour
           | Par of (gate * (gate list)) list * parallelOperator * behaviour
           | Hide of gate list * behaviour
           | Guard of condition * behaviour
           | Let of ((Ident.t list * Ident.t * value) list) * behaviour
           | ValueChoice of (Ident.t list * Ident.t) list * behaviour
           | Exit of exitValue list
           | Enable of behaviour * ((Ident.t list * Ident.t) list) * behaviour
           | Disable of behaviour * behaviour
           | CallProcess of Ident.t * (gate list) * (value list)
           | Parentheses of behaviour

    fun gateToString x = Ident.toString x

    fun offerToString (Send v) = "!" ^ (valueToString v)
      | offerToString (Receive (l, s)) =
        "?" ^ (concatMap ", " Ident.toString l) ^ " : " ^ (Ident.toString s)

    fun conditionToString (SimpleCondition v) =
        surround_sq (valueToString v)
      | conditionToString (EqualCondition (v1, v2)) =
        surround_sq ((valueToString v1) ^ " = " ^ (valueToString v2))

    fun parallelOperatorToString (General l) =
        "|[" ^ (concatMap ", " Ident.toString l) ^ "]|"
      | parallelOperatorToString Async = "|||"
      | parallelOperatorToString Sync = "||"

    fun exitValueToString (SimpleExitValue v) = valueToString v
      | exitValueToString (AnyExitValue s) = "any " ^ (Ident.toString s)

    (**
     * Function: Lotos.priority
     *
     * Returns the priority of a behaviour operator.
     *
     * Parameters:
     *   b - behaviour
     *
     * Returns:
     *   the priority of the passed behaviour operator (higher value meaning higher priority)
     *)
    fun priority Stop = 8
      | priority (Rendezvous _) = 7
      | priority (Choice _) = 5
      | priority (GateChoice _) = 1
      | priority (Parallel _) = 4
      | priority (ParallelAsync _) = 4
      | priority (ParallelSync _) = 4
      | priority (Par _) = 1
      | priority (Hide _) = 1
      | priority (Guard _) = 6
      | priority (Let _) = 1
      | priority (ValueChoice _) = 1
      | priority (Exit _) = 8
      | priority (Enable _) = 2
      | priority (Disable _) = 3
      | priority (CallProcess _) = 8
      | priority (Parentheses _) = 8

    (**
     * Function: Lotos.same
     *
     * Tests whether two priorities correspond to the same operator.
     *
     * Parameters:
     *   b1 - behaviour
     *   b2 - behaviour
     *
     * Returns:
     *   true if the passed behaviours correspond to the same operator,
     *   false otherwise
     *)
    fun same Stop Stop = true
      | same (Rendezvous _) (Rendezvous _) = true
      | same (Choice _) (Choice _) = true
      | same (GateChoice _) (GateChoice _) = true
      | same (Parallel _) (Parallel _) = true
      | same (ParallelAsync _) (ParallelAsync _) = true
      | same (ParallelSync _) (ParallelSync _) = true
      | same (Par _) (Par _) = true
      | same (Hide _) (Hide _) = true
      | same (Guard _) (Guard _) = true
      | same (Let _) (Let _) = true
      | same (ValueChoice _) (ValueChoice _) = true
      | same (Exit _) (Exit _) = true
      | same (Enable _) (Enable _) = true
      | same (Disable _) (Disable _) = true
      | same (CallProcess _) (CallProcess _) = true
      | same (Parentheses _) (Parentheses _) = true
      | same _ _ = false

    (**
     * Function: Lotos.assocLeft
     *
     * Tests whether a behaviour operator is left associative.
     *
     * Parameters:
     *   b - behaviour
     *
     * Returns:
     *   true if the operator of the passed behaviour is left associative,
     *   false otherwise
     *)
    fun assocLeft Stop = false
      | assocLeft (Rendezvous _) = false
      | assocLeft (Choice _) = true
      | assocLeft (GateChoice _) = false
      | assocLeft (Parallel _) = false
      | assocLeft (ParallelAsync _) = false
      | assocLeft (ParallelSync _) = false
      | assocLeft (Par _) = false
      | assocLeft (Hide _) = false
      | assocLeft (Guard _) = false
      | assocLeft (Let _) = false
      | assocLeft (ValueChoice _) = false
      | assocLeft (Exit _) = false
      | assocLeft (Enable _) = true
      | assocLeft (Disable _) = false
      | assocLeft (CallProcess _) = false
      | assocLeft (Parentheses _) = false

    (**
     * Function: Lotos.assocRight
     *
     * Tests whether a behaviour operator is right associative.
     *
     * Parameters:
     *   b - behaviour
     *
     * Returns:
     *   true if the operator of the passed behaviour is right associative,
     *   false otherwise
     *)
    fun assocRight Stop = false
      | assocRight (Rendezvous _) = true
      | assocRight (Choice _) = true
      | assocRight (GateChoice _) = false
      | assocRight (Parallel _) = true
      | assocRight (ParallelAsync _) = true
      | assocRight (ParallelSync _) = true
      | assocRight (Par _) = false
      | assocRight (Hide _) = false
      | assocRight (Guard _) = false
      | assocRight (Let _) = false
      | assocRight (ValueChoice _) = false
      | assocRight (Exit _) = false
      | assocRight (Enable _) = true
      | assocRight (Disable _) = false
      | assocRight (CallProcess _) = false
      | assocRight (Parentheses _) = false

    fun behaviourToString tab tabIncr x =
        let
            val left = false
            val right = true
            fun isParentheses (Parentheses _) = true
              | isParentheses _ = false
            fun addParentheses parent side child =
                let
                    val pParent = priority parent
                    val pChild = priority child
                    val assoc = if side = right then assocRight else assocLeft
                    val add = if pParent = pChild then
                                  not (same parent child andalso assoc parent)
                              else
                                  pParent > pChild
                in
                    if not (isParentheses parent) andalso add then
                        Parentheses (addParentheses (Parentheses child) left child)
                    else
                        case child of
                            Stop => Stop
                          | Rendezvous (gates, offers, cond, b) =>
                            Rendezvous (gates, offers, cond, (addParentheses child right b))
                          | Choice (b1, b2) =>
                            Choice ((addParentheses child left b1), (addParentheses child right b2))
                          | GateChoice (gates, b) =>
                            GateChoice (gates, (addParentheses child right b))
                          | Parallel (b1, gates, b2) =>
                            Parallel ((addParentheses child left b1), gates, (addParentheses child right b2))
                          | ParallelAsync (b1, b2) =>
                            ParallelAsync ((addParentheses child left b1), (addParentheses child right b2))
                          | ParallelSync (b1, b2) =>
                            ParallelSync ((addParentheses child left b1), (addParentheses child right b2))
                          | Par (gates, parOp, b) =>
                            Par (gates, parOp, (addParentheses child right b))
                          | Hide (gates, b) =>
                            Hide (gates, (addParentheses child right b))
                          | Guard (cond, b) =>
                            Guard (cond, (addParentheses child right b))
                          | Let (bindings, b) =>
                            Let (bindings, (addParentheses child right b))
                          | ValueChoice (bindings, b) =>
                            ValueChoice (bindings, (addParentheses child right b))
                          | Exit values => Exit values
                          | Enable (b1, bindings, b2) =>
                            Enable ((addParentheses child left b1), bindings, (addParentheses child right b2))
                          | Disable (b1, b2) =>
                            Disable ((addParentheses child left b1), (addParentheses child right b2))
                          | CallProcess (id, gates, values) => CallProcess (id, gates, values)
                          | Parentheses b => Parentheses (addParentheses child left b)
                end
            fun bts tab b =
                let
                    val tab' = tab ^ tabIncr
                in
                    case b of
                        Stop => tab ^ "stop"
                      | Rendezvous (g, off, c, b) =>
                        tab ^ (Ident.toString g) ^ " " ^ (listToString "" " " offerToString off " ")
                        ^ (case c of
                               NONE => ""
                             | SOME v => (conditionToString v))
                        ^ ";\n" ^ (bts tab' b)
                      | Choice (b1, b2) =>
                        (bts tab b1) ^ "\n" ^ tab ^ "[]\n" ^ (bts tab b2)
                      | GateChoice (l, b) =>
                        tab ^ "choice " ^ (concatMap ", " varInDom l) ^ " []\n" ^ (bts tab' b)
                      | Parallel (b1, l, b2) =>
                        (bts tab b1) ^ "\n"
                        ^ tab ^ "|[" ^ (concatMap ", " gateToString l) ^ "]|\n"
                        ^ (bts tab b2)
                      | ParallelAsync (b1, b2) =>
                        (bts tab b1) ^ "\n"
                        ^ tab ^ "|||\n"
                        ^ (bts tab b2)
                      | ParallelSync (b1, b2) =>
                        (bts tab b1) ^ "\n"
                        ^ tab ^ "||\n"
                        ^ (bts tab b2)
                      | Par (l, parOp, b) =>
                        tab ^ "par " ^ (concatMap ", " varInDom l) ^ " " ^ (parallelOperatorToString parOp) ^ "\n"
                        ^ (bts tab' b)
                      | Hide (l, b) =>
                        tab ^ "hide " ^ (concatMap ", " gateToString l) ^ " in\n"
                        ^ (bts tab' b)
                      | Guard (c, b) =>
                        tab ^ (conditionToString c) ^ " ->\n"
                        ^ (bts tab' b)
                      | Let (l, b) =>
                        let
                            fun binding (ids, srt, vl) =
                                (concatMap ", " Ident.toString ids)
                                ^ " : " ^ (Ident.toString srt)
                                ^ " = " ^ (valueToString vl)
                        in
                            tab ^ "let " ^ (concatMap (",\n" ^ tab ^ "    ") binding l) ^ " in\n" ^
                            (bts tab' b)
                        end
                      | ValueChoice (l, b) =>
                        tab ^ "choice " ^ (concatMap ", " elemsInDom l) ^ " []\n"
                        ^ (bts tab' b)
                      | Exit [] => tab ^ "exit"
                      | Exit l => tab ^ "exit (" ^ (concatMap ", " exitValueToString l) ^ ")"
                      | Enable (b1, [], b2) =>
                        (bts tab b1) ^ "\n"
                        ^ tab ^ ">>\n" ^
                        (bts tab b2)
                      | Enable (b1, l, b2) =>
                        (bts tab b1) ^ " >> accept " ^ (concatMap ", " elemsInDom l) ^ " in\n"
                        ^ (bts tab' b2)
                      | Disable (b1, b2) =>
                        (bts tab b1) ^ " [>\n"
                        ^ (bts tab' b2)
                      | CallProcess (id, [], []) =>
                        tab ^ (Ident.toString id)
                      | CallProcess (id, gates, []) =>
                        tab ^ (Ident.toString id)
                        ^ " " ^ (surround_sq (concatMap ", " Ident.toString gates))
                      | CallProcess (id, [], params) =>
                        tab ^ (Ident.toString id)
                        ^ " " ^ (surround (concatMap ", " valueToString params))
                      | CallProcess (id, gates, params) =>
                        tab ^ (Ident.toString id)
                        ^ " " ^ (surround_sq (concatMap ", " Ident.toString gates))
                        ^ " " ^ (surround (concatMap ", " valueToString params))
                      | Parentheses b =>
                        tab ^ "(\n"
                        ^ (bts tab' b) ^ "\n"
                        ^ tab ^ ")"
                end
        in
            bts tab (addParentheses (Parentheses x) left x)
        end


    (* Processes *)

    datatype functionality = NoExit | ExitWith of Ident.t list

    datatype block = TypeBlock of typeDef
                   | ProcessBlock of process
    and process = Process of { name : Ident.t,
                               gates : gate list,
                               parameters : (Ident.t list * Ident.t) list,
                               func : functionality,
                               body : behaviour,
                               blocks : block list }

    fun functionalityToString NoExit = "noexit"
      | functionalityToString (ExitWith []) = "exit"
      | functionalityToString (ExitWith l) =
        "exit (" ^ (concatMap ", " Ident.toString l) ^ ")"

    fun blockToString tab _ (TypeBlock t) = typeDefToString tab t
      | blockToString tab tabIncr (ProcessBlock p) = processToString tab tabIncr p
    and processToString tab tabIncr (Process x) =
        let
            val gates = listToString " ["
                                     ", "
                                     gateToString
                                     (#gates x) "]"
            val parameters = listToString " ("
                                          ", "
                                          elemsInDom
                                          (#parameters x) ")"
            val blocks = listToString (tab ^ "where\n")
                                      "\n"
                                      (blockToString (tab ^ tabIncr) tabIncr)
                                      (#blocks x) ""
        in
            tab ^ "process " ^ (Ident.toString (#name x))
            ^ gates
            ^ parameters
            ^ " : " ^ (functionalityToString (#func x))
            ^ " :=\n"
            ^ (behaviourToString (tab ^ tabIncr) tabIncr (#body x)) ^ "\n"
            ^ blocks
            ^ tab ^ "endproc\n"
        end


    (* Specifications *)

    type specification = { name : Ident.t,
                           gates : gate list,
                           parameters : (Ident.t list * Ident.t) list,
                           func : functionality,
                           libraries : Ident.t list,
                           types : typeDef list,
                           body : behaviour,
                           blocks : block list }

    fun specificationToString (x : specification) =
        let
            val tab = "    "
            val gates = listToString " ["
                                     ", "
                                     gateToString
                                     (#gates x) "]"
            val parameters = listToString " ("
                                          ", "
                                          elemsInDom
                                          (#parameters x) ")"
            val blocks = listToString "\nwhere\n\n"
                                      "\n"
                                      (blockToString tab tab)
                                      (#blocks x) ""
        in
            "specification " ^ (Ident.toString (#name x))
            ^ gates
            ^ parameters
            ^ " : " ^ (functionalityToString (#func x)) ^ "\n\n"
            ^ (listToString (tab ^ "library ") ", " Ident.toString (#libraries x) " endlib\n\n")
            ^ (concatMap "\n" (typeDefToString tab) (#types x))
            ^ "\nbehaviour\n\n" ^ (behaviourToString tab tab (#body x)) ^ "\n"
            ^ blocks
            ^ "\nendspec\n"
        end

end
