(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module contains the code for the translation of Fiacre type definitions into
 * Lotos type declarations.
 *)

signature TYPES = sig
    (** Group: Exported declarations *)

    (**
     * Function: Types.translateType
     *
     * Translates a Fiacre type declaration into a LOTOS type definition.
     * Updates ".f" and ".t" files accordingly.
     * Adds the translated type to the passed map.
     *
     * Parameters:
     *   fFile - content of ".f" file
     *   tFile - content of ".t" file
     *   types - reference to a map from type names to their LOTOS definitions
     *   decl - type declaration to translate
     *
     * Returns:
     *   SOME <Lotos.typeDef> if the passed declaration is a type declaration,
     *   NONE otherwise
     *)
    val translateType : Helper.file -> Helper.file -> Type Helper.StringMap.map ref -> Declaration -> Lotos.typeDef option
end

structure Types :> TYPES = struct
    (** Group: Internal declarations *)

    open Lotos
    open Helper

    (**
     * Function: Types.translateIntervalType
     *
     * Translates a Fiacre interval type into a LOTOS type definition.
     * Updates ".f" and ".t" files accordingly.
     *
     * Parameters:
     *   fFile - content of ".f" file
     *   tFile - content of ".t" file
     *   name - type name
     *   n - interval lower bound
     *   m - interval upper bound
     *
     * Returns:
     *   a LOTOS type definition for the passed interval type
     *)
    fun translateIntervalType fFile tFile name n m =
        let
            val upName = toUpperCase name
            val nameID = Ident.make upName
            val constrID = Ident.make ("ITV_" ^ upName)
            val nInt = extractConstantFromExpression n
            val mInt = extractConstantFromExpression m
            val errorID = idForError name
            val ops =
                let
                    val o1 = { names = [(constrID, (SOME (Constructor ("C_" ^ upName))))],
                               parameters = [intID],
                               result = nameID }
                    val o2 = { names = [((idForIn name), NONE)],
                               parameters = [intID],
                               result = boolID }
                    val o3 = { names = [((idForToInt name), NONE)],
                               parameters = [nameID],
                               result = intID }
                    val o4 = { names = [((idForIntTo name), NONE)],
                               parameters = [intID],
                               result = nameID }
                    val o5 = { names = [(errorID, (SOME (ImplementedByExternal (Ident.toString errorID))))],
                               parameters = [],
                               result = nameID }
                    val o6 = { names = (map (fn s => ((Ident.make s), NONE))
                                            ["_<_", "_>_", "_<=_", "_>=_"]),
                               parameters = [nameID, nameID],
                               result = boolID }
                    val o7 = { names = [((Ident.make "-"), NONE)],
                               parameters = [nameID],
                               result = nameID }
                    val o8 = { names = (map (fn s => ((Ident.make s), NONE))
                                            ["_+_", "_-_", "_*_", "_div_", "_mod_"]),
                               parameters = [nameID, nameID],
                               result = nameID }
                in
                    [o1, o2, o3, o4, o5, o6, o7, o8, equalityOperations nameID]
                end
            val eqs =
                let
                    open EquationUtils
                    val nVal = EquationUtils.encodeInt nInt
                    val mVal = EquationUtils.encodeInt mInt
                    val name_w = Prefix (constrID, [wVal])
                    val name_z = Prefix (constrID, [zVal])
                    fun intToName x = Prefix ((idForIntTo name), [x])
                    fun nameToInt x = Prefix ((idForToInt name), [x])
                    fun inName x = Prefix ((idForIn name), [x])
                    val e1 = boolID /// [([((zVal </higherEqual/> nVal) &&& (zVal </lowerEqual/> mVal))
                                           =?= trueVal] ==> (inName zVal) === trueVal),
                                         ([] ==> (inName zVal) === falseVal)]
                    val e2 = intID /// [[] ==> (nameToInt name_z) === zVal]
                    val e3 = nameID /// [([(inName zVal) =?= trueVal] ==> (intToName zVal) === name_z),
                                         ([] ==> (intToName zVal) === (Const ((Ident.toString errorID))))]
                    val e4 = boolID /// [([] ==> (name_w </lowerThan/> name_z) === (wVal </lowerThan/> zVal)),
                                         ([] ==> (name_w </higherThan/> name_z) === (wVal </higherThan/> zVal)),
                                         ([] ==> (name_w </lowerEqual/> name_z) === (wVal </lowerEqual/> zVal)),
                                         ([] ==> (name_w </higherEqual/> name_z) === (wVal </higherEqual/> zVal))]
                    val e5 = nameID /// [([] ==> (neg name_w) === (intToName (neg wVal))),
                                         ([] ==> (name_w </plus/> name_z) === (intToName (wVal </plus/> zVal))),
                                         ([] ==> (name_w </minus/> name_z) === (intToName (wVal </minus/> zVal))),
                                         ([] ==> (name_w </mult/> name_z) === (intToName (wVal </mult/> zVal))),
                                         ([] ==> (name_w </div'/> name_z) === (intToName (wVal </div'/> zVal))),
                                         ([] ==> (name_w </mod'/> name_z) === (intToName (wVal </mod'/> zVal)))
                                      ]
                in
                    [e1, e2, e3, e4, e5, equalityEquations]
                end
            fun intToCString n =
                if n >= 0 then
                    Int.toString n
                else
                    "-" ^ (Int.toString (~n))
            fun encodeBound n =
                if n >= 0 then
                    "ADT_POS_INT (" ^ (intToCString n) ^ ")"
                else
                    "ADT_NEG_INT (" ^ (intToCString (~n - 1)) ^ ")"
            val infBound = encodeBound nInt
            val supBound = encodeBound mInt
        in
            appendFile fFile
                       (cErrorFunction upName
                                       ("ERROR_" ^ upName)
                                       ["ERROR while converting Int to interval type " ^ upName ^ ":",
                                        "Int value is out of interval range"]);
            appendFile tFile
                       ["#define ENUM_FIRST_" ^ upName ^ "() (C_" ^ upName ^ " (" ^ infBound ^ "))",
                        "",
                        "#define ENUM_NEXT_" ^ upName ^ "(CAESAR_ADT_0) \\",
			"        (ADT_LE_INT (((CAESAR_ADT_0).CAESAR_ADT_1_C_" ^ upName ^ " = ADT_SUCC_INT ((CAESAR_ADT_0).CAESAR_ADT_1_C_" ^ upName ^ ")), (" ^ supBound ^ ")))",
                        ""];
            TypeDeclaration { name = nameID,
                              parents = [booleanID, integerNumberID],
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [(nameID,
                                        (SOME (ImplementedByIteratedBy (upName,
                                                                        ("ENUM_FIRST_" ^ upName),
                                                                        ("ENUM_NEXT_" ^ upName)))))],
                              operations = ops,
                              equations = ([([xID, yID], nameID), ([wID, zID], intID)], eqs) }
        end

    fun flatten l =
        let
            val i = ref 0
            fun next () = (i := (!i) + 1; !i)
        in
            List.concat (map (fn (l', x) => map (fn e => (e, x, (next ()))) l') l)
        end

    (**
     * Function: Types.translateUnionType
     *
     * Translates a Fiacre union type into a LOTOS type definition.
     *
     * Parameters:
     *   name - type name
     *   l - constructor list
     *
     * Returns:
     *   a LOTOS type definition for the passed union type
     *)
    fun translateUnionType name l =
        let
            val flat = flatten l
            val upName = toUpperCase name
            val nameID = Ident.make upName
            val ops =
                let
                    val o1 =
                        map (fn (ids, t) => { names = map (fn id => ((Ident.make id),
                                                                     (SOME AnonConstructor))) ids,
                                              parameters = List.mapPartial typeToSortIdent [t],
                                              result = nameID }) l
                    val o2 = { names = map (fn (id, _, _) => ((idForIs name id), NONE)) flat,
                               parameters = [nameID],
                               result = boolID }
                    val o3 = List.mapPartial
                                 (fn (id, t, _) =>
                                     case (typeToSortIdent t) of
                                         SOME typeId =>
                                         SOME ({ names = [((idForGet name id), NONE)],
                                                 parameters = [nameID],
                                                 result = typeId })
                                       | NONE => NONE)
                                 flat
                in
                    o1 @ [o2] @ o3 @ [equalityOperations nameID]
                end
            val eqs =
                let
                    open EquationUtils
                    val e1 = map (fn (id, t, n) =>
                                     let
                                         val x_n =
                                             case typeToSortIdent t of
                                                 SOME _ => [Var (idForIndexed "X" n)]
                                               | NONE => []
                                     in
                                         boolID /// ([([] ==> (Prefix ((idForIs name id),

                                                                      [Prefix ((Ident.make id), x_n)])) === trueVal)]
                                                     @ (if (length flat) > 1 then
                                                            [([] ==> (Prefix ((idForIs name id), [xVal])) === falseVal)]
                                                        else
                                                            []))
                                     end)
                                 flat
                    val e2 = List.mapPartial
                                 (fn (id, t, n) =>
                                     case (typeToSortIdent t) of
                                         SOME typeId =>
                                         SOME (let
                                                   val x_n = Var (idForIndexed "X" n)
                                               in
                                                   typeId /// [[] ==> (Prefix ((idForGet name id),
                                                                               [Prefix ((Ident.make id), [x_n])])) === x_n]
                                               end)
                                       | NONE => NONE)
                                 flat
                in
                    e1 @ e2 @ [equalityEquations]
                end
        in
            TypeDeclaration { name = nameID,
                              parents = Ident.distinct (booleanID :: (List.mapPartial (typeToTypeIdent o #2)
                                                                                      l)),
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [(nameID, (SOME (ImplementedBy (upName))))],
                              operations = ops,
                              equations =
                              let
                                  val xy = [([xID, yID], nameID)]
                                  val xs = List.mapPartial
                                               (fn (_, t, n) =>
                                                    case typeToSortIdent t of
                                                        SOME typeId =>
                                                        SOME ([idForIndexed "X" n], typeId)
                                                      | NONE => NONE)
                                               flat
                              in
                                  ((xy @ xs), eqs)
                              end }
        end

    (**
     * Function: Types.translateRecordType
     *
     * Translates a Fiacre record type into a LOTOS type definition.
     *
     * Parameters:
     *   name - type name
     *   l - field list
     *
     * Returns:
     *   a LOTOS type definition for the passed record type
     *)
    fun translateRecordType name l =
        let
            val flat = flatten l
            val upName = toUpperCase name
            val nameID = Ident.make upName
            val ops =
                let
                    val o1 = { names = [(nameID, (SOME AnonConstructor))],
                               parameters = map (fn (_, t, _) => typeToActualSortIdent t) flat,
                               result = nameID }
                    val o2 = map (fn (id, t, _) =>
                                     { names = [((idForGet name id), NONE)],
                                       parameters = [nameID],
                                       result = typeToActualSortIdent t })
                                 flat
                    val o3 = map (fn (id, t, _) =>
                                     { names = [((idForSet name id), NONE)],
                                       parameters = [nameID, (typeToActualSortIdent t)],
                                       result = nameID })
                                 flat
                in
                    o1 :: (o2 @ o3 @ [equalityOperations nameID])
                end
            val eqs =
                let
                    open EquationUtils
                    val x_vect = Prefix (nameID,
                                         (map (fn (_, _, n) =>
                                                  Var (idForIndexed "X" n))
                                              flat))
                    fun x_vect' y = Prefix (nameID,
                                            (map (fn (_, _, n) =>
                                                     Var (idForIndexed (if n <> y then "X" else "Y") n))
                                                 flat))
                    val e1 = map (fn (id, t, n) =>
                                     let
                                         val typeIdent = typeToActualSortIdent t
                                         val x_n = Var (idForIndexed "X" n)
                                         val get_n = Prefix ((idForGet name id), [x_vect])
                                     in
                                         typeIdent /// [[] ==> get_n === x_n]
                                     end)
                                 flat
                    val e2 = nameID /// (map (fn (id, _, n) =>
                                                 let
                                                     val y_n = Var (idForIndexed "Y" n)
                                                     val set_n = Prefix ((idForSet name id), [x_vect, y_n])
                                                 in
                                                     [] ==> set_n === (x_vect' n)
                                                 end)
                                             flat)
                in
                    e1 @ [e2] @ [equalityEquations]
                end
        in
            TypeDeclaration { name = nameID,
                              parents = Ident.distinct (booleanID :: (List.mapPartial (typeToTypeIdent o #2) l)),
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [(nameID, (SOME (ImplementedBy (upName))))],
                              operations = ops,
                              equations =
                              let
                                  val xy = [([xID, yID], nameID)]
                                  val xys = map (fn (_, t, n) =>
                                                    ([(idForIndexed "X" n), (idForIndexed "Y" n)],
                                                     (typeToActualSortIdent t)))
                                                flat
                              in
                                  ((xys @ xy), eqs)
                              end }
        end

    (**
     * Function: Types.translateArrayType
     *
     * Translates a Fiacre array type into a LOTOS type definition.
     * Updates ".f" file accordingly.
     *
     * Parameters:
     *   fFile - content of ".f" file
     *   name - type name
     *   n - array size
     *   elementType - array element type
     *
     * Returns:
     *   a LOTOS type definition for the passed array type
     *)
    fun translateArrayType fFile name n elementType =
        let
            val upName = toUpperCase name
            val nameID = Ident.make upName
            val elementTypeID = typeToActualSortIdent elementType
            val size = extractConstantFromExpression n
            val error1ID = idForError1 name
            val error2ID = idForError2 name
            val ops =
                let
                    val o1 = { names = [(nameID, (SOME AnonConstructor))],
                               parameters = size >< (fn _ => elementTypeID),
                               result = nameID }
                    val o2 = { names = [((idForArrayGet name), NONE)],
                               parameters = [nameID, natID],
                               result = elementTypeID }
                    val o3 = { names = [((idForArraySet name), NONE)],
                               parameters = [nameID, natID, elementTypeID],
                               result = nameID }
                    val o4 = { names = [(error1ID,
                                         (SOME (ImplementedByExternal (Ident.toString error1ID))))],
                               parameters = [],
                               result = elementTypeID }
                    val o5 = { names = [(error2ID,
                                         (SOME (ImplementedByExternal (Ident.toString error2ID))))],
                               parameters = [],
                               result = nameID }
                in
                    [o1, o2, o3, o4, o5, equalityOperations nameID]
                end
            val eqs =
                let
                    open EquationUtils
                    val x_vect = Prefix (nameID, (size >< (fn i => Var (idForIndexed "X" i))))
                    fun x_vect_z z = Prefix (nameID, (size >< (fn i =>
                                                                  if i = z then
                                                                      zVal
                                                                  else
                                                                      Var (idForIndexed "X" i))))
                    val get = Prefix ((idForArrayGet name), [x_vect, pVal])
                    val set = Prefix ((idForArraySet name), [x_vect, pVal, zVal])
                    val e1 = elementTypeID /// ((size >< (fn i =>
                                                             let
                                                                 val x_i = Var (idForIndexed "X" i)
                                                             in
                                                                 [(pVal =?= (encodeNat i))] ==> get === x_i
                                                             end))
                                                @ [[] ==> get === (Const (Ident.toString error1ID))])
                    val e2 = nameID /// ((size >< (fn i =>
                                                      [(pVal =?= (encodeNat i))] ==> set === (x_vect_z i)))
                                         @ [[] ==> set === (Const (Ident.toString error2ID))])
                in
                    [e1, e2, equalityEquations]
                end
        in
            appendFile fFile
                       (cErrorFunction (Ident.toString elementTypeID)
                                       ("ERROR_" ^ upName ^ "_1")
                                       ["ERROR while getting an element in an array of type " ^ upName ^ ":",
                                        "index is out of array bounds"]);
            appendFile fFile
                       (cErrorFunction upName
                                       ("ERROR_" ^ upName ^ "_2")
                                       ["ERROR while setting an element in an array of type " ^ upName ^ ":",
                                        "index is out of array bounds"]);
            TypeDeclaration { name = nameID,
                              parents = Ident.distinct ([booleanID, naturalID]
                                                        @ (case typeToTypeIdent elementType of
                                                               SOME t => [t]
                                                             | NONE => [])),
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [(nameID, (SOME (ImplementedBy (upName))))],
                              operations = ops,
                              equations =
                              let
                                  val x_i = size >< (fn i => idForIndexed "X" i)
                                  val zxyp = [([zID], elementTypeID), ([xID, yID], nameID), ([pID], natID)]
                              in
                                  (([(x_i, elementTypeID)] @ zxyp), eqs)
                              end }
        end

    (**
     * Function: Types.translateQueueType
     *
     * Translates a Fiacre queue type into a LOTOS type definition.
     * Updates ".f" file accordingly.
     *
     * Parameters:
     *   fFile - content of ".f" file
     *   name - type name
     *   n - queue size
     *   elementType - queue element type
     *
     * Returns:
     *   a LOTOS type definition for the passed queue type
     *)
    fun translateQueueType fFile name n elementType =
        let
            val upName = toUpperCase name
            val nameID = Ident.make upName
            val elementTypeID = typeToActualSortIdent elementType
            val size = extractConstantFromExpression n
            val error1ID = idForError1 name
            val error2ID = idForError2 name
            val error3ID = idForError3 name
            val ops =
                let
                    val o1 = (size + 1) >< (fn i => { names = [((idForIndexed (name ^ "_") i),
                                                                (SOME AnonConstructor))],
                                                      parameters = i >< (fn _ => elementTypeID),
                                                      result = nameID })
                    val o2 = { names = [((idForFull name), NONE), ((idForEmpty name), NONE)],
                               parameters = [nameID],
                               result = boolID }
                    val o3 = { names = [((idForEnqueue name), NONE), ((idForAppend name), NONE)],
                               parameters = [nameID, elementTypeID],
                               result = nameID }
                    val o4 = { names = [((idForDequeue name), NONE)],
                               parameters = [nameID],
                               result = nameID }
                    val o5 = { names = [((idForFirst name), NONE)],
                               parameters = [nameID],
                               result = elementTypeID }
                    val o6 = { names = [(error1ID,
                                         (SOME (ImplementedByExternal (Ident.toString error1ID))))],
                               parameters = [],
                               result = nameID }
                    val o7 = { names = [(error2ID,
                                         (SOME (ImplementedByExternal (Ident.toString error2ID))))],
                               parameters = [],
                               result = nameID }
                    val o8 = { names = [(error3ID,
                                         (SOME (ImplementedByExternal (Ident.toString error3ID))))],
                               parameters = [],
                               result = elementTypeID }
                in
                    o1 @ [o2, o3, o4, o5, o6, o7, o8, equalityOperations nameID]
                end
            val eqs =
                let
                    open EquationUtils
                    fun x_i i = Var (idForIndexed "X" i)
                    fun vect_1_n n = n >< (fn i => Var (idForIndexed "X" (i + 1)))
                    fun name_n n v = Prefix ((idForIndexed (name ^ "_") n), v)
                    fun full v = Prefix ((idForFull name), [v])
                    fun empty v = Prefix ((idForEmpty name), [v])
                    fun enqueue v1 v2 = Prefix ((idForEnqueue name), [v1, v2])
                    fun append v1 v2 = Prefix ((idForAppend name), [v1, v2])
                    fun dequeue v = Prefix ((idForDequeue name), [v])
                    fun first v = Prefix ((idForFirst name), [v])
                    val e1 = boolID /// [([] ==> (full (name_n size (vect_1_n size))) === trueVal),
                                         ([] ==> (full xVal) === falseVal)]
                    val e2 = boolID /// [([] ==> (empty (name_n 0 [])) === trueVal),
                                         ([] ==> (empty xVal) === falseVal)]
                    val e3 = nameID /// (size + 1) >< (fn i =>
                                                          let
                                                              val right =
                                                                  if i < size then
                                                                      (name_n (i + 1) (vect_1_n (i + 1)))
                                                                  else
                                                                      Const (Ident.toString error1ID)
                                                          in
                                                              [] ==> (enqueue (name_n i (vect_1_n i))
                                                                              (x_i (i + 1))) === right
                                                          end)
                    val e4 = nameID /// (size + 1) >< (fn i =>
                                                          let
                                                              val right =
                                                                  if i < size then
                                                                      (name_n (i + 1) ((x_i (i + 1)) :: (vect_1_n i)))
                                                                  else
                                                                      Const (Ident.toString error1ID)
                                                          in
                                                              [] ==> (append (name_n i (vect_1_n i))
                                                                             (x_i (i + 1))) === right
                                                          end)
                    val e5 = nameID /// (size + 1) >< (fn i =>
                                                          let
                                                              val right =
                                                                  if i > 0 then
                                                                      (name_n (i - 1) (vect_1_n (i - 1)))
                                                                  else
                                                                      Const (Ident.toString error2ID)
                                                          in
                                                              [] ==> (dequeue (name_n i (vect_1_n i))) === right
                                                          end)
                    val e6 = elementTypeID /// (size + 1) >< (fn i =>
                                                                 let
                                                                     val right =
                                                                         if i > 0 then
                                                                             Var (Ident.make "x1")
                                                                         else
                                                                             Const (Ident.toString error3ID)
                                                                 in
                                                                     [] ==> (first (name_n i (vect_1_n i))) === right
                                                                 end)
                in
                    [e1, e2, e3, e4, e5, e6, equalityEquations]
                end
        in
            appendFile fFile
                       (cErrorFunction upName
                                       ("ERROR_" ^ upName ^ "_1")
                                       ["ERROR while adding an element to a queue of type " ^ upName ^ ":",
                                        "the queue is full"]);
            appendFile fFile
                       (cErrorFunction upName
                                       ("ERROR_" ^ upName ^ "_2")
                                       ["ERROR while removing an element to a queue of type " ^ upName ^ ":",
                                        "the queue is empty"]);
            appendFile fFile
                       (cErrorFunction (Ident.toString elementTypeID)
                                       ("ERROR_" ^ upName ^ "_3")
                                       ["ERROR while accessing an element to a queue of type " ^ upName ^ ":",
                                        "the queue is empty"]);
            TypeDeclaration { name = nameID,
                              parents = Ident.distinct (booleanID
                                                        :: (case typeToTypeIdent elementType of
                                                                SOME t => [t]
                                                              | NONE => [])),
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [(nameID, (SOME (ImplementedBy upName)))],
                              operations = ops,
                              equations =
                              let
                                  val x_i = (size + 1) >< (fn i => idForIndexed "X" (i + 1))
                                  val xy = [([xID, yID], nameID)]
                              in
                                  (([(x_i, elementTypeID)] @ xy), eqs)
                              end }
        end

    fun translateType fFile tFile types (TypeDecl (_, name, desc)) =
        let
            fun put () = types := StringMap.insert (!types, name, desc);
        in
            case desc of
                BoolType => NONE
              | NatType => NONE
              | IntType => NONE
              | NamedType _ => assertion TypeSynonym
              | IntervalType (n, m) => (put (); SOME (translateIntervalType fFile tFile name n m))
              | UnionType l => (put (); SOME (translateUnionType name l))
              | RecordType l => (put (); SOME (translateRecordType name l))
              | ArrayType (n, t) => (put (); SOME (translateArrayType fFile name n t))
              | QueueType (n, t) => (put (); SOME (translateQueueType fFile name n t))
              | AnyArray => assertion InvalidTypeDecl
              | AnyQueue => assertion InvalidTypeDecl
              | BotType => assertion InvalidTypeDecl
              | TopType => assertion InvalidTypeDecl
        end
      | translateType _ _ _ _ = NONE

end
