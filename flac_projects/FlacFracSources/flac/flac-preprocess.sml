(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

signature PREPROCESS = sig
  val preprocess : Fiacre -> Fiacre
end

structure Preprocess :> PREPROCESS = struct

(* utilities *)

(* lexicographic list comparisons *)
fun rcompare _ [] (_ :: _) = LESS
  | rcompare _ (_ :: _) [] = GREATER
  | rcompare cmp (h1 :: t1) (h2 :: t2) = (case cmp (h1,h2) of EQUAL => rcompare cmp t1 t2 | b => b)
  | rcompare _ [] [] = EQUAL;

(* user defined type and channel names *)
val USERNAMES = ref ([] : string list);

(* used to creates new names for types and channels *)
val newName =
    let
        val no = ref 0
    in
        fn prefix =>
           let
               val n = prefix ^ Int.toString (!no)
           in
               no := 1 + !no;
               n
           end
    end

(* holds processed declarations *)
val DECLS = ref ([] : Declaration list);

(* holds defined constants *)
structure StringMap = SplayMapFn (struct
                                      type ord_key = string
                                      val compare = String.compare
                                  end)
val CONSTS = ref (StringMap.empty : Exp StringMap.map);



(* for types ---------------------------------------------------------------------------------- *)

(* type keyed avl of strings *)
structure typeKey : KEY = struct
type key = Type;
fun level ty = case ty of
	  BoolType => 1
	| IntType => 2
	| NamedType _ => 3
	| UnionType _ => 4
	| RecordType _ => 5
	| ArrayType _ => 6
	| QueueType _ => 7
	| IntervalType _ => 8
        | _ => (* unexpected type (Bot, Top, Any_) *) 10;
fun compare (x,y) = case Int.compare (level x,level y) of
	  EQUAL => (case (x,y) of
		(NamedType s1, NamedType s2) => String.compare (s1,s2) 
	      | (UnionType ctyl1, UnionType ctyl2) =>
		rcompare (fn ((c1,ty1),(c2,ty2)) => case rcompare String.compare c1 c2 of EQUAL => compare(ty1,ty2) | b => b) ctyl1 ctyl2
	      | (RecordType ftyl1, RecordType ftyl2) =>
		rcompare (fn ((f1,ty1),(f2,ty2)) => case rcompare String.compare f1 f2 of EQUAL => compare(ty1,ty2) | b => b) ftyl1 ftyl2
	      | (ArrayType (IntExp (sz1,_),ty1), ArrayType (IntExp (sz2,_),ty2)) =>
		(case Int.compare (sz1,sz2) of EQUAL => compare (ty1,ty2) | b => b)
	      | (QueueType (IntExp (sz1,_),ty1), QueueType (IntExp (sz2,_),ty2)) =>
		(case Int.compare (sz1,sz2) of EQUAL => compare (ty1,ty2) | b => b)
	      | (IntervalType (IntExp (f1,ref IntType),IntExp (t1,ref IntType)),IntervalType (IntExp (f2,ref IntType),IntExp (t2,ref IntType))) =>
		(case Int.compare (f1,f2) of EQUAL => Int.compare (t1,t2) | b => b)
	      | _ => EQUAL)
	| v => v;
fun x < y = case compare (x,y) of LESS => true | _ => false;
fun x > y = case compare (x,y) of GREATER => true | _ => false;
end;
structure tyAvlTree = avlTreef (typeKey);

(* names avl : type -> name, if any *)
val TYNAMES = tyAvlTree.newTree() : String.string tyAvlTree.tree ref;
fun insertType ty n = TYNAMES := tyAvlTree.insert (ty,n,!TYNAMES);
fun lookupType ty = tyAvlTree.getData (ty,!TYNAMES);

(* creates a name for a type and its constituents, making sure names are new *)
fun nameType on pref ty =
    (* if ty is named, return its name, otherwise generate a new name and stores it *)
    case lookupType ty of 
	SOME n => (case on of
		       NONE => NamedType n
		     | SOME n' => (* rename n by suggested n' *)
		       (TYNAMES := tyAvlTree.delete (!TYNAMES,ty);
			DECLS := TypeDecl (~1,n',ty) :: !DECLS;
			insertType ty n';
			NamedType n'))
      | _ => let val n = ref ""
	     in case on of NONE => (n := newName pref; while mem (!n) (!USERNAMES) do n := newName pref)
			 | SOME x => n := x;
		DECLS := TypeDecl (0,!n,ty) :: !DECLS;
		insertType ty (!n);
		NamedType (!n)
	     end;

(* returns alias for ty, unless is a constant type *)
(* constants have been computed, and names expanded *)
fun aliasType n ty = case ty of
	  NatType => IntType
	| IntervalType _ => IntType
	| UnionType ctyl => nameType n "u" (UnionType (map (fn a as (_,BotType) => a | (c,cty) => (c,aliasType NONE cty)) ctyl))
	| RecordType ftyl => nameType n "r" (RecordType (map (fn (f,fty) => (f,aliasType NONE fty)) ftyl))
	| ArrayType (sz,ety) => nameType n "a" (ArrayType (sz,aliasType NONE ety))
	| QueueType (sz,ety) => nameType n "q" (QueueType (sz,aliasType NONE ety))
	| _ => ty;
(* same except keeps interval types *)
fun aliasType2 n ty = case ty of
	  NatType => IntType
	| IntervalType _ => nameType n "i" ty
	| UnionType ctyl => nameType n "u" (UnionType (map (fn a as (_,BotType) => a | (c,cty) => (c,aliasType2 NONE cty)) ctyl))
	| RecordType ftyl => nameType n "r" (RecordType (map (fn (f,fty) => (f,aliasType2 NONE fty)) ftyl))
	| ArrayType (sz,ety) => nameType n "a" (ArrayType (sz,aliasType2 NONE ety))
	| QueueType (sz,ety) => nameType n "q" (QueueType (sz,aliasType2 NONE ety))
	| _ => ty;




(* for channels ---------------------------------------------------------------------------------- *)

(* channel keyed avl of strings *)
structure channelKey : KEY = struct
type key = Channel;
fun level ch = case ch of
	  NamedChannel _ => 1
	| ProfileChannel _ => 2
fun compare (x,y) = case Int.compare (level x,level y) of
	  EQUAL => (case (x,y) of
		(NamedChannel s1, NamedChannel s2) => String.compare (s1,s2) 
	      | (ProfileChannel tyl1, ProfileChannel tyl2) => rcompare typeKey.compare tyl1 tyl2
	      | _ => EQUAL)
	| v => v;
fun x < y = case compare (x,y) of LESS => true | _ => false;
fun x > y = case compare (x,y) of GREATER => true | _ => false;
end;
structure chAvlTree = avlTreef (channelKey);

(* names avl : channel -> name, if any *)
val CHNAMES = chAvlTree.newTree() : String.string chAvlTree.tree ref;
fun insertChannel ty n = CHNAMES := chAvlTree.insert (ty,n,!CHNAMES);
fun lookupChannel ty = chAvlTree.getData (ty,!CHNAMES);

(* creates a name for a channel, making sure it is different from all user defined names *)
fun nameChannel on pref ch =
    (* if ch is named, return its name, otherwise generate a new name and store it *)
    case lookupChannel ch of
	SOME n => (case on of
		       NONE => NamedChannel n
		     | SOME n' => (* rename n by suggested n' *)
		       (CHNAMES := chAvlTree.delete (!CHNAMES,ch);
			DECLS := ChannelDecl (~1,n',ch) :: !DECLS;
			insertChannel ch n';
			NamedChannel n'))
      | _ => let val n = ref ""
	     in case on of NONE => (n := newName pref; while mem (!n) (!USERNAMES) do n := newName pref)
			 | SOME x => n := x;			
		DECLS := ChannelDecl (0,!n,ch) :: !DECLS;
		insertChannel ch (!n);
		NamedChannel (!n)
	     end;

(* returns alias for ty, unless is a constant type *)
fun aliasChannel n ch =
    case ch of
        ProfileChannel [] => ch
      | ProfileChannel [ty] => ProfileChannel [aliasType2 NONE ty]
      | ProfileChannel tyl => nameChannel n "c" (ProfileChannel (map (aliasType2 NONE) tyl))
      | _ => raise Fail "aliasChannel"




(* converts sources to lotos requirements ------------------------------------------------------------------ *)
(* - literals tagged with their "new" types, named;
   - arith primitives tagged with their "old" types, named;
   - queue primitives tagged with their "new" types (only sizes useful);
   - ports tagged with their "new" types, named;
*)

fun getTypeBody n =
    let
        val td = List.find (fn TypeDecl (_,a,_) => a=n | _ => false) (!DECLS)
    in
        case Option.valOf td of
            TypeDecl (_, _, ty) => ty
          | _ => raise Fail "getTypeBody"
    end

fun getChannelBody n =
    let
        val cd = List.find (fn ChannelDecl (_,a,_) => a=n | _ => false) (!DECLS)
    in
        case Option.valOf cd of
            ChannelDecl (_, _, ty) => ty
          | _ => raise Fail "getChannelBody"
    end

fun consDecl d = DECLS := d :: !DECLS;

(* converts types captured in program *)
fun conv d = case d of
	  TypeDecl (l,a,b) =>
		(case aliasType2 (SOME a) b of
		     NamedType n => if n=a then () else consDecl (TypeDecl (l,a,getTypeBody n))
		   | ty => consDecl (TypeDecl (l,a,ty)))
	| ChannelDecl (l,a,b) =>
		(case aliasChannel (SOME a) b of
		     NamedChannel n => if n=a then () else consDecl (ChannelDecl (l,a,getChannelBody n))
		   | ch => consDecl (ChannelDecl (l,a,ch)))
	| ConstantDecl (l,n,ty,e) =>
          (CONSTS := StringMap.insert ((!CONSTS), n, convExp e);
           consDecl (ConstantDecl (l,n,aliasType NONE ty,convExp e)))
	| ProcessDecl (l,a,b,d,e,f,p,j) =>
	  consDecl (ProcessDecl (l,a,
				 map (fn (pals,attrl,ch) => (pals,attrl,aliasChannel NONE ch)) b,
				 map (fn (vals,attrl,ty) => (vals,attrl,aliasType NONE ty)) d,
				 e,
				 map (fn (a,ty,c) => (a,aliasType NONE ty,setVar c ty)) f,
				 case p of SOME (l,s) => SOME (l,convStm s) | _ => p,
				 map (fn (l,g,s) => (l,g,convStm s)) j))
	| ComponentDecl (l,a,b,d,e,f,g,p,h) =>
	  consDecl (ComponentDecl (l,a,
				   map (fn (pals,attrl,ch) => (pals,attrl,aliasChannel NONE ch)) b,
				   map (fn (vals,attrl,ty) => (vals,attrl,aliasType NONE ty)) d,
				   map (fn (a,ty,c) => (a,aliasType NONE ty,setVar c ty)) e,
				   map (fn (pprl,lr) => (map (fn (pals,attrl,ch) => (pals,attrl,aliasChannel NONE ch)) pprl,lr)) f,
				   g,
				   case p of SOME (l,s) => SOME (l,convStm s) | _ => p,
				   convComp h))
	| d => consDecl d (* should not happen *)

(* computes a legal initial value for a non-initialized variable of type ty *)
(* note: safe assuming noninitialized vars are initialized before their first use *)
and setVar (SOME e) _ = SOME (convExp e)
  | setVar NONE ty =
    SOME (case ty of
	      BoolType => BoolExp false
	    | NatType => IntExp (0,ref IntType)
	    | IntType => IntExp (0,ref IntType)
	    | IntervalType (f,_) => f
	    | UnionType ((u::_,BotType)::_) => IdentExp (u,ref (ConstrKind,aliasType NONE ty))
	    | UnionType ((u::_,ty)::_) => ConstrExp (u, Option.valOf (setVar NONE ty), ref ty)
	    | RecordType fltyl => RecordExp (flat (map (fn (fl,ty) => map (fn f => (f,Option.valOf (setVar NONE ty))) fl) fltyl), ref (aliasType NONE ty))
	    | ArrayType (IntExp (k,_),ety) => ArrayExp (List.tabulate (k,fn _ => Option.valOf (setVar NONE ety)), ref (aliasType NONE ty))
	    | QueueType _ => QueueExp ([],ref (aliasType NONE ty))
	    | _ => BoolExp false) (* should not happen *)

(* expands constants in expressions *)
and expConst e =
    case e of
  	ParenExp e1 => ParenExp (expConst e1)
      | CondExp (e1, e2, e3, t) => CondExp (expConst e1, expConst e2, expConst e3, t)
      | InfixExp (inf, t, e1, e2) => InfixExp (inf, t, expConst e1, expConst e2)
      | BinopExp (bin, t, e1, e2) => BinopExp (bin, t, expConst e1, expConst e2)
      | UnopExp (un, t, e1) => UnopExp (un, t, expConst e1)
      | RecordExp (l, t) => RecordExp (map (fn (s, e) => (s, expConst e)) l, t)
      | QueueExp (l, t) => QueueExp (map expConst l, t)
      | ArrayExp (l, t) => ArrayExp (map expConst l, t)
      | RefExp (s, t) => RefExp (s, t)
      | ConstrExp (s, e1, t) => ConstrExp (s, expConst e1, t)
      | IntExp (i, t) => IntExp (i, t)
      | BoolExp b => BoolExp b
      | IdentExp (s, ref (ConstantKind, t)) =>
        expConst (Option.valOf (StringMap.find ((!CONSTS), s)) handle _ => IdentExp (s, ref (ConstantKind, t)))
      | IdentExp (s, ref (k, t)) => IdentExp (s, ref (k, t))
      | AnyExp => AnyExp
      | ArrayAccessExp (e1, e2, t) => ArrayAccessExp (expConst e1, expConst e2, t)
      | RecordAccessExp (e1, s, t) => RecordAccessExp (expConst e1, s, t)

(* converts types captured in expressions, where needed *)
(* preserves original types for arithmetic primitives *)
and convExp exp =
    let
        val e = expConst exp
    in
        case e of
    	    ParenExp e1 => ParenExp (convExp e1)
          | CondExp (e1, e2, e3, t) => CondExp (convExp e1, convExp e2, convExp e3, t)
          | RecordAccessExp (a,f,r) => (r := aliasType NONE (!r); RecordAccessExp (convExp a,f,r))
          | ArrayAccessExp (a,i,r) => (r := aliasType NONE (!r); ArrayAccessExp (convExp a,convExp i,r))
          | InfixExp (ope,r,e1,e2) => (r := aliasType2 NONE (!r); InfixExp (ope,r,convExp e1,convExp e2))
          | BinopExp (ope,r,q,ee) =>
            (* for queue ops, can take either new or old types, since only length is useful *)
	        (r := aliasType NONE (!r); BinopExp (ope,r,convExp q,convExp ee))
          | UnopExp (ope,r,ee) =>
            (* for queue ops can take either new or old types, since only length relevant *)
	        (if mem ope [MINUS,PLUS,COERCE] then r := aliasType2 NONE (!r) else r := aliasType NONE (!r); UnopExp (ope,r,convExp ee))
          | ConstrExp (c,e',r) => ConstrExp (c,convExp e',r)
          | IntExp (_,r) => (r := aliasType NONE (!r); e)
          | QueueExp (el,r) => (r := aliasType NONE (!r); QueueExp (map convExp el,r))
          | RecordExp (fl,r) => (r := aliasType NONE (!r); RecordExp (map (fn (f,e') => (f,convExp e')) fl,r))
          | ArrayExp (il,r) => (r := aliasType NONE (!r); ArrayExp (map convExp il,r))
          | IdentExp (_,r as ref (knd,ty)) => (r := (knd,aliasType NONE ty); e)
          | _ => e
    end

(* converts types captured in statements, where needed *)
and convStm s = case s of
	  StSequence (line,s1,s2) => StSequence (line,convStm s1,convStm s2)
	| StCond (line,e1,s1,e2s2l,NONE) =>
	  StCond (line,convExp e1,convStm s1,map (fn (l,e,s) => (l, convExp e, convStm s)) e2s2l, NONE)
	| StCond (line,e1,s1,e2s2l,SOME s3) =>
	  StCond (line,convExp e1,convStm s1,map (fn (l,e,s) => (l, convExp e, convStm s)) e2s2l,SOME (convStm s3))
	| StWhile (line,e,s) => StWhile (line,convExp e,convStm s)
	| StSelect sl => StSelect (map convStm sl)
	| StSignal (_,_,r) => (r := aliasChannel NONE (!r); s)
	| StInput (line,p,r,pl,NONE) => (r := aliasChannel NONE (!r); StInput (line,p,r,map convExp pl,NONE))
	| StInput (line,p,r,pl,SOME exp) => (r := aliasChannel NONE (!r); StInput (line,p,r,map convExp pl,SOME (convExp exp)))
	| StOutput (line,p,r,el) => (r := aliasChannel NONE (!r); StOutput (line,p,r,map convExp el))
	| StAssign (line,patts,exps) => StAssign (line,map convExp patts,map convExp exps)
	| StAny (line,patts,ref types,NONE) => StAny (line,map convExp patts,ref (map (aliasType2 NONE) types),NONE)
	| StAny (line,patts,ref types,SOME exp) => StAny (line,map convExp patts,ref (map (aliasType2 NONE) types),SOME (convExp exp))
	| StCase (line,e,psl) => StCase (line,convExp e,map (fn (l,a,p,s') => (l,a,convExp p,convStm s')) psl)
        | StForeach (line,id,ref t,s) => StForeach (line,id,ref (aliasType2 NONE t),convStm s)
        | StNull => StNull
        | StTo (l,s) => StTo (l,s)

and convComp c = case c of
	  ParComp (l,e,psl) => ParComp (l,e,map (fn (e',c') => (e',convComp c')) psl)
        | InstanceComp (l,p,el) => InstanceComp (l,p,map convExp el);


fun preprocess (p as Program (dl,main)) =
    if !PRELOTOS then
        (USERNAMES := List.mapPartial (fn TypeDecl (_,n,_) => SOME n | ChannelDecl (_,n,_) => SOME n | _ => NONE) dl;
	 List.app conv dl;
	 Program (rev (!DECLS), main))
    else
        p

end


