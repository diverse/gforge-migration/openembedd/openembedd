(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module contains the code for the translation of Fiacre processes/components
 * definitions into Lotos processes declarations.
 *)

signature PROCESSES = sig
    (** Group: Exported declarations *)

    (**
     * Function: Processes.translateProcessOrComponent
     *
     * Translates a Fiacre process or component declaration into a LOTOS process.
     *
     * Parameters:
     *   shared - whether the specification uses shared variables
     *   infos - type informations (<Helper.typeInfos>)
     *   typeProcesses - the set of type process that must be generated
     *                   for the specification to be valid
     *   d - Fiacre declaration to translate
     *
     * Returns:
     *   SOME <Lotos.process> if the passed Fiacre declaration is either a process or
     *   a component declaration, NONE otherwise
     *)
    val translateProcessOrComponent : bool -> Helper.typeInfos -> (Helper.StringSet.set ref) -> Declaration -> Lotos.process option

    (**
     * Function: Processes.generateTypeProcess
     *
     * Returns the type process for a given type name.
     *
     * Parameters:
     *   t - type name
     *
     * Returns:
     *   a process that recursively read or write a value of a given type
     *)
    val generateTypeProcess : string -> Lotos.process

    (**
     * Function: Processes.getSignature
     *
     * Returns the process signature associated with a given declaration.
     *
     * Parameters:
     *   d - Fiacre declaration
     *
     * Returns:
     *   SOME s where s is the signature of the process associated with the passed declaration
     *   if this declaration is either a process or component declaration, NONE otherwise
     *)
    val getSignature : Declaration -> (string * ((Arg * (VarAttr list) * Type) list)) option
end

structure Processes :> PROCESSES = struct
    (** Group: Internal declarations *)

    open Lotos
    open Helper

    fun flattenPorts l =
        List.concat (map (fn (ids, attrs, ch) =>
                             let
                                 val attrs' = if attrs <> [] then attrs else [IN, OUT]
                             in
                                 map (fn (_, id) => (id, attrs', ch)) ids
                             end)
                         l)

    fun flattenParams l =
        List.concat (map (fn (args, attrs, t) =>
                             let
                                 val attrs' = if attrs <> [] then attrs else [READ, WRITE]
                             in
                                 map (fn (_, arg) => (arg, attrs', t)) args
                             end)
                         l)

    fun flattenVars infos l =
        List.concat (map (fn (ids, t, e) =>
                             let
                                 val e' = case e of SOME x => x
                                                  | NONE => Expressions.default (#types infos) t
                             in
                                 map (fn (_, id) => (id, t, e')) ids
                             end)
                         l)

    fun gateList shared ports params =
        let
            fun gatesForParam ((RefArg id), _, _) = [("READ_" ^ id), ("WRITE_" ^ id)]
              | gatesForParam _ = []
            val paramGates = if shared then
                                 (List.concat (map gatesForParam params)) @ ["lock", "unlock"]
                             else
                                 []
        in
            map Ident.make ((map (fn (id, _, _) => id) ports) @ paramGates)
        end

    fun normalize infos s =
        let
            fun linearize s =
                case s of
                    StNull => StNull
                  | StCond (ln, c, i, l, e) => StCond (ln,
                                                       c,
                                                       (linearize i),
                                                       (map (fn (ln, e, s) => (ln, e, (linearize s))) l),
                                                       (case e of SOME e' => SOME (linearize e') | NONE => NONE))
                  | StSelect l => StSelect (map linearize l)
                  | StSequence (ln, (StSequence (ln', s1', s2')), s2) =>
                    linearize (StSequence (ln, s1', (StSequence (ln', s2', s2))))
                  | StSequence (ln, s1, s2) => StSequence (ln, s1, (linearize s2))
                  | StTo _ => s
                  | StWhile (ln, e, s) => StWhile (ln, e, (linearize s))
                  | StAssign _ => s
                  | StAny _ => s
                  | StSignal _ => s
                  | StInput _ => s
                  | StOutput _ => s
                  | StCase (ln, e, l) => StCase (ln, e, (map (fn (l, a, p, s) => (l, a, p, (linearize s))) l))
                  | StForeach (ln, id, t, s) => StForeach (ln, id, t, (linearize s))
            fun containsCommunicationStatement s =
                case s of
                    StNull => false
                  | StCond (_, _, i, l, e) =>
                    (containsCommunicationStatement i)
                    orelse (List.exists (fn (_, _, s') => containsCommunicationStatement s') l)
                    orelse (case e of SOME e' => containsCommunicationStatement e' | NONE => false)
                  | StSelect l => List.exists containsCommunicationStatement l
                  | StSequence (_, s1, s2) =>
                    (containsCommunicationStatement s1) orelse (containsCommunicationStatement s2)
                  | StTo _ => false
                  | StWhile (_, _, s') => containsCommunicationStatement s'
                  | StAssign _ => false
                  | StAny _ => false
                  | StSignal _ => true
                  | StInput _ => true
                  | StOutput _ => true
                  | StCase (_, _, l) => List.exists (fn (_, a, _, s') => !a andalso containsCommunicationStatement s') l
                  | StForeach (_, _, _, s') => containsCommunicationStatement s'
            fun nextStateReachable reachable s =
                case s of
                    StNull => reachable
                  | StCond (_, _, i, l, e) =>
                    (nextStateReachable reachable i)
                    andalso (List.all ((nextStateReachable reachable) o #3) l)
                    andalso (case e of SOME e' => nextStateReachable reachable e' | NONE => true)
                  | StSelect l => List.all (nextStateReachable reachable) l
                  | StSequence (_, s1, s2) =>
                    if containsCommunicationStatement s1 then
                        nextStateReachable false s2
                    else
                        (nextStateReachable reachable s1) orelse (nextStateReachable reachable s2)
                  | StTo _ => true
                  | StWhile _ => reachable
                  | StAssign _ => reachable
                  | StAny _ => reachable
                  | StSignal _ => false
                  | StInput _ => false
                  | StOutput _ => false
                  | StCase (_, _, l) => List.all ((nextStateReachable reachable) o #4) (List.filter (fn (_, ref a, _, _) => a) l)
                  | StForeach _ => reachable
            fun containsTo s =
                case s of
                    StNull => false
                  | StCond (_, _, i, l, e) =>
                    (containsTo i)
                    orelse (List.exists (fn (_, _, s') => containsTo s') l)
                    orelse (case e of SOME e' => containsTo e' | NONE => false)
                  | StSelect l => List.exists containsTo l
                  | StSequence (_, s1, s2) =>
                    (containsTo s1) orelse (containsTo s2)
                  | StTo _ => true
                  | StWhile (_, _, s') => containsTo s'
                  | StAssign _ => false
                  | StAny _ => false
                  | StSignal _ => false
                  | StInput _ => false
                  | StOutput _ => false
                  | StCase (_, _, l) => List.exists (fn (_, _, _, s') => containsTo s') (List.filter (fn (_, ref a, _, _) => a) l)
                  | StForeach (_, _, _, s') => containsTo s'
            val vars = ref []
            fun norm1 (c : Statement) (B : Statement) =
                if not (containsTo B) then
                    B
                else
                    case B of
                        StTo _ => StSequence (~1, c, B)
                      | StCond (ln, cond, i, l, e) =>
                        StCond (ln,
                                cond,
                                (norm1 c i),
                                (map (fn (ln, e, s) => (ln, e, (norm1 c s))) l),
                                (case e of SOME e' => SOME (norm1 c e') | NONE => NONE))
                      | StCase (ln, e, l) => StCase (ln, e, (List.mapPartial (fn (ln', ref a, p, s) => if a then SOME (ln', ref a, p, (norm1 c s)) else NONE) l))
                      | StSelect l => StSelect (map (fn s => norm1 c s) l)
                      | StWhile (ln, e, s) => StWhile (ln, e, (norm1 c s))
                      | StSequence (ln, s1, s2) => StSequence (ln, (norm1 c s1), (norm1 c s2))
                      | _ => raise Fail "Processes.normalize/norm1"
            fun norm0 B1 B2 =
                if nextStateReachable true B1 then
                    if B2 <> StNull then
                        StSequence (~1, B1, B2)
                    else
                        B1
                else
                    case B1 of
                        StSequence (_, B1_1, B1_2) =>
                        if not (containsCommunicationStatement B1_1) then
                            StSequence (~1, B1_1, (norm0 B1_2 B2))
                        else
                            norm0 B1_1 (linearize (StSequence (~1, B1_2, B2)))
                      | StCond (ln, c, i, l, e) =>
                        StCond (ln,
                                c,
                                (norm0 i B2),
                                (map (fn (ln, e, s) => (ln, e, (norm0 s B2))) l),
                                (case e of SOME e' => SOME (norm0 e' B2) | NONE => NONE))
                      | StCase (ln, e, l) => StCase (ln, e, (map (fn (l, a, e, s) => (l, a, e, (norm0 s B2))) (List.mapPartial (fn (l, ref a, p, s) => if a then SOME (l, ref a, p, s) else NONE) l)))
                      | StSelect l => StSelect (map (fn s => norm0 s B2) l)
                      | StSignal _ => norm1 B1 B2
                      | StInput (_, id, (ref channel), patt, cond) =>
                        let
                            val profile = case channel of
                                              ProfileChannel p => p
                                            | NamedChannel n => Option.valOf (StringMap.find ((#channels infos), n))
                            val zi = ListPair.zipEq ((map (fn _ => Ident.new "NORM_") patt), profile)
                            val zi' = map (fn (id, t) => IdentExp ((Ident.toString id), ref (VarKind, t))) zi
                            val patt' = map (fn (p, z) => if p = AnyExp then z else p)
                                            (ListPair.zipEq (patt, zi'))
                            val affect1 = StAny (~1, patt', ref profile, cond)
                            val affect2 = StAssign (~1, zi', patt')
                            val output = StOutput (~1, id, (ref channel), zi')
                        in
                            vars := zi @ (!vars);
                            StSequence (~1, affect1, (StSequence (~1, affect2, (norm1 output B2))))
                        end
                      | StOutput (_, id, (ref channel), exprs) =>
                        let
                            val profile = case channel of
                                              ProfileChannel p => p
                                            | NamedChannel n => Option.valOf (StringMap.find ((#channels infos), n))
                            val zi = ListPair.zipEq ((map (fn _ => Ident.new "NORM_") exprs), profile)
                            val zi' = map (fn (id, t) => IdentExp ((Ident.toString id), ref (VarKind, t))) zi
                            val affect = StAssign (~1, zi', exprs)
                            val output = StOutput (~1, id, (ref channel), zi')
                        in
                            vars := zi @ (!vars);
                            StSequence (~1, affect, (norm1 output B2))
                        end
                      | _ => raise Fail "Processes.normalize/norm0"
            val linearized = linearize s
        in
            if nextStateReachable true linearized then
                (linearized, [])
            else
                ((linearize (norm0 linearized StNull)),
                 (map (fn (id, t) => ((Ident.toString id), t, (Expressions.default (#types infos) t))) (!vars)))
        end

    fun typeOfExpression e =
        case e of
            ParenExp e' => typeOfExpression e'
          | CondExp (_, _, _, ref t) => t
          | InfixExp (_, ref t, _, _) => t
          | BinopExp (_, ref t, _, _) => t
          | UnopExp (_, ref t, _) => t
          | RecordExp (_, ref t) => t
          | QueueExp (_, ref t) => t
          | ArrayExp (_, ref t) => t
          | RefExp (_, ref t) => t
          | ConstrExp (_, _, ref t) => t
          | IntExp (_, ref t) => t
          | BoolExp _ => BoolType
          | IdentExp (_, ref (_, t)) => t
          | AnyExp => assertion UnexpectedAnyExpr
          | ArrayAccessExp (_, _, ref t) => t
          | RecordAccessExp (_, _, ref t) => t

    (* v{x := v'} *)
    fun replace (v : value) (x : string) (v' : value) =
        case v of
            Const _ => v
          | Var id => if (Ident.toString id) = x then v' else v
          | Prefix (func, l) => Prefix (func, (map (fn v => replace v x v') l))
          | Infix (v1, id, v2) => Infix ((replace v1 x v'), id, (replace v2 x v'))
          | Of (v, id) => Of ((replace v x v'), id)

    fun trans (infos : Helper.typeInfos) (S : Statement) (P : Ident.t) (G : Ident.t list) Z Y (w : Ident.t list) (B : behaviour) (lock : bool) additionalProcesses =
        let
            fun expr x y = Expressions.expr (#types infos) x y
            val z = map #1 Z
            val y = map #1 Y
            open EquationUtils
            fun write [] base = Rendezvous (unlockID, [], NONE, base)
              | write (id :: tl) base =
                Rendezvous ((Ident.make ("WRITE_" ^ (Ident.toString id))),
                            [Send (Var id)],
                            NONE,
                            (write tl base))
                fun match (ParenExp e) v T = match e v T
                  | match AnyExp _ _ = trueVal
                  | match (IntExp (n, _)) v T = (v </eq/> (Of ((encodeInt n), T)))
                  | match (BoolExp true) v _ = (v </eq/> trueVal)
                  | match (BoolExp false) v _ = (v </eq/> falseVal)
                  | match (IdentExp (_, ref (VarKind, _))) _ _ = trueVal
                  | match (ArrayAccessExp _) _ _ = trueVal
                  | match (RecordAccessExp _) _ _ = trueVal
                  | match (IdentExp (id, ref (ConstrKind, _))) v T =
                    Prefix ((idForIs (Ident.toString T) id), [v])
                  | match (ConstrExp (id, e, ref t)) v T =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        (Prefix ((idForIs (Ident.toString T) id), [v]))
                            </(bin "andthen")/>
                            (match e (Prefix ((idForGet (Ident.toString T) id), [v])) T')
                    end
                  | match _ _ _ = assertion UndefinedMatch
                fun assign (ParenExp e) v T B = assign e v T B
                  | assign AnyExp _ _ B = B
                  | assign (IntExp _) _ _ B = B
                  | assign (BoolExp _) _ _ B = B
                  | assign (IdentExp (id, ref (VarKind, t))) v T B =
                    if v = (Var (Ident.make id)) then
                        B
                    else
                        Let ([([Ident.make id],
                               (typeToActualSortIdent t),
                               (Expressions.conv v T (typeToActualSortIdent t)))],
                             B)
                  | assign (ArrayAccessExp (e, e', ref t)) v _ B =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        assign e (Prefix ((idForArraySet (Ident.toString T')), [(expr e T'), (expr e' natID), v])) T' B
                    end
                  | assign (RecordAccessExp (e, f, ref t)) v _ B =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        assign e (Prefix ((idForSet (Ident.toString T') f), [(expr e T'), v])) T' B
                    end
                  | assign (IdentExp (_, ref (ConstrKind, _))) _ _ B = B
                  | assign (ConstrExp (id, e, ref t)) v T B =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        assign e (Prefix ((idForGet (Ident.toString T) id), [v])) T' B
                    end
                  | assign _ _ _ _ = assertion UndefinedAssign
                fun substitute (ParenExp e) v T v' = substitute e v T v'
                  | substitute AnyExp _ _ v' = v'
                  | substitute (IntExp _) _ _ v' = v'
                  | substitute (BoolExp _) _ _ v' = v'
                  | substitute (IdentExp (id, ref (VarKind, t))) v T v' = replace v' id (Expressions.conv v T (typeToActualSortIdent t))
                  | substitute (ArrayAccessExp (e, e', ref t)) v _ v' =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        substitute e (Prefix ((idForArraySet (Ident.toString T')), [(expr e T'), (expr e' natID), v])) T' v'
                    end
                  | substitute (RecordAccessExp (e, f, ref t)) v _ v' =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        substitute e (Prefix ((idForSet (Ident.toString T') f), [(expr e T'), v])) T' v'
                    end
                  | substitute (IdentExp (_, ref (ConstrKind, _))) _ _ v' = v'
                  | substitute (ConstrExp (id, e, ref t)) v T v' =
                    let
                        val T' = typeToActualSortIdent t
                    in
                        substitute e (Prefix ((idForGet (Ident.toString T) id), [v])) T' v'
                    end
                  | substitute _ _ _ _ = assertion UndefinedSubstitute
                fun simple s = 
                    case s of
                        StNull => true
                      | StCond _ => false
                      | StSelect _ => false
                      | StSequence _ => false
                      | StTo _ => true
                      | StWhile _ => true
                      | StAssign _ => true
                      | StAny _ => true
                      | StSignal _ => true
                      | StInput _ => true
                      | StOutput _ => true
                      | StCase _ => false
                      | StForeach _ => true
                fun transCase [] _ _ _ _ _ _ _ _ _ =
                    Stop
                  | transCase ((q, S) :: tl) v T P G Z Y w B lock =
                    let
                        val cond = match q v T
                    in
                        Choice ((Guard ((SimpleCondition cond),
                                        (assign q v T (trans infos S P G Z Y w B lock additionalProcesses)))),
                                (Guard ((SimpleCondition (Prefix ((Ident.make "not"), [cond]))),
                                        (transCase tl v T P G Z Y w B lock))))
                    end
                fun simpleCond (ParenExp e) = simpleCond e
                  | simpleCond (RefExp _) = true
                  | simpleCond (IntExp _) = true
                  | simpleCond (BoolExp _) = true
                  | simpleCond (IdentExp _) = true
                  | simpleCond _ = false
        in
            case S of
                StNull => B
              | StCond (_, cond, stIf, l, stElse) =>
                let
                    val condID = Ident.new "COND_"
                    val cond' = if simpleCond cond then expr cond boolID else Var condID
                    val posCond = EqualCondition (cond', trueVal)
                    val negCond = EqualCondition (cond', falseVal)
                    val choice = Choice((Guard (posCond, (trans infos stIf P G Z Y w B lock additionalProcesses))),
                                        (Guard (negCond, (List.foldr (fn ((_, cond, st), acc) =>
                                                                         let
                                                                             val condID = Ident.new "COND_"
                                                                             val cond' = if simpleCond cond then expr cond boolID else Var condID
                                                                             val posCond = EqualCondition (cond', trueVal)
                                                                             val negCond = EqualCondition (cond', falseVal)
                                                                             val choice = Choice ((Guard (posCond, (trans infos st P G Z Y w B lock additionalProcesses))),
                                                                                                  (Guard (negCond, acc)))
                                                                         in
                                                                             if simpleCond cond then
                                                                                 choice
                                                                             else
                                                                                 Let ([([condID], boolID, (expr cond boolID))], choice)
                                                                         end)
                                                                     (trans infos (case stElse of SOME s => s
                                                                                          | NONE => StNull)
                                                                            P
                                                                            G
                                                                            Z
                                                                            Y
                                                                            w
                                                                            B
                                                                            lock
                                                                            additionalProcesses)
                                                                     l))))
                in
                    if simpleCond cond then
                        choice
                    else
                        Let ([([condID], boolID, (expr cond boolID))], choice)
                end
              | StSelect (hd :: tl) => List.foldl (fn (s, acc) =>
                                                      Choice ((trans infos s P G Z Y w B lock additionalProcesses), acc))
                                                  (trans infos hd P G Z Y w B lock additionalProcesses)
                                                  tl
              | StSelect [] => assertion EmptySelectList
              | StSequence (_, s1, s2) =>
                if simple s1 then
                    trans infos s1 P G Z Y w (trans infos s2 P G Z Y w B lock additionalProcesses) lock additionalProcesses
                else
                    let
                        val id = Ident.new "SEQ_"
                    in
                        additionalProcesses := Process { name = id,
                                                         gates = G,
                                                         parameters = (map (fn (x, y) => ([x], y)) (Z @ Y)),
                                                         func = NoExit,
                                                         body = (trans infos s2 P G Z Y w B lock additionalProcesses),
                                                         blocks = [] } :: (!additionalProcesses);
                        trans infos s1 P G Z Y w (CallProcess (id, G, (map Var (z @ y)))) lock additionalProcesses
                    end
              | StTo (_, id) =>
                if lock then
                    write w (CallProcess (P, G, ((map Var z) @ [Const id])))
                else
                    CallProcess (P, G, ((map Var z) @ [Const id]))
              | StWhile (_, e, s) =>
                let
                    val id = Ident.new "WHILE_"
                    val condID = Ident.new "COND_"
                    val cond = if simpleCond e then expr e boolID else Var condID
                    val posCond = EqualCondition (cond, trueVal)
                    val negCond = EqualCondition (cond, falseVal)
                    val call = CallProcess (id, G, (map Var (z @ y)))
                    val choice = Choice ((Guard (posCond, (trans infos s P G Z Y w call lock additionalProcesses))),
                                         (Guard (negCond, B)))
                in
                    additionalProcesses := Process { name = id,
                                                     gates = G,
                                                     parameters = (map (fn (x, y) => ([x], y)) (Z @ Y)),
                                                     func = NoExit,
                                                     body = if simpleCond e then
                                                                choice
                                                            else
                                                              Let ([([condID], boolID, (expr e boolID))], choice),
                                                     blocks = [] } :: (!additionalProcesses);
                    call
                end
              | StAssign (_, patt, exp) =>
                let
                    val zipped = ListPair.zipEq (patt, exp)
                    val bindings = map (fn (q, e) =>
                                           let
                                               val (x, T) =
                                                   case q of
                                                       IdentExp (id, ref (VarKind, _)) =>
                                                       ((Ident.make id), typeToActualSortIdent (typeOfExpression q))
                                                     | _ =>
                                                       ((Ident.new "ASSIGN_"), (typeToActualSortIdent (typeOfExpression e)))
                                           in
                                               (q, x, T, (expr e T))
                                           end)
                                       zipped
                    val matches = map (fn (q, x, t, _) => match q (Var x) t) bindings
                    val matches' = List.filter (fn v => v <> trueVal) matches
                    val assignes = List.foldr (fn ((q, x, t, _), acc) => assign q (Var x) t acc)
                                         B
                                         bindings
                in
                    Let ((map (fn (_, x, y, z) => ([x], y, z)) bindings),
                         (if matches' = [] then
                              assignes
                          else
                              let
                                  val cond = List.foldl (fn (v, acc) => v &&& acc)
                                                        (hd matches')
                                                        (tl matches')
                              in
                                  Choice ((Guard ((SimpleCondition cond), assignes)),
                                          (Guard ((SimpleCondition (Prefix ((Ident.make "not"), [cond]))),
                                                  Stop)))
                              end))
                end
              | StAny (_, patt, ref types, exp) =>
                let
                    val bindings = map (fn (q, t) =>
                                           let
                                               val x = case q of
                                                           IdentExp (id, ref (VarKind, _)) => Ident.make id
                                                         | _ => Ident.new "ASSIGN_"
                                               val T = typeToActualSortIdent t
                                               val T' = typeToActualSortIdent (typeOfExpression q)
                                           in
                                               (q, x, T, T')
                                           end)
                                       (ListPair.zipEq (patt, types))
                    val matches = map (fn (q, x, t, _) => match q (Var x) t) bindings
                    val matches' = List.filter (fn v => v <> trueVal) matches
                    val convs = List.mapPartial
                                    (fn (_, x, t, t') =>
                                        if Ident.toString t <> Ident.toString t' then
                                            SOME ([x], t', (Expressions.conv (Var x) t t'))
                                        else
                                            NONE)
                                    bindings
                    val B' = if convs = [] then B else Let (convs, B)
                    val assignes = List.foldr (fn ((q, x, t, _), acc) => assign q (Var x) t acc)
                                              (case exp of
                                                   SOME e => Guard ((EqualCondition ((expr e boolID),
                                                                                     trueVal)),
                                                                    B')
                                                 | NONE => B')
                                              bindings
                in
                    ValueChoice ((map (fn (_, x, t, _) => ([x], t)) bindings),
                                 (if matches' = [] then
                                      assignes
                                  else
                                      let
                                          val cond = List.foldl (fn (v, acc) => v &&& acc)
                                                                (hd matches')
                                                                (tl matches')
                                      in
                                          Guard ((SimpleCondition cond), assignes)
                                      end))
                end
              | StSignal (_, id, _) => Rendezvous ((Ident.make id), [], NONE, B)
              | StInput (_, id, (ref channel), patt, exp) =>
                let
                    val profile = case channel of
                                      ProfileChannel p => p
                                    | NamedChannel n => Option.valOf (StringMap.find ((#channels infos), n))
                    val bindings = map (fn (q, t) =>
                                           let
                                               val x = case q of
                                                           IdentExp (id, ref (VarKind, _)) => Ident.make id
                                                         | _ => Ident.new "ASSIGN_"
                                               val T = typeToActualSortIdent t
                                               val T' = typeToActualSortIdent (typeOfExpression q) handle _ => T
                                           in
                                               (q, x, T, T')
                                           end)
                                       (ListPair.zipEq (patt, profile))
                    val matches = map (fn (q, x, t, _) => match q (Var x) t) bindings
                    val matches' = List.filter (fn v => v <> trueVal) matches
                    val convs = List.mapPartial
                                    (fn (_, x, t, t') =>
                                        if Ident.toString t <> Ident.toString t' then
                                            SOME ([x], t', (Expressions.conv (Var x) t t'))
                                        else
                                            NONE)
                                    bindings
                    val B' = if convs = [] then B else Let (convs, B)
                    val assignes = List.foldr (fn ((q, x, t, _), acc) => assign q (Var x) t acc)
                                              B'
                                              bindings
                    val substitutes = List.foldr (fn ((q, x, _, t), acc) => substitute q (Var x) t acc)
                                                 (case exp of SOME e => expr e boolID | NONE => trueVal)
                                                 bindings
                    val offers = map (fn (_, x, T, _) => Receive ([x], T)) bindings
                    val cond = if matches' = [] then
                                   substitutes
                               else
                                   Prefix ((Ident.make "andthen"),
                                           [(List.foldl (fn (v, acc) => v &&& acc)
                                                        (hd matches')
                                                        (tl matches')),
                                            substitutes])
		    val	replaces = List.mapPartial
                                       (fn (_, x, t, t') =>
                                           if Ident.toString t <> Ident.toString t' then
                                               SOME ((Ident.toString x), (Expressions.conv (Var x) t t'))
                                           else
                                               NONE)
                                       bindings
		    val	cond' = List.foldl (fn ((x, v), acc) => replace acc x v) cond replaces
                in
                    Rendezvous ((Ident.make id),
                                offers,
                                (if cond' = trueVal then NONE else SOME (SimpleCondition cond')),
                                assignes)
                end
              | StOutput (_, id, (ref channel), l) =>
                let
                    val profile = case channel of
                                      ProfileChannel p => p
                                    | NamedChannel n => Option.valOf (StringMap.find ((#channels infos), n))
                    val offers = map (fn (e, t) =>
                                         Send (expr e (typeToActualSortIdent t)))
                                     (ListPair.zipEq (l, profile))
                in
                    Rendezvous ((Ident.make id), offers, NONE, B)
                end
              | StCase (_, e, l) =>
                let
                    val id = Ident.new "CASE_"
                    val typeE = typeToActualSortIdent (typeOfExpression e)
                    val l' = List.mapPartial (fn (_, ref a, e, s) => if a then SOME (e, s) else NONE) l
                in
                    if simpleCond e then
                        transCase l' (expr e typeE) typeE P G Z Y w B lock
                    else
                        Let ([([id], typeE, (expr e typeE))],
                             (transCase l' (Var id) typeE P G Z Y w B lock))
                end
              | StForeach (_, i, ref t, s) =>
                let
                    val idProcess = Ident.new "FOREACH_"
                    val idIndex = Ident.new "FOREACH_INDEX_"
                    val idVar = Ident.make i
(*
                    val min = IntExp (1, ref IntType)
                    val max = IntExp (3, ref IntType)
*)
                    val (min, max) =
                        (fn IntervalType (m, n) => (extractConstantFromExpression m,
                                                    extractConstantFromExpression n)
                          | _ => assertion InvalidForeachType)
                            (case t of
                                 IntervalType _ => t
                               | NamedType t' => Option.valOf (StringMap.find ((#types infos), t'))
                               | _ => assertion InvalidForeachType)
                    val minExpr = expr (IntExp (min, ref IntType)) intID
                    val maxExpr = expr (IntExp (max, ref IntType)) intID
                    val call = CallProcess (idProcess, G, ((map Var (z @ y)) @ [minExpr]))
                    val cond = (Var idIndex) </eq/> maxExpr
                    val posCond = EqualCondition (cond, trueVal)
                    val negCond = EqualCondition (cond, falseVal)
                    val B' = Choice (Guard (posCond, B),
                                     Guard (negCond, CallProcess (idProcess, G, ((map Var (z @ y)) @ [Prefix ((Ident.make "SUCC"), [Var idIndex])]))))
                in
                    additionalProcesses := Process { name = idProcess,
                                                     gates = G,
                                                     parameters = (map (fn (x, y) => ([x], y)) (Z @ Y)) @ [([idIndex], intID)],
                                                     func = NoExit,
                                                     body = Let ([([idVar], intID, (Var idIndex))],
                                                                 (trans infos s P G Z Y w B' lock additionalProcesses)),
                                                     blocks = [] } :: (!additionalProcesses);
                    call
                end
        end

    fun meta shared (infos : Helper.typeInfos) (S : Statement) (P : Ident.t) (G : Ident.t list) Z Y (B : behaviour) additionalProcesses =
        if shared then
            let
                val y = map #1 Y
                val w = y
                val t = trans infos S P G Z Y w Stop true additionalProcesses
                fun read [] = Choice (t, (Rendezvous (unlockID, [], NONE, B)))
                  | read ((id, t) :: tl) =
                    Rendezvous ((Ident.make ("READ_" ^ (Ident.toString id))),
                                [Receive ([id], t)],
                                NONE,
                                (read tl))
            in
                Rendezvous (lockID, [], NONE, (read Y))
            end
        else
            trans infos S P G Z [] [] Stop false additionalProcesses

    fun translateProcess shared (infos : Helper.typeInfos) (_, name, ports, params, states, vars, init, transitions) =
        let
            val additionalProcesses = ref []
            val ports' : (string * (PortAttr list) * Channel) list= flattenPorts ports
            val params' : (Arg * (VarAttr list) * Type) list = flattenParams params
            val states' : string list = #2 states
            val (init', initVars) =
                normalize infos
                          (case init of
                               SOME (_, s) => s
                             | NONE => StTo (~1, (hd states')) handle _ => assertion NoState)
            val (transitions', transitionsVars) = List.foldr (fn ((ln, id, s), (accT, accV)) =>
                                                                 let
                                                                     val (s', vars) = normalize infos s
                                                                 in
                                                                     (((ln, id, s') :: accT),
                                                                      (accV @ vars))
                                                                 end)
                                                             ([], [])
                                                             transitions
            val vars' : (string * Type * Exp) list = (flattenVars infos vars) @ initVars @ transitionsVars
            val G = gateList shared ports' params'
            val (valueParams, referenceParams) = List.partition (fn ((ValArg _), _, _) => true | _ => false) params'
            val X = map (fn ((ValArg id), _, t) => ((Ident.make id), (typeToActualSortIdent t))
                          | _ => raise Fail "Processes.translateProcess/X")
                        valueParams
            val x = map #1 X
            val Y : (Ident.t * Ident.t) list = map (fn ((RefArg id), _, t) => ((Ident.make id), (typeToActualSortIdent t))
                                                     | _ => raise Fail "Processes.translateProcess/Y")
                                                   referenceParams
            val Z = map (fn (id, t, _) => ((Ident.make id), (typeToActualSortIdent t))) vars'
            val z = map #1 Z
            val auxID = Ident.make ("aux_" ^ name)
            val stateID = Ident.make ("state_" ^ name)
            val sID = Ident.make "s"
            open EquationUtils
            val aux = Process { name = auxID,
                                gates = G,
                                parameters = (map (fn (x, y) => ([x], y)) (X @ Z)) @ [([sID], stateID)],
                                func = NoExit,
                                body =
                                let
                                    val choices = map (fn st =>
                                                          let
                                                              val stVal = Var (Ident.make st)
                                                              val sVal = Var sID
                                                              val comp = sVal </eq/> stVal
                                                              val stat = #3 (Option.valOf (List.find (fn (_, id, _) => id = st) transitions')) handle _ => StNull
                                                              val body = meta shared
                                                                              infos
                                                                              stat
                                                                              auxID
                                                                              G
                                                                              (X @ Z)
                                                                              Y
                                                                              (CallProcess (auxID,
                                                                                            G,
                                                                                            ((map Var (x @ z)) @ [stVal])))
                                                                              additionalProcesses
                                                          in
                                                              Guard ((EqualCondition (comp, trueVal)),
                                                                     body)
                                                          end)
                                                      states'
                                in
                                    List.foldl (fn (x, acc) => Choice (x, acc)) (hd choices) (tl choices)
                                end,
                                blocks = [] }
            val state = TypeDeclaration { name = stateID,
                                          parents = [booleanID],
                                          formalsorts = [],
                                          formaloperations = [],
                                          formalequations = ([], []),
                                          sorts = [(stateID, NONE)],
                                          operations = [{ names = (map (fn id => ((Ident.make id),
                                                                                  (SOME AnonConstructor)))
                                                                       states'),
                                                          parameters = [],
                                                          result = stateID },
                                                        { names = [((Ident.make "_eq_"), NONE)],
                                                          parameters = [stateID, stateID],
                                                          result = boolID }],
                                          equations = ([([xID, yID], stateID)],
                                                       [boolID /// [([] ==> (xVal </eq/> xVal) === trueVal),
                                                                    ([] ==> (xVal </eq/> yVal) === falseVal)]]) }
        in
            Process { name = (Ident.make name),
                      gates = G,
                      parameters = (map (fn (x, y) => ([x], y)) X),
                      func = NoExit,
                      body = meta shared
                                  infos
                                  (if vars' = [] then
                                       init'
                                   else
                                       (StSequence (~1,
                                                    (StAssign (~1,
                                                               (map (fn (id, t, _) => IdentExp (id, ref (VarKind, t))) vars'),
                                                               (map #3 vars'))),
                                                    init')))
                                  auxID
                                  G 
                                  (X @ Z)
                                  Y
                                  (CallProcess ((Ident.make name), G, (map Var x)))
                                  additionalProcesses,
                      blocks = [(ProcessBlock aux), (TypeBlock state)] @ (map ProcessBlock (!additionalProcesses))}
        end

    fun passedByReference c =
        let
            fun pbr (RefExp (id, _)) = SOME id
              | pbr _ = NONE
        in
            case c of
                ParComp (_, _, l) => List.foldl StringSet.union
                                                StringSet.empty
                                                (map (fn (_, c) => passedByReference c) l)
              | InstanceComp (_, _, l) => StringSet.addList (StringSet.empty,
                                                             (List.mapPartial pbr l))
        end

    fun compo shared infos (ParComp (ln, ports, l)) =
        let
            fun ps (SomePorts l) = l
              | ps AllPorts = assertion UnexpectedAllPorts
            val globalSyncSet = StringSet.addList (StringSet.empty, (ps ports))
            val elements = map (fn (ports, comp) => ((StringSet.addList (StringSet.empty, (ps ports))), comp)) l
            val union = List.foldl (fn ((s, _), acc) => StringSet.union (acc, s))
                                   StringSet.empty
                                   elements
            val R1Set = StringSet.intersection (globalSyncSet, union)
            val (sets, Ci) = ListPair.unzip elements
            val Li = map (fn s => StringSet.union (globalSyncSet, s)) sets
            val Lim = #1 (List.foldr (fn (li, (lst, set)) =>
                                              let
                                                  val set' = StringSet.union (set, li)
                                              in
                                                  ((set' :: lst), set')
                                              end)
                                             ([], StringSet.empty)
                                             Li)
            val LiCouples = ListPair.zip ((StringSet.empty :: Li), Lim)
            val Li' = map (fn (li_1, lim) => StringSet.intersection (li_1, lim)) LiCouples
            fun compOp s x y =
                if StringSet.isEmpty s then
                    ParallelAsync (x, y)
                else
                    Parallel (x, (map Ident.make (StringSet.listItems s)), y)
        in
            if not (StringSet.isEmpty R1Set) then assertion R1CompositionRule else ();
            app (fn (s, c) =>
                    let
                        fun visiblePorts (InstanceComp (_, ports, _)) =
                            StringSet.addList (StringSet.empty, ports)
                          | visiblePorts (ParComp (_, set, l)) =
                            List.foldl (fn ((set, comp), acc) =>
                                           StringSet.union ((StringSet.addList (acc, (ps set))),
                                                            (visiblePorts comp)))
                                       (StringSet.addList (StringSet.empty, (ps set)))
                                       l
                        val inter = StringSet.intersection ((visiblePorts c), union)
                        val R2Set = StringSet.difference (inter, s)
                    in
                        if not (StringSet.isEmpty R2Set) then
                            (ErrSrcPrint.print ("Error: composition at line " ^ (Int.toString ln)
                                                ^ ": theoretical limitation to translate 'par' into LOTOS\n");
                             raise BYE)
                        else
                            ()
                    end)
                elements;
            let
                val Cm = List.last Ci
                val Li'Ci = ListPair.zip ((tl Li'), Ci)
            in
                List.foldr (fn ((li', ci), acc) =>
                               compOp li' (compo shared infos ci) acc)
                           (compo shared infos Cm)
                           Li'Ci
            end
        end
      | compo shared infos (InstanceComp ((_, id), ports, exprs)) =
        let
            val gates = (map Ident.make ports)
                        @ (List.concat (List.mapPartial (fn (RefExp (id, _)) =>
                                                            SOME [(Ident.make ("READ_" ^ id)),
                                                                  (Ident.make ("WRITE_" ^ id))]
                                                          | _ => NONE)
                                            exprs))
                        @ (if shared then [lockID, unlockID] else [])
            val sign = Option.valOf (StringMap.find ((#signatures infos), id))
            val byValue = List.filter (fn ((ValArg _), _, _) => true
                                        | _ => false)
                                      sign
            val values = map (fn ((_, _, t), e) => Expressions.expr (#types infos) e (typeToActualSortIdent t))
                             (ListPair.zipEq (byValue, (List.filter (fn (RefExp _) => false | _ => true) exprs)))
        in
            CallProcess ((Ident.make id), gates, values)
        end

    fun translateComponent shared (infos : Helper.typeInfos) typeProcesses (_, name, ports, params, vars, localPorts, prio, init, comp) =
        let
            val additionalProcesses = ref []
            val ports' : (string * (PortAttr list) * Channel) list = flattenPorts ports
            val params' : (Arg * (VarAttr list) * Type) list = flattenParams params
            val vars' : (string * Type * Exp) list = flattenVars infos vars
            val localPorts' = flattenPorts (List.concat (map #1 localPorts))
            val bounds : (Bound * Bound) list = map #2 localPorts
            val init' = case init of SOME (_, s) => s | NONE => StNull

            val G1 = map (Ident.make o #1) ports'
            val G2 = List.concat (map (fn ((RefArg id), _, _) => [(Ident.make ("READ_" ^ id)),
                                                                  (Ident.make ("WRITE_" ^ id))]
                                        | _ => [])
                                      params')
            val G3 = List.concat (map (fn (id, _, _) => [(Ident.make ("READ_" ^ id)), (Ident.make ("WRITE_" ^ id))])
                                      vars')
            val G = if shared then G1 @ G2 @ [lockID, unlockID] else G1
            val G' = if shared then G1 @ G2 @ G3 @ [lockID, unlockID] else G1
            val (valueParams, referenceParams) = List.partition (fn ((ValArg _), _, _) => true | _ => false) params'
            val X = map (fn ((ValArg id), _, t) => ((Ident.make id), (typeToActualSortIdent t))
                          | _ => raise Fail "Processes.translateProcess/X")
                        valueParams
            val x = map #1 X
            val Z = map (fn (id, t, _) => ((Ident.make id), (typeToActualSortIdent t))) vars'
            val z = map #1 Z
            val referenced = passedByReference comp
            val () = app (fn (id, t) => if StringSet.member (referenced, (Ident.toString id)) then
                                            typeProcesses := StringSet.add (!typeProcesses,
                                                                            (Ident.toString t))
                                        else
                                            ())
                         Z
            val referencedZ = List.filter (fn (id, _) => StringSet.member (referenced, (Ident.toString id))) Z
            val w : (Ident.t * Ident.t) list = map (fn ((RefArg id), _, t) => ((Ident.make id), (typeToActualSortIdent t))
                                                     | _ => raise Fail "Processes.translateProcess/Y")
                                                   referenceParams

            val aux1ID = Ident.make ("AUX1_" ^ name)
            val aux2ID = Ident.make ("AUX2_" ^ name)
            val S' = (if vars' = [] then
                          init'
                      else
                          (StSequence (~1,
                                       (StAssign (~1,
                                                  (map (fn (id, t, _) => IdentExp (id, ref (VarKind, t))) vars'),
                                                  (map #3 vars'))),
                                       init')))
            val B =
                if w = [] then
                    trans infos
                          S'
                          aux1ID
                          G'
                          X
                          []
                          []
                          (CallProcess (aux2ID, G', (map Var (x @ z))))
                          false
                          additionalProcesses
                else
                    let
                        fun write [] = Rendezvous (unlockID, [], NONE, (CallProcess (aux2ID, G', (map Var (x @ z)))))
                          | write ((id, _) :: tl) =
                            Rendezvous ((Ident.make ("WRITE_" ^ (Ident.toString id))),
                                        [Send (Var id)],
                                        NONE,
                                        (write tl))
                        val B' = write w
                        val t = trans infos S' aux1ID G' X [] [] B' false additionalProcesses
                        fun read [] = Choice (t,
                                              (Rendezvous (unlockID,
                                                           [],
                                                           NONE,
                                                           (CallProcess (aux1ID, G', (map Var x))))))
                          | read ((id, t) :: tl) =
                            Rendezvous ((Ident.make ("READ_" ^ (Ident.toString id))),
                                        [Receive ([id], t)],
                                        NONE,
                                        (read tl))
                    in
                        Rendezvous (lockID, [], NONE, (read w))
                    end
            val aux1Process = Process { name = aux1ID,
                                        gates = G',
                                        parameters = (map (fn (x, y) => ([x], y)) X),
                                        func = NoExit,
                                        body = B,
                                        blocks = [] }
            fun Zi (id, t) = CallProcess ((Ident.make ("TYPE_" ^ (Ident.toString t))),
                                          [(Ident.make ("READ_" ^ (Ident.toString id))),
                                           (Ident.make ("WRITE_" ^ (Ident.toString id)))],
                                          [Var id])
            val aux2Body = if referencedZ = [] then
                               compo shared infos comp
                           else
                               Parallel ((compo shared infos comp),
                                         G3,
                                         ((List.foldl (fn (x, acc) => ParallelAsync (acc, (Zi x)))
                                                      (Zi (hd referencedZ))
                                                      (tl referencedZ))))
            val aux2Process = Process { name = aux2ID,
                                        gates = G',
                                        parameters = (map (fn (x, y) => ([x], y)) (X @ Z)),
                                        func = NoExit,
                                        body = (if localPorts' = [] then
                                                    aux2Body
                                                else
                                                    Hide ((map (Ident.make o #1) localPorts'), aux2Body)),
                                        blocks = [] }
        in
            if prio <> [] then warning ("component " ^ name) "priorities are ignored" else ();
            if List.exists (fn (Infinite, Infinite) => false
                             | ((Closed f), Infinite) =>
                               if Real.compare (f, 0.0) = EQUAL then false else true
                             | _ => true) bounds then
                warning ("component " ^ name) "time constraints are ignored"
            else
                ();
            Process { name = (Ident.make name),
                      gates = G,
                      parameters = (map (fn (x, y) => ([x], y)) X),
                      func = NoExit,
                      body = (if G3 = [] then
                                  CallProcess (aux1ID, G', (map Var x))
                              else
                                  Hide (G3, (CallProcess (aux1ID, G', (map Var x))))),
                      blocks = [(ProcessBlock aux1Process), (ProcessBlock aux2Process)] @ (map ProcessBlock (!additionalProcesses)) }
        end

    fun translateProcessOrComponent shared (infos : Helper.typeInfos) _ (ProcessDecl p) =
        SOME (translateProcess shared infos p)
      | translateProcessOrComponent shared (infos : Helper.typeInfos) typeProcesses (ComponentDecl c) =
        SOME (translateComponent shared infos typeProcesses c)
      | translateProcessOrComponent _ _ _ _ = NONE

    fun getSignature (ProcessDecl p) = SOME ((#2 p), (flattenParams (#4 p)))
      | getSignature (ComponentDecl c) = SOME ((#2 c), (flattenParams (#4 c)))
      | getSignature _ = NONE

    fun generateTypeProcess t =
        let
            val processID = Ident.make ("TYPE_" ^ t)
            val tID = Ident.make t
            val readID = Ident.make "READ"
            val writeID = Ident.make "WRITE"
            val xID = Ident.make "X"
            val call = CallProcess (processID, [readID, writeID], [Var xID])
        in
            Process { name = processID,
                      gates = [readID, writeID],
                      parameters = [([xID], tID)],
                      func = NoExit,
                      body = Choice ((Rendezvous (readID, [Send (Var xID)], NONE, call)),
                                     (Rendezvous (writeID, [Receive ([xID], tID)], NONE, call))),
                      blocks = [] }
        end

end
