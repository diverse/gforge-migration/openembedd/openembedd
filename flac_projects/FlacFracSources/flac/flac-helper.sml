(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

infixr 9 />
infixr 9 </
infix 6 ///
infix 6 ==>
infix 7 ===
infix 8 =?=
infix 7 &&&
infix 7 ><

structure Helper = struct

    open Lotos


    (* Miscellaneous *)

    fun verbose s = if !VERBOSE then print s else ()

    fun warning pl msg = ErrSrcPrint.print ("Warning: " ^ pl ^ ": " ^ msg ^ "\n")

    structure StringMap = SplayMapFn (struct
                                          type ord_key = string
                                          val compare = String.compare
                                      end)

    structure StringSet = SplaySetFn (struct
                                          type ord_key = string
                                          val compare = String.compare
                                      end)

    datatype assertionKind = InvalidMain
                           | ConstantWaited
                           | NoSort
                           | TypeSynonym
                           | InvalidTypeDecl
                           | InvalidExprType
                           | NoDefault
                           | NoState
                           | InvalidQueueExprType
                           | InvalidArrayExprType
                           | InvalidRecordExprType
                           | InvalidUnionExprType
                           | EmptyUnion
                           | UnexpectedAnyExpr
                           | UnknownType
                           | UnknownReference
                           | UnknownConstructor
                           | InvalidIdentExpr
                           | EmptySelectList
                           | UndefinedMatch
                           | UndefinedAssign
                           | UndefinedSubstitute
                           | UnexpectedAllPorts
                           | R1CompositionRule
                           | ConditionalExpression
                           | InvalidForeachType

    fun assertionToString a =
        case a of
            InvalidMain => "Invalid main (either missing or referencing an unknown identifier)"
          | ConstantWaited => "Constant waited"
          | NoSort => "No sort"
          | TypeSynonym => "Type synonym"
          | InvalidTypeDecl => "Invalid type declaration"
          | InvalidExprType => "Invalid expression type"
          | NoDefault => "No default value"
          | NoState => "No state declared for process"
          | InvalidQueueExprType => "Invalid type for queue expression"
          | InvalidArrayExprType => "Invalid type for array expression"
          | InvalidRecordExprType => "Invalid type for record expression"
          | InvalidUnionExprType => "Invalid type for union expression"
          | EmptyUnion => "Empty union type (no constructor)"
          | UnexpectedAnyExpr => "Unexpected 'any' expression"
          | UnknownType => "Unknown type"
          | UnknownReference => "Unknown reference"
          | UnknownConstructor => "Unknown constructor"
          | InvalidIdentExpr => "Invalid Ident expression"
          | EmptySelectList => "Empty 'select' list"
          | UndefinedMatch => "function 'match' is undefined"
          | UndefinedAssign => "function 'assign' is undefined"
          | UndefinedSubstitute => "function 'substitute' is undefined"
          | UnexpectedAllPorts => "Unexpected '*' (all ports) in composition"
          | R1CompositionRule => "Rule R1 not enforced in composition"
          | ConditionalExpression => "Conditional expressions should be rewritten by preprocess"
          | InvalidForeachType => "Invalid type for foreach variable"

    exception Assertion of assertionKind

    fun assertion a = raise (Assertion a)

    fun op>< (x, y) = List.tabulate (x, y)

    fun extractConstantFromExpression (IntExp (n, _)) = n
      | extractConstantFromExpression _ = assertion ConstantWaited


    (* "string list"-stored files *)

    type file = string list list ref

    fun emptyFile () = ref []

    fun isEmptyFile f = (!f) = []

    fun appendFile f l = f := l :: !f

    fun fileToStringList f h = h @ (List.concat (rev (!f)))


    (* Identifiers *)

    val intID = Ident.make "Int"

    val natID = Ident.make "Nat"

    val boolID = Ident.make "Bool"

    val booleanID = Ident.make "Boolean"

    val xbooleanID = Ident.make "X_Boolean"

    val naturalID = Ident.make "Natural"

    val integerID = Ident.make "Integer"

    val integerNumberID = Ident.make "ExtInteger" (* instead of "IntegerNumber" *)

    val pID = Ident.make "p"

    val wID = Ident.make "w"

    val xID = Ident.make "x"

    val yID = Ident.make "y"

    val zID = Ident.make "z"

    val controllerID = Ident.make "Controller"

    val lockID = Ident.make "LOCK"

    val unlockID = Ident.make "UNLOCK"

    val fiacreID = Ident.make "Fiacre"


    (* Identifier functions *)

    fun idForIn base = Ident.make ("IN_" ^ base)

    fun idForToInt base = Ident.make (base ^ "_TO_INT")

    fun idForIntTo base = Ident.make ("INT_TO_" ^ base)

    fun idForError base = Ident.make ("ERROR_" ^ base)

    fun idForError1 base = Ident.make ("ERROR_" ^ base ^ "_1")

    fun idForError2 base = Ident.make ("ERROR_" ^ base ^ "_2")

    fun idForError3 base = Ident.make ("ERROR_" ^ base ^ "_3")

    fun idForIs base elem = Ident.make ("IS_" ^ base ^ "_" ^ elem)

    fun idForGet base elem = Ident.make ("GET_" ^ base ^ "_" ^ elem)

    fun idForSet base elem = Ident.make ("SET_" ^ base ^ "_" ^ elem)

    fun idForArrayGet base = Ident.make ("GET_" ^ base)

    fun idForArraySet base = Ident.make ("SET_" ^ base)

    fun idForFull base = Ident.make ("FULL_" ^ base)

    fun idForEmpty base = Ident.make ("EMPTY_" ^ base)

    fun idForEnqueue base = Ident.make ("ENQUEUE_" ^ base)

    fun idForAppend base = Ident.make ("APPEND_" ^ base)

    fun idForDequeue base = Ident.make ("DEQUEUE_" ^ base)

    fun idForFirst base = Ident.make ("FIRST_" ^ base)

    fun idForIndexed base i = Ident.make (base ^ (Int.toString i))


    (* Equation utilities *)

    structure EquationUtils = struct
        val pVal = Var pID
        val wVal = Var wID
        val xVal = Var xID
        val yVal = Var yID
        val zVal = Var zID
        val trueVal = Const "true"
        val falseVal = Const "false"
        fun op/> (f, y) = fn x => f (x, y)
        fun op</ (x, f) = f x
        fun bin id (x, y) = Infix (x, (Ident.make id), y)
        val eq = bin "eq"
        val neq = bin "neq"
        val plus = bin "+"
        val minus = bin "-"
        val mult = bin "*"
        val div' = bin "div"
        val mod' = bin "mod"
        val lowerThan = bin "<"
        val lowerEqual = bin "<="
        val higherThan = bin ">"
        val higherEqual = bin ">="
        fun not' v = Prefix ((Ident.make "not"), [v])
        fun neg v = Prefix ((Ident.make "-"), [v])
        fun op/// (id, meq) = (id, [], meq)
        fun op==> (pr, seq) = (pr, seq)
        fun op=== (v1, v2) = (v1, v2)
        fun op=?= (v1, v2) = Simple (v1, v2)
        fun op&&& (v1, v2) = Infix (v1, (Ident.make "and"), v2)
        fun encodeInt 0 = Const "0"
          | encodeInt 1 = Const "1"
          | encodeInt 2 = Const "2"
          | encodeInt 3 = Const "3"
          | encodeInt 4 = Const "4"
          | encodeInt 5 = Const "5"
          | encodeInt 6 = Const "6"
          | encodeInt 7 = Const "7"
          | encodeInt 8 = Const "8"
          | encodeInt 9 = Const "9"
          | encodeInt n =
            if n < 0 then
                neg (encodeInt (~n))
            else
                Prefix ((Ident.make "succ"), [(encodeInt (n - 1))])
        fun encodeNat n = Of ((encodeInt n), natID)
    end


    (* Controller process *)

    val controllerProcess =
        Process { name = controllerID,
                  gates = [lockID, unlockID],
                  parameters = [],
                  func = NoExit,
                  body = (Rendezvous (lockID,
                                      [],
                                      NONE,
                                      Rendezvous (unlockID,
                                                  [],
                                                  NONE,
                                                  (CallProcess (controllerID,
                                                                [lockID, unlockID],
                                                                []))))),
                  blocks = [] }


    (* ExtendedInteger type *)

    val extendedInteger =
        let
            val errorID = Ident.make "ERROR_ZERO_DIV"
            val errorVal = Const (Ident.toString errorID)
            val zeroVal = Of ((Const "0"), intID)
            fun sign v = Prefix ((Ident.make "sign"), [v])
            fun abs v = Prefix ((Ident.make "abs"), [v])
            fun natToInt v = Prefix ((Ident.make "NatToInt"), [v])
            fun intToNat v = Prefix ((Ident.make "IntToNat"), [v])
            open EquationUtils
            val divVal = ((sign xVal) </mult/> (sign yVal)) </mult/> (natToInt ((intToNat (abs (xVal))) </div'/> (intToNat (abs yVal))))
            val modVal = (sign xVal) </mult/> (natToInt ((intToNat (abs (xVal))) </mod'/> (intToNat (abs yVal))))
        in
            TypeDeclaration { name = integerNumberID,
                              parents = [(Ident.make "IntegerNumber"), naturalID],
                              formalsorts = [],
                              formaloperations = [],
                              formalequations = ([], []),
                              sorts = [],
                              operations = [{ names = (map (fn s => ((Ident.make s), NONE))
                                                           ["_div_", "_mod_"]),
                                              parameters = [intID, intID],
                                              result = intID },
                                            { names = [(errorID,
                                                        (SOME (ImplementedByExternal (Ident.toString errorID))))],
                                              parameters = [],
                                              result = intID }],
                              equations = ([([xID, yID], intID)],
                                           [intID /// [([yVal =?= zeroVal] ==> xVal </div'/> yVal
                                                                           === errorVal),
                                                       ([] ==> xVal </div'/> yVal === divVal),
                                                       ([yVal =?= zeroVal] ==> xVal </mod'/> yVal
                                                                           === errorVal),
                                                       ([] ==> xVal </mod'/> yVal === modVal)]]) }
        end


    (* File headers *)

    val fFileHeader = [
        "#define CAESAR_ADT_EXPERT_F 5.3",
        "",
        "ADT_INT ERROR_ZERO_DIV ()",
        "{",
        "    printf(\"ERROR: Division by zero\\n\");",
        "    exit (1);",
        "}",
        ""
    ]

    val tFileHeader = [
        "#define CAESAR_ADT_EXPERT_T 5.3",
        "",
        "#include \"X_BOOLEAN.h\"",
        "",
        "/* forward */ unsigned char ADT_LE_INT ();",
        ""
    ]


    (* Equality definitions *)

    fun equalityOperations nameID =
        { names = (map (fn s => ((Ident.make s), NONE)) ["_eq_", "_neq_"]),
          parameters = [nameID, nameID],
          result = boolID }

    val equalityEquations : complexEq =
        let
            open EquationUtils
        in
            boolID /// [([] ==> (xVal </eq/> xVal) === trueVal),
                        ([] ==> (xVal </eq/> yVal) === falseVal),
                        ([] ==> (xVal </neq/> yVal) === (not' (xVal </eq/> yVal)))]
        end


    (* C utilities *)

    fun cType "NAT" = "ADT_NAT"
      | cType "INT" = "ADT_INT"
      | cType "BOOL" = "ADT_BOOL"
      | cType s = s

    fun cErrorFunction return name lines =
        [(cType return) ^ " " ^ name ^ " ()",
         "{"]
        @ (map (fn l => "    printf (\"" ^ l ^ "\\n\");") lines)
        @ ["    exit (1);",
           "}",
           ""]


    (* Type utilities *)

    fun typeToTypeIdent BoolType = SOME booleanID
      | typeToTypeIdent NatType = SOME naturalID
      | typeToTypeIdent IntType = SOME integerNumberID
      | typeToTypeIdent (NamedType s) = SOME (Ident.make s)
      | typeToTypeIdent (IntervalType (_, _)) = SOME integerNumberID
      | typeToTypeIdent (UnionType _) = NONE
      | typeToTypeIdent (RecordType _) = NONE
      | typeToTypeIdent (ArrayType (_, _)) = NONE
      | typeToTypeIdent (QueueType (_, _)) = NONE
      | typeToTypeIdent AnyArray = NONE
      | typeToTypeIdent AnyQueue = NONE
      | typeToTypeIdent BotType = NONE
      | typeToTypeIdent TopType = NONE

    fun typeToSortIdent BoolType = SOME boolID
      | typeToSortIdent NatType = SOME natID
      | typeToSortIdent IntType = SOME intID
      | typeToSortIdent (NamedType s) = SOME (Ident.make s)
      | typeToSortIdent (IntervalType (_, _)) = SOME intID
      | typeToSortIdent (UnionType _) = NONE
      | typeToSortIdent (RecordType _) = NONE
      | typeToSortIdent (ArrayType (_, _)) = NONE
      | typeToSortIdent (QueueType (_, _)) = NONE
      | typeToSortIdent AnyArray = NONE
      | typeToSortIdent AnyQueue = NONE
      | typeToSortIdent BotType = NONE
      | typeToSortIdent TopType = NONE

    fun typeToActualSortIdent x =
        case typeToSortIdent x of
            SOME s => s
          | NONE => assertion NoSort


    (* Type informations *)
    type typeInfos = { types : Type StringMap.map,
                       channels : (Type list) StringMap.map,
                       signatures : ((Arg * (VarAttr list) * Type) list) StringMap.map }

end
