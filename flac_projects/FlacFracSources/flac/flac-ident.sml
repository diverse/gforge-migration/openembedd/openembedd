(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module contains some identifier-related functions.
 *)

signature IDENT = sig
    (** Group: Exported declarations *)

    (**
     * Type: Ident.t
     *
     * The type of identifiers (with equality).
     *)
    eqtype t

    (**
     * Type: Ident.Invalid
     *
     * Raised when an attempt is made to build an invalid identifier.
     *
     * Parameters:
     *   - invalid identifier that raised the problem
     *)
    exception Invalid of string

    (**
     * Type: Ident.Reserved
     *
     * Raised when an attempt is made to build an identifier that is a
     * equal to a reserved LOTOS keyword.
     *
     * Parameters:
     *   - identifier that raised the problem
     *)
    exception Reserved of string

    (**
     * Function: Ident.make
     *
     * Constructs an identifier from a string.
     *
     * Parameters:
     *   s - string to be used as identifier
     *
     * Returns:
     *   an identifier corresponding to the passed string
     *
     * *Raises*:
     *   - <Ident.Invalid> if the passed string is not a valid identifier
     *   - <Ident.Reserved> if the passed string is a reserved LOTOS identifier
     *)
    val make : string -> t

    (**
     * Function: Ident.toString
     *
     * Converts the passed identifier into its corresponding string.
     *
     * Parameters:
     *   id - identifier to convert
     *
     * Returns:
     *   the string corresponding to the passed identifier
     *)
    val toString : t -> string

    (**
     * Function: Ident.new
     *
     * Constructs an identifier from a base string.
     * The created identifiers will have the form base0, base1, base2, etc.
     * It is guaranteed that between two calls to <Ident.reset>, calls to
     * this function will always return different identifiers whatever the
     * passed base.
     *
     * Parameters:
     *   s - string to be used as base for identifier
     *
     * Returns:
     *   an identifier of the form basen where base is the passed string,
     *   and n an integer that ensures that the returned identifier is unique
     *
     * *Raises*:
     *   - <Ident.Invalid> if the passed string is not a valid identifier
     *)
    val new : string -> t

    (**
     * Function: Ident.reset
     *
     * Resets the internal counters used by <Ident.new> to ensure that
     * different identifiers will be returned.
     *)
    val reset : unit -> unit

    (**
     * Function: Ident.distinct
     *
     * Filters out identical identifiers.
     *
     * Parameters:
     *   l - identifier list to filter
     *
     * Returns:
     *   an identifier list that contains the same identifiers as the passed one,
     *   each identifier being present at most once. There is no guarantee concerning
     *   the order in which the identifiers will appear.
     *)
    val distinct : t list -> t list

    (**
     * Function: Ident.prefix
     *
     * Prefixes all identifiers of a fiacre program.
     *
     * Parameters:
     *   pre - prefix
     *   f - fiacre program
     *
     * Returns:
     *   a fiacre program identical to the passed one except that all identifiers
     *   are prefixed with the passed string
     *)
    val prefix : string -> Fiacre -> Fiacre
end

structure Ident :> IDENT = struct
    (** Group: Internal declarations *)

    type t = string

    exception Invalid of string

    exception Reserved of string

    (**
     * Function: Ident.normalize
     *
     * Normalizes a string to be used as an identifier.
     *
     * Parameters:
     *   s - string to normalize
     *
     * Returns:
     *   the string in its normalized form
     *)
    fun normalize s = toUpperCase s

    (**
     * Function: Ident.isValid
     *
     * Tests whether the passed string is a valid identifier.
     *
     * Parameters:
     *   s - string to test
     *
     * Returns:
     *   true if the passed string is a valid identifier (possibly a reserved keyword),
     *   false otherwise
     *)
    fun isValid s = (String.size s) > 0

    structure S = struct
        type ord_key = string
        val compare = String.compare
    end

    (* Maps with strings as keys. *)
    structure Map = SplayMapFn (S)

    (* Sets with strings as elements. *)
    structure Set = SplaySetFn (S)

    (* Reserved LOTOS identifiers. *)
    val reservedLotosIdents =
        Set.addList (Set.empty,
                     map normalize
                         ["actualizedby", "any",
                          "behaviour",
                          "choice", "comparedby",
                          "endlib", "endproc", "endspec", "endtype", "enumeratedby", "eqns", "exit",
                          "for", "forall", "formaleqns", "formalopns", "formalsorts",
                          "hide",
                          "i", "implementedby", "in", "is", "iteratedby",
                          "let", "library",
                          "noexit",
                          "of", "ofsort", "opnames", "opns",
                          "par", "printedby", "process",
                          "renamedby",
                          "sortnames", "sorts", "specification", "stop",
                          "type",
                          "using",
                          "where"])

    fun make s =
        let
            val normalized = normalize s
        in
            if not (isValid normalized) then
                raise Invalid s
            else if Set.member (reservedLotosIdents, normalized) then
                raise Reserved s
            else
                normalized
        end

    fun toString s = s

    (* Counters used by 'new' to ensures that returned identifiers are different. *)
    val counters = ref (Map.empty : int Map.map)

    fun new s =
        let
            val normalized = normalize s
            val counter = case Map.find (!counters, normalized) of SOME c => c | NONE => 0
        in
            if isValid normalized then
                (counters := Map.insert (!counters, normalized, (counter + 1));
                 make (normalized ^ (Int.toString counter)))
            else
                raise Invalid s
        end

    fun reset () =
        counters := Map.empty

    fun distinct l =
        let
            val set = Set.addList (Set.empty, l)
        in
            Set.listItems set
        end

    fun prefix pre f =
        let
            fun p s = pre ^ (toUpperCase s)
            fun mapFiacre (Program (decls, main)) =
                Program ((map mapDeclaration decls),
                         (case main of SOME s => SOME (mapDeclaration s) | NONE => NONE))
            and mapDeclaration (TypeDecl (l, n, t)) = TypeDecl (l, (p n), (mapType t))
              | mapDeclaration (ChannelDecl (l, n, c)) = ChannelDecl (l, (p n), (mapChannel c))
              | mapDeclaration (ConstantDecl (l, n, t, e)) = ConstantDecl (l, (p n), (mapType t), (mapExp e))
              | mapDeclaration (ProcessDecl (l, n, prt, prm, st, vr, init, trans)) =
                ProcessDecl (l,
                             (p n),
                             (map mapPortDecl prt),
                             (map mapParamDecl prm),
                             (mapStatesDecl st),
                             (map mapVarDecl vr),
                             (case init of SOME (l, s) => SOME (l, mapStatement s) | NONE => NONE),
                             (map mapTransition trans))
              | mapDeclaration (ComponentDecl (l, n, prt, prm, vr, lp, pr, init, comp)) =
                ComponentDecl (l,
                               (p n),
                               (map mapPortDecl prt),
                               (map mapParamDecl prm),
                               (map mapVarDecl vr),
                               (map mapLocPortDecl lp),
                               (map mapPriorDecl pr),
                               (case init of SOME (l, s) => SOME (l, mapStatement s) | NONE => NONE),
                               (mapComposition comp))
              | mapDeclaration (Main (l, s)) = Main (l, (p s))
            and mapArg (ValArg s) = ValArg (p s)
              | mapArg (RefArg s) = RefArg (p s)
            and mapType BoolType = BoolType
              | mapType NatType = NatType
              | mapType IntType = IntType
              | mapType (NamedType n) = NamedType (p n)
              | mapType (IntervalType (e1, e2)) = IntervalType (mapExp e1, mapExp e2)
              | mapType (UnionType l) = UnionType (map (fn (ids, t) => ((map p ids), mapType t)) l)
              | mapType (RecordType l) = RecordType (map (fn (ids, t) => ((map p ids), mapType t)) l)
              | mapType (ArrayType (e, t)) = ArrayType (mapExp e, mapType t)
              | mapType (QueueType (e, t)) = QueueType (mapExp e, mapType t)
              | mapType AnyArray = AnyArray
              | mapType AnyQueue = AnyQueue
              | mapType BotType = BotType
              | mapType TopType = TopType
            and mapChannel (NamedChannel n) = NamedChannel (p n)
              | mapChannel (ProfileChannel p) = ProfileChannel (mapProfile p)
            and mapExp (ParenExp e) = ParenExp (mapExp e)
              | mapExp (CondExp (e1, e2, e3, ref t)) =
                CondExp ((mapExp e1), (mapExp e2), (mapExp e3), ref (mapType t))
              | mapExp (InfixExp (inf, ref t, e1, e2)) = InfixExp (inf, ref (mapType t), mapExp e1, mapExp e2)
              | mapExp (BinopExp (bin, ref t, e1, e2)) = BinopExp (bin, ref (mapType t), mapExp e1, mapExp e2)
              | mapExp (UnopExp (un, ref t, e)) = UnopExp (un, ref (mapType t), mapExp e)
              | mapExp (RecordExp (l, ref t)) = RecordExp (map (fn (id, e) => (p id, mapExp e)) l,
                                                           ref (mapType t))
              | mapExp (QueueExp (l, ref t)) = QueueExp (map mapExp l, ref (mapType t))
              | mapExp (ArrayExp (l, ref t)) = ArrayExp (map mapExp l, ref (mapType t))
              | mapExp (RefExp (n, ref t)) = RefExp (p n, ref (mapType t))
              | mapExp (ConstrExp (n, e, ref t)) = ConstrExp (p n, mapExp e, ref (mapType t))
              | mapExp (IntExp (x, ref t)) = IntExp (x, ref (mapType t))
              | mapExp (BoolExp b) = BoolExp b
              | mapExp (IdentExp (s, ref (k, t))) = IdentExp (p s, ref (k, mapType t))
              | mapExp AnyExp = AnyExp
              | mapExp (ArrayAccessExp (e1, e2, ref t)) = ArrayAccessExp (mapExp e1,
                                                                          mapExp e2,
                                                                          ref (mapType t))
              | mapExp (RecordAccessExp (e, n, ref t)) = RecordAccessExp (mapExp e,
                                                                          (p n),
                                                                          ref (mapType t))
            and mapStatement StNull = StNull
              | mapStatement (StCond (l, e, s, l', s')) =
                StCond (l,
                        mapExp e,
                        mapStatement s,
                        (map (fn (l, e, s) => (l, mapExp e, mapStatement s)) l'),
                        case s' of SOME x => SOME (mapStatement x) | NONE => NONE)
              | mapStatement (StSelect l) = StSelect (map mapStatement l)
              | mapStatement (StSequence (l, s1, s2)) = StSequence (l, mapStatement s1, mapStatement s2)
              | mapStatement (StTo (l, n)) = StTo (l, p n)
              | mapStatement (StWhile (l, e, s)) = StWhile (l, mapExp e, mapStatement s)
              | mapStatement (StAssign (l, patt, exp)) = StAssign (l, (map mapExp patt), (map mapExp exp))
              | mapStatement (StAny (l, patt, ref types, exp)) =
                StAny (l,
                       (map mapExp patt),
                       (ref (map mapType types)),
                       (case exp of
                            SOME e => SOME (mapExp e)
                          | NONE => NONE))
              | mapStatement (StSignal (l, n, ref c)) = StSignal (l, (p n), ref (mapChannel c))
              | mapStatement (StInput (l, n, ref c, patt, e)) =
                StInput (l,
                         (p n),
                         ref (mapChannel c),
                         (map mapExp patt),
                         case e of SOME e' => SOME (mapExp e') | NONE => NONE)
              | mapStatement (StOutput (l, n, ref c, l')) =
                StOutput (l, (p n), ref (mapChannel c), (map mapExp l'))
              | mapStatement (StCase (l, e, l')) =
                StCase (l, mapExp e, (map (fn (l, a, p, s) => (l, a, mapExp p, mapStatement s)) l'))
              | mapStatement (StForeach (l, id, t, s)) = 
                StForeach (l, (p id), t, (mapStatement s))
            and mapComposition (ParComp (l, set, l')) =
                ParComp (l, mapPortSet set, (map (fn (ps, c) => (mapPortSet ps, mapComposition c)) l'))
              | mapComposition (InstanceComp i) = InstanceComp (mapInstance i)
            and mapPortSet AllPorts = AllPorts
              | mapPortSet (SomePorts l) = SomePorts (map p l)
            and mapPortDecl (l, l', c) = (map (fn (l, s) => (l, p s)) l, l', mapChannel c)
            and mapParamDecl (l, l', t) = (map (fn (l, a) => (l, mapArg a)) l, l', mapType t)
            and mapStatesDecl (l, l') = (l, map p l')
            and mapVarDecl (l, t, e) = (map (fn (l, s) => (l, p s)) l,
                                        mapType t,
                                        case e of SOME e' => SOME (mapExp e') | NONE => NONE)
            and mapPriorDecl (l, l') = (map (fn (l, s) => (l, p s)) l, map (fn (l, s) => (l, p s)) l')
            and mapLocPortDecl (l, b) =
                (map (fn (l, l', c) => (map (fn (l, s) => (l, p s)) l, l', mapChannel c)) l, b)
            and mapInstance ((l, s), l', l'') = ((l, p s), map p l', map mapExp l'')
            and mapTransition (l, n, s) = (l, p n, mapStatement s)
            and mapProfile l = map mapType l
        in
            mapFiacre f
        end

end
