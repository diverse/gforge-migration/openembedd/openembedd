(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

signature EXPRESSIONS = sig
    (** Group: Exported declarations *)

    (**
     * Function: Expressions.conv
     *
     * Converts a value.
     *
     * Parameters:
     *   v - value to convert
     *   t - source sort
     *   t' - destination sort
     *
     * Returns:
     *   a code that converts v from sort t to sort t'
     *)
    val conv : Lotos.value -> Ident.t -> Ident.t -> Lotos.value

    (**
     * Function: Expressions.exprTypeToSort
     *
     * Returns the sort of a type expression.
     *
     * Parameters:
     *   t - type expression
     *
     * Returns:
     *   the sort of the passed type expression
     *)
    val exprTypeToSort : Type -> Ident.t

    (**
     * Function: Expressions.expr
     *
     * Encodes an expression.
     *
     * Parameters:
     *   types - map from type names to their actual types
     *   e - expression to encode
     *   t' - sort of expression to encode
     *
     * Returns:
     *   a code that determines the value of the passed expression
     *)
    val expr : Type Helper.StringMap.map -> Exp -> Ident.t -> Lotos.value

    (**
     * Function: Expressions.default
     *
     * computes the default value of a type.
     *
     * Parameters:
     *   types - map from type names to their actual types
     *   t - type 
     *
     * Returns:
     *   a code that computes the default value for the passed type
     *)
    val default  : Type Helper.StringMap.map -> Type -> Exp
end

structure Expressions :> EXPRESSIONS = struct

    open Lotos

    open Helper

    fun conv v t' t =
        let
            val sort' = Ident.toString t'
            val sort = Ident.toString t
            val nat2int = Ident.make "NatToInt"
            val int2nat = Ident.make "IntToNat"
        in
            case (sort', sort) of
                ("NAT", "NAT") => v
              | ("NAT", "INT") => Prefix (nat2int, [v])
              | ("NAT", interval) => Prefix ((idForIntTo interval), [(Prefix (nat2int, [v]))])
              | ("INT", "NAT") => Prefix (int2nat, [v])
              | ("INT", "INT") => v
              | ("INT", interval) => Prefix ((idForIntTo interval), [v])
              | (interval, "NAT") => Prefix (int2nat, [(Prefix ((idForToInt interval), [v]))])
              | (interval, "INT") => Prefix ((idForToInt interval), [v])
              | (interval, interval2) => if interval = interval2 then
                                             v
                                         else
                                             Prefix ((idForIntTo interval2), [(Prefix (idForToInt interval, [v]))])
        end

    fun exprTypeToSort t =
        case t of 
            BoolType => boolID
          | NatType => natID
          | IntType => intID
          | (NamedType s) => Ident.make s
          | _ => assertion InvalidExprType

    val infixOperators =
        let
            open EquationUtils
        in
            [(AND, bin "and"),
             (OR, bin "or"),
             (LE, lowerEqual),
             (LT, lowerThan),
             (GT, higherThan),
             (GE, higherEqual),
             (EQ, eq),
             (NE, neq),
             (ADD, plus),
             (SUB, minus),
             (MOD, mod'),
             (MUL, mult),
             (DIV, div')]
        end

    val arith = [
        ADD,
        SUB,
        MOD,
        MUL,
        DIV
    ]

    fun expr types e t' =
        let
            fun find n = StringMap.find (types, (Ident.toString (exprTypeToSort n)))
            open EquationUtils
        in
            case e of
                ParenExp e' => expr types  e' t'
              | CondExp _ => assertion ConditionalExpression
              | InfixExp (infixOp, ref opType, e1, e2) =>
                let
                    val t = exprTypeToSort opType
                    val e1' = expr types e1 t
                    val e2' = expr types e2 t
                    val infixOp' = assoc infixOp infixOperators
                    val res = e1' </infixOp'/> e2'
                in
                    if (mem infixOp arith) then (conv res t t') else res
                end
              | BinopExp (binop, ref opType, e1, e2) =>
                let
                    val elementType = case find opType of
                                          SOME (QueueType (_, et)) => et
                                        | _ => assertion InvalidQueueExprType
                    val t = exprTypeToSort opType
                    val e1' = expr types  e1 t
                    val e2' = expr types  e2 (exprTypeToSort elementType)
                    val nameFunc = case binop of ENQUEUE => idForEnqueue | APPEND => idForAppend
                in
                    Prefix ((nameFunc (Ident.toString t)), [e1', e2'])
                end
              | UnopExp (MINUS, ref opType, e) =>
                let
                    val t = exprTypeToSort opType
                in
                    conv (Prefix ((Ident.make "-"), [(expr types  e t)])) t t'
                end
              | UnopExp (PLUS, ref opType, e) => expr types  e (exprTypeToSort opType)
              | UnopExp (FIRST, ref opType, e) =>
                let
                    val elementType = case find opType of
                                          SOME (QueueType (_, et)) => et
                                        | _ => assertion InvalidQueueExprType
                    val t = exprTypeToSort opType
                    val e' = expr types  e t
                in
                    conv (Prefix ((idForFirst (Ident.toString t)), [e'])) (exprTypeToSort elementType) t'
                end
              | UnopExp (NOT, ref opType, e) => Prefix ((Ident.make "not"), [(expr types  e (exprTypeToSort opType))])
              | UnopExp (COERCE, _, e') => expr types  e' t'
              | UnopExp (unop, ref opType, e) =>
                let
                    val t = exprTypeToSort opType
                    val e' = expr types  e t
                    val nameFunc = case unop of
                                       FULL => idForFull
                                     | EMPTY => idForEmpty
                                     | DEQUEUE => idForDequeue
                                     | _ => raise Fail "match error in Expressions.expr/UnopExp"
                in
                    Prefix ((nameFunc (Ident.toString t)), [e'])
                end
              | RecordExp (l, ref recType) =>
                let
                    val l' = ListMergeSort.sort (fn ((s1, _), (s2, _)) => s1 > s2) l
                    val elemTypes = case find recType of
                                        SOME (RecordType l) => List.concat (map (fn (ids, t) => map (fn i => (i, t)) ids) l)
                                      | _ => assertion InvalidRecordExprType
                in
                    Prefix ((exprTypeToSort recType),
                            (map (fn (id, e) =>
                                     let
                                         val eType = assoc id elemTypes
                                     in
                                         expr types  e (exprTypeToSort eType)
                                     end)
                                 l'))
                end
                
              | QueueExp (l, ref queueType) =>
                let
                    val elementType = case find queueType of
                                          SOME (QueueType (_, et)) => et
                                        | _ => assertion InvalidQueueExprType
                    val i = List.length l
                in
                    Prefix ((idForIndexed ((Ident.toString (exprTypeToSort queueType)) ^ "_") i),
                            (map (fn e => expr types  e (exprTypeToSort elementType)) l))
                end
              | ArrayExp (l, ref arrType) =>
                let
                    val elementType = case find arrType of
                                          SOME (ArrayType (_, et)) => et
                                        | _ => assertion InvalidArrayExprType
                in
                    Prefix ((exprTypeToSort arrType),
                            (map (fn e => expr types  e (exprTypeToSort elementType)) l))
                end
              | RefExp (id, ref refType) =>
                conv (Var (Ident.make id)) (exprTypeToSort refType) t'
              | ConstrExp (id, e, ref unionType) =>
                let
                    val elementType = case find unionType of
                                          SOME (UnionType l) =>
                                          #2 (Option.valOf (List.find (fn (l', _) => mem id l') l))
                                        | _ => assertion UnknownConstructor
                in
                    Of ((Prefix ((Ident.make id), [(expr types  e (exprTypeToSort elementType))])), t')
                end
              | IntExp (n, _) =>
                let
                    val sort' = Ident.toString t'
                in
                    if n >= 0 then
                        (if n >= 0 andalso n <= 9 andalso (sort' = "INT" orelse sort' = "NAT") then
                             Of ((encodeInt n), t')
                         else if n > 9 andalso (sort' = "INT" orelse sort' = "NAT") then
                             Of ((encodeInt n), t')
                         else
                             conv (expr types  e natID) natID t')
                    else
                        (if sort' = "INT" then
                             Prefix ((Ident.make "Neg"), [(encodeInt (~n - 1))])
                         else
                             conv (expr types  e intID) intID t')
                end
              | BoolExp true => trueVal
              | BoolExp false => falseVal
              | IdentExp (id, ref (VarKind, varType)) =>
                conv (Var (Ident.make id)) (exprTypeToSort varType) t'
              | IdentExp (id, ref (ConstrKind, _)) => Of ((Const id), t')
              | IdentExp _ => assertion InvalidIdentExpr
              | AnyExp => assertion UnexpectedAnyExpr
              | ArrayAccessExp (array, index, ref arrType) =>
                let
                    val elementType = case find arrType of
                                          SOME (ArrayType (_, et)) => et
                                        | _ => assertion InvalidArrayExprType
                    val array' = expr types  array (exprTypeToSort arrType)
                    val index' = expr types  index natID
                    val get = Prefix ((idForArrayGet (Ident.toString (exprTypeToSort arrType))), [array', index'])
                in
                    conv get (exprTypeToSort elementType) t'
                end
              | RecordAccessExp (record, field, ref recType) =>
                let
                    val fieldType = case find recType of
                                        SOME (RecordType l) =>
                                        #2 (Option.valOf (List.find (fn (ids, _) => mem field ids) l))
                                      | _ => assertion InvalidRecordExprType
                    val record' = expr types record (exprTypeToSort recType)
                    val get = Prefix ((idForGet (Ident.toString (exprTypeToSort recType)) field), [record'])
                in
                    conv get (exprTypeToSort fieldType) t'
                end
        end

    fun default types t =
        case t of
            BoolType => BoolExp true
          | NatType => IntExp (0, (ref IntType))
          | IntType => IntExp (0, (ref IntType))
          | NamedType n => default types (Option.valOf (StringMap.find (types, n)) handle _ => assertion UnknownType)
          | IntervalType (n, _) => IntExp ((extractConstantFromExpression n), (ref IntType))
          | UnionType (((id :: _), t') :: _) =>
            (ConstrExp (id, (default types t'), ref t) handle _ => IdentExp (id, ref (ConstrKind, t)))
          | UnionType _ => assertion EmptyUnion
          | RecordType l => RecordExp ((List.concat (map (fn (ids, t') => map (fn id => (id, (default types t'))) ids) l)),
                                       (ref t))
          | ArrayType (n, t') =>
            let
                val size = extractConstantFromExpression n
                val def = default types t'
            in
                ArrayExp ((size >< (fn _ => def)), (ref t))
            end
          | QueueType (_, _) => QueueExp ([], (ref t))
          | _ => assertion NoDefault

end
