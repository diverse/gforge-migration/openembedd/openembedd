(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module contains some utility functions mainly related to string-handling.
 * *The Utils module is opened by this file.*
 *)

signature UTILS = sig
    (** Group: Exported declarations *)

    (**
     * Function: Utils.toUpperCase
     *
     * Converts a string to upper case.
     *
     * Parameters:
     *   s - string to convert
     *
     * Returns:
     *   a string equivalent to the passed one except that all letters are upper case
     *)
    val toUpperCase : string -> string

    (**
     * Function: Utils.toLowerCase
     *
     * Converts a string to lower case.
     *
     * Parameters:
     *   s - string to convert
     *
     * Returns:
     *   a string equivalent to the passed one except that all letters are lower case
     *)
    val toLowerCase : string -> string

    (**
     * Function: Utils.toCapitalized
     *
     * Capitalizes a string.
     *
     * Parameters:
     *   s - string to convert
     *
     * Returns:
     *   a string equivalent to the passed one except that its first character is upper case
     *)
    val toCapitalized : string -> string

    (**
     * Function: Utils.concatMap
     *
     * Maps elements of a list, and then concatenates them.
     *
     * Parameters:
     *   sep - separator
     *   f - function to map
     *   l - list to concatenate
     *
     * Returns:
     *   the string that would be returned by "String.concatWith s (map f l)"
     *)
    val concatMap : string -> ('a -> string) -> 'a list -> string

    (**
     * Function: Utils.surround
     *
     * Surrounds a string with parentheses.
     *
     * Parameters:
     *   s - string to surround
     *
     * Returns:
     *   a string equivalent to the passed one except that is is prefixed with "(",
     *   and suffixed with ")"
     *)
    val surround : string -> string

    (**
     * Function: Utils.surround_sq
     *
     * Surrounds a string with square brackets.
     *
     * Parameters:
     *   s - string to surround
     *
     * Returns:
     *   a string equivalent to the passed one except that is is prefixed with "[",
     *   and suffixed with "]"
     *)
    val surround_sq : string -> string

    (**
     * Function: Utils.identity
     *
     * The identity function.
     *
     * Parameters:
     *   x - any value
     *
     * Returns:
     *   x unchanged
     *)
    val identity : 'a -> 'a

    (**
     * Function: Utils.mem
     *
     * Membership test.
     *
     * Parameters:
     *   x - value to test
     *   l - list to test
     *
     * Returns:
     *   true if x belongs to l, false otherwise
     *)
    val mem : ''a -> ''a list -> bool

    (**
     * Function: Utils.assoc
     *
     * Retrieves from an association list.
     * An association list is a list of (key, value) couples.
     *
     * Parameters:
     *   k - key to search for
     *   l - list
     *
     * Returns:
     *   y where (x, y) is the first couple of l satisfying x = k
     *)
    val assoc : ''a -> (''a * 'b) list -> 'b
end

structure Utils :> UTILS = struct
    val toUpperCase = String.translate (Char.toString o Char.toUpper)

    val toLowerCase = String.translate (Char.toString o Char.toLower)

    fun toCapitalized "" = ""
      | toCapitalized s =
        let
            val first = String.sub (s, 0)
            val remaining = String.extract (s, 1, NONE)
        in
            ((Char.toString o Char.toUpper) first) ^ remaining
        end

    fun concatMap s f l =
        List.foldl
            (fn (x, acc) => if acc = "" then (f x) else acc ^ s ^ (f x))
            ""
            l

    fun surround s = "(" ^ s ^ ")"

    fun surround_sq s = "[" ^ s ^ "]"

    fun identity x = x

    fun mem e l = List.exists (fn x => x = e) l

    fun assoc k l = #2 (Option.valOf (List.find (fn (x, _) => x = k) l)) handle _ => raise LibBase.NotFound
end

open Utils
