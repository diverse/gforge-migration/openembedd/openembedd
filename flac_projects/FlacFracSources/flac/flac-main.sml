(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

(**
 * Note: Note
 * This module is the main one of the flac program.
 *)

(**
 * Function: unit
 *
 * Entry point of the flac program. Analyzes command-line and launches the
 * requested translation with passed arguments.
 *)
val () =
    let
        fun error m = (print (m ^ "\n"); raise BYE)
    in
        (* parse command line *)
        command (CommandLine.arguments ());

        (* check/set error stream *)
        if !ErrPrint.name <> "" then
            let
                val f = OS.Path.mkCanonical (!ErrPrint.name)
            in
                ErrPrint.FILE := TextIO.openOut f handle _ => error ("cannot open " ^ f)
            end
        else
            ();

        (* check/set input stream *)
        if !Input.name <> "" then
            let
                val f = OS.Path.mkCanonical (!Input.name)
            in
                case OS.Path.ext f of
                    SOME "fcr" => ()
                  | _ => error ("unknown format for input file: " ^ f);
                Input.FILE := TextIO.openIn f handle _ => error ("cannot open " ^ f)
            end
        else
            ();

        (* check/set output stream *)
        if !OutPrint.name <> "" then
            let
                val f = OS.Path.mkCanonical (!OutPrint.name)
            in
                case ((OS.Path.ext f), !OUTPUT) of
                    ((SOME "fcr"), NoOutput) => OUTPUT := Fiacre
                  | ((SOME "lotos"), NoOutput) => (OUTPUT := LOTOS; PRELOTOS := true)
                  | ((SOME _), NoOutput) => error ("unknown format for output file: " ^ f)
                  | ((SOME _), _) => ()
                  | (NONE, NoOutput) => ()
                  | (NONE, _) => ();
                OutPrint.FILE := TextIO.openOut f handle _ => error ("cannot open " ^ f)
            end
        else
            ();

        (* go *)
        let
            val (p,p'') = Type.typecheck (Parse.parse ())
            val p' = ElimCond.elimCond p''
        in
            Init.initcheck p';
            case !OUTPUT of
                Fiacre => if !PRELOTOS then
                              Print.print (Preprocess.preprocess p')
                          else if !TRANSF then
                              Print.print p'
                          else
                              Print.print p
              | LOTOS =>
                let
                    val preprocessed = Preprocess.preprocess p'
                    val renamed = (Ident.reset (); Ident.prefix "FIACRE_" preprocessed)
                    val (lotosSpecif, fFile, tFile) = ToLOTOS.translate renamed
                    fun writeLines file lines =
                        let
                            val f = TextIO.openOut file handle _ => error ("cannot open " ^ file)
                        in
                            TextIO.output(f, (String.concatWith "\n" lines));
                            TextIO.closeOut f
                        end
                in
                    OutPrint.print (Lotos.specificationToString lotosSpecif);
                    if !OutPrint.name <> "" then
                        let
                            val base = OS.Path.base (OS.Path.mkCanonical (!OutPrint.name))
                            val name = OS.Path.file base
                        in
                            if fFile <> [] then writeLines (base ^ ".f") fFile else ();
                            if tFile <> [] then writeLines (base ^ ".t") tFile else ();
                            writeLines (base ^ ".svl") [
                                ("\"" ^ name ^ ".bcg\" ="),
                                ("   gate rename \"FIACRE_\\(.*\\)\" -> \"\\1\" in"),
                                ("   multiple rename \"\\([^0-9A-Z_]\\)FIACRE_\" -> \"\\1\" in"),
                                ("   multiple rename \"\\([^0-9A-Z_]\\)ITV_FIACRE_[^ ]* (\\(.*\\))\" -> \"\\1FIACRE_\\2\" in"),
                                ("   \"" ^ name ^ ".lotos\"\n")
                            ]
                        end
                    else
                        ()
                end
              | NoOutput => ()
        end;

        (* exit *)
        ErrPrint.flush ();
        OutPrint.flush ();
        OS.Process.exit OS.Process.success
    end
    handle BYE => (ErrPrint.flush (); OutPrint.flush (); OS.Process.exit OS.Process.failure);
