(******************************************************************************
 *              F L A C    (FIACRE to LOTOS Adaptation Component)
 *-----------------------------------------------------------------------------
 *   INRIA - Institut National de Recherche en Informatique et en Automatique
 *   Centre de Recherche de Grenoble Rhone-Alpes / VASY
 *   655, avenue de l'Europe
 *   38330 Montbonnot Saint Martin
 *   FRANCE
 *-----------------------------------------------------------------------------
 *   $Id$
 *-----------------------------------------------------------------------------
 *   Copyright (C) 2008 INRIA
 *   Contributeurs/Contributors: Xavier Clerc, Frederic Lang
 *-----------------------------------------------------------------------------
 *   Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 *   respectant les principes de diffusion des logiciels libres. Vous pouvez
 *   utiliser, modifier et/ou redistribuer ce programme sous les conditions
 *   de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 *   sur le site "http://www.cecill.info".
 *
 *   En contrepartie de l'accessibilit� au code source et des droits de copie,
 *   de modification et de redistribution accord�s par cette licence, il n'est
 *   offert aux utilisateurs qu'une garantie limit�e. Pour les m�mes raisons,
 *   seule une responsabilit� restreinte p�se sur l'auteur du programme, le
 *   titulaire des droits patrimoniaux et les conc�dants successifs.
 *
 *   A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 *   associ�s au chargement, � l'utilisation, � la modification et/ou au
 *   d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 *   donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 *   manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 *   avertis poss�dant des connaissances informatiques approfondies. Les
 *   utilisateurs sont donc invit�s � charger et tester l'ad�quation du
 *   logiciel � leurs besoins dans des conditions permettant d'assurer la
 *   s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 *   � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
 *
 *   Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 *   pris connaissance de la licence CeCILL et que vous en avez accept� les
 *   termes.
 *-----------------------------------------------------------------------------
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use, 
 *   modify and/or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and, more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *****************************************************************************)

signature LOTOS = sig
    (** Group: Exported declarations *)

    (**
     * Type: Lotos.sortComment
     *
     * The type of special comments for sorts.
     *   - ImplementedBy s for ( *! implementedby s * )
     *   - ImplementedBy (s1, s2, s3) for ( *! implementedby s1 iteratedby s2 and s3 * )
     *)
    datatype sortComment = ImplementedBy of string
                         | ImplementedByIteratedBy of string * string * string

    (**
     * Type: Lotos.operationComment
     *
     * The type of special comments for operations.
     *   - Constructor s for ( *! implementedby s constructor * )
     *   - AnonConstructor for ( *! constructor * )
     *   - ImplementedByExternal s for ( *! implementedby s external * )
     *)
    datatype operationComment = Constructor of string
                              | AnonConstructor
                              | ImplementedByExternal of string

    (**
     * Function: Lotos.sortCommentToString
     *
     * Converts a sort comment into a string.
     *
     * Parameters:
     *   c - sort comment to convert
     *
     * Returns:
     *   a string representation of the passed sort comment
     *)
    val sortCommentToString : sortComment -> string

    (**
     * Function: Lotos.operationCommentToString
     *
     * Converts an operation comment into a string.
     *
     * Parameters:
     *   c - operation comment to convert
     *
     * Returns:
     *   a string representation of the passed operation comment
     *)
    val operationCommentToString : operationComment -> string

    (**
     * Type: Lotos.operation
     *
     * The type of operations.
     * Fields are:
     *   - names, a list of (<Ident.t>, <Lotos.operationComment> option) couples
     *   - parameters, a list of <Ident.t>
     *   - result, an <Ident.t>
     *)
    type operation = { names : (Ident.t * (operationComment option)) list,
                       parameters : Ident.t list,
                       result : Ident.t }

    val operationToString : operation -> string

    (**
     * Type: Lotos.value
     *
     * The type of values.
     *   - Const s for a constant
     *   - Var id for a variable
     *   - Prefix (id, valList) for function id applied to paremters valList
     *   - Infix (v1, id, v2) for binary function id applied to v1 and v2
     *   - Of (v, id) for conversion operation "v of id"
     *)
    datatype value = Const of string
                   | Var of Ident.t
                   | Prefix of Ident.t * (value list)
                   | Infix of value * Ident.t * value
                   | Of of value * Ident.t

    (**
     * Type: Lotos.simpleEq
     *
     * The type of simple equations: value couples.
     *)
    type simpleEq = value * value

    (**
     * Type: Lotos.premEq
     *
     * The type of equations.
     *   - Simple eq for a simple equation
     *   - Value v for an equation reduced to a value
     *)
    datatype premEq = Simple of simpleEq
                    | Value of value

    (**
     * Type: Lotos.mediumEq
     *
     * The type of medium equation. (l, s) couples where
     *   - l is a list of equations
     *   - s is a simple equation
     *)
    type mediumEq = premEq list * simpleEq

    (**
     * Type: Lotos.forAll
     *
     * The type of "for all" clauses. Lists of (ids, dom) couples where
     *   - ids is a list of identifiers
     *   - dom is an identifier
     *)
    type forAll = (Ident.t list * Ident.t) list

    (**
     * Type: Lotos.complexEq
     *
     * The type of complex equations. (s, fa, l) triples where
     *   - s is a sort identifier
     *   - fa is a forall clause for equations
     *   - l is a list of medium equations
     *)
    type complexEq = Ident.t * forAll * (mediumEq list)

    val valueToString : value -> string

    val simpleEqToString : simpleEq -> string

    val premEqToString : premEq -> string

    val mediumEqToString : mediumEq -> string

    val forAllToString : forAll -> string

    val complexEqToString : string -> complexEq -> string

    (**
     * Type: Lotos.typeDeclaration
     *
     * The type of type declarations.
     * Fields are:
     *   - name, an <Ident.t>
     *   - parents, an <Ident.t> list
     *   - formalsorts, an <Ident.t> list
     *   - formaloperations, a <Lotos.operation> list
     *   - formalequations, a list of (<Lotos.forall>, <Lotos.complexEq> list) couples
     *   - sorts, an <Ident.t> list
     *   - operations, a <Lotos.operation> list
     *   - equations, a list of (<Lotos.forall>, <Lotos.complexEq> list) couples
     *)
    type typeDeclaration = { name : Ident.t,
                             parents : Ident.t list,
                             formalsorts : Ident.t list,
                             formaloperations : operation list,
                             formalequations : forAll * (complexEq list),
                             sorts : (Ident.t * (sortComment option)) list,
                             operations : operation list,
                             equations : forAll * (complexEq list) }

    (**
     * Type: Lotos.typeInstances
     *
     * The type of type instances.
     * Fields are:
     *   - name, an <Ident.t>
     *   - formal_name, an <Ident.t>
     *   - parameter_types, a list of <Ident.t>
     *   - sort_names, a list of (<Ident.t>, <Ident.t>) couples
     *   - operation_names, a list of (<Ident.t>, <Ident.t>) couples
     *)
    type typeInstance = { name : Ident.t,
                          formal_name : Ident.t,
                          parameter_types : Ident.t list,
                          sort_names : (Ident.t * Ident.t) list,
                          operation_names : (Ident.t * Ident.t) list }

    (**
     * Type: Lotos.typeAlias
     *
     * The type of type aliases.
     * Fields are:
     *   - name, an <Ident.t>
     *   - formal_name, an <Ident.t>
     *   - sort_names, a list of (<Ident.t>, <Ident.t>) couples
     *   - operation_names, a list of (<Ident.t>, <Ident.t>) couples
     *)
    type typeAlias = { name : Ident.t,
                       formal_name : Ident.t,
                       sort_names : (Ident.t * Ident.t) list,
                       operation_names : (Ident.t * Ident.t) list }

    (**
     * Type: Lotos.typeDef
     *
     * The type of type definitions.
     *   - TypeDeclaration t for type declaration
     *   - TypeInstance t for type instance
     *   - TypeAlias t for type alias
     *)
    datatype typeDef = TypeDeclaration of typeDeclaration
                     | TypeInstance of typeInstance
                     | TypeAlias of typeAlias

    val typeDeclarationToString : string -> typeDeclaration -> string

    val typeInstanceToString : string -> typeInstance -> string

    val typeAliasToString : string -> typeAlias -> string

    val typeDefToString : string -> typeDef -> string

    (**
     * Type: Lotos.gate
     *
     * The type of gates: synonym of <Ident.t> used for increased readability.
     *)
    type gate = Ident.t

    (**
     * Type: Lotos.offer
     *
     * The type of offers.
     *   - Send v for the send of value v
     *   - Receive (ids, s) for the retrieval of ids (<Ident.t> list) of sort s
     *)
    datatype offer = Send of value
                   | Receive of (Ident.t list) * Ident.t

    (**
     * Type: Lotos.condition
     *
     * The type of conditions.
     *   - SimpleCondition v for a condition reduced to value v
     *   - Equal (v1, v2) for a condition that succeeds if v1 and v2 are equal
     *)
    datatype condition = SimpleCondition of value
                       | EqualCondition of value * value

    (**
     * Type: Lotos.parallelOperator
     *
     * The type of parallel operators.
     *   - General l for "|[ g1, g2, ...]|" where g1, g2, ... are elements of l (<Lotos.gate> list)
     *   - Async for |||
     *   - Sync for ||
     *)
    datatype parallelOperator = General of gate list | Async | Sync

    (**
     * Type: Lotos.exitValue
     *
     * The type of exit values.
     *   - SimpleExitValue v to exit with value v
     *   - AnyExitValue s to exit with any value of sort s (<Ident.t>)
     *)
    datatype exitValue = SimpleExitValue of value | AnyExitValue of Ident.t

    (**
     * Type: Lotos.behaviour
     *
     * The type of behaviours.
     *   - Stop for "stop"
     *   - Rendezvous (g, offers, cond, b) for "g !o1 !o2 cond; b" where oi are elements from offers
     *   - Choice (b1, b2) for "b1 [] b2"
     *   - GateChoice (l, b) for "choice g1 in g1', g2' ... [] b" where gi are elements from l
     *     (list of <Ident.t> list, <Ident.t> couples)
     *   - Parallel (b1, l, b2) for "b1 |[ g1, g2, ... ]| b2" where gi are elements from l
     *   - ParallelAsync (b1, b2) for "b1 ||| b2"
     *   - ParallelSync (b1, b2) for "b1 || b2"
     *   - Par (l, po, b) for "par x1 in x1', x2' ... b" where xi are elements from l
     *     (list of <Ident.t> list, <Ident.t> couples)
     *   - Hide (l, b) for "hide g1, g2, ... in b" where gi are elements from l
     *   - Guard (c, b) for "c -> b"
     *   - Let (l, b) for "let x1 : s1 = v1, x2 : s2 = v2 ... in b" where l is a list of
     *     (variable identifiers, sort identifier, value) triples
     *   - ValueChoice (l, b) for "choice v1 in v1', v2' ... [] b" where gi are elements from l
     *   - Exit l for "exit (v1, v2, ...)" where vi are elements from l
     *   - Enable (b1, l, b2) for "b1 >> accept x1 : s1, x2 : s2, ... in b2" where l is a list
     *     of (identifier list, identifier) couples
     *   - Disable (b1, b2) for "b1 [> b2"
     *   - CallProcess (p, g, v) for "p [g1, g2, ...] (v1, v2, ...)" where gi are elements from g
     *     and vi are elements from v
     *   - Parentheses b for enforcing priorities (also used by the pretty-printer)
     *)
    datatype behaviour =
             Stop
           | Rendezvous of gate * (offer list) * (condition option) * behaviour
           | Choice of behaviour * behaviour
           | GateChoice of (gate * (gate list)) list * behaviour
           | Parallel of behaviour * (gate list) * behaviour
           | ParallelAsync of behaviour * behaviour
           | ParallelSync of behaviour * behaviour
           | Par of (gate * (gate list)) list * parallelOperator * behaviour
           | Hide of gate list * behaviour
           | Guard of condition * behaviour
           | Let of ((Ident.t list * Ident.t * value) list) * behaviour
           | ValueChoice of (Ident.t list * Ident.t) list * behaviour
           | Exit of exitValue list
           | Enable of behaviour * ((Ident.t list * Ident.t) list) * behaviour
           | Disable of behaviour * behaviour
           | CallProcess of Ident.t * (gate list) * (value list)
           | Parentheses of behaviour

    val gateToString : gate -> string

    val offerToString : offer -> string

    val conditionToString : condition -> string

    val parallelOperatorToString : parallelOperator -> string

    val exitValueToString : exitValue -> string

    val behaviourToString : string -> string -> behaviour -> string

    (**
     * Type: Lotos.functionality
     *
     * The type of functionalities.
     *   - NoExit for a process that never exits
     *   - Exit l for a process that exits by returning values whose sorts are given by l
     *)
    datatype functionality = NoExit | ExitWith of Ident.t list

    (**
     * Type: Lotos.block
     *
     * The type of blocks.
     *   - TypeBlock t for a block containing a type definition t
     *   - ProcessBlock p for a block containing a process declaration p
     *)
    datatype block = TypeBlock of typeDef
                   | ProcessBlock of process
    (**
     * Type: Lotos.process
     *
     * The type of processes.
     * Fields are:
     *   - name, an <Ident.t>
     *   - gates, an <Ident.t> list
     *   - parameters, a list of (<Ident.t> list, <Ident.t>) couples
     *   - func, a <Lotos.functionality>
     *   - body, a <Lotos.behaviour>
     *   - blocks, a <Lotos.block> list
     *)
    and process = Process of { name : Ident.t,
                               gates : gate list,
                               parameters : (Ident.t list * Ident.t) list,
                               func : functionality,
                               body : behaviour,
                               blocks : block list }

    val functionalityToString : functionality -> string

    val blockToString : string -> string -> block -> string

    val processToString : string -> string -> process -> string

    (**
     * Type: Lotos.specification
     *
     * The type of specifications.
     * Fields are:
     *   - name, an <Ident.t>
     *   - gates, an <Ident.t> list
     *   - parameters, a list of (<Ident.t> list, <Ident.t>) couples
     *   - func, a <Lotos.functionality>
     *   - libraries, an <Ident.t> list
     *   - types, a <Lotos.typeDef> list
     *   - body, a <Lotos.behaviour>
     *   - blocks, a <Lotos.block> list
     *)
    type specification = { name : Ident.t,
                           gates : gate list,
                           parameters : (Ident.t list * Ident.t) list,
                           func : functionality,
                           libraries : Ident.t list,
                           types : typeDef list,
                           body : behaviour,
                           blocks : block list }

    val specificationToString : specification -> string

end
