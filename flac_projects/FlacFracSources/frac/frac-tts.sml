
(* tts abstract syntax and utilities *)
(* identical items are shared with front/frac-fiacre *)


(* max number of digits of interval bounds, for emit *)
val DIGITS = ref 0;


structure TTS = struct

datatype TTS =
	  Program of Declaration list
and Declaration =
	  TypeDecl of
                string * Type
	| ProcessDecl of
		string *
		PortDecl list *
		ParamDecl list *
		StatesDecl *
		VarDecl list *          (* temporaries *)
		VarDecl list *          (* state variables *)
		locPortDecl list *
		PriorDecl list *        (* USELESS, remove *)
		Statement *             (* init statement *)
		Transition list
	| ComponentDecl of
		string *
		PortDecl list *
		ParamDecl list *
		VarDecl list *          (* temporaries *)
		VarDecl list *          (* state variables *)
		locPortDecl list *
		PriorDecl list *		
		Statement *             (* init statement *)
		Composition
	| Main of string
(* shared with Fiacre:
and Arg =
          ValArg of string						(* arg passed by value		*)
	| RefArg of string						(* arg passed by reference	*)
*)
and Type =
	  BoolType
	| NatType
	| IntType
	| NamedType of string
	| IntervalType of int * int
	| UnionType of (string * Type option) list
	| EnumType of string list
	| RecordType of (string * Type) list
	| ArrayType of int * Type
	| QueueType of int * Type
and Exp = 
	  BoolExp of bool
	| IntExp of int * Type
  	| InfixExp of Infix * Type * Exp * Exp
  	| BinopExp of Binop * Type * Exp * Exp
  	| UnopExp of Unop * Type * Exp
	| ArrayAccessExp of Exp * Exp * Type
	| RecordAccessExp of Exp * string * Type
	| EnumExp of string * Type                          (* IdentExp (_,(ConstrKind,ty)) when ty is an enum *)
	| VarExp of string * Type                           (* IdentExp (_,(VarKind,_)) *)
        | CondExp of Exp * Exp * Exp                        (* functional conditional *)
	| RefExp of string * Type                           (* var passed by reference (only in instances) *)
	| RecordExp of (string * Exp) list * Type
	| ArrayExp of Exp list * Type
	| QueueExp of Exp list * Type
        | LetExp of Statement * Exp
	(* *)
	| ConExp of string * Type                           (* IdentExp (_,(ConstrKind,ty)) when ty is a general union *)
	| ConstrExp of string * Exp * Type                  (* construction *)
	| DestrExp of string * Exp                          (* extracts arg of construction, constr needed for C codegen *)
	| MatchesExp of string * Exp                        (* checks if tagge with constructor *)
	| AnyExp                                            (* universal pattern *)
and Statement =
	  StNull
	| StCond of Exp * Statement * Statement
	| StSelect of Statement list			
	| StSequence of Statement * Statement
	| StTo of string list    			    (* possibly several target states *)
	| StWhile of Exp * Statement
	| StForeach of Exp * Statement
	| StAssign of Pattern * Exp 
	| StSignal of string
	| StInput of string * Type list * Pattern list
	| StOutput of string * Type list * Exp list
	(* *)
	| StGuard of Exp
	| StInputList of string * Pattern list list         (* for building compositions *)
	| StBlock of (string * Type) list * Statement
and Composition =
	  ParComp of (string list * Composition) list
	| InstanceComp of Instance
	| PrioSpec of (string * string) list * Composition  (* keeps track of priorities in unfolded compositions *)

withtype

	PortDecl = string * Type list
and	ParamDecl = Arg * Type
and	StatesDecl = string list                             (* states *)
and	VarDecl = string * Type * Exp
and 	PriorDecl = string list * string list
and 	locPortDecl = string * Type list * (Bound * Bound)
and     Instance = string * string list * Exp list
and     Transition = (string list * string ) *               (* tag list * label               *)
		     string list *                           (* source states                  *)
		     Statement *                             (* body                           *)
		     (Bound * Bound) *                       (* time interval                  *)
		     string list                             (* transitions with lower priority *)
and     Pattern = Exp
;

end;  (* TTS *)




(* from now on working on TTS (rather than Fiacre) *)
open TTS;




(* ---- some useful functions on expressions and statements *)

(* true if some subexpression of e satisfies f *)
fun matchExp f e =
    f e orelse case e of
        InfixExp(_,_,e1,e2) => matchExp f e1 orelse matchExp f e2
      | BinopExp(_,_,e1,e2) => matchExp f e1 orelse matchExp f e2
      | UnopExp (_,_,e) => matchExp f e
      | RecordExp(fel,_) => List.exists (fn (_,e) => matchExp f e) fel
      | ArrayExp(el,_) => List.exists (matchExp f) el
      | QueueExp(el,_) => List.exists (matchExp f) el
      | ArrayAccessExp (e1,e2,_) => matchExp f e1 orelse matchExp f e2
      | RecordAccessExp (e,_,_) => matchExp f e
      | CondExp (c,e1,e2) => matchExp f c orelse matchExp f e1 orelse matchExp f e2
      | ConstrExp (_,e,_) => matchExp f e
      | DestrExp (_,e) => matchExp f e
      | MatchesExp (_,e) => matchExp f e
      | LetExp (s,e) => matchExpInStm f s orelse matchExp f e
      | _ => false
and matchExpInStm f s =
    case s of
        StCond (e,s1,s2) => matchExp f e orelse matchExpInStm f s1 orelse matchExpInStm f s2
      | StSelect sl => List.exists (matchExpInStm f) sl
      | StSequence (s1,s2) => matchExpInStm f s1 orelse matchExpInStm f s2
      | StWhile (e,s) => matchExp f e orelse matchExpInStm f s
      | StForeach (v,s) => matchExp f v orelse matchExpInStm f s
      | StAssign (p,e) => matchExp f p orelse matchExp f e
      | StGuard e => matchExp f e
      | StBlock (d,s) => matchExpInStm f s
      | _ => false;


(* applies f to all subexpressions of e, bottom up *)
fun mapExp f e =
    case e of
        InfixExp(inop,ty,e1,e2) => f (InfixExp (inop,ty,mapExp f e1,mapExp f e2))
      | BinopExp(biop,ty,e1,e2) => f (BinopExp(biop,ty,mapExp f e1,mapExp f e2))
      | UnopExp (unop,ty,e) => f (UnopExp (unop,ty,mapExp f e))
      | RecordExp(fel,ty) => f (RecordExp (map (fn (fd,e) => (fd,mapExp f e)) fel,ty))
      | ArrayExp(el,ty) => f (ArrayExp (map (mapExp f) el,ty))
      | QueueExp(el,ty) => f (QueueExp (map (mapExp f) el,ty))
      | ArrayAccessExp (e1,e2,ty) => f (ArrayAccessExp(mapExp f e1,mapExp f e2,ty))
      | RecordAccessExp (e,st,ty) => f (RecordAccessExp(mapExp f e,st,ty))
      | CondExp (c,e1,e2) => f (CondExp (mapExp f c, mapExp f e1, mapExp f e2))
      | ConstrExp (c,e,ty) => f (ConstrExp (c,mapExp f e,ty))
      | DestrExp (c,e) => f (DestrExp (c,mapExp f e))
      | MatchesExp (c,e) => f (MatchesExp (c,mapExp f e))
      | LetExp (s,e) => f (LetExp (mapExpInStm f s,mapExp f e))
      | _ => f e
and mapExpInStm f s =
	case s of
        StCond (e,s1,s2) => StCond (mapExp f e,mapExpInStm f s1, mapExpInStm f s2)
      | StSelect sl => StSelect (map (mapExpInStm f) sl)
      | StSequence (s1,s2) => StSequence (mapExpInStm f s1,mapExpInStm f s2)
      | StWhile (e,s) => StWhile (mapExp f e, mapExpInStm f s)
      | StForeach (v,s) => StForeach (mapExp f v,mapExpInStm f s)
      | StAssign (p,e) => StAssign(mapExp f p,mapExp f e)
      | StGuard e => StGuard (mapExp f e)
      | StBlock (d,s) => StBlock (d,mapExpInStm f s)
      | _ => s;
	

(* true if some substatement of s satisfies f *)
fun matchStm f s =
    f s orelse case s of
	  StCond (_,s1,s2) => matchStm f s1 orelse matchStm f s2
	| StSelect sl => List.exists (matchStm f) sl
	| StSequence (s1,s2) => matchStm f s1 orelse matchStm f s2
	| StWhile (_,s) => matchStm f s
	| StForeach (_,s) => matchStm f s
	| _ => false;

(* applies f to all substatements of s, bottom up *)
fun mapStm f s =
    case s of
	  StCond (e,s1,s2) => f (StCond (e,mapStm f s1, mapStm f s2))
	| StSelect sl => f (StSelect (map (mapStm f) sl))
	| StSequence (s1,s2) => f (StSequence (mapStm f s1,mapStm f s2))
	| StWhile (e,s) => f (StWhile (e, mapStm f s))
	| StForeach (v,s) => f (StForeach (v,mapStm f s))
	| _ => f s;



(* substitution functions for variables, ports, states ... *)

(* renames variables in expression according to substitution m *)
fun aliasvExp m e =
    case e of
        InfixExp(inop,ty,e1,e2) => InfixExp (inop,ty,aliasvExp m e1,aliasvExp m e2)
      | BinopExp(biop,ty,e1,e2) => BinopExp(biop,ty,aliasvExp m e1,aliasvExp m e2)
      | UnopExp (unop,ty,e) => UnopExp (unop,ty,aliasvExp m e)
      | VarExp (st,ty) => VarExp (alias st m,ty)
      | RefExp (st,ty) => RefExp (alias st m,ty)
      | ArrayAccessExp (ac,ex,ty) => ArrayAccessExp(aliasvExp m ac,aliasvExp m ex,ty)
      | RecordAccessExp (ac,st,ty) => RecordAccessExp(aliasvExp m ac,st,ty)
      | CondExp(c,e1,e2) => CondExp(aliasvExp m c,aliasvExp m e1,aliasvExp m e2)
      | RecordExp (fl,ty) => RecordExp (map (fn (f,e) => (f,aliasvExp m e)) fl,ty)
      | ArrayExp (el,ty) => ArrayExp (map (aliasvExp m) el,ty)
      | QueueExp (el,ty) => QueueExp (map (aliasvExp m) el,ty)
      | ConstrExp (c,e,ty) => ConstrExp (c,aliasvExp m e,ty)
      | DestrExp (c,e) => DestrExp (c,aliasvExp m e)
      | MatchesExp (c,e) => MatchesExp (c,aliasvExp m e)
      | LetExp _ => failwith "LetExp in aliasvExp"
      | _ => e;
(* same using mapExp:
fun aliasvExp m e =
    let fun aliasE e = case e of VarExp (st,ty) => VarExp (alias st m,ty)
			       | RefExp (st,ty) => RefExp (alias st m,ty)
			       | _ => e
    in mapExp aliasE e
    end;
*)

(* renames variables in statement according to substitution f *)
fun aliasvSt f s =
    case s of
	StSelect sl => StSelect(map (aliasvSt f) sl)
      | StSequence (s1,s2) => StSequence(aliasvSt f s1,aliasvSt f s2)
      | StCond(e,s1,s2) => StCond(aliasvExp f e,aliasvSt f s1,aliasvSt f s2)
      | StGuard e => StGuard(aliasvExp f e)
      | StWhile(e,s) => StWhile(aliasvExp f e,aliasvSt f s)
      | StForeach(v,s) => StForeach(aliasvExp f v,aliasvSt f s)
      | StAssign (p,e) => StAssign (aliasvExp f p,aliasvExp f e)
      | StInput(p,tyl,pl) => StInput(p,tyl,map (aliasvExp f) pl)
      | StOutput(p,tyl,el) => StOutput(p,tyl,el)
      | _ => s;
(* same using mapStm:
fun aliasvSt f s =
    let fun aliasSt s =
	    case s of
		StCond(e,s1,s2) => StCond(aliasvExp f e,aliasvSt f s1,aliasvSt f s2)
	      | StGuard e => StGuard(aliasvExp f e)
	      | StWhile(e,s) => StWhile(aliasvExp f e,aliasvSt f s)
	      | StForeach(v,s) => StForeach(aliasvExp f v,aliasvSt f s)
	      | StAssign (p,e) => StAssign (aliasvExp f p,aliasvExp f e)
	      | StInput(p,tyl,pl) => StInput(p,tyl,map (aliasvExp f) pl)
	      | StOutput(p,tyl,el) => StOutput(p,tyl,map (aliasvExp f) el)
	      | _ => s
    in mapStm aliasSt s
    end;
*)

(* renames ports in statement according to substitution f *)
fun aliaspSt f s =
    case s of
	StSelect sl => StSelect(map (aliaspSt f) sl)
      | StSequence (s1,s2) => StSequence(aliaspSt f s1,aliaspSt f s2)
      | StCond(e,s1,s2) => StCond(e,aliaspSt f s1,aliaspSt f s2)
      | StWhile(e,s) => StWhile(e,aliaspSt f s)
      | StForeach(v,s) => StForeach(v,aliaspSt f s)
      | StSignal p => StSignal (alias p f)
      | StInput(p,tyl,pl) => StInput(alias p f,tyl,pl)
      | StOutput(p,tyl,el) => StOutput(alias p f,tyl,el)
      | _ => s;
(* same using mapStm:
fun aliaspSt f s =
    let fun aliasSt s =
	    case s of
		StSignal p => StSignal (alias p f)
	      | StInput(p,tyl,pl) => StInput(alias p f,tyl,pl)
	      | StOutput(p,tyl,el) => StOutput(alias p f,tyl,el)
	      | _ => s
    in mapStm aliasSt s
    end;
 *)

(* renames states in statement according to substitution f *)
fun aliassSt f s =
    case s of
	StSelect sl => StSelect(map (aliassSt f) sl)
      | StSequence (s1,s2) => StSequence(aliassSt f s1,aliassSt f s2)
      | StCond(e,s1,s2) => StCond(e,aliassSt f s1,aliassSt f s2)
      | StWhile(e,s) => StWhile(e,aliassSt f s)
      | StForeach(v,s) => StForeach(v,aliassSt f s)
      | StTo stl => StTo (map (fn x => alias x f) stl)
      | _ => s;
(* same using mapStm:
fun aliassSt f s =
    let fun aliasSt s = case s of StTo stl => StTo (map (fn x => alias x f) stl) | _ => s
    in mapStm aliasSt s
    end;
 *)



