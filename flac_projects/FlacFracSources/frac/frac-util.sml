

fun printVerbose ss = if !VERBOSE then ErrPrint.print ss else ();

(* returns value associated with x in l, or x itself if none *)
(* fun alias x l = case List.find (fn (z,_) => z=x) l of SOME (_,y) => y | _ => x; *)
fun alias x ((y,z)::t) = if x=y then z else alias x t | alias x [] = x;


(* error messages : + error defined in frac-flags *)

fun notyet m = (ErrPrint.print ("feature not supported yet (" ^ m ^ "), sorry\n"); ErrPrint.flush (); raise BYE);
fun comperror m = error (ErrPrint.print ("cannot compile: " ^ m ^ "\n"); ErrPrint.flush (); raise BYE);
fun failwith m = (ErrPrint.print ("unexpected failure (" ^ m ^ "), please report it\n"); ErrPrint.flush (); raise BYE);


(* for emit *)

fun pow (x,0) = 1.0 | pow (x,1) = x | pow (x,y) = pow (10.0 * x,y-1);

(* for totts, etc *)

fun range(min,max) = if min=max then [max] else min :: (range(min+1,max))
