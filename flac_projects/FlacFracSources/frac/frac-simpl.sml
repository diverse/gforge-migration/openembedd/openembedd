(* static evaluation and simplification of expressions *)

fun rangeError e =
    let open ErrPrint
    in print "cannot compile: out of range numeric value when computing:\n";
       ErrTtsEmit.print_Exp e; print "\n";
       ErrPrint.flush (); raise BYE
    end;

fun checkRange exp (e as IntExp (n,ty)) = case ty of
        IntType => e
      | NatType => if n>=0 then e else rangeError exp
      | IntervalType (f,t) => if n>=f andalso n<=t then e else rangeError exp;

fun isLiteral e = not (matchExp (fn VarExp _ => true | _ => false) e);

(* symbolic equality of expressions, omitting types *)
fun equal e1 e2 =
    case (e1,e2) of
	(IntExp (k1,_),IntExp(k2,_)) => k1=k2
      | (InfixExp(op1,_,e11,e12),InfixExp(op2,_,e21,e22)) => op1=op2 andalso equal e11 e21 andalso equal e12 e22
      | (BinopExp(op1,_,e11,e12),BinopExp(op2,_,e21,e22)) => op1=op2 andalso equal e11 e21 andalso equal e12 e22
      | (UnopExp(op1,_,e1),UnopExp(op2,_,e2)) => op1=op2 andalso equal e1 e2
      | (ArrayAccessExp(a1,i1,_),ArrayAccessExp(a2,i2,_)) => equal a1 a2 andalso equal i1 i2
      | (RecordAccessExp(r1,f1,_),RecordAccessExp(r2,f2,_)) => equal r1 r2 andalso f1=f2
      | (EnumExp(c1,_),EnumExp(c2,_)) => c1=c2
      | (VarExp (v1,_),VarExp (v2,_)) => v1=v2	 
      | (CondExp(c1,e11,e12),CondExp(c2,e21,e22)) => equal c1 c2 andalso equal e11 e21 andalso equal e12 e22
      | (RefExp(v1,_),RefExp(v2,_)) => v1=v2
      | (RecordExp(fel1,_),RecordExp(fel2,_)) => List.all (fn ((f1,e1),(f2,e2)) => f1=f2 andalso equal e1 e2) (ListPair.zip(fel1,fel2))
      | (ArrayExp(el1,_),ArrayExp(el2,_)) => List.all (fn (e1,e2) => equal e1 e2) (ListPair.zip(el1,el2))
      | (QueueExp(el1,_),QueueExp(el2,_)) => List.all (fn (e1,e2) => equal e1 e2) (ListPair.zip(el1,el2))
      | (LetExp (s1,e1),LetExp (s2,e2)) => equalInSt s1 s2 andalso equal e1 e2
      | (ConExp(c1,_),ConExp(c2,_)) => c1=c2
      | (ConstrExp(c1,e1,_),ConstrExp(c2,e2,_)) => c1=c2 andalso equal e1 e2
      | (DestrExp(c1,e1),DestrExp(c2,e2)) => c1=c2 andalso equal e1 e2
      | (MatchesExp(c1,e1),MatchesExp(c2,e2)) => c1=c2 andalso equal e1 e2
      | _ => e1=e2
and equalInSt s1 s2 = (* only for statements in a LetExp *)
    case (s1,s2) of
	(StCond (e1,s11,s12),StCond(e2,s21,s22)) => equal e1 e2 andalso equalInSt s11 s21 andalso equalInSt s21 s22
      | (StSequence (s11,s12),StSequence (s21,s22)) => equalInSt s11 s21 andalso equalInSt s21 s22
      | (StWhile (e1,s1),StWhile(e2,s2)) => equal e1 e2 andalso equalInSt s1 s2
      | (StForeach (v1,s1),StForeach (v2,s2)) => equal v1 v2 andalso equalInSt s1 s2
      | (StAssign (p1,e1),StAssign (p2,e2)) => equal p1 p2 andalso equal e1 e2
      | _ => (s1=s2);


(* simplifies an expression in context env (possibly partial) *)
fun expSimpl env e =
    case e of
	VarExp (v,_) => (assoc v env handle _ => e)
      | RecordAccessExp (a,f,r) =>
	(case expSimpl env a of
	     RecordExp (fl,_) => assoc f fl
	   | a' => RecordAccessExp (a',f,r))
      | ArrayAccessExp (a,i,r) =>
	(case expSimpl env a of
	     a' as ArrayExp (el,_) =>
	     (case expSimpl env i of
		  IntExp (x,_) => List.nth (el,x)
		| i' => ArrayAccessExp (a',i',r))
	   | a' => ArrayAccessExp (a',expSimpl env i,r))
      | InfixExp (ope,ty,e1,e2) =>
	(case (expSimpl env e1, ope, expSimpl env e2) of
	     (IntExp (i1,_), ADD, IntExp (i2,_)) => checkRange e (IntExp (i1 + i2, ty))
	   | (IntExp (i1,_), SUB, IntExp (i2,_)) => checkRange e (IntExp (i1 - i2, ty))
	   | (IntExp (i1,_), MUL, IntExp (i2,_)) => checkRange e (IntExp (i1 * i2, ty))
	   | (IntExp (i1,_), DIV, IntExp (i2,_)) => checkRange e (IntExp (i1 div i2, ty))
	   | (IntExp (i1,_), MOD, IntExp (i2,_)) => checkRange e (IntExp (i1 mod i2, ty))
	   | (IntExp (i1,_), LE, IntExp (i2,_)) => BoolExp (i1 <= i2)
	   | (IntExp (i1,_), LT, IntExp (i2,_)) => BoolExp (i1 < i2)
	   | (IntExp (i1,_), GE, IntExp (i2,_)) => BoolExp (i1 >= i2)
	   | (IntExp (i1,_), GT, IntExp (i2,_)) => BoolExp (i1 > i2)
	   | (BoolExp true, OR, _) => BoolExp true
	   | (_, OR, BoolExp true) => BoolExp true
	   | (BoolExp false, OR, e2) => e2
	   | (e1, OR, BoolExp false) => e1
	   | (BoolExp true, AND, e2) => e2
	   | (e1, AND, BoolExp true) => e1
	   | (BoolExp false, AND, _) => BoolExp false
	   | (_, AND, BoolExp false) => BoolExp false
	   | (BoolExp false, EQ, e2) => UnopExp(NOT,ty,e2)
	   | (e1, EQ, BoolExp false) => UnopExp(NOT,ty,e1)
	   | (BoolExp true, EQ, e2) => e2
	   | (e1, EQ, BoolExp true) => e1
	   | (e1, EQ, e2) => if isLiteral e1 andalso isLiteral e2 then BoolExp (equal e1 e2) else
			     if equal e1 e2 then BoolExp true else
			     InfixExp (ope,ty,e1,e2)
	   | (e1, NE, e2) => if isLiteral e1 andalso isLiteral e2 then BoolExp (not (equal e1 e2)) else
			     if equal e1 e2 then BoolExp false else
			     InfixExp (ope,ty,e1,e2)
	   | (e1',_,e2') => InfixExp (ope,ty,e1',e2'))
      | BinopExp (ope,qty as QueueType (k,_),q,ee) =>
	(case (ope, expSimpl env q, expSimpl env ee) of
	     (ENQUEUE,QueueExp (el,r),e) =>
	     if List.length el = k then comperror "bad queue operation" else QueueExp (e::el,r)
	   | (APPEND,QueueExp (el,r),e) =>
	     if List.length el = k then comperror "bad queue operation" else QueueExp (el@[e],r)
	   | (_,q',ee') => BinopExp (ope,qty,q',ee'))
      | UnopExp (ope,ty,ee) =>
	(case (ope,expSimpl env ee) of
	     (NOT,BoolExp b) => BoolExp (not b)
	   | (NOT,UnopExp(NOT,ty,e)) => e
	   | (NOT,InfixExp (EQ,ty,e1',e2')) => InfixExp (NE,ty,e1',e2')
	   | (NOT,InfixExp (NE,ty,e1',e2')) => InfixExp (EQ,ty,e1',e2')
	   | (NOT,InfixExp (LT,ty,e1',e2')) => InfixExp (GE,ty,e1',e2')
	   | (NOT,InfixExp (GE,ty,e1',e2')) => InfixExp (LT,ty,e1',e2')
	   | (NOT,InfixExp (GT,ty,e1',e2')) => InfixExp (LE,ty,e1',e2')
	   | (NOT,InfixExp (LE,ty,e1',e2')) => InfixExp (GT,ty,e1',e2')
	   | (PLUS,e) => e
	   | (MINUS, IntExp (n,_)) => checkRange e (IntExp (~n,ty))
	   | (COERCE, IntExp (n,_)) => checkRange e (IntExp (n,ty))
	   | (EMPTY, QueueExp (l,r)) => BoolExp (null l)
	   | (FULL,QueueExp (l,QueueType (k,_))) => BoolExp (List.length l >= k)
	   | (DEQUEUE, QueueExp (l,r)) => (case l of
					       [] => comperror "bad queue operation"
					     | _  => QueueExp (rev (tl (rev l)),r))
	   | (FIRST, QueueExp (l,r)) => (case l of
					     [] => comperror "bad queue operation"
					   | _  => List.last l)
	   | (_,e') => UnopExp (ope,ty,e'))
      | RecordExp (fl,r) => RecordExp (List.map (fn (f,e) => (f,expSimpl env e)) fl, r)
      | ArrayExp (el,r) => ArrayExp (List.map (expSimpl env) el, r)
      | QueueExp (el,r) => QueueExp (List.map (expSimpl env) el, r)
      | CondExp (c,e1,e2) =>
	(case expSimpl env c of
	     BoolExp true => expSimpl env e1
	   | BoolExp false => expSimpl env e2
	   | c' => CondExp (c',expSimpl env e1,expSimpl env e2))
      | LetExp(s,e) => (* ### could be improved ### *)
	(case stSimpl env s of
	     StNull => expSimpl env e
	   | s' => LetExp(s',expSimpl env e))
      | ConstrExp (c,e,ty) => ConstrExp (c,expSimpl env e,ty)
      | DestrExp (c,e) => (case expSimpl env e of
			       ConstrExp (_,e',_) => e'
			     | e' => DestrExp (c,e'))
      | MatchesExp (c,e) => (case expSimpl env e of
				 ConstrExp (c',e',_) => BoolExp (c=c')
			       | e' => MatchesExp(c,e'))
      |  _ => e

and stSimpl env s = (* no StBlock *)
    case s of
	StSequence(s1,s2) => (case (stSimpl env s1, stSimpl env s2) of
				  (StNull,s') => s'
				| (s,StNull) => s
				| ss => StSequence ss)
      | StGuard e => StGuard(expSimpl env e)
      | StAssign(p,e) => StAssign(pattSimpl env p,expSimpl env e)
      | StCond(e,s1,s2) => StCond(expSimpl env e,stSimpl env s1, stSimpl env s2)
      | StWhile(e,s) => StWhile(expSimpl env e,stSimpl env s)
      | StForeach(v,s) => StForeach(v,stSimpl env s)
      | _ => s

and pattSimpl env e = case e of
	  RecordAccessExp (a,f,r) => RecordAccessExp (pattSimpl env a,f,r)
	| ArrayAccessExp (a,i,r) => ArrayAccessExp (pattSimpl env a,expSimpl env i,r)
	| _ => e
;

(* evaluates an expression in context env (total) *)
val expVal = expSimpl;



(* for statement swaps and := expansions: *)

(* variables read or written in a statement/expression/pattern *)
fun readInExp e =
    case e of
	ArrayAccessExp (a,e,_) => readInExp a @ readInExp e
      | RecordAccessExp (r,f,_) => readInExp r
      | VarExp (v,_) => [v]
      | InfixExp (_,_,e1,e2) => readInExp e1 @ readInExp e2
      | BinopExp (_,_,e1,e2) => readInExp e1 @ readInExp e2
      | UnopExp (_,_,e) => readInExp e
      | RecordExp (fl,_) => flat (map (fn (_,e) => readInExp e) fl)
      | ArrayExp (el,_) => flat (map readInExp el)
      | QueueExp (el,_) => flat (map readInExp el)
      | CondExp (c,e1,e2) => readInExp c @ readInExp e1 @ readInExp e2
      | LetExp (s,e) => readInSt s @ readInExp e
      | ConstrExp (c,e,_) => readInExp e
      | DestrExp (c,e) => readInExp e
      | MatchesExp (c,e) => readInExp e
      | _ => []
and readInPatt p =
    case p of
	RecordAccessExp(r,_,_) => readInPatt r
      | ArrayAccessExp(a,i,_) => readInPatt a @ readInExp i
      | _ => []
and readInSt s =
    case s of
	StSequence(s1,s2) => readInSt s1 @ readInSt s2
      | StAssign (p,e) => readInPatt p @ readInExp e
      | StGuard e => readInExp e
      | StCond(c,s1,s2) => readInExp c @ readInSt s1 @ readInSt s2
      | StWhile (c,s) => readInExp c @ readInSt s
      | StForeach (v,s) => readInSt s
      | _ => [];

(* variables written in a statement/expression/pattern *)
fun writtenInExp e = 
    case e of
	ArrayAccessExp (a,e,_) => writtenInExp a @ writtenInExp e
      | RecordAccessExp (r,f,_) => writtenInExp r
      | InfixExp (_,_,e1,e2) => writtenInExp e1 @ writtenInExp e2
      | BinopExp (_,_,e1,e2) => writtenInExp e1 @ writtenInExp e2
      | UnopExp (_,_,e) => writtenInExp e
      | RecordExp (fl,_) => flat (map (fn (_,e) => writtenInExp e) fl)
      | ArrayExp (el,_) => flat (map writtenInExp el)
      | QueueExp (el,_) => flat (map writtenInExp el)
      | CondExp (c,e1,e2) => writtenInExp c @ writtenInExp e1 @ writtenInExp e2
      | LetExp (s,e) => writtenInSt s @ writtenInExp e
      | ConstrExp (c,e,_) => writtenInExp e
      | DestrExp (c,e) => writtenInExp e
      | MatchesExp (c,e) => writtenInExp e
      | _ => []
and writtenInPatt p =
    case p of
	VarExp (v,_) => [v]
      | RecordAccessExp(r,_,_) => writtenInPatt r
      | ArrayAccessExp(a,i,_) => writtenInPatt a @ writtenInExp i
      | _ => []
and writtenInSt s =
    case s of
	StSequence(s1,s2) => writtenInSt s1 @ writtenInSt s2
      | StAssign (p,e) => writtenInPatt p @ writtenInExp e
      | StGuard e => writtenInExp e
      | StCond(c,s1,s2) => writtenInExp c @ writtenInSt s1 @ writtenInSt s2
      | StWhile (c,s) => writtenInExp c @ writtenInSt s
      | StForeach (VarExp(v,_),s) => v :: writtenInSt s
      | _ => [];

