

signature COMPOSE = sig
    val compose : TTS.TTS -> TTS.TTS
end;
 


structure Compose : COMPOSE = struct



(* handling of temporary variables, parameter passing, and simplification:

  step 1 (per declaration):
   - preprocess to sequential transitions, removing all code after StTo and transitions without StTo
   - initialize list of temporary variables to the argument list (those passed by value)

  step 2 (parameter passing):
  in caller component:
    - vars passed as reference are part of state, others are in temporary list
    - expressions passed as parameters can ALWAYS be statically evaluated in caller context
  in callee, whether process or component:
	  if variable is "by ref"
          then replace var name by name of shared var in body
	  else initialize it to the value passed

  step 3 (simplify, after composition):
   - determine which variables in body of process are indeed state variables, then:
     * move local variables not part of state to temporary variables
     * move temp variables part of state to local variables
     * inline variables not part of state and of simple types

  step 4 (emit):
     the temporary variables are created and initialized, but outside of state structure
*)


(* doubles any "_" in name: *)
fun encode id = String.translate (fn #"_" => "__" | c => Char.toString c) id;

(* returns next available integer for id associated with id *)
(* ##### why not using simply unique () ? ###### *)
val aliasInstance =
    let val created = ref ([] : (string * int) list)
	(* unsafe, as "_" legal in Fiacre identifiers ... *)
	fun root ag = hd (String.fields (fn  #":" => true | _ => false) ag)
    in fn id => let val id = root id
		in case List.partition (fn (y,x) => y=id) (!created)  of
		       (nil,neg) => (created := (id,1) :: neg; encode id^"_1")
		     | ((_,h)::_,neg) => (created := (id,h+1) :: neg; encode id^"_"^Int.toString(h+1))
		end
    end;


(* uniquely renames:
     process name, transition tags, variable names (including params passed by value) and states in processes
     component name, variable names (including params passed by value) and ports in components
*)
local
fun renameStm varsMap rename s = (* renames variables and states in a statement *)
    let fun renStm stt =
	    case stt of
		StSelect a1 => StSelect(map renStm a1)
	      | StSequence (a1,a2) => StSequence(renStm a1,renStm a2)
	      | StCond (e,a1,a2) => StCond(aliasvExp varsMap e,renStm a1,renStm a2)
	      | StGuard e => StGuard(aliasvExp varsMap e)
	      | StWhile(a2,a3) => StWhile(aliasvExp varsMap a2,renStm a3)
	      | StForeach(a2,a3) => StForeach(aliasvExp varsMap a2,renStm a3)
	      | StAssign (a2,a3) => StAssign (aliasvExp varsMap a2,aliasvExp varsMap a3) 
	      | StTo sl => StTo (map rename sl)
	      | StInput(st,tyl,pl) => StInput(st,tyl,map (aliasvExp varsMap) pl)
	      | StOutput(p,tyl,expl) => StOutput(p,tyl,map (aliasvExp varsMap) expl)
	      | _ => stt
    in renStm s
    end;
in
fun renameComponent (ProcessDecl (b,c,d,stl,k,f,h,i,init,g)) =
    (* Renames states, all variables and transitions *)
    let val instName = aliasInstance b
	fun renames s = instName ^ "_s" ^ encode s
	fun renamev s = instName ^ "_v" ^ encode s
	val varsMap = List.mapPartial (fn (ValArg v,_) => SOME (v,renamev v) | _ => NONE) d @
		      map (fn (n,_,_) => (n,renamev n)) f
	val transCounter = ref ~1;
	fun renameTrans (([tag],lab),from,stm,b,p) =
	    (transCounter := !transCounter + 1;
	     (([instName^"_t"^Int.toString(!transCounter)],lab),map renames from,renameStm varsMap renames stm,b,p))
    in  ProcessDecl (instName,c,
		     map (fn (ValArg v,ty) => (ValArg (alias v varsMap),ty) | x => x) d,
		     map renames stl,
		     map (fn (s,ty,v) => (alias s varsMap,ty,(* no vars in initial v *) v)) k,
		     map (fn (s,ty,v) => (alias s varsMap,ty,aliasvExp varsMap v)) f,h,i,
		     renameStm varsMap renames init,
		     map renameTrans g)
	handle BYE => raise BYE | _ => failwith "renameComponent"
    end 
  | renameComponent (ComponentDecl (b,c,d,k,e,f,g,init,h)) = 
    (* Renames all variables and local ports. *)
    let val instName = aliasInstance b
	fun renamev s = instName ^ "_v" ^ encode s
	fun renamep s = instName ^ "_" ^ encode s
	val varsMap = List.mapPartial (fn (ValArg v,_) => SOME (v,renamev v) | _ => NONE) d @
		      map (fn (n,_,_) => (n,renamev n)) e
	val localportMap = map (fn (p,_,_) => (p,renamep p)) f
	fun renameComp cp =
	    case cp of
		ParComp scpl => ParComp(map (fn (pl,c) => (map (fn p => alias p localportMap) pl,renameComp c)) scpl) 
	      | InstanceComp(i,pl,el) => InstanceComp(i,map (fn p => alias p localportMap) pl,map (aliasvExp varsMap) el)
    in ComponentDecl (instName,c,
		      map (fn (ValArg v,ty) => (ValArg (alias v varsMap),ty) | x => x) d,
		      map (fn (s,ty,v) => (alias s varsMap,ty,(* no vars in initial v *) v)) k,
		      map (fn (s,ty,v) => (alias s varsMap,ty,aliasvExp varsMap v)) e,
		      map (fn (p,ch,b) => (alias p localportMap,ch,b)) f,
		      map (fn (l1,l2) => (map (fn p => alias p localportMap) l1,map (fn p => alias p localportMap) l2)) g,
		      renameStm varsMap (fn s => s) init,
		      renameComp h)
       handle BYE => raise BYE | _ => failwith "renameComponent"
    end
  | renameComponent d = failwith "renameComponent";
end;



(* where instances are stored *)
val ENV = ref ([] : Declaration list);
fun lookupenv sst = Option.valOf (List.find (fn ProcessDecl (s,_,_,_,_,_,_,_,_,_) => sst = s
					      | ComponentDecl (s,_,_,_,_,_,_,_,_) => sst = s
					      | _ => false) (!ENV));

(* records architecture if -arch *)
val ATREE = ref ([] : string list);
fun print_arch () =
    let fun accu (""::t) = (ATREE := t; [])
	  | accu (h::t) = h :: accu t
	val cl = rev (accu (!ATREE))
	open OutPrint 
    in print_list (""," ","") print cl; print "\n"; flush ()
    end;


local
(* evaluates init statements, returns updated tmp and vars bindings, and StTo (initial state) *)
(* note: typer guarantees that no init statement reads or writes shared variables *)
fun evalInitStatement init env tmp' vars' =
    let val initial = ref []
	val vars' = ref vars'
	val tmp' = ref tmp'
	val store = ref (env @ map (fn (s,_,v) => (s,v)) (!tmp') @ map (fn (v,_,e) => (v,e)) (!vars'))
	exception Break
	fun chopList f [] accu = (rev accu,[])
	  | chopList f (l as (h::t)) accu = if f h then (rev accu,l) else chopList f t (h::accu)
	fun eval e = expVal (!store) e
	fun evalInit s =
	    case s of
		StSequence(s1,s2) => (evalInit s1; evalInit s2)
	      | StCond(c,s1,s2) => (case eval c of BoolExp b => evalInit (if b then s1 else s2))
	      | StTo sl => (initial := sl; raise Break)
	      | StWhile(c,s) => (while case eval c of BoolExp b => b do evalInit s)
	      | StForeach _ => evalInit (ToTTS.unrollForeach s)
	      | StAssign(RecordAccessExp(r,f,ty),e) =>
		let val RecordExp (fl,rty) = eval r
		    val e' = eval e
		in evalInit (StAssign(r,RecordExp (map (fn (f',e) => (f',if f=f' then e' else e)) fl,rty)))
		end
	      | StAssign(ArrayAccessExp(a,i,ty),e) =>
		let val IntExp (k,_) = eval i
		     val ArrayExp (al,aty) = eval a
		     val e' = eval e
		 in evalInit (StAssign(a,ArrayExp(List.tabulate(length al,fn i => if i=k then e' else List.nth (al,i)),aty)))
		 end
	      | StAssign(VarExp(v,_),e) =>
		(case chopList (fn (v',_,_) => v=v') (!vars') [] of
		     (l1,(v,ty,_)::l2) => vars' := l1 @ (v,ty,eval e)::l2
		   | _ => (case chopList (fn (v',_,_) => v=v') (!tmp') [] of
			       (l1,(v,ty,_)::l2) => tmp' := l1 @ (v,ty,eval e)::l2
			     | _ => failwith "unfoldProgram4");
		 store := env @ map (fn (s,_,v) => (s,v)) (!tmp') @ map (fn (v,_,e) => (v,e)) (!vars'))
	      | StGuard _ => failwith "unexpected guard statement in init statement"
	      | StNull => ()
	      | _ => failwith "unexpected statement in init statement"
    in evalInit init handle Break => () | BYE => raise BYE | _ => failwith "evalInitStatement.evalInit";
       (!tmp',!vars',StTo (!initial))
    end
    handle BYE => raise BYE
	 | _ => comperror "cannot evaluate init statement, missing initialization ?"
in
(* flattens composition in main. The result is a component only with process intances *)
(* called once, before instance, in compose *)
(* evaluates initializations and pass parameters through temporary variables or state variables *)
fun unfoldProgram (c as ComponentDecl _) =
    let (* portenv: port renaming
	   shvenv : shvar renaming
	   env    : maps reachable vars (including shared) to values, for evaluations of init statements and parameters passed
	*)
	val _ = if !ARCH then let open OutPrint in print "Component hierarchy:\n"; flush () end else ()
	fun unfoldComponent (ComponentDecl (st,prs,pas,tmp,vars,lprs,prl,init,cp)) portenv shvenv env =
	    let	(* get values of tmp vars in env: *)
		val tmp' = map (fn (v,ty,_) => (v,ty,assoc v env)) tmp
		(* evaluate local vars in env extended with tmp values: *)
		val vars' = 
		    let val env' = env @ map (fn (s,_,v) => (s,v)) tmp'
		    in map (fn (v,ty,e) => (v,ty,expVal env' e)) vars
		    end
		(* evaluate init clause in env'', this may update env' and vars' *)
		val (tmp',vars',initial) = evalInitStatement init env tmp' vars'
		(* updated environment with both tmp and local vars: *)
		val env'' = env @ map (fn (s,_,v) => (s,v)) tmp' @ map (fn (s,_,v) => (s,v)) vars'
		(* initialize cumulated lists for composition *)
		val ltmp = ref tmp' (* tmp variable declarations *)
	        val lvars = ref vars' (* local variable declarations *)
		val lports = ref lprs (* local port declarations *)
		(* unfold composition *)
		fun unfoldComp (ParComp cpl) prio =
			PrioSpec (prio, ParComp (map (fn (x,y) => (map (fn x => alias x portenv) x,unfoldComp y [])) cpl))
		  | unfoldComp (InstanceComp (id,pl,el)) _ = 
			let (* evaluate ports and values passed: *)
			    val pvalues = map (fn x => alias x portenv) pl
			    (* renames shared variables in instance parameters *)
			    val evalues = map (fn RefExp (v,ty) => RefExp (alias v shvenv,ty) | e => expVal env'' e) el
			in case lookupenv id of
				cp as ComponentDecl _ => (* instance est un composant *)
				let val c' as ComponentDecl (n',cprs,cpas,_,_,_,_,_,_) = renameComponent cp
				    val portEnv = map (fn ((id',_),id) => (id',id)) (ListPair.zipEq (cprs,pvalues))
				    val shvEnv = List.mapPartial (fn ((RefArg v',_),RefExp (v,_)) => SOME (v',v)
								   | _ => NONE) (ListPair.zipEq (cpas,evalues))
				    val Env = List.mapPartial (fn ((ValArg v',_),e) => SOME (v',e)
								| ((RefArg v',_),RefExp (v,ty)) => SOME (v',assoc v env'')
								  handle _ => comperror ("shared variable "^v^" must be initialized"))
							      (ListPair.zipEq (cpas,evalues)) @ env''
				    val _ = if !ARCH then ATREE := n' :: !ATREE else ()
                                            (* let open OutPrint
					       in print "  proc "; print n'; print "\n"; flush ()
					       end
					     *)
				    val c'' as ComponentDecl (_,_,_,ctmp,cvars,clprs,_,_,c) = unfoldComponent c' portEnv shvEnv Env
				in (* if !ARCH then print_arch () else (); *)
                                   (* let open OutPrint
				      in print "done \n"; flush ()
				      end;
				    *)
				    ltmp := !ltmp @ ctmp;
				    lvars := !lvars @ cvars;
				    lports := !lports @ clprs;
				    c
				end
			      | cp as ProcessDecl _ => (* instance est un process *)
				let val c' as ProcessDecl (n',cprs,cpas,_,_,_,_,_,_,_) = renameComponent cp
				    val portEnv = map (fn ((id',_),id) => (id',id)) (ListPair.zipEq (cprs,pvalues))
				    val shvEnv = List.mapPartial (fn ((RefArg v',_),RefExp (v,_)) => SOME (v',v)
								   | _ => NONE) (ListPair.zipEq (cpas,evalues))
				    val Env = List.mapPartial (fn ((ValArg v',_),e) => SOME (v',e)
								| ((RefArg v',_),RefExp (v,ty)) => SOME (v',assoc v env'')
								  handle _ => comperror ("shared variable "^v^" must be initialized"))
							      (ListPair.zipEq (cpas,evalues)) @ env''
				    val _ = if !ARCH then ATREE := n' :: !ATREE else ()
                                            (* let open OutPrint
					       in print "  proc "; print n'; print "\n"; flush ()
					       end
					     *)
				    val ProcessDecl (id',_,_,_,_,_,_,_,_,_) = unfoldComponent c' portEnv shvEnv Env
				in (* vars cumulated in makeComposition, rather than here *)
				    InstanceComp (id',[],[])
				end
			end
		fun flattenPrio [] = []
		  | flattenPrio ((l1,l2)::t) =
		    flat (map (fn x => map (fn y => (alias x portenv,alias y portenv)) l2) l1) @ flattenPrio t
		val _ = if !ARCH then ATREE := st :: "" :: !ATREE else ()
                        (* let open OutPrint
                           in print "unfolding component "; print st; print "\n"; flush ()
			   end
			 *)
		val newComp = unfoldComp cp (flattenPrio prl)
	    in 	if !ARCH then print_arch () else ();
		ComponentDecl (st,prs,pas,!ltmp,!lvars,!lports,[],initial,newComp)
	    end
  	  | unfoldComponent (ProcessDecl (cst,_,_,csts,ctmp,cvars,clprs,prl,init,ctrans)) portenv shvenv env =
		let (* renames shared variables in expressions *)
	            fun renameEx e = mapExp (fn VarExp (v,ty) => VarExp (alias v shvenv,ty) | e => e) e
		    fun renameSt s1 = 
		      case s1 of
			StAssign(p,e) => StAssign(renameEx p,renameEx e)
		      | StSelect sl => StSelect(map renameSt sl)
		      | StSequence(a1,a2) => StSequence(renameSt a1,renameSt a2)
		      | StCond (e,a1,a2) => StCond(renameEx e,renameSt a1,renameSt a2)
		      | StGuard e => StGuard(renameEx e)
		      | StWhile(a2,a3) => StWhile(renameEx a2,renameSt a3)
		      | StForeach(v,a3) => StForeach(renameEx v,renameSt a3)
		      | StSignal p => StSignal(alias p portenv)
		      | StInput(p,a3,a4) => StInput(alias p portenv,a3,a4)
		      | StOutput(p,a3,a5) => StOutput(alias p portenv,a3,map renameEx a5)
		      | _ => s1
		   (* initialize temporaries to values passed, evaluate local vars, and update transitions: *)
		   val ctmp' = map (fn (v,ty,_) => (v,ty,assoc v env)) ctmp
		   val vars' = 
		       let val env' = env @ map (fn (s,_,v) => (s,v)) ctmp'
		       in map (fn (v,ty,e) => (v,ty,expVal env' e)) cvars
		       end
		   (* evaluate init clause in env'', this may update env' and vars' *)
		   val (ctmp',vars',initial) = evalInitStatement init env ctmp' vars'
		   val p = ProcessDecl (cst,
					[],
					[],
					csts,
					ctmp',
					vars',
					clprs,
					prl,
					initial,
					map (fn (a,c,d,e,f) => (a,c,renameSt d,e,f)) ctrans)

	        in ENV := p :: !ENV;
		   if !ARCH then (ATREE := cst :: "" :: !ATREE; print_arch ()) else ();
		   p
	        end
    in 
	unfoldComponent c [] [] [] handle BYE => raise BYE | _ => failwith "unfoldProgram"
    end

  | unfoldProgram (d as ProcessDecl _) = (* case main is a process, no more used (dummy component now added in frac-tts) *)
    renameComponent d

  | unfoldProgram d = d;
end;




(* computes composition *)
fun makeComposition (c as ComponentDecl (st,prs,pas,tmp,vars,lprs,pris,init,cp)) =
    let	(* concats two statements, preserving normalized property for sequences *)
	fun joinTrans (s1,StNull) = s1
	  | joinTrans (StNull,s2) = s2
	  | joinTrans (s1,s2) =
	    let fun join s =
		    case s of
			StSequence(s1,s2) => StSequence(s1,join s2)
		      | StNull => s2
		      | _ => StSequence (s,s2)
	    in join s1
	    end handle _ => failwith "joinTrans"
	fun breakTrans s =
	    let (* splits s into (s1,s',s2), where (s1;s';s2)==s and s' is the first st in s satisfying f (StNull if none) *)
		(* applied after normSeq *)
		fun chop f s =
		    let fun toSeq [] = StNull | toSeq [s] = s | toSeq (h::t) = StSequence (h,toSeq t)
			fun rchop s l1 =
			    case s of
				StSequence (s1,s2) => if f s1 then (toSeq (rev l1),s1,s2) else rchop s2 (s1::l1)
			      | s => if f s then (toSeq (rev l1),s,StNull) else (toSeq (rev (s::l1)),StNull,StNull)
		    in rchop s []
		    end
		fun break s =
		    case chop (fn StTo _ => true | _ => false) s of
			(_,StNull,_) => (* no target state *) comperror "transition with no target state" (* (StNull,StNull,StNull,[]) *)
		      | (s1,StTo tg,_) =>
			(case chop (fn StInput _ => true | StOutput _ => true | StInputList _ => true | StSignal _ => true | _ => false) s1 of
			     (ss1,StNull,_) => (* no action *) (ss1,StNull,StNull,tg)
			   | (ss1,act,ss2) => (ss1,act,ss2,tg))
	    in break s
	    end handle BYE => raise BYE | _ => failwith "breakTrans"
	fun label s = case s of StInput(p,_,_) => p | StInputList(p,_) => p | StOutput(p,_,_) => p | StSignal p => p | _ => "";
	(* computes the transitions of a parComposition  e1->p1 || .. || en->pn; in which each pi is a ProcessDecl (set of transitions, recursively)
	   step 1: for each process ei-pi, retain the transitions with labels not in ei
           step 2: for each label l in union(ei):
                     get for each process having l in e the transitions labelled by l ==> [[T1],...[Tn]] : transition list list
                     retain the synchronized product of these transitions (raise error if some list member is empty ?, meaning that that interaction is dead)
                       where the synchronised product is the set {t1 x ... x tn | ti in Ti}
	   return the union of transitions kept at steps 1 and 2, encapsulated in a process whose states and local vars are updated
	 *)
	fun composeBlock (ParComp cpl) prios = 
	    let	(* compose each member of cpl, recursively *)
		val procList = map (fn (x,y) => (x,composeBlock y [])) cpl
		(* expose content of processes, with their transitions broken *)
		val broken = map (fn (pl, ProcessDecl (_,_,_,_,_,_,_,_,_,trl)) =>
				     (pl,map (fn ((tgl,_),fr,s,bn,pr) => let val br as (_,act,_,_) = breakTrans s
									 in (((tgl,label act),fr,s,bn,pr),br)
									 end) trl)) procList
   		(* step 1 : find union of transitions non synchronized: *)
		(* transition are rebuilt so that unreachable statements (after "to") are removed *)
		val notsynched =
		    flat (map (fn (pl,trl) => List.mapPartial (fn ((tlb as (tag,lb),fr,_,bn,pr),(bef,act,aft,tg)) =>
								  if mem lb pl then NONE
								  else SOME (tlb,fr,joinTrans(bef,joinTrans(act,joinTrans(aft,StTo tg))),bn,pr)) trl) broken)
		(* step 2 : *)
		(* all labels in port lists *)
		val labels = makeset (flat (map fst procList))
		(* transitions synchronizing on label lab : *)
		fun sync lab =
		    let	(* get transitions synchronizing on lab: *)
			val trset = List.mapPartial (fn (pl,trl) => if mem lab pl
								    then SOME (case List.filter (fn (((_,lb),_,_,_,_),(_,act,_,_)) => lb=lab) trl of
										   [] => comperror ("no interactions on synchronized port "^lab)
										 | l => l)
								    else NONE)
						    broken
			(* statement list -> sequence statement *)
			fun mkSeq [] = StNull | mkSeq [s] = s | mkSeq (h::t) = StSequence (h,mkSeq t)
			(* builds communication statement *)
			(* returns Output if present in interaction, required to reject open input in removePorts *)
			fun mkComm (OutSignal,InSignal) =
			    case (OutSignal,InSignal) of
				(StOutput(st1,pr11,expl1),StInput(st2,pr21,stl2)) =>
				let val assList = map (fn (x,y) => ToTTS.convAssignMatch (StAssign(x,y))) (ListPair.zip (stl2,expl1))
				in (OutSignal,List.foldr StSequence StNull assList)
				end
			      | (StOutput(st1,pr11,expl1),StInputList(st2,stl2)) =>
				let val assList = flat (map (fn stList => map (fn (x,y) => ToTTS.convAssignMatch(StAssign(x,y))) (ListPair.zip (stList,expl1))) stl2)
				in (OutSignal,List.foldr StSequence StNull assList)
				end
			      | (StInput(st2,pr21,stl2),StOutput(st1,pr11,expl1)) =>
				let val assList = map (fn (x,y) => ToTTS.convAssignMatch(StAssign(x,y))) (ListPair.zip (stl2,expl1))
				in (InSignal,List.foldr StSequence StNull assList)
				end
			      | (StInputList(st2,stl2),StOutput(st1,pr11,expl1)) =>
				let val assList = flat (map (fn stList => map (fn (x,y) => ToTTS.convAssignMatch(StAssign(x,y))) (ListPair.zip (stList,expl1))) stl2)
				in (InSignal,List.foldr StSequence StNull assList)
				end
			      | (StOutput(st1,pr11,expl1),StOutput(st2,pr21,expl2)) =>
				let val condList as hc::tc = map (fn (x,y) => InfixExp(EQ,BoolType,x,y)) (ListPair.zip (expl2,expl1))
				in (OutSignal,StGuard (List.foldr (fn (x,y) => InfixExp(EQ,BoolType,x,y)) hc tc))
				end
			      | (StInput(st1,pr11,stl1),StInput(st2,pr12,stl2)) => (StInputList(st1,[stl1]@[stl2]),StNull)
			      | (StInputList(st1,stl1),StInput(st2,pr21,stl2)) => (StInputList(st1,stl1@[stl2]),StNull)
			      | (StInput(st2,pr21,stl2),StInputList(st1,stl1)) => (StInputList(st1,stl1@[stl2]),StNull)
			      | (StInputList(st2,stl2),StInputList(st1,stl1)) => (StInputList(st1,stl1@stl2),StNull)
			      | (StSignal _,StSignal _) => (OutSignal,StNull)
			      | _ => failwith "makeComposition.mkComm"
			(* synchonizes transitions *)
			fun synctuple tuple =
			    let fun unzip [((tagslab,from,_,bnd,prl),x)] = ((tagslab,from,bnd,prl),x)
				  | unzip ((((tag,lab),from,_,bnd,prl),(bef,act,aft,tgt))::t) =
				    let val (((tag',_),from',bnd',prl'),(bef',act',aft',tgt')) = unzip t
					val (comm,more) = mkComm(act,act')
				    in (*
					composing priorities:
					composing tr  <prl>
                                        with      tr' <prl'>
				        =>        tr__tr' <all transitions in composition involving one in prl @
				        all transitions in composition involving one in prl'>
					*)
					(((tag' @ tag,lab),from @ from',bnd,prl @ prl'),(joinTrans(bef,joinTrans(bef',more)),comm,joinTrans(aft,aft'),tgt @ tgt'))
				    end
				val ((tagslab,from,bnd,prl),(bef,act,aft,tgt)) = unzip tuple
			    in (tagslab,from, joinTrans (bef,joinTrans (act, joinTrans (aft,StTo tgt))),bnd,prl)
			    end
			fun tuples [x] = map (fn z => [z]) x 
			  | tuples (h::t) = flat (map (fn tp => map (fn x => x::tp) h) (tuples t))
			  | tuples [] = []
		    in map synctuple (tuples trset)

		    end
		(* reports priorities; does NOT build closure:
		 foreach p>q :
		 let e = transitions labelled q
		 in foreach transition labelled p:
		    add set e to its "lower" list
		 end
		 *)
		fun insertPrios [] trl = trl
		  | insertPrios ((p,q)::pl) trl =
		    insertPrios pl (
		                    case flat (List.mapPartial (fn tr as ((tags,lab),_,_,_,_) => if lab=q then SOME tags else NONE) trl) of
					[] => trl
				      | qtags => map (fn tr as ((tags,lab),fr,s,bn,pr) =>
							 if lab = p then ((tags,lab),fr,s,bn,makeset (pr @ qtags)) else tr) trl)
	    in ProcessDecl (let val h::t = map (fn (_,ProcessDecl (st,_,_,_,_,_,_,_,_,_)) => st) procList
			    in List.foldl (fn (x,y) => x ^ "_" ^ y) h t
			    end,
			    [],
			    [],
			    rev (flat (map (fn (_,ProcessDecl (_,_,_,s,_,_,_,_,_,_)) => s) procList)),
			    flat (map (fn (_,ProcessDecl (_,_,_,_,v,_,_,_,_,_)) => v) procList),
			    flat (map (fn (_,ProcessDecl (_,_,_,_,_,v,_,_,_,_)) => v) procList),
			    flat (map (fn (_,ProcessDecl (_,_,_,_,_,_,lp,_,_,_)) => lp) procList),
			    [],
			    StTo (rev (flat (map (fn (_,ProcessDecl (_,_,_,_,_,_,_,_,StTo s,_)) => s) procList))),
			    insertPrios prios (flat (map sync labels) @ notsynched))
	    end

	  | composeBlock (PrioSpec (prs,comp)) _ = composeBlock comp prs

	  | composeBlock (InstanceComp (sst,_,_)) _ = lookupenv sst

	val ProcessDecl (tag,p,a,s,k,v,x,_,init,trl) = composeBlock cp [] handle BYE => raise BYE | _ => failwith "composition"

    in ProcessDecl (tag,p,a,s,k @ tmp,v @ vars,x @ lprs,[],init,trl)
    end

(* (dummy main component now added in frac-precompose, hence main is always a component)
  | makeComposition (d as ProcessDecl (tag,p,a,s,k,v,x,y,trl)) =
    (* no compositions, but must check akk transitions have some target state (done by breakTrans in above) *)
    (* ######### SHOULD SIMPLIFY TOO, and detect dead trans and ..... ######### *)
    let fun checkTarget (_,_,s,_) =
	    if matchStm (fn StTo _ => true | _ => false) s then () else comperror "no target statement in transition"
    in List.app checkTarget trl;
       d
    end
 *)

  | makeComposition d = d;



fun compose (Program ss) =
    let val main = let val Main c = List.last ss in c end
	fun name (ComponentDecl (n,_,_,_,_,_,_,_,_)) = n
	  | name (ProcessDecl (n,_,_,_,_,_,_,_,_,_)) = n
	  | name _ = ""
    in ENV := ss;
       let val r = unfoldProgram (lookupenv main)
       in if !NOCOMP (* debug mode -nocomp, don't compose, retains instances only *)
	  then Program (List.filter (fn x => case name x of "" => false | n => not (List.exists (fn y => n = name y) ss)) (!ENV) @ [r])
	  else Program [makeComposition r]
       end
    end
    handle BYE => raise BYE | _ => failwith "compose";

end; (* Compose *)

