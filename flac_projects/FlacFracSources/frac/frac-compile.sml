
signature COMPILE = sig
val compile : Fiacre.Fiacre -> unit
end;

structure Compile : COMPILE = struct

fun compile p = 
    (printVerbose "::Compilation to tts\n";
     let val result =
	     (Emit.emit o 
	      PostCompose.postcompose o 
	      Compose.compose o 
	      ToTTS.toTTS) p
     in printVerbose "::Done\n";
	result
     end);

end

