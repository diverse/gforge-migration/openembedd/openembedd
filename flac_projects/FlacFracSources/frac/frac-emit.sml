(*
applied before emit, normalizes the types of state variables,
introduicing the necessary type declarations;
- interval and nat types replaced by int;
- unions, records and queues are named (but not arrays);
*)

signature NAME = sig
  val nameTypes : TTS -> Declaration list * TTS
end

structure Name :> NAME = struct

(* utilities *)

(* lexicographic list comparisons *)
fun rcompare _ [] (_ :: _) = LESS
  | rcompare _ (_ :: _) [] = GREATER
  | rcompare cmp (h1 :: t1) (h2 :: t2) = (case cmp (h1,h2) of EQUAL => rcompare cmp t1 t2 | b => b)
  | rcompare _ [] [] = EQUAL;

(* used to creates new names for types and channels *)
val newName =
    let val no = ref 0
    in fn prefix =>
           let val n = prefix ^ Int.toString (!no)
           in no := 1 + !no;
              n
           end
    end;

(* holds processed declarations *)
val DECLS = ref ([] : Declaration list);


(* for types ---------------------------------------------------------------------------------- *)

(* type keyed avl of strings *)
structure typeKey : KEY = struct
type key = Type;
fun level ty = case ty of
	  BoolType => 1
	| IntType => 2
	| NamedType _ => 3
	| EnumType _ => 4
	| UnionType _ => 5
	| RecordType _ => 6
	| ArrayType _ => 7
	| QueueType _ => 8
        | _ => (* unexpected type *) 10;
fun compare (x,y) = case Int.compare (level x,level y) of
	  EQUAL => (case (x,y) of
		(NamedType s1, NamedType s2) => String.compare (s1,s2) 
	      | (EnumType cl1, EnumType cl2) =>	rcompare (fn (c1,c2) => String.compare (c1,c2)) cl1 cl2
	      | (UnionType ctyl1, UnionType ctyl2) =>
		rcompare (fn ((c1,ty1),(c2,ty2)) =>
			     case String.compare (c1,c2) of
				 EQUAL => (case (ty1,ty2) of
					       (NONE,NONE) => EQUAL
					     | (NONE,_) => LESS
					     | (_,NONE) => GREATER
					     | (SOME ty1,SOME ty2) => compare(ty1,ty2))
			       | b => b)
			 ctyl1
			 ctyl2
	      | (RecordType ftyl1, RecordType ftyl2) =>
		rcompare (fn ((f1,ty1),(f2,ty2)) => case String.compare (f1,f2) of EQUAL => compare(ty1,ty2) | b => b) ftyl1 ftyl2
	      | (ArrayType (sz1,ty1), ArrayType (sz2,ty2)) =>
		(case Int.compare (sz1,sz2) of EQUAL => compare (ty1,ty2) | b => b)
	      | (QueueType (sz1,ty1), QueueType (sz2,ty2)) =>
		(case Int.compare (sz1,sz2) of EQUAL => compare (ty1,ty2) | b => b)
	      | _ => EQUAL)
	| v => v;

fun x < y = case compare (x,y) of LESS => true | _ => false;
fun x > y = case compare (x,y) of GREATER => true | _ => false;
end;
structure tyAvlTree = avlTreef (typeKey);

(* names avl : type -> name, if any *)
val TYNAMES = tyAvlTree.newTree() : String.string tyAvlTree.tree ref;
fun insertType ty n = TYNAMES := tyAvlTree.insert (ty,n,!TYNAMES);
fun lookupType ty = tyAvlTree.getData (ty,!TYNAMES);

(* creates a name for a type and its constituents, making sure names are new *)
fun nameType pref ty =
    (* if ty is named, return its name, otherwise generate a new name and stores it *)
    case lookupType ty of 
	SOME n => NamedType n
      | _ => let val n = newName pref
	     in DECLS := TypeDecl (n,ty) :: !DECLS;
		insertType ty n;
		NamedType n
	     end;

(* returns alias for ty, unless is a constant type *)
(* constants have been computed, and names expanded *)
fun aliasType ty =
    case ty of
	NatType => IntType
      | IntervalType _ => IntType
      | EnumType cl => nameType "e" ty
      | UnionType ctyl => nameType "u" (UnionType (map (fn (f,SOME ty) => (f,SOME (aliasType ty)) | co => co) ctyl))
      | RecordType ftyl => nameType "r" (RecordType (map (fn (f,ty) => (f,aliasType ty)) ftyl))
      | ArrayType (sz,ety) => ArrayType (sz,aliasType ety)
      | QueueType (sz,ety) => nameType "q" (QueueType (sz,aliasType ety))
      | _ => ty;

(* names types in embedded StBlocks *)
fun nameInExp e =
    case e of
 	InfixExp(EQ,ty,e1,e2) => InfixExp(EQ,aliasType ty,nameInExp e1,nameInExp e2)
      | InfixExp(NE,ty,e1,e2) => InfixExp(NE,aliasType ty,nameInExp e1,nameInExp e2)
      | InfixExp(f,ty,e1,e2) => InfixExp(f,ty,nameInExp e1,nameInExp e2)
      | BinopExp(f,ty,e1,e2) => BinopExp(f,ty,nameInExp e1,nameInExp e2)
      | UnopExp(f,ty,e) => UnopExp(f,ty,nameInExp e)
      | RecordExp(fl,ty) => RecordExp(map (fn (f,e) => (f,nameInExp e)) fl,ty)
      | ArrayExp(el,ty) => ArrayExp(map nameInExp el,ty)
      | QueueExp(el,ty) => QueueExp(map nameInExp el,ty)
      | ArrayAccessExp(e1,e2,ty) => ArrayAccessExp(nameInExp e1,nameInExp e2,ty)
      | RecordAccessExp(e,f,ty) => RecordAccessExp(nameInExp e,f,ty)
      | CondExp(c,e1,e2) => CondExp(nameInExp c,nameInExp e1,nameInExp e2)
      | ConstrExp(c,e,ty) => ConstrExp(c,nameInExp e,ty)
      | DestrExp (c,e) => DestrExp (c,nameInExp e)
      | MatchesExp(c,e) => MatchesExp(c,nameInExp e)
      | LetExp(s,e) => LetExp(nameInSt s,nameInExp e)
      | _ => e
and nameInSt s =
    case s of
	StGuard e => StGuard (nameInExp e)
      | StBlock(dl,s) => StBlock (map (fn (s,ty) => (s,aliasType ty)) dl, nameInSt s)
      | StSequence(s1,s2) => StSequence(nameInSt s1, nameInSt s2)
      | StWhile(e,s) => StWhile(nameInExp e, nameInSt s)
      | StForeach(v,s) => StForeach(v, nameInSt s)
      | StCond(e,s1,s2) => StCond(nameInExp e, nameInSt s1, nameInSt s2)
      | StAssign(p,e) => StAssign(nameInExp p,nameInExp e)
      | _ => s;

(* names types in declarations and in constants (so that enum/union declarations are not lost!) *)
fun nameTypes (Program dl) =
    let val dl' = map (fn ProcessDecl (b,c,d,e,kl,vl,f,g,init,h) =>
			  let val kl' = map (fn (s,ty,e) => (s,aliasType ty,nameInExp e)) kl
			      val vl' = map (fn (s,ty,e) => (s,aliasType ty,nameInExp e)) vl
			      val h' = List.map (fn (tg,fr,s,b,p) => (tg,fr,nameInSt s,b,p)) h
			  in ProcessDecl (b,c,d,e,kl',vl',f,g,init,h')
			  end
			| d => d) dl
    in (rev (!DECLS),Program dl')
    end;

end;






(* prints the abstract tts by Prn *)
functor TtsEmit (Prn : PRN) = struct

open Prn;

fun newline n = if n<0 then () else (print "\n"; repeat n print " ");

fun print_Fiacre (Program dl) =
	List.app (fn
		  TypeDecl d => print_TypeDecl 0 d
	     (* | ChannelDecl d => print_ChannelDecl 0 d *)
	        | ProcessDecl d => print_Process 0 d
	        | ComponentDecl d => print_Component 0 d
		| Main d => (newline 0; print d)) dl
and print_TypeDecl c (a,b) =
	(newline c; print "type "; print a; print " is "; print_Type b; newline 0)
(*
and print_ChannelDecl c (l,a,b) =
	(newline c; print "channel "; print a; print " is "; print_Channel b; newline 0)
*)
and print_Type ty = (case ty of
	  BoolType => print "bool"
	| NatType => print "nat"
	| IntType => print "int"
	| NamedType a => print a
	| IntervalType(a,b) => (printi a; print ".."; printi b)
	| EnumType a => print_list ("enum "," | "," end") print a  (* no enums shown *)
	| UnionType a => print_list ("union "," | "," end") (fn (xl,NONE) => print xl
							      | (xl,SOME y) => (print xl; print " of "; print_Type y)) a
	| RecordType a 	=> print_list ("record ",", "," end") (fn (xl,y) => (print xl; print ":"; print_Type y)) a
	| ArrayType (e,b) => (print "array "; printi e; print " of "; print_Type b)
	| QueueType (e,b) => (print "queue "; printi e; print " of "; print_Type b))
and print_Channel tyl =
        case tyl of [] => print "none" | _ => print_list (""," # ","") print_Type tyl
and print_Process i (a,b,d,sd,k,f,h,g,init,j) =
	(newline 0; print "process "; print a;
	 case b of [] => () | _ => (newline 0; newline (i+4); print_list ("[",", ","]") print_PortDecl b);
	 case d of [] => () | _ => (newline 0; newline (i+4); print_list ("{",", ","}") print_VarParam d);
	 newline (i+4); print "is";
	 newline 0; newline (i+4); print_StatesDecl sd;
	 case k of [] => () | _ => (newline 0; newline (i+4); print_list ("tmp ",", ","") print_VarDecl k);
	 case f of [] => () | _ => (newline 0; newline (i+4); print_list ("var ",", ","") print_VarDecl f);
	 case h of [] => () | _ => (newline 0; newline (i+4); print_list ("port ",", ","") print_locPortDecl h);
	 case g of [] => () | _ => (newline 0; newline (i+4); print_priorities g);
	 case init of StNull => () | StTo [] => () | _ => (newline 0; newline (i+4); print "init "; print_Statement (i+4) init);
	 app (print_Transition (i+4)) j;
	 newline 0)
and print_Component i (a,b,d,k,e,f,g,init,h) =
	(newline 0; print "component "; print a;
	 case b of [] => () | _ => (newline (i+4); print_list ("[",", ","]") print_PortDecl b);
	 case d of [] => () | _ => (newline (i+4); print_list ("{",", ","}") print_VarParam d);
	 newline (i+4); print "is";
	 case k of [] => () | _ => (newline (i+4); print_list ("tmp ",", ","") print_VarDecl k);
	 case e of [] => () | _ => (newline (i+4); print_list ("var ",", ","") print_VarDecl e);
	 case f of [] => () | _ => (newline (i+4); print_list ("port ",", ","") print_locPortDecl f);
	 case g of [] => () | _ => (newline 0; newline (i+4); print_priorities g);
	 case init of StNull => () | StTo [] => () | _ => (newline 0; newline (i+4); print "init "; print_Statement (i+4) init);
	 print_Composition (i+4) h false;
	 newline 0)
and print_priorities [] = ()
  | print_priorities l = print_list ("priority ",", ","")
				    (fn (l1,l2) => (print_list (""," ","") print l1; print " > "; print_list (""," ","") print l2)) 
				    l
and print_locPortDecl (name,ch,(left,right)) =
	(print name; print ":";
	 print_Channel ch;
	 print_TimeConstraint (left,right))
and print_Composition i c x = (case c of (* x true if starts at beginning of line *)
	  ParComp cl  => (if x then () else newline i; print "par";
			  let val (pl,h)::t = cl
			      val no = ref false
			  in newline (i+3);
			     case pl of [] => no:=true | _ => (print_list ("",",","") print pl; print " -> ");
			     print_Composition (i+3) h (!no);
			     List.app (fn (pl,c) =>
					  (newline i; print "|| ";
					   case pl of [] => no:=true | _ => (print_list ("",",","") print pl; print " -> ");
					   print_Composition (i+3) c (!no))) t
			  end;
			  newline i; print "end")
	| InstanceComp c => print_InstanceComp i c
	| PrioSpec (prl,c) => (newline i; print "prio "; print_list ("",", ","") (fn (x,y) => (print x; print " > "; print y)) prl;
				  newline i; print_Composition i c x))
and print_InstanceComp i (id,pl,al) =
	(print id;
	 if null pl then () else print_list (" [",",","]") print pl;
	 if null al then () else print_list (" (",",",")") print_Exp al)
and print_PortDecl (name,ch) =
	(print name; print ":"; print_Channel ch)
and print_arg (ValArg s) = print s | print_arg (RefArg s) = print s
and print_VarParam (name,ty) =
	(print_arg name; print ":"; print_Type ty)
and print_VarDecl (a,b,c) =
	(print a; print ": "; print_Type b; print " := "; print_Exp c)
and print_StatesDecl a =
	(print_list ("states ",", ","") print a)
and print_TimeConstraint (left,right) =
        if case (left,right) of (Closed d,Infinite) => d <= 0.0 | _ => false then () else
	(print " in "; print_Interval (left,right))
and print_Interval (left,right) =
	(case left of Open b => (print "]"; print (Real.toString b))
		    | Closed b => (print "["; print (Real.toString b));
	 print ",";
	 case right of Open b => (print (Real.toString b); print "[")
		    | Closed b => (print (Real.toString b); print "]")
		    | Infinite => print "...[")
and print_Transition i ((tags,lab),b,c,bnd,prl) =
	(newline 0; newline i; print "Trans::";
	 print (String.concatWith "__" tags); case lab of "" => () | _ => print (":" ^ lab);
	 newline (i+2); print "from "; print_list (""," ","") print b; print " "; print_Statement (i+3) c;
	 newline (i+2); print "in "; print_Interval bnd;
	 case prl of [] => () | _ => (newline (i+2); print "> "; print_list (""," ","") print prl))
and print_Statement e st = case st of
	  StNull => (newline e; print "null")
	| StGuard a => (newline e; print "IF "; print_Exp a)
	| StCond(a,b,c) =>
		(newline e; print "if "; print_Exp a; print " then "; print_Statement (e+3) b;
		 newline e; print " else ";  print_Statement (e+3) c;
		 newline e; print " end /* if */")
	| StSelect (a::t) =>
		(newline e; print "select";
		 print_Statement (e+3) a;
		 List.app (fn y => (newline e; print "[]"; print_Statement (e+3) y)) t;
		 newline e; print "end /* select */")
	| StSequence(a,b) => (print_Statement e a; print "; "; print_Statement e b)
	| StSignal a => (newline e; print a)
	| StInput (a,r,b) =>
		(newline e; print a; print_pdetails r; print "? "; print_list ("",",","") print_Patt b)
	| StInputList (a,b) =>
		(print a; print_list ("[","-","]") (fn y => print_list ("",",","") print_Exp y) b)
  	| StOutput (a,r,b) =>
		(newline e; print a; print_pdetails r; print "! "; print_list ("",", ","") print_Exp b)
	| StAssign (a,b) =>
		(newline e; print_Patt a; print " := "; print_Exp b)
	| StWhile(a,b) => (newline e; print "while "; print_Exp a; print " do "; 
			     print_Statement (e+3) b; 
			     newline e; print " end /* while */")
	| StForeach(a,b) => (newline e; print "foreach "; print_Exp a; print " do "; 
			     print_Statement (e+3) b; 
			     newline e; print " end /* foreach */")
	| StTo a => (newline e; print "to "; print_list (""," ","") print a)
	| StBlock (dl,s) => (newline e; print "block {"; 
			     map (fn (v,ty) => (newline (e+2); print v; print " : "; print_Type ty; print "; ")) dl;
			     print_Statement (e+2) s;
			     newline e; print "} /* block */")
	| _ => failwith "unexpected statement in print_Statement"
and print_Patt e =
        let val d = !DETAILS in DETAILS := false; print_Exp e; DETAILS := d end
and print_Exp e = case e of
	  VarExp (s,r)          => (print s; print_details r)
	| EnumExp (s,r)         => (print s; print_details r)
	| ConExp (s,r)          => (print s; print_details r)
	| ArrayAccessExp(a,b,_) => (print_Exp a; print "["; print_Exp b; print "]")
 	| RecordAccessExp(a,b,_) => (print_Exp a; print "."; print b)
	| IntExp (n,r)          => (printi n; print_details r)
	| BoolExp b             => print (Bool.toString b)
	| InfixExp(x,r,y,z)     => (print "("; print_Exp y; print " "; print_Infix x; print_details r; print " "; print_Exp z; print ")")
	| BinopExp(x,r,y,z)	=> (print_Binop x; print_details r; print "("; print_Exp y; print ","; print_Exp z; print ")")
	| UnopExp(a,r,b)        => (print_Unop a; print_details r; print "("; print_Exp b; print ")")
	| ConstrExp(c,e2,r)     => (print c; print "("; print_Exp e2; print_details r; print ")")
	| RecordExp (fl,r)      => let val b = !DETAILS
				   in DETAILS := false; print_list ("{",",","}") (fn (f,l) => (print f; print "="; print_Exp l)) fl;
				      DETAILS := b; print_details r
				   end
	| ArrayExp (el,r)       => let val b = !DETAILS
				   in DETAILS := false; print_list ("[",",","]") print_Exp el; DETAILS := b; print_details r
			           end
	| QueueExp (el,r)   	=> (print_list ("{|",",","|}") print_Exp el; print_details r)
	| CondExp (c,e1,e2)     => (print "("; print_Exp c; print ") ? ("; print_Exp e1; print ") : ("; print_Exp e2; print ")")
	| RefExp (v,r)          => (print "&"; print v; print_details r)
        | LetExp(s,e)           => (print "({"; print_Statement (~1000) s; print "; "; print_Exp e; print "})")
	| DestrExp (c,e)        => (print "Destr ("; print_Exp e; print ")")
	| MatchesExp (c,e)      => (print "Matches ("; print c; print ","; print_Exp e; print ")")
	| _                     => failwith "unexpected exp in print_Exp"
and print_details ty =
	if !DETAILS then (print "::"; print_Type ty) else ()
and print_rdetails tyl =
	if !DETAILS then (print "::"; print_list (""," * ","") print_Type tyl) else ()
and print_pdetails ty =
	if !DETAILS then (print "::"; print_Channel ty) else ()
and print_Infix ope = print (case ope of
	  AND => "and"	| OR  => "or"	| LE  => "<="	| LT  => "<"	| GT  => ">"	| GE  => ">="
	| EQ  => "="	| NE  => "<>"	| ADD => "+"	| SUB => "-"	| MOD => "%"	| MUL => "*"	| DIV => "/")
and print_Binop ENQUEUE = print "enqueue" | print_Binop APPEND = print "append"
and print_Unop ope = print (case ope of
	  COERCE => "$" | NOT => "not" | FULL => "full" | EMPTY => "empty" | DEQUEUE => "dequeue" | PLUS => "+" | MINUS => "-" | FIRST => "first")
;

fun emitTts s0 = (print_Fiacre s0; newline 0; flush ()) handle BYE => raise BYE | _ => failwith "emit tts";

end;

structure OutTtsEmit = TtsEmit(OutPrint);
structure ErrTtsEmit = TtsEmit(ErrPrint);




(* prints the TTS Net by Prn *)
functor NetEmit (Prn : PRN) = struct

open Prn;

fun newline n = if n<0 then () else (print "\n"; repeat n print " ");

fun print_Fiacre (Program dl) =
    app (fn ProcessDecl d => print_Process 0 d | _ => ()) dl
					       
and print_Process i (a,_,_,b,_,_,_,d,StTo e,c) =
	(newline i; print "net process_"; print a;
         newline i; newline i; print_StatesDecl (b,e);
	 app (print_Transition i) c;
	 app (print_prior i) c;
	 newline i)

(* each state in initial state is a marked place *)
(* ##### also prints other states in case no transition refers to it. wise ? ##### *)
and print_StatesDecl (a,b) =
    List.app (fn s => (print "pl "; print s; if mem s b then print " (1)" else print " (0)"; print "\n")) a

and print_Interval (left,right) =
    let fun printb b = print (Int.toString (Real.trunc (b * (pow (10.0,!DIGITS)))))
    in case left of
	   Open b => (print "]"; printb b)
	 | Closed b => (print "["; printb b)
	 | Infinite => print "]w";
       print ",";
       case right of
	   Open b => (printb b; print "[")
	 | Closed b => (printb b; print "]")
	 | Infinite => print "w["
    end

and print_Transition i (([tag],lab),from,c,interv,_) =
	(newline i; print "tr "; print tag; print " "; case lab of "" => () | _ => (print ":"; print lab);
	 print_Interval interv;
	 print_list (" "," ","") print from;
	 print " -> ";
	 print_list (""," ","") print (get_target c))

(* gets target state *)
and get_target st =
    case st of
	StSequence(stm1,stm2) => get_target stm1 @ get_target stm2
      | StTo stl => stl
      | _ => []

and print_prior i (_,_,_,_,[] ) = ()
  | print_prior i (([tg],_),_,_,_,prl) = (newline i; print "pr "; print tg; print " > "; print_list (""," ","") print prl)

fun emitFiacre s0 = (print_Fiacre s0; newline 0; flush ()) handle BYE => raise BYE | _ => failwith "emit net";
   
end;




(* prints the TTS C code by Prn *)
functor CEmit (Prn : PRN) = struct

open Prn;

fun newline n = if n<0 then () else (print "\n"; repeat n print " ");

(* type environment *)
val TYPES = ref ([] : (string * Type) list);
fun getBoundType s = assoc s (!TYPES);

(* holds temporary variables *)
(* #### could be avoided if temp variables has a specific constructor introduced by simplifier (e.g. TmpVarExp) #### *)
val TEMP = ref ([] : string list);

(* for tmp variable generation *)
(* q, e, r, a, c used in postcompose, with their own numbering; avoid them here *)
(* j, z, used here; i avoided because i386 is typically used in library headers ... *)
val new = let val UNIQUE = ref 0
	  in fn s => let val u = !UNIQUE
		     in UNIQUE := 1 + !UNIQUE;
			let val v = s ^ Int.toString u
			in TEMP := v :: !TEMP;
			   v
			end
		     end
	  end;


fun print_Fiacre (typeDecs,Program [ProcessDecl (a,_,_,_,k,f,_,_,_,j)]) =
    (newline 0; newline 0; print "/* Process "; print a; print " */";
     newline 0; newline 0; print "static long avail;    /* for sprint_* */";
     newline 0; print "static char tmpbuff[16]; /* for sprint_* */";
     TYPES := map (fn TypeDecl (s,ty) => (s,ty)) typeDecs;
     app print_TypeDecl typeDecs;
     TEMP := map (fn (v,_,_) => v) k;
     print_tmpValueDecl f k;
     (* creates value type, compare_value, sprint_value: *)
     newline 0; print "/* state type */";
     print_TypeDecl (TypeDecl ("value",RecordType (map (fn (y,ty,_) => (y,ty)) f)));
     newline 0; newline 0; print "  /* called by store when some value equal to v is already stored */";
     newline 0; print "void free_value (struct value *v) {";
     newline 0; print "  free(v);";
     newline 0; print "  }";
     print_initialFunc f k;
     print_transFunc j;
     print_indepFunc f;
     print_obsFunc f;
     newline 0)

(* emit type declarations for named types *)
and print_TypeDecl (TypeDecl (a,ty)) =
    (print "\n\n"; print_Type ty a; print ";";
     print_compareFunc (a,ty);
     print_sprintFunc (a,ty))

(* prints compare_a, sprint_a functions for type a *)
and print_compareFunc (a,ty) =
    let fun compare i (NamedType n) e1 e2 = (newline i; print ("{int r = compare_"^n^"("^e1^","^e2^"); if (r) return r;}"))
	  | compare i (ty as ArrayType _) e1 e2 =
	    let fun dims (ArrayType (s,t)) accu = dims t (s::accu)
		  | dims ty accu = (rev accu,ty)
		val (dm,ty) = dims ty []
		val ivars = map (fn _ => new "j") dm
	    in newline i; print "{int "; print_list ("",", ",";") print ivars;
	       let fun printdims i [] x y = compare i ty x y
		     | printdims i ((j,dm)::t) x y =
		       (newline (i+2); print ("for ("^j^"=0;"^j^"<"^Int.toString dm^";"^j^"++) {");
			printdims (i+4) t (x^"["^j^"]") (y^"["^j^"]");
			newline (i+2); print "}")
	       in printdims i (ListPair.zip(ivars,dm)) e1 e2
	       end;
	       newline i; print "}"
	    end
	  | compare i BoolType e1 e2 = (newline i; print ("if (!("^e1^") && ("^e2^")) return -1;");
					newline i; print ("if (("^e1^") && !("^e2^")) return 1;"))
	  | compare i _ e1 e2 = (newline i; print ("if (("^e1^") < ("^e2^")) return -1;");
				 newline i; print ("if (("^e1^") > ("^e2^")) return 1;"))
    in case String.sub(a,0) of
	   #"e" => (* enum *)
	   (newline 0; newline 0; print "int compare_"; print a; print " (enum "; print a; print " e1, enum "; print a; print " e2) {";
	    newline 2; print "if (e1 < e2) return -1;";
	    newline 2; print "if (e1 > e2) return 1;";
	    newline 2; print "return 0;";
	    newline 0; print "}")
	 | #"u" => (* union *)
	   let val UnionType ctyl = getBoundType a
	       val zary = List.mapPartial (fn (c,NONE) => SOME c | _ => NONE) ctyl
	       val oary = List.mapPartial (fn (c,SOME ty) => SOME (c,ty) | _ => NONE) ctyl
	   in newline 0; print "\nint compare_"; print a; print " (struct "; print a; print " e1, struct "; print a; print " e2) {";
	      newline 2; print ("if (e1.con < e2.con) return -1;");
	      newline 2; print ("if (e1.con > e2.con) return 1;");
	      newline 2; print ("switch (e1.con) {");
	      List.app (fn c => (newline 4; print "case "; print c; print " : {break;}")) zary;
	      List.app (fn (c,ty) => (newline 4; print "case "; print c; print " : {";
				      compare 6 ty ("e1.arg."^c) ("e2.arg."^c);
				      print " break;}")) oary;
	      newline 2; print "}";
	      newline 2; print "return 0;";
	      newline 0; print "}"
	   end
	 | #"q" => (* queue, a record since normalized *)
	   let val QueueType (sz,ety) = getBoundType a
	   in newline 0; print "\nint compare_"; print a; print " (struct "; print a; print " e1, struct "; print a; print " e2) {";
	      List.app (fn (f,ty) => compare 2 ty ("e1."^f) ("e2."^f)) [("len",IntType),("items",ArrayType(sz,ety))];
	      newline 2; print "return 0;";
	      newline 0; print "}"
	   end
	 | #"r" => (* record *)
	   let val RecordType ftyl = getBoundType a
	   in newline 0; print "\nint compare_"; print a; print " (struct "; print a; print " e1, struct "; print a; print " e2) {";
	      List.app (fn (f,ty) => compare 2 ty ("e1."^f) ("e2."^f)) ftyl;
	      newline 2; print "return 0;";
	      newline 0; print "}"
	   end
	 | #"v" => (* value type, a record *)
	   let val RecordType ftyl = ty
	   in newline 0; print "\nint compare_"; print a; print " (struct "; print a; print " *e1, struct "; print a; print " *e2) {";
	      List.app (fn (f,ty) => compare 2 ty ("e1->"^f) ("e2->"^f)) ftyl;
	      newline 2; print "return 0;";
	      newline 0; print "}"
	   end
    end

(* prints sprint_a function for type a *)
and print_sprintFunc (a,ty) =
     let fun sprint i (NamedType n) e = print ("{long r = sprint_"^n^" (buff,"^e^"); if (r<0) return r;} ")
	   | sprint i (ty as ArrayType (sz,ety)) e =
	     let val j = new "j"
	         val z = new "z"
	     in newline i; print "{int "; print j; print "; ";
	        newline (i+1); print "int "; print z; print " = 0;";
	        newline (i+2); print "CATI(1,\"[\"); "; 
	        newline (i+2); print ("for ("^j^"=0;"^j^"<"^Int.toString sz^";"^j^"++) {");
	        newline (i+2); print ("if ("^z^") {CATI(1,\",\");} "^z^"++; ");
	        sprint (i+2) ety (e^"["^j^"]");
	        newline (i+2); print "}";
	        newline (i+2); print "CATI(1,\"]\"); "; 
	        newline i; print "}"
	     end
	   | sprint i (ty as BoolType) e = print ("if ("^e^") {CATI(4,\"true\");} else {CATI(5,\"false\");} ")
	   | sprint i _ e = print ("CATV(\"%ld\","^e^"); ")
     in case String.sub(a,0) of
	    #"e" => (* enum *)
	    let val EnumType cl = ty
	    in newline 0; newline 0; print "long sprint_"; print a; print " (char *buff, enum "; print a; print " e) {";
	       newline 2; print "switch (e) {";
	       List.app (fn c => (newline 4; print "case "; print c;
				  print " : {CATI("; printi (String.size c); print ",\""; print c; print "\"); break;}"))
			cl;
	       newline 2; print "}";
	       newline 2; print "return 0;";
	       newline 0; print "}"
	    end
	  | #"u" => (* union *)
	    let val UnionType ctyl = getBoundType a
		val zary = List.mapPartial (fn (c,NONE) => SOME c | _ => NONE) ctyl
		val oary = List.mapPartial (fn (c,SOME ty) => SOME (c,ty) | _ => NONE) ctyl
	    in newline 0; newline 0; print "long sprint_"; print a; print " (char *buff, struct "; print a; print " e) {";
	       newline 2; print "switch (e.con) {";
	       List.app (fn (c,SOME ty) => (newline 4; print "case "; print c;
					    print " : {CATI("; printi (String.size c); print ",\""; print c; print "\"); ";
					    print "CATI(1,\"(\");"; sprint 6 ty ("e.arg."^c); print " CATI(1,\")\")";
					    print "; break;} ")
			  | (c,_) => (newline 4; print "case "; print c;
				      print " : {CATI("; printi (String.size c); print ",\""; print c; print "\"); break;} ")
			) ctyl;
	       newline 2; print "}";
	       newline 2; print "return 0;";
	       newline 0; print "}"
	    end
	  | #"q" => (* queue *)
	    let val QueueType(sz,ety) = ty
	    in newline 0; newline 0; print "long sprint_"; print a; print " (char *buff, struct "; print a; print " e) {";
	       newline 2; print "{int qi;";
	       newline 4; print "CATI(2,\"{|\"); ";
	       newline 6; print ("for (qi=0;qi<e.len;qi++) {");
	       print "if (qi) {CATI(1,\",\");} ";
	       sprint 8 ety ("e.items[qi]");
	       newline 6; print "}";
	       newline 4; print "CATI(2,\"|}\"); ";
	       newline 2; print "}";
	       newline 2; print "return 0;";
	       newline 0; print "}"
	     end
	  | #"r" => (* record *)
	    let val RecordType ftyl = ty
		fun spr [(f,ty)] = (newline 2; print ("CATI(" ^ Int.toString (String.size f + 1) ^ ",\"" ^ f ^ "=" ^ "\"); ");
				    sprint 2 ty ("e." ^ f))
		  | spr (h::(t as _::_)) = (spr [h]; newline 2; print "CATI(1,\",\"); "; spr t)
	    in newline 0; newline 0; print "long sprint_"; print a; print " (char *buff, struct "; print a; print " e) {";
	       newline 2; print "CATI(1,\"{\"); ";
	       spr ftyl;
	       newline 2; print "CATI(1,\"}\"); ";
	       newline 2; print "return 0;";
	       newline 0; print "}"
	     end
	  | #"v" => (* value type, a record; also initialize avail, avoid surrounding quotes *)
	    let val RecordType ftyl = ty
		fun spr [(f,ty)] = (newline 2; print ("CATI(" ^ Int.toString (String.size f + 1) ^ ",\"" ^ f ^ "=" ^ "\"); ");
				    sprint 2 ty ("v->" ^ f))
		  | spr (h::(t as _::_)) = (spr [h]; newline 2; print "CATI(1,\",\"); "; spr t)
	    in newline 0; print "\nlong sprint_state (long sz, char *buff, key s) {";
	       newline 2; print "struct value *v = lookup(s);";
	       newline 2; print "avail = sz;";
	       newline 2; print "strcpy(buff,\"\\0\");";
	       newline 2; print "/* prints state into buff: */";
	       (* newline 2; print "CATI(1,\"{\"); "; *)
	       spr ftyl;
	       (* newline 2; print "CATI(1,\"}\"); "; *)
	       newline 2; print "return strlen(buff);";
	       newline 0; print "}"
	     end
     end

(* prints value type. Note: bad syntax if a empty, but should not be empty otherwise .c not necessary (see comment in emitFiacre below) *)
and print_tmpValueDecl a b =
    let fun print_VarDecl (st,ty,_) = (newline 0; print_Type ty st; print ";")
    in (* temporaries: *)
	newline 0; newline 0; print "/* temporary variables */";
	case a of [] => () | _ => List.app print_VarDecl b;
	newline 0
    end

and print_initialFunc a b =
    let (* initializers have been statically evaluated, hence can only be "value" expressions, without variables *)
	fun print_Initializer st v stt =
	    case v of
		BoolExp _        => (newline 0; print stt; print st; print "="; print_Exp v ""; print";")
	      | IntExp _         => (newline 0; print stt; print st; print "="; print_Exp v ""; print";")
	      | EnumExp (a,_)    => (newline 0; print stt; print st; print "="; print a; print";")
	      | RecordExp (fl,_) => List.app (fn (f,l) => (print_Initializer (st^"."^f) l stt)) fl
	      | ArrayExp (el,_)  => List.app (fn (x,y) => print_Initializer (st^"["^ Int.toString x  ^"]") y stt) 
					     (ListPair.zip(List.tabulate(List.length el,fn z => z),el)) 
	      | QueueExp (el,QueueType (z,ty)) =>
		let val len = List.length el
		in newline 0; print stt; print st; print ".len="; printi (length el); print";";
		   List.app (fn (x,y) => print_Initializer (st^".items["^ Int.toString x  ^"]") y stt) 
			    (ListPair.zip(List.tabulate(len,fn z => z),el));
		   List.app (fn x => default_Initializer (st^".items["^ Int.toString x  ^"]") ty stt) 
			    (List.tabulate(z-len,fn z => z+len))
		end
	      | ConExp (c,ty) =>
		(newline 0; print stt; print st; print ".con="; print c; print";") (* no arg *)
	      | ConstrExp (c,e,_) =>
		(newline 0; print stt; print st; print ".con="; print c; print";";
		 print_Initializer (st ^ ".arg." ^ c) e stt)
	      | _ => failwith "unexpected initializer"
	(* for initialization of "missing" elements in queues *)
	and default_Initializer st ty stt =
	    case ty of
		QueueType (z,ty) =>
		(newline 0; print stt; print st; print ".len=0;";
		 List.app (fn x => default_Initializer (st^".items["^ Int.toString x  ^"]") ty stt) (List.tabulate(z,fn z => z)))
	      | ArrayType (z,ty) =>
		List.app (fn x => default_Initializer (st^"["^ Int.toString x  ^"]") ty stt) (List.tabulate(z,fn z => z))
	      | RecordType ftyl =>
		List.app (fn (f,ty) => (default_Initializer (st^"."^f) ty stt)) ftyl
	      | BoolType => print_Initializer st (BoolExp false) stt
	      | _ => print_Initializer st (IntExp (0,IntType)) stt
    in newline 0; newline 0; print "/* initializes temporaries and creates initial state */";
       newline 0; print "key initial() {";
       newline 0; print "struct value *start = (struct value *) malloc(sizeof(struct value));";
       (* initialize temporaries: *)
       newline 0; print "/* temporaries: */";
       List.app (fn x as (st,_,v) => print_Initializer st v "") b;
       (* initialize state variables: *)
       List.app (fn x as (st,_,v) => print_Initializer st v "start->") a;
       newline 0; print "init_storage(compare_value,free_value);";
       newline 0; print "return store (start);"; 
       newline 0; print "}"
    end

and print_transFunc a =
    (newline 0; print "\n/* -- predicates and actions */";
     newline 0; print "/* by default, pre_t* returns true and act_t* is the identity function */";
     newline 0; newline 0; print "/* translation table for transition names */";
     newline 0; print ("char *transtable["^ Int.toString(List.length a) ^"] = "); 
     print_list ("{",",","}") (fn (([st],_),_,_,_,_) => print ("\""^ hd (String.fields (fn #":" => true | _ => false) st)^"\"")) a; print "; ";
     newline 0; print "\nchar **transitions (long *sz) {";
     newline 0; print "*sz ="; print (Int.toString(List.length a)); print "; ";
     newline 0; print "return transtable;";
     newline 0; print "}";
     List.app (fn (i,(([tag],lab),_,stm,_,_)) => print_preFunc (tag^(case lab of "" => "" | _ => (":"^lab))) i stm [])
	      (ListPair.zip(List.tabulate(List.length a,fn y=>y),a));
     List.app (fn (i,(([tag],lab),_,stm,_,_)) => print_actFunc (tag^(case lab of "" => "" | _ => (":"^lab))) i stm)
	      (ListPair.zip(List.tabulate(List.length a,fn y=>y),a)))

(* returns true if statement or expression updates some state variable *)
and assigns dl st =
    case st of
	StGuard e => assignse dl e
      | StSequence(stm1,stm2) => assigns dl stm1 orelse assigns dl stm2
      | StAssign(p,exp) => assignsp dl p orelse assignse dl exp
      | StWhile (e,s) => assignse dl e orelse assigns dl s
      | StForeach (v,s) => assignse dl v orelse assigns dl s
      | StCond (e,s1,s2) => assignse dl e orelse assigns dl s1 orelse assigns dl s2
      | StBlock (_,s) => assigns dl s
      | _ => false
and assignsp dl p =
    case p of
	VarExp (v,_) => (case List.find (fn (v',_) => v=v') dl of SOME _ => false | _ => not (mem v (!TEMP)))
      | ArrayAccessExp(a,i,_) => assignsp dl a orelse assignse dl i
      | RecordAccessExp(r,_,_) => assignsp dl r
and assignse dl e =
    matchExp (fn LetExp (s,e) => assigns dl s | _ => false) e

and print_preFunc tag index stm dl =
    case stm of
	StBlock(dl',s) => print_preFunc tag index s (dl @ dl')
      | StSequence(s,_) => print_preFunc tag index s dl
      | StGuard e =>
	let fun print_unroll_Exp e prf = (* user friendly *)
		case e of
		    LetExp (s,e') => (print_Statement 2 s prf; print_unroll_Exp e' prf)
		  | InfixExp (AND,_,e1,e2) => (print_unroll_Exp e1 prf; print_unroll_Exp e2 prf)
		  | UnopExp(NOT,_,UnopExp(NOT,_,e)) => print_unroll_Exp e prf
		  | UnopExp(NOT,_,e) => (newline 2; print "if ("; print_Exp e prf; print ") return 0;")
		  | _ => (newline 2; print "if (!("; print_Exp e prf; print ")) return 0;")
	    fun print_guard dl e prf =
		if null dl
		then print_unroll_Exp e prf
		else (newline 2;
		      print "{"; List.app (fn (s,ty) => (TEMP := s :: !TEMP; newline 4; print_Type ty s; print "; ")) dl;
		      print_unroll_Exp e prf;
		      newline 2; print "}")
	in newline 0; newline 0; print ("bool pre_"^ Int.toString(index) ^" (key s) {");
	   newline 2; print "/* "; print tag; print " */";
	   if assignse dl e
	   then (* #### improve: make local copies of state variables updated in the block, rather than all #### *)
	        (newline 2; print "struct value new = *(struct value *)(lookup(s));";
		 print_guard dl e "new.")
	   else (newline 2; print ("struct value *v = lookup(s);");
		 print_guard dl e "v->");
	   newline 2; print "return 1;"; 
	   newline 0; print "}"	   
	end
      | _ => (* no guard *) ()

and print_actFunc tag index stm =
    if assigns [] stm
    then let fun print_act s dl =
		 case s of
		     StBlock(dl',s) => print_act s (dl @ dl')
		   | StSequence(s1,s2) =>
		     let fun hasGuard s = case s of StGuard _ => true | StBlock (_,s) => hasGuard s | _ => false
		     in if matchStm hasGuard s1
			then print_act s2 dl
			else (newline 0; newline 0; print ("key act_"^ Int.toString(index) ^" (key s) {");
			      newline 0; print "/* "; print tag; print " */";
			      newline 2; print ("struct value *v = lookup(s);");
			      newline 2; print ("struct value *new = (struct value *) malloc(sizeof(struct value));");
			      newline 2; print ("memcpy(new,v,sizeof(struct value));");
			      print_Statement 2 s "new->";
			      newline 2; print ("return store(new);");
			      newline 0; print "}")
		     end
		   | _ => ()
	 in print_act stm []
	 end
    else ()

(* prints independent function *)
and print_indepFunc varL =
    (newline 0; newline 0; print "/* -- independance relation for transitions, for partial order modes and reset in timed modes -- */";
     newline 0; print "/* indices refer to transtable; specifies independance for ALL transitions */";
     newline 0; print "bool independant (long t1, long t2) {";
     newline 2; print "  /* always true */";
     newline 2; print "  return 1;";
     newline 0; print "}")

(* prints structure components in C style *)
and print_obsFunc varL =
    let	fun getVarNames  st ty =
	    case ty of
		BoolType => [("(long) v->"^st,st)]
	      | NatType => [("(long) v->"^st,st)]
	      | IntType => [("(long) v->"^st,st)]
	      | EnumType cl => [("(long) v->"^st,st)]
	      | UnionType _ => [("(long) v->"^st^".con",st)]  (* ##### IMPROVE ##### *)
	      | RecordType stList => List.concat(List.map (fn (x,y)=> getVarNames (st^"."^x) y) stList)
	      | ArrayType(s,tty) => List.concat(List.tabulate(s,fn y=> getVarNames (st^"["^ Int.toString(y)  ^"]") tty))
	      | QueueType (s,tty) => getVarNames st (RecordType [("len",IntType),("items",ArrayType(s,tty))])
	      | NamedType aa => getVarNames st (getBoundType aa)
	val varNames = flat (map (fn (stl,b,c) => getVarNames stl b) varL)
	val (str1,str2) = ListPair.unzip(varNames)
    in  newline 0; print "\n/* -- state observers for kts output -- */";
	newline 0; print ("char *obsnames[" ^ Int.toString(List.length str2) ^ "] = {");
	print_list ("",",","") (fn y => print ("\"" ^ y ^ "\"")) str2; print "};";
	newline 0; print ("\nchar **obs_names (long *count) {*count = " ^ Int.toString(List.length str2)  ^"; return obsnames;}");
	newline 0; print ("\n/* returns values of observables for state s (abstractions for noninteger fields) */");
	newline 0; print ("long ovalues["^ Int.toString(List.length str2)  ^"];");
	case str2 of [] => () | _ =>
	   (newline 0; newline 0; print "long *obs_values (key s) {";
	    newline 0; print "struct value *v = lookup(s);";
	    newline 0; print_list ("",";\n","; ")
				  (fn (x,y) => print ("ovalues[" ^ Int.toString(x) ^ "]=" ^ y))
				  (ListPair.zip(List.tabulate(List.length str1,(fn y=>y)),str1));
	    newline 0; print "return ovalues;";
	    newline 0; print "}")
    end 

and print_Type ty a =
    case ty of
	BoolType => (print "bool"; print " "; print a)
      | NatType  => (print "unsigned long"; print " "; print a)   (* no more nats, normally *)
      | IntType  => (print "long"; print " "; print a)
      | NamedType nt =>	(case getBoundType nt of
			     EnumType _ => (print "enum "; print nt; print " "; print a)
			   | UnionType _ => (print "struct "; print nt;  print " "; print a) (* unions are records *)
			   | RecordType _ => (print "struct "; print nt;  print " "; print a)
			   | QueueType _ => (print "struct "; print nt;  print " "; print a) (* queues are records *)
			   | _ => failwith "print_Named_Type")
      | EnumType cl => (print "enum "; print a; print_list (" {",",","}") print cl)
      | UnionType ctyl => let val l = List.mapPartial (fn (c,SOME ty) => SOME (c,ty) | _ => NONE) ctyl
			  in print "struct "; print a; print " {";
			     newline 2; print "enum "; print_list (" {",",","}") (fn (c,_) => print c) ctyl; print " con;";
			     newline 2; print "union {";
			     List.app (fn (c,ty) => (newline 4; print_Type ty c; print ";")) l;
			     newline 2; print "} arg;";
			     newline 0; print "}"
			  end
      | RecordType rl => (print "struct "; print a;
			  print_list (" {","","") (fn (x,y) => (newline 2; print_Type y x; print "; ")) rl;
			  newline 0; print "}")
      | ArrayType (s,t) => let fun dims (ArrayType (s,t)) accu = dims t (s::accu)
				 | dims ty accu = (rev accu,ty)
			       val (dm,ty) = dims ty []
			   in print_Type ty ""; print " ";  print a;
			      List.app (fn s => (print "["; print (Int.toString s); print"]")) dm
			   end
      | QueueType(s,b) => (print "struct "; print a; print " {";
			   newline 2; print "int len;";
			   newline 2; print_Type (ArrayType (s,b)) "items"; print "; ";
			   newline 0; print "}")
      | _ => failwith "print_Type"

(* prints statements in a act_* (without StGuard) *)
and print_Statement i stm stt =
    case stm of
	StSequence(stm1,stm2) => (print_Statement i stm1 stt; print_Statement i stm2 stt)
      | StCond(e,s1,StNull) => (newline i; print "if ("; print_Exp e stt; print ") {";
				newline (i+2); print_Statement i s1 stt;
				newline i; print "}")
      | StCond(e,StNull,s2) => (newline i; print "if (!("; print_Exp e stt; print ")) {";
				newline (i+2); print_Statement i s2 stt;
				newline i; print "}")
      | StCond(e,s1,s2) => (newline i; print "if ("; print_Exp e stt; print ") {";
			    newline (i+2); print_Statement i s1 stt;
			    newline i; print "} else {";
			    newline (i+2); print_Statement i s2 stt;
			    newline i; print "}")
      | StWhile (e,s) => (newline i; print "while ("; print_Exp e stt; print ") do {";
			  newline (i+2); print_Statement i s stt;
			  newline i; print "}")
      | StForeach (e as VarExp(v,IntervalType(fr,to)),s) => (newline i; print "for (";
							     print_Exp e stt; print "="; printi fr; print ";";
							     print_Exp e stt; print "<="; printi to; print ";";
							     print_Exp e stt; print "++) {";
							     newline (i+2); print_Statement i s stt;
							     newline i; print "}")
      | StAssign(acc,exp) =>
	(* expansion must be applied recursively. e.g.  a := b when a : array 4 of array 8 of ... *)
	if acc=exp then () else
	(case exp of
	     LetExp (stm,e) => (print_Statement i stm stt; print_Statement i (StAssign(acc,e)) stt)
	   | RecordExp (fle,RecordType ftyl) =>
	     List.app (fn (f,e) => print_Statement i (StAssign(RecordAccessExp(acc,f,assoc f ftyl),e)) stt) fle
	   | ArrayExp (el,ArrayType (sz,ety)) =>
	     (List.tabulate (sz,fn j => print_Statement i (StAssign(ArrayAccessExp(acc,IntExp(j,IntType),ety),List.nth(el,j))) stt); ())
	   | QueueExp (el,QueueType(sz,ety)) =>
	     let val len = length el
	     in print_Statement i (StAssign(RecordAccessExp(acc,"len",IntType),IntExp(len,IntType))) stt;
		print_Statement i (StAssign(RecordAccessExp(acc,"items",ArrayType(sz,ety)),
					    ArrayExp(List.tabulate(sz,fn j => if j<len then List.nth(el,j) else IntExp(0,IntType)),
						     ArrayType(sz,ety)))) stt
	     end
	   | BinopExp(ENQUEUE,QueueType(sz,ety),q,e) =>
	     (* if not (full q) :
		   acc.len := q.len+1;
	        for (i=0; i<sz; i++) {acc.items[i] := q.items[i];}
		  acc.items[q.len] := e;
	      *)
	     let val aty = ArrayType(sz,ety)
	     in newline i; print "/* enqueue begin */";
		newline i; print "FailIfFull("; print_Exp q stt; print ","; printi sz; print ");";
		print_Statement i (StAssign(RecordAccessExp(acc,"len",IntType),InfixExp(ADD,IntType,RecordAccessExp(q,"len",IntType),IntExp(1,IntType)))) stt;
		List.app (fn j => print_Statement i (StAssign(ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp(j,IntType),ety),
							      ArrayAccessExp(RecordAccessExp(q,"items",aty),IntExp(j,IntType),ety))) stt)
			 (List.tabulate(sz,fn k => k));
		print_Statement i (StAssign(ArrayAccessExp(RecordAccessExp(acc,"items",aty),RecordAccessExp(q,"len",IntType),ety),e)) stt;
		newline i; print "/* enqueue end */"
	     end
	   | BinopExp(APPEND,QueueType(sz,ety),q,e) => (* assign result then modify *)
	     (* if not (full q) :
		   acc.len := q.len+1;
	        for (i=1; i<sz; i++) {acc.items[i] := q.items[i-1];}
		  acc.items[0] := e;
	      *)
	     let val aty = ArrayType(sz,ety)
	     in newline i; print "/* append begin */";
		newline i; print "FailIfFull("; print_Exp q stt; print ","; printi sz; print ");";
		print_Statement i (StAssign(RecordAccessExp(acc,"len",IntType),InfixExp(ADD,IntType,RecordAccessExp(q,"len",IntType),IntExp(1,IntType)))) stt;
		List.app (fn j => print_Statement i (StAssign(ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp(j,IntType),ety),
							      ArrayAccessExp(RecordAccessExp(q,"items",aty),IntExp(j-1,IntType),ety))) stt)
			 (List.tabulate(sz-2,fn k => k+1));
		print_Statement i (StAssign(ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp(0,IntType),ety),e)) stt;
		newline i; print "/* append end */"		    
	     end
	   | UnopExp(DEQUEUE,QueueType(sz,ety),q) => (* assign result then modify *)
	     (* if not (empty q) :
		   acc.len := q.len-1;
	        for (i=1; i<sz; i++) {acc.items[i-1] := q.items[i];}
		  acc.items[sz-1] := default
	      *)
	     let val aty = ArrayType(sz,ety)
		 fun default_assign acc ty =
		     case ty of
			 qty as QueueType (z,ety) =>
			 let val aty = ArrayType(sz,ety)
			 in newline 0; print_Statement i (StAssign(RecordAccessExp(acc,"len",IntType),IntExp(0,IntType))) stt;
			    List.app (fn x => default_assign (ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp (x,IntType),ety)) ety)
				     (List.tabulate(z,fn z => z))
			 end
		       | ArrayType (z,ety) =>
			 List.app (fn x => default_assign (ArrayAccessExp(acc,IntExp (x,IntType),ety)) ety) (List.tabulate(z,fn z => z))
		       | RecordType ftyl =>
			 List.app (fn (f,ty') => default_assign (RecordAccessExp(acc,f,ty')) ty') ftyl
		       | BoolType => print_Statement i (StAssign(acc,BoolExp false)) stt
		       | _ => print_Statement i (StAssign(acc,IntExp (0,IntType))) stt
	     in newline i; print "/* dequeue begin */";
		newline i; print "FailIfEmpty("; print_Exp q stt; print ","; printi sz; print ");";
		print_Statement i (StAssign(RecordAccessExp(acc,"len",IntType),InfixExp(SUB,IntType,RecordAccessExp(q,"len",IntType),IntExp(1,IntType)))) stt;
		List.app (fn j => print_Statement i (StAssign(ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp(j-1,IntType),ety),
							      ArrayAccessExp(RecordAccessExp(q,"items",aty),IntExp(j,IntType),ety))) stt)
			 (List.tabulate(sz-1,fn k => k+1));
		default_assign (ArrayAccessExp(RecordAccessExp(acc,"items",aty),IntExp(sz-1,IntType),ety)) ety;
		newline i; print "/* dequeue end */"
	     end
	   | UnopExp(FIRST,QueueType(sz,ety),q) => (* *)
	     (* if not (empty q) :
		   acc := q.items[0]
	      *)
	     let val aty = ArrayType(sz,ety)
	     in newline i; print "/* first begin */";
		newline i; print "FailIfEmpty("; print_Exp q stt; print ","; printi sz; print "); ";
		print_Statement i (StAssign(acc,ArrayAccessExp(RecordAccessExp(q,"items",aty),IntExp(0,IntType),ety))) stt;
		newline i; print "/* first end */"
	     end
	   | ConExp (c,ty) =>
	     (newline i; print_Exp acc stt; print ".con = "; print c; print "; ")
	   | ConstrExp (c,e,UnionType ctyl) =>
	     (newline i; print_Exp acc stt; print ".con = "; print c; print "; ";
	      print_Statement i (StAssign(RecordAccessExp(acc,"arg." ^ c,let val SOME ty = assoc c ctyl in ty end),e)) stt)
	   | _ => let fun ASSIGN i ty acc exp =
			  case ty of
			      NamedType nn => (* should not happen *) ASSIGN i (getBoundType nn) acc exp
			    | ArrayType _ =>
			      (* could be expanded in postcompose ... *)
			      (* {int i; .....; int b = 1; for (i=0; i<sz; i++) {acc[i] = exp[i];}} *)
			      let fun dims (ArrayType (s,t)) accu = dims t (s::accu) | dims ty accu = (rev accu,ty)
				  val (dm,ty) = dims ty []
				  val ivars = map (fn _ => new "j") dm
			      in newline i; print "{int "; print_list ("",", ","; ") print ivars;
				 let fun printdims i [] acc exp = ASSIGN i ty acc exp
				       | printdims i ((j,dm)::t) acc exp =
					 (newline (i+2); print ("for ("^j^"=0;"^j^"<"^Int.toString dm^";"^j^"++) {");
				          (* any ty in following line; will not be used *)
					  printdims (i+4) t (ArrayAccessExp(acc,VarExp(j,IntType),ty)) (ArrayAccessExp(exp,VarExp(j,IntType),ty));
					  newline (i+2); print "}")
				 in printdims i (ListPair.zip(ivars,dm)) acc exp;
				    newline i; print "}"
				 end
			      end
			    | _ => (newline i; print_Exp acc stt; print "="; print_Exp exp stt; print"; ")
		  in case acc of
			 VarExp (_,ty) => ASSIGN i ty acc exp
		       | RecordAccessExp (_,_,ty) => ASSIGN i ty acc exp
		       | ArrayAccessExp (_,_,ty) => ASSIGN i ty acc exp
		  end)
      | StGuard exp => ()
      | StBlock (dl,ss) =>
	(newline i; print "{";
	 List.app (fn (s,ty) => (TEMP := s :: !TEMP; print_Type ty s; print "; ")) dl;
	 print_Statement i ss stt;
	 newline i; print "}")
      | _ => ()

(* prints guards or rhs of assignements or initializer components *)
and print_Exp e stt =
    case e of
	VarExp (a,_)            => (if mem a (!TEMP) then () else print stt; print a)
      | RecordAccessExp (a,b,_) => (print_Exp a stt; print "."; print b)
      | ArrayAccessExp (a,b,_)  => (print_Exp a stt; print "["; print_Exp b stt; print "]")
      | IntExp (n,_)            => printi n
      | BoolExp b               => if b then print "1" else print "0" 
      | EnumExp (e,_)           => print e
      | DestrExp (c,e)          => (print_Exp e stt; print (".arg." ^ c))
      | MatchesExp (c,e)        => (print "("; print_Exp e stt; print ".con == "; print c; print ")")
      | InfixExp(x,ty,y,z) 	  =>
	(case x of
	     ADD => print_arith_infix ty (y,z) "ADD" "+" stt
	   | SUB => print_arith_infix ty (y,z) "SUB" "-" stt
	   | MUL => print_arith_infix ty (y,z) "MUL" "*" stt
	   | DIV => print_arith_infix ty (y,z) "DIV" "/" stt
	   | MOD => print_arith_infix ty (y,z) "MOD" "%" stt
	   | EQ  => (* expansion for all constituents *)
	     let fun EQ ty e1 e2 =
		     case ty of
		         NamedType n => (print "(! compare_"; print n; print "("; print_Exp e1 stt; print ","; print_Exp e2 stt; print "))")
		       | ArrayType _ => (* could be expanded in postcompose ... *)
			 (* ({int i; .....; int b = 1; for (i=0; b && (i<sz); i++) {.... {b = EQ ety (e1[i]) (e2[i])}; b;} ...}) *)
			 let fun dims (ArrayType (s,t)) accu = dims t (s::accu) | dims ty accu = (rev accu,ty)
			     val (dm,ty) = dims ty []
			     val ivars = map (fn _ => new "j") dm
			     val b = new "b"
			 in print "({int "; print_list ("",", ","; ") print ivars;
			    print "int "; print b; print "=1; ";
			    let fun printdims [] x y = (print ("{"^b^" = "); EQ ty x y; print ";}")
				  | printdims ((j,dm)::t) x y =
				    (print ("for ("^j^"=0;"^b^"&&("^j^"<"^Int.toString dm^");"^j^"++) {");
				     (* any ty in following line; will not be used *)
				     printdims t (ArrayAccessExp(x,VarExp(j,IntType),ty)) (ArrayAccessExp(y,VarExp(j,IntType),ty));
				     print "}")
			    in printdims (ListPair.zip(ivars,dm)) e1 e2;
			       print (" "^b^";})")
			    end
			 end	   
		       | _ => (print "("; print_Exp e1 stt; print " == "; print_Exp e2 stt; print ")")
	     in (* if matches (y,z) then print "1" else *) EQ ty y z
	     end
	   | NE  => (* if matches (y,z) then print "0" else *) (print "("; print "!"; print_Exp (InfixExp (EQ,ty,y,z)) stt; print ")")
	   | _ => let val f = case x of AND => "&&" | OR  => "||" | LE  => "<=" | LT  => "<" | GT  => ">" | GE  => ">="
		  in print "("; print_Exp y stt; print " "; print f; print " "; print_Exp z stt; print ")"
		  end)
      | BinopExp(x,ty,y,z)	  => failwith "unexpected queue exp"
      | UnopExp(a,ty,b)         =>
	(case a of
	     MINUS  => print_arith_unop ty b "MINUS" "-" stt
	   | PLUS   => print_Exp b stt
	   | COERCE => print_arith_unop ty b "COERCE" "" stt
	   | NOT    => (case b of
			    UnopExp(NOT,_,b') => print_Exp b' stt
			  | _ => (print "("; print "!"; print_Exp b stt; print ")"))
	   | _ =>
	     let val QueueType(sz,_) = ty
	     in case a of
		    EMPTY => (print "("; print "EMPTY("; print_Exp b stt; print "))")
		  | FULL  => (print "("; print "FULL("; print_Exp b stt; print ","; printi sz; print "))")
		  | _ => failwith "unexpected queue exp"
	     end)
      | CondExp(c,e1,e2)        => (print "(("; print_Exp c stt; print ") ? ("; print_Exp e1 stt; print ") : ("; print_Exp e2 stt; print "))")
      | LetExp(s,e)             => (print "({"; print_Statement (~1000) s stt; print_Exp e stt; print ";})")
      | RecordExp _             => failwith "record expression in emit"
      | ArrayExp _              => failwith "array expression in emit"
      | QueueExp _              => failwith "queue expression in emit"
      | ConExp (c,_)            => failwith ("0-ary construction ("^c^" in print_Exp")
      | ConstrExp (c,_,_)       => failwith ("1-ary construction ("^c^" ...) in print_Exp")
      | _                       => failwith "unexpected exp in print_Exp"

and print_arith_infix ty (y,z) f ope stt =
    case ty of
	IntervalType(l,h) => (print ("I" ^ f); print "("; print_Exp y stt; print ","; print_Exp z stt; print ","; printi l; print ","; printi h; print ")")
      | NatType => (print ("N" ^ f); print "("; print_Exp y stt; print ","; print_Exp z stt; print ")")
      | _ => (print "("; print_Exp y stt; print " "; print ope; print " "; print_Exp z stt; print ")")

and print_arith_unop ty x f ope stt =
    case ty of
	IntervalType(l,h) => (print ("I" ^ f); print "("; print_Exp x stt; print ","; printi l; print ","; printi h; print ")")
      | NatType => (print ("N" ^ f); print "("; print_Exp x stt; print ")")
      | _ => (print "("; print ope; print_Exp x stt; print ")")


(* emits .c iff some data *)
fun emitFiacre (s0 as Program [ProcessDecl (_,_,_,_,_,f,_,_,_,_)]) =
    (newline 0; newline 0; print "#include <stdlib.h>";
     newline 0; print "#include <math.h>";
     newline 0; print "#include <string.h>";
     newline 0; print "#include <stdio.h>";
     newline 0; newline 0; print "/* fiacre headers */";
     newline 0; print "#include \"fiacre.h\"";
     if List.null f then
	 (newline 0; print "/* minimal functions */";
	  newline 0; print "typedef int bool;";
	  newline 0; print "typedef long key;";
	  newline 0; print "char buff[1];";
	  newline 0; print "long sprint_state (long sz, char *buff, key s) {strcpy(buff,\"\\0\"); return 0;}";
	  newline 0; print "key initial() {return 0;}";
	  newline 0; print "char **transitions (long *sz) {*sz =0; return 0;}";
	  newline 0; print "char **obs_names (long *count) {*count = 0; return 0;}";
	  newline 0; print "long *obs_values (key s) {return 0;}";
	  newline 0; print "bool independant (long t1, long t2) {return 1;}";
	  newline 0; flush ())
     else
	 (newline 0; newline 0; print "/* storage API */";
	  newline 0; print "#include \"avl.h\"";
	  newline 0; print "\n/* type declarations and utilities */";
	  newline 0; print "\ntypedef int bool;";		      
	  print_Fiacre (Name.nameTypes s0);
	  newline 0; flush ()))
    handle BYE => raise BYE | _ => failwith "emit c";
   
end;



structure OutNetEmit = NetEmit(OutPrint);
structure OutCEmit = CEmit(OutPrint);




signature EMIT = sig
val emit : TTS -> unit
(* etc ... *)
end;


structure Emit : EMIT = struct
fun emit t =
    case !OUTPUT of
	TTS => let val dir = !OutPrint.name
		   val base = #base(OS.Path.splitBaseExt (#file (OS.Path.splitDirFile dir)))
		   val fnet = OS.Path.joinDirFile {dir=dir,file=OS.Path.joinBaseExt {base=base,ext=SOME "net"}}
		   val fdat = OS.Path.joinDirFile {dir=dir,file=OS.Path.joinBaseExt {base=base,ext=SOME "c"}}
	       in OutPrint.name := fnet;
		  OutPrint.FILE := TextIO.openOut fnet handle x => (ErrPrint.print ("cannot open "^fnet^"\n"); raise BYE);
		  printVerbose "::Writing Net(.net) File\n";
		  OutNetEmit.emitFiacre t;
		  printVerbose "::Done\n";
		  OutPrint.name := fdat;
		  OutPrint.FILE := TextIO.openOut fdat handle x => (ErrPrint.print ("cannot open "^fdat^"\n"); raise BYE);
		  printVerbose "::Writing Data(.c) File\n";		 
		  OutCEmit.emitFiacre t;
		  printVerbose "::Done\n"
	       end
      | TTSText => (printVerbose "::Writing Text File\n";
		    (* OutTtsEmit.emitTts t; *)
 		    let val (decls,Program dl) = Name.nameTypes t
 		    in OutTtsEmit.emitTts (Program (decls @ dl))
 		    end;
		    printVerbose "::Done\n")
      | _ => ErrPrint.print ("unknown code generator\n");

end;


