
(* output options *)
datatype output = NoOutput | Fiacre | TTS | TTSText | LOTOS;
val OUTPUT = ref NoOutput;  (* output option, default just parses *)

(* mode flags *)
val VERBOSE = ref false;
val DETAILS = ref false;  (* for debugging specializations, with flags -g *)
val NOCOMP = ref false;   (* for debugging compositions *)
val MAXIMIZE = ref true;  (* debug option for type instances of primitives *)
val TYPECHECK = ref true; (* typechecks or not *)
val TRANSF = ref false;   (* if -F, prints result of pre-processing rather than fiacre sources *)
val ARCH = ref false;     (* print instances tree if -arch *)

(* error handling *)
exception BYE;  (* signals termination *)
fun error m = (ErrPrint.print m; ErrPrint.print "\n"; ErrPrint.flush (); raise BYE);

(* banner *)
val BANNER = "Frac version "^version^release^" -- "^date^" -- FERIA/SVF & INRIA/VASY\n";

(* removes "/" at end of file, if any *)
fun strip f =
    let val k = String.size f
    in if #"/" = String.sub(f,k-1) then String.substring (f,0,k-1) else f
    end;

(* command line parser *)
fun command args =
	let open OutPrint
	    fun usage () =
		(print "usage: frac [-h | -help]\n";
		 print "            [-q | -v] [-o errfile]\n";
		 print "            [-p | -c | -f | -t]\n";
		 print "            [infile] [outfile]\n")
	    fun fracerror m =
	        (print "frac: "; print m; print "\n"; usage(); raise BYE)
	    fun help () =
		(print BANNER;
		 usage();
		 print "FLAGS        WHAT                                    DEFAULT\n";
		 print "-h | -help   this mode                               \n";
	         print "error format:                                        \n";
		 print "-q            quiet                                  -q\n";
		 print "-v            verbose                                -q\n";
		 print "-o errfile    save errors in errfile (stderr if -)   stderr\n";
		 print "output format:                                       \n";
		 print "-p            just parses                            -c\n";
		 print "-c            just parses and checks                 -c\n";
		 print "-f | -fcr     checks then outputs fiacre source      -c\n";
		 print "              (implicit if outfile has extension .fcr)\n";
		 print "-t | -tts     checks then compiles to .tts           -c\n";
		 print "              (implicit if outfile has extension .tts)\n";
		 print "files:                                               \n";
		 print "infile        input file (stdin if -)                stdin\n";
		 print "outfile       output file (stdout if -)              stdout\n";
		 flush ();
		 OS.Process.exit OS.Process.success)
	    val FILES = ref 0
	    fun concat l = String.concat (map (fn x => " "^x) l)
	    fun parse l = case l of
		[] => ()
	      | "-help"::_ => help()
	      | "-h"::_    => help()
	      | "-q"::t    => (VERBOSE := false; parse t)
	      | "-v"::t    => (VERBOSE := true; parse t)
	      | "-o"::file::t => (if String.size file > 1 andalso "-" = String.substring(file,0,1)
				  then fracerror ("bad command line, skipping "^file^" ... "^concat t)
				  else ErrPrint.name := strip file;
				  parse t)
	      | "-p"::t    => (OUTPUT := NoOutput; TYPECHECK := false; parse t)
	      | "-c"::t    => (OUTPUT := NoOutput; TYPECHECK := true; parse t)
	      | "-f"::t    => (OUTPUT := Fiacre; parse t)
	      | "-fcr"::t  => (OUTPUT := Fiacre; parse t)
	      | "-t"::t    => (OUTPUT := TTS; parse t)
	      | "-tts"::t  => (OUTPUT := TTS; parse t)
(* private *) | "-F"::t    => (OUTPUT := Fiacre; TRANSF := true; parse t)
(* private *) | "-Fd"::t   => (OUTPUT := Fiacre; TRANSF := true; DETAILS := true; parse t)
(* private *) | "-G"::t    => (OUTPUT := TTSText; parse t)
(* private *) | "-Gd"::t   => (OUTPUT := TTSText; DETAILS := true; parse t)
(* private *) | "-nocomp"::t => (OUTPUT := TTSText; NOCOMP := true; parse t)
(* private *) | "-arch"::t => (ARCH := true; parse t)
	      | file::t    => if String.size file > 1 andalso "-" = String.substring(file,0,1)
			      then fracerror ("bad command line, skipping "^file^" ... "^concat t)
	                      else (FILES := 1 + !FILES; case !FILES of
			    1 => if file = "-" then parse t else (Input.name := strip file; parse t)
			  | 2 => if file = "-" then parse t else (OutPrint.name := strip file; parse t)
			  | _ => fracerror ("bad command line, skipping "^file^" ... "^concat t))
	in parse args
	end;




