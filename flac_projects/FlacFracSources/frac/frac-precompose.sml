(* converts fiacre into tts abstract syntax.
- types are expanded by typechecker, hence type and channel decls can be removed
  (types that need be named will be given names when code is emitted);
- constant identifiers replaced by their values, constant decls removed;
- flattens declarations (one ident per entry);
- checks all constructions are allowed;
- normalizes statements, ready to compose;
*)



signature TOTTS = sig
val toTTS : Fiacre.Fiacre -> TTS.TTS
val convAssignMatch : TTS.Statement -> TTS.Statement
val unrollForeach : TTS.Statement -> TTS.Statement
val normSequence : TTS.Statement -> TTS.Statement
end;

structure ToTTS : TOTTS = struct

open Fiacre;


(* some translation routines *)

fun elimSelect s =
    let open TTS
    in case s of
	   StSelect sl => flat (map elimSelect sl)
	 | StSequence(s1,s2) =>
	   let val sl1 = elimSelect s1
	       and sl2 = elimSelect s2
	   in flat (map (fn x => map (fn y => StSequence (x,y)) sl2) sl1)
	   end
         (* | StNull => [] *) (* would causes failure in composition ... *)
	 | _ => [s]
    end;

(* normalizes a sequence statement so that lefts of StSequence is never a sequence *)
fun normSequence s =
    let open TTS
    in case s of
	   StSequence (StSequence(s1,s2),s3) => normSequence (StSequence(s1,StSequence(s2,s3)))
	 | StSequence (StNull,s) => normSequence s
	 | StSequence (s,StNull) => normSequence s
	 | StSequence (s1,s2) => StSequence(normSequence s1,normSequence s2)
	 | StWhile (c,s) => StWhile (c,normSequence s)
	 | StForeach (v,s) => StForeach (v,normSequence s)
	 | StCond(c,s1,s2) => StCond(c,normSequence s1,normSequence s2)
	 | _ => s
    end;

(* frac requires cond and while statements to be deterministic, and without StTo *)
fun determSt s =
    case s of
	StSequence(_,s1,s2) => determSt s1 andalso determSt s2
      | StCond (_,c,s1,(lcsl),s2) => determSt s1 andalso List.all (fn (_,_,s) => determSt s) lcsl andalso
				     (case s2 of SOME s => determSt s | _ => true)
      | StWhile (_,c,s) => determSt s
      | StForeach (_,_,_,s) => determSt s
      | StCase (_,_,l) => List.all (fn (_,_,p,s) => universal p andalso determSt s) l (* pessimistic, NEEDs exhaustiveness analysis result ... *)
      | StNull => true
      | StAssign (_,pl,el) => List.all universal pl
      | s => false
and universal p =
    case p of
      IdentExp (_,ref (VarKind,_)) => true
    | ArrayAccessExp _ => true
    | RecordAccessExp _ => true
    | _ => false;

(* processes a single match, returns condition, remaining patter and expression *)
fun convMatch patt exp =
    let open TTS
    in case patt of
	   ConExp (c,ty) => (InfixExp(EQ,ty,patt,exp),NONE,NONE)
	 | IntExp n => (InfixExp(EQ,IntType,patt,exp),NONE,NONE)
	 | BoolExp b => (InfixExp(EQ,BoolType,patt,exp),NONE,NONE)
	 | EnumExp (c,ty) => (InfixExp(EQ,ty,patt,exp),NONE,NONE)
	 | ConstrExp(c,patt,_) =>
	   let val (f,acc,exp') = convMatch patt (DestrExp (c,exp))
	   in (InfixExp(AND,BoolType,MatchesExp(c,exp),f),acc,exp')
	   end
	 | AnyExp => (BoolExp true,NONE,NONE)
	 | _ => (* accesses *) (BoolExp true,SOME patt,SOME exp)
    end;


(* converts a simple assignement statement wrt pattern matching *)
fun convAssignMatch (s as (TTS.StAssign (patt,exp))) =
    let open TTS
    in case convMatch patt exp of
	   (BoolExp true,SOME _,_) => s
	 | (BoolExp true,_,_) => StNull
	 | (cond,SOME acc,SOME exp) => StSequence(StGuard cond,StAssign(acc,exp))
	 | (cond,_,_) => StGuard cond
    end;

(* sequentializes simultaneous statements.  eg:  x,y:=1,2  =>  x:=1;y:=2 *)
(* ##### A FAIRE: handle circular assignements, using extra variables ##### *)
fun expandAssign (pel as (pl,el)) =
	let val wpl = makeset (flat (map writtenInExp pl))
	    val wel = makeset (flat (map readInExp el))
	    fun expand ([p],[e]) = convAssignMatch (TTS.StAssign (p,e))
	      | expand (p::pl,e::el) = TTS.StSequence (convAssignMatch (TTS.StAssign (p,e)),expand (pl,el))
	in if List.all (fn v => not (mem v wel)) wpl
	   then expand pel
	   else notyet "circularity in multiple assignement"
	end;


(* expands any assignements into a select statement over the type of lhs *)
fun expandAny (pl,SOME e) = TTS.StSequence (expandAny (pl,NONE),TTS.StGuard e)
  | expandAny (pl,_) =
    let	(* type of pattern *)
	open TTS
	fun getType patt =
	    case patt of
		VarExp (_,ty) => ty
	      | ArrayAccessExp(_,_,ty) => ty
	      | RecordAccessExp(_,_,ty) => ty
	      | IntExp (_,ty) => ty
	      | BoolExp _ => BoolType
	      | _ => failwith "unexpected pattern in any statement"
	val h::t = map (fn p =>
			   case getType p of
			       BoolType => StSelect [convAssignMatch(StAssign(p,BoolExp true)),convAssignMatch(StAssign(p,BoolExp false))]
	      		     | ty as IntervalType(lmin,lmax) => StSelect (map (fn x => convAssignMatch(StAssign(p,IntExp (x,ty)))) (range(lmin,lmax)))
	      		     | IntType => comperror "any not allowed at infinite types yet"
	      		     | NatType => comperror "any not allowed at infinite types yet"
	      		     | _ => failwith "unexpected type in any statement")
		       pl
    in foldr (fn (x,y) => StSequence(x,y)) h t
    end;

(* StCase of line * Exp * (line * bool ref * Pattern * Statement) list *)
fun expandCase (exp,matches) =
    let open TTS
	fun convMatches [] = failwith "iff formed case statement"
	  | convMatches ((patt,s)::m) =
	    (case convMatch patt exp of
		 (BoolExp true,SOME acc,SOME exp) => StSequence(StAssign(acc,exp),s)
	       | (BoolExp true,NONE,_) => s
	       | (cond,SOME acc,SOME exp) => (case m of
			 [] => StSequence(StGuard cond,StSequence(StAssign(acc,exp),s))
			| _ => StSelect[StSequence(StGuard cond,StSequence(StAssign(acc,exp),s)),
						   StSequence(StGuard(UnopExp(NOT,BoolType,cond)),convMatches m)])
	       | (cond,_,_) => (case m of
			 [] => StSequence(StGuard cond,s)
			| _ => StSelect[StSequence(StGuard cond,s),
					StSequence(StGuard(UnopExp(NOT,BoolType,cond)),convMatches m)]))
    in convMatches matches
    end;

(* expands input statement, taking care of where condition *)
(* patt matching delayed until makeComposition, so that temp variables avoided *)
(* #### should input statements be expanded like any statements ? #### *)
fun expandInput (str,ch,pl,w) =
    let open TTS
    in case w of SOME e => StSequence (StInput (str,ch,pl),StGuard e) | _ => StInput (str,ch,pl)
    end;

(* replaces conditionals by a select of guard statements *)
fun expandCond s =
    let open TTS
    in case s of
	   StCond(e,a3,a5) => StSelect [StSequence(StGuard e,a3),StSequence(StGuard(UnopExp(NOT,BoolType,e)),a5)]
	 | _ => s
    end;

(* unrolls foreach statements *)
fun unrollForeach (TTS.StForeach (VarExp(str,ty as TTS.IntervalType(lmin,lmax)),s)) =
    let open TTS
	fun unroll [v] = StSequence(StAssign(VarExp(str,ty),v),s)
	  | unroll (h::t) = StSequence (StAssign(VarExp(str,ty),h),StSequence(s,unroll t))
    in unroll (map (fn x => IntExp (x,ty)) (range(lmin,lmax)))
    end;

(* computes a legal initial value for a non-initialized variable of type ty *)
(* note: safe assuming noninitialized vars are initialized before their first use *)
fun defaultValue ty =
    let open TTS
    in case ty of
 	   BoolType => BoolExp false
 	 | NatType => IntExp (0,ty)
 	 | IntType => IntExp (0,ty)
 	 | IntervalType (f,_) => IntExp(f,ty)
 	 | EnumType cl => EnumExp(hd cl,ty)
 	 | UnionType ((c,NONE)::_) => ConExp (c,ty)
 	 | UnionType ((c,SOME cty)::_) => ConstrExp (c,defaultValue cty,ty)
 	 | RecordType ftyl => RecordExp (map (fn (f,ty) => (f,defaultValue ty)) ftyl,ty)
 	 | ArrayType (k,ety) => let val d = defaultValue ety in ArrayExp (List.tabulate (k,fn _ => d),ty) end
 	 | QueueType (k,ety) => QueueExp ([],ty)
 	 | _ => failwith "defaultValue"
     end;



(* converter *)

(* constants environment *)
structure StringMap = SplayMapFn (struct type ord_key = string val compare = String.compare end)
val CONSTS = ref (StringMap.empty : TTS.Exp StringMap.map);

(* for computing tags: *)
val trno = ref 0;

fun convProg (Program (dl, d)) =
    let val (dl',d') = 
	    case d of
		SOME (d' as Main (_,m)) => (case Option.valOf (List.find (fn ProcessDecl (_,s,_,_,_,_,_,_) => m = s
								     | ComponentDecl (_,s,_,_,_,_,_,_,_) => m = s
								     | _ => false) dl) of
					  ComponentDecl (_,s,pl,al,_,_,_,_,_) =>
					  if null pl andalso null al then (dl,d') else comperror "main not closed"
					| ProcessDecl (_,s,pl,al,_,_,_,_) => (* add dummy component declaration *)
					  if null pl andalso null al then
					      (dl @ [ComponentDecl (0,"_main_",[],[],[],[],[],NONE,
								    ParComp (0,SomePorts [],[(SomePorts [],InstanceComp ((0,m),[],[]))]))],
					       (Main (0,"_main_")))
					  else comperror "main not closed")
	      | _ => comperror "no main specified"
    in TTS.Program (List.mapPartial convDecl (dl' @ [d']))
    end

and convDecl d =
    case d of
        ConstantDecl (l,s,ty,e) => (CONSTS := StringMap.insert ((!CONSTS), s, convExp e); NONE)
      | ProcessDecl (_,s,pl,al,(ls,stl),vl,init,trl) =>
	(* temporary variables initialized to params passed by value *)	
	(trno := 0;
	 let val params = flat (map (fn (lsl,_,ty) => let val ty = convType ty in map (fn (_,a) => (a,ty)) lsl end) al)
	 in SOME (TTS.ProcessDecl (s,
				   flat (map (fn (lsl,_,ch) => let val ch = convChannel ch in map (fn (_,s) => (s,ch)) lsl end) pl),
				   params,
				   stl,
				   List.mapPartial (fn (ValArg s,ty) => SOME (s,ty,defaultValue ty) | _ => NONE) params,
				   flat (map (fn (lsl,ty,eo) => let val ty = convType ty
								in map (fn (_,s) => (s,ty,case eo of SOME e => convExp e | _ => defaultValue ty)) lsl
								end) vl),
				   [],
				   [],
				   let val (_,str,_)::_ = trl
				   in case init of SOME (_,s) => TTS.StSequence(convInitStm s,TTS.StTo [str]) | _ => TTS.StTo [str]
				   end,
				   flat (map (convTransition s) trl)))
	 end)
      | ComponentDecl (_,s,pl,al,vl,lpll,prl,init,comp) =>
	(let val params = flat (map (fn (lsl,_,ty) => let val ty = convType ty in map (fn (_,a) => (a,ty)) lsl end) al)
	 in SOME (TTS.ComponentDecl (s,
				     flat (map (fn (lsl,_,ch) => let val ch = convChannel ch in map (fn (_,s) => (s,ch)) lsl end) pl),
				     flat (map (fn (lsl,_,ty) => let val ty = convType ty in map (fn (_,a) => (a,ty)) lsl end) al),
				     List.mapPartial (fn (ValArg s,ty) => SOME (s,ty,defaultValue ty) | _ => NONE) params,
				     flat (map (fn (lsl,ty,eo) => let val ty = convType ty
								  in map (fn (_,s) => (s,ty,case eo of SOME e => convExp e | _ => defaultValue ty)) lsl
								  end)
					       vl),
				     flat (map (fn (lpl,b) => flat (map (fn (lsl,_,ch) =>
									    let val ch = convChannel ch
									    in map (fn (_,s) => (s,ch,b)) lsl
									    end)
									lpl))
					       lpll),
				     map (fn (l1,l2) => (map snd l1,map snd l2)) prl,
				     case init of SOME (_,s) => convInitStm s | _ => TTS.StNull,
				     convComp comp))
	 end)
      | Main (_,s) => SOME (TTS.Main s)
      | _ => NONE (* TypeDecl, ChannelDecl *)

and convBound b = (* computes max digits, for conversion to integers in emit *)
    let fun sz b = let val d = Real.toDecimal b in Int.max (length (#digits d),#exp d) - #exp d end
    in case b of
	   Open b => DIGITS := Int.max (!DIGITS, sz b)
	 | Closed b => DIGITS := Int.max (!DIGITS, sz b)
	 | _ => ();
       b
    end

and convTransition id (l,st,s) = (* adds tag and interval, eliminate Selects *)
    let val sl = elimSelect (convStm s)
    in map (fn s' =>
	       (trno := 1 + !trno;
		((["trans_"^id^(Int.toString (!trno))],""),[st],normSequence s',(Closed 0.0,Infinite),[]))) sl
    end

and convComp c =
    case c of
	InstanceComp ((_,id),pl,el) => TTS.InstanceComp (id,pl,map convExp el)
      | ParComp (l,SomePorts [],scl) => TTS.ParComp (map (fn (SomePorts s2,c) => (s2,convComp c)) scl)
      | _ => failwith "ill-defined ParComp in convComp"

and convExp e = (* ~ fiacre 2.0, except for specializations *)
    case e of
	CondExp (c,e1,e2,_) => TTS.CondExp(convExp c,convExp e1,convExp e2)
      | InfixExp (ope,ref ty,e1,e2) => TTS.InfixExp (ope,convType ty,convExp e1,convExp e2)
      | BinopExp (ope,ref ty,e1,e2) => TTS.BinopExp (ope,convType ty,convExp e1,convExp e2)
      | UnopExp (ope,ref ty,e) => TTS.UnopExp (ope,convType ty,convExp e)
      | RecordExp (fel,ref ty) => TTS.RecordExp (map (fn (f,e) => (f,convExp e)) fel, convType ty)
      | QueueExp (el,ref ty) => TTS.QueueExp (map convExp el,convType ty)
      | ArrayExp (el, ref ty) => TTS.ArrayExp (map convExp el,convType ty)
      | RefExp (s,ref ty) => TTS.RefExp (s,convType ty)
      | ConstrExp (c,e,ref ty) => TTS.ConstrExp (c,convExp e,convType ty)
      | IntExp (n,ref ty) => TTS.IntExp (n,convType ty)
      | BoolExp b => TTS.BoolExp b
      | IdentExp (s,ref (VarKind,ty)) => TTS.VarExp (s,convType ty)
      | IdentExp (s,ref (ConstrKind,ty)) =>
	(case convType ty of
	     ty' as TTS.EnumType _ => TTS.EnumExp (s,ty')
	   | ty' as TTS.UnionType _ => TTS.ConExp (s,ty'))
      | IdentExp (s,ref (ConstantKind,ty)) => Option.valOf (StringMap.find ((!CONSTS), s))
      | IdentExp (s,ref (OpenKind,ty)) => failwith "unexpected open ident"
      | AnyExp => TTS.AnyExp
      | ArrayAccessExp (a,i,ref (ArrayType (_,ty))) => (* BEWARE: in frontend: ty is type of a, in backend: ty is type of elements *)
	TTS.ArrayAccessExp (convExp a,convExp i,convType ty)
      | RecordAccessExp (r,f,ref (RecordType ftyl)) => (* BEWARE: in frontend: ty is type of a, in backend: ty is type of elements *)
	let val ty = #2 (Option.valOf (List.find (fn (fl,_) => mem f fl) ftyl))
	in TTS.RecordAccessExp (convExp r,f,convType ty)
	end
      | ParenExp e => convExp e
      | _ => failwith "unexpected exp"

(* typer guarantees no Comms {nor StTo for  components} *)
(* frac only admits deterministic only init statements, but possibly with "to" *)
and convInitStm s = 
    case s of
	StNull => TTS.StNull
      | StCond (l,e,s1,[],so) => TTS.StCond (convExp e,convInitStm s1,case so of SOME s' => convInitStm s' | _ => TTS.StNull)
      | StCond (l,e,s1,(l',e',s')::t,so) => TTS.StCond (convExp e,convInitStm s1,convInitStm (StCond (l',e',s',t,so)))
      | StSequence (l,s1,s2) => TTS.StSequence (convInitStm s1,convInitStm s2)
      | StTo (l,str) => TTS.StTo [str]
      | StWhile (l,e,s) => if determSt s then TTS.StWhile (convExp e, convInitStm s) else
			   notyet "nondeterministic while statement"
      | StForeach (l,str,ref ty,s) => let val s' = convInitStm s
				      in if determSt s andalso not (mem str (writtenInSt s'))
					 then TTS.StForeach (VarExp(str,convType ty),s')
					 else unrollForeach (TTS.StForeach (VarExp(str,convType ty),s'))
				      end
      | StAssign (l,pl,el) => expandAssign (map convExp pl,map convExp el)
      | StCase (l,e,lbpsl) => expandCase (convExp e,map (fn (_,_,e,s) => (convExp e,convInitStm s)) lbpsl)
      | _ => notyet "nondeterministic init statement"

and convStm s =
    case s of
	StNull => TTS.StNull
      | StCond (l,e,s1,[],so) =>
	let val s' = TTS.StCond (convExp e,convStm s1,case so of SOME s' => convStm s' | _ => TTS.StNull)
	in if determSt s then s' else expandCond s'
	end
      | StCond (l,e,s1,(l',e',s')::t,so) =>
	let val s' = TTS.StCond (convExp e,convStm s1,convStm (StCond (l',e',s',t,so)))
	in if determSt s then s' else expandCond s'
	end
      | StSelect sl => TTS.StSelect (map convStm sl)
      | StSequence (l,s1,s2) => TTS.StSequence (convStm s1,convStm s2)
      | StTo (l,str) => TTS.StTo [str]
      | StWhile (l,e,s) => if determSt s then TTS.StWhile (convExp e, convStm s) else
			   notyet "nondeterministic while statement"
      | StForeach (l,str,ref ty,s) => let val s' = convStm s
				      in if determSt s andalso not (mem str (writtenInSt s'))
					 then TTS.StForeach (VarExp(str,convType ty),s')
					 else unrollForeach (TTS.StForeach (VarExp(str,convType ty),s'))
				      end
      | StAssign (l,pl,el) => expandAssign (map convExp pl,map convExp el)
      | StAny (l,pl,ref tyl,eo) => expandAny (map convExp pl,case eo of SOME e => SOME (convExp e) | _ => NONE)
      | StSignal (l,str,_) => TTS.StSignal str
      | StInput (l,str,ref (ProfileChannel tyl),pl,eo) =>
	expandInput (str,map convType tyl,map convExp pl,case eo of SOME e => SOME (convExp e) | _ => NONE)
      | StOutput (l,str,ref (ProfileChannel tyl),el) =>	TTS.StOutput (str,map convType tyl,map convExp el)
      | StCase (l,e,lbpsl) => expandCase (convExp e,map (fn (_,_,e,s) => (convExp e,convStm s)) lbpsl)
      | _ => failwith "unexpected statement in convStm"

and convChannel ch =
    case ch of
	ProfileChannel tyl => map convType tyl
      | _ => failwith "unexpected named channel"

(* fails if type currently unsupported *)
and convType ty =
    case ty of
	BoolType => TTS.BoolType
      | NatType => TTS.NatType
      | IntType => TTS.IntType
      | NamedType s => TTS.NamedType s
      | IntervalType (IntExp (l,_),IntExp(h,_)) => TTS.IntervalType (l,h)
      | UnionType ctyl =>
	if List.all (fn (_,BotType) => true | _ => false) ctyl
	then TTS.EnumType (flat (map fst ctyl))
	else TTS.UnionType (flat (map (fn (cl,BotType) => map (fn c => (c,NONE)) cl
					| (cl,ty) => let val ty' = convType ty in map (fn c => (c,SOME ty')) cl end) ctyl))
      | RecordType ftyl => TTS.RecordType (flat (map (fn (fl,ty) => let val ty' = convType ty in map (fn f => (f,ty')) fl end) ftyl))
      | ArrayType (IntExp(k,_),ty) => TTS.ArrayType (k,convType ty)
      | QueueType (IntExp(k,_),ty) => TTS.QueueType (k,convType ty)
      | _ => failwith "unexpected type";

fun toTTS p = convProg p handle BYE => raise BYE | _ => failwith "toTTS";

end;


