
(* rm -rf d, if it exists *)
fun removeDir d =
    (case hosttype of
	"i386-linux" => (OS.Process.system ("rm -rf "^d); ())
      | _ => (* as it should be; but fails on linux : *)
	let open OS.FileSys
	in if isDir d handle _ => false then
	       (let val stream = openDir d
		in while case readDir stream of
			     SOME f => (remove (OS.Path.joinDirFile {dir=d,file=f}); true)
			   | _ => false do ();
		   rmDir d
		end
		handle _ => (print ("cannot remove directory "^d^"\n"); raise BYE))
	   else if (TextIO.openIn d; true) handle _ => false then
	       (remove d handle _ => (print ("cannot remove file "^d^"\n"); raise BYE))
	   else
	       ()
	end)
    handle _ => ();
 

(* main *)
fun top () =
	let val args = CommandLine.arguments ()
	    val () = command args
	in (* check/set error stream *)
	   case !ErrPrint.name of
		  "" => ()
		| f  => let val f = OS.Path.mkCanonical f
			in ErrPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE)
			end;
	   (* check/set input stream *)
	   case !Input.name of
		  "" => ()
		| f  =>	let val f = OS.Path.mkCanonical f
			in case OS.Path.ext f of
				  SOME e => if mem e ["fcr"] then () else (print ("unknown format for input file: "^f^"\n"); raise BYE)
				| e      => (print ("unknown format for input file: "^f^"\n"); raise BYE);
		   	   Input.FILE := TextIO.openIn f handle x => (print ("cannot open "^f^"\n"); raise BYE)
			end;

	   (* check/set output stream *)
	   case !OutPrint.name of
		  "" => (case !OUTPUT of
                                 TTS => (print ("cannot create tts on standard output, please specify outfile\n"); raise BYE)
                               | _ => ())
		| f  =>	let val f = OS.Path.mkCanonical (strip f)
			in case !OUTPUT of
			       NoOutput => (case OS.Path.ext f of
						SOME "fcr" => (OUTPUT := Fiacre;
                                                               OutPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE))
                                              | SOME "tts" => (OUTPUT := TTS;
                                                               (* create f dir; files will be created by printer *)
                                                               removeDir f;      (* rm -rf f *)
                                                               OS.FileSys.mkDir f handle x => (print ("cannot create "^f^"\n"); raise BYE))
					      | SOME "lotos" => (print ("cannot generate Lotos code, consider flac\n"); raise BYE)
					      | _ => (print ("unknown format for output file: "^f^"\n"); raise BYE))
			     | Fiacre => (OutPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE))
			     | TTSText => (OutPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE))
			     | TTS => (removeDir f;      (* rm -rf f *)
                                       OS.FileSys.mkDir f handle x => (print ("cannot create "^f^"\n"); raise BYE))
			     | LOTOS => (print ("cannot generate Lotos code, consider flac\n"); raise BYE)
			end;
	   (* go *)
	   let val (p,p') = Type.typecheck (Parse.parse ())
	   in Init.initcheck p';
              case !OUTPUT of
		  NoOutput => ()
		| Fiacre => Print.print (if !TRANSF then p' else p)
		| _ => Compile.compile p'
	   end;
	   (* exit *)
	   ErrPrint.flush (); OutPrint.flush (); OS.Process.exit OS.Process.success
	end
	handle BYE => (ErrPrint.flush (); OutPrint.flush (); OS.Process.exit OS.Process.failure);


(* making the application (for mlton) ----------------------------------- *)

(top (); ());

