
#include "avl.h"

#ifndef NULL
#define NULL 0
#endif

/* since some compilers complain if compare_value and free_value are not defined,
   these are defined from references assigned in init_storage. */
int (*COMPARE)();
int (*FREE)();

int init_storage (void *compare, void *free) {
  COMPARE = compare;
  FREE = free;
  return 0;
}

#define compare_value(x,y) ((*COMPARE)(x,y))
#define free_value(x) ((*FREE)(x))



/* storage */

typedef struct avl {
  short path;
  short balance;
  key key;
  struct avl *left;
  struct avl *right;
} avl;

avl *storage = NULL;


/* SearchInsert (combines lookup and add) */

#define AllocNode(e)            {node = (avl *)malloc(sizeof(avl)); \
                                 node->path=0; node->key=(key)(e); \
                                 node->balance=0; \
                                 node->left=NULL; node->right=NULL;}
/* Rotate*: use P, PP, TMP as temporaries. */
#define RotateRight(Q)          {P=Q->left; PP=P->right; \
			         P->right=Q; Q->left=PP; Q=P;}
#define RotateLeftRight(Q)      {TMP=Q->left; RotateLeft(TMP); \
				 Q->left=TMP; RotateRight(Q);}
#define RotateLeft(Q)           {P=Q->right; PP=P->left; \
			         P->left=Q; Q->right=PP; Q=P;}
#define RotateRightLeft(Q)      {TMP=Q->right; RotateRight(TMP); \
				 Q->right=TMP; RotateLeft(Q);}

key SearchInsert (void *e)
{
  avl *P=storage;     /* moves down the tree                */
  avl *PP=NULL;       /* father of P                        */
  avl *A=P;           /* deepest on path with non-0 balance */
  avl *AA=NULL;       /* father of A                        */
  avl *TMP;
  avl *node;

  /* search: */ 
  if (P==NULL) {
    /* avl is empty */
    AllocNode(e);
    storage = node;
    return ((key)e);
  }
  
  do {
    if (P->balance!=0) {A=P; AA=PP;}
    switch (compare_value((void *)(P->key),e)) {
    case  1 : P->path=1; PP=P; P=P->left; break;
    case  0 : free_value(e); return(P->key);
    case -1 : P->path=0; PP=P; P=P->right; break;
    }
  } while (P!=NULL);

  /* insertion at PP: */
  
  AllocNode(e);
  if (PP->path) {PP->left=node;} else {PP->right=node;}

  /* update balance between A and node: */
  P=A;
  while (P!=node) {
    if (P->path) {
      P->balance=P->balance+1; P=P->left;
    } else {
      P->balance=P->balance-1; P=P->right;
    }
  }

  /* re-balance: */
  switch (A->balance) {
  case 2 : 
    switch ((A->left)->balance) {
    case 1 : 
      RotateRight(A); 
      A->balance=0;
      (A->right)->balance=0;
      break;
    case -1 :
      RotateLeftRight(A);
      switch (A->balance) {
      case  1 : (A->left)->balance=0; (A->right)->balance= -1; break;
      case  0 : (A->left)->balance=0; (A->right)->balance=0;  break;
      case -1 : (A->left)->balance= -1; (A->right)->balance=0; break;
      }
      A->balance=0;
      break;
    }
    break;
  case -2 :
    switch ((A->right)->balance) {
    case -1 :
      RotateLeft(A); 
      A->balance=0;
      (A->left)->balance=0;
      break;
    case  1 :
      RotateRightLeft(A);
      switch (A->balance) {
      case  1 : (A->left)->balance=0; (A->right)->balance=1; break;
      case  0 : (A->left)->balance=0; (A->right)->balance=0;  break;
      case -1 : (A->left)->balance=1; (A->right)->balance=0; break;
      }
      A->balance=0;
      break;
    }
    break;
  default: return((key)e);
  }

  /* update link to A: */
  if (AA==NULL) {
    storage = A;
  } else {
    if (AA->path) {
      AA->left=A;
    } else {
      AA->right=A;
    }
  }

  return((key)e);
}

