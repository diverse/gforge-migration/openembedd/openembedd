

signature POSTCOMPOSE = sig
    val postcompose : TTS.TTS -> TTS.TTS
end;
 


structure PostCompose : POSTCOMPOSE = struct


(* attaches time constraints to tts transitions *)
fun insertTime (ProcessDecl (b,c,d,e,k,f,h,i,init,g))=
    let fun getPortName s1 =
	    case s1 of
		StSequence(a1,a2) => (case getPortName a1 of  "" => getPortName a2 | st => st)
	      | StSignal pn=> pn
	      | StInput(pn,_,_)=> pn
	      | StInputList(pn,_) => pn
	      | StOutput(pn,_,_)=> pn
	      | _ => ""
	fun updateBound (tag,st,stm,bnd,prl) = 
            let val port = getPortName stm
	    in case List.find (fn (x,_,_) => x=port) h of SOME (_,_,tc) => (tag,st,stm,tc,prl) | NONE => (tag,st,stm,bnd,prl)
            end
    in ProcessDecl (b,c,d,e,k,f,[],i,init,map updateBound g) handle BYE => raise BYE | _ => failwith "insertTime" 
    end
  |  insertTime d = d;



(* replaces communication statements by null, after composition *)
(* also concats tags and lab into a single name for the transition *)
(* also builds relation priotity, replacing elem tags by transition names *)
fun removePorts (ProcessDecl (b,c,d,e,k,f,h,i,init,trl))=
    let val L = ref ""
	fun rmIO s =
	    case s of
		StSignal p => (L := p; StNull)
	      | StOutput (p,_,_) => (L := p; StNull)
	      | StInput (p,_,_) => comperror ("open input on port "^p)
	      | StInputList (p,_) => comperror ("open input on port "^p)
	      | _ => s
	(* maps elementary tags to transition names *)
	(* should use a tree rather than an a-list ? *)
	val dict = ref []
	fun record tg name =
	    case List.find (fn (g,_) => g=tg) (!dict) of
		SOME (_,r) => r := name :: !r | _ => dict := (tg,ref [name]) :: !dict
	fun get tg =
	    let val ref l = assoc tg (!dict) in l end
        (* first pass: remove ports, build transition names and record them in dict *)
	val trl' = map (fn ((tags,lab),fr,stm,bnd,prl) =>
			   let val trname = String.concatWith "_" tags
			   in List.app (fn tg => record tg trname) tags;
			      (([trname],lab),fr,mapStm rmIO stm,bnd,prl)
			   end) trl
        (* second pass: update priority relation (replace tags by transition names) *)
	val trl'' = map (fn (namelab,fr,stm,bnd,prl) => (namelab,fr,stm,bnd,makeset (flat (map get prl)))) trl'
    in ProcessDecl (b,c,d,e,k,f,h,i,init,trl'')
       handle BYE => raise BYE | _ => failwith "removePorts"
    end
  | removePorts d = d;




(* moves conditional statements after assignements in front. *)
(* general form (S is state):
      S := f S; if c S then st end ==> if c (f S) then S := f S; st end
   example 1: (independant)
      Z := 3; if X>Y then s end ==> if X>Y then Z := 3; s end
   example 2: (not independant), with analysis: (not implemented)
      a[z]:=3; if a[x]>a[y] then s end ==> if ((x=z) ? 3 : a[x]) > ((y=z) ? 3 : a[y]) then a[z] := 3; s end
   example 2.2: (not independant), with state copying:
      a[z]:=3; if a=b then s end ==> if (a[z]:=3;a=b) then s end
   example 3: (not independant but substitutable)
      X := Y+3; if X>Y then s end ==> if Y+3>Y then X := Y+3; s end
*)
fun normGuards (ProcessDecl (b,c,d,e,k,f,h,i,init,g)) =
    let fun swap s =
	    let fun swapGuard s =
		    case s of
			StSequence(s1 as StAssign(p,e),ss as StSequence(StGuard c,s2)) =>
			if equal p e then ss else
			let val ws1 = writtenInSt s1
			    val rc = readInExp c
			    val rs1 = readInSt s1
			    val wc = writtenInExp c
			in if List.all (fn v => not (mem v wc)) rs1 andalso     (* W(c) inter R(s1) empty *)
			      List.all (fn v => not (mem v ws1)) rc andalso     (* R(c) inter W(s1) empty *)
			      List.all (fn v => not (mem v wc)) ws1             (* W(c) inter W(s1) empty *)
			   then (* independant *)
			        StSequence (StGuard c, swapGuard (StSequence(s1,s2)))
			   else (* *)
			        if List.all (fn v => not (mem v wc)) (rs1 @ ws1) andalso (* (R(s1) union W(s1)) inter W(c) empty *)
				   not (matchExp (fn LetExp _ => true | _ => false) e)   (* in case several instances of p in e *)
				   (* ### additional restrictions ? #### single # of p, e "simple" ? ### *)
			        then (* substitutable *)
				     let val c' = mapExp (fn x => if equal p x then e else x) c
				     in (* for debug: 
					let open ErrPrint
					    open ErrTtsEmit
					in print "\n\n----------------\n";
					   print " p  = "; print_Exp p; print "\n";
					   print " e  = "; print_Exp e; print "\n";
					   print " c  = "; print_Exp c; print "\n";
					   print " c' = "; print_Exp c'; print "\n";
					   flush ()
					end;
					*)
					StSequence (StGuard c',swapGuard (StSequence(s1,s2)))
				     end
				else (* general case  *)
				     (* *)
				     StSequence (StGuard (LetExp(s1,c)),swapGuard (StSequence(s1,s2)))
			end
		      | StSequence(s1 as StCond _,ss as StSequence(StGuard c,s2)) =>
			let val ws1 = writtenInSt s1
			    val rc = readInExp c
			    val rs1 = readInSt s1
			    val wc = writtenInExp c
			in if List.all (fn v => not (mem v wc)) rs1 andalso
			      List.all (fn v => not (mem v ws1)) rc andalso
			      List.all (fn v => not (mem v wc)) ws1
			   then (* independant *)
			        StSequence (StGuard c, swapGuard (StSequence(s1,s2)))
			   else StSequence (StGuard (LetExp(s1,c)),swapGuard (StSequence(s1,s2)))

			end
		      | StSequence(s1 as StWhile _,ss as StSequence(StGuard c,s2)) =>
			let val ws1 = writtenInSt s1
			    val rc = readInExp c
			    val rs1 = readInSt s1
			    val wc = writtenInExp c
			in if List.all (fn v => not (mem v wc)) rs1 andalso
			      List.all (fn v => not (mem v ws1)) rc andalso
			      List.all (fn v => not (mem v wc)) ws1
			   then (* independant *)
			        StSequence (StGuard c, swapGuard (StSequence(s1,s2)))
			   else StSequence (StGuard (LetExp(s1,c)),swapGuard (StSequence(s1,s2)))
			end
		      | StSequence(s1 as StForeach _,ss as StSequence(StGuard c,s2)) =>
			let val ws1 = writtenInSt s1
			    val rc = readInExp c
			    val rs1 = readInSt s1
			    val wc = writtenInExp c
			in if List.all (fn v => not (mem v wc)) rs1 andalso
			      List.all (fn v => not (mem v ws1)) rc andalso
			      List.all (fn v => not (mem v wc)) ws1
			   then (* independant *)
			        StSequence (StGuard c, swapGuard (StSequence(s1,s2)))
			   else StSequence (StGuard (LetExp(s1,c)),swapGuard (StSequence(s1,s2)))
			end
		      | s => s
	    in mapStm swapGuard (ToTTS.normSequence s)  (* normSequence needed for e.g. abp.fcr, due to removePorts introducing StNull ? *)
	    end
	(* groups conditions in embedded guarded statements *)
	fun group s =
	   let fun grpGuard s =
		    case s of
		      StSequence(StNull,s') => grpGuard s'
	  	    | StSequence(s',StNull) => grpGuard s'
	  	    | StSequence (StGuard e1,StSequence (StGuard e2,s2)) => grpGuard (StSequence (StGuard(InfixExp(AND,BoolType,e1,e2)),s2))
	  	    | _ => s
	   in mapStm grpGuard s
	   end
    in ProcessDecl (b,c,d,e,k,f,h,i,init,map (fn (tg,fr,s,b,p) => (tg,fr,group (swap s),b,p)) g)
       handle BYE => raise BYE | _ => failwith "normGuards"
    end

 | normGuards d = d;



(* analyzes usage of variables:
  - removes unused variables
  - makes temporaries assigned and part of state local variables
  - makes loca variables not part of state temporary variables
  - inlines temporaries not assigned and of basic types
 A variable is part of state iff
  - some transition reads it before writing it (if it does)
  - some (possibly the same) transition writes it
 also:
  - simplifies expressions after inlining
  - removes trivially dead transitions
*)
fun simplify (d as ProcessDecl (name,p,a,states,tmpvars,locvars,locports,prio,init,trl)) =
    let val REMOVED = ref false (* true if some useless assignement(s) removed *)
	fun moveVariables (ProcessDecl (name,p,a,states,tmpvars,locvars,locports,prio,init,trl)) =
	    let (* encode usages of variables in process.
		   State means "read by t (before being written in t) and written in t' (possibly t=t') *)
		datatype Usage = Unused | Read | Write | WritePartial | WriteRead | State
		(* accumulate usages of variables in the different transitions *)
		val accu = ref (map (fn (s,_,_) => (s,Unused)) tmpvars @ map (fn (s,_,_) => (s,Unused)) locvars)
		fun merge all = (*  v has State if one transition at least reads it (before writing it)
		                    and some transition writes it (possibly the same) *)
		    accu := List.map (fn (N as (s,ref new),O as (_,old)) =>
					 case (new,old) of (_             ,Unused)       => (s,new)
							 | (State         ,_)            => (s,State)
							 | (Write         ,Read)         => (s,State)
							 | (WritePartial  ,Read)         => (s,State)
							 | (WriteRead     ,Read)         => (s,State)
							 | (WriteRead     ,Write)        => (s,WriteRead)
							 | (Read          ,Write)        => (s,State)
							 | (Read          ,WritePartial) => (s,State)
							 | (WriteRead     ,WritePartial) => (s,WriteRead)
							 | (Read          ,WriteRead)    => (s,State)
							 | _                             => O) (ListPair.zip (all,!accu))

		(* updates usage info for s within a transition *)
		fun record s new all ty =
		    let val r as ref old = assoc s all
		    in case (new,old) of (Read        ,Write)        => r := WriteRead
				       | (Read        ,WritePartial) => r := State   (* since R/W may concern different fields *)
				       | (Write       ,Read)         => r := State
				       | (WritePartial,Read)         => r := State
				       | (_           ,Unused)       => r := new
				       | _                           => ()
		    end
		(* updates variable usage in an expression, called by analyze *)
		fun usage all e m =
		    case e of
		        VarExp (v,ty) => record v m all ty
  		      | InfixExp(_,_,e1,e2) => (usage all e1 m; usage all e2 m)
  		      | BinopExp(_,_,e1,e2) => (usage all e1 m; usage all e2 m)
  		      | UnopExp(_,_,e) => usage all e m
		      | RecordExp (fl,_) => List.app (fn (_,e) => usage all e m) fl
		      | ArrayExp (el,_) => List.app (fn e => usage all e m) el
		      | QueueExp (el,_) => List.app (fn e => usage all e m) el
		      | ArrayAccessExp (e1,e2,_) => (usage all e2 Read; usage all e1 (case m of Write => WritePartial | _ => m))
		      | RecordAccessExp (e,_,_) => usage all e (case m of Write => WritePartial | _ => m)
		      | CondExp(c,e1,e2) => (usage all c m; usage all e1 m; usage all e2 m)
		      | LetExp(s,e) => (analyze all s; usage all e Read)
		      | ConstrExp(_,e,_) => usage all e Read
		      | DestrExp (_,e) => usage all e Read
		      | MatchesExp (c,e) => usage all e Read
		      | _ => ()
		(* for each variable in statement s, computes usage list for variables *)
		and analyze all s =
		    case s of
			StSequence (s1,s2) => (analyze all s1; analyze all s2)
		      | StWhile(e,s) => (usage all e Read; analyze all s)
		      | StForeach(v,s) => (usage all v Write; analyze all s)
		      | StCond(e,s1,s2) => (usage all e Read; analyze all s1; analyze all s2)
		      | StAssign (p,e) => (usage all e Read; usage all p Write) (* pattern matching assumed removed *)
		      | StGuard e => usage all e Read
		      | _ => ()
	    in (* analyze usage of variables *)
		List.app (fn (_,_,s,_,_) => let val all = map (fn (x,_) => (x,ref Unused)) (!accu) in analyze all s; merge all end) trl;
		let val len = length tmpvars
		    val tmp = List.take (!accu,len)
		    val loc = List.drop (!accu,len)
		    val (tmpunused,tmpused) = List.partition (fn ((s,_,_),(_,Unused)) => true
							       | ((s,_,_),(_,Write)) => true
							       | ((s,_,_),(_,WritePartial)) => true
							       | _ => false)
							     (ListPair.zip (tmpvars,tmp))
		    val (tmp2loc,tmpleft) = List.partition (fn ((s,_,_),(_,State)) => true | _ => false) tmpused
		    val (locunused,locused) = List.partition (fn ((s,_,_),(_,Unused)) => true
							       | ((s,_,_),(_,Write)) => true
							       | ((s,_,_),(_,WritePartial)) => true
							       | _ => false)
							     (ListPair.zip (locvars,loc))
		    val (locleft,loc2tmp) = List.partition (fn ((s,_,_),(_,State)) => true | _ => false) locused
		    val unused = map (fn ((s,_,_),_) => s) (tmpunused @ locunused)
		    val newloc = tmp2loc @ locleft
		    val (inlined,newtmp) =  List.partition (fn ((_,ty,_),(_,Read)) =>
							       (case ty of BoolType => true
									 | IntType => true | NatType => true | IntervalType _ => true
									 | EnumType _ => true | UnionType _ => true
									 | _ => false)
							     | _ => false) (tmpleft @ loc2tmp)
                    (* for debug:
		    val _ =
			let open ErrPrint
			in print "----------------\n";
			   print "unused  ="; List.app (fn ((s,_,_),_) => (print s; print " ")) (tmpunused @ locunused); print "\n"; flush ();
			   print "inlined ="; List.app (fn ((s,_,_),_) => (print s; print " ")) inlined; print "\n"; flush ();
			   print "newtmp  ="; List.app (fn ((s,_,_),_) => (print s; print " ")) newtmp; print "\n"; flush ();
			   print "newloc  ="; List.app (fn ((s,_,_),_) => (print s; print " ")) newloc; print "\n"; flush ()
			end
		    *)
		    (* inlines inlined variables and simplifies expressions *)
		    exception DeadTrans
		    fun letExpTail (LetExp (_,e)) = letExpTail e | letExpTail e = e
 		    (* simplifies exp and removes assignements involving unused *)
 		    fun inlinee env e =
 			let val e' = expSimpl env e
 			in mapExp (fn e1 as LetExp (s,e2) =>
 				      (case mapStm (fn s as StAssign(p,e) =>
						      (* remove assignements of written-only variables *)
 						      if matchExp (fn VarExp (s,_) => mem s unused | _ => false) p then StNull else s
 						    | s => s) s of
 					  StNull => e2
 					| s' => LetExp(s',e2))
 				    | e => e) e'
 			end
 		    (* simplifies statement *)
		    fun inline env s =
			case s of
			    StGuard(e as LetExp _) => let val e' = inlinee env e
							in case letExpTail e' of
							       BoolExp false => raise DeadTrans
							     | _ => StGuard e'
							end
			  | StGuard e => (case inlinee env e of
						   BoolExp true => StNull
						 | BoolExp false => raise DeadTrans
						 | e' => StGuard e')
			  | StSequence(s1,s2) => (case (inline env s1,inline env s2) of
						      (StNull,s2') => s2'
						    | (s1',StNull) => s1'
						    | (s1',s2') => StSequence(s1',s2'))
			  | StWhile(e,s) => (case inlinee env e of
						 BoolExp true => comperror "loop detected in while statement"
					       | BoolExp false => inline env s
					       | e' => StWhile(e',inline env s))
			  | StForeach(v,s) => StForeach(v,inline env s)
			  | StCond(e,s1,s2) => (case inlinee env e of
						 BoolExp true => inline env s1
					       | BoolExp false => inline env s2
					       | e' => StCond(e',inline env s1,inline env s2))
			  | StAssign(p,e) =>
			    let val p' = inlinee env p
			    in (* remove assignements of written-only variables *)
			       if matchExp (fn VarExp (v,_) => mem v unused | _ => false) p'
			       then (REMOVED := true; StNull)
			       else StAssign (p',inlinee env e)
			    end
			  | StTo sl => StTo (isort sl String.>)
			  | _ => s
		    val inlinenv = map (fn ((s,_,v),_) => (s,v)) inlined
		in ProcessDecl (name,p,a,states,
				map fst newtmp,
				map fst newloc,
				locports,prio,
				let val StTo e = init in StTo (isort e String.>) end,
				List.mapPartial (fn (tg,fr,s,b,p) =>
					SOME (tg,isort fr String.>,inline inlinenv s,b,p) handle DeadTrans => (REMOVED := true; NONE)) trl)
		end handle BYE => raise BYE | _ => failwith "moveVariables"
	    end
	  | moveVariables d = d

	(* removes equal tansitions *)
        fun prune (ProcessDecl (name,p,a,states,tmpvars,locvars,locports,prio,init,trl')) =
		   let fun toInt b = Real.trunc (b * (pow (10.0,!DIGITS)))
		       fun eqBounds (Infinite,Infinite) = true
			 | eqBounds (Closed b1,Closed b2) = toInt b1 = toInt b2
			 | eqBounds (Open b1,Open b2) = toInt b1 = toInt b2
			 | eqBounds _ = false
		       fun eqtr (_,fr,s,(b1,b2),prl1) (_,fr',s',(b1',b2'),prl2) =
			   fr=fr' andalso s=s' andalso eqBounds(b1,b1') andalso eqBounds(b2,b2') (* #### and for prio lists ? #### *)
		       fun mkset [] = [] | mkset (h::t) = h :: mkset (List.filter (fn tr => not (eqtr h tr)) t)
		   in ProcessDecl (name,p,a,states,tmpvars,locvars,locports,prio,init,mkset trl')
		   end

	(* apply recursively until no transition or statement removed *)
	fun recMove d = (REMOVED := false;
			 let val d' = moveVariables d
			 in if !REMOVED then recMove d' else prune d'
			 end);

    in recMove d
    end
  | simplify d = d;



(* normalizes usage of explicit structures (ArrayExp/RecordExp/QueueExp and primitives returning queues) in
   statements and expressions for C generation. Introduces StBlock *)
fun normStructures (ProcessDecl(b,c,d,e,k,f,h,i,init,g)) =
    let val UNIQUE = ref 0;
	(* new safe since all fiacre variables contains a "_" : *)
	fun new s = let val u = !UNIQUE in UNIQUE := 1 + !UNIQUE; s ^ Int.toString u end;
	fun zap () = UNIQUE := 0
	fun normSt s =
	    let val decs = ref []
		fun fls s =
		    case s of (* first three cases handled as special case in emit *)
			StAssign(p,RecordExp (fl,ty)) => StAssign(fle p,RecordExp (map (fn (f,e) => (f,fle e)) fl,ty))
		      | StAssign(p,ArrayExp(el,ty)) => StAssign(fle p,ArrayExp(map fle el,ty))
		      | StAssign(p,QueueExp(el,ty)) => StAssign(fle p,QueueExp(map fle el,ty))
		      | StAssign(p,e) => StAssign(fle p,fle e)
		      | StGuard e => StGuard(fle e)
		      | StSequence(s1,s2) => StSequence(normSt s1,normSt s2)
		      | StWhile(e,s) => StWhile(fle e,fls s)
		      | StForeach(v,s) => StForeach(fle v,fls s)
		      | StCond(c,s1,s2) => StCond(fle c,fls s1,fls s2)
		      | _ => s
		and fle e =
		    case e of
  			InfixExp (ope,ty,e1,e2) => InfixExp (ope,ty,fle e1,fle e2)
  		      | BinopExp (ope,ty as QueueType(_,ety),e1,e2) => (* ope is enqueue or append *)
			let val q = new "q"
			    val e = new "e"
			in decs := (q,ty) :: (e,ety) :: !decs;
			   LetExp (StAssign (VarExp (e,ety),fle e2),
				   LetExp (StAssign(VarExp(q,ty),BinopExp (ope,ty,fle e1,VarExp(e,ety))),
					   VarExp(q,ty)))
			end
		      | UnopExp (DEQUEUE,ty,e) =>
			let val q = new "q"
			in decs := (q,ty) :: !decs;
			   LetExp (StAssign(VarExp(q,ty),UnopExp (DEQUEUE,ty,fle e)),
				   VarExp (q,ty))
			end
		      | UnopExp (FIRST,ty as QueueType(s_,ety),e) =>
			let val f = new "e"
			in decs := (f,ety) :: !decs;
			   LetExp (StAssign(VarExp(f,ety),UnopExp (FIRST,ty,fle e)),
				   VarExp (f,ety))
			end
		      | UnopExp (ope,ty,e) => UnopExp (ope,ty,fle e)
		      | ArrayAccessExp (a,e,ty) => ArrayAccessExp (fle a,fle e,ty) 
		      | RecordAccessExp (r,f,ty) => RecordAccessExp (fle r,f,ty)
		      | CondExp (c,e1,e2) => CondExp (fle c,fle e1,fle e2) 
		      | LetExp (s,e) => LetExp (normSt s,fle e)
		      | RecordExp (fl,ty) =>
			let val r = new "r"
			in decs := (r,ty) :: !decs;
			   LetExp (StAssign(VarExp(r,ty),RecordExp (map (fn (f,e) => (f,fle e)) fl,ty)),
				   VarExp (r,ty))
			end
		      | ArrayExp (el,ty) =>
			let val a = new "a"
			in decs := (a,ty) :: !decs;
			   LetExp (StAssign(VarExp(a,ty),ArrayExp (map fle el,ty)),
				   VarExp (a,ty))
			end
		      | QueueExp (el,ty as QueueType(sz,ety)) =>
			let val q = new "q"
			in decs := (q,ty) :: !decs;
			   LetExp (StAssign(VarExp(q,ty),QueueExp (map fle el,ty)),
				   VarExp (q,ty))
			end
		      | ConExp (c,ty) =>
			let val C = new "c"
			in decs := (C,ty) :: !decs;
			   LetExp (StAssign(VarExp(C,ty),ConExp (c,ty)),
				   VarExp (C,ty))
			end
		      | ConstrExp (c,e,ty) =>
			let val C = new "c"
			in decs := (C,ty) :: !decs;
			   LetExp (StAssign(VarExp(C,ty),ConstrExp(c,fle e,ty)),
				   VarExp (C,ty))
			end
		      | DestrExp (c,e) => DestrExp (c,fle e)
		      | MatchesExp (c,e) => MatchesExp (c, fle e)
		      | _ => e
		val s' = fls s
	    in if null (!decs) then s' else StBlock (rev (!decs),s')
	    end;

    in ProcessDecl(b,c,d,e,k,f,h,i,init,map (fn (tag,fr,s,bnd,prl) => (zap (); (tag,fr,normSt s,bnd,prl))) g)
       handle BYE => raise BYE | _ => failwith "normStructures" 
    end
  | normStructures d = d;


fun postcompose (Program dl) =
    if !NOCOMP then Program dl else
    Program ((map normStructures o
	      map simplify o
	      map normGuards o
	      map removePorts o
	      map insertTime) dl);


end; (* PostCompose *)

