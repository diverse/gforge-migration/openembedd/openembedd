

(* prints the source by Prn *)
functor SrcPrint (Prn : PRN) = struct

open Prn;

fun newline n = (print "\n"; repeat n print " ");

fun print_Fiacre (Program (dl,main)) =
	(List.app (fn TypeDecl d => print_TypeDecl 0 d
	            | ChannelDecl d => print_ChannelDecl 0 d
	            | ConstantDecl d => print_ConstantDecl 0 d
	            | ProcessDecl d => print_ProcessDecl 0 d
	            | ComponentDecl d => print_ComponentDecl 0 d) dl;
	 case main of NONE => () | SOME (Main (_,id)) => (newline 0; print id; print "\n"))
and print_TypeDecl c (l,a,b) =
	(newline c; print "type "; print a; print " is "; print_Type b; newline 0)
and print_ChannelDecl c (l,a,b) =
	(newline c; print "channel "; print a; print " is "; print_Channel b; newline 0)
and print_Val e =
        let val d = !DETAILS in DETAILS := false; print_Exp e; DETAILS := d end
and print_Type ty = (case ty of
	  BoolType => print "bool"
	| NatType => print "nat"
	| IntType => print "int"
	| NamedType a => print a
	| IntervalType(a,b) => (print_Val a; print ".."; print_Val b)
	| UnionType a => print_list ("union "," | "," end") (fn (xl,BotType) => print_list ("",",","") print xl
							     | (xl,y) => (print_list ("",",","") print xl; print " of "; print_Type y)) a
	| RecordType [] => (* for printing type errors *) print "record ... end"
	| RecordType a 	=> print_list ("record ",", "," end") (fn (xl,y) => (print_list ("",",","") print xl; print ":"; print_Type y)) a
	| ArrayType (e,b) => (print "array "; print_Val e; print " of "; print_Type b)
	| QueueType (IntExp (~1,_),b) => (print "queue "; print "?"; print " of "; print_Type b)
	| QueueType (e,b) => (print "queue "; print_Val e; print " of "; print_Type b)
	(* for printing type errors: *)
	| AnyArray => print "array ... of ..."
	| AnyQueue => print "queue ... of ..."
	| BotType  => if !DETAILS then print "0" else print "?"
	| TopType => if !DETAILS then print "1" else print "?")
and print_Channel ch = (case ch of
	  NamedChannel a => print a
	| ProfileChannel [] => print "none"
	| ProfileChannel tyl => print_list (""," # ","") print_Type tyl)
and print_ConstantDecl i (_,n,ty,e) =
        (newline 0; print "const "; print n; print " : "; print_Type ty;
	 print " is "; print_Exp e; print "\n")
and print_ProcessDecl i (_,a,b,d,e,f,p,j) =
	(newline 0; print "process "; print a;
	 case b of [] => () | _ => (newline (i+4); print_list ("[",", ","]") print_PortDecl b);
	 case d of [] => () | _ => (newline (i+4); print_list ("(",", ",")") print_VarParam d);
	 newline (i+4); print "is";
         newline (i+4); print_StatesDecl e;
	 case f of [] => () | _ => (newline (i+4); print_list ("var ",", ","") print_VarDecl f);
	 case p of SOME (_,s) => (newline (i+4); print "init "; print_Statement (i+7) s) | _ => ();
	 app (print_Transition (i+4)) j;
	 newline 0)
and print_ComponentDecl i (_,a,b,d,e,f,g,p,h) =
	(newline 0; print "component "; print a;
	 case b of [] => () | _ => (newline (i+4); print_list ("[",", ","]") print_PortDecl b);
	 case d of [] => () | _ => (newline (i+4); print_list ("(",", ",")") print_VarParam d);
	 newline (i+4); print "is";
	 case e of [] => () | _ => (newline (i+4); print_list ("var ",", ","") print_VarDecl e);
	 case f of [] => () | _ => (newline (i+4); print_list ("port ",", ","") print_locPortDecl f);
         case g of [] => () | _ => (newline (i+4); print_list ("priority ",", ","") print_PriorDecl g);
	 case p of SOME (_,s) => (newline (i+4); print "init "; print_Statement (i+7) s) | _ => ();
	 newline (i+4); print_Composition (i+4) h true;
	 newline 0)
and print_locPortDecl (pprl,(left,right)) =
	(print_list ("",",","") print_PortDecl pprl; print_TimeConstraint (left,right))
and print_PortSet pl = (case pl of
	  AllPorts => print "*"
	| SomePorts l => print_list ("",",","") print l)
and print_Composition i c x = (case c of (* x true if starts at beginning of line *)
	  ParComp (_,pl,cl)  => (if x then () else newline i; print "par";
				 case pl of SomePorts [] => () | _ => (print " "; print_PortSet pl; print " in");
				 let val (pl,h)::t = cl
				     val no = ref false
				 in newline (i+3);
				    case pl of SomePorts [] => no:=true | _ => (print_PortSet pl; print " -> ");
				    print_Composition (i+3) h (!no);
				    List.app (fn (pl,c) =>
						(newline i; print "|| ";
						 case pl of SomePorts [] => no:=true | _ => (print_PortSet pl; print " -> ");
						 print_Composition (i+3) c (!no))) t
				 end;
				 newline i; print "end")
	| InstanceComp c => print_InstanceComp i c)
and print_InstanceComp i ((_,id),pl,al) =
	(print id;
	 if null pl then () else print_list (" [",",","]") print pl;
	 if null al then () else print_list (" (",",",")") print_Exp al)
and print_PortDecl (pals,attrl,ch) =
	(print_list ("",",","") (print o snd) pals; print ": ";
	 if null attrl then () else print_list (""," ","") (fn IN => print "in " | OUT => print "out ") attrl;
	 print_Channel ch)
and print_VarParam (vals,attrl,ty) =
	(print_list ("",",","") (fn (_,RefArg n) => (print "&"; print n) | (_,ValArg n) => print n) vals; print ": ";
	 if null attrl then () else print_list (""," ","") (fn READ => print "read " | WRITE => print "write ") attrl;
	 print_Type ty)
and print_VarDecl (a,b,c) =
	(print_list ("",",","") (print o snd) a; print ": "; print_Type b;
	 case c of SOME ival => (print " := "; print_Exp ival) | _ => ())
and print_StatesDecl (_,a) =
	(print_list ("states ",", ","") print a)
and print_PriorDecl (a,b) =
	(print_list ("","|","") (print o snd) a; print " > "; print_list ("","|","") (print o snd) b)
and print_TimeConstraint (left,right) =
        if case (left,right) of (Closed d,Infinite) => d <= 0.0 | _ => false then () else
	(print " in "; print_Interval (left,right))
and print_Interval (left,right) =
	(case left of Open b => (print "]"; print (Real.toString b))
		    | Closed b => (print "["; print (Real.toString b));
	 print ",";
	 case right of Open b => (print (Real.toString b); print "[")
		    | Closed b => (print (Real.toString b); print "]")
		    | Infinite => print "...[")
and print_Transition i (_,b,c) =
	(newline i; print "from "; print b; print " "; print_Statement (i+3) c)
and print_Statement e st = (case st of
	  StNull => (newline e; print "null")
	| StCond(l,a,b,c,els) =>
		(newline e; print "if "; print_Exp a; print " then"; print_Statement (e+3) b;
		 app (fn (_,a,b) => (newline e; print "elsif "; print_Exp a; print " then"; print_Statement (e+3) b)) c;
		 case els of SOME d => (newline e; print "else";  print_Statement (e+3) d) | _ => ();
		 newline e; print "end")
	| StSelect (a::t) =>
		(newline e; print "select";
		 print_Statement (e+3) a;
		 List.app (fn y => (newline e; print "[]"; print_Statement (e+3) y)) t;
		 (* print_list ("select"," []","") (fn y => (newline (e+6); print_Statement e y)) a; *)
		 newline e; print "end")
	| StSequence(_,a,b) => (print_Statement e a; print "; "; print_Statement e b)
	| StSignal (_,a,r) => (newline e; print a; print_pdetails (!r))
	| StInput (_,a,r,b,p) =>
		(newline e; print a; print_pdetails (!r); print "? "; print_list ("",",","") print_Patt b;
		 case p of SOME c => (print " where "; print_Exp c) | _ => ())
 	| StOutput (_,a,r,b) =>
		(newline e; print a; print_pdetails (!r); print "! "; print_list ("",", ","") print_Exp b)
	| StAssign (l,a,b) =>
		(newline e; print_list ("",",","") print_Patt a; print " := "; print_list ("",",","") print_Exp b)
	| StAny (l,a,r,w) => (print_list ("",",","") print_Patt a; print_rdetails (!r); print ":= any";
		case w of SOME d => (print " where "; print_Exp d) | _ => ())
	| StWhile(l,a,b) => (newline e; print "while "; print_Exp a; print " do"; 
			     print_Statement (e+3) b; 
			     newline e; print "end")
	| StCase (_,a,psl) =>
	        let fun printMatches ((_,ref b,a1,s1)::psl) =
			(newline (e+2); print_Patt a1; print " -> "; print_Statement (e+3) s1;
			 List.app (fn (_,ref b,a,s) => (newline e; print "| "; print_Patt a; print " -> "; print_Statement (e+3) s)) psl)
		in newline e; print "case "; print_Exp a; print " of";
		   printMatches (if !TRANSF then List.filter (fn (_,ref b,_,_) => b) psl else psl);
		   newline e; print "end"
		end
	| StForeach (_,v,t,s) => (newline e; print "foreach "; print v; print_details (!t); print " do ";
				  print_Statement (e+3) s;
				  newline e; print "end")
	| StTo (l,a) => (newline e; print "to "; print a))
and print_Patt e =
        let val d = !DETAILS in DETAILS := false; print_Exp e; DETAILS := d end
and print_Exp e = case e of
	  IdentExp (a,ref (_,ty)) => (print a (* ; print_details ty *))
	| AnyExp 		=> print "any"
	| ArrayAccessExp(a,b,_) => (print_Exp a; print "["; print_Exp b; print "]")
 	| RecordAccessExp(a,b,_) => (print_Exp a; print "."; print b)
	| IntExp (n,r)          => (printi n; print_details (!r))
	| BoolExp b             => print (Bool.toString b)
	| BinopExp(x,r,y,z)	=> (print_Binop x; print_details (!r); print "("; print_Exp y; print ","; print_Exp z; print ")")
	| UnopExp(a,r,b)        => (print_Unop a; print_details (!r); print " "; print_Exp b)
	| ConstrExp(c,e2,r)     => (print c; print " "; print_Exp e2; print_details (!r))
	| RecordExp (fl,r)      => let val b = !DETAILS
				   in DETAILS := false; print_list ("{",",","}") (fn (f,l) => (print f; print "="; print_Exp l)) fl;
				      DETAILS := b; print_details (!r)
				   end
	| ArrayExp (el,r)       => let val b = !DETAILS
				   in DETAILS := false; print_list ("[",",","]") print_Exp el; DETAILS := b; print_details (!r)
			           end
	| QueueExp (el,r)   	=> (print_list ("{|",",","|}") print_Exp el; print_details (!r))
	| RefExp (v,r)          => (print "&"; print v; print_details (!r))
	| ParenExp a	        => (print "("; print_Exp a; print ")")
	| _ => (* parenthesizes infixes and conditionals if -p; helps check parsing *)
	  (if !TYPECHECK then () else print "(";
	   case e of
          InfixExp(x,r,y,z) => (print_Exp y; print " "; print_Infix x; print_details (!r); print " "; print_Exp z)
	| CondExp (c,e1,e2,r) => (print_Exp c; print " ? "; print_Exp e1; print " : "; print_Exp e2);
	   if !TYPECHECK then () else print ")")
and print_details ty =
	if !DETAILS then (print "::"; print_Type ty) else ()
and print_rdetails tyl =
	if !DETAILS then (print "::"; print_list (""," * ","") print_Type tyl) else ()
and print_pdetails ty =
	if !DETAILS then (print "::"; print_Channel ty) else ()
and print_Infix ope = print (case ope of
	  AND => "and"	| OR  => "or"	| LE  => "<="	| LT  => "<"	| GT  => ">"	| GE  => ">="
	| EQ  => "="	| NE  => "<>"	| ADD => "+"	| SUB => "-"	| MOD => "%"	| MUL => "*"	| DIV => "/")
and print_Binop ENQUEUE = print "enqueue" | print_Binop APPEND = print "append"
and print_Unop ope = print (case ope of
	  COERCE => "$" | NOT => "not" | FULL => "full" | EMPTY => "empty" | DEQUEUE => "dequeue" | PLUS => "+" | MINUS => "-" | FIRST => "first")
;

fun printFiacre s0 = (print_Fiacre s0; newline 0; flush ())
   
end




structure ErrSrcPrint = SrcPrint(ErrPrint);
structure OutSrcPrint = SrcPrint(OutPrint);

signature PRINT = sig val print : Fiacre -> unit end;

structure Print : PRINT = struct
fun print st = case !OUTPUT of
	  Fiacre => OutSrcPrint.printFiacre st
	| TTS => ()
	| LOTOS => ()
	| _ => ()
end;

