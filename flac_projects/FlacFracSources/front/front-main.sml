
(* main *)
fun top () =
	let val args = CommandLine.arguments ()
	    val () = command args
	in (* check/set error stream *)
	   case !ErrPrint.name of
		  "" => ()
		| f  => let val f = OS.Path.mkCanonical f
			in ErrPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE)
			end;
	   (* check/set input stream *)
	   case !Input.name of
		  "" => ()
		| f  =>	let val f = OS.Path.mkCanonical f
			in case OS.Path.ext f of
				  SOME e => if mem e ["fcr"] then () else (print ("unknown format for input file: "^f^"\n"); raise BYE)
				| e      => (print ("unknown format for input file: "^f^"\n"); raise BYE);
		   	   Input.FILE := TextIO.openIn f handle x => (print ("cannot open "^f^"\n"); raise BYE)
			end;
	   (* check/set output stream *)
	   case !OutPrint.name of
		  "" => ()
		| f  =>	let val f = OS.Path.mkCanonical f
			in case OS.Path.ext f of
				  SOME e => (case !OUTPUT of
					NoOutput => (case e of
						  "fcr" => OUTPUT := Fiacre
						| _ => (print ("unknown format for output file: "^f^"\n"); raise BYE))
				      | _ => ())
				| e      => (case !OUTPUT of
					NoOutput => (print ("unknown format for output file: "^f^"\n"); raise BYE)
				      | _ => ());
		   	   OutPrint.FILE := TextIO.openOut f handle x => (print ("cannot open "^f^"\n"); raise BYE)
			end;
	   (* go *)
	   let val (p,p') = Type.typecheck (Parse.parse ())
	   in
               Init.initcheck p';
               Print.print (if !TRANSF then (* ElimCond.elimCond *) p' else p)
	   end;
	   (* exit *)
	   ErrPrint.flush (); OutPrint.flush (); OS.Process.exit OS.Process.success
	end
	handle BYE => (ErrPrint.flush (); OutPrint.flush (); OS.Process.exit OS.Process.failure);



(* making the application (for mlton) ----------------------------------- *)

top ();
