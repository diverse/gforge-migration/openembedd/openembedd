
signature ELIMCOND = sig
val elimCond : Fiacre -> Fiacre
end;

structure ElimCond : ELIMCOND = struct 

(* tmpvar is the temporary variable generator. temporary names
   are built from TMPBASE and counter TMPCNT.
   TMPBASE is initialized in elimCond. *)
val TMPCNT = ref 0;
val TMPBASE = ref "z";
val TMPDECS = ref ([] : ((line * string) list * Type * Exp option) list);
fun inittmp () = (TMPDECS := []; TMPCNT := 0);
fun tmpvar ty e = (* line number ~1 tells it is temp var *)
    let val n = !TMPBASE ^ "__" ^ Int.toString (!TMPCNT)
    in TMPCNT := 1 + !TMPCNT;
       TMPDECS := ([(~1,n)],ty,e) :: !TMPDECS;
       IdentExp (n,ref (VarKind,ty))
    end;

(* returns a binding made of temporary variables each assigned a conditional
   value, and the expression obtained by replacing the cond values by the variables.
   if the expression does not contain any conditional, then returns the empty binding
   and the original expression. binding is in reverse order *)
fun normExp e =
    let val B = ref []
	fun eece e = case e of
			 ParenExp e'                 => ParenExp (eece e')
		       | CondExp (c,e1,e2,r)         =>
	                 let val tmp = tmpvar (!r) NONE
			 in B := (tmp,CondExp(eece c,eece e1,eece e2,r)) :: !B; tmp
			 end
		       | InfixExp (f,rty,e1,e2)      => InfixExp (f,rty,eece e1,eece e2) 
		       | BinopExp (f,rty,e1,e2)      => BinopExp(f,rty,eece e1,eece e2)
		       | UnopExp (f,rty,e1)          => UnopExp(f,rty,eece e1)
		       | ConstrExp (c,e1,rty)        => ConstrExp (c,eece e1,rty)
		       | ArrayAccessExp (e1,e2,rty)  => ArrayAccessExp (eece e1,eece e2,rty)
		       | RecordAccessExp (e1,fl,rty) => RecordAccessExp (eece e1,fl,rty)
		       | RecordExp (fl,rty)          => RecordExp (map (fn (f,e) => (f,eece e)) fl,rty)
		       | QueueExp (el,rty)           => QueueExp (map eece el,rty)
		       | ArrayExp (el,rty)           => ArrayExp (map eece el,rty)
		       | _                           => e
	val e' = eece e (* case e of CondExp (c,e1,e2,r) => CondExp (eece c,eece e1,eece e2,r) | _ => eece e *)
    in (!B,e')
    end;

(* as above, iterated on a list *)
fun lNormExp el = let val (bl,el') = ListPair.unzip (map normExp el) in (flat (rev bl),el') end;

(* as normExp, for patterns *)
fun normPatt p =
    let val B = ref []
        fun recp p = case p of
		RecordAccessExp (re,f,r) => RecordAccessExp (recp re,f,r)
	      | ArrayAccessExp (ar,i,r) => let val (b,i') = normExp i in B := b :: !B; ArrayAccessExp (recp ar,i',r) end
	      | ConstrExp (c,e,r) => ConstrExp (c,recp e,r)
	      | ParenExp e => ParenExp (recp e)
	      | _ => (* , variable, constant or any *) p
	val p' = recp p
    in (flat (!B),p')
    end;

(* as above, iterated on a list *)
fun lNormPatt pl = let val (bl,pl') = ListPair.unzip (map normPatt pl) in (flat (rev bl),pl') end;

fun mkSeq _ [] = StNull | mkSeq _ [s] = s | mkSeq ln (h::t) = StSequence(ln,h,mkSeq ln t);

fun mkSt ln b s =
    mkSeq ln (map (fn (p,CondExp(c,e1,e2,_)) => StCond(ln,c,StAssign(ln,[p],[e1]),[],SOME (StAssign(ln,[p],[e2])))) (rev b) @ [s]);

(* eliminates CondExp from a statement *)
(* notes:
   - unfolds StCond's (easier that way)
   - removes redundant matches
   - may duplicate some pattern(s), but that should not break preprocess code since patterns are not converted.
*)
fun normSt s = case s of
	  StCond (ln,c,s',lcsl,os) =>
	  let val (b,c') = normExp c
	      val os' = case lcsl of
			    (ln1,c1,s1)::sl => SOME (normSt (StCond (ln1,c1,s1,sl,os)))
			  | _ => (case os of SOME s'' => SOME (normSt s'') | _ => NONE)
	  in case b of
	       [] => StCond (ln,c,normSt s',[],os')
	     | _ => mkSt ln b (StCond (ln,c',normSt s',[],os'))
	  end
	| StSelect sl => StSelect (map normSt sl)
	| StSequence (ln,s1,s2) => StSequence (ln,normSt s1,normSt s2)
	| StWhile (ln,c,s') =>
	  let val (b,c') = normExp c
	  in case b of
		 [] => StWhile (ln,c,normSt s')
	       | _ => StSequence(ln,StAssign(ln,[c'],[BoolExp true]),StWhile(ln,c',mkSt ln b (StCond(ln,c',normSt s',[],NONE))))
	  end
	| StAssign (ln,pl,el) =>
	  let val (bp,pl') = lNormPatt pl
	      val (be,el') = lNormExp el
	  in case bp@be of
		 [] => s
	       | b => (case (pl',el') of
			   ([IdentExp(v,ref (VarKind,_))],[IdentExp(e,ref (VarKind,_))]) =>
			   (* optimization: if e is a temporary, then remove it and generate StCond statement from its value *)
		           if String.> (e,!TMPBASE)  (* e is a temporary name *)
			   then let val ([(_,CondExp(c,e1,e2,rty))],b') = List.partition (fn (IdentExp(k,_),_) => k=e) b
				in TMPDECS := List.filter (fn ([(_,x)],_,_) => x<>e) (!TMPDECS);
				   mkSt ln b' (StCond (ln,c,StAssign(ln,pl',[e1]),[],SOME (StAssign(ln,pl',[e2]))))
				end
			   else mkSt ln b (StAssign (ln,pl',el'))
			 | _ => mkSt ln b (StAssign (ln,pl',el')))
	  end
	| StAny (ln,pl,rtyl,eo) =>
	  let val (bp,pl') = lNormPatt pl
	      val (be,eo') = case eo of SOME e => let val (b,e') = normExp e in (b,SOME e') end | _ => ([],NONE)
	  in  case bp@be of [] => s | b => mkSt ln b (StAny (ln,pl',rtyl,eo'))
	  end
	| StInput (ln,q,rch,pl,eo) =>
	  let val (bp,pl') = lNormPatt pl
	      val (be,eo') = case eo of SOME e => let val (b,e') = normExp e in (b,SOME e') end | _ => ([],NONE)
	  in  case bp@be of [] => s | b => mkSt ln b (StInput (ln,q,rch,pl',eo'))
	  end
	| StOutput (ln,q,rch,el) =>
	  let val (b,el') = lNormExp el
	  in case b of [] => s | _ => mkSt ln b (StOutput(ln,q,rch,el'))
	  end
	| StCase (ln,e,ml) =>
	  let val ml = List.filter (fn (_,ref b,_,_) => b) ml
	      val pl = map (fn x => #3 x) ml
	      val (b,pl') = lNormPatt pl
	  in case b of
		 [] => StCase (ln,e,map (fn (ln,b,p,s) => (ln,b,p,normSt s)) ml)
	       | _ => mkSt ln b (StCase (ln,e,map (fn (p,(ln,r,_,s)) => (ln,r,p,normSt s)) (ListPair.zip (pl',ml))))
	  end
	| StForeach (ln,v,rty,s) => StForeach (ln,v,rty,normSt s)
        | _ => s;

(* moves conditional initializations in vardec to init statement *)
fun normInit statedec (vardec,prelude) =
    let val _ = TMPDECS := []
	val lni = ref ~1
	val P = ref (case prelude of
			 NONE => (case statedec of SOME (_,s0::_) => StTo (~1,s0) | _ => StNull)
		       | SOME (ln,s) => (lni := ln; s))    (* line numbers ? *)
	val set = ref false
	fun insertP s = P := (case !P of StNull => s | _ => StSequence (!lni,s,!P))
        val vardec' = map (fn (lnvl,ty,SOME e) =>
			      let val (b,e') = normExp e
			      in case b of
				     [] => (lnvl,ty,SOME e)
				   | _ => (List.app
					       (fn (ln,v) => (* initialize in prelude rather than verdec *)
						   (set := true;
						    case e' of (* optimization, as for StAssign case in normSt *)
							IdentExp(e,ref (VarKind,_)) =>
							if String.> (e,!TMPBASE)
							then let val ([(_,CondExp(c,e1,e2,rty))],b') = List.partition (fn (IdentExp(k,_),_) => k=e) b
								 val pl = [IdentExp(v,ref (VarKind,ty))]
							     in TMPDECS := List.filter (fn ([(_,x)],_,_) => x<>e) (!TMPDECS);
								insertP (mkSt (!lni) b' (StCond (!lni,c,StAssign(!lni,pl,[e1]),[],SOME (StAssign(ln,pl,[e2])))))
							     end
							else insertP (mkSt (!lni) b (StAssign (!lni,[IdentExp(v,ref (VarKind,ty))],[e'])))
						      | _ => insertP (mkSt (!lni) b (StAssign (!lni,[IdentExp(v,ref (VarKind,ty))],[e'])))))
					       lnvl;
					   (lnvl,ty,NONE))
			      end
			    | d => d) vardec
	val prelude' = if !set then SOME (!lni,normSt (!P)) else prelude
    in (vardec' @ !TMPDECS, prelude')
    end;

(* eliminates conditional expressions from a process *)
fun normProcess (line,name,portdec,argdec,statedec,vardec,prelude,transitions) =
    let val transitions' = (inittmp (); map (fn (ln,src,stmt) => (ln,src,normSt stmt)) transitions)
	val (vardec',prelude') = normInit (SOME statedec) (vardec @ !TMPDECS,prelude)
    in (line,name,portdec,argdec,statedec,vardec',prelude',transitions')
    end;

(* eliminates conditional expressions from a composition *)
fun normComposition c prelude = 
    let fun normInstExp e = case e of RefExp _ => ([],e) | _ => normExp e
	fun normComp c =
	    let val B = ref []
		fun norm c = case c of
				 ParComp (ln,ql,pcl) => ParComp (ln,ql,map (fn (e,c) => (e,norm c)) pcl)
			       | InstanceComp (lnp,ql,el) => InstanceComp (lnp,ql,map (fn e => let val (b,e') = normInstExp e in B := b :: !B; e' end) el)
		val c' = norm c
	    in (flat (!B), c')
	    end
	val (b,c') = normComp c
    in (c',
	case prelude of
		SOME (ln,s) => SOME (ln, (case b of [] => s | _ => StSequence(~1,s,mkSt ~1 b StNull)))
	      | NONE => (case b of [] => NONE | _ => SOME (~1,mkSt ~1 b StNull)))
    end;


(* eliminates conditional expressions from a component *)
fun normComponent (line,name,portdec,argdec,vardec,locportdec,priodec,prelude,composition) =
    let val (composition',prelude') = (inittmp (); normComposition composition prelude)
	val (vardec',prelude'') = normInit NONE (vardec @ !TMPDECS,prelude')
    in (line,name,portdec,argdec,vardec',locportdec,priodec,prelude'',composition')
    end;

(* eliminates conditional expressions from a fiacre program. to be applied after typechecking *)
fun elimCond (Program (dl,main)) =
    ((* set temp var base: *)
     TMPBASE := "z"; while String.>(!Type.MAXVAR,!TMPBASE) do TMPBASE := "z" ^ !TMPBASE;
     (* transform: *)
     Program (map (fn ProcessDecl d => ProcessDecl (normProcess d)
		    | ComponentDecl d => ComponentDecl (normComponent d)
		    | d => d) dl,
	      main));

end;

