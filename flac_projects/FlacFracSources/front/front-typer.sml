
signature TYPE = sig
val typecheck : Fiacre -> Fiacre
end;


structure Type (* : TYPE *) = struct

open ErrSrcPrint; (* print, flush, print_... on Err *)



(* -------------------------- error handling ----------------------- *)

(* for checkStatement: *)
datatype kind = Shm | Com;
type StKind = kind list;


(* type errors and warnings: *)
datatype TypeError =
	  DUP of string * string					(* duplicated decl              *)
	| UNBOUND of string * string					(* for types or variables 	*)
	| UNKNOWN of string * string					(* for fields, ports, states    *)
	| ACCESS of string * string					(* violations, for shared vars	*)
	| SUBT of Exp * Type * Type					(* ill-typed expression		*)
	| EQUAL of Exp * Type * Type					(* ill-typed equality		*)
	| FIELD of Pattern * string					(* missing field		*)
	| HIDES of string * string * string				(* redefined shared var		*)
	| REF of VarAttr list * string                                  (* r/w attr to non-ref var      *)
	| ARRAYINIT of Type list					(* bad array initializer	*)
	| QUEUEINIT of Type list					(* bad queue initializer	*)
	| ANYTY of Pattern * Type					(* any on an illegal type	*)
	| CONSTR of string                                              (* non constructor application  *)
	| AMBIGUOUS of Exp * Type					(* undefined queue type		*)
	(* empty interval type *)
	| IEMPTY                                                        (* empty interval type          *)
	(* for foreach *)
	| FETY of string * Type                                        (* type not an interval type     *)
	| FEVAR of string                                              (* iteration ident not a var     *)
        (* delays and priorities *)
	| DELAY	of Bound * Bound					(* ill formed interval   	*)
	| PRIO of string * string                                       (* ill formed prio relation     *)
	(* in communications *)
	| PMODE of string * PortAttr					(* io mode error		*)
	| COMM of string * Exp list * Profile				(* comm arity error     	*)
	| SINGLECOM of Statement * StKind * StKind			(* single com violation		*)
	| COMINWHILE of Statement					(* com in while loop		*)
	(* about states *)
	| NOTARGET of string						(* no target state		*)
	| DUPFROM of string						(* state at origin of >1 trans	*)
	(* in assignements *)
	| ASSIGN 							(* mismatch in lhs/rhs of := 	*)
	| LHS of Exp							(* ill-formed lhs *)	
	| INDEP of Exp * Exp						(* nonindep assign accesses	*)
        (* init errors *)
	| ISHV of string				        	(* shared var read in init      *)
	| IOST							    	(* comm in init statement	*)
	| TOST								(* to in comp init statement	*)
	| IWHERE                                                        (* where in init statement      *)
	| IXHAUST of Pattern list                                       (* match noin exhaustive        *)
	(* const errors *)
	| OVFQ								(* queue overflow	        *)
	| UDFQ								(* queue underflow		*)
	| OVFI								(* numeric out of range		*)
        (* constructions *)
	| ZeroARY of string                                             (* expecting a 0-ary constr     *)
	| OneARY of string                                              (* expecting a 1-ary constr     *)
	(* missing main *)
	| NOMAIN
	(* in instances, details below *)
	| INST of string * InstError					(* error in component instance	*)
        (* in compositions *)
	| SORT of string list * string list                             (* port not in sort             *)
        (* for pattern matching *)
	| XHAUST of Pattern list                                        (* pattern not exhaustive       *)
	| REDUNT of Pattern                                             (* redundant pattern            *)
and InstError =								(* errors in instances		*)
	  PortCount of int * int					(* wrong port count 		*)
	| ArgCount of int * int						(* wrong arg count 		*)
	| PortProf of string * Profile * Profile			(* profile mismatch 		*)
	| PortAttr of string * PortAttr list * PortAttr list		(* attr mismatch 		*)
	| VarAttr of string * VarAttr list * VarAttr list		(* shared var mode violation 	*)
	| VarType of string * Type * Type				(* shared var type mismatch 	*)
	| ExpExpected of Exp						(* &a found where a expected	*)
	| RefExpected of Exp;						(* a found where &a expected	*)

(* prints a type error/warning: *)
fun print_error M when line err =
	let open ErrSrcPrint
	in print M; print ": "; print when;
	   if line >= 0 then (print " line "; printi (line+1)) else (); (* line<0 if irrelevant *)
	   print ": ";
	   case err of
	   DUP (what,name) => print (what ^ " " ^ name ^ " declared twice")
	 | UNBOUND (what,name) => print ("unbound "^what ^" "^ name)
	 | UNKNOWN (what,name) => print ("unknown "^what ^" "^ name)
	 | ACCESS (violation,name) => print ("shared variable "^name^" may not be "^violation)
	 | ASSIGN => print "arities mismatch between lhs and rhs of assignement"
	 | LHS e => print "ill-formed pattern"
	 | DELAY i => (print "ill-formed or empty interval "; print_Interval i)
	 | COMM (name,el,tyl) =>
		(print ("profile arity mismatch in communication on port "^name); print "\n";
		 print "  expected: "; printi (length tyl); print "\n";
		 print "  found:    "; printi (length el))
	 | SUBT (e,ty1,ty2) =>
		(print "ill typed expression\n"; 
		 print "  in: "; print_Exp e; print "\n";
		 print "  expected: "; print_Type ty2;
		 print "\n  found:    "; print_Type ty1)
	 | PMODE (p,attr) =>
		(print "unauthorized communication mode (";
		 case attr of IN => "in" | OUT => "out"; print ")\n";
		 print "on port "; print p)
	 | EQUAL (e,ty1,ty2) =>
		(print "ill typed equality\n"; 
		 print "  in: "; print_Exp e; print "\n";
		 print "  left:  "; print_Type ty1; print "\n";
		 print "  right: "; print_Type ty2)
	 | FIELD (e,f) =>
		(print "ill typed pattern\n";
		 print "   record: "; print_Exp e; print "\n";
		 print "   has no field: "; print f)
	 | HIDES (what,hides,name) => print (what^" "^name^" hides a "^hides)
	 | REF (attrs,name) => print ("read and/or write attribute to a variable not passed by reference ("^name^")")
	 | ARRAYINIT tyl => print "array components of uncompatible types"
	 | QUEUEINIT tyl => print "queue components of uncompatible types"
	 | SINGLECOM (s,k1,k2) =>
		let fun print_stkind kl = print_list ("",", ","") (fn Shm => print "shared variables" | Com => print "communication") kl
		in print "single communication constraint violated\n";
		   print "  in: "; print_Statement 0 s; print "\n";
		   print "  statement uses both\n";
		   print "    : "; print_stkind k1; print "\n";
		   print "    : "; print_stkind k2
		end
	 | COMINWHILE s =>
		(print "communication in a while loop:\n";
		 print "  in: "; print_Statement 0 s) 
	 | NOTARGET s => (case s of
	          "" => print "no target statement in some path of init statement"
		| _ => (print "no target statement in some path from state "; print s))
	 | AMBIGUOUS (e,ty) =>
		(print "ambiguous queue type in expression:\n";
		 print "       : "; print_Exp e; print "\n";
		 print " found : "; print_Type ty)
	 | IEMPTY =>
	        print "empty interval type\n"
	 | FETY (v,ty) =>
	        (print "iteration variable "; print v; print " must have an interval type\n";
		 print " found : "; print_Type ty)
	 | FEVAR v =>
	        (print "iteration variable "; print v; print " must be a variable\n")
	 | INDEP (p1,p2) =>
		(print "non-independent assignment patterns:\n";
		 print "    : "; print_Exp p1; print "\n";
		 print "  vs: "; print_Exp p2)
	 | INST (c,PortCount (n,en)) =>
		(print "bad port parameter count in instance of "; print c; print "\n";
		 print "   found   : "; printi n; print "\n";
		 print "   expected: "; printi en)
	 | INST (c,ArgCount (n,en)) =>
		(print "bad argument count in instance of "; print c; print "\n";
		 print "   found   : "; printi n; print "\n";
		 print "   expected: "; printi en)
	 | INST (c,PortProf (port,prl,eprl)) => (* profile mismatch *)
		(print "profile mismatch in instance of "; print c; print " for port "; print port; print "\n";
		 print "   found   : "; print_Channel (ProfileChannel prl); print "\n";
		 print "   expected: "; print_Channel (ProfileChannel eprl))
 	 | INST (c,PortAttr (port,atl,eatl)) => 	(* attr mismatch *)
		let fun print_attrs atl = print_list ("",", ","") (fn IN => print "in" | OUT => print "out") atl
		in print "port attribute error in instance of "; print c; print " for port "; print port; print "\n";
		   print "   found   : "; print_attrs atl; print "\n";
		   print "   expected a subset of: \n";
		   print "             "; print_attrs eatl
		end
	 | INST (c,VarAttr (v,atl,eatl)) => (* shared var mode violation *)
		let fun print_attrs atl = print_list ("",", ","") (fn READ => print "read" | WRITE => print "write") atl
		in print "shared variable access error in instance of "; print c; print " for variable "; print v; print "\n";
		   print "   found   : "; print_attrs atl; print "\n";
		   print "   should include: \n";
		   print "             "; print_attrs eatl
		end
	 | INST (c,VarType (v,ty,ety)) => (* shared var type mismatch *)
		(print "shared variable type mismatch in instance of "; print c; print " for variable "; print v; print "\n";
		 print "   found   : "; print_Type ty; print "\n";
		 print "   expected: "; print_Type ety)
	 | INST (c,ExpExpected e) =>
		(print "expression expected in instance of "; print c; print "\n";
		 print "   found   : "; print_Exp e; print "\n";
		 print "   expected: some expression")
	 | INST (c,RefExpected e) =>
		(print "variable reference expected in instance of "; print c; print "\n";
		 print "   found   : "; print_Exp e; print "\n";
		 print "   expected: some variable reference")
	 | SORT (pl,st) =>
	        (print "synchronized port(s) not in component sort\n";
		 print "   ports : "; print_list (""," ","\n") print pl;
		 print "   sort  : "; print_list (""," ","\n") print st)
         | XHAUST pl => print "match is not exhaustive"
         | REDUNT p  => print "pattern is redundant"
	 | ISHV v => print ("shared variables ("^v^") may not be read nor written in init statements")
	 | IOST => print "communications forbidden in init statements"
	 | TOST => print "jumps forbidden in component init statements"
         | IWHERE => print "where clauses forbidden in init statements"
         | IXHAUST pl => print "matches must be exhaustive in init statements"
	 | ANYTY (p,ty) =>
		(print "any applied to an illegal type in pattern "; print_Exp p; print "\n";
		 print "   found   : "; print_Type ty; print "\n";
		 print "   expected: a boolean or numeric type")
	| DUPFROM s =>
		(print "several transitions found from state: "; print s)
	| OVFQ => print "queue overflow when computing constant"
	| UDFQ => print "queue underflow when computing constant"
	| OVFI => print "numeric out of range when computing constant"
        | ZeroARY c => (print "expected a 0-ary constructor, found: "; print c)
        | OneARY c => (print "expected a 1-ary constructor, found: "; print c)
	| CONSTR c => (print c; print " is not a union constructor")
	| NOMAIN => print "no main is specified";
	   print "\n"; flush ()
	end;

exception TYPE;

val errcount = ref 0;
fun type_error when line err =
	(errcount := 1 + !errcount; print_error "Error" when line err; raise TYPE);

val warncount = ref 0;
fun type_warning when line err =
	(warncount := 1 + !warncount; print_error "Warning" when line err);



(* ----------------- managing the typing context -------------------- *)

(* the typing context associates with every ident some information
   necessary for typing. Is made of two avl trees:
   - the global context, genv, capturing constant, type and channel 
     abbreviations, the constructors visible at toplevel, processes
     and components.
   - a local context for each process or component;
*)

(* typing contexts have type: Idents avltree.tree ref *)
datatype Idents =
	  Constant of Type * Exp                        (* toplevel constants  *)
        | Var of VarAttr list * Type * bool		(* variables, bool tells if local *)
        | Port of PortAttr list * Profile 	 	(* port			*)
        | TypeVar of Type				(* type abbreviation	*)
        | ChannelVar of Channel				(* channel abbreviation	*)
        | Constr of Type * Type			        (* constructor, argty, result ty *)
        | State						(* state		*)
        | Comp of Idents list * Idents list;		(* component (ports,vars) *)

(* note: all idents are stored in a single avl.
   to allow distinct classes to share names, access keys are encoded as follows:
	vars,constructors: 	v%var
	ports:      	  	p%port
	types,channels:   	t%type
	states:     	  	s%st
	processes/components: 	c%b
*)

(* global typing context: *)
val genv = avlTree.newTree() : Idents avlTree.tree ref;

(* insertion, lookup in typing context *)
fun insert k d env = env := avlTree.insert (k,d,!env);
fun lookup k env = avlTree.getData (k,!env);

(* specialized insertion routines *)
fun addType line t body =
	(* type abbreviations are registered in global context, duplicates forbidden *)
	case lookup ("t%"^t) genv of NONE => insert ("t%"^t) (TypeVar body) genv | _ =>
	type_error "declaration" line (DUP ("type",t));
fun addChannel line t body =
	(* channel abbreviations are registered in global context, duplicates forbidden *)
	case lookup ("t%"^t) genv of NONE => insert ("t%"^t) (ChannelVar body) genv | _ =>
	type_error "declaration" line (DUP ("channel",t));
fun addConstant line name e ty =
	(* constants are registered in global context, duplicates forbidden, always accessed by value *)
	case lookup ("v%"^name) genv of
	       NONE => insert ("v%"^name) (Constant (ty,e)) genv
	     | _ => type_error "constant declaration" line (DUP ("constant",name));
val MAXVAR = ref ""; (* maintains lexicographically largest variable, for temp var generator in later passes *)
fun addVar env line mode lcl v ty =
	(* formal parameters and local variables are registered in local context, duplicates forbidden *)
        let val (name,attrs) = case v of
		      RefArg n => (n,if null mode then [READ, WRITE] else mode)
		    | ValArg n => (n,if null mode then [] else type_error "parameters list" line (REF (mode,n)))
	in if String.>(name,!MAXVAR) then MAXVAR := name else ();
	   case lookup ("v%"^name) genv of
	       NONE => (case lookup ("v%"^name) env of
			    NONE => insert ("v%"^name) (Var (attrs,ty,lcl)) env
			  | _ => type_error "parameters list" line (DUP ("variable",name)))
	     | _ => type_error "variable declaration" line (HIDES ("variable","constant or constructor",name))
	end;
fun addPort env line mode p pr =
	(* ports and local ports are registered in local context, duplicates forbidden *)
	case lookup ("p%"^p) env of NONE => insert ("p%"^p) (Port (mode,pr)) env | _ =>
	type_error "declaration" line (DUP ("port",p));
fun addConstr env line e ty ty' =
	(* union constructors registered in global context if appearing in a type dec of
           component header, otherwise in local context. union constructors may not hide variables *)
	case lookup ("v%"^e) genv of
	   NONE => (case lookup ("v%"^e) env of
		        NONE => insert ("v%"^e) (Constr (ty,ty')) env
		      | _ => type_error "declaration" line (HIDES ("constructor","local variable",e)))
	| _ => type_error "declaration" line (HIDES ("constructor","constant or constructor",e));
fun addState env line s =
	(* states are registered in local context *)
	case lookup ("s%"^s) env of NONE => insert ("s%"^s) State env | _ =>
	type_error "state declaration" line (DUP ("state",s));
fun addComponent line c ports vars =
	(* process/component variables are registered in global context, duplicates forbidden *)
	case lookup ("c%"^c) genv of NONE => insert ("c%"^c) (Comp (ports,vars)) genv | _ =>
	type_error "declaration" line (DUP ("process",c));


(* lookups variable v in local context (for parameters, local variables
   or local constructors), then in global context (for global constructors
   and constants) *)
fun getVar v env line wheree =
	case lookup ("v%"^v) env of
	  NONE => (case lookup ("v%"^v) genv of
		       NONE => type_error wheree line (UNBOUND ("variable",v))
		     | SOME x => x)
	| SOME x => x;

(* lookups port p in local context *)
fun getPort p env line wheree =
	case lookup ("p%"^p) env of
	  NONE => type_error wheree line (UNBOUND ("port",p))
	| SOME x => x;

(* lookups process or component c in global envionment *)
fun getComponent c line wheree =
	case lookup ("c%"^c) genv of
	  NONE => type_error wheree line (UNBOUND ("component",c))
	| SOME x => x;


(* ----------------------------- type operators --------------------- *)

infix sub lub glb psub;

(* all types in these routines are assumed not to contain variables *)

(* true if ty is a subtype of ty' *)
fun ty sub ty' = ty = ty' orelse
	case (ty,ty') of
	  (NatType, IntType) => true
	| (IntervalType (IntExp (f,_), IntExp (t,_)), NatType) => f >= 0 andalso t >= 0
	| (IntervalType _, IntType) => true
	| (IntervalType (IntExp (f1,_), IntExp (t1,_)), IntervalType (IntExp (f2,_), IntExp (t2,_))) => f1 >= f2 andalso t1 <= t2
	| (UnionType fl1, UnionType fl2) =>
	        (let val ffl = ListPair.zipEq (fl1, fl2) (* both assumed sorted *)
		 in List.all (fn ((f1,BotType),(f2,BotType)) => f1=f2
			       | ((_,BotType),_) => false
			       | (_,(_,BotType)) => false
			       | ((f1,ty1),(f2,ty2)) => f1=f2 andalso ty1 sub ty2) ffl
		 end handle _ => false)
	| (RecordType fl1, RecordType fl2) =>
		(let val ffl = ListPair.zipEq (fl1, fl2) (* both assumed sorted *)
		 in List.all (fn ((f1,ty1),(f2,ty2)) => f1=f2 andalso ty1 sub ty2) ffl
		 end handle _ => false)
	| (ArrayType (IntExp (k,_),ty), ArrayType (IntExp (k',_),ty')) => k=k' andalso (ty sub ty')
	| (QueueType (IntExp (k,_),ty), QueueType (IntExp (k',_),ty')) => (case (k,k') of
		  (~1,~1) => ty sub ty'
		| (~1,_)  => ty sub ty'
		| (_,~1)  => false
		| _       => k=k' andalso (ty sub ty'))
	| (_, TopType) => true
	| (BotType,_) => true
	| _ => false;

(* computes lub of ty1 and ty2 *)
fun ty1 lub ty2 =
	if ty1 = ty2 then ty1 else
	case (ty1,ty2) of
	  (NatType, IntType) => ty2
	| (IntType, NatType) => ty1
	| (IntervalType (IntExp (f,_), IntExp (t,_)), NatType) => if f>=0 andalso t>=0 then ty2 else IntType
	| (IntervalType (IntExp (f,_), IntExp (t,_)), IntType) => IntType
	| (NatType, IntervalType (IntExp (f,_), IntExp (t,_))) => if f>=0 andalso t>=0 then ty2 else IntType
	| (IntType, IntervalType (IntExp (f,_), IntExp (t,_))) => IntType
	| (IntervalType (IntExp (f1,_), IntExp (t1,_)), IntervalType (IntExp (f2,_), IntExp (t2,_))) =>
		let val (f,t) = (Int.min (f1,f2),Int.max(t1,t2))
		in IntervalType (IntExp (f,ref IntType),IntExp (t,ref IntType))
		end
	| (UnionType fl1, UnionType fl2) =>
		if (* both assumed sorted *)
		   List.all (fn ((f1,BotType),(f2,BotType)) => true
			       | ((_,BotType),_) => false
			       | (_,(_,BotType)) => false
			       | ((f1,_),(f2,_)) => f1=f2) (ListPair.zipEq (fl1, fl2))
		then UnionType (map (fn (f,t1) => (f,t1 lub assoc f fl2)) fl1)
		else TopType
	| (RecordType fl1, RecordType fl2) =>
		if map fst fl1 = map fst fl2 (* both assumed sorted *)
		then RecordType (map (fn (f,t1) => (f,t1 lub assoc f fl2)) fl1)
		else TopType
	| (ArrayType (IntExp (k,r),ty), ArrayType (IntExp (k',_),ty')) =>
		if k=k' then ArrayType (IntExp (k,r),ty lub ty') else TopType
	| (QueueType (IntExp (k,r),ty), QueueType (IntExp (k',r'),ty')) => (case (k,k') of
		  (~1,~1) => QueueType (IntExp (~1,ref BotType),ty lub ty')
		| (~1,_)  => QueueType (IntExp (k',r'),ty lub ty')
		| (_,~1)  => QueueType (IntExp (k,r),ty lub ty')
		| _       => if k=k' then QueueType (IntExp (k,r),ty lub ty') else TopType)
	| (BotType,_) => ty2
	| (_,BotType) => ty1
	| _ => TopType;

(* lub of a list of types *)
fun rlub [] = TopType | rlub [t] = t | rlub (h::t) = h lub (rlub t);

(* computes glb of ty1 and ty2 *)
fun ty1 glb ty2 =
	if ty1 = ty2 then ty1 else
	case (ty1,ty2) of
	  (NatType, IntType) => ty1
	| (IntType, NatType) => ty2
	| (IntervalType (IntExp (f,_), IntExp (t,_)), NatType) => 
	  if f>=0 then ty1 else
	  if t>=0 then IntervalType (IntExp (0,ref IntType), IntExp (t,ref IntType)) else
	  BotType
	| (IntervalType (IntExp (f,_), IntExp (t,_)), IntType) => ty1
	| (NatType, IntervalType (IntExp (f,_), IntExp (t,_))) =>
	  if f>=0 then ty2 else
	  if t>=0 then IntervalType (IntExp (0,ref IntType), IntExp (t,ref IntType)) else
	  BotType
	| (IntType, IntervalType (IntExp (f,_), IntExp (t,_))) => ty2
	| (IntervalType (IntExp (f1,_), IntExp (t1,_)), IntervalType (IntExp (f2,_), IntExp (t2,_))) =>
		let val (f,t) = (Int.max (f1,f2),Int.min(t1,t2))
		in if f <= t then IntervalType (IntExp (f,ref IntType),IntExp (t,ref IntType)) else BotType
		end
	| (UnionType fl1, UnionType fl2) =>
		if (* both assumed sorted *)
		   List.all (fn ((f1,BotType),(f2,BotType)) => true
			       | ((_,BotType),_) => false
			       | (_,(_,BotType)) => false
			       | ((f1,_),(f2,_)) => f1=f2) (ListPair.zipEq (fl1, fl2))
		then UnionType (map (fn (f,t1) => (f,t1 glb assoc f fl2)) fl1)
		else BotType
	| (RecordType fl1, RecordType fl2) =>
		if map fst fl1 = map fst fl2 (* both assumed sorted *)
		then RecordType (map (fn (f,t1) => (f,t1 glb assoc f fl2)) fl1)
		else BotType
	| (ArrayType (IntExp (k,r),ty), ArrayType (IntExp (k',_),ty')) =>
		if k=k' then ArrayType (IntExp (k,r),ty glb ty') else BotType
	| (QueueType (IntExp (k,r),ty), QueueType (IntExp (k',r'),ty')) => (case (k,k') of
		  (~1,~1) => QueueType (IntExp (~1,ref BotType),ty glb ty')
		| (~1,_)  => QueueType (IntExp (k',r'),ty glb ty')
		| (_,~1)  => QueueType (IntExp (k,r),ty glb ty')
		| _       => if k=k' then QueueType (IntExp (k,r),ty glb ty') else BotType)
	| (TopType,_) => ty2
	| (_,TopType) => ty1
	| _ => BotType;

(* glb of a list of types *)
fun rglb [] = BotType | rglb [t] = t | rglb (h::t) = h glb (rglb t);

(* sub for profiles (or type lists) *)
fun tyl1 psub tyl2 = List.all (op sub) (ListPair.zipEq (tyl1,tyl2)) handle _ => false;

(* true if type contains top (meaning it is illegal) *)
fun topInType ty = case ty of
	  RecordType fl => List.exists (fn (_,ty') => topInType ty') fl
	| UnionType fl => List.exists (fn (_,BotType) => false | (_,ty') => topInType ty') fl
	| ArrayType (_,ty') => topInType ty'
	| QueueType (_,ty') => topInType ty'
	| TopType => true
	| _ => false;

(* checks well-definedness of queue types: *)
fun isDefined ty = case ty of
	  RecordType fl => List.all (fn (_,ty') => isDefined ty') fl
	| UnionType fl => List.all (fn (_,BotType) => true | (_,ty') => isDefined ty') fl
	| ArrayType (_,ty') => isDefined ty'
	| QueueType (IntExp (k,_),ty') => k>=0 andalso isDefined ty'
	| BotType => false
	| TopType => false
	| _ => true;



(* --------------------------- typing expressions ------------------ *)

(* asserts that e, the type computed for x, is a subtype or r *)
fun checksub line x r e =
	if e sub r then e else type_error "expression" line (SUBT (x,e,r));

(* for checkStatement, Shm checks *)
val SHM = ref false;

(* some shorthands: *)
fun interval_type f t = IntervalType (IntExp (f,ref IntType), IntExp (t,ref IntType));
fun array_type k ty = ArrayType (IntExp (k,ref NatType),ty);
fun queue_type k ty = QueueType (IntExp (k,ref NatType),ty);

(* for checking initialisation statements *)
(* loc is 0 if s is in a process transition, 1 if in a process prelude, or 2 if in a comp prelude *)
val loc = ref 0;
fun bad_init line e = (* e is ISHV v, IOST, or TOST, or ... *)
	let val m = (if !loc>1 then "component" else "process") ^ " init statement"
	in type_error m line e
	end;

(* checks an expression in typing context env, returns a type and annotate  *)
(* overloaded primitives whose behavior may depend on their type have their type field set *)
fun typeExp env line e = case e of
          IdentExp (v,r) => (case getVar v env line "expression" of
		  Var (attrs,ty,lcl) =>
			(r := (VarKind,ty);
			 if null attrs then ty else
			 (SHM := true;
			  case !loc of
			      0 => if mem READ attrs then ty else
				   type_error "expression" line (ACCESS("read",v))
			    | 1 => bad_init line (ISHV v)
			    | 2 => if lcl then ty else bad_init line (ISHV v)))
		| Constant (ty,_) => (r := (ConstantKind,ty); ty)
		| Constr (argty,ty) =>
			(r := (ConstrKind,ty);
			 case argty of
			    BotType => ty
			  | _ => type_error "expression" line (ZeroARY v))
		| _ => type_error "expression" line (UNBOUND("variable",v)))
	| RecordAccessExp (a,f,r) => (case typeExp env line a of
		  rty as RecordType fl =>
			(r := rty; assoc [f] fl handle _ => type_error "expression" line (FIELD (a,f)))
		| ty => type_error "expression" line (SUBT (e,ty,RecordType [])))
	| ArrayAccessExp (a,i,r) => (case typeExp env line a of
		  aty as ArrayType (IntExp (k,_),ty) =>
			(r := aty; checksub line e (interval_type 0 (k-1)) (typeExp env line i); ty)
		| ty => type_error "expression" line (SUBT (e,ty,AnyArray)))
  	| InfixExp (ope,r,e1,e2) =>
		if mem ope [ADD,SUB,DIV,MUL,MOD]
		then let val ty1 = checksub line e1 IntType (typeExp env line e1)
			 val ty2 = checksub line e2 IntType (typeExp env line e2)
			 val ty = ty1 lub ty2
		     in r := ty; ty
		     end else
		if mem ope [LE,LT,GE,GT]
		then let val ty1 = checksub line e1 IntType (typeExp env line e1)
		 	 val ty2 = checksub line e2 IntType (typeExp env line e2)
		         val ty = ty1 lub ty2
		     in  r := ty; BoolType
		     end else
		if mem ope [EQ,NE]
		then let val ty1 = typeExp env line e1
			 val ty2 = typeExp env line e2
			 val ty = ty1 lub ty2
		     in (* should share some non-any supertype *)
			if topInType ty then type_error "expression" line (EQUAL (e,ty1,ty2)) else r := ty;
			BoolType
		     end else
		(* OR, AND *)
		let val ty1 = checksub line e1 BoolType (typeExp env line e1)
		    val ty2 = checksub line e2 BoolType (typeExp env line e2)
	        in r := BoolType; BoolType
	        end
  	| BinopExp (ope,r,q,ee) => (* enqueue, append *)
		(case typeExp env line q of
		  qty as QueueType (IntExp (k,_),ty) =>
			let val ety = ty lub (typeExp env line ee)
			    val top = queue_type k ety
			in r := top; top
			end
		| ty => type_error "expression" line (SUBT (e,ty,AnyQueue)))
  	| UnopExp (ope,r,ee) => (case ope of  (* not, full, empty, dequeue, minus *)
		  NOT     => (r := BoolType; checksub line ee BoolType (typeExp env line ee))
		| PLUS    => let val ty = checksub line ee IntType (typeExp env line ee) in r := ty; ty end
		| MINUS   => let val ty = checksub line ee IntType (typeExp env line ee) in r := ty; ty end
		| COERCE  => let val ty = checksub line ee IntType (typeExp env line ee) in r := BotType;  BotType end
		| EMPTY   => (case typeExp env line ee of
			     qty as QueueType _ => (r := qty; BoolType)
			   | ty => type_error "expression" line (SUBT (e,ty,AnyQueue)))
		| FULL    => (case typeExp env line ee of
			     qty as QueueType _ => (r := qty; BoolType)
			   | ty => type_error "expression" line (SUBT (e,ty,AnyQueue)))
		| DEQUEUE => (case typeExp env line ee of
			     qty as QueueType _ => (r := qty; qty)
			   | ty => type_error "expression" line (SUBT (e,ty,AnyQueue)))
		| FIRST   => (case typeExp env line ee of
			     qty as QueueType (_,ty) => (r := qty; ty)
			   | ty => type_error "expression" line (SUBT (e,ty,AnyQueue))))
	| ConstrExp (c,e',r) => (* union constructions *)
	        (case getVar c env line "construction" of
		     Constr (argty,ty) => (case argty of
			     BotType =>  type_error "construction" line (OneARY c)
			   | _ => (checksub line e' argty (typeExp env line e'); r := ty; ty))
	     	   | Constant _ => (* c not a constructor *)
		           type_error "construction" line (CONSTR c)
		   | Var _ => (* c not a constructor *)
		           type_error "construction" line (CONSTR c)
		   | _ => type_error "expression" line (UNBOUND ("variable",c)))
	| IntExp (k,r) => (r := interval_type k k; !r)
	| BoolExp _ => BoolType
	| QueueExp ([],ref qty) => qty
	| QueueExp (el,r) =>
		let val tyl = List.map (typeExp env line) el
		    val aty = rlub tyl
		in (* TopType could be left and trapped later, but clearer that way ... *)
		   if topInType aty then type_error "queue expression" line (QUEUEINIT tyl) else
		   (* put temporary type in r and return infinite queue type *)
		   (r := queue_type (length el) aty; queue_type (~1) aty)
		end
	| RecordExp (fl,r) =>
	        let val rty = RecordType (List.map (fn (f,v) => ([f],typeExp env line v)) fl)
		in r := rty; rty
		end
	| ArrayExp (il,r) => 
		let val tyl = List.map (typeExp env line) il
		    val aty = rlub tyl
		in (* TopType could be left and trapped later, but clearer that way ... *)
		   if topInType aty then type_error "array initializer" line (ARRAYINIT tyl) else
		   let val ill = length il
		       val aty' = array_type ill aty
		   in r := aty'; aty'
		   end
		end
	| CondExp (c,e1,e2,r) =>
	        let val _ = checksub line c BoolType (typeExp env line c)
		    val ty1 = typeExp env line e1
		    val ty2 = typeExp env line e2
		    val ty = ty1 lub ty2
		in r := ty; ty
		end
	| ParenExp e => typeExp env line e;


(* called for initializing upper bounds for types, promotes numeric types *)
fun top ty = case ty of
	  BoolType => ty
	| NatType => IntType
	| IntervalType _ => IntType
	| RecordType fl => RecordType (map (fn (f,t) => (f,top t)) fl)
	| UnionType fl => UnionType (map (fn x as (f,BotType) => x | (f,t) => (f,top t)) fl)
	| ArrayType (e,t) => ArrayType (e,top t)
	| QueueType (e,t) => QueueType (e,top t)
	| _ => ty;

fun relaxExp line bnd e =
	if not (!MAXIMIZE) then () else case e of
	  ArrayAccessExp (a,i,r as ref (ArrayType (IntExp (k,rty),_))) => (* !r is type of a *)
		let val aty = ArrayType (IntExp (k,rty),bnd)
		in r := aty;
		   relaxExp line (interval_type 0 (k-1)) i;
		   relaxExp line aty a
		end
	| RecordAccessExp (a,f,r as ref (RecordType fl)) => (* !r is type of a *)
		let val rty = RecordType (map (fn gty as ([g],_) => if g=f then ([f],bnd) else gty) fl)
		in r := rty;
		   relaxExp line rty a
		end
	| InfixExp (ope,r as ref ty,e1,e2) => (* r is type of arguments *)
		if mem ope [ADD,SUB,DIV,MUL,MOD]
		then (r := bnd; relaxExp line bnd e1; relaxExp line bnd e2) else
		if mem ope [LE,LT,GE,GT]
		then (r := IntType; relaxExp line IntType e1; relaxExp line IntType e2) else
		if mem ope [EQ,NE]
		then let val ty' = top ty in r := ty'; relaxExp line ty' e1; relaxExp line ty' e2 end else
		(r := bnd; relaxExp line BoolType e1; relaxExp line BoolType e2) (* OR, AND *)
  	| BinopExp (ope,r,q,ee) => (* enqueue, append, !r is type of queue *)
		let val QueueType (_,ty) = bnd in r := bnd; relaxExp line ty ee; relaxExp line bnd q end
  	| UnopExp (ope,r as ref ty,ee) => (case ope of
		  MINUS   => (r := bnd; relaxExp line bnd ee)
		| PLUS    => (r := bnd; relaxExp line bnd ee)
		| COERCE  => (r := bnd; relaxExp line IntType ee)
		| FULL    => let val QueueType (IntExp (k,_),ty') = ty
			         val qty = queue_type k (top ty')
			     in r := qty; relaxExp line qty ee
			     end
		| EMPTY   => let val QueueType (IntExp (k,rty),ty') = ty
			         val qty = QueueType (IntExp (k,rty),top ty')
			     in r := qty; relaxExp line qty ee
			     end
		| DEQUEUE => (r := bnd; relaxExp line bnd ee)
		| FIRST   => let val QueueType (IntExp (k,rty),_) = ty
				 val qty = QueueType (IntExp (k,rty),bnd)
			     in r := qty; relaxExp line qty ee
			     end
		| _       => ())
	| ConstrExp (c,e,r) =>
		let val UnionType tfl = bnd
		in r := bnd; case assoc [c] tfl of BotType => () | ty => relaxExp line ty e
		end
	| RecordExp (fl,r) =>
	        let val RecordType tfl = bnd
		in List.app (fn ((_,e),(_,ty)) => relaxExp line ty e) (ListPair.zip (fl,tfl)); r := bnd
		end
	| ArrayExp (el,r) =>
	        let val ArrayType (_,ty) = bnd
		in List.app (relaxExp line ty) el; r := bnd
		end
	| QueueExp (el,r) =>
	        (* if bnd defined and !r "fits" in bnd then ok else fail *)
		if not (isDefined bnd) then type_error "expression" line (AMBIGUOUS (e,bnd)) else
		let val QueueType (IntExp (k,_),ty) = bnd
		    val QueueType (IntExp (k',_),_) = !r
		in if k' <= k
		   then (List.app (relaxExp line ty) el; r := bnd)
	           else type_error "expression" line (SUBT (e,!r,bnd))
		end
	| CondExp (c,e1,e2,r) => (r := bnd; relaxExp line BoolType c; relaxExp line bnd e1; relaxExp line bnd e2)
	| IntExp (_,r) => r := bnd
	| IdentExp (_,r as ref (knd,_)) => r := (knd,bnd)
	| ParenExp e => relaxExp line bnd e
	| _ => ();

(* typechecks a typebounded expression, then relaxes it % bound : *)
fun checkExp line env tybnd e =
	(checksub line e tybnd (typeExp env line e); relaxExp line tybnd e);



(* ----------------- evaluation of constants  ---------------------- *)

(* evaluation of expressions in global environment (used for declared constants and expressions in types: *)

fun checkRange line (e as IntExp (n,ref ty)) = case ty of
        IntType => e
      | NatType => if n>=0 then e else type_error "constant expression" line OVFI
      | IntervalType (IntExp (f,_), IntExp (t,_)) => if n>=f andalso n<=t then e else type_error "constant expression" line OVFI;

(* evaluates an expression in global context genv. e assumed well typed, with primitives
   annotated with their actual result types *)
exception Undef;
fun expVal line e = case e of
          IdentExp (v,_) => (* constructors evaluated as themselves here *)
		(case lookup ("v%"^v) genv of
		  SOME (Constant (_,z)) => z
		| SOME (Constr _) => e
		| _ => raise Undef)
	| RecordAccessExp (a,f,r) =>
                let val RecordExp (fl,_) = expVal line a
		in assoc f fl
		end
	| ArrayAccessExp (a,i,r) =>
		let val ArrayExp (el,_) = expVal line a
		    val IntExp (x,_) = expVal line i
		in List.nth (el,x)
		end
	| ConstrExp (c,e,r) => ConstrExp (c,expVal line e,r)
  	| InfixExp (ope,ref ty,e1,e2) =>
		(case (expVal line e1, ope, expVal line e2) of
		  (IntExp (i1,_), ADD, IntExp (i2,_)) => checkRange line (IntExp (i1 + i2, ref ty))
		| (IntExp (i1,_), SUB, IntExp (i2,_)) => checkRange line (IntExp (i1 - i2, ref ty))
		| (IntExp (i1,_), MUL, IntExp (i2,_)) => checkRange line (IntExp (i1 * i2, ref ty))
		| (IntExp (i1,_), DIV, IntExp (i2,_)) => checkRange line (IntExp (i1 div i2, ref ty))
		| (IntExp (i1,_), MOD, IntExp (i2,_)) => checkRange line (IntExp (i1 mod i2, ref ty))
		| (IntExp (i1,_), LE, IntExp (i2,_)) => BoolExp (i1 <= i2)
		| (IntExp (i1,_), LT, IntExp (i2,_)) => BoolExp (i1 < i2)
		| (IntExp (i1,_), GE, IntExp (i2,_)) => BoolExp (i1 >= i2)
		| (IntExp (i1,_), GT, IntExp (i2,_)) => BoolExp (i1 > i2)
		| (BoolExp b1, OR, BoolExp b2) => BoolExp (b1 orelse b2)
		| (BoolExp b1, AND, BoolExp b2) => BoolExp (b1 andalso b2)
		| (e1, EQ, e2) => BoolExp (e1 = e2)
		| (e1, NE, e2) => BoolExp (e1 <> e2))
  	| BinopExp (ope,ref (qty as QueueType (IntExp (k,_),_)),q,ee) =>
		(case (ope, expVal line q, expVal line ee) of
		  (ENQUEUE,QueueExp (el,r),e) =>
			if List.length el = k
			then type_error "constant expression" line OVFQ
			else QueueExp (e::el,r)
		| (APPEND,QueueExp (el,r),e) =>
			if List.length el = k
			then type_error "constant expression" line OVFQ
			else QueueExp (el@[e],r))
  	| UnopExp (ope,ref ty,ee) =>
		(case (ope,expVal line ee) of
		  (NOT,BoolExp b) => BoolExp (not b)
		| (PLUS,e) => e
		| (MINUS, IntExp (n,_)) => checkRange line (IntExp (~n, ref ty))
		| (COERCE, IntExp (n,_)) => checkRange line (IntExp (n, ref ty))
		| (EMPTY, QueueExp (l,r)) => BoolExp (null l)
		| (FULL,QueueExp (l,ref (QueueType (IntExp (k,_),_)))) => BoolExp (List.length l >= k)
		| (DEQUEUE, QueueExp (l,r)) => (case l of
			  [] => type_error "constant expression" line UDFQ
			| _  => QueueExp (rev (tl (rev l)),r))
		| (FIRST, QueueExp (l,r)) => (case l of
			  [] => type_error "constant expression" line UDFQ
			| _  => List.last l))
	| RecordExp (fl,r) => RecordExp (List.map (fn (f,e) => (f,expVal line e)) fl, r)
	| ArrayExp (el,r) => ArrayExp (List.map (expVal line) el, r)
	| CondExp (c,e1,e2,_) => (case expVal line c of BoolExp true => expVal line e1 | _ => expVal line e2)
	| ParenExp e => expVal line e
	|  _ => e;
		     					

(* checkEvalConst: typechecks and evaluates a constant, must be a subtype of ty *)
fun checkEvalConst line e ty = (checkExp line genv ty e; expVal line e);



(* ------------------------- checking types and channels ----------- *)

fun checkDups _ _ [] = () | checkDups line item (h::t) =
	if mem h t then type_error "type declaration" line (DUP(item,h)) else checkDups line item t;

(* expands then sorts fields for record and union types *)
(* could be in parser if no type sharing in records and unions was allowed ... *)
fun sortfields fl =
    let val fl' = flat (map (fn (ll,ty) => map (fn x => ([x],ty)) ll) fl)
    in isort fl' (fn (([f1],_),([f2],_)) => String.< (f2,f1))
    end;

(* replace type idents by their definition in ty, returns expanded type *)
(* also registers union constructors and computes constants in sized types *)
fun checkType env line ty =
	let fun check ty = case ty of
		  NamedType id => (case lookup ("t%"^id) genv of SOME (TypeVar body) => body | _ =>
		    	type_error "declaration" line (UNBOUND ("type identifier",id)))
	        | UnionType ftyl'' =>
		        let val ftyl = sortfields ftyl''
			in checkDups line "union constructor" (flat (map fst ftyl));
		           let val u as UnionType ftyl' = UnionType (map (fn (f,ty) => (f,check ty)) ftyl)
			   in List.app (fn ([c],ty') => addConstr env line c ty' u) ftyl'; u
			   end
			end
	        | RecordType ftyl' =>
		        let val ftyl = sortfields ftyl'
			in checkDups line "field" (flat (map fst ftyl));
			   RecordType (map (fn (f,ty) => (f,check ty)) ftyl)
			end
		| ArrayType (e,ty) =>
		        ArrayType (checkEvalConst line e NatType, check ty)
		| QueueType (e,ty) =>
		        QueueType (checkEvalConst line e NatType, check ty)
		| IntervalType (f,t) =>
		        let val e1 as IntExp (ff,_) = checkEvalConst line f IntType
			    val e2 as IntExp (tt,_) = checkEvalConst line t IntType
			in if ff <= tt then IntervalType (e1, e2) else
			   type_error "interval type" line IEMPTY
			end
		| _ => ty
	in check ty
	end;

(* replace channel idents by their definition in ch, returns expanded channel *)
fun checkChannel env line ch =
	let fun check ch = case ch of
		(* NamedChannel never generated by parser, idents appear as typeids *)
		  ProfileChannel [NamedType id] => (case lookup ("t%"^id) genv of
			  SOME (ChannelVar body) => body
			| SOME (TypeVar body) => ProfileChannel [body]
			| _ => type_error "declaration" line (UNBOUND ("channel identifier",id)))
		| ProfileChannel tyl => ProfileChannel (map (checkType env line) tyl)
	in check ch
	end;



(* -------------- typing patterns ---------------------------------- *)

(* checks a lhs of assignement in typing context env, returns type of pattern *)
fun checkPattern env line e = case e of
	  AnyExp => (* "any" pattern *) TopType
	| IdentExp (v,r) => (case getVar v env line "pattern" of
		  Var (attrs,ty,lcl) =>
			(r := (VarKind,ty);
			 if null attrs then ty else
			 (SHM := true;
			  case !loc of
			       0 => if mem WRITE attrs then ty else
			            type_error "pattern" line (ACCESS("written",v))
			     | 1 => bad_init line (ISHV v)
			     | 2 => if lcl then ty else bad_init line (ISHV v)))
		| Constant (ty,_) => (r := (ConstantKind,ty); ty) 
		| Constr (argty,ty) =>
			(r := (ConstrKind,ty);
			 case argty of
			    BotType => ty
			  | _ => type_error "pattern" line (ZeroARY v))
		| _ => type_error "pattern" line (UNBOUND("variable",v)))
	| RecordAccessExp (a,f,r) => (case checkPattern env line a of
		  rty as RecordType fl => 
			(r := rty; assoc [f] fl handle _ => type_error "assignement lhs" line (FIELD (a,f)))
		| ty => type_error "pattern" line (SUBT (e,ty,RecordType [])))
	| ArrayAccessExp (a,i,r) => (case checkPattern env line a of
		  aty as ArrayType (IntExp (k,_),ty) => 
			(r := aty; checkExp line env (interval_type 0 (k-1)) i; ty)
		| ty => type_error "pattern" line (SUBT (e,ty,AnyArray)))
        | ConstrExp (c,e',r) => (* as in typeExp except checksub reversed *)
	        (case getVar c env line "pattern" of
		     Constr (argty,ty) => (case argty of
			     BotType =>  type_error "pattern" line (OneARY c)
			   | _ => (checksub line e' (checkPattern env line e') argty; r := ty; ty))
	     	   | Constant _ => (* c not a constructor *)
		           type_error "pattern" line (CONSTR c)
		   | Var _ => (* c not a constructor *)
		           type_error "pattern" line (CONSTR c)
		   | _ => type_error "pattern" line (UNBOUND ("variable",c)))
	| IntExp (n,r) => (r := IntType; IntType)
	| BoolExp b => BoolType
	| ParenExp e => checkPattern env line e
	| _ => type_error "unexpected case in checkPattern, please report it" line (LHS e);

(* checks independence of assignment pattern components *)
fun checkPatternIndep line patts =
	let datatype PA = UU | VV of string | AA of int | RR of string | ZZ
	    fun patt2PA p l = case p of
		  RecordAccessExp (a,f,_) => patt2PA a (RR f :: l)
		| ArrayAccessExp (a,e,_) => (let val IntExp (i,_) = expVal line e in patt2PA a (AA i :: l) end
					     handle Undef => patt2PA a (UU :: l))
		| ConstrExp (_,p,_) => patt2PA p l
		| IdentExp (v,ref (VarKind,_)) => VV v :: l
		| IdentExp (v,ref (ConstantKind,z)) => let val SOME (Constant (_,z)) = lookup ("v%"^v) genv in patt2PA z l end
		| IdentExp (v,_) => (* 0-ary constructor *) ZZ :: l
		| IntExp _ => (* integer pattern *) ZZ :: l
		| BoolExp _ => (* boolean pattern *) ZZ :: l
                | AnyExp => ZZ :: l
		| ParenExp p => patt2PA p l
		| _ => (patt2PA (expVal line p) l handle Undef => l)
	    fun indep (p1,l1) (p2,l2) =
		if List.exists (fn (VV s, VV s') => s <> s'
			         | (AA i, AA i') => i <> i'
			         | (RR f, RR f') => f <> f'
                                 | (ZZ, _) => true
                                 | (_, ZZ) => true
			         | _ => false) (ListPair.zip (l1,l2)) then true else
		(type_warning "assignement lhs" line (INDEP (p1,p2)); false)
	    fun check [_] = true | check (h::t) = List.all (fn p => indep h p) t andalso check t
	in check (List.map (fn p => (p,patt2PA p [])) patts)
	end;

(* checking pattern exhaustiveness and redundancy *)

datatype coverage =
         VAL of int                            (* int or nat constants                           *)
       | CON of string * int * coverage        (* constructions or intervals. constr, count, arg *)
       | ALL;                                  (* any or variables                               *)

(* returns coverage associated with a pattern, ty is type of pattern, sorted and expanded (by sortfields);
   beware: types in patts are not relaxed, which is why type of pattern is used instead *)
fun covOfPatt ty e = case e of
	AnyExp => ALL
      | IdentExp (_,ref (VarKind,_)) => ALL
      | IdentExp (v,ref (ConstantKind,_)) => covOfPatt ty (let val SOME (Constant (_,z)) = lookup ("v%"^v) genv in z end)
      | IdentExp (v,ref (ConstrKind,_)) => let val UnionType fl = ty in CON (v,length fl,ALL) end
      | RecordAccessExp (a,f,r) => ALL
      | ArrayAccessExp (a,i,r) => ALL
      | ConstrExp (c,e',_) => let val UnionType fl = ty in CON (c,length fl,covOfPatt (assoc [c] fl) e') end
      | IntExp (n,_) => (case ty of
	     IntervalType (IntExp (k1,_), IntExp (k2,_)) => CON (Int.toString n,(k2-k1)+1,ALL)
	   | _ => VAL n)
      | BoolExp b => CON (Bool.toString b,2,ALL)
      | ParenExp e => covOfPatt ty e
      | _ => (ErrPrint.print "unexpected case in covOfPatt, please report it\n"; ErrPrint.flush (); raise BYE);

(* breaks l into (l',aa), where l'@aa = l and l' is the longest prefix of l not containing any ALL;
   also marks useful duplicated patterns as redundant *)
fun break (l as ((_,ALL)::_)) accu = (rev accu,l)
  | break ((rh as (r,h))::t) accu = (if !r andalso List.exists (fn (_,x) => x=h) accu then r := false else (); break t (rh::accu))
  | break _ accu = (rev accu,[]);

(* gets toplevel constructors and count from coverage list *)
fun analyze ((_,CON (c,m,_))::t) cnl _ = analyze t (if mem c cnl then cnl else c::cnl) m
  | analyze (_::t) cnl k = analyze t cnl k
  | analyze _ cnl k = (cnl,k);

(* exhaustiveness check, sets redundancy as a side-effet: *)
fun exhaustive [] = false
  | exhaustive cl =
    let val (cl',aa) = break cl []
	val (constrs,count) = analyze cl' [] 0
	val e' = (* map then all necessary, since all stops at first false ... *)
	    List.all (fn b => b)
		     (List.map (fn c => exhaustive (List.map (fn (r,CON (_,_,arg)) => (r,arg) | v => v)
							     (List.filter (fn (_,CON (i,_,_)) => i=c | _ => true) cl')))
			       constrs)
	val e = if null cl' orelse null constrs orelse length constrs < count then false else e'
    in (* patts in aa are redundant, except the first if not e : *)
	case aa of [] => () | (r,_)::t => (if e then r := false else (); List.app (fn (r,_) => r := false) t);
	e orelse not (null aa)
    end;

(* main routine for assignement patterns *)
fun checkTupleMatch line tyl pl =
    if List.all (fn (ty,p) => exhaustive [(ref true,covOfPatt ty p)]) (ListPair.zip (tyl,pl)) then () else
    if !loc > 0
    then bad_init line (IXHAUST pl)
    else type_warning "assignement" line (XHAUST pl);

(* main routine for case patterns *)
fun checkCaseMatch line ty psl =
    let val e = exhaustive (map (fn (_,r,p,_) => (r,covOfPatt ty p)) psl)
    in if e then () else
       if !loc > 0
       then bad_init line (IXHAUST (map (fn (_,_,p,_) => p) psl))
       else type_warning "case statement" line (XHAUST (map (fn (_,_,p,_) => p) psl));
       List.app (fn (line,ref b,p,_) => if b then () else type_warning "case statement" line (REDUNT p)) psl
    end;



(* ------------------- typing statements --------------------------- *)

(* for checkStatement: *)
fun maxmin ((x1,y1),(x2,y2)) = (union x1 x2,Int.min(y1,y2));

(* returns [Shm] if e reads or writes some shared var, or otherwise [] *)
fun resetShm () = SHM := false;
fun checkShm () = if !SHM then (SHM := false; [Shm]) else [];

(* checks if ty enabled for any: *)
fun check_any_enabled line p ty = case ty of
	  BoolType => () | IntType => () | NatType => () | IntervalType _ => ()
	| _ => type_error "any assignement" line (ANYTY (p,ty));

(* checks a statement in context, returns: (StKind, lower bound for number of terminating "to") *)
fun checkStatement env s = (case s of
	  StNull => ([],0)
	| StTo (line,target) =>
		if !loc > 1 then bad_init line TOST else
		(case lookup ("s%"^target) env of SOME State => ([],1) | _ =>
		 type_error "to statement" line (UNKNOWN ("target state",target)))
	| StSequence (line,s1,s2) =>
		let val (k1,toc1) = checkStatement env s1
		    val (k2,toc2) = checkStatement env s2
		    val k = case (k1,k2) of
			   ([],_) => k2
			 | (_,[]) => k1
			 | ([Shm],_) => if mem Com k2
					then type_error "statement" line (SINGLECOM (s,k1,k2))
					else k1
			 | (_,[Shm]) => if mem Com k1
					then type_error "statement" line (SINGLECOM (s,k1,k2))
					else k2
			 | _ => type_error "statement" line (SINGLECOM (s,k1,k2))
		in (k,Int.max(toc1,toc2))
		end
	| StCond (line,e1,s1,e2s2l,s3) =>
		(resetShm ();
		 checkExp line env BoolType e1;
		 List.foldl maxmin ([],1)
			((checkShm (),1) ::
			 checkStatement env s1 ::
			 List.map (fn (line,e,s) => (checkExp line env BoolType e; checkStatement env s)) e2s2l @
			 (case s3 of NONE => [([],0)] | SOME s => [checkStatement env s]) ))
	| StWhile (line,e,s) =>
		(resetShm ();
		 checkExp line env BoolType e;
		 let val sh = checkShm ()
		     val (k,c) = checkStatement env s
		 in if mem Com k
		    then type_error "statement" line (COMINWHILE s)
		    else (union sh k, 0)  (* 0 since condition may never hold *)
		 end)
	| StForeach (line,v,t,s') =>
		(case getVar v env line "foreach statement" of
		     Var (attrs,ty as IntervalType it,lcl) => 
			   let val shared = ref false
			   in t := ty;
			      if null attrs then () else
			      (shared := true;
			       case !loc of
				   0 => if mem READ attrs
					then if mem WRITE attrs
					     then ()
					     else type_error "expression" line (ACCESS("written",v))
					else type_error "expression" line (ACCESS("read",v))
				 | 1 => bad_init line (ISHV v)
				 | 2 => if lcl then () else bad_init line (ISHV v));
			      let val ck as (knd,_) = checkStatement env s'
			      in if !shared andalso mem Com knd
				 then type_error "statement" line (SINGLECOM (s,[Shm],knd))
				 else ck
			      end
			   end
		   | Var (_,ty,_) => type_error "foreach statement" line (FETY (v,ty))
		   | _ => type_error "foreach statement" line (FEVAR v))
	| StSelect sl => List.foldl maxmin ([],1) (List.map (checkStatement env) sl)
	| StSignal (line,p,r) =>
		if !loc > 0 then bad_init line IOST else
		((* raises COMM if none is not profile *)
		 case lookup ("p%"^p) env of
		  SOME (Port (_,tyl)) =>
		        (r := ProfileChannel tyl;
			 case tyl of [] => ([Com],0) | _ => type_error "communication" line (COMM (p,[],tyl)))
		| _ => type_error "communication" line (UNKNOWN ("port",p)))
	| StInput (line,p,r,pl,exp) =>
		if !loc > 0 then bad_init line IOST else
		let val _ = resetShm ()
		in case lookup ("p%"^p) env of
		    SOME (Port (attrs,pty)) =>
			(r := ProfileChannel pty;
			 if not (mem IN attrs) then type_error "input communication" line (PMODE (p,IN)) else
			 (* profile must b a subtype of accesses *)
			 List.app (fn (p,ty) => (checksub line p (checkPattern env line p) ty; ()))
				  (ListPair.zipEq (pl,pty)
				   handle _ => type_error "input communication" line (COMM (p,pl,pty)));
			 checkPatternIndep line pl;
			 case exp of NONE => () | SOME e => checkExp line env BoolType e;
			 case checkShm() of [] => ([Com],0) | _ => type_error "statement" line (SINGLECOM (s,[Shm],[Com])))
		  | _ => type_error "input communication" line (UNKNOWN ("port",p))
		end
	| StOutput (line,p,r,el) =>
		if !loc > 0 then bad_init line IOST else
		let val _ = resetShm ()
		in case lookup ("p%"^p) env of
		    SOME (Port (attrs,pty)) =>
			(r := ProfileChannel pty;
			 if not (mem OUT attrs) then type_error "output communication" line (PMODE(p,OUT)) else
			 (* type of messages must be a subtype of profile *)
			 List.app (fn (e,ty) => checkExp line env ty e)
				  (ListPair.zipEq (el,pty)
				   handle _ => type_error "output communication" line (COMM (p,el,pty)));
			 case checkShm() of [] => ([Com],0) | _ => type_error "statement" line (SINGLECOM (s,[Shm],[Com])))
		  | _ => type_error "output communication" line (UNKNOWN ("port",p))
		end
	| StAssign (line,patts,exps) =>
		let val _ = resetShm ()
		    val ptyl = List.map (fn (p,e) => let val pty = checkPattern env line p
						     in checkExp line env pty e; pty 
						     end)
					(ListPair.zipEq (patts,exps) handle _ => type_error "assignement" line ASSIGN)
		in checkPatternIndep line patts;
		   checkTupleMatch line ptyl patts;
		   (checkShm (),0)
		end
	| StAny (line,patts,r,exp) =>
		let val _ = resetShm ()
		    val ptyl = List.map (fn p => let val pty = checkPattern env line p
						 in check_any_enabled line p pty; pty
						 end)
					patts
		in case exp of NONE => ()
			     | SOME e => if !loc > 0
					 then bad_init line IWHERE
					 else checkExp line env BoolType e;
		   checkPatternIndep line patts;
		   checkTupleMatch line ptyl patts; (* exhaustiveness not garanteed if where present ... *)
                   r := ptyl;
		   (checkShm (),0)
		end
	| StCase (line,e,psl) =>
		 let val _ = resetShm ()
		     val pty = rglb (map (fn (_,_,p,_) => checkPattern env line p) psl)
		 in checkExp line env pty e;  (* e must have a subtype of pty *)
		    checkCaseMatch line pty psl;
		    let val shm = checkShm ()
			val (coms,tgt) = List.foldl maxmin ([],1) (List.map (fn (_,_,_,s) => checkStatement env s) psl)
		    in case shm of [] => () | _ => if mem Com coms then type_error "statement" line (SINGLECOM (s,[Shm],coms)) else ();
		       (coms,tgt)
		    end
		 end
	) handle TYPE => ([],1);  (* prevents "target" error when other typing error *)


(* ------------------- checking other elements --------------------- *)

fun checkPortDecl env (pl as ((line,_)::_),attrs,ch) =
	(* ports are registered in the local context. *)
	(* because we will need these for checking compositions, port channels
	   and the union constructors they refer too are registered in the global env *)
	(* by default, ports have the in and out attributes;
	   - if in (or out) only is specified, port only has in (or out);
	   - if both or none are specified, port has both *)
	let val ch' as ProfileChannel pr = checkChannel genv line ch 	
	    fun check (line,p) =
		addPort env line (case attrs of [] => [IN,OUT] | _ => attrs) p pr handle TYPE => ()
	in List.app check pl; (pl,attrs,ch')
	end;

fun checkArgDecl env (vl as ((line,_)::_),attrs,ty) =
	(* formal parameters are registered in the local context *)
	(* because we will need these for checking compositions, arg types and the
	   union constructors they refer too are registered in the global env *)
	let val ty' = checkType genv line ty
	    fun check (line,id) = addVar env line attrs false id ty' handle TYPE => ()
	in List.app check vl; (vl,attrs,ty')
	end;

fun checkStateDecl env (d as (line,states)) =
	(* states are registered in the local context *)
	(List.app (addState env line) states handle TYPE => (); d);

(* checks and registers a constant declaration *)
fun checkConstantDecl (d as (line,id,ty,e)) =
        let val ty' = checkType genv line ty
	    val e' = checkEvalConst line e ty'
	in addConstant line id e' ty'; ConstantDecl (line,id,ty',e')
	end handle TYPE => ConstantDecl d;

(* checks a local var declaration found in a process (found=1) or component (found=2) *)
fun checkVarDecl found env (d as (vl as ((line,_)::_),ty,ival)) =
	(* local variables are registered in the local context *)
	let val ty' = checkType env line ty
	    fun check (line,id) =
		(* this is very permissive:
		   local vars may be used in initial values of previously initialized local vars. *)
		(* NOTE: it is still not checked if vars are indeed initialized before their first use *)
		(case found of
		        1 => addVar env line [] true (ValArg id) ty'
		      | 2 => addVar env line [READ,WRITE] true (RefArg id) ty';
		 case ival of
			NONE => () (* initialization mandatory in components, but will
				      be checked later by generic mechanism *)
		      | SOME e => checkExp line env (checkPattern env line (IdentExp (id,ref (OpenKind,BotType)))) e)
	in List.app check vl; (vl,ty',ival)
	end handle TYPE => d;

(* list of current source states *)
val sources = ref ([] : string list);

fun checkTransition env (line,source,statement) =
	((* source state declared ? *)
	 case lookup ("s%"^source) env of SOME State => () | _ =>
		type_error "transition" line (UNKNOWN ("source state",source));
	 (* source state is fresh : *)
	 if mem source (!sources) then type_error "transition" line (DUPFROM source) else
		sources := source :: !sources;
	 (* check statement and then that target state specified along any path : *)
	 let val (_,tocnt) = checkStatement env statement
	 in if tocnt < 1 then type_warning "transition" line (NOTARGET source) else ()
	 end)
	handle TYPE => ();

(* checks compositions, checks port guards, expands stars *)
fun checkComposition env comp = 
    let fun rcheck comp =
	    case comp of
		ParComp (line,pl,pcl) =>
		let val z = List.map (fn (e,c) => (e,rcheck c)) pcl
		    val sort = runion (List.map (fn (AllPorts,(_,s)) => s
						  | (SomePorts pl,(_,s)) => 
							      if List.all (fn x => mem x s) pl then s
							      else type_error "composition" line (SORT (List.filter (fn x => not (mem x s)) pl,s))) z)
		in (case pl of
			AllPorts => ParComp (line,SomePorts [],List.map (fn (_,(c,s)) => (SomePorts s,c)) z)
		      | SomePorts el => ParComp (line,
						 SomePorts [],
						 List.map (fn (e,(c,s)) =>
							if List.all (fn x => mem x s) el
							then (case e of AllPorts => (SomePorts s,c)
								      | SomePorts el' => (SomePorts (union el el'),c))
							else type_error "composition" line (SORT (List.filter (fn x => not (mem x s)) el,s))) z),
		    sort)
		end
	      | InstanceComp ((line,name),ports,args) =>
		(let val Comp (pl,al) = getComponent name line "composition"
		     fun inst_error err = type_error "composition" line (INST (name,err))
		 in (* check ports *)
		     List.app (fn (port,Port (at,pr)) =>
				  let val Port (at',pr') = getPort port env line "component instance"
				  in if subset at at'
				     then if pr psub pr' andalso pr' psub pr then () else
					  inst_error (PortProf (port,pr',pr))
				     else inst_error (PortAttr (port,at,at'))
				  end)
			      (ListPair.zipEq (ports,pl) handle _ => inst_error (PortCount (length ports,length pl)));
		     (* check args *)
		     List.app (fn (arg,Var (at,ty,_)) =>
				  if mem READ at orelse mem WRITE at
				  then (case arg of
					    RefExp (v,r) => (case lookup ("v%"^v) env of
							         SOME (Var (at',_,_)) =>
							         if subset at at'
							         then let val ty' = typeExp env line (IdentExp (v,ref (OpenKind,BotType)))
								      in if ty' sub ty andalso ty sub ty' then r := ty else
							                 inst_error (VarType (v,ty',ty))
								      end
							         else inst_error (VarAttr (v,at',at))
							       | _ => type_error "instance argument" line (UNBOUND ("variable",v)))
					  | _ => inst_error (RefExpected arg))
				  else (case arg of
					    RefExp _ => inst_error (ExpExpected arg)
					  | _ => checkExp line env ty arg))
			      (ListPair.zipEq (args,al) handle _ => inst_error (ArgCount (length args,length al)))
		 end handle TYPE => ();
		 (comp,ports))
    in fst (rcheck comp)
    end;

(* checks a local port declaration *)
fun checkLocPortDecl env (d as (dl,interval)) =
	(* local ports registered in local env *)
	let fun checkInterval interval =
		let val bad = case interval of
			  (Closed l, Closed r) => l>r
			| (Closed l, Open r) => l >= r
			| (Open l, Closed r) => l >= r
			| (Open l, Open r) => l >= r
			| _ => false
		in if bad then type_error "delay declaration" (#1 (hd (#1 (hd dl)))) (DELAY interval) else (); interval
		end
	in (List.map (checkPortDecl env) dl, checkInterval interval)
	end handle TYPE => d;

fun checkPrio env (tl1,tl2) = 
	(* checks existence of ports. No priority relation checks, currently *)
	(List.app (fn (line,g) => (getPort g env line "priority declaration"; ())) tl1;
	 List.app (fn (line,g) => (getPort g env line "priority declaration"; ())) tl2)
	handle TYPE => ();

(* checks a type declaration *)
fun checkTypeDecl (d as (line,id,ty)) =
	(* type abbreviations and the constructors they possibly refer too
	   are registered in the global typing context *)
        let val ty' = checkType genv line ty
	in addType line id ty'; TypeDecl (line,id,ty')
	end handle TYPE => TypeDecl d;

(* checks a channel declaration *)
fun checkChannelDecl (d as (line,id,ty)) =
	(* channel abbreviations and the constructors they possibly refer too
	   are registered in the global typing context *)
        let val ty' = checkChannel genv line ty
	in addChannel line id ty'; ChannelDecl (line,id,ty')
	end handle TYPE => ChannelDecl d;

(* registers process or component in global env *)
fun registerComponent line c portdec argdec env =
	(* info for ports and args is retrieved from local env of component *)
	let val ports = flat (map (fn (d,_,_) => map (fn (_,p) => getPort p env line "") d) portdec)
	    val args = flat (map (fn (d,_,_) => map (fn (_,ValArg v) => getVar v env line ""
						      | (_,RefArg v) => getVar v env line "") d) argdec)
	in addComponent line c ports args
	end;

(* checks optional prelude *)
fun checkPrelude env w prel =
	(case prel of NONE => ()
		    | SOME (line,s) =>
		      (loc := w;
		       let val (_,tocnt) = checkStatement env s
		       in loc := 0;
			  if tocnt < 1 andalso w < 2 then
			      if w > 0
			      then bad_init line (NOTARGET "")
			      else type_warning "init statement" line (NOTARGET "") 
			  else ()
		       end);
	 prel);

(* checks a process declaration *)
fun checkProcessDecl (p as (line,name,portdec,argdec,statedec,vardec,prelude,transitions)) =
	(* process names are registered in the global typing context *)
	let val env = avlTree.newTree() : Idents avlTree.tree ref (* local typing context *)
	    val p' = ProcessDecl
		     (line,
		      name,
	 	      List.map (checkPortDecl env) portdec,
		      List.map (checkArgDecl env) argdec,
		      checkStateDecl env statedec,
		      List.map (checkVarDecl 1 env) vardec,
		      checkPrelude env 1 prelude,
		      (sources := []; List.app (checkTransition env) transitions; transitions))
	in registerComponent line name portdec argdec env; p'
	end handle TYPE => ProcessDecl p; 

(* checks a component declaration *)
fun checkComponentDecl (c as (line,name,portdec,argdec,vardec,locportdec,priodec,prelude,composition)) =
	(* component names are registered in the global typing context *)
	let val env = avlTree.newTree() : Idents avlTree.tree ref (* local typing context *)
	    val c' = ComponentDecl
		     (line, 
		      name,
		      List.map (checkPortDecl env) portdec,
		      List.map (checkArgDecl env) argdec,
		      List.map (checkVarDecl 2 env) vardec,
		      List.map (checkLocPortDecl env) locportdec,
		      (List.app (checkPrio env) priodec; priodec),
		      checkPrelude env 2 prelude,
		      checkComposition env composition)
	in registerComponent line name portdec argdec env; c'
	end handle TYPE => ComponentDecl c;

(* checks the body ident of a program *)
fun checkMain (line,id) =
	(getComponent id line "main"; ()) handle TYPE => ();

(* checks a fiacre program, side-effects a type-annotated program *)
fun checkDecl d = case d of
	  TypeDecl d => checkTypeDecl d
	| ChannelDecl d => checkChannelDecl d
	| ConstantDecl d => checkConstantDecl d
	| ProcessDecl d => checkProcessDecl d
	| ComponentDecl d => checkComponentDecl d;

(* main typechecking routine, returns p paired with "normalized" p, *)
(* also side effects type annotations, etc, in expressions and/or statements *)
(* "normalized p": type expressions replaced by types, stars expanded, constants evaluated, etc *)
fun typecheck (p as Program (dl,main)) =
	(* note: there must be a main when compiling for TINA or CADP *)
	if !TYPECHECK
	then (if !VERBOSE then print "::Typechecking\n" else ();
	      let val dl' = List.map checkDecl dl
	      in case main of
		     NONE => ((case !OUTPUT of
				   TTS => type_error "program" (~1) NOMAIN
				 | LOTOS => type_error "program" (~1) NOMAIN
				 | _ => type_warning "program" (~1) NOMAIN)
		              handle TYPE => ())
		   | SOME (Main ls) => checkMain ls;
		 if !VERBOSE then print "::Done\n" else ();
		 if !errcount>0 then raise BYE else (p,Program (dl',main))
	      end)
	else (p,p);

end; (* Type *)
