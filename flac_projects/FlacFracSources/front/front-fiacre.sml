
type line = int;	 (* source line number, for type error messages *)


structure Fiacre = struct

datatype kind =								(* kinds of idents in exprs	*)
	  OpenKind							(* undetermined			*)
	| VarKind							(* variable			*)
	| ConstantKind							(* constant and value		*)
	| ConstrKind;							(* constructor			*)

datatype Fiacre =
	  Program of Declaration list * Declaration option		(* declarations, optional main	*)
and Declaration =
	  TypeDecl of line * string * Type				(* lineno, name, body		*)
	| ChannelDecl of line * string * Channel			(* lineno, name, body		*)
	| ConstantDecl of line * string * Type * Exp			(* lineno, name, type, body	*)
	| ProcessDecl of
		line *							(* lineno			*)
		string *						(* name				*)
		PortDecl list *						(* ports parameters		*)
		ParamDecl list *					(* value or ref parameters	*)
		StatesDecl *						(* states			*)
		VarDecl list *						(* local variables		*)
		(line * Statement) option *				(* optional init statement	*)
		Transition list						(* the list of transitions	*)
	| ComponentDecl of
		line *							(* lineno			*)
		string *						(* name				*)
		PortDecl list *						(* ports parameters		*)
		ParamDecl list *					(* value or ref parameters	*)
		VarDecl list *						(* local variables		*)
		locPortDecl list *					(* hidden ports and delays	*)
		PriorDecl list *					(* local priority relation	*)
		(line * Statement) option *				(* optional init statement	*)
		Composition						(* body				*)
	| Main of line * string                                         (* program main                 *)
and Arg =
	  ValArg of string						(* arg passed by value		*)
	| RefArg of string						(* arg passed by reference	*)
and Type =
	  BoolType							(* bool				*)
	| NatType							(* nat				*)
	| IntType							(* int				*)
	| NamedType of string						(* abbreviation			*)
	| IntervalType of Exp * Exp			                (* interval exp..exp		*)
	| UnionType of (string list * Type) list			(* constructors, BotType or type*)
	| RecordType of (string list * Type) list			(* record			*)
	| ArrayType of Exp * Type				        (* array exp of type		*)
	| QueueType of Exp * Type				        (* queue exp of type		*)
	| AnyArray							(* top array type, internal	*)
	| AnyQueue							(* top queue type, internal	*)
	| BotType							(* bottom type, internal	*)
	| TopType							(* top type, internal		*)
and Channel =
	  NamedChannel of string					(* abbreviation			*)
	| ProfileChannel of Profile					(* none encoded ProfileChannel[]*)
and Exp = 
  	  ParenExp of Exp						(* (exp), kept for tracability	*)
	| CondExp of Exp * Exp * Exp * Type ref                         (* conditional exp              *)
  	| InfixExp of Infix * Type ref * Exp * Exp			(* infix operators		*)
  	| BinopExp of Binop * Type ref * Exp * Exp			(* binary operators		*)
  	| UnopExp of Unop * Type ref * Exp				(* unary operators		*)
	| RecordExp of (string * Exp) list * Type ref			(* {f1=e1, ..., fn=vn}		*)
	| QueueExp of Exp list * Type ref				(* queue constants, type	*)
	| ArrayExp of Exp list * Type ref				(* [e1, ..., en]		*)
        | RefExp of string * Type ref                  			(* only in instance arguments	*)
	| ConstrExp of string * Exp * Type ref				(* constructions		*)
	| IntExp of int * Type ref					(* numeric literal, type	*)
	| BoolExp of bool						(* bool literal			*)
	| IdentExp of string * (kind * Type) ref			(* var, constant or constructor	*)
	| AnyExp							(* "any" pattern		*)
	| ArrayAccessExp of Exp * Exp * Type ref			(* array subscript		*)
	| RecordAccessExp of Exp * string * Type ref			(* record field access		*)
and Infix =
  	  AND | OR | LE | LT | GT | GE | EQ | NE | ADD | SUB | MOD | MUL | DIV
and Binop =
	  ENQUEUE | APPEND
and Unop =
  	  MINUS	| PLUS | NOT | COERCE | FULL | EMPTY | DEQUEUE	| FIRST
and Statement =
	  StNull							(* does nothing statement	*)
	| StCond of					(* if e1 then s1 [elsif ei then si] [else sn] 	*)
		line *							(* if' lineno			*)
		Exp *							(* e1				*)
		Statement *						(* s1				*)
		(line * Exp * Statement) list *				(* list of (ei,si) 		*)
		Statement option					(* sn				*)
	| StSelect of Statement list					(* select s1 .. sn		*)
	| StSequence of line * Statement * Statement			(* s1; s2			*)
	| StTo of line * string						(* to state			*)
	| StWhile of line * Exp * Statement				(* condition, loop		*)
	| StAssign of line * Pattern list * Exp list 			(* access list := exp list	*)
	| StAny of line * Pattern list * (Type list) ref * Exp option	(* access list:=any [where exp]	*)
	| StSignal of line * string * Channel ref			(* event			*)
	| StInput of line * string * Channel ref * Pattern list * Exp option (* p?access list [where exp] *)
	| StOutput of line * string * Channel ref * Exp list		(* p!exp list			*)
	| StCase of line * Exp * (line * bool ref * Pattern * Statement) list	(* case exp of matches	*)
	| StForeach of line * string * Type ref * Statement             (* foreach v do ... end         *)
and Bound = 
	  Open of real							(* ]from or to[			*)
	| Closed of real						(* [from or to]			*)
	| Infinite							(* ...				*)
and VarAttr =
	  READ | WRITE
and PortAttr =
	  IN | OUT
and Composition =
	  ParComp of line * PortSet * (PortSet * Composition) list	(* par set_0 in set_i -> comp_i	*)
	| InstanceComp of Instance					(* name [pi] (ei or &v)		*)
and PortSet =
	  AllPorts							(* sort ( * )			*)
	| SomePorts of string list					(* port list, possibly empty	*)

withtype

	TypeDecl = line * string * Type
and 	ChannelDecl = line * string * Channel
and	PortDecl = (line * string) list * PortAttr list * Channel
and	ParamDecl = (line * Arg) list * VarAttr list * Type
and	StatesDecl = line * string list
and	VarDecl = (line * string) list * Type * Exp option
and 	PriorDecl = (line * string) list * (line * string) list
and 	locPortDecl = ((line * string) list * PortAttr list * Channel) list * (Bound * Bound)
and     Instance = (line * string) * string list * Exp list
and	Transition = line * string * Statement
and     Profile = Type list
and     Literal = Exp
and     Pattern = Exp;

end;


open Fiacre;


(* sorts record fields in record expressions *)
fun sortvfields fl = isort fl (fn ((f1,_),(f2,_)) => String.< (f2,f1));

(* propagates unary - or + to enclosed literal, if any, so that negative integers
   can appear in patterns *)
fun normPlus (UnopExp(_,_,a as IntExp _)) = a | normPlus e = e;
fun normMinus (UnopExp(_,_,IntExp (k,rty))) = IntExp (~k,rty) | normMinus e = e;




