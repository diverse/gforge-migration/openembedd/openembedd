

signature PARSE = sig
val parse : unit -> Fiacre
end;


structure Parse :> PARSE = struct

open ErrPrint;  (* prints on Err *)

structure FiacreLrVals = FiacreLrValsFun(structure Token = LrParser.Token);
  
structure FiacreLex = FiacreLexFun(structure Tokens = FiacreLrVals.Tokens);
  
structure FiacreParser =
	Join(structure LrParser = LrParser
	     structure ParserData = FiacreLrVals.ParserData
	     structure Lex = FiacreLex);
  
fun invoke lexstream =
	let fun error (s,i:int,c:int) =
		(print("Error: syntax error at line " ^ Int.toString (i+1) ^ (* *) "." ^ Int.toString (c+1)^ (* *) ":" ^ 
			String.substring (s,13,String.size s - 13) (* s *) ^ "\n");
		 if !VERBOSE then print("::Done\n") else ();
		 raise BYE)
	in FiacreParser.parse(0,lexstream,error,())
	end;

fun parse () =
      let fun readline _ = case TextIO.inputLine (!Input.FILE) of SOME l => l | NONE =>  ""
	  val lexer = FiacreParser.makeLexer readline
	  val dummyEOF = FiacreLrVals.Tokens.EOF(0,0)
	  fun loop lexer =
              let val (result,lexer) = invoke lexer
		  val (nextToken,lexer) = FiacreParser.Stream.get lexer
              in if TextIO.endOfStream (!Input.FILE)
		 then (if !VERBOSE then print "::Done\n" else (); result)
		 else (ignore (FiacreParser.sameToken(nextToken,dummyEOF)); loop lexer)
              end
      in if !VERBOSE then print "::Parsing\n" else ();
	 loop lexer
      end
     
end; (* Parse *)
