
(* output options *)
datatype output = NoOutput | Fiacre | TTS | LOTOS;
val OUTPUT = ref NoOutput;  (* output option, default just parses *)

(* mode flags *)
val VERBOSE = ref true;
val DETAILS = ref false;  (* for debugging specializations, with flags -g *)
val MAXIMIZE = ref true;  (* debug option for type instances of primitives *)
val TYPECHECK = ref true; (* typechecks or not *)
val TRANSF = ref false;   (* if -F, prints result of pre-processing rather than fiacre sources *)

(* error handling *)
exception BYE;  (* signals termination *)
fun error m = (ErrPrint.print m; ErrPrint.print "\n"; ErrPrint.flush (); raise BYE);

(* banner *)
val BANNER = "Front version "^version^release^" -- "^date^" -- FERIA/SVF & INRIA/VASY\n";


(* command line parser *)
fun command args =
	let open OutPrint
	    fun usage () =
		(print "usage: front [-h | -help]\n";
		 print "            [-q | -v] [-o errfile]\n";
		 print "            [-p | -c | -f]\n";
		 print "            [infile] [outfile]\n")
	    fun fronterror m =
	        (print "front: "; print m; print "\n"; usage(); raise BYE)
	    fun help () =
		(print BANNER;
		 usage();
		 print "FLAGS        WHAT                                    DEFAULT\n";
		 print "-h | -help   this mode                               \n";
	         print "error format:                                        \n";
		 print "-q            quiet                                  -v\n";
		 print "-v            verbose                                -v\n";
		 print "-o errfile    save errors in errfile (stderr if -)   stderr\n";
		 print "output format:                                       \n";
		 print "-p            just parses                            -c\n";
		 print "-c            just parses and checks                 -c\n";
		 print "-f | -fcr     outputs fiacre source                  -c\n";
		 print "files:                                               \n";
		 print "infile        input file (stdin if -)                stdin\n";
		 print "outfile       output file (stdout if -)              stdout\n";
		 flush ();
		 OS.Process.exit OS.Process.success)
	    val FILES = ref 0
	    fun concat l = String.concat (map (fn x => " "^x) l)
	    fun parse l = case l of
		[] => ()
	      | "-help"::_ => help()
	      | "-h"::_    => help()
	      | "-q"::t    => (VERBOSE := false; parse t)
	      | "-v"::t    => (VERBOSE := true; parse t)
	      | "-o"::file::t => (if String.size file > 1 andalso "-" = String.substring(file,0,1)
				  then fronterror ("bad command line, skipping "^file^" ... "^concat t)
				  else ErrPrint.name := file;
				  parse t)
	      | "-p"::t    => (OUTPUT := NoOutput; TYPECHECK := false; parse t)
	      | "-c"::t    => (OUTPUT := NoOutput; TYPECHECK := true; parse t)
	      | "-f"::t    => (OUTPUT := Fiacre; parse t)
	      | "-fcr"::t  => (OUTPUT := Fiacre; parse t)
(* private *) | "-g"::t    => (OUTPUT := Fiacre; DETAILS := true; MAXIMIZE := false; parse t)
(* private *) | "-G"::t    => (OUTPUT := Fiacre; DETAILS := true; MAXIMIZE := true; parse t)
(* private *) | "-F"::t    => (OUTPUT := Fiacre; TRANSF := true; parse t)
	      | file::t    => if String.size file > 1 andalso "-" = String.substring(file,0,1)
			      then fronterror ("bad command line, skipping "^file^" ... "^concat t)
	                      else (FILES := 1 + !FILES; case !FILES of
			    1 => if file = "-" then parse t else (Input.name := file; parse t)
			  | 2 => if file = "-" then parse t else (OutPrint.name := file; parse t)
			  | _ => fronterror ("bad command line, skipping "^file^" ... "^concat t))
	in parse args
	end;




