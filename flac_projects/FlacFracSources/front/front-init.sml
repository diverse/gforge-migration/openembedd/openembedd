
(**
 * Note: Note
 * This module contains the implementation of the algorithm for checking
 * well-initialization of Fiacre programs, as proposed by F. Lang in <REF>.
 *)

signature INIT = sig
    (** Group: Exported declarations *)

    (**
     * Function: Init.initcheck
     *
     * Checks that all variables are initialized before use.
     * Prints warnings on the error output.
     *
     * Parameters:
     *   p - fiacre program to check
     *
     * Returns:
     *   unit
     *
     * *Raises*:
     *   - <BYE> after printing an error message if an unexpected exception is raised
     *)
    val initcheck : Fiacre -> unit
end

structure Init :> INIT = struct
    (** Group: Internal declarations *)

    (**
     * Function: Init.verbose
     *
     * Prints the passed string on the standard output if verbose mode is
     * activated, does nothing otherwise.
     *
     * Parameters:
     *   s - string to print
     *
     * Returns:
     *   unit
     *)
    fun verbose s = if !VERBOSE then ErrPrint.print s else ()

    (**
     * Type: Init.initWarning
     *
     * Type of initialization warning messages
     *
     *)    
    datatype initWarning  =
    (**
     * Constructor: Init.UninitializedInVarDecl
     *
     * Indicates that a variable occuring in a variable declaration
     * may be uninitialized.
     *
     * Parameters:
     *   - line number of error
     *   - place of error
     *   - name of variable
     *)
       UninitializedInVarDecl of (line * string * string)

    (**
     * Constructor: Init.UninitializedInStatement
     *
     * Indicates that a variable occuring in a statement
     * may be uninitialized.
     *
     * Parameters:
     *   - line number of error
     *   - name of variable
     *)
    | UninitializedInStatement of (line * string)

    (**
     * Constructor: Init.UninitializedInInstance
     *
     * Indicates that a variable occuring in an instance
     * may be uninitialized.
     *
     * Parameters:
     *   - line number of error
     *   - name of component
     *   - name of variable
     *)
    | UninitializedInInstance of (line * string * string)

    (**
     * Constructor: Init.InvalidComponentInit
     *
     * Indicates that a component 'init' clause is invalid.
     *
     * Parameters:
     *   - line number of error
     *   - name of component
     *)
    | InvalidComponentInit of (line * string)

    (**
     * Constructor: Init.StatementUnreachable
     *
     * Indicates that a statement is unreachable.
     *
     * Parameters:
     *   - line number of error
     *)
    | StatementUnreachable of line

    (**
     * Constructor: Init.StateUnreachable
     *
     * Indicates that a state is unreachable.
     *
     * Parameters:
     *   - line number of error
     *   - name of component
     *   - name of state
     *)
    | StateUnreachable of line * string * string
    ;

    (**
     * Constructor: Init.warning
     *
     * Prints initialization warning on error stream
     *
     * Parameters:
     *   - a warning message of type initWarning
     *)
    fun warning x =
	let open ErrPrint
	    fun tell place ln msg =
		(print "Warning: "; print place; print " line "; printi (ln + 1); print ": "; print msg; print "\n"; flush ())
 	in case x of
	       UninitializedInVarDecl (ln, p, s) => tell p ln (s ^ " must be initialized before use")
	     | UninitializedInStatement (ln, s) => tell "statement" ln (s ^ " must be initialized before use")
	     | UninitializedInInstance (ln, c, s) => tell ("component '" ^ c ^ "'") ln (s ^ " must be initialized before use")
	     | InvalidComponentInit (ln, c) => tell ("component '" ^ c ^ "'") ln ("invalid init clause")
	     | StatementUnreachable ln => tell "statement" ln "cannot be reached"
	     | StateUnreachable (ln, p, s) => tell ("process '" ^ p ^ "'") ln ("state '" ^ s ^ "' cannot be reached")
	end;

    structure S = struct
        type ord_key = string
        val compare = String.compare
    end

    (* Maps with strings as keys. *)
    structure Map = SplayMapFn (S)

    (* Sets with strings as elements. *)
    structure Set = SplaySetFn (S)

    fun setToString s =
        String.concatWith ", " (Set.listItems s)

    fun contains e l =
        List.exists (fn x => x = e) l

    (**
     * Function: Init.var
     *
     * Extracts the set of variables from an expression.
     *
     * Parameters:
     *   e - expression
     *
     * Returns:
     *   the set of variables that occur in the passed expression
     *)
    fun var (ParenExp e) = var e
      | var (InfixExp (_, _, e1, e2)) = Set.union ((var e1), (var e2))
      | var (BinopExp (_, _, e1, e2)) = Set.union ((var e1), (var e2))
      | var (UnopExp (_, _, e)) = var e
      | var (RecordExp (l, _)) = varList (map #2 l)
      | var (QueueExp (l, _)) = varList l
      | var (ArrayExp (l, _)) = varList l
      | var (RefExp (id, _)) = Set.singleton id
      | var (ConstrExp (_, e, _)) = var e
      | var (IdentExp (id, (ref (VarKind, _)))) = Set.singleton id
      | var (ArrayAccessExp (e1, e2, _)) = Set.union ((var e1), (var e2))
      | var (RecordAccessExp (e, _, _)) = var e
      | var (CondExp (c,e1,e2,_)) = Set.union ((var c), Set.union ((var e1), (var e2)))
      | var _ = Set.empty
    (**
     * Function: Init.varList
     *
     * Extracts the set of variables from an expression list.
     *
     * Parameters:
     *   l - expression list
     *
     * Returns:
     *   the set of variables that occur in the passed expression list
     *)
    and varList l =
        List.foldl (fn (exp, acc) => Set.union (acc, (var exp)))
                   Set.empty
                   l

    (**
     * Function: Init.use
     *
     * Extracts the set of used variables from a pattern.
     *
     * Parameters:
     *   patt - pattern
     *
     * Returns:
     *   the set of used variables that occur in the passed pattern
     *)
    fun use (ParenExp p) = use p
      | use (ConstrExp (_, p, _)) = use p
      | use (ArrayAccessExp (e1, e2, _)) = Set.union ((var e1), (var e2))
      | use (RecordAccessExp (e, _, _)) = var e
      | use _ = Set.empty

    (**
     * Function: Init.def
     *
     * Extracts the set of defined variables from a pattern.
     *
     * Parameters:
     *   patt - pattern
     *
     * Returns:
     *   the set of defined variables that occur in the passed pattern
     *)
    fun def (ParenExp p) = def p
      | def (RefExp (id, _)) = Set.singleton id
      | def (ConstrExp (_, p, _)) = def p
      | def (IdentExp (id, (ref (VarKind, _)))) = Set.singleton id
      | def _ = Set.empty
    (**
     * Function: Init.defList
     *
     * Extracts the set of defined variables from a pattern list.
     *
     * Parameters:
     *   patt - pattern
     *
     * Returns:
     *   the set of defined variables that occur in the passed pattern list
     *)
    and defList l =
        List.foldl (fn (pat, acc) => Set.union (acc, (def pat)))
                   Set.empty
                   l

    val epsilon = ""

    (**
     * Function: Init.Y
     *
     * Checks that a statement does not use any uninitialized variable,
     * and computes the map of variables that are initialized after the
     * statement. Prints warnings on the error output.
     *
     * Parameters:
     *   x - set of already initialized variables
     *   st - statement to check
     *
     * Returns:
     *   a map from state identifiers to sets of initialized variables
     *
     *)
    fun Y x st =
        let
            val intersect = Map.unionWith Set.intersection
            val intersectList = List.foldl intersect Map.empty
            (* 'subset ln f e s' checks that (f e) is a subset of s.
                If not, an 'UninitializedInStatement' exception is raised. *)
            fun subset ln f e s =
                let
                    val diff = Set.difference ((f e), s)
                in
                    if not (Set.isEmpty diff) then
                        warning (UninitializedInStatement (ln, (setToString diff)))
                    else
                        ()
                end
            fun all l f = app f l
            fun op|-> (k, v) = Map.insert (Map.empty, k, v) 
	    infix 6 |->
        in
            case st of
                StNull => epsilon |-> x
              | StCond (ln, e, st1, l, stn) =>
                (subset ln var e x;
                 intersectList (map (Y x)
                                    (st1
                                     :: (map (fn (ln, e, st) => (subset ln var e x; st)) l)
                                     @ (case stn of SOME s => [s] | NONE => [StNull]))))
              | StSelect l => intersectList (map (Y x) l)
              | StSequence (ln, st1, st2) =>
                let
                    val y1 = (Y x st1)
                in
                    let
                        val (y1_without_epsilon, y1_epsilon) = Map.remove (y1, epsilon)
                        val y2 = Y y1_epsilon st2
                    in
                        intersect (y1_without_epsilon, y2)
                    end
                    handle LibBase.NotFound =>
                           (warning (StatementUnreachable ln);
                            y1)
                end
              | StTo (_, id) => id |-> x
              | StWhile (ln, e, st) => (subset ln var e x; Map.insert ((Y x st), epsilon, x))
	      | StForeach (_, v, _, st) => Y (Set.union (x, def (IdentExp (v,ref (VarKind,BotType))))) st
              | StAssign (ln, patt, exp) =>
                (all patt (fn p => subset ln use p x);
                 all exp (fn e => subset ln var e x);
                 epsilon |-> (Set.union (x, (defList patt))))
              | StAny (ln, patt, _, exp) =>
                let
                    val x_union_def_patt = Set.union (x, (defList patt))
                in
                    all patt (fn p => subset ln use p x);
                    case exp of SOME e => subset ln var e x_union_def_patt | NONE => ();
                    epsilon |-> x_union_def_patt
                end
              | StSignal _ => epsilon |-> x
              | StInput (ln, _, _, patt, exp) =>
                let
                    val x_union_def_patt = Set.union (x, (defList patt))
                in
                    all patt (fn p => subset ln use p x);
                    case exp of SOME e => subset ln var e x_union_def_patt | NONE => ();
                    epsilon |-> x_union_def_patt
                end
              | StOutput (ln, _, _, l) => (all l (fn e => subset ln var e x); epsilon |-> x)
              | StCase (ln, e, l) =>
                (subset ln var e x;
                 intersectList (map (fn (_, _, patt, st) =>
                                        (subset ln use patt x;
                                         Y (Set.union (x, (def patt))) st))
                                    (List.filter (fn (lnm,ref b,_,_) =>
						     (if b then () else
						      warning (StatementUnreachable lnm);
						      b)) l)))
        end

    (**
     * Function: Init.flattenParameters
     *
     * Flattens a list of parameters
     *
     * Parameters:
     *   params - list of parameter declarations
     *
     * Returns:
     *   a list of (p, a) couples where p is a parameter identifier,
     *   and a its related attributes
     *)
    fun flattenParameters params =
        List.concat (map (fn (args, attrs, _) =>
                             (map (fn (_, id) => (id, attrs)) args))
                         params)

    (**
     * Function: Init.usedAndDefined
     *
     * Computes the sets of used and defined variables from
     * lists of parameters and variables.
     *
     * Parameters:
     *   vars - list of variable declarations
     *   params - list of parameter declarations
     *
     * Returns:
     *   a couple (u, d) where u is the set of variables occuring in
     *   the expressions of the list of variable declarations,
     *   and d is the set of variables that are defined by the list of
     *   parameter declarations
     *)
    fun usedAndDefined vars params =
        let
            val usedByVarDecls = varList (List.mapPartial #3 vars)
            val definedByParams =
                List.foldl (fn (((ValArg id), _), acc) => Set.add (acc, id)
                             | (((RefArg id), attrs), acc) =>
                               if (attrs = []) orelse (contains READ attrs) then
                                   Set.add (acc, id)
                               else
                                   acc)
                           Set.empty
                           (flattenParameters params)
        in
            (usedByVarDecls, definedByParams)
        end

    (**
     * Function: Init.initializedVariables
     *
     * Computes the list of initialized variables from a list of variable
     * declarations.
     *
     * Parameters:
     *   vars - list of variable declarations
     *
     * Returns:
     *   the list of variables that are initialized by the passed list of
     *   variable declarations
     *)
    fun initializedVariables vars =
        map #2 (List.concat (List.mapPartial
                                 (fn (l, _, (SOME _)) => SOME l
                                   | _ => NONE)
                                 vars))

    (**
     * Function: Init.checkProcess
     *
     * Checks all variable uses inside a process.
     *
     * Parameters:
     *   p - process to check
     *
     * Returns:
     *   unit
     *
     *)
    fun checkProcess (ln, name, _, params, states, vars, init, trans) =
        let
            val (usedByVarDecls, definedByParams) = usedAndDefined vars params
            val difference = Set.difference (usedByVarDecls, definedByParams)
        in
            if not (Set.isEmpty difference) then
                warning (UninitializedInVarDecl (ln,
						 ("process '" ^ name ^ "'"),
						 (setToString difference)))
            else ();
                let
                    val x = ref (Map.empty : Set.set Map.map)
                    val visited = ref Set.empty
                    val revisit = ref Set.empty
                    val initVars = initializedVariables vars
                    fun stmtForTrans "" = (case init of
					       SOME (_, st) => st
                                             | NONE => (StTo (ln, (#2 (hd trans))))
					       handle Empty => StNull)
                      | stmtForTrans id = (case List.find (fn (_, id', _) => id = id') trans of
					       SOME x => #3 x
                                             | NONE => StNull)
		in
                    revisit := Set.add (!revisit, "");
                    x := Map.insert (!x, "", (Set.addList (definedByParams, initVars)));
                    while not (Set.isEmpty (!revisit)) do
			let
                            val i = hd (Set.listItems (!revisit))
			in
                            revisit := Set.delete (!revisit, i);
                            visited := Set.add (!visited, i);
                            let
				val y = Y (Option.valOf (Map.find (!x, i))) (stmtForTrans i)
                            in
				Map.appi
                                    (fn ("", _) => ()
                                      | (j, y_j) =>
					if not (Set.member (!visited, j)) then
                                            (x := Map.insert (!x, j, y_j);
                                             revisit := Set.add (!revisit, j))
					else
                                            let
						val x_j = Option.valOf (Map.find (!x, j))
                                            in
						if not (Set.isSubset (x_j, y_j)) then
                                                    (x := Map.insert (!x, j, (Set.intersection (x_j, y_j)));
                                                     revisit := Set.add (!revisit, j))
						else
                                                    ()
                                            end)
                                    y
                            end
			end;
                    app (fn s => if not (Set.member (!visited, s)) then
                                     warning (StateUnreachable (ln,name,s))
				 else
                                     ())
			(#2 states)
		end
        end

    (**
     * Function: Init.checkComponent
     *
     * Checks all variable uses inside a component.
     *
     * Parameters:
     *   paramMap - map from process/component identifiers to theirs related lists of parameters
     *   c - component to check
     *
     * Returns:
     *   unit
     *
     *)
    fun checkComponent paramMap (ln, name, _, params, vars, _, _, init, body) =
        let
            val (usedByVarDecls, definedByParams) = usedAndDefined vars params
            val difference = Set.difference (usedByVarDecls, definedByParams)
        in
            if not (Set.isEmpty difference) then
                warning (UninitializedInVarDecl (ln,
						    ("component '" ^ name ^ "'"),
						    (setToString difference)))
            else ();
                let
                    val initVars = initializedVariables vars
                    val x_0 = Set.addList (definedByParams, initVars)
                    val y = Y x_0 (case init of SOME (_, s) => s | NONE => StNull)
                    fun inst (ParComp (_, _, l)) = List.concat (map inst (map #2 l))
                      | inst (InstanceComp i) = [i]
                in
                    if ((Map.numItems y) <> 1)
                       orelse ((Map.find (y, epsilon)) = NONE) then
                        warning (InvalidComponentInit (ln, name))
                    else ();
                        let
                            val y_eps = Option.valOf (Map.find (y, epsilon))
                        in
                            app (fn ((ln, id), _, exp) =>
                                    let
                                        val couples =
                                            ListPair.zipEq ((Option.valOf ((Map.find (paramMap, id)))),
                                                            exp)
                                        val vars =
                                            List.foldl
                                                (fn (((_, attrs), (RefExp (e, _))), acc) =>
                                                    if (attrs = [])
                                                       orelse (contains READ attrs) then
                                                        Set.add (acc, e)
                                                    else
                                                        acc
                                                  | ((_, e), acc) => Set.union (acc, (var e)))
                                                Set.empty
                                                couples
                                        val diff = Set.difference (vars, y_eps)
                                    in
                                        if not (Set.isEmpty diff) then
                                            warning (UninitializedInInstance (ln,
										 name,
										 (setToString diff)))
                                        else
                                            ()
                                    end)
                                (inst body)
                        end
                end
        end

    fun initcheck (Program (decls, main)) =
	if !TYPECHECK then (
        let
            val paramMap =
                List.foldl (fn ((ProcessDecl (_, id, _, params, _, _, _, _)), acc) =>
                               Map.insert (acc, id, (flattenParameters params))
                             | ((ComponentDecl (_, id, _, params, _, _, _, _, _)), acc) =>
                               Map.insert (acc, id, (flattenParameters params))
                             | (_, acc) => acc)
                           Map.empty
                           ((case main of SOME m => [m] | NONE => []) @ decls)
	    fun check x y =
		case (x,y) of
		    (_, ProcessDecl p) => checkProcess p
		  | (paramMap, ComponentDecl c) => checkComponent paramMap c
		  | _ => ()

        in
            verbose "::Checking initialization of variables\n";
            app (check paramMap) decls;
            case main of
                SOME m => check paramMap m
              | NONE => ();
            verbose "::Done\n"
        end
        handle e =>
               (ErrPrint.print ("Internal error in initialization check: " ^ (exnMessage e) ^ "\n");
                app (fn s => ErrPrint.print ("    " ^ s ^ "\n")) (MLton.Exn.history e);
                raise BYE)
        ) else ()


end
