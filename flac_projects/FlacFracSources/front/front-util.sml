
(* adhoc IO utilities ---------------------------------------------- *)

(* Input structure stores name and stream for input,
   Output structures provide printing utilities for output or errors *)

(* input file and stream *)
structure Input = struct
val name = ref "";
val FILE = ref TextIO.stdIn;	(* default input stream *)
end;

(* signature for printing functions *)
signature PRN = sig
val name : string ref
val FILE : TextIO.outstream ref
val print : string -> unit
val printi : int -> unit
val flush : unit -> unit
val print_list : string * string * string -> ('a -> unit) -> 'a list -> unit
end;

signature DEFAULT = sig
val default : TextIO.outstream
end

functor Outputf (Default : DEFAULT) : PRN = struct
val name = ref "";
val FILE = ref Default.default;
fun print s = TextIO.output (!FILE, s);
fun printi n = (if n<0 then print "-" else (); print (Int.toString (Int.abs n)));
fun flush () = TextIO.flushOut (!FILE);
(* prints a list, p is printer for components *)
fun print_list (lpar:string,sep:string,rpar:string) p l =
	(print lpar;
	 case l of h::t => (p h; List.app (fn e => (print sep; p e)) t) | _ => ();
	 print rpar);
end;

(* for printing on stdout or output file *)
structure OutPrint = Outputf (struct val default = TextIO.stdOut end);

(* for printing on stderr or error file *)
structure ErrPrint = Outputf (struct val default = TextIO.stdErr end);



(* generic utilities and complements ------------------------------- *)

fun fst (x,_) = x;
fun snd (_,y) = y;

fun repeat n f x = if n=0 then () else (f x; repeat (n-1) f x);

(* x assumed to belong to l *)
fun assoc x ((y,z)::t) = if x=y then z else assoc x t;

(* flattens a list of lists *)
fun flat [] = [] | flat (h::t) = h @ flat t;

(* sets as lists; mem, union, inter *)
fun mem x l = let fun rmem [] = false | rmem (h::t) = (h=x) orelse rmem t in rmem l end;
fun union l1 l2 = l1 @ List.filter (fn f => not (mem f l1)) l2;
fun runion l = let fun ru [] u = u | ru (h::t) u = ru t (union h u) in ru l [] end;
fun inter l1 l2 = List.filter (fn f => mem f l2) l1;
fun subset l1 l2 = List.all (fn x => mem x l2) l1;
fun makeset [] = [] | makeset (h::t) = h :: makeset (List.filter (fn x => x<>h) t);

(* insertion sort *)
fun isort l sup =
let fun insert x [] = [x] | insert x (l as (h::t)) = if sup (x,h) then h::insert x t else x::l
    fun sort [] = [] | sort (h::t) = insert h (sort t)
in sort l
end;



(* avl trees ------------------------------------------------------- *)
(* adapted from M. R. Hansen's at http://www2.imm.dtu.dk/courses/02140/plan.html *)

signature KEY = sig
type key
val compare : key * key -> General.order
val < : key * key -> bool
val > : key * key -> bool
end


functor avlTreef (Key : KEY) = struct

datatype 'a tree = Lf 
	      | Br of 'a tree * Key.key * int * 'a * 'a tree

exception AVL

fun newTree() = ref Lf

fun height Lf              = 0
  | height(Br(_, _, h,_ , _)) = h

fun rotLeft(Br(Br(llt, vl, h, ldc, lrt), v, _,dc , rt)) = Br(llt, vl, h , ldc, Br(lrt, v, h-1, dc, rt))
  | rotLeft(arg) = arg

fun rotRight(Br(lt, v, _, dc, Br(rlt, vr, h, rdc, rrt))) = Br(Br(lt, v, h-1, dc, rlt), vr, h, rdc, rrt)
  | rotRight(arg) = arg

fun dRotLeftRight(Br(Br(llt, vl, h, ldc, Br(lrlt, vlr, _, lrdc, lrrt)), v, _, rdc,rt)) =
                                     Br(Br(llt, vl, h-1, ldc,lrlt), vlr, h, lrdc, Br(lrrt, v, h-1, rdc, rt))
  | dRotLeftRight(arg) = arg

     
fun dRotRightLeft(Br(lt, v, _, ldc, Br(Br(rllt, vrl, _, rldc, rlrt), vr, h, rdc, rrt))) =
                                     Br(Br(lt, v, h-1, ldc, rllt), vrl, h, rldc, Br(rlrt, vr, h-1, rdc, rrt))
  | dRotRightLeft(arg) = arg

fun insert(i, dc, Lf)            = Br(Lf, i, 1, dc, Lf) 
  | insert(i, dc, t as Br(lt, v, h, ndc, rt)) = 
       case Key.compare(i, v) of
           LESS    => let val nlt as Br(_, nvl, _ , nldc, _) = insert(i, dc, lt)
                          val nt = Br(nlt, v, 1 + Int.max(height nlt, height rt), ndc, rt)
                      in if height nlt - height rt = 2
                         then if Key.< (i,nvl) then rotLeft nt
                              else dRotLeftRight nt
                         else nt
                      end
         | EQUAL   => t
         | GREATER => let val nrt as Br(_, nvr, _, nvdc, _) = insert(i, dc, rt)
                          val nt = Br(lt, v, 1 + Int.max(height lt, height nrt), ndc, nrt)
                      in if height nrt - height lt = 2
                         then if Key.> (i,nvr) then rotRight nt
                              else dRotRightLeft nt
                         else nt
                      end

fun isAVL Lf                = true
  | isAVL(Br(tl, v, _, dc, tr)) = isAVL tl andalso isAVL tr
                              andalso abs(height tl - height tr) <= 1

fun member(i, Lf)             = (false)
  | member(i, Br(t1,j, _, dc,t2)) = case Key.compare(i,j) of
                                  EQUAL   => (true)
                                | LESS    => member(i,t1)
                                | GREATER => member(i,t2)

fun getData(i,Lf) = NONE
  | getData(i, Br(t1,j, _, dc,t2)) = case Key.compare(i,j) of
                                  EQUAL   => (SOME (dc))
                                | LESS    => getData(i,t1)
                                | GREATER => getData(i,t2)

fun toList Lf             = []
  | toList(Br(t1,j,_, dc, t2)) = toList t1 @ [j] @ toList t2

fun min Lf               = raise AVL
  | min(Br(Lf, i,_, dc,_))  = i
  | min(Br(t, _, _, dc, _))  = min t;

fun max Lf               = raise AVL
  | max(Br(_, i, _,dc, Lf)) = i
  | max(Br(_, _, _,dc, t))  = max t

fun balLeftRight(t as Br(Br(llt, _, _, _, lrt), _, _, _, rt)) =
             if height llt >= height lrt 
             then rotLeft t
             else dRotLeftRight t
  | balLeftRight(arg) = arg

fun balRightLeft(t as Br(lt, v, _, _, Br(rlt, vr, _, _, rrt))) =
             if height rrt >= height rlt 
             then rotRight t
             else dRotRightLeft t
  | balRightLeft(arg) = arg

fun delete(Lf, _)            = Lf
  | delete(Br(t1, i, _, dc, t2), j) = 
      case Key.compare (i,j) of 
           LESS    => let val nt2 = delete(t2,j)
                          val nt = Br(t1, i, 1 + Int.max(height t1, height nt2), dc,nt2)
                      in if height t1 = height nt2 + 2
                         then balLeftRight nt
                         else nt
                      end
         | GREATER => let val nt1 = delete(t1, j)
                          val nt = Br(nt1, i,1 + Int.max(height nt1, height t2) , dc, t2)
                      in if height nt1 + 2 = height t2
                         then balRightLeft nt
                         else nt
                      end
         | EQUAL   => (case (t1, t2) of
                            (Lf , _) => t2
                          | (_,  Lf) => t1
                          |  _       => if height t1 <= height t2 
                                        then let val m = min t2
                                                 val nt2 = delete(t2,m) 
                                             in Br(t1, m, 1 + Int.max(height t1, height nt2), dc, nt2) end
                                        else let val m = max t1
                                                 val nt1 = delete(t1,m)
                                             in Br(nt1, m, 1 + Int.max(height nt1, height t2), dc, t2) end);

(*
fun toString t = foldl (fn (i,s) => s ^ "  " ^ (i)) "" (toList t) 

fun deleteList xs t = foldl (fn (a, tr) => ((print (toString tr)); (print (if isAVL tr then " TRUE\n" else " FALSE\n")); delete(tr, a))) t xs

fun multiInsert((a,b)::t,root) = (multiInsert(t,insert(a,b,root)))
  | multiInsert(nil,root) = root
*)

end; (* avlTreef *)


(* string keyed avl trees *)
structure avlTree = avlTreef (struct open String type key = string end);


(* examples:
val t1 = ref (multiInsert([("rua",NewIntKey(1)),("casa",NewIntKey(2)),("teste",NewIntKey(3)),("funciona",NewIntKey(4)), ("nao",NewIntKey(5))],Lf));

member("rua",!t1);

t1:= deleteList ["rua","npao"] (!t1);

member("rua",!t1);
*)

