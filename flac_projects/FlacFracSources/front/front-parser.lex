structure Tokens = Tokens

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val lineno = ref 0
val charno = ref 0
fun eof () = Tokens.EOF(!lineno,!charno)
fun error (e,l : int,c : int) =
  ErrPrint.print("Error: lexical error at line " ^ (Int.toString (l+1)) ^ "." ^ (Int.toString (c+1)) ^ ": " ^ e ^ "\n")

fun incl n = (lineno := n + !lineno; charno := 0)
fun incc n = (charno := n + !charno; !charno)
fun mkpos z = let val lc = (!lineno, !charno) in charno := z + !charno; lc end
fun mkpos2 z = let val llc = (!lineno, !lineno, !charno) in charno := z + !charno; llc end

val size = String.size

(* comment nesting level *)
val comment = ref 0;

%%
%header (functor FiacreLexFun(structure Tokens: Fiacre_TOKENS));
%s COMMENT;
alpha=[A-Za-z];
digit=[0-9];
ws = [\ \t];
underline = "_";
dot = ".";
comma = ",";
%%
<INITIAL>\n		=> (incl 1; continue());
<INITIAL>\r		=> (incc 1; continue());
<INITIAL>{ws}		=> (incc 1; continue());
<INITIAL>"init"		=> (Tokens.INIT(mkpos2 4));
<INITIAL>"union"	=> (Tokens.UNION(mkpos 5));
<INITIAL>"record"	=> (Tokens.RECORD(mkpos 6));
<INITIAL>"queue"	=> (Tokens.QUEUE(mkpos 5)); 
<INITIAL>"const"	=> (Tokens.CONST(mkpos 5));
<INITIAL>"process"	=> (Tokens.PROCESS(mkpos 7));
<INITIAL>"component"	=> (Tokens.COMPONENT(mkpos 9));
<INITIAL>"var"		=> (Tokens.VAR(mkpos 3));
<INITIAL>"from"		=> (Tokens.FROM(mkpos2 4));
<INITIAL>"any"		=> (Tokens.ANY(mkpos 3));
<INITIAL>"where"	=> (Tokens.WHERE(mkpos 5));
<INITIAL>"if"		=> (Tokens.IF(mkpos2 2));
<INITIAL>"of"		=> (Tokens.OF(mkpos 2));
<INITIAL>"then"		=> (Tokens.THEN(mkpos 4));
<INITIAL>"else"		=> (Tokens.ELSE(mkpos 4));
<INITIAL>"elsif"	=> (Tokens.ELSIF(mkpos2 5));
<INITIAL>"select"	=> (Tokens.SELECT(mkpos 6));
<INITIAL>"case"         => (Tokens.CASE(mkpos2 4));
<INITIAL>"end"		=> (Tokens.END(mkpos 3));
<INITIAL>"to"		=> (Tokens.TO(mkpos2 2));
<INITIAL>"array"	=> (Tokens.ARRAY(mkpos 5));
<INITIAL>"bool"		=> (Tokens.T_BOOL(mkpos 4));
<INITIAL>"nat"		=> (Tokens.T_NAT(mkpos 3));
<INITIAL>"int"		=> (Tokens.T_INT(mkpos 3));
<INITIAL>"true"		=> (Tokens.TRUE(mkpos 4));
<INITIAL>"false"	=> (Tokens.FALSE(mkpos 5));
<INITIAL>"full"		=> (Tokens.FULL(mkpos 4));
<INITIAL>"first"	=> (Tokens.FIRST(mkpos 5));
<INITIAL>"empty"	=> (Tokens.EMPTY(mkpos 5));
<INITIAL>"dequeue"	=> (Tokens.DEQUEUE(mkpos 7));
<INITIAL>"enqueue"	=> (Tokens.ENQUEUE(mkpos 7));
<INITIAL>"append"	=> (Tokens.APPEND(mkpos 6));
<INITIAL>"not"		=> (Tokens.NOT(mkpos 3));
<INITIAL>"in"		=> (Tokens.IN(mkpos 2));
<INITIAL>"out"		=> (Tokens.OUT(mkpos 3)); 
<INITIAL>"none"		=> (Tokens.NONE(mkpos 4));
<INITIAL>"states"	=> (Tokens.STATES(mkpos2 6));
<INITIAL>"priority"	=> (Tokens.PRIORITY(mkpos 8));
<INITIAL>"port"		=> (Tokens.PORT(mkpos 4));
<INITIAL>"read"		=> (Tokens.READ(mkpos 4));
<INITIAL>"write"	=> (Tokens.WRITE(mkpos 5));
<INITIAL>"null"  	=> (Tokens.NULL(mkpos 4));
<INITIAL>"while"	=> (Tokens.WHILE(mkpos2 5));
<INITIAL>"foreach"	=> (Tokens.FOREACH(mkpos2 7));
<INITIAL>"do"		=> (Tokens.DO(mkpos 2));
<INITIAL>"?"		=> (Tokens.RCVE(mkpos 1));
<INITIAL>"and"		=> (Tokens.AND(mkpos 3));
<INITIAL>"or"		=> (Tokens.OR(mkpos 2));
<INITIAL>"channel"      => (Tokens.CHANNEL(mkpos 7));
<INITIAL>"type"		=> (Tokens.TYPE(mkpos 4));
<INITIAL>"is"		=> (Tokens.IS(mkpos 2));
<INITIAL>"par"          => (Tokens.PAR(mkpos2 3));
<INITIAL>"..."          => (Tokens.INFTY(mkpos 3));
<INITIAL>"&"            => (Tokens.AMP(mkpos 1));
<INITIAL>"|"		=> (Tokens.MID(mkpos 1));
<INITIAL>"||"		=> (Tokens.DBAR(mkpos 2));
<INITIAL>"+"		=> (Tokens.ADD(mkpos 1));
<INITIAL>"%"		=> (Tokens.MOD(mkpos 1));
<INITIAL>"-"		=> (Tokens.SUB(mkpos 1));
<INITIAL>"*"		=> (Tokens.MUL(mkpos 1));
<INITIAL>"#"		=> (Tokens.SHARP(mkpos 1));
<INITIAL>"/"		=> (Tokens.DIV(mkpos 1));
<INITIAL>"$"		=> (Tokens.COERCE(mkpos 1));
<INITIAL>"<"		=> (Tokens.LT(mkpos 1));
<INITIAL>"<="		=> (Tokens.LE(mkpos 2));
<INITIAL>">="		=> (Tokens.GE(mkpos 2));
<INITIAL>">"		=> (Tokens.GT(mkpos 1));
<INITIAL>"="		=> (Tokens.EQ(mkpos 1));
<INITIAL>"<>"		=> (Tokens.NE(mkpos 2));
<INITIAL>"!"		=> (Tokens.SEND(mkpos 1));
<INITIAL>".."		=> (Tokens.TWODOT(mkpos 2));
<INITIAL>"."		=> (Tokens.DOT(mkpos 1));
<INITIAL>":=" 		=> (Tokens.ASSIGN(mkpos2 2));
<INITIAL>":" 		=> (Tokens.COLON(mkpos 1));
<INITIAL>","		=> (Tokens.COMMA(mkpos 1));
<INITIAL>";"		=> (Tokens.SEMICOLON(mkpos2 1));
<INITIAL>"[]"		=> (Tokens.ALT(mkpos 2));
<INITIAL>"["		=> (Tokens.LBRACK(mkpos 1));
<INITIAL>"]"		=> (Tokens.RBRACK(mkpos 1));
<INITIAL>"("		=> (Tokens.LPAREN(mkpos 1));
<INITIAL>")"		=> (Tokens.RPAREN(mkpos 1));
<INITIAL>"{"		=> (Tokens.LBRACE(mkpos 1));
<INITIAL>"}"		=> (Tokens.RBRACE(mkpos 1));
<INITIAL>"{|"		=> (Tokens.LQBRACE(mkpos 2));
<INITIAL>"|}"		=> (Tokens.RQBRACE(mkpos 2));
<INITIAL>"->"           => (Tokens.ARROW(mkpos2 2));
<INITIAL>{alpha}({alpha}|{digit}|{underline})*	=> (let val (x,y) = mkpos (size yytext) in Tokens.IDENT((yytext,!lineno),x,y) end);
<INITIAL>{digit}+	=> (let val (x,y) = mkpos (size yytext) in Tokens.NAT(valOf (Int.fromString yytext),x,y) end);
<INITIAL>{digit}+({dot}){digit}+    => (let val (x,y) = mkpos (size yytext) in Tokens.DEC(valOf (Real.fromString yytext),x,y) end);
<INITIAL>eof		=> (incc 1; Tokens.EOF(mkpos (size yytext)));
<INITIAL>"/*"		=> (YYBEGIN COMMENT; incc 2; comment := !comment + 1; continue());
<COMMENT>"/*"		=> (YYBEGIN COMMENT; incc 2; comment := !comment + 1; continue());
<COMMENT>[^*\n]         => (incc 1; continue());
<COMMENT>"\n"           => (incl 1; continue());
<COMMENT>"*/"           => (incc 2; comment := !comment - 1; if !comment>0 then YYBEGIN COMMENT else YYBEGIN INITIAL; continue ());
<COMMENT>"*"            => (incc 1; continue());
<INITIAL>.		=> (let val (x,y) = mkpos 1 in error ("Ignoring bad character "^yytext,x,y); continue() end);
