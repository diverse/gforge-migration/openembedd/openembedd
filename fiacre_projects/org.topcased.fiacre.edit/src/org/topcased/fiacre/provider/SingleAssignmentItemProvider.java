/**
 * <copyright>
 * </copyright>
 *
 * $Id: SingleAssignmentItemProvider.java,v 1.2 2008-12-12 14:48:46 cbrunett Exp $
 */
package org.topcased.fiacre.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.topcased.fiacre.FiacreFactory;
import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.SingleAssignment;

/**
 * This is the item provider adapter for a {@link org.topcased.fiacre.model.SingleAssignment} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SingleAssignmentItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleAssignmentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS);
			childrenFeatures.add(FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SingleAssignment.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/SingleAssignment"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_SingleAssignment_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SingleAssignment.class)) {
		case FiacrePackage.SINGLE_ASSIGNMENT__LHS:
		case FiacrePackage.SINGLE_ASSIGNMENT__RHS:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createNatLiteral()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createBoolLiteral()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createVarRef()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createConstrPattern()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createArrayPattern()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createFieldPattern()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createAnyPattern()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS,
				FiacreFactory.eINSTANCE.createConstantRef()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createUnExp()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createBinExp()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createNatLiteral()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createBoolLiteral()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createVarRef()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createArrayElem()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createRecordElem()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createInlineQueue()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createInlineArray()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createInlineRecord()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createConstrExp()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createConstantRef()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS,
				FiacreFactory.eINSTANCE.createCondExp()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == FiacrePackage.Literals.SINGLE_ASSIGNMENT__LHS
				|| childFeature == FiacrePackage.Literals.SINGLE_ASSIGNMENT__RHS;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return FiacreEditPlugin.INSTANCE;
	}

}
