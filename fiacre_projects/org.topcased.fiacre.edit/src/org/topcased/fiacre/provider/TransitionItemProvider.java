/**
 * <copyright>
 * </copyright>
 *
 * $Id: TransitionItemProvider.java,v 1.2 2008-12-12 14:48:46 cbrunett Exp $
 */
package org.topcased.fiacre.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.topcased.fiacre.FiacreFactory;
import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Transition;

/**
 * This is the item provider adapter for a {@link org.topcased.fiacre.model.Transition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TransitionItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFromPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the From feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFromPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_Transition_from_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_Transition_from_feature", "_UI_Transition_type"),
				FiacrePackage.Literals.TRANSITION__FROM, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_Transition_name_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_Transition_name_feature", "_UI_Transition_type"),
				FiacrePackage.Literals.TRANSITION__NAME, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FiacrePackage.Literals.TRANSITION__ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Transition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/Transition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Transition) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Transition_type")
				: getString("_UI_Transition_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Transition.class)) {
		case FiacrePackage.TRANSITION__NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), false, true));
			return;
		case FiacrePackage.TRANSITION__ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createNullStmt()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createWhileStmt()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createIfStmt()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createSelect()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createTo()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createDeterministicAssignment()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createNonDeterministicAssignment()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createSynchronization()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createReception()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createEmission()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createSeq()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createCaseStmt()));

		newChildDescriptors.add(createChildParameter(
				FiacrePackage.Literals.TRANSITION__ACTION,
				FiacreFactory.eINSTANCE.createForeach()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return FiacreEditPlugin.INSTANCE;
	}

}
