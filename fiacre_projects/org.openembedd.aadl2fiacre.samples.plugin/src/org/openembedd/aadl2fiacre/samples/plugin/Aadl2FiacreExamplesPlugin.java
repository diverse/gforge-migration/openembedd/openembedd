package org.openembedd.aadl2fiacre.samples.plugin;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openembedd.wizards.AbstractDemoPlugin;

/**
 * The main plugin class to be used in the desktop.
 */
public class Aadl2FiacreExamplesPlugin extends AbstractDemoPlugin
{
	public ImageDescriptor getImageDescriptor(String path)
	{
		return AbstractUIPlugin.imageDescriptorFromPlugin("org.topcased.fiacre.samples", path);
	}
}
