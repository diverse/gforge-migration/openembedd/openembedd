/**
 * <copyright>
 * </copyright>
 *
 * $Id: IntervalImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Interval;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interval</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.IntervalImpl#getMini <em>Mini</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.IntervalImpl#getMaxi <em>Maxi</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IntervalImpl extends TypeImpl implements Interval {
	/**
	 * The default value of the '{@link #getMini() <em>Mini</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMini()
	 * @generated
	 * @ordered
	 */
	protected static final int MINI_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMini() <em>Mini</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMini()
	 * @generated
	 * @ordered
	 */
	protected int mini = MINI_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxi() <em>Maxi</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxi()
	 * @generated
	 * @ordered
	 */
	protected static final int MAXI_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxi() <em>Maxi</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxi()
	 * @generated
	 * @ordered
	 */
	protected int maxi = MAXI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntervalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.INTERVAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMini() {
		return mini;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMini(int newMini) {
		int oldMini = mini;
		mini = newMini;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.INTERVAL__MINI, oldMini, mini));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxi() {
		return maxi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxi(int newMaxi) {
		int oldMaxi = maxi;
		maxi = newMaxi;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.INTERVAL__MAXI, oldMaxi, maxi));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.INTERVAL__MINI:
			return new Integer(getMini());
		case FiacrePackage.INTERVAL__MAXI:
			return new Integer(getMaxi());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.INTERVAL__MINI:
			setMini(((Integer) newValue).intValue());
			return;
		case FiacrePackage.INTERVAL__MAXI:
			setMaxi(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.INTERVAL__MINI:
			setMini(MINI_EDEFAULT);
			return;
		case FiacrePackage.INTERVAL__MAXI:
			setMaxi(MAXI_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.INTERVAL__MINI:
			return mini != MINI_EDEFAULT;
		case FiacrePackage.INTERVAL__MAXI:
			return maxi != MAXI_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mini: ");
		result.append(mini);
		result.append(", maxi: ");
		result.append(maxi);
		result.append(')');
		return result.toString();
	}

} //IntervalImpl
