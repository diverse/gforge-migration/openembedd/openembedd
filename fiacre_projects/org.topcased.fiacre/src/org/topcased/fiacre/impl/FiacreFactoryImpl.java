/**
 * <copyright>
 * </copyright>
 *
 * $Id: FiacreFactoryImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.topcased.fiacre.FiacreFactory;
import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FiacreFactoryImpl extends EFactoryImpl implements FiacreFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FiacreFactory init() {
		try {
			FiacreFactory theFiacreFactory = (FiacreFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://org.topcased.fiacre");
			if (theFiacreFactory != null) {
				return theFiacreFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FiacreFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiacreFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case FiacrePackage.PROGRAM:
			return createProgram();
		case FiacrePackage.TYPE_DECL:
			return createTypeDecl();
		case FiacrePackage.CHANNEL_DECL:
			return createChannelDecl();
		case FiacrePackage.COMPONENT_DECL:
			return createComponentDecl();
		case FiacrePackage.PROCESS_DECL:
			return createProcessDecl();
		case FiacrePackage.ARGUMENT_VARIABLE:
			return createArgumentVariable();
		case FiacrePackage.LOCAL_VARIABLE:
			return createLocalVariable();
		case FiacrePackage.SHUFFLE:
			return createShuffle();
		case FiacrePackage.SYNC:
			return createSync();
		case FiacrePackage.PAR:
			return createPar();
		case FiacrePackage.INSTANCE:
			return createInstance();
		case FiacrePackage.INTERFACED_COMP:
			return createInterfacedComp();
		case FiacrePackage.STATE:
			return createState();
		case FiacrePackage.TRANSITION:
			return createTransition();
		case FiacrePackage.NULL_STMT:
			return createNullStmt();
		case FiacrePackage.WHILE_STMT:
			return createWhileStmt();
		case FiacrePackage.IF_STMT:
			return createIfStmt();
		case FiacrePackage.SELECT:
			return createSelect();
		case FiacrePackage.TO:
			return createTo();
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT:
			return createDeterministicAssignment();
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT:
			return createNonDeterministicAssignment();
		case FiacrePackage.SINGLE_ASSIGNMENT:
			return createSingleAssignment();
		case FiacrePackage.UN_EXP:
			return createUnExp();
		case FiacrePackage.BIN_EXP:
			return createBinExp();
		case FiacrePackage.NAT_LITERAL:
			return createNatLiteral();
		case FiacrePackage.BOOL_LITERAL:
			return createBoolLiteral();
		case FiacrePackage.VAR_REF:
			return createVarRef();
		case FiacrePackage.ARRAY_ELEM:
			return createArrayElem();
		case FiacrePackage.RECORD_ELEM:
			return createRecordElem();
		case FiacrePackage.INLINE_QUEUE:
			return createInlineQueue();
		case FiacrePackage.BOOL_TYPE:
			return createBoolType();
		case FiacrePackage.NAT_TYPE:
			return createNatType();
		case FiacrePackage.INT_TYPE:
			return createIntType();
		case FiacrePackage.INTERVAL:
			return createInterval();
		case FiacrePackage.RECORD:
			return createRecord();
		case FiacrePackage.FIELD:
			return createField();
		case FiacrePackage.ARRAY:
			return createArray();
		case FiacrePackage.QUEUE:
			return createQueue();
		case FiacrePackage.TYPE_ID:
			return createTypeId();
		case FiacrePackage.CHANNEL_ID:
			return createChannelId();
		case FiacrePackage.PROFILE:
			return createProfile();
		case FiacrePackage.OR_CHANNEL:
			return createOrChannel();
		case FiacrePackage.INLINE_ARRAY:
			return createInlineArray();
		case FiacrePackage.INLINE_RECORD:
			return createInlineRecord();
		case FiacrePackage.VALUED_FIELD:
			return createValuedField();
		case FiacrePackage.SYNCHRONIZATION:
			return createSynchronization();
		case FiacrePackage.RECEPTION:
			return createReception();
		case FiacrePackage.EMISSION:
			return createEmission();
		case FiacrePackage.SEQ:
			return createSeq();
		case FiacrePackage.LOCAL_PORT_DECL:
			return createLocalPortDecl();
		case FiacrePackage.PARAM_PORT_DECL:
			return createParamPortDecl();
		case FiacrePackage.PRIORITY:
			return createPriority();
		case FiacrePackage.CONSTR_EXP:
			return createConstrExp();
		case FiacrePackage.UNION:
			return createUnion();
		case FiacrePackage.CONSTR_PATTERN:
			return createConstrPattern();
		case FiacrePackage.ARRAY_PATTERN:
			return createArrayPattern();
		case FiacrePackage.FIELD_PATTERN:
			return createFieldPattern();
		case FiacrePackage.ANY_PATTERN:
			return createAnyPattern();
		case FiacrePackage.CASE_STMT:
			return createCaseStmt();
		case FiacrePackage.RULE:
			return createRule();
		case FiacrePackage.REF_ARG:
			return createRefArg();
		case FiacrePackage.CONSTANT_DECL:
			return createConstantDecl();
		case FiacrePackage.CONSTANT_REF:
			return createConstantRef();
		case FiacrePackage.CONSTR:
			return createConstr();
		case FiacrePackage.FINITE_BOUND:
			return createFiniteBound();
		case FiacrePackage.INFINITE_BOUND:
			return createInfiniteBound();
		case FiacrePackage.COND_EXP:
			return createCondExp();
		case FiacrePackage.FOREACH:
			return createForeach();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case FiacrePackage.UNOP:
			return createUnopFromString(eDataType, initialValue);
		case FiacrePackage.BIN_OP:
			return createBinOpFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case FiacrePackage.UNOP:
			return convertUnopToString(eDataType, instanceValue);
		case FiacrePackage.BIN_OP:
			return convertBinOpToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Program createProgram() {
		ProgramImpl program = new ProgramImpl();
		return program;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeDecl createTypeDecl() {
		TypeDeclImpl typeDecl = new TypeDeclImpl();
		return typeDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChannelDecl createChannelDecl() {
		ChannelDeclImpl channelDecl = new ChannelDeclImpl();
		return channelDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDecl createComponentDecl() {
		ComponentDeclImpl componentDecl = new ComponentDeclImpl();
		return componentDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessDecl createProcessDecl() {
		ProcessDeclImpl processDecl = new ProcessDeclImpl();
		return processDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgumentVariable createArgumentVariable() {
		ArgumentVariableImpl argumentVariable = new ArgumentVariableImpl();
		return argumentVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalVariable createLocalVariable() {
		LocalVariableImpl localVariable = new LocalVariableImpl();
		return localVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Shuffle createShuffle() {
		ShuffleImpl shuffle = new ShuffleImpl();
		return shuffle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sync createSync() {
		SyncImpl sync = new SyncImpl();
		return sync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Par createPar() {
		ParImpl par = new ParImpl();
		return par;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance createInstance() {
		InstanceImpl instance = new InstanceImpl();
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfacedComp createInterfacedComp() {
		InterfacedCompImpl interfacedComp = new InterfacedCompImpl();
		return interfacedComp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullStmt createNullStmt() {
		NullStmtImpl nullStmt = new NullStmtImpl();
		return nullStmt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileStmt createWhileStmt() {
		WhileStmtImpl whileStmt = new WhileStmtImpl();
		return whileStmt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfStmt createIfStmt() {
		IfStmtImpl ifStmt = new IfStmtImpl();
		return ifStmt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Select createSelect() {
		SelectImpl select = new SelectImpl();
		return select;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public To createTo() {
		ToImpl to = new ToImpl();
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeterministicAssignment createDeterministicAssignment() {
		DeterministicAssignmentImpl deterministicAssignment = new DeterministicAssignmentImpl();
		return deterministicAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonDeterministicAssignment createNonDeterministicAssignment() {
		NonDeterministicAssignmentImpl nonDeterministicAssignment = new NonDeterministicAssignmentImpl();
		return nonDeterministicAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleAssignment createSingleAssignment() {
		SingleAssignmentImpl singleAssignment = new SingleAssignmentImpl();
		return singleAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnExp createUnExp() {
		UnExpImpl unExp = new UnExpImpl();
		return unExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinExp createBinExp() {
		BinExpImpl binExp = new BinExpImpl();
		return binExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NatLiteral createNatLiteral() {
		NatLiteralImpl natLiteral = new NatLiteralImpl();
		return natLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolLiteral createBoolLiteral() {
		BoolLiteralImpl boolLiteral = new BoolLiteralImpl();
		return boolLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VarRef createVarRef() {
		VarRefImpl varRef = new VarRefImpl();
		return varRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayElem createArrayElem() {
		ArrayElemImpl arrayElem = new ArrayElemImpl();
		return arrayElem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordElem createRecordElem() {
		RecordElemImpl recordElem = new RecordElemImpl();
		return recordElem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineQueue createInlineQueue() {
		InlineQueueImpl inlineQueue = new InlineQueueImpl();
		return inlineQueue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolType createBoolType() {
		BoolTypeImpl boolType = new BoolTypeImpl();
		return boolType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NatType createNatType() {
		NatTypeImpl natType = new NatTypeImpl();
		return natType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntType createIntType() {
		IntTypeImpl intType = new IntTypeImpl();
		return intType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interval createInterval() {
		IntervalImpl interval = new IntervalImpl();
		return interval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Record createRecord() {
		RecordImpl record = new RecordImpl();
		return record;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field createField() {
		FieldImpl field = new FieldImpl();
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Array createArray() {
		ArrayImpl array = new ArrayImpl();
		return array;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Queue createQueue() {
		QueueImpl queue = new QueueImpl();
		return queue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeId createTypeId() {
		TypeIdImpl typeId = new TypeIdImpl();
		return typeId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChannelId createChannelId() {
		ChannelIdImpl channelId = new ChannelIdImpl();
		return channelId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Profile createProfile() {
		ProfileImpl profile = new ProfileImpl();
		return profile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrChannel createOrChannel() {
		OrChannelImpl orChannel = new OrChannelImpl();
		return orChannel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineArray createInlineArray() {
		InlineArrayImpl inlineArray = new InlineArrayImpl();
		return inlineArray;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineRecord createInlineRecord() {
		InlineRecordImpl inlineRecord = new InlineRecordImpl();
		return inlineRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValuedField createValuedField() {
		ValuedFieldImpl valuedField = new ValuedFieldImpl();
		return valuedField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synchronization createSynchronization() {
		SynchronizationImpl synchronization = new SynchronizationImpl();
		return synchronization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reception createReception() {
		ReceptionImpl reception = new ReceptionImpl();
		return reception;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Emission createEmission() {
		EmissionImpl emission = new EmissionImpl();
		return emission;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Seq createSeq() {
		SeqImpl seq = new SeqImpl();
		return seq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalPortDecl createLocalPortDecl() {
		LocalPortDeclImpl localPortDecl = new LocalPortDeclImpl();
		return localPortDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParamPortDecl createParamPortDecl() {
		ParamPortDeclImpl paramPortDecl = new ParamPortDeclImpl();
		return paramPortDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority createPriority() {
		PriorityImpl priority = new PriorityImpl();
		return priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrExp createConstrExp() {
		ConstrExpImpl constrExp = new ConstrExpImpl();
		return constrExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Union createUnion() {
		UnionImpl union = new UnionImpl();
		return union;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrPattern createConstrPattern() {
		ConstrPatternImpl constrPattern = new ConstrPatternImpl();
		return constrPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayPattern createArrayPattern() {
		ArrayPatternImpl arrayPattern = new ArrayPatternImpl();
		return arrayPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldPattern createFieldPattern() {
		FieldPatternImpl fieldPattern = new FieldPatternImpl();
		return fieldPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyPattern createAnyPattern() {
		AnyPatternImpl anyPattern = new AnyPatternImpl();
		return anyPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseStmt createCaseStmt() {
		CaseStmtImpl caseStmt = new CaseStmtImpl();
		return caseStmt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule createRule() {
		RuleImpl rule = new RuleImpl();
		return rule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArg createRefArg() {
		RefArgImpl refArg = new RefArgImpl();
		return refArg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantDecl createConstantDecl() {
		ConstantDeclImpl constantDecl = new ConstantDeclImpl();
		return constantDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantRef createConstantRef() {
		ConstantRefImpl constantRef = new ConstantRefImpl();
		return constantRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constr createConstr() {
		ConstrImpl constr = new ConstrImpl();
		return constr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiniteBound createFiniteBound() {
		FiniteBoundImpl finiteBound = new FiniteBoundImpl();
		return finiteBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfiniteBound createInfiniteBound() {
		InfiniteBoundImpl infiniteBound = new InfiniteBoundImpl();
		return infiniteBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CondExp createCondExp() {
		CondExpImpl condExp = new CondExpImpl();
		return condExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Foreach createForeach() {
		ForeachImpl foreach = new ForeachImpl();
		return foreach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unop createUnopFromString(EDataType eDataType, String initialValue) {
		Unop result = Unop.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnopToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinOp createBinOpFromString(EDataType eDataType, String initialValue) {
		BinOp result = BinOp.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBinOpToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiacrePackage getFiacrePackage() {
		return (FiacrePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FiacrePackage getPackage() {
		return FiacrePackage.eINSTANCE;
	}

} //FiacreFactoryImpl
