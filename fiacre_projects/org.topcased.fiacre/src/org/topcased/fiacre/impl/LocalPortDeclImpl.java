/**
 * <copyright>
 * </copyright>
 *
 * $Id: LocalPortDeclImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.LocalPortDecl;
import org.topcased.fiacre.model.MaxBound;
import org.topcased.fiacre.model.MinBound;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Port Decl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.LocalPortDeclImpl#getMini <em>Mini</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.LocalPortDeclImpl#getMaxi <em>Maxi</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LocalPortDeclImpl extends PortDeclImpl implements LocalPortDecl {
	/**
	 * The cached value of the '{@link #getMini() <em>Mini</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMini()
	 * @generated
	 * @ordered
	 */
	protected MinBound mini;

	/**
	 * The cached value of the '{@link #getMaxi() <em>Maxi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxi()
	 * @generated
	 * @ordered
	 */
	protected MaxBound maxi;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalPortDeclImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.LOCAL_PORT_DECL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinBound getMini() {
		return mini;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMini(MinBound newMini,
			NotificationChain msgs) {
		MinBound oldMini = mini;
		mini = newMini;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.LOCAL_PORT_DECL__MINI,
					oldMini, newMini);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMini(MinBound newMini) {
		if (newMini != mini) {
			NotificationChain msgs = null;
			if (mini != null)
				msgs = ((InternalEObject) mini).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.LOCAL_PORT_DECL__MINI, null,
						msgs);
			if (newMini != null)
				msgs = ((InternalEObject) newMini).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.LOCAL_PORT_DECL__MINI, null,
						msgs);
			msgs = basicSetMini(newMini, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.LOCAL_PORT_DECL__MINI, newMini, newMini));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxBound getMaxi() {
		return maxi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxi(MaxBound newMaxi,
			NotificationChain msgs) {
		MaxBound oldMaxi = maxi;
		maxi = newMaxi;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.LOCAL_PORT_DECL__MAXI,
					oldMaxi, newMaxi);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxi(MaxBound newMaxi) {
		if (newMaxi != maxi) {
			NotificationChain msgs = null;
			if (maxi != null)
				msgs = ((InternalEObject) maxi).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.LOCAL_PORT_DECL__MAXI, null,
						msgs);
			if (newMaxi != null)
				msgs = ((InternalEObject) newMaxi).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.LOCAL_PORT_DECL__MAXI, null,
						msgs);
			msgs = basicSetMaxi(newMaxi, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.LOCAL_PORT_DECL__MAXI, newMaxi, newMaxi));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.LOCAL_PORT_DECL__MINI:
			return basicSetMini(null, msgs);
		case FiacrePackage.LOCAL_PORT_DECL__MAXI:
			return basicSetMaxi(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.LOCAL_PORT_DECL__MINI:
			return getMini();
		case FiacrePackage.LOCAL_PORT_DECL__MAXI:
			return getMaxi();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.LOCAL_PORT_DECL__MINI:
			setMini((MinBound) newValue);
			return;
		case FiacrePackage.LOCAL_PORT_DECL__MAXI:
			setMaxi((MaxBound) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.LOCAL_PORT_DECL__MINI:
			setMini((MinBound) null);
			return;
		case FiacrePackage.LOCAL_PORT_DECL__MAXI:
			setMaxi((MaxBound) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.LOCAL_PORT_DECL__MINI:
			return mini != null;
		case FiacrePackage.LOCAL_PORT_DECL__MAXI:
			return maxi != null;
		}
		return super.eIsSet(featureID);
	}

} //LocalPortDeclImpl
