/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeDeclImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.ArgumentVariable;
import org.topcased.fiacre.model.LocalPortDecl;
import org.topcased.fiacre.model.LocalVariable;
import org.topcased.fiacre.model.NodeDecl;
import org.topcased.fiacre.model.ParamPortDecl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Decl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.NodeDeclImpl#getArgs <em>Args</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.NodeDeclImpl#getVars <em>Vars</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.NodeDeclImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.NodeDeclImpl#getLocalPorts <em>Local Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class NodeDeclImpl extends DeclarationImpl implements NodeDecl {
	/**
	 * The cached value of the '{@link #getArgs() <em>Args</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgs()
	 * @generated
	 * @ordered
	 */
	protected EList<ArgumentVariable> args;

	/**
	 * The cached value of the '{@link #getVars() <em>Vars</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVars()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalVariable> vars;

	/**
	 * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<ParamPortDecl> ports;

	/**
	 * The cached value of the '{@link #getLocalPorts() <em>Local Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalPortDecl> localPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeDeclImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.NODE_DECL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArgumentVariable> getArgs() {
		if (args == null) {
			args = new EObjectContainmentEList<ArgumentVariable>(
					ArgumentVariable.class, this, FiacrePackage.NODE_DECL__ARGS);
		}
		return args;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LocalVariable> getVars() {
		if (vars == null) {
			vars = new EObjectContainmentEList<LocalVariable>(
					LocalVariable.class, this, FiacrePackage.NODE_DECL__VARS);
		}
		return vars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParamPortDecl> getPorts() {
		if (ports == null) {
			ports = new EObjectContainmentEList<ParamPortDecl>(
					ParamPortDecl.class, this, FiacrePackage.NODE_DECL__PORTS);
		}
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LocalPortDecl> getLocalPorts() {
		if (localPorts == null) {
			localPorts = new EObjectContainmentEList<LocalPortDecl>(
					LocalPortDecl.class, this,
					FiacrePackage.NODE_DECL__LOCAL_PORTS);
		}
		return localPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.NODE_DECL__ARGS:
			return ((InternalEList<?>) getArgs()).basicRemove(otherEnd, msgs);
		case FiacrePackage.NODE_DECL__VARS:
			return ((InternalEList<?>) getVars()).basicRemove(otherEnd, msgs);
		case FiacrePackage.NODE_DECL__PORTS:
			return ((InternalEList<?>) getPorts()).basicRemove(otherEnd, msgs);
		case FiacrePackage.NODE_DECL__LOCAL_PORTS:
			return ((InternalEList<?>) getLocalPorts()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.NODE_DECL__ARGS:
			return getArgs();
		case FiacrePackage.NODE_DECL__VARS:
			return getVars();
		case FiacrePackage.NODE_DECL__PORTS:
			return getPorts();
		case FiacrePackage.NODE_DECL__LOCAL_PORTS:
			return getLocalPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.NODE_DECL__ARGS:
			getArgs().clear();
			getArgs().addAll((Collection<? extends ArgumentVariable>) newValue);
			return;
		case FiacrePackage.NODE_DECL__VARS:
			getVars().clear();
			getVars().addAll((Collection<? extends LocalVariable>) newValue);
			return;
		case FiacrePackage.NODE_DECL__PORTS:
			getPorts().clear();
			getPorts().addAll((Collection<? extends ParamPortDecl>) newValue);
			return;
		case FiacrePackage.NODE_DECL__LOCAL_PORTS:
			getLocalPorts().clear();
			getLocalPorts().addAll(
					(Collection<? extends LocalPortDecl>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.NODE_DECL__ARGS:
			getArgs().clear();
			return;
		case FiacrePackage.NODE_DECL__VARS:
			getVars().clear();
			return;
		case FiacrePackage.NODE_DECL__PORTS:
			getPorts().clear();
			return;
		case FiacrePackage.NODE_DECL__LOCAL_PORTS:
			getLocalPorts().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.NODE_DECL__ARGS:
			return args != null && !args.isEmpty();
		case FiacrePackage.NODE_DECL__VARS:
			return vars != null && !vars.isEmpty();
		case FiacrePackage.NODE_DECL__PORTS:
			return ports != null && !ports.isEmpty();
		case FiacrePackage.NODE_DECL__LOCAL_PORTS:
			return localPorts != null && !localPorts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NodeDeclImpl
