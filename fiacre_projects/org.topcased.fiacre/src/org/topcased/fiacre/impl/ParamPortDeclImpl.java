/**
 * <copyright>
 * </copyright>
 *
 * $Id: ParamPortDeclImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.ParamPortDecl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Param Port Decl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ParamPortDeclImpl extends PortDeclImpl implements ParamPortDecl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParamPortDeclImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.PARAM_PORT_DECL;
	}

} //ParamPortDeclImpl
