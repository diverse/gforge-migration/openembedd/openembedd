/**
 * <copyright>
 * </copyright>
 *
 * $Id: BinExpImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.BinExp;
import org.topcased.fiacre.model.BinOp;
import org.topcased.fiacre.model.Exp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bin Exp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.BinExpImpl#getR_exp <em>Rexp</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.BinExpImpl#getL_exp <em>Lexp</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.BinExpImpl#getBinOp <em>Bin Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BinExpImpl extends ExpImpl implements BinExp {
	/**
	 * The cached value of the '{@link #getR_exp() <em>Rexp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getR_exp()
	 * @generated
	 * @ordered
	 */
	protected Exp r_exp;

	/**
	 * The cached value of the '{@link #getL_exp() <em>Lexp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getL_exp()
	 * @generated
	 * @ordered
	 */
	protected Exp l_exp;

	/**
	 * The default value of the '{@link #getBinOp() <em>Bin Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinOp()
	 * @generated
	 * @ordered
	 */
	protected static final BinOp BIN_OP_EDEFAULT = BinOp.BOR_LITERAL;

	/**
	 * The cached value of the '{@link #getBinOp() <em>Bin Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinOp()
	 * @generated
	 * @ordered
	 */
	protected BinOp binOp = BIN_OP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.BIN_EXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getR_exp() {
		return r_exp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetR_exp(Exp newR_exp, NotificationChain msgs) {
		Exp oldR_exp = r_exp;
		r_exp = newR_exp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.BIN_EXP__REXP, oldR_exp,
					newR_exp);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setR_exp(Exp newR_exp) {
		if (newR_exp != r_exp) {
			NotificationChain msgs = null;
			if (r_exp != null)
				msgs = ((InternalEObject) r_exp).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.BIN_EXP__REXP,
						null, msgs);
			if (newR_exp != null)
				msgs = ((InternalEObject) newR_exp).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.BIN_EXP__REXP,
						null, msgs);
			msgs = basicSetR_exp(newR_exp, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.BIN_EXP__REXP, newR_exp, newR_exp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getL_exp() {
		return l_exp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetL_exp(Exp newL_exp, NotificationChain msgs) {
		Exp oldL_exp = l_exp;
		l_exp = newL_exp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.BIN_EXP__LEXP, oldL_exp,
					newL_exp);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setL_exp(Exp newL_exp) {
		if (newL_exp != l_exp) {
			NotificationChain msgs = null;
			if (l_exp != null)
				msgs = ((InternalEObject) l_exp).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.BIN_EXP__LEXP,
						null, msgs);
			if (newL_exp != null)
				msgs = ((InternalEObject) newL_exp).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.BIN_EXP__LEXP,
						null, msgs);
			msgs = basicSetL_exp(newL_exp, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.BIN_EXP__LEXP, newL_exp, newL_exp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinOp getBinOp() {
		return binOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinOp(BinOp newBinOp) {
		BinOp oldBinOp = binOp;
		binOp = newBinOp == null ? BIN_OP_EDEFAULT : newBinOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.BIN_EXP__BIN_OP, oldBinOp, binOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.BIN_EXP__REXP:
			return basicSetR_exp(null, msgs);
		case FiacrePackage.BIN_EXP__LEXP:
			return basicSetL_exp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.BIN_EXP__REXP:
			return getR_exp();
		case FiacrePackage.BIN_EXP__LEXP:
			return getL_exp();
		case FiacrePackage.BIN_EXP__BIN_OP:
			return getBinOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.BIN_EXP__REXP:
			setR_exp((Exp) newValue);
			return;
		case FiacrePackage.BIN_EXP__LEXP:
			setL_exp((Exp) newValue);
			return;
		case FiacrePackage.BIN_EXP__BIN_OP:
			setBinOp((BinOp) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.BIN_EXP__REXP:
			setR_exp((Exp) null);
			return;
		case FiacrePackage.BIN_EXP__LEXP:
			setL_exp((Exp) null);
			return;
		case FiacrePackage.BIN_EXP__BIN_OP:
			setBinOp(BIN_OP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.BIN_EXP__REXP:
			return r_exp != null;
		case FiacrePackage.BIN_EXP__LEXP:
			return l_exp != null;
		case FiacrePackage.BIN_EXP__BIN_OP:
			return binOp != BIN_OP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (binOp: ");
		result.append(binOp);
		result.append(')');
		return result.toString();
	}

} //BinExpImpl
