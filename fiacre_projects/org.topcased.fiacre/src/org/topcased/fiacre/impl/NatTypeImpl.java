/**
 * <copyright>
 * </copyright>
 *
 * $Id: NatTypeImpl.java,v 1.2 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.NatType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nat Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NatTypeImpl extends BasicTypeImpl implements NatType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NatTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.NAT_TYPE;
	}

} //NatTypeImpl
