/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArgumentVariableImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.ArgumentVariable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Argument Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.ArgumentVariableImpl#isRead <em>Read</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.ArgumentVariableImpl#isWrite <em>Write</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.ArgumentVariableImpl#isRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArgumentVariableImpl extends VariableImpl implements
		ArgumentVariable {
	/**
	 * The default value of the '{@link #isRead() <em>Read</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRead()
	 * @generated
	 * @ordered
	 */
	protected static final boolean READ_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRead() <em>Read</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRead()
	 * @generated
	 * @ordered
	 */
	protected boolean read = READ_EDEFAULT;

	/**
	 * The default value of the '{@link #isWrite() <em>Write</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWrite()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WRITE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isWrite() <em>Write</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWrite()
	 * @generated
	 * @ordered
	 */
	protected boolean write = WRITE_EDEFAULT;

	/**
	 * The default value of the '{@link #isRef() <em>Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRef()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REF_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRef() <em>Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRef()
	 * @generated
	 * @ordered
	 */
	protected boolean ref = REF_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.ARGUMENT_VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRead(boolean newRead) {
		boolean oldRead = read;
		read = newRead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.ARGUMENT_VARIABLE__READ, oldRead, read));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isWrite() {
		return write;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWrite(boolean newWrite) {
		boolean oldWrite = write;
		write = newWrite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.ARGUMENT_VARIABLE__WRITE, oldWrite, write));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRef() {
		return ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRef(boolean newRef) {
		boolean oldRef = ref;
		ref = newRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.ARGUMENT_VARIABLE__REF, oldRef, ref));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.ARGUMENT_VARIABLE__READ:
			return isRead() ? Boolean.TRUE : Boolean.FALSE;
		case FiacrePackage.ARGUMENT_VARIABLE__WRITE:
			return isWrite() ? Boolean.TRUE : Boolean.FALSE;
		case FiacrePackage.ARGUMENT_VARIABLE__REF:
			return isRef() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.ARGUMENT_VARIABLE__READ:
			setRead(((Boolean) newValue).booleanValue());
			return;
		case FiacrePackage.ARGUMENT_VARIABLE__WRITE:
			setWrite(((Boolean) newValue).booleanValue());
			return;
		case FiacrePackage.ARGUMENT_VARIABLE__REF:
			setRef(((Boolean) newValue).booleanValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.ARGUMENT_VARIABLE__READ:
			setRead(READ_EDEFAULT);
			return;
		case FiacrePackage.ARGUMENT_VARIABLE__WRITE:
			setWrite(WRITE_EDEFAULT);
			return;
		case FiacrePackage.ARGUMENT_VARIABLE__REF:
			setRef(REF_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.ARGUMENT_VARIABLE__READ:
			return read != READ_EDEFAULT;
		case FiacrePackage.ARGUMENT_VARIABLE__WRITE:
			return write != WRITE_EDEFAULT;
		case FiacrePackage.ARGUMENT_VARIABLE__REF:
			return ref != REF_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (read: ");
		result.append(read);
		result.append(", write: ");
		result.append(write);
		result.append(", ref: ");
		result.append(ref);
		result.append(')');
		return result.toString();
	}

} //ArgumentVariableImpl
