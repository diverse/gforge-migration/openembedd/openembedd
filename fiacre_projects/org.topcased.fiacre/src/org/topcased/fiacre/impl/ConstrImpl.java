/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstrImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Constr;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ConstrImpl extends LabeledTypeImpl implements Constr {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstrImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.CONSTR;
	}

} //ConstrImpl
