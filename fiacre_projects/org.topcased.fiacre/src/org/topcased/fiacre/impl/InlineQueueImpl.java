/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineQueueImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.InlineQueue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inline Queue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class InlineQueueImpl extends InlineCollectionImpl implements
		InlineQueue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InlineQueueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.INLINE_QUEUE;
	}

} //InlineQueueImpl
