/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProcessDeclImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.ProcessDecl;
import org.topcased.fiacre.model.State;
import org.topcased.fiacre.model.Statement;
import org.topcased.fiacre.model.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Decl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.ProcessDeclImpl#getStates <em>States</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.ProcessDeclImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.ProcessDeclImpl#getInitAction <em>Init Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProcessDeclImpl extends NodeDeclImpl implements ProcessDecl {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getInitAction() <em>Init Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitAction()
	 * @generated
	 * @ordered
	 */
	protected Statement initAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessDeclImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.PROCESS_DECL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this,
					FiacrePackage.PROCESS_DECL__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(
					Transition.class, this,
					FiacrePackage.PROCESS_DECL__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement getInitAction() {
		return initAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitAction(Statement newInitAction,
			NotificationChain msgs) {
		Statement oldInitAction = initAction;
		initAction = newInitAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.PROCESS_DECL__INIT_ACTION,
					oldInitAction, newInitAction);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitAction(Statement newInitAction) {
		if (newInitAction != initAction) {
			NotificationChain msgs = null;
			if (initAction != null)
				msgs = ((InternalEObject) initAction).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.PROCESS_DECL__INIT_ACTION,
						null, msgs);
			if (newInitAction != null)
				msgs = ((InternalEObject) newInitAction).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.PROCESS_DECL__INIT_ACTION,
						null, msgs);
			msgs = basicSetInitAction(newInitAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.PROCESS_DECL__INIT_ACTION, newInitAction,
					newInitAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.PROCESS_DECL__STATES:
			return ((InternalEList<?>) getStates()).basicRemove(otherEnd, msgs);
		case FiacrePackage.PROCESS_DECL__TRANSITIONS:
			return ((InternalEList<?>) getTransitions()).basicRemove(otherEnd,
					msgs);
		case FiacrePackage.PROCESS_DECL__INIT_ACTION:
			return basicSetInitAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.PROCESS_DECL__STATES:
			return getStates();
		case FiacrePackage.PROCESS_DECL__TRANSITIONS:
			return getTransitions();
		case FiacrePackage.PROCESS_DECL__INIT_ACTION:
			return getInitAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.PROCESS_DECL__STATES:
			getStates().clear();
			getStates().addAll((Collection<? extends State>) newValue);
			return;
		case FiacrePackage.PROCESS_DECL__TRANSITIONS:
			getTransitions().clear();
			getTransitions()
					.addAll((Collection<? extends Transition>) newValue);
			return;
		case FiacrePackage.PROCESS_DECL__INIT_ACTION:
			setInitAction((Statement) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.PROCESS_DECL__STATES:
			getStates().clear();
			return;
		case FiacrePackage.PROCESS_DECL__TRANSITIONS:
			getTransitions().clear();
			return;
		case FiacrePackage.PROCESS_DECL__INIT_ACTION:
			setInitAction((Statement) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.PROCESS_DECL__STATES:
			return states != null && !states.isEmpty();
		case FiacrePackage.PROCESS_DECL__TRANSITIONS:
			return transitions != null && !transitions.isEmpty();
		case FiacrePackage.PROCESS_DECL__INIT_ACTION:
			return initAction != null;
		}
		return super.eIsSet(featureID);
	}

} //ProcessDeclImpl
