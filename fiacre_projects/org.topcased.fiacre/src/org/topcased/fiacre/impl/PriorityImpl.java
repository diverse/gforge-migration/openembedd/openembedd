/**
 * <copyright>
 * </copyright>
 *
 * $Id: PriorityImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.PortDecl;
import org.topcased.fiacre.model.Priority;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Priority</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.PriorityImpl#getInf <em>Inf</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.PriorityImpl#getSup <em>Sup</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PriorityImpl extends EObjectImpl implements Priority {
	/**
	 * The cached value of the '{@link #getInf() <em>Inf</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInf()
	 * @generated
	 * @ordered
	 */
	protected EList<PortDecl> inf;

	/**
	 * The cached value of the '{@link #getSup() <em>Sup</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSup()
	 * @generated
	 * @ordered
	 */
	protected EList<PortDecl> sup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.PRIORITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortDecl> getInf() {
		if (inf == null) {
			inf = new EObjectResolvingEList<PortDecl>(PortDecl.class, this,
					FiacrePackage.PRIORITY__INF);
		}
		return inf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortDecl> getSup() {
		if (sup == null) {
			sup = new EObjectResolvingEList<PortDecl>(PortDecl.class, this,
					FiacrePackage.PRIORITY__SUP);
		}
		return sup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.PRIORITY__INF:
			return getInf();
		case FiacrePackage.PRIORITY__SUP:
			return getSup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.PRIORITY__INF:
			getInf().clear();
			getInf().addAll((Collection<? extends PortDecl>) newValue);
			return;
		case FiacrePackage.PRIORITY__SUP:
			getSup().clear();
			getSup().addAll((Collection<? extends PortDecl>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.PRIORITY__INF:
			getInf().clear();
			return;
		case FiacrePackage.PRIORITY__SUP:
			getSup().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.PRIORITY__INF:
			return inf != null && !inf.isEmpty();
		case FiacrePackage.PRIORITY__SUP:
			return sup != null && !sup.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PriorityImpl
