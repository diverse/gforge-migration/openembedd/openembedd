/**
 * <copyright>
 * </copyright>
 *
 * $Id: IntTypeImpl.java,v 1.2 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.IntType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class IntTypeImpl extends BasicTypeImpl implements IntType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.INT_TYPE;
	}

} //IntTypeImpl
