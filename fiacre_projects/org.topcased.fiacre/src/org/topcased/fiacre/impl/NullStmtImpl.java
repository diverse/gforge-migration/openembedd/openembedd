/**
 * <copyright>
 * </copyright>
 *
 * $Id: NullStmtImpl.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.ecore.EClass;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.NullStmt;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Null Stmt</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NullStmtImpl extends StatementImpl implements NullStmt {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NullStmtImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.NULL_STMT;
	}

} //NullStmtImpl
