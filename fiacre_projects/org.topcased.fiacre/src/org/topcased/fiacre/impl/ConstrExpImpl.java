/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstrExpImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Constr;
import org.topcased.fiacre.model.ConstrExp;
import org.topcased.fiacre.model.Exp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constr Exp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.ConstrExpImpl#getArg <em>Arg</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.ConstrExpImpl#getConstr <em>Constr</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConstrExpImpl extends ExpImpl implements ConstrExp {
	/**
	 * The cached value of the '{@link #getArg() <em>Arg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArg()
	 * @generated
	 * @ordered
	 */
	protected Exp arg;

	/**
	 * The cached value of the '{@link #getConstr() <em>Constr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstr()
	 * @generated
	 * @ordered
	 */
	protected Constr constr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstrExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.CONSTR_EXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getArg() {
		return arg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArg(Exp newArg, NotificationChain msgs) {
		Exp oldArg = arg;
		arg = newArg;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.CONSTR_EXP__ARG, oldArg,
					newArg);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArg(Exp newArg) {
		if (newArg != arg) {
			NotificationChain msgs = null;
			if (arg != null)
				msgs = ((InternalEObject) arg).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.CONSTR_EXP__ARG,
						null, msgs);
			if (newArg != null)
				msgs = ((InternalEObject) newArg).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.CONSTR_EXP__ARG,
						null, msgs);
			msgs = basicSetArg(newArg, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.CONSTR_EXP__ARG, newArg, newArg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constr getConstr() {
		if (constr != null && constr.eIsProxy()) {
			InternalEObject oldConstr = (InternalEObject) constr;
			constr = (Constr) eResolveProxy(oldConstr);
			if (constr != oldConstr) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FiacrePackage.CONSTR_EXP__CONSTR, oldConstr, constr));
			}
		}
		return constr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constr basicGetConstr() {
		return constr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstr(Constr newConstr) {
		Constr oldConstr = constr;
		constr = newConstr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.CONSTR_EXP__CONSTR, oldConstr, constr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.CONSTR_EXP__ARG:
			return basicSetArg(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.CONSTR_EXP__ARG:
			return getArg();
		case FiacrePackage.CONSTR_EXP__CONSTR:
			if (resolve)
				return getConstr();
			return basicGetConstr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.CONSTR_EXP__ARG:
			setArg((Exp) newValue);
			return;
		case FiacrePackage.CONSTR_EXP__CONSTR:
			setConstr((Constr) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.CONSTR_EXP__ARG:
			setArg((Exp) null);
			return;
		case FiacrePackage.CONSTR_EXP__CONSTR:
			setConstr((Constr) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.CONSTR_EXP__ARG:
			return arg != null;
		case FiacrePackage.CONSTR_EXP__CONSTR:
			return constr != null;
		}
		return super.eIsSet(featureID);
	}

} //ConstrExpImpl
