/**
 * <copyright>
 * </copyright>
 *
 * $Id: CondExpImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.CondExp;
import org.topcased.fiacre.model.Exp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cond Exp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.CondExpImpl#getCond <em>Cond</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.CondExpImpl#getIff <em>Iff</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.CondExpImpl#getIft <em>Ift</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CondExpImpl extends ExpImpl implements CondExp {
	/**
	 * The cached value of the '{@link #getCond() <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCond()
	 * @generated
	 * @ordered
	 */
	protected Exp cond;

	/**
	 * The cached value of the '{@link #getIff() <em>Iff</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIff()
	 * @generated
	 * @ordered
	 */
	protected Exp iff;

	/**
	 * The cached value of the '{@link #getIft() <em>Ift</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIft()
	 * @generated
	 * @ordered
	 */
	protected Exp ift;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CondExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.COND_EXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getCond() {
		return cond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCond(Exp newCond, NotificationChain msgs) {
		Exp oldCond = cond;
		cond = newCond;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.COND_EXP__COND, oldCond,
					newCond);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCond(Exp newCond) {
		if (newCond != cond) {
			NotificationChain msgs = null;
			if (cond != null)
				msgs = ((InternalEObject) cond).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__COND,
						null, msgs);
			if (newCond != null)
				msgs = ((InternalEObject) newCond).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__COND,
						null, msgs);
			msgs = basicSetCond(newCond, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.COND_EXP__COND, newCond, newCond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getIff() {
		return iff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIff(Exp newIff, NotificationChain msgs) {
		Exp oldIff = iff;
		iff = newIff;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.COND_EXP__IFF, oldIff,
					newIff);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIff(Exp newIff) {
		if (newIff != iff) {
			NotificationChain msgs = null;
			if (iff != null)
				msgs = ((InternalEObject) iff).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__IFF,
						null, msgs);
			if (newIff != null)
				msgs = ((InternalEObject) newIff).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__IFF,
						null, msgs);
			msgs = basicSetIff(newIff, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.COND_EXP__IFF, newIff, newIff));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getIft() {
		return ift;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIft(Exp newIft, NotificationChain msgs) {
		Exp oldIft = ift;
		ift = newIft;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, FiacrePackage.COND_EXP__IFT, oldIft,
					newIft);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIft(Exp newIft) {
		if (newIft != ift) {
			NotificationChain msgs = null;
			if (ift != null)
				msgs = ((InternalEObject) ift).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__IFT,
						null, msgs);
			if (newIft != null)
				msgs = ((InternalEObject) newIft).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FiacrePackage.COND_EXP__IFT,
						null, msgs);
			msgs = basicSetIft(newIft, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.COND_EXP__IFT, newIft, newIft));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.COND_EXP__COND:
			return basicSetCond(null, msgs);
		case FiacrePackage.COND_EXP__IFF:
			return basicSetIff(null, msgs);
		case FiacrePackage.COND_EXP__IFT:
			return basicSetIft(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.COND_EXP__COND:
			return getCond();
		case FiacrePackage.COND_EXP__IFF:
			return getIff();
		case FiacrePackage.COND_EXP__IFT:
			return getIft();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.COND_EXP__COND:
			setCond((Exp) newValue);
			return;
		case FiacrePackage.COND_EXP__IFF:
			setIff((Exp) newValue);
			return;
		case FiacrePackage.COND_EXP__IFT:
			setIft((Exp) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.COND_EXP__COND:
			setCond((Exp) null);
			return;
		case FiacrePackage.COND_EXP__IFF:
			setIff((Exp) null);
			return;
		case FiacrePackage.COND_EXP__IFT:
			setIft((Exp) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.COND_EXP__COND:
			return cond != null;
		case FiacrePackage.COND_EXP__IFF:
			return iff != null;
		case FiacrePackage.COND_EXP__IFT:
			return ift != null;
		}
		return super.eIsSet(featureID);
	}

} //CondExpImpl
