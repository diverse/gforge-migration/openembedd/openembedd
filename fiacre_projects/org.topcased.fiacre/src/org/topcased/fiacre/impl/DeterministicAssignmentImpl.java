/**
 * <copyright>
 * </copyright>
 *
 * $Id: DeterministicAssignmentImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.DeterministicAssignment;
import org.topcased.fiacre.model.SingleAssignment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deterministic Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.DeterministicAssignmentImpl#getAssignments <em>Assignments</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DeterministicAssignmentImpl extends AssignmentImpl implements
		DeterministicAssignment {
	/**
	 * The cached value of the '{@link #getAssignments() <em>Assignments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignments()
	 * @generated
	 * @ordered
	 */
	protected EList<SingleAssignment> assignments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeterministicAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.DETERMINISTIC_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleAssignment> getAssignments() {
		if (assignments == null) {
			assignments = new EObjectContainmentEList<SingleAssignment>(
					SingleAssignment.class, this,
					FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS);
		}
		return assignments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS:
			return ((InternalEList<?>) getAssignments()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS:
			return getAssignments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS:
			getAssignments().clear();
			getAssignments().addAll(
					(Collection<? extends SingleAssignment>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS:
			getAssignments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS:
			return assignments != null && !assignments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DeterministicAssignmentImpl
