/**
 * <copyright>
 * </copyright>
 *
 * $Id: NonDeterministicAssignmentImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Exp;
import org.topcased.fiacre.model.NonDeterministicAssignment;
import org.topcased.fiacre.model.Pattern;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Deterministic Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.NonDeterministicAssignmentImpl#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.NonDeterministicAssignmentImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NonDeterministicAssignmentImpl extends AssignmentImpl implements
		NonDeterministicAssignment {
	/**
	 * The cached value of the '{@link #getLhs() <em>Lhs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLhs()
	 * @generated
	 * @ordered
	 */
	protected EList<Pattern> lhs;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected Exp condition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NonDeterministicAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.NON_DETERMINISTIC_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Pattern> getLhs() {
		if (lhs == null) {
			lhs = new EObjectContainmentEList<Pattern>(Pattern.class, this,
					FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS);
		}
		return lhs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exp getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(Exp newCondition,
			NotificationChain msgs) {
		Exp oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION,
					oldCondition, newCondition);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(Exp newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject) condition)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION,
								null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject) newCondition)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION,
								null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION,
					newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS:
			return ((InternalEList<?>) getLhs()).basicRemove(otherEnd, msgs);
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION:
			return basicSetCondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS:
			return getLhs();
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION:
			return getCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS:
			getLhs().clear();
			getLhs().addAll((Collection<? extends Pattern>) newValue);
			return;
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION:
			setCondition((Exp) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS:
			getLhs().clear();
			return;
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION:
			setCondition((Exp) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__LHS:
			return lhs != null && !lhs.isEmpty();
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT__CONDITION:
			return condition != null;
		}
		return super.eIsSet(featureID);
	}

} //NonDeterministicAssignmentImpl
