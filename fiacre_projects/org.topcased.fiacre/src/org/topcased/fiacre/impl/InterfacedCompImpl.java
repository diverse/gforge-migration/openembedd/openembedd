/**
 * <copyright>
 * </copyright>
 *
 * $Id: InterfacedCompImpl.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.Composition;
import org.topcased.fiacre.model.InterfacedComp;
import org.topcased.fiacre.model.PortDecl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interfaced Comp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.fiacre.impl.InterfacedCompImpl#getComposition <em>Composition</em>}</li>
 *   <li>{@link org.topcased.fiacre.impl.InterfacedCompImpl#getSyncPorts <em>Sync Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InterfacedCompImpl extends EObjectImpl implements InterfacedComp {
	/**
	 * The cached value of the '{@link #getComposition() <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComposition()
	 * @generated
	 * @ordered
	 */
	protected Composition composition;

	/**
	 * The cached value of the '{@link #getSyncPorts() <em>Sync Ports</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<PortDecl> syncPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfacedCompImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FiacrePackage.Literals.INTERFACED_COMP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composition getComposition() {
		return composition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComposition(Composition newComposition,
			NotificationChain msgs) {
		Composition oldComposition = composition;
		composition = newComposition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					FiacrePackage.INTERFACED_COMP__COMPOSITION, oldComposition,
					newComposition);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComposition(Composition newComposition) {
		if (newComposition != composition) {
			NotificationChain msgs = null;
			if (composition != null)
				msgs = ((InternalEObject) composition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.INTERFACED_COMP__COMPOSITION,
						null, msgs);
			if (newComposition != null)
				msgs = ((InternalEObject) newComposition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- FiacrePackage.INTERFACED_COMP__COMPOSITION,
						null, msgs);
			msgs = basicSetComposition(newComposition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FiacrePackage.INTERFACED_COMP__COMPOSITION, newComposition,
					newComposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortDecl> getSyncPorts() {
		if (syncPorts == null) {
			syncPorts = new EObjectResolvingEList<PortDecl>(PortDecl.class,
					this, FiacrePackage.INTERFACED_COMP__SYNC_PORTS);
		}
		return syncPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FiacrePackage.INTERFACED_COMP__COMPOSITION:
			return basicSetComposition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FiacrePackage.INTERFACED_COMP__COMPOSITION:
			return getComposition();
		case FiacrePackage.INTERFACED_COMP__SYNC_PORTS:
			return getSyncPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FiacrePackage.INTERFACED_COMP__COMPOSITION:
			setComposition((Composition) newValue);
			return;
		case FiacrePackage.INTERFACED_COMP__SYNC_PORTS:
			getSyncPorts().clear();
			getSyncPorts().addAll((Collection<? extends PortDecl>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FiacrePackage.INTERFACED_COMP__COMPOSITION:
			setComposition((Composition) null);
			return;
		case FiacrePackage.INTERFACED_COMP__SYNC_PORTS:
			getSyncPorts().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FiacrePackage.INTERFACED_COMP__COMPOSITION:
			return composition != null;
		case FiacrePackage.INTERFACED_COMP__SYNC_PORTS:
			return syncPorts != null && !syncPorts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InterfacedCompImpl
