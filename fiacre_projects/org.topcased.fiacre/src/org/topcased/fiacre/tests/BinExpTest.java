/**
 * <copyright>
 * </copyright>
 *
 * $Id: BinExpTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.BinExp;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Bin Exp</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BinExpTest extends ExpTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BinExpTest.class);
	}

	/**
	 * Constructs a new Bin Exp test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinExpTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Bin Exp test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BinExp getFixture() {
		return (BinExp) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createBinExp());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BinExpTest
