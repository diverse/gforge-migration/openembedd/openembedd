/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineCollectionTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import org.topcased.fiacre.model.InlineCollection;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Inline Collection</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class InlineCollectionTest extends ExpTest {

	/**
	 * Constructs a new Inline Collection test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineCollectionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Inline Collection test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected InlineCollection getFixture() {
		return (InlineCollection) fixture;
	}

} //InlineCollectionTest
