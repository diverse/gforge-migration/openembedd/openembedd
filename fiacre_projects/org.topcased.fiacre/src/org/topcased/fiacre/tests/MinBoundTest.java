/**
 * <copyright>
 * </copyright>
 *
 * $Id: MinBoundTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.MinBound;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Min Bound</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MinBoundTest extends TestCase {

	/**
	 * The fixture for this Min Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MinBound fixture = null;

	/**
	 * Constructs a new Min Bound test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinBoundTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Min Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MinBound fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Min Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MinBound getFixture() {
		return fixture;
	}

} //MinBoundTest
