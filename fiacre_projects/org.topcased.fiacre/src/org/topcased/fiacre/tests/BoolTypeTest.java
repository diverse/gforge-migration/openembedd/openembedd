/**
 * <copyright>
 * </copyright>
 *
 * $Id: BoolTypeTest.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.BoolType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Bool Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BoolTypeTest extends BasicTypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BoolTypeTest.class);
	}

	/**
	 * Constructs a new Bool Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Bool Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BoolType getFixture() {
		return (BoolType) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createBoolType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BoolTypeTest
