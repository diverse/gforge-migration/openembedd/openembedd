/**
 * <copyright>
 * </copyright>
 *
 * $Id: LocalPortDeclTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.LocalPortDecl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Local Port Decl</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LocalPortDeclTest extends PortDeclTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LocalPortDeclTest.class);
	}

	/**
	 * Constructs a new Local Port Decl test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalPortDeclTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Local Port Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LocalPortDecl getFixture() {
		return (LocalPortDecl) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createLocalPortDecl());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LocalPortDeclTest
