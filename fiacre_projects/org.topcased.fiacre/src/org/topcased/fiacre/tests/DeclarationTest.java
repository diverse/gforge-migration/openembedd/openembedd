/**
 * <copyright>
 * </copyright>
 *
 * $Id: DeclarationTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.Declaration;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeclarationTest extends TestCase {

	/**
	 * The fixture for this Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Declaration fixture = null;

	/**
	 * Constructs a new Declaration test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Declaration fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Declaration getFixture() {
		return fixture;
	}

} //DeclarationTest
