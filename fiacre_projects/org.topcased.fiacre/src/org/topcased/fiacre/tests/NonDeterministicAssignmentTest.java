/**
 * <copyright>
 * </copyright>
 *
 * $Id: NonDeterministicAssignmentTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.NonDeterministicAssignment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Non Deterministic Assignment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class NonDeterministicAssignmentTest extends AssignmentTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NonDeterministicAssignmentTest.class);
	}

	/**
	 * Constructs a new Non Deterministic Assignment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonDeterministicAssignmentTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Non Deterministic Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NonDeterministicAssignment getFixture() {
		return (NonDeterministicAssignment) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createNonDeterministicAssignment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //NonDeterministicAssignmentTest
