/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeDeclTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import org.topcased.fiacre.model.NodeDecl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Node Decl</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class NodeDeclTest extends DeclarationTest {

	/**
	 * Constructs a new Node Decl test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeDeclTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Node Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NodeDecl getFixture() {
		return (NodeDecl) fixture;
	}

} //NodeDeclTest
