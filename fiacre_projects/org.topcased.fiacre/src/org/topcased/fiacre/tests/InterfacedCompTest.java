/**
 * <copyright>
 * </copyright>
 *
 * $Id: InterfacedCompTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.InterfacedComp;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interfaced Comp</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InterfacedCompTest extends TestCase {

	/**
	 * The fixture for this Interfaced Comp test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfacedComp fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InterfacedCompTest.class);
	}

	/**
	 * Constructs a new Interfaced Comp test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfacedCompTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interfaced Comp test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InterfacedComp fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interfaced Comp test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfacedComp getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createInterfacedComp());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InterfacedCompTest
