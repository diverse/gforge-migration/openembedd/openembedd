/**
 * <copyright>
 * </copyright>
 *
 * $Id: CommunicationTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import org.topcased.fiacre.model.Communication;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CommunicationTest extends StatementTest {

	/**
	 * Constructs a new Communication test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Communication test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Communication getFixture() {
		return (Communication) fixture;
	}

} //CommunicationTest
