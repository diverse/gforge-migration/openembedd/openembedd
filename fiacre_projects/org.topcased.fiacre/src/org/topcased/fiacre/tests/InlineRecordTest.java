/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineRecordTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.InlineRecord;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Inline Record</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InlineRecordTest extends ExpTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InlineRecordTest.class);
	}

	/**
	 * Constructs a new Inline Record test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineRecordTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Inline Record test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected InlineRecord getFixture() {
		return (InlineRecord) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createInlineRecord());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InlineRecordTest
