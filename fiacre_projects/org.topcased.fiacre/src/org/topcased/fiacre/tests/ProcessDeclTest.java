/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProcessDeclTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.ProcessDecl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Process Decl</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcessDeclTest extends NodeDeclTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProcessDeclTest.class);
	}

	/**
	 * Constructs a new Process Decl test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessDeclTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Process Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ProcessDecl getFixture() {
		return (ProcessDecl) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createProcessDecl());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ProcessDeclTest
