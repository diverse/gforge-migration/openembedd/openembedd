/**
 * <copyright>
 * </copyright>
 *
 * $Id: PortDeclTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.PortDecl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Port Decl</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class PortDeclTest extends TestCase {

	/**
	 * The fixture for this Port Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortDecl fixture = null;

	/**
	 * Constructs a new Port Decl test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDeclTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Port Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(PortDecl fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Port Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortDecl getFixture() {
		return fixture;
	}

} //PortDeclTest
