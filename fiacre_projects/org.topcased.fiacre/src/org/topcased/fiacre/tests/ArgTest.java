/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArgTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.Arg;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Arg</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ArgTest extends TestCase {

	/**
	 * The fixture for this Arg test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Arg fixture = null;

	/**
	 * Constructs a new Arg test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Arg test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Arg fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Arg test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Arg getFixture() {
		return fixture;
	}

} //ArgTest
