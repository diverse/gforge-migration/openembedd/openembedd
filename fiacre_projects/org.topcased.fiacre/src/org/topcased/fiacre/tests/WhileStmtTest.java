/**
 * <copyright>
 * </copyright>
 *
 * $Id: WhileStmtTest.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.WhileStmt;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>While Stmt</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class WhileStmtTest extends StatementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(WhileStmtTest.class);
	}

	/**
	 * Constructs a new While Stmt test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileStmtTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this While Stmt test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected WhileStmt getFixture() {
		return (WhileStmt) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createWhileStmt());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //WhileStmtTest
