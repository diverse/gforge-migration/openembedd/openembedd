/**
 * <copyright>
 * </copyright>
 *
 * $Id: BasicTypeTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import org.topcased.fiacre.model.BasicType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BasicTypeTest extends TypeTest {

	/**
	 * Constructs a new Basic Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Basic Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BasicType getFixture() {
		return (BasicType) fixture;
	}

} //BasicTypeTest
