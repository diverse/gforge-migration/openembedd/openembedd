/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExpTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import org.topcased.fiacre.model.Exp;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Exp</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ExpTest extends ArgTest {

	/**
	 * Constructs a new Exp test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Exp test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Exp getFixture() {
		return (Exp) fixture;
	}

} //ExpTest
