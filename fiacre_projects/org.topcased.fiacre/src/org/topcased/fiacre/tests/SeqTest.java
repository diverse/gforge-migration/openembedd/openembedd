/**
 * <copyright>
 * </copyright>
 *
 * $Id: SeqTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.Seq;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Seq</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SeqTest extends StatementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SeqTest.class);
	}

	/**
	 * Constructs a new Seq test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SeqTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Seq test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Seq getFixture() {
		return (Seq) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createSeq());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SeqTest
