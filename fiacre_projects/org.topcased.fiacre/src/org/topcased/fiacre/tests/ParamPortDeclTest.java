/**
 * <copyright>
 * </copyright>
 *
 * $Id: ParamPortDeclTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.ParamPortDecl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Param Port Decl</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ParamPortDeclTest extends PortDeclTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ParamPortDeclTest.class);
	}

	/**
	 * Constructs a new Param Port Decl test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParamPortDeclTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Param Port Decl test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ParamPortDecl getFixture() {
		return (ParamPortDecl) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createParamPortDecl());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ParamPortDeclTest
