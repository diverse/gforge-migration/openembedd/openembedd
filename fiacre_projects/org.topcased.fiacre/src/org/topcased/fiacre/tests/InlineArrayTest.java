/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineArrayTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.InlineArray;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Inline Array</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InlineArrayTest extends InlineCollectionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InlineArrayTest.class);
	}

	/**
	 * Constructs a new Inline Array test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlineArrayTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Inline Array test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected InlineArray getFixture() {
		return (InlineArray) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createInlineArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InlineArrayTest
