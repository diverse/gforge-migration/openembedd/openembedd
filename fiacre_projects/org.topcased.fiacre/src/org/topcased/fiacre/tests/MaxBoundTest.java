/**
 * <copyright>
 * </copyright>
 *
 * $Id: MaxBoundTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.MaxBound;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Max Bound</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MaxBoundTest extends TestCase {

	/**
	 * The fixture for this Max Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaxBound fixture = null;

	/**
	 * Constructs a new Max Bound test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxBoundTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Max Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MaxBound fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Max Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaxBound getFixture() {
		return fixture;
	}

} //MaxBoundTest
