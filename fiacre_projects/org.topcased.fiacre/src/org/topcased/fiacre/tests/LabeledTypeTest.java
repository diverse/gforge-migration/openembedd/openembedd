/**
 * <copyright>
 * </copyright>
 *
 * $Id: LabeledTypeTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import org.topcased.fiacre.model.LabeledType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Labeled Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class LabeledTypeTest extends TestCase {

	/**
	 * The fixture for this Labeled Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LabeledType fixture = null;

	/**
	 * Constructs a new Labeled Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LabeledTypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Labeled Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(LabeledType fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Labeled Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LabeledType getFixture() {
		return fixture;
	}

} //LabeledTypeTest
