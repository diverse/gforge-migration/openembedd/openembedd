/**
 * <copyright>
 * </copyright>
 *
 * $Id: FiniteBoundTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.FiniteBound;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Finite Bound</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FiniteBoundTest extends TestCase {

	/**
	 * The fixture for this Finite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FiniteBound fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FiniteBoundTest.class);
	}

	/**
	 * Constructs a new Finite Bound test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiniteBoundTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Finite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FiniteBound fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Finite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FiniteBound getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createFiniteBound());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FiniteBoundTest
