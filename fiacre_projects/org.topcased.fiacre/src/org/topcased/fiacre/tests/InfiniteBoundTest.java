/**
 * <copyright>
 * </copyright>
 *
 * $Id: InfiniteBoundTest.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.topcased.fiacre.FiacreFactory;

import org.topcased.fiacre.model.InfiniteBound;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Infinite Bound</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InfiniteBoundTest extends TestCase {

	/**
	 * The fixture for this Infinite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InfiniteBound fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InfiniteBoundTest.class);
	}

	/**
	 * Constructs a new Infinite Bound test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfiniteBoundTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Infinite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InfiniteBound fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Infinite Bound test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InfiniteBound getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FiacreFactory.eINSTANCE.createInfiniteBound());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InfiniteBoundTest
