/**
 * <copyright>
 * </copyright>
 *
 * $Id: FiacrePackage.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.topcased.fiacre.FiacreFactory
 * @model kind="package"
 * @generated
 */
public interface FiacrePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fiacre";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.topcased.fiacre";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fiacre";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FiacrePackage eINSTANCE = org.topcased.fiacre.impl.FiacrePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ProgramImpl <em>Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ProgramImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProgram()
	 * @generated
	 */
	int PROGRAM = 0;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__DECLARATIONS = 0;

	/**
	 * The feature id for the '<em><b>Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__ROOT = 1;

	/**
	 * The number of structural features of the '<em>Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.DeclarationImpl <em>Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.DeclarationImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getDeclaration()
	 * @generated
	 */
	int DECLARATION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__NAME = 0;

	/**
	 * The number of structural features of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.NodeDeclImpl <em>Node Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.NodeDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNodeDecl()
	 * @generated
	 */
	int NODE_DECL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL__ARGS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vars</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL__VARS = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL__PORTS = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Local Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL__LOCAL_PORTS = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Node Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_DECL_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.TypeDeclImpl <em>Type Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.TypeDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTypeDecl()
	 * @generated
	 */
	int TYPE_DECL = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECL__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECL__IS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECL_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ChannelDeclImpl <em>Channel Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ChannelDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannelDecl()
	 * @generated
	 */
	int CHANNEL_DECL = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_DECL__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_DECL__IS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Channel Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_DECL_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ComponentDeclImpl <em>Component Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ComponentDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getComponentDecl()
	 * @generated
	 */
	int COMPONENT_DECL = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__NAME = NODE_DECL__NAME;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__ARGS = NODE_DECL__ARGS;

	/**
	 * The feature id for the '<em><b>Vars</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__VARS = NODE_DECL__VARS;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__PORTS = NODE_DECL__PORTS;

	/**
	 * The feature id for the '<em><b>Local Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__LOCAL_PORTS = NODE_DECL__LOCAL_PORTS;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__BODY = NODE_DECL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL__PRIORITY = NODE_DECL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Component Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECL_FEATURE_COUNT = NODE_DECL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ProcessDeclImpl <em>Process Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ProcessDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProcessDecl()
	 * @generated
	 */
	int PROCESS_DECL = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__NAME = NODE_DECL__NAME;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__ARGS = NODE_DECL__ARGS;

	/**
	 * The feature id for the '<em><b>Vars</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__VARS = NODE_DECL__VARS;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__PORTS = NODE_DECL__PORTS;

	/**
	 * The feature id for the '<em><b>Local Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__LOCAL_PORTS = NODE_DECL__LOCAL_PORTS;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__STATES = NODE_DECL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__TRANSITIONS = NODE_DECL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Init Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL__INIT_ACTION = NODE_DECL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Process Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_DECL_FEATURE_COUNT = NODE_DECL_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.PortDeclImpl <em>Port Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.PortDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPortDecl()
	 * @generated
	 */
	int PORT_DECL = 7;

	/**
	 * The feature id for the '<em><b>Channel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DECL__CHANNEL = 0;

	/**
	 * The feature id for the '<em><b>In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DECL__IN = 1;

	/**
	 * The feature id for the '<em><b>Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DECL__OUT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DECL__NAME = 3;

	/**
	 * The number of structural features of the '<em>Port Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DECL_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.VariableImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 62;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ArgumentVariableImpl <em>Argument Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ArgumentVariableImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArgumentVariable()
	 * @generated
	 */
	int ARGUMENT_VARIABLE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE__TYPE = VARIABLE__TYPE;

	/**
	 * The feature id for the '<em><b>Read</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE__READ = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Write</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE__WRITE = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE__REF = VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Argument Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.LocalVariableImpl <em>Local Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.LocalVariableImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLocalVariable()
	 * @generated
	 */
	int LOCAL_VARIABLE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__TYPE = VARIABLE__TYPE;

	/**
	 * The feature id for the '<em><b>Initializer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__INITIALIZER = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Local Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.CompositionImpl <em>Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.CompositionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getComposition()
	 * @generated
	 */
	int COMPOSITION = 10;

	/**
	 * The number of structural features of the '<em>Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ShuffleImpl <em>Shuffle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ShuffleImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getShuffle()
	 * @generated
	 */
	int SHUFFLE = 11;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHUFFLE__ARGS = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Shuffle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHUFFLE_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.SyncImpl <em>Sync</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.SyncImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSync()
	 * @generated
	 */
	int SYNC = 12;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC__ARGS = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sync</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ParImpl <em>Par</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ParImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPar()
	 * @generated
	 */
	int PAR = 13;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAR__ARGS = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Par</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAR_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InstanceImpl <em>Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InstanceImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInstance()
	 * @generated
	 */
	int INSTANCE = 14;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__TYPE = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__ARGS = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__PORTS = COMPOSITION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__NAME = COMPOSITION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InterfacedCompImpl <em>Interfaced Comp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InterfacedCompImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInterfacedComp()
	 * @generated
	 */
	int INTERFACED_COMP = 15;

	/**
	 * The feature id for the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_COMP__COMPOSITION = 0;

	/**
	 * The feature id for the '<em><b>Sync Ports</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_COMP__SYNC_PORTS = 1;

	/**
	 * The number of structural features of the '<em>Interfaced Comp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACED_COMP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ArgImpl <em>Arg</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ArgImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArg()
	 * @generated
	 */
	int ARG = 74;

	/**
	 * The number of structural features of the '<em>Arg</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARG_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ExpImpl <em>Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ExpImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getExp()
	 * @generated
	 */
	int EXP = 16;

	/**
	 * The number of structural features of the '<em>Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXP_FEATURE_COUNT = ARG_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.StateImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getState()
	 * @generated
	 */
	int STATE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 0;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.TransitionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 18;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__FROM = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.StatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.StatementImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getStatement()
	 * @generated
	 */
	int STATEMENT = 26;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.NullStmtImpl <em>Null Stmt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.NullStmtImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNullStmt()
	 * @generated
	 */
	int NULL_STMT = 19;

	/**
	 * The number of structural features of the '<em>Null Stmt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.AssignmentImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 20;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.CommunicationImpl <em>Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.CommunicationImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCommunication()
	 * @generated
	 */
	int COMMUNICATION = 21;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PORT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.WhileStmtImpl <em>While Stmt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.WhileStmtImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getWhileStmt()
	 * @generated
	 */
	int WHILE_STMT = 22;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_STMT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_STMT__BODY = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>While Stmt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.IfStmtImpl <em>If Stmt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.IfStmtImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getIfStmt()
	 * @generated
	 */
	int IF_STMT = 23;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STMT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STMT__THEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STMT__ELSE = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Stmt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.SelectImpl <em>Select</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.SelectImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSelect()
	 * @generated
	 */
	int SELECT = 24;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Select</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ToImpl <em>To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ToImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTo()
	 * @generated
	 */
	int TO = 25;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO__DEST = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.DeterministicAssignmentImpl <em>Deterministic Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.DeterministicAssignmentImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getDeterministicAssignment()
	 * @generated
	 */
	int DETERMINISTIC_ASSIGNMENT = 27;

	/**
	 * The feature id for the '<em><b>Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS = ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deterministic Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_ASSIGNMENT_FEATURE_COUNT = ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.NonDeterministicAssignmentImpl <em>Non Deterministic Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.NonDeterministicAssignmentImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNonDeterministicAssignment()
	 * @generated
	 */
	int NON_DETERMINISTIC_ASSIGNMENT = 28;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_DETERMINISTIC_ASSIGNMENT__LHS = ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_DETERMINISTIC_ASSIGNMENT__CONDITION = ASSIGNMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Non Deterministic Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_DETERMINISTIC_ASSIGNMENT_FEATURE_COUNT = ASSIGNMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.SingleAssignmentImpl <em>Single Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.SingleAssignmentImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSingleAssignment()
	 * @generated
	 */
	int SINGLE_ASSIGNMENT = 29;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ASSIGNMENT__LHS = 0;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ASSIGNMENT__RHS = 1;

	/**
	 * The number of structural features of the '<em>Single Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ASSIGNMENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.UnExpImpl <em>Un Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.UnExpImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnExp()
	 * @generated
	 */
	int UN_EXP = 30;

	/**
	 * The feature id for the '<em><b>Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_EXP__EXP = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_EXP__UNOP = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Un Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_EXP_FEATURE_COUNT = EXP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.BinExpImpl <em>Bin Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.BinExpImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBinExp()
	 * @generated
	 */
	int BIN_EXP = 31;

	/**
	 * The feature id for the '<em><b>Rexp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__REXP = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lexp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__LEXP = EXP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bin Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__BIN_OP = EXP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Bin Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP_FEATURE_COUNT = EXP_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.LiteralImpl <em>Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.LiteralImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLiteral()
	 * @generated
	 */
	int LITERAL = 71;

	/**
	 * The number of structural features of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_FEATURE_COUNT = EXP_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.NatLiteralImpl <em>Nat Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.NatLiteralImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNatLiteral()
	 * @generated
	 */
	int NAT_LITERAL = 32;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAT_LITERAL__VALUE = LITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Nat Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAT_LITERAL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.BoolLiteralImpl <em>Bool Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.BoolLiteralImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBoolLiteral()
	 * @generated
	 */
	int BOOL_LITERAL = 33;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_LITERAL__VALUE = LITERAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bool Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_LITERAL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.PatternImpl <em>Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.PatternImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPattern()
	 * @generated
	 */
	int PATTERN = 66;

	/**
	 * The number of structural features of the '<em>Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.VarRefImpl <em>Var Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.VarRefImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getVarRef()
	 * @generated
	 */
	int VAR_REF = 34;

	/**
	 * The feature id for the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_REF__DECL = PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Var Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_REF_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ArrayElemImpl <em>Array Elem</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ArrayElemImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArrayElem()
	 * @generated
	 */
	int ARRAY_ELEM = 35;

	/**
	 * The feature id for the '<em><b>Array</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_ELEM__ARRAY = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_ELEM__INDEX = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array Elem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_ELEM_FEATURE_COUNT = EXP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.RecordElemImpl <em>Record Elem</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.RecordElemImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRecordElem()
	 * @generated
	 */
	int RECORD_ELEM = 36;

	/**
	 * The feature id for the '<em><b>Record</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_ELEM__RECORD = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_ELEM__FIELD = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Record Elem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_ELEM_FEATURE_COUNT = EXP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InlineCollectionImpl <em>Inline Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InlineCollectionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineCollection()
	 * @generated
	 */
	int INLINE_COLLECTION = 78;

	/**
	 * The feature id for the '<em><b>Elems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_COLLECTION__ELEMS = EXP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inline Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_COLLECTION_FEATURE_COUNT = EXP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InlineQueueImpl <em>Inline Queue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InlineQueueImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineQueue()
	 * @generated
	 */
	int INLINE_QUEUE = 37;

	/**
	 * The feature id for the '<em><b>Elems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_QUEUE__ELEMS = INLINE_COLLECTION__ELEMS;

	/**
	 * The number of structural features of the '<em>Inline Queue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_QUEUE_FEATURE_COUNT = INLINE_COLLECTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.TypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getType()
	 * @generated
	 */
	int TYPE = 38;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.BasicTypeImpl <em>Basic Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.BasicTypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBasicType()
	 * @generated
	 */
	int BASIC_TYPE = 39;

	/**
	 * The number of structural features of the '<em>Basic Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.BoolTypeImpl <em>Bool Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.BoolTypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBoolType()
	 * @generated
	 */
	int BOOL_TYPE = 40;

	/**
	 * The number of structural features of the '<em>Bool Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.NatTypeImpl <em>Nat Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.NatTypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNatType()
	 * @generated
	 */
	int NAT_TYPE = 41;

	/**
	 * The number of structural features of the '<em>Nat Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAT_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.IntTypeImpl <em>Int Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.IntTypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getIntType()
	 * @generated
	 */
	int INT_TYPE = 42;

	/**
	 * The number of structural features of the '<em>Int Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.IntervalImpl <em>Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.IntervalImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInterval()
	 * @generated
	 */
	int INTERVAL = 43;

	/**
	 * The feature id for the '<em><b>Mini</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERVAL__MINI = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Maxi</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERVAL__MAXI = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERVAL_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.RecordImpl <em>Record</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.RecordImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRecord()
	 * @generated
	 */
	int RECORD = 44;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD__FIELDS = TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.LabeledTypeImpl <em>Labeled Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.LabeledTypeImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLabeledType()
	 * @generated
	 */
	int LABELED_TYPE = 80;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABELED_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABELED_TYPE__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Labeled Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABELED_TYPE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.FieldImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getField()
	 * @generated
	 */
	int FIELD = 45;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__NAME = LABELED_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__TYPE = LABELED_TYPE__TYPE;

	/**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_FEATURE_COUNT = LABELED_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ArrayImpl <em>Array</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ArrayImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArray()
	 * @generated
	 */
	int ARRAY = 46;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY__SIZE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY__TYPE = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.QueueImpl <em>Queue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.QueueImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getQueue()
	 * @generated
	 */
	int QUEUE = 47;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE__SIZE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE__TYPE = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Queue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.TypeIdImpl <em>Type Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.TypeIdImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTypeId()
	 * @generated
	 */
	int TYPE_ID = 48;

	/**
	 * The feature id for the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ID__DECL = TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ID_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ChannelImpl <em>Channel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ChannelImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannel()
	 * @generated
	 */
	int CHANNEL = 49;

	/**
	 * The number of structural features of the '<em>Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ChannelIdImpl <em>Channel Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ChannelIdImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannelId()
	 * @generated
	 */
	int CHANNEL_ID = 50;

	/**
	 * The feature id for the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_ID__DECL = CHANNEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Channel Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANNEL_ID_FEATURE_COUNT = CHANNEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ProfileImpl <em>Profile</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ProfileImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProfile()
	 * @generated
	 */
	int PROFILE = 51;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE__TYPES = CHANNEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Profile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_FEATURE_COUNT = CHANNEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.OrChannelImpl <em>Or Channel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.OrChannelImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getOrChannel()
	 * @generated
	 */
	int OR_CHANNEL = 52;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CHANNEL__LHS = CHANNEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CHANNEL__RHS = CHANNEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Or Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_CHANNEL_FEATURE_COUNT = CHANNEL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InlineArrayImpl <em>Inline Array</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InlineArrayImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineArray()
	 * @generated
	 */
	int INLINE_ARRAY = 53;

	/**
	 * The feature id for the '<em><b>Elems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_ARRAY__ELEMS = INLINE_COLLECTION__ELEMS;

	/**
	 * The number of structural features of the '<em>Inline Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_ARRAY_FEATURE_COUNT = INLINE_COLLECTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InlineRecordImpl <em>Inline Record</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InlineRecordImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineRecord()
	 * @generated
	 */
	int INLINE_RECORD = 54;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_RECORD__VALUES = EXP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inline Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INLINE_RECORD_FEATURE_COUNT = EXP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ValuedFieldImpl <em>Valued Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ValuedFieldImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getValuedField()
	 * @generated
	 */
	int VALUED_FIELD = 55;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_FIELD__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_FIELD__FIELD = 1;

	/**
	 * The number of structural features of the '<em>Valued Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_FIELD_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.SynchronizationImpl <em>Synchronization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.SynchronizationImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSynchronization()
	 * @generated
	 */
	int SYNCHRONIZATION = 56;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION__PORT = COMMUNICATION__PORT;

	/**
	 * The number of structural features of the '<em>Synchronization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ReceptionImpl <em>Reception</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ReceptionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getReception()
	 * @generated
	 */
	int RECEPTION = 57;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTION__PORT = COMMUNICATION__PORT;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTION__PATTERN = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Where</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTION__WHERE = COMMUNICATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reception</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTION_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.EmissionImpl <em>Emission</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.EmissionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getEmission()
	 * @generated
	 */
	int EMISSION = 58;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMISSION__PORT = COMMUNICATION__PORT;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMISSION__ARGS = COMMUNICATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Emission</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMISSION_FEATURE_COUNT = COMMUNICATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.SeqImpl <em>Seq</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.SeqImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSeq()
	 * @generated
	 */
	int SEQ = 59;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQ__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Seq</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQ_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.LocalPortDeclImpl <em>Local Port Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.LocalPortDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLocalPortDecl()
	 * @generated
	 */
	int LOCAL_PORT_DECL = 60;

	/**
	 * The feature id for the '<em><b>Channel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__CHANNEL = PORT_DECL__CHANNEL;

	/**
	 * The feature id for the '<em><b>In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__IN = PORT_DECL__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__OUT = PORT_DECL__OUT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__NAME = PORT_DECL__NAME;

	/**
	 * The feature id for the '<em><b>Mini</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__MINI = PORT_DECL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Maxi</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL__MAXI = PORT_DECL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Local Port Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_PORT_DECL_FEATURE_COUNT = PORT_DECL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ParamPortDeclImpl <em>Param Port Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ParamPortDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getParamPortDecl()
	 * @generated
	 */
	int PARAM_PORT_DECL = 61;

	/**
	 * The feature id for the '<em><b>Channel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAM_PORT_DECL__CHANNEL = PORT_DECL__CHANNEL;

	/**
	 * The feature id for the '<em><b>In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAM_PORT_DECL__IN = PORT_DECL__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAM_PORT_DECL__OUT = PORT_DECL__OUT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAM_PORT_DECL__NAME = PORT_DECL__NAME;

	/**
	 * The number of structural features of the '<em>Param Port Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAM_PORT_DECL_FEATURE_COUNT = PORT_DECL_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.PriorityImpl <em>Priority</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.PriorityImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPriority()
	 * @generated
	 */
	int PRIORITY = 63;

	/**
	 * The feature id for the '<em><b>Inf</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY__INF = 0;

	/**
	 * The feature id for the '<em><b>Sup</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY__SUP = 1;

	/**
	 * The number of structural features of the '<em>Priority</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ConstrExpImpl <em>Constr Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ConstrExpImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstrExp()
	 * @generated
	 */
	int CONSTR_EXP = 64;

	/**
	 * The feature id for the '<em><b>Arg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_EXP__ARG = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_EXP__CONSTR = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constr Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_EXP_FEATURE_COUNT = EXP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.UnionImpl <em>Union</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.UnionImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnion()
	 * @generated
	 */
	int UNION = 65;

	/**
	 * The feature id for the '<em><b>Constr</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNION__CONSTR = TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Union</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNION_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ConstrPatternImpl <em>Constr Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ConstrPatternImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstrPattern()
	 * @generated
	 */
	int CONSTR_PATTERN = 67;

	/**
	 * The feature id for the '<em><b>Arg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_PATTERN__ARG = PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_PATTERN__CONSTR = PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constr Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ArrayPatternImpl <em>Array Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ArrayPatternImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArrayPattern()
	 * @generated
	 */
	int ARRAY_PATTERN = 68;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_PATTERN__INDEX = PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Array</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_PATTERN__ARRAY = PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.FieldPatternImpl <em>Field Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.FieldPatternImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getFieldPattern()
	 * @generated
	 */
	int FIELD_PATTERN = 69;

	/**
	 * The feature id for the '<em><b>Record</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_PATTERN__RECORD = PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_PATTERN__FIELD = PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Field Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.AnyPatternImpl <em>Any Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.AnyPatternImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getAnyPattern()
	 * @generated
	 */
	int ANY_PATTERN = 70;

	/**
	 * The number of structural features of the '<em>Any Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.CaseStmtImpl <em>Case Stmt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.CaseStmtImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCaseStmt()
	 * @generated
	 */
	int CASE_STMT = 72;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STMT__RULES = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STMT__EXP = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Case Stmt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STMT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.RuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.RuleImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRule()
	 * @generated
	 */
	int RULE = 73;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__LHS = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__ACTION = 1;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.RefArgImpl <em>Ref Arg</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.RefArgImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRefArg()
	 * @generated
	 */
	int REF_ARG = 75;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARG__REF = ARG_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ref Arg</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARG_FEATURE_COUNT = ARG_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ConstantDeclImpl <em>Constant Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ConstantDeclImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstantDecl()
	 * @generated
	 */
	int CONSTANT_DECL = 76;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECL__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECL__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECL__VALUE = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constant Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECL_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ConstantRefImpl <em>Constant Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ConstantRefImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstantRef()
	 * @generated
	 */
	int CONSTANT_REF = 77;

	/**
	 * The feature id for the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_REF__DECL = EXP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_REF_FEATURE_COUNT = EXP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ConstrImpl <em>Constr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ConstrImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstr()
	 * @generated
	 */
	int CONSTR = 79;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR__NAME = LABELED_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR__TYPE = LABELED_TYPE__TYPE;

	/**
	 * The number of structural features of the '<em>Constr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTR_FEATURE_COUNT = LABELED_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.model.MinBound <em>Min Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.model.MinBound
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getMinBound()
	 * @generated
	 */
	int MIN_BOUND = 83;

	/**
	 * The number of structural features of the '<em>Min Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIN_BOUND_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.FiniteBoundImpl <em>Finite Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.FiniteBoundImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getFiniteBound()
	 * @generated
	 */
	int FINITE_BOUND = 81;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINITE_BOUND__STRICT = MIN_BOUND_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Val</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINITE_BOUND__VAL = MIN_BOUND_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Finite Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINITE_BOUND_FEATURE_COUNT = MIN_BOUND_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.model.MaxBound <em>Max Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.model.MaxBound
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getMaxBound()
	 * @generated
	 */
	int MAX_BOUND = 84;

	/**
	 * The number of structural features of the '<em>Max Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_BOUND_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.InfiniteBoundImpl <em>Infinite Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.InfiniteBoundImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInfiniteBound()
	 * @generated
	 */
	int INFINITE_BOUND = 82;

	/**
	 * The number of structural features of the '<em>Infinite Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFINITE_BOUND_FEATURE_COUNT = MAX_BOUND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.CondExpImpl <em>Cond Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.CondExpImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCondExp()
	 * @generated
	 */
	int COND_EXP = 85;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_EXP__COND = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iff</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_EXP__IFF = EXP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ift</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_EXP__IFT = EXP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cond Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_EXP_FEATURE_COUNT = EXP_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.impl.ForeachImpl <em>Foreach</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.impl.ForeachImpl
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getForeach()
	 * @generated
	 */
	int FOREACH = 86;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREACH__BODY = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREACH__ITER = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Foreach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREACH_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.model.Unop <em>Unop</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.model.Unop
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnop()
	 * @generated
	 */
	int UNOP = 87;

	/**
	 * The meta object id for the '{@link org.topcased.fiacre.model.BinOp <em>Bin Op</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.fiacre.model.BinOp
	 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBinOp()
	 * @generated
	 */
	int BIN_OP = 88;

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Program <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Program</em>'.
	 * @see org.topcased.fiacre.model.Program
	 * @generated
	 */
	EClass getProgram();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Program#getDeclarations <em>Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declarations</em>'.
	 * @see org.topcased.fiacre.model.Program#getDeclarations()
	 * @see #getProgram()
	 * @generated
	 */
	EReference getProgram_Declarations();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.Program#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root</em>'.
	 * @see org.topcased.fiacre.model.Program#getRoot()
	 * @see #getProgram()
	 * @generated
	 */
	EReference getProgram_Root();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Declaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration</em>'.
	 * @see org.topcased.fiacre.model.Declaration
	 * @generated
	 */
	EClass getDeclaration();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Declaration#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.Declaration#getName()
	 * @see #getDeclaration()
	 * @generated
	 */
	EAttribute getDeclaration_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.NodeDecl <em>Node Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Decl</em>'.
	 * @see org.topcased.fiacre.model.NodeDecl
	 * @generated
	 */
	EClass getNodeDecl();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.NodeDecl#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.NodeDecl#getArgs()
	 * @see #getNodeDecl()
	 * @generated
	 */
	EReference getNodeDecl_Args();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.NodeDecl#getVars <em>Vars</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vars</em>'.
	 * @see org.topcased.fiacre.model.NodeDecl#getVars()
	 * @see #getNodeDecl()
	 * @generated
	 */
	EReference getNodeDecl_Vars();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.NodeDecl#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ports</em>'.
	 * @see org.topcased.fiacre.model.NodeDecl#getPorts()
	 * @see #getNodeDecl()
	 * @generated
	 */
	EReference getNodeDecl_Ports();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.NodeDecl#getLocalPorts <em>Local Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Local Ports</em>'.
	 * @see org.topcased.fiacre.model.NodeDecl#getLocalPorts()
	 * @see #getNodeDecl()
	 * @generated
	 */
	EReference getNodeDecl_LocalPorts();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.TypeDecl <em>Type Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Decl</em>'.
	 * @see org.topcased.fiacre.model.TypeDecl
	 * @generated
	 */
	EClass getTypeDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.TypeDecl#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see org.topcased.fiacre.model.TypeDecl#getIs()
	 * @see #getTypeDecl()
	 * @generated
	 */
	EReference getTypeDecl_Is();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ChannelDecl <em>Channel Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Channel Decl</em>'.
	 * @see org.topcased.fiacre.model.ChannelDecl
	 * @generated
	 */
	EClass getChannelDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ChannelDecl#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see org.topcased.fiacre.model.ChannelDecl#getIs()
	 * @see #getChannelDecl()
	 * @generated
	 */
	EReference getChannelDecl_Is();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ComponentDecl <em>Component Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Decl</em>'.
	 * @see org.topcased.fiacre.model.ComponentDecl
	 * @generated
	 */
	EClass getComponentDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ComponentDecl#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see org.topcased.fiacre.model.ComponentDecl#getBody()
	 * @see #getComponentDecl()
	 * @generated
	 */
	EReference getComponentDecl_Body();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.ComponentDecl#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Priority</em>'.
	 * @see org.topcased.fiacre.model.ComponentDecl#getPriority()
	 * @see #getComponentDecl()
	 * @generated
	 */
	EReference getComponentDecl_Priority();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ProcessDecl <em>Process Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Decl</em>'.
	 * @see org.topcased.fiacre.model.ProcessDecl
	 * @generated
	 */
	EClass getProcessDecl();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.ProcessDecl#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.topcased.fiacre.model.ProcessDecl#getStates()
	 * @see #getProcessDecl()
	 * @generated
	 */
	EReference getProcessDecl_States();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.ProcessDecl#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see org.topcased.fiacre.model.ProcessDecl#getTransitions()
	 * @see #getProcessDecl()
	 * @generated
	 */
	EReference getProcessDecl_Transitions();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ProcessDecl#getInitAction <em>Init Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Init Action</em>'.
	 * @see org.topcased.fiacre.model.ProcessDecl#getInitAction()
	 * @see #getProcessDecl()
	 * @generated
	 */
	EReference getProcessDecl_InitAction();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.PortDecl <em>Port Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Decl</em>'.
	 * @see org.topcased.fiacre.model.PortDecl
	 * @generated
	 */
	EClass getPortDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.PortDecl#getChannel <em>Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Channel</em>'.
	 * @see org.topcased.fiacre.model.PortDecl#getChannel()
	 * @see #getPortDecl()
	 * @generated
	 */
	EReference getPortDecl_Channel();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.PortDecl#isIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>In</em>'.
	 * @see org.topcased.fiacre.model.PortDecl#isIn()
	 * @see #getPortDecl()
	 * @generated
	 */
	EAttribute getPortDecl_In();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.PortDecl#isOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Out</em>'.
	 * @see org.topcased.fiacre.model.PortDecl#isOut()
	 * @see #getPortDecl()
	 * @generated
	 */
	EAttribute getPortDecl_Out();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.PortDecl#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.PortDecl#getName()
	 * @see #getPortDecl()
	 * @generated
	 */
	EAttribute getPortDecl_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ArgumentVariable <em>Argument Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument Variable</em>'.
	 * @see org.topcased.fiacre.model.ArgumentVariable
	 * @generated
	 */
	EClass getArgumentVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.ArgumentVariable#isRead <em>Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Read</em>'.
	 * @see org.topcased.fiacre.model.ArgumentVariable#isRead()
	 * @see #getArgumentVariable()
	 * @generated
	 */
	EAttribute getArgumentVariable_Read();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.ArgumentVariable#isWrite <em>Write</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Write</em>'.
	 * @see org.topcased.fiacre.model.ArgumentVariable#isWrite()
	 * @see #getArgumentVariable()
	 * @generated
	 */
	EAttribute getArgumentVariable_Write();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.ArgumentVariable#isRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ref</em>'.
	 * @see org.topcased.fiacre.model.ArgumentVariable#isRef()
	 * @see #getArgumentVariable()
	 * @generated
	 */
	EAttribute getArgumentVariable_Ref();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.LocalVariable <em>Local Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Variable</em>'.
	 * @see org.topcased.fiacre.model.LocalVariable
	 * @generated
	 */
	EClass getLocalVariable();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.LocalVariable#getInitializer <em>Initializer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initializer</em>'.
	 * @see org.topcased.fiacre.model.LocalVariable#getInitializer()
	 * @see #getLocalVariable()
	 * @generated
	 */
	EReference getLocalVariable_Initializer();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Composition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composition</em>'.
	 * @see org.topcased.fiacre.model.Composition
	 * @generated
	 */
	EClass getComposition();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Shuffle <em>Shuffle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shuffle</em>'.
	 * @see org.topcased.fiacre.model.Shuffle
	 * @generated
	 */
	EClass getShuffle();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Shuffle#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.Shuffle#getArgs()
	 * @see #getShuffle()
	 * @generated
	 */
	EReference getShuffle_Args();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Sync <em>Sync</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync</em>'.
	 * @see org.topcased.fiacre.model.Sync
	 * @generated
	 */
	EClass getSync();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Sync#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.Sync#getArgs()
	 * @see #getSync()
	 * @generated
	 */
	EReference getSync_Args();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Par <em>Par</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Par</em>'.
	 * @see org.topcased.fiacre.model.Par
	 * @generated
	 */
	EClass getPar();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Par#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.Par#getArgs()
	 * @see #getPar()
	 * @generated
	 */
	EReference getPar_Args();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Instance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance</em>'.
	 * @see org.topcased.fiacre.model.Instance
	 * @generated
	 */
	EClass getInstance();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.Instance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.Instance#getType()
	 * @see #getInstance()
	 * @generated
	 */
	EReference getInstance_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Instance#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.Instance#getArgs()
	 * @see #getInstance()
	 * @generated
	 */
	EReference getInstance_Args();

	/**
	 * Returns the meta object for the reference list '{@link org.topcased.fiacre.model.Instance#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ports</em>'.
	 * @see org.topcased.fiacre.model.Instance#getPorts()
	 * @see #getInstance()
	 * @generated
	 */
	EReference getInstance_Ports();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Instance#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.Instance#getName()
	 * @see #getInstance()
	 * @generated
	 */
	EAttribute getInstance_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InterfacedComp <em>Interfaced Comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interfaced Comp</em>'.
	 * @see org.topcased.fiacre.model.InterfacedComp
	 * @generated
	 */
	EClass getInterfacedComp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.InterfacedComp#getComposition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Composition</em>'.
	 * @see org.topcased.fiacre.model.InterfacedComp#getComposition()
	 * @see #getInterfacedComp()
	 * @generated
	 */
	EReference getInterfacedComp_Composition();

	/**
	 * Returns the meta object for the reference list '{@link org.topcased.fiacre.model.InterfacedComp#getSyncPorts <em>Sync Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sync Ports</em>'.
	 * @see org.topcased.fiacre.model.InterfacedComp#getSyncPorts()
	 * @see #getInterfacedComp()
	 * @generated
	 */
	EReference getInterfacedComp_SyncPorts();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Exp <em>Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exp</em>'.
	 * @see org.topcased.fiacre.model.Exp
	 * @generated
	 */
	EClass getExp();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.topcased.fiacre.model.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.State#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.State#getName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.topcased.fiacre.model.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.Transition#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see org.topcased.fiacre.model.Transition#getFrom()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_From();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Transition#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.topcased.fiacre.model.Transition#getAction()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Action();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Transition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.Transition#getName()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.NullStmt <em>Null Stmt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null Stmt</em>'.
	 * @see org.topcased.fiacre.model.NullStmt
	 * @generated
	 */
	EClass getNullStmt();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see org.topcased.fiacre.model.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Communication <em>Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication</em>'.
	 * @see org.topcased.fiacre.model.Communication
	 * @generated
	 */
	EClass getCommunication();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.Communication#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see org.topcased.fiacre.model.Communication#getPort()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_Port();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.WhileStmt <em>While Stmt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Stmt</em>'.
	 * @see org.topcased.fiacre.model.WhileStmt
	 * @generated
	 */
	EClass getWhileStmt();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.WhileStmt#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.topcased.fiacre.model.WhileStmt#getCondition()
	 * @see #getWhileStmt()
	 * @generated
	 */
	EReference getWhileStmt_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.WhileStmt#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see org.topcased.fiacre.model.WhileStmt#getBody()
	 * @see #getWhileStmt()
	 * @generated
	 */
	EReference getWhileStmt_Body();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.IfStmt <em>If Stmt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Stmt</em>'.
	 * @see org.topcased.fiacre.model.IfStmt
	 * @generated
	 */
	EClass getIfStmt();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.IfStmt#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.topcased.fiacre.model.IfStmt#getCondition()
	 * @see #getIfStmt()
	 * @generated
	 */
	EReference getIfStmt_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.IfStmt#getThen <em>Then</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then</em>'.
	 * @see org.topcased.fiacre.model.IfStmt#getThen()
	 * @see #getIfStmt()
	 * @generated
	 */
	EReference getIfStmt_Then();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.IfStmt#getElse <em>Else</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else</em>'.
	 * @see org.topcased.fiacre.model.IfStmt#getElse()
	 * @see #getIfStmt()
	 * @generated
	 */
	EReference getIfStmt_Else();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Select <em>Select</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Select</em>'.
	 * @see org.topcased.fiacre.model.Select
	 * @generated
	 */
	EClass getSelect();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Select#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see org.topcased.fiacre.model.Select#getStatements()
	 * @see #getSelect()
	 * @generated
	 */
	EReference getSelect_Statements();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.To <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>To</em>'.
	 * @see org.topcased.fiacre.model.To
	 * @generated
	 */
	EClass getTo();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.To#getDest <em>Dest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dest</em>'.
	 * @see org.topcased.fiacre.model.To#getDest()
	 * @see #getTo()
	 * @generated
	 */
	EReference getTo_Dest();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see org.topcased.fiacre.model.Statement
	 * @generated
	 */
	EClass getStatement();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.DeterministicAssignment <em>Deterministic Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deterministic Assignment</em>'.
	 * @see org.topcased.fiacre.model.DeterministicAssignment
	 * @generated
	 */
	EClass getDeterministicAssignment();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.DeterministicAssignment#getAssignments <em>Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assignments</em>'.
	 * @see org.topcased.fiacre.model.DeterministicAssignment#getAssignments()
	 * @see #getDeterministicAssignment()
	 * @generated
	 */
	EReference getDeterministicAssignment_Assignments();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.NonDeterministicAssignment <em>Non Deterministic Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Deterministic Assignment</em>'.
	 * @see org.topcased.fiacre.model.NonDeterministicAssignment
	 * @generated
	 */
	EClass getNonDeterministicAssignment();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.NonDeterministicAssignment#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lhs</em>'.
	 * @see org.topcased.fiacre.model.NonDeterministicAssignment#getLhs()
	 * @see #getNonDeterministicAssignment()
	 * @generated
	 */
	EReference getNonDeterministicAssignment_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.NonDeterministicAssignment#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.topcased.fiacre.model.NonDeterministicAssignment#getCondition()
	 * @see #getNonDeterministicAssignment()
	 * @generated
	 */
	EReference getNonDeterministicAssignment_Condition();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.SingleAssignment <em>Single Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Assignment</em>'.
	 * @see org.topcased.fiacre.model.SingleAssignment
	 * @generated
	 */
	EClass getSingleAssignment();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.SingleAssignment#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see org.topcased.fiacre.model.SingleAssignment#getLhs()
	 * @see #getSingleAssignment()
	 * @generated
	 */
	EReference getSingleAssignment_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.SingleAssignment#getRhs <em>Rhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs</em>'.
	 * @see org.topcased.fiacre.model.SingleAssignment#getRhs()
	 * @see #getSingleAssignment()
	 * @generated
	 */
	EReference getSingleAssignment_Rhs();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.UnExp <em>Un Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Un Exp</em>'.
	 * @see org.topcased.fiacre.model.UnExp
	 * @generated
	 */
	EClass getUnExp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.UnExp#getExp <em>Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Exp</em>'.
	 * @see org.topcased.fiacre.model.UnExp#getExp()
	 * @see #getUnExp()
	 * @generated
	 */
	EReference getUnExp_Exp();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.UnExp#getUnop <em>Unop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unop</em>'.
	 * @see org.topcased.fiacre.model.UnExp#getUnop()
	 * @see #getUnExp()
	 * @generated
	 */
	EAttribute getUnExp_Unop();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.BinExp <em>Bin Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bin Exp</em>'.
	 * @see org.topcased.fiacre.model.BinExp
	 * @generated
	 */
	EClass getBinExp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.BinExp#getR_exp <em>Rexp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rexp</em>'.
	 * @see org.topcased.fiacre.model.BinExp#getR_exp()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_R_exp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.BinExp#getL_exp <em>Lexp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lexp</em>'.
	 * @see org.topcased.fiacre.model.BinExp#getL_exp()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_L_exp();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.BinExp#getBinOp <em>Bin Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bin Op</em>'.
	 * @see org.topcased.fiacre.model.BinExp#getBinOp()
	 * @see #getBinExp()
	 * @generated
	 */
	EAttribute getBinExp_BinOp();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.NatLiteral <em>Nat Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nat Literal</em>'.
	 * @see org.topcased.fiacre.model.NatLiteral
	 * @generated
	 */
	EClass getNatLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.NatLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.topcased.fiacre.model.NatLiteral#getValue()
	 * @see #getNatLiteral()
	 * @generated
	 */
	EAttribute getNatLiteral_Value();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.BoolLiteral <em>Bool Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bool Literal</em>'.
	 * @see org.topcased.fiacre.model.BoolLiteral
	 * @generated
	 */
	EClass getBoolLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.BoolLiteral#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.topcased.fiacre.model.BoolLiteral#isValue()
	 * @see #getBoolLiteral()
	 * @generated
	 */
	EAttribute getBoolLiteral_Value();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.VarRef <em>Var Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Var Ref</em>'.
	 * @see org.topcased.fiacre.model.VarRef
	 * @generated
	 */
	EClass getVarRef();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.VarRef#getDecl <em>Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Decl</em>'.
	 * @see org.topcased.fiacre.model.VarRef#getDecl()
	 * @see #getVarRef()
	 * @generated
	 */
	EReference getVarRef_Decl();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ArrayElem <em>Array Elem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Elem</em>'.
	 * @see org.topcased.fiacre.model.ArrayElem
	 * @generated
	 */
	EClass getArrayElem();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ArrayElem#getArray <em>Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Array</em>'.
	 * @see org.topcased.fiacre.model.ArrayElem#getArray()
	 * @see #getArrayElem()
	 * @generated
	 */
	EReference getArrayElem_Array();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ArrayElem#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Index</em>'.
	 * @see org.topcased.fiacre.model.ArrayElem#getIndex()
	 * @see #getArrayElem()
	 * @generated
	 */
	EReference getArrayElem_Index();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.RecordElem <em>Record Elem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Elem</em>'.
	 * @see org.topcased.fiacre.model.RecordElem
	 * @generated
	 */
	EClass getRecordElem();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.RecordElem#getRecord <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Record</em>'.
	 * @see org.topcased.fiacre.model.RecordElem#getRecord()
	 * @see #getRecordElem()
	 * @generated
	 */
	EReference getRecordElem_Record();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.RecordElem#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field</em>'.
	 * @see org.topcased.fiacre.model.RecordElem#getField()
	 * @see #getRecordElem()
	 * @generated
	 */
	EReference getRecordElem_Field();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InlineQueue <em>Inline Queue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inline Queue</em>'.
	 * @see org.topcased.fiacre.model.InlineQueue
	 * @generated
	 */
	EClass getInlineQueue();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.BasicType <em>Basic Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Type</em>'.
	 * @see org.topcased.fiacre.model.BasicType
	 * @generated
	 */
	EClass getBasicType();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.BoolType <em>Bool Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bool Type</em>'.
	 * @see org.topcased.fiacre.model.BoolType
	 * @generated
	 */
	EClass getBoolType();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.NatType <em>Nat Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nat Type</em>'.
	 * @see org.topcased.fiacre.model.NatType
	 * @generated
	 */
	EClass getNatType();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.IntType <em>Int Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Type</em>'.
	 * @see org.topcased.fiacre.model.IntType
	 * @generated
	 */
	EClass getIntType();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Interval <em>Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interval</em>'.
	 * @see org.topcased.fiacre.model.Interval
	 * @generated
	 */
	EClass getInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Interval#getMini <em>Mini</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mini</em>'.
	 * @see org.topcased.fiacre.model.Interval#getMini()
	 * @see #getInterval()
	 * @generated
	 */
	EAttribute getInterval_Mini();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Interval#getMaxi <em>Maxi</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maxi</em>'.
	 * @see org.topcased.fiacre.model.Interval#getMaxi()
	 * @see #getInterval()
	 * @generated
	 */
	EAttribute getInterval_Maxi();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Record <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record</em>'.
	 * @see org.topcased.fiacre.model.Record
	 * @generated
	 */
	EClass getRecord();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Record#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see org.topcased.fiacre.model.Record#getFields()
	 * @see #getRecord()
	 * @generated
	 */
	EReference getRecord_Fields();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see org.topcased.fiacre.model.Field
	 * @generated
	 */
	EClass getField();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Array <em>Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array</em>'.
	 * @see org.topcased.fiacre.model.Array
	 * @generated
	 */
	EClass getArray();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Array#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.topcased.fiacre.model.Array#getSize()
	 * @see #getArray()
	 * @generated
	 */
	EAttribute getArray_Size();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Array#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.Array#getType()
	 * @see #getArray()
	 * @generated
	 */
	EReference getArray_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Queue <em>Queue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Queue</em>'.
	 * @see org.topcased.fiacre.model.Queue
	 * @generated
	 */
	EClass getQueue();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Queue#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.topcased.fiacre.model.Queue#getSize()
	 * @see #getQueue()
	 * @generated
	 */
	EAttribute getQueue_Size();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Queue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.Queue#getType()
	 * @see #getQueue()
	 * @generated
	 */
	EReference getQueue_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.TypeId <em>Type Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Id</em>'.
	 * @see org.topcased.fiacre.model.TypeId
	 * @generated
	 */
	EClass getTypeId();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.TypeId#getDecl <em>Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Decl</em>'.
	 * @see org.topcased.fiacre.model.TypeId#getDecl()
	 * @see #getTypeId()
	 * @generated
	 */
	EReference getTypeId_Decl();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Channel <em>Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Channel</em>'.
	 * @see org.topcased.fiacre.model.Channel
	 * @generated
	 */
	EClass getChannel();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ChannelId <em>Channel Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Channel Id</em>'.
	 * @see org.topcased.fiacre.model.ChannelId
	 * @generated
	 */
	EClass getChannelId();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.ChannelId#getDecl <em>Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Decl</em>'.
	 * @see org.topcased.fiacre.model.ChannelId#getDecl()
	 * @see #getChannelId()
	 * @generated
	 */
	EReference getChannelId_Decl();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Profile <em>Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Profile</em>'.
	 * @see org.topcased.fiacre.model.Profile
	 * @generated
	 */
	EClass getProfile();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Profile#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see org.topcased.fiacre.model.Profile#getTypes()
	 * @see #getProfile()
	 * @generated
	 */
	EReference getProfile_Types();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.OrChannel <em>Or Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Channel</em>'.
	 * @see org.topcased.fiacre.model.OrChannel
	 * @generated
	 */
	EClass getOrChannel();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.OrChannel#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see org.topcased.fiacre.model.OrChannel#getLhs()
	 * @see #getOrChannel()
	 * @generated
	 */
	EReference getOrChannel_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.OrChannel#getRhs <em>Rhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs</em>'.
	 * @see org.topcased.fiacre.model.OrChannel#getRhs()
	 * @see #getOrChannel()
	 * @generated
	 */
	EReference getOrChannel_Rhs();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InlineArray <em>Inline Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inline Array</em>'.
	 * @see org.topcased.fiacre.model.InlineArray
	 * @generated
	 */
	EClass getInlineArray();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InlineRecord <em>Inline Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inline Record</em>'.
	 * @see org.topcased.fiacre.model.InlineRecord
	 * @generated
	 */
	EClass getInlineRecord();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.InlineRecord#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see org.topcased.fiacre.model.InlineRecord#getValues()
	 * @see #getInlineRecord()
	 * @generated
	 */
	EReference getInlineRecord_Values();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ValuedField <em>Valued Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Valued Field</em>'.
	 * @see org.topcased.fiacre.model.ValuedField
	 * @generated
	 */
	EClass getValuedField();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ValuedField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.topcased.fiacre.model.ValuedField#getValue()
	 * @see #getValuedField()
	 * @generated
	 */
	EReference getValuedField_Value();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.ValuedField#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field</em>'.
	 * @see org.topcased.fiacre.model.ValuedField#getField()
	 * @see #getValuedField()
	 * @generated
	 */
	EReference getValuedField_Field();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Synchronization <em>Synchronization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synchronization</em>'.
	 * @see org.topcased.fiacre.model.Synchronization
	 * @generated
	 */
	EClass getSynchronization();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Reception <em>Reception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reception</em>'.
	 * @see org.topcased.fiacre.model.Reception
	 * @generated
	 */
	EClass getReception();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Reception#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pattern</em>'.
	 * @see org.topcased.fiacre.model.Reception#getPattern()
	 * @see #getReception()
	 * @generated
	 */
	EReference getReception_Pattern();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Reception#getWhere <em>Where</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Where</em>'.
	 * @see org.topcased.fiacre.model.Reception#getWhere()
	 * @see #getReception()
	 * @generated
	 */
	EReference getReception_Where();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Emission <em>Emission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Emission</em>'.
	 * @see org.topcased.fiacre.model.Emission
	 * @generated
	 */
	EClass getEmission();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Emission#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see org.topcased.fiacre.model.Emission#getArgs()
	 * @see #getEmission()
	 * @generated
	 */
	EReference getEmission_Args();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Seq <em>Seq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq</em>'.
	 * @see org.topcased.fiacre.model.Seq
	 * @generated
	 */
	EClass getSeq();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Seq#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see org.topcased.fiacre.model.Seq#getStatements()
	 * @see #getSeq()
	 * @generated
	 */
	EReference getSeq_Statements();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.LocalPortDecl <em>Local Port Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Port Decl</em>'.
	 * @see org.topcased.fiacre.model.LocalPortDecl
	 * @generated
	 */
	EClass getLocalPortDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.LocalPortDecl#getMini <em>Mini</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mini</em>'.
	 * @see org.topcased.fiacre.model.LocalPortDecl#getMini()
	 * @see #getLocalPortDecl()
	 * @generated
	 */
	EReference getLocalPortDecl_Mini();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.LocalPortDecl#getMaxi <em>Maxi</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Maxi</em>'.
	 * @see org.topcased.fiacre.model.LocalPortDecl#getMaxi()
	 * @see #getLocalPortDecl()
	 * @generated
	 */
	EReference getLocalPortDecl_Maxi();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ParamPortDecl <em>Param Port Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Param Port Decl</em>'.
	 * @see org.topcased.fiacre.model.ParamPortDecl
	 * @generated
	 */
	EClass getParamPortDecl();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.topcased.fiacre.model.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.Variable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.Variable#getName()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Variable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.Variable#getType()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Priority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Priority</em>'.
	 * @see org.topcased.fiacre.model.Priority
	 * @generated
	 */
	EClass getPriority();

	/**
	 * Returns the meta object for the reference list '{@link org.topcased.fiacre.model.Priority#getInf <em>Inf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Inf</em>'.
	 * @see org.topcased.fiacre.model.Priority#getInf()
	 * @see #getPriority()
	 * @generated
	 */
	EReference getPriority_Inf();

	/**
	 * Returns the meta object for the reference list '{@link org.topcased.fiacre.model.Priority#getSup <em>Sup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sup</em>'.
	 * @see org.topcased.fiacre.model.Priority#getSup()
	 * @see #getPriority()
	 * @generated
	 */
	EReference getPriority_Sup();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ConstrExp <em>Constr Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constr Exp</em>'.
	 * @see org.topcased.fiacre.model.ConstrExp
	 * @generated
	 */
	EClass getConstrExp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ConstrExp#getArg <em>Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Arg</em>'.
	 * @see org.topcased.fiacre.model.ConstrExp#getArg()
	 * @see #getConstrExp()
	 * @generated
	 */
	EReference getConstrExp_Arg();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.ConstrExp#getConstr <em>Constr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constr</em>'.
	 * @see org.topcased.fiacre.model.ConstrExp#getConstr()
	 * @see #getConstrExp()
	 * @generated
	 */
	EReference getConstrExp_Constr();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Union <em>Union</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Union</em>'.
	 * @see org.topcased.fiacre.model.Union
	 * @generated
	 */
	EClass getUnion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.Union#getConstr <em>Constr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constr</em>'.
	 * @see org.topcased.fiacre.model.Union#getConstr()
	 * @see #getUnion()
	 * @generated
	 */
	EReference getUnion_Constr();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Pattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern</em>'.
	 * @see org.topcased.fiacre.model.Pattern
	 * @generated
	 */
	EClass getPattern();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ConstrPattern <em>Constr Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constr Pattern</em>'.
	 * @see org.topcased.fiacre.model.ConstrPattern
	 * @generated
	 */
	EClass getConstrPattern();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ConstrPattern#getArg <em>Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Arg</em>'.
	 * @see org.topcased.fiacre.model.ConstrPattern#getArg()
	 * @see #getConstrPattern()
	 * @generated
	 */
	EReference getConstrPattern_Arg();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.ConstrPattern#getConstr <em>Constr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constr</em>'.
	 * @see org.topcased.fiacre.model.ConstrPattern#getConstr()
	 * @see #getConstrPattern()
	 * @generated
	 */
	EReference getConstrPattern_Constr();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ArrayPattern <em>Array Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Pattern</em>'.
	 * @see org.topcased.fiacre.model.ArrayPattern
	 * @generated
	 */
	EClass getArrayPattern();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ArrayPattern#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Index</em>'.
	 * @see org.topcased.fiacre.model.ArrayPattern#getIndex()
	 * @see #getArrayPattern()
	 * @generated
	 */
	EReference getArrayPattern_Index();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ArrayPattern#getArray <em>Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Array</em>'.
	 * @see org.topcased.fiacre.model.ArrayPattern#getArray()
	 * @see #getArrayPattern()
	 * @generated
	 */
	EReference getArrayPattern_Array();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.FieldPattern <em>Field Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Pattern</em>'.
	 * @see org.topcased.fiacre.model.FieldPattern
	 * @generated
	 */
	EClass getFieldPattern();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.FieldPattern#getRecord <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Record</em>'.
	 * @see org.topcased.fiacre.model.FieldPattern#getRecord()
	 * @see #getFieldPattern()
	 * @generated
	 */
	EReference getFieldPattern_Record();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.FieldPattern#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field</em>'.
	 * @see org.topcased.fiacre.model.FieldPattern#getField()
	 * @see #getFieldPattern()
	 * @generated
	 */
	EReference getFieldPattern_Field();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.AnyPattern <em>Any Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Any Pattern</em>'.
	 * @see org.topcased.fiacre.model.AnyPattern
	 * @generated
	 */
	EClass getAnyPattern();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Literal <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal</em>'.
	 * @see org.topcased.fiacre.model.Literal
	 * @generated
	 */
	EClass getLiteral();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.CaseStmt <em>Case Stmt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Stmt</em>'.
	 * @see org.topcased.fiacre.model.CaseStmt
	 * @generated
	 */
	EClass getCaseStmt();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.CaseStmt#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rules</em>'.
	 * @see org.topcased.fiacre.model.CaseStmt#getRules()
	 * @see #getCaseStmt()
	 * @generated
	 */
	EReference getCaseStmt_Rules();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.CaseStmt#getExp <em>Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Exp</em>'.
	 * @see org.topcased.fiacre.model.CaseStmt#getExp()
	 * @see #getCaseStmt()
	 * @generated
	 */
	EReference getCaseStmt_Exp();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see org.topcased.fiacre.model.Rule
	 * @generated
	 */
	EClass getRule();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Rule#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see org.topcased.fiacre.model.Rule#getLhs()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Rule#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.topcased.fiacre.model.Rule#getAction()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Action();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Arg <em>Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arg</em>'.
	 * @see org.topcased.fiacre.model.Arg
	 * @generated
	 */
	EClass getArg();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.RefArg <em>Ref Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Arg</em>'.
	 * @see org.topcased.fiacre.model.RefArg
	 * @generated
	 */
	EClass getRefArg();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.RefArg#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see org.topcased.fiacre.model.RefArg#getRef()
	 * @see #getRefArg()
	 * @generated
	 */
	EReference getRefArg_Ref();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ConstantDecl <em>Constant Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Decl</em>'.
	 * @see org.topcased.fiacre.model.ConstantDecl
	 * @generated
	 */
	EClass getConstantDecl();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ConstantDecl#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.ConstantDecl#getType()
	 * @see #getConstantDecl()
	 * @generated
	 */
	EReference getConstantDecl_Type();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.ConstantDecl#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.topcased.fiacre.model.ConstantDecl#getValue()
	 * @see #getConstantDecl()
	 * @generated
	 */
	EReference getConstantDecl_Value();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.ConstantRef <em>Constant Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Ref</em>'.
	 * @see org.topcased.fiacre.model.ConstantRef
	 * @generated
	 */
	EClass getConstantRef();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.ConstantRef#getDecl <em>Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Decl</em>'.
	 * @see org.topcased.fiacre.model.ConstantRef#getDecl()
	 * @see #getConstantRef()
	 * @generated
	 */
	EReference getConstantRef_Decl();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InlineCollection <em>Inline Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inline Collection</em>'.
	 * @see org.topcased.fiacre.model.InlineCollection
	 * @generated
	 */
	EClass getInlineCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.fiacre.model.InlineCollection#getElems <em>Elems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elems</em>'.
	 * @see org.topcased.fiacre.model.InlineCollection#getElems()
	 * @see #getInlineCollection()
	 * @generated
	 */
	EReference getInlineCollection_Elems();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Constr <em>Constr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constr</em>'.
	 * @see org.topcased.fiacre.model.Constr
	 * @generated
	 */
	EClass getConstr();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.LabeledType <em>Labeled Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Labeled Type</em>'.
	 * @see org.topcased.fiacre.model.LabeledType
	 * @generated
	 */
	EClass getLabeledType();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.LabeledType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.fiacre.model.LabeledType#getName()
	 * @see #getLabeledType()
	 * @generated
	 */
	EAttribute getLabeledType_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.LabeledType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.topcased.fiacre.model.LabeledType#getType()
	 * @see #getLabeledType()
	 * @generated
	 */
	EReference getLabeledType_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.FiniteBound <em>Finite Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Finite Bound</em>'.
	 * @see org.topcased.fiacre.model.FiniteBound
	 * @generated
	 */
	EClass getFiniteBound();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.FiniteBound#isStrict <em>Strict</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strict</em>'.
	 * @see org.topcased.fiacre.model.FiniteBound#isStrict()
	 * @see #getFiniteBound()
	 * @generated
	 */
	EAttribute getFiniteBound_Strict();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.fiacre.model.FiniteBound#getVal <em>Val</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Val</em>'.
	 * @see org.topcased.fiacre.model.FiniteBound#getVal()
	 * @see #getFiniteBound()
	 * @generated
	 */
	EAttribute getFiniteBound_Val();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.InfiniteBound <em>Infinite Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Infinite Bound</em>'.
	 * @see org.topcased.fiacre.model.InfiniteBound
	 * @generated
	 */
	EClass getInfiniteBound();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.MinBound <em>Min Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Min Bound</em>'.
	 * @see org.topcased.fiacre.model.MinBound
	 * @generated
	 */
	EClass getMinBound();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.MaxBound <em>Max Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Max Bound</em>'.
	 * @see org.topcased.fiacre.model.MaxBound
	 * @generated
	 */
	EClass getMaxBound();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.CondExp <em>Cond Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cond Exp</em>'.
	 * @see org.topcased.fiacre.model.CondExp
	 * @generated
	 */
	EClass getCondExp();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.CondExp#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see org.topcased.fiacre.model.CondExp#getCond()
	 * @see #getCondExp()
	 * @generated
	 */
	EReference getCondExp_Cond();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.CondExp#getIff <em>Iff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iff</em>'.
	 * @see org.topcased.fiacre.model.CondExp#getIff()
	 * @see #getCondExp()
	 * @generated
	 */
	EReference getCondExp_Iff();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.CondExp#getIft <em>Ift</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ift</em>'.
	 * @see org.topcased.fiacre.model.CondExp#getIft()
	 * @see #getCondExp()
	 * @generated
	 */
	EReference getCondExp_Ift();

	/**
	 * Returns the meta object for class '{@link org.topcased.fiacre.model.Foreach <em>Foreach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Foreach</em>'.
	 * @see org.topcased.fiacre.model.Foreach
	 * @generated
	 */
	EClass getForeach();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.fiacre.model.Foreach#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see org.topcased.fiacre.model.Foreach#getBody()
	 * @see #getForeach()
	 * @generated
	 */
	EReference getForeach_Body();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.fiacre.model.Foreach#getIter <em>Iter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iter</em>'.
	 * @see org.topcased.fiacre.model.Foreach#getIter()
	 * @see #getForeach()
	 * @generated
	 */
	EReference getForeach_Iter();

	/**
	 * Returns the meta object for enum '{@link org.topcased.fiacre.model.Unop <em>Unop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unop</em>'.
	 * @see org.topcased.fiacre.model.Unop
	 * @generated
	 */
	EEnum getUnop();

	/**
	 * Returns the meta object for enum '{@link org.topcased.fiacre.model.BinOp <em>Bin Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Bin Op</em>'.
	 * @see org.topcased.fiacre.model.BinOp
	 * @generated
	 */
	EEnum getBinOp();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FiacreFactory getFiacreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ProgramImpl <em>Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ProgramImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProgram()
		 * @generated
		 */
		EClass PROGRAM = eINSTANCE.getProgram();

		/**
		 * The meta object literal for the '<em><b>Declarations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAM__DECLARATIONS = eINSTANCE.getProgram_Declarations();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAM__ROOT = eINSTANCE.getProgram_Root();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.DeclarationImpl <em>Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.DeclarationImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getDeclaration()
		 * @generated
		 */
		EClass DECLARATION = eINSTANCE.getDeclaration();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECLARATION__NAME = eINSTANCE.getDeclaration_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.NodeDeclImpl <em>Node Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.NodeDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNodeDecl()
		 * @generated
		 */
		EClass NODE_DECL = eINSTANCE.getNodeDecl();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_DECL__ARGS = eINSTANCE.getNodeDecl_Args();

		/**
		 * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_DECL__VARS = eINSTANCE.getNodeDecl_Vars();

		/**
		 * The meta object literal for the '<em><b>Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_DECL__PORTS = eINSTANCE.getNodeDecl_Ports();

		/**
		 * The meta object literal for the '<em><b>Local Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_DECL__LOCAL_PORTS = eINSTANCE.getNodeDecl_LocalPorts();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.TypeDeclImpl <em>Type Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.TypeDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTypeDecl()
		 * @generated
		 */
		EClass TYPE_DECL = eINSTANCE.getTypeDecl();

		/**
		 * The meta object literal for the '<em><b>Is</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_DECL__IS = eINSTANCE.getTypeDecl_Is();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ChannelDeclImpl <em>Channel Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ChannelDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannelDecl()
		 * @generated
		 */
		EClass CHANNEL_DECL = eINSTANCE.getChannelDecl();

		/**
		 * The meta object literal for the '<em><b>Is</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANNEL_DECL__IS = eINSTANCE.getChannelDecl_Is();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ComponentDeclImpl <em>Component Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ComponentDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getComponentDecl()
		 * @generated
		 */
		EClass COMPONENT_DECL = eINSTANCE.getComponentDecl();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_DECL__BODY = eINSTANCE.getComponentDecl_Body();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_DECL__PRIORITY = eINSTANCE
				.getComponentDecl_Priority();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ProcessDeclImpl <em>Process Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ProcessDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProcessDecl()
		 * @generated
		 */
		EClass PROCESS_DECL = eINSTANCE.getProcessDecl();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_DECL__STATES = eINSTANCE.getProcessDecl_States();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_DECL__TRANSITIONS = eINSTANCE
				.getProcessDecl_Transitions();

		/**
		 * The meta object literal for the '<em><b>Init Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_DECL__INIT_ACTION = eINSTANCE
				.getProcessDecl_InitAction();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.PortDeclImpl <em>Port Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.PortDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPortDecl()
		 * @generated
		 */
		EClass PORT_DECL = eINSTANCE.getPortDecl();

		/**
		 * The meta object literal for the '<em><b>Channel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DECL__CHANNEL = eINSTANCE.getPortDecl_Channel();

		/**
		 * The meta object literal for the '<em><b>In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DECL__IN = eINSTANCE.getPortDecl_In();

		/**
		 * The meta object literal for the '<em><b>Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DECL__OUT = eINSTANCE.getPortDecl_Out();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DECL__NAME = eINSTANCE.getPortDecl_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ArgumentVariableImpl <em>Argument Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ArgumentVariableImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArgumentVariable()
		 * @generated
		 */
		EClass ARGUMENT_VARIABLE = eINSTANCE.getArgumentVariable();

		/**
		 * The meta object literal for the '<em><b>Read</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_VARIABLE__READ = eINSTANCE
				.getArgumentVariable_Read();

		/**
		 * The meta object literal for the '<em><b>Write</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_VARIABLE__WRITE = eINSTANCE
				.getArgumentVariable_Write();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARGUMENT_VARIABLE__REF = eINSTANCE.getArgumentVariable_Ref();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.LocalVariableImpl <em>Local Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.LocalVariableImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLocalVariable()
		 * @generated
		 */
		EClass LOCAL_VARIABLE = eINSTANCE.getLocalVariable();

		/**
		 * The meta object literal for the '<em><b>Initializer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCAL_VARIABLE__INITIALIZER = eINSTANCE
				.getLocalVariable_Initializer();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.CompositionImpl <em>Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.CompositionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getComposition()
		 * @generated
		 */
		EClass COMPOSITION = eINSTANCE.getComposition();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ShuffleImpl <em>Shuffle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ShuffleImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getShuffle()
		 * @generated
		 */
		EClass SHUFFLE = eINSTANCE.getShuffle();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHUFFLE__ARGS = eINSTANCE.getShuffle_Args();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.SyncImpl <em>Sync</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.SyncImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSync()
		 * @generated
		 */
		EClass SYNC = eINSTANCE.getSync();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC__ARGS = eINSTANCE.getSync_Args();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ParImpl <em>Par</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ParImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPar()
		 * @generated
		 */
		EClass PAR = eINSTANCE.getPar();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAR__ARGS = eINSTANCE.getPar_Args();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InstanceImpl <em>Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InstanceImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInstance()
		 * @generated
		 */
		EClass INSTANCE = eINSTANCE.getInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTANCE__TYPE = eINSTANCE.getInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTANCE__ARGS = eINSTANCE.getInstance_Args();

		/**
		 * The meta object literal for the '<em><b>Ports</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTANCE__PORTS = eINSTANCE.getInstance_Ports();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTANCE__NAME = eINSTANCE.getInstance_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InterfacedCompImpl <em>Interfaced Comp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InterfacedCompImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInterfacedComp()
		 * @generated
		 */
		EClass INTERFACED_COMP = eINSTANCE.getInterfacedComp();

		/**
		 * The meta object literal for the '<em><b>Composition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_COMP__COMPOSITION = eINSTANCE
				.getInterfacedComp_Composition();

		/**
		 * The meta object literal for the '<em><b>Sync Ports</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACED_COMP__SYNC_PORTS = eINSTANCE
				.getInterfacedComp_SyncPorts();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ExpImpl <em>Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ExpImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getExp()
		 * @generated
		 */
		EClass EXP = eINSTANCE.getExp();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.StateImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__NAME = eINSTANCE.getState_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.TransitionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__FROM = eINSTANCE.getTransition_From();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ACTION = eINSTANCE.getTransition_Action();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__NAME = eINSTANCE.getTransition_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.NullStmtImpl <em>Null Stmt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.NullStmtImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNullStmt()
		 * @generated
		 */
		EClass NULL_STMT = eINSTANCE.getNullStmt();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.AssignmentImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.CommunicationImpl <em>Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.CommunicationImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCommunication()
		 * @generated
		 */
		EClass COMMUNICATION = eINSTANCE.getCommunication();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__PORT = eINSTANCE.getCommunication_Port();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.WhileStmtImpl <em>While Stmt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.WhileStmtImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getWhileStmt()
		 * @generated
		 */
		EClass WHILE_STMT = eINSTANCE.getWhileStmt();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_STMT__CONDITION = eINSTANCE.getWhileStmt_Condition();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_STMT__BODY = eINSTANCE.getWhileStmt_Body();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.IfStmtImpl <em>If Stmt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.IfStmtImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getIfStmt()
		 * @generated
		 */
		EClass IF_STMT = eINSTANCE.getIfStmt();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STMT__CONDITION = eINSTANCE.getIfStmt_Condition();

		/**
		 * The meta object literal for the '<em><b>Then</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STMT__THEN = eINSTANCE.getIfStmt_Then();

		/**
		 * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STMT__ELSE = eINSTANCE.getIfStmt_Else();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.SelectImpl <em>Select</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.SelectImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSelect()
		 * @generated
		 */
		EClass SELECT = eINSTANCE.getSelect();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELECT__STATEMENTS = eINSTANCE.getSelect_Statements();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ToImpl <em>To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ToImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTo()
		 * @generated
		 */
		EClass TO = eINSTANCE.getTo();

		/**
		 * The meta object literal for the '<em><b>Dest</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TO__DEST = eINSTANCE.getTo_Dest();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.StatementImpl <em>Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.StatementImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getStatement()
		 * @generated
		 */
		EClass STATEMENT = eINSTANCE.getStatement();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.DeterministicAssignmentImpl <em>Deterministic Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.DeterministicAssignmentImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getDeterministicAssignment()
		 * @generated
		 */
		EClass DETERMINISTIC_ASSIGNMENT = eINSTANCE
				.getDeterministicAssignment();

		/**
		 * The meta object literal for the '<em><b>Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DETERMINISTIC_ASSIGNMENT__ASSIGNMENTS = eINSTANCE
				.getDeterministicAssignment_Assignments();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.NonDeterministicAssignmentImpl <em>Non Deterministic Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.NonDeterministicAssignmentImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNonDeterministicAssignment()
		 * @generated
		 */
		EClass NON_DETERMINISTIC_ASSIGNMENT = eINSTANCE
				.getNonDeterministicAssignment();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_DETERMINISTIC_ASSIGNMENT__LHS = eINSTANCE
				.getNonDeterministicAssignment_Lhs();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_DETERMINISTIC_ASSIGNMENT__CONDITION = eINSTANCE
				.getNonDeterministicAssignment_Condition();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.SingleAssignmentImpl <em>Single Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.SingleAssignmentImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSingleAssignment()
		 * @generated
		 */
		EClass SINGLE_ASSIGNMENT = eINSTANCE.getSingleAssignment();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SINGLE_ASSIGNMENT__LHS = eINSTANCE.getSingleAssignment_Lhs();

		/**
		 * The meta object literal for the '<em><b>Rhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SINGLE_ASSIGNMENT__RHS = eINSTANCE.getSingleAssignment_Rhs();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.UnExpImpl <em>Un Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.UnExpImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnExp()
		 * @generated
		 */
		EClass UN_EXP = eINSTANCE.getUnExp();

		/**
		 * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UN_EXP__EXP = eINSTANCE.getUnExp_Exp();

		/**
		 * The meta object literal for the '<em><b>Unop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UN_EXP__UNOP = eINSTANCE.getUnExp_Unop();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.BinExpImpl <em>Bin Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.BinExpImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBinExp()
		 * @generated
		 */
		EClass BIN_EXP = eINSTANCE.getBinExp();

		/**
		 * The meta object literal for the '<em><b>Rexp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__REXP = eINSTANCE.getBinExp_R_exp();

		/**
		 * The meta object literal for the '<em><b>Lexp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__LEXP = eINSTANCE.getBinExp_L_exp();

		/**
		 * The meta object literal for the '<em><b>Bin Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BIN_EXP__BIN_OP = eINSTANCE.getBinExp_BinOp();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.NatLiteralImpl <em>Nat Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.NatLiteralImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNatLiteral()
		 * @generated
		 */
		EClass NAT_LITERAL = eINSTANCE.getNatLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAT_LITERAL__VALUE = eINSTANCE.getNatLiteral_Value();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.BoolLiteralImpl <em>Bool Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.BoolLiteralImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBoolLiteral()
		 * @generated
		 */
		EClass BOOL_LITERAL = eINSTANCE.getBoolLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOL_LITERAL__VALUE = eINSTANCE.getBoolLiteral_Value();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.VarRefImpl <em>Var Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.VarRefImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getVarRef()
		 * @generated
		 */
		EClass VAR_REF = eINSTANCE.getVarRef();

		/**
		 * The meta object literal for the '<em><b>Decl</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VAR_REF__DECL = eINSTANCE.getVarRef_Decl();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ArrayElemImpl <em>Array Elem</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ArrayElemImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArrayElem()
		 * @generated
		 */
		EClass ARRAY_ELEM = eINSTANCE.getArrayElem();

		/**
		 * The meta object literal for the '<em><b>Array</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_ELEM__ARRAY = eINSTANCE.getArrayElem_Array();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_ELEM__INDEX = eINSTANCE.getArrayElem_Index();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.RecordElemImpl <em>Record Elem</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.RecordElemImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRecordElem()
		 * @generated
		 */
		EClass RECORD_ELEM = eINSTANCE.getRecordElem();

		/**
		 * The meta object literal for the '<em><b>Record</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_ELEM__RECORD = eINSTANCE.getRecordElem_Record();

		/**
		 * The meta object literal for the '<em><b>Field</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_ELEM__FIELD = eINSTANCE.getRecordElem_Field();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InlineQueueImpl <em>Inline Queue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InlineQueueImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineQueue()
		 * @generated
		 */
		EClass INLINE_QUEUE = eINSTANCE.getInlineQueue();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.TypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.BasicTypeImpl <em>Basic Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.BasicTypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBasicType()
		 * @generated
		 */
		EClass BASIC_TYPE = eINSTANCE.getBasicType();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.BoolTypeImpl <em>Bool Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.BoolTypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBoolType()
		 * @generated
		 */
		EClass BOOL_TYPE = eINSTANCE.getBoolType();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.NatTypeImpl <em>Nat Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.NatTypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getNatType()
		 * @generated
		 */
		EClass NAT_TYPE = eINSTANCE.getNatType();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.IntTypeImpl <em>Int Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.IntTypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getIntType()
		 * @generated
		 */
		EClass INT_TYPE = eINSTANCE.getIntType();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.IntervalImpl <em>Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.IntervalImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInterval()
		 * @generated
		 */
		EClass INTERVAL = eINSTANCE.getInterval();

		/**
		 * The meta object literal for the '<em><b>Mini</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERVAL__MINI = eINSTANCE.getInterval_Mini();

		/**
		 * The meta object literal for the '<em><b>Maxi</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERVAL__MAXI = eINSTANCE.getInterval_Maxi();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.RecordImpl <em>Record</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.RecordImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRecord()
		 * @generated
		 */
		EClass RECORD = eINSTANCE.getRecord();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD__FIELDS = eINSTANCE.getRecord_Fields();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.FieldImpl <em>Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.FieldImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getField()
		 * @generated
		 */
		EClass FIELD = eINSTANCE.getField();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ArrayImpl <em>Array</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ArrayImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArray()
		 * @generated
		 */
		EClass ARRAY = eINSTANCE.getArray();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY__SIZE = eINSTANCE.getArray_Size();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY__TYPE = eINSTANCE.getArray_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.QueueImpl <em>Queue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.QueueImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getQueue()
		 * @generated
		 */
		EClass QUEUE = eINSTANCE.getQueue();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEUE__SIZE = eINSTANCE.getQueue_Size();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUEUE__TYPE = eINSTANCE.getQueue_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.TypeIdImpl <em>Type Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.TypeIdImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getTypeId()
		 * @generated
		 */
		EClass TYPE_ID = eINSTANCE.getTypeId();

		/**
		 * The meta object literal for the '<em><b>Decl</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_ID__DECL = eINSTANCE.getTypeId_Decl();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ChannelImpl <em>Channel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ChannelImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannel()
		 * @generated
		 */
		EClass CHANNEL = eINSTANCE.getChannel();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ChannelIdImpl <em>Channel Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ChannelIdImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getChannelId()
		 * @generated
		 */
		EClass CHANNEL_ID = eINSTANCE.getChannelId();

		/**
		 * The meta object literal for the '<em><b>Decl</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANNEL_ID__DECL = eINSTANCE.getChannelId_Decl();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ProfileImpl <em>Profile</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ProfileImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getProfile()
		 * @generated
		 */
		EClass PROFILE = eINSTANCE.getProfile();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROFILE__TYPES = eINSTANCE.getProfile_Types();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.OrChannelImpl <em>Or Channel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.OrChannelImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getOrChannel()
		 * @generated
		 */
		EClass OR_CHANNEL = eINSTANCE.getOrChannel();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_CHANNEL__LHS = eINSTANCE.getOrChannel_Lhs();

		/**
		 * The meta object literal for the '<em><b>Rhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_CHANNEL__RHS = eINSTANCE.getOrChannel_Rhs();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InlineArrayImpl <em>Inline Array</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InlineArrayImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineArray()
		 * @generated
		 */
		EClass INLINE_ARRAY = eINSTANCE.getInlineArray();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InlineRecordImpl <em>Inline Record</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InlineRecordImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineRecord()
		 * @generated
		 */
		EClass INLINE_RECORD = eINSTANCE.getInlineRecord();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INLINE_RECORD__VALUES = eINSTANCE.getInlineRecord_Values();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ValuedFieldImpl <em>Valued Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ValuedFieldImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getValuedField()
		 * @generated
		 */
		EClass VALUED_FIELD = eINSTANCE.getValuedField();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUED_FIELD__VALUE = eINSTANCE.getValuedField_Value();

		/**
		 * The meta object literal for the '<em><b>Field</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUED_FIELD__FIELD = eINSTANCE.getValuedField_Field();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.SynchronizationImpl <em>Synchronization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.SynchronizationImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSynchronization()
		 * @generated
		 */
		EClass SYNCHRONIZATION = eINSTANCE.getSynchronization();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ReceptionImpl <em>Reception</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ReceptionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getReception()
		 * @generated
		 */
		EClass RECEPTION = eINSTANCE.getReception();

		/**
		 * The meta object literal for the '<em><b>Pattern</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTION__PATTERN = eINSTANCE.getReception_Pattern();

		/**
		 * The meta object literal for the '<em><b>Where</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTION__WHERE = eINSTANCE.getReception_Where();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.EmissionImpl <em>Emission</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.EmissionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getEmission()
		 * @generated
		 */
		EClass EMISSION = eINSTANCE.getEmission();

		/**
		 * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMISSION__ARGS = eINSTANCE.getEmission_Args();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.SeqImpl <em>Seq</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.SeqImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getSeq()
		 * @generated
		 */
		EClass SEQ = eINSTANCE.getSeq();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQ__STATEMENTS = eINSTANCE.getSeq_Statements();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.LocalPortDeclImpl <em>Local Port Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.LocalPortDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLocalPortDecl()
		 * @generated
		 */
		EClass LOCAL_PORT_DECL = eINSTANCE.getLocalPortDecl();

		/**
		 * The meta object literal for the '<em><b>Mini</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCAL_PORT_DECL__MINI = eINSTANCE.getLocalPortDecl_Mini();

		/**
		 * The meta object literal for the '<em><b>Maxi</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCAL_PORT_DECL__MAXI = eINSTANCE.getLocalPortDecl_Maxi();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ParamPortDeclImpl <em>Param Port Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ParamPortDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getParamPortDecl()
		 * @generated
		 */
		EClass PARAM_PORT_DECL = eINSTANCE.getParamPortDecl();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.VariableImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__TYPE = eINSTANCE.getVariable_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.PriorityImpl <em>Priority</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.PriorityImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPriority()
		 * @generated
		 */
		EClass PRIORITY = eINSTANCE.getPriority();

		/**
		 * The meta object literal for the '<em><b>Inf</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY__INF = eINSTANCE.getPriority_Inf();

		/**
		 * The meta object literal for the '<em><b>Sup</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY__SUP = eINSTANCE.getPriority_Sup();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ConstrExpImpl <em>Constr Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ConstrExpImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstrExp()
		 * @generated
		 */
		EClass CONSTR_EXP = eINSTANCE.getConstrExp();

		/**
		 * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTR_EXP__ARG = eINSTANCE.getConstrExp_Arg();

		/**
		 * The meta object literal for the '<em><b>Constr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTR_EXP__CONSTR = eINSTANCE.getConstrExp_Constr();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.UnionImpl <em>Union</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.UnionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnion()
		 * @generated
		 */
		EClass UNION = eINSTANCE.getUnion();

		/**
		 * The meta object literal for the '<em><b>Constr</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNION__CONSTR = eINSTANCE.getUnion_Constr();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.PatternImpl <em>Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.PatternImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getPattern()
		 * @generated
		 */
		EClass PATTERN = eINSTANCE.getPattern();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ConstrPatternImpl <em>Constr Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ConstrPatternImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstrPattern()
		 * @generated
		 */
		EClass CONSTR_PATTERN = eINSTANCE.getConstrPattern();

		/**
		 * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTR_PATTERN__ARG = eINSTANCE.getConstrPattern_Arg();

		/**
		 * The meta object literal for the '<em><b>Constr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTR_PATTERN__CONSTR = eINSTANCE.getConstrPattern_Constr();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ArrayPatternImpl <em>Array Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ArrayPatternImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArrayPattern()
		 * @generated
		 */
		EClass ARRAY_PATTERN = eINSTANCE.getArrayPattern();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_PATTERN__INDEX = eINSTANCE.getArrayPattern_Index();

		/**
		 * The meta object literal for the '<em><b>Array</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_PATTERN__ARRAY = eINSTANCE.getArrayPattern_Array();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.FieldPatternImpl <em>Field Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.FieldPatternImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getFieldPattern()
		 * @generated
		 */
		EClass FIELD_PATTERN = eINSTANCE.getFieldPattern();

		/**
		 * The meta object literal for the '<em><b>Record</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_PATTERN__RECORD = eINSTANCE.getFieldPattern_Record();

		/**
		 * The meta object literal for the '<em><b>Field</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_PATTERN__FIELD = eINSTANCE.getFieldPattern_Field();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.AnyPatternImpl <em>Any Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.AnyPatternImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getAnyPattern()
		 * @generated
		 */
		EClass ANY_PATTERN = eINSTANCE.getAnyPattern();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.LiteralImpl <em>Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.LiteralImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLiteral()
		 * @generated
		 */
		EClass LITERAL = eINSTANCE.getLiteral();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.CaseStmtImpl <em>Case Stmt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.CaseStmtImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCaseStmt()
		 * @generated
		 */
		EClass CASE_STMT = eINSTANCE.getCaseStmt();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_STMT__RULES = eINSTANCE.getCaseStmt_Rules();

		/**
		 * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_STMT__EXP = eINSTANCE.getCaseStmt_Exp();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.RuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.RuleImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRule()
		 * @generated
		 */
		EClass RULE = eINSTANCE.getRule();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__LHS = eINSTANCE.getRule_Lhs();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__ACTION = eINSTANCE.getRule_Action();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ArgImpl <em>Arg</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ArgImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getArg()
		 * @generated
		 */
		EClass ARG = eINSTANCE.getArg();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.RefArgImpl <em>Ref Arg</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.RefArgImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getRefArg()
		 * @generated
		 */
		EClass REF_ARG = eINSTANCE.getRefArg();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARG__REF = eINSTANCE.getRefArg_Ref();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ConstantDeclImpl <em>Constant Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ConstantDeclImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstantDecl()
		 * @generated
		 */
		EClass CONSTANT_DECL = eINSTANCE.getConstantDecl();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_DECL__TYPE = eINSTANCE.getConstantDecl_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_DECL__VALUE = eINSTANCE.getConstantDecl_Value();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ConstantRefImpl <em>Constant Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ConstantRefImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstantRef()
		 * @generated
		 */
		EClass CONSTANT_REF = eINSTANCE.getConstantRef();

		/**
		 * The meta object literal for the '<em><b>Decl</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_REF__DECL = eINSTANCE.getConstantRef_Decl();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InlineCollectionImpl <em>Inline Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InlineCollectionImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInlineCollection()
		 * @generated
		 */
		EClass INLINE_COLLECTION = eINSTANCE.getInlineCollection();

		/**
		 * The meta object literal for the '<em><b>Elems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INLINE_COLLECTION__ELEMS = eINSTANCE
				.getInlineCollection_Elems();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ConstrImpl <em>Constr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ConstrImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getConstr()
		 * @generated
		 */
		EClass CONSTR = eINSTANCE.getConstr();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.LabeledTypeImpl <em>Labeled Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.LabeledTypeImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getLabeledType()
		 * @generated
		 */
		EClass LABELED_TYPE = eINSTANCE.getLabeledType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABELED_TYPE__NAME = eINSTANCE.getLabeledType_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LABELED_TYPE__TYPE = eINSTANCE.getLabeledType_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.FiniteBoundImpl <em>Finite Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.FiniteBoundImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getFiniteBound()
		 * @generated
		 */
		EClass FINITE_BOUND = eINSTANCE.getFiniteBound();

		/**
		 * The meta object literal for the '<em><b>Strict</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FINITE_BOUND__STRICT = eINSTANCE.getFiniteBound_Strict();

		/**
		 * The meta object literal for the '<em><b>Val</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FINITE_BOUND__VAL = eINSTANCE.getFiniteBound_Val();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.InfiniteBoundImpl <em>Infinite Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.InfiniteBoundImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getInfiniteBound()
		 * @generated
		 */
		EClass INFINITE_BOUND = eINSTANCE.getInfiniteBound();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.model.MinBound <em>Min Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.model.MinBound
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getMinBound()
		 * @generated
		 */
		EClass MIN_BOUND = eINSTANCE.getMinBound();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.model.MaxBound <em>Max Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.model.MaxBound
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getMaxBound()
		 * @generated
		 */
		EClass MAX_BOUND = eINSTANCE.getMaxBound();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.CondExpImpl <em>Cond Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.CondExpImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getCondExp()
		 * @generated
		 */
		EClass COND_EXP = eINSTANCE.getCondExp();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COND_EXP__COND = eINSTANCE.getCondExp_Cond();

		/**
		 * The meta object literal for the '<em><b>Iff</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COND_EXP__IFF = eINSTANCE.getCondExp_Iff();

		/**
		 * The meta object literal for the '<em><b>Ift</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COND_EXP__IFT = eINSTANCE.getCondExp_Ift();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.impl.ForeachImpl <em>Foreach</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.impl.ForeachImpl
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getForeach()
		 * @generated
		 */
		EClass FOREACH = eINSTANCE.getForeach();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOREACH__BODY = eINSTANCE.getForeach_Body();

		/**
		 * The meta object literal for the '<em><b>Iter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOREACH__ITER = eINSTANCE.getForeach_Iter();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.model.Unop <em>Unop</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.model.Unop
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getUnop()
		 * @generated
		 */
		EEnum UNOP = eINSTANCE.getUnop();

		/**
		 * The meta object literal for the '{@link org.topcased.fiacre.model.BinOp <em>Bin Op</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.fiacre.model.BinOp
		 * @see org.topcased.fiacre.impl.FiacrePackageImpl#getBinOp()
		 * @generated
		 */
		EEnum BIN_OP = eINSTANCE.getBinOp();

	}

} //FiacrePackage
