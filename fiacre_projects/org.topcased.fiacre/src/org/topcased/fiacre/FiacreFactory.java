/**
 * <copyright>
 * </copyright>
 *
 * $Id: FiacreFactory.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre;

import org.eclipse.emf.ecore.EFactory;

import org.topcased.fiacre.model.AnyPattern;
import org.topcased.fiacre.model.ArgumentVariable;
import org.topcased.fiacre.model.Array;
import org.topcased.fiacre.model.ArrayElem;
import org.topcased.fiacre.model.ArrayPattern;
import org.topcased.fiacre.model.BinExp;
import org.topcased.fiacre.model.BoolLiteral;
import org.topcased.fiacre.model.BoolType;
import org.topcased.fiacre.model.CaseStmt;
import org.topcased.fiacre.model.ChannelDecl;
import org.topcased.fiacre.model.ChannelId;
import org.topcased.fiacre.model.ComponentDecl;
import org.topcased.fiacre.model.CondExp;
import org.topcased.fiacre.model.ConstantDecl;
import org.topcased.fiacre.model.ConstantRef;
import org.topcased.fiacre.model.Constr;
import org.topcased.fiacre.model.ConstrExp;
import org.topcased.fiacre.model.ConstrPattern;
import org.topcased.fiacre.model.DeterministicAssignment;
import org.topcased.fiacre.model.Emission;
import org.topcased.fiacre.model.Field;
import org.topcased.fiacre.model.FieldPattern;
import org.topcased.fiacre.model.FiniteBound;
import org.topcased.fiacre.model.Foreach;
import org.topcased.fiacre.model.IfStmt;
import org.topcased.fiacre.model.InfiniteBound;
import org.topcased.fiacre.model.InlineArray;
import org.topcased.fiacre.model.InlineQueue;
import org.topcased.fiacre.model.InlineRecord;
import org.topcased.fiacre.model.Instance;
import org.topcased.fiacre.model.IntType;
import org.topcased.fiacre.model.InterfacedComp;
import org.topcased.fiacre.model.Interval;
import org.topcased.fiacre.model.LocalPortDecl;
import org.topcased.fiacre.model.LocalVariable;
import org.topcased.fiacre.model.NatLiteral;
import org.topcased.fiacre.model.NatType;
import org.topcased.fiacre.model.NonDeterministicAssignment;
import org.topcased.fiacre.model.NullStmt;
import org.topcased.fiacre.model.OrChannel;
import org.topcased.fiacre.model.Par;
import org.topcased.fiacre.model.ParamPortDecl;
import org.topcased.fiacre.model.Priority;
import org.topcased.fiacre.model.ProcessDecl;
import org.topcased.fiacre.model.Profile;
import org.topcased.fiacre.model.Program;
import org.topcased.fiacre.model.Queue;
import org.topcased.fiacre.model.Reception;
import org.topcased.fiacre.model.Record;
import org.topcased.fiacre.model.RecordElem;
import org.topcased.fiacre.model.RefArg;
import org.topcased.fiacre.model.Rule;
import org.topcased.fiacre.model.Select;
import org.topcased.fiacre.model.Seq;
import org.topcased.fiacre.model.Shuffle;
import org.topcased.fiacre.model.SingleAssignment;
import org.topcased.fiacre.model.State;
import org.topcased.fiacre.model.Sync;
import org.topcased.fiacre.model.Synchronization;
import org.topcased.fiacre.model.To;
import org.topcased.fiacre.model.Transition;
import org.topcased.fiacre.model.TypeDecl;
import org.topcased.fiacre.model.TypeId;
import org.topcased.fiacre.model.UnExp;
import org.topcased.fiacre.model.Union;
import org.topcased.fiacre.model.ValuedField;
import org.topcased.fiacre.model.VarRef;
import org.topcased.fiacre.model.WhileStmt;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.topcased.fiacre.FiacrePackage
 * @generated
 */
public interface FiacreFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FiacreFactory eINSTANCE = org.topcased.fiacre.impl.FiacreFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Program</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Program</em>'.
	 * @generated
	 */
	Program createProgram();

	/**
	 * Returns a new object of class '<em>Type Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Decl</em>'.
	 * @generated
	 */
	TypeDecl createTypeDecl();

	/**
	 * Returns a new object of class '<em>Channel Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Channel Decl</em>'.
	 * @generated
	 */
	ChannelDecl createChannelDecl();

	/**
	 * Returns a new object of class '<em>Component Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Decl</em>'.
	 * @generated
	 */
	ComponentDecl createComponentDecl();

	/**
	 * Returns a new object of class '<em>Process Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Decl</em>'.
	 * @generated
	 */
	ProcessDecl createProcessDecl();

	/**
	 * Returns a new object of class '<em>Argument Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Argument Variable</em>'.
	 * @generated
	 */
	ArgumentVariable createArgumentVariable();

	/**
	 * Returns a new object of class '<em>Local Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Local Variable</em>'.
	 * @generated
	 */
	LocalVariable createLocalVariable();

	/**
	 * Returns a new object of class '<em>Shuffle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Shuffle</em>'.
	 * @generated
	 */
	Shuffle createShuffle();

	/**
	 * Returns a new object of class '<em>Sync</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync</em>'.
	 * @generated
	 */
	Sync createSync();

	/**
	 * Returns a new object of class '<em>Par</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Par</em>'.
	 * @generated
	 */
	Par createPar();

	/**
	 * Returns a new object of class '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instance</em>'.
	 * @generated
	 */
	Instance createInstance();

	/**
	 * Returns a new object of class '<em>Interfaced Comp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interfaced Comp</em>'.
	 * @generated
	 */
	InterfacedComp createInterfacedComp();

	/**
	 * Returns a new object of class '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State</em>'.
	 * @generated
	 */
	State createState();

	/**
	 * Returns a new object of class '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition</em>'.
	 * @generated
	 */
	Transition createTransition();

	/**
	 * Returns a new object of class '<em>Null Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Stmt</em>'.
	 * @generated
	 */
	NullStmt createNullStmt();

	/**
	 * Returns a new object of class '<em>While Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Stmt</em>'.
	 * @generated
	 */
	WhileStmt createWhileStmt();

	/**
	 * Returns a new object of class '<em>If Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Stmt</em>'.
	 * @generated
	 */
	IfStmt createIfStmt();

	/**
	 * Returns a new object of class '<em>Select</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Select</em>'.
	 * @generated
	 */
	Select createSelect();

	/**
	 * Returns a new object of class '<em>To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>To</em>'.
	 * @generated
	 */
	To createTo();

	/**
	 * Returns a new object of class '<em>Deterministic Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deterministic Assignment</em>'.
	 * @generated
	 */
	DeterministicAssignment createDeterministicAssignment();

	/**
	 * Returns a new object of class '<em>Non Deterministic Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Deterministic Assignment</em>'.
	 * @generated
	 */
	NonDeterministicAssignment createNonDeterministicAssignment();

	/**
	 * Returns a new object of class '<em>Single Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Assignment</em>'.
	 * @generated
	 */
	SingleAssignment createSingleAssignment();

	/**
	 * Returns a new object of class '<em>Un Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Un Exp</em>'.
	 * @generated
	 */
	UnExp createUnExp();

	/**
	 * Returns a new object of class '<em>Bin Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bin Exp</em>'.
	 * @generated
	 */
	BinExp createBinExp();

	/**
	 * Returns a new object of class '<em>Nat Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nat Literal</em>'.
	 * @generated
	 */
	NatLiteral createNatLiteral();

	/**
	 * Returns a new object of class '<em>Bool Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bool Literal</em>'.
	 * @generated
	 */
	BoolLiteral createBoolLiteral();

	/**
	 * Returns a new object of class '<em>Var Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Var Ref</em>'.
	 * @generated
	 */
	VarRef createVarRef();

	/**
	 * Returns a new object of class '<em>Array Elem</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Elem</em>'.
	 * @generated
	 */
	ArrayElem createArrayElem();

	/**
	 * Returns a new object of class '<em>Record Elem</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Elem</em>'.
	 * @generated
	 */
	RecordElem createRecordElem();

	/**
	 * Returns a new object of class '<em>Inline Queue</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline Queue</em>'.
	 * @generated
	 */
	InlineQueue createInlineQueue();

	/**
	 * Returns a new object of class '<em>Bool Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bool Type</em>'.
	 * @generated
	 */
	BoolType createBoolType();

	/**
	 * Returns a new object of class '<em>Nat Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nat Type</em>'.
	 * @generated
	 */
	NatType createNatType();

	/**
	 * Returns a new object of class '<em>Int Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Type</em>'.
	 * @generated
	 */
	IntType createIntType();

	/**
	 * Returns a new object of class '<em>Interval</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interval</em>'.
	 * @generated
	 */
	Interval createInterval();

	/**
	 * Returns a new object of class '<em>Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record</em>'.
	 * @generated
	 */
	Record createRecord();

	/**
	 * Returns a new object of class '<em>Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field</em>'.
	 * @generated
	 */
	Field createField();

	/**
	 * Returns a new object of class '<em>Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array</em>'.
	 * @generated
	 */
	Array createArray();

	/**
	 * Returns a new object of class '<em>Queue</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Queue</em>'.
	 * @generated
	 */
	Queue createQueue();

	/**
	 * Returns a new object of class '<em>Type Id</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Id</em>'.
	 * @generated
	 */
	TypeId createTypeId();

	/**
	 * Returns a new object of class '<em>Channel Id</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Channel Id</em>'.
	 * @generated
	 */
	ChannelId createChannelId();

	/**
	 * Returns a new object of class '<em>Profile</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Profile</em>'.
	 * @generated
	 */
	Profile createProfile();

	/**
	 * Returns a new object of class '<em>Or Channel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Channel</em>'.
	 * @generated
	 */
	OrChannel createOrChannel();

	/**
	 * Returns a new object of class '<em>Inline Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline Array</em>'.
	 * @generated
	 */
	InlineArray createInlineArray();

	/**
	 * Returns a new object of class '<em>Inline Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline Record</em>'.
	 * @generated
	 */
	InlineRecord createInlineRecord();

	/**
	 * Returns a new object of class '<em>Valued Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Valued Field</em>'.
	 * @generated
	 */
	ValuedField createValuedField();

	/**
	 * Returns a new object of class '<em>Synchronization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synchronization</em>'.
	 * @generated
	 */
	Synchronization createSynchronization();

	/**
	 * Returns a new object of class '<em>Reception</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reception</em>'.
	 * @generated
	 */
	Reception createReception();

	/**
	 * Returns a new object of class '<em>Emission</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Emission</em>'.
	 * @generated
	 */
	Emission createEmission();

	/**
	 * Returns a new object of class '<em>Seq</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq</em>'.
	 * @generated
	 */
	Seq createSeq();

	/**
	 * Returns a new object of class '<em>Local Port Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Local Port Decl</em>'.
	 * @generated
	 */
	LocalPortDecl createLocalPortDecl();

	/**
	 * Returns a new object of class '<em>Param Port Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Param Port Decl</em>'.
	 * @generated
	 */
	ParamPortDecl createParamPortDecl();

	/**
	 * Returns a new object of class '<em>Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority</em>'.
	 * @generated
	 */
	Priority createPriority();

	/**
	 * Returns a new object of class '<em>Constr Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constr Exp</em>'.
	 * @generated
	 */
	ConstrExp createConstrExp();

	/**
	 * Returns a new object of class '<em>Union</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Union</em>'.
	 * @generated
	 */
	Union createUnion();

	/**
	 * Returns a new object of class '<em>Constr Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constr Pattern</em>'.
	 * @generated
	 */
	ConstrPattern createConstrPattern();

	/**
	 * Returns a new object of class '<em>Array Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Pattern</em>'.
	 * @generated
	 */
	ArrayPattern createArrayPattern();

	/**
	 * Returns a new object of class '<em>Field Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Pattern</em>'.
	 * @generated
	 */
	FieldPattern createFieldPattern();

	/**
	 * Returns a new object of class '<em>Any Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Any Pattern</em>'.
	 * @generated
	 */
	AnyPattern createAnyPattern();

	/**
	 * Returns a new object of class '<em>Case Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Stmt</em>'.
	 * @generated
	 */
	CaseStmt createCaseStmt();

	/**
	 * Returns a new object of class '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule</em>'.
	 * @generated
	 */
	Rule createRule();

	/**
	 * Returns a new object of class '<em>Ref Arg</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ref Arg</em>'.
	 * @generated
	 */
	RefArg createRefArg();

	/**
	 * Returns a new object of class '<em>Constant Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Decl</em>'.
	 * @generated
	 */
	ConstantDecl createConstantDecl();

	/**
	 * Returns a new object of class '<em>Constant Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Ref</em>'.
	 * @generated
	 */
	ConstantRef createConstantRef();

	/**
	 * Returns a new object of class '<em>Constr</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constr</em>'.
	 * @generated
	 */
	Constr createConstr();

	/**
	 * Returns a new object of class '<em>Finite Bound</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Finite Bound</em>'.
	 * @generated
	 */
	FiniteBound createFiniteBound();

	/**
	 * Returns a new object of class '<em>Infinite Bound</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Infinite Bound</em>'.
	 * @generated
	 */
	InfiniteBound createInfiniteBound();

	/**
	 * Returns a new object of class '<em>Cond Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cond Exp</em>'.
	 * @generated
	 */
	CondExp createCondExp();

	/**
	 * Returns a new object of class '<em>Foreach</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Foreach</em>'.
	 * @generated
	 */
	Foreach createForeach();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FiacrePackage getFiacrePackage();

} //FiacreFactory
