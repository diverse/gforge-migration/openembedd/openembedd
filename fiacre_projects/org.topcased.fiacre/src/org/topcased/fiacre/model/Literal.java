/**
 * <copyright>
 * </copyright>
 *
 * $Id: Literal.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getLiteral()
 * @model abstract="true"
 * @generated
 */
public interface Literal extends Exp, Pattern {
} // Literal
