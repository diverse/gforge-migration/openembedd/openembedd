/**
 * <copyright>
 * </copyright>
 *
 * $Id: Unop.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Unop</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.topcased.fiacre.FiacrePackage#getUnop()
 * @model
 * @generated
 */
public enum Unop implements Enumerator {
	/**
	 * The '<em><b>UMINUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UMINUS
	 * @generated
	 * @ordered
	 */
	UMINUS_LITERAL(0, "UMINUS", "UMINUS"),

	/**
	 * The '<em><b>UDOLLAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UDOLLAR
	 * @generated
	 * @ordered
	 */
	UDOLLAR_LITERAL(1, "UDOLLAR", "UDOLLAR"),

	/**
	 * The '<em><b>UNOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNOT
	 * @generated
	 * @ordered
	 */
	UNOT_LITERAL(2, "UNOT", "UNOT"),

	/**
	 * The '<em><b>UFULL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UFULL
	 * @generated
	 * @ordered
	 */
	UFULL_LITERAL(3, "UFULL", "UFULL"),

	/**
	 * The '<em><b>UEMPTY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UEMPTY
	 * @generated
	 * @ordered
	 */
	UEMPTY_LITERAL(4, "UEMPTY", "UEMPTY"),

	/**
	 * The '<em><b>DEQUEUE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEQUEUE
	 * @generated
	 * @ordered
	 */
	DEQUEUE_LITERAL(5, "DEQUEUE", "DEQUEUE"),

	/**
	 * The '<em><b>FIRST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIRST
	 * @generated
	 * @ordered
	 */
	FIRST_LITERAL(6, "FIRST", "FIRST");

	/**
	 * The '<em><b>UMINUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UMINUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UMINUS_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UMINUS = 0;

	/**
	 * The '<em><b>UDOLLAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UDOLLAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UDOLLAR_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UDOLLAR = 1;

	/**
	 * The '<em><b>UNOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNOT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNOT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNOT = 2;

	/**
	 * The '<em><b>UFULL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UFULL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UFULL_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UFULL = 3;

	/**
	 * The '<em><b>UEMPTY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UEMPTY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UEMPTY_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UEMPTY = 4;

	/**
	 * The '<em><b>DEQUEUE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DEQUEUE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEQUEUE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DEQUEUE = 5;

	/**
	 * The '<em><b>FIRST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST = 6;

	/**
	 * An array of all the '<em><b>Unop</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Unop[] VALUES_ARRAY = new Unop[] { UMINUS_LITERAL,
			UDOLLAR_LITERAL, UNOT_LITERAL, UFULL_LITERAL, UEMPTY_LITERAL,
			DEQUEUE_LITERAL, FIRST_LITERAL, };

	/**
	 * A public read-only list of all the '<em><b>Unop</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Unop> VALUES = Collections.unmodifiableList(Arrays
			.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Unop</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Unop get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Unop result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Unop</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Unop getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Unop result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Unop</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Unop get(int value) {
		switch (value) {
		case UMINUS:
			return UMINUS_LITERAL;
		case UDOLLAR:
			return UDOLLAR_LITERAL;
		case UNOT:
			return UNOT_LITERAL;
		case UFULL:
			return UFULL_LITERAL;
		case UEMPTY:
			return UEMPTY_LITERAL;
		case DEQUEUE:
			return DEQUEUE_LITERAL;
		case FIRST:
			return FIRST_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Unop(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //Unop
