/**
 * <copyright>
 * </copyright>
 *
 * $Id: LocalPortDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Port Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.LocalPortDecl#getMini <em>Mini</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.LocalPortDecl#getMaxi <em>Maxi</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getLocalPortDecl()
 * @model
 * @generated
 */
public interface LocalPortDecl extends PortDecl {
	/**
	 * Returns the value of the '<em><b>Mini</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mini</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mini</em>' containment reference.
	 * @see #setMini(MinBound)
	 * @see org.topcased.fiacre.FiacrePackage#getLocalPortDecl_Mini()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MinBound getMini();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.LocalPortDecl#getMini <em>Mini</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mini</em>' containment reference.
	 * @see #getMini()
	 * @generated
	 */
	void setMini(MinBound value);

	/**
	 * Returns the value of the '<em><b>Maxi</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maxi</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maxi</em>' containment reference.
	 * @see #setMaxi(MaxBound)
	 * @see org.topcased.fiacre.FiacrePackage#getLocalPortDecl_Maxi()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MaxBound getMaxi();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.LocalPortDecl#getMaxi <em>Maxi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maxi</em>' containment reference.
	 * @see #getMaxi()
	 * @generated
	 */
	void setMaxi(MaxBound value);

} // LocalPortDecl
