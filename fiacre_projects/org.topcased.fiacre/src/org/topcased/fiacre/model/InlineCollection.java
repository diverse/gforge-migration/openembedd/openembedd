/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineCollection.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inline Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.InlineCollection#getElems <em>Elems</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getInlineCollection()
 * @model abstract="true"
 * @generated
 */
public interface InlineCollection extends Exp {
	/**
	 * Returns the value of the '<em><b>Elems</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Exp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elems</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elems</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getInlineCollection_Elems()
	 * @model containment="true"
	 * @generated
	 */
	EList<Exp> getElems();

} // InlineCollection
