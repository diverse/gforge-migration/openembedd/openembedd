/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProcessDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ProcessDecl#getStates <em>States</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ProcessDecl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ProcessDecl#getInitAction <em>Init Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getProcessDecl()
 * @model
 * @generated
 */
public interface ProcessDecl extends NodeDecl {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getProcessDecl_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getProcessDecl_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Init Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Action</em>' containment reference.
	 * @see #setInitAction(Statement)
	 * @see org.topcased.fiacre.FiacrePackage#getProcessDecl_InitAction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Statement getInitAction();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ProcessDecl#getInitAction <em>Init Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Action</em>' containment reference.
	 * @see #getInitAction()
	 * @generated
	 */
	void setInitAction(Statement value);

} // ProcessDecl
