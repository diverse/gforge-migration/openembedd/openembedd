/**
 * <copyright>
 * </copyright>
 *
 * $Id: Rule.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Rule#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Rule#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getRule()
 * @model
 * @generated
 */
public interface Rule extends EObject {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(Pattern)
	 * @see org.topcased.fiacre.FiacrePackage#getRule_Lhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Pattern getLhs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Rule#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(Pattern value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(Statement)
	 * @see org.topcased.fiacre.FiacrePackage#getRule_Action()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Statement getAction();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Rule#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Statement value);

} // Rule
