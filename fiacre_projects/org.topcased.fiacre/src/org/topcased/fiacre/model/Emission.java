/**
 * <copyright>
 * </copyright>
 *
 * $Id: Emission.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Emission</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Emission#getArgs <em>Args</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getEmission()
 * @model
 * @generated
 */
public interface Emission extends Communication {
	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Exp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getEmission_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Exp> getArgs();

} // Emission
