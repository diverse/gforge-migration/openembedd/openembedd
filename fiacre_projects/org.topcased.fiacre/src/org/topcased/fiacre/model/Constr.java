/**
 * <copyright>
 * </copyright>
 *
 * $Id: Constr.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constr</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getConstr()
 * @model
 * @generated
 */
public interface Constr extends LabeledType {
} // Constr
