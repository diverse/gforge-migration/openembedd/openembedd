/**
 * <copyright>
 * </copyright>
 *
 * $Id: NonDeterministicAssignment.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Deterministic Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.NonDeterministicAssignment#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.NonDeterministicAssignment#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getNonDeterministicAssignment()
 * @model
 * @generated
 */
public interface NonDeterministicAssignment extends Assignment {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Pattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getNonDeterministicAssignment_Lhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Pattern> getLhs();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getNonDeterministicAssignment_Condition()
	 * @model containment="true"
	 * @generated
	 */
	Exp getCondition();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.NonDeterministicAssignment#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Exp value);

} // NonDeterministicAssignment
