/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineArray.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inline Array</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getInlineArray()
 * @model
 * @generated
 */
public interface InlineArray extends InlineCollection {
} // InlineArray
