/**
 * <copyright>
 * </copyright>
 *
 * $Id: Assignment.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getAssignment()
 * @model abstract="true"
 * @generated
 */
public interface Assignment extends Statement {
} // Assignment
