/**
 * <copyright>
 * </copyright>
 *
 * $Id: TypeDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.TypeDecl#getIs <em>Is</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getTypeDecl()
 * @model
 * @generated
 */
public interface TypeDecl extends Declaration {
	/**
	 * Returns the value of the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is</em>' containment reference.
	 * @see #setIs(Type)
	 * @see org.topcased.fiacre.FiacrePackage#getTypeDecl_Is()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Type getIs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.TypeDecl#getIs <em>Is</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is</em>' containment reference.
	 * @see #getIs()
	 * @generated
	 */
	void setIs(Type value);

} // TypeDecl
