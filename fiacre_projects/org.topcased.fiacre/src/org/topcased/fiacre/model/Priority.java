/**
 * <copyright>
 * </copyright>
 *
 * $Id: Priority.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Priority#getInf <em>Inf</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Priority#getSup <em>Sup</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getPriority()
 * @model
 * @generated
 */
public interface Priority extends EObject {
	/**
	 * Returns the value of the '<em><b>Inf</b></em>' reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.PortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inf</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inf</em>' reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getPriority_Inf()
	 * @model required="true"
	 * @generated
	 */
	EList<PortDecl> getInf();

	/**
	 * Returns the value of the '<em><b>Sup</b></em>' reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.PortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sup</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sup</em>' reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getPriority_Sup()
	 * @model required="true"
	 * @generated
	 */
	EList<PortDecl> getSup();

} // Priority
