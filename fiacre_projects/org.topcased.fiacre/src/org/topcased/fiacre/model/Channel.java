/**
 * <copyright>
 * </copyright>
 *
 * $Id: Channel.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Channel</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getChannel()
 * @model abstract="true"
 * @generated
 */
public interface Channel extends EObject {
} // Channel
