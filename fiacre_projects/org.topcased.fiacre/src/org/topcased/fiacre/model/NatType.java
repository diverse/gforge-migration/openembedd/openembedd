/**
 * <copyright>
 * </copyright>
 *
 * $Id: NatType.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nat Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getNatType()
 * @model
 * @generated
 */
public interface NatType extends BasicType {
} // NatType
