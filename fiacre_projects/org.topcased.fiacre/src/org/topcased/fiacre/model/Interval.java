/**
 * <copyright>
 * </copyright>
 *
 * $Id: Interval.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Interval#getMini <em>Mini</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Interval#getMaxi <em>Maxi</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getInterval()
 * @model
 * @generated
 */
public interface Interval extends Type {
	/**
	 * Returns the value of the '<em><b>Mini</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mini</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mini</em>' attribute.
	 * @see #setMini(int)
	 * @see org.topcased.fiacre.FiacrePackage#getInterval_Mini()
	 * @model required="true"
	 * @generated
	 */
	int getMini();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Interval#getMini <em>Mini</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mini</em>' attribute.
	 * @see #getMini()
	 * @generated
	 */
	void setMini(int value);

	/**
	 * Returns the value of the '<em><b>Maxi</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maxi</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maxi</em>' attribute.
	 * @see #setMaxi(int)
	 * @see org.topcased.fiacre.FiacrePackage#getInterval_Maxi()
	 * @model required="true"
	 * @generated
	 */
	int getMaxi();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Interval#getMaxi <em>Maxi</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maxi</em>' attribute.
	 * @see #getMaxi()
	 * @generated
	 */
	void setMaxi(int value);

} // Interval
