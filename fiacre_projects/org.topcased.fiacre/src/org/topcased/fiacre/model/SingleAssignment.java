/**
 * <copyright>
 * </copyright>
 *
 * $Id: SingleAssignment.java,v 1.3 2008-12-12 14:57:35 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.SingleAssignment#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.SingleAssignment#getRhs <em>Rhs</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getSingleAssignment()
 * @model
 * @generated
 */
public interface SingleAssignment extends EObject {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(Pattern)
	 * @see org.topcased.fiacre.FiacrePackage#getSingleAssignment_Lhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Pattern getLhs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.SingleAssignment#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(Pattern value);

	/**
	 * Returns the value of the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs</em>' containment reference.
	 * @see #setRhs(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getSingleAssignment_Rhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getRhs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.SingleAssignment#getRhs <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs</em>' containment reference.
	 * @see #getRhs()
	 * @generated
	 */
	void setRhs(Exp value);

} // SingleAssignment
