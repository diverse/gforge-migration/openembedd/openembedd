/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstantRef.java,v 1.3 2008-12-12 14:57:35 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ConstantRef#getDecl <em>Decl</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getConstantRef()
 * @model
 * @generated
 */
public interface ConstantRef extends Exp, Pattern {
	/**
	 * Returns the value of the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decl</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decl</em>' reference.
	 * @see #setDecl(ConstantDecl)
	 * @see org.topcased.fiacre.FiacrePackage#getConstantRef_Decl()
	 * @model required="true"
	 * @generated
	 */
	ConstantDecl getDecl();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ConstantRef#getDecl <em>Decl</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decl</em>' reference.
	 * @see #getDecl()
	 * @generated
	 */
	void setDecl(ConstantDecl value);

} // ConstantRef
