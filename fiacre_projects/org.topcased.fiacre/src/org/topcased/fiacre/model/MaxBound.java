/**
 * <copyright>
 * </copyright>
 *
 * $Id: MaxBound.java,v 1.3 2008-12-12 14:57:35 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getMaxBound()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface MaxBound extends EObject {
} // MaxBound
