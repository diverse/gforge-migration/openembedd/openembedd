/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineQueue.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inline Queue</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getInlineQueue()
 * @model
 * @generated
 */
public interface InlineQueue extends InlineCollection {
} // InlineQueue
