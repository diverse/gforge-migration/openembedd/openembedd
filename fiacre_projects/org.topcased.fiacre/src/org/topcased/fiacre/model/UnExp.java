/**
 * <copyright>
 * </copyright>
 *
 * $Id: UnExp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Un Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.UnExp#getExp <em>Exp</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.UnExp#getUnop <em>Unop</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getUnExp()
 * @model
 * @generated
 */
public interface UnExp extends Exp {
	/**
	 * Returns the value of the '<em><b>Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exp</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exp</em>' containment reference.
	 * @see #setExp(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getUnExp_Exp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getExp();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.UnExp#getExp <em>Exp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exp</em>' containment reference.
	 * @see #getExp()
	 * @generated
	 */
	void setExp(Exp value);

	/**
	 * Returns the value of the '<em><b>Unop</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.fiacre.model.Unop}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unop</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unop</em>' attribute.
	 * @see org.topcased.fiacre.model.Unop
	 * @see #setUnop(Unop)
	 * @see org.topcased.fiacre.FiacrePackage#getUnExp_Unop()
	 * @model required="true"
	 * @generated
	 */
	Unop getUnop();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.UnExp#getUnop <em>Unop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unop</em>' attribute.
	 * @see org.topcased.fiacre.model.Unop
	 * @see #getUnop()
	 * @generated
	 */
	void setUnop(Unop value);

} // UnExp
