/**
 * <copyright>
 * </copyright>
 *
 * $Id: InfiniteBound.java,v 1.3 2008-12-12 14:57:35 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Infinite Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getInfiniteBound()
 * @model
 * @generated
 */
public interface InfiniteBound extends MaxBound {
} // InfiniteBound
