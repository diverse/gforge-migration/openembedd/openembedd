/**
 * <copyright>
 * </copyright>
 *
 * $Id: Type.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getType()
 * @model abstract="true"
 * @generated
 */
public interface Type extends EObject {
} // Type
