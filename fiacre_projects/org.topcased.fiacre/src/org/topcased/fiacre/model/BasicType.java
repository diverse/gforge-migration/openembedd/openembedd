/**
 * <copyright>
 * </copyright>
 *
 * $Id: BasicType.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getBasicType()
 * @model abstract="true"
 * @generated
 */
public interface BasicType extends Type {
} // BasicType
