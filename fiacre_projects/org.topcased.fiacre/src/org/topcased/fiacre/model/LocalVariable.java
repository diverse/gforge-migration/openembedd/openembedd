/**
 * <copyright>
 * </copyright>
 *
 * $Id: LocalVariable.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.LocalVariable#getInitializer <em>Initializer</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getLocalVariable()
 * @model
 * @generated
 */
public interface LocalVariable extends Variable {
	/**
	 * Returns the value of the '<em><b>Initializer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initializer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initializer</em>' containment reference.
	 * @see #setInitializer(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getLocalVariable_Initializer()
	 * @model containment="true"
	 * @generated
	 */
	Exp getInitializer();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.LocalVariable#getInitializer <em>Initializer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initializer</em>' containment reference.
	 * @see #getInitializer()
	 * @generated
	 */
	void setInitializer(Exp value);

} // LocalVariable
