/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.NodeDecl#getArgs <em>Args</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.NodeDecl#getVars <em>Vars</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.NodeDecl#getPorts <em>Ports</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.NodeDecl#getLocalPorts <em>Local Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getNodeDecl()
 * @model abstract="true"
 * @generated
 */
public interface NodeDecl extends Declaration {
	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.ArgumentVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getNodeDecl_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<ArgumentVariable> getArgs();

	/**
	 * Returns the value of the '<em><b>Vars</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.LocalVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vars</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vars</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getNodeDecl_Vars()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocalVariable> getVars();

	/**
	 * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.ParamPortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getNodeDecl_Ports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParamPortDecl> getPorts();

	/**
	 * Returns the value of the '<em><b>Local Ports</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.LocalPortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Ports</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getNodeDecl_LocalPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocalPortDecl> getLocalPorts();

} // NodeDecl
