/**
 * <copyright>
 * </copyright>
 *
 * $Id: Instance.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Instance#getType <em>Type</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Instance#getArgs <em>Args</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Instance#getPorts <em>Ports</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Instance#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getInstance()
 * @model
 * @generated
 */
public interface Instance extends Composition {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(NodeDecl)
	 * @see org.topcased.fiacre.FiacrePackage#getInstance_Type()
	 * @model required="true"
	 * @generated
	 */
	NodeDecl getType();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Instance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(NodeDecl value);

	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Arg}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getInstance_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Arg> getArgs();

	/**
	 * Returns the value of the '<em><b>Ports</b></em>' reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.PortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getInstance_Ports()
	 * @model
	 * @generated
	 */
	EList<PortDecl> getPorts();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.topcased.fiacre.FiacrePackage#getInstance_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Instance#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Instance
