/**
 * <copyright>
 * </copyright>
 *
 * $Id: InterfacedComp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interfaced Comp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.InterfacedComp#getComposition <em>Composition</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.InterfacedComp#getSyncPorts <em>Sync Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getInterfacedComp()
 * @model
 * @generated
 */
public interface InterfacedComp extends EObject {
	/**
	 * Returns the value of the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composition</em>' containment reference.
	 * @see #setComposition(Composition)
	 * @see org.topcased.fiacre.FiacrePackage#getInterfacedComp_Composition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Composition getComposition();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.InterfacedComp#getComposition <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Composition</em>' containment reference.
	 * @see #getComposition()
	 * @generated
	 */
	void setComposition(Composition value);

	/**
	 * Returns the value of the '<em><b>Sync Ports</b></em>' reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.PortDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Ports</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Ports</em>' reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getInterfacedComp_SyncPorts()
	 * @model
	 * @generated
	 */
	EList<PortDecl> getSyncPorts();

} // InterfacedComp
