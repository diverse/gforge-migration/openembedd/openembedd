/**
 * <copyright>
 * </copyright>
 *
 * $Id: NullStmt.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Null Stmt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getNullStmt()
 * @model
 * @generated
 */
public interface NullStmt extends Statement {
} // NullStmt
