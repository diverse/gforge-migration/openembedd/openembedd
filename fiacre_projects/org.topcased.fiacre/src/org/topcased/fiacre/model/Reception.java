/**
 * <copyright>
 * </copyright>
 *
 * $Id: Reception.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reception</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Reception#getPattern <em>Pattern</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Reception#getWhere <em>Where</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getReception()
 * @model
 * @generated
 */
public interface Reception extends Communication {
	/**
	 * Returns the value of the '<em><b>Pattern</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Pattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pattern</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pattern</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getReception_Pattern()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Pattern> getPattern();

	/**
	 * Returns the value of the '<em><b>Where</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Where</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Where</em>' containment reference.
	 * @see #setWhere(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getReception_Where()
	 * @model containment="true"
	 * @generated
	 */
	Exp getWhere();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Reception#getWhere <em>Where</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Where</em>' containment reference.
	 * @see #getWhere()
	 * @generated
	 */
	void setWhere(Exp value);

} // Reception
