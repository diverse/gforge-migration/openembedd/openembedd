/**
 * <copyright>
 * </copyright>
 *
 * $Id: Union.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Union</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Union#getConstr <em>Constr</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getUnion()
 * @model
 * @generated
 */
public interface Union extends Type {
	/**
	 * Returns the value of the '<em><b>Constr</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Constr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constr</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constr</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getUnion_Constr()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Constr> getConstr();

} // Union
