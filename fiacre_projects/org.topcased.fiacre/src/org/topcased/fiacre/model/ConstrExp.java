/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConstrExp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constr Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ConstrExp#getArg <em>Arg</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ConstrExp#getConstr <em>Constr</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getConstrExp()
 * @model
 * @generated
 */
public interface ConstrExp extends Exp {
	/**
	 * Returns the value of the '<em><b>Arg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arg</em>' containment reference.
	 * @see #setArg(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getConstrExp_Arg()
	 * @model containment="true"
	 * @generated
	 */
	Exp getArg();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ConstrExp#getArg <em>Arg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arg</em>' containment reference.
	 * @see #getArg()
	 * @generated
	 */
	void setArg(Exp value);

	/**
	 * Returns the value of the '<em><b>Constr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constr</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constr</em>' reference.
	 * @see #setConstr(Constr)
	 * @see org.topcased.fiacre.FiacrePackage#getConstrExp_Constr()
	 * @model required="true"
	 * @generated
	 */
	Constr getConstr();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ConstrExp#getConstr <em>Constr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constr</em>' reference.
	 * @see #getConstr()
	 * @generated
	 */
	void setConstr(Constr value);

} // ConstrExp
