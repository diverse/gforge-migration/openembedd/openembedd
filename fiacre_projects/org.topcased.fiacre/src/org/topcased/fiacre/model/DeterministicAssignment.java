/**
 * <copyright>
 * </copyright>
 *
 * $Id: DeterministicAssignment.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deterministic Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.DeterministicAssignment#getAssignments <em>Assignments</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getDeterministicAssignment()
 * @model
 * @generated
 */
public interface DeterministicAssignment extends Assignment {
	/**
	 * Returns the value of the '<em><b>Assignments</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.SingleAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignments</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getDeterministicAssignment_Assignments()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<SingleAssignment> getAssignments();

} // DeterministicAssignment
