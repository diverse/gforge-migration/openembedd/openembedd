/**
 * <copyright>
 * </copyright>
 *
 * $Id: BinExp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bin Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.BinExp#getR_exp <em>Rexp</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.BinExp#getL_exp <em>Lexp</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.BinExp#getBinOp <em>Bin Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getBinExp()
 * @model
 * @generated
 */
public interface BinExp extends Exp {
	/**
	 * Returns the value of the '<em><b>Rexp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rexp</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rexp</em>' containment reference.
	 * @see #setR_exp(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getBinExp_R_exp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getR_exp();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.BinExp#getR_exp <em>Rexp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rexp</em>' containment reference.
	 * @see #getR_exp()
	 * @generated
	 */
	void setR_exp(Exp value);

	/**
	 * Returns the value of the '<em><b>Lexp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lexp</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lexp</em>' containment reference.
	 * @see #setL_exp(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getBinExp_L_exp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getL_exp();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.BinExp#getL_exp <em>Lexp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lexp</em>' containment reference.
	 * @see #getL_exp()
	 * @generated
	 */
	void setL_exp(Exp value);

	/**
	 * Returns the value of the '<em><b>Bin Op</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.fiacre.model.BinOp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bin Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bin Op</em>' attribute.
	 * @see org.topcased.fiacre.model.BinOp
	 * @see #setBinOp(BinOp)
	 * @see org.topcased.fiacre.FiacrePackage#getBinExp_BinOp()
	 * @model required="true"
	 * @generated
	 */
	BinOp getBinOp();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.BinExp#getBinOp <em>Bin Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bin Op</em>' attribute.
	 * @see org.topcased.fiacre.model.BinOp
	 * @see #getBinOp()
	 * @generated
	 */
	void setBinOp(BinOp value);

} // BinExp
