/**
 * <copyright>
 * </copyright>
 *
 * $Id: TypeId.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.TypeId#getDecl <em>Decl</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getTypeId()
 * @model
 * @generated
 */
public interface TypeId extends Type {
	/**
	 * Returns the value of the '<em><b>Decl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decl</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decl</em>' reference.
	 * @see #setDecl(TypeDecl)
	 * @see org.topcased.fiacre.FiacrePackage#getTypeId_Decl()
	 * @model required="true"
	 * @generated
	 */
	TypeDecl getDecl();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.TypeId#getDecl <em>Decl</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decl</em>' reference.
	 * @see #getDecl()
	 * @generated
	 */
	void setDecl(TypeDecl value);

} // TypeId
