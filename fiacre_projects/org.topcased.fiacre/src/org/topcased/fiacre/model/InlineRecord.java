/**
 * <copyright>
 * </copyright>
 *
 * $Id: InlineRecord.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inline Record</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.InlineRecord#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getInlineRecord()
 * @model
 * @generated
 */
public interface InlineRecord extends Exp {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.ValuedField}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getInlineRecord_Values()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ValuedField> getValues();

} // InlineRecord
