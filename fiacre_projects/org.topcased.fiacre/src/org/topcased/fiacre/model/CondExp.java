/**
 * <copyright>
 * </copyright>
 *
 * $Id: CondExp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cond Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.CondExp#getCond <em>Cond</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.CondExp#getIff <em>Iff</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.CondExp#getIft <em>Ift</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getCondExp()
 * @model
 * @generated
 */
public interface CondExp extends Exp {
	/**
	 * Returns the value of the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond</em>' containment reference.
	 * @see #setCond(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getCondExp_Cond()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getCond();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.CondExp#getCond <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond</em>' containment reference.
	 * @see #getCond()
	 * @generated
	 */
	void setCond(Exp value);

	/**
	 * Returns the value of the '<em><b>Iff</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iff</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iff</em>' containment reference.
	 * @see #setIff(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getCondExp_Iff()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getIff();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.CondExp#getIff <em>Iff</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iff</em>' containment reference.
	 * @see #getIff()
	 * @generated
	 */
	void setIff(Exp value);

	/**
	 * Returns the value of the '<em><b>Ift</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ift</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ift</em>' containment reference.
	 * @see #setIft(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getCondExp_Ift()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getIft();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.CondExp#getIft <em>Ift</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ift</em>' containment reference.
	 * @see #getIft()
	 * @generated
	 */
	void setIft(Exp value);

} // CondExp
