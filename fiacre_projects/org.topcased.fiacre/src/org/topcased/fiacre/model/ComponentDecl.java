/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ComponentDecl#getBody <em>Body</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ComponentDecl#getPriority <em>Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getComponentDecl()
 * @model
 * @generated
 */
public interface ComponentDecl extends NodeDecl {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Composition)
	 * @see org.topcased.fiacre.FiacrePackage#getComponentDecl_Body()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Composition getBody();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ComponentDecl#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Composition value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.fiacre.model.Priority}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' containment reference list.
	 * @see org.topcased.fiacre.FiacrePackage#getComponentDecl_Priority()
	 * @model containment="true"
	 * @generated
	 */
	EList<Priority> getPriority();

} // ComponentDecl
