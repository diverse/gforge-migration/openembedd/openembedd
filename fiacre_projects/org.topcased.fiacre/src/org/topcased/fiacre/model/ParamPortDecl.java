/**
 * <copyright>
 * </copyright>
 *
 * $Id: ParamPortDecl.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Param Port Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getParamPortDecl()
 * @model
 * @generated
 */
public interface ParamPortDecl extends PortDecl {
} // ParamPortDecl
