/**
 * <copyright>
 * </copyright>
 *
 * $Id: Synchronization.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronization</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getSynchronization()
 * @model
 * @generated
 */
public interface Synchronization extends Communication {
} // Synchronization
