/**
 * <copyright>
 * </copyright>
 *
 * $Id: BoolType.java,v 1.2 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getBoolType()
 * @model
 * @generated
 */
public interface BoolType extends BasicType {
} // BoolType
