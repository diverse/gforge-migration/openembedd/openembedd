/**
 * <copyright>
 * </copyright>
 *
 * $Id: Field.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.fiacre.FiacrePackage#getField()
 * @model
 * @generated
 */
public interface Field extends LabeledType {
} // Field
