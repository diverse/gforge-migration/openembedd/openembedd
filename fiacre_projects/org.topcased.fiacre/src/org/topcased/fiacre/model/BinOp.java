/**
 * <copyright>
 * </copyright>
 *
 * $Id: BinOp.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Bin Op</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.topcased.fiacre.FiacrePackage#getBinOp()
 * @model
 * @generated
 */
public enum BinOp implements Enumerator {
	/**
	 * The '<em><b>BOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOR
	 * @generated
	 * @ordered
	 */
	BOR_LITERAL(0, "BOR", "BOR"),

	/**
	 * The '<em><b>BAND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BAND
	 * @generated
	 * @ordered
	 */
	BAND_LITERAL(1, "BAND", "BAND"),

	/**
	 * The '<em><b>BEQ</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BEQ
	 * @generated
	 * @ordered
	 */
	BEQ_LITERAL(2, "BEQ", "BEQ"),

	/**
	 * The '<em><b>BNE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BNE
	 * @generated
	 * @ordered
	 */
	BNE_LITERAL(3, "BNE", "BNE"),

	/**
	 * The '<em><b>BLT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLT
	 * @generated
	 * @ordered
	 */
	BLT_LITERAL(4, "BLT", "BLT"),

	/**
	 * The '<em><b>BGT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BGT
	 * @generated
	 * @ordered
	 */
	BGT_LITERAL(5, "BGT", "BGT"),

	/**
	 * The '<em><b>BLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLE
	 * @generated
	 * @ordered
	 */
	BLE_LITERAL(6, "BLE", "BLE"),

	/**
	 * The '<em><b>BGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BGE
	 * @generated
	 * @ordered
	 */
	BGE_LITERAL(7, "BGE", "BGE"),

	/**
	 * The '<em><b>BADD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BADD
	 * @generated
	 * @ordered
	 */
	BADD_LITERAL(8, "BADD", "BADD"),

	/**
	 * The '<em><b>BMINUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BMINUS
	 * @generated
	 * @ordered
	 */
	BMINUS_LITERAL(9, "BMINUS", "BMINUS"),

	/**
	 * The '<em><b>BMUL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BMUL
	 * @generated
	 * @ordered
	 */
	BMUL_LITERAL(10, "BMUL", "BMUL"),

	/**
	 * The '<em><b>BDIV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BDIV
	 * @generated
	 * @ordered
	 */
	BDIV_LITERAL(11, "BDIV", "BDIV"),

	/**
	 * The '<em><b>BMOD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BMOD
	 * @generated
	 * @ordered
	 */
	BMOD_LITERAL(12, "BMOD", "BMOD"),

	/**
	 * The '<em><b>ENQUEUE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENQUEUE
	 * @generated
	 * @ordered
	 */
	ENQUEUE_LITERAL(13, "ENQUEUE", "ENQUEUE"),

	/**
	 * The '<em><b>APPEND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPEND
	 * @generated
	 * @ordered
	 */
	APPEND_LITERAL(14, "APPEND", "APPEND");

	/**
	 * The '<em><b>BOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOR_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BOR = 0;

	/**
	 * The '<em><b>BAND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BAND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BAND_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BAND = 1;

	/**
	 * The '<em><b>BEQ</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BEQ</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BEQ_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BEQ = 2;

	/**
	 * The '<em><b>BNE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BNE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BNE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BNE = 3;

	/**
	 * The '<em><b>BLT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BLT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BLT = 4;

	/**
	 * The '<em><b>BGT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BGT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BGT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BGT = 5;

	/**
	 * The '<em><b>BLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BLE = 6;

	/**
	 * The '<em><b>BGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BGE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BGE = 7;

	/**
	 * The '<em><b>BADD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BADD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BADD_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BADD = 8;

	/**
	 * The '<em><b>BMINUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BMINUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BMINUS_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BMINUS = 9;

	/**
	 * The '<em><b>BMUL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BMUL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BMUL_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BMUL = 10;

	/**
	 * The '<em><b>BDIV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BDIV</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BDIV_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BDIV = 11;

	/**
	 * The '<em><b>BMOD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BMOD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BMOD_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BMOD = 12;

	/**
	 * The '<em><b>ENQUEUE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ENQUEUE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENQUEUE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ENQUEUE = 13;

	/**
	 * The '<em><b>APPEND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>APPEND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #APPEND_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int APPEND = 14;

	/**
	 * An array of all the '<em><b>Bin Op</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final BinOp[] VALUES_ARRAY = new BinOp[] { BOR_LITERAL,
			BAND_LITERAL, BEQ_LITERAL, BNE_LITERAL, BLT_LITERAL, BGT_LITERAL,
			BLE_LITERAL, BGE_LITERAL, BADD_LITERAL, BMINUS_LITERAL,
			BMUL_LITERAL, BDIV_LITERAL, BMOD_LITERAL, ENQUEUE_LITERAL,
			APPEND_LITERAL, };

	/**
	 * A public read-only list of all the '<em><b>Bin Op</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<BinOp> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Bin Op</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinOp get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BinOp result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Bin Op</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinOp getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BinOp result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Bin Op</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinOp get(int value) {
		switch (value) {
		case BOR:
			return BOR_LITERAL;
		case BAND:
			return BAND_LITERAL;
		case BEQ:
			return BEQ_LITERAL;
		case BNE:
			return BNE_LITERAL;
		case BLT:
			return BLT_LITERAL;
		case BGT:
			return BGT_LITERAL;
		case BLE:
			return BLE_LITERAL;
		case BGE:
			return BGE_LITERAL;
		case BADD:
			return BADD_LITERAL;
		case BMINUS:
			return BMINUS_LITERAL;
		case BMUL:
			return BMUL_LITERAL;
		case BDIV:
			return BDIV_LITERAL;
		case BMOD:
			return BMOD_LITERAL;
		case ENQUEUE:
			return ENQUEUE_LITERAL;
		case APPEND:
			return APPEND_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private BinOp(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //BinOp
