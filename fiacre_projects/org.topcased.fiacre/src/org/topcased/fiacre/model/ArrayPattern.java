/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArrayPattern.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ArrayPattern#getIndex <em>Index</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ArrayPattern#getArray <em>Array</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getArrayPattern()
 * @model
 * @generated
 */
public interface ArrayPattern extends Pattern {
	/**
	 * Returns the value of the '<em><b>Index</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' containment reference.
	 * @see #setIndex(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getArrayPattern_Index()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getIndex();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArrayPattern#getIndex <em>Index</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' containment reference.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(Exp value);

	/**
	 * Returns the value of the '<em><b>Array</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array</em>' containment reference.
	 * @see #setArray(Pattern)
	 * @see org.topcased.fiacre.FiacrePackage#getArrayPattern_Array()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Pattern getArray();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArrayPattern#getArray <em>Array</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array</em>' containment reference.
	 * @see #getArray()
	 * @generated
	 */
	void setArray(Pattern value);

} // ArrayPattern
