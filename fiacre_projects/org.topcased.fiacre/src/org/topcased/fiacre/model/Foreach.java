/**
 * <copyright>
 * </copyright>
 *
 * $Id: Foreach.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Foreach</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.Foreach#getBody <em>Body</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.Foreach#getIter <em>Iter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getForeach()
 * @model
 * @generated
 */
public interface Foreach extends Statement {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Statement)
	 * @see org.topcased.fiacre.FiacrePackage#getForeach_Body()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Statement getBody();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Foreach#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Statement value);

	/**
	 * Returns the value of the '<em><b>Iter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iter</em>' reference.
	 * @see #setIter(LocalVariable)
	 * @see org.topcased.fiacre.FiacrePackage#getForeach_Iter()
	 * @model required="true"
	 * @generated
	 */
	LocalVariable getIter();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.Foreach#getIter <em>Iter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iter</em>' reference.
	 * @see #getIter()
	 * @generated
	 */
	void setIter(LocalVariable value);

} // Foreach
