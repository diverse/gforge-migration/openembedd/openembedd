/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArgumentVariable.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ArgumentVariable#isRead <em>Read</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ArgumentVariable#isWrite <em>Write</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ArgumentVariable#isRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getArgumentVariable()
 * @model
 * @generated
 */
public interface ArgumentVariable extends Variable {
	/**
	 * Returns the value of the '<em><b>Read</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read</em>' attribute.
	 * @see #setRead(boolean)
	 * @see org.topcased.fiacre.FiacrePackage#getArgumentVariable_Read()
	 * @model required="true"
	 * @generated
	 */
	boolean isRead();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArgumentVariable#isRead <em>Read</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Read</em>' attribute.
	 * @see #isRead()
	 * @generated
	 */
	void setRead(boolean value);

	/**
	 * Returns the value of the '<em><b>Write</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write</em>' attribute.
	 * @see #setWrite(boolean)
	 * @see org.topcased.fiacre.FiacrePackage#getArgumentVariable_Write()
	 * @model required="true"
	 * @generated
	 */
	boolean isWrite();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArgumentVariable#isWrite <em>Write</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Write</em>' attribute.
	 * @see #isWrite()
	 * @generated
	 */
	void setWrite(boolean value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' attribute.
	 * @see #setRef(boolean)
	 * @see org.topcased.fiacre.FiacrePackage#getArgumentVariable_Ref()
	 * @model required="true"
	 * @generated
	 */
	boolean isRef();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArgumentVariable#isRef <em>Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' attribute.
	 * @see #isRef()
	 * @generated
	 */
	void setRef(boolean value);

} // ArgumentVariable
