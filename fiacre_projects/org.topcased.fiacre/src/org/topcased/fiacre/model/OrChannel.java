/**
 * <copyright>
 * </copyright>
 *
 * $Id: OrChannel.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Channel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.OrChannel#getLhs <em>Lhs</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.OrChannel#getRhs <em>Rhs</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getOrChannel()
 * @model
 * @generated
 */
public interface OrChannel extends Channel {
	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(Channel)
	 * @see org.topcased.fiacre.FiacrePackage#getOrChannel_Lhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Channel getLhs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.OrChannel#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(Channel value);

	/**
	 * Returns the value of the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs</em>' containment reference.
	 * @see #setRhs(Channel)
	 * @see org.topcased.fiacre.FiacrePackage#getOrChannel_Rhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Channel getRhs();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.OrChannel#getRhs <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs</em>' containment reference.
	 * @see #getRhs()
	 * @generated
	 */
	void setRhs(Channel value);

} // OrChannel
