/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArrayElem.java,v 1.3 2008-12-12 14:57:36 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Elem</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.ArrayElem#getArray <em>Array</em>}</li>
 *   <li>{@link org.topcased.fiacre.model.ArrayElem#getIndex <em>Index</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getArrayElem()
 * @model
 * @generated
 */
public interface ArrayElem extends Exp {
	/**
	 * Returns the value of the '<em><b>Array</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array</em>' containment reference.
	 * @see #setArray(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getArrayElem_Array()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getArray();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArrayElem#getArray <em>Array</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array</em>' containment reference.
	 * @see #getArray()
	 * @generated
	 */
	void setArray(Exp value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' containment reference.
	 * @see #setIndex(Exp)
	 * @see org.topcased.fiacre.FiacrePackage#getArrayElem_Index()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getIndex();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.ArrayElem#getIndex <em>Index</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' containment reference.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(Exp value);

} // ArrayElem
