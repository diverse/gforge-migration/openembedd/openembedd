/**
 * <copyright>
 * </copyright>
 *
 * $Id: To.java,v 1.3 2008-12-12 14:57:35 cbrunett Exp $
 */
package org.topcased.fiacre.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>To</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.fiacre.model.To#getDest <em>Dest</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.fiacre.FiacrePackage#getTo()
 * @model
 * @generated
 */
public interface To extends Statement {
	/**
	 * Returns the value of the '<em><b>Dest</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest</em>' reference.
	 * @see #setDest(State)
	 * @see org.topcased.fiacre.FiacrePackage#getTo_Dest()
	 * @model required="true"
	 * @generated
	 */
	State getDest();

	/**
	 * Sets the value of the '{@link org.topcased.fiacre.model.To#getDest <em>Dest</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest</em>' reference.
	 * @see #getDest()
	 * @generated
	 */
	void setDest(State value);

} // To
