/**
 * <copyright>
 * </copyright>
 *
 * $Id: FiacreSwitch.java,v 1.3 2008-12-12 14:57:37 cbrunett Exp $
 */
package org.topcased.fiacre.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.topcased.fiacre.FiacrePackage;

import org.topcased.fiacre.model.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.topcased.fiacre.FiacrePackage
 * @generated
 */
public class FiacreSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FiacrePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiacreSwitch() {
		if (modelPackage == null) {
			modelPackage = FiacrePackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		} else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(
					eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case FiacrePackage.PROGRAM: {
			Program program = (Program) theEObject;
			T result = caseProgram(program);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.DECLARATION: {
			Declaration declaration = (Declaration) theEObject;
			T result = caseDeclaration(declaration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.NODE_DECL: {
			NodeDecl nodeDecl = (NodeDecl) theEObject;
			T result = caseNodeDecl(nodeDecl);
			if (result == null)
				result = caseDeclaration(nodeDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.TYPE_DECL: {
			TypeDecl typeDecl = (TypeDecl) theEObject;
			T result = caseTypeDecl(typeDecl);
			if (result == null)
				result = caseDeclaration(typeDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CHANNEL_DECL: {
			ChannelDecl channelDecl = (ChannelDecl) theEObject;
			T result = caseChannelDecl(channelDecl);
			if (result == null)
				result = caseDeclaration(channelDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.COMPONENT_DECL: {
			ComponentDecl componentDecl = (ComponentDecl) theEObject;
			T result = caseComponentDecl(componentDecl);
			if (result == null)
				result = caseNodeDecl(componentDecl);
			if (result == null)
				result = caseDeclaration(componentDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PROCESS_DECL: {
			ProcessDecl processDecl = (ProcessDecl) theEObject;
			T result = caseProcessDecl(processDecl);
			if (result == null)
				result = caseNodeDecl(processDecl);
			if (result == null)
				result = caseDeclaration(processDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PORT_DECL: {
			PortDecl portDecl = (PortDecl) theEObject;
			T result = casePortDecl(portDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ARGUMENT_VARIABLE: {
			ArgumentVariable argumentVariable = (ArgumentVariable) theEObject;
			T result = caseArgumentVariable(argumentVariable);
			if (result == null)
				result = caseVariable(argumentVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.LOCAL_VARIABLE: {
			LocalVariable localVariable = (LocalVariable) theEObject;
			T result = caseLocalVariable(localVariable);
			if (result == null)
				result = caseVariable(localVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.COMPOSITION: {
			Composition composition = (Composition) theEObject;
			T result = caseComposition(composition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SHUFFLE: {
			Shuffle shuffle = (Shuffle) theEObject;
			T result = caseShuffle(shuffle);
			if (result == null)
				result = caseComposition(shuffle);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SYNC: {
			Sync sync = (Sync) theEObject;
			T result = caseSync(sync);
			if (result == null)
				result = caseComposition(sync);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PAR: {
			Par par = (Par) theEObject;
			T result = casePar(par);
			if (result == null)
				result = caseComposition(par);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INSTANCE: {
			Instance instance = (Instance) theEObject;
			T result = caseInstance(instance);
			if (result == null)
				result = caseComposition(instance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INTERFACED_COMP: {
			InterfacedComp interfacedComp = (InterfacedComp) theEObject;
			T result = caseInterfacedComp(interfacedComp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.EXP: {
			Exp exp = (Exp) theEObject;
			T result = caseExp(exp);
			if (result == null)
				result = caseArg(exp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.STATE: {
			State state = (State) theEObject;
			T result = caseState(state);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.TRANSITION: {
			Transition transition = (Transition) theEObject;
			T result = caseTransition(transition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.NULL_STMT: {
			NullStmt nullStmt = (NullStmt) theEObject;
			T result = caseNullStmt(nullStmt);
			if (result == null)
				result = caseStatement(nullStmt);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ASSIGNMENT: {
			Assignment assignment = (Assignment) theEObject;
			T result = caseAssignment(assignment);
			if (result == null)
				result = caseStatement(assignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.COMMUNICATION: {
			Communication communication = (Communication) theEObject;
			T result = caseCommunication(communication);
			if (result == null)
				result = caseStatement(communication);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.WHILE_STMT: {
			WhileStmt whileStmt = (WhileStmt) theEObject;
			T result = caseWhileStmt(whileStmt);
			if (result == null)
				result = caseStatement(whileStmt);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.IF_STMT: {
			IfStmt ifStmt = (IfStmt) theEObject;
			T result = caseIfStmt(ifStmt);
			if (result == null)
				result = caseStatement(ifStmt);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SELECT: {
			Select select = (Select) theEObject;
			T result = caseSelect(select);
			if (result == null)
				result = caseStatement(select);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.TO: {
			To to = (To) theEObject;
			T result = caseTo(to);
			if (result == null)
				result = caseStatement(to);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.STATEMENT: {
			Statement statement = (Statement) theEObject;
			T result = caseStatement(statement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.DETERMINISTIC_ASSIGNMENT: {
			DeterministicAssignment deterministicAssignment = (DeterministicAssignment) theEObject;
			T result = caseDeterministicAssignment(deterministicAssignment);
			if (result == null)
				result = caseAssignment(deterministicAssignment);
			if (result == null)
				result = caseStatement(deterministicAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.NON_DETERMINISTIC_ASSIGNMENT: {
			NonDeterministicAssignment nonDeterministicAssignment = (NonDeterministicAssignment) theEObject;
			T result = caseNonDeterministicAssignment(nonDeterministicAssignment);
			if (result == null)
				result = caseAssignment(nonDeterministicAssignment);
			if (result == null)
				result = caseStatement(nonDeterministicAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SINGLE_ASSIGNMENT: {
			SingleAssignment singleAssignment = (SingleAssignment) theEObject;
			T result = caseSingleAssignment(singleAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.UN_EXP: {
			UnExp unExp = (UnExp) theEObject;
			T result = caseUnExp(unExp);
			if (result == null)
				result = caseExp(unExp);
			if (result == null)
				result = caseArg(unExp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.BIN_EXP: {
			BinExp binExp = (BinExp) theEObject;
			T result = caseBinExp(binExp);
			if (result == null)
				result = caseExp(binExp);
			if (result == null)
				result = caseArg(binExp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.NAT_LITERAL: {
			NatLiteral natLiteral = (NatLiteral) theEObject;
			T result = caseNatLiteral(natLiteral);
			if (result == null)
				result = caseLiteral(natLiteral);
			if (result == null)
				result = caseExp(natLiteral);
			if (result == null)
				result = casePattern(natLiteral);
			if (result == null)
				result = caseArg(natLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.BOOL_LITERAL: {
			BoolLiteral boolLiteral = (BoolLiteral) theEObject;
			T result = caseBoolLiteral(boolLiteral);
			if (result == null)
				result = caseLiteral(boolLiteral);
			if (result == null)
				result = caseExp(boolLiteral);
			if (result == null)
				result = casePattern(boolLiteral);
			if (result == null)
				result = caseArg(boolLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.VAR_REF: {
			VarRef varRef = (VarRef) theEObject;
			T result = caseVarRef(varRef);
			if (result == null)
				result = casePattern(varRef);
			if (result == null)
				result = caseExp(varRef);
			if (result == null)
				result = caseArg(varRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ARRAY_ELEM: {
			ArrayElem arrayElem = (ArrayElem) theEObject;
			T result = caseArrayElem(arrayElem);
			if (result == null)
				result = caseExp(arrayElem);
			if (result == null)
				result = caseArg(arrayElem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.RECORD_ELEM: {
			RecordElem recordElem = (RecordElem) theEObject;
			T result = caseRecordElem(recordElem);
			if (result == null)
				result = caseExp(recordElem);
			if (result == null)
				result = caseArg(recordElem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INLINE_QUEUE: {
			InlineQueue inlineQueue = (InlineQueue) theEObject;
			T result = caseInlineQueue(inlineQueue);
			if (result == null)
				result = caseInlineCollection(inlineQueue);
			if (result == null)
				result = caseExp(inlineQueue);
			if (result == null)
				result = caseArg(inlineQueue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.TYPE: {
			Type type = (Type) theEObject;
			T result = caseType(type);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.BASIC_TYPE: {
			BasicType basicType = (BasicType) theEObject;
			T result = caseBasicType(basicType);
			if (result == null)
				result = caseType(basicType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.BOOL_TYPE: {
			BoolType boolType = (BoolType) theEObject;
			T result = caseBoolType(boolType);
			if (result == null)
				result = caseBasicType(boolType);
			if (result == null)
				result = caseType(boolType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.NAT_TYPE: {
			NatType natType = (NatType) theEObject;
			T result = caseNatType(natType);
			if (result == null)
				result = caseBasicType(natType);
			if (result == null)
				result = caseType(natType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INT_TYPE: {
			IntType intType = (IntType) theEObject;
			T result = caseIntType(intType);
			if (result == null)
				result = caseBasicType(intType);
			if (result == null)
				result = caseType(intType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INTERVAL: {
			Interval interval = (Interval) theEObject;
			T result = caseInterval(interval);
			if (result == null)
				result = caseType(interval);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.RECORD: {
			Record record = (Record) theEObject;
			T result = caseRecord(record);
			if (result == null)
				result = caseType(record);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.FIELD: {
			Field field = (Field) theEObject;
			T result = caseField(field);
			if (result == null)
				result = caseLabeledType(field);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ARRAY: {
			Array array = (Array) theEObject;
			T result = caseArray(array);
			if (result == null)
				result = caseType(array);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.QUEUE: {
			Queue queue = (Queue) theEObject;
			T result = caseQueue(queue);
			if (result == null)
				result = caseType(queue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.TYPE_ID: {
			TypeId typeId = (TypeId) theEObject;
			T result = caseTypeId(typeId);
			if (result == null)
				result = caseType(typeId);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CHANNEL: {
			Channel channel = (Channel) theEObject;
			T result = caseChannel(channel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CHANNEL_ID: {
			ChannelId channelId = (ChannelId) theEObject;
			T result = caseChannelId(channelId);
			if (result == null)
				result = caseChannel(channelId);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PROFILE: {
			Profile profile = (Profile) theEObject;
			T result = caseProfile(profile);
			if (result == null)
				result = caseChannel(profile);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.OR_CHANNEL: {
			OrChannel orChannel = (OrChannel) theEObject;
			T result = caseOrChannel(orChannel);
			if (result == null)
				result = caseChannel(orChannel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INLINE_ARRAY: {
			InlineArray inlineArray = (InlineArray) theEObject;
			T result = caseInlineArray(inlineArray);
			if (result == null)
				result = caseInlineCollection(inlineArray);
			if (result == null)
				result = caseExp(inlineArray);
			if (result == null)
				result = caseArg(inlineArray);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INLINE_RECORD: {
			InlineRecord inlineRecord = (InlineRecord) theEObject;
			T result = caseInlineRecord(inlineRecord);
			if (result == null)
				result = caseExp(inlineRecord);
			if (result == null)
				result = caseArg(inlineRecord);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.VALUED_FIELD: {
			ValuedField valuedField = (ValuedField) theEObject;
			T result = caseValuedField(valuedField);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SYNCHRONIZATION: {
			Synchronization synchronization = (Synchronization) theEObject;
			T result = caseSynchronization(synchronization);
			if (result == null)
				result = caseCommunication(synchronization);
			if (result == null)
				result = caseStatement(synchronization);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.RECEPTION: {
			Reception reception = (Reception) theEObject;
			T result = caseReception(reception);
			if (result == null)
				result = caseCommunication(reception);
			if (result == null)
				result = caseStatement(reception);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.EMISSION: {
			Emission emission = (Emission) theEObject;
			T result = caseEmission(emission);
			if (result == null)
				result = caseCommunication(emission);
			if (result == null)
				result = caseStatement(emission);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.SEQ: {
			Seq seq = (Seq) theEObject;
			T result = caseSeq(seq);
			if (result == null)
				result = caseStatement(seq);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.LOCAL_PORT_DECL: {
			LocalPortDecl localPortDecl = (LocalPortDecl) theEObject;
			T result = caseLocalPortDecl(localPortDecl);
			if (result == null)
				result = casePortDecl(localPortDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PARAM_PORT_DECL: {
			ParamPortDecl paramPortDecl = (ParamPortDecl) theEObject;
			T result = caseParamPortDecl(paramPortDecl);
			if (result == null)
				result = casePortDecl(paramPortDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.VARIABLE: {
			Variable variable = (Variable) theEObject;
			T result = caseVariable(variable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PRIORITY: {
			Priority priority = (Priority) theEObject;
			T result = casePriority(priority);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CONSTR_EXP: {
			ConstrExp constrExp = (ConstrExp) theEObject;
			T result = caseConstrExp(constrExp);
			if (result == null)
				result = caseExp(constrExp);
			if (result == null)
				result = caseArg(constrExp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.UNION: {
			Union union = (Union) theEObject;
			T result = caseUnion(union);
			if (result == null)
				result = caseType(union);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.PATTERN: {
			Pattern pattern = (Pattern) theEObject;
			T result = casePattern(pattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CONSTR_PATTERN: {
			ConstrPattern constrPattern = (ConstrPattern) theEObject;
			T result = caseConstrPattern(constrPattern);
			if (result == null)
				result = casePattern(constrPattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ARRAY_PATTERN: {
			ArrayPattern arrayPattern = (ArrayPattern) theEObject;
			T result = caseArrayPattern(arrayPattern);
			if (result == null)
				result = casePattern(arrayPattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.FIELD_PATTERN: {
			FieldPattern fieldPattern = (FieldPattern) theEObject;
			T result = caseFieldPattern(fieldPattern);
			if (result == null)
				result = casePattern(fieldPattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ANY_PATTERN: {
			AnyPattern anyPattern = (AnyPattern) theEObject;
			T result = caseAnyPattern(anyPattern);
			if (result == null)
				result = casePattern(anyPattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.LITERAL: {
			Literal literal = (Literal) theEObject;
			T result = caseLiteral(literal);
			if (result == null)
				result = caseExp(literal);
			if (result == null)
				result = casePattern(literal);
			if (result == null)
				result = caseArg(literal);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CASE_STMT: {
			CaseStmt caseStmt = (CaseStmt) theEObject;
			T result = caseCaseStmt(caseStmt);
			if (result == null)
				result = caseStatement(caseStmt);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.RULE: {
			Rule rule = (Rule) theEObject;
			T result = caseRule(rule);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.ARG: {
			Arg arg = (Arg) theEObject;
			T result = caseArg(arg);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.REF_ARG: {
			RefArg refArg = (RefArg) theEObject;
			T result = caseRefArg(refArg);
			if (result == null)
				result = caseArg(refArg);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CONSTANT_DECL: {
			ConstantDecl constantDecl = (ConstantDecl) theEObject;
			T result = caseConstantDecl(constantDecl);
			if (result == null)
				result = caseDeclaration(constantDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CONSTANT_REF: {
			ConstantRef constantRef = (ConstantRef) theEObject;
			T result = caseConstantRef(constantRef);
			if (result == null)
				result = caseExp(constantRef);
			if (result == null)
				result = casePattern(constantRef);
			if (result == null)
				result = caseArg(constantRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INLINE_COLLECTION: {
			InlineCollection inlineCollection = (InlineCollection) theEObject;
			T result = caseInlineCollection(inlineCollection);
			if (result == null)
				result = caseExp(inlineCollection);
			if (result == null)
				result = caseArg(inlineCollection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.CONSTR: {
			Constr constr = (Constr) theEObject;
			T result = caseConstr(constr);
			if (result == null)
				result = caseLabeledType(constr);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.LABELED_TYPE: {
			LabeledType labeledType = (LabeledType) theEObject;
			T result = caseLabeledType(labeledType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.FINITE_BOUND: {
			FiniteBound finiteBound = (FiniteBound) theEObject;
			T result = caseFiniteBound(finiteBound);
			if (result == null)
				result = caseMinBound(finiteBound);
			if (result == null)
				result = caseMaxBound(finiteBound);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.INFINITE_BOUND: {
			InfiniteBound infiniteBound = (InfiniteBound) theEObject;
			T result = caseInfiniteBound(infiniteBound);
			if (result == null)
				result = caseMaxBound(infiniteBound);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.MIN_BOUND: {
			MinBound minBound = (MinBound) theEObject;
			T result = caseMinBound(minBound);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.MAX_BOUND: {
			MaxBound maxBound = (MaxBound) theEObject;
			T result = caseMaxBound(maxBound);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.COND_EXP: {
			CondExp condExp = (CondExp) theEObject;
			T result = caseCondExp(condExp);
			if (result == null)
				result = caseExp(condExp);
			if (result == null)
				result = caseArg(condExp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FiacrePackage.FOREACH: {
			Foreach foreach = (Foreach) theEObject;
			T result = caseForeach(foreach);
			if (result == null)
				result = caseStatement(foreach);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Program</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProgram(Program object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclaration(Declaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeDecl(NodeDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeDecl(TypeDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Channel Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Channel Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChannelDecl(ChannelDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDecl(ComponentDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessDecl(ProcessDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortDecl(PortDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Argument Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Argument Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArgumentVariable(ArgumentVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalVariable(LocalVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComposition(Composition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shuffle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shuffle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShuffle(Shuffle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSync(Sync object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Par</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Par</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePar(Par object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstance(Instance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interfaced Comp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interfaced Comp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfacedComp(InterfacedComp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExp(Exp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Stmt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullStmt(NullStmt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssignment(Assignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunication(Communication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Stmt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhileStmt(WhileStmt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Stmt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfStmt(IfStmt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Select</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Select</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelect(Select object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTo(To object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatement(Statement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deterministic Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deterministic Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeterministicAssignment(DeterministicAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Non Deterministic Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Non Deterministic Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNonDeterministicAssignment(NonDeterministicAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleAssignment(SingleAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Un Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Un Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnExp(UnExp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bin Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bin Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinExp(BinExp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nat Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nat Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNatLiteral(NatLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bool Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bool Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoolLiteral(BoolLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Var Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Var Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVarRef(VarRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Elem</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Elem</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayElem(ArrayElem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Elem</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Elem</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordElem(RecordElem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Queue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Queue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlineQueue(InlineQueue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicType(BasicType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bool Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bool Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoolType(BoolType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nat Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nat Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNatType(NatType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntType(IntType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterval(Interval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecord(Record object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseField(Field object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArray(Array object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Queue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Queue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQueue(Queue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeId(TypeId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Channel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Channel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChannel(Channel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Channel Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Channel Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChannelId(ChannelId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Profile</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Profile</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProfile(Profile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Channel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Channel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrChannel(OrChannel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Array</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Array</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlineArray(InlineArray object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Record</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Record</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlineRecord(InlineRecord object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Valued Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Valued Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValuedField(ValuedField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronization(Synchronization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reception</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reception</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReception(Reception object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Emission</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Emission</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEmission(Emission object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Seq</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSeq(Seq object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Port Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Port Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalPortDecl(LocalPortDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Param Port Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Param Port Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParamPortDecl(ParamPortDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriority(Priority object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constr Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constr Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrExp(ConstrExp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Union</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Union</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnion(Union object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePattern(Pattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constr Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constr Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrPattern(ConstrPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayPattern(ArrayPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFieldPattern(FieldPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Any Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Any Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnyPattern(AnyPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLiteral(Literal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Stmt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Stmt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseStmt(CaseStmt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRule(Rule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arg</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arg</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArg(Arg object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Arg</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Arg</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefArg(RefArg object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantDecl(ConstantDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantRef(ConstantRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlineCollection(InlineCollection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constr</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstr(Constr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Labeled Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Labeled Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabeledType(LabeledType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Finite Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Finite Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFiniteBound(FiniteBound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Infinite Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Infinite Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInfiniteBound(InfiniteBound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Min Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Min Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMinBound(MinBound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxBound(MaxBound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cond Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cond Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondExp(CondExp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Foreach</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Foreach</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForeach(Foreach object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //FiacreSwitch
