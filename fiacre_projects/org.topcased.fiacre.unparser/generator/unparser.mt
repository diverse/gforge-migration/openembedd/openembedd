<%
metamodel http://org.topcased.fiacre
%>

<%-- ======================	declarations ====================== --%>

<%script name="UnparseProgram" type="fiacre.Program" file="<%root.name%>.fcr"%>
<%declarations.UnparseDeclaration.sep("\n\n")%>
<%root.name%>
<%""%>

<%script name="UnparseDeclaration" type="fiacre.Declaration"%>
<declaration>

<%script name="UnparseDeclaration" type="fiacre.ProcessDecl"%>
process <%name%><%UnparseNodeDecl%> is
	states <%states.name.sep(", ")%>
<%if (!(vars.nSize() == 0)){%>
	var <%vars.UnparseVarDecl().sep(", ")%>
<%}%>
<%if (!(localPorts.nSize() == 0)){%>
	port
<%localPorts.UnparsePortDecl().indentTab.sep(",\n").indentTab()%>
<%}%>
	init
	<%initAction.UnparseStatement.indentTab%>
<%transitions.UnparseTransition().sep("\n").indentTab()%>

<%script name="UnparsePriority" type="fiacre.Priority"%>
<%sup.name.sep("|")%> > <%inf.name.sep("|")%>

<%script name="UnparseDeclaration" type="fiacre.ComponentDecl"%>
component <%name%><%UnparseNodeDecl%> is
<%if (!(vars.nSize() == 0)){%>
	var <%vars.UnparseVarDecl().sep(",")%>
<%}%>
<%if (!(localPorts.nSize() == 0)){%>
	port
<%localPorts.UnparsePortDecl().indentTab.sep(",\n").indentTab()%>
<%}%>
<%if (!(priority.nSize() == 0)){%>
	priority
<%priority.UnparsePriority().indentTab.sep(",\n").indentTab%>
<%}%>
	<%body.UnparseComposition%>

<%script name="UnparseDeclaration" type="fiacre.TypeDecl"%>
type <%name%> is <%is.UnparseType%>

<%script name="UnparseDeclaration" type="fiacre.ChannelDecl"%>
channel <%name%> is <%is.UnparseChannel%>

<%script name="UnparseDeclaration" type="fiacre.ConstantDecl"%>
const <%name%>: <%type.UnparseType%> is <%value.UnparseExp%>

<%-- ==============================	composition ====================== --%>

<%script name="UnparseArg" type="fiacre.Arg"%>
<arg>

<%script name="UnparseArg" type="fiacre.Exp"%>
<%UnparseExp%>

<%script name="UnparseArg" type="fiacre.RefArg"%>
&<%ref.name%>

<%script name="UnparseComposition" type="fiacre.Composition"%>
<composition>

<%script name="UnparseComposition" type="fiacre.Sync"%>
par
 * -> <%args.UnparseComposition.indentTab.sep("\n || * -> ").indentTab()%>
end

<%script name="UnparseComposition" type="fiacre.Shuffle"%>
par
  -> <%args.UnparseComposition.indentTab.sep("\n || -> ").indentTab%>
end

<%script name="UnparseComposition" type="fiacre.Par"%>
par
<%args.UnparsePar.indentTab.sep("\n || ").indentTab%>
end

<%script name="UnparseComposition" type="fiacre.Instance"%>
<%type.name%><%if (ports) {%>[<%ports.name.sep(",")%>]<%}%><%if (args) {%>(<%args.UnparseArg.sep(",")%>)<%}%>

<%script name="UnparsePar" type="fiacre.InterfacedComp"%>
<%if (syncPorts) {%><%syncPorts.name.sep(",")%> -> <%}%><%composition.UnparseComposition%>

<%-- ==============================	channels ====================== --%>

<%script name="UnparseChannel" type="fiacre.Channel"%>
<channel>

<%script name="UnparseChannel" type="fiacre.ChannelId"%>
<%decl.name%>

<%script name="UnparseChannel" type="fiacre.Profile"%>
<%if (types.nSize() == 0){%>none<%}else{%><%types.UnparseType.sep("#")%><%}%>

<%script name="UnparseChannel" type="fiacre.OrChannel"%>
<%if (lhs) {%><%lhs.UnparseChannel%><%}else{%>none<%}%> | <%if (rhs) {%><%rhs.UnparseChannel%><%}else{%>none<%}%>

<%-- ==============================	processes ====================== --%>

<%script name="UnparseMin" type="MinBound"%>
<%script name="UnparseMax" type="MaxBound"%>

<%script name="UnparseMin" type="FiniteBound"%>
<%if (strict) {%>]<%}else{%>[<%}%><%val.toString.replaceAll("\D","")%>

<%script name="UnparseMax" type="FiniteBound"%>
<%val.toString.replaceAll("\D","").%><%if (strict) {%>[<%}else{%>]<%}%>

<%script name="UnparseMax" type="InfiniteBound"%>
...[

<%script name="UnparsePortTiming" type="fiacre.LocalPortDecl"%>
 in <%mini.UnparseMin()%>, <%maxi.UnparseMax()%>
 
<%script name="UnparsePortDecl" type="fiacre.PortDecl"%>
<%name%> : <%if (in) {%>in <%}%><%if (out) {%>out <%}%><%if (channel) {%><%channel.UnparseChannel%><%}else{%> none <%}%>

<%script name="UnparsePortDecl" type="fiacre.LocalPortDecl"%>
<%name%> : <%if (in) {%>in <%}%><%if (out) {%>out <%}%><%if (channel) {%><%channel.UnparseChannel%><%}else{%> none <%}%><%UnparsePortTiming%>

<%script name="UnparseArgDecl" type="fiacre.ArgumentVariable"%>
<%if (ref) {%>&<%}%><%name%> :	<%if (read) {%>read <%}%><%if (write) {%>write <%}%> <%type.UnparseType%>

<%script name="UnparseVarDecl" type="fiacre.LocalVariable"%>
<%name%> : <%type.UnparseType%><%if (initializer != null) {%> := <%initializer.UnparseExp%> <%}%>

<%script name="UnparseNodeDecl" type="fiacre.NodeDecl"%>
<%if (ports.nSize() > 0) {%>[<%ports.UnparsePortDecl.sep(",")%>]<%}%><%if (args.nSize() > 0) {%>(<%args.UnparseArgDecl.sep(",")%>)<%}%>

<%script name="UnparseTransition" type="fiacre.Transition"%>
<%if (name) {%><%name%>: <%}%>from <%from.name%>
<%action.UnparseStatement.indentTab()%>

<%-- ==============================	types ====================== --%>

<%script name="UnparseType" type="fiacre.Type"%>
<type>

<%script name="UnparseType" type="fiacre.BoolType"%>
bool

<%script name="UnparseType" type="fiacre.IntType"%>
int

<%script name="UnparseType" type="fiacre.NatType"%>
nat

<%script name="UnparseType" type="fiacre.Interval"%>
<%""+mini%>..<%""+maxi%>

<%script name="UnparseType" type="fiacre.TypeId"%>
<%decl.name%>

<%script name="UnparseFieldDecl" type="fiacre.Field"%>
<%name%>: <%type.UnparseType.trim%>

<%script name="UnparseConstrDecl" type="fiacre.Constr"%>
	<%name%><%if (type) {%> of <%type.UnparseType.trim%><%}%>

<%script name="UnparseType" type="fiacre.Record"%>
record
<%fields.UnparseFieldDecl.sep(",\n").indentTab%>
end

<%script name="UnparseType" type="fiacre.Union"%>
union
<%constr.UnparseConstrDecl.sepStr("\n|")%>
end

<%script name="UnparseType" type="fiacre.Array"%>
array <%""+size%> of <%type.UnparseType%>

<%script name="UnparseType" type="fiacre.Queue"%>
queue <%""+size%> of <%type.UnparseType%>

<%-- ==============================	operateurs ====================== --%>

<%script name="UnparseBop" type="fiacre.BinExp"%>
<%if (args(0) == "BOR"){%> or <%}%>
<%if (args(0) == "BAND"){%> and <%}%>
<%if (args(0) == "BEQ"){%>=<%}%>
<%if (args(0) == "BNE"){%><><%}%>
<%if (args(0) == "BLT"){%><<%}%>
<%if (args(0) == "BGT"){%>><%}%>
<%if (args(0) == "BLE"){%><=<%}%>
<%if (args(0) == "BGE"){%>>=<%}%>
<%if (args(0) == "BADD"){%>+<%}%>
<%if (args(0) == "BMINUS"){%>-<%}%>
<%if (args(0) == "BMUL"){%>*<%}%>
<%if (args(0) == "BDIV"){%>/<%}%>
<%if (args(0) == "BMOD"){%>%<%}%>
<%if (args(0) == "ENQUEUE"){%> enqueue <%}%>
<%if (args(0) == "APPEND"){%> append <%}%>

<%script name="UnparseUop" type="fiacre.UnExp"%>
<%if (args(0) == "UMINUS"){%>-<%}%>
<%if (args(0) == "UDOLLARD"){%>$<%}%>
<%if (args(0) == "UNOT"){%>not<%}%>
<%if (args(0) == "UFULL"){%>full<%}%>
<%if (args(0) == "UEMPTY"){%>empty<%}%>
<%if (args(0) == "DEQUEUE"){%>dequeue<%}%>
<%if (args(0) == "FIRST"){%>first<%}%>

<%script name="UnparseValuedField" type="fiacre.ValuedField"%>
<%field.name%>=<%value.UnparseExp%>

<%-- ==============================	patterns ====================== --%>

<%script name="UnparsePattern" type="fiacre.Pattern"%>
<pattern>

<%script name="UnparsePattern" type="fiacre.ConstrPattern"%>
<%if (arg) {%> (<%constr.name%> <%arg.UnparsePattern%>) <%}else{%> <%constr.name%><%}%>

<%script name="UnparsePattern" type="fiacre.VarRef"%>
<%decl.name%>

<%script name="UnparsePattern" type="fiacre.Literal"%>
<%UnparseExp%>

<%script name="UnparsePattern" type="fiacre.ArrayPattern"%>
<%array.UnparsePattern%>[<%index.UnparseExp%>]

<%script name="UnparsePattern" type="fiacre.FieldPattern"%>
(<%record.UnparsePattern%>.<%field.name%>)

<%script name="UnparsePattern" type="fiacre.AnyPattern"%>
any

<%-- ==============================	expressions ====================== --%>

<%script name="UnparseExp" type="fiacre.Exp"%>
<exp>

<%script name="UnparseExp" type="fiacre.BoolLiteral"%>
<%""+value%>

<%script name="UnparseExp" type="fiacre.NatLiteral"%>
<%self.value%>

<%script name="UnparseExp" type="fiacre.VarRef"%>
<%decl.name%>

<%script name="UnparseExp" type="fiacre.BinExp"%>
<%if (binOp == "ENQUEUE"){%> enqueue(<%l_exp.UnparseExp%>,<%r_exp.UnparseExp%>)<%}else{%>(<%l_exp.UnparseExp%><%UnparseBop(binOp)%><%r_exp.UnparseExp%>)<%}%>

<%script name="UnparseExp" type="fiacre.UnExp"%>
(<%UnparseUop(unop)%> <%exp.UnparseExp%>)

<%script name="UnparseExp" type="fiacre.RecordElem"%>
(<%record.UnparseExp%>.<%field.name%>)

<%script name="UnparseExp" type="fiacre.ConstrExp"%>
<%if (arg){%>(<%constr.name%> <%arg.UnparseExp%>)<%}else{%><%constr.name%><%}%>

<%script name="UnparseExp" type="fiacre.ArrayElem"%>
<%array.UnparseExp%>[<%index.UnparseExp%>]

<%script name="UnparseExp" type="fiacre.CondExp"%>
(<%cond.UnparseExp%> ? <%ift.UnparseExp%> : <%iff.UnparseExp%>)

<%script name="UnparseExp" type="fiacre.InlineRecord"%>
{<%values.UnparseValuedField.sep(",")%>}

<%script name="UnparseExp" type="fiacre.InlineArray"%>
[<%elems.UnparseExp.sep(",")%>]

<%script name="UnparseExp" type="fiacre.InlineQueue"%>
{|<%elems.UnparseExp.sep(",")%>|}

<%script name="UnparseExp" type="fiacre.ConstantRef"%>
<%decl.name%>

<%-- ==============================	actions ====================== --%>

<%script name="UnparseRule" type="fiacre.Rule"%>
<%lhs.UnparsePattern%> -> <%action.UnparseStatement%>

<%script name="UnparseStatement" type="fiacre.Statement"%>
<statement>

<%script name="UnparseStatement" type="fiacre.DeterministicAssignment"%>
<%assignments.lhs.UnparsePattern.sep(", ")%> := <%assignments.rhs.UnparseExp.sep(",")%>

<%script name="UnparseStatement" type="fiacre.NonDeterministicAssignment"%>
<% lhs.UnparsePattern.sep(",")%> := any<%if (condition){%> where <%condition.UnparseExp%><%}%>

<%script name="UnparseStatement" type="fiacre.To"%>
to <%dest.name%>

<%script name="UnparseStatement" type="fiacre.Seq"%>
<%statements.UnparseStatement.sepStr(";\n")%>

<%script name="UnparseStatement" type="fiacre.Select"%>
select
<%statements.UnparseStatement.indentTab.sepStr("\n[]")%>
end

<%script name="UnparseStatement" type="fiacre.Foreach"%>
foreach <%iter.name%> do
<%body.UnparseStatement.indentTab%>
end

<%script name="UnparseStatement" type="fiacre.CaseStmt"%>
case <%exp.UnparseExp%> of
<%rules.UnparseRule.indentTab.sepStr("\n|").indentTab%>
end

<%script name="UnparseStatement" type="fiacre.Emission"%>
<%port.name%>!<% args.UnparseExp.sep(",")%>

<%script name="UnparseStatement" type="fiacre.Reception"%>
<%port.name%>?<%pattern.UnparsePattern.sep(",")%><%if (where){%> where <%where.UnparseExp%> <%}%>

<%script name="UnparseStatement" type="fiacre.Synchronization"%>
<%port.name%>

<%script name="UnparseStatement" type="fiacre.NullStmt"%>
null

<%script name="UnparseStatement" type="fiacre.IfStmt"%>
if <%condition.UnparseExp%> then
<%then.UnparseStatement.indentTab%>
<%if (else){%>
else
<%else.UnparseStatement.indentTab%>
<%}%>
end

<%script name="UnparseStatement" type="fiacre.WhileStmt"%>
while <%condition.UnparseExp%> do
<%body.UnparseStatement.indentTab%>
end
