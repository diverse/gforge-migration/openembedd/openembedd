package org.topcased.fiacre.unparser;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import fr.obeo.acceleo.gen.template.Template;
import fr.obeo.acceleo.gen.template.eval.LaunchManager;
import fr.obeo.acceleo.gen.template.scripts.SpecificScript;
import fr.obeo.acceleo.tools.plugins.AcceleoModuleProvider;

public class UnparseAction implements IObjectActionDelegate {

	private ISelection selection;

	/**
	 * Constructor for Action1.
	 */
	public UnparseAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		String generatorPath = "generator/unparser.mt";
		if (! (selection instanceof IStructuredSelection)) return;
        IStructuredSelection sel = (IStructuredSelection) selection;
        IFile f = (IFile) sel.getFirstElement();
        String name = f.getProject().getName() + "/" + f.getProjectRelativePath();
        URI uri = URI.createPlatformResourceURI(name, false);

		final XMIResource resource = new XMIResourceImpl(uri);
		try {
			resource.load(null);
		}
		catch (Exception e) {
			Shell shell = new Shell();
			MessageDialog.openInformation(
					shell,
					"Fiacre unparser",
					"Selection not found:"+uri);
			return;
		}
		String result;
		File script = AcceleoModuleProvider.getDefault().getFile("org.topcased.fiacre.unparser",new Path (generatorPath));
		if (script == null || !script.exists()) {
			Shell shell = new Shell();
			MessageDialog.openInformation(
					shell,
					"Unparser",
					"Script not found.");
			return;
		}
		SpecificScript aScript = new SpecificScript(script, null);
		try {
			aScript.reset();
		}
		catch (Exception e) {
			Shell shell = new Shell();
			MessageDialog.openInformation(
					shell,
					"Fiacre unparser",
					"Invalid script.");
			return;
		}
		EObject root = resource.getContents().get(0);
		try{
			Template template = aScript.getRootTemplate(root, true);
			result = template.evaluate(root, LaunchManager.create("run", true)).toString();
		}
		catch (Exception e) {
			Shell shell = new Shell();
			MessageDialog.openInformation(
					shell,
					"Fiacre unparser",
					"run time error in script");
			return;
		}
		
		URI out = URI.createFileURI(f.getProject().getName() + "/" + f.getProjectRelativePath().removeFileExtension()+".fcr");
		IFile o = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(out.toString()));
		try {
			if (o.exists()) o.delete(true, false, null);
			o.create(new ByteArrayInputStream(result.getBytes()), IResource.REPLACE|IResource.FORCE, null);
		}
		catch (Exception e) {
			Shell shell = new Shell();
			MessageDialog.openInformation(
					shell,
					"Fiacre unparser",
					"write error:"+e.getMessage());
			return;
		}
		Shell shell = new Shell();
		MessageDialog.openInformation(
			shell,
			"Unparser",
			"unparse was executed.");
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
