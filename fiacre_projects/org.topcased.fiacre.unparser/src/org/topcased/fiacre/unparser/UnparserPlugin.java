package org.topcased.fiacre.unparser;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The main plugin class to be used in the desktop.
 */
public class UnparserPlugin extends AbstractUIPlugin {

		//The shared instance.
		private static UnparserPlugin plugin;
		//Resource bundle.
		private ResourceBundle resourceBundle;
		
		/**
		 * The constructor.
		 */
		public UnparserPlugin() {
			super();
			plugin = this;
			try {
				resourceBundle = ResourceBundle.getBundle("org.topcased.fiacre.unparser.UnparserPlugin");
			} catch (MissingResourceException x) {
				resourceBundle = null;
			}
		}

		/**
		 * This method is called upon plug-in activation
		 */
		public void start(BundleContext context) throws Exception {
			super.start(context);
		}

		/**
		 * This method is called when the plug-in is stopped
		 */
		public void stop(BundleContext context) throws Exception {
			super.stop(context);
		}

		/**
		 * Returns the shared instance.
		 */
		public static UnparserPlugin getDefault() {
			return plugin;
		}

		/**
		 * Returns the string from the plugin's resource bundle,
		 * or 'key' if not found.
		 */
		public static String getResourceString(String key) {
			ResourceBundle bundle = UnparserPlugin.getDefault().getResourceBundle();
			try {
				return (bundle != null) ? bundle.getString(key) : key;
			} catch (MissingResourceException e) {
				return key;
			}
		}

		/**
		 * Returns the plugin's resource bundle,
		 */
		public ResourceBundle getResourceBundle() {
			return resourceBundle;
		}

		public static void logErrorMessage(String message) {
			log(new Status(IStatus.ERROR, getPluginId(), Status.OK, message, null));
		}

		public static void log(IStatus status) {
			getDefault().getLog().log(status);
		}

		public static String getPluginId() {
			return plugin.getBundle().getSymbolicName();
		}
	}

