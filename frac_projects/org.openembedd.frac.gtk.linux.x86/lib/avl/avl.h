
#include "stdlib.h"

/* storage interface */

/* all storage libs provide: */

/* values as seen by applications : */
typedef long key;

/* initialization function (possibly with empty body) */
/* called by applications before any lookup or store */
extern int init_storage();

/* lookup : key -> value
   given a key, retreives the data value stored under that key.
   key is assumed defined.
   For avl's, assuming values are structures, lookup just returns
   the key cast to a value pointer:
*/
#define lookup(k) ((void*)(k))

/* store : value -> key
   stores a value in the database, returning an access key.
   if data is new, returns a new key, otherwise frees the
   data and returns the key of the equal data stored in the database.
*/
extern key SearchInsert();
#define store(data) (SearchInsert(data))


/* avl requires a comparison function for user data:
      compare_value : data * data -> int
   and a freeing function for values:
      free_value : (void*) -> void
   These are passed as arguments to init_storage.
*/



