/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SynDEx.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Syn DEx</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.SynDEx#getSynDExelements <em>Syn DExelements</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getSynDEx()
 * @model
 * @generated
 */
public interface SynDEx extends EObject
{
	/**
	 * Returns the value of the '<em><b>Syn DExelements</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.Element}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Syn DExelements</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Syn DExelements</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getSynDEx_SynDExelements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Element> getSynDExelements();

} // SynDEx
