/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ElementaryStructure.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Elementary Structure</b></em>'. <!--
 * end-user-doc -->
 * 
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getElementaryStructure()
 * @model
 * @generated
 */
public interface ElementaryStructure extends InternalStructure
{} // ElementaryStructure
