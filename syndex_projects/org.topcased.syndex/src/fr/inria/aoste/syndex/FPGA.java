/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: FPGA.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>FPGA</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getFPGA()
 * @model
 * @generated
 */
public interface FPGA extends Processor
{} // FPGA
