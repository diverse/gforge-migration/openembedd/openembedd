/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AtomicApplicationComponent.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Atomic Application Component</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.AtomicApplicationComponent#getAtomicApplicationStructure <em>Atomic Application
 * Structure</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getAtomicApplicationComponent()
 * @model abstract="true" annotation="http://www.topcased.org/uuid uuid='11501873764313'"
 * @generated
 */
public interface AtomicApplicationComponent extends ApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>Atomic Application Structure</b></em>' containment reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Application Structure</em>' containment reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Atomic Application Structure</em>' containment reference.
	 * @see #setAtomicApplicationStructure(ElementaryStructure)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAtomicApplicationComponent_AtomicApplicationStructure()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ElementaryStructure getAtomicApplicationStructure();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AtomicApplicationComponent#getAtomicApplicationStructure
	 * <em>Atomic Application Structure</em>}' containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Atomic Application Structure</em>' containment reference.
	 * @see #getAtomicApplicationStructure()
	 * @generated
	 */
	void setAtomicApplicationStructure(ElementaryStructure value);

} // AtomicApplicationComponent
