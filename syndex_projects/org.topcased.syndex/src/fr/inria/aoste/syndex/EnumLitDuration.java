/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: EnumLitDuration.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Enum Lit Duration</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.EnumLitDuration#getValue <em>Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration <em>Parent Enum Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getEnumLitDuration()
 * @model
 * @generated
 */
public interface EnumLitDuration extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getEnumLitDuration_Value()
	 * @model required="true"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.EnumLitDuration#getValue <em>Value</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

	/**
	 * Returns the value of the '<em><b>Parent Enum Duration</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link fr.inria.aoste.syndex.Duration#getEnumDuration <em>Enum Duration</em>}'. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Parent Enum Duration</em>' container reference isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Enum Duration</em>' container reference.
	 * @see #setParentEnumDuration(Duration)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getEnumLitDuration_ParentEnumDuration()
	 * @see fr.inria.aoste.syndex.Duration#getEnumDuration
	 * @model opposite="enumDuration" required="true" transient="false"
	 * @generated
	 */
	Duration getParentEnumDuration();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration
	 * <em>Parent Enum Duration</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Enum Duration</em>' container reference.
	 * @see #getParentEnumDuration()
	 * @generated
	 */
	void setParentEnumDuration(Duration value);

} // EnumLitDuration
