package fr.inria.aoste.syndex.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import fr.inria.aoste.syndex.SynDExPlugin;
import fr.inria.aoste.syndex.m2t.jobs.ModelToTextJob;
import fr.inria.aoste.syndex.presentation.SyndexEditor;

public class GenerateSDXAction extends Action
{
	private EditingDomainActionBarContributor	actionBar;

	/**
	 * @param actionBar
	 */
	public GenerateSDXAction(EditingDomainActionBarContributor actionBar)
	{
		super("Generate the SDX textual file", AbstractUIPlugin.imageDescriptorFromPlugin(SynDExPlugin.getPlugin()
				.getSymbolicName(), "icons/SDX.gif"));
		this.actionBar = actionBar;
	}

	/**
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		IEditorPart editor = actionBar.getActiveEditor();
		if (!(editor instanceof SyndexEditor))
		{
			SynDExPlugin.getPlugin().getLog().log(
				new Status(IStatus.WARNING, SynDExPlugin.getPlugin().getSymbolicName(),
					"The SynDEx editor is not active."));
			return;
		}

		IFile syndexFile = null;
		EList<Resource> res = ((SyndexEditor) editor).getEditingDomain().getResourceSet().getResources();
		if (res == null || res.isEmpty())
		{
			SynDExPlugin.getPlugin().getLog().log(
				new Status(IStatus.WARNING, SynDExPlugin.getPlugin().getSymbolicName(),
					"No resource edited in the SynDEx editor."));
			return;
		}
		Resource resource = res.get(0);

		URI uri = resource.getURI();
		uri = resource.getResourceSet().getURIConverter().normalize(uri);
		String scheme = uri.scheme();
		if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0)))
		{
			String path = uri.toPlatformString(true);
			syndexFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(path));
		}

		if (syndexFile != null)
		{
			ModelToTextJob execute = new ModelToTextJob(syndexFile);
			execute.setUser(true);
			execute.schedule();
		}
		else
		{
			MessageDialog.openError(new Shell(), "SDX Generation", "No SynDEx file found.");
		}
	}
}
