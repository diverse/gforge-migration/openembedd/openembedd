/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ParameterValue.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Parameter Value</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ParameterValue#getValueParameter <em>Value Parameter</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ParameterValue#getParamDeclaration <em>Param Declaration</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getParameterValue()
 * @model
 * @generated
 */
public interface ParameterValue extends Element
{
	/**
	 * Returns the value of the '<em><b>Value Parameter</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Parameter</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value Parameter</em>' attribute.
	 * @see #setValueParameter(String)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getParameterValue_ValueParameter()
	 * @model
	 * @generated
	 */
	String getValueParameter();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ParameterValue#getValueParameter <em>Value Parameter</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Value Parameter</em>' attribute.
	 * @see #getValueParameter()
	 * @generated
	 */
	void setValueParameter(String value);

	/**
	 * Returns the value of the '<em><b>Param Declaration</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Param Declaration</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Param Declaration</em>' reference.
	 * @see #setParamDeclaration(ParameterDeclaration)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getParameterValue_ParamDeclaration()
	 * @model
	 * @generated
	 */
	ParameterDeclaration getParamDeclaration();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ParameterValue#getParamDeclaration
	 * <em>Param Declaration</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Param Declaration</em>' reference.
	 * @see #getParamDeclaration()
	 * @generated
	 */
	void setParamDeclaration(ParameterDeclaration value);

} // ParameterValue
