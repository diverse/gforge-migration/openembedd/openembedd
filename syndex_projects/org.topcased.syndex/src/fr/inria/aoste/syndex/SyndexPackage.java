/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexPackage.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.SyndexFactory
 * @model kind="package" annotation="http://www.topcased.org/uuid uuid='11501873764270'"
 * @generated
 */
public interface SyndexPackage extends EPackage
{
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNAME																= "syndex";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_URI																= "http://www.inria.fr/aoste/SynDEx/1.1";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_PREFIX															= "syndex";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	SyndexPackage	eINSTANCE															= fr.inria.aoste.syndex.impl.SyndexPackageImpl
																								.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ElementImpl <em>Element</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ElementImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getElement()
	 * @generated
	 */
	int				ELEMENT																= 45;

	/**
	 * The number of structural features of the '<em>Element</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ELEMENT_FEATURE_COUNT												= 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.NamedElementImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getNamedElement()
	 * @generated
	 */
	int				NAMED_ELEMENT														= 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				NAMED_ELEMENT__NAME													= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				NAMED_ELEMENT_FEATURE_COUNT											= ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ComponentImpl <em>Component</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ComponentImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getComponent()
	 * @generated
	 */
	int				COMPONENT															= 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT__NAME														= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT__EXECUTION_PHASES											= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_FEATURE_COUNT												= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ApplicationComponentImpl
	 * <em>Application Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ApplicationComponentImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplicationComponent()
	 * @generated
	 */
	int				APPLICATION_COMPONENT												= 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_COMPONENT__NAME											= COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_COMPONENT__EXECUTION_PHASES								= COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_COMPONENT__PARAM_DECLARATIONS							= COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Application Component</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_COMPONENT_FEATURE_COUNT									= COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.AtomicApplicationComponentImpl
	 * <em>Atomic Application Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.AtomicApplicationComponentImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAtomicApplicationComponent()
	 * @generated
	 */
	int				ATOMIC_APPLICATION_COMPONENT										= 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ATOMIC_APPLICATION_COMPONENT__NAME									= APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES						= APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS					= APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE			= APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Atomic Application Component</em>' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT							= APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ActuatorImpl <em>Actuator</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ActuatorImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getActuator()
	 * @generated
	 */
	int				ACTUATOR															= 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR__NAME														= ATOMIC_APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR__EXECUTION_PHASES											= ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR__PARAM_DECLARATIONS										= ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR__ATOMIC_APPLICATION_STRUCTURE								= ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Input Port Actuator</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR__INPUT_PORT_ACTUATOR										= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Actuator</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ACTUATOR_FEATURE_COUNT												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.PortImpl <em>Port</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.PortImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getPort()
	 * @generated
	 */
	int				PORT																= 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT__NAME															= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT__PARENT_PORT													= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT__SIZE_PORT														= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT__TYPE_PORT														= NAMED_ELEMENT_FEATURE_COUNT + 2;
	/**
	 * The feature id for the '<em><b>Parent Condition Reference</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				PORT__PARENT_CONDITION_REFERENCE									= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Parent Compound Structure</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				PORT__PARENT_COMPOUND_STRUCTURE										= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Port</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				PORT_FEATURE_COUNT													= NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.SensorImpl <em>Sensor</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.SensorImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSensor()
	 * @generated
	 */
	int				SENSOR																= 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR__NAME														= ATOMIC_APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR__EXECUTION_PHASES											= ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR__PARAM_DECLARATIONS											= ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR__ATOMIC_APPLICATION_STRUCTURE								= ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Output Port Sensor</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR__OUTPUT_PORT_SENSOR											= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sensor</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SENSOR_FEATURE_COUNT												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ApplicationImpl <em>Application</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ApplicationImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplication()
	 * @generated
	 */
	int				APPLICATION															= 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__NAME													= APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__EXECUTION_PHASES										= APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__PARAM_DECLARATIONS										= APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Input Port Application</b></em>' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__INPUT_PORT_APPLICATION									= APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Port Application</b></em>' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__OUTPUT_PORT_APPLICATION								= APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Application Internal Structure</b></em>' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__APPLICATION_INTERNAL_STRUCTURE							= APPLICATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Main</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__MAIN													= APPLICATION_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Port Condition</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION__PORT_CONDITION											= APPLICATION_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Application</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_FEATURE_COUNT											= APPLICATION_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InputImpl <em>Input</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InputImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInput()
	 * @generated
	 */
	int				INPUT																= 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INPUT__NAME															= PORT__NAME;

	/**
	 * The feature id for the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INPUT__PARENT_PORT													= PORT__PARENT_PORT;

	/**
	 * The feature id for the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INPUT__SIZE_PORT													= PORT__SIZE_PORT;

	/**
	 * The feature id for the '<em><b>Type Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INPUT__TYPE_PORT													= PORT__TYPE_PORT;

	/**
	 * The number of structural features of the '<em>Input</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INPUT_FEATURE_COUNT													= PORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.OutputImpl <em>Output</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.OutputImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getOutput()
	 * @generated
	 */
	int				OUTPUT																= 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUTPUT__NAME														= PORT__NAME;

	/**
	 * The feature id for the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUTPUT__PARENT_PORT													= PORT__PARENT_PORT;

	/**
	 * The feature id for the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUTPUT__SIZE_PORT													= PORT__SIZE_PORT;

	/**
	 * The feature id for the '<em><b>Type Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUTPUT__TYPE_PORT													= PORT__TYPE_PORT;

	/**
	 * The number of structural features of the '<em>Output</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUTPUT_FEATURE_COUNT												= PORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.FunctionImpl <em>Function</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.FunctionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getFunction()
	 * @generated
	 */
	int				FUNCTION															= 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__NAME														= ATOMIC_APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__EXECUTION_PHASES											= ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__PARAM_DECLARATIONS										= ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__ATOMIC_APPLICATION_STRUCTURE								= ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Input Port Function</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__INPUT_PORT_FUNCTION										= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Port Function</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION__OUTPUT_PORT_FUNCTION										= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Function</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FUNCTION_FEATURE_COUNT												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConstantImpl <em>Constant</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConstantImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConstant()
	 * @generated
	 */
	int				CONSTANT															= 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__NAME														= ATOMIC_APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__EXECUTION_PHASES											= ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__PARAM_DECLARATIONS										= ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__ATOMIC_APPLICATION_STRUCTURE								= ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE;

	/**
	 * The feature id for the '<em><b>One Output Constant</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__ONE_OUTPUT_CONSTANT										= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__CONSTANT_VALUE											= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Enum Values</b></em>' attribute list. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT__ENUM_VALUES												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Constant</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONSTANT_FEATURE_COUNT												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.DelayImpl <em>Delay</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.DelayImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDelay()
	 * @generated
	 */
	int				DELAY																= 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__NAME															= ATOMIC_APPLICATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__EXECUTION_PHASES												= ATOMIC_APPLICATION_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>Param Declarations</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__PARAM_DECLARATIONS											= ATOMIC_APPLICATION_COMPONENT__PARAM_DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Atomic Application Structure</b></em>' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__ATOMIC_APPLICATION_STRUCTURE									= ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE;

	/**
	 * The feature id for the '<em><b>One Input Delay</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__ONE_INPUT_DELAY												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>One Output Delay</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__ONE_OUTPUT_DELAY												= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Delay Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__DELAY_VALUE													= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Enum Value</b></em>' attribute list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY__ENUM_VALUE													= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Delay</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DELAY_FEATURE_COUNT													= ATOMIC_APPLICATION_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConnectionImpl <em>Connection</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnection()
	 * @generated
	 */
	int				CONNECTION															= 13;

	/**
	 * The number of structural features of the '<em>Connection</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTION_FEATURE_COUNT											= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ApplicationConnectionImpl
	 * <em>Application Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ApplicationConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplicationConnection()
	 * @generated
	 */
	int				APPLICATION_CONNECTION												= 47;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION		= CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION		= CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Application Connection</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APPLICATION_CONNECTION_FEATURE_COUNT								= CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.DataConnectionImpl <em>Data Connection</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.DataConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDataConnection()
	 * @generated
	 */
	int				DATA_CONNECTION														= 14;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATA_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION			= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATA_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION			= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source Data Connection</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATA_CONNECTION__SOURCE_DATA_CONNECTION								= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Dataconnection</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATA_CONNECTION__TARGET_DATACONNECTION								= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Connection</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATA_CONNECTION_FEATURE_COUNT										= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InstanceImpl <em>Instance</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInstance()
	 * @generated
	 */
	int				INSTANCE															= 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INSTANCE__NAME														= NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Instance</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INSTANCE_FEATURE_COUNT												= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl <em>Component Instance</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ComponentInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getComponentInstance()
	 * @generated
	 */
	int				COMPONENT_INSTANCE													= 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__NAME											= INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Port Instances</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__PORT_INSTANCES									= INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Referenced Component</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__REFERENCED_COMPONENT							= INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Repetition Factor</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__REPETITION_FACTOR								= INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parameter Values</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__PARAMETER_VALUES								= INSTANCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Referenced HW Component</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT							= INSTANCE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Component Instance</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPONENT_INSTANCE_FEATURE_COUNT									= INSTANCE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.PortInstanceImpl <em>Port Instance</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.PortInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getPortInstance()
	 * @generated
	 */
	int				PORT_INSTANCE														= 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT_INSTANCE__NAME													= INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Parent Component Instance</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT_INSTANCE__PARENT_COMPONENT_INSTANCE							= INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port Instance</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PORT_INSTANCE_FEATURE_COUNT											= INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.INInstanceImpl <em>IN Instance</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.INInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINInstance()
	 * @generated
	 */
	int				IN_INSTANCE															= 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_INSTANCE__NAME													= PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Parent Component Instance</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_INSTANCE__PARENT_COMPONENT_INSTANCE								= PORT_INSTANCE__PARENT_COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owner Input</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_INSTANCE__OWNER_INPUT											= PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IN Instance</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_INSTANCE_FEATURE_COUNT											= PORT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.OutInstanceImpl <em>Out Instance</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.OutInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getOutInstance()
	 * @generated
	 */
	int				OUT_INSTANCE														= 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUT_INSTANCE__NAME													= PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Parent Component Instance</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUT_INSTANCE__PARENT_COMPONENT_INSTANCE								= PORT_INSTANCE__PARENT_COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owner Output</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUT_INSTANCE__OWNER_OUTPUT											= PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Out Instance</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				OUT_INSTANCE_FEATURE_COUNT											= PORT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl
	 * <em>Internal Dependency IN</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InternalDependencyINImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependencyIN()
	 * @generated
	 */
	int				INTERNAL_DEPENDENCY_IN												= 20;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__PARENT_COUMPOUND_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__PARENT_CONDITION_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source IDI</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__SOURCE_IDI									= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target IDI</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__TARGET_IDI									= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source IDIC</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__SOURCE_IDIC									= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target IDIC</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN__TARGET_IDIC									= APPLICATION_CONNECTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Internal Dependency IN</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_IN_FEATURE_COUNT								= APPLICATION_CONNECTION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl
	 * <em>Internal Dependency OUT</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependencyOUT()
	 * @generated
	 */
	int				INTERNAL_DEPENDENCY_OUT												= 21;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_OUT__PARENT_COUMPOUND_APPLICATION_CONNECTION	= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_OUT__PARENT_CONDITION_APPLICATION_CONNECTION	= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source IDO</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_OUT__SOURCE_IDO									= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target IDO</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_OUT__TARGET_IDO									= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Internal Dependency OUT</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_OUT_FEATURE_COUNT								= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConnecteableElementImpl
	 * <em>Connecteable Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConnecteableElementImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnecteableElement()
	 * @generated
	 */
	int				CONNECTEABLE_ELEMENT												= 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTEABLE_ELEMENT__NAME											= NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Connecteable Element</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTEABLE_ELEMENT_FEATURE_COUNT									= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InCondPortImpl <em>In Cond Port</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InCondPortImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInCondPort()
	 * @generated
	 */
	int				IN_COND_PORT														= 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_PORT__NAME													= PORT__NAME;

	/**
	 * The feature id for the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_PORT__PARENT_PORT											= PORT__PARENT_PORT;

	/**
	 * The feature id for the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_PORT__SIZE_PORT												= PORT__SIZE_PORT;

	/**
	 * The feature id for the '<em><b>Type Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_PORT__TYPE_PORT												= PORT__TYPE_PORT;

	/**
	 * The number of structural features of the '<em>In Cond Port</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_PORT_FEATURE_COUNT											= PORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.HardwareComponentImpl <em>Hardware Component</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.HardwareComponentImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHardwareComponent()
	 * @generated
	 */
	int				HARDWARE_COMPONENT													= 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_COMPONENT__NAME											= COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_COMPONENT__EXECUTION_PHASES								= COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_COMPONENT__HW_CONNECTION									= COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT								= COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Hardware Component</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_COMPONENT_FEATURE_COUNT									= COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.HardwareImpl <em>Hardware</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.HardwareImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHardware()
	 * @generated
	 */
	int				HARDWARE															= 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__NAME														= HARDWARE_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__EXECUTION_PHASES											= HARDWARE_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__HW_CONNECTION												= HARDWARE_COMPONENT__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__IN_OUT_HW_COMPONENT										= HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Component HW Instance</b></em>' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__COMPONENT_HW_INSTANCE										= HARDWARE_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE__MAIN														= HARDWARE_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Hardware</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HARDWARE_FEATURE_COUNT												= HARDWARE_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.MediaImpl <em>Media</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.MediaImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getMedia()
	 * @generated
	 */
	int				MEDIA																= 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__NAME															= HARDWARE_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__EXECUTION_PHASES												= HARDWARE_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__HW_CONNECTION												= HARDWARE_COMPONENT__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__IN_OUT_HW_COMPONENT											= HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Bandwidth</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__BANDWIDTH													= HARDWARE_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__SIZE															= HARDWARE_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Memory</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__PARENT_MEMORY												= HARDWARE_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Broadcast</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__BROADCAST													= HARDWARE_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA__DURATION														= HARDWARE_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Media</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MEDIA_FEATURE_COUNT													= HARDWARE_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ProcessorImpl <em>Processor</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ProcessorImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getProcessor()
	 * @generated
	 */
	int				PROCESSOR															= 44;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__NAME														= HARDWARE_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__EXECUTION_PHASES											= HARDWARE_COMPONENT__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__HW_CONNECTION											= HARDWARE_COMPONENT__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__IN_OUT_HW_COMPONENT										= HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Parent Processor</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__PARENT_PROCESSOR											= HARDWARE_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Duration SW</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR__DURATION_SW												= HARDWARE_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Processor</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PROCESSOR_FEATURE_COUNT												= HARDWARE_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.FPGAImpl <em>FPGA</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.FPGAImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getFPGA()
	 * @generated
	 */
	int				FPGA																= 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__NAME															= PROCESSOR__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__EXECUTION_PHASES												= PROCESSOR__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__HW_CONNECTION													= PROCESSOR__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__IN_OUT_HW_COMPONENT											= PROCESSOR__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Parent Processor</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__PARENT_PROCESSOR												= PROCESSOR__PARENT_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Duration SW</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA__DURATION_SW													= PROCESSOR__DURATION_SW;

	/**
	 * The number of structural features of the '<em>FPGA</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				FPGA_FEATURE_COUNT													= PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.AsicImpl <em>Asic</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.AsicImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAsic()
	 * @generated
	 */
	int				ASIC																= 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__NAME															= PROCESSOR__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__EXECUTION_PHASES												= PROCESSOR__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__HW_CONNECTION													= PROCESSOR__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__IN_OUT_HW_COMPONENT											= PROCESSOR__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Parent Processor</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__PARENT_PROCESSOR												= PROCESSOR__PARENT_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Duration SW</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC__DURATION_SW													= PROCESSOR__DURATION_SW;

	/**
	 * The number of structural features of the '<em>Asic</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASIC_FEATURE_COUNT													= PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.DSPImpl <em>DSP</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.DSPImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDSP()
	 * @generated
	 */
	int				DSP																	= 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__NAME															= PROCESSOR__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__EXECUTION_PHASES												= PROCESSOR__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__HW_CONNECTION													= PROCESSOR__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__IN_OUT_HW_COMPONENT											= PROCESSOR__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Parent Processor</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__PARENT_PROCESSOR												= PROCESSOR__PARENT_PROCESSOR;

	/**
	 * The feature id for the '<em><b>Duration SW</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP__DURATION_SW													= PROCESSOR__DURATION_SW;

	/**
	 * The number of structural features of the '<em>DSP</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DSP_FEATURE_COUNT													= PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.RAMImpl <em>RAM</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.RAMImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRAM()
	 * @generated
	 */
	int				RAM																	= 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__NAME															= MEDIA__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__EXECUTION_PHASES												= MEDIA__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__HW_CONNECTION													= MEDIA__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__IN_OUT_HW_COMPONENT											= MEDIA__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Bandwidth</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__BANDWIDTH														= MEDIA__BANDWIDTH;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__SIZE															= MEDIA__SIZE;

	/**
	 * The feature id for the '<em><b>Parent Memory</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__PARENT_MEMORY													= MEDIA__PARENT_MEMORY;

	/**
	 * The feature id for the '<em><b>Broadcast</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__BROADCAST														= MEDIA__BROADCAST;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM__DURATION														= MEDIA__DURATION;

	/**
	 * The number of structural features of the '<em>RAM</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				RAM_FEATURE_COUNT													= MEDIA_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.SAMImpl <em>SAM</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.SAMImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSAM()
	 * @generated
	 */
	int				SAM																	= 31;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__NAME															= MEDIA__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__EXECUTION_PHASES												= MEDIA__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__HW_CONNECTION													= MEDIA__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__IN_OUT_HW_COMPONENT											= MEDIA__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Bandwidth</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__BANDWIDTH														= MEDIA__BANDWIDTH;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__SIZE															= MEDIA__SIZE;

	/**
	 * The feature id for the '<em><b>Parent Memory</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__PARENT_MEMORY													= MEDIA__PARENT_MEMORY;

	/**
	 * The feature id for the '<em><b>Broadcast</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__BROADCAST														= MEDIA__BROADCAST;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM__DURATION														= MEDIA__DURATION;

	/**
	 * The number of structural features of the '<em>SAM</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAM_FEATURE_COUNT													= MEDIA_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.SAMPTPImpl <em>SAMPTP</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.SAMPTPImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSAMPTP()
	 * @generated
	 */
	int				SAMPTP																= 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__NAME														= MEDIA__NAME;

	/**
	 * The feature id for the '<em><b>Execution Phases</b></em>' attribute list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__EXECUTION_PHASES											= MEDIA__EXECUTION_PHASES;

	/**
	 * The feature id for the '<em><b>HW Connection</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__HW_CONNECTION												= MEDIA__HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>In Out HW Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__IN_OUT_HW_COMPONENT											= MEDIA__IN_OUT_HW_COMPONENT;

	/**
	 * The feature id for the '<em><b>Bandwidth</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__BANDWIDTH													= MEDIA__BANDWIDTH;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__SIZE														= MEDIA__SIZE;

	/**
	 * The feature id for the '<em><b>Parent Memory</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__PARENT_MEMORY												= MEDIA__PARENT_MEMORY;

	/**
	 * The feature id for the '<em><b>Broadcast</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__BROADCAST													= MEDIA__BROADCAST;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP__DURATION													= MEDIA__DURATION;

	/**
	 * The number of structural features of the '<em>SAMPTP</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SAMPTP_FEATURE_COUNT												= MEDIA_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.HWConnectionImpl <em>HW Connection</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.HWConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHWConnection()
	 * @generated
	 */
	int				HW_CONNECTION														= 33;

	/**
	 * The feature id for the '<em><b>Parent HW Connection</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HW_CONNECTION__PARENT_HW_CONNECTION									= CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>HW Connection</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				HW_CONNECTION_FEATURE_COUNT											= CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConnectionHWImpl <em>Connection HW</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConnectionHWImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnectionHW()
	 * @generated
	 */
	int				CONNECTION_HW														= 34;

	/**
	 * The feature id for the '<em><b>Parent HW Connection</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTION_HW__PARENT_HW_CONNECTION									= HW_CONNECTION__PARENT_HW_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source In Out</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTION_HW__SOURCE_IN_OUT										= HW_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target In Out</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTION_HW__TARGET_IN_OUT										= HW_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Connection HW</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONNECTION_HW_FEATURE_COUNT											= HW_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.CaracteristicsImpl <em>Caracteristics</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.CaracteristicsImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCaracteristics()
	 * @generated
	 */
	int				CARACTERISTICS														= 35;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CARACTERISTICS__NAME												= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Adequation</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CARACTERISTICS__ADEQUATION											= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Caracteristics</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CARACTERISTICS_FEATURE_COUNT										= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl <em>Association SWto HW</em>}
	 * ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAssociationSWtoHW()
	 * @generated
	 */
	int				ASSOCIATION_SWTO_HW													= 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW__NAME											= CARACTERISTICS__NAME;

	/**
	 * The feature id for the '<em><b>Adequation</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW__ADEQUATION										= CARACTERISTICS__ADEQUATION;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW__DURATION										= CARACTERISTICS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Appli</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW__SOURCE_APPLI									= CARACTERISTICS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target HW</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW__TARGET_HW										= CARACTERISTICS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Association SWto HW</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_SWTO_HW_FEATURE_COUNT									= CARACTERISTICS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.AssociationComtoProcImpl
	 * <em>Association Comto Proc</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.AssociationComtoProcImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAssociationComtoProc()
	 * @generated
	 */
	int				ASSOCIATION_COMTO_PROC												= 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC__NAME										= CARACTERISTICS__NAME;

	/**
	 * The feature id for the '<em><b>Adequation</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC__ADEQUATION									= CARACTERISTICS__ADEQUATION;

	/**
	 * The feature id for the '<em><b>Source Association Com To Proc</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC				= CARACTERISTICS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC__DURATION									= CARACTERISTICS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target HW Process</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS							= CARACTERISTICS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Association Comto Proc</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ASSOCIATION_COMTO_PROC_FEATURE_COUNT								= CARACTERISTICS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.DurationImpl <em>Duration</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.DurationImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDuration()
	 * @generated
	 */
	int				DURATION															= 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DURATION__NAME														= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Enum Duration</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DURATION__ENUM_DURATION												= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent Duration</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DURATION__PARENT_DURATION											= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Duration</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DURATION_FEATURE_COUNT												= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.EnumLitDurationImpl <em>Enum Lit Duration</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.EnumLitDurationImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getEnumLitDuration()
	 * @generated
	 */
	int				ENUM_LIT_DURATION													= 39;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ENUM_LIT_DURATION__NAME												= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ENUM_LIT_DURATION__VALUE											= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent Enum Duration</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ENUM_LIT_DURATION__PARENT_ENUM_DURATION								= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Lit Duration</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ENUM_LIT_DURATION_FEATURE_COUNT										= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.DatatypeImpl <em>Datatype</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.DatatypeImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDatatype()
	 * @generated
	 */
	int				DATATYPE															= 40;

	/**
	 * The number of structural features of the '<em>Datatype</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				DATATYPE_FEATURE_COUNT												= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.SizeImpl <em>Size</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.SizeImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSize()
	 * @generated
	 */
	int				SIZE																= 41;

	/**
	 * The number of structural features of the '<em>Size</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SIZE_FEATURE_COUNT													= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ArrayImpl <em>Array</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ArrayImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getArray()
	 * @generated
	 */
	int				ARRAY																= 42;

	/**
	 * The feature id for the '<em><b>Vectors</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARRAY__VECTORS														= SIZE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Array</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARRAY_FEATURE_COUNT													= SIZE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.VectorImpl <em>Vector</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.VectorImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getVector()
	 * @generated
	 */
	int				VECTOR																= 43;

	/**
	 * The feature id for the '<em><b>Vector</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				VECTOR__VECTOR														= SIZE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Vector</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				VECTOR_FEATURE_COUNT												= SIZE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.SynDExImpl <em>Syn DEx</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.SynDExImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSynDEx()
	 * @generated
	 */
	int				SYN_DEX																= 46;

	/**
	 * The feature id for the '<em><b>Syn DExelements</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SYN_DEX__SYN_DEXELEMENTS											= 0;

	/**
	 * The number of structural features of the '<em>Syn DEx</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				SYN_DEX_FEATURE_COUNT												= 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InternalStructureImpl <em>Internal Structure</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InternalStructureImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalStructure()
	 * @generated
	 */
	int				INTERNAL_STRUCTURE													= 48;

	/**
	 * The feature id for the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE						= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Internal Connector</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_STRUCTURE__INTERNAL_CONNECTOR								= ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Internal Structure</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_STRUCTURE_FEATURE_COUNT									= ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ElementaryStructureImpl
	 * <em>Elementary Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ElementaryStructureImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getElementaryStructure()
	 * @generated
	 */
	int				ELEMENTARY_STRUCTURE												= 49;

	/**
	 * The feature id for the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ELEMENTARY_STRUCTURE__OWNER_INTERNAL_STRUCTURE						= INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Internal Connector</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ELEMENTARY_STRUCTURE__INTERNAL_CONNECTOR							= INTERNAL_STRUCTURE__INTERNAL_CONNECTOR;

	/**
	 * The number of structural features of the '<em>Elementary Structure</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ELEMENTARY_STRUCTURE_FEATURE_COUNT									= INTERNAL_STRUCTURE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.CompoundStructureImpl <em>Compound Structure</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.CompoundStructureImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCompoundStructure()
	 * @generated
	 */
	int				COMPOUND_STRUCTURE													= 50;

	/**
	 * The feature id for the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPOUND_STRUCTURE__OWNER_INTERNAL_STRUCTURE						= INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Internal Connector</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPOUND_STRUCTURE__INTERNAL_CONNECTOR								= INTERNAL_STRUCTURE__INTERNAL_CONNECTOR;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				COMPOUND_STRUCTURE__COMPONENT_INSTANCE								= INTERNAL_STRUCTURE_FEATURE_COUNT + 0;
	/**
	 * The feature id for the '<em><b>Port Conditioned Structure</b></em>' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE							= INTERNAL_STRUCTURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Compound Structure</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				COMPOUND_STRUCTURE_FEATURE_COUNT									= INTERNAL_STRUCTURE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.INOUTImpl <em>INOUT</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.INOUTImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINOUT()
	 * @generated
	 */
	int				INOUT																= 51;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT__NAME															= PORT__NAME;

	/**
	 * The feature id for the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT__PARENT_PORT													= PORT__PARENT_PORT;

	/**
	 * The feature id for the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT__SIZE_PORT													= PORT__SIZE_PORT;

	/**
	 * The feature id for the '<em><b>Type Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT__TYPE_PORT													= PORT__TYPE_PORT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT__TYPE															= PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>INOUT</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT_FEATURE_COUNT													= PORT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InternalDataConnectionImpl
	 * <em>Internal Data Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InternalDataConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDataConnection()
	 * @generated
	 */
	int				INTERNAL_DATA_CONNECTION											= 52;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION	= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION	= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source Out Instance</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE						= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target IN Instance</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE						= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target IN Instance C</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C						= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Internal Data Connection</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DATA_CONNECTION_FEATURE_COUNT								= APPLICATION_CONNECTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ParameterDeclarationImpl
	 * <em>Parameter Declaration</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ParameterDeclarationImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getParameterDeclaration()
	 * @generated
	 */
	int				PARAMETER_DECLARATION												= 53;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARAMETER_DECLARATION__NAME											= NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Parameter Declaration</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARAMETER_DECLARATION_FEATURE_COUNT									= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ParameterValueImpl <em>Parameter Value</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ParameterValueImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getParameterValue()
	 * @generated
	 */
	int				PARAMETER_VALUE														= 54;

	/**
	 * The feature id for the '<em><b>Value Parameter</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARAMETER_VALUE__VALUE_PARAMETER									= ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Param Declaration</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARAMETER_VALUE__PARAM_DECLARATION									= ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Parameter Value</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARAMETER_VALUE_FEATURE_COUNT										= ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConditionedStructureImpl
	 * <em>Conditioned Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConditionedStructureImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConditionedStructure()
	 * @generated
	 */
	int				CONDITIONED_STRUCTURE												= 55;

	/**
	 * The feature id for the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITIONED_STRUCTURE__OWNER_INTERNAL_STRUCTURE						= INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE;

	/**
	 * The feature id for the '<em><b>Internal Connector</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITIONED_STRUCTURE__INTERNAL_CONNECTOR							= INTERNAL_STRUCTURE__INTERNAL_CONNECTOR;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITIONED_STRUCTURE__CONDITIONS									= INTERNAL_STRUCTURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Cond</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITIONED_STRUCTURE__PORT_COND									= INTERNAL_STRUCTURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conditioned Structure</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITIONED_STRUCTURE_FEATURE_COUNT									= INTERNAL_STRUCTURE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.ConditionImpl <em>Condition</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.ConditionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCondition()
	 * @generated
	 */
	int				CONDITION															= 56;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__NAME														= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__OWNER_INTERNAL_STRUCTURE									= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Internal Connector</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__INTERNAL_CONNECTOR										= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Condition</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__VALUE_CONDITION											= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Condition Port</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__CONDITION_PORT											= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				CONDITION__COMPONENT_INSTANCE										= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Port Condition</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				CONDITION__CONDITION_PORT_REFERENCE									= NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Condition</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated NOT
	 * @ordered
	 */
	int				CONDITION_FEATURE_COUNT												= NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.RepetitionConnectionImpl
	 * <em>Repetition Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.RepetitionConnectionImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRepetitionConnection()
	 * @generated
	 */
	int				REPETITION_CONNECTION												= 57;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Repetition Kind</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION__REPETITION_KIND								= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Repetition Connection</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION					= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Repetition Connection</b></em>' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION					= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Repetition Connection</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				REPETITION_CONNECTION_FEATURE_COUNT									= APPLICATION_CONNECTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.INOUTInstanceImpl <em>INOUT Instance</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.INOUTInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINOUTInstance()
	 * @generated
	 */
	int				INOUT_INSTANCE														= 58;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT_INSTANCE__NAME												= PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Parent Component Instance</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT_INSTANCE__PARENT_COMPONENT_INSTANCE							= PORT_INSTANCE__PARENT_COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owner INOUT</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT_INSTANCE__OWNER_INOUT											= PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>INOUT Instance</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INOUT_INSTANCE_FEATURE_COUNT										= PORT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InCondInstanceImpl <em>In Cond Instance</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InCondInstanceImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInCondInstance()
	 * @generated
	 */
	int				IN_COND_INSTANCE													= 59;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_INSTANCE__NAME												= PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Parent Component Instance</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_INSTANCE__PARENT_COMPONENT_INSTANCE							= PORT_INSTANCE__PARENT_COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owner Cond</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_INSTANCE__OWNER_COND										= PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>In Cond Instance</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				IN_COND_INSTANCE_FEATURE_COUNT										= PORT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyImpl
	 * <em>Internal Dependency</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.impl.InternalDependencyImpl
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependency()
	 * @generated
	 */
	int				INTERNAL_DEPENDENCY													= 60;

	/**
	 * The feature id for the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY__PARENT_COUMPOUND_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY__PARENT_CONDITION_APPLICATION_CONNECTION		= APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY__SOURCE											= APPLICATION_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY__TARGET											= APPLICATION_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Internal Dependency</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				INTERNAL_DEPENDENCY_FEATURE_COUNT									= APPLICATION_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.CodeExecutionPhaseKind
	 * <em>Code Execution Phase Kind</em>} ' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.CodeExecutionPhaseKind
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCodeExecutionPhaseKind()
	 * @generated
	 */
	int				CODE_EXECUTION_PHASE_KIND											= 61;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.RepetitionConnectionKind
	 * <em>Repetition Connection Kind</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.RepetitionConnectionKind
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRepetitionConnectionKind()
	 * @generated
	 */
	int				REPETITION_CONNECTION_KIND											= 62;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.syndex.DataTypeK <em>Data Type K</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see fr.inria.aoste.syndex.DataTypeK
	 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDataTypeK()
	 * @generated
	 */
	int				DATA_TYPE_K															= 63;

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Actuator <em>Actuator</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Actuator</em>'.
	 * @see fr.inria.aoste.syndex.Actuator
	 * @generated
	 */
	EClass getActuator();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Actuator#getInputPortActuator <em>Input Port Actuator</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Input Port Actuator</em>'.
	 * @see fr.inria.aoste.syndex.Actuator#getInputPortActuator()
	 * @see #getActuator()
	 * @generated
	 */
	EReference getActuator_InputPortActuator();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.AtomicApplicationComponent
	 * <em>Atomic Application Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Atomic Application Component</em>'.
	 * @see fr.inria.aoste.syndex.AtomicApplicationComponent
	 * @generated
	 */
	EClass getAtomicApplicationComponent();

	/**
	 * Returns the meta object for the containment reference '
	 * {@link fr.inria.aoste.syndex.AtomicApplicationComponent#getAtomicApplicationStructure
	 * <em>Atomic Application Structure</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Atomic Application Structure</em>'.
	 * @see fr.inria.aoste.syndex.AtomicApplicationComponent#getAtomicApplicationStructure()
	 * @see #getAtomicApplicationComponent()
	 * @generated
	 */
	EReference getAtomicApplicationComponent_AtomicApplicationStructure();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Port <em>Port</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Port</em>'.
	 * @see fr.inria.aoste.syndex.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Port#getParentPort <em>Parent Port</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Port</em>'.
	 * @see fr.inria.aoste.syndex.Port#getParentPort()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_ParentPort();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.syndex.Port#getSizePort
	 * <em>Size Port</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Size Port</em>'.
	 * @see fr.inria.aoste.syndex.Port#getSizePort()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_SizePort();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Port#getTypePort <em>Type Port</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Type Port</em>'.
	 * @see fr.inria.aoste.syndex.Port#getTypePort()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_TypePort();

	/**
	 * Returns the meta object for the container reference '{@link fr.inria.aoste.syndex.Port#getParentCondition
	 * <em>Parent Condition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Parent Condition</em>'.
	 * @see fr.inria.aoste.syndex.Port#getParentConditionedStructure()
	 * @see #getPort()
	 * @generated NOT
	 */
	EReference getPort_ParentConditionReference();

	/**
	 * Returns the meta object for the container reference '
	 * {@link fr.inria.aoste.syndex.Port#getParentCompoundStructure <em>Parent Compound Structure</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Parent Compound Structure</em>'.
	 * @see fr.inria.aoste.syndex.Port#getParentCompoundStructure()
	 * @see #getPort()
	 * @generated NOT
	 */
	EReference getPort_ParentCompoundStructure();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.NamedElement <em>Named Element</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.aoste.syndex.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.aoste.syndex.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ApplicationComponent
	 * <em>Application Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Application Component</em>'.
	 * @see fr.inria.aoste.syndex.ApplicationComponent
	 * @generated
	 */
	EClass getApplicationComponent();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.ApplicationComponent#getParamDeclarations <em>Param Declarations</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Param Declarations</em>'.
	 * @see fr.inria.aoste.syndex.ApplicationComponent#getParamDeclarations()
	 * @see #getApplicationComponent()
	 * @generated
	 */
	EReference getApplicationComponent_ParamDeclarations();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Sensor <em>Sensor</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Sensor</em>'.
	 * @see fr.inria.aoste.syndex.Sensor
	 * @generated
	 */
	EClass getSensor();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Sensor#getOutputPortSensor <em>Output Port Sensor</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Output Port Sensor</em>'.
	 * @see fr.inria.aoste.syndex.Sensor#getOutputPortSensor()
	 * @see #getSensor()
	 * @generated
	 */
	EReference getSensor_OutputPortSensor();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Application <em>Application</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Application</em>'.
	 * @see fr.inria.aoste.syndex.Application
	 * @generated
	 */
	EClass getApplication();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Application#getInputPortApplication <em>Input Port Application</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Input Port Application</em>'.
	 * @see fr.inria.aoste.syndex.Application#getInputPortApplication()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_InputPortApplication();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Application#getOutputPortApplication <em>Output Port Application</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Output Port Application</em>'.
	 * @see fr.inria.aoste.syndex.Application#getOutputPortApplication()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_OutputPortApplication();

	/**
	 * Returns the meta object for the containment reference '
	 * {@link fr.inria.aoste.syndex.Application#getApplicationInternalStructure <em>Application Internal Structure</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Application Internal Structure</em>'.
	 * @see fr.inria.aoste.syndex.Application#getApplicationInternalStructure()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_ApplicationInternalStructure();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Application#isMain <em>Main</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Main</em>'.
	 * @see fr.inria.aoste.syndex.Application#isMain()
	 * @see #getApplication()
	 * @generated
	 */
	EAttribute getApplication_Main();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Application#getPortCondition <em>Port Condition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Port Condition</em>'.
	 * @see fr.inria.aoste.syndex.Application#getPortCondition()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_PortCondition();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Input <em>Input</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Input</em>'.
	 * @see fr.inria.aoste.syndex.Input
	 * @generated
	 */
	EClass getInput();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Component <em>Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Component</em>'.
	 * @see fr.inria.aoste.syndex.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.syndex.Component#getExecutionPhases
	 * <em>Execution Phases</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute list '<em>Execution Phases</em>'.
	 * @see fr.inria.aoste.syndex.Component#getExecutionPhases()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_ExecutionPhases();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Output <em>Output</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Output</em>'.
	 * @see fr.inria.aoste.syndex.Output
	 * @generated
	 */
	EClass getOutput();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Function <em>Function</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Function</em>'.
	 * @see fr.inria.aoste.syndex.Function
	 * @generated
	 */
	EClass getFunction();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Function#getInputPortFunction <em>Input Port Function</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Input Port Function</em>'.
	 * @see fr.inria.aoste.syndex.Function#getInputPortFunction()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_InputPortFunction();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Function#getOutputPortFunction <em>Output Port Function</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Output Port Function</em>'.
	 * @see fr.inria.aoste.syndex.Function#getOutputPortFunction()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_OutputPortFunction();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Constant <em>Constant</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see fr.inria.aoste.syndex.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for the containment reference '
	 * {@link fr.inria.aoste.syndex.Constant#getOneOutputConstant <em>One Output Constant</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>One Output Constant</em>'.
	 * @see fr.inria.aoste.syndex.Constant#getOneOutputConstant()
	 * @see #getConstant()
	 * @generated
	 */
	EReference getConstant_OneOutputConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Constant#getConstantValue
	 * <em>Constant Value</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Constant Value</em>'.
	 * @see fr.inria.aoste.syndex.Constant#getConstantValue()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_ConstantValue();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.syndex.Constant#getEnumValues
	 * <em>Enum Values</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute list '<em>Enum Values</em>'.
	 * @see fr.inria.aoste.syndex.Constant#getEnumValues()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_EnumValues();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Delay <em>Delay</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Delay</em>'.
	 * @see fr.inria.aoste.syndex.Delay
	 * @generated
	 */
	EClass getDelay();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.syndex.Delay#getOneInputDelay
	 * <em>One Input Delay</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>One Input Delay</em>'.
	 * @see fr.inria.aoste.syndex.Delay#getOneInputDelay()
	 * @see #getDelay()
	 * @generated
	 */
	EReference getDelay_OneInputDelay();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.syndex.Delay#getOneOutputDelay
	 * <em>One Output Delay</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>One Output Delay</em>'.
	 * @see fr.inria.aoste.syndex.Delay#getOneOutputDelay()
	 * @see #getDelay()
	 * @generated
	 */
	EReference getDelay_OneOutputDelay();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Delay#getDelayValue <em>Delay Value</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Delay Value</em>'.
	 * @see fr.inria.aoste.syndex.Delay#getDelayValue()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_DelayValue();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.syndex.Delay#getEnumValue
	 * <em>Enum Value</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute list '<em>Enum Value</em>'.
	 * @see fr.inria.aoste.syndex.Delay#getEnumValue()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_EnumValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Connection <em>Connection</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Connection</em>'.
	 * @see fr.inria.aoste.syndex.Connection
	 * @generated
	 */
	EClass getConnection();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.DataConnection <em>Data Connection</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Data Connection</em>'.
	 * @see fr.inria.aoste.syndex.DataConnection
	 * @generated
	 */
	EClass getDataConnection();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.DataConnection#getSourceDataConnection
	 * <em>Source Data Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source Data Connection</em>'.
	 * @see fr.inria.aoste.syndex.DataConnection#getSourceDataConnection()
	 * @see #getDataConnection()
	 * @generated
	 */
	EReference getDataConnection_SourceDataConnection();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.DataConnection#getTargetDataconnection
	 * <em>Target Dataconnection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target Dataconnection</em>'.
	 * @see fr.inria.aoste.syndex.DataConnection#getTargetDataconnection()
	 * @see #getDataConnection()
	 * @generated
	 */
	EReference getDataConnection_TargetDataconnection();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Instance <em>Instance</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Instance</em>'.
	 * @see fr.inria.aoste.syndex.Instance
	 * @generated
	 */
	EClass getInstance();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Component Instance</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance
	 * @generated
	 */
	EClass getComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.ComponentInstance#getPortInstances <em>Port Instances</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Port Instances</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance#getPortInstances()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_PortInstances();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.ComponentInstance#getReferencedComponent
	 * <em>Referenced Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Referenced Component</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance#getReferencedComponent()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ReferencedComponent();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.ComponentInstance#getRepetitionFactor
	 * <em>Repetition Factor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Repetition Factor</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance#getRepetitionFactor()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_RepetitionFactor();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.ComponentInstance#getParameterValues <em>Parameter Values</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Parameter Values</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance#getParameterValues()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ParameterValues();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.ComponentInstance#getReferencedHWComponent <em>Referenced HW Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Referenced HW Component</em>'.
	 * @see fr.inria.aoste.syndex.ComponentInstance#getReferencedHWComponent()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ReferencedHWComponent();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.PortInstance <em>Port Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Port Instance</em>'.
	 * @see fr.inria.aoste.syndex.PortInstance
	 * @generated
	 */
	EClass getPortInstance();

	/**
	 * Returns the meta object for the container reference '
	 * {@link fr.inria.aoste.syndex.PortInstance#getParentComponentInstance <em>Parent Component Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Parent Component Instance</em>'.
	 * @see fr.inria.aoste.syndex.PortInstance#getParentComponentInstance()
	 * @see #getPortInstance()
	 * @generated
	 */
	EReference getPortInstance_ParentComponentInstance();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.INInstance <em>IN Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>IN Instance</em>'.
	 * @see fr.inria.aoste.syndex.INInstance
	 * @generated
	 */
	EClass getINInstance();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.INInstance#getOwnerInput
	 * <em>Owner Input</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner Input</em>'.
	 * @see fr.inria.aoste.syndex.INInstance#getOwnerInput()
	 * @see #getINInstance()
	 * @generated
	 */
	EReference getINInstance_OwnerInput();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.OutInstance <em>Out Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Out Instance</em>'.
	 * @see fr.inria.aoste.syndex.OutInstance
	 * @generated
	 */
	EClass getOutInstance();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.OutInstance#getOwnerOutput
	 * <em>Owner Output</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner Output</em>'.
	 * @see fr.inria.aoste.syndex.OutInstance#getOwnerOutput()
	 * @see #getOutInstance()
	 * @generated
	 */
	EReference getOutInstance_OwnerOutput();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InternalDependencyIN
	 * <em>Internal Dependency IN</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Internal Dependency IN</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN
	 * @generated
	 */
	EClass getInternalDependencyIN();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDI
	 * <em>Source IDI</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source IDI</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDI()
	 * @see #getInternalDependencyIN()
	 * @generated
	 */
	EReference getInternalDependencyIN_SourceIDI();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDI
	 * <em>Target IDI</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target IDI</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDI()
	 * @see #getInternalDependencyIN()
	 * @generated
	 */
	EReference getInternalDependencyIN_TargetIDI();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDIC
	 * <em>Source IDIC</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source IDIC</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDIC()
	 * @see #getInternalDependencyIN()
	 * @generated
	 */
	EReference getInternalDependencyIN_SourceIDIC();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDIC
	 * <em>Target IDIC</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target IDIC</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDIC()
	 * @see #getInternalDependencyIN()
	 * @generated
	 */
	EReference getInternalDependencyIN_TargetIDIC();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InternalDependencyOUT
	 * <em>Internal Dependency OUT</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Internal Dependency OUT</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyOUT
	 * @generated
	 */
	EClass getInternalDependencyOUT();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyOUT#getSourceIDO
	 * <em>Source IDO</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source IDO</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyOUT#getSourceIDO()
	 * @see #getInternalDependencyOUT()
	 * @generated
	 */
	EReference getInternalDependencyOUT_SourceIDO();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependencyOUT#getTargetIDO
	 * <em>Target IDO</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target IDO</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependencyOUT#getTargetIDO()
	 * @see #getInternalDependencyOUT()
	 * @generated
	 */
	EReference getInternalDependencyOUT_TargetIDO();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ConnecteableElement
	 * <em>Connecteable Element</em>} '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Connecteable Element</em>'.
	 * @see fr.inria.aoste.syndex.ConnecteableElement
	 * @generated
	 */
	EClass getConnecteableElement();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InCondPort <em>In Cond Port</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>In Cond Port</em>'.
	 * @see fr.inria.aoste.syndex.InCondPort
	 * @generated
	 */
	EClass getInCondPort();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.HardwareComponent <em>Hardware Component</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Hardware Component</em>'.
	 * @see fr.inria.aoste.syndex.HardwareComponent
	 * @generated
	 */
	EClass getHardwareComponent();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.HardwareComponent#getHWConnection <em>HW Connection</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>HW Connection</em>'.
	 * @see fr.inria.aoste.syndex.HardwareComponent#getHWConnection()
	 * @see #getHardwareComponent()
	 * @generated
	 */
	EReference getHardwareComponent_HWConnection();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.HardwareComponent#getInOutHWComponent <em>In Out HW Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>In Out HW Component</em>'.
	 * @see fr.inria.aoste.syndex.HardwareComponent#getInOutHWComponent()
	 * @see #getHardwareComponent()
	 * @generated
	 */
	EReference getHardwareComponent_InOutHWComponent();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Hardware <em>Hardware</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Hardware</em>'.
	 * @see fr.inria.aoste.syndex.Hardware
	 * @generated
	 */
	EClass getHardware();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Hardware#getComponentHWInstance <em>Component HW Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Component HW Instance</em>'.
	 * @see fr.inria.aoste.syndex.Hardware#getComponentHWInstance()
	 * @see #getHardware()
	 * @generated
	 */
	EReference getHardware_ComponentHWInstance();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Hardware#isMain <em>Main</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Main</em>'.
	 * @see fr.inria.aoste.syndex.Hardware#isMain()
	 * @see #getHardware()
	 * @generated
	 */
	EAttribute getHardware_Main();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Media <em>Media</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Media</em>'.
	 * @see fr.inria.aoste.syndex.Media
	 * @generated
	 */
	EClass getMedia();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Media#getBandwidth <em>Bandwidth</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Bandwidth</em>'.
	 * @see fr.inria.aoste.syndex.Media#getBandwidth()
	 * @see #getMedia()
	 * @generated
	 */
	EAttribute getMedia_Bandwidth();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Media#getSize <em>Size</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see fr.inria.aoste.syndex.Media#getSize()
	 * @see #getMedia()
	 * @generated
	 */
	EAttribute getMedia_Size();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Media#getParentMemory
	 * <em>Parent Memory</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Memory</em>'.
	 * @see fr.inria.aoste.syndex.Media#getParentMemory()
	 * @see #getMedia()
	 * @generated
	 */
	EReference getMedia_ParentMemory();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Media#isBroadcast <em>Broadcast</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Broadcast</em>'.
	 * @see fr.inria.aoste.syndex.Media#isBroadcast()
	 * @see #getMedia()
	 * @generated
	 */
	EAttribute getMedia_Broadcast();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Media#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Duration</em>'.
	 * @see fr.inria.aoste.syndex.Media#getDuration()
	 * @see #getMedia()
	 * @generated
	 */
	EReference getMedia_Duration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.FPGA <em>FPGA</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>FPGA</em>'.
	 * @see fr.inria.aoste.syndex.FPGA
	 * @generated
	 */
	EClass getFPGA();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Asic <em>Asic</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Asic</em>'.
	 * @see fr.inria.aoste.syndex.Asic
	 * @generated
	 */
	EClass getAsic();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.DSP <em>DSP</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>DSP</em>'.
	 * @see fr.inria.aoste.syndex.DSP
	 * @generated
	 */
	EClass getDSP();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.RAM <em>RAM</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>RAM</em>'.
	 * @see fr.inria.aoste.syndex.RAM
	 * @generated
	 */
	EClass getRAM();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.SAM <em>SAM</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SAM</em>'.
	 * @see fr.inria.aoste.syndex.SAM
	 * @generated
	 */
	EClass getSAM();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.SAMPTP <em>SAMPTP</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>SAMPTP</em>'.
	 * @see fr.inria.aoste.syndex.SAMPTP
	 * @generated
	 */
	EClass getSAMPTP();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.HWConnection <em>HW Connection</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>HW Connection</em>'.
	 * @see fr.inria.aoste.syndex.HWConnection
	 * @generated
	 */
	EClass getHWConnection();

	/**
	 * Returns the meta object for the container reference '
	 * {@link fr.inria.aoste.syndex.HWConnection#getParentHWConnection <em>Parent HW Connection</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Parent HW Connection</em>'.
	 * @see fr.inria.aoste.syndex.HWConnection#getParentHWConnection()
	 * @see #getHWConnection()
	 * @generated
	 */
	EReference getHWConnection_ParentHWConnection();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ConnectionHW <em>Connection HW</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Connection HW</em>'.
	 * @see fr.inria.aoste.syndex.ConnectionHW
	 * @generated
	 */
	EClass getConnectionHW();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.ConnectionHW#getSourceInOut
	 * <em>Source In Out</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source In Out</em>'.
	 * @see fr.inria.aoste.syndex.ConnectionHW#getSourceInOut()
	 * @see #getConnectionHW()
	 * @generated
	 */
	EReference getConnectionHW_SourceInOut();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.ConnectionHW#getTargetInOut
	 * <em>Target In Out</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target In Out</em>'.
	 * @see fr.inria.aoste.syndex.ConnectionHW#getTargetInOut()
	 * @see #getConnectionHW()
	 * @generated
	 */
	EReference getConnectionHW_TargetInOut();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Caracteristics <em>Caracteristics</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Caracteristics</em>'.
	 * @see fr.inria.aoste.syndex.Caracteristics
	 * @generated
	 */
	EClass getCaracteristics();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Caracteristics#isAdequation
	 * <em>Adequation</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Adequation</em>'.
	 * @see fr.inria.aoste.syndex.Caracteristics#isAdequation()
	 * @see #getCaracteristics()
	 * @generated
	 */
	EAttribute getCaracteristics_Adequation();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.AssociationSWtoHW <em>Association SWto HW</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Association SWto HW</em>'.
	 * @see fr.inria.aoste.syndex.AssociationSWtoHW
	 * @generated
	 */
	EClass getAssociationSWtoHW();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getDuration
	 * <em>Duration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see fr.inria.aoste.syndex.AssociationSWtoHW#getDuration()
	 * @see #getAssociationSWtoHW()
	 * @generated
	 */
	EAttribute getAssociationSWtoHW_Duration();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getSourceAppli
	 * <em>Source Appli</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source Appli</em>'.
	 * @see fr.inria.aoste.syndex.AssociationSWtoHW#getSourceAppli()
	 * @see #getAssociationSWtoHW()
	 * @generated
	 */
	EReference getAssociationSWtoHW_SourceAppli();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getTargetHW
	 * <em>Target HW</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target HW</em>'.
	 * @see fr.inria.aoste.syndex.AssociationSWtoHW#getTargetHW()
	 * @see #getAssociationSWtoHW()
	 * @generated
	 */
	EReference getAssociationSWtoHW_TargetHW();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.AssociationComtoProc
	 * <em>Association Comto Proc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Association Comto Proc</em>'.
	 * @see fr.inria.aoste.syndex.AssociationComtoProc
	 * @generated
	 */
	EClass getAssociationComtoProc();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.AssociationComtoProc#getSourceAssociationComToProc
	 * <em>Source Association Com To Proc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source Association Com To Proc</em>'.
	 * @see fr.inria.aoste.syndex.AssociationComtoProc#getSourceAssociationComToProc()
	 * @see #getAssociationComtoProc()
	 * @generated
	 */
	EReference getAssociationComtoProc_SourceAssociationComToProc();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.AssociationComtoProc#getDuration
	 * <em>Duration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Duration</em>'.
	 * @see fr.inria.aoste.syndex.AssociationComtoProc#getDuration()
	 * @see #getAssociationComtoProc()
	 * @generated
	 */
	EReference getAssociationComtoProc_Duration();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.AssociationComtoProc#getTargetHWProcess
	 * <em>Target HW Process</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target HW Process</em>'.
	 * @see fr.inria.aoste.syndex.AssociationComtoProc#getTargetHWProcess()
	 * @see #getAssociationComtoProc()
	 * @generated
	 */
	EReference getAssociationComtoProc_TargetHWProcess();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Duration <em>Duration</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Duration</em>'.
	 * @see fr.inria.aoste.syndex.Duration
	 * @generated
	 */
	EClass getDuration();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Duration#getEnumDuration <em>Enum Duration</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Enum Duration</em>'.
	 * @see fr.inria.aoste.syndex.Duration#getEnumDuration()
	 * @see #getDuration()
	 * @generated
	 */
	EReference getDuration_EnumDuration();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.syndex.Duration#getParentDuration
	 * <em>Parent Duration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Parent Duration</em>'.
	 * @see fr.inria.aoste.syndex.Duration#getParentDuration()
	 * @see #getDuration()
	 * @generated
	 */
	EReference getDuration_ParentDuration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.EnumLitDuration <em>Enum Lit Duration</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Enum Lit Duration</em>'.
	 * @see fr.inria.aoste.syndex.EnumLitDuration
	 * @generated
	 */
	EClass getEnumLitDuration();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.EnumLitDuration#getValue <em>Value</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.syndex.EnumLitDuration#getValue()
	 * @see #getEnumLitDuration()
	 * @generated
	 */
	EAttribute getEnumLitDuration_Value();

	/**
	 * Returns the meta object for the container reference '
	 * {@link fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration <em>Parent Enum Duration</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Parent Enum Duration</em>'.
	 * @see fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration()
	 * @see #getEnumLitDuration()
	 * @generated
	 */
	EReference getEnumLitDuration_ParentEnumDuration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Datatype <em>Datatype</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Datatype</em>'.
	 * @see fr.inria.aoste.syndex.Datatype
	 * @generated
	 */
	EClass getDatatype();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Size <em>Size</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Size</em>'.
	 * @see fr.inria.aoste.syndex.Size
	 * @generated
	 */
	EClass getSize();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Array <em>Array</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Array</em>'.
	 * @see fr.inria.aoste.syndex.Array
	 * @generated
	 */
	EClass getArray();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.syndex.Array#getVectors
	 * <em>Vectors</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Vectors</em>'.
	 * @see fr.inria.aoste.syndex.Array#getVectors()
	 * @see #getArray()
	 * @generated
	 */
	EReference getArray_Vectors();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Vector <em>Vector</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Vector</em>'.
	 * @see fr.inria.aoste.syndex.Vector
	 * @generated
	 */
	EClass getVector();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Vector#getVector <em>Vector</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Vector</em>'.
	 * @see fr.inria.aoste.syndex.Vector#getVector()
	 * @see #getVector()
	 * @generated
	 */
	EAttribute getVector_Vector();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Processor <em>Processor</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Processor</em>'.
	 * @see fr.inria.aoste.syndex.Processor
	 * @generated
	 */
	EClass getProcessor();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Processor#getParentProcessor
	 * <em>Parent Processor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Processor</em>'.
	 * @see fr.inria.aoste.syndex.Processor#getParentProcessor()
	 * @see #getProcessor()
	 * @generated
	 */
	EReference getProcessor_ParentProcessor();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Processor#getDurationSW
	 * <em>Duration SW</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Duration SW</em>'.
	 * @see fr.inria.aoste.syndex.Processor#getDurationSW()
	 * @see #getProcessor()
	 * @generated
	 */
	EReference getProcessor_DurationSW();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Element <em>Element</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Element</em>'.
	 * @see fr.inria.aoste.syndex.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.SynDEx <em>Syn DEx</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Syn DEx</em>'.
	 * @see fr.inria.aoste.syndex.SynDEx
	 * @generated
	 */
	EClass getSynDEx();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.SynDEx#getSynDExelements <em>Syn DExelements</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Syn DExelements</em>'.
	 * @see fr.inria.aoste.syndex.SynDEx#getSynDExelements()
	 * @see #getSynDEx()
	 * @generated
	 */
	EReference getSynDEx_SynDExelements();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ApplicationConnection
	 * <em>Application Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Application Connection</em>'.
	 * @see fr.inria.aoste.syndex.ApplicationConnection
	 * @generated
	 */
	EClass getApplicationConnection();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.ApplicationConnection#getParentCoumpoundApplicationConnection
	 * <em>Parent Coumpound Application Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Coumpound Application Connection</em>'.
	 * @see fr.inria.aoste.syndex.ApplicationConnection#getParentCoumpoundApplicationConnection()
	 * @see #getApplicationConnection()
	 * @generated
	 */
	EReference getApplicationConnection_ParentCoumpoundApplicationConnection();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.ApplicationConnection#getParentConditionApplicationConnection
	 * <em>Parent Condition Application Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Condition Application Connection</em>'.
	 * @see fr.inria.aoste.syndex.ApplicationConnection#getParentConditionApplicationConnection()
	 * @see #getApplicationConnection()
	 * @generated
	 */
	EReference getApplicationConnection_ParentConditionApplicationConnection();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InternalStructure <em>Internal Structure</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Internal Structure</em>'.
	 * @see fr.inria.aoste.syndex.InternalStructure
	 * @generated
	 */
	EClass getInternalStructure();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.InternalStructure#getOwnerInternalStructure <em>Owner Internal Structure</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner Internal Structure</em>'.
	 * @see fr.inria.aoste.syndex.InternalStructure#getOwnerInternalStructure()
	 * @see #getInternalStructure()
	 * @generated
	 */
	EReference getInternalStructure_OwnerInternalStructure();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.InternalStructure#getInternalConnector <em>Internal Connector</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Internal Connector</em>'.
	 * @see fr.inria.aoste.syndex.InternalStructure#getInternalConnector()
	 * @see #getInternalStructure()
	 * @generated
	 */
	EReference getInternalStructure_InternalConnector();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ElementaryStructure
	 * <em>Elementary Structure</em>} '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Elementary Structure</em>'.
	 * @see fr.inria.aoste.syndex.ElementaryStructure
	 * @generated
	 */
	EClass getElementaryStructure();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.CompoundStructure <em>Compound Structure</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Compound Structure</em>'.
	 * @see fr.inria.aoste.syndex.CompoundStructure
	 * @generated
	 */
	EClass getCompoundStructure();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.CompoundStructure#getComponentInstance <em>Component Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Component Instance</em>'.
	 * @see fr.inria.aoste.syndex.CompoundStructure#getComponentInstance()
	 * @see #getCompoundStructure()
	 * @generated
	 */
	EReference getCompoundStructure_ComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.CompoundStructure#getPortCompoundStructure <em>Port Compound Structure</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Port Compound Structure</em>'.
	 * @see fr.inria.aoste.syndex.CompoundStructure#getPortCompoundStructure()
	 * @see #getCompoundStructure()
	 * @generated NOT
	 */
	EReference getCompoundStructure_PortCompoundStructure();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.INOUT <em>INOUT</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>INOUT</em>'.
	 * @see fr.inria.aoste.syndex.INOUT
	 * @generated
	 */
	EClass getINOUT();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.INOUT#getType <em>Type</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.inria.aoste.syndex.INOUT#getType()
	 * @see #getINOUT()
	 * @generated
	 */
	EAttribute getINOUT_Type();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InternalDataConnection
	 * <em>Internal Data Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Internal Data Connection</em>'.
	 * @see fr.inria.aoste.syndex.InternalDataConnection
	 * @generated
	 */
	EClass getInternalDataConnection();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.InternalDataConnection#getSourceOutInstance <em>Source Out Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source Out Instance</em>'.
	 * @see fr.inria.aoste.syndex.InternalDataConnection#getSourceOutInstance()
	 * @see #getInternalDataConnection()
	 * @generated
	 */
	EReference getInternalDataConnection_SourceOutInstance();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstance <em>Target IN Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target IN Instance</em>'.
	 * @see fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstance()
	 * @see #getInternalDataConnection()
	 * @generated
	 */
	EReference getInternalDataConnection_TargetINInstance();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstanceC <em>Target IN Instance C</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target IN Instance C</em>'.
	 * @see fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstanceC()
	 * @see #getInternalDataConnection()
	 * @generated
	 */
	EReference getInternalDataConnection_TargetINInstanceC();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ParameterDeclaration
	 * <em>Parameter Declaration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Parameter Declaration</em>'.
	 * @see fr.inria.aoste.syndex.ParameterDeclaration
	 * @generated
	 */
	EClass getParameterDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ParameterValue <em>Parameter Value</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Parameter Value</em>'.
	 * @see fr.inria.aoste.syndex.ParameterValue
	 * @generated
	 */
	EClass getParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.ParameterValue#getValueParameter
	 * <em>Value Parameter</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Value Parameter</em>'.
	 * @see fr.inria.aoste.syndex.ParameterValue#getValueParameter()
	 * @see #getParameterValue()
	 * @generated
	 */
	EAttribute getParameterValue_ValueParameter();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.ParameterValue#getParamDeclaration
	 * <em>Param Declaration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Param Declaration</em>'.
	 * @see fr.inria.aoste.syndex.ParameterValue#getParamDeclaration()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_ParamDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.ConditionedStructure
	 * <em>Conditioned Structure</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Conditioned Structure</em>'.
	 * @see fr.inria.aoste.syndex.ConditionedStructure
	 * @generated
	 */
	EClass getConditionedStructure();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.ConditionedStructure#getConditions <em>Conditions</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Conditions</em>'.
	 * @see fr.inria.aoste.syndex.ConditionedStructure#getConditions()
	 * @see #getConditionedStructure()
	 * @generated
	 */
	EReference getConditionedStructure_Conditions();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.ConditionedStructure#getPortCond <em>Port Cond</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Port Cond</em>'.
	 * @see fr.inria.aoste.syndex.ConditionedStructure#getPortCond()
	 * @see #getConditionedStructure()
	 * @generated
	 */
	EReference getConditionedStructure_PortCond();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.Condition <em>Condition</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see fr.inria.aoste.syndex.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.Condition#getValueCondition
	 * <em>Value Condition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Value Condition</em>'.
	 * @see fr.inria.aoste.syndex.Condition#getValueCondition()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_ValueCondition();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.Condition#getConditionPort
	 * <em>Condition Port</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Condition Port</em>'.
	 * @see fr.inria.aoste.syndex.Condition#getConditionPort()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_ConditionPort();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Condition#getComponentInstance <em>Component Instance</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Component Instance</em>'.
	 * @see fr.inria.aoste.syndex.Condition#getComponentInstance()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_ComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link fr.inria.aoste.syndex.Condition#getPortCondition <em>Port Condition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Port Condition</em>'.
	 * @see fr.inria.aoste.syndex.Condition#getPortCondition()
	 * @see #getCondition()
	 * @generated NOT
	 */
	EReference getCondition_ConditionPortReference();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.RepetitionConnection
	 * <em>Repetition Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Repetition Connection</em>'.
	 * @see fr.inria.aoste.syndex.RepetitionConnection
	 * @generated
	 */
	EClass getRepetitionConnection();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.syndex.RepetitionConnection#getRepetitionKind
	 * <em>Repetition Kind</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Repetition Kind</em>'.
	 * @see fr.inria.aoste.syndex.RepetitionConnection#getRepetitionKind()
	 * @see #getRepetitionConnection()
	 * @generated
	 */
	EAttribute getRepetitionConnection_RepetitionKind();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.RepetitionConnection#getSourceRepetitionConnection
	 * <em>Source Repetition Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source Repetition Connection</em>'.
	 * @see fr.inria.aoste.syndex.RepetitionConnection#getSourceRepetitionConnection()
	 * @see #getRepetitionConnection()
	 * @generated
	 */
	EReference getRepetitionConnection_SourceRepetitionConnection();

	/**
	 * Returns the meta object for the reference '
	 * {@link fr.inria.aoste.syndex.RepetitionConnection#getTargetRepetitionConnection
	 * <em>Target Repetition Connection</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target Repetition Connection</em>'.
	 * @see fr.inria.aoste.syndex.RepetitionConnection#getTargetRepetitionConnection()
	 * @see #getRepetitionConnection()
	 * @generated
	 */
	EReference getRepetitionConnection_TargetRepetitionConnection();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.INOUTInstance <em>INOUT Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>INOUT Instance</em>'.
	 * @see fr.inria.aoste.syndex.INOUTInstance
	 * @generated
	 */
	EClass getINOUTInstance();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.INOUTInstance#getOwnerINOUT
	 * <em>Owner INOUT</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner INOUT</em>'.
	 * @see fr.inria.aoste.syndex.INOUTInstance#getOwnerINOUT()
	 * @see #getINOUTInstance()
	 * @generated
	 */
	EReference getINOUTInstance_OwnerINOUT();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InCondInstance <em>In Cond Instance</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>In Cond Instance</em>'.
	 * @see fr.inria.aoste.syndex.InCondInstance
	 * @generated
	 */
	EClass getInCondInstance();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InCondInstance#getOwnerCond
	 * <em>Owner Cond</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Owner Cond</em>'.
	 * @see fr.inria.aoste.syndex.InCondInstance#getOwnerCond()
	 * @see #getInCondInstance()
	 * @generated
	 */
	EReference getInCondInstance_OwnerCond();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.syndex.InternalDependency <em>Internal Dependency</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Internal Dependency</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependency
	 * @generated
	 */
	EClass getInternalDependency();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependency#getSource
	 * <em>Source</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependency#getSource()
	 * @see #getInternalDependency()
	 * @generated
	 */
	EReference getInternalDependency_Source();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.syndex.InternalDependency#getTarget
	 * <em>Target</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.inria.aoste.syndex.InternalDependency#getTarget()
	 * @see #getInternalDependency()
	 * @generated
	 */
	EReference getInternalDependency_Target();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.syndex.CodeExecutionPhaseKind
	 * <em>Code Execution Phase Kind</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Code Execution Phase Kind</em>'.
	 * @see fr.inria.aoste.syndex.CodeExecutionPhaseKind
	 * @generated
	 */
	EEnum getCodeExecutionPhaseKind();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.syndex.RepetitionConnectionKind
	 * <em>Repetition Connection Kind</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Repetition Connection Kind</em>'.
	 * @see fr.inria.aoste.syndex.RepetitionConnectionKind
	 * @generated
	 */
	EEnum getRepetitionConnectionKind();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.syndex.DataTypeK <em>Data Type K</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Data Type K</em>'.
	 * @see fr.inria.aoste.syndex.DataTypeK
	 * @generated
	 */
	EEnum getDataTypeK();

	/**
	 * Returns the factory that creates the instances of the model. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SyndexFactory getSyndexFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ActuatorImpl <em>Actuator</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ActuatorImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getActuator()
		 * @generated
		 */
		EClass		ACTUATOR														= eINSTANCE.getActuator();

		/**
		 * The meta object literal for the '<em><b>Input Port Actuator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ACTUATOR__INPUT_PORT_ACTUATOR									= eINSTANCE
																							.getActuator_InputPortActuator();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.AtomicApplicationComponentImpl
		 * <em>Atomic Application Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.AtomicApplicationComponentImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAtomicApplicationComponent()
		 * @generated
		 */
		EClass		ATOMIC_APPLICATION_COMPONENT									= eINSTANCE
																							.getAtomicApplicationComponent();

		/**
		 * The meta object literal for the '<em><b>Atomic Application Structure</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE		= eINSTANCE
																							.getAtomicApplicationComponent_AtomicApplicationStructure();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.PortImpl <em>Port</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.PortImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getPort()
		 * @generated
		 */
		EClass		PORT															= eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Parent Port</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PORT__PARENT_PORT												= eINSTANCE.getPort_ParentPort();

		/**
		 * The meta object literal for the '<em><b>Size Port</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PORT__SIZE_PORT													= eINSTANCE.getPort_SizePort();

		/**
		 * The meta object literal for the '<em><b>Type Port</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	PORT__TYPE_PORT													= eINSTANCE.getPort_TypePort();

		/**
		 * The meta object literal for the '<em><b>Parent Condition</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated NOT
		 */
		EReference	PORT__PARENT_CONDITION											= eINSTANCE
																							.getPort_ParentConditionReference();

		/**
		 * The meta object literal for the '<em><b>Parent Compound Structure</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated NOT
		 */
		EReference	PORT__PARENT_COMPOUND_STRUCTURE									= eINSTANCE
																							.getPort_ParentCompoundStructure();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.NamedElementImpl <em>Named Element</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.NamedElementImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass		NAMED_ELEMENT													= eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NAMED_ELEMENT__NAME												= eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ApplicationComponentImpl
		 * <em>Application Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ApplicationComponentImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplicationComponent()
		 * @generated
		 */
		EClass		APPLICATION_COMPONENT											= eINSTANCE
																							.getApplicationComponent();

		/**
		 * The meta object literal for the '<em><b>Param Declarations</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION_COMPONENT__PARAM_DECLARATIONS						= eINSTANCE
																							.getApplicationComponent_ParamDeclarations();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.SensorImpl <em>Sensor</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.SensorImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSensor()
		 * @generated
		 */
		EClass		SENSOR															= eINSTANCE.getSensor();

		/**
		 * The meta object literal for the '<em><b>Output Port Sensor</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SENSOR__OUTPUT_PORT_SENSOR										= eINSTANCE
																							.getSensor_OutputPortSensor();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ApplicationImpl <em>Application</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ApplicationImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplication()
		 * @generated
		 */
		EClass		APPLICATION														= eINSTANCE.getApplication();

		/**
		 * The meta object literal for the '<em><b>Input Port Application</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION__INPUT_PORT_APPLICATION								= eINSTANCE
																							.getApplication_InputPortApplication();

		/**
		 * The meta object literal for the '<em><b>Output Port Application</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION__OUTPUT_PORT_APPLICATION							= eINSTANCE
																							.getApplication_OutputPortApplication();

		/**
		 * The meta object literal for the '<em><b>Application Internal Structure</b></em>' containment reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION__APPLICATION_INTERNAL_STRUCTURE						= eINSTANCE
																							.getApplication_ApplicationInternalStructure();

		/**
		 * The meta object literal for the '<em><b>Main</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	APPLICATION__MAIN												= eINSTANCE.getApplication_Main();

		/**
		 * The meta object literal for the '<em><b>Port Condition</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION__PORT_CONDITION										= eINSTANCE
																							.getApplication_PortCondition();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InputImpl <em>Input</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InputImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInput()
		 * @generated
		 */
		EClass		INPUT															= eINSTANCE.getInput();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ComponentImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getComponent()
		 * @generated
		 */
		EClass		COMPONENT														= eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Execution Phases</b></em>' attribute list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	COMPONENT__EXECUTION_PHASES										= eINSTANCE
																							.getComponent_ExecutionPhases();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.OutputImpl <em>Output</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.OutputImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getOutput()
		 * @generated
		 */
		EClass		OUTPUT															= eINSTANCE.getOutput();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.FunctionImpl <em>Function</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.FunctionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getFunction()
		 * @generated
		 */
		EClass		FUNCTION														= eINSTANCE.getFunction();

		/**
		 * The meta object literal for the '<em><b>Input Port Function</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	FUNCTION__INPUT_PORT_FUNCTION									= eINSTANCE
																							.getFunction_InputPortFunction();

		/**
		 * The meta object literal for the '<em><b>Output Port Function</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	FUNCTION__OUTPUT_PORT_FUNCTION									= eINSTANCE
																							.getFunction_OutputPortFunction();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConstantImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConstant()
		 * @generated
		 */
		EClass		CONSTANT														= eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '<em><b>One Output Constant</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONSTANT__ONE_OUTPUT_CONSTANT									= eINSTANCE
																							.getConstant_OneOutputConstant();

		/**
		 * The meta object literal for the '<em><b>Constant Value</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CONSTANT__CONSTANT_VALUE										= eINSTANCE
																							.getConstant_ConstantValue();

		/**
		 * The meta object literal for the '<em><b>Enum Values</b></em>' attribute list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CONSTANT__ENUM_VALUES											= eINSTANCE
																							.getConstant_EnumValues();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.DelayImpl <em>Delay</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.DelayImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDelay()
		 * @generated
		 */
		EClass		DELAY															= eINSTANCE.getDelay();

		/**
		 * The meta object literal for the '<em><b>One Input Delay</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DELAY__ONE_INPUT_DELAY											= eINSTANCE
																							.getDelay_OneInputDelay();

		/**
		 * The meta object literal for the '<em><b>One Output Delay</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DELAY__ONE_OUTPUT_DELAY											= eINSTANCE
																							.getDelay_OneOutputDelay();

		/**
		 * The meta object literal for the '<em><b>Delay Value</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DELAY__DELAY_VALUE												= eINSTANCE.getDelay_DelayValue();

		/**
		 * The meta object literal for the '<em><b>Enum Value</b></em>' attribute list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DELAY__ENUM_VALUE												= eINSTANCE.getDelay_EnumValue();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConnectionImpl <em>Connection</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnection()
		 * @generated
		 */
		EClass		CONNECTION														= eINSTANCE.getConnection();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.DataConnectionImpl
		 * <em>Data Connection</em>} ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.DataConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDataConnection()
		 * @generated
		 */
		EClass		DATA_CONNECTION													= eINSTANCE.getDataConnection();

		/**
		 * The meta object literal for the '<em><b>Source Data Connection</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DATA_CONNECTION__SOURCE_DATA_CONNECTION							= eINSTANCE
																							.getDataConnection_SourceDataConnection();

		/**
		 * The meta object literal for the '<em><b>Target Dataconnection</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DATA_CONNECTION__TARGET_DATACONNECTION							= eINSTANCE
																							.getDataConnection_TargetDataconnection();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InstanceImpl <em>Instance</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInstance()
		 * @generated
		 */
		EClass		INSTANCE														= eINSTANCE.getInstance();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl
		 * <em>Component Instance</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ComponentInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getComponentInstance()
		 * @generated
		 */
		EClass		COMPONENT_INSTANCE												= eINSTANCE.getComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port Instances</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COMPONENT_INSTANCE__PORT_INSTANCES								= eINSTANCE
																							.getComponentInstance_PortInstances();

		/**
		 * The meta object literal for the '<em><b>Referenced Component</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COMPONENT_INSTANCE__REFERENCED_COMPONENT						= eINSTANCE
																							.getComponentInstance_ReferencedComponent();

		/**
		 * The meta object literal for the '<em><b>Repetition Factor</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	COMPONENT_INSTANCE__REPETITION_FACTOR							= eINSTANCE
																							.getComponentInstance_RepetitionFactor();

		/**
		 * The meta object literal for the '<em><b>Parameter Values</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COMPONENT_INSTANCE__PARAMETER_VALUES							= eINSTANCE
																							.getComponentInstance_ParameterValues();

		/**
		 * The meta object literal for the '<em><b>Referenced HW Component</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT						= eINSTANCE
																							.getComponentInstance_ReferencedHWComponent();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.PortInstanceImpl <em>Port Instance</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.PortInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getPortInstance()
		 * @generated
		 */
		EClass		PORT_INSTANCE													= eINSTANCE.getPortInstance();

		/**
		 * The meta object literal for the '<em><b>Parent Component Instance</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PORT_INSTANCE__PARENT_COMPONENT_INSTANCE						= eINSTANCE
																							.getPortInstance_ParentComponentInstance();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.INInstanceImpl <em>IN Instance</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.INInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINInstance()
		 * @generated
		 */
		EClass		IN_INSTANCE														= eINSTANCE.getINInstance();

		/**
		 * The meta object literal for the '<em><b>Owner Input</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	IN_INSTANCE__OWNER_INPUT										= eINSTANCE
																							.getINInstance_OwnerInput();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.OutInstanceImpl <em>Out Instance</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.OutInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getOutInstance()
		 * @generated
		 */
		EClass		OUT_INSTANCE													= eINSTANCE.getOutInstance();

		/**
		 * The meta object literal for the '<em><b>Owner Output</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OUT_INSTANCE__OWNER_OUTPUT										= eINSTANCE
																							.getOutInstance_OwnerOutput();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl
		 * <em>Internal Dependency IN</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InternalDependencyINImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependencyIN()
		 * @generated
		 */
		EClass		INTERNAL_DEPENDENCY_IN											= eINSTANCE
																							.getInternalDependencyIN();

		/**
		 * The meta object literal for the '<em><b>Source IDI</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_IN__SOURCE_IDI								= eINSTANCE
																							.getInternalDependencyIN_SourceIDI();

		/**
		 * The meta object literal for the '<em><b>Target IDI</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_IN__TARGET_IDI								= eINSTANCE
																							.getInternalDependencyIN_TargetIDI();

		/**
		 * The meta object literal for the '<em><b>Source IDIC</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_IN__SOURCE_IDIC								= eINSTANCE
																							.getInternalDependencyIN_SourceIDIC();

		/**
		 * The meta object literal for the '<em><b>Target IDIC</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_IN__TARGET_IDIC								= eINSTANCE
																							.getInternalDependencyIN_TargetIDIC();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl
		 * <em>Internal Dependency OUT</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependencyOUT()
		 * @generated
		 */
		EClass		INTERNAL_DEPENDENCY_OUT											= eINSTANCE
																							.getInternalDependencyOUT();

		/**
		 * The meta object literal for the '<em><b>Source IDO</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_OUT__SOURCE_IDO								= eINSTANCE
																							.getInternalDependencyOUT_SourceIDO();

		/**
		 * The meta object literal for the '<em><b>Target IDO</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY_OUT__TARGET_IDO								= eINSTANCE
																							.getInternalDependencyOUT_TargetIDO();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConnecteableElementImpl
		 * <em>Connecteable Element</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConnecteableElementImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnecteableElement()
		 * @generated
		 */
		EClass		CONNECTEABLE_ELEMENT											= eINSTANCE
																							.getConnecteableElement();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InCondPortImpl <em>In Cond Port</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InCondPortImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInCondPort()
		 * @generated
		 */
		EClass		IN_COND_PORT													= eINSTANCE.getInCondPort();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.HardwareComponentImpl
		 * <em>Hardware Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.HardwareComponentImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHardwareComponent()
		 * @generated
		 */
		EClass		HARDWARE_COMPONENT												= eINSTANCE.getHardwareComponent();

		/**
		 * The meta object literal for the '<em><b>HW Connection</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	HARDWARE_COMPONENT__HW_CONNECTION								= eINSTANCE
																							.getHardwareComponent_HWConnection();

		/**
		 * The meta object literal for the '<em><b>In Out HW Component</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT							= eINSTANCE
																							.getHardwareComponent_InOutHWComponent();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.HardwareImpl <em>Hardware</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.HardwareImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHardware()
		 * @generated
		 */
		EClass		HARDWARE														= eINSTANCE.getHardware();

		/**
		 * The meta object literal for the '<em><b>Component HW Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	HARDWARE__COMPONENT_HW_INSTANCE									= eINSTANCE
																							.getHardware_ComponentHWInstance();

		/**
		 * The meta object literal for the '<em><b>Main</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	HARDWARE__MAIN													= eINSTANCE.getHardware_Main();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.MediaImpl <em>Media</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.MediaImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getMedia()
		 * @generated
		 */
		EClass		MEDIA															= eINSTANCE.getMedia();

		/**
		 * The meta object literal for the '<em><b>Bandwidth</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MEDIA__BANDWIDTH												= eINSTANCE.getMedia_Bandwidth();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MEDIA__SIZE														= eINSTANCE.getMedia_Size();

		/**
		 * The meta object literal for the '<em><b>Parent Memory</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MEDIA__PARENT_MEMORY											= eINSTANCE.getMedia_ParentMemory();

		/**
		 * The meta object literal for the '<em><b>Broadcast</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MEDIA__BROADCAST												= eINSTANCE.getMedia_Broadcast();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MEDIA__DURATION													= eINSTANCE.getMedia_Duration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.FPGAImpl <em>FPGA</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.FPGAImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getFPGA()
		 * @generated
		 */
		EClass		FPGA															= eINSTANCE.getFPGA();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.AsicImpl <em>Asic</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.AsicImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAsic()
		 * @generated
		 */
		EClass		ASIC															= eINSTANCE.getAsic();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.DSPImpl <em>DSP</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.DSPImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDSP()
		 * @generated
		 */
		EClass		DSP																= eINSTANCE.getDSP();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.RAMImpl <em>RAM</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.RAMImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRAM()
		 * @generated
		 */
		EClass		RAM																= eINSTANCE.getRAM();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.SAMImpl <em>SAM</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.SAMImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSAM()
		 * @generated
		 */
		EClass		SAM																= eINSTANCE.getSAM();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.SAMPTPImpl <em>SAMPTP</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.SAMPTPImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSAMPTP()
		 * @generated
		 */
		EClass		SAMPTP															= eINSTANCE.getSAMPTP();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.HWConnectionImpl <em>HW Connection</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.HWConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getHWConnection()
		 * @generated
		 */
		EClass		HW_CONNECTION													= eINSTANCE.getHWConnection();

		/**
		 * The meta object literal for the '<em><b>Parent HW Connection</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	HW_CONNECTION__PARENT_HW_CONNECTION								= eINSTANCE
																							.getHWConnection_ParentHWConnection();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConnectionHWImpl <em>Connection HW</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConnectionHWImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConnectionHW()
		 * @generated
		 */
		EClass		CONNECTION_HW													= eINSTANCE.getConnectionHW();

		/**
		 * The meta object literal for the '<em><b>Source In Out</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONNECTION_HW__SOURCE_IN_OUT									= eINSTANCE
																							.getConnectionHW_SourceInOut();

		/**
		 * The meta object literal for the '<em><b>Target In Out</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONNECTION_HW__TARGET_IN_OUT									= eINSTANCE
																							.getConnectionHW_TargetInOut();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.CaracteristicsImpl
		 * <em>Caracteristics</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.CaracteristicsImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCaracteristics()
		 * @generated
		 */
		EClass		CARACTERISTICS													= eINSTANCE.getCaracteristics();

		/**
		 * The meta object literal for the '<em><b>Adequation</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CARACTERISTICS__ADEQUATION										= eINSTANCE
																							.getCaracteristics_Adequation();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl
		 * <em>Association SWto HW</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAssociationSWtoHW()
		 * @generated
		 */
		EClass		ASSOCIATION_SWTO_HW												= eINSTANCE.getAssociationSWtoHW();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ASSOCIATION_SWTO_HW__DURATION									= eINSTANCE
																							.getAssociationSWtoHW_Duration();

		/**
		 * The meta object literal for the '<em><b>Source Appli</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ASSOCIATION_SWTO_HW__SOURCE_APPLI								= eINSTANCE
																							.getAssociationSWtoHW_SourceAppli();

		/**
		 * The meta object literal for the '<em><b>Target HW</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ASSOCIATION_SWTO_HW__TARGET_HW									= eINSTANCE
																							.getAssociationSWtoHW_TargetHW();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.AssociationComtoProcImpl
		 * <em>Association Comto Proc</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.AssociationComtoProcImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getAssociationComtoProc()
		 * @generated
		 */
		EClass		ASSOCIATION_COMTO_PROC											= eINSTANCE
																							.getAssociationComtoProc();

		/**
		 * The meta object literal for the '<em><b>Source Association Com To Proc</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC			= eINSTANCE
																							.getAssociationComtoProc_SourceAssociationComToProc();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ASSOCIATION_COMTO_PROC__DURATION								= eINSTANCE
																							.getAssociationComtoProc_Duration();

		/**
		 * The meta object literal for the '<em><b>Target HW Process</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS						= eINSTANCE
																							.getAssociationComtoProc_TargetHWProcess();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.DurationImpl <em>Duration</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.DurationImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDuration()
		 * @generated
		 */
		EClass		DURATION														= eINSTANCE.getDuration();

		/**
		 * The meta object literal for the '<em><b>Enum Duration</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DURATION__ENUM_DURATION											= eINSTANCE
																							.getDuration_EnumDuration();

		/**
		 * The meta object literal for the '<em><b>Parent Duration</b></em>' reference list feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DURATION__PARENT_DURATION										= eINSTANCE
																							.getDuration_ParentDuration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.EnumLitDurationImpl
		 * <em>Enum Lit Duration</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.EnumLitDurationImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getEnumLitDuration()
		 * @generated
		 */
		EClass		ENUM_LIT_DURATION												= eINSTANCE.getEnumLitDuration();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ENUM_LIT_DURATION__VALUE										= eINSTANCE
																							.getEnumLitDuration_Value();

		/**
		 * The meta object literal for the '<em><b>Parent Enum Duration</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ENUM_LIT_DURATION__PARENT_ENUM_DURATION							= eINSTANCE
																							.getEnumLitDuration_ParentEnumDuration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.DatatypeImpl <em>Datatype</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.DatatypeImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDatatype()
		 * @generated
		 */
		EClass		DATATYPE														= eINSTANCE.getDatatype();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.SizeImpl <em>Size</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.SizeImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSize()
		 * @generated
		 */
		EClass		SIZE															= eINSTANCE.getSize();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ArrayImpl <em>Array</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ArrayImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getArray()
		 * @generated
		 */
		EClass		ARRAY															= eINSTANCE.getArray();

		/**
		 * The meta object literal for the '<em><b>Vectors</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARRAY__VECTORS													= eINSTANCE.getArray_Vectors();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.VectorImpl <em>Vector</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.VectorImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getVector()
		 * @generated
		 */
		EClass		VECTOR															= eINSTANCE.getVector();

		/**
		 * The meta object literal for the '<em><b>Vector</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	VECTOR__VECTOR													= eINSTANCE.getVector_Vector();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ProcessorImpl <em>Processor</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ProcessorImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getProcessor()
		 * @generated
		 */
		EClass		PROCESSOR														= eINSTANCE.getProcessor();

		/**
		 * The meta object literal for the '<em><b>Parent Processor</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PROCESSOR__PARENT_PROCESSOR										= eINSTANCE
																							.getProcessor_ParentProcessor();

		/**
		 * The meta object literal for the '<em><b>Duration SW</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PROCESSOR__DURATION_SW											= eINSTANCE
																							.getProcessor_DurationSW();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ElementImpl <em>Element</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ElementImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getElement()
		 * @generated
		 */
		EClass		ELEMENT															= eINSTANCE.getElement();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.SynDExImpl <em>Syn DEx</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.SynDExImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getSynDEx()
		 * @generated
		 */
		EClass		SYN_DEX															= eINSTANCE.getSynDEx();

		/**
		 * The meta object literal for the '<em><b>Syn DExelements</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SYN_DEX__SYN_DEXELEMENTS										= eINSTANCE
																							.getSynDEx_SynDExelements();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ApplicationConnectionImpl
		 * <em>Application Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ApplicationConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getApplicationConnection()
		 * @generated
		 */
		EClass		APPLICATION_CONNECTION											= eINSTANCE
																							.getApplicationConnection();

		/**
		 * The meta object literal for the '<em><b>Parent Coumpound Application Connection</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION	= eINSTANCE
																							.getApplicationConnection_ParentCoumpoundApplicationConnection();

		/**
		 * The meta object literal for the '<em><b>Parent Condition Application Connection</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION	= eINSTANCE
																							.getApplicationConnection_ParentConditionApplicationConnection();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InternalStructureImpl
		 * <em>Internal Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InternalStructureImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalStructure()
		 * @generated
		 */
		EClass		INTERNAL_STRUCTURE												= eINSTANCE.getInternalStructure();

		/**
		 * The meta object literal for the '<em><b>Owner Internal Structure</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE					= eINSTANCE
																							.getInternalStructure_OwnerInternalStructure();

		/**
		 * The meta object literal for the '<em><b>Internal Connector</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_STRUCTURE__INTERNAL_CONNECTOR							= eINSTANCE
																							.getInternalStructure_InternalConnector();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ElementaryStructureImpl
		 * <em>Elementary Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ElementaryStructureImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getElementaryStructure()
		 * @generated
		 */
		EClass		ELEMENTARY_STRUCTURE											= eINSTANCE
																							.getElementaryStructure();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.CompoundStructureImpl
		 * <em>Compound Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.CompoundStructureImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCompoundStructure()
		 * @generated
		 */
		EClass		COMPOUND_STRUCTURE												= eINSTANCE.getCompoundStructure();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	COMPOUND_STRUCTURE__COMPONENT_INSTANCE							= eINSTANCE
																							.getCompoundStructure_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port Compound Structure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated NOT
		 */
		EReference	COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE						= eINSTANCE
																							.getCompoundStructure_PortCompoundStructure();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.INOUTImpl <em>INOUT</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.INOUTImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINOUT()
		 * @generated
		 */
		EClass		INOUT															= eINSTANCE.getINOUT();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	INOUT__TYPE														= eINSTANCE.getINOUT_Type();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InternalDataConnectionImpl
		 * <em>Internal Data Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InternalDataConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDataConnection()
		 * @generated
		 */
		EClass		INTERNAL_DATA_CONNECTION										= eINSTANCE
																							.getInternalDataConnection();

		/**
		 * The meta object literal for the '<em><b>Source Out Instance</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE					= eINSTANCE
																							.getInternalDataConnection_SourceOutInstance();

		/**
		 * The meta object literal for the '<em><b>Target IN Instance</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE					= eINSTANCE
																							.getInternalDataConnection_TargetINInstance();

		/**
		 * The meta object literal for the '<em><b>Target IN Instance C</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C					= eINSTANCE
																							.getInternalDataConnection_TargetINInstanceC();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ParameterDeclarationImpl
		 * <em>Parameter Declaration</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ParameterDeclarationImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getParameterDeclaration()
		 * @generated
		 */
		EClass		PARAMETER_DECLARATION											= eINSTANCE
																							.getParameterDeclaration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ParameterValueImpl
		 * <em>Parameter Value</em>} ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ParameterValueImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getParameterValue()
		 * @generated
		 */
		EClass		PARAMETER_VALUE													= eINSTANCE.getParameterValue();

		/**
		 * The meta object literal for the '<em><b>Value Parameter</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	PARAMETER_VALUE__VALUE_PARAMETER								= eINSTANCE
																							.getParameterValue_ValueParameter();

		/**
		 * The meta object literal for the '<em><b>Param Declaration</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PARAMETER_VALUE__PARAM_DECLARATION								= eINSTANCE
																							.getParameterValue_ParamDeclaration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConditionedStructureImpl
		 * <em>Conditioned Structure</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConditionedStructureImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getConditionedStructure()
		 * @generated
		 */
		EClass		CONDITIONED_STRUCTURE											= eINSTANCE
																							.getConditionedStructure();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONDITIONED_STRUCTURE__CONDITIONS								= eINSTANCE
																							.getConditionedStructure_Conditions();

		/**
		 * The meta object literal for the '<em><b>Port Cond</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONDITIONED_STRUCTURE__PORT_COND								= eINSTANCE
																							.getConditionedStructure_PortCond();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.ConditionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCondition()
		 * @generated
		 */
		EClass		CONDITION														= eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Value Condition</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CONDITION__VALUE_CONDITION										= eINSTANCE
																							.getCondition_ValueCondition();

		/**
		 * The meta object literal for the '<em><b>Condition Port</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONDITION__CONDITION_PORT										= eINSTANCE
																							.getCondition_ConditionPort();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CONDITION__COMPONENT_INSTANCE									= eINSTANCE
																							.getCondition_ComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Port Condition</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated NOT
		 */
		EReference	CONDITION__CONDITION_PORT_REFERENCE								= eINSTANCE
																							.getCondition_ConditionPortReference();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.RepetitionConnectionImpl
		 * <em>Repetition Connection</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.RepetitionConnectionImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRepetitionConnection()
		 * @generated
		 */
		EClass		REPETITION_CONNECTION											= eINSTANCE
																							.getRepetitionConnection();

		/**
		 * The meta object literal for the '<em><b>Repetition Kind</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	REPETITION_CONNECTION__REPETITION_KIND							= eINSTANCE
																							.getRepetitionConnection_RepetitionKind();

		/**
		 * The meta object literal for the '<em><b>Source Repetition Connection</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION				= eINSTANCE
																							.getRepetitionConnection_SourceRepetitionConnection();

		/**
		 * The meta object literal for the '<em><b>Target Repetition Connection</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION				= eINSTANCE
																							.getRepetitionConnection_TargetRepetitionConnection();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.INOUTInstanceImpl <em>INOUT Instance</em>}
		 * ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.INOUTInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getINOUTInstance()
		 * @generated
		 */
		EClass		INOUT_INSTANCE													= eINSTANCE.getINOUTInstance();

		/**
		 * The meta object literal for the '<em><b>Owner INOUT</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INOUT_INSTANCE__OWNER_INOUT										= eINSTANCE
																							.getINOUTInstance_OwnerINOUT();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InCondInstanceImpl
		 * <em>In Cond Instance</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InCondInstanceImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInCondInstance()
		 * @generated
		 */
		EClass		IN_COND_INSTANCE												= eINSTANCE.getInCondInstance();

		/**
		 * The meta object literal for the '<em><b>Owner Cond</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	IN_COND_INSTANCE__OWNER_COND									= eINSTANCE
																							.getInCondInstance_OwnerCond();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.impl.InternalDependencyImpl
		 * <em>Internal Dependency</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.impl.InternalDependencyImpl
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getInternalDependency()
		 * @generated
		 */
		EClass		INTERNAL_DEPENDENCY												= eINSTANCE.getInternalDependency();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY__SOURCE										= eINSTANCE
																							.getInternalDependency_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERNAL_DEPENDENCY__TARGET										= eINSTANCE
																							.getInternalDependency_Target();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.CodeExecutionPhaseKind
		 * <em>Code Execution Phase Kind</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.CodeExecutionPhaseKind
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getCodeExecutionPhaseKind()
		 * @generated
		 */
		EEnum		CODE_EXECUTION_PHASE_KIND										= eINSTANCE
																							.getCodeExecutionPhaseKind();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.RepetitionConnectionKind
		 * <em>Repetition Connection Kind</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.RepetitionConnectionKind
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getRepetitionConnectionKind()
		 * @generated
		 */
		EEnum		REPETITION_CONNECTION_KIND										= eINSTANCE
																							.getRepetitionConnectionKind();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.syndex.DataTypeK <em>Data Type K</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see fr.inria.aoste.syndex.DataTypeK
		 * @see fr.inria.aoste.syndex.impl.SyndexPackageImpl#getDataTypeK()
		 * @generated
		 */
		EEnum		DATA_TYPE_K														= eINSTANCE.getDataTypeK();

	}

} // SyndexPackage
