/**
 * <copyright> </copyright>
 * 
 * $Id: AssociationComtoProcValidator.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.validation;

import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.Processor;

/**
 * A sample validator interface for {@link fr.inria.aoste.syndex.AssociationComtoProc}. This doesn't really do anything,
 * and it's not a real EMF artifact. It was generated by the org.eclipse.emf.examples.generator.validator plug-in to
 * illustrate how EMF's code generator can be extended. This can be disabled with -vmargs
 * -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface AssociationComtoProcValidator
{
	boolean validate();

	boolean validateSourceAssociationComToProc(Media value);
	boolean validateDuration(Duration value);
	boolean validateTargetHWProcess(Processor value);
}
