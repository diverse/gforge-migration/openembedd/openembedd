/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Hardware.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Hardware</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Hardware#getComponentHWInstance <em>Component HW Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Hardware#isMain <em>Main</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getHardware()
 * @model
 * @generated
 */
public interface Hardware extends HardwareComponent
{
	/**
	 * Returns the value of the '<em><b>Component HW Instance</b></em>' containment reference list. The list contents
	 * are of type {@link fr.inria.aoste.syndex.ComponentInstance}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component HW Instance</em>' containment reference list isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Component HW Instance</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getHardware_ComponentHWInstance()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentHWInstance();

	/**
	 * Returns the value of the '<em><b>Main</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main</em>' attribute isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Main</em>' attribute.
	 * @see #setMain(boolean)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getHardware_Main()
	 * @model
	 * @generated
	 */
	boolean isMain();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Hardware#isMain <em>Main</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Main</em>' attribute.
	 * @see #isMain()
	 * @generated
	 */
	void setMain(boolean value);

} // Hardware
