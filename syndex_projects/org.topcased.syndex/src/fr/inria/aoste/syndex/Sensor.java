/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Sensor.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Sensor</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Sensor#getOutputPortSensor <em>Output Port Sensor</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getSensor()
 * @model
 * @generated
 */
public interface Sensor extends AtomicApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>Output Port Sensor</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.Output}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Port Sensor</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Output Port Sensor</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getSensor_OutputPortSensor()
	 * @model containment="true"
	 * @generated
	 */
	EList<Output> getOutputPortSensor();

} // Sensor
