/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InCondInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>In Cond Instance</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InCondInstance#getOwnerCond <em>Owner Cond</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInCondInstance()
 * @model
 * @generated
 */
public interface InCondInstance extends PortInstance
{
	/**
	 * Returns the value of the '<em><b>Owner Cond</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner Cond</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner Cond</em>' reference.
	 * @see #setOwnerCond(InCondPort)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInCondInstance_OwnerCond()
	 * @model required="true"
	 * @generated
	 */
	InCondPort getOwnerCond();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InCondInstance#getOwnerCond <em>Owner Cond</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Owner Cond</em>' reference.
	 * @see #getOwnerCond()
	 * @generated
	 */
	void setOwnerCond(InCondPort value);

} // InCondInstance
