/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Media.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Media</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Media#getBandwidth <em>Bandwidth</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Media#getSize <em>Size</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Media#getParentMemory <em>Parent Memory</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Media#isBroadcast <em>Broadcast</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Media#getDuration <em>Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia()
 * @model
 * @generated
 */
public interface Media extends HardwareComponent
{
	/**
	 * Returns the value of the '<em><b>Bandwidth</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bandwidth</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Bandwidth</em>' attribute.
	 * @see #setBandwidth(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia_Bandwidth()
	 * @model required="true"
	 * @generated
	 */
	int getBandwidth();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Media#getBandwidth <em>Bandwidth</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Bandwidth</em>' attribute.
	 * @see #getBandwidth()
	 * @generated
	 */
	void setBandwidth(int value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia_Size()
	 * @model required="true"
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Media#getSize <em>Size</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

	/**
	 * Returns the value of the '<em><b>Parent Memory</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Memory</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Memory</em>' reference.
	 * @see #setParentMemory(Media)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia_ParentMemory()
	 * @model
	 * @generated
	 */
	Media getParentMemory();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Media#getParentMemory <em>Parent Memory</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Memory</em>' reference.
	 * @see #getParentMemory()
	 * @generated
	 */
	void setParentMemory(Media value);

	/**
	 * Returns the value of the '<em><b>Broadcast</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Broadcast</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Broadcast</em>' attribute.
	 * @see #setBroadcast(boolean)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia_Broadcast()
	 * @model
	 * @generated
	 */
	boolean isBroadcast();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Media#isBroadcast <em>Broadcast</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Broadcast</em>' attribute.
	 * @see #isBroadcast()
	 * @generated
	 */
	void setBroadcast(boolean value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration</em>' reference.
	 * @see #setDuration(Duration)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getMedia_Duration()
	 * @model
	 * @generated
	 */
	Duration getDuration();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Media#getDuration <em>Duration</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration</em>' reference.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Duration value);

} // Media
