/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: OutInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Out Instance</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.OutInstance#getOwnerOutput <em>Owner Output</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getOutInstance()
 * @model
 * @generated
 */
public interface OutInstance extends PortInstance
{
	/**
	 * Returns the value of the '<em><b>Owner Output</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner Output</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner Output</em>' reference.
	 * @see #setOwnerOutput(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getOutInstance_OwnerOutput()
	 * @model required="true"
	 * @generated
	 */
	Output getOwnerOutput();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.OutInstance#getOwnerOutput <em>Owner Output</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Owner Output</em>' reference.
	 * @see #getOwnerOutput()
	 * @generated
	 */
	void setOwnerOutput(Output value);

} // OutInstance
