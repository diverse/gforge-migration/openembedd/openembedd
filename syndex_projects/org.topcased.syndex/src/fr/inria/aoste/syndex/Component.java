/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Component.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Component</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Component#getExecutionPhases <em>Execution Phases</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Execution Phases</b></em>' attribute list. The list contents are of type
	 * {@link fr.inria.aoste.syndex.CodeExecutionPhaseKind}. The literals are from the enumeration
	 * {@link fr.inria.aoste.syndex.CodeExecutionPhaseKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Phases</em>' attribute list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Execution Phases</em>' attribute list.
	 * @see fr.inria.aoste.syndex.CodeExecutionPhaseKind
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponent_ExecutionPhases()
	 * @model default=""
	 * @generated
	 */
	EList<CodeExecutionPhaseKind> getExecutionPhases();

} // Component
