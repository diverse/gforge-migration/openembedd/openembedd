/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: CompoundStructure.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Compound Structure</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.CompoundStructure#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getCompoundStructure()
 * @model
 * @generated
 */
public interface CompoundStructure extends InternalStructure
{
	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.ComponentInstance}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Component Instance</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCompoundStructure_ComponentInstance()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentInstance();

	/**
	 * Returns the value of the '<em><b>Port Compound Structure</b></em>' containment reference list. The list contents
	 * are of type {@link fr.inria.aoste.syndex.Port}. It is bidirectional and its opposite is '
	 * {@link fr.inria.aoste.syndex.Port#getParentConditionedStructure <em>Parent Conditioned Structure</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Conditioned Structure</em>' containment reference list isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Port Conditioned Structure</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConditionedStructure_PortConditionedStructure()
	 * @see fr.inria.aoste.syndex.Port#getParentConditionedStructure
	 * @model type="fr.inria.aoste.syndex.Port" opposite="parentConditionedStructure" containment="true" required="true"
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	EList getPortCompoundStructure();

} // CompoundStructure
