/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConditionItemProvider.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.SynDExPlugin;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.syndex.Condition} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ConditionItemProvider extends NamedElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConditionItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addOwnerInternalStructurePropertyDescriptor(object);
			addValueConditionPropertyDescriptor(object);
			addConditionPortPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Owner Internal Structure feature. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addOwnerInternalStructurePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_InternalStructure_ownerInternalStructure_feature"), getString(
				"_UI_PropertyDescriptor_description", "_UI_InternalStructure_ownerInternalStructure_feature",
				"_UI_InternalStructure_type"), SyndexPackage.Literals.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE,
			true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Value Condition feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addValueConditionPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Condition_valueCondition_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Condition_valueCondition_feature",
				"_UI_Condition_type"), SyndexPackage.Literals.CONDITION__VALUE_CONDITION, true, false, false,
			ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Condition Port feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addConditionPortPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
					getResourceLocator(), getString("_UI_Condition_conditionPort_feature"), getString(
						"_UI_PropertyDescriptor_description", "_UI_Condition_conditionPort_feature",
						"_UI_Condition_type"), SyndexPackage.Literals.CONDITION__CONDITION_PORT, true, false, true,
					null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR);
			childrenFeatures.add(SyndexPackage.Literals.CONDITION__COMPONENT_INSTANCE);
			childrenFeatures.add(SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Condition.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Condition"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Condition) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Condition_type") : getString("_UI_Condition_type")
				+ " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Condition.class))
		{
		case SyndexPackage.CONDITION__VALUE_CONDITION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR,
			SyndexFactory.eINSTANCE.createDataConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR,
			SyndexFactory.eINSTANCE.createInternalDependencyIN()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR,
			SyndexFactory.eINSTANCE.createInternalDependencyOUT()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR,
			SyndexFactory.eINSTANCE.createInternalDataConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR,
			SyndexFactory.eINSTANCE.createRepetitionConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.CONDITION__COMPONENT_INSTANCE,
			SyndexFactory.eINSTANCE.createComponentInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE,
			SyndexFactory.eINSTANCE.createInput()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE,
			SyndexFactory.eINSTANCE.createOutput()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE,
			SyndexFactory.eINSTANCE.createInCondPort()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE,
			SyndexFactory.eINSTANCE.createINOUT()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public String getCreateChildText(Object owner, Object feature, Object child, Collection selection)
	{
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == SyndexPackage.Literals.CONDITION__CONDITION_PORT_REFERENCE;

		if (qualify)
		{
			return getString("_UI_CreateChild_text2", new Object[] { getTypeText(childObject),
					getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return SynDExPlugin.INSTANCE;
	}

}
