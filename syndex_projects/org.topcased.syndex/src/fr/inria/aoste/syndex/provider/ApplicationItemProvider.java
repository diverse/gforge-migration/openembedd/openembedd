/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationItemProvider.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.SynDExPlugin;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.syndex.Application} object. <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ApplicationItemProvider extends ApplicationComponentItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addMainPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Main feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMainPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Application_main_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Application_main_feature", "_UI_Application_type"),
			SyndexPackage.Literals.APPLICATION__MAIN, true, false, false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(SyndexPackage.Literals.APPLICATION__INPUT_PORT_APPLICATION);
			childrenFeatures.add(SyndexPackage.Literals.APPLICATION__OUTPUT_PORT_APPLICATION);
			childrenFeatures.add(SyndexPackage.Literals.APPLICATION__APPLICATION_INTERNAL_STRUCTURE);
			childrenFeatures.add(SyndexPackage.Literals.APPLICATION__PORT_CONDITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Application.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Application"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Application) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Application_type")
				: getString("_UI_Application_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Application.class))
		{
		case SyndexPackage.APPLICATION__MAIN:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.APPLICATION__INPUT_PORT_APPLICATION,
			SyndexFactory.eINSTANCE.createInput()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.APPLICATION__OUTPUT_PORT_APPLICATION,
			SyndexFactory.eINSTANCE.createOutput()));

		newChildDescriptors.add(createChildParameter(
			SyndexPackage.Literals.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, SyndexFactory.eINSTANCE
					.createElementaryStructure()));

		newChildDescriptors.add(createChildParameter(
			SyndexPackage.Literals.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, SyndexFactory.eINSTANCE
					.createCompoundStructure()));

		newChildDescriptors.add(createChildParameter(
			SyndexPackage.Literals.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, SyndexFactory.eINSTANCE
					.createConditionedStructure()));

		newChildDescriptors.add(createChildParameter(
			SyndexPackage.Literals.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, SyndexFactory.eINSTANCE
					.createCondition()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.APPLICATION__PORT_CONDITION,
			SyndexFactory.eINSTANCE.createInCondPort()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return SynDExPlugin.INSTANCE;
	}

}
