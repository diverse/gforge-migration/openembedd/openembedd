/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SynDExItemProvider.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.inria.aoste.syndex.SynDEx;
import fr.inria.aoste.syndex.SynDExPlugin;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.syndex.SynDEx} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class SynDExItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SynDExItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SynDEx.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SynDEx"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		return getString("_UI_SynDEx_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(SynDEx.class))
		{
		case SyndexPackage.SYN_DEX__SYN_DEXELEMENTS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createActuator()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createSensor()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createApplication()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInput()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createOutput()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createConstant()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createDelay()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createDataConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createComponentInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createINInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createOutInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInternalDependencyIN()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInternalDependencyOUT()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createConnecteableElement()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInCondPort()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createHardware()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createMedia()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createProcessor()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createFPGA()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createAsic()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createDSP()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createRAM()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createSAM()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createSAMPTP()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createConnectionHW()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createAssociationSWtoHW()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createAssociationComtoProc()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createDuration()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createEnumLitDuration()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createDatatype()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createSize()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createArray()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createVector()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createElementaryStructure()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createCompoundStructure()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createINOUT()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInternalDataConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createParameterDeclaration()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createParameterValue()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createConditionedStructure()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createCondition()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createRepetitionConnection()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createINOUTInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInCondInstance()));

		newChildDescriptors.add(createChildParameter(SyndexPackage.Literals.SYN_DEX__SYN_DEXELEMENTS,
			SyndexFactory.eINSTANCE.createInternalDependency()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return SynDExPlugin.INSTANCE;
	}

}
