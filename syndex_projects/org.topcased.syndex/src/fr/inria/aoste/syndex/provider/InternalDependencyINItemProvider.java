/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependencyINItemProvider.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import fr.inria.aoste.syndex.SynDExPlugin;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.syndex.InternalDependencyIN} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class InternalDependencyINItemProvider extends ApplicationConnectionItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider,
		IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalDependencyINItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addSourceIDIPropertyDescriptor(object);
			addTargetIDIPropertyDescriptor(object);
			addSourceIDICPropertyDescriptor(object);
			addTargetIDICPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source IDI feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSourceIDIPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_InternalDependencyIN_sourceIDI_feature"), getString("_UI_PropertyDescriptor_description",
				"_UI_InternalDependencyIN_sourceIDI_feature", "_UI_InternalDependencyIN_type"),
			SyndexPackage.Literals.INTERNAL_DEPENDENCY_IN__SOURCE_IDI, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Target IDI feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addTargetIDIPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_InternalDependencyIN_targetIDI_feature"), getString("_UI_PropertyDescriptor_description",
				"_UI_InternalDependencyIN_targetIDI_feature", "_UI_InternalDependencyIN_type"),
			SyndexPackage.Literals.INTERNAL_DEPENDENCY_IN__TARGET_IDI, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Source IDIC feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSourceIDICPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_InternalDependencyIN_sourceIDIC_feature"), getString("_UI_PropertyDescriptor_description",
				"_UI_InternalDependencyIN_sourceIDIC_feature", "_UI_InternalDependencyIN_type"),
			SyndexPackage.Literals.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Target IDIC feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addTargetIDICPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_InternalDependencyIN_targetIDIC_feature"), getString("_UI_PropertyDescriptor_description",
				"_UI_InternalDependencyIN_targetIDIC_feature", "_UI_InternalDependencyIN_type"),
			SyndexPackage.Literals.INTERNAL_DEPENDENCY_IN__TARGET_IDIC, true, false, true, null, null, null));
	}

	/**
	 * This returns InternalDependencyIN.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/InternalDependencyIN"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		return getString("_UI_InternalDependencyIN_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return SynDExPlugin.INSTANCE;
	}

}
