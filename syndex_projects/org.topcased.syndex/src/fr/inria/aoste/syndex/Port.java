/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Port.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Port</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Port#getParentPort <em>Parent Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Port#getSizePort <em>Size Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Port#getTypePort <em>Type Port</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getPort()
 * @model abstract="true" annotation="http://www.topcased.org/uuid uuid='115217288922755'"
 * @generated
 */
public interface Port extends NamedElement, ConnecteableElement
{
	/**
	 * Returns the value of the '<em><b>Parent Port</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Port</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Port</em>' reference.
	 * @see #setParentPort(Component)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPort_ParentPort()
	 * @model required="true"
	 * @generated
	 */
	Component getParentPort();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Port#getParentPort <em>Parent Port</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Port</em>' reference.
	 * @see #getParentPort()
	 * @generated
	 */
	void setParentPort(Component value);

	/**
	 * Returns the value of the '<em><b>Size Port</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Port</em>' containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Size Port</em>' containment reference.
	 * @see #setSizePort(Size)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPort_SizePort()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Size getSizePort();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Port#getSizePort <em>Size Port</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Size Port</em>' containment reference.
	 * @see #getSizePort()
	 * @generated
	 */
	void setSizePort(Size value);

	/**
	 * Returns the value of the '<em><b>Type Port</b></em>' attribute. The default value is <code>""</code>. The
	 * literals are from the enumeration {@link fr.inria.aoste.syndex.DataTypeK}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Port</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type Port</em>' attribute.
	 * @see fr.inria.aoste.syndex.DataTypeK
	 * @see #setTypePort(DataTypeK)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPort_TypePort()
	 * @model default="" required="true"
	 * @generated
	 */
	DataTypeK getTypePort();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Port#getTypePort <em>Type Port</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Type Port</em>' attribute.
	 * @see fr.inria.aoste.syndex.DataTypeK
	 * @see #getTypePort()
	 * @generated
	 */
	void setTypePort(DataTypeK value);

	/**
	 * Returns the value of the '<em><b>Parent Condition</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link fr.inria.aoste.syndex.Condition#getPortCondition <em>Port Condition</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Condition</em>' container reference isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Condition</em>' container reference.
	 * @see #setParentCondition(Condition)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPort_ParentCondition()
	 * @see fr.inria.aoste.syndex.Condition#getPortCondition
	 * @model opposite="portCondition"
	 * @generated NOT
	 */
	Condition getParentConditionReference();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Port#getParentCondition <em>Parent Condition</em>}' container
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Condition</em>' container reference.
	 * @see #getParentCondition()
	 * @generated NOT
	 */
	void setParentConditionReference(Condition value);

	/**
	 * Returns the value of the '<em><b>Parent Compound Structure</b></em>' container reference. It is bidirectional and
	 * its opposite is '{@link fr.inria.aoste.syndex.CompoundStructure#getPortCompoundStructure
	 * <em>Port Compound Structure</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Compound Structure</em>' container reference isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Compound Structure</em>' container reference.
	 * @see #setParentCompoundStructure(CompoundStructure)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPort_ParentCompoundStructure()
	 * @see fr.inria.aoste.syndex.CompoundStructure#getPortCompoundStructure
	 * @model opposite="portCompoundStructure"
	 * @generated NOT
	 */
	CompoundStructure getParentCompoundStructure();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Port#getParentConditionedStructure
	 * <em>Parent Conditioned Structure</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Conditioned Structure</em>' container reference.
	 * @see #getParentConditionedStructure()
	 * @generated NOT
	 */
	void setParentCompoundStructure(CompoundStructure value);

} // Port
