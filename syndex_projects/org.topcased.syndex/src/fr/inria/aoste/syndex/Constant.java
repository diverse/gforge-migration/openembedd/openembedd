/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Constant.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Constant</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Constant#getOneOutputConstant <em>One Output Constant</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Constant#getConstantValue <em>Constant Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Constant#getEnumValues <em>Enum Values</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getConstant()
 * @model
 * @generated
 */
public interface Constant extends AtomicApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>One Output Constant</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>One Output Constant</em>' containment reference isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>One Output Constant</em>' containment reference.
	 * @see #setOneOutputConstant(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConstant_OneOutputConstant()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Output getOneOutputConstant();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Constant#getOneOutputConstant <em>One Output Constant</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>One Output Constant</em>' containment reference.
	 * @see #getOneOutputConstant()
	 * @generated
	 */
	void setOneOutputConstant(Output value);

	/**
	 * Returns the value of the '<em><b>Constant Value</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Value</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Constant Value</em>' attribute.
	 * @see #setConstantValue(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConstant_ConstantValue()
	 * @model required="true"
	 * @generated
	 */
	int getConstantValue();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Constant#getConstantValue <em>Constant Value</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Constant Value</em>' attribute.
	 * @see #getConstantValue()
	 * @generated
	 */
	void setConstantValue(int value);

	/**
	 * Returns the value of the '<em><b>Enum Values</b></em>' attribute list. The list contents are of type
	 * {@link java.lang.Integer}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Values</em>' attribute list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Enum Values</em>' attribute list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConstant_EnumValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getEnumValues();

} // Constant
