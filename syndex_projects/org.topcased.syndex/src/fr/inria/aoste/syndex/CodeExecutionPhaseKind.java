/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: CodeExecutionPhaseKind.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>Code Execution Phase Kind</b></em>', and utility methods for working with them. <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getCodeExecutionPhaseKind()
 * @model
 * @generated
 */
public enum CodeExecutionPhaseKind implements Enumerator
{
	/**
	 * The '<em><b>Loopseq</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #LOOPSEQ_VALUE
	 * @generated
	 * @ordered
	 */
	LOOPSEQ(1, "loopseq", "loopseq"),

	/**
	 * The '<em><b>Initseq</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #INITSEQ_VALUE
	 * @generated
	 * @ordered
	 */
	INITSEQ(2, "initseq", "initseq"),

	/**
	 * The '<em><b>Endseq</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ENDSEQ_VALUE
	 * @generated
	 * @ordered
	 */
	ENDSEQ(3, "endseq", "endseq");

	/**
	 * The '<em><b>Loopseq</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Loopseq</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #LOOPSEQ
	 * @model name="loopseq"
	 * @generated
	 * @ordered
	 */
	public static final int								LOOPSEQ_VALUE	= 1;

	/**
	 * The '<em><b>Initseq</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Initseq</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INITSEQ
	 * @model name="initseq"
	 * @generated
	 * @ordered
	 */
	public static final int								INITSEQ_VALUE	= 2;

	/**
	 * The '<em><b>Endseq</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Endseq</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ENDSEQ
	 * @model name="endseq"
	 * @generated
	 * @ordered
	 */
	public static final int								ENDSEQ_VALUE	= 3;

	/**
	 * An array of all the '<em><b>Code Execution Phase Kind</b></em>' enumerators. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	private static final CodeExecutionPhaseKind[]		VALUES_ARRAY	= new CodeExecutionPhaseKind[] { LOOPSEQ,
			INITSEQ, ENDSEQ,											};

	/**
	 * A public read-only list of all the '<em><b>Code Execution Phase Kind</b></em>' enumerators. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<CodeExecutionPhaseKind>	VALUES			= Collections.unmodifiableList(Arrays
																				.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Code Execution Phase Kind</b></em>' literal with the specified literal value. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static CodeExecutionPhaseKind get(String literal)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			CodeExecutionPhaseKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Code Execution Phase Kind</b></em>' literal with the specified name. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static CodeExecutionPhaseKind getByName(String name)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			CodeExecutionPhaseKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Code Execution Phase Kind</b></em>' literal with the specified integer value. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static CodeExecutionPhaseKind get(int value)
	{
		switch (value)
		{
		case LOOPSEQ_VALUE:
			return LOOPSEQ;
		case INITSEQ_VALUE:
			return INITSEQ;
		case ENDSEQ_VALUE:
			return ENDSEQ;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int		value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private CodeExecutionPhaseKind(int value, String name, String literal)
	{
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue()
	{
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral()
	{
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		return literal;
	}

} // CodeExecutionPhaseKind
