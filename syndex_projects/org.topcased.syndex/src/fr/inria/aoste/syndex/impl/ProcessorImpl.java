/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ProcessorImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Processor</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ProcessorImpl#getParentProcessor <em>Parent Processor</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ProcessorImpl#getDurationSW <em>Duration SW</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ProcessorImpl extends HardwareComponentImpl implements Processor
{
	/**
	 * The cached value of the '{@link #getParentProcessor() <em>Parent Processor</em>}' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getParentProcessor()
	 * @generated
	 * @ordered
	 */
	protected Processor	parentProcessor;

	/**
	 * The cached value of the '{@link #getDurationSW() <em>Duration SW</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDurationSW()
	 * @generated
	 * @ordered
	 */
	protected Duration	durationSW;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ProcessorImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Processor getParentProcessor()
	{
		if (parentProcessor != null && parentProcessor.eIsProxy())
		{
			InternalEObject oldParentProcessor = (InternalEObject) parentProcessor;
			parentProcessor = (Processor) eResolveProxy(oldParentProcessor);
			if (parentProcessor != oldParentProcessor)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.PROCESSOR__PARENT_PROCESSOR, oldParentProcessor, parentProcessor));
			}
		}
		return parentProcessor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Processor basicGetParentProcessor()
	{
		return parentProcessor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentProcessor(Processor newParentProcessor)
	{
		Processor oldParentProcessor = parentProcessor;
		parentProcessor = newParentProcessor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PROCESSOR__PARENT_PROCESSOR,
				oldParentProcessor, parentProcessor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration getDurationSW()
	{
		if (durationSW != null && durationSW.eIsProxy())
		{
			InternalEObject oldDurationSW = (InternalEObject) durationSW;
			durationSW = (Duration) eResolveProxy(oldDurationSW);
			if (durationSW != oldDurationSW)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.PROCESSOR__DURATION_SW,
						oldDurationSW, durationSW));
			}
		}
		return durationSW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration basicGetDurationSW()
	{
		return durationSW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDurationSW(Duration newDurationSW)
	{
		Duration oldDurationSW = durationSW;
		durationSW = newDurationSW;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PROCESSOR__DURATION_SW, oldDurationSW,
				durationSW));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.PROCESSOR__PARENT_PROCESSOR:
			if (resolve)
				return getParentProcessor();
			return basicGetParentProcessor();
		case SyndexPackage.PROCESSOR__DURATION_SW:
			if (resolve)
				return getDurationSW();
			return basicGetDurationSW();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.PROCESSOR__PARENT_PROCESSOR:
			setParentProcessor((Processor) newValue);
			return;
		case SyndexPackage.PROCESSOR__DURATION_SW:
			setDurationSW((Duration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PROCESSOR__PARENT_PROCESSOR:
			setParentProcessor((Processor) null);
			return;
		case SyndexPackage.PROCESSOR__DURATION_SW:
			setDurationSW((Duration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PROCESSOR__PARENT_PROCESSOR:
			return parentProcessor != null;
		case SyndexPackage.PROCESSOR__DURATION_SW:
			return durationSW != null;
		}
		return super.eIsSet(featureID);
	}

} // ProcessorImpl
