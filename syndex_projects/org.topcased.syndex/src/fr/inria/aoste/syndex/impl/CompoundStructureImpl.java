/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: CompoundStructureImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Compound Structure</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.CompoundStructureImpl#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CompoundStructureImpl extends InternalStructureImpl implements CompoundStructure
{
	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance>	componentInstance;

	/**
	 * The cached value of the '{@link #getPortCoumpoundStructure() <em>Port Coumpound Structure</em>}' containment
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPortCoumpoundStructure()
	 * @generated NOT
	 * @ordered
	 */
	@SuppressWarnings("unchecked")
	protected EList						portCompoundStructure	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CompoundStructureImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.COMPOUND_STRUCTURE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ComponentInstance> getComponentInstance()
	{
		if (componentInstance == null)
		{
			componentInstance = new EObjectContainmentEList<ComponentInstance>(ComponentInstance.class, this,
				SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE);
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public EList getPortCompoundStructure()
	{
		if (portCompoundStructure == null)
		{
			portCompoundStructure = new EObjectContainmentWithInverseEList(Port.class, this,
				SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE,
				SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE);
		}
		return portCompoundStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			return ((InternalEList) getPortCompoundStructure()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE:
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			return ((InternalEList<?>) getComponentInstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE:
			return getComponentInstance();
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			return getPortCompoundStructure();

		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE:
			getComponentInstance().clear();
			getComponentInstance().addAll((Collection<? extends ComponentInstance>) newValue);
			return;
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			getPortCompoundStructure().clear();
			getPortCompoundStructure().addAll((Collection) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE:
			getComponentInstance().clear();
			return;
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			getPortCompoundStructure().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPOUND_STRUCTURE__COMPONENT_INSTANCE:
			return componentInstance != null && !componentInstance.isEmpty();
		case SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE:
			return portCompoundStructure != null && !portCompoundStructure.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // CompoundStructureImpl
