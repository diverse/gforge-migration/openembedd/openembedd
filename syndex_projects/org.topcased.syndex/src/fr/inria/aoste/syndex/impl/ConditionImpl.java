/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConditionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Condition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionImpl#getOwnerInternalStructure <em>Owner Internal Structure</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionImpl#getInternalConnector <em>Internal Connector</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionImpl#getValueCondition <em>Value Condition</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionImpl#getConditionPort <em>Condition Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionImpl#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ConditionImpl extends NamedElementImpl implements Condition
{
	/**
	 * The cached value of the '{@link #getOwnerInternalStructure() <em>Owner Internal Structure</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOwnerInternalStructure()
	 * @generated
	 * @ordered
	 */
	protected ApplicationComponent			ownerInternalStructure;

	/**
	 * The cached value of the '{@link #getInternalConnector() <em>Internal Connector</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInternalConnector()
	 * @generated
	 * @ordered
	 */
	protected EList<ApplicationConnection>	internalConnector;

	/**
	 * The default value of the '{@link #getValueCondition() <em>Value Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getValueCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String			VALUE_CONDITION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getValueCondition() <em>Value Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getValueCondition()
	 * @generated
	 * @ordered
	 */
	protected String						valueCondition				= VALUE_CONDITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConditionPort() <em>Condition Port</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getConditionPort()
	 * @generated
	 * @ordered
	 */
	protected InCondPort					conditionPort;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance>		componentInstance;

	/**
	 * The cached value of the '{@link #getPortCondition() <em>Port Condition</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPortConditions()
	 * @generated NOT
	 * @ordered
	 */
	@SuppressWarnings("unchecked")
	protected EList							portCondition				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConditionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.CONDITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent getOwnerInternalStructure()
	{
		if (ownerInternalStructure != null && ownerInternalStructure.eIsProxy())
		{
			InternalEObject oldOwnerInternalStructure = (InternalEObject) ownerInternalStructure;
			ownerInternalStructure = (ApplicationComponent) eResolveProxy(oldOwnerInternalStructure);
			if (ownerInternalStructure != oldOwnerInternalStructure)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE, oldOwnerInternalStructure,
						ownerInternalStructure));
			}
		}
		return ownerInternalStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent basicGetOwnerInternalStructure()
	{
		return ownerInternalStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerInternalStructure(ApplicationComponent newOwnerInternalStructure)
	{
		ApplicationComponent oldOwnerInternalStructure = ownerInternalStructure;
		ownerInternalStructure = newOwnerInternalStructure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE,
				oldOwnerInternalStructure, ownerInternalStructure));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ApplicationConnection> getInternalConnector()
	{
		if (internalConnector == null)
		{
			internalConnector = new EObjectContainmentEList<ApplicationConnection>(ApplicationConnection.class, this,
				SyndexPackage.CONDITION__INTERNAL_CONNECTOR);
		}
		return internalConnector;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getValueCondition()
	{
		return valueCondition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setValueCondition(String newValueCondition)
	{
		String oldValueCondition = valueCondition;
		valueCondition = newValueCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONDITION__VALUE_CONDITION,
				oldValueCondition, valueCondition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort getConditionPort()
	{
		if (conditionPort != null && conditionPort.eIsProxy())
		{
			InternalEObject oldConditionPort = (InternalEObject) conditionPort;
			conditionPort = (InCondPort) eResolveProxy(oldConditionPort);
			if (conditionPort != oldConditionPort)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.CONDITION__CONDITION_PORT,
						oldConditionPort, conditionPort));
			}
		}
		return conditionPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort basicGetConditionPort()
	{
		return conditionPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setConditionPort(InCondPort newConditionPort)
	{
		InCondPort oldConditionPort = conditionPort;
		conditionPort = newConditionPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONDITION__CONDITION_PORT,
				oldConditionPort, conditionPort));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ComponentInstance> getComponentInstance()
	{
		if (componentInstance == null)
		{
			componentInstance = new EObjectContainmentEList<ComponentInstance>(ComponentInstance.class, this,
				SyndexPackage.CONDITION__COMPONENT_INSTANCE);
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public EList getConditionPortReference()
	{
		if (portCondition == null)
		{
			portCondition = new EObjectContainmentWithInverseEList(Port.class, this,
				SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE, SyndexPackage.PORT__PARENT_CONDITION_REFERENCE);
		}
		return portCondition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
			return ((InternalEList<?>) getInternalConnector()).basicRemove(otherEnd, msgs);
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
			return ((InternalEList<?>) getComponentInstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			return ((InternalEList) getConditionPortReference()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE:
			if (resolve)
				return getOwnerInternalStructure();
			return basicGetOwnerInternalStructure();
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
			return getInternalConnector();
		case SyndexPackage.CONDITION__VALUE_CONDITION:
			return getValueCondition();
		case SyndexPackage.CONDITION__CONDITION_PORT:
			if (resolve)
				return getConditionPort();
			return basicGetConditionPort();
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
			return getComponentInstance();
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			return getConditionPortReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE:
			setOwnerInternalStructure((ApplicationComponent) newValue);
			return;
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
			getInternalConnector().clear();
			getInternalConnector().addAll((Collection<? extends ApplicationConnection>) newValue);
			return;
		case SyndexPackage.CONDITION__VALUE_CONDITION:
			setValueCondition((String) newValue);
			return;
		case SyndexPackage.CONDITION__CONDITION_PORT:
			setConditionPort((InCondPort) newValue);
			return;
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
			getComponentInstance().clear();
			getComponentInstance().addAll((Collection<? extends ComponentInstance>) newValue);
			return;
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			getConditionPortReference().clear();
			getConditionPortReference().addAll((Collection) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE:
			setOwnerInternalStructure((ApplicationComponent) null);
			return;
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
			getInternalConnector().clear();
			return;
		case SyndexPackage.CONDITION__VALUE_CONDITION:
			setValueCondition(VALUE_CONDITION_EDEFAULT);
			return;
		case SyndexPackage.CONDITION__CONDITION_PORT:
			setConditionPort((InCondPort) null);
			return;
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
			getComponentInstance().clear();
			return;
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			getConditionPortReference().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE:
			return ownerInternalStructure != null;
		case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
			return internalConnector != null && !internalConnector.isEmpty();
		case SyndexPackage.CONDITION__VALUE_CONDITION:
			return VALUE_CONDITION_EDEFAULT == null ? valueCondition != null : !VALUE_CONDITION_EDEFAULT
					.equals(valueCondition);
		case SyndexPackage.CONDITION__CONDITION_PORT:
			return conditionPort != null;
		case SyndexPackage.CONDITION__COMPONENT_INSTANCE:
			return componentInstance != null && !componentInstance.isEmpty();
		case SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE:
			return portCondition != null && !portCondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
	{
		if (baseClass == InternalStructure.class)
		{
			switch (derivedFeatureID)
			{
			case SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE:
				return SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE;
			case SyndexPackage.CONDITION__INTERNAL_CONNECTOR:
				return SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
	{
		if (baseClass == InternalStructure.class)
		{
			switch (baseFeatureID)
			{
			case SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE:
				return SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE;
			case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
				return SyndexPackage.CONDITION__INTERNAL_CONNECTOR;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (valueCondition: ");
		result.append(valueCondition);
		result.append(')');
		return result.toString();
	}

} // ConditionImpl
