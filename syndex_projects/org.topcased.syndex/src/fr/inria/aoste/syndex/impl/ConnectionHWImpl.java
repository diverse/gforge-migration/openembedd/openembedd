/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConnectionHWImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Connection HW</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ConnectionHWImpl#getSourceInOut <em>Source In Out</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConnectionHWImpl#getTargetInOut <em>Target In Out</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ConnectionHWImpl extends HWConnectionImpl implements ConnectionHW
{
	/**
	 * The cached value of the '{@link #getSourceInOut() <em>Source In Out</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSourceInOut()
	 * @generated
	 * @ordered
	 */
	protected INOUTInstance	sourceInOut;

	/**
	 * The cached value of the '{@link #getTargetInOut() <em>Target In Out</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getTargetInOut()
	 * @generated
	 * @ordered
	 */
	protected INOUTInstance	targetInOut;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConnectionHWImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.CONNECTION_HW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUTInstance getSourceInOut()
	{
		if (sourceInOut != null && sourceInOut.eIsProxy())
		{
			InternalEObject oldSourceInOut = (InternalEObject) sourceInOut;
			sourceInOut = (INOUTInstance) eResolveProxy(oldSourceInOut);
			if (sourceInOut != oldSourceInOut)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT, oldSourceInOut, sourceInOut));
			}
		}
		return sourceInOut;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUTInstance basicGetSourceInOut()
	{
		return sourceInOut;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceInOut(INOUTInstance newSourceInOut)
	{
		INOUTInstance oldSourceInOut = sourceInOut;
		sourceInOut = newSourceInOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT,
				oldSourceInOut, sourceInOut));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUTInstance getTargetInOut()
	{
		if (targetInOut != null && targetInOut.eIsProxy())
		{
			InternalEObject oldTargetInOut = (InternalEObject) targetInOut;
			targetInOut = (INOUTInstance) eResolveProxy(oldTargetInOut);
			if (targetInOut != oldTargetInOut)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.CONNECTION_HW__TARGET_IN_OUT, oldTargetInOut, targetInOut));
			}
		}
		return targetInOut;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUTInstance basicGetTargetInOut()
	{
		return targetInOut;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetInOut(INOUTInstance newTargetInOut)
	{
		INOUTInstance oldTargetInOut = targetInOut;
		targetInOut = newTargetInOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONNECTION_HW__TARGET_IN_OUT,
				oldTargetInOut, targetInOut));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT:
			if (resolve)
				return getSourceInOut();
			return basicGetSourceInOut();
		case SyndexPackage.CONNECTION_HW__TARGET_IN_OUT:
			if (resolve)
				return getTargetInOut();
			return basicGetTargetInOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT:
			setSourceInOut((INOUTInstance) newValue);
			return;
		case SyndexPackage.CONNECTION_HW__TARGET_IN_OUT:
			setTargetInOut((INOUTInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT:
			setSourceInOut((INOUTInstance) null);
			return;
		case SyndexPackage.CONNECTION_HW__TARGET_IN_OUT:
			setTargetInOut((INOUTInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONNECTION_HW__SOURCE_IN_OUT:
			return sourceInOut != null;
		case SyndexPackage.CONNECTION_HW__TARGET_IN_OUT:
			return targetInOut != null;
		}
		return super.eIsSet(featureID);
	}

} // ConnectionHWImpl
