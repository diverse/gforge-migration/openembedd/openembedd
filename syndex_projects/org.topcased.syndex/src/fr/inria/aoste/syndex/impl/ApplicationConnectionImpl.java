/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationConnectionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Application Connection</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationConnectionImpl#getParentCoumpoundApplicationConnection <em>Parent
 * Coumpound Application Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationConnectionImpl#getParentConditionApplicationConnection <em>Parent
 * Condition Application Connection</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ApplicationConnectionImpl extends ConnectionImpl implements ApplicationConnection
{
	/**
	 * The cached value of the '{@link #getParentCoumpoundApplicationConnection()
	 * <em>Parent Coumpound Application Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParentCoumpoundApplicationConnection()
	 * @generated
	 * @ordered
	 */
	protected CompoundStructure		parentCoumpoundApplicationConnection;

	/**
	 * The cached value of the '{@link #getParentConditionApplicationConnection()
	 * <em>Parent Condition Application Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParentConditionApplicationConnection()
	 * @generated
	 * @ordered
	 */
	protected ConditionedStructure	parentConditionApplicationConnection;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ApplicationConnectionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.APPLICATION_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CompoundStructure getParentCoumpoundApplicationConnection()
	{
		if (parentCoumpoundApplicationConnection != null && parentCoumpoundApplicationConnection.eIsProxy())
		{
			InternalEObject oldParentCoumpoundApplicationConnection = (InternalEObject) parentCoumpoundApplicationConnection;
			parentCoumpoundApplicationConnection = (CompoundStructure) eResolveProxy(oldParentCoumpoundApplicationConnection);
			if (parentCoumpoundApplicationConnection != oldParentCoumpoundApplicationConnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION,
						oldParentCoumpoundApplicationConnection, parentCoumpoundApplicationConnection));
			}
		}
		return parentCoumpoundApplicationConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CompoundStructure basicGetParentCoumpoundApplicationConnection()
	{
		return parentCoumpoundApplicationConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentCoumpoundApplicationConnection(CompoundStructure newParentCoumpoundApplicationConnection)
	{
		CompoundStructure oldParentCoumpoundApplicationConnection = parentCoumpoundApplicationConnection;
		parentCoumpoundApplicationConnection = newParentCoumpoundApplicationConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION,
				oldParentCoumpoundApplicationConnection, parentCoumpoundApplicationConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConditionedStructure getParentConditionApplicationConnection()
	{
		if (parentConditionApplicationConnection != null && parentConditionApplicationConnection.eIsProxy())
		{
			InternalEObject oldParentConditionApplicationConnection = (InternalEObject) parentConditionApplicationConnection;
			parentConditionApplicationConnection = (ConditionedStructure) eResolveProxy(oldParentConditionApplicationConnection);
			if (parentConditionApplicationConnection != oldParentConditionApplicationConnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION,
						oldParentConditionApplicationConnection, parentConditionApplicationConnection));
			}
		}
		return parentConditionApplicationConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConditionedStructure basicGetParentConditionApplicationConnection()
	{
		return parentConditionApplicationConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentConditionApplicationConnection(ConditionedStructure newParentConditionApplicationConnection)
	{
		ConditionedStructure oldParentConditionApplicationConnection = parentConditionApplicationConnection;
		parentConditionApplicationConnection = newParentConditionApplicationConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION,
				oldParentConditionApplicationConnection, parentConditionApplicationConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION:
			if (resolve)
				return getParentCoumpoundApplicationConnection();
			return basicGetParentCoumpoundApplicationConnection();
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION:
			if (resolve)
				return getParentConditionApplicationConnection();
			return basicGetParentConditionApplicationConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION:
			setParentCoumpoundApplicationConnection((CompoundStructure) newValue);
			return;
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION:
			setParentConditionApplicationConnection((ConditionedStructure) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION:
			setParentCoumpoundApplicationConnection((CompoundStructure) null);
			return;
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION:
			setParentConditionApplicationConnection((ConditionedStructure) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION:
			return parentCoumpoundApplicationConnection != null;
		case SyndexPackage.APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION:
			return parentConditionApplicationConnection != null;
		}
		return super.eIsSet(featureID);
	}

} // ApplicationConnectionImpl
