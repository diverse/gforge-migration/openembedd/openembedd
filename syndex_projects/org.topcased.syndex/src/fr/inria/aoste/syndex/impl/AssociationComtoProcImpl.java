/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AssociationComtoProcImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.AssociationComtoProc;
import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Association Comto Proc</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationComtoProcImpl#getSourceAssociationComToProc <em>Source Association
 * Com To Proc</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationComtoProcImpl#getDuration <em>Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationComtoProcImpl#getTargetHWProcess <em>Target HW Process</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class AssociationComtoProcImpl extends CaracteristicsImpl implements AssociationComtoProc
{
	/**
	 * The cached value of the '{@link #getSourceAssociationComToProc() <em>Source Association Com To Proc</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceAssociationComToProc()
	 * @generated
	 * @ordered
	 */
	protected Media		sourceAssociationComToProc;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Duration	duration;

	/**
	 * The cached value of the '{@link #getTargetHWProcess() <em>Target HW Process</em>}' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getTargetHWProcess()
	 * @generated
	 * @ordered
	 */
	protected Processor	targetHWProcess;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AssociationComtoProcImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.ASSOCIATION_COMTO_PROC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Media getSourceAssociationComToProc()
	{
		if (sourceAssociationComToProc != null && sourceAssociationComToProc.eIsProxy())
		{
			InternalEObject oldSourceAssociationComToProc = (InternalEObject) sourceAssociationComToProc;
			sourceAssociationComToProc = (Media) eResolveProxy(oldSourceAssociationComToProc);
			if (sourceAssociationComToProc != oldSourceAssociationComToProc)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC,
						oldSourceAssociationComToProc, sourceAssociationComToProc));
			}
		}
		return sourceAssociationComToProc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Media basicGetSourceAssociationComToProc()
	{
		return sourceAssociationComToProc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceAssociationComToProc(Media newSourceAssociationComToProc)
	{
		Media oldSourceAssociationComToProc = sourceAssociationComToProc;
		sourceAssociationComToProc = newSourceAssociationComToProc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC, oldSourceAssociationComToProc,
				sourceAssociationComToProc));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration getDuration()
	{
		if (duration != null && duration.eIsProxy())
		{
			InternalEObject oldDuration = (InternalEObject) duration;
			duration = (Duration) eResolveProxy(oldDuration);
			if (duration != oldDuration)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION, oldDuration, duration));
			}
		}
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration basicGetDuration()
	{
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDuration(Duration newDuration)
	{
		Duration oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION,
				oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Processor getTargetHWProcess()
	{
		if (targetHWProcess != null && targetHWProcess.eIsProxy())
		{
			InternalEObject oldTargetHWProcess = (InternalEObject) targetHWProcess;
			targetHWProcess = (Processor) eResolveProxy(oldTargetHWProcess);
			if (targetHWProcess != oldTargetHWProcess)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS, oldTargetHWProcess, targetHWProcess));
			}
		}
		return targetHWProcess;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Processor basicGetTargetHWProcess()
	{
		return targetHWProcess;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetHWProcess(Processor newTargetHWProcess)
	{
		Processor oldTargetHWProcess = targetHWProcess;
		targetHWProcess = newTargetHWProcess;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS, oldTargetHWProcess, targetHWProcess));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC:
			if (resolve)
				return getSourceAssociationComToProc();
			return basicGetSourceAssociationComToProc();
		case SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION:
			if (resolve)
				return getDuration();
			return basicGetDuration();
		case SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS:
			if (resolve)
				return getTargetHWProcess();
			return basicGetTargetHWProcess();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC:
			setSourceAssociationComToProc((Media) newValue);
			return;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION:
			setDuration((Duration) newValue);
			return;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS:
			setTargetHWProcess((Processor) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC:
			setSourceAssociationComToProc((Media) null);
			return;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION:
			setDuration((Duration) null);
			return;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS:
			setTargetHWProcess((Processor) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC:
			return sourceAssociationComToProc != null;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__DURATION:
			return duration != null;
		case SyndexPackage.ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS:
			return targetHWProcess != null;
		}
		return super.eIsSet(featureID);
	}

} // AssociationComtoProcImpl
