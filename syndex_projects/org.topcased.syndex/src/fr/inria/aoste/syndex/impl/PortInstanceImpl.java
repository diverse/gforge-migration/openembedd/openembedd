/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: PortInstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.PortInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Port Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.PortInstanceImpl#getParentComponentInstance <em>Parent Component Instance</em>}
 * </li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class PortInstanceImpl extends InstanceImpl implements PortInstance
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PortInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.PORT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance getParentComponentInstance()
	{
		if (eContainerFeatureID != SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE)
			return null;
		return (ComponentInstance) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetParentComponentInstance(ComponentInstance newParentComponentInstance,
			NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newParentComponentInstance,
			SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentComponentInstance(ComponentInstance newParentComponentInstance)
	{
		if (newParentComponentInstance != eInternalContainer()
				|| (eContainerFeatureID != SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE && newParentComponentInstance != null))
		{
			if (EcoreUtil.isAncestor(this, newParentComponentInstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentComponentInstance != null)
				msgs = ((InternalEObject) newParentComponentInstance).eInverseAdd(this,
					SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES, ComponentInstance.class, msgs);
			msgs = basicSetParentComponentInstance(newParentComponentInstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE, newParentComponentInstance,
				newParentComponentInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentComponentInstance((ComponentInstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			return basicSetParentComponentInstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			return eInternalContainer().eInverseRemove(this, SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES,
				ComponentInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			return getParentComponentInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			setParentComponentInstance((ComponentInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			setParentComponentInstance((ComponentInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE:
			return getParentComponentInstance() != null;
		}
		return super.eIsSet(featureID);
	}

} // PortInstanceImpl
