/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: RepetitionConnectionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.RepetitionConnection;
import fr.inria.aoste.syndex.RepetitionConnectionKind;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Repetition Connection</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.RepetitionConnectionImpl#getRepetitionKind <em>Repetition Kind</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.RepetitionConnectionImpl#getSourceRepetitionConnection <em>Source Repetition
 * Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.RepetitionConnectionImpl#getTargetRepetitionConnection <em>Target Repetition
 * Connection</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class RepetitionConnectionImpl extends ApplicationConnectionImpl implements RepetitionConnection
{
	/**
	 * The default value of the '{@link #getRepetitionKind() <em>Repetition Kind</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getRepetitionKind()
	 * @generated
	 * @ordered
	 */
	protected static final RepetitionConnectionKind	REPETITION_KIND_EDEFAULT	= RepetitionConnectionKind.FORK;

	/**
	 * The cached value of the '{@link #getRepetitionKind() <em>Repetition Kind</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getRepetitionKind()
	 * @generated
	 * @ordered
	 */
	protected RepetitionConnectionKind				repetitionKind				= REPETITION_KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceRepetitionConnection() <em>Source Repetition Connection</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceRepetitionConnection()
	 * @generated
	 * @ordered
	 */
	protected Size									sourceRepetitionConnection;

	/**
	 * The cached value of the '{@link #getTargetRepetitionConnection() <em>Target Repetition Connection</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTargetRepetitionConnection()
	 * @generated
	 * @ordered
	 */
	protected Size									targetRepetitionConnection;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected RepetitionConnectionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.REPETITION_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public RepetitionConnectionKind getRepetitionKind()
	{
		return repetitionKind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRepetitionKind(RepetitionConnectionKind newRepetitionKind)
	{
		RepetitionConnectionKind oldRepetitionKind = repetitionKind;
		repetitionKind = newRepetitionKind == null ? REPETITION_KIND_EDEFAULT : newRepetitionKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.REPETITION_CONNECTION__REPETITION_KIND,
				oldRepetitionKind, repetitionKind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size getSourceRepetitionConnection()
	{
		if (sourceRepetitionConnection != null && sourceRepetitionConnection.eIsProxy())
		{
			InternalEObject oldSourceRepetitionConnection = (InternalEObject) sourceRepetitionConnection;
			sourceRepetitionConnection = (Size) eResolveProxy(oldSourceRepetitionConnection);
			if (sourceRepetitionConnection != oldSourceRepetitionConnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION,
						oldSourceRepetitionConnection, sourceRepetitionConnection));
			}
		}
		return sourceRepetitionConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size basicGetSourceRepetitionConnection()
	{
		return sourceRepetitionConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceRepetitionConnection(Size newSourceRepetitionConnection)
	{
		Size oldSourceRepetitionConnection = sourceRepetitionConnection;
		sourceRepetitionConnection = newSourceRepetitionConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION, oldSourceRepetitionConnection,
				sourceRepetitionConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size getTargetRepetitionConnection()
	{
		if (targetRepetitionConnection != null && targetRepetitionConnection.eIsProxy())
		{
			InternalEObject oldTargetRepetitionConnection = (InternalEObject) targetRepetitionConnection;
			targetRepetitionConnection = (Size) eResolveProxy(oldTargetRepetitionConnection);
			if (targetRepetitionConnection != oldTargetRepetitionConnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION,
						oldTargetRepetitionConnection, targetRepetitionConnection));
			}
		}
		return targetRepetitionConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size basicGetTargetRepetitionConnection()
	{
		return targetRepetitionConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetRepetitionConnection(Size newTargetRepetitionConnection)
	{
		Size oldTargetRepetitionConnection = targetRepetitionConnection;
		targetRepetitionConnection = newTargetRepetitionConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION, oldTargetRepetitionConnection,
				targetRepetitionConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.REPETITION_CONNECTION__REPETITION_KIND:
			return getRepetitionKind();
		case SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION:
			if (resolve)
				return getSourceRepetitionConnection();
			return basicGetSourceRepetitionConnection();
		case SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION:
			if (resolve)
				return getTargetRepetitionConnection();
			return basicGetTargetRepetitionConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.REPETITION_CONNECTION__REPETITION_KIND:
			setRepetitionKind((RepetitionConnectionKind) newValue);
			return;
		case SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION:
			setSourceRepetitionConnection((Size) newValue);
			return;
		case SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION:
			setTargetRepetitionConnection((Size) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.REPETITION_CONNECTION__REPETITION_KIND:
			setRepetitionKind(REPETITION_KIND_EDEFAULT);
			return;
		case SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION:
			setSourceRepetitionConnection((Size) null);
			return;
		case SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION:
			setTargetRepetitionConnection((Size) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.REPETITION_CONNECTION__REPETITION_KIND:
			return repetitionKind != REPETITION_KIND_EDEFAULT;
		case SyndexPackage.REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION:
			return sourceRepetitionConnection != null;
		case SyndexPackage.REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION:
			return targetRepetitionConnection != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (repetitionKind: ");
		result.append(repetitionKind);
		result.append(')');
		return result.toString();
	}

} // RepetitionConnectionImpl
