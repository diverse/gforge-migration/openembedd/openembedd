/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AssociationSWtoHWImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.AssociationSWtoHW;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Association SWto HW</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl#getDuration <em>Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl#getSourceAppli <em>Source Appli</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.AssociationSWtoHWImpl#getTargetHW <em>Target HW</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class AssociationSWtoHWImpl extends CaracteristicsImpl implements AssociationSWtoHW
{
	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final int	DURATION_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected int				duration			= DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceAppli() <em>Source Appli</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceAppli()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance	sourceAppli;

	/**
	 * The cached value of the '{@link #getTargetHW() <em>Target HW</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetHW()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance	targetHW;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AssociationSWtoHWImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.ASSOCIATION_SWTO_HW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getDuration()
	{
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDuration(int newDuration)
	{
		int oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.ASSOCIATION_SWTO_HW__DURATION,
				oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance getSourceAppli()
	{
		if (sourceAppli != null && sourceAppli.eIsProxy())
		{
			InternalEObject oldSourceAppli = (InternalEObject) sourceAppli;
			sourceAppli = (ComponentInstance) eResolveProxy(oldSourceAppli);
			if (sourceAppli != oldSourceAppli)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI, oldSourceAppli, sourceAppli));
			}
		}
		return sourceAppli;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance basicGetSourceAppli()
	{
		return sourceAppli;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceAppli(ComponentInstance newSourceAppli)
	{
		ComponentInstance oldSourceAppli = sourceAppli;
		sourceAppli = newSourceAppli;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI,
				oldSourceAppli, sourceAppli));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance getTargetHW()
	{
		if (targetHW != null && targetHW.eIsProxy())
		{
			InternalEObject oldTargetHW = (InternalEObject) targetHW;
			targetHW = (ComponentInstance) eResolveProxy(oldTargetHW);
			if (targetHW != oldTargetHW)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW, oldTargetHW, targetHW));
			}
		}
		return targetHW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance basicGetTargetHW()
	{
		return targetHW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetHW(ComponentInstance newTargetHW)
	{
		ComponentInstance oldTargetHW = targetHW;
		targetHW = newTargetHW;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW,
				oldTargetHW, targetHW));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_SWTO_HW__DURATION:
			return new Integer(getDuration());
		case SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI:
			if (resolve)
				return getSourceAppli();
			return basicGetSourceAppli();
		case SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW:
			if (resolve)
				return getTargetHW();
			return basicGetTargetHW();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_SWTO_HW__DURATION:
			setDuration(((Integer) newValue).intValue());
			return;
		case SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI:
			setSourceAppli((ComponentInstance) newValue);
			return;
		case SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW:
			setTargetHW((ComponentInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_SWTO_HW__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI:
			setSourceAppli((ComponentInstance) null);
			return;
		case SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW:
			setTargetHW((ComponentInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ASSOCIATION_SWTO_HW__DURATION:
			return duration != DURATION_EDEFAULT;
		case SyndexPackage.ASSOCIATION_SWTO_HW__SOURCE_APPLI:
			return sourceAppli != null;
		case SyndexPackage.ASSOCIATION_SWTO_HW__TARGET_HW:
			return targetHW != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} // AssociationSWtoHWImpl
