/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: OutInstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Out Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.OutInstanceImpl#getOwnerOutput <em>Owner Output</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class OutInstanceImpl extends PortInstanceImpl implements OutInstance
{
	/**
	 * The cached value of the '{@link #getOwnerOutput() <em>Owner Output</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOwnerOutput()
	 * @generated
	 * @ordered
	 */
	protected Output	ownerOutput;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected OutInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.OUT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output getOwnerOutput()
	{
		if (ownerOutput != null && ownerOutput.eIsProxy())
		{
			InternalEObject oldOwnerOutput = (InternalEObject) ownerOutput;
			ownerOutput = (Output) eResolveProxy(oldOwnerOutput);
			if (ownerOutput != oldOwnerOutput)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT,
						oldOwnerOutput, ownerOutput));
			}
		}
		return ownerOutput;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output basicGetOwnerOutput()
	{
		return ownerOutput;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerOutput(Output newOwnerOutput)
	{
		Output oldOwnerOutput = ownerOutput;
		ownerOutput = newOwnerOutput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT,
				oldOwnerOutput, ownerOutput));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT:
			if (resolve)
				return getOwnerOutput();
			return basicGetOwnerOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT:
			setOwnerOutput((Output) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT:
			setOwnerOutput((Output) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.OUT_INSTANCE__OWNER_OUTPUT:
			return ownerOutput != null;
		}
		return super.eIsSet(featureID);
	}

} // OutInstanceImpl
