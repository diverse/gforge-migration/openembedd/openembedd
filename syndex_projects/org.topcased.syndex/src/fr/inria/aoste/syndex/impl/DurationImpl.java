/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: DurationImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Duration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.DurationImpl#getEnumDuration <em>Enum Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.DurationImpl#getParentDuration <em>Parent Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class DurationImpl extends NamedElementImpl implements Duration
{
	/**
	 * The cached value of the '{@link #getEnumDuration() <em>Enum Duration</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getEnumDuration()
	 * @generated
	 * @ordered
	 */
	protected EList<EnumLitDuration>	enumDuration;

	/**
	 * The cached value of the '{@link #getParentDuration() <em>Parent Duration</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParentDuration()
	 * @generated
	 * @ordered
	 */
	protected EList<HardwareComponent>	parentDuration;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DurationImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.DURATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<EnumLitDuration> getEnumDuration()
	{
		if (enumDuration == null)
		{
			enumDuration = new EObjectContainmentWithInverseEList<EnumLitDuration>(EnumLitDuration.class, this,
				SyndexPackage.DURATION__ENUM_DURATION, SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION);
		}
		return enumDuration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<HardwareComponent> getParentDuration()
	{
		if (parentDuration == null)
		{
			parentDuration = new EObjectResolvingEList<HardwareComponent>(HardwareComponent.class, this,
				SyndexPackage.DURATION__PARENT_DURATION);
		}
		return parentDuration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEnumDuration()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			return ((InternalEList<?>) getEnumDuration()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			return getEnumDuration();
		case SyndexPackage.DURATION__PARENT_DURATION:
			return getParentDuration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			getEnumDuration().clear();
			getEnumDuration().addAll((Collection<? extends EnumLitDuration>) newValue);
			return;
		case SyndexPackage.DURATION__PARENT_DURATION:
			getParentDuration().clear();
			getParentDuration().addAll((Collection<? extends HardwareComponent>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			getEnumDuration().clear();
			return;
		case SyndexPackage.DURATION__PARENT_DURATION:
			getParentDuration().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DURATION__ENUM_DURATION:
			return enumDuration != null && !enumDuration.isEmpty();
		case SyndexPackage.DURATION__PARENT_DURATION:
			return parentDuration != null && !parentDuration.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // DurationImpl
