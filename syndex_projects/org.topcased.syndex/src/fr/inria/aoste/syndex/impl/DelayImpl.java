/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: DelayImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Delay</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.DelayImpl#getOneInputDelay <em>One Input Delay</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.DelayImpl#getOneOutputDelay <em>One Output Delay</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.DelayImpl#getDelayValue <em>Delay Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.DelayImpl#getEnumValue <em>Enum Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class DelayImpl extends AtomicApplicationComponentImpl implements Delay
{
	/**
	 * The cached value of the '{@link #getOneInputDelay() <em>One Input Delay</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOneInputDelay()
	 * @generated
	 * @ordered
	 */
	protected Input				oneInputDelay;

	/**
	 * The cached value of the '{@link #getOneOutputDelay() <em>One Output Delay</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOneOutputDelay()
	 * @generated
	 * @ordered
	 */
	protected Output			oneOutputDelay;

	/**
	 * The default value of the '{@link #getDelayValue() <em>Delay Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDelayValue()
	 * @generated
	 * @ordered
	 */
	protected static final int	DELAY_VALUE_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getDelayValue() <em>Delay Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDelayValue()
	 * @generated
	 * @ordered
	 */
	protected int				delayValue				= DELAY_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEnumValue() <em>Enum Value</em>}' attribute list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getEnumValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer>	enumValue;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DelayImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.DELAY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input getOneInputDelay()
	{
		return oneInputDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetOneInputDelay(Input newOneInputDelay, NotificationChain msgs)
	{
		Input oldOneInputDelay = oneInputDelay;
		oneInputDelay = newOneInputDelay;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.DELAY__ONE_INPUT_DELAY, oldOneInputDelay, newOneInputDelay);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOneInputDelay(Input newOneInputDelay)
	{
		if (newOneInputDelay != oneInputDelay)
		{
			NotificationChain msgs = null;
			if (oneInputDelay != null)
				msgs = ((InternalEObject) oneInputDelay).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.DELAY__ONE_INPUT_DELAY, null, msgs);
			if (newOneInputDelay != null)
				msgs = ((InternalEObject) newOneInputDelay).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.DELAY__ONE_INPUT_DELAY, null, msgs);
			msgs = basicSetOneInputDelay(newOneInputDelay, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.DELAY__ONE_INPUT_DELAY,
				newOneInputDelay, newOneInputDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output getOneOutputDelay()
	{
		return oneOutputDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetOneOutputDelay(Output newOneOutputDelay, NotificationChain msgs)
	{
		Output oldOneOutputDelay = oneOutputDelay;
		oneOutputDelay = newOneOutputDelay;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.DELAY__ONE_OUTPUT_DELAY, oldOneOutputDelay, newOneOutputDelay);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOneOutputDelay(Output newOneOutputDelay)
	{
		if (newOneOutputDelay != oneOutputDelay)
		{
			NotificationChain msgs = null;
			if (oneOutputDelay != null)
				msgs = ((InternalEObject) oneOutputDelay).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.DELAY__ONE_OUTPUT_DELAY, null, msgs);
			if (newOneOutputDelay != null)
				msgs = ((InternalEObject) newOneOutputDelay).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.DELAY__ONE_OUTPUT_DELAY, null, msgs);
			msgs = basicSetOneOutputDelay(newOneOutputDelay, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.DELAY__ONE_OUTPUT_DELAY,
				newOneOutputDelay, newOneOutputDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getDelayValue()
	{
		return delayValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDelayValue(int newDelayValue)
	{
		int oldDelayValue = delayValue;
		delayValue = newDelayValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.DELAY__DELAY_VALUE, oldDelayValue,
				delayValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Integer> getEnumValue()
	{
		if (enumValue == null)
		{
			enumValue = new EDataTypeUniqueEList<Integer>(Integer.class, this, SyndexPackage.DELAY__ENUM_VALUE);
		}
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.DELAY__ONE_INPUT_DELAY:
			return basicSetOneInputDelay(null, msgs);
		case SyndexPackage.DELAY__ONE_OUTPUT_DELAY:
			return basicSetOneOutputDelay(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.DELAY__ONE_INPUT_DELAY:
			return getOneInputDelay();
		case SyndexPackage.DELAY__ONE_OUTPUT_DELAY:
			return getOneOutputDelay();
		case SyndexPackage.DELAY__DELAY_VALUE:
			return new Integer(getDelayValue());
		case SyndexPackage.DELAY__ENUM_VALUE:
			return getEnumValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.DELAY__ONE_INPUT_DELAY:
			setOneInputDelay((Input) newValue);
			return;
		case SyndexPackage.DELAY__ONE_OUTPUT_DELAY:
			setOneOutputDelay((Output) newValue);
			return;
		case SyndexPackage.DELAY__DELAY_VALUE:
			setDelayValue(((Integer) newValue).intValue());
			return;
		case SyndexPackage.DELAY__ENUM_VALUE:
			getEnumValue().clear();
			getEnumValue().addAll((Collection<? extends Integer>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DELAY__ONE_INPUT_DELAY:
			setOneInputDelay((Input) null);
			return;
		case SyndexPackage.DELAY__ONE_OUTPUT_DELAY:
			setOneOutputDelay((Output) null);
			return;
		case SyndexPackage.DELAY__DELAY_VALUE:
			setDelayValue(DELAY_VALUE_EDEFAULT);
			return;
		case SyndexPackage.DELAY__ENUM_VALUE:
			getEnumValue().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DELAY__ONE_INPUT_DELAY:
			return oneInputDelay != null;
		case SyndexPackage.DELAY__ONE_OUTPUT_DELAY:
			return oneOutputDelay != null;
		case SyndexPackage.DELAY__DELAY_VALUE:
			return delayValue != DELAY_VALUE_EDEFAULT;
		case SyndexPackage.DELAY__ENUM_VALUE:
			return enumValue != null && !enumValue.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (delayValue: ");
		result.append(delayValue);
		result.append(", enumValue: ");
		result.append(enumValue);
		result.append(')');
		return result.toString();
	}

} // DelayImpl
