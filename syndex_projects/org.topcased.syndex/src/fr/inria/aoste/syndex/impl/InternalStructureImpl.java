/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalStructureImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Internal Structure</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalStructureImpl#getOwnerInternalStructure <em>Owner Internal Structure
 * </em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalStructureImpl#getInternalConnector <em>Internal Connector</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class InternalStructureImpl extends ElementImpl implements InternalStructure
{
	/**
	 * The cached value of the '{@link #getOwnerInternalStructure() <em>Owner Internal Structure</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOwnerInternalStructure()
	 * @generated
	 * @ordered
	 */
	protected ApplicationComponent			ownerInternalStructure;

	/**
	 * The cached value of the '{@link #getInternalConnector() <em>Internal Connector</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInternalConnector()
	 * @generated
	 * @ordered
	 */
	protected EList<ApplicationConnection>	internalConnector;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InternalStructureImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INTERNAL_STRUCTURE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent getOwnerInternalStructure()
	{
		if (ownerInternalStructure != null && ownerInternalStructure.eIsProxy())
		{
			InternalEObject oldOwnerInternalStructure = (InternalEObject) ownerInternalStructure;
			ownerInternalStructure = (ApplicationComponent) eResolveProxy(oldOwnerInternalStructure);
			if (ownerInternalStructure != oldOwnerInternalStructure)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE, oldOwnerInternalStructure,
						ownerInternalStructure));
			}
		}
		return ownerInternalStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent basicGetOwnerInternalStructure()
	{
		return ownerInternalStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerInternalStructure(ApplicationComponent newOwnerInternalStructure)
	{
		ApplicationComponent oldOwnerInternalStructure = ownerInternalStructure;
		ownerInternalStructure = newOwnerInternalStructure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE, oldOwnerInternalStructure,
				ownerInternalStructure));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ApplicationConnection> getInternalConnector()
	{
		if (internalConnector == null)
		{
			internalConnector = new EObjectContainmentEList<ApplicationConnection>(ApplicationConnection.class, this,
				SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR);
		}
		return internalConnector;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
			return ((InternalEList<?>) getInternalConnector()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE:
			if (resolve)
				return getOwnerInternalStructure();
			return basicGetOwnerInternalStructure();
		case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
			return getInternalConnector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE:
			setOwnerInternalStructure((ApplicationComponent) newValue);
			return;
		case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
			getInternalConnector().clear();
			getInternalConnector().addAll((Collection<? extends ApplicationConnection>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE:
			setOwnerInternalStructure((ApplicationComponent) null);
			return;
		case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
			getInternalConnector().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE:
			return ownerInternalStructure != null;
		case SyndexPackage.INTERNAL_STRUCTURE__INTERNAL_CONNECTOR:
			return internalConnector != null && !internalConnector.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // InternalStructureImpl
