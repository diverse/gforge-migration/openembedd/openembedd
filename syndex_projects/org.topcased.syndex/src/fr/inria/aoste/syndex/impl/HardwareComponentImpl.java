/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: HardwareComponentImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.HWConnection;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Hardware Component</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.HardwareComponentImpl#getHWConnection <em>HW Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.HardwareComponentImpl#getInOutHWComponent <em>In Out HW Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class HardwareComponentImpl extends ComponentImpl implements HardwareComponent
{
	/**
	 * The cached value of the '{@link #getHWConnection() <em>HW Connection</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHWConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<HWConnection>	hwConnection;

	/**
	 * The cached value of the '{@link #getInOutHWComponent() <em>In Out HW Component</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInOutHWComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<INOUT>			inOutHWComponent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected HardwareComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.HARDWARE_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<HWConnection> getHWConnection()
	{
		if (hwConnection == null)
		{
			hwConnection = new EObjectContainmentWithInverseEList<HWConnection>(HWConnection.class, this,
				SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION, SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION);
		}
		return hwConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<INOUT> getInOutHWComponent()
	{
		if (inOutHWComponent == null)
		{
			inOutHWComponent = new EObjectContainmentEList<INOUT>(INOUT.class, this,
				SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT);
		}
		return inOutHWComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getHWConnection()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			return ((InternalEList<?>) getHWConnection()).basicRemove(otherEnd, msgs);
		case SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT:
			return ((InternalEList<?>) getInOutHWComponent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			return getHWConnection();
		case SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT:
			return getInOutHWComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			getHWConnection().clear();
			getHWConnection().addAll((Collection<? extends HWConnection>) newValue);
			return;
		case SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT:
			getInOutHWComponent().clear();
			getInOutHWComponent().addAll((Collection<? extends INOUT>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			getHWConnection().clear();
			return;
		case SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT:
			getInOutHWComponent().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION:
			return hwConnection != null && !hwConnection.isEmpty();
		case SyndexPackage.HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT:
			return inOutHWComponent != null && !inOutHWComponent.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // HardwareComponentImpl
