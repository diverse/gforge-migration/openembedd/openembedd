/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: DataConnectionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.DataConnection;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Data Connection</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.DataConnectionImpl#getSourceDataConnection <em>Source Data Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.DataConnectionImpl#getTargetDataconnection <em>Target Dataconnection</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class DataConnectionImpl extends ApplicationConnectionImpl implements DataConnection
{
	/**
	 * The cached value of the '{@link #getSourceDataConnection() <em>Source Data Connection</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceDataConnection()
	 * @generated
	 * @ordered
	 */
	protected Output	sourceDataConnection;

	/**
	 * The cached value of the '{@link #getTargetDataconnection() <em>Target Dataconnection</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTargetDataconnection()
	 * @generated
	 * @ordered
	 */
	protected Input		targetDataconnection;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DataConnectionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.DATA_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output getSourceDataConnection()
	{
		if (sourceDataConnection != null && sourceDataConnection.eIsProxy())
		{
			InternalEObject oldSourceDataConnection = (InternalEObject) sourceDataConnection;
			sourceDataConnection = (Output) eResolveProxy(oldSourceDataConnection);
			if (sourceDataConnection != oldSourceDataConnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION, oldSourceDataConnection,
						sourceDataConnection));
			}
		}
		return sourceDataConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output basicGetSourceDataConnection()
	{
		return sourceDataConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceDataConnection(Output newSourceDataConnection)
	{
		Output oldSourceDataConnection = sourceDataConnection;
		sourceDataConnection = newSourceDataConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION, oldSourceDataConnection, sourceDataConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input getTargetDataconnection()
	{
		if (targetDataconnection != null && targetDataconnection.eIsProxy())
		{
			InternalEObject oldTargetDataconnection = (InternalEObject) targetDataconnection;
			targetDataconnection = (Input) eResolveProxy(oldTargetDataconnection);
			if (targetDataconnection != oldTargetDataconnection)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION, oldTargetDataconnection,
						targetDataconnection));
			}
		}
		return targetDataconnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input basicGetTargetDataconnection()
	{
		return targetDataconnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetDataconnection(Input newTargetDataconnection)
	{
		Input oldTargetDataconnection = targetDataconnection;
		targetDataconnection = newTargetDataconnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION,
				oldTargetDataconnection, targetDataconnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION:
			if (resolve)
				return getSourceDataConnection();
			return basicGetSourceDataConnection();
		case SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION:
			if (resolve)
				return getTargetDataconnection();
			return basicGetTargetDataconnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION:
			setSourceDataConnection((Output) newValue);
			return;
		case SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION:
			setTargetDataconnection((Input) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION:
			setSourceDataConnection((Output) null);
			return;
		case SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION:
			setTargetDataconnection((Input) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.DATA_CONNECTION__SOURCE_DATA_CONNECTION:
			return sourceDataConnection != null;
		case SyndexPackage.DATA_CONNECTION__TARGET_DATACONNECTION:
			return targetDataconnection != null;
		}
		return super.eIsSet(featureID);
	}

} // DataConnectionImpl
