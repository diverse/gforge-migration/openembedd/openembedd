/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: FunctionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Function</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.FunctionImpl#getInputPortFunction <em>Input Port Function</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.FunctionImpl#getOutputPortFunction <em>Output Port Function</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FunctionImpl extends AtomicApplicationComponentImpl implements Function
{
	/**
	 * The cached value of the '{@link #getInputPortFunction() <em>Input Port Function</em>}' containment reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInputPortFunction()
	 * @generated
	 * @ordered
	 */
	protected EList<Input>	inputPortFunction;

	/**
	 * The cached value of the '{@link #getOutputPortFunction() <em>Output Port Function</em>}' containment reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOutputPortFunction()
	 * @generated
	 * @ordered
	 */
	protected EList<Output>	outputPortFunction;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected FunctionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.FUNCTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Input> getInputPortFunction()
	{
		if (inputPortFunction == null)
		{
			inputPortFunction = new EObjectContainmentEList<Input>(Input.class, this,
				SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION);
		}
		return inputPortFunction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Output> getOutputPortFunction()
	{
		if (outputPortFunction == null)
		{
			outputPortFunction = new EObjectContainmentEList<Output>(Output.class, this,
				SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION);
		}
		return outputPortFunction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION:
			return ((InternalEList<?>) getInputPortFunction()).basicRemove(otherEnd, msgs);
		case SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION:
			return ((InternalEList<?>) getOutputPortFunction()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION:
			return getInputPortFunction();
		case SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION:
			return getOutputPortFunction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION:
			getInputPortFunction().clear();
			getInputPortFunction().addAll((Collection<? extends Input>) newValue);
			return;
		case SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION:
			getOutputPortFunction().clear();
			getOutputPortFunction().addAll((Collection<? extends Output>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION:
			getInputPortFunction().clear();
			return;
		case SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION:
			getOutputPortFunction().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.FUNCTION__INPUT_PORT_FUNCTION:
			return inputPortFunction != null && !inputPortFunction.isEmpty();
		case SyndexPackage.FUNCTION__OUTPUT_PORT_FUNCTION:
			return outputPortFunction != null && !outputPortFunction.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // FunctionImpl
