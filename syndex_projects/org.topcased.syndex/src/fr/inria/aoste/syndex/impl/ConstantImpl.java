/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConstantImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Constant</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ConstantImpl#getOneOutputConstant <em>One Output Constant</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConstantImpl#getConstantValue <em>Constant Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConstantImpl#getEnumValues <em>Enum Values</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ConstantImpl extends AtomicApplicationComponentImpl implements Constant
{
	/**
	 * The cached value of the '{@link #getOneOutputConstant() <em>One Output Constant</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOneOutputConstant()
	 * @generated
	 * @ordered
	 */
	protected Output			oneOutputConstant;

	/**
	 * The default value of the '{@link #getConstantValue() <em>Constant Value</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getConstantValue()
	 * @generated
	 * @ordered
	 */
	protected static final int	CONSTANT_VALUE_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getConstantValue() <em>Constant Value</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getConstantValue()
	 * @generated
	 * @ordered
	 */
	protected int				constantValue			= CONSTANT_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEnumValues() <em>Enum Values</em>}' attribute list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getEnumValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer>	enumValues;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConstantImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.CONSTANT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output getOneOutputConstant()
	{
		return oneOutputConstant;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetOneOutputConstant(Output newOneOutputConstant, NotificationChain msgs)
	{
		Output oldOneOutputConstant = oneOutputConstant;
		oneOutputConstant = newOneOutputConstant;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT, oldOneOutputConstant, newOneOutputConstant);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOneOutputConstant(Output newOneOutputConstant)
	{
		if (newOneOutputConstant != oneOutputConstant)
		{
			NotificationChain msgs = null;
			if (oneOutputConstant != null)
				msgs = ((InternalEObject) oneOutputConstant).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT, null, msgs);
			if (newOneOutputConstant != null)
				msgs = ((InternalEObject) newOneOutputConstant).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT, null, msgs);
			msgs = basicSetOneOutputConstant(newOneOutputConstant, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT,
				newOneOutputConstant, newOneOutputConstant));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getConstantValue()
	{
		return constantValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setConstantValue(int newConstantValue)
	{
		int oldConstantValue = constantValue;
		constantValue = newConstantValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.CONSTANT__CONSTANT_VALUE,
				oldConstantValue, constantValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Integer> getEnumValues()
	{
		if (enumValues == null)
		{
			enumValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, SyndexPackage.CONSTANT__ENUM_VALUES);
		}
		return enumValues;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT:
			return basicSetOneOutputConstant(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT:
			return getOneOutputConstant();
		case SyndexPackage.CONSTANT__CONSTANT_VALUE:
			return new Integer(getConstantValue());
		case SyndexPackage.CONSTANT__ENUM_VALUES:
			return getEnumValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT:
			setOneOutputConstant((Output) newValue);
			return;
		case SyndexPackage.CONSTANT__CONSTANT_VALUE:
			setConstantValue(((Integer) newValue).intValue());
			return;
		case SyndexPackage.CONSTANT__ENUM_VALUES:
			getEnumValues().clear();
			getEnumValues().addAll((Collection<? extends Integer>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT:
			setOneOutputConstant((Output) null);
			return;
		case SyndexPackage.CONSTANT__CONSTANT_VALUE:
			setConstantValue(CONSTANT_VALUE_EDEFAULT);
			return;
		case SyndexPackage.CONSTANT__ENUM_VALUES:
			getEnumValues().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONSTANT__ONE_OUTPUT_CONSTANT:
			return oneOutputConstant != null;
		case SyndexPackage.CONSTANT__CONSTANT_VALUE:
			return constantValue != CONSTANT_VALUE_EDEFAULT;
		case SyndexPackage.CONSTANT__ENUM_VALUES:
			return enumValues != null && !enumValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (constantValue: ");
		result.append(constantValue);
		result.append(", enumValues: ");
		result.append(enumValues);
		result.append(')');
		return result.toString();
	}

} // ConstantImpl
