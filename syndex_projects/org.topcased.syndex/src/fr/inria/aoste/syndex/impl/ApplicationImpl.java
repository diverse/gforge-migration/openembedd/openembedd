/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Application</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationImpl#getInputPortApplication <em>Input Port Application</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationImpl#getOutputPortApplication <em>Output Port Application</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationImpl#getApplicationInternalStructure <em>Application Internal
 * Structure</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationImpl#isMain <em>Main</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationImpl#getPortCondition <em>Port Condition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ApplicationImpl extends ApplicationComponentImpl implements Application
{
	/**
	 * The cached value of the '{@link #getInputPortApplication() <em>Input Port Application</em>}' containment
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInputPortApplication()
	 * @generated
	 * @ordered
	 */
	protected EList<Input>			inputPortApplication;

	/**
	 * The cached value of the '{@link #getOutputPortApplication() <em>Output Port Application</em>}' containment
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOutputPortApplication()
	 * @generated
	 * @ordered
	 */
	protected EList<Output>			outputPortApplication;

	/**
	 * The cached value of the '{@link #getApplicationInternalStructure() <em>Application Internal Structure</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getApplicationInternalStructure()
	 * @generated
	 * @ordered
	 */
	protected InternalStructure		applicationInternalStructure;

	/**
	 * The default value of the '{@link #isMain() <em>Main</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isMain()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	MAIN_EDEFAULT	= false;

	/**
	 * The cached value of the '{@link #isMain() <em>Main</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isMain()
	 * @generated
	 * @ordered
	 */
	protected boolean				main			= MAIN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortCondition() <em>Port Condition</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPortCondition()
	 * @generated
	 * @ordered
	 */
	protected EList<InCondPort>		portCondition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ApplicationImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.APPLICATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Input> getInputPortApplication()
	{
		if (inputPortApplication == null)
		{
			inputPortApplication = new EObjectContainmentEList<Input>(Input.class, this,
				SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION);
		}
		return inputPortApplication;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Output> getOutputPortApplication()
	{
		if (outputPortApplication == null)
		{
			outputPortApplication = new EObjectContainmentEList<Output>(Output.class, this,
				SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION);
		}
		return outputPortApplication;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalStructure getApplicationInternalStructure()
	{
		return applicationInternalStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetApplicationInternalStructure(InternalStructure newApplicationInternalStructure,
			NotificationChain msgs)
	{
		InternalStructure oldApplicationInternalStructure = applicationInternalStructure;
		applicationInternalStructure = newApplicationInternalStructure;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, oldApplicationInternalStructure,
				newApplicationInternalStructure);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setApplicationInternalStructure(InternalStructure newApplicationInternalStructure)
	{
		if (newApplicationInternalStructure != applicationInternalStructure)
		{
			NotificationChain msgs = null;
			if (applicationInternalStructure != null)
				msgs = ((InternalEObject) applicationInternalStructure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, null, msgs);
			if (newApplicationInternalStructure != null)
				msgs = ((InternalEObject) newApplicationInternalStructure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, null, msgs);
			msgs = basicSetApplicationInternalStructure(newApplicationInternalStructure, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE, newApplicationInternalStructure,
				newApplicationInternalStructure));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean isMain()
	{
		return main;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMain(boolean newMain)
	{
		boolean oldMain = main;
		main = newMain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.APPLICATION__MAIN, oldMain, main));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<InCondPort> getPortCondition()
	{
		if (portCondition == null)
		{
			portCondition = new EObjectContainmentEList<InCondPort>(InCondPort.class, this,
				SyndexPackage.APPLICATION__PORT_CONDITION);
		}
		return portCondition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
			return ((InternalEList<?>) getInputPortApplication()).basicRemove(otherEnd, msgs);
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
			return ((InternalEList<?>) getOutputPortApplication()).basicRemove(otherEnd, msgs);
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
			return basicSetApplicationInternalStructure(null, msgs);
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			return ((InternalEList<?>) getPortCondition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
			return getInputPortApplication();
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
			return getOutputPortApplication();
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
			return getApplicationInternalStructure();
		case SyndexPackage.APPLICATION__MAIN:
			return isMain() ? Boolean.TRUE : Boolean.FALSE;
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			return getPortCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
			getInputPortApplication().clear();
			getInputPortApplication().addAll((Collection<? extends Input>) newValue);
			return;
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
			getOutputPortApplication().clear();
			getOutputPortApplication().addAll((Collection<? extends Output>) newValue);
			return;
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
			setApplicationInternalStructure((InternalStructure) newValue);
			return;
		case SyndexPackage.APPLICATION__MAIN:
			setMain(((Boolean) newValue).booleanValue());
			return;
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			getPortCondition().clear();
			getPortCondition().addAll((Collection<? extends InCondPort>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
			getInputPortApplication().clear();
			return;
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
			getOutputPortApplication().clear();
			return;
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
			setApplicationInternalStructure((InternalStructure) null);
			return;
		case SyndexPackage.APPLICATION__MAIN:
			setMain(MAIN_EDEFAULT);
			return;
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			getPortCondition().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION__INPUT_PORT_APPLICATION:
			return inputPortApplication != null && !inputPortApplication.isEmpty();
		case SyndexPackage.APPLICATION__OUTPUT_PORT_APPLICATION:
			return outputPortApplication != null && !outputPortApplication.isEmpty();
		case SyndexPackage.APPLICATION__APPLICATION_INTERNAL_STRUCTURE:
			return applicationInternalStructure != null;
		case SyndexPackage.APPLICATION__MAIN:
			return main != MAIN_EDEFAULT;
		case SyndexPackage.APPLICATION__PORT_CONDITION:
			return portCondition != null && !portCondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (main: ");
		result.append(main);
		result.append(')');
		return result.toString();
	}

} // ApplicationImpl
