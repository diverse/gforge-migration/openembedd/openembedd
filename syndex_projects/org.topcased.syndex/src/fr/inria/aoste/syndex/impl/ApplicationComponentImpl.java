/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationComponentImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Application Component</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ApplicationComponentImpl#getParamDeclarations <em>Param Declarations</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ApplicationComponentImpl extends ComponentImpl implements ApplicationComponent
{
	/**
	 * The cached value of the '{@link #getParamDeclarations() <em>Param Declarations</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParamDeclarations()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterDeclaration>	paramDeclarations;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ApplicationComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.APPLICATION_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ParameterDeclaration> getParamDeclarations()
	{
		if (paramDeclarations == null)
		{
			paramDeclarations = new EObjectContainmentEList<ParameterDeclaration>(ParameterDeclaration.class, this,
				SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS);
		}
		return paramDeclarations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS:
			return ((InternalEList<?>) getParamDeclarations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS:
			return getParamDeclarations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS:
			getParamDeclarations().clear();
			getParamDeclarations().addAll((Collection<? extends ParameterDeclaration>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS:
			getParamDeclarations().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.APPLICATION_COMPONENT__PARAM_DECLARATIONS:
			return paramDeclarations != null && !paramDeclarations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ApplicationComponentImpl
