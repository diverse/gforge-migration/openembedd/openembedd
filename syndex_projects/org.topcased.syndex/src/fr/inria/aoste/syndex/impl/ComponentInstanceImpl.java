/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ComponentInstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.PortInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Component Instance</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl#getPortInstances <em>Port Instances</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl#getReferencedComponent <em>Referenced Component</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl#getRepetitionFactor <em>Repetition Factor</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl#getParameterValues <em>Parameter Values</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentInstanceImpl#getReferencedHWComponent <em>Referenced HW Component
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ComponentInstanceImpl extends InstanceImpl implements ComponentInstance
{
	/**
	 * The cached value of the '{@link #getPortInstances() <em>Port Instances</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPortInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<PortInstance>	portInstances;

	/**
	 * The cached value of the '{@link #getReferencedComponent() <em>Referenced Component</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReferencedComponent()
	 * @generated
	 * @ordered
	 */
	protected ApplicationComponent	referencedComponent;

	/**
	 * The default value of the '{@link #getRepetitionFactor() <em>Repetition Factor</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRepetitionFactor()
	 * @generated
	 * @ordered
	 */
	protected static final int		REPETITION_FACTOR_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getRepetitionFactor() <em>Repetition Factor</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRepetitionFactor()
	 * @generated
	 * @ordered
	 */
	protected int					repetitionFactor			= REPETITION_FACTOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameterValues() <em>Parameter Values</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParameterValues()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterValue>	parameterValues;

	/**
	 * The cached value of the '{@link #getReferencedHWComponent() <em>Referenced HW Component</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getReferencedHWComponent()
	 * @generated
	 * @ordered
	 */
	protected HardwareComponent		referencedHWComponent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ComponentInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.COMPONENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<PortInstance> getPortInstances()
	{
		if (portInstances == null)
		{
			portInstances = new EObjectContainmentWithInverseEList<PortInstance>(PortInstance.class, this,
				SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES,
				SyndexPackage.PORT_INSTANCE__PARENT_COMPONENT_INSTANCE);
		}
		return portInstances;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent getReferencedComponent()
	{
		if (referencedComponent != null && referencedComponent.eIsProxy())
		{
			InternalEObject oldReferencedComponent = (InternalEObject) referencedComponent;
			referencedComponent = (ApplicationComponent) eResolveProxy(oldReferencedComponent);
			if (referencedComponent != oldReferencedComponent)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT, oldReferencedComponent,
						referencedComponent));
			}
		}
		return referencedComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ApplicationComponent basicGetReferencedComponent()
	{
		return referencedComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setReferencedComponent(ApplicationComponent newReferencedComponent)
	{
		ApplicationComponent oldReferencedComponent = referencedComponent;
		referencedComponent = newReferencedComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT, oldReferencedComponent, referencedComponent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getRepetitionFactor()
	{
		return repetitionFactor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRepetitionFactor(int newRepetitionFactor)
	{
		int oldRepetitionFactor = repetitionFactor;
		repetitionFactor = newRepetitionFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.COMPONENT_INSTANCE__REPETITION_FACTOR,
				oldRepetitionFactor, repetitionFactor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ParameterValue> getParameterValues()
	{
		if (parameterValues == null)
		{
			parameterValues = new EObjectContainmentEList<ParameterValue>(ParameterValue.class, this,
				SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES);
		}
		return parameterValues;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HardwareComponent getReferencedHWComponent()
	{
		if (referencedHWComponent != null && referencedHWComponent.eIsProxy())
		{
			InternalEObject oldReferencedHWComponent = (InternalEObject) referencedHWComponent;
			referencedHWComponent = (HardwareComponent) eResolveProxy(oldReferencedHWComponent);
			if (referencedHWComponent != oldReferencedHWComponent)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT, oldReferencedHWComponent,
						referencedHWComponent));
			}
		}
		return referencedHWComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HardwareComponent basicGetReferencedHWComponent()
	{
		return referencedHWComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setReferencedHWComponent(HardwareComponent newReferencedHWComponent)
	{
		HardwareComponent oldReferencedHWComponent = referencedHWComponent;
		referencedHWComponent = newReferencedHWComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT, oldReferencedHWComponent,
				referencedHWComponent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPortInstances()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			return ((InternalEList<?>) getPortInstances()).basicRemove(otherEnd, msgs);
		case SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES:
			return ((InternalEList<?>) getParameterValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			return getPortInstances();
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT:
			if (resolve)
				return getReferencedComponent();
			return basicGetReferencedComponent();
		case SyndexPackage.COMPONENT_INSTANCE__REPETITION_FACTOR:
			return new Integer(getRepetitionFactor());
		case SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES:
			return getParameterValues();
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT:
			if (resolve)
				return getReferencedHWComponent();
			return basicGetReferencedHWComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			getPortInstances().clear();
			getPortInstances().addAll((Collection<? extends PortInstance>) newValue);
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT:
			setReferencedComponent((ApplicationComponent) newValue);
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REPETITION_FACTOR:
			setRepetitionFactor(((Integer) newValue).intValue());
			return;
		case SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES:
			getParameterValues().clear();
			getParameterValues().addAll((Collection<? extends ParameterValue>) newValue);
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT:
			setReferencedHWComponent((HardwareComponent) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			getPortInstances().clear();
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT:
			setReferencedComponent((ApplicationComponent) null);
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REPETITION_FACTOR:
			setRepetitionFactor(REPETITION_FACTOR_EDEFAULT);
			return;
		case SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES:
			getParameterValues().clear();
			return;
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT:
			setReferencedHWComponent((HardwareComponent) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT_INSTANCE__PORT_INSTANCES:
			return portInstances != null && !portInstances.isEmpty();
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT:
			return referencedComponent != null;
		case SyndexPackage.COMPONENT_INSTANCE__REPETITION_FACTOR:
			return repetitionFactor != REPETITION_FACTOR_EDEFAULT;
		case SyndexPackage.COMPONENT_INSTANCE__PARAMETER_VALUES:
			return parameterValues != null && !parameterValues.isEmpty();
		case SyndexPackage.COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT:
			return referencedHWComponent != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (repetitionFactor: ");
		result.append(repetitionFactor);
		result.append(')');
		return result.toString();
	}

} // ComponentInstanceImpl
