/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexPackageImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.Array;
import fr.inria.aoste.syndex.Asic;
import fr.inria.aoste.syndex.AssociationComtoProc;
import fr.inria.aoste.syndex.AssociationSWtoHW;
import fr.inria.aoste.syndex.AtomicApplicationComponent;
import fr.inria.aoste.syndex.Caracteristics;
import fr.inria.aoste.syndex.CodeExecutionPhaseKind;
import fr.inria.aoste.syndex.Component;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.ConnecteableElement;
import fr.inria.aoste.syndex.Connection;
import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.DataConnection;
import fr.inria.aoste.syndex.DataTypeK;
import fr.inria.aoste.syndex.Datatype;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Element;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.HWConnection;
import fr.inria.aoste.syndex.Hardware;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Instance;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependency;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.NamedElement;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.PortInstance;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.RepetitionConnection;
import fr.inria.aoste.syndex.RepetitionConnectionKind;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SynDEx;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.Vector;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class SyndexPackageImpl extends EPackageImpl implements SyndexPackage
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	actuatorEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	atomicApplicationComponentEClass	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	portEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	namedElementEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	applicationComponentEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	sensorEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	applicationEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inputEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	componentEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	outputEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	functionEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	constantEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	delayEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	connectionEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	dataConnectionEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	instanceEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	componentInstanceEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	portInstanceEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inInstanceEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	outInstanceEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	internalDependencyINEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	internalDependencyOUTEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	connecteableElementEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inCondPortEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	hardwareComponentEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	hardwareEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	mediaEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	fpgaEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	asicEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	dspEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	ramEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	samEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	samptpEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	hwConnectionEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	connectionHWEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	caracteristicsEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	associationSWtoHWEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	associationComtoProcEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	durationEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	enumLitDurationEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	datatypeEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	sizeEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	arrayEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	vectorEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	processorEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	elementEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	synDExEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	applicationConnectionEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	internalStructureEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	elementaryStructureEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	compoundStructureEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inoutEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	internalDataConnectionEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	parameterDeclarationEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	parameterValueEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	conditionedStructureEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	conditionEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	repetitionConnectionEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inoutInstanceEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	inCondInstanceEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	internalDependencyEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	codeExecutionPhaseKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	repetitionConnectionKindEEnum		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	dataTypeKEEnum						= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with {@link org.eclipse.emf.ecore.EPackage.Registry
	 * EPackage.Registry} by the package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory method {@link #init init()}, which also
	 * performs initialization of the package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.syndex.SyndexPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SyndexPackageImpl()
	{
		super(eNS_URI, SyndexFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * Simple dependencies are satisfied by calling this method on all dependent packages before doing anything else.
	 * This method drives initialization for interdependent packages directly, in parallel with this package, itself.
	 * <p>
	 * Of this package and its interdependencies, all packages which have not yet been registered by their URI values
	 * are first created and registered. The packages are then initialized in two steps: meta-model objects for all of
	 * the packages are created before any are initialized, since one package's meta-model objects may refer to those of
	 * another.
	 * <p>
	 * Invocation of this method will not affect any packages that have already been initialized. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SyndexPackage init()
	{
		if (isInited)
			return (SyndexPackage) EPackage.Registry.INSTANCE.getEPackage(SyndexPackage.eNS_URI);

		// Obtain or create and register package
		SyndexPackageImpl theSyndexPackage = (SyndexPackageImpl) (EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof SyndexPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(eNS_URI)
				: new SyndexPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSyndexPackage.createPackageContents();

		// Initialize created meta-data
		theSyndexPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSyndexPackage.freeze();

		return theSyndexPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getActuator()
	{
		return actuatorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getActuator_InputPortActuator()
	{
		return (EReference) actuatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAtomicApplicationComponent()
	{
		return atomicApplicationComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAtomicApplicationComponent_AtomicApplicationStructure()
	{
		return (EReference) atomicApplicationComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getPort()
	{
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPort_ParentPort()
	{
		return (EReference) portEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPort_SizePort()
	{
		return (EReference) portEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getPort_TypePort()
	{
		return (EAttribute) portEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EReference getPort_ParentConditionReference()
	{
		return (EReference) portEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EReference getPort_ParentCompoundStructure()
	{
		return (EReference) portEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getNamedElement()
	{
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNamedElement_Name()
	{
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getApplicationComponent()
	{
		return applicationComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplicationComponent_ParamDeclarations()
	{
		return (EReference) applicationComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSensor()
	{
		return sensorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSensor_OutputPortSensor()
	{
		return (EReference) sensorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getApplication()
	{
		return applicationEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplication_InputPortApplication()
	{
		return (EReference) applicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplication_OutputPortApplication()
	{
		return (EReference) applicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplication_ApplicationInternalStructure()
	{
		return (EReference) applicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getApplication_Main()
	{
		return (EAttribute) applicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplication_PortCondition()
	{
		return (EReference) applicationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInput()
	{
		return inputEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getComponent()
	{
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getComponent_ExecutionPhases()
	{
		return (EAttribute) componentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getOutput()
	{
		return outputEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getFunction()
	{
		return functionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getFunction_InputPortFunction()
	{
		return (EReference) functionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getFunction_OutputPortFunction()
	{
		return (EReference) functionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConstant()
	{
		return constantEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getConstant_OneOutputConstant()
	{
		return (EReference) constantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getConstant_ConstantValue()
	{
		return (EAttribute) constantEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getConstant_EnumValues()
	{
		return (EAttribute) constantEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDelay()
	{
		return delayEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDelay_OneInputDelay()
	{
		return (EReference) delayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDelay_OneOutputDelay()
	{
		return (EReference) delayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDelay_DelayValue()
	{
		return (EAttribute) delayEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDelay_EnumValue()
	{
		return (EAttribute) delayEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConnection()
	{
		return connectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDataConnection()
	{
		return dataConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDataConnection_SourceDataConnection()
	{
		return (EReference) dataConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDataConnection_TargetDataconnection()
	{
		return (EReference) dataConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInstance()
	{
		return instanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getComponentInstance()
	{
		return componentInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getComponentInstance_PortInstances()
	{
		return (EReference) componentInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getComponentInstance_ReferencedComponent()
	{
		return (EReference) componentInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getComponentInstance_RepetitionFactor()
	{
		return (EAttribute) componentInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getComponentInstance_ParameterValues()
	{
		return (EReference) componentInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getComponentInstance_ReferencedHWComponent()
	{
		return (EReference) componentInstanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getPortInstance()
	{
		return portInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPortInstance_ParentComponentInstance()
	{
		return (EReference) portInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getINInstance()
	{
		return inInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getINInstance_OwnerInput()
	{
		return (EReference) inInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getOutInstance()
	{
		return outInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getOutInstance_OwnerOutput()
	{
		return (EReference) outInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInternalDependencyIN()
	{
		return internalDependencyINEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyIN_SourceIDI()
	{
		return (EReference) internalDependencyINEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyIN_TargetIDI()
	{
		return (EReference) internalDependencyINEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyIN_SourceIDIC()
	{
		return (EReference) internalDependencyINEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyIN_TargetIDIC()
	{
		return (EReference) internalDependencyINEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInternalDependencyOUT()
	{
		return internalDependencyOUTEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyOUT_SourceIDO()
	{
		return (EReference) internalDependencyOUTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependencyOUT_TargetIDO()
	{
		return (EReference) internalDependencyOUTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConnecteableElement()
	{
		return connecteableElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInCondPort()
	{
		return inCondPortEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getHardwareComponent()
	{
		return hardwareComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getHardwareComponent_HWConnection()
	{
		return (EReference) hardwareComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getHardwareComponent_InOutHWComponent()
	{
		return (EReference) hardwareComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getHardware()
	{
		return hardwareEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getHardware_ComponentHWInstance()
	{
		return (EReference) hardwareEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getHardware_Main()
	{
		return (EAttribute) hardwareEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getMedia()
	{
		return mediaEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getMedia_Bandwidth()
	{
		return (EAttribute) mediaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getMedia_Size()
	{
		return (EAttribute) mediaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMedia_ParentMemory()
	{
		return (EReference) mediaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getMedia_Broadcast()
	{
		return (EAttribute) mediaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMedia_Duration()
	{
		return (EReference) mediaEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getFPGA()
	{
		return fpgaEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAsic()
	{
		return asicEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDSP()
	{
		return dspEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getRAM()
	{
		return ramEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSAM()
	{
		return samEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSAMPTP()
	{
		return samptpEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getHWConnection()
	{
		return hwConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getHWConnection_ParentHWConnection()
	{
		return (EReference) hwConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConnectionHW()
	{
		return connectionHWEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getConnectionHW_SourceInOut()
	{
		return (EReference) connectionHWEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getConnectionHW_TargetInOut()
	{
		return (EReference) connectionHWEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCaracteristics()
	{
		return caracteristicsEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getCaracteristics_Adequation()
	{
		return (EAttribute) caracteristicsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAssociationSWtoHW()
	{
		return associationSWtoHWEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getAssociationSWtoHW_Duration()
	{
		return (EAttribute) associationSWtoHWEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAssociationSWtoHW_SourceAppli()
	{
		return (EReference) associationSWtoHWEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAssociationSWtoHW_TargetHW()
	{
		return (EReference) associationSWtoHWEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAssociationComtoProc()
	{
		return associationComtoProcEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAssociationComtoProc_SourceAssociationComToProc()
	{
		return (EReference) associationComtoProcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAssociationComtoProc_Duration()
	{
		return (EReference) associationComtoProcEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAssociationComtoProc_TargetHWProcess()
	{
		return (EReference) associationComtoProcEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDuration()
	{
		return durationEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDuration_EnumDuration()
	{
		return (EReference) durationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDuration_ParentDuration()
	{
		return (EReference) durationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getEnumLitDuration()
	{
		return enumLitDurationEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getEnumLitDuration_Value()
	{
		return (EAttribute) enumLitDurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getEnumLitDuration_ParentEnumDuration()
	{
		return (EReference) enumLitDurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDatatype()
	{
		return datatypeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSize()
	{
		return sizeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getArray()
	{
		return arrayEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArray_Vectors()
	{
		return (EReference) arrayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getVector()
	{
		return vectorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getVector_Vector()
	{
		return (EAttribute) vectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getProcessor()
	{
		return processorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getProcessor_ParentProcessor()
	{
		return (EReference) processorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getProcessor_DurationSW()
	{
		return (EReference) processorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getElement()
	{
		return elementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSynDEx()
	{
		return synDExEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSynDEx_SynDExelements()
	{
		return (EReference) synDExEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getApplicationConnection()
	{
		return applicationConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplicationConnection_ParentCoumpoundApplicationConnection()
	{
		return (EReference) applicationConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getApplicationConnection_ParentConditionApplicationConnection()
	{
		return (EReference) applicationConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInternalStructure()
	{
		return internalStructureEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalStructure_OwnerInternalStructure()
	{
		return (EReference) internalStructureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalStructure_InternalConnector()
	{
		return (EReference) internalStructureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getElementaryStructure()
	{
		return elementaryStructureEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCompoundStructure()
	{
		return compoundStructureEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCompoundStructure_ComponentInstance()
	{
		return (EReference) compoundStructureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EReference getCompoundStructure_PortCompoundStructure()
	{
		return (EReference) compoundStructureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getINOUT()
	{
		return inoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getINOUT_Type()
	{
		return (EAttribute) inoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInternalDataConnection()
	{
		return internalDataConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDataConnection_SourceOutInstance()
	{
		return (EReference) internalDataConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDataConnection_TargetINInstance()
	{
		return (EReference) internalDataConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDataConnection_TargetINInstanceC()
	{
		return (EReference) internalDataConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getParameterDeclaration()
	{
		return parameterDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getParameterValue()
	{
		return parameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getParameterValue_ValueParameter()
	{
		return (EAttribute) parameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getParameterValue_ParamDeclaration()
	{
		return (EReference) parameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConditionedStructure()
	{
		return conditionedStructureEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getConditionedStructure_Conditions()
	{
		return (EReference) conditionedStructureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getConditionedStructure_PortCond()
	{
		return (EReference) conditionedStructureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCondition()
	{
		return conditionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getCondition_ValueCondition()
	{
		return (EAttribute) conditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCondition_ConditionPort()
	{
		return (EReference) conditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCondition_ComponentInstance()
	{
		return (EReference) conditionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EReference getCondition_ConditionPortReference()
	{
		return (EReference) conditionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getRepetitionConnection()
	{
		return repetitionConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getRepetitionConnection_RepetitionKind()
	{
		return (EAttribute) repetitionConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getRepetitionConnection_SourceRepetitionConnection()
	{
		return (EReference) repetitionConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getRepetitionConnection_TargetRepetitionConnection()
	{
		return (EReference) repetitionConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getINOUTInstance()
	{
		return inoutInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getINOUTInstance_OwnerINOUT()
	{
		return (EReference) inoutInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInCondInstance()
	{
		return inCondInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInCondInstance_OwnerCond()
	{
		return (EReference) inCondInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInternalDependency()
	{
		return internalDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependency_Source()
	{
		return (EReference) internalDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInternalDependency_Target()
	{
		return (EReference) internalDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getCodeExecutionPhaseKind()
	{
		return codeExecutionPhaseKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getRepetitionConnectionKind()
	{
		return repetitionConnectionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getDataTypeK()
	{
		return dataTypeKEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SyndexFactory getSyndexFactory()
	{
		return (SyndexFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package. This method is guarded to have no affect on any invocation but
	 * its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void createPackageContents()
	{
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		actuatorEClass = createEClass(ACTUATOR);
		createEReference(actuatorEClass, ACTUATOR__INPUT_PORT_ACTUATOR);

		atomicApplicationComponentEClass = createEClass(ATOMIC_APPLICATION_COMPONENT);
		createEReference(atomicApplicationComponentEClass, ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE);

		portEClass = createEClass(PORT);
		createEReference(portEClass, PORT__PARENT_PORT);
		createEReference(portEClass, PORT__SIZE_PORT);
		createEAttribute(portEClass, PORT__TYPE_PORT);
		createEReference(portEClass, PORT__PARENT_CONDITION_REFERENCE);
		createEReference(portEClass, PORT__PARENT_COMPOUND_STRUCTURE);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		applicationComponentEClass = createEClass(APPLICATION_COMPONENT);
		createEReference(applicationComponentEClass, APPLICATION_COMPONENT__PARAM_DECLARATIONS);

		sensorEClass = createEClass(SENSOR);
		createEReference(sensorEClass, SENSOR__OUTPUT_PORT_SENSOR);

		applicationEClass = createEClass(APPLICATION);
		createEReference(applicationEClass, APPLICATION__INPUT_PORT_APPLICATION);
		createEReference(applicationEClass, APPLICATION__OUTPUT_PORT_APPLICATION);
		createEReference(applicationEClass, APPLICATION__APPLICATION_INTERNAL_STRUCTURE);
		createEAttribute(applicationEClass, APPLICATION__MAIN);
		createEReference(applicationEClass, APPLICATION__PORT_CONDITION);

		inputEClass = createEClass(INPUT);

		componentEClass = createEClass(COMPONENT);
		createEAttribute(componentEClass, COMPONENT__EXECUTION_PHASES);

		outputEClass = createEClass(OUTPUT);

		functionEClass = createEClass(FUNCTION);
		createEReference(functionEClass, FUNCTION__INPUT_PORT_FUNCTION);
		createEReference(functionEClass, FUNCTION__OUTPUT_PORT_FUNCTION);

		constantEClass = createEClass(CONSTANT);
		createEReference(constantEClass, CONSTANT__ONE_OUTPUT_CONSTANT);
		createEAttribute(constantEClass, CONSTANT__CONSTANT_VALUE);
		createEAttribute(constantEClass, CONSTANT__ENUM_VALUES);

		delayEClass = createEClass(DELAY);
		createEReference(delayEClass, DELAY__ONE_INPUT_DELAY);
		createEReference(delayEClass, DELAY__ONE_OUTPUT_DELAY);
		createEAttribute(delayEClass, DELAY__DELAY_VALUE);
		createEAttribute(delayEClass, DELAY__ENUM_VALUE);

		connectionEClass = createEClass(CONNECTION);

		dataConnectionEClass = createEClass(DATA_CONNECTION);
		createEReference(dataConnectionEClass, DATA_CONNECTION__SOURCE_DATA_CONNECTION);
		createEReference(dataConnectionEClass, DATA_CONNECTION__TARGET_DATACONNECTION);

		instanceEClass = createEClass(INSTANCE);

		componentInstanceEClass = createEClass(COMPONENT_INSTANCE);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__PORT_INSTANCES);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__REFERENCED_COMPONENT);
		createEAttribute(componentInstanceEClass, COMPONENT_INSTANCE__REPETITION_FACTOR);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__PARAMETER_VALUES);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__REFERENCED_HW_COMPONENT);

		portInstanceEClass = createEClass(PORT_INSTANCE);
		createEReference(portInstanceEClass, PORT_INSTANCE__PARENT_COMPONENT_INSTANCE);

		inInstanceEClass = createEClass(IN_INSTANCE);
		createEReference(inInstanceEClass, IN_INSTANCE__OWNER_INPUT);

		outInstanceEClass = createEClass(OUT_INSTANCE);
		createEReference(outInstanceEClass, OUT_INSTANCE__OWNER_OUTPUT);

		internalDependencyINEClass = createEClass(INTERNAL_DEPENDENCY_IN);
		createEReference(internalDependencyINEClass, INTERNAL_DEPENDENCY_IN__SOURCE_IDI);
		createEReference(internalDependencyINEClass, INTERNAL_DEPENDENCY_IN__TARGET_IDI);
		createEReference(internalDependencyINEClass, INTERNAL_DEPENDENCY_IN__SOURCE_IDIC);
		createEReference(internalDependencyINEClass, INTERNAL_DEPENDENCY_IN__TARGET_IDIC);

		internalDependencyOUTEClass = createEClass(INTERNAL_DEPENDENCY_OUT);
		createEReference(internalDependencyOUTEClass, INTERNAL_DEPENDENCY_OUT__SOURCE_IDO);
		createEReference(internalDependencyOUTEClass, INTERNAL_DEPENDENCY_OUT__TARGET_IDO);

		connecteableElementEClass = createEClass(CONNECTEABLE_ELEMENT);

		inCondPortEClass = createEClass(IN_COND_PORT);

		hardwareComponentEClass = createEClass(HARDWARE_COMPONENT);
		createEReference(hardwareComponentEClass, HARDWARE_COMPONENT__HW_CONNECTION);
		createEReference(hardwareComponentEClass, HARDWARE_COMPONENT__IN_OUT_HW_COMPONENT);

		hardwareEClass = createEClass(HARDWARE);
		createEReference(hardwareEClass, HARDWARE__COMPONENT_HW_INSTANCE);
		createEAttribute(hardwareEClass, HARDWARE__MAIN);

		mediaEClass = createEClass(MEDIA);
		createEAttribute(mediaEClass, MEDIA__BANDWIDTH);
		createEAttribute(mediaEClass, MEDIA__SIZE);
		createEReference(mediaEClass, MEDIA__PARENT_MEMORY);
		createEAttribute(mediaEClass, MEDIA__BROADCAST);
		createEReference(mediaEClass, MEDIA__DURATION);

		fpgaEClass = createEClass(FPGA);

		asicEClass = createEClass(ASIC);

		dspEClass = createEClass(DSP);

		ramEClass = createEClass(RAM);

		samEClass = createEClass(SAM);

		samptpEClass = createEClass(SAMPTP);

		hwConnectionEClass = createEClass(HW_CONNECTION);
		createEReference(hwConnectionEClass, HW_CONNECTION__PARENT_HW_CONNECTION);

		connectionHWEClass = createEClass(CONNECTION_HW);
		createEReference(connectionHWEClass, CONNECTION_HW__SOURCE_IN_OUT);
		createEReference(connectionHWEClass, CONNECTION_HW__TARGET_IN_OUT);

		caracteristicsEClass = createEClass(CARACTERISTICS);
		createEAttribute(caracteristicsEClass, CARACTERISTICS__ADEQUATION);

		associationSWtoHWEClass = createEClass(ASSOCIATION_SWTO_HW);
		createEAttribute(associationSWtoHWEClass, ASSOCIATION_SWTO_HW__DURATION);
		createEReference(associationSWtoHWEClass, ASSOCIATION_SWTO_HW__SOURCE_APPLI);
		createEReference(associationSWtoHWEClass, ASSOCIATION_SWTO_HW__TARGET_HW);

		associationComtoProcEClass = createEClass(ASSOCIATION_COMTO_PROC);
		createEReference(associationComtoProcEClass, ASSOCIATION_COMTO_PROC__SOURCE_ASSOCIATION_COM_TO_PROC);
		createEReference(associationComtoProcEClass, ASSOCIATION_COMTO_PROC__DURATION);
		createEReference(associationComtoProcEClass, ASSOCIATION_COMTO_PROC__TARGET_HW_PROCESS);

		durationEClass = createEClass(DURATION);
		createEReference(durationEClass, DURATION__ENUM_DURATION);
		createEReference(durationEClass, DURATION__PARENT_DURATION);

		enumLitDurationEClass = createEClass(ENUM_LIT_DURATION);
		createEAttribute(enumLitDurationEClass, ENUM_LIT_DURATION__VALUE);
		createEReference(enumLitDurationEClass, ENUM_LIT_DURATION__PARENT_ENUM_DURATION);

		datatypeEClass = createEClass(DATATYPE);

		sizeEClass = createEClass(SIZE);

		arrayEClass = createEClass(ARRAY);
		createEReference(arrayEClass, ARRAY__VECTORS);

		vectorEClass = createEClass(VECTOR);
		createEAttribute(vectorEClass, VECTOR__VECTOR);

		processorEClass = createEClass(PROCESSOR);
		createEReference(processorEClass, PROCESSOR__PARENT_PROCESSOR);
		createEReference(processorEClass, PROCESSOR__DURATION_SW);

		elementEClass = createEClass(ELEMENT);

		synDExEClass = createEClass(SYN_DEX);
		createEReference(synDExEClass, SYN_DEX__SYN_DEXELEMENTS);

		applicationConnectionEClass = createEClass(APPLICATION_CONNECTION);
		createEReference(applicationConnectionEClass, APPLICATION_CONNECTION__PARENT_COUMPOUND_APPLICATION_CONNECTION);
		createEReference(applicationConnectionEClass, APPLICATION_CONNECTION__PARENT_CONDITION_APPLICATION_CONNECTION);

		internalStructureEClass = createEClass(INTERNAL_STRUCTURE);
		createEReference(internalStructureEClass, INTERNAL_STRUCTURE__OWNER_INTERNAL_STRUCTURE);
		createEReference(internalStructureEClass, INTERNAL_STRUCTURE__INTERNAL_CONNECTOR);

		elementaryStructureEClass = createEClass(ELEMENTARY_STRUCTURE);

		compoundStructureEClass = createEClass(COMPOUND_STRUCTURE);
		createEReference(compoundStructureEClass, COMPOUND_STRUCTURE__COMPONENT_INSTANCE);
		createEReference(compoundStructureEClass, COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE);

		inoutEClass = createEClass(INOUT);
		createEAttribute(inoutEClass, INOUT__TYPE);

		internalDataConnectionEClass = createEClass(INTERNAL_DATA_CONNECTION);
		createEReference(internalDataConnectionEClass, INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE);
		createEReference(internalDataConnectionEClass, INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE);
		createEReference(internalDataConnectionEClass, INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C);

		parameterDeclarationEClass = createEClass(PARAMETER_DECLARATION);

		parameterValueEClass = createEClass(PARAMETER_VALUE);
		createEAttribute(parameterValueEClass, PARAMETER_VALUE__VALUE_PARAMETER);
		createEReference(parameterValueEClass, PARAMETER_VALUE__PARAM_DECLARATION);

		conditionedStructureEClass = createEClass(CONDITIONED_STRUCTURE);
		createEReference(conditionedStructureEClass, CONDITIONED_STRUCTURE__CONDITIONS);
		createEReference(conditionedStructureEClass, CONDITIONED_STRUCTURE__PORT_COND);

		conditionEClass = createEClass(CONDITION);
		createEAttribute(conditionEClass, CONDITION__VALUE_CONDITION);
		createEReference(conditionEClass, CONDITION__CONDITION_PORT);
		createEReference(conditionEClass, CONDITION__COMPONENT_INSTANCE);
		createEReference(conditionEClass, CONDITION__CONDITION_PORT_REFERENCE);

		repetitionConnectionEClass = createEClass(REPETITION_CONNECTION);
		createEAttribute(repetitionConnectionEClass, REPETITION_CONNECTION__REPETITION_KIND);
		createEReference(repetitionConnectionEClass, REPETITION_CONNECTION__SOURCE_REPETITION_CONNECTION);
		createEReference(repetitionConnectionEClass, REPETITION_CONNECTION__TARGET_REPETITION_CONNECTION);

		inoutInstanceEClass = createEClass(INOUT_INSTANCE);
		createEReference(inoutInstanceEClass, INOUT_INSTANCE__OWNER_INOUT);

		inCondInstanceEClass = createEClass(IN_COND_INSTANCE);
		createEReference(inCondInstanceEClass, IN_COND_INSTANCE__OWNER_COND);

		internalDependencyEClass = createEClass(INTERNAL_DEPENDENCY);
		createEReference(internalDependencyEClass, INTERNAL_DEPENDENCY__SOURCE);
		createEReference(internalDependencyEClass, INTERNAL_DEPENDENCY__TARGET);

		// Create enums
		codeExecutionPhaseKindEEnum = createEEnum(CODE_EXECUTION_PHASE_KIND);
		repetitionConnectionKindEEnum = createEEnum(REPETITION_CONNECTION_KIND);
		dataTypeKEEnum = createEEnum(DATA_TYPE_K);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model. This method is guarded to have no affect on any
	 * invocation but its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void initializePackageContents()
	{
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		actuatorEClass.getESuperTypes().add(this.getAtomicApplicationComponent());
		atomicApplicationComponentEClass.getESuperTypes().add(this.getApplicationComponent());
		portEClass.getESuperTypes().add(this.getNamedElement());
		portEClass.getESuperTypes().add(this.getConnecteableElement());
		namedElementEClass.getESuperTypes().add(this.getElement());
		applicationComponentEClass.getESuperTypes().add(this.getComponent());
		sensorEClass.getESuperTypes().add(this.getAtomicApplicationComponent());
		applicationEClass.getESuperTypes().add(this.getApplicationComponent());
		inputEClass.getESuperTypes().add(this.getPort());
		componentEClass.getESuperTypes().add(this.getNamedElement());
		outputEClass.getESuperTypes().add(this.getPort());
		functionEClass.getESuperTypes().add(this.getAtomicApplicationComponent());
		constantEClass.getESuperTypes().add(this.getAtomicApplicationComponent());
		delayEClass.getESuperTypes().add(this.getAtomicApplicationComponent());
		connectionEClass.getESuperTypes().add(this.getElement());
		dataConnectionEClass.getESuperTypes().add(this.getApplicationConnection());
		instanceEClass.getESuperTypes().add(this.getNamedElement());
		instanceEClass.getESuperTypes().add(this.getElement());
		componentInstanceEClass.getESuperTypes().add(this.getInstance());
		portInstanceEClass.getESuperTypes().add(this.getInstance());
		portInstanceEClass.getESuperTypes().add(this.getConnecteableElement());
		inInstanceEClass.getESuperTypes().add(this.getPortInstance());
		outInstanceEClass.getESuperTypes().add(this.getPortInstance());
		internalDependencyINEClass.getESuperTypes().add(this.getApplicationConnection());
		internalDependencyOUTEClass.getESuperTypes().add(this.getApplicationConnection());
		connecteableElementEClass.getESuperTypes().add(this.getNamedElement());
		connecteableElementEClass.getESuperTypes().add(this.getElement());
		inCondPortEClass.getESuperTypes().add(this.getPort());
		hardwareComponentEClass.getESuperTypes().add(this.getComponent());
		hardwareEClass.getESuperTypes().add(this.getHardwareComponent());
		mediaEClass.getESuperTypes().add(this.getHardwareComponent());
		fpgaEClass.getESuperTypes().add(this.getProcessor());
		asicEClass.getESuperTypes().add(this.getProcessor());
		dspEClass.getESuperTypes().add(this.getProcessor());
		ramEClass.getESuperTypes().add(this.getMedia());
		samEClass.getESuperTypes().add(this.getMedia());
		samptpEClass.getESuperTypes().add(this.getMedia());
		hwConnectionEClass.getESuperTypes().add(this.getConnection());
		connectionHWEClass.getESuperTypes().add(this.getHWConnection());
		caracteristicsEClass.getESuperTypes().add(this.getNamedElement());
		associationSWtoHWEClass.getESuperTypes().add(this.getCaracteristics());
		associationComtoProcEClass.getESuperTypes().add(this.getCaracteristics());
		durationEClass.getESuperTypes().add(this.getNamedElement());
		enumLitDurationEClass.getESuperTypes().add(this.getNamedElement());
		datatypeEClass.getESuperTypes().add(this.getElement());
		sizeEClass.getESuperTypes().add(this.getElement());
		arrayEClass.getESuperTypes().add(this.getSize());
		vectorEClass.getESuperTypes().add(this.getSize());
		processorEClass.getESuperTypes().add(this.getHardwareComponent());
		applicationConnectionEClass.getESuperTypes().add(this.getConnection());
		internalStructureEClass.getESuperTypes().add(this.getElement());
		elementaryStructureEClass.getESuperTypes().add(this.getInternalStructure());
		compoundStructureEClass.getESuperTypes().add(this.getInternalStructure());
		inoutEClass.getESuperTypes().add(this.getPort());
		internalDataConnectionEClass.getESuperTypes().add(this.getApplicationConnection());
		parameterDeclarationEClass.getESuperTypes().add(this.getNamedElement());
		parameterValueEClass.getESuperTypes().add(this.getElement());
		conditionedStructureEClass.getESuperTypes().add(this.getInternalStructure());
		conditionEClass.getESuperTypes().add(this.getNamedElement());
		conditionEClass.getESuperTypes().add(this.getInternalStructure());
		repetitionConnectionEClass.getESuperTypes().add(this.getApplicationConnection());
		inoutInstanceEClass.getESuperTypes().add(this.getPortInstance());
		inCondInstanceEClass.getESuperTypes().add(this.getPortInstance());
		internalDependencyEClass.getESuperTypes().add(this.getApplicationConnection());

		// Initialize classes and features; add operations and parameters
		initEClass(actuatorEClass, Actuator.class, "Actuator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActuator_InputPortActuator(), this.getInput(), null, "inputPortActuator", null, 0, -1,
			Actuator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(atomicApplicationComponentEClass, AtomicApplicationComponent.class, "AtomicApplicationComponent",
			IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAtomicApplicationComponent_AtomicApplicationStructure(), this.getElementaryStructure(), null,
			"atomicApplicationStructure", null, 1, 1, AtomicApplicationComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
			IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_ParentPort(), this.getComponent(), null, "parentPort", null, 1, 1, Port.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getPort_SizePort(), this.getSize(), null, "sizePort", null, 1, 1, Port.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEAttribute(getPort_TypePort(), this.getDataTypeK(), "typePort", "", 1, 1, Port.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPort_ParentConditionReference(), this.getCondition(), this
				.getCondition_ConditionPortReference(), "parentConditionReference", null, 0, 1, Port.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getPort_ParentCompoundStructure(), this.getCompoundStructure(), this
				.getCompoundStructure_PortCompoundStructure(), "parentCompoundStructure", null, 0, 1, Port.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applicationComponentEClass, ApplicationComponent.class, "ApplicationComponent", IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApplicationComponent_ParamDeclarations(), this.getParameterDeclaration(), null,
			"paramDeclarations", null, 0, -1, ApplicationComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sensorEClass, Sensor.class, "Sensor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSensor_OutputPortSensor(), this.getOutput(), null, "outputPortSensor", null, 0, -1,
			Sensor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applicationEClass, Application.class, "Application", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApplication_InputPortApplication(), this.getInput(), null, "inputPortApplication", null, 0,
			-1, Application.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplication_OutputPortApplication(), this.getOutput(), null, "outputPortApplication", null,
			0, -1, Application.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplication_ApplicationInternalStructure(), this.getInternalStructure(), null,
			"applicationInternalStructure", null, 1, 1, Application.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getApplication_Main(), ecorePackage.getEBoolean(), "main", null, 0, 1, Application.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getApplication_PortCondition(), this.getInCondPort(), null, "portCondition", null, 0, -1,
			Application.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputEClass, Input.class, "Input", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentEClass, Component.class, "Component", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComponent_ExecutionPhases(), this.getCodeExecutionPhaseKind(), "executionPhases", "", 0, -1,
			Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(outputEClass, Output.class, "Output", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunction_InputPortFunction(), this.getInput(), null, "inputPortFunction", null, 1, -1,
			Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_OutputPortFunction(), this.getOutput(), null, "outputPortFunction", null, 1, -1,
			Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constantEClass, Constant.class, "Constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstant_OneOutputConstant(), this.getOutput(), null, "oneOutputConstant", null, 1, 1,
			Constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConstant_ConstantValue(), ecorePackage.getEInt(), "constantValue", null, 1, 1,
			Constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEAttribute(getConstant_EnumValues(), ecorePackage.getEInt(), "enumValues", null, 0, -1, Constant.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(delayEClass, Delay.class, "Delay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDelay_OneInputDelay(), this.getInput(), null, "oneInputDelay", null, 1, 1, Delay.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getDelay_OneOutputDelay(), this.getOutput(), null, "oneOutputDelay", null, 1, 1, Delay.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_DelayValue(), ecorePackage.getEInt(), "delayValue", null, 0, 1, Delay.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_EnumValue(), ecorePackage.getEInt(), "enumValue", null, 0, -1, Delay.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionEClass, Connection.class, "Connection", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataConnectionEClass, DataConnection.class, "DataConnection", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataConnection_SourceDataConnection(), this.getOutput(), null, "sourceDataConnection", null,
			1, 1, DataConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataConnection_TargetDataconnection(), this.getInput(), null, "targetDataconnection", null,
			1, 1, DataConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(instanceEClass, Instance.class, "Instance", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentInstanceEClass, ComponentInstance.class, "ComponentInstance", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstance_PortInstances(), this.getPortInstance(), this
				.getPortInstance_ParentComponentInstance(), "portInstances", null, 0, -1, ComponentInstance.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstance_ReferencedComponent(), this.getApplicationComponent(), null,
			"referencedComponent", null, 0, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComponentInstance_RepetitionFactor(), ecorePackage.getEInt(), "repetitionFactor", null, 0, 1,
			ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstance_ParameterValues(), this.getParameterValue(), null, "parameterValues", null,
			0, -1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstance_ReferencedHWComponent(), this.getHardwareComponent(), null,
			"referencedHWComponent", null, 0, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portInstanceEClass, PortInstance.class, "PortInstance", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortInstance_ParentComponentInstance(), this.getComponentInstance(), this
				.getComponentInstance_PortInstances(), "parentComponentInstance", null, 0, 1, PortInstance.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(inInstanceEClass, INInstance.class, "INInstance", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getINInstance_OwnerInput(), this.getInput(), null, "ownerInput", null, 1, 1, INInstance.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(outInstanceEClass, OutInstance.class, "OutInstance", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutInstance_OwnerOutput(), this.getOutput(), null, "ownerOutput", null, 1, 1,
			OutInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalDependencyINEClass, InternalDependencyIN.class, "InternalDependencyIN", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalDependencyIN_SourceIDI(), this.getInput(), null, "sourceIDI", null, 1, 1,
			InternalDependencyIN.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDependencyIN_TargetIDI(), this.getINInstance(), null, "targetIDI", null, 1, 1,
			InternalDependencyIN.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDependencyIN_SourceIDIC(), this.getInCondPort(), null, "sourceIDIC", null, 1, 1,
			InternalDependencyIN.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDependencyIN_TargetIDIC(), this.getInCondInstance(), null, "targetIDIC", null, 1, 1,
			InternalDependencyIN.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalDependencyOUTEClass, InternalDependencyOUT.class, "InternalDependencyOUT", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalDependencyOUT_SourceIDO(), this.getOutInstance(), null, "sourceIDO", null, 1, 1,
			InternalDependencyOUT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDependencyOUT_TargetIDO(), this.getOutput(), null, "targetIDO", null, 1, 1,
			InternalDependencyOUT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteableElementEClass, ConnecteableElement.class, "ConnecteableElement", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(inCondPortEClass, InCondPort.class, "InCondPort", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(hardwareComponentEClass, HardwareComponent.class, "HardwareComponent", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHardwareComponent_HWConnection(), this.getHWConnection(), this
				.getHWConnection_ParentHWConnection(), "HWConnection", null, 0, -1, HardwareComponent.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getHardwareComponent_InOutHWComponent(), this.getINOUT(), null, "inOutHWComponent", null, 0, -1,
			HardwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hardwareEClass, Hardware.class, "Hardware", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHardware_ComponentHWInstance(), this.getComponentInstance(), null, "ComponentHWInstance",
			null, 1, -1, Hardware.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHardware_Main(), ecorePackage.getEBoolean(), "main", null, 0, 1, Hardware.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mediaEClass, Media.class, "Media", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMedia_Bandwidth(), ecorePackage.getEInt(), "bandwidth", null, 1, 1, Media.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMedia_Size(), ecorePackage.getEInt(), "size", null, 1, 1, Media.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMedia_ParentMemory(), this.getMedia(), null, "parentMemory", null, 0, 1, Media.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMedia_Broadcast(), ecorePackage.getEBoolean(), "broadcast", null, 0, 1, Media.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMedia_Duration(), this.getDuration(), null, "duration", null, 0, 1, Media.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(fpgaEClass, fr.inria.aoste.syndex.FPGA.class, "FPGA", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(asicEClass, Asic.class, "Asic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dspEClass, fr.inria.aoste.syndex.DSP.class, "DSP", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(ramEClass, fr.inria.aoste.syndex.RAM.class, "RAM", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(samEClass, fr.inria.aoste.syndex.SAM.class, "SAM", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(samptpEClass, fr.inria.aoste.syndex.SAMPTP.class, "SAMPTP", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);

		initEClass(hwConnectionEClass, HWConnection.class, "HWConnection", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHWConnection_ParentHWConnection(), this.getHardwareComponent(), this
				.getHardwareComponent_HWConnection(), "parentHWConnection", null, 1, 1, HWConnection.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(connectionHWEClass, ConnectionHW.class, "ConnectionHW", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionHW_SourceInOut(), this.getINOUTInstance(), null, "sourceInOut", null, 1, 1,
			ConnectionHW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionHW_TargetInOut(), this.getINOUTInstance(), null, "targetInOut", null, 1, 1,
			ConnectionHW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(caracteristicsEClass, Caracteristics.class, "Caracteristics", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCaracteristics_Adequation(), ecorePackage.getEBoolean(), "adequation", null, 1, 1,
			Caracteristics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(associationSWtoHWEClass, AssociationSWtoHW.class, "AssociationSWtoHW", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssociationSWtoHW_Duration(), ecorePackage.getEInt(), "duration", null, 1, 1,
			AssociationSWtoHW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationSWtoHW_SourceAppli(), this.getComponentInstance(), null, "sourceAppli", null, 1,
			1, AssociationSWtoHW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationSWtoHW_TargetHW(), this.getComponentInstance(), null, "targetHW", null, 1, 1,
			AssociationSWtoHW.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(associationComtoProcEClass, AssociationComtoProc.class, "AssociationComtoProc", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssociationComtoProc_SourceAssociationComToProc(), this.getMedia(), null,
			"sourceAssociationComToProc", null, 1, 1, AssociationComtoProc.class, !IS_TRANSIENT, !IS_VOLATILE,
			IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationComtoProc_Duration(), this.getDuration(), null, "duration", null, 1, 1,
			AssociationComtoProc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationComtoProc_TargetHWProcess(), this.getProcessor(), null, "targetHWProcess", null,
			1, 1, AssociationComtoProc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(durationEClass, Duration.class, "Duration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDuration_EnumDuration(), this.getEnumLitDuration(), this
				.getEnumLitDuration_ParentEnumDuration(), "enumDuration", null, 0, -1, Duration.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getDuration_ParentDuration(), this.getHardwareComponent(), null, "parentDuration", null, 0, -1,
			Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumLitDurationEClass, EnumLitDuration.class, "EnumLitDuration", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnumLitDuration_Value(), ecorePackage.getEInt(), "Value", null, 1, 1, EnumLitDuration.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumLitDuration_ParentEnumDuration(), this.getDuration(), this.getDuration_EnumDuration(),
			"parentEnumDuration", null, 1, 1, EnumLitDuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(datatypeEClass, Datatype.class, "Datatype", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sizeEClass, Size.class, "Size", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(arrayEClass, Array.class, "Array", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArray_Vectors(), this.getVector(), null, "vectors", null, 1, -1, Array.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);

		initEClass(vectorEClass, Vector.class, "Vector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVector_Vector(), ecorePackage.getEString(), "vector", "", 1, 1, Vector.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processorEClass, Processor.class, "Processor", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessor_ParentProcessor(), this.getProcessor(), null, "parentProcessor", null, 0, 1,
			Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessor_DurationSW(), this.getDuration(), null, "durationSW", null, 0, 1, Processor.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(elementEClass, Element.class, "Element", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(synDExEClass, SynDEx.class, "SynDEx", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSynDEx_SynDExelements(), this.getElement(), null, "SynDExelements", null, 0, -1,
			SynDEx.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applicationConnectionEClass, ApplicationConnection.class, "ApplicationConnection", IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApplicationConnection_ParentCoumpoundApplicationConnection(), this.getCompoundStructure(),
			null, "parentCoumpoundApplicationConnection", null, 0, 1, ApplicationConnection.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getApplicationConnection_ParentConditionApplicationConnection(), this.getConditionedStructure(),
			null, "parentConditionApplicationConnection", null, 0, 1, ApplicationConnection.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);

		initEClass(internalStructureEClass, InternalStructure.class, "InternalStructure", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalStructure_OwnerInternalStructure(), this.getApplicationComponent(), null,
			"ownerInternalStructure", null, 1, 1, InternalStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalStructure_InternalConnector(), this.getApplicationConnection(), null,
			"internalConnector", null, 0, -1, InternalStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(elementaryStructureEClass, ElementaryStructure.class, "ElementaryStructure", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(compoundStructureEClass, CompoundStructure.class, "CompoundStructure", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompoundStructure_ComponentInstance(), this.getComponentInstance(), null,
			"componentInstance", null, 1, -1, CompoundStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompoundStructure_PortCompoundStructure(), this.getPort(), this
				.getPort_ParentCompoundStructure(), "portCompoundStructure", null, 1, -1, CompoundStructure.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(inoutEClass, fr.inria.aoste.syndex.INOUT.class, "INOUT", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getINOUT_Type(), ecorePackage.getEString(), "type", null, 0, 1,
			fr.inria.aoste.syndex.INOUT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalDataConnectionEClass, InternalDataConnection.class, "InternalDataConnection", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalDataConnection_SourceOutInstance(), this.getOutInstance(), null, "sourceOutInstance",
			null, 1, 1, InternalDataConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDataConnection_TargetINInstance(), this.getINInstance(), null, "targetINInstance",
			null, 1, 1, InternalDataConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDataConnection_TargetINInstanceC(), this.getInCondInstance(), null,
			"targetINInstanceC", null, 1, 1, InternalDataConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterDeclarationEClass, ParameterDeclaration.class, "ParameterDeclaration", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(parameterValueEClass, ParameterValue.class, "ParameterValue", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterValue_ValueParameter(), ecorePackage.getEString(), "valueParameter", null, 0, 1,
			ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getParameterValue_ParamDeclaration(), this.getParameterDeclaration(), null, "paramDeclaration",
			null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionedStructureEClass, ConditionedStructure.class, "ConditionedStructure", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionedStructure_Conditions(), this.getCondition(), null, "conditions", null, 0, -1,
			ConditionedStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConditionedStructure_PortCond(), this.getInCondPort(), null, "portCond", null, 1, -1,
			ConditionedStructure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCondition_ValueCondition(), ecorePackage.getEString(), "valueCondition", null, 0, 1,
			Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getCondition_ConditionPort(), this.getInCondPort(), null, "conditionPort", null, 1, 1,
			Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCondition_ComponentInstance(), this.getComponentInstance(), null, "componentInstance", null,
			0, -1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCondition_ConditionPortReference(), this.getPort(), this.getPort_ParentConditionReference(),
			"portConditionReference", null, 1, -1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(repetitionConnectionEClass, RepetitionConnection.class, "RepetitionConnection", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRepetitionConnection_RepetitionKind(), this.getRepetitionConnectionKind(), "repetitionKind",
			null, 1, 1, RepetitionConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepetitionConnection_SourceRepetitionConnection(), this.getSize(), null,
			"sourceRepetitionConnection", null, 1, 1, RepetitionConnection.class, !IS_TRANSIENT, !IS_VOLATILE,
			IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepetitionConnection_TargetRepetitionConnection(), this.getSize(), null,
			"targetRepetitionConnection", null, 1, 1, RepetitionConnection.class, !IS_TRANSIENT, !IS_VOLATILE,
			IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inoutInstanceEClass, INOUTInstance.class, "INOUTInstance", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getINOUTInstance_OwnerINOUT(), this.getINOUT(), null, "ownerINOUT", null, 1, 1,
			INOUTInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inCondInstanceEClass, InCondInstance.class, "InCondInstance", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInCondInstance_OwnerCond(), this.getInCondPort(), null, "ownerCond", null, 1, 1,
			InCondInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalDependencyEClass, InternalDependency.class, "InternalDependency", !IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalDependency_Source(), this.getInput(), null, "source", null, 1, 1,
			InternalDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInternalDependency_Target(), this.getOutput(), null, "target", null, 1, 1,
			InternalDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(codeExecutionPhaseKindEEnum, CodeExecutionPhaseKind.class, "CodeExecutionPhaseKind");
		addEEnumLiteral(codeExecutionPhaseKindEEnum, CodeExecutionPhaseKind.LOOPSEQ);
		addEEnumLiteral(codeExecutionPhaseKindEEnum, CodeExecutionPhaseKind.INITSEQ);
		addEEnumLiteral(codeExecutionPhaseKindEEnum, CodeExecutionPhaseKind.ENDSEQ);

		initEEnum(repetitionConnectionKindEEnum, RepetitionConnectionKind.class, "RepetitionConnectionKind");
		addEEnumLiteral(repetitionConnectionKindEEnum, RepetitionConnectionKind.FORK);
		addEEnumLiteral(repetitionConnectionKindEEnum, RepetitionConnectionKind.JOIN);

		initEEnum(dataTypeKEEnum, DataTypeK.class, "DataTypeK");
		addEEnumLiteral(dataTypeKEEnum, DataTypeK.INT);
		addEEnumLiteral(dataTypeKEEnum, DataTypeK.DOUBLE);
		addEEnumLiteral(dataTypeKEEnum, DataTypeK.FLOAT);
		addEEnumLiteral(dataTypeKEEnum, DataTypeK.STRING);
		addEEnumLiteral(dataTypeKEEnum, DataTypeK.BOOL);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.topcased.org/uuid
		createUuidAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.topcased.org/uuid</b>. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	protected void createUuidAnnotations()
	{
		String source = "http://www.topcased.org/uuid";
		addAnnotation(this, source, new String[] { "uuid", "11501873764270" });
		addAnnotation(actuatorEClass, source, new String[] { "uuid", "11501873764302" });
		addAnnotation(atomicApplicationComponentEClass, source, new String[] { "uuid", "11501873764313" });
		addAnnotation(portEClass, source, new String[] { "uuid", "115217288922755" });
	}

} // SyndexPackageImpl
