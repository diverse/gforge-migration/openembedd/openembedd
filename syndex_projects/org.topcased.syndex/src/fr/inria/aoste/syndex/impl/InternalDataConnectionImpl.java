/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDataConnectionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Internal Data Connection</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDataConnectionImpl#getSourceOutInstance <em>Source Out Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDataConnectionImpl#getTargetINInstance <em>Target IN Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDataConnectionImpl#getTargetINInstanceC <em>Target IN Instance C</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InternalDataConnectionImpl extends ApplicationConnectionImpl implements InternalDataConnection
{
	/**
	 * The cached value of the '{@link #getSourceOutInstance() <em>Source Out Instance</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceOutInstance()
	 * @generated
	 * @ordered
	 */
	protected OutInstance		sourceOutInstance;

	/**
	 * The cached value of the '{@link #getTargetINInstance() <em>Target IN Instance</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTargetINInstance()
	 * @generated
	 * @ordered
	 */
	protected INInstance		targetINInstance;

	/**
	 * The cached value of the '{@link #getTargetINInstanceC() <em>Target IN Instance C</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTargetINInstanceC()
	 * @generated
	 * @ordered
	 */
	protected InCondInstance	targetINInstanceC;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InternalDataConnectionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INTERNAL_DATA_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public OutInstance getSourceOutInstance()
	{
		if (sourceOutInstance != null && sourceOutInstance.eIsProxy())
		{
			InternalEObject oldSourceOutInstance = (InternalEObject) sourceOutInstance;
			sourceOutInstance = (OutInstance) eResolveProxy(oldSourceOutInstance);
			if (sourceOutInstance != oldSourceOutInstance)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE, oldSourceOutInstance,
						sourceOutInstance));
			}
		}
		return sourceOutInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public OutInstance basicGetSourceOutInstance()
	{
		return sourceOutInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceOutInstance(OutInstance newSourceOutInstance)
	{
		OutInstance oldSourceOutInstance = sourceOutInstance;
		sourceOutInstance = newSourceOutInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE, oldSourceOutInstance, sourceOutInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INInstance getTargetINInstance()
	{
		if (targetINInstance != null && targetINInstance.eIsProxy())
		{
			InternalEObject oldTargetINInstance = (InternalEObject) targetINInstance;
			targetINInstance = (INInstance) eResolveProxy(oldTargetINInstance);
			if (targetINInstance != oldTargetINInstance)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE, oldTargetINInstance,
						targetINInstance));
			}
		}
		return targetINInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INInstance basicGetTargetINInstance()
	{
		return targetINInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetINInstance(INInstance newTargetINInstance)
	{
		INInstance oldTargetINInstance = targetINInstance;
		targetINInstance = newTargetINInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE, oldTargetINInstance, targetINInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondInstance getTargetINInstanceC()
	{
		if (targetINInstanceC != null && targetINInstanceC.eIsProxy())
		{
			InternalEObject oldTargetINInstanceC = (InternalEObject) targetINInstanceC;
			targetINInstanceC = (InCondInstance) eResolveProxy(oldTargetINInstanceC);
			if (targetINInstanceC != oldTargetINInstanceC)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C, oldTargetINInstanceC,
						targetINInstanceC));
			}
		}
		return targetINInstanceC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondInstance basicGetTargetINInstanceC()
	{
		return targetINInstanceC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetINInstanceC(InCondInstance newTargetINInstanceC)
	{
		InCondInstance oldTargetINInstanceC = targetINInstanceC;
		targetINInstanceC = newTargetINInstanceC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C, oldTargetINInstanceC, targetINInstanceC));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE:
			if (resolve)
				return getSourceOutInstance();
			return basicGetSourceOutInstance();
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE:
			if (resolve)
				return getTargetINInstance();
			return basicGetTargetINInstance();
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C:
			if (resolve)
				return getTargetINInstanceC();
			return basicGetTargetINInstanceC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE:
			setSourceOutInstance((OutInstance) newValue);
			return;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE:
			setTargetINInstance((INInstance) newValue);
			return;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C:
			setTargetINInstanceC((InCondInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE:
			setSourceOutInstance((OutInstance) null);
			return;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE:
			setTargetINInstance((INInstance) null);
			return;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C:
			setTargetINInstanceC((InCondInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DATA_CONNECTION__SOURCE_OUT_INSTANCE:
			return sourceOutInstance != null;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE:
			return targetINInstance != null;
		case SyndexPackage.INTERNAL_DATA_CONNECTION__TARGET_IN_INSTANCE_C:
			return targetINInstanceC != null;
		}
		return super.eIsSet(featureID);
	}

} // InternalDataConnectionImpl
