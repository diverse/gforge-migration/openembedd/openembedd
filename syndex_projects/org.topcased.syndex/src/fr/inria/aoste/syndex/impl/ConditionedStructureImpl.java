/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConditionedStructureImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Conditioned Structure</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionedStructureImpl#getConditions <em>Conditions</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ConditionedStructureImpl#getPortCond <em>Port Cond</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ConditionedStructureImpl extends InternalStructureImpl implements ConditionedStructure
{
	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition>	conditions;

	/**
	 * The cached value of the '{@link #getPortCond() <em>Port Cond</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPortCond()
	 * @generated
	 * @ordered
	 */
	protected EList<InCondPort>	portCond;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConditionedStructureImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.CONDITIONED_STRUCTURE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Condition> getConditions()
	{
		if (conditions == null)
		{
			conditions = new EObjectContainmentEList<Condition>(Condition.class, this,
				SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<InCondPort> getPortCond()
	{
		if (portCond == null)
		{
			portCond = new EObjectContainmentEList<InCondPort>(InCondPort.class, this,
				SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND);
		}
		return portCond;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS:
			return ((InternalEList<?>) getConditions()).basicRemove(otherEnd, msgs);
		case SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND:
			return ((InternalEList<?>) getPortCond()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS:
			return getConditions();
		case SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND:
			return getPortCond();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS:
			getConditions().clear();
			getConditions().addAll((Collection<? extends Condition>) newValue);
			return;
		case SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND:
			getPortCond().clear();
			getPortCond().addAll((Collection<? extends InCondPort>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS:
			getConditions().clear();
			return;
		case SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND:
			getPortCond().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.CONDITIONED_STRUCTURE__CONDITIONS:
			return conditions != null && !conditions.isEmpty();
		case SyndexPackage.CONDITIONED_STRUCTURE__PORT_COND:
			return portCond != null && !portCond.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ConditionedStructureImpl
