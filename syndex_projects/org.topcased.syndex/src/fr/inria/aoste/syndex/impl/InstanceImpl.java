/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.ecore.EClass;

import fr.inria.aoste.syndex.Instance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public abstract class InstanceImpl extends NamedElementImpl implements Instance
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INSTANCE;
	}

} // InstanceImpl
