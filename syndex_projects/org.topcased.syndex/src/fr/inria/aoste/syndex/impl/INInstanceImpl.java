/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: INInstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>IN Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.INInstanceImpl#getOwnerInput <em>Owner Input</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class INInstanceImpl extends PortInstanceImpl implements INInstance
{
	/**
	 * The cached value of the '{@link #getOwnerInput() <em>Owner Input</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOwnerInput()
	 * @generated
	 * @ordered
	 */
	protected Input	ownerInput;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected INInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.IN_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input getOwnerInput()
	{
		if (ownerInput != null && ownerInput.eIsProxy())
		{
			InternalEObject oldOwnerInput = (InternalEObject) ownerInput;
			ownerInput = (Input) eResolveProxy(oldOwnerInput);
			if (ownerInput != oldOwnerInput)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.IN_INSTANCE__OWNER_INPUT,
						oldOwnerInput, ownerInput));
			}
		}
		return ownerInput;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input basicGetOwnerInput()
	{
		return ownerInput;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerInput(Input newOwnerInput)
	{
		Input oldOwnerInput = ownerInput;
		ownerInput = newOwnerInput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.IN_INSTANCE__OWNER_INPUT,
				oldOwnerInput, ownerInput));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_INSTANCE__OWNER_INPUT:
			if (resolve)
				return getOwnerInput();
			return basicGetOwnerInput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_INSTANCE__OWNER_INPUT:
			setOwnerInput((Input) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_INSTANCE__OWNER_INPUT:
			setOwnerInput((Input) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_INSTANCE__OWNER_INPUT:
			return ownerInput != null;
		}
		return super.eIsSet(featureID);
	}

} // INInstanceImpl
