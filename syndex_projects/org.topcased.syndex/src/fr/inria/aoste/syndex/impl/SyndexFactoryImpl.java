/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexFactoryImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.Array;
import fr.inria.aoste.syndex.Asic;
import fr.inria.aoste.syndex.AssociationComtoProc;
import fr.inria.aoste.syndex.AssociationSWtoHW;
import fr.inria.aoste.syndex.CodeExecutionPhaseKind;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.ConnecteableElement;
import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.DSP;
import fr.inria.aoste.syndex.DataConnection;
import fr.inria.aoste.syndex.DataTypeK;
import fr.inria.aoste.syndex.Datatype;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.FPGA;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.Hardware;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependency;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.RAM;
import fr.inria.aoste.syndex.RepetitionConnection;
import fr.inria.aoste.syndex.RepetitionConnectionKind;
import fr.inria.aoste.syndex.SAM;
import fr.inria.aoste.syndex.SAMPTP;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SynDEx;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.Vector;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class SyndexFactoryImpl extends EFactoryImpl implements SyndexFactory
{
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static SyndexFactory init()
	{
		try
		{
			SyndexFactory theSyndexFactory = (SyndexFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://www.inria.fr/aoste/SynDEx/1.1");
			if (theSyndexFactory != null)
			{
				return theSyndexFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SyndexFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SyndexFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
		case SyndexPackage.ACTUATOR:
			return createActuator();
		case SyndexPackage.SENSOR:
			return createSensor();
		case SyndexPackage.APPLICATION:
			return createApplication();
		case SyndexPackage.INPUT:
			return createInput();
		case SyndexPackage.OUTPUT:
			return createOutput();
		case SyndexPackage.FUNCTION:
			return createFunction();
		case SyndexPackage.CONSTANT:
			return createConstant();
		case SyndexPackage.DELAY:
			return createDelay();
		case SyndexPackage.DATA_CONNECTION:
			return createDataConnection();
		case SyndexPackage.COMPONENT_INSTANCE:
			return createComponentInstance();
		case SyndexPackage.IN_INSTANCE:
			return createINInstance();
		case SyndexPackage.OUT_INSTANCE:
			return createOutInstance();
		case SyndexPackage.INTERNAL_DEPENDENCY_IN:
			return createInternalDependencyIN();
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT:
			return createInternalDependencyOUT();
		case SyndexPackage.CONNECTEABLE_ELEMENT:
			return createConnecteableElement();
		case SyndexPackage.IN_COND_PORT:
			return createInCondPort();
		case SyndexPackage.HARDWARE:
			return createHardware();
		case SyndexPackage.MEDIA:
			return createMedia();
		case SyndexPackage.FPGA:
			return createFPGA();
		case SyndexPackage.ASIC:
			return createAsic();
		case SyndexPackage.DSP:
			return createDSP();
		case SyndexPackage.RAM:
			return createRAM();
		case SyndexPackage.SAM:
			return createSAM();
		case SyndexPackage.SAMPTP:
			return createSAMPTP();
		case SyndexPackage.CONNECTION_HW:
			return createConnectionHW();
		case SyndexPackage.ASSOCIATION_SWTO_HW:
			return createAssociationSWtoHW();
		case SyndexPackage.ASSOCIATION_COMTO_PROC:
			return createAssociationComtoProc();
		case SyndexPackage.DURATION:
			return createDuration();
		case SyndexPackage.ENUM_LIT_DURATION:
			return createEnumLitDuration();
		case SyndexPackage.DATATYPE:
			return createDatatype();
		case SyndexPackage.SIZE:
			return createSize();
		case SyndexPackage.ARRAY:
			return createArray();
		case SyndexPackage.VECTOR:
			return createVector();
		case SyndexPackage.PROCESSOR:
			return createProcessor();
		case SyndexPackage.SYN_DEX:
			return createSynDEx();
		case SyndexPackage.ELEMENTARY_STRUCTURE:
			return createElementaryStructure();
		case SyndexPackage.COMPOUND_STRUCTURE:
			return createCompoundStructure();
		case SyndexPackage.INOUT:
			return createINOUT();
		case SyndexPackage.INTERNAL_DATA_CONNECTION:
			return createInternalDataConnection();
		case SyndexPackage.PARAMETER_DECLARATION:
			return createParameterDeclaration();
		case SyndexPackage.PARAMETER_VALUE:
			return createParameterValue();
		case SyndexPackage.CONDITIONED_STRUCTURE:
			return createConditionedStructure();
		case SyndexPackage.CONDITION:
			return createCondition();
		case SyndexPackage.REPETITION_CONNECTION:
			return createRepetitionConnection();
		case SyndexPackage.INOUT_INSTANCE:
			return createINOUTInstance();
		case SyndexPackage.IN_COND_INSTANCE:
			return createInCondInstance();
		case SyndexPackage.INTERNAL_DEPENDENCY:
			return createInternalDependency();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue)
	{
		switch (eDataType.getClassifierID())
		{
		case SyndexPackage.CODE_EXECUTION_PHASE_KIND:
			return createCodeExecutionPhaseKindFromString(eDataType, initialValue);
		case SyndexPackage.REPETITION_CONNECTION_KIND:
			return createRepetitionConnectionKindFromString(eDataType, initialValue);
		case SyndexPackage.DATA_TYPE_K:
			return createDataTypeKFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue)
	{
		switch (eDataType.getClassifierID())
		{
		case SyndexPackage.CODE_EXECUTION_PHASE_KIND:
			return convertCodeExecutionPhaseKindToString(eDataType, instanceValue);
		case SyndexPackage.REPETITION_CONNECTION_KIND:
			return convertRepetitionConnectionKindToString(eDataType, instanceValue);
		case SyndexPackage.DATA_TYPE_K:
			return convertDataTypeKToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Actuator createActuator()
	{
		ActuatorImpl actuator = new ActuatorImpl();
		return actuator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Sensor createSensor()
	{
		SensorImpl sensor = new SensorImpl();
		return sensor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Application createApplication()
	{
		ApplicationImpl application = new ApplicationImpl();
		return application;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input createInput()
	{
		InputImpl input = new InputImpl();
		return input;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output createOutput()
	{
		OutputImpl output = new OutputImpl();
		return output;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Function createFunction()
	{
		FunctionImpl function = new FunctionImpl();
		return function;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Constant createConstant()
	{
		ConstantImpl constant = new ConstantImpl();
		return constant;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Delay createDelay()
	{
		DelayImpl delay = new DelayImpl();
		return delay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DataConnection createDataConnection()
	{
		DataConnectionImpl dataConnection = new DataConnectionImpl();
		return dataConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComponentInstance createComponentInstance()
	{
		ComponentInstanceImpl componentInstance = new ComponentInstanceImpl();
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INInstance createINInstance()
	{
		INInstanceImpl inInstance = new INInstanceImpl();
		return inInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public OutInstance createOutInstance()
	{
		OutInstanceImpl outInstance = new OutInstanceImpl();
		return outInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalDependencyIN createInternalDependencyIN()
	{
		InternalDependencyINImpl internalDependencyIN = new InternalDependencyINImpl();
		return internalDependencyIN;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalDependencyOUT createInternalDependencyOUT()
	{
		InternalDependencyOUTImpl internalDependencyOUT = new InternalDependencyOUTImpl();
		return internalDependencyOUT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConnecteableElement createConnecteableElement()
	{
		ConnecteableElementImpl connecteableElement = new ConnecteableElementImpl();
		return connecteableElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort createInCondPort()
	{
		InCondPortImpl inCondPort = new InCondPortImpl();
		return inCondPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Hardware createHardware()
	{
		HardwareImpl hardware = new HardwareImpl();
		return hardware;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Media createMedia()
	{
		MediaImpl media = new MediaImpl();
		return media;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public FPGA createFPGA()
	{
		FPGAImpl fpga = new FPGAImpl();
		return fpga;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Asic createAsic()
	{
		AsicImpl asic = new AsicImpl();
		return asic;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DSP createDSP()
	{
		DSPImpl dsp = new DSPImpl();
		return dsp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public RAM createRAM()
	{
		RAMImpl ram = new RAMImpl();
		return ram;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SAM createSAM()
	{
		SAMImpl sam = new SAMImpl();
		return sam;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SAMPTP createSAMPTP()
	{
		SAMPTPImpl samptp = new SAMPTPImpl();
		return samptp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConnectionHW createConnectionHW()
	{
		ConnectionHWImpl connectionHW = new ConnectionHWImpl();
		return connectionHW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AssociationSWtoHW createAssociationSWtoHW()
	{
		AssociationSWtoHWImpl associationSWtoHW = new AssociationSWtoHWImpl();
		return associationSWtoHW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AssociationComtoProc createAssociationComtoProc()
	{
		AssociationComtoProcImpl associationComtoProc = new AssociationComtoProcImpl();
		return associationComtoProc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration createDuration()
	{
		DurationImpl duration = new DurationImpl();
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EnumLitDuration createEnumLitDuration()
	{
		EnumLitDurationImpl enumLitDuration = new EnumLitDurationImpl();
		return enumLitDuration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Datatype createDatatype()
	{
		DatatypeImpl datatype = new DatatypeImpl();
		return datatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size createSize()
	{
		SizeImpl size = new SizeImpl();
		return size;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Array createArray()
	{
		ArrayImpl array = new ArrayImpl();
		return array;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Vector createVector()
	{
		VectorImpl vector = new VectorImpl();
		return vector;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Processor createProcessor()
	{
		ProcessorImpl processor = new ProcessorImpl();
		return processor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SynDEx createSynDEx()
	{
		SynDExImpl synDEx = new SynDExImpl();
		return synDEx;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ElementaryStructure createElementaryStructure()
	{
		ElementaryStructureImpl elementaryStructure = new ElementaryStructureImpl();
		return elementaryStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CompoundStructure createCompoundStructure()
	{
		CompoundStructureImpl compoundStructure = new CompoundStructureImpl();
		return compoundStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUT createINOUT()
	{
		INOUTImpl inout = new INOUTImpl();
		return inout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalDataConnection createInternalDataConnection()
	{
		InternalDataConnectionImpl internalDataConnection = new InternalDataConnectionImpl();
		return internalDataConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ParameterDeclaration createParameterDeclaration()
	{
		ParameterDeclarationImpl parameterDeclaration = new ParameterDeclarationImpl();
		return parameterDeclaration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ParameterValue createParameterValue()
	{
		ParameterValueImpl parameterValue = new ParameterValueImpl();
		return parameterValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConditionedStructure createConditionedStructure()
	{
		ConditionedStructureImpl conditionedStructure = new ConditionedStructureImpl();
		return conditionedStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Condition createCondition()
	{
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public RepetitionConnection createRepetitionConnection()
	{
		RepetitionConnectionImpl repetitionConnection = new RepetitionConnectionImpl();
		return repetitionConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUTInstance createINOUTInstance()
	{
		INOUTInstanceImpl inoutInstance = new INOUTInstanceImpl();
		return inoutInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondInstance createInCondInstance()
	{
		InCondInstanceImpl inCondInstance = new InCondInstanceImpl();
		return inCondInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InternalDependency createInternalDependency()
	{
		InternalDependencyImpl internalDependency = new InternalDependencyImpl();
		return internalDependency;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CodeExecutionPhaseKind createCodeExecutionPhaseKindFromString(EDataType eDataType, String initialValue)
	{
		CodeExecutionPhaseKind result = CodeExecutionPhaseKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertCodeExecutionPhaseKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public RepetitionConnectionKind createRepetitionConnectionKindFromString(EDataType eDataType, String initialValue)
	{
		RepetitionConnectionKind result = RepetitionConnectionKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertRepetitionConnectionKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DataTypeK createDataTypeKFromString(EDataType eDataType, String initialValue)
	{
		DataTypeK result = DataTypeK.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertDataTypeKToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SyndexPackage getSyndexPackage()
	{
		return (SyndexPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SyndexPackage getPackage()
	{
		return SyndexPackage.eINSTANCE;
	}

} // SyndexFactoryImpl
