/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ParameterValueImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Parameter Value</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ParameterValueImpl#getValueParameter <em>Value Parameter</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.ParameterValueImpl#getParamDeclaration <em>Param Declaration</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ParameterValueImpl extends ElementImpl implements ParameterValue
{
	/**
	 * The default value of the '{@link #getValueParameter() <em>Value Parameter</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getValueParameter()
	 * @generated
	 * @ordered
	 */
	protected static final String	VALUE_PARAMETER_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getValueParameter() <em>Value Parameter</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getValueParameter()
	 * @generated
	 * @ordered
	 */
	protected String				valueParameter				= VALUE_PARAMETER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParamDeclaration() <em>Param Declaration</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParamDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ParameterDeclaration	paramDeclaration;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ParameterValueImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getValueParameter()
	{
		return valueParameter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setValueParameter(String newValueParameter)
	{
		String oldValueParameter = valueParameter;
		valueParameter = newValueParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PARAMETER_VALUE__VALUE_PARAMETER,
				oldValueParameter, valueParameter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ParameterDeclaration getParamDeclaration()
	{
		if (paramDeclaration != null && paramDeclaration.eIsProxy())
		{
			InternalEObject oldParamDeclaration = (InternalEObject) paramDeclaration;
			paramDeclaration = (ParameterDeclaration) eResolveProxy(oldParamDeclaration);
			if (paramDeclaration != oldParamDeclaration)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION, oldParamDeclaration, paramDeclaration));
			}
		}
		return paramDeclaration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ParameterDeclaration basicGetParamDeclaration()
	{
		return paramDeclaration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParamDeclaration(ParameterDeclaration newParamDeclaration)
	{
		ParameterDeclaration oldParamDeclaration = paramDeclaration;
		paramDeclaration = newParamDeclaration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION,
				oldParamDeclaration, paramDeclaration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.PARAMETER_VALUE__VALUE_PARAMETER:
			return getValueParameter();
		case SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION:
			if (resolve)
				return getParamDeclaration();
			return basicGetParamDeclaration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.PARAMETER_VALUE__VALUE_PARAMETER:
			setValueParameter((String) newValue);
			return;
		case SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION:
			setParamDeclaration((ParameterDeclaration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PARAMETER_VALUE__VALUE_PARAMETER:
			setValueParameter(VALUE_PARAMETER_EDEFAULT);
			return;
		case SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION:
			setParamDeclaration((ParameterDeclaration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PARAMETER_VALUE__VALUE_PARAMETER:
			return VALUE_PARAMETER_EDEFAULT == null ? valueParameter != null : !VALUE_PARAMETER_EDEFAULT
					.equals(valueParameter);
		case SyndexPackage.PARAMETER_VALUE__PARAM_DECLARATION:
			return paramDeclaration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (valueParameter: ");
		result.append(valueParameter);
		result.append(')');
		return result.toString();
	}

} // ParameterValueImpl
