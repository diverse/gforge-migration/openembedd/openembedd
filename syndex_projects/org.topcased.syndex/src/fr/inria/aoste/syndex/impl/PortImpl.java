/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: PortImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.syndex.Component;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.DataTypeK;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Port</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.PortImpl#getParentPort <em>Parent Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.PortImpl#getSizePort <em>Size Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.PortImpl#getTypePort <em>Type Port</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class PortImpl extends NamedElementImpl implements Port
{
	/**
	 * The cached value of the '{@link #getParentPort() <em>Parent Port</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getParentPort()
	 * @generated
	 * @ordered
	 */
	protected Component					parentPort;

	/**
	 * The cached value of the '{@link #getSizePort() <em>Size Port</em>}' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSizePort()
	 * @generated
	 * @ordered
	 */
	protected Size						sizePort;

	/**
	 * The default value of the '{@link #getTypePort() <em>Type Port</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTypePort()
	 * @generated
	 * @ordered
	 */
	protected static final DataTypeK	TYPE_PORT_EDEFAULT	= DataTypeK.INT;

	/**
	 * The cached value of the '{@link #getTypePort() <em>Type Port</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTypePort()
	 * @generated
	 * @ordered
	 */
	protected DataTypeK					typePort			= TYPE_PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PortImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.PORT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Component getParentPort()
	{
		if (parentPort != null && parentPort.eIsProxy())
		{
			InternalEObject oldParentPort = (InternalEObject) parentPort;
			parentPort = (Component) eResolveProxy(oldParentPort);
			if (parentPort != oldParentPort)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.PORT__PARENT_PORT,
						oldParentPort, parentPort));
			}
		}
		return parentPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Component basicGetParentPort()
	{
		return parentPort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentPort(Component newParentPort)
	{
		Component oldParentPort = parentPort;
		parentPort = newParentPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PORT__PARENT_PORT, oldParentPort,
				parentPort));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Size getSizePort()
	{
		return sizePort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSizePort(Size newSizePort, NotificationChain msgs)
	{
		Size oldSizePort = sizePort;
		sizePort = newSizePort;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.PORT__SIZE_PORT, oldSizePort, newSizePort);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSizePort(Size newSizePort)
	{
		if (newSizePort != sizePort)
		{
			NotificationChain msgs = null;
			if (sizePort != null)
				msgs = ((InternalEObject) sizePort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.PORT__SIZE_PORT, null, msgs);
			if (newSizePort != null)
				msgs = ((InternalEObject) newSizePort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.PORT__SIZE_PORT, null, msgs);
			msgs = basicSetSizePort(newSizePort, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PORT__SIZE_PORT, newSizePort,
				newSizePort));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DataTypeK getTypePort()
	{
		return typePort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTypePort(DataTypeK newTypePort)
	{
		DataTypeK oldTypePort = typePort;
		typePort = newTypePort == null ? TYPE_PORT_EDEFAULT : newTypePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PORT__TYPE_PORT, oldTypePort, typePort));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Condition getParentConditionReference()
	{
		if (eContainerFeatureID != SyndexPackage.PORT__PARENT_CONDITION_REFERENCE)
			return null;
		return (Condition) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CompoundStructure getParentCompoundStructure()
	{
		if (eContainerFeatureID != SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE)
			return null;
		return (CompoundStructure) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public NotificationChain basicSetParentCondition(Condition newParentCondition, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newParentCondition, SyndexPackage.PORT__PARENT_CONDITION_REFERENCE,
			msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public NotificationChain basicSetParentCompoundStructure(CompoundStructure newParentCompoundStructure,
			NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newParentCompoundStructure,
			SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setParentConditionReference(Condition newParentCondition)
	{
		if (newParentCondition != eInternalContainer()
				|| (eContainerFeatureID != SyndexPackage.PORT__PARENT_CONDITION_REFERENCE && newParentCondition != null))
		{
			if (EcoreUtil.isAncestor(this, newParentCondition))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentCondition != null)
				msgs = ((InternalEObject) newParentCondition).eInverseAdd(this,
					SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE, Condition.class, msgs);
			msgs = basicSetParentCondition(newParentCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PORT__PARENT_CONDITION_REFERENCE,
				newParentCondition, newParentCondition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setParentCompoundStructure(CompoundStructure newParentCompoundStructure)
	{
		if (newParentCompoundStructure != eInternalContainer()
				|| (eContainerFeatureID != SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE && newParentCompoundStructure != null))
		{
			if (EcoreUtil.isAncestor(this, newParentCompoundStructure))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentCompoundStructure != null)
				msgs = ((InternalEObject) newParentCompoundStructure).eInverseAdd(this,
					SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE, CompoundStructure.class, msgs);
			msgs = basicSetParentCompoundStructure(newParentCompoundStructure, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE,
				newParentCompoundStructure, newParentCompoundStructure));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentCondition((Condition) otherEnd, msgs);
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentCompoundStructure((CompoundStructure) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__SIZE_PORT:
			return basicSetSizePort(null, msgs);
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			return basicSetParentCondition(null, msgs);
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			return basicSetParentCompoundStructure(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			return eInternalContainer().eInverseRemove(this, SyndexPackage.CONDITION__CONDITION_PORT_REFERENCE,
				Condition.class, msgs);
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			return eInternalContainer().eInverseRemove(this, SyndexPackage.COMPOUND_STRUCTURE__PORT_COMPOUND_STRUCTURE,
				CompoundStructure.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__PARENT_PORT:
			if (resolve)
				return getParentPort();
			return basicGetParentPort();
		case SyndexPackage.PORT__SIZE_PORT:
			return getSizePort();
		case SyndexPackage.PORT__TYPE_PORT:
			return getTypePort();
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			return getParentConditionReference();
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			return getParentCompoundStructure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__PARENT_PORT:
			setParentPort((Component) newValue);
			return;
		case SyndexPackage.PORT__SIZE_PORT:
			setSizePort((Size) newValue);
			return;
		case SyndexPackage.PORT__TYPE_PORT:
			setTypePort((DataTypeK) newValue);
			return;
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			setParentConditionReference((Condition) newValue);
			return;
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			setParentCompoundStructure((CompoundStructure) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__PARENT_PORT:
			setParentPort((Component) null);
			return;
		case SyndexPackage.PORT__SIZE_PORT:
			setSizePort((Size) null);
			return;
		case SyndexPackage.PORT__TYPE_PORT:
			setTypePort(TYPE_PORT_EDEFAULT);
			return;
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			setParentConditionReference((Condition) null);
			return;
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			setParentCompoundStructure((CompoundStructure) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.PORT__PARENT_PORT:
			return parentPort != null;
		case SyndexPackage.PORT__SIZE_PORT:
			return sizePort != null;
		case SyndexPackage.PORT__TYPE_PORT:
			return typePort != TYPE_PORT_EDEFAULT;
		case SyndexPackage.PORT__PARENT_CONDITION_REFERENCE:
			return getParentConditionReference() != null;
		case SyndexPackage.PORT__PARENT_COMPOUND_STRUCTURE:
			return getParentCompoundStructure() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typePort: ");
		result.append(typePort);
		result.append(')');
		return result.toString();
	}

} // PortImpl
