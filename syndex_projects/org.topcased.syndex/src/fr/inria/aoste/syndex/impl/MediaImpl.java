/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: MediaImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Media</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.MediaImpl#getBandwidth <em>Bandwidth</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.MediaImpl#getSize <em>Size</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.MediaImpl#getParentMemory <em>Parent Memory</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.MediaImpl#isBroadcast <em>Broadcast</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.MediaImpl#getDuration <em>Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MediaImpl extends HardwareComponentImpl implements Media
{
	/**
	 * The default value of the '{@link #getBandwidth() <em>Bandwidth</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getBandwidth()
	 * @generated
	 * @ordered
	 */
	protected static final int		BANDWIDTH_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getBandwidth() <em>Bandwidth</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getBandwidth()
	 * @generated
	 * @ordered
	 */
	protected int					bandwidth			= BANDWIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final int		SIZE_EDEFAULT		= 0;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected int					size				= SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParentMemory() <em>Parent Memory</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getParentMemory()
	 * @generated
	 * @ordered
	 */
	protected Media					parentMemory;

	/**
	 * The default value of the '{@link #isBroadcast() <em>Broadcast</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #isBroadcast()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	BROADCAST_EDEFAULT	= false;

	/**
	 * The cached value of the '{@link #isBroadcast() <em>Broadcast</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #isBroadcast()
	 * @generated
	 * @ordered
	 */
	protected boolean				broadcast			= BROADCAST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Duration				duration;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected MediaImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.MEDIA;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getBandwidth()
	{
		return bandwidth;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBandwidth(int newBandwidth)
	{
		int oldBandwidth = bandwidth;
		bandwidth = newBandwidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.MEDIA__BANDWIDTH, oldBandwidth,
				bandwidth));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSize(int newSize)
	{
		int oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.MEDIA__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Media getParentMemory()
	{
		if (parentMemory != null && parentMemory.eIsProxy())
		{
			InternalEObject oldParentMemory = (InternalEObject) parentMemory;
			parentMemory = (Media) eResolveProxy(oldParentMemory);
			if (parentMemory != oldParentMemory)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.MEDIA__PARENT_MEMORY,
						oldParentMemory, parentMemory));
			}
		}
		return parentMemory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Media basicGetParentMemory()
	{
		return parentMemory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentMemory(Media newParentMemory)
	{
		Media oldParentMemory = parentMemory;
		parentMemory = newParentMemory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.MEDIA__PARENT_MEMORY, oldParentMemory,
				parentMemory));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean isBroadcast()
	{
		return broadcast;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBroadcast(boolean newBroadcast)
	{
		boolean oldBroadcast = broadcast;
		broadcast = newBroadcast;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.MEDIA__BROADCAST, oldBroadcast,
				broadcast));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration getDuration()
	{
		if (duration != null && duration.eIsProxy())
		{
			InternalEObject oldDuration = (InternalEObject) duration;
			duration = (Duration) eResolveProxy(oldDuration);
			if (duration != oldDuration)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SyndexPackage.MEDIA__DURATION,
						oldDuration, duration));
			}
		}
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration basicGetDuration()
	{
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDuration(Duration newDuration)
	{
		Duration oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.MEDIA__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.MEDIA__BANDWIDTH:
			return new Integer(getBandwidth());
		case SyndexPackage.MEDIA__SIZE:
			return new Integer(getSize());
		case SyndexPackage.MEDIA__PARENT_MEMORY:
			if (resolve)
				return getParentMemory();
			return basicGetParentMemory();
		case SyndexPackage.MEDIA__BROADCAST:
			return isBroadcast() ? Boolean.TRUE : Boolean.FALSE;
		case SyndexPackage.MEDIA__DURATION:
			if (resolve)
				return getDuration();
			return basicGetDuration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.MEDIA__BANDWIDTH:
			setBandwidth(((Integer) newValue).intValue());
			return;
		case SyndexPackage.MEDIA__SIZE:
			setSize(((Integer) newValue).intValue());
			return;
		case SyndexPackage.MEDIA__PARENT_MEMORY:
			setParentMemory((Media) newValue);
			return;
		case SyndexPackage.MEDIA__BROADCAST:
			setBroadcast(((Boolean) newValue).booleanValue());
			return;
		case SyndexPackage.MEDIA__DURATION:
			setDuration((Duration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.MEDIA__BANDWIDTH:
			setBandwidth(BANDWIDTH_EDEFAULT);
			return;
		case SyndexPackage.MEDIA__SIZE:
			setSize(SIZE_EDEFAULT);
			return;
		case SyndexPackage.MEDIA__PARENT_MEMORY:
			setParentMemory((Media) null);
			return;
		case SyndexPackage.MEDIA__BROADCAST:
			setBroadcast(BROADCAST_EDEFAULT);
			return;
		case SyndexPackage.MEDIA__DURATION:
			setDuration((Duration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.MEDIA__BANDWIDTH:
			return bandwidth != BANDWIDTH_EDEFAULT;
		case SyndexPackage.MEDIA__SIZE:
			return size != SIZE_EDEFAULT;
		case SyndexPackage.MEDIA__PARENT_MEMORY:
			return parentMemory != null;
		case SyndexPackage.MEDIA__BROADCAST:
			return broadcast != BROADCAST_EDEFAULT;
		case SyndexPackage.MEDIA__DURATION:
			return duration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bandwidth: ");
		result.append(bandwidth);
		result.append(", size: ");
		result.append(size);
		result.append(", broadcast: ");
		result.append(broadcast);
		result.append(')');
		return result.toString();
	}

} // MediaImpl
