/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: HWConnectionImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.syndex.HWConnection;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>HW Connection</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.HWConnectionImpl#getParentHWConnection <em>Parent HW Connection</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class HWConnectionImpl extends ConnectionImpl implements HWConnection
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected HWConnectionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.HW_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HardwareComponent getParentHWConnection()
	{
		if (eContainerFeatureID != SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION)
			return null;
		return (HardwareComponent) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetParentHWConnection(HardwareComponent newParentHWConnection, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newParentHWConnection,
			SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentHWConnection(HardwareComponent newParentHWConnection)
	{
		if (newParentHWConnection != eInternalContainer()
				|| (eContainerFeatureID != SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION && newParentHWConnection != null))
		{
			if (EcoreUtil.isAncestor(this, newParentHWConnection))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentHWConnection != null)
				msgs = ((InternalEObject) newParentHWConnection).eInverseAdd(this,
					SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION, HardwareComponent.class, msgs);
			msgs = basicSetParentHWConnection(newParentHWConnection, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION,
				newParentHWConnection, newParentHWConnection));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentHWConnection((HardwareComponent) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			return basicSetParentHWConnection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			return eInternalContainer().eInverseRemove(this, SyndexPackage.HARDWARE_COMPONENT__HW_CONNECTION,
				HardwareComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			return getParentHWConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			setParentHWConnection((HardwareComponent) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			setParentHWConnection((HardwareComponent) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HW_CONNECTION__PARENT_HW_CONNECTION:
			return getParentHWConnection() != null;
		}
		return super.eIsSet(featureID);
	}

} // HWConnectionImpl
