/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: EnumLitDurationImpl.java,v 1.1 2008-11-18 10:44:02 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Enum Lit Duration</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.EnumLitDurationImpl#getValue <em>Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.EnumLitDurationImpl#getParentEnumDuration <em>Parent Enum Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class EnumLitDurationImpl extends NamedElementImpl implements EnumLitDuration
{
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final int	VALUE_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected int				value			= VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EnumLitDurationImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.ENUM_LIT_DURATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue()
	{
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setValue(int newValue)
	{
		int oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.ENUM_LIT_DURATION__VALUE, oldValue,
				value));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Duration getParentEnumDuration()
	{
		if (eContainerFeatureID != SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION)
			return null;
		return (Duration) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetParentEnumDuration(Duration newParentEnumDuration, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newParentEnumDuration,
			SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentEnumDuration(Duration newParentEnumDuration)
	{
		if (newParentEnumDuration != eInternalContainer()
				|| (eContainerFeatureID != SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION && newParentEnumDuration != null))
		{
			if (EcoreUtil.isAncestor(this, newParentEnumDuration))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentEnumDuration != null)
				msgs = ((InternalEObject) newParentEnumDuration).eInverseAdd(this,
					SyndexPackage.DURATION__ENUM_DURATION, Duration.class, msgs);
			msgs = basicSetParentEnumDuration(newParentEnumDuration, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION, newParentEnumDuration, newParentEnumDuration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentEnumDuration((Duration) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			return basicSetParentEnumDuration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			return eInternalContainer().eInverseRemove(this, SyndexPackage.DURATION__ENUM_DURATION, Duration.class,
				msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__VALUE:
			return new Integer(getValue());
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			return getParentEnumDuration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__VALUE:
			setValue(((Integer) newValue).intValue());
			return;
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			setParentEnumDuration((Duration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			setParentEnumDuration((Duration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ENUM_LIT_DURATION__VALUE:
			return value != VALUE_EDEFAULT;
		case SyndexPackage.ENUM_LIT_DURATION__PARENT_ENUM_DURATION:
			return getParentEnumDuration() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} // EnumLitDurationImpl
