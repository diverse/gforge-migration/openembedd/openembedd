/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AtomicApplicationComponentImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.AtomicApplicationComponent;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Atomic Application Component</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.AtomicApplicationComponentImpl#getAtomicApplicationStructure <em>Atomic
 * Application Structure</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class AtomicApplicationComponentImpl extends ApplicationComponentImpl implements
		AtomicApplicationComponent
{
	/**
	 * The cached value of the '{@link #getAtomicApplicationStructure() <em>Atomic Application Structure</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAtomicApplicationStructure()
	 * @generated
	 * @ordered
	 */
	protected ElementaryStructure	atomicApplicationStructure;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AtomicApplicationComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.ATOMIC_APPLICATION_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ElementaryStructure getAtomicApplicationStructure()
	{
		return atomicApplicationStructure;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetAtomicApplicationStructure(ElementaryStructure newAtomicApplicationStructure,
			NotificationChain msgs)
	{
		ElementaryStructure oldAtomicApplicationStructure = atomicApplicationStructure;
		atomicApplicationStructure = newAtomicApplicationStructure;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE,
				oldAtomicApplicationStructure, newAtomicApplicationStructure);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setAtomicApplicationStructure(ElementaryStructure newAtomicApplicationStructure)
	{
		if (newAtomicApplicationStructure != atomicApplicationStructure)
		{
			NotificationChain msgs = null;
			if (atomicApplicationStructure != null)
				msgs = ((InternalEObject) atomicApplicationStructure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE, null, msgs);
			if (newAtomicApplicationStructure != null)
				msgs = ((InternalEObject) newAtomicApplicationStructure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE, null, msgs);
			msgs = basicSetAtomicApplicationStructure(newAtomicApplicationStructure, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE,
				newAtomicApplicationStructure, newAtomicApplicationStructure));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE:
			return basicSetAtomicApplicationStructure(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE:
			return getAtomicApplicationStructure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE:
			setAtomicApplicationStructure((ElementaryStructure) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE:
			setAtomicApplicationStructure((ElementaryStructure) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT__ATOMIC_APPLICATION_STRUCTURE:
			return atomicApplicationStructure != null;
		}
		return super.eIsSet(featureID);
	}

} // AtomicApplicationComponentImpl
