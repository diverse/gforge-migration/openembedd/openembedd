/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: HardwareImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.Hardware;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Hardware</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.HardwareImpl#getComponentHWInstance <em>Component HW Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.HardwareImpl#isMain <em>Main</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class HardwareImpl extends HardwareComponentImpl implements Hardware
{
	/**
	 * The cached value of the '{@link #getComponentHWInstance() <em>Component HW Instance</em>}' containment reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getComponentHWInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentInstance>	componentHWInstance;

	/**
	 * The default value of the '{@link #isMain() <em>Main</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isMain()
	 * @generated
	 * @ordered
	 */
	protected static final boolean		MAIN_EDEFAULT	= false;

	/**
	 * The cached value of the '{@link #isMain() <em>Main</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #isMain()
	 * @generated
	 * @ordered
	 */
	protected boolean					main			= MAIN_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected HardwareImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.HARDWARE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ComponentInstance> getComponentHWInstance()
	{
		if (componentHWInstance == null)
		{
			componentHWInstance = new EObjectContainmentEList<ComponentInstance>(ComponentInstance.class, this,
				SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE);
		}
		return componentHWInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean isMain()
	{
		return main;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMain(boolean newMain)
	{
		boolean oldMain = main;
		main = newMain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.HARDWARE__MAIN, oldMain, main));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE:
			return ((InternalEList<?>) getComponentHWInstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE:
			return getComponentHWInstance();
		case SyndexPackage.HARDWARE__MAIN:
			return isMain() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE:
			getComponentHWInstance().clear();
			getComponentHWInstance().addAll((Collection<? extends ComponentInstance>) newValue);
			return;
		case SyndexPackage.HARDWARE__MAIN:
			setMain(((Boolean) newValue).booleanValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE:
			getComponentHWInstance().clear();
			return;
		case SyndexPackage.HARDWARE__MAIN:
			setMain(MAIN_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.HARDWARE__COMPONENT_HW_INSTANCE:
			return componentHWInstance != null && !componentHWInstance.isEmpty();
		case SyndexPackage.HARDWARE__MAIN:
			return main != MAIN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (main: ");
		result.append(main);
		result.append(')');
		return result.toString();
	}

} // HardwareImpl
