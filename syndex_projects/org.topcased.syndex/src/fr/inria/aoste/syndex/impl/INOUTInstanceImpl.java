/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: INOUTInstanceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>INOUT Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.INOUTInstanceImpl#getOwnerINOUT <em>Owner INOUT</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class INOUTInstanceImpl extends PortInstanceImpl implements INOUTInstance
{
	/**
	 * The cached value of the '{@link #getOwnerINOUT() <em>Owner INOUT</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOwnerINOUT()
	 * @generated
	 * @ordered
	 */
	protected INOUT	ownerINOUT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected INOUTInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INOUT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUT getOwnerINOUT()
	{
		if (ownerINOUT != null && ownerINOUT.eIsProxy())
		{
			InternalEObject oldOwnerINOUT = (InternalEObject) ownerINOUT;
			ownerINOUT = (INOUT) eResolveProxy(oldOwnerINOUT);
			if (ownerINOUT != oldOwnerINOUT)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INOUT_INSTANCE__OWNER_INOUT, oldOwnerINOUT, ownerINOUT));
			}
		}
		return ownerINOUT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INOUT basicGetOwnerINOUT()
	{
		return ownerINOUT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerINOUT(INOUT newOwnerINOUT)
	{
		INOUT oldOwnerINOUT = ownerINOUT;
		ownerINOUT = newOwnerINOUT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INOUT_INSTANCE__OWNER_INOUT,
				oldOwnerINOUT, ownerINOUT));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.INOUT_INSTANCE__OWNER_INOUT:
			if (resolve)
				return getOwnerINOUT();
			return basicGetOwnerINOUT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.INOUT_INSTANCE__OWNER_INOUT:
			setOwnerINOUT((INOUT) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INOUT_INSTANCE__OWNER_INOUT:
			setOwnerINOUT((INOUT) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INOUT_INSTANCE__OWNER_INOUT:
			return ownerINOUT != null;
		}
		return super.eIsSet(featureID);
	}

} // INOUTInstanceImpl
