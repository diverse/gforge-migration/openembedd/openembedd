/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ComponentImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import fr.inria.aoste.syndex.CodeExecutionPhaseKind;
import fr.inria.aoste.syndex.Component;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.ComponentImpl#getExecutionPhases <em>Execution Phases</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ComponentImpl extends NamedElementImpl implements Component
{
	/**
	 * The cached value of the '{@link #getExecutionPhases() <em>Execution Phases</em>}' attribute list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getExecutionPhases()
	 * @generated
	 * @ordered
	 */
	protected EList<CodeExecutionPhaseKind>	executionPhases;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<CodeExecutionPhaseKind> getExecutionPhases()
	{
		if (executionPhases == null)
		{
			executionPhases = new EDataTypeUniqueEList<CodeExecutionPhaseKind>(CodeExecutionPhaseKind.class, this,
				SyndexPackage.COMPONENT__EXECUTION_PHASES);
		}
		return executionPhases;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT__EXECUTION_PHASES:
			return getExecutionPhases();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT__EXECUTION_PHASES:
			getExecutionPhases().clear();
			getExecutionPhases().addAll((Collection<? extends CodeExecutionPhaseKind>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT__EXECUTION_PHASES:
			getExecutionPhases().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.COMPONENT__EXECUTION_PHASES:
			return executionPhases != null && !executionPhases.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executionPhases: ");
		result.append(executionPhases);
		result.append(')');
		return result.toString();
	}

} // ComponentImpl
