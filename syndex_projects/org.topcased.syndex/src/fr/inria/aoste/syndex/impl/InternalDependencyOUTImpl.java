/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependencyOUTImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Internal Dependency OUT</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl#getSourceIDO <em>Source IDO</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyOUTImpl#getTargetIDO <em>Target IDO</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InternalDependencyOUTImpl extends ApplicationConnectionImpl implements InternalDependencyOUT
{
	/**
	 * The cached value of the '{@link #getSourceIDO() <em>Source IDO</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceIDO()
	 * @generated
	 * @ordered
	 */
	protected OutInstance	sourceIDO;

	/**
	 * The cached value of the '{@link #getTargetIDO() <em>Target IDO</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetIDO()
	 * @generated
	 * @ordered
	 */
	protected Output		targetIDO;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InternalDependencyOUTImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INTERNAL_DEPENDENCY_OUT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public OutInstance getSourceIDO()
	{
		if (sourceIDO != null && sourceIDO.eIsProxy())
		{
			InternalEObject oldSourceIDO = (InternalEObject) sourceIDO;
			sourceIDO = (OutInstance) eResolveProxy(oldSourceIDO);
			if (sourceIDO != oldSourceIDO)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO, oldSourceIDO, sourceIDO));
			}
		}
		return sourceIDO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public OutInstance basicGetSourceIDO()
	{
		return sourceIDO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceIDO(OutInstance newSourceIDO)
	{
		OutInstance oldSourceIDO = sourceIDO;
		sourceIDO = newSourceIDO;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO,
				oldSourceIDO, sourceIDO));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output getTargetIDO()
	{
		if (targetIDO != null && targetIDO.eIsProxy())
		{
			InternalEObject oldTargetIDO = (InternalEObject) targetIDO;
			targetIDO = (Output) eResolveProxy(oldTargetIDO);
			if (targetIDO != oldTargetIDO)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO, oldTargetIDO, targetIDO));
			}
		}
		return targetIDO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Output basicGetTargetIDO()
	{
		return targetIDO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetIDO(Output newTargetIDO)
	{
		Output oldTargetIDO = targetIDO;
		targetIDO = newTargetIDO;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO,
				oldTargetIDO, targetIDO));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO:
			if (resolve)
				return getSourceIDO();
			return basicGetSourceIDO();
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO:
			if (resolve)
				return getTargetIDO();
			return basicGetTargetIDO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO:
			setSourceIDO((OutInstance) newValue);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO:
			setTargetIDO((Output) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO:
			setSourceIDO((OutInstance) null);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO:
			setTargetIDO((Output) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__SOURCE_IDO:
			return sourceIDO != null;
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT__TARGET_IDO:
			return targetIDO != null;
		}
		return super.eIsSet(featureID);
	}

} // InternalDependencyOUTImpl
