/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependencyINImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Internal Dependency IN</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl#getSourceIDI <em>Source IDI</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl#getTargetIDI <em>Target IDI</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl#getSourceIDIC <em>Source IDIC</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.impl.InternalDependencyINImpl#getTargetIDIC <em>Target IDIC</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InternalDependencyINImpl extends ApplicationConnectionImpl implements InternalDependencyIN
{
	/**
	 * The cached value of the '{@link #getSourceIDI() <em>Source IDI</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceIDI()
	 * @generated
	 * @ordered
	 */
	protected Input				sourceIDI;

	/**
	 * The cached value of the '{@link #getTargetIDI() <em>Target IDI</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetIDI()
	 * @generated
	 * @ordered
	 */
	protected INInstance		targetIDI;

	/**
	 * The cached value of the '{@link #getSourceIDIC() <em>Source IDIC</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSourceIDIC()
	 * @generated
	 * @ordered
	 */
	protected InCondPort		sourceIDIC;

	/**
	 * The cached value of the '{@link #getTargetIDIC() <em>Target IDIC</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTargetIDIC()
	 * @generated
	 * @ordered
	 */
	protected InCondInstance	targetIDIC;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InternalDependencyINImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.INTERNAL_DEPENDENCY_IN;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input getSourceIDI()
	{
		if (sourceIDI != null && sourceIDI.eIsProxy())
		{
			InternalEObject oldSourceIDI = (InternalEObject) sourceIDI;
			sourceIDI = (Input) eResolveProxy(oldSourceIDI);
			if (sourceIDI != oldSourceIDI)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI, oldSourceIDI, sourceIDI));
			}
		}
		return sourceIDI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Input basicGetSourceIDI()
	{
		return sourceIDI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceIDI(Input newSourceIDI)
	{
		Input oldSourceIDI = sourceIDI;
		sourceIDI = newSourceIDI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI,
				oldSourceIDI, sourceIDI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INInstance getTargetIDI()
	{
		if (targetIDI != null && targetIDI.eIsProxy())
		{
			InternalEObject oldTargetIDI = (InternalEObject) targetIDI;
			targetIDI = (INInstance) eResolveProxy(oldTargetIDI);
			if (targetIDI != oldTargetIDI)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI, oldTargetIDI, targetIDI));
			}
		}
		return targetIDI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public INInstance basicGetTargetIDI()
	{
		return targetIDI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetIDI(INInstance newTargetIDI)
	{
		INInstance oldTargetIDI = targetIDI;
		targetIDI = newTargetIDI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI,
				oldTargetIDI, targetIDI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort getSourceIDIC()
	{
		if (sourceIDIC != null && sourceIDIC.eIsProxy())
		{
			InternalEObject oldSourceIDIC = (InternalEObject) sourceIDIC;
			sourceIDIC = (InCondPort) eResolveProxy(oldSourceIDIC);
			if (sourceIDIC != oldSourceIDIC)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC, oldSourceIDIC, sourceIDIC));
			}
		}
		return sourceIDIC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort basicGetSourceIDIC()
	{
		return sourceIDIC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceIDIC(InCondPort newSourceIDIC)
	{
		InCondPort oldSourceIDIC = sourceIDIC;
		sourceIDIC = newSourceIDIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC,
				oldSourceIDIC, sourceIDIC));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondInstance getTargetIDIC()
	{
		if (targetIDIC != null && targetIDIC.eIsProxy())
		{
			InternalEObject oldTargetIDIC = (InternalEObject) targetIDIC;
			targetIDIC = (InCondInstance) eResolveProxy(oldTargetIDIC);
			if (targetIDIC != oldTargetIDIC)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC, oldTargetIDIC, targetIDIC));
			}
		}
		return targetIDIC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondInstance basicGetTargetIDIC()
	{
		return targetIDIC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTargetIDIC(InCondInstance newTargetIDIC)
	{
		InCondInstance oldTargetIDIC = targetIDIC;
		targetIDIC = newTargetIDIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC,
				oldTargetIDIC, targetIDIC));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI:
			if (resolve)
				return getSourceIDI();
			return basicGetSourceIDI();
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI:
			if (resolve)
				return getTargetIDI();
			return basicGetTargetIDI();
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC:
			if (resolve)
				return getSourceIDIC();
			return basicGetSourceIDIC();
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC:
			if (resolve)
				return getTargetIDIC();
			return basicGetTargetIDIC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI:
			setSourceIDI((Input) newValue);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI:
			setTargetIDI((INInstance) newValue);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC:
			setSourceIDIC((InCondPort) newValue);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC:
			setTargetIDIC((InCondInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI:
			setSourceIDI((Input) null);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI:
			setTargetIDI((INInstance) null);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC:
			setSourceIDIC((InCondPort) null);
			return;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC:
			setTargetIDIC((InCondInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDI:
			return sourceIDI != null;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDI:
			return targetIDI != null;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__SOURCE_IDIC:
			return sourceIDIC != null;
		case SyndexPackage.INTERNAL_DEPENDENCY_IN__TARGET_IDIC:
			return targetIDIC != null;
		}
		return super.eIsSet(featureID);
	}

} // InternalDependencyINImpl
