/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InCondInstanceImpl.java,v 1.1 2008-11-18 10:44:02 cbrunett Exp $
 */
package fr.inria.aoste.syndex.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.SyndexPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>In Cond Instance</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.impl.InCondInstanceImpl#getOwnerCond <em>Owner Cond</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InCondInstanceImpl extends PortInstanceImpl implements InCondInstance
{
	/**
	 * The cached value of the '{@link #getOwnerCond() <em>Owner Cond</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOwnerCond()
	 * @generated
	 * @ordered
	 */
	protected InCondPort	ownerCond;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InCondInstanceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SyndexPackage.Literals.IN_COND_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort getOwnerCond()
	{
		if (ownerCond != null && ownerCond.eIsProxy())
		{
			InternalEObject oldOwnerCond = (InternalEObject) ownerCond;
			ownerCond = (InCondPort) eResolveProxy(oldOwnerCond);
			if (ownerCond != oldOwnerCond)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
						SyndexPackage.IN_COND_INSTANCE__OWNER_COND, oldOwnerCond, ownerCond));
			}
		}
		return ownerCond;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public InCondPort basicGetOwnerCond()
	{
		return ownerCond;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOwnerCond(InCondPort newOwnerCond)
	{
		InCondPort oldOwnerCond = ownerCond;
		ownerCond = newOwnerCond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyndexPackage.IN_COND_INSTANCE__OWNER_COND,
				oldOwnerCond, ownerCond));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_COND_INSTANCE__OWNER_COND:
			if (resolve)
				return getOwnerCond();
			return basicGetOwnerCond();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_COND_INSTANCE__OWNER_COND:
			setOwnerCond((InCondPort) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_COND_INSTANCE__OWNER_COND:
			setOwnerCond((InCondPort) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case SyndexPackage.IN_COND_INSTANCE__OWNER_COND:
			return ownerCond != null;
		}
		return super.eIsSet(featureID);
	}

} // InCondInstanceImpl
