/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: HardwareComponent.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Hardware Component</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.HardwareComponent#getHWConnection <em>HW Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.HardwareComponent#getInOutHWComponent <em>In Out HW Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getHardwareComponent()
 * @model abstract="true"
 * @generated
 */
public interface HardwareComponent extends Component
{
	/**
	 * Returns the value of the '<em><b>HW Connection</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.HWConnection}. It is bidirectional and its opposite is '
	 * {@link fr.inria.aoste.syndex.HWConnection#getParentHWConnection <em>Parent HW Connection</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>HW Connection</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>HW Connection</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getHardwareComponent_HWConnection()
	 * @see fr.inria.aoste.syndex.HWConnection#getParentHWConnection
	 * @model opposite="parentHWConnection" containment="true"
	 * @generated
	 */
	EList<HWConnection> getHWConnection();

	/**
	 * Returns the value of the '<em><b>In Out HW Component</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.INOUT}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Out HW Component</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>In Out HW Component</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getHardwareComponent_InOutHWComponent()
	 * @model containment="true"
	 * @generated
	 */
	EList<INOUT> getInOutHWComponent();

} // HardwareComponent
