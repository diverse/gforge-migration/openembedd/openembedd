/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDataConnection.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Internal Data Connection</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InternalDataConnection#getSourceOutInstance <em>Source Out Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstance <em>Target IN Instance</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstanceC <em>Target IN Instance C</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDataConnection()
 * @model
 * @generated
 */
public interface InternalDataConnection extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Source Out Instance</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Out Instance</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Out Instance</em>' reference.
	 * @see #setSourceOutInstance(OutInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDataConnection_SourceOutInstance()
	 * @model required="true"
	 * @generated
	 */
	OutInstance getSourceOutInstance();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDataConnection#getSourceOutInstance
	 * <em>Source Out Instance</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Out Instance</em>' reference.
	 * @see #getSourceOutInstance()
	 * @generated
	 */
	void setSourceOutInstance(OutInstance value);

	/**
	 * Returns the value of the '<em><b>Target IN Instance</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target IN Instance</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target IN Instance</em>' reference.
	 * @see #setTargetINInstance(INInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDataConnection_TargetINInstance()
	 * @model required="true"
	 * @generated
	 */
	INInstance getTargetINInstance();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstance
	 * <em>Target IN Instance</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target IN Instance</em>' reference.
	 * @see #getTargetINInstance()
	 * @generated
	 */
	void setTargetINInstance(INInstance value);

	/**
	 * Returns the value of the '<em><b>Target IN Instance C</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target IN Instance C</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target IN Instance C</em>' reference.
	 * @see #setTargetINInstanceC(InCondInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDataConnection_TargetINInstanceC()
	 * @model required="true"
	 * @generated
	 */
	InCondInstance getTargetINInstanceC();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDataConnection#getTargetINInstanceC
	 * <em>Target IN Instance C</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target IN Instance C</em>' reference.
	 * @see #getTargetINInstanceC()
	 * @generated
	 */
	void setTargetINInstanceC(InCondInstance value);

} // InternalDataConnection
