/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Function.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Function</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Function#getInputPortFunction <em>Input Port Function</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Function#getOutputPortFunction <em>Output Port Function</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getFunction()
 * @model
 * @generated
 */
public interface Function extends AtomicApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>Input Port Function</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.Input}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Port Function</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Input Port Function</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getFunction_InputPortFunction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Input> getInputPortFunction();

	/**
	 * Returns the value of the '<em><b>Output Port Function</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.Output}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Port Function</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Output Port Function</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getFunction_OutputPortFunction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Output> getOutputPortFunction();

} // Function
