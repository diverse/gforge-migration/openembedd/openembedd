/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexSwitch.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.Array;
import fr.inria.aoste.syndex.Asic;
import fr.inria.aoste.syndex.AssociationComtoProc;
import fr.inria.aoste.syndex.AssociationSWtoHW;
import fr.inria.aoste.syndex.AtomicApplicationComponent;
import fr.inria.aoste.syndex.Caracteristics;
import fr.inria.aoste.syndex.Component;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.ConnecteableElement;
import fr.inria.aoste.syndex.Connection;
import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.DSP;
import fr.inria.aoste.syndex.DataConnection;
import fr.inria.aoste.syndex.Datatype;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Element;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.FPGA;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.HWConnection;
import fr.inria.aoste.syndex.Hardware;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Instance;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependency;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.NamedElement;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.PortInstance;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.RAM;
import fr.inria.aoste.syndex.RepetitionConnection;
import fr.inria.aoste.syndex.SAM;
import fr.inria.aoste.syndex.SAMPTP;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SynDEx;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.Vector;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance hierarchy. It supports the call
 * {@link #doSwitch(EObject) doSwitch(object)} to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the inheritance hierarchy until a non-null result is
 * returned, which is the result of the switch. <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage
 * @generated
 */
public class SyndexSwitch<T>
{
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static SyndexPackage	modelPackage;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SyndexSwitch()
	{
		if (modelPackage == null)
		{
			modelPackage = SyndexPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject)
	{
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject)
	{
		if (theEClass.eContainer() == modelPackage)
		{
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else
		{
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject)
	{
		switch (classifierID)
		{
		case SyndexPackage.ACTUATOR:
		{
			Actuator actuator = (Actuator) theEObject;
			T result = caseActuator(actuator);
			if (result == null)
				result = caseAtomicApplicationComponent(actuator);
			if (result == null)
				result = caseApplicationComponent(actuator);
			if (result == null)
				result = caseComponent(actuator);
			if (result == null)
				result = caseNamedElement(actuator);
			if (result == null)
				result = caseElement(actuator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ATOMIC_APPLICATION_COMPONENT:
		{
			AtomicApplicationComponent atomicApplicationComponent = (AtomicApplicationComponent) theEObject;
			T result = caseAtomicApplicationComponent(atomicApplicationComponent);
			if (result == null)
				result = caseApplicationComponent(atomicApplicationComponent);
			if (result == null)
				result = caseComponent(atomicApplicationComponent);
			if (result == null)
				result = caseNamedElement(atomicApplicationComponent);
			if (result == null)
				result = caseElement(atomicApplicationComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.PORT:
		{
			Port port = (Port) theEObject;
			T result = casePort(port);
			if (result == null)
				result = caseNamedElement(port);
			if (result == null)
				result = caseConnecteableElement(port);
			if (result == null)
				result = caseElement(port);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.NAMED_ELEMENT:
		{
			NamedElement namedElement = (NamedElement) theEObject;
			T result = caseNamedElement(namedElement);
			if (result == null)
				result = caseElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.APPLICATION_COMPONENT:
		{
			ApplicationComponent applicationComponent = (ApplicationComponent) theEObject;
			T result = caseApplicationComponent(applicationComponent);
			if (result == null)
				result = caseComponent(applicationComponent);
			if (result == null)
				result = caseNamedElement(applicationComponent);
			if (result == null)
				result = caseElement(applicationComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.SENSOR:
		{
			Sensor sensor = (Sensor) theEObject;
			T result = caseSensor(sensor);
			if (result == null)
				result = caseAtomicApplicationComponent(sensor);
			if (result == null)
				result = caseApplicationComponent(sensor);
			if (result == null)
				result = caseComponent(sensor);
			if (result == null)
				result = caseNamedElement(sensor);
			if (result == null)
				result = caseElement(sensor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.APPLICATION:
		{
			Application application = (Application) theEObject;
			T result = caseApplication(application);
			if (result == null)
				result = caseApplicationComponent(application);
			if (result == null)
				result = caseComponent(application);
			if (result == null)
				result = caseNamedElement(application);
			if (result == null)
				result = caseElement(application);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INPUT:
		{
			Input input = (Input) theEObject;
			T result = caseInput(input);
			if (result == null)
				result = casePort(input);
			if (result == null)
				result = caseNamedElement(input);
			if (result == null)
				result = caseConnecteableElement(input);
			if (result == null)
				result = caseElement(input);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.COMPONENT:
		{
			Component component = (Component) theEObject;
			T result = caseComponent(component);
			if (result == null)
				result = caseNamedElement(component);
			if (result == null)
				result = caseElement(component);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.OUTPUT:
		{
			Output output = (Output) theEObject;
			T result = caseOutput(output);
			if (result == null)
				result = casePort(output);
			if (result == null)
				result = caseNamedElement(output);
			if (result == null)
				result = caseConnecteableElement(output);
			if (result == null)
				result = caseElement(output);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.FUNCTION:
		{
			Function function = (Function) theEObject;
			T result = caseFunction(function);
			if (result == null)
				result = caseAtomicApplicationComponent(function);
			if (result == null)
				result = caseApplicationComponent(function);
			if (result == null)
				result = caseComponent(function);
			if (result == null)
				result = caseNamedElement(function);
			if (result == null)
				result = caseElement(function);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONSTANT:
		{
			Constant constant = (Constant) theEObject;
			T result = caseConstant(constant);
			if (result == null)
				result = caseAtomicApplicationComponent(constant);
			if (result == null)
				result = caseApplicationComponent(constant);
			if (result == null)
				result = caseComponent(constant);
			if (result == null)
				result = caseNamedElement(constant);
			if (result == null)
				result = caseElement(constant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.DELAY:
		{
			Delay delay = (Delay) theEObject;
			T result = caseDelay(delay);
			if (result == null)
				result = caseAtomicApplicationComponent(delay);
			if (result == null)
				result = caseApplicationComponent(delay);
			if (result == null)
				result = caseComponent(delay);
			if (result == null)
				result = caseNamedElement(delay);
			if (result == null)
				result = caseElement(delay);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONNECTION:
		{
			Connection connection = (Connection) theEObject;
			T result = caseConnection(connection);
			if (result == null)
				result = caseElement(connection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.DATA_CONNECTION:
		{
			DataConnection dataConnection = (DataConnection) theEObject;
			T result = caseDataConnection(dataConnection);
			if (result == null)
				result = caseApplicationConnection(dataConnection);
			if (result == null)
				result = caseConnection(dataConnection);
			if (result == null)
				result = caseElement(dataConnection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INSTANCE:
		{
			Instance instance = (Instance) theEObject;
			T result = caseInstance(instance);
			if (result == null)
				result = caseNamedElement(instance);
			if (result == null)
				result = caseElement(instance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.COMPONENT_INSTANCE:
		{
			ComponentInstance componentInstance = (ComponentInstance) theEObject;
			T result = caseComponentInstance(componentInstance);
			if (result == null)
				result = caseInstance(componentInstance);
			if (result == null)
				result = caseNamedElement(componentInstance);
			if (result == null)
				result = caseElement(componentInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.PORT_INSTANCE:
		{
			PortInstance portInstance = (PortInstance) theEObject;
			T result = casePortInstance(portInstance);
			if (result == null)
				result = caseInstance(portInstance);
			if (result == null)
				result = caseConnecteableElement(portInstance);
			if (result == null)
				result = caseNamedElement(portInstance);
			if (result == null)
				result = caseElement(portInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.IN_INSTANCE:
		{
			INInstance inInstance = (INInstance) theEObject;
			T result = caseINInstance(inInstance);
			if (result == null)
				result = casePortInstance(inInstance);
			if (result == null)
				result = caseInstance(inInstance);
			if (result == null)
				result = caseConnecteableElement(inInstance);
			if (result == null)
				result = caseNamedElement(inInstance);
			if (result == null)
				result = caseElement(inInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.OUT_INSTANCE:
		{
			OutInstance outInstance = (OutInstance) theEObject;
			T result = caseOutInstance(outInstance);
			if (result == null)
				result = casePortInstance(outInstance);
			if (result == null)
				result = caseInstance(outInstance);
			if (result == null)
				result = caseConnecteableElement(outInstance);
			if (result == null)
				result = caseNamedElement(outInstance);
			if (result == null)
				result = caseElement(outInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INTERNAL_DEPENDENCY_IN:
		{
			InternalDependencyIN internalDependencyIN = (InternalDependencyIN) theEObject;
			T result = caseInternalDependencyIN(internalDependencyIN);
			if (result == null)
				result = caseApplicationConnection(internalDependencyIN);
			if (result == null)
				result = caseConnection(internalDependencyIN);
			if (result == null)
				result = caseElement(internalDependencyIN);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INTERNAL_DEPENDENCY_OUT:
		{
			InternalDependencyOUT internalDependencyOUT = (InternalDependencyOUT) theEObject;
			T result = caseInternalDependencyOUT(internalDependencyOUT);
			if (result == null)
				result = caseApplicationConnection(internalDependencyOUT);
			if (result == null)
				result = caseConnection(internalDependencyOUT);
			if (result == null)
				result = caseElement(internalDependencyOUT);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONNECTEABLE_ELEMENT:
		{
			ConnecteableElement connecteableElement = (ConnecteableElement) theEObject;
			T result = caseConnecteableElement(connecteableElement);
			if (result == null)
				result = caseNamedElement(connecteableElement);
			if (result == null)
				result = caseElement(connecteableElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.IN_COND_PORT:
		{
			InCondPort inCondPort = (InCondPort) theEObject;
			T result = caseInCondPort(inCondPort);
			if (result == null)
				result = casePort(inCondPort);
			if (result == null)
				result = caseNamedElement(inCondPort);
			if (result == null)
				result = caseConnecteableElement(inCondPort);
			if (result == null)
				result = caseElement(inCondPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.HARDWARE_COMPONENT:
		{
			HardwareComponent hardwareComponent = (HardwareComponent) theEObject;
			T result = caseHardwareComponent(hardwareComponent);
			if (result == null)
				result = caseComponent(hardwareComponent);
			if (result == null)
				result = caseNamedElement(hardwareComponent);
			if (result == null)
				result = caseElement(hardwareComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.HARDWARE:
		{
			Hardware hardware = (Hardware) theEObject;
			T result = caseHardware(hardware);
			if (result == null)
				result = caseHardwareComponent(hardware);
			if (result == null)
				result = caseComponent(hardware);
			if (result == null)
				result = caseNamedElement(hardware);
			if (result == null)
				result = caseElement(hardware);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.MEDIA:
		{
			Media media = (Media) theEObject;
			T result = caseMedia(media);
			if (result == null)
				result = caseHardwareComponent(media);
			if (result == null)
				result = caseComponent(media);
			if (result == null)
				result = caseNamedElement(media);
			if (result == null)
				result = caseElement(media);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.FPGA:
		{
			FPGA fpga = (FPGA) theEObject;
			T result = caseFPGA(fpga);
			if (result == null)
				result = caseProcessor(fpga);
			if (result == null)
				result = caseHardwareComponent(fpga);
			if (result == null)
				result = caseComponent(fpga);
			if (result == null)
				result = caseNamedElement(fpga);
			if (result == null)
				result = caseElement(fpga);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ASIC:
		{
			Asic asic = (Asic) theEObject;
			T result = caseAsic(asic);
			if (result == null)
				result = caseProcessor(asic);
			if (result == null)
				result = caseHardwareComponent(asic);
			if (result == null)
				result = caseComponent(asic);
			if (result == null)
				result = caseNamedElement(asic);
			if (result == null)
				result = caseElement(asic);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.DSP:
		{
			DSP dsp = (DSP) theEObject;
			T result = caseDSP(dsp);
			if (result == null)
				result = caseProcessor(dsp);
			if (result == null)
				result = caseHardwareComponent(dsp);
			if (result == null)
				result = caseComponent(dsp);
			if (result == null)
				result = caseNamedElement(dsp);
			if (result == null)
				result = caseElement(dsp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.RAM:
		{
			RAM ram = (RAM) theEObject;
			T result = caseRAM(ram);
			if (result == null)
				result = caseMedia(ram);
			if (result == null)
				result = caseHardwareComponent(ram);
			if (result == null)
				result = caseComponent(ram);
			if (result == null)
				result = caseNamedElement(ram);
			if (result == null)
				result = caseElement(ram);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.SAM:
		{
			SAM sam = (SAM) theEObject;
			T result = caseSAM(sam);
			if (result == null)
				result = caseMedia(sam);
			if (result == null)
				result = caseHardwareComponent(sam);
			if (result == null)
				result = caseComponent(sam);
			if (result == null)
				result = caseNamedElement(sam);
			if (result == null)
				result = caseElement(sam);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.SAMPTP:
		{
			SAMPTP samptp = (SAMPTP) theEObject;
			T result = caseSAMPTP(samptp);
			if (result == null)
				result = caseMedia(samptp);
			if (result == null)
				result = caseHardwareComponent(samptp);
			if (result == null)
				result = caseComponent(samptp);
			if (result == null)
				result = caseNamedElement(samptp);
			if (result == null)
				result = caseElement(samptp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.HW_CONNECTION:
		{
			HWConnection hwConnection = (HWConnection) theEObject;
			T result = caseHWConnection(hwConnection);
			if (result == null)
				result = caseConnection(hwConnection);
			if (result == null)
				result = caseElement(hwConnection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONNECTION_HW:
		{
			ConnectionHW connectionHW = (ConnectionHW) theEObject;
			T result = caseConnectionHW(connectionHW);
			if (result == null)
				result = caseHWConnection(connectionHW);
			if (result == null)
				result = caseConnection(connectionHW);
			if (result == null)
				result = caseElement(connectionHW);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CARACTERISTICS:
		{
			Caracteristics caracteristics = (Caracteristics) theEObject;
			T result = caseCaracteristics(caracteristics);
			if (result == null)
				result = caseNamedElement(caracteristics);
			if (result == null)
				result = caseElement(caracteristics);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ASSOCIATION_SWTO_HW:
		{
			AssociationSWtoHW associationSWtoHW = (AssociationSWtoHW) theEObject;
			T result = caseAssociationSWtoHW(associationSWtoHW);
			if (result == null)
				result = caseCaracteristics(associationSWtoHW);
			if (result == null)
				result = caseNamedElement(associationSWtoHW);
			if (result == null)
				result = caseElement(associationSWtoHW);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ASSOCIATION_COMTO_PROC:
		{
			AssociationComtoProc associationComtoProc = (AssociationComtoProc) theEObject;
			T result = caseAssociationComtoProc(associationComtoProc);
			if (result == null)
				result = caseCaracteristics(associationComtoProc);
			if (result == null)
				result = caseNamedElement(associationComtoProc);
			if (result == null)
				result = caseElement(associationComtoProc);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.DURATION:
		{
			Duration duration = (Duration) theEObject;
			T result = caseDuration(duration);
			if (result == null)
				result = caseNamedElement(duration);
			if (result == null)
				result = caseElement(duration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ENUM_LIT_DURATION:
		{
			EnumLitDuration enumLitDuration = (EnumLitDuration) theEObject;
			T result = caseEnumLitDuration(enumLitDuration);
			if (result == null)
				result = caseNamedElement(enumLitDuration);
			if (result == null)
				result = caseElement(enumLitDuration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.DATATYPE:
		{
			Datatype datatype = (Datatype) theEObject;
			T result = caseDatatype(datatype);
			if (result == null)
				result = caseElement(datatype);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.SIZE:
		{
			Size size = (Size) theEObject;
			T result = caseSize(size);
			if (result == null)
				result = caseElement(size);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ARRAY:
		{
			Array array = (Array) theEObject;
			T result = caseArray(array);
			if (result == null)
				result = caseSize(array);
			if (result == null)
				result = caseElement(array);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.VECTOR:
		{
			Vector vector = (Vector) theEObject;
			T result = caseVector(vector);
			if (result == null)
				result = caseSize(vector);
			if (result == null)
				result = caseElement(vector);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.PROCESSOR:
		{
			Processor processor = (Processor) theEObject;
			T result = caseProcessor(processor);
			if (result == null)
				result = caseHardwareComponent(processor);
			if (result == null)
				result = caseComponent(processor);
			if (result == null)
				result = caseNamedElement(processor);
			if (result == null)
				result = caseElement(processor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ELEMENT:
		{
			Element element = (Element) theEObject;
			T result = caseElement(element);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.SYN_DEX:
		{
			SynDEx synDEx = (SynDEx) theEObject;
			T result = caseSynDEx(synDEx);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.APPLICATION_CONNECTION:
		{
			ApplicationConnection applicationConnection = (ApplicationConnection) theEObject;
			T result = caseApplicationConnection(applicationConnection);
			if (result == null)
				result = caseConnection(applicationConnection);
			if (result == null)
				result = caseElement(applicationConnection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INTERNAL_STRUCTURE:
		{
			InternalStructure internalStructure = (InternalStructure) theEObject;
			T result = caseInternalStructure(internalStructure);
			if (result == null)
				result = caseElement(internalStructure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.ELEMENTARY_STRUCTURE:
		{
			ElementaryStructure elementaryStructure = (ElementaryStructure) theEObject;
			T result = caseElementaryStructure(elementaryStructure);
			if (result == null)
				result = caseInternalStructure(elementaryStructure);
			if (result == null)
				result = caseElement(elementaryStructure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.COMPOUND_STRUCTURE:
		{
			CompoundStructure compoundStructure = (CompoundStructure) theEObject;
			T result = caseCompoundStructure(compoundStructure);
			if (result == null)
				result = caseInternalStructure(compoundStructure);
			if (result == null)
				result = caseElement(compoundStructure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INOUT:
		{
			INOUT inout = (INOUT) theEObject;
			T result = caseINOUT(inout);
			if (result == null)
				result = casePort(inout);
			if (result == null)
				result = caseNamedElement(inout);
			if (result == null)
				result = caseConnecteableElement(inout);
			if (result == null)
				result = caseElement(inout);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INTERNAL_DATA_CONNECTION:
		{
			InternalDataConnection internalDataConnection = (InternalDataConnection) theEObject;
			T result = caseInternalDataConnection(internalDataConnection);
			if (result == null)
				result = caseApplicationConnection(internalDataConnection);
			if (result == null)
				result = caseConnection(internalDataConnection);
			if (result == null)
				result = caseElement(internalDataConnection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.PARAMETER_DECLARATION:
		{
			ParameterDeclaration parameterDeclaration = (ParameterDeclaration) theEObject;
			T result = caseParameterDeclaration(parameterDeclaration);
			if (result == null)
				result = caseNamedElement(parameterDeclaration);
			if (result == null)
				result = caseElement(parameterDeclaration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.PARAMETER_VALUE:
		{
			ParameterValue parameterValue = (ParameterValue) theEObject;
			T result = caseParameterValue(parameterValue);
			if (result == null)
				result = caseElement(parameterValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONDITIONED_STRUCTURE:
		{
			ConditionedStructure conditionedStructure = (ConditionedStructure) theEObject;
			T result = caseConditionedStructure(conditionedStructure);
			if (result == null)
				result = caseInternalStructure(conditionedStructure);
			if (result == null)
				result = caseElement(conditionedStructure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.CONDITION:
		{
			Condition condition = (Condition) theEObject;
			T result = caseCondition(condition);
			if (result == null)
				result = caseNamedElement(condition);
			if (result == null)
				result = caseInternalStructure(condition);
			if (result == null)
				result = caseElement(condition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.REPETITION_CONNECTION:
		{
			RepetitionConnection repetitionConnection = (RepetitionConnection) theEObject;
			T result = caseRepetitionConnection(repetitionConnection);
			if (result == null)
				result = caseApplicationConnection(repetitionConnection);
			if (result == null)
				result = caseConnection(repetitionConnection);
			if (result == null)
				result = caseElement(repetitionConnection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INOUT_INSTANCE:
		{
			INOUTInstance inoutInstance = (INOUTInstance) theEObject;
			T result = caseINOUTInstance(inoutInstance);
			if (result == null)
				result = casePortInstance(inoutInstance);
			if (result == null)
				result = caseInstance(inoutInstance);
			if (result == null)
				result = caseConnecteableElement(inoutInstance);
			if (result == null)
				result = caseNamedElement(inoutInstance);
			if (result == null)
				result = caseElement(inoutInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.IN_COND_INSTANCE:
		{
			InCondInstance inCondInstance = (InCondInstance) theEObject;
			T result = caseInCondInstance(inCondInstance);
			if (result == null)
				result = casePortInstance(inCondInstance);
			if (result == null)
				result = caseInstance(inCondInstance);
			if (result == null)
				result = caseConnecteableElement(inCondInstance);
			if (result == null)
				result = caseNamedElement(inCondInstance);
			if (result == null)
				result = caseElement(inCondInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SyndexPackage.INTERNAL_DEPENDENCY:
		{
			InternalDependency internalDependency = (InternalDependency) theEObject;
			T result = caseInternalDependency(internalDependency);
			if (result == null)
				result = caseApplicationConnection(internalDependency);
			if (result == null)
				result = caseConnection(internalDependency);
			if (result == null)
				result = caseElement(internalDependency);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Actuator</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actuator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActuator(Actuator object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Application Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Application Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicApplicationComponent(AtomicApplicationComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplicationComponent(ApplicationComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sensor</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sensor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSensor(Sensor object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplication(Application object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInput(Input object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutput(Output object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction(Function object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstant(Constant object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelay(Delay object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnection(Connection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Connection</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataConnection(DataConnection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instance</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstance(Instance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstance(ComponentInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Instance</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortInstance(PortInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IN Instance</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IN Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINInstance(INInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Instance</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutInstance(OutInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Dependency IN</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Dependency IN</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalDependencyIN(InternalDependencyIN object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Dependency OUT</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Dependency OUT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalDependencyOUT(InternalDependencyOUT object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connecteable Element</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connecteable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnecteableElement(ConnecteableElement object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Cond Port</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Cond Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInCondPort(InCondPort object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hardware Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hardware Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHardwareComponent(HardwareComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hardware</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hardware</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHardware(Hardware object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Media</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Media</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMedia(Media object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FPGA</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FPGA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFPGA(FPGA object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asic</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsic(Asic object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DSP</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DSP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDSP(DSP object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RAM</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRAM(RAM object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SAM</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSAM(SAM object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SAMPTP</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SAMPTP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSAMPTP(SAMPTP object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HW Connection</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HW Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHWConnection(HWConnection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection HW</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection HW</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionHW(ConnectionHW object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Caracteristics</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Caracteristics</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaracteristics(Caracteristics object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association SWto HW</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association SWto HW</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationSWtoHW(AssociationSWtoHW object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association Comto Proc</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association Comto Proc</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationComtoProc(AssociationComtoProc object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duration</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDuration(Duration object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Lit Duration</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Lit Duration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumLitDuration(EnumLitDuration object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Datatype</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Datatype</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatatype(Datatype object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Size</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Size</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSize(Size object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArray(Array object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vector</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVector(Vector object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Processor</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Processor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessor(Processor object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElement(Element object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Syn DEx</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Syn DEx</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynDEx(SynDEx object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application Connection</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplicationConnection(ApplicationConnection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Structure</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Structure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalStructure(InternalStructure object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elementary Structure</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elementary Structure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementaryStructure(ElementaryStructure object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compound Structure</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compound Structure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompoundStructure(CompoundStructure object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INOUT</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INOUT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINOUT(INOUT object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Data Connection</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Data Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalDataConnection(InternalDataConnection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Declaration</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterDeclaration(ParameterDeclaration object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Value</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterValue(ParameterValue object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditioned Structure</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditioned Structure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionedStructure(ConditionedStructure object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondition(Condition object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Repetition Connection</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Repetition Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepetitionConnection(RepetitionConnection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INOUT Instance</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INOUT Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINOUTInstance(INOUTInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Cond Instance</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Cond Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInCondInstance(InCondInstance object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Dependency</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalDependency(InternalDependency object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch, but this is the last case
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object)
	{
		return null;
	}

} // SyndexSwitch
