/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexResourceImpl.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc --> The <b>Resource </b> associated with the package. <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.util.SyndexResourceFactoryImpl
 * @generated
 */
public class SyndexResourceImpl extends XMIResourceImpl
{
	/**
	 * Creates an instance of the resource. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param uri
	 *        the URI of the new resource.
	 * @generated
	 */
	public SyndexResourceImpl(URI uri)
	{
		super(uri);
	}

} // SyndexResourceImpl
