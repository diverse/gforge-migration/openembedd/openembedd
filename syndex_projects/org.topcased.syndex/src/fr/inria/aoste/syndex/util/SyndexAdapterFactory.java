/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexAdapterFactory.java,v 1.1 2008-11-18 10:44:01 cbrunett Exp $
 */
package fr.inria.aoste.syndex.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ApplicationConnection;
import fr.inria.aoste.syndex.Array;
import fr.inria.aoste.syndex.Asic;
import fr.inria.aoste.syndex.AssociationComtoProc;
import fr.inria.aoste.syndex.AssociationSWtoHW;
import fr.inria.aoste.syndex.AtomicApplicationComponent;
import fr.inria.aoste.syndex.Caracteristics;
import fr.inria.aoste.syndex.Component;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.ConnecteableElement;
import fr.inria.aoste.syndex.Connection;
import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.DSP;
import fr.inria.aoste.syndex.DataConnection;
import fr.inria.aoste.syndex.Datatype;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Duration;
import fr.inria.aoste.syndex.Element;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.EnumLitDuration;
import fr.inria.aoste.syndex.FPGA;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.HWConnection;
import fr.inria.aoste.syndex.Hardware;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Instance;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependency;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.NamedElement;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.ParameterDeclaration;
import fr.inria.aoste.syndex.ParameterValue;
import fr.inria.aoste.syndex.Port;
import fr.inria.aoste.syndex.PortInstance;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.RAM;
import fr.inria.aoste.syndex.RepetitionConnection;
import fr.inria.aoste.syndex.SAM;
import fr.inria.aoste.syndex.SAMPTP;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.Size;
import fr.inria.aoste.syndex.SynDEx;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.Vector;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides an adapter <code>createXXX</code>
 * method for each class of the model. <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage
 * @generated
 */
public class SyndexAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static SyndexPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SyndexAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = SyndexPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object. <!-- begin-user-doc --> This
	 * implementation returns <code>true</code> if the object is either the model's package or is an instance object of
	 * the model. <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch the delegates to the <code>createXXX</code> methods. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SyndexSwitch<Adapter>	modelSwitch	= new SyndexSwitch<Adapter>()
												{
													@Override
													public Adapter caseActuator(Actuator object)
													{
														return createActuatorAdapter();
													}
													@Override
													public Adapter caseAtomicApplicationComponent(
															AtomicApplicationComponent object)
													{
														return createAtomicApplicationComponentAdapter();
													}
													@Override
													public Adapter casePort(Port object)
													{
														return createPortAdapter();
													}
													@Override
													public Adapter caseNamedElement(NamedElement object)
													{
														return createNamedElementAdapter();
													}
													@Override
													public Adapter caseApplicationComponent(ApplicationComponent object)
													{
														return createApplicationComponentAdapter();
													}
													@Override
													public Adapter caseSensor(Sensor object)
													{
														return createSensorAdapter();
													}
													@Override
													public Adapter caseApplication(Application object)
													{
														return createApplicationAdapter();
													}
													@Override
													public Adapter caseInput(Input object)
													{
														return createInputAdapter();
													}
													@Override
													public Adapter caseComponent(Component object)
													{
														return createComponentAdapter();
													}
													@Override
													public Adapter caseOutput(Output object)
													{
														return createOutputAdapter();
													}
													@Override
													public Adapter caseFunction(Function object)
													{
														return createFunctionAdapter();
													}
													@Override
													public Adapter caseConstant(Constant object)
													{
														return createConstantAdapter();
													}
													@Override
													public Adapter caseDelay(Delay object)
													{
														return createDelayAdapter();
													}
													@Override
													public Adapter caseConnection(Connection object)
													{
														return createConnectionAdapter();
													}
													@Override
													public Adapter caseDataConnection(DataConnection object)
													{
														return createDataConnectionAdapter();
													}
													@Override
													public Adapter caseInstance(Instance object)
													{
														return createInstanceAdapter();
													}
													@Override
													public Adapter caseComponentInstance(ComponentInstance object)
													{
														return createComponentInstanceAdapter();
													}
													@Override
													public Adapter casePortInstance(PortInstance object)
													{
														return createPortInstanceAdapter();
													}
													@Override
													public Adapter caseINInstance(INInstance object)
													{
														return createINInstanceAdapter();
													}
													@Override
													public Adapter caseOutInstance(OutInstance object)
													{
														return createOutInstanceAdapter();
													}
													@Override
													public Adapter caseInternalDependencyIN(InternalDependencyIN object)
													{
														return createInternalDependencyINAdapter();
													}
													@Override
													public Adapter caseInternalDependencyOUT(
															InternalDependencyOUT object)
													{
														return createInternalDependencyOUTAdapter();
													}
													@Override
													public Adapter caseConnecteableElement(ConnecteableElement object)
													{
														return createConnecteableElementAdapter();
													}
													@Override
													public Adapter caseInCondPort(InCondPort object)
													{
														return createInCondPortAdapter();
													}
													@Override
													public Adapter caseHardwareComponent(HardwareComponent object)
													{
														return createHardwareComponentAdapter();
													}
													@Override
													public Adapter caseHardware(Hardware object)
													{
														return createHardwareAdapter();
													}
													@Override
													public Adapter caseMedia(Media object)
													{
														return createMediaAdapter();
													}
													@Override
													public Adapter caseFPGA(FPGA object)
													{
														return createFPGAAdapter();
													}
													@Override
													public Adapter caseAsic(Asic object)
													{
														return createAsicAdapter();
													}
													@Override
													public Adapter caseDSP(DSP object)
													{
														return createDSPAdapter();
													}
													@Override
													public Adapter caseRAM(RAM object)
													{
														return createRAMAdapter();
													}
													@Override
													public Adapter caseSAM(SAM object)
													{
														return createSAMAdapter();
													}
													@Override
													public Adapter caseSAMPTP(SAMPTP object)
													{
														return createSAMPTPAdapter();
													}
													@Override
													public Adapter caseHWConnection(HWConnection object)
													{
														return createHWConnectionAdapter();
													}
													@Override
													public Adapter caseConnectionHW(ConnectionHW object)
													{
														return createConnectionHWAdapter();
													}
													@Override
													public Adapter caseCaracteristics(Caracteristics object)
													{
														return createCaracteristicsAdapter();
													}
													@Override
													public Adapter caseAssociationSWtoHW(AssociationSWtoHW object)
													{
														return createAssociationSWtoHWAdapter();
													}
													@Override
													public Adapter caseAssociationComtoProc(AssociationComtoProc object)
													{
														return createAssociationComtoProcAdapter();
													}
													@Override
													public Adapter caseDuration(Duration object)
													{
														return createDurationAdapter();
													}
													@Override
													public Adapter caseEnumLitDuration(EnumLitDuration object)
													{
														return createEnumLitDurationAdapter();
													}
													@Override
													public Adapter caseDatatype(Datatype object)
													{
														return createDatatypeAdapter();
													}
													@Override
													public Adapter caseSize(Size object)
													{
														return createSizeAdapter();
													}
													@Override
													public Adapter caseArray(Array object)
													{
														return createArrayAdapter();
													}
													@Override
													public Adapter caseVector(Vector object)
													{
														return createVectorAdapter();
													}
													@Override
													public Adapter caseProcessor(Processor object)
													{
														return createProcessorAdapter();
													}
													@Override
													public Adapter caseElement(Element object)
													{
														return createElementAdapter();
													}
													@Override
													public Adapter caseSynDEx(SynDEx object)
													{
														return createSynDExAdapter();
													}
													@Override
													public Adapter caseApplicationConnection(
															ApplicationConnection object)
													{
														return createApplicationConnectionAdapter();
													}
													@Override
													public Adapter caseInternalStructure(InternalStructure object)
													{
														return createInternalStructureAdapter();
													}
													@Override
													public Adapter caseElementaryStructure(ElementaryStructure object)
													{
														return createElementaryStructureAdapter();
													}
													@Override
													public Adapter caseCompoundStructure(CompoundStructure object)
													{
														return createCompoundStructureAdapter();
													}
													@Override
													public Adapter caseINOUT(INOUT object)
													{
														return createINOUTAdapter();
													}
													@Override
													public Adapter caseInternalDataConnection(
															InternalDataConnection object)
													{
														return createInternalDataConnectionAdapter();
													}
													@Override
													public Adapter caseParameterDeclaration(ParameterDeclaration object)
													{
														return createParameterDeclarationAdapter();
													}
													@Override
													public Adapter caseParameterValue(ParameterValue object)
													{
														return createParameterValueAdapter();
													}
													@Override
													public Adapter caseConditionedStructure(ConditionedStructure object)
													{
														return createConditionedStructureAdapter();
													}
													@Override
													public Adapter caseCondition(Condition object)
													{
														return createConditionAdapter();
													}
													@Override
													public Adapter caseRepetitionConnection(RepetitionConnection object)
													{
														return createRepetitionConnectionAdapter();
													}
													@Override
													public Adapter caseINOUTInstance(INOUTInstance object)
													{
														return createINOUTInstanceAdapter();
													}
													@Override
													public Adapter caseInCondInstance(InCondInstance object)
													{
														return createInCondInstanceAdapter();
													}
													@Override
													public Adapter caseInternalDependency(InternalDependency object)
													{
														return createInternalDependencyAdapter();
													}
													@Override
													public Adapter defaultCase(EObject object)
													{
														return createEObjectAdapter();
													}
												};

	/**
	 * Creates an adapter for the <code>target</code>. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param target
	 *        the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Actuator <em>Actuator</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Actuator
	 * @generated
	 */
	public Adapter createActuatorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.AtomicApplicationComponent
	 * <em>Atomic Application Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.AtomicApplicationComponent
	 * @generated
	 */
	public Adapter createAtomicApplicationComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Port <em>Port</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Port
	 * @generated
	 */
	public Adapter createPortAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ApplicationComponent
	 * <em>Application Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ApplicationComponent
	 * @generated
	 */
	public Adapter createApplicationComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Sensor <em>Sensor</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Sensor
	 * @generated
	 */
	public Adapter createSensorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Application <em>Application</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Application
	 * @generated
	 */
	public Adapter createApplicationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Input <em>Input</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Input
	 * @generated
	 */
	public Adapter createInputAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Component <em>Component</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Component
	 * @generated
	 */
	public Adapter createComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Output <em>Output</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Output
	 * @generated
	 */
	public Adapter createOutputAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Function <em>Function</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Function
	 * @generated
	 */
	public Adapter createFunctionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Constant <em>Constant</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Constant
	 * @generated
	 */
	public Adapter createConstantAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Delay <em>Delay</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Delay
	 * @generated
	 */
	public Adapter createDelayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Connection <em>Connection</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Connection
	 * @generated
	 */
	public Adapter createConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.DataConnection
	 * <em>Data Connection</em>} '. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.DataConnection
	 * @generated
	 */
	public Adapter createDataConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Instance <em>Instance</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Instance
	 * @generated
	 */
	public Adapter createInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ComponentInstance
	 * <em>Component Instance</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ComponentInstance
	 * @generated
	 */
	public Adapter createComponentInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.PortInstance <em>Port Instance</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.PortInstance
	 * @generated
	 */
	public Adapter createPortInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.INInstance <em>IN Instance</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.INInstance
	 * @generated
	 */
	public Adapter createINInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.OutInstance <em>Out Instance</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.OutInstance
	 * @generated
	 */
	public Adapter createOutInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InternalDependencyIN
	 * <em>Internal Dependency IN</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InternalDependencyIN
	 * @generated
	 */
	public Adapter createInternalDependencyINAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InternalDependencyOUT
	 * <em>Internal Dependency OUT</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InternalDependencyOUT
	 * @generated
	 */
	public Adapter createInternalDependencyOUTAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ConnecteableElement
	 * <em>Connecteable Element</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ConnecteableElement
	 * @generated
	 */
	public Adapter createConnecteableElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InCondPort <em>In Cond Port</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InCondPort
	 * @generated
	 */
	public Adapter createInCondPortAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.HardwareComponent
	 * <em>Hardware Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.HardwareComponent
	 * @generated
	 */
	public Adapter createHardwareComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Hardware <em>Hardware</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Hardware
	 * @generated
	 */
	public Adapter createHardwareAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Media <em>Media</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Media
	 * @generated
	 */
	public Adapter createMediaAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.FPGA <em>FPGA</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.FPGA
	 * @generated
	 */
	public Adapter createFPGAAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Asic <em>Asic</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Asic
	 * @generated
	 */
	public Adapter createAsicAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.DSP <em>DSP</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.DSP
	 * @generated
	 */
	public Adapter createDSPAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.RAM <em>RAM</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.RAM
	 * @generated
	 */
	public Adapter createRAMAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.SAM <em>SAM</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.SAM
	 * @generated
	 */
	public Adapter createSAMAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.SAMPTP <em>SAMPTP</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.SAMPTP
	 * @generated
	 */
	public Adapter createSAMPTPAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.HWConnection <em>HW Connection</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.HWConnection
	 * @generated
	 */
	public Adapter createHWConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ConnectionHW <em>Connection HW</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ConnectionHW
	 * @generated
	 */
	public Adapter createConnectionHWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Caracteristics
	 * <em>Caracteristics</em>} '. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Caracteristics
	 * @generated
	 */
	public Adapter createCaracteristicsAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.AssociationSWtoHW
	 * <em>Association SWto HW</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.AssociationSWtoHW
	 * @generated
	 */
	public Adapter createAssociationSWtoHWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.AssociationComtoProc
	 * <em>Association Comto Proc</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.AssociationComtoProc
	 * @generated
	 */
	public Adapter createAssociationComtoProcAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Duration <em>Duration</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Duration
	 * @generated
	 */
	public Adapter createDurationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.EnumLitDuration
	 * <em>Enum Lit Duration</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.EnumLitDuration
	 * @generated
	 */
	public Adapter createEnumLitDurationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Datatype <em>Datatype</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Datatype
	 * @generated
	 */
	public Adapter createDatatypeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Size <em>Size</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Size
	 * @generated
	 */
	public Adapter createSizeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Array <em>Array</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Array
	 * @generated
	 */
	public Adapter createArrayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Vector <em>Vector</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Vector
	 * @generated
	 */
	public Adapter createVectorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Processor <em>Processor</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Processor
	 * @generated
	 */
	public Adapter createProcessorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Element <em>Element</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Element
	 * @generated
	 */
	public Adapter createElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.SynDEx <em>Syn DEx</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.SynDEx
	 * @generated
	 */
	public Adapter createSynDExAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ApplicationConnection
	 * <em>Application Connection</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ApplicationConnection
	 * @generated
	 */
	public Adapter createApplicationConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InternalStructure
	 * <em>Internal Structure</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InternalStructure
	 * @generated
	 */
	public Adapter createInternalStructureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ElementaryStructure
	 * <em>Elementary Structure</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ElementaryStructure
	 * @generated
	 */
	public Adapter createElementaryStructureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.CompoundStructure
	 * <em>Compound Structure</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.CompoundStructure
	 * @generated
	 */
	public Adapter createCompoundStructureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.INOUT <em>INOUT</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.INOUT
	 * @generated
	 */
	public Adapter createINOUTAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InternalDataConnection
	 * <em>Internal Data Connection</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InternalDataConnection
	 * @generated
	 */
	public Adapter createInternalDataConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ParameterDeclaration
	 * <em>Parameter Declaration</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ParameterDeclaration
	 * @generated
	 */
	public Adapter createParameterDeclarationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ParameterValue
	 * <em>Parameter Value</em>} '. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ParameterValue
	 * @generated
	 */
	public Adapter createParameterValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.ConditionedStructure
	 * <em>Conditioned Structure</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.ConditionedStructure
	 * @generated
	 */
	public Adapter createConditionedStructureAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.Condition <em>Condition</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.Condition
	 * @generated
	 */
	public Adapter createConditionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.RepetitionConnection
	 * <em>Repetition Connection</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.RepetitionConnection
	 * @generated
	 */
	public Adapter createRepetitionConnectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.INOUTInstance <em>INOUT Instance</em>}
	 * '. <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.INOUTInstance
	 * @generated
	 */
	public Adapter createINOUTInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InCondInstance
	 * <em>In Cond Instance</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InCondInstance
	 * @generated
	 */
	public Adapter createInCondInstanceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.inria.aoste.syndex.InternalDependency
	 * <em>Internal Dependency</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see fr.inria.aoste.syndex.InternalDependency
	 * @generated
	 */
	public Adapter createInternalDependencyAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case. <!-- begin-user-doc --> This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} // SyndexAdapterFactory
