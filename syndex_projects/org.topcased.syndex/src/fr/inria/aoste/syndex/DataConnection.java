/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: DataConnection.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Data Connection</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.DataConnection#getSourceDataConnection <em>Source Data Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.DataConnection#getTargetDataconnection <em>Target Dataconnection</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getDataConnection()
 * @model
 * @generated
 */
public interface DataConnection extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Source Data Connection</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Data Connection</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Data Connection</em>' reference.
	 * @see #setSourceDataConnection(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDataConnection_SourceDataConnection()
	 * @model required="true"
	 * @generated
	 */
	Output getSourceDataConnection();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.DataConnection#getSourceDataConnection
	 * <em>Source Data Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Data Connection</em>' reference.
	 * @see #getSourceDataConnection()
	 * @generated
	 */
	void setSourceDataConnection(Output value);

	/**
	 * Returns the value of the '<em><b>Target Dataconnection</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Dataconnection</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target Dataconnection</em>' reference.
	 * @see #setTargetDataconnection(Input)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDataConnection_TargetDataconnection()
	 * @model required="true"
	 * @generated
	 */
	Input getTargetDataconnection();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.DataConnection#getTargetDataconnection
	 * <em>Target Dataconnection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target Dataconnection</em>' reference.
	 * @see #getTargetDataconnection()
	 * @generated
	 */
	void setTargetDataconnection(Input value);

} // DataConnection
