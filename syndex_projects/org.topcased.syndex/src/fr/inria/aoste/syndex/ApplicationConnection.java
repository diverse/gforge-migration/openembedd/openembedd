/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationConnection.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Application Connection</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ApplicationConnection#getParentCoumpoundApplicationConnection <em>Parent Coumpound
 * Application Connection</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ApplicationConnection#getParentConditionApplicationConnection <em>Parent Condition
 * Application Connection</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getApplicationConnection()
 * @model abstract="true"
 * @generated
 */
public interface ApplicationConnection extends Connection
{
	/**
	 * Returns the value of the '<em><b>Parent Coumpound Application Connection</b></em>' reference. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Parent Coumpound Application Connection</em>' reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Coumpound Application Connection</em>' reference.
	 * @see #setParentCoumpoundApplicationConnection(CompoundStructure)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplicationConnection_ParentCoumpoundApplicationConnection()
	 * @model
	 * @generated
	 */
	CompoundStructure getParentCoumpoundApplicationConnection();

	/**
	 * Sets the value of the '
	 * {@link fr.inria.aoste.syndex.ApplicationConnection#getParentCoumpoundApplicationConnection
	 * <em>Parent Coumpound Application Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Coumpound Application Connection</em>' reference.
	 * @see #getParentCoumpoundApplicationConnection()
	 * @generated
	 */
	void setParentCoumpoundApplicationConnection(CompoundStructure value);

	/**
	 * Returns the value of the '<em><b>Parent Condition Application Connection</b></em>' reference. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Parent Condition Application Connection</em>' reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Condition Application Connection</em>' reference.
	 * @see #setParentConditionApplicationConnection(ConditionedStructure)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplicationConnection_ParentConditionApplicationConnection()
	 * @model
	 * @generated
	 */
	ConditionedStructure getParentConditionApplicationConnection();

	/**
	 * Sets the value of the '
	 * {@link fr.inria.aoste.syndex.ApplicationConnection#getParentConditionApplicationConnection
	 * <em>Parent Condition Application Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Condition Application Connection</em>' reference.
	 * @see #getParentConditionApplicationConnection()
	 * @generated
	 */
	void setParentConditionApplicationConnection(ConditionedStructure value);

} // ApplicationConnection
