/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Caracteristics.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Caracteristics</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Caracteristics#isAdequation <em>Adequation</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getCaracteristics()
 * @model abstract="true"
 * @generated
 */
public interface Caracteristics extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Adequation</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adequation</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Adequation</em>' attribute.
	 * @see #setAdequation(boolean)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCaracteristics_Adequation()
	 * @model required="true"
	 * @generated
	 */
	boolean isAdequation();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Caracteristics#isAdequation <em>Adequation</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Adequation</em>' attribute.
	 * @see #isAdequation()
	 * @generated
	 */
	void setAdequation(boolean value);

} // Caracteristics
