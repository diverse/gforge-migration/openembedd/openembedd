/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalStructure.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Internal Structure</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InternalStructure#getOwnerInternalStructure <em>Owner Internal Structure</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalStructure#getInternalConnector <em>Internal Connector</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalStructure()
 * @model abstract="true"
 * @generated
 */
public interface InternalStructure extends Element
{
	/**
	 * Returns the value of the '<em><b>Owner Internal Structure</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner Internal Structure</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner Internal Structure</em>' reference.
	 * @see #setOwnerInternalStructure(ApplicationComponent)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalStructure_OwnerInternalStructure()
	 * @model required="true"
	 * @generated
	 */
	ApplicationComponent getOwnerInternalStructure();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalStructure#getOwnerInternalStructure
	 * <em>Owner Internal Structure</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Owner Internal Structure</em>' reference.
	 * @see #getOwnerInternalStructure()
	 * @generated
	 */
	void setOwnerInternalStructure(ApplicationComponent value);

	/**
	 * Returns the value of the '<em><b>Internal Connector</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.ApplicationConnection}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Connector</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Internal Connector</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalStructure_InternalConnector()
	 * @model containment="true"
	 * @generated
	 */
	EList<ApplicationConnection> getInternalConnector();

} // InternalStructure
