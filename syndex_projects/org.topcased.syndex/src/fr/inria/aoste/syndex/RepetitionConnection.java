/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: RepetitionConnection.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Repetition Connection</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.RepetitionConnection#getRepetitionKind <em>Repetition Kind</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.RepetitionConnection#getSourceRepetitionConnection <em>Source Repetition Connection
 * </em>}</li>
 * <li>{@link fr.inria.aoste.syndex.RepetitionConnection#getTargetRepetitionConnection <em>Target Repetition Connection
 * </em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getRepetitionConnection()
 * @model
 * @generated
 */
public interface RepetitionConnection extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Repetition Kind</b></em>' attribute. The literals are from the enumeration
	 * {@link fr.inria.aoste.syndex.RepetitionConnectionKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repetition Kind</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Repetition Kind</em>' attribute.
	 * @see fr.inria.aoste.syndex.RepetitionConnectionKind
	 * @see #setRepetitionKind(RepetitionConnectionKind)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getRepetitionConnection_RepetitionKind()
	 * @model required="true"
	 * @generated
	 */
	RepetitionConnectionKind getRepetitionKind();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.RepetitionConnection#getRepetitionKind
	 * <em>Repetition Kind</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Repetition Kind</em>' attribute.
	 * @see fr.inria.aoste.syndex.RepetitionConnectionKind
	 * @see #getRepetitionKind()
	 * @generated
	 */
	void setRepetitionKind(RepetitionConnectionKind value);

	/**
	 * Returns the value of the '<em><b>Source Repetition Connection</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Repetition Connection</em>' reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Repetition Connection</em>' reference.
	 * @see #setSourceRepetitionConnection(Size)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getRepetitionConnection_SourceRepetitionConnection()
	 * @model required="true"
	 * @generated
	 */
	Size getSourceRepetitionConnection();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.RepetitionConnection#getSourceRepetitionConnection
	 * <em>Source Repetition Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Repetition Connection</em>' reference.
	 * @see #getSourceRepetitionConnection()
	 * @generated
	 */
	void setSourceRepetitionConnection(Size value);

	/**
	 * Returns the value of the '<em><b>Target Repetition Connection</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Repetition Connection</em>' reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target Repetition Connection</em>' reference.
	 * @see #setTargetRepetitionConnection(Size)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getRepetitionConnection_TargetRepetitionConnection()
	 * @model required="true"
	 * @generated
	 */
	Size getTargetRepetitionConnection();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.RepetitionConnection#getTargetRepetitionConnection
	 * <em>Target Repetition Connection</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target Repetition Connection</em>' reference.
	 * @see #getTargetRepetitionConnection()
	 * @generated
	 */
	void setTargetRepetitionConnection(Size value);

} // RepetitionConnection
