/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: PortInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Port Instance</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.PortInstance#getParentComponentInstance <em>Parent Component Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getPortInstance()
 * @model abstract="true"
 * @generated
 */
public interface PortInstance extends Instance, ConnecteableElement
{
	/**
	 * Returns the value of the '<em><b>Parent Component Instance</b></em>' container reference. It is bidirectional and
	 * its opposite is '{@link fr.inria.aoste.syndex.ComponentInstance#getPortInstances <em>Port Instances</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Component Instance</em>' container reference isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Component Instance</em>' container reference.
	 * @see #setParentComponentInstance(ComponentInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getPortInstance_ParentComponentInstance()
	 * @see fr.inria.aoste.syndex.ComponentInstance#getPortInstances
	 * @model opposite="portInstances" transient="false"
	 * @generated
	 */
	ComponentInstance getParentComponentInstance();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.PortInstance#getParentComponentInstance
	 * <em>Parent Component Instance</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Component Instance</em>' container reference.
	 * @see #getParentComponentInstance()
	 * @generated
	 */
	void setParentComponentInstance(ComponentInstance value);

} // PortInstance
