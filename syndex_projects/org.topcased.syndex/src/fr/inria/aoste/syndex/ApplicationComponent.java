/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ApplicationComponent.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Application Component</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ApplicationComponent#getParamDeclarations <em>Param Declarations</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getApplicationComponent()
 * @model abstract="true"
 * @generated
 */
public interface ApplicationComponent extends Component
{
	/**
	 * Returns the value of the '<em><b>Param Declarations</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.ParameterDeclaration}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Param Declarations</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Param Declarations</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplicationComponent_ParamDeclarations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterDeclaration> getParamDeclarations();

} // ApplicationComponent
