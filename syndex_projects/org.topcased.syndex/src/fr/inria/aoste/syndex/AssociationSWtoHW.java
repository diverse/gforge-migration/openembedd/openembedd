/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AssociationSWtoHW.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Association SWto HW</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.AssociationSWtoHW#getDuration <em>Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.AssociationSWtoHW#getSourceAppli <em>Source Appli</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.AssociationSWtoHW#getTargetHW <em>Target HW</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationSWtoHW()
 * @model
 * @generated
 */
public interface AssociationSWtoHW extends Caracteristics
{
	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationSWtoHW_Duration()
	 * @model required="true"
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Source Appli</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Appli</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Appli</em>' reference.
	 * @see #setSourceAppli(ComponentInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationSWtoHW_SourceAppli()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getSourceAppli();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getSourceAppli <em>Source Appli</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Appli</em>' reference.
	 * @see #getSourceAppli()
	 * @generated
	 */
	void setSourceAppli(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>Target HW</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target HW</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target HW</em>' reference.
	 * @see #setTargetHW(ComponentInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationSWtoHW_TargetHW()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getTargetHW();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationSWtoHW#getTargetHW <em>Target HW</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target HW</em>' reference.
	 * @see #getTargetHW()
	 * @generated
	 */
	void setTargetHW(ComponentInstance value);

} // AssociationSWtoHW
