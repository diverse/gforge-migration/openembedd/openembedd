/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Delay.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Delay</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Delay#getOneInputDelay <em>One Input Delay</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Delay#getOneOutputDelay <em>One Output Delay</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Delay#getDelayValue <em>Delay Value</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Delay#getEnumValue <em>Enum Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getDelay()
 * @model
 * @generated
 */
public interface Delay extends AtomicApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>One Input Delay</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>One Input Delay</em>' containment reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>One Input Delay</em>' containment reference.
	 * @see #setOneInputDelay(Input)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDelay_OneInputDelay()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Input getOneInputDelay();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Delay#getOneInputDelay <em>One Input Delay</em>}' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>One Input Delay</em>' containment reference.
	 * @see #getOneInputDelay()
	 * @generated
	 */
	void setOneInputDelay(Input value);

	/**
	 * Returns the value of the '<em><b>One Output Delay</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>One Output Delay</em>' containment reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>One Output Delay</em>' containment reference.
	 * @see #setOneOutputDelay(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDelay_OneOutputDelay()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Output getOneOutputDelay();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Delay#getOneOutputDelay <em>One Output Delay</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>One Output Delay</em>' containment reference.
	 * @see #getOneOutputDelay()
	 * @generated
	 */
	void setOneOutputDelay(Output value);

	/**
	 * Returns the value of the '<em><b>Delay Value</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Value</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Delay Value</em>' attribute.
	 * @see #setDelayValue(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDelay_DelayValue()
	 * @model
	 * @generated
	 */
	int getDelayValue();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Delay#getDelayValue <em>Delay Value</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Delay Value</em>' attribute.
	 * @see #getDelayValue()
	 * @generated
	 */
	void setDelayValue(int value);

	/**
	 * Returns the value of the '<em><b>Enum Value</b></em>' attribute list. The list contents are of type
	 * {@link java.lang.Integer}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Value</em>' attribute list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Enum Value</em>' attribute list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDelay_EnumValue()
	 * @model
	 * @generated
	 */
	EList<Integer> getEnumValue();

} // Delay
