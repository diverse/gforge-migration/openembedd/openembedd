/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependency.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Internal Dependency</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InternalDependency#getSource <em>Source</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDependency#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependency()
 * @model
 * @generated
 */
public interface InternalDependency extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Input)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependency_Source()
	 * @model required="true"
	 * @generated
	 */
	Input getSource();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependency#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Input value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependency_Target()
	 * @model required="true"
	 * @generated
	 */
	Output getTarget();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependency#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Output value);

} // InternalDependency
