/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependencyIN.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Internal Dependency IN</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDI <em>Source IDI</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDI <em>Target IDI</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDIC <em>Source IDIC</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDIC <em>Target IDIC</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyIN()
 * @model
 * @generated
 */
public interface InternalDependencyIN extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Source IDI</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source IDI</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source IDI</em>' reference.
	 * @see #setSourceIDI(Input)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyIN_SourceIDI()
	 * @model required="true"
	 * @generated
	 */
	Input getSourceIDI();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDI <em>Source IDI</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source IDI</em>' reference.
	 * @see #getSourceIDI()
	 * @generated
	 */
	void setSourceIDI(Input value);

	/**
	 * Returns the value of the '<em><b>Target IDI</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target IDI</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target IDI</em>' reference.
	 * @see #setTargetIDI(INInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyIN_TargetIDI()
	 * @model required="true"
	 * @generated
	 */
	INInstance getTargetIDI();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDI <em>Target IDI</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target IDI</em>' reference.
	 * @see #getTargetIDI()
	 * @generated
	 */
	void setTargetIDI(INInstance value);

	/**
	 * Returns the value of the '<em><b>Source IDIC</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source IDIC</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source IDIC</em>' reference.
	 * @see #setSourceIDIC(InCondPort)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyIN_SourceIDIC()
	 * @model required="true"
	 * @generated
	 */
	InCondPort getSourceIDIC();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyIN#getSourceIDIC <em>Source IDIC</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source IDIC</em>' reference.
	 * @see #getSourceIDIC()
	 * @generated
	 */
	void setSourceIDIC(InCondPort value);

	/**
	 * Returns the value of the '<em><b>Target IDIC</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target IDIC</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target IDIC</em>' reference.
	 * @see #setTargetIDIC(InCondInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyIN_TargetIDIC()
	 * @model required="true"
	 * @generated
	 */
	InCondInstance getTargetIDIC();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyIN#getTargetIDIC <em>Target IDIC</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target IDIC</em>' reference.
	 * @see #getTargetIDIC()
	 * @generated
	 */
	void setTargetIDIC(InCondInstance value);

} // InternalDependencyIN
