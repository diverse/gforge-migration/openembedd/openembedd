/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: INOUTInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>INOUT Instance</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.INOUTInstance#getOwnerINOUT <em>Owner INOUT</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getINOUTInstance()
 * @model
 * @generated
 */
public interface INOUTInstance extends PortInstance
{
	/**
	 * Returns the value of the '<em><b>Owner INOUT</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner INOUT</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner INOUT</em>' reference.
	 * @see #setOwnerINOUT(INOUT)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getINOUTInstance_OwnerINOUT()
	 * @model required="true"
	 * @generated
	 */
	INOUT getOwnerINOUT();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.INOUTInstance#getOwnerINOUT <em>Owner INOUT</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Owner INOUT</em>' reference.
	 * @see #getOwnerINOUT()
	 * @generated
	 */
	void setOwnerINOUT(INOUT value);

} // INOUTInstance
