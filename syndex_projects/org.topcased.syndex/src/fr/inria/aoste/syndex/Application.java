/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Application.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Application</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Application#getInputPortApplication <em>Input Port Application</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Application#getOutputPortApplication <em>Output Port Application</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Application#getApplicationInternalStructure <em>Application Internal Structure</em>}
 * </li>
 * <li>{@link fr.inria.aoste.syndex.Application#isMain <em>Main</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Application#getPortCondition <em>Port Condition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication()
 * @model
 * @generated
 */
public interface Application extends ApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>Input Port Application</b></em>' containment reference list. The list contents
	 * are of type {@link fr.inria.aoste.syndex.Input}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Port Application</em>' containment reference list isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Input Port Application</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication_InputPortApplication()
	 * @model containment="true"
	 * @generated
	 */
	EList<Input> getInputPortApplication();

	/**
	 * Returns the value of the '<em><b>Output Port Application</b></em>' containment reference list. The list contents
	 * are of type {@link fr.inria.aoste.syndex.Output}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Port Application</em>' containment reference list isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Output Port Application</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication_OutputPortApplication()
	 * @model containment="true"
	 * @generated
	 */
	EList<Output> getOutputPortApplication();

	/**
	 * Returns the value of the '<em><b>Application Internal Structure</b></em>' containment reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Internal Structure</em>' containment reference isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Application Internal Structure</em>' containment reference.
	 * @see #setApplicationInternalStructure(InternalStructure)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication_ApplicationInternalStructure()
	 * @model containment="true" required="true"
	 * @generated
	 */
	InternalStructure getApplicationInternalStructure();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Application#getApplicationInternalStructure
	 * <em>Application Internal Structure</em>}' containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Application Internal Structure</em>' containment reference.
	 * @see #getApplicationInternalStructure()
	 * @generated
	 */
	void setApplicationInternalStructure(InternalStructure value);

	/**
	 * Returns the value of the '<em><b>Main</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main</em>' attribute isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Main</em>' attribute.
	 * @see #setMain(boolean)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication_Main()
	 * @model
	 * @generated
	 */
	boolean isMain();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Application#isMain <em>Main</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Main</em>' attribute.
	 * @see #isMain()
	 * @generated
	 */
	void setMain(boolean value);

	/**
	 * Returns the value of the '<em><b>Port Condition</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.InCondPort}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Condition</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Port Condition</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getApplication_PortCondition()
	 * @model containment="true"
	 * @generated
	 */
	EList<InCondPort> getPortCondition();

} // Application
