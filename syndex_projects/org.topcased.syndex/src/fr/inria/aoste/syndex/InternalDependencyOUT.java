/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: InternalDependencyOUT.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Internal Dependency OUT</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyOUT#getSourceIDO <em>Source IDO</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.InternalDependencyOUT#getTargetIDO <em>Target IDO</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyOUT()
 * @model
 * @generated
 */
public interface InternalDependencyOUT extends ApplicationConnection
{
	/**
	 * Returns the value of the '<em><b>Source IDO</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source IDO</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source IDO</em>' reference.
	 * @see #setSourceIDO(OutInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyOUT_SourceIDO()
	 * @model required="true"
	 * @generated
	 */
	OutInstance getSourceIDO();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyOUT#getSourceIDO <em>Source IDO</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source IDO</em>' reference.
	 * @see #getSourceIDO()
	 * @generated
	 */
	void setSourceIDO(OutInstance value);

	/**
	 * Returns the value of the '<em><b>Target IDO</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target IDO</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target IDO</em>' reference.
	 * @see #setTargetIDO(Output)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getInternalDependencyOUT_TargetIDO()
	 * @model required="true"
	 * @generated
	 */
	Output getTargetIDO();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.InternalDependencyOUT#getTargetIDO <em>Target IDO</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target IDO</em>' reference.
	 * @see #getTargetIDO()
	 * @generated
	 */
	void setTargetIDO(Output value);

} // InternalDependencyOUT
