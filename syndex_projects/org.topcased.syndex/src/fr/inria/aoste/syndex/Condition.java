/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Condition.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Condition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Condition#getValueCondition <em>Value Condition</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Condition#getConditionPort <em>Condition Port</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Condition#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getCondition()
 * @model
 * @generated
 */
public interface Condition extends NamedElement, InternalStructure
{
	/**
	 * Returns the value of the '<em><b>Value Condition</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Condition</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value Condition</em>' attribute.
	 * @see #setValueCondition(String)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCondition_ValueCondition()
	 * @model
	 * @generated
	 */
	String getValueCondition();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Condition#getValueCondition <em>Value Condition</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Value Condition</em>' attribute.
	 * @see #getValueCondition()
	 * @generated
	 */
	void setValueCondition(String value);

	/**
	 * Returns the value of the '<em><b>Condition Port</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Port</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Condition Port</em>' reference.
	 * @see #setConditionPort(InCondPort)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCondition_ConditionPort()
	 * @model required="true"
	 * @generated
	 */
	InCondPort getConditionPort();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Condition#getConditionPort <em>Condition Port</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Condition Port</em>' reference.
	 * @see #getConditionPort()
	 * @generated
	 */
	void setConditionPort(InCondPort value);

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.ComponentInstance}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Component Instance</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCondition_ComponentInstance()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentInstance> getComponentInstance();

	/**
	 * Returns the value of the '<em><b>Condition Port Reference</b></em>' containment reference list. The list contents
	 * are of type {@link fr.inria.aoste.syndex.Port}. It is bidirectional and its opposite is '
	 * {@link fr.inria.aoste.syndex.Port#getParentCondition <em>Parent Condition</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Port Reference</em>' containment reference list isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Condition Port Reference</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getCondition_ConditionPortReference()
	 * @see fr.inria.aoste.syndex.Port#getParentCondition
	 * @model type="fr.inria.aoste.syndex.Port" opposite="parentCondition" containment="true" required="true"
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	EList getConditionPortReference();

} // Condition
