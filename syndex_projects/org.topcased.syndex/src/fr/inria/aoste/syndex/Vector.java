/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Vector.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Vector</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Vector#getVector <em>Vector</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getVector()
 * @model
 * @generated
 */
public interface Vector extends Size
{
	/**
	 * Returns the value of the '<em><b>Vector</b></em>' attribute. The default value is <code>""</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vector</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Vector</em>' attribute.
	 * @see #setVector(String)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getVector_Vector()
	 * @model default="" required="true"
	 * @generated
	 */
	String getVector();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Vector#getVector <em>Vector</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Vector</em>' attribute.
	 * @see #getVector()
	 * @generated
	 */
	void setVector(String value);

} // Vector
