/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Duration.java,v 1.1 2008-11-18 10:43:59 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Duration</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Duration#getEnumDuration <em>Enum Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Duration#getParentDuration <em>Parent Duration</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getDuration()
 * @model
 * @generated
 */
public interface Duration extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Enum Duration</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.EnumLitDuration}. It is bidirectional and its opposite is '
	 * {@link fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration <em>Parent Enum Duration</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Duration</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Enum Duration</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDuration_EnumDuration()
	 * @see fr.inria.aoste.syndex.EnumLitDuration#getParentEnumDuration
	 * @model opposite="parentEnumDuration" containment="true"
	 * @generated
	 */
	EList<EnumLitDuration> getEnumDuration();

	/**
	 * Returns the value of the '<em><b>Parent Duration</b></em>' reference list. The list contents are of type
	 * {@link fr.inria.aoste.syndex.HardwareComponent}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Duration</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Duration</em>' reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getDuration_ParentDuration()
	 * @model
	 * @generated
	 */
	EList<HardwareComponent> getParentDuration();

} // Duration
