/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConnectionHW.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Connection HW</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ConnectionHW#getSourceInOut <em>Source In Out</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ConnectionHW#getTargetInOut <em>Target In Out</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getConnectionHW()
 * @model
 * @generated
 */
public interface ConnectionHW extends HWConnection
{
	/**
	 * Returns the value of the '<em><b>Source In Out</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source In Out</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source In Out</em>' reference.
	 * @see #setSourceInOut(INOUTInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConnectionHW_SourceInOut()
	 * @model required="true"
	 * @generated
	 */
	INOUTInstance getSourceInOut();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ConnectionHW#getSourceInOut <em>Source In Out</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source In Out</em>' reference.
	 * @see #getSourceInOut()
	 * @generated
	 */
	void setSourceInOut(INOUTInstance value);

	/**
	 * Returns the value of the '<em><b>Target In Out</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target In Out</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target In Out</em>' reference.
	 * @see #setTargetInOut(INOUTInstance)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConnectionHW_TargetInOut()
	 * @model required="true"
	 * @generated
	 */
	INOUTInstance getTargetInOut();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ConnectionHW#getTargetInOut <em>Target In Out</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target In Out</em>' reference.
	 * @see #getTargetInOut()
	 * @generated
	 */
	void setTargetInOut(INOUTInstance value);

} // ConnectionHW
