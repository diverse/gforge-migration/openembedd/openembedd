/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Array.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Array</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Array#getVectors <em>Vectors</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getArray()
 * @model
 * @generated
 */
public interface Array extends Size
{
	/**
	 * Returns the value of the '<em><b>Vectors</b></em>' containment reference list. The list contents are of type
	 * {@link fr.inria.aoste.syndex.Vector}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vectors</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Vectors</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getArray_Vectors()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Vector> getVectors();

} // Array
