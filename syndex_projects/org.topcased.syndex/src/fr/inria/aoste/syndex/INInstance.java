/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: INInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>IN Instance</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.INInstance#getOwnerInput <em>Owner Input</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getINInstance()
 * @model
 * @generated
 */
public interface INInstance extends PortInstance
{
	/**
	 * Returns the value of the '<em><b>Owner Input</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner Input</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner Input</em>' reference.
	 * @see #setOwnerInput(Input)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getINInstance_OwnerInput()
	 * @model required="true"
	 * @generated
	 */
	Input getOwnerInput();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.INInstance#getOwnerInput <em>Owner Input</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Owner Input</em>' reference.
	 * @see #getOwnerInput()
	 * @generated
	 */
	void setOwnerInput(Input value);

} // INInstance
