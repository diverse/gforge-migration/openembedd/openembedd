/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Actuator.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Actuator</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Actuator#getInputPortActuator <em>Input Port Actuator</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getActuator()
 * @model annotation="http://www.topcased.org/uuid uuid='11501873764302'"
 * @generated
 */
public interface Actuator extends AtomicApplicationComponent
{
	/**
	 * Returns the value of the '<em><b>Input Port Actuator</b></em>' containment reference list. The list contents are
	 * of type {@link fr.inria.aoste.syndex.Input}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Port Actuator</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Input Port Actuator</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getActuator_InputPortActuator()
	 * @model containment="true"
	 * @generated
	 */
	EList<Input> getInputPortActuator();

} // Actuator
