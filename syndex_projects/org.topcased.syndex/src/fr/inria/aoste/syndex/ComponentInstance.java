/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ComponentInstance.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Component Instance</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ComponentInstance#getPortInstances <em>Port Instances</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ComponentInstance#getReferencedComponent <em>Referenced Component</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ComponentInstance#getRepetitionFactor <em>Repetition Factor</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ComponentInstance#getParameterValues <em>Parameter Values</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ComponentInstance#getReferencedHWComponent <em>Referenced HW Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends Instance
{
	/**
	 * Returns the value of the '<em><b>Port Instances</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.PortInstance}. It is bidirectional and its opposite is '
	 * {@link fr.inria.aoste.syndex.PortInstance#getParentComponentInstance <em>Parent Component Instance</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Instances</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Port Instances</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance_PortInstances()
	 * @see fr.inria.aoste.syndex.PortInstance#getParentComponentInstance
	 * @model opposite="parentComponentInstance" containment="true"
	 * @generated
	 */
	EList<PortInstance> getPortInstances();

	/**
	 * Returns the value of the '<em><b>Referenced Component</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Component</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Referenced Component</em>' reference.
	 * @see #setReferencedComponent(ApplicationComponent)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance_ReferencedComponent()
	 * @model
	 * @generated
	 */
	ApplicationComponent getReferencedComponent();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ComponentInstance#getReferencedComponent
	 * <em>Referenced Component</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Referenced Component</em>' reference.
	 * @see #getReferencedComponent()
	 * @generated
	 */
	void setReferencedComponent(ApplicationComponent value);

	/**
	 * Returns the value of the '<em><b>Repetition Factor</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repetition Factor</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Repetition Factor</em>' attribute.
	 * @see #setRepetitionFactor(int)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance_RepetitionFactor()
	 * @model
	 * @generated
	 */
	int getRepetitionFactor();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ComponentInstance#getRepetitionFactor
	 * <em>Repetition Factor</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Repetition Factor</em>' attribute.
	 * @see #getRepetitionFactor()
	 * @generated
	 */
	void setRepetitionFactor(int value);

	/**
	 * Returns the value of the '<em><b>Parameter Values</b></em>' containment reference list. The list contents are of
	 * type {@link fr.inria.aoste.syndex.ParameterValue}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Values</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parameter Values</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance_ParameterValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterValue> getParameterValues();

	/**
	 * Returns the value of the '<em><b>Referenced HW Component</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced HW Component</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Referenced HW Component</em>' reference.
	 * @see #setReferencedHWComponent(HardwareComponent)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getComponentInstance_ReferencedHWComponent()
	 * @model
	 * @generated
	 */
	HardwareComponent getReferencedHWComponent();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.ComponentInstance#getReferencedHWComponent
	 * <em>Referenced HW Component</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Referenced HW Component</em>' reference.
	 * @see #getReferencedHWComponent()
	 * @generated
	 */
	void setReferencedHWComponent(HardwareComponent value);

} // ComponentInstance
