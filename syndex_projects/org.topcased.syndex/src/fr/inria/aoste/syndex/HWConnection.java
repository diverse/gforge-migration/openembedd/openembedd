/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: HWConnection.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>HW Connection</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.HWConnection#getParentHWConnection <em>Parent HW Connection</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getHWConnection()
 * @model abstract="true"
 * @generated
 */
public interface HWConnection extends Connection
{
	/**
	 * Returns the value of the '<em><b>Parent HW Connection</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link fr.inria.aoste.syndex.HardwareComponent#getHWConnection <em>HW Connection</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent HW Connection</em>' container reference isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent HW Connection</em>' container reference.
	 * @see #setParentHWConnection(HardwareComponent)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getHWConnection_ParentHWConnection()
	 * @see fr.inria.aoste.syndex.HardwareComponent#getHWConnection
	 * @model opposite="HWConnection" required="true" transient="false"
	 * @generated
	 */
	HardwareComponent getParentHWConnection();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.HWConnection#getParentHWConnection
	 * <em>Parent HW Connection</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent HW Connection</em>' container reference.
	 * @see #getParentHWConnection()
	 * @generated
	 */
	void setParentHWConnection(HardwareComponent value);

} // HWConnection
