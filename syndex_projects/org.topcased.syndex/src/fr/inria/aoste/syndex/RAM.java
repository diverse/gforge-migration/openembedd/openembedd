/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: RAM.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>RAM</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getRAM()
 * @model
 * @generated
 */
public interface RAM extends Media
{} // RAM
