/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: ConditionedStructure.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Conditioned Structure</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.ConditionedStructure#getConditions <em>Conditions</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.ConditionedStructure#getPortCond <em>Port Cond</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getConditionedStructure()
 * @model
 * @generated
 */
public interface ConditionedStructure extends InternalStructure
{
	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference list. The list contents are of type
	 * {@link fr.inria.aoste.syndex.Condition}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' containment reference list isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Conditions</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConditionedStructure_Conditions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Condition> getConditions();

	/**
	 * Returns the value of the '<em><b>Port Cond</b></em>' containment reference list. The list contents are of type
	 * {@link fr.inria.aoste.syndex.InCondPort}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Cond</em>' containment reference list isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Port Cond</em>' containment reference list.
	 * @see fr.inria.aoste.syndex.SyndexPackage#getConditionedStructure_PortCond()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<InCondPort> getPortCond();

} // ConditionedStructure
