/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: Processor.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Processor</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.Processor#getParentProcessor <em>Parent Processor</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.Processor#getDurationSW <em>Duration SW</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getProcessor()
 * @model
 * @generated
 */
public interface Processor extends HardwareComponent
{
	/**
	 * Returns the value of the '<em><b>Parent Processor</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Processor</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Processor</em>' reference.
	 * @see #setParentProcessor(Processor)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getProcessor_ParentProcessor()
	 * @model
	 * @generated
	 */
	Processor getParentProcessor();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Processor#getParentProcessor <em>Parent Processor</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Parent Processor</em>' reference.
	 * @see #getParentProcessor()
	 * @generated
	 */
	void setParentProcessor(Processor value);

	/**
	 * Returns the value of the '<em><b>Duration SW</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration SW</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration SW</em>' reference.
	 * @see #setDurationSW(Duration)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getProcessor_DurationSW()
	 * @model
	 * @generated
	 */
	Duration getDurationSW();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.Processor#getDurationSW <em>Duration SW</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration SW</em>' reference.
	 * @see #getDurationSW()
	 * @generated
	 */
	void setDurationSW(Duration value);

} // Processor
