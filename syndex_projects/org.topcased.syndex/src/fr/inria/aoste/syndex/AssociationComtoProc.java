/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: AssociationComtoProc.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Association Comto Proc</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link fr.inria.aoste.syndex.AssociationComtoProc#getSourceAssociationComToProc <em>Source Association Com To
 * Proc </em>}</li>
 * <li>{@link fr.inria.aoste.syndex.AssociationComtoProc#getDuration <em>Duration</em>}</li>
 * <li>{@link fr.inria.aoste.syndex.AssociationComtoProc#getTargetHWProcess <em>Target HW Process</em>}</li>
 * </ul>
 * </p>
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationComtoProc()
 * @model
 * @generated
 */
public interface AssociationComtoProc extends Caracteristics
{
	/**
	 * Returns the value of the '<em><b>Source Association Com To Proc</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Association Com To Proc</em>' reference isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Association Com To Proc</em>' reference.
	 * @see #setSourceAssociationComToProc(Media)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationComtoProc_SourceAssociationComToProc()
	 * @model required="true"
	 * @generated
	 */
	Media getSourceAssociationComToProc();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationComtoProc#getSourceAssociationComToProc
	 * <em>Source Association Com To Proc</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Association Com To Proc</em>' reference.
	 * @see #getSourceAssociationComToProc()
	 * @generated
	 */
	void setSourceAssociationComToProc(Media value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration</em>' reference.
	 * @see #setDuration(Duration)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationComtoProc_Duration()
	 * @model required="true"
	 * @generated
	 */
	Duration getDuration();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationComtoProc#getDuration <em>Duration</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration</em>' reference.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Duration value);

	/**
	 * Returns the value of the '<em><b>Target HW Process</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target HW Process</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target HW Process</em>' reference.
	 * @see #setTargetHWProcess(Processor)
	 * @see fr.inria.aoste.syndex.SyndexPackage#getAssociationComtoProc_TargetHWProcess()
	 * @model required="true"
	 * @generated
	 */
	Processor getTargetHWProcess();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.syndex.AssociationComtoProc#getTargetHWProcess
	 * <em>Target HW Process</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target HW Process</em>' reference.
	 * @see #getTargetHWProcess()
	 * @generated
	 */
	void setTargetHWProcess(Processor value);

} // AssociationComtoProc
