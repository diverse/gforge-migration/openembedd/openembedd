/**
 * The SynDEX metamodel is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * $Id: SyndexFactory.java,v 1.1 2008-11-18 10:44:00 cbrunett Exp $
 */
package fr.inria.aoste.syndex;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a create method for each non-abstract class of
 * the model. <!-- end-user-doc -->
 * 
 * @see fr.inria.aoste.syndex.SyndexPackage
 * @generated
 */
public interface SyndexFactory extends EFactory
{
	/**
	 * The singleton instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	SyndexFactory	eINSTANCE	= fr.inria.aoste.syndex.impl.SyndexFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Actuator</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Actuator</em>'.
	 * @generated
	 */
	Actuator createActuator();

	/**
	 * Returns a new object of class '<em>Sensor</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Sensor</em>'.
	 * @generated
	 */
	Sensor createSensor();

	/**
	 * Returns a new object of class '<em>Application</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Application</em>'.
	 * @generated
	 */
	Application createApplication();

	/**
	 * Returns a new object of class '<em>Input</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Input</em>'.
	 * @generated
	 */
	Input createInput();

	/**
	 * Returns a new object of class '<em>Output</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Output</em>'.
	 * @generated
	 */
	Output createOutput();

	/**
	 * Returns a new object of class '<em>Function</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Function</em>'.
	 * @generated
	 */
	Function createFunction();

	/**
	 * Returns a new object of class '<em>Constant</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Constant</em>'.
	 * @generated
	 */
	Constant createConstant();

	/**
	 * Returns a new object of class '<em>Delay</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Delay</em>'.
	 * @generated
	 */
	Delay createDelay();

	/**
	 * Returns a new object of class '<em>Data Connection</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Data Connection</em>'.
	 * @generated
	 */
	DataConnection createDataConnection();

	/**
	 * Returns a new object of class '<em>Component Instance</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Component Instance</em>'.
	 * @generated
	 */
	ComponentInstance createComponentInstance();

	/**
	 * Returns a new object of class '<em>IN Instance</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>IN Instance</em>'.
	 * @generated
	 */
	INInstance createINInstance();

	/**
	 * Returns a new object of class '<em>Out Instance</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Out Instance</em>'.
	 * @generated
	 */
	OutInstance createOutInstance();

	/**
	 * Returns a new object of class '<em>Internal Dependency IN</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Internal Dependency IN</em>'.
	 * @generated
	 */
	InternalDependencyIN createInternalDependencyIN();

	/**
	 * Returns a new object of class '<em>Internal Dependency OUT</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Internal Dependency OUT</em>'.
	 * @generated
	 */
	InternalDependencyOUT createInternalDependencyOUT();

	/**
	 * Returns a new object of class '<em>Connecteable Element</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Connecteable Element</em>'.
	 * @generated
	 */
	ConnecteableElement createConnecteableElement();

	/**
	 * Returns a new object of class '<em>In Cond Port</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>In Cond Port</em>'.
	 * @generated
	 */
	InCondPort createInCondPort();

	/**
	 * Returns a new object of class '<em>Hardware</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Hardware</em>'.
	 * @generated
	 */
	Hardware createHardware();

	/**
	 * Returns a new object of class '<em>Media</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Media</em>'.
	 * @generated
	 */
	Media createMedia();

	/**
	 * Returns a new object of class '<em>FPGA</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>FPGA</em>'.
	 * @generated
	 */
	FPGA createFPGA();

	/**
	 * Returns a new object of class '<em>Asic</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Asic</em>'.
	 * @generated
	 */
	Asic createAsic();

	/**
	 * Returns a new object of class '<em>DSP</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>DSP</em>'.
	 * @generated
	 */
	DSP createDSP();

	/**
	 * Returns a new object of class '<em>RAM</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>RAM</em>'.
	 * @generated
	 */
	RAM createRAM();

	/**
	 * Returns a new object of class '<em>SAM</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SAM</em>'.
	 * @generated
	 */
	SAM createSAM();

	/**
	 * Returns a new object of class '<em>SAMPTP</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>SAMPTP</em>'.
	 * @generated
	 */
	SAMPTP createSAMPTP();

	/**
	 * Returns a new object of class '<em>Connection HW</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Connection HW</em>'.
	 * @generated
	 */
	ConnectionHW createConnectionHW();

	/**
	 * Returns a new object of class '<em>Association SWto HW</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Association SWto HW</em>'.
	 * @generated
	 */
	AssociationSWtoHW createAssociationSWtoHW();

	/**
	 * Returns a new object of class '<em>Association Comto Proc</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Association Comto Proc</em>'.
	 * @generated
	 */
	AssociationComtoProc createAssociationComtoProc();

	/**
	 * Returns a new object of class '<em>Duration</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Duration</em>'.
	 * @generated
	 */
	Duration createDuration();

	/**
	 * Returns a new object of class '<em>Enum Lit Duration</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Enum Lit Duration</em>'.
	 * @generated
	 */
	EnumLitDuration createEnumLitDuration();

	/**
	 * Returns a new object of class '<em>Datatype</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Datatype</em>'.
	 * @generated
	 */
	Datatype createDatatype();

	/**
	 * Returns a new object of class '<em>Size</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Size</em>'.
	 * @generated
	 */
	Size createSize();

	/**
	 * Returns a new object of class '<em>Array</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Array</em>'.
	 * @generated
	 */
	Array createArray();

	/**
	 * Returns a new object of class '<em>Vector</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Vector</em>'.
	 * @generated
	 */
	Vector createVector();

	/**
	 * Returns a new object of class '<em>Processor</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Processor</em>'.
	 * @generated
	 */
	Processor createProcessor();

	/**
	 * Returns a new object of class '<em>Syn DEx</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Syn DEx</em>'.
	 * @generated
	 */
	SynDEx createSynDEx();

	/**
	 * Returns a new object of class '<em>Elementary Structure</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Elementary Structure</em>'.
	 * @generated
	 */
	ElementaryStructure createElementaryStructure();

	/**
	 * Returns a new object of class '<em>Compound Structure</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Compound Structure</em>'.
	 * @generated
	 */
	CompoundStructure createCompoundStructure();

	/**
	 * Returns a new object of class '<em>INOUT</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>INOUT</em>'.
	 * @generated
	 */
	INOUT createINOUT();

	/**
	 * Returns a new object of class '<em>Internal Data Connection</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Internal Data Connection</em>'.
	 * @generated
	 */
	InternalDataConnection createInternalDataConnection();

	/**
	 * Returns a new object of class '<em>Parameter Declaration</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Parameter Declaration</em>'.
	 * @generated
	 */
	ParameterDeclaration createParameterDeclaration();

	/**
	 * Returns a new object of class '<em>Parameter Value</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Parameter Value</em>'.
	 * @generated
	 */
	ParameterValue createParameterValue();

	/**
	 * Returns a new object of class '<em>Conditioned Structure</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Conditioned Structure</em>'.
	 * @generated
	 */
	ConditionedStructure createConditionedStructure();

	/**
	 * Returns a new object of class '<em>Condition</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Condition</em>'.
	 * @generated
	 */
	Condition createCondition();

	/**
	 * Returns a new object of class '<em>Repetition Connection</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Repetition Connection</em>'.
	 * @generated
	 */
	RepetitionConnection createRepetitionConnection();

	/**
	 * Returns a new object of class '<em>INOUT Instance</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>INOUT Instance</em>'.
	 * @generated
	 */
	INOUTInstance createINOUTInstance();

	/**
	 * Returns a new object of class '<em>In Cond Instance</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>In Cond Instance</em>'.
	 * @generated
	 */
	InCondInstance createInCondInstance();

	/**
	 * Returns a new object of class '<em>Internal Dependency</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Internal Dependency</em>'.
	 * @generated
	 */
	InternalDependency createInternalDependency();

	/**
	 * Returns the package supported by this factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	SyndexPackage getSyndexPackage();

} // SyndexFactory
