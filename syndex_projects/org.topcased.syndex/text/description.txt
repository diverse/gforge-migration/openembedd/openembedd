Model Syndex

This model description is not a real EMF artifact. It was generated by the
org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's
code generator can be extended.
This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.

Package syndex <http://www.inria.fr/aoste/SynDEx/1.1>

  Class Actuator -> AtomicApplicationComponent
    Reference inputPortActuator : Input<<0..*>>

  Class AtomicApplicationComponent -> ApplicationComponent
    Reference atomicApplicationStructure : ElementaryStructure<<1..1>>

  Class Port -> NamedElement, ConnecteableElement
    Reference parentPort : Component<<1..1>>
    Reference sizePort : Size<<1..1>>
    Attribute typePort : DataTypeK<<1..1>>

  Class NamedElement -> Element
    Attribute name : EString

  Class ApplicationComponent -> Component
    Reference paramDeclarations : ParameterDeclaration<<0..*>>

  Class Sensor -> AtomicApplicationComponent
    Reference outputPortSensor : Output<<0..*>>

  Class Application -> ApplicationComponent
    Reference inputPortApplication : Input<<0..*>>
    Reference outputPortApplication : Output<<0..*>>
    Reference applicationInternalStructure : InternalStructure<<1..1>>
    Attribute main : EBoolean
    Reference portCondition : InCondPort<<0..*>>

  Class Input -> Port

  Class Component -> NamedElement
    Attribute executionPhases : CodeExecutionPhaseKind<<0..*>>

  Class Output -> Port

  Class Function -> AtomicApplicationComponent
    Reference inputPortFunction : Input<<1..*>>
    Reference outputPortFunction : Output<<1..*>>

  Class Constant -> AtomicApplicationComponent
    Reference oneOutputConstant : Output<<1..1>>
    Attribute constantValue : EInt<<1..1>>
    Attribute enumValues : EInt<<0..*>>

  Class Delay -> AtomicApplicationComponent
    Reference oneInputDelay : Input<<1..1>>
    Reference oneOutputDelay : Output<<1..1>>
    Attribute delayValue : EInt
    Attribute enumValue : EInt<<0..*>>

  Class Connection -> Element

  Class DataConnection -> ApplicationConnection
    Reference sourceDataConnection : Output<<1..1>>
    Reference targetDataconnection : Input<<1..1>>

  Class Instance -> NamedElement, Element

  Class ComponentInstance -> Instance
    Reference portInstances : PortInstance<<0..*>>
    Reference referencedComponent : ApplicationComponent
    Attribute repetitionFactor : EInt
    Reference parameterValues : ParameterValue<<0..*>>
    Reference referencedHWComponent : HardwareComponent

  Class PortInstance -> Instance, ConnecteableElement
    Reference parentComponentInstance : ComponentInstance

  Class INInstance -> PortInstance
    Reference ownerInput : Input<<1..1>>

  Class OutInstance -> PortInstance
    Reference ownerOutput : Output<<1..1>>

  Class InternalDependencyIN -> ApplicationConnection
    Reference sourceIDI : Input<<1..1>>
    Reference targetIDI : INInstance<<1..1>>
    Reference sourceIDIC : InCondPort<<1..1>>
    Reference targetIDIC : InCondInstance<<1..1>>

  Class InternalDependencyOUT -> ApplicationConnection
    Reference sourceIDO : OutInstance<<1..1>>
    Reference targetIDO : Output<<1..1>>

  Class ConnecteableElement -> NamedElement, Element

  Class InCondPort -> Port

  Class HardwareComponent -> Component
    Reference HWConnection : HWConnection<<0..*>>
    Reference inOutHWComponent : INOUT<<0..*>>

  Class Hardware -> HardwareComponent
    Reference ComponentHWInstance : ComponentInstance<<1..*>>
    Attribute main : EBoolean

  Class Media -> HardwareComponent
    Attribute bandwidth : EInt<<1..1>>
    Attribute size : EInt<<1..1>>
    Reference parentMemory : Media
    Attribute broadcast : EBoolean
    Reference duration : Duration

  Class FPGA -> Processor

  Class Asic -> Processor

  Class DSP -> Processor

  Class RAM -> Media

  Class SAM -> Media

  Class SAMPTP -> Media

  Class HWConnection -> Connection
    Reference parentHWConnection : HardwareComponent<<1..1>>

  Class ConnectionHW -> HWConnection
    Reference sourceInOut : INOUTInstance<<1..1>>
    Reference targetInOut : INOUTInstance<<1..1>>

  Class Caracteristics -> NamedElement
    Attribute adequation : EBoolean<<1..1>>

  Class AssociationSWtoHW -> Caracteristics
    Attribute duration : EInt<<1..1>>
    Reference sourceAppli : ComponentInstance<<1..1>>
    Reference targetHW : ComponentInstance<<1..1>>

  Class AssociationComtoProc -> Caracteristics
    Reference sourceAssociationComToProc : Media<<1..1>>
    Reference duration : Duration<<1..1>>
    Reference targetHWProcess : Processor<<1..1>>

  Class Duration -> NamedElement
    Reference enumDuration : EnumLitDuration<<0..*>>
    Reference parentDuration : HardwareComponent<<0..*>>

  Class EnumLitDuration -> NamedElement
    Attribute Value : EInt<<1..1>>
    Reference parentEnumDuration : Duration<<1..1>>

  Class Datatype -> Element

  Class Size -> Element

  Class Array -> Size
    Reference vectors : Vector<<1..*>>

  Class Vector -> Size
    Attribute vector : EString<<1..1>>

  Class Processor -> HardwareComponent
    Reference parentProcessor : Processor
    Reference durationSW : Duration

  Class Element

  Class SynDEx
    Reference SynDExelements : Element<<0..*>>

  Class ApplicationConnection -> Connection
    Reference parentCoumpoundApplicationConnection : CompoundStructure
    Reference parentConditionApplicationConnection : ConditionedStructure

  Class InternalStructure -> Element
    Reference ownerInternalStructure : ApplicationComponent<<1..1>>
    Reference internalConnector : ApplicationConnection<<0..*>>

  Class ElementaryStructure -> InternalStructure

  Class CompoundStructure -> InternalStructure
    Reference componentInstance : ComponentInstance<<1..*>>

  Class INOUT -> Port
    Attribute type : EString

  Class InternalDataConnection -> ApplicationConnection
    Reference sourceOutInstance : OutInstance<<1..1>>
    Reference targetINInstance : INInstance<<1..1>>
    Reference targetINInstanceC : InCondInstance<<1..1>>

  Class ParameterDeclaration -> NamedElement

  Class ParameterValue -> Element
    Attribute valueParameter : EString
    Reference paramDeclaration : ParameterDeclaration

  Class ConditionedStructure -> InternalStructure
    Reference conditions : Condition<<0..*>>
    Reference portCond : InCondPort<<1..*>>

  Class Condition -> NamedElement, InternalStructure
    Attribute valueCondition : EString
    Reference conditionPort : InCondPort<<1..1>>
    Reference componentInstance : ComponentInstance<<0..*>>

  Class RepetitionConnection -> ApplicationConnection
    Attribute repetitionKind : RepetitionConnectionKind<<1..1>>
    Reference sourceRepetitionConnection : Size<<1..1>>
    Reference targetRepetitionConnection : Size<<1..1>>

  Class INOUTInstance -> PortInstance
    Reference ownerINOUT : INOUT<<1..1>>

  Class InCondInstance -> PortInstance
    Reference ownerCond : InCondPort<<1..1>>

  Class InternalDependency -> ApplicationConnection
    Reference source : Input<<1..1>>
    Reference target : Output<<1..1>>

  Enum CodeExecutionPhaseKind
    Literal loopseq = 1
    Literal initseq = 2
    Literal endseq = 3

  Enum RepetitionConnectionKind
    Literal fork = 0
    Literal join = 1

  Enum DataTypeK
    Literal int = 0
    Literal double = 1
    Literal float = 2
    Literal String = 3
    Literal bool = 4
