/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.topcased.modeler.commands.GEFtoEMFCommandStackWrapper;
import org.topcased.modeler.documentation.EAnnotationDocPage;
import org.topcased.modeler.documentation.IDocPage;
import org.topcased.modeler.editor.Modeler;

import fr.inria.aoste.syndex.modeler.SyndexPlugin;
import fr.inria.aoste.syndex.modeler.actions.GenerateSDXAction;

/**
 * Generated Model editor
 * 
 * @generated
 */
public class SyndexEditor extends Modeler
{

	/**
	 * @see org.topcased.modeler.editor.Modeler#getAdapterFactories()
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	protected List getAdapterFactories()
	{
		List factories = new ArrayList();
		factories.add(new fr.inria.aoste.syndex.provider.SyndexItemProviderAdapterFactory());
		factories.add(new fr.inria.aoste.syndex.modeler.providers.SyndexModelerProviderAdapterFactory());

		factories.addAll(super.getAdapterFactories());

		return factories;
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#getId()
	 * @generated
	 */
	public String getId()
	{
		return "fr.inria.aoste.syndex.modeler.editor.SyndexEditor";
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#getAdapter(java.lang.Class)
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public Object getAdapter(Class type)
	{
		if (type == IDocPage.class)
		{
			GEFtoEMFCommandStackWrapper wrapper = new GEFtoEMFCommandStackWrapper(getCommandStack());
			return new EAnnotationDocPage(wrapper);
		}
		return super.getAdapter(type);
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#getPreferenceStore()
	 * @generated
	 */
	public IPreferenceStore getPreferenceStore()
	{
		return SyndexPlugin.getDefault().getPreferenceStore();
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#createActions()
	 * @generated NOT
	 */
	protected void createActions()
	{
		super.createActions();

		ActionRegistry registry = getActionRegistry();
		IAction action = new GenerateSDXAction(this);
		registry.registerAction(action);
	}
}
