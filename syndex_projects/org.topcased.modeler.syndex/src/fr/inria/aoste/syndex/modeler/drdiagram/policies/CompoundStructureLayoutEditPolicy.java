/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.ComponentInstance;

/**
 * @generated
 */
public class CompoundStructureLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public CompoundStructureLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof ComponentInstance)
		{
			return true;
		}
		return false;
	}

}
