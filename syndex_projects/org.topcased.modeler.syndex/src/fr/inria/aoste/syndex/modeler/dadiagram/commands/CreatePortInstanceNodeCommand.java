/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.OutInstance;

public class CreatePortInstanceNodeCommand extends CompoundCommand
{
	/** The graphNode that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the editPart in which the new PortInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the ParameterInstance to add
	 */
	public CreatePortInstanceNodeCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Create Port Instance GraphElement Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		initialize();
	}

	@SuppressWarnings("unchecked")
	private void initialize()
	{
		EObject eObject = Utils.getElement(container);

		int nbIn = 0, nbOut = 0, nbcond = 0;

		for (Iterator it = container.getContained().iterator(); it.hasNext();)
		{
			GraphElement elt = (GraphElement) it.next();
			EObject port = Utils.getElement(elt);
			if (port instanceof INInstance)
				nbIn++;
			if (port instanceof OutInstance)
				nbOut++;
			if (port instanceof InCondInstance)
				nbcond++;

		}

		if (eObject instanceof ComponentInstance)
		{
			ComponentInstance mi = (ComponentInstance) eObject;

			if (!mi.getPortInstances().isEmpty()
					&& (nbIn < mi.getPortInstances().size() || nbOut < mi.getPortInstances().size() || nbcond < mi
							.getPortInstances().size()))
				add(new CreatePortInstanceCommand(editDomain, container, creationUtils, mi.getPortInstances()));

			add(new PositionInstanceCommand(container));
		}
	}
}
