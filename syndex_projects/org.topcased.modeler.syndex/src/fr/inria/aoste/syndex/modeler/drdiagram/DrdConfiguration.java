/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

import fr.inria.aoste.syndex.modeler.SyndexPlugin;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ActuatorEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ApplicationEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.CompoundStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConditionEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConditionedStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConstantEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.DelayEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ElementaryStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.FunctionEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.InCondPortEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.InputEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.OutputEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ParameterDeclarationEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ParameterValueEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.SensorEditPart;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 * 
 * @generated
 */
public class DrdConfiguration implements IConfiguration
{
	/**
	 * @generated
	 */
	private DrdPaletteManager	paletteManager;

	/**
	 * @generated
	 */
	private DrdEditPartFactory	editPartFactory;

	/**
	 * @generated
	 */
	private DrdCreationUtils	creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * 
	 * @generated
	 */
	private DiagramGraphConf	diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 * 
	 * @generated
	 */
	public DrdConfiguration()
	{
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 * 
	 * @generated
	 */
	private void registerAdapters()
	{
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ActuatorEditPart.class, fr.inria.aoste.syndex.Actuator.class),
			ActuatorEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(SensorEditPart.class, fr.inria.aoste.syndex.Sensor.class),
			SensorEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ApplicationEditPart.class, fr.inria.aoste.syndex.Application.class),
			ApplicationEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(FunctionEditPart.class, fr.inria.aoste.syndex.Function.class),
			FunctionEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ConstantEditPart.class, fr.inria.aoste.syndex.Constant.class),
			ConstantEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(DelayEditPart.class, fr.inria.aoste.syndex.Delay.class),
			DelayEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InputEditPart.class, fr.inria.aoste.syndex.Input.class),
			InputEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(OutputEditPart.class, fr.inria.aoste.syndex.Output.class),
			OutputEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ComponentInstanceEditPart.class,
				fr.inria.aoste.syndex.ComponentInstance.class), ComponentInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InCondPortEditPart.class, fr.inria.aoste.syndex.InCondPort.class),
			InCondPortEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ConditionEditPart.class, fr.inria.aoste.syndex.Condition.class),
			ConditionEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(CompoundStructureEditPart.class,
				fr.inria.aoste.syndex.CompoundStructure.class), CompoundStructureEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ConditionedStructureEditPart.class,
				fr.inria.aoste.syndex.ConditionedStructure.class), ConditionedStructureEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ElementaryStructureEditPart.class,
				fr.inria.aoste.syndex.ElementaryStructure.class), ElementaryStructureEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ParameterDeclarationEditPart.class,
				fr.inria.aoste.syndex.ParameterDeclaration.class), ParameterDeclarationEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ParameterValueEditPart.class, fr.inria.aoste.syndex.ParameterValue.class),
			ParameterValueEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId()
	{
		return new String("fr.inria.aoste.syndex.modeler.drdiagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName()
	{
		return new String("Definition Definition Diagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			editPartFactory = new DrdEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager()
	{
		if (paletteManager == null)
		{
			paletteManager = new DrdPaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils()
	{
		if (creationUtils == null)
		{
			creationUtils = new DrdCreationUtils(getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf()
	{
		if (diagramGraphConf == null)
		{
			URL url = SyndexPlugin.getDefault().getBundle().getResource(
				"fr/inria/aoste/syndex/modeler/drdiagram/diagram.graphconf");
			if (url != null)
			{
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null && resource.getContents().get(0) instanceof DiagramGraphConf)
				{
					diagramGraphConf = (DiagramGraphConf) resource.getContents().get(0);
				}
			}
			else
			{
				new MissingGraphConfFileException(
					"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}
