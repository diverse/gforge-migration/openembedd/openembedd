/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.figures;

/**
 * @generated
 */
public class VectorFigure extends org.topcased.draw2d.figures.EditableLabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public VectorFigure()
	{
		super();
	}

}
