/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.figures;

import org.eclipse.draw2d.IFigure;
import org.topcased.draw2d.figures.IContainerFigure;
import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.ILabelFigure;

/**
 * @generated NOT
 */

public class CompoundStructureFigure extends org.eclipse.draw2d.Figure implements IContainerFigure, ILabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public CompoundStructureFigure()
	{
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.IContainerFigure#getContentPane()
	 * @generated NOT
	 */
	public IFigure getContentPane()
	{
		// TODO : return the Figure that represent the ContentPane
		return this;
	}

	/**
	 * @see org.topcased.draw2d.figures.ILabelFigure#getLabel()
	 * @generated NOT
	 */
	public ILabel getLabel()
	{
		// TODO : return the Figure that represent the Label
		return null;
	}

}
