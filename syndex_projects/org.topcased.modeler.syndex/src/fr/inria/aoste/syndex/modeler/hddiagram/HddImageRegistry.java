/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.topcased.modeler.ImageRegistry;

import fr.inria.aoste.syndex.modeler.SyndexPlugin;

/**
 * Handle image
 * 
 * @generated
 */
public final class HddImageRegistry
{

	/**
	 * The bundle id of the images
	 * 
	 * @generated
	 */
	public static final String	BUNDLE	= "fr.inria.aoste.syndex.modeler.hddiagram.images";

	/**
	 * The constructor
	 * 
	 * @generated
	 */
	private HddImageRegistry()
	{
	// do nothing
	}

	/**
	 * Clients should not dispose the Image returned.
	 * 
	 * @param key
	 *        the key (one of the constants defined in this class)
	 * @return the Image associated with the given key
	 * @generated
	 */
	public static Image getImage(String key)
	{
		return ImageRegistry.getInstance().get(SyndexPlugin.getDefault().getBundle(), getImageLocation(key));
	}

	/**
	 * Return the image location
	 * 
	 * @param key
	 *        the key
	 * @return the Image location associated with the given key
	 * @generated
	 */
	private static String getImageLocation(String key)
	{
		return ResourceBundle.getBundle(BUNDLE).getString(key);
	}

	/**
	 * Build an image descriptor for the given key
	 * 
	 * @param key
	 *        the key
	 * @return the ImageDescriptor associated with the given key
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(String key)
	{
		try
		{
			return ImageRegistry.getInstance().getDescriptor(SyndexPlugin.getDefault().getBundle(),
				getImageLocation(key));
		}
		catch (MissingResourceException mre)
		{
			return null;
		}
	}

}
