/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Media;
import fr.inria.aoste.syndex.RAM;
import fr.inria.aoste.syndex.SAM;
import fr.inria.aoste.syndex.SAMPTP;
import fr.inria.aoste.syndex.modeler.hddiagram.HddSimpleObjectConstants;

/**
 * Media restore connection command
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class MediaRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public MediaRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof Media)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);
					if (eltObject2 instanceof RAM)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinementMemoryLinkFromRAMToMedia_ParentMemory(elt2, elt);
						}
					}
					if (eltObject2 instanceof SAM)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinementMemoryLinkFromSAMToMedia_ParentMemory(elt2, elt);
						}
					}
					if (eltObject2 instanceof SAMPTP)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinementMemoryLinkFromSAMPTPToMedia_ParentMemory(elt2, elt);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinementMemoryLinkFromRAMToMedia_ParentMemory(GraphElement srcElt, GraphElement targetElt)
	{
		RAM sourceObject = (RAM) Utils.getElement(srcElt);
		Media targetObject = (Media) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentMemory()) && sourceObject.equals(targetObject.getParentMemory()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK).size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK);
				RafinementMemoryLinkEdgeCreationCommand cmd = new RafinementMemoryLinkEdgeCreationCommand(null, edge,
					srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinementMemoryLinkFromSAMToMedia_ParentMemory(GraphElement srcElt, GraphElement targetElt)
	{
		SAM sourceObject = (SAM) Utils.getElement(srcElt);
		Media targetObject = (Media) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentMemory()) && sourceObject.equals(targetObject.getParentMemory()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK).size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK);
				RafinementMemoryLinkEdgeCreationCommand cmd = new RafinementMemoryLinkEdgeCreationCommand(null, edge,
					srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinementMemoryLinkFromSAMPTPToMedia_ParentMemory(GraphElement srcElt, GraphElement targetElt)
	{
		SAMPTP sourceObject = (SAMPTP) Utils.getElement(srcElt);
		Media targetObject = (Media) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentMemory()) && sourceObject.equals(targetObject.getParentMemory()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK).size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK);
				RafinementMemoryLinkEdgeCreationCommand cmd = new RafinementMemoryLinkEdgeCreationCommand(null, edge,
					srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

}
