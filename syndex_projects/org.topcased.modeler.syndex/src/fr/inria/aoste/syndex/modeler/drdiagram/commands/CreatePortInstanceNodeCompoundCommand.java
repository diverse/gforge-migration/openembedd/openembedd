/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

@SuppressWarnings("unchecked")
public class CreatePortInstanceNodeCompoundCommand extends CompoundCommand
{
	/** The graphNode that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the editPart in which the new PortInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the ParameterInstance to add
	 */
	public CreatePortInstanceNodeCompoundCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Create Port GraphElement Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		initialize();
	}

	private void initialize()
	{
		EObject eObject = Utils.getElement(container);

		int nbIn = 0, nbOut = 0;

		for (Iterator it = container.getContained().iterator(); it.hasNext();)
		{
			GraphElement elt = (GraphElement) it.next();
			EObject port = Utils.getElement(elt);
			if (port instanceof Input)
				nbIn++;
			else if (port instanceof Output)
				nbOut++;

		}

		if (eObject instanceof CompoundStructure)
		{
			CompoundStructure mi = (CompoundStructure) eObject;

			if (!mi.getPortCompoundStructure().isEmpty()
					&& (nbIn < mi.getPortCompoundStructure().size() || nbOut < mi.getPortCompoundStructure().size()))
				add(new CreatePortInstanceCompoundCommand(editDomain, container, creationUtils, mi
						.getPortCompoundStructure()));

			add(new PositionInstanceCompoundCommand(container));
		}
	}
}
