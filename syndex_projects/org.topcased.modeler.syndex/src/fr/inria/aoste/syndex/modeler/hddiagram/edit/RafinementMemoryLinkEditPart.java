/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.topcased.modeler.ModelerColorConstants;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.edit.GraphEdgeEditPart;

import fr.inria.aoste.syndex.modeler.hddiagram.figures.RafinementMemoryLinkFigure;

/**
 * RafinementMemoryLink controller
 * 
 * @generated
 */
public class RafinementMemoryLinkEditPart extends GraphEdgeEditPart
{

	/**
	 * Constructor
	 * 
	 * @param model
	 *        the graph object
	 * @generated
	 */
	public RafinementMemoryLinkEditPart(GraphEdge model)
	{
		super(model);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.CHANGE_FONT_EDITPOLICY, null);

	}

	/**
	 * @return the Figure
	 * @generated
	 */
	protected IFigure createFigure()
	{
		RafinementMemoryLinkFigure connection = new RafinementMemoryLinkFigure();

		createTargetDecoration(connection);

		return connection;
	}

	/**
	 * @param connection
	 *        the PolylineConnection
	 * @generated
	 */
	private void createTargetDecoration(PolylineConnection connection)
	{

		PolygonDecoration decoration = new PolygonDecoration();
		decoration.setScale(14, 6);
		decoration.setBackgroundColor(ModelerColorConstants.white);
		connection.setTargetDecoration(decoration);

	}

}
