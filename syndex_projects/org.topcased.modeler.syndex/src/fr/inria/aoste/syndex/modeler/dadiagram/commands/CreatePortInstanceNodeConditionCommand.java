/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

@SuppressWarnings("unchecked")
public class CreatePortInstanceNodeConditionCommand extends CompoundCommand
{
	/** The graphNode that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the editPart in which the new PortInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the ParameterInstance to add
	 */
	public CreatePortInstanceNodeConditionCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Create Port GraphElement Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		initialize();
	}

	private void initialize()
	{
		EObject eObject = Utils.getElement(container);

		int nbIn = 0, nbOut = 0;
		for (Iterator it = container.getContained().iterator(); it.hasNext();)
		{
			GraphElement elt = (GraphElement) it.next();
			EObject port = Utils.getElement(elt);
			if (port instanceof Input)
				nbIn++;
			else if (port instanceof Output)
				nbOut++;

		}

		if (eObject instanceof Condition)
		{
			Condition mi = (Condition) eObject;

			if (!mi.getConditionPortReference().isEmpty()
					&& (nbIn < mi.getConditionPortReference().size() || nbOut < mi.getConditionPortReference().size()))
				add(new CreatePortInstanceConditionCommand(editDomain, container, creationUtils, mi
						.getConditionPortReference()));

			add(new PositionInstanceConditionCommand(container));
		}
	}
}
