/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 * 
 * @generated
 */
public interface DrdEditPolicyConstants
{

	/**
	 * The key used to install an <i>Actuator</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	ACTUATOR_EDITPOLICY				= "Actuator EditPolicy";

	/**
	 * The key used to install an <i>Sensor</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	SENSOR_EDITPOLICY				= "Sensor EditPolicy";

	/**
	 * The key used to install an <i>Application</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	APPLICATION_EDITPOLICY			= "Application EditPolicy";

	/**
	 * The key used to install an <i>Function</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	FUNCTION_EDITPOLICY				= "Function EditPolicy";

	/**
	 * The key used to install an <i>Constant</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CONSTANT_EDITPOLICY				= "Constant EditPolicy";

	/**
	 * The key used to install an <i>Delay</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	DELAY_EDITPOLICY				= "Delay EditPolicy";

	/**
	 * The key used to install an <i>Input</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INPUT_EDITPOLICY				= "Input EditPolicy";

	/**
	 * The key used to install an <i>Output</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	OUTPUT_EDITPOLICY				= "Output EditPolicy";

	/**
	 * The key used to install an <i>ComponentInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	COMPONENTINSTANCE_EDITPOLICY	= "ComponentInstance EditPolicy";

	/**
	 * The key used to install an <i>InCondPort</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INCONDPORT_EDITPOLICY			= "InCondPort EditPolicy";

	/**
	 * The key used to install an <i>Condition</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CONDITION_EDITPOLICY			= "Condition EditPolicy";

	/**
	 * The key used to install an <i>CompoundStructure</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	COMPOUNDSTRUCTURE_EDITPOLICY	= "CompoundStructure EditPolicy";

	/**
	 * The key used to install an <i>ConditionedStructure</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CONDITIONEDSTRUCTURE_EDITPOLICY	= "ConditionedStructure EditPolicy";

	/**
	 * The key used to install an <i>ElementaryStructure</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	ELEMENTARYSTRUCTURE_EDITPOLICY	= "ElementaryStructure EditPolicy";

	/**
	 * The key used to install an <i>ParameterDeclaration</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PARAMETERDECLARATION_EDITPOLICY	= "ParameterDeclaration EditPolicy";

	/**
	 * The key used to install an <i>ParameterValue</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PARAMETERVALUE_EDITPOLICY		= "ParameterValue EditPolicy";

}
