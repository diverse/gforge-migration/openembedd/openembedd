/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.GraphEdgeEditPart;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.modeler.hddiagram.edit.AsicEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ConnectionHWEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.DSPEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.DurationEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.EnumLitDurationEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.FPGAEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.HardwareEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.HddDiagramEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.INOUTEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.INOUTInstanceEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.MediaEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ProcessorEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.RAMEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.RafinementMemoryLinkEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.RafinnementProcessLinkEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.SAMEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.SAMPTPEditPart;
import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * Part Factory : associates a model object to its controller. <br>
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class HddEditPartFactory implements EditPartFactory
{
	/**
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,java.lang.Object)
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof Diagram)
		{
			return new HddDiagramEditPart((Diagram) model);
		}
		else if (model instanceof GraphNode)
		{
			final GraphNode node = (GraphNode) model;
			EObject element = Utils.getElement(node);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new NodeSyndexSwitch(node).doSwitch(element);
				}

				return new EMFGraphNodeEditPart(node);
			}

			if (node.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
			}
			return new GraphNodeEditPart(node);
		}
		else if (model instanceof GraphEdge)
		{
			final GraphEdge edge = (GraphEdge) model;
			EObject element = Utils.getElement(edge);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new EdgeSyndexSwitch(edge).doSwitch(element);
				}

				return new EMFGraphEdgeEditPart(edge);
			}

			if (edge.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
				if (HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK
						.equals(((SimpleSemanticModelElement) edge.getSemanticModel()).getTypeInfo()))
				{
					return new RafinnementProcessLinkEditPart(edge);
				}
				if (HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK
						.equals(((SimpleSemanticModelElement) edge.getSemanticModel()).getTypeInfo()))
				{
					return new RafinementMemoryLinkEditPart(edge);
				}
			}

			return new GraphEdgeEditPart(edge);
		}

		throw new IllegalStateException("No edit part matches with the '" + model.getClass().getName()
				+ "' model element. Check HddEditPartFactory#createEditPart(EditPart,Object) class");
	}

	/**
	 * @generated
	 */
	private class NodeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical node
		 * 
		 * @generated
		 */
		private GraphNode	node;

		/**
		 * Constructor
		 * 
		 * @param node
		 *        the graphical node
		 * @generated
		 */
		public NodeSyndexSwitch(GraphNode node)
		{
			this.node = node;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINOUT(fr.inria.aoste.syndex.INOUT)
		 * @generated
		 */
		public Object caseINOUT(fr.inria.aoste.syndex.INOUT object)
		{
			return new INOUTEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseHardware(fr.inria.aoste.syndex.Hardware)
		 * @generated
		 */
		public Object caseHardware(fr.inria.aoste.syndex.Hardware object)
		{
			return new HardwareEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseFPGA(fr.inria.aoste.syndex.FPGA)
		 * @generated
		 */
		public Object caseFPGA(fr.inria.aoste.syndex.FPGA object)
		{
			return new FPGAEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseAsic(fr.inria.aoste.syndex.Asic)
		 * @generated
		 */
		public Object caseAsic(fr.inria.aoste.syndex.Asic object)
		{
			return new AsicEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDSP(fr.inria.aoste.syndex.DSP)
		 * @generated
		 */
		public Object caseDSP(fr.inria.aoste.syndex.DSP object)
		{
			return new DSPEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseRAM(fr.inria.aoste.syndex.RAM)
		 * @generated
		 */
		public Object caseRAM(fr.inria.aoste.syndex.RAM object)
		{
			return new RAMEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSAM(fr.inria.aoste.syndex.SAM)
		 * @generated
		 */
		public Object caseSAM(fr.inria.aoste.syndex.SAM object)
		{
			return new SAMEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSAMPTP(fr.inria.aoste.syndex.SAMPTP)
		 * @generated
		 */
		public Object caseSAMPTP(fr.inria.aoste.syndex.SAMPTP object)
		{
			return new SAMPTPEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseMedia(fr.inria.aoste.syndex.Media)
		 * @generated
		 */
		public Object caseMedia(fr.inria.aoste.syndex.Media object)
		{
			return new MediaEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseProcessor(fr.inria.aoste.syndex.Processor)
		 * @generated
		 */
		public Object caseProcessor(fr.inria.aoste.syndex.Processor object)
		{
			return new ProcessorEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance)
		 * @generated
		 */
		public Object caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance object)
		{
			return new ComponentInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINOUTInstance(fr.inria.aoste.syndex.INOUTInstance)
		 * @generated
		 */
		public Object caseINOUTInstance(fr.inria.aoste.syndex.INOUTInstance object)
		{
			return new INOUTInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseEnumLitDuration(fr.inria.aoste.syndex.EnumLitDuration)
		 * @generated
		 */
		public Object caseEnumLitDuration(fr.inria.aoste.syndex.EnumLitDuration object)
		{
			return new EnumLitDurationEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDuration(fr.inria.aoste.syndex.Duration)
		 * @generated
		 */
		public Object caseDuration(fr.inria.aoste.syndex.Duration object)
		{
			return new DurationEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical edge
		 * 
		 * @generated
		 */
		private GraphEdge	edge;

		/**
		 * Constructor
		 * 
		 * @param edge
		 *        the graphical edge
		 * @generated
		 */
		public EdgeSyndexSwitch(GraphEdge edge)
		{
			this.edge = edge;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConnectionHW(fr.inria.aoste.syndex.ConnectionHW)
		 * @generated
		 */
		public Object caseConnectionHW(fr.inria.aoste.syndex.ConnectionHW object)
		{
			return new ConnectionHWEditPart(edge);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphEdgeEditPart(edge);
		}
	}

}
