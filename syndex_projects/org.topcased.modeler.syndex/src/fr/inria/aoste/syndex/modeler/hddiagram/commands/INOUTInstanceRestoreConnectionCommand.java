/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ConnectionHW;
import fr.inria.aoste.syndex.INOUTInstance;

/**
 * INOUTInstance restore connection command
 * 
 * @generated
 */
public class INOUTInstanceRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public INOUTInstanceRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof INOUTInstance)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);

					if (eltObject2 instanceof INOUTInstance)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if the elt is the source of the edge or if it is the target and that the
							// SourceTargetCouple is reversible
							createConnectionHWFromINOUTInstanceToINOUTInstance(elt, elt2);
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createConnectionHWFromINOUTInstanceToINOUTInstance(elt2, elt);
						}
					}

				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private void createConnectionHWFromINOUTInstanceToINOUTInstance(GraphElement srcElt, GraphElement targetElt)
	{
		EList edgeObjectList = ((fr.inria.aoste.syndex.Hardware) Utils.getDiagramModelObject(srcElt)).getHWConnection();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof ConnectionHW)
			{
				ConnectionHW edgeObject = (ConnectionHW) obj;
				if (false)
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, ConnectionHW.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							ConnectionHWEdgeCreationCommand cmd = new ConnectionHWEdgeCreationCommand(getEditDomain(),
								(GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

}
