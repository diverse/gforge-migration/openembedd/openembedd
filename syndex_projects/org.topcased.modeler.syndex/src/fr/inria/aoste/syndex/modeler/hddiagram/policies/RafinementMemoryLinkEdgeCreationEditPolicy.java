/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy;
import org.topcased.modeler.utils.SourceTargetData;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.modeler.hddiagram.HddSimpleObjectConstants;
import fr.inria.aoste.syndex.modeler.hddiagram.commands.RafinementMemoryLinkEdgeCreationCommand;

/**
 * RafinementMemoryLink edge creation
 * 
 * @generated
 */
public class RafinementMemoryLinkEdgeCreationEditPolicy extends AbstractEdgeCreationEditPolicy
{
	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#createCommand(org.eclipse.gef.EditDomain,
	 *      org.topcased.modeler.di.model.GraphEdge, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected CreateTypedEdgeCommand createCommand(EditDomain domain, GraphEdge edge, GraphElement source)
	{
		return new RafinementMemoryLinkEdgeCreationCommand(domain, edge, source);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkEdge(org.topcased.modeler.di.model.GraphEdge)
	 * @generated
	 */
	protected boolean checkEdge(GraphEdge edge)
	{
		if (edge.getSemanticModel() instanceof SimpleSemanticModelElement)
		{
			return HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK
					.equals(((SimpleSemanticModelElement) edge.getSemanticModel()).getTypeInfo());
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkSource(org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkSource(GraphElement source)
	{
		EObject object = Utils.getElement(source);
		if (object instanceof fr.inria.aoste.syndex.RAM)
		{
			return true;
		}
		if (object instanceof fr.inria.aoste.syndex.SAM)
		{
			return true;
		}
		if (object instanceof fr.inria.aoste.syndex.SAMPTP)
		{
			return true;
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkTargetForSource(org.topcased.modeler.di.model.GraphElement,
	 *      org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkTargetForSource(GraphElement source, GraphElement target)
	{
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof fr.inria.aoste.syndex.RAM && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}

		if (sourceObject instanceof fr.inria.aoste.syndex.SAM && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}

		if (sourceObject instanceof fr.inria.aoste.syndex.SAMPTP && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkCommand(org.eclipse.gef.commands.Command)
	 * @generated
	 */
	protected boolean checkCommand(Command command)
	{
		return command instanceof RafinementMemoryLinkEdgeCreationCommand;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#getSourceTargetData(org.topcased.modeler.di.model.GraphElement,
	 *      org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected SourceTargetData getSourceTargetData(GraphElement source, GraphElement target)
	{
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof fr.inria.aoste.syndex.RAM && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentMemory", null, "parentMemory");
		}
		if (sourceObject instanceof fr.inria.aoste.syndex.SAM && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentMemory", null, "parentMemory");
		}
		if (sourceObject instanceof fr.inria.aoste.syndex.SAMPTP && targetObject instanceof fr.inria.aoste.syndex.Media)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentMemory", null, "parentMemory");
		}
		return null;
	}

}
