/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.actions;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IWorkbenchPart;

import fr.inria.aoste.syndex.modeler.SyndexActionConstants;
import fr.inria.aoste.syndex.modeler.SyndexRequestConstants;

public class ReloadCompoundstructureAction extends WorkbenchPartAction implements ISelectionChangedListener
{
	/**
	 * The selected EditPart object
	 */
	private AbstractGraphicalEditPart	template;

	/**
	 * @param part
	 */
	public ReloadCompoundstructureAction(IWorkbenchPart part)
	{
		super(part);
	}

	/**
	 * Edit the comment of a SIGNAL Identifier
	 * 
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		// construct the Reload ModelInstance Request
		Request request = new Request(SyndexRequestConstants.REQ_RELOAD_COMPOUNDSTRUCTURE);

		// get the Command
		Command actionCommand = template.getCommand(request);

		// execute the command
		getCommandStack().execute(actionCommand);
	}

	/**
	 * Determine if the action must appear in the context menu
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (template == null)
			return false;

		Request request = new Request(SyndexRequestConstants.REQ_RELOAD_COMPOUNDSTRUCTURE);

		return template.understandsRequest(request);
	}

	/**
	 * Initializes the edit comment action
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	protected void init()
	{
		setId(SyndexActionConstants.RELOAD_COMPOUNDSTRUCTURE);
		setText("Reload ConditionedStructure");
	}

	/**
	 * Sets the selected EditPart and refreshes the enabled state of this action.
	 * 
	 * @param event
	 * 
	 * @see ISelectionChangedListener#selectionChanged(SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event)
	{
		ISelection s = event.getSelection();
		if (!(s instanceof IStructuredSelection))
			return;
		IStructuredSelection selection = (IStructuredSelection) s;
		template = null;

		if (selection != null && selection.size() == 1)
		{
			Object obj = selection.getFirstElement();
			if (obj instanceof AbstractGraphicalEditPart)
				template = (AbstractGraphicalEditPart) obj;
		}
		refresh();
	}

}
