/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.commands;

import org.eclipse.gef.EditPart;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;

/**
 * ComponentInstance restore connection command
 * 
 * @generated
 */
public class ComponentInstanceRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public ComponentInstanceRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

	// Do nothing
	}

}
