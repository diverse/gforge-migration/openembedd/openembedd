/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.edit.DiagramEditPart;

import fr.inria.aoste.syndex.modeler.drdiagram.figures.DrdDiagramFigure;
import fr.inria.aoste.syndex.modeler.drdiagram.policies.DrdDiagramLayoutEditPolicy;

/**
 * @generated
 */
public class DrdDiagramEditPart extends DiagramEditPart
{

	/**
	 * The Constructor
	 * 
	 * @param model
	 *        the root model element
	 * @generated
	 */
	public DrdDiagramEditPart(Diagram model)
	{
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy()
	{
		return new DrdDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated
	 */
	protected IFigure createBodyFigure()
	{
		return new DrdDiagramFigure();
	}
}
