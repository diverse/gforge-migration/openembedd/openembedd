/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import org.eclipse.gef.EditDomain;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;

/**
 * RafinementMemoryLink edge creation command
 * 
 * @generated
 */
public class RafinementMemoryLinkEdgeCreationCommand extends CreateTypedEdgeCommand
{

	/**
	 * @param domain
	 *        the edit domain
	 * @param newObj
	 *        the graph edge of the new connection
	 * @param src
	 *        the graph element of the source
	 * @generated
	 */
	public RafinementMemoryLinkEdgeCreationCommand(EditDomain domain, GraphEdge newObj, GraphElement src)
	{
		this(domain, newObj, src, true);
	}

	/**
	 * @param domain
	 *        the edit domain
	 * @param newObj
	 *        the graph edge of the new connection
	 * @param src
	 *        the graph element of the source
	 * @param needModelUpdate
	 *        set it to true if the model need to be updated
	 * @generated
	 */
	public RafinementMemoryLinkEdgeCreationCommand(EditDomain domain, GraphEdge newObj, GraphElement src,
			boolean needModelUpdate)
	{
		super(domain, newObj, src, needModelUpdate);
	}

	/**
	 * @generated
	 */
	protected void redoModel()
	{
		// TODO add specific code if super method is not sufficient
		super.redoModel();
	}

	/**
	 * @generated
	 */
	protected void undoModel()
	{
		// TODO add specific code if super method is not sufficient
		super.undoModel();
	}

}
