/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

import fr.inria.aoste.syndex.SyndexPackage;

/**
 * Generated Palette Manager
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class DadPaletteManager extends ModelerPaletteManager
{
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer	objectsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	portsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param utils
	 *        the creation utils for the tools of the palette
	 * @generated
	 */
	public DadPaletteManager(ICreationUtils utils)
	{
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void createCategories()
	{
		createObjectsDrawer();
		createPortsDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void updateCategories()
	{
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(objectsDrawer);
		createObjectsDrawer();

		getRoot().remove(portsDrawer);
		createPortsDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createObjectsDrawer()
	{
		objectsDrawer = new PaletteDrawer("Objects", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getComponentInstance(),
			"componentinstance");
		entries.add(new ModelerCreationToolEntry("ComponentInstance", "ComponentInstance", factory, DadImageRegistry
				.getImageDescriptor("COMPONENTINSTANCE"), DadImageRegistry
				.getImageDescriptor("COMPONENTINSTANCE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getParameterValue(), "default");
		entries.add(new ModelerCreationToolEntry("Parameter Value", "Parameter Value", factory, DadImageRegistry
				.getImageDescriptor("PARAMETERVALUE"), DadImageRegistry.getImageDescriptor("PARAMETERVALUE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getCondition(), "default");
		entries.add(new ModelerCreationToolEntry("ConditionContainer", "ConditionContainer", factory, DadImageRegistry
				.getImageDescriptor("CONDITION"), DadImageRegistry.getImageDescriptor("CONDITION_LARGE")));

		objectsDrawer.addAll(entries);
		getRoot().add(objectsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createPortsDrawer()
	{
		portsDrawer = new PaletteDrawer("Ports", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInput(), "default");
		entries.add(new ModelerCreationToolEntry("Input", "Input", factory, DadImageRegistry
				.getImageDescriptor("INPUT"), DadImageRegistry.getImageDescriptor("INPUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getOutput(), "default");
		entries.add(new ModelerCreationToolEntry("Output", "Output", factory, DadImageRegistry
				.getImageDescriptor("OUTPUT"), DadImageRegistry.getImageDescriptor("OUTPUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getINInstance(), "default");
		entries.add(new ModelerCreationToolEntry("InputInstance", "InputInstance", factory, DadImageRegistry
				.getImageDescriptor("ININSTANCE"), DadImageRegistry.getImageDescriptor("ININSTANCE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getOutInstance(), "default");
		entries.add(new ModelerCreationToolEntry("OutputInstance", "OutputInstance", factory, DadImageRegistry
				.getImageDescriptor("OUTINSTANCE"), DadImageRegistry.getImageDescriptor("OUTINSTANCE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInCondPort(), "default");
		entries.add(new ModelerCreationToolEntry("InputCond", "InputCond", factory, DadImageRegistry
				.getImageDescriptor("INCONDPORT"), DadImageRegistry.getImageDescriptor("INCONDPORT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInCondInstance(), "default");
		entries.add(new ModelerCreationToolEntry("IncondInstance", "IncondInstance", factory, DadImageRegistry
				.getImageDescriptor("INCONDINSTANCE"), DadImageRegistry.getImageDescriptor("INCONDINSTANCE_LARGE")));

		portsDrawer.addAll(entries);
		getRoot().add(portsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createConnectionsDrawer()
	{
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInternalDataConnection(),
			"default");
		entries.add(new ModelerConnectionCreationToolEntry("DataConnection", "DataConnection", factory,
			DadImageRegistry.getImageDescriptor("INTERNALDATACONNECTION"), DadImageRegistry
					.getImageDescriptor("INTERNALDATACONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInternalDependencyIN(),
			"default");
		entries.add(new ModelerConnectionCreationToolEntry("InternalINDependency", "InternalINDependency", factory,
			DadImageRegistry.getImageDescriptor("INTERNALDEPENDENCYIN"), DadImageRegistry
					.getImageDescriptor("INTERNALDEPENDENCYIN_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInternalDependencyOUT(),
			"default");
		entries.add(new ModelerConnectionCreationToolEntry("InternalOutDependency", "InternalOutDependency", factory,
			DadImageRegistry.getImageDescriptor("INTERNALDEPENDENCYOUT"), DadImageRegistry
					.getImageDescriptor("INTERNALDEPENDENCYOUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInternalDependency(),
			"default");
		entries.add(new ModelerConnectionCreationToolEntry("InternalDependency", "InternalDependency", factory,
			DadImageRegistry.getImageDescriptor("INTERNALDEPENDENCY"), DadImageRegistry
					.getImageDescriptor("INTERNALDEPENDENCY_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
