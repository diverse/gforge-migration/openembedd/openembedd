/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 * 
 * @generated
 */
public interface HddEditPolicyConstants
{

	/**
	 * The key used to install an <i>INOUT</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INOUT_EDITPOLICY					= "INOUT EditPolicy";

	/**
	 * The key used to install an <i>Hardware</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	HARDWARE_EDITPOLICY					= "Hardware EditPolicy";

	/**
	 * The key used to install an <i>FPGA</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	FPGA_EDITPOLICY						= "FPGA EditPolicy";

	/**
	 * The key used to install an <i>Asic</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	ASIC_EDITPOLICY						= "Asic EditPolicy";

	/**
	 * The key used to install an <i>DSP</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	DSP_EDITPOLICY						= "DSP EditPolicy";

	/**
	 * The key used to install an <i>RAM</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	RAM_EDITPOLICY						= "RAM EditPolicy";

	/**
	 * The key used to install an <i>SAM</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	SAM_EDITPOLICY						= "SAM EditPolicy";

	/**
	 * The key used to install an <i>SAMPTP</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	SAMPTP_EDITPOLICY					= "SAMPTP EditPolicy";

	/**
	 * The key used to install an <i>Media</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	MEDIA_EDITPOLICY					= "Media EditPolicy";

	/**
	 * The key used to install an <i>Processor</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PROCESSOR_EDITPOLICY				= "Processor EditPolicy";

	/**
	 * The key used to install an <i>ComponentInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	COMPONENTINSTANCE_EDITPOLICY		= "ComponentInstance EditPolicy";

	/**
	 * The key used to install an <i>INOUTInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INOUTINSTANCE_EDITPOLICY			= "INOUTInstance EditPolicy";

	/**
	 * The key used to install an <i>EnumLitDuration</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	ENUMLITDURATION_EDITPOLICY			= "EnumLitDuration EditPolicy";

	/**
	 * The key used to install an <i>Duration</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	DURATION_EDITPOLICY					= "Duration EditPolicy";

	/**
	 * The key used to install an <i>ConnectionHW</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CONNECTIONHW_EDITPOLICY				= "ConnectionHW EditPolicy";

	/**
	 * The key used to install an <i>RafinnementProcessLink</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	RAFINNEMENTPROCESSLINK_EDITPOLICY	= "RafinnementProcessLink EditPolicy";

	/**
	 * The key used to install an <i>RafinementMemoryLink</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	RAFINEMENTMEMORYLINK_EDITPOLICY		= "RafinementMemoryLink EditPolicy";

}
