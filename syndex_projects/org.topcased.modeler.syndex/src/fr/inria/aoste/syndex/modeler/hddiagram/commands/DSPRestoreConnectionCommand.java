/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.DSP;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.modeler.hddiagram.HddSimpleObjectConstants;

/**
 * DSP restore connection command
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class DSPRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public DSPRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof DSP)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);
					if (eltObject2 instanceof Processor)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if the elt is the source of the edge or if it is the target and that the
							// SourceTargetCouple is reversible
							createRafinnementProcessLinkFromDSPToProcessor_ParentProcessor(elt, elt2);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinnementProcessLinkFromDSPToProcessor_ParentProcessor(GraphElement srcElt,
			GraphElement targetElt)
	{
		DSP sourceObject = (DSP) Utils.getElement(srcElt);
		Processor targetObject = (Processor) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentProcessor())
				&& sourceObject.equals(targetObject.getParentProcessor()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK)
					.size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK);
				RafinnementProcessLinkEdgeCreationCommand cmd = new RafinnementProcessLinkEdgeCreationCommand(null,
					edge, srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

}
