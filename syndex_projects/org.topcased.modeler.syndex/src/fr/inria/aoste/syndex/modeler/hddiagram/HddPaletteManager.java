/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

import fr.inria.aoste.syndex.SyndexPackage;

/**
 * Generated Palette Manager
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class HddPaletteManager extends ModelerPaletteManager
{
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer	operatorDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	memoryDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	hardwareDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	caracteriticsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param utils
	 *        the creation utils for the tools of the palette
	 * @generated
	 */
	public HddPaletteManager(ICreationUtils utils)
	{
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void createCategories()
	{
		createOperatorDrawer();
		createMemoryDrawer();
		createHardwareDrawer();
		createCaracteriticsDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void updateCategories()
	{
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(operatorDrawer);
		createOperatorDrawer();

		getRoot().remove(memoryDrawer);
		createMemoryDrawer();

		getRoot().remove(hardwareDrawer);
		createHardwareDrawer();

		getRoot().remove(caracteriticsDrawer);
		createCaracteriticsDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createOperatorDrawer()
	{
		operatorDrawer = new PaletteDrawer("Operator", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getProcessor(), "default");
		entries.add(new ModelerCreationToolEntry("Processor", "Processor", factory, HddImageRegistry
				.getImageDescriptor("PROCESSOR"), HddImageRegistry.getImageDescriptor("PROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getFPGA(), "default");
		entries.add(new ModelerCreationToolEntry("FPGA", "FPGA", factory, HddImageRegistry.getImageDescriptor("FPGA"),
			HddImageRegistry.getImageDescriptor("FPGA_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getDSP(), "default");
		entries.add(new ModelerCreationToolEntry("DSP", "DSP", factory, HddImageRegistry.getImageDescriptor("DSP"),
			HddImageRegistry.getImageDescriptor("DSP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getAsic(), "default");
		entries.add(new ModelerCreationToolEntry("Asic", "Asic", factory, HddImageRegistry.getImageDescriptor("ASIC"),
			HddImageRegistry.getImageDescriptor("ASIC_LARGE")));

		operatorDrawer.addAll(entries);
		getRoot().add(operatorDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createMemoryDrawer()
	{
		memoryDrawer = new PaletteDrawer("Memory", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getMedia(), "default");
		entries.add(new ModelerCreationToolEntry("Medium", "Medium", factory, HddImageRegistry
				.getImageDescriptor("MEDIA"), HddImageRegistry.getImageDescriptor("MEDIA_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getRAM(), "default");
		entries.add(new ModelerCreationToolEntry("RAM", "RAM", factory, HddImageRegistry.getImageDescriptor("RAM"),
			HddImageRegistry.getImageDescriptor("RAM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getSAM(), "default");
		entries.add(new ModelerCreationToolEntry("SAM", "SAM", factory, HddImageRegistry.getImageDescriptor("SAM"),
			HddImageRegistry.getImageDescriptor("SAM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getSAMPTP(), "default");
		entries.add(new ModelerCreationToolEntry("SAMPTP", "SAMPTP", factory, HddImageRegistry
				.getImageDescriptor("SAMPTP"), HddImageRegistry.getImageDescriptor("SAMPTP_LARGE")));

		memoryDrawer.addAll(entries);
		getRoot().add(memoryDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createHardwareDrawer()
	{
		hardwareDrawer = new PaletteDrawer("Hardware", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getHardware(), "default");
		entries.add(new ModelerCreationToolEntry("Hardware", "Hardware", factory, HddImageRegistry
				.getImageDescriptor("HARDWARE"), HddImageRegistry.getImageDescriptor("HARDWARE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getComponentInstance(),
			"default");
		entries.add(new ModelerCreationToolEntry("Component Instance", "Component Instance", factory, HddImageRegistry
				.getImageDescriptor("COMPONENTINSTANCE"), HddImageRegistry
				.getImageDescriptor("COMPONENTINSTANCE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getINOUTInstance(), "default");
		entries.add(new ModelerCreationToolEntry("InOut Instance", "InOut Instance", factory, HddImageRegistry
				.getImageDescriptor("INOUTINSTANCE"), HddImageRegistry.getImageDescriptor("INOUTINSTANCE_LARGE")));

		hardwareDrawer.addAll(entries);
		getRoot().add(hardwareDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createCaracteriticsDrawer()
	{
		caracteriticsDrawer = new PaletteDrawer("Caracteritics", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getDuration(), "default");
		entries.add(new ModelerCreationToolEntry("Duration", "Duration", factory, HddImageRegistry
				.getImageDescriptor("DURATION"), HddImageRegistry.getImageDescriptor("DURATION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getEnumLitDuration(),
			"default");
		entries.add(new ModelerCreationToolEntry("EnumerationDuration", "EnumerationDuration", factory,
			HddImageRegistry.getImageDescriptor("ENUMLITDURATION"), HddImageRegistry
					.getImageDescriptor("ENUMLITDURATION_LARGE")));

		caracteriticsDrawer.addAll(entries);
		getRoot().add(caracteriticsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	private void createConnectionsDrawer()
	{
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getINOUT(), "default");
		entries.add(new ModelerCreationToolEntry("INOUT", "INOUT", factory, HddImageRegistry
				.getImageDescriptor("INOUT"), HddImageRegistry.getImageDescriptor("INOUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getConnectionHW(), "default");
		entries.add(new ModelerConnectionCreationToolEntry("Connector", "Connector", factory, HddImageRegistry
				.getImageDescriptor("CONNECTIONHW"), HddImageRegistry.getImageDescriptor("CONNECTIONHW_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
			HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK, "default", false);
		entries.add(new ModelerConnectionCreationToolEntry("ProcessorRaffinement", "ProcessorRaffinement", factory,
			HddImageRegistry.getImageDescriptor("RAFINNEMENTPROCESSLINK"), HddImageRegistry
					.getImageDescriptor("RAFINNEMENTPROCESSLINK_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
			HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINEMENTMEMORYLINK, "default", false);
		entries.add(new ModelerConnectionCreationToolEntry("MemoryRaffinement", "MemoryRaffinement", factory,
			HddImageRegistry.getImageDescriptor("RAFINEMENTMEMORYLINK"), HddImageRegistry
					.getImageDescriptor("RAFINEMENTMEMORYLINK_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
