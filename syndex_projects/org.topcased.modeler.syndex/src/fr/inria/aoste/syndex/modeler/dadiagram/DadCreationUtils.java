/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class DadCreationUtils extends AbstractCreationUtils
{

	/**
	 * Constructor
	 * 
	 * @param diagramConf
	 *        the Diagram Graphical Configuration
	 * @generated
	 */
	public DadCreationUtils(DiagramGraphConf diagramConf)
	{
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	private class GraphicSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The presentation of the graphical element
		 * 
		 * @generated
		 */
		private String	presentation;

		/**
		 * Constructor
		 * 
		 * @param presentation
		 *        the presentation of the graphical element
		 * @generated
		 */
		public GraphicSyndexSwitch(String presentation)
		{
			this.presentation = presentation;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance)
		 * @generated
		 */
		public Object caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance object)
		{
			if ("componentinstance".equals(presentation))
			{
				return createGraphElementComponentInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseParameterValue(fr.inria.aoste.syndex.ParameterValue)
		 * @generated
		 */
		public Object caseParameterValue(fr.inria.aoste.syndex.ParameterValue object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementParameterValue(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseCondition(fr.inria.aoste.syndex.Condition)
		 * @generated
		 */
		public Object caseCondition(fr.inria.aoste.syndex.Condition object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementCondition(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInput(fr.inria.aoste.syndex.Input)
		 * @generated
		 */
		public Object caseInput(fr.inria.aoste.syndex.Input object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInput(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutput(fr.inria.aoste.syndex.Output)
		 * @generated
		 */
		public Object caseOutput(fr.inria.aoste.syndex.Output object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementOutput(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINInstance(fr.inria.aoste.syndex.INInstance)
		 * @generated
		 */
		public Object caseINInstance(fr.inria.aoste.syndex.INInstance object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementINInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutInstance(fr.inria.aoste.syndex.OutInstance)
		 * @generated
		 */
		public Object caseOutInstance(fr.inria.aoste.syndex.OutInstance object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementOutInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondPort(fr.inria.aoste.syndex.InCondPort)
		 * @generated
		 */
		public Object caseInCondPort(fr.inria.aoste.syndex.InCondPort object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInCondPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondInstance(fr.inria.aoste.syndex.InCondInstance)
		 * @generated
		 */
		public Object caseInCondInstance(fr.inria.aoste.syndex.InCondInstance object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInCondInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDataConnection(fr.inria.aoste.syndex.InternalDataConnection)
		 * @generated
		 */
		public Object caseInternalDataConnection(fr.inria.aoste.syndex.InternalDataConnection object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInternalDataConnection(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependencyIN(fr.inria.aoste.syndex.InternalDependencyIN)
		 * @generated
		 */
		public Object caseInternalDependencyIN(fr.inria.aoste.syndex.InternalDependencyIN object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInternalDependencyIN(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependencyOUT(fr.inria.aoste.syndex.InternalDependencyOUT)
		 * @generated
		 */
		public Object caseInternalDependencyOUT(fr.inria.aoste.syndex.InternalDependencyOUT object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInternalDependencyOUT(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependency(fr.inria.aoste.syndex.InternalDependency)
		 * @generated
		 */
		public Object caseInternalDependency(fr.inria.aoste.syndex.InternalDependency object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInternalDependency(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject,
	 *      java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation)
	{
		Object graphElt = null;

		if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(obj.eClass().getEPackage().getNsURI()))
		{
			graphElt = new GraphicSyndexSwitch(presentation).doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementComponentInstance(fr.inria.aoste.syndex.ComponentInstance element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameterValue(fr.inria.aoste.syndex.ParameterValue element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementCondition(fr.inria.aoste.syndex.Condition element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInput(fr.inria.aoste.syndex.Input element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementOutput(fr.inria.aoste.syndex.Output element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementINInstance(fr.inria.aoste.syndex.INInstance element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementOutInstance(fr.inria.aoste.syndex.OutInstance element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInCondPort(fr.inria.aoste.syndex.InCondPort element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInCondInstance(fr.inria.aoste.syndex.InCondInstance element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInternalDataConnection(
			fr.inria.aoste.syndex.InternalDataConnection element, String presentation)
	{
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInternalDependencyIN(fr.inria.aoste.syndex.InternalDependencyIN element,
			String presentation)
	{
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInternalDependencyOUT(fr.inria.aoste.syndex.InternalDependencyOUT element,
			String presentation)
	{
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInternalDependency(fr.inria.aoste.syndex.InternalDependency element,
			String presentation)
	{
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj
	 *        the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj)
	{
		return obj;
	}

}
