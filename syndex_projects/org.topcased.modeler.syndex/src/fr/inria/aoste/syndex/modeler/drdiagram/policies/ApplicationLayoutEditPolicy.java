/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.ConditionedStructure;
import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.ParameterDeclaration;

/**
 * @generated
 */
public class ApplicationLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public ApplicationLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof Input)
		{
			return true;
		}
		if (child instanceof Output)
		{
			return true;
		}
		if (child instanceof CompoundStructure)
		{
			return true;
		}
		if (child instanceof ConditionedStructure)
		{
			return true;
		}
		if (child instanceof ElementaryStructure)
		{
			return true;
		}
		if (child instanceof InCondPort)
		{
			return true;
		}
		if (child instanceof ParameterDeclaration)
		{
			return true;
		}
		return false;
	}

}
