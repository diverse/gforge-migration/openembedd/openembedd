/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.commands;

import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.InternalStructure;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexFactory;

public class AddNewPortCompoundCommand extends Command
{
	private GraphNode	internalStructureNode;

	private GraphNode	inputPortNode;

	private GraphNode	outputPortNode;

	private Modeler		modeler;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that will contain the newly created Port
	 */
	public AddNewPortCompoundCommand(GraphNode element, Modeler editor)
	{
		super("Add new port");
		this.internalStructureNode = element;
		this.modeler = editor;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}

	/**
	 * Add a new InputPort object to the MultipleInputsOperator model object and a new OutputPort if the model object is
	 * a CartesianProduct
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@SuppressWarnings("unchecked")
	public void redo()
	{
		ICreationUtils factory = modeler.getActiveConfiguration().getCreationUtils();
		InternalStructure sigop = (InternalStructure) Utils.getElement(internalStructureNode);

		if (sigop instanceof CompoundStructure)
		{
			// create the model object
			Output outputPort = SyndexFactory.eINSTANCE.createOutput();
			((CompoundStructure) sigop).getPortCompoundStructure().add(outputPort);

			// create the graphNode and its position property
			outputPortNode = (GraphNode) factory.createGraphElement(outputPort);
			((CompoundStructure) sigop).getPortCompoundStructure().add(outputPortNode);

			// create the model object
			Input inputPort = SyndexFactory.eINSTANCE.createInput();
			((CompoundStructure) sigop).getPortCompoundStructure().add(inputPort);

			// create the graphNode and its position property
			inputPortNode = (GraphNode) factory.createGraphElement(inputPort);
			((CompoundStructure) sigop).getPortCompoundStructure().add(inputPortNode);

		}

	}

	/**
	 * Remove the last added InputPort (and OutputPort)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{
		return;
	}
}
