/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexFactory;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.Vector;

/**
 * Generated Palette Manager
 * 
 * @generated
 */
public class DrdPaletteManager extends ModelerPaletteManager
{
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer	operationDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	portDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer	internalstructureDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param utils
	 *        the creation utils for the tools of the palette
	 * @generated
	 */
	public DrdPaletteManager(ICreationUtils utils)
	{
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void createCategories()
	{
		createOperationDrawer();
		createPortDrawer();
		createInternalStructureDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 * 
	 * @generated
	 */
	protected void updateCategories()
	{
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(operationDrawer);
		createOperationDrawer();

		getRoot().remove(portDrawer);
		createPortDrawer();

		getRoot().remove(internalstructureDrawer);
		createInternalStructureDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated NOT
	 */

	@SuppressWarnings("unchecked")
	private void createOperationDrawer()
	{
		operationDrawer = new PaletteDrawer("Operation", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getSensor(), "default");
		entries.add(new ModelerCreationToolEntry("Sensor", "Sensor", factory, DrdImageRegistry
				.getImageDescriptor("SENSOR"), DrdImageRegistry.getImageDescriptor("SENSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getActuator(), "default");
		entries.add(new ModelerCreationToolEntry("Actuator", "Actuator", factory, DrdImageRegistry
				.getImageDescriptor("ACTUATOR"), DrdImageRegistry.getImageDescriptor("ACTUATOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getApplication(), "default");
		entries.add(new ModelerCreationToolEntry("Application", "Application", factory, DrdImageRegistry
				.getImageDescriptor("APPLICATION"), DrdImageRegistry.getImageDescriptor("APPLICATION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getFunction(), "default");
		entries.add(new ModelerCreationToolEntry("Function", "Function", factory, DrdImageRegistry
				.getImageDescriptor("FUNCTION"), DrdImageRegistry.getImageDescriptor("FUNCTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getConstant(), "default")
		{
			public EObject getNewModelObject()
			{
				Constant element = (Constant) super.getNewModelObject();
				EObject outPort = SyndexFactory.eINSTANCE.createOutput();
				EObject vector = SyndexFactory.eINSTANCE.createVector();
				element.setOneOutputConstant((Output) outPort);
				((Output) outPort).setParentPort(element);
				((Output) outPort).setSizePort((Vector) vector);
				return element;
			}
		};
		entries.add(new ModelerCreationToolEntry("Constant", "Constant", factory, DrdImageRegistry
				.getImageDescriptor("CONSTANT"), DrdImageRegistry.getImageDescriptor("CONSTANT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getDelay(), "default")
		{
			public EObject getNewModelObject()
			{
				Delay element = (Delay) super.getNewModelObject();
				EObject inPort = SyndexFactory.eINSTANCE.createInput();
				EObject outPort = SyndexFactory.eINSTANCE.createOutput();
				EObject vector = SyndexFactory.eINSTANCE.createVector();
				element.setOneInputDelay((Input) inPort);
				element.setOneOutputDelay((Output) outPort);
				((Input) inPort).setParentPort(element);
				((Input) inPort).setSizePort((Vector) vector);
				((Output) outPort).setParentPort(element);
				((Output) outPort).setSizePort((Vector) vector);
				return element;
			}
		};
		entries.add(new ModelerCreationToolEntry("Delay", "Delay", factory, DrdImageRegistry
				.getImageDescriptor("DELAY"), DrdImageRegistry.getImageDescriptor("DELAY_LARGE")));

		operationDrawer.addAll(entries);
		getRoot().add(operationDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	private void createPortDrawer()
	{
		portDrawer = new PaletteDrawer("Port", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInput(), "default")
		{
			public EObject getNewModelObject()
			{
				Input element = (Input) super.getNewModelObject();
				EObject vector = SyndexFactory.eINSTANCE.createVector();
				element.setSizePort((Vector) vector);
				// ((Vector) vector).eContainer();
				return element;
			}
		};
		entries.add(new ModelerCreationToolEntry("Input", "Input", factory, DrdImageRegistry
				.getImageDescriptor("INPUT"), DrdImageRegistry.getImageDescriptor("INPUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getOutput(), "default")
		{
			public EObject getNewModelObject()
			{
				Output element = (Output) super.getNewModelObject();
				EObject vector = SyndexFactory.eINSTANCE.createVector();
				element.setSizePort((Vector) vector);
				// ((Vector) vector).eContainer();
				return element;
			}
		};
		entries.add(new ModelerCreationToolEntry("Output", "Output", factory, DrdImageRegistry
				.getImageDescriptor("OUTPUT"), DrdImageRegistry.getImageDescriptor("OUTPUT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getInCondPort(), "default")
		{
			public EObject getNewModelObject()
			{
				InCondPort element = (InCondPort) super.getNewModelObject();
				EObject vector = SyndexFactory.eINSTANCE.createVector();
				element.setSizePort((Vector) vector);
				// ((Vector) vector).eContainer();
				return element;
			}
		};
		entries.add(new ModelerCreationToolEntry("ConditionPort", "ConditionPort", factory, DrdImageRegistry
				.getImageDescriptor("INCONDPORT"), DrdImageRegistry.getImageDescriptor("INCONDPORT_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getParameterDeclaration(),
			"default");
		entries.add(new ModelerCreationToolEntry("Parameter Description", "Parameter Description", factory,
			DrdImageRegistry.getImageDescriptor("PARAMETERDECLARATION"), DrdImageRegistry
					.getImageDescriptor("PARAMETERDECLARATION_LARGE")));

		portDrawer.addAll(entries);
		getRoot().add(portDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private void createInternalStructureDrawer()
	{
		internalstructureDrawer = new PaletteDrawer("InternalStructure", null);
		List entries = new ArrayList();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getCompoundStructure(),
			"default");
		entries.add(new ModelerCreationToolEntry("Compound Structure", "Compound Structure", factory, DrdImageRegistry
				.getImageDescriptor("COMPOUNDSTRUCTURE"), DrdImageRegistry
				.getImageDescriptor("COMPOUNDSTRUCTURE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getConditionedStructure(),
			"default");
		entries.add(new ModelerCreationToolEntry("Conditioned Structure", "Conditioned Structure", factory,
			DrdImageRegistry.getImageDescriptor("CONDITIONEDSTRUCTURE"), DrdImageRegistry
					.getImageDescriptor("CONDITIONEDSTRUCTURE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils, SyndexPackage.eINSTANCE.getElementaryStructure(),
			"default");
		entries.add(new ModelerCreationToolEntry("Elementary Structure", "Elementary Structure", factory,
			DrdImageRegistry.getImageDescriptor("ELEMENTARYSTRUCTURE"), DrdImageRegistry
					.getImageDescriptor("ELEMENTARYSTRUCTURE_LARGE")));

		internalstructureDrawer.addAll(entries);
		getRoot().add(internalstructureDrawer);
	}

}
