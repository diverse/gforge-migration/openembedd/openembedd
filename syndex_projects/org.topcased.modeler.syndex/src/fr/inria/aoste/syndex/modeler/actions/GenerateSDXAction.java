package fr.inria.aoste.syndex.modeler.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.topcased.modeler.editor.Modeler;

import fr.inria.aoste.syndex.m2t.jobs.ModelToTextJob;
import fr.inria.aoste.syndex.modeler.SyndexActionConstants;
import fr.inria.aoste.syndex.modeler.SyndexImageRegistry;

public class GenerateSDXAction extends WorkbenchPartAction
{
	/**
	 * @param part
	 */
	public GenerateSDXAction(IWorkbenchPart part)
	{
		super(part);
	}

	@Override
	protected boolean calculateEnabled()
	{
		return true;
	}

	/**
	 * Initializes the action
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	protected void init()
	{
		setId(SyndexActionConstants.GENERATE_SDX_CODE);
		setText("Generate the SDX textual file");
		setImageDescriptor(SyndexImageRegistry.getImageDescriptor("BUTTON"));
	}

	/**
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		// get the modeler
		Modeler modeler = (Modeler) getWorkbenchPart();
		IFile syndexFile = null;
		Resource resource = null;
		URI uri = null;
		for (Resource res : modeler.getEditingDomain().getResourceSet().getResources())
		{
			uri = res.getURI();
			if ("syndex".equals(uri.fileExtension()))
			{
				resource = res;
				break;
			}
		}

		if (resource != null)
		{
			uri = resource.getResourceSet().getURIConverter().normalize(uri);
			String scheme = uri.scheme();
			if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0)))
			{
				String path = uri.toPlatformString(true);
				syndexFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(path));
			}
		}

		if (syndexFile != null)
		{
			ModelToTextJob execute = new ModelToTextJob(syndexFile);
			execute.setUser(true);
			execute.schedule();
		}
		else
			MessageDialog.openError(new Shell(), "SDX Generation", "No SynDEx file found.");
	}
}
