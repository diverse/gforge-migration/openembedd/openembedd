/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.figures;

/**
 * @generated
 */
public class ParameterDeclarationFigure extends org.topcased.draw2d.figures.CommentFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public ParameterDeclarationFigure()
	{
		super();
	}

}
