/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Asic;
import fr.inria.aoste.syndex.DSP;
import fr.inria.aoste.syndex.FPGA;
import fr.inria.aoste.syndex.Processor;
import fr.inria.aoste.syndex.modeler.hddiagram.HddSimpleObjectConstants;

/**
 * Processor restore connection command
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class ProcessorRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public ProcessorRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof Processor)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);
					if (eltObject2 instanceof FPGA)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinnementProcessLinkFromFPGAToProcessor_ParentProcessor(elt2, elt);
						}
					}
					if (eltObject2 instanceof Asic)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinnementProcessLinkFromAsicToProcessor_ParentProcessor(elt2, elt);
						}
					}
					if (eltObject2 instanceof DSP)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createRafinnementProcessLinkFromDSPToProcessor_ParentProcessor(elt2, elt);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinnementProcessLinkFromFPGAToProcessor_ParentProcessor(GraphElement srcElt,
			GraphElement targetElt)
	{
		FPGA sourceObject = (FPGA) Utils.getElement(srcElt);
		Processor targetObject = (Processor) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentProcessor())
				&& sourceObject.equals(targetObject.getParentProcessor()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK)
					.size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK);
				RafinnementProcessLinkEdgeCreationCommand cmd = new RafinnementProcessLinkEdgeCreationCommand(null,
					edge, srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinnementProcessLinkFromAsicToProcessor_ParentProcessor(GraphElement srcElt,
			GraphElement targetElt)
	{
		Asic sourceObject = (Asic) Utils.getElement(srcElt);
		Processor targetObject = (Processor) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentProcessor())
				&& sourceObject.equals(targetObject.getParentProcessor()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK)
					.size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK);
				RafinnementProcessLinkEdgeCreationCommand cmd = new RafinnementProcessLinkEdgeCreationCommand(null,
					edge, srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createRafinnementProcessLinkFromDSPToProcessor_ParentProcessor(GraphElement srcElt,
			GraphElement targetElt)
	{
		DSP sourceObject = (DSP) Utils.getElement(srcElt);
		Processor targetObject = (Processor) Utils.getElement(targetElt);

		if (targetObject.equals(sourceObject.getParentProcessor())
				&& sourceObject.equals(targetObject.getParentProcessor()))
		{
			// check if the relation does not exists yet
			if (getExistingEdges(srcElt, targetElt, HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK)
					.size() == 0)
			{
				GraphEdge edge = Utils.createGraphEdge(HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK);
				RafinnementProcessLinkEdgeCreationCommand cmd = new RafinnementProcessLinkEdgeCreationCommand(null,
					edge, srcElt, false);
				cmd.setTarget(targetElt);
				add(cmd);
			}
		}
	}

}
