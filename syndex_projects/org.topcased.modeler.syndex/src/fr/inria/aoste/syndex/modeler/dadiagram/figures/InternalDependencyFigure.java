/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.swt.SWT;

/**
 * @generated
 */
public class InternalDependencyFigure extends PolylineConnectionEx
{

	/**
	 * The constructor
	 * 
	 * @generated
	 */
	public InternalDependencyFigure()
	{
		super();
		setLineStyle(SWT.LINE_SOLID);
	}

}
