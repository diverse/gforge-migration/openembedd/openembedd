/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.topcased.modeler.figures.DiagramFigure;

/**
 * The figure to display a Application Definition Diagram.
 * 
 * @generated
 */
public class DadDiagramFigure extends DiagramFigure
{

}
