/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

public class PositionInstanceConditionCommand extends Command
{
	private GraphNode	container;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that represent the ModelInstance on which we will operate
	 */
	public PositionInstanceConditionCommand(GraphNode element)
	{
		super("Position the port instance in a  conditioned structure");
		this.container = element;

	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}

	/**
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@SuppressWarnings("unchecked")
	public void redo()
	{
		Condition mi = (Condition) Utils.getElement(container);
		// ConditionedStructure ci= (ConditionedStructure) mi.eContainer();
		ApplicationComponent id = mi.getOwnerInternalStructure();
		if (id == null)
			return;

		// partie Application
		if (id instanceof Application)
		{
			EList outs = ((Application) id).getOutputPortApplication(), ins = ((Application) id)
					.getInputPortApplication(), incond = ((Application) id).getPortCondition();

			int nbOuts = outs.size(), nbIns = ins.size(), nbInConds = incond.size();

			int height = 10;

			if (nbIns > 0 || nbOuts > 0 || nbInConds > 0)
			{

				int space_out = height / (nbOuts > 0 ? nbOuts : 1), space_in = height / (nbIns > 0 ? nbIns : 1), space_incond = height
						/ (nbInConds > 0 ? nbInConds : 1);
				int init_out = (space_out - 10) / 2, init_in = (space_in - 10) / 2, init_incond = (space_incond - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof Output)
					{

						index = outs.indexOf(((Output) obj).getParentConditionReference());
						elt.setPosition(new Point(0, init_out + (index * space_out)));
					}
					if (obj instanceof Input)
					{

						index = ins.indexOf(((Input) obj).getParentConditionReference());
						elt.setPosition(new Point(0, init_in + (index * space_in)));
					}
					if (obj instanceof InCondPort)
					{

						index = incond.indexOf(((InCondPort) obj).getParentConditionReference());
						elt.setPosition(new Point(0, init_incond + (index * space_incond)));
					}

				}
			}
		}
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{

	}
}
