/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.Sensor;

public class PositionInstanceCommand extends Command
{
	private GraphNode	container;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that represent the ModelInstance on which we will operate
	 */
	public PositionInstanceCommand(GraphNode element)
	{
		super("Position the port instance in a model instance");
		this.container = element;

	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}

	/**
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@SuppressWarnings("unchecked")
	public void redo()
	{
		ComponentInstance mi = (ComponentInstance) Utils.getElement(container);
		ApplicationComponent id = mi.getReferencedComponent();
		if (id == null)
			return;

		// Partie Actuator

		if (id instanceof Actuator)
		{
			EList ins = ((Actuator) id).getInputPortActuator();

			int nbIns = ins.size();

			int height = 10;

			if (nbIns > 0)
			{

				int space_ins = height / (nbIns > 0 ? nbIns : 1);
				int init_out = (space_ins - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof INInstance)
					{

						// index = ins.indexOf(((INInstance) obj).getOwnerInput());
						index = ins.indexOf(((INInstance) obj).getOwnerInput());
						elt.setPosition(new Point(0, init_out + (index * space_ins)));
					}
				}
			}
		}

		// Partie Sensor

		if (id instanceof Sensor)
		{
			EList outs = ((Sensor) id).getOutputPortSensor();

			int nbOuts = outs.size();

			int height = 10;

			if (nbOuts > 0)
			{

				int space_out = height / (nbOuts > 0 ? nbOuts : 1);
				int init_out = (space_out - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof OutInstance)
					{

						index = outs.indexOf(((OutInstance) obj).getOwnerOutput());
						elt.setPosition(new Point(0, init_out + (index * space_out)));
					}

				}
			}
		}

		// Partie Application

		if (id instanceof Application)
		{
			EList outs = ((Application) id).getOutputPortApplication(), ins = ((Application) id)
					.getInputPortApplication(), incond = ((Application) id).getPortCondition();

			int nbOuts = outs.size(), nbIns = ins.size(), nbcond = incond.size();

			int height = 10;

			if (nbIns > 0 || nbOuts > 0 || nbcond > 0)
			{

				int space_out = height / (nbOuts > 0 ? nbOuts : 1), space_in = height / (nbIns > 0 ? nbIns : 1), space_incond = height
						/ (nbcond > 0 ? nbcond : 1);
				int init_out = (space_out - 10) / 2, init_in = (space_in - 10) / 2, init_incond = (space_incond - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof INInstance)
					{
						index = ins.indexOf(((INInstance) obj).getOwnerInput());
						elt.setPosition(new Point(0, init_in + (index * space_in)));
					}
					if (obj instanceof OutInstance)
					{

						index = outs.indexOf(((OutInstance) obj).getOwnerOutput());
						elt.setPosition(new Point(0, init_out + (index * space_out)));
					}
					if (obj instanceof InCondInstance)
					{
						index = ins.indexOf(((InCondInstance) obj).getOwnerCond());
						elt.setPosition(new Point(0, init_incond + (index * space_incond)));
					}
				}
			}
		}

		// Partie Constant

		if (id instanceof Constant)
		{
			Output outs = ((Constant) id).getOneOutputConstant();
			EObject obj = Utils.getElement(container);

			if (obj instanceof OutInstance)
			{

				outs.getParentPort();
			}
		}

		// Partie Delay

		if (id instanceof Delay)
		{

			Output outs = ((Delay) id).getOneOutputDelay();
			Input ins = ((Delay) id).getOneInputDelay();
			EObject obj = Utils.getElement(container);

			if (obj instanceof OutInstance)
			{

				outs.getParentPort();

			}

			if (obj instanceof OutInstance)
			{

				ins.getParentPort();
			}

		}

		// Partie Function

		if (id instanceof Function)
		{
			EList outs = ((Function) id).getOutputPortFunction(), ins = ((Function) id).getInputPortFunction();

			int nbOuts = outs.size(), nbIns = ins.size();

			int height = 10;

			if (nbIns > 0 || nbOuts > 0)
			{

				int space_out = height / (nbOuts > 0 ? nbOuts : 1), space_in = height / (nbIns > 0 ? nbIns : 1);
				int init_out = (space_out - 10) / 2, init_in = (space_in - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof INInstance)
					{

						index = ins.indexOf(((INInstance) obj).getOwnerInput());
						elt.setPosition(new Point(0, init_in + (index * space_in)));
					}
					if (obj instanceof OutInstance)
					{

						index = outs.indexOf(((OutInstance) obj).getOwnerOutput());
						elt.setPosition(new Point(0, init_out + (index * space_out)));
					}
				}
			}
		}

		// Fin fonction
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{

	}
}
