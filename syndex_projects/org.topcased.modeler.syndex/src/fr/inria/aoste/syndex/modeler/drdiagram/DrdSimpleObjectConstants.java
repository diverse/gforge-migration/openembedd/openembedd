/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

/**
 * A Set of properties that are used for the graphical objects that are not associated with a model object. Each name is
 * used as the typeInfo attribute in the DI file.
 * 
 * @generated
 */
public interface DrdSimpleObjectConstants
{}
