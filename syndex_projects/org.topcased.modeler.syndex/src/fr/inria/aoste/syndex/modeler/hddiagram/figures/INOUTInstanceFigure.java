/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.figures;

import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.ILabelFigure;

/**
 * @generated
 */
public class INOUTInstanceFigure extends org.topcased.draw2d.figures.PortFigure implements ILabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public INOUTInstanceFigure()
	{
		// TODO : here, you can specify the type of port (IN, OUT or INOUT) to have a better display
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ILabelFigure#getLabel()
	 * @generated
	 */
	public ILabel getLabel()
	{
		// TODO : return the Figure that represent the Label
		return null;
	}

}
