/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.eclipse.draw2d.IFigure;
import org.topcased.draw2d.layout.BorderAttachedLayout;

/**
 * @generated
 */
public class ConditionFigure extends org.topcased.draw2d.figures.ContainerFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public ConditionFigure()
	{
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ContainerFigure#createContainer()
	 * @generated NOT
	 */
	protected IFigure createContainer()
	{
		IFigure container = super.createContainer();
		container.setLayoutManager(new BorderAttachedLayout());
		return container;
	}
}
