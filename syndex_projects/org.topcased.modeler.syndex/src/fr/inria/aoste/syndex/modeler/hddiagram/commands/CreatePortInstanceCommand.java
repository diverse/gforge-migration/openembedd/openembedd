/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

/*
 * 
 * authors : Fadoi LAKHAL, Yves SOREL INRIA Rocquencourt
 */

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.commands.CreateGraphNodeCommand;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.INOUTInstance;

@SuppressWarnings("unchecked")
public class CreatePortInstanceCommand extends CompoundCommand
{
	/** The GraphNode that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/** Lists containing all new EObjects to add to the container graphNode */
	private EList			newObjects	= new BasicEList();

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new InputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the InputInstance to add
	 */
	public CreatePortInstanceCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			INOUTInstance inout)
	{
		super("Add an InputInstance Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (inout != null)
			newObjects.add(inout);

		initialize();
	}

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new InputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param list
	 *        the list of Port Instances (ParameterInstance, InputInstance, or OutputInstance) to add
	 */
	public CreatePortInstanceCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			Collection list)
	{
		super("Add a list of Port Instance Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (list != null)
			newObjects.addAll(list);

		initialize();
	}

	private void initialize()
	{
		EObject eObject = Utils.getElement(container);
		if (eObject instanceof ComponentInstance)
		{
			for (Iterator<INOUTInstance> iter = newObjects.iterator(); iter.hasNext();)
			{
				EObject port = iter.next();
				GraphNode node = getGraphicalRepresentation(port);

				if (node == null)
				{
					node = (GraphNode) creationUtils.createGraphElement(port);

					if (port instanceof INOUTInstance)
						add(new CreateGraphNodeCommand(editDomain, node, container, null, node.getSize(),
							PositionConstants.RIGHT));

				}
			}
		}
	}

	/**
	 * Get the graphNode associated with the model object in the current selected editPart
	 * 
	 * @param modelObject
	 *        the model object to display
	 * @return the GraphNode or null if it does not exist
	 */
	private GraphNode getGraphicalRepresentation(EObject modelObject)
	{
		for (Iterator iter = container.getContained().iterator(); iter.hasNext();)
		{
			GraphElement elt = (GraphElement) iter.next();
			if (Utils.getElement(elt) == modelObject && elt instanceof GraphNode)
				return (GraphNode) elt;
		}
		return null;
	}

}
