/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.InternalStructure;

public class HierarchieCommand extends Command
{

	private EObject					obj;

	private ModelerGraphicalViewer	viewer;

	private String					diagramId;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that will contain the newly created Port
	 */
	public HierarchieCommand(GraphElement element, ModelerGraphicalViewer viewer, String diagramId)
	{
		super("essai command");
		obj = Utils.getElement(element);
		this.viewer = viewer;
		this.diagramId = diagramId;
		// container =(GraphNode) Utils.getElement(element);
	}

	public HierarchieCommand(EObject element, ModelerGraphicalViewer viewer, String diagramId)
	{
		super("essai");
		obj = element;
		this.viewer = viewer;
		this.diagramId = diagramId;
	}
	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();

	}

	/**
	 * Add a new InputPort object to the MultipleInputsOperator model object and a new OutputPort if the model object is
	 * a CartesianProduct
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@SuppressWarnings("unchecked")
	public void redo()
	{
		Modeler editor = viewer.getModelerEditor();
		ComponentInstance mi = (ComponentInstance) this.obj;
		Application appli = (Application) mi.getReferencedComponent();
		// CompoundStructure cp=(CompoundStructure)appli.getApplicationInternalStructure();
		InternalStructure is = (InternalStructure) appli.getApplicationInternalStructure();
		// GraphElement cpo;
		// cpo=(GraphElement) cp;

		// deactivate the active tool
		viewer.getEditDomain().setActiveTool(null);
		viewer.getEditDomain().getPaletteViewer().setActiveTool(null);

		// We search if a diagram already exists and let the user choose if at least two exist.
		List existingDiagramList = DiagramsUtils.findAllDiagrams(editor.getDiagrams());
		boolean diagFound = false;
		for (Iterator it = existingDiagramList.iterator(); diagFound == false && it.hasNext();)
		{
			Diagram diag = (Diagram) it.next();
			EList<EObject> liststructure = diag.getSemanticModel().eCrossReferences();
			EObject cpm = liststructure.get(0);
			if (diag.getSemanticModel().getPresentation().equals(diagramId) && cpm.equals(is))

			{
				diagFound = true;
				editor.setActiveDiagram(diag);
			}
		}
	}

	/**
	 * Remove the last added InputPort (and OutputPort)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{
		return;
	}
}
