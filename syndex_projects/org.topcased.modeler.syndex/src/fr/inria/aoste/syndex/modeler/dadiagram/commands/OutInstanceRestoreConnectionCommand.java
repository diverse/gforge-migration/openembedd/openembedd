/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependencyOUT;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;

/**
 * OutInstance restore connection command
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class OutInstanceRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public OutInstanceRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof OutInstance)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);

					if (eltObject2 instanceof INInstance)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if the elt is the source of the edge or if it is the target and that the
							// SourceTargetCouple is reversible
							createInternalDataConnectionFromOutInstanceToINInstance_TargetINInstance(elt, elt2);
						}
					}

					if (eltObject2 instanceof InCondInstance)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if the elt is the source of the edge or if it is the target and that the
							// SourceTargetCouple is reversible
							createInternalDataConnectionFromOutInstanceToInCondInstance_TargetINInstanceC(elt, elt2);
						}
					}

					if (eltObject2 instanceof Output)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if the elt is the source of the edge or if it is the target and that the
							// SourceTargetCouple is reversible
							createInternalDependencyOUTFromOutInstanceToOutput_TargetIDO(elt, elt2);
						}
					}

				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createInternalDataConnectionFromOutInstanceToINInstance_TargetINInstance(GraphElement srcElt,
			GraphElement targetElt)
	{
		OutInstance sourceObject = (OutInstance) Utils.getElement(srcElt);
		INInstance targetObject = (INInstance) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDataConnection)
			{
				InternalDataConnection edgeObject = (InternalDataConnection) obj;
				if (targetObject.equals(edgeObject.getTargetINInstance())
						&& sourceObject.equals(edgeObject.getSourceOutInstance()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDataConnection.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDataConnectionEdgeCreationCommand cmd = new InternalDataConnectionEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createInternalDataConnectionFromOutInstanceToInCondInstance_TargetINInstanceC(GraphElement srcElt,
			GraphElement targetElt)
	{
		OutInstance sourceObject = (OutInstance) Utils.getElement(srcElt);
		InCondInstance targetObject = (InCondInstance) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDataConnection)
			{
				InternalDataConnection edgeObject = (InternalDataConnection) obj;
				if (targetObject.equals(edgeObject.getTargetINInstanceC())
						&& sourceObject.equals(edgeObject.getSourceOutInstance()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDataConnection.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDataConnectionEdgeCreationCommand cmd = new InternalDataConnectionEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	private void createInternalDependencyOUTFromOutInstanceToOutput_TargetIDO(GraphElement srcElt,
			GraphElement targetElt)
	{
		OutInstance sourceObject = (OutInstance) Utils.getElement(srcElt);
		Output targetObject = (Output) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDependencyOUT)
			{
				InternalDependencyOUT edgeObject = (InternalDependencyOUT) obj;
				if (targetObject.equals(edgeObject.getTargetIDO()) && sourceObject.equals(edgeObject.getSourceIDO())
						&& targetObject.equals(sourceObject.getOwnerOutput()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDependencyOUT.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDependencyOUTEdgeCreationCommand cmd = new InternalDependencyOUTEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

}
