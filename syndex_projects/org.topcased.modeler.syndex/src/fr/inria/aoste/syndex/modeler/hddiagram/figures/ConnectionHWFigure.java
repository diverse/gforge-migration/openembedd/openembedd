/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.figures;

import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;

/**
 * @generated
 */
public class ConnectionHWFigure extends PolylineConnectionEx
{

	/**
	 * The constructor
	 * 
	 * @generated
	 */
	public ConnectionHWFigure()
	{
		super();
	}

}
