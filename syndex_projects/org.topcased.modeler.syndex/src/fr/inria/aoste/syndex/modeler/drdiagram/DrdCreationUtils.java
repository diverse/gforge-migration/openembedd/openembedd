/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 * 
 * @generated
 */
public class DrdCreationUtils extends AbstractCreationUtils
{

	/**
	 * Constructor
	 * 
	 * @param diagramConf
	 *        the Diagram Graphical Configuration
	 * @generated
	 */
	public DrdCreationUtils(DiagramGraphConf diagramConf)
	{
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private class GraphicSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The presentation of the graphical element
		 * 
		 * @generated
		 */
		private String	presentation;

		/**
		 * Constructor
		 * 
		 * @param presentation
		 *        the presentation of the graphical element
		 * @generated
		 */
		public GraphicSyndexSwitch(String presentation)
		{
			this.presentation = presentation;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSensor(fr.inria.aoste.syndex.Sensor)
		 * @generated
		 */
		public Object caseSensor(fr.inria.aoste.syndex.Sensor object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementSensor(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseActuator(fr.inria.aoste.syndex.Actuator)
		 * @generated
		 */
		public Object caseActuator(fr.inria.aoste.syndex.Actuator object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementActuator(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseApplication(fr.inria.aoste.syndex.Application)
		 * @generated
		 */
		public Object caseApplication(fr.inria.aoste.syndex.Application object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementApplication(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseFunction(fr.inria.aoste.syndex.Function)
		 * @generated
		 */
		public Object caseFunction(fr.inria.aoste.syndex.Function object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementFunction(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConstant(fr.inria.aoste.syndex.Constant)
		 * @generated
		 */
		public Object caseConstant(fr.inria.aoste.syndex.Constant object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementConstant(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDelay(fr.inria.aoste.syndex.Delay)
		 * @generated
		 */
		public Object caseDelay(fr.inria.aoste.syndex.Delay object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementDelay(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInput(fr.inria.aoste.syndex.Input)
		 * @generated
		 */
		public Object caseInput(fr.inria.aoste.syndex.Input object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInput(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutput(fr.inria.aoste.syndex.Output)
		 * @generated
		 */
		public Object caseOutput(fr.inria.aoste.syndex.Output object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementOutput(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondPort(fr.inria.aoste.syndex.InCondPort)
		 * @generated
		 */
		public Object caseInCondPort(fr.inria.aoste.syndex.InCondPort object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementInCondPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseParameterDeclaration(fr.inria.aoste.syndex.ParameterDeclaration)
		 * @generated
		 */
		public Object caseParameterDeclaration(fr.inria.aoste.syndex.ParameterDeclaration object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementParameterDeclaration(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseCompoundStructure(fr.inria.aoste.syndex.CompoundStructure)
		 * @generated
		 */
		public Object caseCompoundStructure(fr.inria.aoste.syndex.CompoundStructure object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementCompoundStructure(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConditionedStructure(fr.inria.aoste.syndex.ConditionedStructure)
		 * @generated
		 */
		public Object caseConditionedStructure(fr.inria.aoste.syndex.ConditionedStructure object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementConditionedStructure(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseElementaryStructure(fr.inria.aoste.syndex.ElementaryStructure)
		 * @generated
		 */
		public Object caseElementaryStructure(fr.inria.aoste.syndex.ElementaryStructure object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementElementaryStructure(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject,
	 *      java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation)
	{
		Object graphElt = null;

		if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(obj.eClass().getEPackage().getNsURI()))
		{
			graphElt = new GraphicSyndexSwitch(presentation).doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSensor(fr.inria.aoste.syndex.Sensor element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementActuator(fr.inria.aoste.syndex.Actuator element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementApplication(fr.inria.aoste.syndex.Application element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFunction(fr.inria.aoste.syndex.Function element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated NOT
	 */
	protected GraphElement createGraphElementConstant(fr.inria.aoste.syndex.Constant element, String presentation)
	{
		GraphNode graphNode = createGraphNode(element, presentation);
		graphNode.getContained().add(createGraphNode(element.getOneOutputConstant()));
		return graphNode;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated NOT
	 */
	protected GraphElement createGraphElementDelay(fr.inria.aoste.syndex.Delay element, String presentation)
	{
		GraphNode graphNode = createGraphNode(element, presentation);
		graphNode.getContained().add(createGraphNode(element.getOneInputDelay()));
		graphNode.getContained().add(createGraphNode(element.getOneOutputDelay()));
		return graphNode;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInput(fr.inria.aoste.syndex.Input element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementOutput(fr.inria.aoste.syndex.Output element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementInCondPort(fr.inria.aoste.syndex.InCondPort element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameterDeclaration(fr.inria.aoste.syndex.ParameterDeclaration element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return
	 * @generated
	 */
	protected GraphElement createGraphElementCompoundStructure(fr.inria.aoste.syndex.CompoundStructure element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementConditionedStructure(fr.inria.aoste.syndex.ConditionedStructure element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementElementaryStructure(fr.inria.aoste.syndex.ElementaryStructure element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj
	 *        the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj)
	{
		return obj;
	}

}
