/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import org.eclipse.gef.EditDomain;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ComponentInstance;

public class ComponentInstanceClassifierEdgeCreationCommand extends CreateTypedEdgeCommand
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param domain
	 * @param newObj
	 * @param src
	 * @param needModelUpdate
	 * @generated
	 */
	public ComponentInstanceClassifierEdgeCreationCommand(EditDomain domain, GraphEdge newObj, GraphElement src,
			boolean needModelUpdate)
	{
		super(domain, newObj, src, needModelUpdate);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void redoModel()
	{
		if (Utils.getElement(getSource()) instanceof ComponentInstance)
		{
			((ComponentInstance) Utils.getElement(getSource())).setReferencedComponent((ApplicationComponent) Utils
					.getElement(getTarget()));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void undoModel()
	{
		if (Utils.getElement(getSource()) instanceof ComponentInstance)
		{
			((ComponentInstance) Utils.getElement(getSource())).setReferencedComponent(null);
		}
	}

}
