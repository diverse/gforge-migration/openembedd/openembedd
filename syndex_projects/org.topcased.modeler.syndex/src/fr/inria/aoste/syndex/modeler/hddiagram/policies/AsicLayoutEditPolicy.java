/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.INOUT;

/**
 * @generated
 */
public class AsicLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public AsicLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isAttachedToBorder(org.topcased.modeler.di.model.GraphNode)
	 * @generated
	 */
	protected boolean isAttachedToBorder(GraphNode node)
	{
		if (Utils.getElement(node) instanceof INOUT)
		{
			return true;
		}
		return super.isAttachedToBorder(node);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof INOUT)
		{
			return true;
		}
		return false;
	}

}
