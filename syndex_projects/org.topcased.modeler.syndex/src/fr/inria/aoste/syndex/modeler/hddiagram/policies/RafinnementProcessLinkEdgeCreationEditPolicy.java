/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy;
import org.topcased.modeler.utils.SourceTargetData;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.modeler.hddiagram.HddSimpleObjectConstants;
import fr.inria.aoste.syndex.modeler.hddiagram.commands.RafinnementProcessLinkEdgeCreationCommand;

/**
 * RafinnementProcessLink edge creation
 * 
 * @generated
 */
public class RafinnementProcessLinkEdgeCreationEditPolicy extends AbstractEdgeCreationEditPolicy
{
	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#createCommand(org.eclipse.gef.EditDomain,
	 *      org.topcased.modeler.di.model.GraphEdge, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected CreateTypedEdgeCommand createCommand(EditDomain domain, GraphEdge edge, GraphElement source)
	{
		return new RafinnementProcessLinkEdgeCreationCommand(domain, edge, source);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkEdge(org.topcased.modeler.di.model.GraphEdge)
	 * @generated
	 */
	protected boolean checkEdge(GraphEdge edge)
	{
		if (edge.getSemanticModel() instanceof SimpleSemanticModelElement)
		{
			return HddSimpleObjectConstants.SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK
					.equals(((SimpleSemanticModelElement) edge.getSemanticModel()).getTypeInfo());
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkSource(org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkSource(GraphElement source)
	{
		EObject object = Utils.getElement(source);
		if (object instanceof fr.inria.aoste.syndex.FPGA)
		{
			return true;
		}
		if (object instanceof fr.inria.aoste.syndex.Asic)
		{
			return true;
		}
		if (object instanceof fr.inria.aoste.syndex.DSP)
		{
			return true;
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkTargetForSource(org.topcased.modeler.di.model.GraphElement,
	 *      org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkTargetForSource(GraphElement source, GraphElement target)
	{
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof fr.inria.aoste.syndex.FPGA
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}

		if (sourceObject instanceof fr.inria.aoste.syndex.Asic
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}

		if (sourceObject instanceof fr.inria.aoste.syndex.DSP
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			if (!sourceObject.equals(targetObject))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkCommand(org.eclipse.gef.commands.Command)
	 * @generated
	 */
	protected boolean checkCommand(Command command)
	{
		return command instanceof RafinnementProcessLinkEdgeCreationCommand;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#getSourceTargetData(org.topcased.modeler.di.model.GraphElement,
	 *      org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected SourceTargetData getSourceTargetData(GraphElement source, GraphElement target)
	{
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof fr.inria.aoste.syndex.FPGA
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentProcessor", null, "parentProcessor");
		}
		if (sourceObject instanceof fr.inria.aoste.syndex.Asic
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentProcessor", null, "parentProcessor");
		}
		if (sourceObject instanceof fr.inria.aoste.syndex.DSP
				&& targetObject instanceof fr.inria.aoste.syndex.Processor)
		{
			return new SourceTargetData(false, false, SourceTargetData.NONE, null, null, null, null, null,
				"parentProcessor", null, "parentProcessor");
		}
		return null;
	}

}
