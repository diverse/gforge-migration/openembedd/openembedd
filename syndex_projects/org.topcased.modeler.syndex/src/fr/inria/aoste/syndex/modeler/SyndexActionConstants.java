/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Fadoi LAKHAL, Yves SOREL (INRIA Rocquencourt)
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler;

public interface SyndexActionConstants
{

	String	ADD_NEW_PORT				= "addNewPort";

	String	ADD_NEW_PORT_CONDITIONED	= "addNewPortConditioned";

	String	ADD_NEW_PORT_COMPOUND		= "addNewPortCompound";

	String	RELOAD_COMPONENTINSTANCE	= "reloadComponentInstance";

	String	RELOAD_CONDITIONEDSTRUCTURE	= "reloadConditionedStructure";

	String	RELOAD_COMPOUNDSTRUCTURE	= "reloadCompoundStructure";

	String	OPEN_PARENT_DIAGRAM			= "openparentdiagram";

	String	GENERATE_SDX_CODE			= "generateSDXCode";

}
