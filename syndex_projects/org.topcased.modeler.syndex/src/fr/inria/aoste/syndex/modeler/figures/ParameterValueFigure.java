/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.figures;

/**
 * @generated
 */
public class ParameterValueFigure extends org.topcased.draw2d.figures.CommentFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public ParameterValueFigure()
	{
		super();
	}

}
