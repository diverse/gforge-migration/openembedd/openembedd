/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.InCondPort;

/**
 * @generated
 */
public class ConditionLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public ConditionLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof ComponentInstance)
		{
			return true;
		}
		if (child instanceof InCondPort)
		{
			return true;
		}
		return false;
	}

}
