/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

/**
 * @generated
 */
public class EditPart2ModelAdapterFactory extends org.topcased.modeler.editor.EditPart2ModelAdapterFactory
{

	/**
	 * Constructor
	 * 
	 * @param adaptableClass
	 * @param adapterType
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EditPart2ModelAdapterFactory(Class adaptableClass, Class adapterType)
	{
		super(adaptableClass, adapterType);
	}

}
