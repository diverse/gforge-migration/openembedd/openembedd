/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;

import fr.inria.aoste.syndex.modeler.dadiagram.figures.InternalDataConnectionFigure;

/**
 * InternalDataConnection controller
 * 
 * @generated
 */
public class InternalDataConnectionEditPart extends EMFGraphEdgeEditPart
{

	/**
	 * Constructor
	 * 
	 * @param model
	 *        the graph object
	 * @generated
	 */
	public InternalDataConnectionEditPart(GraphEdge model)
	{
		super(model);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.CHANGE_FONT_EDITPOLICY, null);

	}

	/**
	 * @return the Figure
	 * @generated
	 */
	protected IFigure createFigure()
	{
		InternalDataConnectionFigure connection = new InternalDataConnectionFigure();

		createTargetDecoration(connection);

		return connection;
	}

	/**
	 * @param connection
	 *        the PolylineConnection
	 * @generated
	 */
	private void createTargetDecoration(PolylineConnection connection)
	{

		PolylineDecoration decoration = new PolylineDecoration();
		decoration.setScale(10, 5);
		connection.setTargetDecoration(decoration);

	}

}
