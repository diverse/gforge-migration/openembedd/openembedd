/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.ParameterValue;

/**
 * @generated
 */
public class ComponentInstanceLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public ComponentInstanceLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof OutInstance)
		{
			return true;
		}
		if (child instanceof INInstance)
		{
			return true;
		}
		if (child instanceof InCondPort)
		{
			return true;
		}
		if (child instanceof ParameterValue)
		{
			return true;
		}
		if (child instanceof InCondInstance)
		{
			return true;
		}
		return false;
	}

}
