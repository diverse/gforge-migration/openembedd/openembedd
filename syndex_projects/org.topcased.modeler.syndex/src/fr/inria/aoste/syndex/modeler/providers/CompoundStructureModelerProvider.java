/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.providers;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EAttribute;
import org.topcased.modeler.providers.ILabelFeatureProvider;

/**
 * This is the item provider adpater for a {@link fr.inria.aoste.syndex.CompoundStructure} object.
 * 
 * @generated
 */
public class CompoundStructureModelerProvider extends InternalStructureModelerProvider implements ILabelFeatureProvider
{
	/**
	 * This constructs an instance from a factory and a notifier.
	 * 
	 * @param adapterFactory
	 *        the adapter factory
	 * @generated
	 */
	public CompoundStructureModelerProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * @see org.topcased.modeler.providers.ILabelFeatureProvider#getLabelFeature(java.lang.Object)
	 * @generated
	 */
	public EAttribute getLabelFeature(Object object)
	{
		return null;
	}
}
