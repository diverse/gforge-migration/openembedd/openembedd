/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.commands;

import java.util.Iterator;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexFactory;

public class UpdateCompoundPortInstanceListsCommand extends CompoundCommand
{
	/** The graph node that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param gNode
	 *        the GraphNode
	 */
	public UpdateCompoundPortInstanceListsCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Update port lists");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (Utils.getElement(container) instanceof CompoundStructure)
			initialize();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */

	@SuppressWarnings("unchecked")
	public void initialize()
	{

		CompoundStructure mi = (CompoundStructure) Utils.getElement(container);
		ApplicationComponent id = mi.getOwnerInternalStructure();

		if (id == null)
		{
			mi.getPortCompoundStructure().clear();
			container.getContained().clear();
		}
		else
		{
			if (id instanceof Application)
			{
				int inputmax = ((Application) id).getInputPortApplication().size();
				int outputmax = ((Application) id).getOutputPortApplication().size();
				int resultat = inputmax + outputmax;

				boolean resin = true;
				boolean res = true;
				if (mi.getPortCompoundStructure().size() == resultat)
				{
					res = false;
					resin = false;;
				}
				for (Iterator it = ((Application) id).getInputPortApplication().iterator(); it.hasNext();)
				{
					Input in = (Input) it.next();

					if (res)
					{
						Input ref = SyndexFactory.eINSTANCE.createInput();
						ref.setName(in.getName());
						ref.setParentCompoundStructure(mi);
						ref.setParentCompoundStructure(mi);
					}

				}
				for (Iterator it = ((Application) id).getOutputPortApplication().iterator(); it.hasNext();)
				{

					Output out = (Output) it.next();

					if (resin)
					{
						Output ref = SyndexFactory.eINSTANCE.createOutput();
						ref.setName(out.getName());
						ref.setParentCompoundStructure(mi);
						ref.setParentCompoundStructure(mi);
					}

				}
			}
			// fin else
		}

		add(new CreatePortInstanceNodeCompoundCommand(editDomain, container, creationUtils));
	}

}
