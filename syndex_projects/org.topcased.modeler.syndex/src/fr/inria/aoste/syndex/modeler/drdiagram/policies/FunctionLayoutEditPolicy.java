/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

/**
 * @generated
 */
public class FunctionLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public FunctionLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof Input)
		{
			return true;
		}
		if (child instanceof Output)
		{
			return true;
		}
		if (child instanceof ElementaryStructure)
		{
			return true;
		}
		return false;
	}

}
