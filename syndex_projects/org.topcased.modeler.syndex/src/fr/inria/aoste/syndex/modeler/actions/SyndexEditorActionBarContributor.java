/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.actions;

import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.actions.RetargetAction;
import org.topcased.modeler.actions.ModelerActionBarContributor;

import fr.inria.aoste.syndex.modeler.SyndexActionConstants;
import fr.inria.aoste.syndex.modeler.SyndexImageRegistry;

/**
 * Generated Actions
 * 
 * @generated
 */
public class SyndexEditorActionBarContributor extends ModelerActionBarContributor
{
	/**
	 * @see org.eclipse.gef.ui.actions.ActionBarContributor#buildActions()
	 * @generated NOT
	 */
	protected void buildActions()
	{
		super.buildActions();

		RetargetAction action = new RetargetAction(SyndexActionConstants.GENERATE_SDX_CODE, "Generate SDX File");
		action.setImageDescriptor(SyndexImageRegistry.getImageDescriptor("BUTTON"));
		action.setToolTipText("Generate SDX File (F12)");
		addRetargetAction(action);
	}

	/**
	 * Add the actions to the ToolBar
	 * 
	 * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToToolBar(org.eclipse.jface.action.IToolBarManager)
	 * @generated NOT
	 */
	public void contributeToToolBar(IToolBarManager toolBarManager)
	{
		super.contributeToToolBar(toolBarManager);

		// Change Aspect actions
		toolBarManager.add(new Separator());
		toolBarManager.add(getAction(SyndexActionConstants.GENERATE_SDX_CODE));
	}
}
