/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.policies.LabelDirectEditPolicy;
import org.topcased.modeler.edit.policies.ResizableEditPolicy;
import org.topcased.modeler.edit.policies.RestoreEditPolicy;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.requests.RestoreConnectionsRequest;

import fr.inria.aoste.syndex.CompoundStructure;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.modeler.drdiagram.commands.CompoundStructureRestoreConnectionCommand;
import fr.inria.aoste.syndex.modeler.drdiagram.commands.UpdateCompoundPortInstanceListsCommand;
import fr.inria.aoste.syndex.modeler.drdiagram.figures.CompoundStructureFigure;
import fr.inria.aoste.syndex.modeler.drdiagram.policies.CompoundStructureLayoutEditPolicy;

/**
 * The CompoundStructure object controller
 * 
 * @generated
 */
public class CompoundStructureEditPart extends EMFGraphNodeEditPart
{
	/**
	 * Constructor
	 * 
	 * @param obj
	 *        the graph node
	 * @generated
	 */
	public CompoundStructureEditPart(GraphNode obj)
	{
		super(obj);
	}

	/**
	 * Creates edit policies and associates these with roles
	 * 
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.RESTORE_EDITPOLICY, new RestoreEditPolicy()
		{
			protected Command getRestoreConnectionsCommand(RestoreConnectionsRequest request)
			{
				return new CompoundStructureRestoreConnectionCommand(getHost());
			}
		});

		installEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY, new ResizableEditPolicy());

		installEditPolicy(EditPolicy.LAYOUT_ROLE, new CompoundStructureLayoutEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new LabelDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 * @generated NOT
	 */
	protected IFigure createFigure()
	{
		Command command = new UpdateCompoundPortInstanceListsCommand(getViewer().getEditDomain(),
			(GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer()).getModelerEditor().getActiveConfiguration()
					.getCreationUtils());
		if (command.canExecute())
			command.execute();

		IFigure fig = new CompoundStructureFigure();

		return fig;
	}

	/**
	 * @see org.topcased.modeler.edit.BaseEditPart#handleModelChanged(org.eclipse.emf.common.notify.Notification)
	 * @generated NOT
	 */
	protected void handleModelChanged(Notification msg)
	{
		if (msg.getNotifier() instanceof CompoundStructure)
		{
			if (msg.getFeatureID(CompoundStructure.class) == SyndexPackage.COMPOUND_STRUCTURE__OWNER_INTERNAL_STRUCTURE)
			{
				Command command = new UpdateCompoundPortInstanceListsCommand(getViewer().getEditDomain(),
					(GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer()).getModelerEditor()
							.getActiveConfiguration().getCreationUtils());
				if (command.canExecute())
					command.execute();

				this.refreshVisuals();
			}

		}
		super.handleModelChanged(msg);
	}

}
