/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.InternalDataConnection;
import fr.inria.aoste.syndex.InternalDependencyIN;
import fr.inria.aoste.syndex.OutInstance;

/**
 * InCondInstance restore connection command
 * 
 * @generated
 */
public class InCondInstanceRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public InCondInstanceRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	protected void initializeCommands()
	{

		GraphElement elt = getGraphElement();
		EObject eltObject = Utils.getElement(elt);

		if (eltObject instanceof InCondInstance)
		{
			Iterator itDiagContents = getModeler().getActiveDiagram().eAllContents();
			while (itDiagContents.hasNext())
			{
				Object obj = itDiagContents.next();
				// FIXME Change the way to handle EList GraphNodes
				if (obj instanceof GraphElement
						&& DIUtils.getProperty((GraphElement) obj, ModelerPropertyConstants.ESTRUCTURAL_FEATURE_ID) == null)
				{
					boolean autoRef = obj.equals(elt);
					GraphElement elt2 = (GraphElement) obj;
					EObject eltObject2 = Utils.getElement(elt2);

					if (eltObject2 instanceof OutInstance)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createInternalDataConnectionFromOutInstanceToInCondInstance_TargetINInstanceC(elt2, elt);
						}
					}

					if (eltObject2 instanceof Input)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createInternalDependencyINFromInputToInCondInstance_TargetIDIC(elt2, elt);
						}
					}

					if (eltObject2 instanceof InCondPort)
					{
						if (autoRef)
						{
							// autoRef not allowed
						}
						else
						{
							// if elt is the target of the edge or if it is the source and that the SourceTargetCouple
							// is reversible
							createInternalDependencyINFromInCondPortToInCondInstance_TargetIDIC(elt2, elt);
						}
					}

				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private void createInternalDataConnectionFromOutInstanceToInCondInstance_TargetINInstanceC(GraphElement srcElt,
			GraphElement targetElt)
	{
		OutInstance sourceObject = (OutInstance) Utils.getElement(srcElt);
		InCondInstance targetObject = (InCondInstance) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDataConnection)
			{
				InternalDataConnection edgeObject = (InternalDataConnection) obj;
				if (targetObject.equals(edgeObject.getTargetINInstanceC())
						&& sourceObject.equals(edgeObject.getSourceOutInstance()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDataConnection.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDataConnectionEdgeCreationCommand cmd = new InternalDataConnectionEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private void createInternalDependencyINFromInputToInCondInstance_TargetIDIC(GraphElement srcElt,
			GraphElement targetElt)
	{
		Input sourceObject = (Input) Utils.getElement(srcElt);
		InCondInstance targetObject = (InCondInstance) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDependencyIN)
			{
				InternalDependencyIN edgeObject = (InternalDependencyIN) obj;
				if (targetObject.equals(edgeObject.getTargetIDIC()) && sourceObject.equals(edgeObject.getSourceIDI()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDependencyIN.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDependencyINEdgeCreationCommand cmd = new InternalDependencyINEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

	/**
	 * @param srcElt
	 *        the source element
	 * @param targetElt
	 *        the target element
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private void createInternalDependencyINFromInCondPortToInCondInstance_TargetIDIC(GraphElement srcElt,
			GraphElement targetElt)
	{
		InCondPort sourceObject = (InCondPort) Utils.getElement(srcElt);
		InCondInstance targetObject = (InCondInstance) Utils.getElement(targetElt);

		EList edgeObjectList = ((fr.inria.aoste.syndex.InternalStructure) Utils.getDiagramModelObject(srcElt))
				.getInternalConnector();
		for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
		{
			Object obj = it.next();
			if (obj instanceof InternalDependencyIN)
			{
				InternalDependencyIN edgeObject = (InternalDependencyIN) obj;
				if (targetObject.equals(edgeObject.getTargetIDIC()) && sourceObject.equals(edgeObject.getSourceIDIC()))
				{
					// check if the relation does not exists yet
					List existing = getExistingEdges(srcElt, targetElt, InternalDependencyIN.class);
					if (!isAlreadyPresent(existing, edgeObject))
					{
						ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
						// restore the link with its default presentation
						GraphElement edge = factory.createGraphElement(edgeObject);
						if (edge instanceof GraphEdge)
						{
							InternalDependencyINEdgeCreationCommand cmd = new InternalDependencyINEdgeCreationCommand(
								getEditDomain(), (GraphEdge) edge, srcElt, false);
							cmd.setTarget(targetElt);
							add(cmd);
						}
					}
				}
			}
		}
	}

}
