/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler;

public interface SyndexRequestConstants
{

	String	REQ_ADD_NEW_PORT				= "addNewPort";

	String	REQ_ADD_NEW_PORT_CONDITIONED	= "addNewPortConditioned";

	String	REQ_ADD_NEW_PORT_COMPOUND		= "addNewPortCompound";

	String	REQ_RELOAD_COMPONENTINSTANCE	= "reloadComponentInstance";

	String	REQ_RELOAD_CONDITIONEDSTRUCTURE	= "reloadConditionedStructure";

	String	REQ_RELOAD_COMPOUNDSTRUCTURE	= "reloadCompoundStructure";

	String	REQ_OPEN_PARENT_DIAGRAM			= "openparentdiagram";

}
