/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.GraphEdgeEditPart;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.modeler.drdiagram.edit.ActuatorEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ApplicationEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.CompoundStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConditionEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConditionedStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ConstantEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.DelayEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.DrdDiagramEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ElementaryStructureEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.FunctionEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.InCondPortEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.InputEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.OutputEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ParameterDeclarationEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.ParameterValueEditPart;
import fr.inria.aoste.syndex.modeler.drdiagram.edit.SensorEditPart;
import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * Part Factory : associates a model object to its controller. <br>
 * 
 * @generated
 */
public class DrdEditPartFactory implements EditPartFactory
{
	/**
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,java.lang.Object)
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof Diagram)
		{
			return new DrdDiagramEditPart((Diagram) model);
		}
		else if (model instanceof GraphNode)
		{
			final GraphNode node = (GraphNode) model;
			EObject element = Utils.getElement(node);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new NodeSyndexSwitch(node).doSwitch(element);
				}

				return new EMFGraphNodeEditPart(node);
			}

			if (node.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
			}
			return new GraphNodeEditPart(node);
		}
		else if (model instanceof GraphEdge)
		{
			final GraphEdge edge = (GraphEdge) model;
			EObject element = Utils.getElement(edge);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new EdgeSyndexSwitch(edge).doSwitch(element);
				}

				return new EMFGraphEdgeEditPart(edge);
			}

			if (edge.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
			}

			return new GraphEdgeEditPart(edge);
		}

		throw new IllegalStateException("No edit part matches with the '" + model.getClass().getName()
				+ "' model element. Check DrdEditPartFactory#createEditPart(EditPart,Object) class");
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private class NodeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical node
		 * 
		 * @generated
		 */
		private GraphNode	node;

		/**
		 * Constructor
		 * 
		 * @param node
		 *        the graphical node
		 * @generated
		 */
		public NodeSyndexSwitch(GraphNode node)
		{
			this.node = node;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseActuator(fr.inria.aoste.syndex.Actuator)
		 * @generated
		 */
		public Object caseActuator(fr.inria.aoste.syndex.Actuator object)
		{
			return new ActuatorEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSensor(fr.inria.aoste.syndex.Sensor)
		 * @generated
		 */
		public Object caseSensor(fr.inria.aoste.syndex.Sensor object)
		{
			return new SensorEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseApplication(fr.inria.aoste.syndex.Application)
		 * @generated
		 */
		public Object caseApplication(fr.inria.aoste.syndex.Application object)
		{
			return new ApplicationEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseFunction(fr.inria.aoste.syndex.Function)
		 * @generated
		 */
		public Object caseFunction(fr.inria.aoste.syndex.Function object)
		{
			return new FunctionEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConstant(fr.inria.aoste.syndex.Constant)
		 * @generated
		 */
		public Object caseConstant(fr.inria.aoste.syndex.Constant object)
		{
			return new ConstantEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDelay(fr.inria.aoste.syndex.Delay)
		 * @generated
		 */
		public Object caseDelay(fr.inria.aoste.syndex.Delay object)
		{
			return new DelayEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInput(fr.inria.aoste.syndex.Input)
		 * @generated
		 */
		public Object caseInput(fr.inria.aoste.syndex.Input object)
		{
			return new InputEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutput(fr.inria.aoste.syndex.Output)
		 * @generated
		 */
		public Object caseOutput(fr.inria.aoste.syndex.Output object)
		{
			return new OutputEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance)
		 * @generated
		 */
		public Object caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance object)
		{
			return new ComponentInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondPort(fr.inria.aoste.syndex.InCondPort)
		 * @generated
		 */
		public Object caseInCondPort(fr.inria.aoste.syndex.InCondPort object)
		{
			return new InCondPortEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseCondition(fr.inria.aoste.syndex.Condition)
		 * @generated
		 */
		public Object caseCondition(fr.inria.aoste.syndex.Condition object)
		{
			return new ConditionEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseCompoundStructure(fr.inria.aoste.syndex.CompoundStructure)
		 * @generated
		 */
		public Object caseCompoundStructure(fr.inria.aoste.syndex.CompoundStructure object)
		{
			return new CompoundStructureEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConditionedStructure(fr.inria.aoste.syndex.ConditionedStructure)
		 * @generated
		 */
		public Object caseConditionedStructure(fr.inria.aoste.syndex.ConditionedStructure object)
		{
			return new ConditionedStructureEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseElementaryStructure(fr.inria.aoste.syndex.ElementaryStructure)
		 * @generated
		 */
		public Object caseElementaryStructure(fr.inria.aoste.syndex.ElementaryStructure object)
		{
			return new ElementaryStructureEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseParameterDeclaration(fr.inria.aoste.syndex.ParameterDeclaration)
		 * @generated
		 */
		public Object caseParameterDeclaration(fr.inria.aoste.syndex.ParameterDeclaration object)
		{
			return new ParameterDeclarationEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseParameterValue(fr.inria.aoste.syndex.ParameterValue)
		 * @generated
		 */
		public Object caseParameterValue(fr.inria.aoste.syndex.ParameterValue object)
		{
			return new ParameterValueEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private class EdgeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical edge
		 * 
		 * @generated
		 */
		private GraphEdge	edge;

		/**
		 * Constructor
		 * 
		 * @param edge
		 *        the graphical edge
		 * @generated
		 */
		public EdgeSyndexSwitch(GraphEdge edge)
		{
			this.edge = edge;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphEdgeEditPart(edge);
		}
	}

}
