/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

/**
 * @generated
 */
@SuppressWarnings("unchecked")
public class EditPart2ModelAdapterFactory extends org.topcased.modeler.editor.EditPart2ModelAdapterFactory
{

	/**
	 * Constructor
	 * 
	 * @param adaptableClass
	 * @param adapterType
	 * @generated
	 */
	public EditPart2ModelAdapterFactory(Class adaptableClass, Class adapterType)
	{
		super(adaptableClass, adapterType);
	}

}
