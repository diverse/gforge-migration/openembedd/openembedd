/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.Constant;
import fr.inria.aoste.syndex.Delay;
import fr.inria.aoste.syndex.Function;
import fr.inria.aoste.syndex.INInstance;
import fr.inria.aoste.syndex.InCondInstance;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.OutInstance;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.SyndexFactory;

public class UpdatePortInstanceListsCommand extends CompoundCommand
{
	/** The graph node that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param gNode
	 *        the GraphNode
	 */
	public UpdatePortInstanceListsCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Update portInstance lists");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (Utils.getElement(container) instanceof ComponentInstance)
			initialize();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@SuppressWarnings("unchecked")
	public void initialize()
	{

		ComponentInstance mi = (ComponentInstance) Utils.getElement(container);
		ApplicationComponent id = mi.getReferencedComponent();
		if (id == null)
		{
			mi.getPortInstances().clear();

		}
		else
		{
			// Partie Actuator
			if (id instanceof Actuator)
				for (Iterator it = ((Actuator) id).getInputPortActuator().iterator(); it.hasNext();)
				{
					Input in = (Input) it.next();
					boolean res = true;
					for (Iterator itp = mi.getPortInstances().iterator(); res && itp.hasNext();)
					{
						INInstance pi = (INInstance) itp.next();
						if (pi.getOwnerInput().equals(in))
							res = false;
					}
					if (res)
					{
						INInstance ref = SyndexFactory.eINSTANCE.createINInstance();
						ref.setName(in.getName());
						ref.setOwnerInput(in);
						ref.setParentComponentInstance(mi);
					}
				}

			// Partie Sensor
			if (id instanceof Sensor)
				for (Iterator it = ((Sensor) id).getOutputPortSensor().iterator(); it.hasNext();)
				{
					Output out = (Output) it.next();
					boolean res = true;
					for (Iterator itp = mi.getPortInstances().iterator(); res && itp.hasNext();)
					{
						OutInstance pi = (OutInstance) itp.next();
						if (pi.getOwnerOutput().equals(out))
							res = false;
					}
					if (res)
					{
						OutInstance ref = SyndexFactory.eINSTANCE.createOutInstance();
						ref.setName(out.getName());
						ref.setOwnerOutput(out);
						ref.setParentComponentInstance(mi);
					}
				}

			// Partie Application

			if (id instanceof Application)
			{

				int inputmax = ((Application) id).getInputPortApplication().size();
				int outputmax = ((Application) id).getOutputPortApplication().size();
				int incondmax = ((Application) id).getPortCondition().size();
				int resultat = inputmax + outputmax + incondmax;

				boolean resin = true;
				boolean res = true;
				boolean resincond = true;
				if (mi.getPortInstances().size() == resultat)
				{
					res = false;
					resin = false;
					resincond = false;
				}
				for (Iterator it = ((Application) id).getInputPortApplication().iterator(); it.hasNext();)
				{
					Input in = (Input) it.next();

					if (res)
					{
						INInstance ref = SyndexFactory.eINSTANCE.createINInstance();
						ref.setName(in.getName());
						ref.setOwnerInput(in);
						ref.setParentComponentInstance(mi);
					}

				}
				for (Iterator it = ((Application) id).getOutputPortApplication().iterator(); it.hasNext();)
				{

					Output out = (Output) it.next();

					if (resin)
					{
						OutInstance ref = SyndexFactory.eINSTANCE.createOutInstance();
						ref.setName(out.getName());
						ref.setOwnerOutput(out);
						ref.setParentComponentInstance(mi);
					}

				}
				for (Iterator it = ((Application) id).getPortCondition().iterator(); it.hasNext();)
				{

					InCondPort incond = (InCondPort) it.next();

					if (resincond)
					{
						InCondInstance ref = SyndexFactory.eINSTANCE.createInCondInstance();
						ref.setName(incond.getName());
						ref.setOwnerCond(incond);
						ref.setParentComponentInstance(mi);
					}

				}
			}

			// Partie Constant

			if (id instanceof Constant)
			{
				int outputmax = 1;
				boolean res = true;

				if (mi.getPortInstances().size() == outputmax)
				{
					res = false;
				}
				if (res)
				{
					OutInstance ref = SyndexFactory.eINSTANCE.createOutInstance();
					ref.setName(((Constant) id).getOneOutputConstant().getName());
					ref.setOwnerOutput(((Constant) id).getOneOutputConstant());
					ref.setParentComponentInstance(mi);
				}
			}

			// Partie Delay

			if (id instanceof Delay)
			{
				int outputmax = 1;
				int inputmax = 1;
				int resultat = inputmax + outputmax;
				boolean res = true;
				boolean resin = true;

				if (mi.getPortInstances().size() == resultat)
				{
					res = false;
					resin = false;
				}
				if (res)
				{
					// Pour Input

					INInstance ref = SyndexFactory.eINSTANCE.createINInstance();
					ref.setName(((Delay) id).getOneInputDelay().getName());
					ref.setOwnerInput((((Delay) id).getOneInputDelay()));
					ref.setParentComponentInstance(mi);
				}
				// Pour Output
				if (resin)
				{
					OutInstance ref1 = SyndexFactory.eINSTANCE.createOutInstance();
					ref1.setName(((Delay) id).getOneOutputDelay().getName());
					ref1.setOwnerOutput((((Delay) id).getOneOutputDelay()));
					ref1.setParentComponentInstance(mi);

				}
			}

			// Partie Function

			if (id instanceof Function)
			{

				int inputmax = ((Function) id).getInputPortFunction().size();
				int outputmax = ((Function) id).getOutputPortFunction().size();
				int resultat = inputmax + outputmax;

				boolean resin = true;
				boolean res = true;
				if (mi.getPortInstances().size() == resultat)
				{
					res = false;
					resin = false;;
				}
				for (Iterator it = ((Function) id).getInputPortFunction().iterator(); it.hasNext();)
				{
					Input in = (Input) it.next();

					if (res)
					{
						INInstance ref = SyndexFactory.eINSTANCE.createINInstance();
						ref.setName(in.getName());
						ref.setOwnerInput(in);
						ref.setParentComponentInstance(mi);
					}

				}
				for (Iterator it = ((Function) id).getOutputPortFunction().iterator(); it.hasNext();)
				{

					Output out = (Output) it.next();

					if (resin)
					{
						OutInstance ref = SyndexFactory.eINSTANCE.createOutInstance();
						ref.setName(out.getName());
						ref.setOwnerOutput(out);
						ref.setParentComponentInstance(mi);
					}

				}
			}

			// fin du else
		}
		add(new CreatePortInstanceNodeCommand(editDomain, container, creationUtils));
	}
}
