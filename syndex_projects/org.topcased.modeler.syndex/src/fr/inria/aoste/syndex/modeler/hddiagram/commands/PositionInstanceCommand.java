/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INOUTInstance;

public class PositionInstanceCommand extends Command
{
	private GraphNode	container;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that represent the ModelInstance on which we will operate
	 */
	public PositionInstanceCommand(GraphNode element)
	{
		super("Position the port instance in a model instance");
		this.container = element;

	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}

	/**
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@SuppressWarnings("unchecked")
	public void redo()
	{
		ComponentInstance mi = (ComponentInstance) Utils.getElement(container);
		HardwareComponent hw = mi.getReferencedHWComponent();
		if (hw == null)
			return;

		// Partie Processor

		if (hw instanceof HardwareComponent)
		{
			EList ins = hw.getInOutHWComponent();

			int nbIns = ins.size();

			int height = 10;

			if (nbIns > 0)
			{
				// place the ParameterInstance, InputInstance, and OutputInstance
				int space_ins = height / (nbIns > 0 ? nbIns : 1);
				int init_out = (space_ins - 10) / 2;

				for (Iterator it = container.getContained().iterator(); it.hasNext();)
				{
					GraphElement elt = (GraphElement) it.next();
					EObject obj = Utils.getElement(elt);
					int index;

					if (obj instanceof INOUTInstance)
					{
						// Search index of the parameter in the InterfaceDefinition parameter list
						index = ins.indexOf(((INOUTInstance) obj).getOwnerINOUT());
						elt.setPosition(new Point(0, init_out + (index * space_ins)));
					}
				}
			}
		}

		// Fin fonction
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{

	}
}
