/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 * 
 * @generated
 */
public interface DadEditPolicyConstants
{

	/**
	 * The key used to install an <i>SynDEx</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	SYNDEX_EDITPOLICY					= "SynDEx EditPolicy";

	/**
	 * The key used to install an <i>Application</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	APPLICATION_EDITPOLICY				= "Application EditPolicy";

	/**
	 * The key used to install an <i>ComponentInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	COMPONENTINSTANCE_EDITPOLICY		= "ComponentInstance EditPolicy";

	/**
	 * The key used to install an <i>OutInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	OUTINSTANCE_EDITPOLICY				= "OutInstance EditPolicy";

	/**
	 * The key used to install an <i>INInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	ININSTANCE_EDITPOLICY				= "INInstance EditPolicy";

	/**
	 * The key used to install an <i>Output</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	OUTPUT_EDITPOLICY					= "Output EditPolicy";

	/**
	 * The key used to install an <i>Input</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INPUT_EDITPOLICY					= "Input EditPolicy";

	/**
	 * The key used to install an <i>InCondPort</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INCONDPORT_EDITPOLICY				= "InCondPort EditPolicy";

	/**
	 * The key used to install an <i>ParameterValue</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	PARAMETERVALUE_EDITPOLICY			= "ParameterValue EditPolicy";

	/**
	 * The key used to install an <i>InternalDataConnection</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INTERNALDATACONNECTION_EDITPOLICY	= "InternalDataConnection EditPolicy";

	/**
	 * The key used to install an <i>InternalDependencyIN</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INTERNALDEPENDENCYIN_EDITPOLICY		= "InternalDependencyIN EditPolicy";

	/**
	 * The key used to install an <i>InternalDependencyOUT</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INTERNALDEPENDENCYOUT_EDITPOLICY	= "InternalDependencyOUT EditPolicy";

	/**
	 * The key used to install an <i>Condition</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	CONDITION_EDITPOLICY				= "Condition EditPolicy";

	/**
	 * The key used to install an <i>InCondInstance</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INCONDINSTANCE_EDITPOLICY			= "InCondInstance EditPolicy";

	/**
	 * The key used to install an <i>InternalDependency</i> EditPolicy.
	 * 
	 * @generated
	 */
	String	INTERNALDEPENDENCY_EDITPOLICY		= "InternalDependency EditPolicy";

}
