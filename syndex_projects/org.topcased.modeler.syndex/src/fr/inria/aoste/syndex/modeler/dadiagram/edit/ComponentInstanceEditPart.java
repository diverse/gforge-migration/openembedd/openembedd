/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.tools.SelectEditPartTracker;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.policies.LabelDirectEditPolicy;
import org.topcased.modeler.edit.policies.ResizableEditPolicy;
import org.topcased.modeler.edit.policies.RestoreEditPolicy;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.requests.RestoreConnectionsRequest;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.modeler.dadiagram.commands.ComponentInstanceRestoreConnectionCommand;
import fr.inria.aoste.syndex.modeler.dadiagram.commands.HierarchieCommand;
import fr.inria.aoste.syndex.modeler.dadiagram.figures.ComponentInstanceFigure;
import fr.inria.aoste.syndex.modeler.dadiagram.policies.ComponentInstanceLayoutEditPolicy;

/**
 * The ComponentInstance object controller
 * 
 * @generated
 */
public class ComponentInstanceEditPart extends EMFGraphNodeEditPart
{
	/**
	 * Constructor
	 * 
	 * @param obj
	 *        the graph node
	 * @generated
	 */
	public ComponentInstanceEditPart(GraphNode obj)
	{
		super(obj);
	}

	/**
	 * Creates edit policies and associates these with roles
	 * 
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.RESTORE_EDITPOLICY, new RestoreEditPolicy()
		{
			protected Command getRestoreConnectionsCommand(RestoreConnectionsRequest request)
			{
				return new ComponentInstanceRestoreConnectionCommand(getHost());
			}
		});

		installEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY, new ResizableEditPolicy());

		installEditPolicy(EditPolicy.LAYOUT_ROLE, new ComponentInstanceLayoutEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new LabelDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 * @generated NOT
	 */
	protected IFigure createFigure()
	{
		Command command = new fr.inria.aoste.syndex.modeler.dadiagram.commands.UpdatePortInstanceListsCommand(
			getViewer().getEditDomain(), (GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer())
					.getModelerEditor().getActiveConfiguration().getCreationUtils());
		if (command.canExecute())
			command.execute();

		IFigure fig = new ComponentInstanceFigure();

		return fig;
	}

	/**
	 * @see org.topcased.modeler.edit.BaseEditPart#handleModelChanged(org.eclipse.emf.common.notify.Notification)
	 * @generated NOT
	 */
	protected void handleModelChanged(Notification msg)
	{
		if (msg.getNotifier() instanceof ComponentInstance)
		{
			if (msg.getFeatureID(ComponentInstance.class) == SyndexPackage.COMPONENT_INSTANCE__REFERENCED_COMPONENT)
			{
				Command command = new fr.inria.aoste.syndex.modeler.dadiagram.commands.UpdatePortInstanceListsCommand(
					getViewer().getEditDomain(), (GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer())
							.getModelerEditor().getActiveConfiguration().getCreationUtils());
				if (command.canExecute())
					command.execute();

				this.refreshVisuals();
			}

		}
		super.handleModelChanged(msg);
	}

	/**
	 * 
	 * @generated NOT
	 */
	@Override
	public DragTracker getDragTracker(Request req)
	{
		return new SelectEditPartTracker(this);
	}

	/**
	 * 
	 * @generated NOT
	 */
	@Override
	public void performRequest(Request req)
	{
		if (req.getType().equals(RequestConstants.REQ_OPEN))
		{
			if (getViewer() instanceof ModelerGraphicalViewer)
			{
				ModelerGraphicalViewer viewer = (ModelerGraphicalViewer) getViewer();
				Command command = new HierarchieCommand((GraphNode) getModel(), viewer,
					"fr.inria.aoste.syndex.modeler.dadiagram");
				if (command.canExecute())
					command.execute();
			}
		}
	}

}
