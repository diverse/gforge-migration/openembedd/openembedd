/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.figures;

import org.eclipse.draw2d.IFigure;
import org.topcased.draw2d.layout.BorderAttachedLayout;

/**
 * @generated
 */
public class FPGAFigure extends org.topcased.draw2d.figures.ContainerFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public FPGAFigure()
	{
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ContainerFigure#createContainer()
	 * @generated
	 */
	protected IFigure createContainer()
	{
		IFigure container = super.createContainer();
		container.setLayoutManager(new BorderAttachedLayout());
		return container;
	}

}
