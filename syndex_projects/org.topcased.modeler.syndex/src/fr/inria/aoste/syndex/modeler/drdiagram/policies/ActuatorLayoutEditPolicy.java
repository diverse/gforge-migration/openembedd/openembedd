/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.ElementaryStructure;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.ParameterDeclaration;

/**
 * @generated
 */
public class ActuatorLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public ActuatorLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof Input)
		{
			return true;
		}
		if (child instanceof ElementaryStructure)
		{
			return true;
		}
		if (child instanceof ParameterDeclaration)
		{
			return true;
		}
		return false;
	}

}
