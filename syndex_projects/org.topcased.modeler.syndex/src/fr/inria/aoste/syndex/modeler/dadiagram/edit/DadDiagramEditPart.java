/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.edit.DiagramEditPart;

import fr.inria.aoste.syndex.modeler.dadiagram.figures.DadDiagramFigure;
import fr.inria.aoste.syndex.modeler.dadiagram.policies.DadDiagramLayoutEditPolicy;

/**
 * @generated
 */
public class DadDiagramEditPart extends DiagramEditPart
{

	/**
	 * The Constructor
	 * 
	 * @param model
	 *        the root model element
	 * @generated
	 */
	public DadDiagramEditPart(Diagram model)
	{
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy()
	{
		return new DadDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated
	 */
	protected IFigure createBodyFigure()
	{
		return new DadDiagramFigure();
	}
}
