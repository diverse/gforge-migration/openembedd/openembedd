/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.edit.DiagramEditPart;

import fr.inria.aoste.syndex.modeler.hddiagram.figures.HddDiagramFigure;
import fr.inria.aoste.syndex.modeler.hddiagram.policies.HddDiagramLayoutEditPolicy;

/**
 * @generated
 */
public class HddDiagramEditPart extends DiagramEditPart
{

	/**
	 * The Constructor
	 * 
	 * @param model
	 *        the root model element
	 * @generated
	 */
	public HddDiagramEditPart(Diagram model)
	{
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy()
	{
		return new HddDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated
	 */
	protected IFigure createBodyFigure()
	{
		return new HddDiagramFigure();
	}
}
