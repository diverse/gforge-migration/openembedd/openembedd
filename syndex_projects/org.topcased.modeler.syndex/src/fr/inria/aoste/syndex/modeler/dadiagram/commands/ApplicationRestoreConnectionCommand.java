/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import org.eclipse.gef.EditPart;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;

/**
 * Application restore connection command
 * 
 * @generated
 */
public class ApplicationRestoreConnectionCommand extends AbstractRestoreConnectionCommand
{
	/**
	 * @param part
	 *        the EditPart that is restored
	 * @generated
	 */
	public ApplicationRestoreConnectionCommand(EditPart part)
	{
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
	protected void initializeCommands()
	{

	// Do nothing
	}

}
