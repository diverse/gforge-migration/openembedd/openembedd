/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram;

/**
 * @generated
 */
public class EditPart2ModelAdapterFactory extends org.topcased.modeler.editor.EditPart2ModelAdapterFactory
{

	/**
	 * Constructor
	 * 
	 * @param adaptableClass
	 * @param adapterType
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EditPart2ModelAdapterFactory(Class adaptableClass, Class adapterType)
	{
		super(adaptableClass, adapterType);
	}

}
