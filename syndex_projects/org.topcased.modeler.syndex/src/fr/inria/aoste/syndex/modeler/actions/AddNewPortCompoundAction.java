/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.actions;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IWorkbenchPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;

import fr.inria.aoste.syndex.modeler.SyndexActionConstants;
import fr.inria.aoste.syndex.modeler.SyndexRequestConstants;

public class AddNewPortCompoundAction extends WorkbenchPartAction implements ISelectionChangedListener
{
	/**
	 * The selected EditPart object
	 */
	private EMFGraphNodeEditPart	template;

	/**
	 * @param part
	 */
	public AddNewPortCompoundAction(IWorkbenchPart part)
	{
		super(part);
	}

	/**
	 * Add a new port
	 * 
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		// construct the AddNewPortRequest
		Request request = new Request(SyndexRequestConstants.REQ_ADD_NEW_PORT_COMPOUND);

		// get the Command
		Command actionCommand = template.getCommand(request);

		// execute the command
		getCommandStack().execute(actionCommand);
	}

	/**
	 * Determine if the action must appear in the context menu
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (template == null)
			return false;

		// construct the AddNewPortRequest
		Request request = new Request(SyndexRequestConstants.REQ_ADD_NEW_PORT_COMPOUND);

		// return true if the EditPart can understand the Request
		return template.understandsRequest(request);
	}

	/**
	 * Initializes the delete model object action
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	protected void init()
	{
		setId(SyndexActionConstants.ADD_NEW_PORT_COMPOUND);
		setText("Add a New Port");
	}

	/**
	 * Sets the selected EditPart and refreshes the enabled state of this action.
	 * 
	 * @param event
	 * 
	 * @see ISelectionChangedListener#selectionChanged(SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event)
	{
		ISelection s = event.getSelection();
		if (!(s instanceof IStructuredSelection))
			return;
		IStructuredSelection selection = (IStructuredSelection) s;
		template = null;

		if (selection != null && selection.size() == 1)
		{
			Object obj = selection.getFirstElement();
			if (obj instanceof EMFGraphNodeEditPart)
			{
				EMFGraphNodeEditPart emfG = (EMFGraphNodeEditPart) obj;

				template = emfG;

			}

			refresh();
		}

	}
}
