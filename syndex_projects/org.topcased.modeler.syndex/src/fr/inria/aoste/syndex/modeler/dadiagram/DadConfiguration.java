/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

import fr.inria.aoste.syndex.modeler.SyndexPlugin;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ApplicationEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ConditionEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.INInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InCondInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InCondPortEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InputEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDataConnectionEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyINEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyOUTEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.OutInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.OutputEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ParameterValueEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.SynDExEditPart;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 * 
 * @generated
 */
public class DadConfiguration implements IConfiguration
{
	/**
	 * @generated
	 */
	private DadPaletteManager	paletteManager;

	/**
	 * @generated
	 */
	private DadEditPartFactory	editPartFactory;

	/**
	 * @generated
	 */
	private DadCreationUtils	creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * 
	 * @generated
	 */
	private DiagramGraphConf	diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 * 
	 * @generated
	 */
	public DadConfiguration()
	{
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 * 
	 * @generated
	 */
	private void registerAdapters()
	{
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(SynDExEditPart.class, fr.inria.aoste.syndex.SynDEx.class),
			SynDExEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ApplicationEditPart.class, fr.inria.aoste.syndex.Application.class),
			ApplicationEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ComponentInstanceEditPart.class,
				fr.inria.aoste.syndex.ComponentInstance.class), ComponentInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(OutInstanceEditPart.class, fr.inria.aoste.syndex.OutInstance.class),
			OutInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(INInstanceEditPart.class, fr.inria.aoste.syndex.INInstance.class),
			INInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(OutputEditPart.class, fr.inria.aoste.syndex.Output.class),
			OutputEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InputEditPart.class, fr.inria.aoste.syndex.Input.class),
			InputEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InCondPortEditPart.class, fr.inria.aoste.syndex.InCondPort.class),
			InCondPortEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ParameterValueEditPart.class, fr.inria.aoste.syndex.ParameterValue.class),
			ParameterValueEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InternalDataConnectionEditPart.class,
				fr.inria.aoste.syndex.InternalDataConnection.class), InternalDataConnectionEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InternalDependencyINEditPart.class,
				fr.inria.aoste.syndex.InternalDependencyIN.class), InternalDependencyINEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InternalDependencyOUTEditPart.class,
				fr.inria.aoste.syndex.InternalDependencyOUT.class), InternalDependencyOUTEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ConditionEditPart.class, fr.inria.aoste.syndex.Condition.class),
			ConditionEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InCondInstanceEditPart.class, fr.inria.aoste.syndex.InCondInstance.class),
			InCondInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(InternalDependencyEditPart.class,
				fr.inria.aoste.syndex.InternalDependency.class), InternalDependencyEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId()
	{
		return new String("fr.inria.aoste.syndex.modeler.dadiagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName()
	{
		return new String("Application Definition Diagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			editPartFactory = new DadEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager()
	{
		if (paletteManager == null)
		{
			paletteManager = new DadPaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils()
	{
		if (creationUtils == null)
		{
			creationUtils = new DadCreationUtils(getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf()
	{
		if (diagramGraphConf == null)
		{
			URL url = SyndexPlugin.getDefault().getBundle().getResource(
				"fr/inria/aoste/syndex/modeler/dadiagram/diagram.graphconf");
			if (url != null)
			{
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null && resource.getContents().get(0) instanceof DiagramGraphConf)
				{
					diagramGraphConf = (DiagramGraphConf) resource.getContents().get(0);
				}
			}
			else
			{
				new MissingGraphConfFileException(
					"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}
