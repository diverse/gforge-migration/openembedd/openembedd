/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Iterator;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.SyndexFactory;

public class ConditionUpdatePortInstanceListsCommand extends CompoundCommand
{
	/** The graph node that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	@SuppressWarnings("unused")
	private EditDomain		editDomain;

	@SuppressWarnings("unused")
	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param gNode
	 *        the GraphNode
	 */
	public ConditionUpdatePortInstanceListsCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Update portInstance lists");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (Utils.getElement(container) instanceof Condition)
			initialize();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */

	@SuppressWarnings("unchecked")
	public void initialize()
	{

		Condition mi = (Condition) Utils.getElement(container);
		// ConditionedStructure ci= (ConditionedStructure) mi.eContainer();
		ApplicationComponent id = mi.getOwnerInternalStructure();

		if (id == null)
		{
			mi.getConditionPortReference().clear();
			container.getContained().clear();
		}

		else
		{

			// Partie Application

			if (id instanceof Application)
			{
				boolean res = true;

				int inputmax = ((Application) id).getInputPortApplication().size();
				int outputmax = ((Application) id).getOutputPortApplication().size();
				int inputcondmax = ((Application) id).getPortCondition().size();
				int resultat = inputmax + outputmax + inputcondmax;
				boolean resin = true;
				boolean resincond = true;

				if (mi.getConditionPortReference().size() == resultat)
				{
					res = false;
					resin = false;
					resincond = false;
				}

				for (Iterator it = ((Application) id).getOutputPortApplication().iterator(); it.hasNext();)
				{
					Output out = (Output) it.next();

					if (res)
					{
						Output ref = SyndexFactory.eINSTANCE.createOutput();
						ref.setName(out.getName());
						ref.setParentConditionReference(mi);
						ref.setParentPort(id);

					}

				}
				for (Iterator it = ((Application) id).getInputPortApplication().iterator(); it.hasNext();)
				{
					Input in = (Input) it.next();

					if (resin)
					{
						Input ref = SyndexFactory.eINSTANCE.createInput();
						ref.setName(in.getName());
						ref.setParentConditionReference(mi);
						ref.setParentPort(id);
					}

				}

				for (Iterator it = ((Application) id).getPortCondition().iterator(); it.hasNext();)
				{
					InCondPort incond = (InCondPort) it.next();

					if (resincond)
					{
						InCondPort ref = SyndexFactory.eINSTANCE.createInCondPort();
						ref.setName(incond.getName());
						ref.setParentConditionReference(mi);
						ref.setParentPort(id);
					}

				}

			}
			// finelse
		}

		// add(new CreatePortInstanceNodeConditionCommand(editDomain, container, creationUtils));
	}

}
