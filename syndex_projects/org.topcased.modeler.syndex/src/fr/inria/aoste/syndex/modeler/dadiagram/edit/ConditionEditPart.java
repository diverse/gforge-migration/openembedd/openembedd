/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.policies.LabelDirectEditPolicy;
import org.topcased.modeler.edit.policies.ResizableEditPolicy;
import org.topcased.modeler.edit.policies.RestoreEditPolicy;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.requests.RestoreConnectionsRequest;

import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.SyndexPackage;
import fr.inria.aoste.syndex.modeler.dadiagram.commands.ConditionRestoreConnectionCommand;
import fr.inria.aoste.syndex.modeler.dadiagram.commands.ConditionUpdatePortInstanceListsCommand;
import fr.inria.aoste.syndex.modeler.dadiagram.figures.ConditionFigure;
import fr.inria.aoste.syndex.modeler.dadiagram.policies.ConditionLayoutEditPolicy;

/**
 * The Condition object controller
 * 
 * @generated
 */
public class ConditionEditPart extends EMFGraphNodeEditPart
{
	/**
	 * Constructor
	 * 
	 * @param obj
	 *        the graph node
	 * @generated
	 */
	public ConditionEditPart(GraphNode obj)
	{
		super(obj);
	}

	/**
	 * Creates edit policies and associates these with roles
	 * 
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.RESTORE_EDITPOLICY, new RestoreEditPolicy()
		{
			protected Command getRestoreConnectionsCommand(RestoreConnectionsRequest request)
			{
				return new ConditionRestoreConnectionCommand(getHost());
			}
		});

		installEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY, new ResizableEditPolicy());

		installEditPolicy(EditPolicy.LAYOUT_ROLE, new ConditionLayoutEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new LabelDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 * @generated NOT
	 */
	protected IFigure createFigure()
	{
		Command command = new ConditionUpdatePortInstanceListsCommand(getViewer().getEditDomain(),
			(GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer()).getModelerEditor().getActiveConfiguration()
					.getCreationUtils());
		if (command.canExecute())
			command.execute();

		IFigure fig = new ConditionFigure();

		return fig;
	}

	/**
	 * @see org.topcased.modeler.edit.BaseEditPart#handleModelChanged(org.eclipse.emf.common.notify.Notification)
	 * @generated NOT
	 */
	protected void handleModelChanged(Notification msg)
	{
		if (msg.getNotifier() instanceof Condition)
		{
			if (msg.getFeatureID(Condition.class) == SyndexPackage.CONDITION__OWNER_INTERNAL_STRUCTURE)
			{
				Command command = new ConditionUpdatePortInstanceListsCommand(getViewer().getEditDomain(),
					(GraphNode) getModel(), ((ModelerGraphicalViewer) getViewer()).getModelerEditor()
							.getActiveConfiguration().getCreationUtils());
				if (command.canExecute())
					command.execute();

				this.refreshVisuals();
			}
			// Update the tooltip

		}
		super.handleModelChanged(msg);
	}

}
