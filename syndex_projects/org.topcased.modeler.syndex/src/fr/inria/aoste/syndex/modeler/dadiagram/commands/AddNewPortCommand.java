/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import org.eclipse.gef.commands.Command;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Actuator;
import fr.inria.aoste.syndex.Application;
import fr.inria.aoste.syndex.ApplicationComponent;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;
import fr.inria.aoste.syndex.Sensor;
import fr.inria.aoste.syndex.SyndexFactory;

public class AddNewPortCommand extends Command
{
	private GraphNode	applicationComponentNode;

	private GraphNode	inputPortNode;

	private GraphNode	outputPortNode;

	private Modeler		modeler;

	/**
	 * Constructor
	 * 
	 * @param element
	 *        the GraphNode that will contain the newly created Port
	 */
	public AddNewPortCommand(GraphNode element, Modeler editor)
	{
		super("Add new port");
		this.applicationComponentNode = element;
		this.modeler = editor;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	public boolean canExecute()
	{
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}

	/**
	 * Add a new InputPort object to the MultipleInputsOperator model object and a new OutputPort if the model object is
	 * a CartesianProduct
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	public void redo()
	{
		ICreationUtils factory = modeler.getActiveConfiguration().getCreationUtils();
		ApplicationComponent sigop = (ApplicationComponent) Utils.getElement(applicationComponentNode);

		if (sigop instanceof Sensor)
		{
			// create the model object
			Output outputPort = SyndexFactory.eINSTANCE.createOutput();
			((Sensor) sigop).getOutputPortSensor().add(outputPort);

			// create the graphNode and its position property
			outputPortNode = (GraphNode) factory.createGraphElement(outputPort);
			((Sensor) sigop).getOutputPortSensor().add((Output) outputPortNode);
		}
		if (sigop instanceof Actuator)
		{
			// create the model object
			Input inputPort = SyndexFactory.eINSTANCE.createInput();
			((Actuator) sigop).getInputPortActuator().add(inputPort);

			// create the graphNode and its position property
			inputPortNode = (GraphNode) factory.createGraphElement(inputPort);
			((Actuator) sigop).getInputPortActuator().add((Input) inputPortNode);
		}

		if (sigop instanceof Application)
		{
			// create the model object
			Input inputPort = SyndexFactory.eINSTANCE.createInput();
			((Application) sigop).getInputPortApplication().add(inputPort);

			// create the graphNode and its position property
			inputPortNode = (GraphNode) factory.createGraphElement(inputPort);
			((Application) sigop).getInputPortApplication().add((Input) inputPortNode);

			Output outputPort = SyndexFactory.eINSTANCE.createOutput();
			((Application) sigop).getOutputPortApplication().add(outputPort);

			// create the graphNode and its position property
			outputPortNode = (GraphNode) factory.createGraphElement(outputPort);
			((Application) sigop).getOutputPortApplication().add((Output) outputPortNode);

		}
	}

	/**
	 * Remove the last added InputPort (and OutputPort)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo()
	{
		return;
	}
}
