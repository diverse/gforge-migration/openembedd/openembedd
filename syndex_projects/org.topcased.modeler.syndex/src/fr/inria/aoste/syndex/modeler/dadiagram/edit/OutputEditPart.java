/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.topcased.draw2d.figures.LabelledPortFigure;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.policies.LabelDirectEditPolicy;
import org.topcased.modeler.edit.policies.ResizableEditPolicy;
import org.topcased.modeler.edit.policies.RestoreEditPolicy;
import org.topcased.modeler.requests.RestoreConnectionsRequest;

import fr.inria.aoste.syndex.modeler.dadiagram.DadEditPolicyConstants;
import fr.inria.aoste.syndex.modeler.dadiagram.commands.OutputRestoreConnectionCommand;
import fr.inria.aoste.syndex.modeler.dadiagram.figures.OutputFigure;
import fr.inria.aoste.syndex.modeler.dadiagram.policies.InternalDependencyEdgeCreationEditPolicy;
import fr.inria.aoste.syndex.modeler.dadiagram.policies.InternalDependencyOUTEdgeCreationEditPolicy;

/**
 * The Output object controller
 * 
 * @generated
 */
public class OutputEditPart extends org.topcased.modeler.edit.PortEditPart
{
	/**
	 * Constructor
	 * 
	 * @param obj
	 *        the graph node
	 * @generated
	 */
	public OutputEditPart(GraphNode obj)
	{
		super(obj);
	}

	/**
	 * Creates edit policies and associates these with roles
	 * 
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(DadEditPolicyConstants.INTERNALDEPENDENCYOUT_EDITPOLICY,
			new InternalDependencyOUTEdgeCreationEditPolicy());

		installEditPolicy(DadEditPolicyConstants.INTERNALDEPENDENCY_EDITPOLICY,
			new InternalDependencyEdgeCreationEditPolicy());

		installEditPolicy(ModelerEditPolicyConstants.RESTORE_EDITPOLICY, new RestoreEditPolicy()
		{
			protected Command getRestoreConnectionsCommand(RestoreConnectionsRequest request)
			{
				return new OutputRestoreConnectionCommand(getHost());
			}
		});

		installEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY, new ResizableEditPolicy());

		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new LabelDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 * @generated
	 */
	protected IFigure createFigure()
	{

		return new LabelledPortFigure(new OutputFigure());

	}

}
