/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.policies;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

/**
 * @generated
 */
public class ConditionedStructureLayoutEditPolicy extends org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy
{
	/**
	 * Default contructor.
	 * 
	 * @generated
	 */
	public ConditionedStructureLayoutEditPolicy()
	{
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	protected boolean isValid(EObject child, EObject parent)
	{
		if (child instanceof ComponentInstance)
		{
			return true;
		}
		if (child instanceof Condition)
		{
			return true;
		}
		if (child instanceof Input)
		{
			return true;
		}
		if (child instanceof Output)
		{
			return true;
		}
		return false;
	}

}
