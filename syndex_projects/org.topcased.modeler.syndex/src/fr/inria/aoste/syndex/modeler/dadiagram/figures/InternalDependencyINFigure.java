/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.swt.SWT;

/**
 * @generated
 */
public class InternalDependencyINFigure extends PolylineConnectionEx
{

	/**
	 * The constructor
	 * 
	 * @generated
	 */
	public InternalDependencyINFigure()
	{
		super();
		setLineStyle(SWT.LINE_SOLID);
	}

}
