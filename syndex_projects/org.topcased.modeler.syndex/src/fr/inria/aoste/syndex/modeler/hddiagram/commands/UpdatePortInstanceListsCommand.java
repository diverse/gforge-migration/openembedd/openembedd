/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.commands;

import java.util.Iterator;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.ComponentInstance;
import fr.inria.aoste.syndex.HardwareComponent;
import fr.inria.aoste.syndex.INOUT;
import fr.inria.aoste.syndex.INOUTInstance;
import fr.inria.aoste.syndex.SyndexFactory;

public class UpdatePortInstanceListsCommand extends CompoundCommand
{
	/** The graph node that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/**
	 * The Constructor
	 * 
	 * @param gNode
	 *        the GraphNode
	 */
	public UpdatePortInstanceListsCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils)
	{
		super("Update portInstance lists");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (Utils.getElement(container) instanceof ComponentInstance)
			initialize();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@SuppressWarnings("unchecked")
	public void initialize()
	{

		ComponentInstance mi = (ComponentInstance) Utils.getElement(container);
		HardwareComponent hw = mi.getReferencedHWComponent();
		if (hw == null)
		{
			mi.getPortInstances().clear();

		}
		else
		{
			// Partie Processor
			if (hw instanceof HardwareComponent)
				for (Iterator it = hw.getInOutHWComponent().iterator(); it.hasNext();)
				{
					INOUT inout = (INOUT) it.next();
					boolean res = true;
					for (Iterator itp = mi.getPortInstances().iterator(); res && itp.hasNext();)
					{
						INOUTInstance pi = (INOUTInstance) itp.next();
						if (pi.getOwnerINOUT().equals(inout))
							res = false;
					}
					if (res)
					{
						INOUTInstance ref = SyndexFactory.eINSTANCE.createINOUTInstance();
						ref.setName(inout.getName());
						ref.setOwnerINOUT(inout);
						ref.setParentComponentInstance(mi);
					}
				}

			// fin du else
		}
		add(new CreatePortInstanceNodeCommand(editDomain, container, creationUtils));
	}
}
