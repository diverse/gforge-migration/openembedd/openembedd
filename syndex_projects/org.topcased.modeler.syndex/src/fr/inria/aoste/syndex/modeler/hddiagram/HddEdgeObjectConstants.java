/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

/**
 * An interface defining EdgeObject Constants.<br>
 * 
 * @generated
 */
public interface HddEdgeObjectConstants
{}
