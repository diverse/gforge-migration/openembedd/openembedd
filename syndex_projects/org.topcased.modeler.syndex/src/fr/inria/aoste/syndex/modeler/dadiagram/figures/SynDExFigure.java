/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.ILabelFigure;

/**
 * @generated
 */
public class SynDExFigure extends org.eclipse.draw2d.Figure implements ILabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public SynDExFigure()
	{
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ILabelFigure#getLabel()
	 * @generated
	 */
	public ILabel getLabel()
	{
		// TODO : return the Figure that represent the Label
		return null;
	}

}
