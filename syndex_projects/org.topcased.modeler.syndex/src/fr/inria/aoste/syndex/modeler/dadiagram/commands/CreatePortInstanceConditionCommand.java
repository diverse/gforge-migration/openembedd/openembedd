/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/

package fr.inria.aoste.syndex.modeler.dadiagram.commands;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.modeler.commands.CreateGraphNodeCommand;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.Condition;
import fr.inria.aoste.syndex.InCondPort;
import fr.inria.aoste.syndex.Input;
import fr.inria.aoste.syndex.Output;

public class CreatePortInstanceConditionCommand extends CompoundCommand
{
	/** The GraphNode that should be updated */
	private GraphNode		container;

	/** The EditDomain associated */
	private EditDomain		editDomain;

	private ICreationUtils	creationUtils;

	/** Lists containing all new EObjects to add to the container graphNode */
	@SuppressWarnings("unchecked")
	private EList			newObjects	= new BasicEList();

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new InputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the InputInstance to add
	 */
	@SuppressWarnings("unchecked")
	public CreatePortInstanceConditionCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			Input in)
	{
		super("Add an Input Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (in != null)
			newObjects.add(in);

		initialize();
	}

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new OutputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the OutputInstance to add
	 */
	@SuppressWarnings("unchecked")
	public CreatePortInstanceConditionCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			Output out)
	{
		super("Add an Output Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (out != null)
			newObjects.add(out);

		initialize();
	}

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new OutputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param in
	 *        the OutputInstance to add
	 */
	@SuppressWarnings("unchecked")
	public CreatePortInstanceConditionCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			InCondPort incond)
	{
		super("Add an Incond Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (incond != null)
			newObjects.add(incond);

		initialize();
	}

	/**
	 * Constructor
	 * 
	 * @param domain
	 *        the EditDomain
	 * @param container
	 *        the graphNode in which the new InputInstance will be added
	 * @param creationUtils
	 *        the creationUtils factory
	 * @param list
	 *        the list of Port Instances (ParameterInstance, InputInstance, or OutputInstance) to add
	 */
	@SuppressWarnings("unchecked")
	public CreatePortInstanceConditionCommand(EditDomain domain, GraphNode container, ICreationUtils creationUtils,
			Collection list)
	{
		super("Add a list of Port Instance Command");

		this.editDomain = domain;
		this.container = container;
		this.creationUtils = creationUtils;

		if (list != null)
			newObjects.addAll(list);

		initialize();
	}

	@SuppressWarnings("unchecked")
	private void initialize()
	{
		EObject eObject = Utils.getElement(container);
		if (eObject instanceof Condition)
		{

			for (Iterator iter = newObjects.iterator(); iter.hasNext();)
			{
				EObject port = (EObject) iter.next();

				// check if the element is already present
				GraphNode node = getGraphicalRepresentation(port);

				if (node == null)
				{
					// create the new graphNode and add it to the parentNode
					node = (GraphNode) creationUtils.createGraphElement(port);

					if (port instanceof Output)
						add(new CreateGraphNodeCommand(editDomain, node, container, null, node.getSize(),
							PositionConstants.RIGHT));
					else
						add(new CreateGraphNodeCommand(editDomain, node, container, null, node.getSize(),
							PositionConstants.LEFT));
				}
			}
		}
	}

	/**
	 * Get the graphNode associated with the model object in the current selected editPart
	 * 
	 * @param modelObject
	 *        the model object to display
	 * @return the GraphNode or null if it does not exist
	 */
	@SuppressWarnings("unchecked")
	private GraphNode getGraphicalRepresentation(EObject modelObject)
	{
		for (Iterator iter = container.getContained().iterator(); iter.hasNext();)
		{
			GraphElement elt = (GraphElement) iter.next();
			if (Utils.getElement(elt) == modelObject && elt instanceof GraphNode)
				return (GraphNode) elt;
		}
		return null;
	}

}
