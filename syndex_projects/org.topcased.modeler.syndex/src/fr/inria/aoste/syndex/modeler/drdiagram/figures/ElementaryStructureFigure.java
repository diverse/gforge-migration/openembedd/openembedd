/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.figures;

/**
 * @generated
 */
public class ElementaryStructureFigure extends org.topcased.draw2d.figures.EditableLabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public ElementaryStructureFigure()
	{
		super();
	}

}
