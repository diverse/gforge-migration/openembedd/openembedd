/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 * 
 * @generated
 */
public class HddCreationUtils extends AbstractCreationUtils
{

	/**
	 * Constructor
	 * 
	 * @param diagramConf
	 *        the Diagram Graphical Configuration
	 * @generated
	 */
	public HddCreationUtils(DiagramGraphConf diagramConf)
	{
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private class GraphicSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The presentation of the graphical element
		 * 
		 * @generated
		 */
		private String	presentation;

		/**
		 * Constructor
		 * 
		 * @param presentation
		 *        the presentation of the graphical element
		 * @generated
		 */
		public GraphicSyndexSwitch(String presentation)
		{
			this.presentation = presentation;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseProcessor(fr.inria.aoste.syndex.Processor)
		 * @generated
		 */
		public Object caseProcessor(fr.inria.aoste.syndex.Processor object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementProcessor(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseFPGA(fr.inria.aoste.syndex.FPGA)
		 * @generated
		 */
		public Object caseFPGA(fr.inria.aoste.syndex.FPGA object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementFPGA(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDSP(fr.inria.aoste.syndex.DSP)
		 * @generated
		 */
		public Object caseDSP(fr.inria.aoste.syndex.DSP object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementDSP(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseAsic(fr.inria.aoste.syndex.Asic)
		 * @generated
		 */
		public Object caseAsic(fr.inria.aoste.syndex.Asic object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementAsic(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseMedia(fr.inria.aoste.syndex.Media)
		 * @generated
		 */
		public Object caseMedia(fr.inria.aoste.syndex.Media object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementMedia(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseRAM(fr.inria.aoste.syndex.RAM)
		 * @generated
		 */
		public Object caseRAM(fr.inria.aoste.syndex.RAM object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementRAM(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSAM(fr.inria.aoste.syndex.SAM)
		 * @generated
		 */
		public Object caseSAM(fr.inria.aoste.syndex.SAM object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementSAM(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSAMPTP(fr.inria.aoste.syndex.SAMPTP)
		 * @generated
		 */
		public Object caseSAMPTP(fr.inria.aoste.syndex.SAMPTP object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementSAMPTP(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseHardware(fr.inria.aoste.syndex.Hardware)
		 * @generated
		 */
		public Object caseHardware(fr.inria.aoste.syndex.Hardware object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementHardware(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance)
		 * @generated
		 */
		public Object caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementComponentInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINOUTInstance(fr.inria.aoste.syndex.INOUTInstance)
		 * @generated
		 */
		public Object caseINOUTInstance(fr.inria.aoste.syndex.INOUTInstance object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementINOUTInstance(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseDuration(fr.inria.aoste.syndex.Duration)
		 * @generated
		 */
		public Object caseDuration(fr.inria.aoste.syndex.Duration object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementDuration(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseEnumLitDuration(fr.inria.aoste.syndex.EnumLitDuration)
		 * @generated
		 */
		public Object caseEnumLitDuration(fr.inria.aoste.syndex.EnumLitDuration object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementEnumLitDuration(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINOUT(fr.inria.aoste.syndex.INOUT)
		 * @generated
		 */
		public Object caseINOUT(fr.inria.aoste.syndex.INOUT object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementINOUT(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseConnectionHW(fr.inria.aoste.syndex.ConnectionHW)
		 * @generated
		 */
		public Object caseConnectionHW(fr.inria.aoste.syndex.ConnectionHW object)
		{
			if ("default".equals(presentation))
			{
				return createGraphElementConnectionHW(object, presentation);
			}
			return null;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject,
	 *      java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation)
	{
		Object graphElt = null;

		if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(obj.eClass().getEPackage().getNsURI()))
		{
			graphElt = new GraphicSyndexSwitch(presentation).doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementProcessor(fr.inria.aoste.syndex.Processor element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFPGA(fr.inria.aoste.syndex.FPGA element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDSP(fr.inria.aoste.syndex.DSP element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAsic(fr.inria.aoste.syndex.Asic element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementMedia(fr.inria.aoste.syndex.Media element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementRAM(fr.inria.aoste.syndex.RAM element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSAM(fr.inria.aoste.syndex.SAM element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSAMPTP(fr.inria.aoste.syndex.SAMPTP element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementHardware(fr.inria.aoste.syndex.Hardware element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementComponentInstance(fr.inria.aoste.syndex.ComponentInstance element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementINOUTInstance(fr.inria.aoste.syndex.INOUTInstance element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDuration(fr.inria.aoste.syndex.Duration element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEnumLitDuration(fr.inria.aoste.syndex.EnumLitDuration element,
			String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementINOUT(fr.inria.aoste.syndex.INOUT element, String presentation)
	{
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element
	 *        the model element
	 * @param presentation
	 *        the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementConnectionHW(fr.inria.aoste.syndex.ConnectionHW element,
			String presentation)
	{
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj
	 *        the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj)
	{
		return obj;
	}

}
