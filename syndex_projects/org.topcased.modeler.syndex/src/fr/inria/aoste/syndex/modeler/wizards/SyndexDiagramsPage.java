/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.modeler.wizards.DiagramsPage;

/**
 * @generated
 */
public class SyndexDiagramsPage extends DiagramsPage
{

	/**
	 * @generated NOT
	 */
	public static String	choixdiag	= "";

	/**
	 * @param pageName
	 * @param selection
	 * @generated
	 */
	public SyndexDiagramsPage(String pageName, IStructuredSelection selection)
	{
		super(pageName, selection, true);
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getEditorID()
	 * @generated
	 */
	public String getEditorID()
	{
		return "fr.inria.aoste.syndex.modeler.editor.SyndexEditor";
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getFileExtension()
	 * @generated
	 */
	public String getFileExtension()
	{
		return "syndex";
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getAdapterFactory()
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public ComposedAdapterFactory getAdapterFactory()
	{
		List factories = new ArrayList();
		factories.add(new fr.inria.aoste.syndex.provider.SyndexItemProviderAdapterFactory());
		factories.add(new ResourceItemProviderAdapterFactory());
		factories.add(new ReflectiveItemProviderAdapterFactory());

		return new ComposedAdapterFactory(factories);
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getDefaultTemplateId()
	 * @return String
	 * @generated NOT
	 */
	public String getDefaultTemplateId()
	{
		return "fr.inria.aoste.syndex.modeler.templates.drdiagram";

	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getNameTemplateSelected(String appli)
	 * @return String
	 * @generated NOT
	 */
	public static String methodeChoix(String appli)
	{
		choixdiag = appli;
		return appli;
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage# getModelNameWithoutExtension(String fullName)
	 * @return String
	 * @generated NOT
	 */
	@SuppressWarnings("static-access")
	public String getModelNameWithoutExtension(String fullName)
	{
		if (this.choixdiag.equals("Definition Definition Diagram"))
		{
			fullName = "[Appli]_" + fullName;
		}
		else
		{
			fullName = "[HW]_" + fullName;
		}
		String[] nameSplitted = fullName.split("." + getFileExtension());
		return nameSplitted[0];
	}
}
