/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

/**
 * A Set of properties that are used for the graphical objects that are not associated with a model object. Each name is
 * used as the typeInfo attribute in the DI file.
 * 
 * @generated
 */
public interface HddSimpleObjectConstants
{
	/**
	 * The name that identify the <i>RafinementMemoryLink</i> SimpleObject.
	 * 
	 * @generated
	 */
	String	SIMPLE_OBJECT_RAFINEMENTMEMORYLINK		= "RafinementMemoryLink";
	/**
	 * The name that identify the <i>RafinnementProcessLink</i> SimpleObject.
	 * 
	 * @generated
	 */
	String	SIMPLE_OBJECT_RAFINNEMENTPROCESSLINK	= "RafinnementProcessLink";
}
