/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.drdiagram.figures;

import org.topcased.modeler.figures.DiagramFigure;

/**
 * The figure to display a Definition Definition Diagram.
 * 
 * @generated
 */
public class DrdDiagramFigure extends DiagramFigure
{

}
