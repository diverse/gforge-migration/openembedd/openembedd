/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.figures;

import org.topcased.modeler.figures.DiagramFigure;

/**
 * The figure to display a Hardware Definition Diagram.
 * 
 * @generated
 */
public class HddDiagramFigure extends DiagramFigure
{

}
