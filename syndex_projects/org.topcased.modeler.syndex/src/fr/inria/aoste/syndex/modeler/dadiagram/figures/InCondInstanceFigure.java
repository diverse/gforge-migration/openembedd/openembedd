/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram.figures;

import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.ILabelFigure;

/**
 * @generated
 */
public class InCondInstanceFigure extends org.topcased.draw2d.figures.PortFigure implements ILabelFigure
{
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public InCondInstanceFigure()
	{
		// TODO : here, you can specify the type of port (IN, OUT or INOUT) to have a better display
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ILabelFigure#getLabel()
	 * @generated
	 */
	public ILabel getLabel()
	{
		// TODO : return the Figure that represent the Label
		return null;
	}

}
