/*******************************************************************************
 * No CopyrightText Defined in the configurator file.
 ******************************************************************************/
package fr.inria.aoste.syndex.modeler.dadiagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.edit.GraphEdgeEditPart;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.utils.Utils;

import fr.inria.aoste.syndex.modeler.dadiagram.edit.ApplicationEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ConditionEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.DadDiagramEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.INInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InCondInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InCondPortEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InputEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDataConnectionEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyINEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.InternalDependencyOUTEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.OutInstanceEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.OutputEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.ParameterValueEditPart;
import fr.inria.aoste.syndex.modeler.dadiagram.edit.SynDExEditPart;
import fr.inria.aoste.syndex.util.SyndexSwitch;

/**
 * Part Factory : associates a model object to its controller. <br>
 * 
 * @generated
 */
@SuppressWarnings("unchecked")
public class DadEditPartFactory implements EditPartFactory
{
	/**
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,java.lang.Object)
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model instanceof Diagram)
		{
			return new DadDiagramEditPart((Diagram) model);
		}
		else if (model instanceof GraphNode)
		{
			final GraphNode node = (GraphNode) model;
			EObject element = Utils.getElement(node);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new NodeSyndexSwitch(node).doSwitch(element);
				}

				return new EMFGraphNodeEditPart(node);
			}

			if (node.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
			}
			return new GraphNodeEditPart(node);
		}
		else if (model instanceof GraphEdge)
		{
			final GraphEdge edge = (GraphEdge) model;
			EObject element = Utils.getElement(edge);
			if (element != null)
			{
				if ("http://www.inria.fr/aoste/SynDEx/1.1".equals(element.eClass().getEPackage().getNsURI()))
				{
					return (EditPart) new EdgeSyndexSwitch(edge).doSwitch(element);
				}

				return new EMFGraphEdgeEditPart(edge);
			}

			if (edge.getSemanticModel() instanceof SimpleSemanticModelElement)
			{
				// Manage the Element that are not associated with a model object
			}

			return new GraphEdgeEditPart(edge);
		}

		throw new IllegalStateException("No edit part matches with the '" + model.getClass().getName()
				+ "' model element. Check DadEditPartFactory#createEditPart(EditPart,Object) class");
	}

	/**
	 * @generated
	 */
	private class NodeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical node
		 * 
		 * @generated
		 */
		private GraphNode	node;

		/**
		 * Constructor
		 * 
		 * @param node
		 *        the graphical node
		 * @generated
		 */
		public NodeSyndexSwitch(GraphNode node)
		{
			this.node = node;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseSynDEx(fr.inria.aoste.syndex.SynDEx)
		 * @generated
		 */
		public Object caseSynDEx(fr.inria.aoste.syndex.SynDEx object)
		{
			return new SynDExEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseApplication(fr.inria.aoste.syndex.Application)
		 * @generated
		 */
		public Object caseApplication(fr.inria.aoste.syndex.Application object)
		{
			return new ApplicationEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance)
		 * @generated
		 */
		public Object caseComponentInstance(fr.inria.aoste.syndex.ComponentInstance object)
		{
			return new ComponentInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutInstance(fr.inria.aoste.syndex.OutInstance)
		 * @generated
		 */
		public Object caseOutInstance(fr.inria.aoste.syndex.OutInstance object)
		{
			return new OutInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseINInstance(fr.inria.aoste.syndex.INInstance)
		 * @generated
		 */
		public Object caseINInstance(fr.inria.aoste.syndex.INInstance object)
		{
			return new INInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseOutput(fr.inria.aoste.syndex.Output)
		 * @generated
		 */
		public Object caseOutput(fr.inria.aoste.syndex.Output object)
		{
			return new OutputEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInput(fr.inria.aoste.syndex.Input)
		 * @generated
		 */
		public Object caseInput(fr.inria.aoste.syndex.Input object)
		{
			return new InputEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondPort(fr.inria.aoste.syndex.InCondPort)
		 * @generated
		 */
		public Object caseInCondPort(fr.inria.aoste.syndex.InCondPort object)
		{
			return new InCondPortEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseParameterValue(fr.inria.aoste.syndex.ParameterValue)
		 * @generated
		 */
		public Object caseParameterValue(fr.inria.aoste.syndex.ParameterValue object)
		{
			return new ParameterValueEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseCondition(fr.inria.aoste.syndex.Condition)
		 * @generated
		 */
		public Object caseCondition(fr.inria.aoste.syndex.Condition object)
		{
			return new ConditionEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInCondInstance(fr.inria.aoste.syndex.InCondInstance)
		 * @generated
		 */
		public Object caseInCondInstance(fr.inria.aoste.syndex.InCondInstance object)
		{
			return new InCondInstanceEditPart(node);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeSyndexSwitch extends SyndexSwitch
	{
		/**
		 * The graphical edge
		 * 
		 * @generated
		 */
		private GraphEdge	edge;

		/**
		 * Constructor
		 * 
		 * @param edge
		 *        the graphical edge
		 * @generated
		 */
		public EdgeSyndexSwitch(GraphEdge edge)
		{
			this.edge = edge;
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDataConnection(fr.inria.aoste.syndex.InternalDataConnection)
		 * @generated
		 */
		public Object caseInternalDataConnection(fr.inria.aoste.syndex.InternalDataConnection object)
		{
			return new InternalDataConnectionEditPart(edge);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependencyIN(fr.inria.aoste.syndex.InternalDependencyIN)
		 * @generated
		 */
		public Object caseInternalDependencyIN(fr.inria.aoste.syndex.InternalDependencyIN object)
		{
			return new InternalDependencyINEditPart(edge);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependencyOUT(fr.inria.aoste.syndex.InternalDependencyOUT)
		 * @generated
		 */
		public Object caseInternalDependencyOUT(fr.inria.aoste.syndex.InternalDependencyOUT object)
		{
			return new InternalDependencyOUTEditPart(edge);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#caseInternalDependency(fr.inria.aoste.syndex.InternalDependency)
		 * @generated
		 */
		public Object caseInternalDependency(fr.inria.aoste.syndex.InternalDependency object)
		{
			return new InternalDependencyEditPart(edge);
		}

		/**
		 * @see fr.inria.aoste.syndex.util.SyndexSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object)
		{
			return new EMFGraphEdgeEditPart(edge);
		}
	}

}
