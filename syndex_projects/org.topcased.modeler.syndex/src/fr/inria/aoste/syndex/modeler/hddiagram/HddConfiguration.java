/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

import fr.inria.aoste.syndex.modeler.SyndexPlugin;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.AsicEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ComponentInstanceEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ConnectionHWEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.DSPEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.DurationEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.EnumLitDurationEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.FPGAEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.HardwareEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.INOUTEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.INOUTInstanceEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.MediaEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.ProcessorEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.RAMEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.SAMEditPart;
import fr.inria.aoste.syndex.modeler.hddiagram.edit.SAMPTPEditPart;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 * 
 * @generated
 */
public class HddConfiguration implements IConfiguration
{
	/**
	 * @generated
	 */
	private HddPaletteManager	paletteManager;

	/**
	 * @generated
	 */
	private HddEditPartFactory	editPartFactory;

	/**
	 * @generated
	 */
	private HddCreationUtils	creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * 
	 * @generated
	 */
	private DiagramGraphConf	diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 * 
	 * @generated
	 */
	public HddConfiguration()
	{
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 * 
	 * @generated
	 */
	private void registerAdapters()
	{
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(INOUTEditPart.class, fr.inria.aoste.syndex.INOUT.class),
			INOUTEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(HardwareEditPart.class, fr.inria.aoste.syndex.Hardware.class),
			HardwareEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(FPGAEditPart.class, fr.inria.aoste.syndex.FPGA.class), FPGAEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(AsicEditPart.class, fr.inria.aoste.syndex.Asic.class), AsicEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(DSPEditPart.class, fr.inria.aoste.syndex.DSP.class), DSPEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(RAMEditPart.class, fr.inria.aoste.syndex.RAM.class), RAMEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(SAMEditPart.class, fr.inria.aoste.syndex.SAM.class), SAMEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(SAMPTPEditPart.class, fr.inria.aoste.syndex.SAMPTP.class),
			SAMPTPEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(MediaEditPart.class, fr.inria.aoste.syndex.Media.class),
			MediaEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ProcessorEditPart.class, fr.inria.aoste.syndex.Processor.class),
			ProcessorEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ComponentInstanceEditPart.class,
				fr.inria.aoste.syndex.ComponentInstance.class), ComponentInstanceEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(INOUTInstanceEditPart.class, fr.inria.aoste.syndex.INOUTInstance.class),
			INOUTInstanceEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
					new EditPart2ModelAdapterFactory(EnumLitDurationEditPart.class,
						fr.inria.aoste.syndex.EnumLitDuration.class), EnumLitDurationEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(DurationEditPart.class, fr.inria.aoste.syndex.Duration.class),
			DurationEditPart.class);
		Platform.getAdapterManager().registerAdapters(
			new EditPart2ModelAdapterFactory(ConnectionHWEditPart.class, fr.inria.aoste.syndex.ConnectionHW.class),
			ConnectionHWEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId()
	{
		return new String("fr.inria.aoste.syndex.modeler.hddiagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName()
	{
		return new String("Hardware Definition Diagram");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			editPartFactory = new HddEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager()
	{
		if (paletteManager == null)
		{
			paletteManager = new HddPaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils()
	{
		if (creationUtils == null)
		{
			creationUtils = new HddCreationUtils(getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf()
	{
		if (diagramGraphConf == null)
		{
			URL url = SyndexPlugin.getDefault().getBundle().getResource(
				"fr/inria/aoste/syndex/modeler/hddiagram/diagram.graphconf");
			if (url != null)
			{
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null && resource.getContents().get(0) instanceof DiagramGraphConf)
				{
					diagramGraphConf = (DiagramGraphConf) resource.getContents().get(0);
				}
			}
			else
			{
				new MissingGraphConfFileException(
					"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}
