/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT authors : Fadoi LAKHAL, Yves SOREL
 * INRIA Rocquencourt
 *******************************************************************************************/
package fr.inria.aoste.syndex.modeler.hddiagram.edit;

import org.eclipse.draw2d.IFigure;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;

import fr.inria.aoste.syndex.modeler.hddiagram.figures.ConnectionHWFigure;

/**
 * ConnectionHW controller
 * 
 * @generated
 */
public class ConnectionHWEditPart extends EMFGraphEdgeEditPart
{

	/**
	 * Constructor
	 * 
	 * @param model
	 *        the graph object
	 * @generated
	 */
	public ConnectionHWEditPart(GraphEdge model)
	{
		super(model);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 * @generated
	 */
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(ModelerEditPolicyConstants.CHANGE_FONT_EDITPOLICY, null);

	}

	/**
	 * @return the Figure
	 * @generated
	 */
	protected IFigure createFigure()
	{
		ConnectionHWFigure connection = new ConnectionHWFigure();

		return connection;
	}

}
