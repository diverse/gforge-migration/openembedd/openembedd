/**
 * The MARTE to SynDEx transformation is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
package fr.inria.aoste.syndex.marte2syndex.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import fr.inria.aoste.syndex.marte2syndex.Constants;
import fr.inria.aoste.syndex.marte2syndex.atl.ATLInterface;

public class Uml2SynDexAction implements IObjectActionDelegate
{
	/**
	 * Selected UML file on which the transformation is called
	 */
	private IFile	srcFile;
	/**
	 * Shell from which this action is called
	 */
	private Shell	shell;

	/**
	 * Constructor
	 */
	public Uml2SynDexAction()
	{
		this.srcFile = null;
	}

	/**
	 * Get the shell that activate this action
	 * 
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
		shell = targetPart.getSite().getShell();
	}

	/**
	 * Execute the UML2 to SynDEx transformation on the selected file
	 * 
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action)
	{
		if (srcFile != null)
		{
			IPath srcPath = srcFile.getLocation();
			IPath tgtPath = srcPath.removeFileExtension().addFileExtension(Constants.SYNDEX_EXT);

			ATLInterface.getInstance().transform(srcPath.toOSString(), tgtPath.toOSString());
			try
			{
				srcFile.getParent().refreshLocal(2, null);
			}
			catch (CoreException e)
			{
				e.printStackTrace();
			}
			MessageDialog.openInformation(shell, "Marte2SynDEx Plug-in",
				"The transformation Marte->SynDEx was executed.");
		}
		else
		{
			System.out.println("No file was selected!");
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		// String srcStr,tgtStr;
		if (selection instanceof StructuredSelection)
		{
			// the selection should be a single *.uml file
			if (((StructuredSelection) selection).size() == 1)
			{
				IFile tmp = (IFile) ((StructuredSelection) selection).getFirstElement();
				if (Constants.UML_EXT.equals(tmp.getFileExtension()))
					srcFile = tmp;
				else
					srcFile = null;
			}
			else
				srcFile = null;
		}
	}

}
