/**
 * The MARTE to SynDEx transformation is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
package fr.inria.aoste.syndex.marte2syndex;

/**
 * Constants used by other classes of this plug-in
 */
public interface Constants
{
	/**
	 * Uri of the UML2 metamodel
	 */
	public final static String	UML_MM_URI		= "uri:http://www.eclipse.org/uml2/2.1.0/UML";

	/**
	 * Uri of the SynDEx metamodel
	 */
	public final static String	SYNDEX_MM_URI	= "uri:http://www.inria.fr/aoste/SynDEx/1.1";

	/**
	 * Name of the UML2 metamodel in the ATL transformation file
	 */
	public final static String	UML_MM_NAME		= "UML2";

	/**
	 * Name of the SynDEx metamodel in the ATL transformation file
	 */
	public final static String	SYNDEX_MM_NAME	= "syndex";

	/**
	 * Extension of UML2 file
	 */
	public final static String	UML_EXT			= "uml";

	/**
	 * Extension of SynDEx file
	 */
	public final static String	SYNDEX_EXT		= "syndex";
}
