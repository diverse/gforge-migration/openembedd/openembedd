/**
 * The MARTE to SynDEx transformation is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
package fr.inria.aoste.syndex.marte2syndex;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Marte2SynDExPlugin extends AbstractUIPlugin
{

	// The plug-in ID
	public static final String			PLUGIN_ID	= "fr.inria.aoste.syndex.marte2syndex";

	// The shared instance
	private static Marte2SynDExPlugin	plugin;

	/**
	 * The constructor
	 */
	public Marte2SynDExPlugin()
	{}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Marte2SynDExPlugin getDefault()
	{
		return plugin;
	}

}
