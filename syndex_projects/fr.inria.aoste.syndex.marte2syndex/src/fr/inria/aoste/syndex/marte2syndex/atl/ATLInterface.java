/**
 * The MARTE to SynDEx transformation is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
package fr.inria.aoste.syndex.marte2syndex.atl;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;

import fr.inria.aoste.syndex.marte2syndex.Constants;

public class ATLInterface
{
	/**
	 * Instance of the current class
	 */
	private static ATLInterface	instance	= null;
	/**
	 * Name of the ATL compiled transformation
	 */
	private final static String	atlTransfo	= "MARTE2SYNDEX.asm";
	/**
	 * The model handler used for the transformation
	 */
	private AtlEMFModelHandler	modelHandler;
	/**
	 * The URL to the transformation resource file
	 */
	private URL					transfoResource;
	/**
	 * The UML metamodel
	 */
	private ASMEMFModel			umlMM;
	/**
	 * The SynDEx metamodel
	 */
	private ASMEMFModel			sdxMM;

	/**
	 * This method returns the instance of the ATL interface (there is only one instance of this object)
	 * 
	 * @return the instance of the ATLInterface
	 */
	public static ATLInterface getInstance()
	{
		if (instance == null)
			instance = new ATLInterface();
		return instance;
	}

	/**
	 * Initialize the metamodel and transformation resources and update the parameter
	 * 
	 * @param models
	 *        the map that will be updated with resource elements
	 */
	private void init(Map<String, Object> models)
	{
		modelHandler = (AtlEMFModelHandler) AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);
		transfoResource = ATLInterface.class.getResource(atlTransfo);

		umlMM = (ASMEMFModel) modelHandler
				.loadModel(Constants.UML_MM_NAME, modelHandler.getMof(), Constants.UML_MM_URI);
		sdxMM = (ASMEMFModel) modelHandler.loadModel(Constants.SYNDEX_MM_NAME, modelHandler.getMof(),
			Constants.SYNDEX_MM_URI);

		models.put(Constants.UML_MM_NAME, umlMM);
		models.put(Constants.SYNDEX_MM_NAME, sdxMM);
	}

	/**
	 * Call the UML->SynDEx transformation onto the <code>inFilePath</code> and store the result in the
	 * <code>outFilePath</code> file.
	 * 
	 * @param inFilePath
	 *        The path to the input UML2 file
	 * @param outFilePath
	 *        The path to the output SynDEx file
	 */
	public void transform(String inFilePath, String outFilePath)
	{
		try
		{
			Map<String, Object> models = new HashMap<String, Object>();
			init(models);

			// get/create models
			ASMEMFModel inModel = (ASMEMFModel) modelHandler.loadModel("IN", umlMM, URI.createFileURI(inFilePath));
			ASMEMFModel outModel = (ASMEMFModel) modelHandler.newModel("OUT", URI.createFileURI(outFilePath)
					.toFileString(), sdxMM);
			// load models
			models.put("IN", inModel);
			models.put("OUT", outModel);
			// add the continue after errors options
			Map<String, String> options = new HashMap<String, String>();
			options.put("continueAfterError", "true");
			// launch
			AtlLauncher.getDefault().launch(transfoResource, Collections.EMPTY_MAP, models, Collections.EMPTY_MAP,
				Collections.EMPTY_LIST, options);

			modelHandler.saveModel(outModel, outFilePath, false);
			dispose(models);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Dispose all objects loaded in the parameter
	 * 
	 * @param models
	 *        the map that contains resource elements
	 */
	private void dispose(Map<String, Object> models)
	{
		for (Object model : models.values())
			((ASMEMFModel) model).dispose();
	}
}
