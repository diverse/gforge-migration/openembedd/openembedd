package fr.inria.aoste.syndex.samples.plugin;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openembedd.wizards.AbstractDemoPlugin;

/**
 * The main plugin class to be used in the desktop.
 */
public class SyndexExamplesPlugin extends AbstractDemoPlugin
{
	public ImageDescriptor getImageDescriptor(String path)
	{
		return AbstractUIPlugin.imageDescriptorFromPlugin("fr.inria.aoste.syndex.samples", path);
	}
}
