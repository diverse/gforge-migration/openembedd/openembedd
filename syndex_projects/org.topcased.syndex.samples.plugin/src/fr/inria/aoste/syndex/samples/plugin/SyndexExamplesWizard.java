package fr.inria.aoste.syndex.samples.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openembedd.wizards.AbstractNewExampleWizard;

public class SyndexExamplesWizard extends AbstractNewExampleWizard
{
	protected Collection<ProjectDescriptor> getProjectDescriptors()
	{
		// We need the statements example to be unzipped along with the
		// EMF library example model, edit and editor examples
		List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>(1);
		projects.add(new ProjectDescriptor("fr.inria.aoste.syndex.samples.plugin",
			"zip/fr.inria.aoste.syndex.samples.zip", "fr.inria.aoste.syndex.samples"));
		return projects;
	}
}
