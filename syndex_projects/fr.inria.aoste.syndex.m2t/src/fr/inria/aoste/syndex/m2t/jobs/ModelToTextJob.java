package fr.inria.aoste.syndex.m2t.jobs;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import fr.inria.aoste.syndex.m2t.AnalyseSAX;
import fr.inria.aoste.syndex.m2t.M2TPlugin;

public class ModelToTextJob extends Job
{
	public final static String	SDX_EXTENSION			= "syndex";
	public final static String	SDX_TEXTUAL_EXTENSION	= "sdx";
	public final static String	SOFTWARE_PREFIX			= "[Appli]_";
	public final static String	HARDWARE_PREFIX			= "[HW]_";

	private IFile				syndexFile;

	/**
	 * Constructor
	 * 
	 * @param syndexFile
	 *        the syndex file *.syndex containing the model to generate in textual form
	 */
	public ModelToTextJob(IFile syndexFile)
	{
		super("Generate SDX file");
		this.syndexFile = syndexFile;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor)
	{
		String workspacePath = Platform.getInstanceLocation().getURL().getPath();
		String filename = syndexFile.getFullPath().lastSegment();
		String modelName;

		monitor.beginTask("SDX file generation", 60);
		monitor.subTask("Checking model file");
		// The absolute path to the syndex file directory
		String tmpPath = workspacePath + syndexFile.getFullPath().removeLastSegments(1).toString() + '/';
		// Absolute path to the software and hardware model file and to the destination textual file
		String swPath = null, hwPath = null, dstPath = null;
		if (filename.startsWith(SOFTWARE_PREFIX))
		{ // SOFTWARE [Appli]_
			swPath = workspacePath + syndexFile.getFullPath().toString();
			modelName = filename.substring(SOFTWARE_PREFIX.length());
			hwPath = tmpPath + HARDWARE_PREFIX + modelName;
			if (!(new File(hwPath)).exists())
				hwPath = null;
			dstPath = tmpPath + modelName.subSequence(0, modelName.length() - SDX_EXTENSION.length())
					+ SDX_TEXTUAL_EXTENSION;
		}
		else if (filename.startsWith(HARDWARE_PREFIX))
		{ // HARDWARE [HW]_
			hwPath = workspacePath + syndexFile.getFullPath().toString();
			modelName = filename.substring(HARDWARE_PREFIX.length());
			swPath = tmpPath + SOFTWARE_PREFIX + modelName;
			if (!(new File(swPath)).exists())
				swPath = null;
			dstPath = tmpPath + modelName.subSequence(0, modelName.length() - SDX_EXTENSION.length())
					+ SDX_TEXTUAL_EXTENSION;
		}
		monitor.worked(20);

		// Parse the file(s)
		AnalyseSAX parser = new AnalyseSAX();
		try
		{
			if (swPath != null)
			{
				monitor.subTask("Generating application aspect");
				parser.xmiParse(swPath, dstPath, false);
				monitor.worked(20);

				if (hwPath != null)
				{
					monitor.subTask("Generating architecture aspect after software one");
					// Append the transformation of the hardware part to the software one
					parser.xmiParse(hwPath, dstPath, true);
					monitor.worked(20);
				}
			}
			else if (hwPath != null)
			{
				monitor.subTask("Generating architecture aspect");
				parser.xmiParse(hwPath, dstPath, false);
				monitor.worked(40);
			}

			// Refresh the parent directory to see the new sdx file created
			try
			{
				syndexFile.getParent().refreshLocal(2, null);
			}
			catch (CoreException ce)
			{
				M2TPlugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, M2TPlugin.PLUGIN_ID, 0, "Refreshing error.", ce));
			}
		}
		catch (IOException e)
		{
			return new Status(IStatus.ERROR, M2TPlugin.PLUGIN_ID, 0, "Problem to access to file(s).", e);
		}
		monitor.done();
		return Status.OK_STATUS;
	}

}
