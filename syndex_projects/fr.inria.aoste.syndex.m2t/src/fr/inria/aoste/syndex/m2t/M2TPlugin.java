/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Christian Brunette (OpenEmbeDD integration team) - modification
 *******************************************************************************************/
package fr.inria.aoste.syndex.m2t;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class M2TPlugin extends AbstractUIPlugin
{

	// The plug-in ID
	public static final String	PLUGIN_ID	= "fr.inria.aoste.syndex.m2t";

	// The shared instance
	private static M2TPlugin		plugin;

	/**
	 * The constructor
	 */
	public M2TPlugin()
	{}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static M2TPlugin getDefault()
	{
		return plugin;
	}

}
