/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Fadoi LAKHAL, Yves SOREL (INRIA Rocquencourt) - initial API
 * @author Christian Brunette (OpenEmbeDD integration team) - modification
 *******************************************************************************************/
package fr.inria.aoste.syndex.m2t;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

@SuppressWarnings("unchecked")
public class AnalyseSAX extends DefaultHandler
{
	// declaration first
	int							jfirst						= 0;
	int							jinout						= 0;
	int							jarchic						= 0;
	String						tempName2					= "truc";
	String						NomBalise					= "truc";
	boolean						boolOperator				= false;
	String						concat						= "truc";
	String						tempEnum					= "truc";
	String						tempRefComp					= "truc";
	String						ProcName					= "";
	String						concatIO					= "";
	String						ConcatArchi					= "";
	String						Namearchi					= "";
	String						Name1						= "";
	HashMap<String, String>		SyndexElementMap			= new HashMap<String, String>(150);
	HashMap<String, String>		mapenum						= new HashMap<String, String>(150);
	HashMap<String, String>		MapRef						= new HashMap<String, String>(150);
	HashMap<String, String>		MapCArchiTarget				= new HashMap<String, String>(150);
	HashMap<String, String>		maparchiconcat				= new HashMap<String, String>(150);
	HashMap<String, String>		maprefarchi					= new HashMap<String, String>(150);
	List						listenum					= new ArrayList(50);

	// declaration second
	String						tempParentPortSecond		= "";
	String						tempNameSecond				= "";
	String						tempduration				= "";
	String						tempSourceAppli				= "truc";
	String						tempTargetHW				= "truc";
	String						tempTypeAssoc				= "truc";
	String						tempStartSecond				= "truc";
	String						concatSecond1				= "truc";
	String						concatSecond2				= "truc";
	String						tempOwnerINOUT				= "truc";
	String						AdequationName				= "truc";
	String						tempSourceAppli2			= "";
	String						tempSourceAppli3			= "";
	String						tempTargetAppli2			= "";
	String						tempTargetAppli3			= "";
	String						NameDuration				= "";
	String						DurationInitial				= "";
	String						NameAppli					= "";
	String						NameCI						= "";
	String						NameProcDuration			= "";
	String						DurationProc				= "";
	String						DurationProcinitial			= "";
	String						concatHW					= "";
	String						tempStartexecution			= "";
	String						durationSW					= "";
	boolean						adeq						= false;
	boolean						ProcDuration				= false;
	int							OperatorCpt					= 0;
	int							jSecond						= 0;
	int							jsecond						= 0;
	int							josecond					= 0;
	int							jHW							= 0;
	int							jportcondi					= 0;
	StringBuffer				SourceAppliBuf				= new StringBuffer();
	StringBuffer				TargetAppliBuf				= new StringBuffer();
	HashMap<String, String>		mapassoc					= new HashMap<String, String>(150);
	HashMap<String, String>		mapassocessai				= new HashMap<String, String>(150);
	HashMap<String, String>		maparchitecture				= new HashMap<String, String>(150);
	HashMap<String, String>		mapnomarchi					= new HashMap<String, String>(150);
	HashMap<String, String>		concatSecondMap				= new HashMap<String, String>(150);
	HashMap<String, String>		mapconnectionArchi			= new HashMap<String, String>(150);
	HashMap<String, String>		mapcompoinstance			= new HashMap<String, String>(150);
	HashMap<String, String>		mapadeq						= new HashMap<String, String>(150);
	HashMap<String, String>		mapDurationInitial			= new HashMap<String, String>(150);
	HashMap<String, String>		mapconcatName				= new HashMap<String, String>(150);
	HashMap<String, String>		MapProcDuration				= new HashMap<String, String>(150);
	HashMap<String, String>		SyndexElementMapHW			= new HashMap<String, String>(150);
	List						OperatorList				= new ArrayList(150);
	List						ListProcDuration			= new ArrayList(150);

	// declaration analyse
	String						ownerTemp					= "truc";
	String						referenceArchiTemp			= "truc";
	String						referencedComponentTemp		= "truc";
	String						SyndexType					= "truc";
	String						tempNameObj					= "truc";
	String						tempName					= "truc";
	String						tempStart					= "truc";
	String						tempParentPort				= "truc";
	String						tempParentPort2				= "truc";
	String						tempConnect					= "truc";
	String						tempTargetConnect			= "truc";
	String						tempSource					= "truc";
	String						tempTarget					= "truc";
	String						tempTargetI					= "truc";
	String						tempTargetIDI				= "truc";
	String						tempTargetIDO				= "truc";
	String						tempSourceI					= "truc";
	String						tempSourceIDI				= "truc";
	String						tempSourceIDIC				= "truc";
	String						tempSourceIDO				= "truc";
	String						tempSource2					= "truc";
	String						source						= "";
	String						target						= "";
	String						tempCArchi					= "truc";
	String						concatActuator				= "truc";
	String						compound1					= "truc";
	String						compound2					= "truc";
	String						compound3					= "truc";
	String						tempCipi					= "truc";
	String						executionPhase				= "truc";
	String						executionPhase1				= "truc";
	String						protocoleType				= "truc";
	String						tempType					= "truc";
	String						concatArchi1				= "truc";
	String						concatArchi2				= "truc";
	String						NameArchi					= "";
	String						SourceInOut					= "";
	String						TargetInOut					= "";
	String						typePort					= "";
	String						portSensor					= "";
	String						portActuator				= "";
	String						portOutFunction				= "";
	String						portInFunction				= "";
	String						paramDeclarations			= "";
	String						valueParam					= "";
	String						outputConstant				= "";
	String						inputDelay					= "";
	String						outputDelay					= "";
	String						constantValue				= "";
	String						delayValue					= "";
	String						portConstant				= "";
	String						portConstant1				= "";
	String						portInDelay					= "";
	String						portInDelay1				= "";
	String						portOutDelay				= "";
	String						portOutDelay1				= "";
	String						Duration1					= "";
	String						SourceComtoProc				= "";
	String						ConnectionType				= "";
	String						workpath					= "";
	String						portcompound				= "";
	String						portInP						= "";
	String						portOutP					= "";
	String						SizeType					= "";
	String						ownerTemp2					= "";
	String						portinCond					= "";
	String						valeurcondition				= "";
	String						StructureType				= "";
	String						conditionPort				= "";
	String						Conditionnement				= "";
	String						concatcondition				= "";

	boolean						broadcast					= false;
	boolean						inF							= false;
	boolean						outF						= false;
	boolean						inD							= false;
	boolean						outD						= false;
	boolean						pV							= false;
	boolean						boolParam					= false;
	boolean						boolpV						= false;
	boolean						inP							= false;
	boolean						outP						= false;
	boolean						condition					= false;
	boolean						inCond						= false;

	int							jconcatcondi				= 0;
	int							jcond						= 0;
	int							jarchi						= 0;
	int							jparchi						= 0;
	int							j							= 0;
	int							jo							= 0;
	int							jci							= 0;
	int							jpi							= 0;
	int							position					= 0;
	int							cpt							= 0;
	int							jportcompound				= 0;
	int							jcondsecond					= 0;
	List						listIncond					= new ArrayList<String>(150);
	List						list1						= new ArrayList<String>(150);
	List						list2						= new ArrayList(150);
	List						listInputActuator			= new ArrayList(150);
	List						listOutputSensor			= new ArrayList(150);
	List						listOutputSensor1			= new ArrayList(150);
	List						listInputFunction			= new ArrayList(150);
	List						listOutputFunction			= new ArrayList(150);
	List						listOutputP					= new ArrayList(150);
	List						listInputP					= new ArrayList(150);
	List						listInputApplication		= new ArrayList(150);
	List						listOutputApplication		= new ArrayList(150);
	List						listProcessorinout			= new ArrayList(150);
	List						listdependance				= new ArrayList(200);
	List						listOperators				= new ArrayList(150);
	List						listMedias					= new ArrayList(150);
	List						listconnecthw				= new ArrayList(150);
	List						listalgoM					= new ArrayList(150);
	List						listarchiM					= new ArrayList(150);
	List						listexecutionPhaseA			= new ArrayList(150);
	List						listexecutionPhaseF			= new ArrayList(150);
	List						listexecutionPhaseS			= new ArrayList(150);
	List						listexecutionPhaseAc		= new ArrayList(150);
	List						listexecutionPhaseD			= new ArrayList(150);
	List						listexecutionPhaseH			= new ArrayList(150);
	List						listexecutionPhaseR			= new ArrayList(150);
	List						listexecutionPhaseM			= new ArrayList(150);
	List						listexecutionPhaseFp		= new ArrayList(150);
	List						listexecutionPhaseAsic		= new ArrayList(150);
	List						listexecutionPhaseSampp		= new ArrayList(150);
	List						listexecutionPhaseSamp		= new ArrayList(150);
	List						listexecutionPhaseDsp		= new ArrayList(150);
	List						listexecutionPhaseProcessor	= new ArrayList(150);
	List						listFpgainout				= new ArrayList(150);
	List						listDspinout				= new ArrayList(150);
	List						listAsicinout				= new ArrayList(150);
	List						listreference				= new ArrayList(150);
	StringBuffer				ExecutionPhaseBuf			= new StringBuffer();
	static StringBuffer			buffer						= new StringBuffer();
	static StringBuffer			hardwareBuffer				= new StringBuffer();
	static StringBuffer			associationBuffer			= new StringBuffer();
	Iterator<String>			itr;
	HashMap<String, String>		map							= new HashMap<String, String>(150);
	HashMap<String, HashMap>	AssocMap					= new HashMap<String, HashMap>(150);
	HashMap<String, String>		mapconnect					= new HashMap<String, String>(150);
	HashMap<String, String>		mapconnect2					= new HashMap<String, String>(150);
	HashMap<String, String>		mapconcat					= new HashMap<String, String>(150);
	HashMap<String, String>		mapEssai					= new HashMap<String, String>(150);
	HashMap<String, String>		mapArchi					= new HashMap<String, String>(150);
	HashMap<String, String>		mapinterconne				= new HashMap<String, String>(150);
	HashMap<String, String>		mapcipi						= new HashMap<String, String>(150);
	HashMap<String, String>		mapinout					= new HashMap<String, String>(150);
	HashMap<String, Boolean>	mapmedia					= new HashMap<String, Boolean>(150);
	HashMap<String, String>		mapCArchi					= new HashMap<String, String>(150);
	HashMap<String, Boolean>	mapmediasammp				= new HashMap<String, Boolean>(150);
	HashMap<String, Boolean>	mapmediaram					= new HashMap<String, Boolean>(150);
	HashMap<String, Boolean>	mapmediaMedia				= new HashMap<String, Boolean>(150);
	HashMap<String, String>		mapDuration					= new HashMap<String, String>(150);
	HashMap<String, String>		mapFpgainout				= new HashMap<String, String>(150);
	HashMap<String, String>		mapDspinout					= new HashMap<String, String>(150);
	HashMap<String, String>		mapAsicinout				= new HashMap<String, String>(150);
	HashMap<String, String>		mapportcompound				= new HashMap<String, String>(150);
	HashMap<String, String>		mapconditionnement			= new HashMap<String, String>(150);
	static boolean				SaxFirst					= true;
	static boolean				SaxSecond					= true;
	static boolean				SWHW_case;
	static int					Nbrsax;

	/**
	 * @param srcPath
	 * @param dstPath
	 * @param append
	 *        true if the current parsing must be append to the destination file.
	 * @throws IOException
	 */
	public void xmiParse(String srcPath, String dstPath, boolean append) throws IOException
	{
		if (srcPath == null || dstPath == null)
		{
			M2TPlugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, M2TPlugin.PLUGIN_ID,
					"SynDEx Analyzer: The source and/or destination files are null."));
			return;
		}

		SWHW_case = append;
		if (SWHW_case)
		{
			SaxFirst = true;
			SaxSecond = true;
		}

		// Create the SDX file
		FileWriter out = new FileWriter(dstPath, SWHW_case);

		// Execute the parser
		DefaultHandler handler = new AnalyseSAX();
		try
		{
			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
			// 1- associate SyndexElement.j with names
			saxParser.parse(new File(srcPath), handler);
			SaxFirst = false;
			// 2- list of all kinds of operator with duration
			saxParser.parse(new File(srcPath), handler);
			SaxSecond = false;
			// 3- process data
			saxParser.parse(new File(srcPath), handler);
		}
		catch (Throwable t)
		{
			M2TPlugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, M2TPlugin.PLUGIN_ID, 0, "SynDEx Analyzer: problem during the parsing.", t));
			return;
		}

		if (!SWHW_case)
		{
			out.write(buffer.toString());
			buffer.delete(0, buffer.length());
		}
		else
		{
			out.write(hardwareBuffer.toString());
			out.write(associationBuffer.toString());
			hardwareBuffer.delete(0, hardwareBuffer.length());
			associationBuffer.delete(0, associationBuffer.length());
		}
		out.close();

	}

	/***************************************************************************************************
	 * The following methods are called automatically by the parser when an event is detected in the XML file
	 ***************************************************************************************************/

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
	 */
	public void error(SAXParseException e) throws SAXParseException
	{
		throw e;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	public void startDocument() throws SAXException
	{
		if (!SaxFirst & !SaxSecond)
		{
			if (!SWHW_case)
			{
				buffer.append("syndex_version : " + "\"" + "6.8.5" + "\"" + "\n");
				buffer.append("application description : " + "\"\"" + "\n\n");
				buffer.append("# Algorithm" + "\n\n");
				hardwareBuffer.append("# Architectures\n");
				associationBuffer.append("\n\n" + "#Main Algorithm / Main Architecture" + "\n\n");

			}
			if (position == 1)
			{
				buffer.append("Jamais");
				buffer.append("Architecture" + "\n\n");
			}
		}
	}
	// /**
	// * @param tampon
	// * @throws IOException
	// */
	// public void WriteToFile(String tampon) throws IOException
	// {
	// File outputFile = new File("Testsdx.sdx");
	// FileWriter out = new FileWriter(outputFile);
	// out.write(tampon);
	// out.close();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	public void endDocument() throws SAXException
	{
		if (!SaxFirst && !SaxSecond)
		{
			SaxFirst = true;
			SaxSecond = true;

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
	 * org.xml.sax.Attributes)
	 */
	public void startElement(String namespaceURI, String simpleName, String qualifiedName, Attributes attrs)
			throws SAXException
	{
		String nomElement = simpleName;
		String Name = "";
		String ListeDuree = "";
		String Listeneuve = "";
		if (nomElement.equals(""))
			nomElement = qualifiedName;
		if (SaxFirst)
		{
			// Recupere la map de SynDex.Element.i = avec le nom de l'element
			// Creation d'une map entre duree et fonction
			NomBalise = nomElement;
			for (int i = 0; i < attrs.getLength(); i++)
			{
				String TagName = attrs.getLocalName(i);
				if (TagName.equals(""))
					TagName = attrs.getQName(i);
				String valeurattribut = attrs.getValue(i);
				if (NomBalise.equals("SynDExelements"))
				{
					if (TagName.equals("name"))
					{
						Name1 = attrs.getValue(TagName);
						concat = "//@" + "SynDExelements" + "." + jfirst;
						SyndexElementMap.put(concat, Name1);
					}
					if (tempTypeAssoc.equals("application"))
					{
						NameAppli = tempNameSecond;
					}
				}
				if (TagName.equals("xsi:type") && attrs.getValue(i).equals("syndex:AssociationComtoProc"))
				{
					tempType = "associationComtoProc";
				}
				if (TagName.equals("xsi:type") && attrs.getValue(i).equals("syndex:Duration"))
				{
					tempType = "duration";
				}
				if (TagName.equals("referencedHWComponent"))
				{
					maprefarchi.put(ConcatArchi, SyndexElementMap.get(attrs.getValue(TagName)));
				}
				if (NomBalise.equals("ComponentHWInstance"))
				{
					ConcatArchi = concat + "/@" + NomBalise + "." + jarchic;
					if (TagName.equals("name"))
					{
						Namearchi = attrs.getValue(TagName);
						maparchiconcat.put(ConcatArchi, Namearchi);
						jarchic++;
					}
				}
				if (TagName.equals("referencedComponent"))
				{
					Name = attrs.getValue(TagName);
					MapRef.put(concat, Name);
				}
				if (TagName.equals("name"))
				{
					Name = attrs.getValue(TagName);
					if (NomBalise.equals("inOutHWComponent"))
					{
						Name = attrs.getValue(TagName);
						concatIO = concat + "/@" + NomBalise + "." + jinout;
						MapCArchiTarget.put(concatIO, Name);
						jinout++;
					}
					if (NomBalise.equals("SynDExelements") & tempType.equals("duration"))
					{
						NameDuration = Name;
					}
				}
				if (TagName.equals("Value"))
				{
					if (NomBalise.equals("enumDuration") & tempType.equals("duration"))
					{
						DurationInitial = Name + "=" + valeurattribut + ";\n";
						if (mapDurationInitial.containsKey(NameDuration) == true)
						{
							// MAJ de l operateur
							String listeprec = mapDurationInitial.get(NameDuration);
							DurationInitial = DurationInitial + listeprec;
							mapDurationInitial.put(NameDuration, DurationInitial);
						}
						else
						{
							mapDurationInitial.put(NameDuration, DurationInitial);
						}
					}
				}

				if (TagName.equals("name"))
				{
					tempNameSecond = attrs.getValue(TagName);
				}

				if (TagName.equals("parentPort"))
				{
					tempParentPortSecond = attrs.getValue(TagName);
				}
			}

			if (NomBalise.equals("oneInputDelay"))
			{
				String delay = tempParentPortSecond + "/@" + NomBalise;
				mapconcat.put(delay, tempNameSecond);
				mapconcatName.put(delay, NameAppli);
			}
			if (NomBalise.equals("oneOutputConstant") | NomBalise.equals("oneOutputDelay"))
			{

				String oneoutput = tempParentPortSecond + "/@" + NomBalise;
				mapconcat.put(oneoutput, tempNameSecond);
				mapconcatName.put(oneoutput, NameAppli);
			}
			if (NomBalise.equals("inputPortActuator") | NomBalise.equals("inputPortFunction")
					| NomBalise.equals("inputPortApplication"))
			{
				concatActuator = tempParentPortSecond + "/@" + NomBalise + "." + jsecond;
				mapconcat.put(concatActuator, tempNameSecond);
				mapconcatName.put(concatActuator, NameAppli);
				jsecond++;
			}
			if (NomBalise.equals("outputPortSensor") | NomBalise.equals("outputPortFunction")
					| NomBalise.equals("outputPortApplication"))
			{
				concatActuator = tempParentPortSecond + "/@" + NomBalise + "." + josecond;
				mapconcat.put(concatActuator, tempNameSecond);
				mapconcatName.put(concatActuator, NameAppli);
				josecond = josecond + 1;
			}
			if (NomBalise.equals("portCondition"))
			{
				concatActuator = tempParentPortSecond + "/@" + NomBalise + "." + jcondsecond;
				mapconcat.put(concatActuator, tempNameSecond);
				mapconcatName.put(concatActuator, NameAppli);
				jcondsecond = jcondsecond + 1;
			}
		}
		if (!SaxFirst & SaxSecond)
		{
			tempStartSecond = nomElement;
			for (int i = 0; i < attrs.getLength(); i++)
			{
				String attName = attrs.getLocalName(i);
				String atrsource = attrs.getLocalName(i);
				String atrduration = attrs.getLocalName(i);
				String atrtarget = attrs.getLocalName(i);
				String atrRef2 = attrs.getLocalName(i);
				String owner2 = attrs.getLocalName(i);
				String referencedComponent = attrs.getValue(i);
				String valeur = attrs.getValue(i);
				if (attName.equals(""))
					attName = attrs.getQName(i);
				if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Application"))
				{
					tempTypeAssoc = "application";
				}
				if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:CompoundStructure"))
				{
					StructureType = "compoundStructure";
				}
				if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:ConditionedStructure"))
				{
					StructureType = "conditionnedStructure";
				}
				if (attName.equals("xsi:type") && attrs.getValue(i).equals("syndex:Hardware"))
				{
					tempCArchi = "hw";
				}
				if (attName.equals("xsi:type") && attrs.getValue(i).equals("syndex:Duration"))
				{
					tempCArchi = "duration";
				}
				if (attName.equals("xsi:type") && attrs.getValue(i).equals("syndex:AssociationSWtoHW"))
				{
					tempCArchi = "associationSWtoHW";
				}
				if (attName.equals("xsi:type") && attrs.getValue(i).equals("syndex:AssociationComtoProc"))
				{
					tempCArchi = "associationComtoProc";
				}
				if (attName.equals("name"))
				{
					tempNameSecond = attrs.getValue(attName);
					if (tempStartSecond.equals("SynDExelements"))
					{
						if (tempTypeAssoc.equals("application"))
						{
							NameAppli = tempNameSecond;
						}
						concatHW = "//@" + "SynDExelements" + "." + jHW;
						SyndexElementMapHW.put(concatHW, tempNameSecond);
					}
				}
				if (attName.equals("ownerInternalStructure"))
				{
					concatSecond1 = attrs.getValue(attName) + "/@" + tempStartSecond;
					if (StructureType.equals("conditionnedStructure"))
					{
						concatSecond1 = attrs.getValue(attName) + "/@" + "applicationInternalStructure";
					}
				}
				if (tempStartSecond.equals("componentInstance"))
				{
					if (tempTypeAssoc.equals("application"))
					{
						concatSecond2 = concatSecond1 + "/@" + tempStartSecond + "." + jSecond;
						mapcompoinstance.put(concatSecond2, tempNameSecond);
					}
				}
				if (tempStartSecond.equals("portCompoundStructure"))
				{
					portcompound = concatSecond1 + "/@" + tempStartSecond + "." + jportcompound;
					mapportcompound.put(portcompound, tempNameSecond);
				}

				if (tempStartSecond.equals("ComponentHWInstance") && tempCArchi.equals("hw"))
				{
					concatArchi1 = concatHW + "/@" + tempStartSecond + "." + jarchi;
				}
				if (attName.equals("name"))
				{
					NameArchi = attrs.getValue(attName);
					if (tempStartSecond.equals("ComponentHWInstance") && tempCArchi.equals("hw"))
					{
						NameCI = NameArchi;
					}
				}
				if (attName.equals("referencedComponent"))
				{
					atrRef2 = attrs.getValue(attName);
					concatSecondMap.put(concatSecond2, SyndexElementMap.get(atrRef2));
					jSecond = jSecond + 1;
				}
				if (attName.equals("duration"))
				{
					if (tempCArchi.equals("associationSWtoHW"))
					{
						atrduration = attrs.getValue(attName);
						tempduration = atrduration;
					}
					if (tempCArchi.equals("associationComtoProc"))
					{
						Duration1 = SyndexElementMap.get(valeur);
						mapDuration.put(SourceComtoProc, mapDurationInitial.get(Duration1));
					}
				}
				if (attName.equals("parentDuration"))
				{
					if (tempCArchi.equals("duration"))
					{
						ProcDuration = true;
						NameProcDuration = SyndexElementMap.get(attrs.getValue(attName));
					}
				}

				if (attName.equals("durationSW"))
				{
					durationSW = SyndexElementMap.get(attrs.getValue(attName));
				}

				// j'ai enlever Le boolean
				if (attName.equals("Value") && tempStartSecond.equals("enumDuration") && tempCArchi.equals("duration"))
				{
					DurationProcinitial = tempNameSecond + " = " + attrs.getValue(attName);
					DurationProc = DurationProc + DurationProcinitial + " ;" + "\n";
				}
				if (attName.equals("adequation"))
				{
					adeq = true;
				}
				if (attName.equals("sourceAppli"))
				{
					atrsource = attrs.getValue(attName);
					tempSourceAppli = concatSecondMap.get(atrsource);
					ListeDuree = tempSourceAppli + '=' + tempduration.toString() + '\n';
					if (adeq)
					{
						tempSourceAppli2 = mapcompoinstance.get(atrsource);
						SourceAppliBuf.append(atrsource);
						int cut = atrsource.indexOf("@app") - 1;
						int taille = SourceAppliBuf.length();
						SourceAppliBuf = SourceAppliBuf.delete(cut, taille);
						atrsource = SourceAppliBuf.toString();
						tempSourceAppli3 = SyndexElementMap.get(atrsource);

						tempSourceAppli2 = "[\\\\" + tempSourceAppli3 + "\\\\" + tempSourceAppli2 + ",attach_ref]\n";
						if (mapadeq.containsKey(tempNameSecond) == true)
						{
							String listeprec = mapadeq.get(tempNameSecond);
							Listeneuve = tempSourceAppli2 + listeprec;
							mapadeq.put(tempNameSecond, Listeneuve);
						}
						else
						{
							mapadeq.put(tempNameSecond, tempSourceAppli2);
						}
					}
				}
				if (attName.equals("targetHW"))
				{
					atrtarget = attrs.getValue(attName);
					String Operator = maprefarchi.get(atrtarget);
					if (mapassoc.containsKey(Operator) == true)
					{
						// MAJ de l operateur
						String listeprec = mapassoc.get(Operator);
						ListeDuree = ListeDuree + listeprec;
						mapassoc.put(Operator, ListeDuree);
					}
					else
					{
						mapassoc.put(Operator, ListeDuree);
					}
					if (adeq)
					{
						tempTargetAppli2 = maparchiconcat.get(atrtarget);
						TargetAppliBuf.append(atrtarget);
						int cut = atrtarget.indexOf("@arc") - 1;
						int taille = TargetAppliBuf.length();
						TargetAppliBuf = TargetAppliBuf.delete(cut, taille);
						atrtarget = TargetAppliBuf.toString();
						tempTargetAppli3 = SyndexElementMap.get(atrtarget);
						tempTargetAppli2 = " " + tempNameSecond + "  on  " + tempTargetAppli3 + "." + tempTargetAppli2;
						tempTargetAppli2 = "absolute constraint :" + tempTargetAppli2 + " ;";
					}
				}
				if (attName.equals("ownerINOUT"))
				{
					if (tempStartSecond.equals("portInstances"))
					{
						tempOwnerINOUT = attrs.getValue(attName);
					}
				}
				if (attName.equals("sourceAssociationComToProc"))
				{
					if (tempCArchi.equals("associationComtoProc"))
					{
						SourceComtoProc = SyndexElementMap.get(valeur);
					}
				}
				/*
				 * if(attName.equals("parentPort")){ tempParentPortSecond=attrs.getValue(attName); }
				 */

				if (attName.equals("ownerInput") | attName.equals("ownerOutput") | attName.equals("ownerCond"))
				{
					owner2 = attrs.getValue(attName);
					ownerTemp2 = owner2;
				}
				if (attName.equals("ownerInternalStructure"))
				{
					compound1 = attrs.getValue(attName) + "/@" + tempStartSecond;
					if (StructureType.equals("conditionnedStructure"))
					{
						compound1 = attrs.getValue(attName) + "/@" + "applicationInternalStructure";
					}
				}

				if (attName.equals("valueParameter"))
				{
					if (tempStart.equals("parameterValues"))
					{
						pV = true;
						if (!boolpV)
						{
							valueParam = valeur;
							boolpV = true;
						}
						else
						{
							valueParam = valueParam + ";" + valeur;
						}
					}
				}

				if (attName.equals("referencedComponent"))
				{
					referencedComponent = attrs.getValue(attName);
					referencedComponentTemp = referencedComponent;
				}
			}

			if (tempStartSecond.equals("portInstances"))
			{
				if (tempCArchi.equals("hw"))
				{
					concatArchi2 = concatArchi1 + "/@" + tempStartSecond + "." + jparchi;
					mapCArchi.put(concatArchi2, NameCI);
					jparchi++;
				}
			}

			if (tempStartSecond.equals("conditions"))
			{
				condition = true;
				concatcondition = compound1 + "/@" + tempStartSecond + "." + jconcatcondi;
				jconcatcondi++;
			}
			if (tempStartSecond.equals("portConditionReference"))
			{
				portcompound = concatcondition + "/@" + tempStartSecond + "." + jportcondi;
				mapportcompound.put(portcompound, tempNameSecond);
			}
			// ci
			if (tempStartSecond.equals("componentInstance"))
			{
				if (tempTypeAssoc.equals("application") || condition)
				{
					if (StructureType.equals("compoundStructure"))
					{
						compound2 = compound1 + "/@" + tempStartSecond + "." + jci;
						tempCipi = tempNameSecond;
					}
					if (StructureType.equals("conditionnedStructure"))
					{
						compound2 = concatcondition + "/@" + tempStartSecond + "." + jci;
						tempCipi = tempNameSecond;
					}
				}
				jci = jci + 1;
			}

			// portInstances
			if (tempStartSecond.equals("portInstances"))
			{
				if (tempTypeAssoc.equals("application"))
				{
					compound3 = compound2 + "/@" + tempStartSecond + "." + jpi;
					mapinterconne.put(compound3, mapconcat.get(ownerTemp2));
					mapcipi.put(compound3, tempCipi);
				}
				jpi = jpi + 1;
			}
		}
		// fin modifs 223
		if (!SaxFirst & !SaxSecond)
		{
			tempStart = nomElement;
			tempStartexecution = nomElement;
			if (attrs != null)
				for (int i = 0; i < attrs.getLength(); i++)
				{
					String attName = attrs.getLocalName(i);
					String atrName = attrs.getValue(i);
					String portName = attrs.getValue(i);
					String connectName = attrs.getValue(i);
					String connectTargetName = attrs.getValue(i);
					String connectTargetNameI = attrs.getValue(i);
					String connectSourceNameI = attrs.getValue(i);
					// String referencedComponent=attrs.getValue(i);
					String referenceArchitecture = attrs.getValue(i);
					String owner = attrs.getValue(i);
					if (attName.equals(""))
						attName = attrs.getQName(i);
					// Pour Actuator
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Actuator"))
					{
						SyndexType = "actuator";
					}
					// Pour Sensor
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Sensor"))
					{
						SyndexType = "sensor";
					}
					// Pour Function
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Function"))
					{
						SyndexType = "function";
					}
					// Pour DataConnection
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:DataConnection"))
					{
						SyndexType = "dataconnection";
					}
					// Pour Application
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Application"))
					{
						SyndexType = "";
						SyndexType = "application";
					}
					// Pour Constant
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Constant"))
					{
						SyndexType = "constant";
					}
					// Pour Delay
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Delay"))
					{
						SyndexType = "delay";
					}
					// Pour Process
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Processor"))
					{
						SyndexType = "processor";
						boolOperator = true;
					}
					// Pour FPGA
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:FPGA"))
					{
						SyndexType = "fpga";
						boolOperator = true;
					}
					// Pour ASIC
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:ASIC"))
					{
						SyndexType = "asic";
						boolOperator = true;
					}
					// Pour DSP
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:DSP"))
					{
						SyndexType = "dsp";
						boolOperator = true;
					}
					// Pour SAMPP
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:SAM"))
					{
						SyndexType = "sampp";
					}
					// Pour SAMMP
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:SAMPTP"))
					{
						SyndexType = "samp";
					}
					// Pour RAM
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:RAM"))
					{
						SyndexType = "ram";
					}
					// Pour Media
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Media"))
					{
						SyndexType = "media";
					}
					// Pour Hardware
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Hardware"))
					{
						SyndexType = "hardware";
					}
					if (attName.equals("parentProcessor") && boolOperator)
					{
						ProcName = attrs.getValue(i);
					}
					// associationSWtoHW
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:AssociationSWtoHW"))
					{
						SyndexType = "associationSWtoHW";

					}
					// associationComtoProc
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:AssociationComtoProc"))
					{
						SyndexType = "associationComtoProc";
					}
					// associationComtoProc
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Duration"))
					{
						SyndexType = "duration";
					}
					// internalINConnection
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:InternalDependencyIN"))
					{
						ConnectionType = "internaldependencyIN";
					}
					// internalOUTConnection
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:InternalDependencyOUT"))
					{
						ConnectionType = "internaldependencyOUT";
					}
					// internaldataConnection
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:InternalDataConnection"))
					{
						ConnectionType = "internaldataconnection";
					}
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:InternalDependency"))
					{
						ConnectionType = "internaldependency";
					}
					// size & vector
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:Vector"))
					{
						SizeType = "vector";
					}
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:CompoundStructure"))
					{
						StructureType = "compoundstructure";
					}
					if (attName.equals("xsi:type") & attrs.getValue(i).equals("syndex:ConditionedStructure"))
					{
						StructureType = "conditionnedstructure";
					}

					if (attName.equals("name"))
					{
						atrName = attrs.getValue(attName);
						tempName = atrName;
						list1.add(tempName);
						map.put(tempStart, tempName);
						if (SyndexType.equals("associationSWtoHW"))
						{
							if (tempStart.equals("SynDExelements"))
							{
								list1.clear();
							}
						}
						if (tempStart.equals("inputPortFunction"))
						{
							if (SyndexType.equals("function"))
							{
								inF = true;
							}
						}
						// Outputs
						if (tempStart.equals("outputPortFunction"))
						{
							if (SyndexType.equals("function"))
							{
								outF = true;
							}
						}
						if (tempStart.equals("inputPortApplication"))
						{
							if (SyndexType.equals("application"))
							{
								inP = true;
							}
						}
						if (tempStart.equals("portCondition"))
						{
							if (SyndexType.equals("application"))
							{
								inCond = true;
							}
						}
						// Outputs
						if (tempStart.equals("outputPortApplication"))
						{
							if (SyndexType.equals("application"))
							{
								outP = true;
							}
						}
						if (tempStart.equals("oneInputDelay"))
						{
							if (SyndexType.equals("delay"))
							{
								inD = true;
							}
						}
						if (tempStart.equals("oneOutputDelay"))
						{
							if (SyndexType.equals("delay"))
							{
								outD = true;
							}
						}
						if (tempStart.equals("inOutHWComponent"))
						{
							if (SyndexType.equals("processor"))
							{
								listProcessorinout.add(tempName);
							}
						}
						if (tempStart.equals("inOutHWComponent"))
						{
							if (SyndexType.equals("fga"))
							{
								listFpgainout.add(tempName);
							}
						}
						if (tempStart.equals("inOutHWComponent"))
						{
							if (SyndexType.equals("dsp"))
							{
								listDspinout.add(tempName);
							}
						}
						if (tempStart.equals("inOutHWComponent"))
						{
							if (SyndexType.equals("asic"))
							{
								listAsicinout.add(tempName);
							}
						}
						if (tempStart.equals("paramDeclarations"))
						{

							if (!boolParam)
							{
								paramDeclarations = tempName;
								boolParam = true;
							}
							else
							{
								paramDeclarations = paramDeclarations + ";" + tempName;
							}
						}
						if (tempStart.equals("oneOutputConstant"))
						{
							outputConstant = atrName;
						}
						if (tempStart.equals("oneInputDelay"))
						{
							inputDelay = atrName;
						}
						if (tempStart.equals("oneOutputDelay"))
						{
							outputDelay = atrName;
						}
					}
					if (attName.equals("type"))
					{
						if (tempStart.equals("inOutHWComponent"))
						{
							if (SyndexType.equals("processor"))
							{
								mapinout.put(tempName, attrs.getValue(attName));
							}
							if (SyndexType.equals("fpga"))
							{
								mapFpgainout.put(tempName, attrs.getValue(attName));
							}
							if (SyndexType.equals("dsp"))
							{
								mapDspinout.put(tempName, attrs.getValue(attName));
							}
							if (SyndexType.equals("asic"))
							{
								mapAsicinout.put(tempName, attrs.getValue(attName));
							}
						}
					}

					if (attName.equals("vector") && SizeType.equals("vector"))
					{
						if (SyndexType.equals("sensor"))
						{
							portSensor = attrs.getValue(attName);
						}
						if (SyndexType.equals("actuator"))
						{
							portActuator = attrs.getValue(attName);
						}
						if (SyndexType.equals("function"))
						{
							if (inF)
							{
								portInFunction = attrs.getValue(attName);
							}
							if (outF)
							{
								portOutFunction = attrs.getValue(attName);
							}

						}
						if (SyndexType.equals("application"))
						{
							if (inP | inCond)
							{
								portInP = attrs.getValue(attName);
							}
							if (outP)
							{
								portOutP = attrs.getValue(attName);
							}
							/*
							 * if(inCond){ portinCond=attrs.getValue(attName); }
							 */
						}
						if (SyndexType.equals("constant"))
						{
							portConstant = attrs.getValue(attName);
						}
						if (SyndexType.equals("delay"))
						{
							if (inD)
							{
								portInDelay = attrs.getValue(attName);
							}
							if (outD)
							{
								portOutDelay = attrs.getValue(attName);
							}
						}

					}
					if (attName.equals("parentPort"))
					{
						portName = attrs.getValue(attName);
						tempParentPort = portName;
						StringTokenizer s1 = new StringTokenizer(tempParentPort, "/");
						tempParentPort2 = s1.nextToken();
						mapconnect2.put(tempParentPort, tempNameObj);
						mapconnect.put(tempParentPort2, tempNameObj);
					}
					if (attName.equals("sourceDataConnection"))
					{
						connectName = attrs.getValue(attName);
						tempConnect = connectName;
						StringTokenizer st = new StringTokenizer(tempConnect, "/");
						tempSource = st.nextToken();
					}
					if (attName.equals("targetDataconnection"))
					{
						connectTargetName = attrs.getValue(attName);
						tempTargetConnect = connectTargetName;
						StringTokenizer s2 = new StringTokenizer(tempTargetConnect, "/");
						tempTarget = s2.nextToken();
					}
					if (attName.equals("sourceOutInstance"))
					{

						connectSourceNameI = attrs.getValue(attName);
						tempSourceI = connectSourceNameI;
					}
					if (attName.equals("sourceIDI") || attName.equals("sourceIDIC"))
					{

						connectSourceNameI = attrs.getValue(attName);
						tempSourceIDI = connectSourceNameI;
					}
					// ////////////

					if (attName.equals("sourceIDO"))
					{
						tempSourceIDO = attrs.getValue(attName);
					}
					if (attName.equals("source"))
					{
						source = attrs.getValue(attName);
					}
					if (attName.equals("target"))
					{
						target = attrs.getValue(attName);
					}
					if (attName.equals("targetIDI"))
					{
						connectTargetNameI = attrs.getValue(attName);
						tempTargetIDI = connectTargetNameI;
					}

					if (attName.equals("targetIDIC"))
					{
						connectTargetNameI = attrs.getValue(attName);
						tempTargetIDI = connectTargetNameI;
					}

					if (attName.equals("targetINInstance") || attName.equals("targetINInstanceC"))
					{
						connectTargetNameI = attrs.getValue(attName);
						tempTargetI = connectTargetNameI;
					}

					if (attName.equals("targetIDO"))
					{
						tempTargetIDO = attrs.getValue(attName);
					}
					if (attName.equals("sourceInOut"))
					{
						connectSourceNameI = attrs.getValue(attName);
						SourceInOut = connectSourceNameI;
					}
					if (attName.equals("targetInOut"))
					{
						connectTargetNameI = attrs.getValue(attName);
						TargetInOut = connectTargetNameI;
					}

					if (attName.equals("referencedHWComponent"))
					{
						referenceArchitecture = attrs.getValue(attName);
						referenceArchiTemp = referenceArchitecture;
					}

					if (attName.equals("protocoleType"))
					{
						protocoleType = attrs.getValue(attName);
					}
					if (attName.equals("broadcast"))
					{
						if (SyndexType.equals("sampp"))
						{
							if (tempStart.equals("SynDExelements"))
							{
								broadcast = true;
								mapmedia.put(tempName, broadcast);
							}
						}
						if (SyndexType.equals("samp"))
						{
							if (tempStart.equals("SynDExelements"))
							{
								broadcast = true;
								mapmedia.put(tempName, broadcast);
							}
						}
						if (SyndexType.equals("ram"))
						{
							if (tempStart.equals("SynDExelements"))
							{
								broadcast = true;
								mapmedia.put(tempName, broadcast);
							}
						}
						if (SyndexType.equals("media"))
						{
							if (tempStart.equals("SynDExelements"))
							{
								broadcast = true;
								mapmedia.put(tempName, broadcast);
							}
						}
					}
					if (attName.equals("main"))
					{
						if (SyndexType.equals("application"))
						{
							listalgoM.add("main algorithm " + tempName + ";\n");
						}
						if (SyndexType.equals("hardware"))
						{
							listarchiM.add("main architecture " + tempName + ";\n");
						}
					}
					if (attName.equals("typePort"))
					{
						typePort = owner;
					}

					if (attName.equals("valueCondition"))
					{
						if (tempStart.equals("conditions"))
						{
							valeurcondition = attrs.getValue(attName);
						}
					}

					if (attName.equals("conditionPort"))
					{
						if (tempStart.equals("conditions"))
						{
							conditionPort = mapconcat.get(attrs.getValue(attName));
							conditionPort = "conditions: " + conditionPort + " = " + valeurcondition + ";";
						}
					}

					if (attName.equals("constantValue"))
					{
						if (SyndexType.equals("constant"))
						{
							constantValue = owner;
						}
					}

					if (attName.equals("name"))
					{
						if (tempStart.equals("componentInstance"))
						{
							listreference.add(owner);
						}
					}
					if (attName.equals("delayValue"))
					{
						if (SyndexType.equals("delay"))
						{
							delayValue = owner;
						}
					}
					// fin attName
				}
			if (SyndexType.equals("sampp"))
			{
				if (tempStart.equals("SynDExelements") & !broadcast)
				{
					mapmedia.put(tempName, broadcast);
				}
			}
			if (SyndexType.equals("samp"))
			{
				if (tempStart.equals("SynDExelements") & !broadcast)
				{
					mapmedia.put(tempName, broadcast);
				}
			}
			if (SyndexType.equals("ram"))
			{
				if (tempStart.equals("SynDExelements") & !broadcast)
				{
					mapmedia.put(tempName, broadcast);
				}
			}
			if (SyndexType.equals("media"))
			{
				if (tempStart.equals("SynDExelements") & !broadcast)
				{
					mapmedia.put(tempName, broadcast);
				}
			}
			if (tempStart.equals("SynDExelements"))
			{
				tempNameObj = tempName;
			}
			// Table d'association
			if (tempStart.equals("inputPortActuator") | tempStart.equals("inputPortFunction")
					| tempStart.equals("inputPortApplication") | tempStart.equals("oneInputDelay")
					| tempStart.equals("portCondition"))
			{
				j++;
			}
			if (tempStart.equals("outputPortSensor") | tempStart.equals("outputPortFunction")
					| tempStart.equals("outputPortApplication") | tempStart.equals("oneOutputDelay")
					| tempStart.equals("oneOutputConstant"))
			{
				jo = jo + 1;
			}

			// reference architecture
			if (tempStart.equals("ComponentHWInstance"))
			{
				if (SyndexType.equals("hardware"))
				{
					mapArchi.put(tempName, SyndexElementMap.get(referenceArchiTemp));
				}
			}

			if (tempStart.equals("internalConnector"))
			{
				if (ConnectionType.equals("internaldataconnection"))
				{
					listdependance.add("strong_precedence_data" + " " + mapcipi.get(tempSourceI) + "."
							+ mapinterconne.get(tempSourceI) + " -> " + mapcipi.get(tempTargetI) + "."
							+ mapinterconne.get(tempTargetI) + ";" + "\n");

				}

				if (ConnectionType.equals("internaldependencyIN"))
				{

					listdependance.add("strong_precedence_data" + " " + mapportcompound.get(tempSourceIDI) + " -> "
							+ mapcipi.get(tempTargetIDI) + "." + mapinterconne.get(tempTargetIDI) + ";" + "\n");

				}
				if (ConnectionType.equals("internaldependencyOUT"))
				{
					listdependance.add("strong_precedence_data" + " " + mapcipi.get(tempSourceIDO) + "."
							+ mapinterconne.get(tempSourceIDO) + " -> " + mapportcompound.get(tempTargetIDO) + ";"
							+ "\n");
				}
				if (ConnectionType.equals("internaldependency"))
				{
					listdependance.add("strong_precedence_data" + " " + mapportcompound.get(source) + " -> "
							+ mapportcompound.get(target) + ";" + "\n");
				}

			}
			if (tempStart.equals("HWConnection"))
			{
				listconnecthw.add(mapCArchi.get(SourceInOut) + "." + mapconnectionArchi.get(SourceInOut) + " "
						+ mapCArchi.get(TargetInOut) + ";\n");
			}
		}
	}

	/*
	 * Fonction endElement
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(String namespaceURI, String simpleName, String qualifiedName) throws SAXException
	{
		String nomElement = simpleName;
		if (nomElement.equals(""))
			nomElement = qualifiedName;
		if (SaxFirst)
		{
			if (nomElement.equals("ComponentHWInstance"))
			{
				jparchi = 0;
				jarchi = jarchi + 1;
			}
			if (nomElement.equals("SynDExelements"))
			{
				jfirst++;
				jarchic = 0;
				jinout = 0;
				jarchi = 0;
				jsecond = 0;
				josecond = 0;
				jcondsecond = 0;
			}
		}
		/*****
		 * 2eme SAX pour endElement
		 * 
		 */

		if (!SaxFirst & SaxSecond)
		{

			if (nomElement.equals("conditions"))
			{
				condition = false;
				jportcondi = 0;
			}
			if (nomElement.equals("componentInstance"))
			{
				jpi = 0;
			}
			if (nomElement.equals("portInstances"))
			{
				mapconnectionArchi.put(concatArchi2, MapCArchiTarget.get(tempOwnerINOUT));
			}
			if (nomElement.equals("ComponentHWInstance"))
			{
				jparchi = 0;
				jarchi = jarchi + 1;
			}
			if (nomElement.equals("portCompoundStructure"))
			{
				jportcompound = jportcompound + 1;
			}
			if (nomElement.equals("portConditionReference"))
			{
				jportcondi = jportcondi + 1;
			}
			if (nomElement.equals("componentInstance"))
			{
				if (!pV)
				{
					mapEssai.put(tempCipi, SyndexElementMap.get(referencedComponentTemp));
				}
				else
				{
					valueParam = "<" + valueParam + "> " + tempCipi;

					mapEssai.put(valueParam, SyndexElementMap.get(referencedComponentTemp));

					pV = false;
					boolpV = false;
				}
				valueParam = "";
			}
			if (nomElement.equals("conditions"))
			{
				jci = 0;
			}
			if (nomElement.equals("SynDExelements"))
			{
				jci = 0;
				jHW++;
				jsecond = 0;
				josecond = 0;
				jarchi = 0;
				adeq = false;
				jportcompound = 0;
				jconcatcondi = 0;

				if (tempCArchi.equals("duration"))
				{
					if (ProcDuration)
					{
						MapProcDuration.put(NameProcDuration, DurationProc);
					}
					else
					{
						NameProcDuration = "ExtraDuration";
						MapProcDuration.put(NameProcDuration, DurationProc);
					}
					DurationProcinitial = "";
					DurationProc = "";
					ProcDuration = false;
					NameProcDuration = "";
					NameDuration = "";
					tempCArchi = "";
				}

				if (tempCArchi.equals("application"))
				{
					tempCArchi = "";
				}
				if (tempCArchi.equals("hw"))
				{
					tempCArchi = "";
				}
				if (tempCArchi.equals("associationSWtoHW"))
				{
					tempCArchi = "";
				}
				if (tempCArchi.equals("associationComtoProc"))
				{
					tempCArchi = "";
				}
			}
		}
		/**
		 * 3 eme SAX pour endElement
		 */
		if (!SaxFirst & !SaxSecond)
		{
			if (nomElement.equals("internalConnector"))
			{
				tempTargetI = "";
				tempSourceIDO = "";
				tempTargetIDO = "";
				tempSourceIDI = "";
			}
			if (nomElement.equals("inputPortFunction"))
			{
				inF = false;
			}
			if (nomElement.equals("outputPortFunction"))
			{
				outF = false;
			}
			if (nomElement.equals("inputPortApplication"))
			{
				inP = false;
			}
			if (nomElement.equals("outputPortApplication"))
			{
				outP = false;
			}
			if (nomElement.equals("portCondition"))
			{
				inCond = false;
			}
			if (nomElement.equals("oneInputDelay"))
			{
				inD = false;
			}
			if (nomElement.equals("oneOutputDelay"))
			{
				outD = false;
			}
			/*
			 * if(nomElement.equals("componentInstance")){ jpi=0; }
			 */
			if (nomElement.equals("executionPhases"))
			{
				if (SyndexType.equals("application"))
				{}
				if (SyndexType.equals("function"))
				{

				}
			}
			if (nomElement.equals("conditions"))
			{
				itr = listreference.iterator();
				Conditionnement = "references: \n";
				while (itr.hasNext())
				{

					String element = itr.next();
					String element1 = mapEssai.get(element);
					Conditionnement = Conditionnement + element1 + " " + element + ";" + "\n";
				}
				Conditionnement = Conditionnement + "dependences: " + "\n";
				itr = listdependance.iterator();
				while (itr.hasNext())
				{
					String element = itr.next();
					Conditionnement = Conditionnement + element;
				}

				mapconditionnement.put(conditionPort, Conditionnement);
				conditionPort = "";
				Conditionnement = "";
				listreference.clear();
				listdependance.clear();
			}

			if (nomElement.equals("sizePort"))
			{
				if (SyndexType.equals("sensor"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					portSensor = "! " + typePort + "[" + portSensor + "] " + tempName + " " + jo + ";\n";
					listOutputSensor.add(portSensor);
					portSensor = "truc";
					listOutputSensor1.addAll(listOutputSensor);
				}
				if (SyndexType.equals("actuator"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					portActuator = "? " + typePort + "[" + portActuator + "] " + tempName + "	" + j + ";\n";
					listInputActuator.add(portActuator);
					portActuator = "";
				}
				if (SyndexType.equals("function"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					if (inF)
					{
						portInFunction = "? " + typePort + "[" + portInFunction + "] " + tempName + "	" + j + ";\n";
						listInputFunction.add(portInFunction);
						portInFunction = "";
					}
					if (outF)
					{
						portOutFunction = "! " + typePort + "[" + portOutFunction + "] " + tempName + " " + jo + ";\n";
						listOutputFunction.add(portOutFunction);
						portOutFunction = "";
					}
				}
				if (SyndexType.equals("application"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					if (inP || inCond)
					{
						portInP = "? " + typePort + "[" + portInP + "] " + tempName + "	" + j + ";\n";
						listInputP.add(portInP);
						portInP = "";
					}
					if (outP)
					{
						portOutP = "!" + typePort + "[" + portOutP + "] " + tempName + " " + jo + ";\n";
						listOutputP.add(portOutP);
						portOutP = "";
					}

				}
				if (SyndexType.equals("constant"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					portConstant1 = "! " + typePort + "[" + portConstant + "] " + tempName + " 1" + ";\n";
				}
				if (SyndexType.equals("delay"))
				{
					if (typePort.equals(""))
					{
						typePort = "int";
					}
					if (inD)
					{
						portInDelay1 = "? " + typePort + "[" + portInDelay + "] " + tempName + " 1;";
					}
					if (outD)
					{
						portOutDelay1 = "! " + typePort + "[" + portOutDelay + "] " + tempName + " 1;";
					}
				}
			}
			if (nomElement.equals("SynDExelements"))
			{
				j = 0;
				jo = 0;
				jcond = 0;

				jSecond = 0;
				if (SyndexType.equals("associationComtoProc"))
				{
					SyndexType = "";
					list1.clear();
				}
				if (SyndexType.equals("duration"))
				{
					SyndexType = "";
					list1.clear();
				}
				// Actuator
				if (SyndexType.equals("actuator"))
				{
					if (paramDeclarations.equals(""))
					{
						paramDeclarations = "";
					}
					else
					{
						paramDeclarations = "<" + paramDeclarations + ">";
					}
					buffer.append("def actuator" + " " + list1.get(0) + paramDeclarations + " :" + " \n");
					list1.remove(0);
					itr = listInputActuator.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					if (listexecutionPhaseAc.isEmpty())
					{
						buffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseAc.iterator();
						buffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						buffer.append(";\n\n");
					}
					paramDeclarations = "";
					listexecutionPhaseAc.clear();
					listInputActuator.clear();
					list1.clear();
					SyndexType = "";
					map.clear();
					boolParam = false;
				}

				// Sensor
				if (SyndexType.equals("sensor"))
				{
					if (paramDeclarations.equals(""))
					{
						paramDeclarations = "";
					}
					else
					{
						paramDeclarations = "<" + paramDeclarations + ">";
					}
					buffer.append("def sensor" + " " + list1.get(0) + paramDeclarations + ":" + " \n");
					list1.remove(0);
					itr = listOutputSensor1.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					if (listexecutionPhaseS.isEmpty())
					{
						buffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseS.iterator();
						buffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						buffer.append(";\n\n");
					}
					boolParam = false;
					listexecutionPhaseS.clear();
					paramDeclarations = "";
					listOutputSensor.clear();
					listOutputSensor1.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
				}
				// Constant
				if (SyndexType.equals("constant"))
				{
					buffer.append("def constant" + " " + list1.get(0) + " :" + " \n");
					list1.remove(0);
					buffer.append(portConstant1 + "\n\n");
					portConstant1 = "";
					list1.clear();
					SyndexType = "truc";
				}
				// Delay
				if (SyndexType.equals("delay"))
				{
					buffer.append("def memory" + " " + list1.get(0) + " :" + " \n");
					list1.remove(0);
					buffer.append(portInDelay1 + "\n" + portOutDelay1 + "\n");
					portOutDelay = "";
					portInDelay = "";
					list1.clear();
					SyndexType = "truc";
					if (listexecutionPhaseD.isEmpty())
					{
						buffer.append("\n\n");
					}
					else
					{
						itr = listexecutionPhaseD.iterator();
						buffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						buffer.append(";\n\n");
					}
					listexecutionPhaseD.clear();
				}
				// Function
				if (SyndexType.equals("function"))
				{
					if (paramDeclarations.equals(""))
					{
						paramDeclarations = "";
					}
					else
					{
						paramDeclarations = "<" + paramDeclarations + ">";
					}
					buffer.append("def algorithm" + " " + list1.get(0) + paramDeclarations + " :" + "\n");
					list1.remove(0);
					itr = listInputFunction.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					itr = listOutputFunction.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					if (listexecutionPhaseF.isEmpty())
					{
						buffer.append("\n\n");
					}
					else
					{
						itr = listexecutionPhaseF.iterator();
						buffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						buffer.append(";\n\n");
					}
					listexecutionPhaseF.clear();
					paramDeclarations = "";
					listInputFunction.clear();
					listOutputFunction.clear();
					list1.clear();
					map.clear();
					SyndexType = "truc";
					boolParam = false;
				}
				// dataconnection
				if (SyndexType.equals("dataconnection"))
				{
					buffer.append("strong_precedence_data" + " " + mapconnect.get(tempSource) + "."
							+ mapconcat.get(tempConnect) + "->" + mapconnect.get(tempTarget) + "."
							+ mapconcat.get(tempTargetConnect) + "\n");
				}
				// Processor
				if (SyndexType.equals("processor"))
				{
					hardwareBuffer.append("\ndef operator" + " " + list1.get(0) + ":" + " \n");
					Iterator<String> deff = mapinout.keySet().iterator();
					Iterator<String> deff1 = mapinout.values().iterator();
					while (deff.hasNext() & deff1.hasNext())
					{
						String element = deff.next();
						String element1 = deff1.next();
						hardwareBuffer.append("gate" + " " + element1 + " " + element + ";\n");
					}
					String test = SyndexElementMap.get(ProcName);
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (mapassoc.containsKey(test))
					{
						String asd = mapassoc.get(test);
						hardwareBuffer.append(asd);
					}
					if (listexecutionPhaseProcessor.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseProcessor.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseProcessor.clear();
					list1.remove(0);
					listProcessorinout.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					protocoleType = "truc";
				}
				// Fpga
				if (SyndexType.equals("fpga"))
				{
					hardwareBuffer.append("def operator" + " " + list1.get(0) + ":" + " \n");
					Iterator<String> deff = mapFpgainout.keySet().iterator();
					Iterator<String> deff1 = mapFpgainout.values().iterator();

					while (deff.hasNext() & deff1.hasNext())
					{

						String element = deff.next();
						String element1 = deff1.next();
						hardwareBuffer.append("gate" + " " + element1 + " " + element + ";\n");

					}
					String test = SyndexElementMap.get(list1.get(0));
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (mapassoc.containsKey(test))
					{
						String asd = mapassoc.get(test);
						hardwareBuffer.append(asd);
					}
					if (listexecutionPhaseFp.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseFp.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseFp.clear();
					listProcessorinout.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					protocoleType = "truc";
				}
				// Asic
				if (SyndexType.equals("asic"))
				{
					hardwareBuffer.append("def operator" + " " + list1.get(0) + ":" + " \n");
					Iterator<String> deff = mapAsicinout.keySet().iterator();
					Iterator<String> deff1 = mapAsicinout.values().iterator();
					while (deff.hasNext() & deff1.hasNext())
					{
						String element = deff.next();
						String element1 = deff1.next();
						hardwareBuffer.append("gate" + " " + element1 + " " + element + ";\n");
					}
					String test = SyndexElementMap.get(list1.get(0));
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (mapassoc.containsKey(test))
					{
						String asd = mapassoc.get(test);
						hardwareBuffer.append(asd);
					}
					if (listexecutionPhaseAsic.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseAsic.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseAsic.clear();
					listProcessorinout.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					protocoleType = "truc";
				}
				// DSP
				if (SyndexType.equals("dsp"))
				{
					hardwareBuffer.append("\ndef operator" + " " + list1.get(0) + ":" + " \n");
					Iterator<String> deff = mapDspinout.keySet().iterator();
					Iterator<String> deff1 = mapDspinout.values().iterator();
					while (deff.hasNext() & deff1.hasNext())
					{
						String element = deff.next();
						String element1 = deff1.next();
						hardwareBuffer.append("gate" + " " + element1 + " " + element + ";\n");
					}
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (mapassoc.containsKey(list1.get(0)))
					{
						String asd = mapassoc.get(list1.get(0));
						hardwareBuffer.append(asd);
					}
					if (listexecutionPhaseDsp.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseDsp.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseDsp.clear();
					listProcessorinout.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					protocoleType = "truc";
				}
				// SAMPP
				if (SyndexType.equals("sampp"))
				{
					hardwareBuffer.append("\ndef media" + " " + list1.get(0) + ":" + " \n");
					hardwareBuffer.append(SyndexType + ";\n");
					if (mapDuration.containsKey(list1.get(0)))
					{
						String asd = mapDuration.get(list1.get(0));
						hardwareBuffer.append(asd);
					}
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (listexecutionPhaseSampp.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseSampp.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseSampp.clear();
					list1.clear();
					SyndexType = "truc";
					mapenum.clear();
					map.clear();
					listenum.clear();
				}
				// SAMMP
				if (SyndexType.equals("samp"))
				{
					hardwareBuffer.append("\ndef media" + " " + list1.get(0) + ":" + " \n");
					hardwareBuffer.append(SyndexType + ";\n");
					if (mapDuration.containsKey(list1.get(0)))
					{
						String asd = mapDuration.get(list1.get(0));
						hardwareBuffer.append(asd);
					}
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (listexecutionPhaseSamp.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseSamp.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseSamp.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					listenum.clear();
				}
				// RAM
				if (SyndexType.equals("ram"))
				{
					hardwareBuffer.append("\ndef media" + " " + list1.get(0) + ":" + " \n");
					hardwareBuffer.append(SyndexType + ";\n");
					if (mapDuration.containsKey(list1.get(0)))
					{
						String asd = mapDuration.get(list1.get(0));
						hardwareBuffer.append(asd);
					}
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (listexecutionPhaseR.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseR.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							hardwareBuffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseR.clear();
					list1.remove(0);
					list1.clear();
					SyndexType = "truc";
					map.clear();
					listenum.clear();
				}
				// MEdia
				if (SyndexType.equals("media"))
				{
					hardwareBuffer.append("\ndef media" + " " + list1.get(0) + ":" + " \n");
					hardwareBuffer.append(SyndexType + ";\n");
					if (mapDuration.containsKey(list1.get(0)))
					{
						String asd = mapDuration.get(list1.get(0));
						hardwareBuffer.append(asd);
					}
					if (MapProcDuration.containsKey(list1.get(0)))
					{
						hardwareBuffer.append(MapProcDuration.get(list1.get(0)));
					}
					if (listexecutionPhaseM.isEmpty())
					{
						hardwareBuffer.append("\n");
					}
					else
					{
						itr = listexecutionPhaseM.iterator();
						hardwareBuffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						hardwareBuffer.append(";\n\n");
					}
					listexecutionPhaseM.clear();
					list1.clear();
					SyndexType = "truc";
					map.clear();
					listenum.clear();
				}
				// Application
				if (SyndexType.equals("application"))
				{
					if (paramDeclarations.equals(""))
					{
						paramDeclarations = "";
					}
					else
					{
						paramDeclarations = "<" + paramDeclarations + ">";
					}
					buffer.append("def algorithm" + " " + list1.get(0) + " " + paramDeclarations + ":" + "\n");
					list1.remove(0);
					itr = listInputP.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					itr = listOutputP.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}
					itr = listIncond.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						buffer.append(element);
					}

					/* if compound Structure */
					if (StructureType.equals("compoundstructure"))
					{
						buffer.append("conditions: true;" + "\n");
						itr = listreference.iterator();
						buffer.append("references:\n");
						while (itr.hasNext())
						{

							String element = itr.next();
							String element1 = mapEssai.get(element);
							buffer.append(element1 + " " + element + ";" + "\n");
						}
						itr = listdependance.iterator();
						buffer.append("dependences:\n");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
					}
					if (StructureType.equals("conditionnedstructure"))
					{
						Iterator<String> def = mapconditionnement.keySet().iterator();
						Iterator<String> def1 = mapconditionnement.values().iterator();
						while (def.hasNext() & def1.hasNext())
						{
							String element = def.next();
							String element1 = def1.next();
							buffer.append(element + "\n" + element1);
						}
					}
					/* fin if compoundStructure */

					if (listexecutionPhaseA.isEmpty())
					{
						buffer.append("\n\n");
					}
					else
					{
						itr = listexecutionPhaseA.iterator();
						buffer.append("code_phases: ");
						while (itr.hasNext())
						{
							String element = itr.next();
							buffer.append(element);
						}
						buffer.append(";\n\n");
					}
					listexecutionPhaseA.clear();
					if (cpt == 0)
					{
						itr = listalgoM.iterator();
						while (itr.hasNext())
						{
							String element = itr.next();
							associationBuffer.append(element);
							cpt = 1;
						}
					}
					listInputP.clear();
					listOutputP.clear();
					listIncond.clear();
					listInputApplication.clear();
					listOutputApplication.clear();
					list1.clear();
					listdependance.clear();
					// mapEssai.clear();
					map.clear();
					SyndexType = "truc";
					boolParam = false;
					paramDeclarations = "";
					listreference.clear();
					StructureType = "";
					mapconditionnement.clear();
				}
				// Hardware
				if (SyndexType.equals("hardware"))
				{
					hardwareBuffer.append("\ndef architecture" + "	" + list1.get(0) + ":" + "\n");
					list1.remove(0);
					Iterator<String> def = mapArchi.keySet().iterator();
					Iterator<String> def1 = mapArchi.values().iterator();
					while (def.hasNext() & def1.hasNext())
					{
						String element = def.next();
						String element1 = def1.next();
						if (mapmedia.containsKey(element1))
						{
							if (mapmedia.get(element1))
							{
								listMedias.add(element1 + " " + element + " " + "broadcast" + ";");
							}
							else
							{
								listMedias.add(element1 + " " + element + " " + "no_broadcast" + ";");
							}
						}
						else
						{
							listOperators.add(element1 + " " + element + ";");
						}
					}
					map.clear();
					SyndexType = "truc";
					itr = listOperators.iterator();
					hardwareBuffer.append("operators:\n");
					while (itr.hasNext())
					{
						String elemento = itr.next();
						hardwareBuffer.append(elemento + "\n");
					}
					itr = listMedias.iterator();
					hardwareBuffer.append("medias:\n");
					while (itr.hasNext())
					{
						String elementm = itr.next();
						hardwareBuffer.append(elementm + "\n");
					}
					itr = listconnecthw.iterator();
					hardwareBuffer.append("connections:\n");
					while (itr.hasNext())
					{
						String element = itr.next();
						hardwareBuffer.append(element);
					}
					itr = listarchiM.iterator();
					while (itr.hasNext())
					{
						String element = itr.next();
						associationBuffer.append(element);
					}
					associationBuffer.append("\n\n" + "#Extra durations" + "\n\n");
					String ExtraDuration = "ExtraDuration";
					if (MapProcDuration.containsKey(ExtraDuration))
					{
						associationBuffer.append("extra_durations_operator " + SyndexElementMap.get(NameProcDuration)
								+ " :\n");
						associationBuffer.append(MapProcDuration.get(ExtraDuration));
					}
					else
					{

					}
					Iterator<String> def2 = mapadeq.keySet().iterator();
					Iterator<String> def3 = mapadeq.values().iterator();
					associationBuffer.append("\n\n" + "# Software Components\n");
					while (def2.hasNext() & def3.hasNext())
					{
						String element = def2.next();
						String element1 = def3.next();
						associationBuffer.append("software_component " + element + ":\n" + element1 + "\n");
					}
					associationBuffer.append("# Constraints\n");
					associationBuffer.append(tempTargetAppli2);
					list1.clear();
					mapadeq.clear();
					listarchiM.clear();
					listconnecthw.clear();
					listMedias.clear();
					listOperators.clear();
				}

			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(char buf[], int offset, int len) throws SAXException
	{
		String s = new String(buf, offset, len);
		if (tempStartexecution.equals("executionPhases"))
		{
			if (SyndexType.equals("application"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseA.add(s);
			}
			if (SyndexType.equals("function"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseF.add(s);
			}
			if (SyndexType.equals("sensor"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseS.add(s);
			}
			if (SyndexType.equals("actuator"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseAc.add(s);
			}
			if (SyndexType.equals("delay"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseD.add(s);
			}
			if (SyndexType.equals("hardware"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseH.add(s);
			}
			if (SyndexType.equals("ram"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseR.add(s);
			}
			if (SyndexType.equals("media"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseM.add(s);
			}
			if (SyndexType.equals("fpga"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseFp.add(s);
			}
			if (SyndexType.equals("asic"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseAsic.add(s);
			}
			if (SyndexType.equals("sampp"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseSampp.add(s);
			}
			if (SyndexType.equals("samp"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseSamp.add(s);
			}
			if (SyndexType.equals("dsp"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseDsp.add(s);
			}
			if (SyndexType.equals("processor"))
			{
				StringTokenizer s1 = new StringTokenizer(s, "\n");
				s = s1.nextToken();
				listexecutionPhaseProcessor.add(s);
			}

		}

		if (tempStart.equals("vector"))
		{
			if (SyndexType.equals("sensor"))
			{
				portSensor = s;
			}
			if (SyndexType.equals("actuator"))
			{
				portActuator = s;
			}
			if (SyndexType.equals("function"))
			{
				if (inF)
				{
					portInFunction = s;
				}
				if (outF)
				{
					portOutFunction = s;
				}
			}
			if (SyndexType.equals("application"))
			{
				if (inP)
				{
					portInP = s;
				}
				if (outP)
				{
					portOutP = s;
				}
			}
			if (SyndexType.equals("constant"))
			{
				portConstant = s;
			}
			if (SyndexType.equals("delay"))
			{
				if (inD)
				{
					portInDelay = s;
				}
				if (outD)
				{
					portOutDelay = s;
				}
			}

		}
	}
}
