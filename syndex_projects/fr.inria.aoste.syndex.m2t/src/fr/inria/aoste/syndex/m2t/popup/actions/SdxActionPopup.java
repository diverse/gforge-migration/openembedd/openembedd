/****************************************************************************************
 * The SynDEX graphical editor is under CeCILL-B FREE SOFTWARE LICENSE AGREEMENT
 * 
 * @author Fadoi LAKHAL, Yves SOREL (INRIA Rocquencourt) - initial API
 * @author Christian Brunette (OpenEmbeDD integration team) - modification
 *******************************************************************************************/
package fr.inria.aoste.syndex.m2t.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;

import fr.inria.aoste.syndex.m2t.jobs.ModelToTextJob;

/**
 * This action calls the model to text transformation from syndex model to syndex textual file.
 * 
 */
public class SdxActionPopup implements IActionDelegate
{
	private IFile	syndexFile;

	/**
	 * The constructor.
	 */
	public SdxActionPopup()
	{
		this.syndexFile = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action)
	{
		if (syndexFile != null)
		{
			ModelToTextJob execute = new ModelToTextJob(syndexFile);
			execute.setUser(true);
			execute.schedule();
		}
		else
			MessageDialog.openError(new Shell(), "SDX Code Generation", "No syndex model file selected.");
	}

	/**
	 * Selection in the workbench has been changed. We can change the state of the 'real' action here if we want, but
	 * this can only happen after the delegate has been created.
	 * 
	 * @see IActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		if (selection instanceof StructuredSelection)
		{
			// the selection should be a single *.<SDX_EXTENSION> file
			if (((StructuredSelection) selection).size() == 1)
				syndexFile = (IFile) ((StructuredSelection) selection).getFirstElement();
			else
				syndexFile = null;
		}
	}
}
