package fr.inria.ctrte.bdd;



import java.util.Random;



import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class BDDCompute {

	/***
	 *  a + b -> a
	 * @param bdd
	 * @return
	 */
	
	public static BDD reduceMaximunBDD(BDD bdd)
	{
		if (bdd.isOne() || bdd.isZero() ) 
			return bdd;
		int var = bdd.var();	
		BDD bddVar = bdd.getFactory().ithVar(var);
		BDD branch0, branch1;
		branch0 = bdd.low();
		//restrict(bddVar.not());
		branch1 = bdd.high();
		//restrict(bddVar);
		branch0 = branch0.and((branch0.and(branch1)).not());
		branch1 = reduceMaximunBDD(branch1);
		branch0 = reduceMaximunBDD(branch0);
		return bddVar.ite(branch1, branch0);
	}
	
	
	/***
	 *  a+ b -->  a ./ b + / a . b 
	 * @param bdd
	 * @return
	 */
	public static BDD reduceMinimumBDD(BDD bdd)
	{
		if (bdd.isOne() || bdd.isZero() ) 
			return bdd;
		int var = bdd.var();
		BDD bddVar = bdd.getFactory().ithVar(var);
		BDD branch0, branch1;
		branch0 = bdd.low();
		//restrict(bddVar.not());
		branch1 = bdd.high();
		//restrict(bddVar);		
		branch1 = branch1.and((branch1.and(branch0)).not());
		branch1 = reduceMinimumBDD(branch1);
		branch0 = reduceMinimumBDD(branch0); 
		return bddVar.ite(branch1, branch0);
	}
	
	/***
	 * 
	 * @param bdd
	 * @param n
	 * @return
	 */
	private static BDD reduceMaximunBDD(BDD bdd, int n[])
	{
		if (bdd.isOne() )
		{
			BDD local=bdd.getFactory().one();
			for(int i=0;i<n.length;i++)
			{
				if (n[i]==0)
				{
					local=local.and(bdd.getFactory().ithVar(i));
				}
			}
			return local;
		}
		if ( bdd.isZero() ) 
			return bdd;
		int var = bdd.var();
		n[var]=1;
		BDD bddVar = bdd.getFactory().ithVar(var);
		BDD branch0, branch1;
		branch0 = bdd.low();
		//restrict(bddVar.not());
		branch1 = bdd.high();
		//restrict(bddVar);
		branch0 = branch0.and((branch0.and(branch1)).not());
		branch1 = reduceMaximunBDD(branch1,n);
		branch0 = reduceMaximunBDD(branch0,n);
		n[var]=0;
		return bddVar.ite(branch1, branch0);
	}
	 
	public static BDD orclock(int n, BDDFactory bddf)
	{
		BDD b= bddf.zero();
		for(int i=0;i<n ;i++)
		{
			b= b.or(bddf.ithVar(i));	
		}
		return b;
	}
	
	/***
	 * 
	 * @param bdd
	 * @param n
	 * @return
	 */
	private static BDD reduceMinimumBDD(BDD bdd, int n[])
	{
		if (bdd.isOne() )
		{
			BDD local=bdd.getFactory().one();
			for(int i=0;i<n.length;i++)
			{
				if (n[i]==0)
				{
					local=local.and(bdd.getFactory().ithVar(i).not());
				}
			}
			return local;
		}
		if ( bdd.isZero() ) 
			return bdd;
		int var = bdd.var();
		n[var]=1;
		BDD bddVar = bdd.getFactory().ithVar(var);
		BDD branch0, branch1;
		branch0 = bdd.low();
		//restrict(bddVar.not());
		branch1 = bdd.high();
		//restrict(bddVar);		
		branch1 = branch1.and((branch1.and(branch0)).not());
		branch1 = reduceMinimumBDD(branch1,n);
		branch0 = reduceMinimumBDD(branch0,n); 
		n[var]=0;
		return bddVar.ite(branch1, branch0);
	}
	
	
	/***
	 * 
	 * @param bdd
	 * @return
	 */
	
	public static BDD extractFactor(BDD bdd)
	{
	
		BDDFactory bddf=bdd.getFactory();
		BDD restrict = bddf.one();
		for( int i: bdd.scanSet())
		{
			BDD clk=bddf.ithVar(i);
			BDD bone=bdd.restrict(clk);
			BDD bzero=bdd.restrict(clk.not());
			if (!bone.isZero() && bzero.isZero() )
			{				
			
				restrict=restrict.and(clk);
			}
			else
			if (!bzero.isZero() && bone.isZero())
			{				
			
				restrict=restrict.and(clk.not());
			}		
		}
		return restrict;
	}
	
	
	/***
	 *  a + b -> a ./b + /a .b
	 * @param d : nb var
	 * @param nbvar
	 * @return
	 */
	public static BDD minimalBDD(BDD d, int nbvar)
	{
		int tb[]=new int[nbvar];
		for (int i=0;i<nbvar;i++)
		{
			tb[i]=0;
		}
		BDD b=reduceMinimumBDD(d, tb);
		return b;
	}
	
	/***
	 * a + b -> a.b 
	 * @param d : nb var
	 * @param nbvar
	 * @return
	 */
	public static BDD maximalBDD(BDD d, int nbvar)
	{
		int tb[]=new int[nbvar];
		for (int i=0;i<nbvar;i++)
		{
			tb[i]=0;
		}
		BDD b=reduceMaximunBDD(d, tb);
		return b;
	}
	
	
	/***
	 * 
	 * @param d
	 * @param nbvar
	 * @return
	 */
	public static int[] randomBDD(BDD d, int nbvar)
	{
		int tb[]=new int[nbvar];
		for (int i=0;i<nbvar;i++)
		{
			tb[i]=-1;
		}
		generateRandom(tb,d);
		for (int i=0;i<nbvar;i++)
		{
			if (tb[i]==-1)
				{
					if (rand.nextBoolean())	
						tb[i]=1;
					else
						tb[i]=0;
				}
		}		
		return tb;
	}
	
	private static Random 	rand = new Random();
	
	private static  int generateRandom(int tb[], BDD bdd)	
	{
		BDD bdd0, bdd1;
		if (bdd.isOne())
			return 0;	
		if (bdd.isZero())
		{
	
			return 0;
		}
		bdd0=bdd.low();
		bdd1=bdd.high();
		int var= bdd.var();
		if (bdd0.isZero())
		{
			tb[var]=1;
			return generateRandom(tb, bdd1);
		}
		if (bdd1.isZero())
		{
			tb[var]=0;			
			return generateRandom(tb, bdd0);
		}
		int rd = (int) (rand.nextDouble() * 2);
		if (rd==0)
		{
			tb[var]=1;
			return generateRandom(tb, bdd1);
		}
		tb[var]=0;
		return generateRandom(tb, bdd0);		
	}
}
