package fr.inria.ctrte.bdd;

import java.util.ArrayList;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class Cjeton {
	private String s = null;
	private Code c = null;
	boolean not;
	private Code xcode = null;
	
	private boolean ite=false;
	
	protected boolean isIte()
	{
		return ite;
	}


	protected void setIte( boolean ite )
	{
		this.ite = ite;
		
	
		
	}

	public boolean isNot()
	{
		return not;
	}
  
	public void setNot( boolean not )
	{
		this.not = not;
	}

	public enum Code {
		var(0, "var"), lparen(1, "lparen"), rparen(2, "rparen"), and(3, "and"), xor(4, "xor"), or(
				5, "or"), not(6, "not"), ite(7,"ite"), comma(8, "comma");

		int n;
		String s;

		private Code(int n, String s)
		{
			this.n = n;
			this.s = s;
		}
		public boolean notOp()
		{
			return !isOp();
		}
		
		public boolean isOp()
		{
			if (n == 3)
				return true;
			if (n == 4)
				return true;
			if (n == 5)
				return true;
			//if (n==7)
				//return true;
			return false;
		}
		public boolean parent()
		{
			if (n == 1)
				return true;
			if (n == 2)
				return true;
			return false;
		}
	}

	ArrayList<Cjeton> arrayjeton = new ArrayList<Cjeton>();
	ArrayList<Cjeton> light = new ArrayList<Cjeton>();

	public ArrayList<Cjeton> getArrayjeton()
	{
		return arrayjeton;
	}

	public ArrayList<Cjeton> getLight()
	{
		return light;
	}

	public Cjeton()
	{
		super();
		s = "@" ;				
		c = Code.lparen;
	}

	public Cjeton(String s, Code c)
	{
		this.s = s;
		this.c = c;
		if (c==Code.lparen)
			xcode=c;
	}

	public void add( Cjeton cj )
	{
		arrayjeton.add(cj);
	}

	@Override
	public String toString()
	{

		if (xcode != null)
			return toString2(0);
		return toString(0);
	}

	private String toString2( int n )
	{
		String tmp = "";
		for (int i = 0; i < n; i++)
		{
			tmp += " ";
		}
		String ls = tmp;
		if (not)
			ls += "not ";
		if (xcode != null )
		{
			ls += "opc " + xcode + arrayjeton.size()+" \n";
			String rt = "";
			for (Cjeton j : arrayjeton)
			{
				if (j.getC()==null  || j.getC().notOp())
				{
					ls += rt + tmp + j.toString2(n + 2)+" ";
					rt = "\n";
				}
			}
		} else
		{
			ls +=   s ;
		}
		return ls;
	}

	private String toString( int n )
	{

		String ls = "";
		int d = 3;
		if (not)
		{
			ls += "! ->";
		}
		ls += s;
		if (arrayjeton.size() == 0)
			return ls;
		if (not)
			ls += "\n";
		String tmp = "";
		for (int i = 0; i < n; i++)
		{
			tmp += " ";
		}
		String rt = "";
		for (Cjeton j : arrayjeton)
		{
			// if (j.getC()==Code.var)
			{
				ls += rt + tmp + j.toString(n + d) + " ";// +j.getC();
				rt = "\n";
			}

		}
		return ls;
	}

	public String getS()
	{
		return s;
	}

	public Code getXcode()
	{
		return xcode;
	}

	public Code getC()
	{
		return c;
	}

	public void transform()
	{
		mytransform();
		
		computereduce();
	}
	
	private void computereduce()
	{
		light = new ArrayList<Cjeton>();
	
		for(Cjeton cj:arrayjeton)
		{
			
			if (cj.getC()==null || cj.getC().notOp())
			{
				light.add(cj);
				cj.computereduce();
			}			
			
		}
	}
	
	private void mytransform()
	{
		
		if (arrayjeton.size() == 0)
		{			
			xcode = Code.var;
			return;
		}
		if (arrayjeton.size() == 1)// node supression
		{
			c= arrayjeton.get(0).getC();
			s=arrayjeton.get(0).getS();
			xcode=arrayjeton.get(0).xcode;
			ite=arrayjeton.get(0).ite;
			not=arrayjeton.get(0).not;			
			arrayjeton=arrayjeton.get(0).arrayjeton;
			transform();
			return;
		}

		Code op = null;
		boolean cptand = false, cptor = false, cptxor = false;
		for (Cjeton j : arrayjeton)
		{
			 j.transform();
			/** recherche de op le moins prioriataie ( priorite and , xor ,or ) */
			Code local = j.getC();
			if (local != null)
			{
				switch (local) {
					case or:
						op = local;
						cptor = true;
						break;
					case xor:
						if (op != Code.or)
							op = local;
						cptxor = true;
						break;
					case and:
						cptand = true;
						break;
					default:
						break;
				}
			}

		}
		// si il y a plusieur operator
		if ((cptor && cptxor) || (cptor && cptand) || (cptand && cptxor))
		{
			transform(cptor, cptxor, cptand);
			return;
		}
		if (cptor)
			xcode = Code.or;
		if (cptxor)
			xcode = Code.xor;
		if (cptand)
			xcode = Code.and;

	}

	private void transform( boolean or , boolean xor , boolean and )
	{
		xor = !or && xor;
		and = !(or || xor) && and;
		// or , xor ,and --> 1 seul a 1 , 2 autre a 0 ;
		Cjeton l = new Cjeton();
		ArrayList<Cjeton> newaj = new ArrayList<Cjeton>();
		newaj.add(l);
		for (Cjeton j : arrayjeton)
		{
			Code local = j.getC();
			if (( local == Code.or )|| ((local == Code.xor) && xor))
			{
				newaj.add(j);
				l.mytransform();
				l = new Cjeton();
				newaj.add(l);
			} else
			{
				j.mytransform();
				l.add(j);
			}
		}
		l.mytransform();
		arrayjeton = newaj;
	
		if (and)
			xcode = Code.and;
		if (xor)
			xcode = Code.xor;
		if (or)
			xcode = Code.or;
	}

	protected void setC( Code c )
	{
		this.c = c;
	}

	public BDD createBDD(BDDFactory bdd, ArrayList<String> dico)
	{
		//System.out.println("in");
		if (ite==true )
		{
			
			//System.out.println("ite in"+light.size());
			BDD iteif=light.get(0).createBDD(bdd, dico);
			BDD itethen=light.get(1).createBDD(bdd, dico);
			BDD iteelse=  light.get(2).createBDD(bdd, dico);
			return iteif.ite(itethen, iteelse);
			
		}
		
		if (light.size()==0 )
		{		
			//System.out.println(s);
			BDD d;
			if (s.equals("true") )
			{
				d=bdd.one();
			}
			else
			{
				if (s.equals("false") )
						{
					d=bdd.zero();
						}
				else	
				{
			 d=bdd.ithVar(dico.indexOf(s));
				}
			}
			if (not)
				return d.not();
			return d;
		}		
		if (light.size()==1)
		{			
			BDD d2;
		//	System.out.println(light.get(0));
			d2=light.get(0).createBDD(bdd, dico);
			return d2;
		}
		BDD r=null;	
	//	System.out.println(xcode +" " + s + " "+ ite + " ");
		switch (xcode) {				
			case or:
				r=bdd.zero();
				break;
			case and:
				r=bdd.one();
				break;
			case xor:
				r=bdd.zero();
				break;
			default:
				r=bdd.zero();
				break;
		}
		for(int i=0;i<light.size();i++)
		{
			BDD tmp=light.get(i).createBDD(bdd, dico);
			BDD old=r;
			switch (xcode) {
				case or:
					r=r.or(tmp);
					break;
				case and:
					r=r.and(tmp);
					break;
				case xor:
					r=r.xor(tmp);
					break;
				default:
					break;
			}
			old.free();
			tmp.free();
		}
	
		//System.out.println(".."+r);
		
		if (not)
			return r.not();
		return r;
	}


	protected void setXcode( Code xcode )
	{
		this.xcode = xcode;
	}
}
