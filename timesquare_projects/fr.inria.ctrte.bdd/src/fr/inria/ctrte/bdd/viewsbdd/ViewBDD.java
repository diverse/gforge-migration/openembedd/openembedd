package fr.inria.ctrte.bdd.viewsbdd;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import fr.inria.ctrte.bdd.BDDCompute;
import fr.inria.ctrte.bdd.Cparser;

/**

 */

public class ViewBDD extends ViewPart {

	protected class ComputeSelectionListener implements SelectionListener {
		
		public void widgetDefaultSelected( SelectionEvent e )
		{
			widgetSelected(e);

		}

		
		public void widgetSelected( SelectionEvent e )
		{
			try
			{
				cp = new Cparser(t.getText());			  
				//cp.disp();			
				x = cp.createBDD(bdd); 
				int n=cp.getDico().size();
				switch ( com.getSelectionIndex()) {
					case 0:
						
						break;
					case 1: x= BDDCompute.reduceMinimumBDD(x);
						
						break;
					case 2: x= BDDCompute.reduceMaximunBDD(x);
						
						break;
					case 3: x= BDDCompute.minimalBDD(x,n); 
					
						break;
					case 4: x= BDDCompute.maximalBDD(x,n);
					
					break;
					case 5:
						x= BDDCompute.extractFactor(x);
						break;
						
					default:
						break;
				}
				dispBDD(x);				
				int rd[]=BDDCompute.randomBDD(x,n);
				String resul =BDDFactory.nameFactory +  "\n" +
						"[";
				for (int i=0;i<rd.length;i++)
				{
					if (i!=0)
						 resul +=" ; ";
					switch ( rd[i] )
					{
						case 0: resul +="< " +cp.getDico().get(i)+" : 0> "  ; break ;
						case 1: resul +="< " +cp.getDico().get(i)+" : 1> "  ; break ;
						case -1: resul +="< " +cp.getDico().get(i)+" : x> "  ; break ;
					

					}
					
				}
				 resul +="]";
				t2.setText(resul);
				//System.out.println(x);
			} catch (Exception ex)
			{
				ex.printStackTrace();
				System.out.println(ex);
			}

		}
	}

	public class ResizeComposite implements ControlListener {
		
		public void controlMoved( ControlEvent e )
		{

		}

		
		public void controlResized( ControlEvent e )
		{			
			recomputsize();

		}
	}

	/**
	 * The constructor.
	 */
	public ViewBDD()
	{
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	BDDFactory bdd = null;
	BDD x;
	BDD n[];

	

	Text t = null, t2=null;
	Button b = null;
	Combo com;
	Composite comp = null;
	Group parent = null;
	private Tree tree = null;

	private void recomputsize()
	{
		Point pt = parent.getSize();
		comp.setSize(pt.x - 5, 25);
		comp.setLocation(5, 5);
		t.setLocation(0, 0);
		t.setSize(pt.x - 115, 20);
		b.setLocation(pt.x - 105, 0);
		b.setSize(100, 20);
		com.setLocation(5,40);
		com.setSize(pt.x - 10, 20);
		com.setVisible(true);	
		tree.setSize(pt.x - 10, pt.y - 170);
		tree.setLocation(5, 65);
		t2.setLocation(5, pt.y - 100);
		t2.setSize(pt.x-10 , 90);
		parent.layout();
		parent.redraw();  
	}

	Cparser cp = null;

	@Override
	public void createPartControl( Composite inparents )
	{
		bdd = BDDFactory.init(20000, 20000);		
		parent = new Group(inparents, SWT.NONE);
		GridLayout grid = new GridLayout(1, true);
		grid.marginBottom = 5; 
		grid.marginTop = 5;
		grid.marginLeft = 5;
		grid.marginRight = 5;

		//parent.setLayout(grid);
		parent.addControlListener(new ResizeComposite());
		comp = new Composite(parent, SWT.FILL);
		comp.setLayout(new GridLayout(2,false));

		t = new Text(comp, SWT.NONE);
		t.setText("e . ! f .(a.b+c.d ) ");
		t.setSize(250, 20);
		b = new Button(comp, SWT.NONE);
		b.setText("Compute");
		b.setSize(100, 20);
		b.addSelectionListener(new ComputeSelectionListener());
	
		com = new Combo(parent, SWT.NONE);
		com.setText("Option ");
		com.setSize(100, 40);  
		com.setItems(new String [] { "normal" ,"Xminimal","Xmaximal" ,"minimal" , "maximal" ,"factor" });
		com.select(0);
		
		tree = new Tree(parent, SWT.NONE);

		cp = new Cparser(t.getText());		
	//	cp.disp();
	
		x = cp.createBDD(bdd);
		dispBDD(x);
		t2 = new Text(parent,  SWT.MULTI );
		t2.setText(" .. ");
		t2.setSize(250, 40);
		
		System.out.println(x.nodeCount() + " ");
		System.out.println("x:" + x);

	//	PlatformUI.getWorkbench().getHelpSystem().setHelp(tree, "fr.inria.ctrte.bdd.viewer");
		makeActions();
		recomputsize();
	}

	private void makeActions()
	{

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus()
	{
		parent.setFocus();
	}

	private int dispBDD( BDD bdd )
	{
		if (tree==null)
			return 0;
		tree.removeAll();
		TreeItem t = new TreeItem(tree, SWT.NONE);

		t.setImage(folder);
		t.setText("@root");

		dispstr(t, bdd, "");
	//	System.out.println("node "+bdd.nodeCount() +" "+ bdd.pathCount());	
		bdd.printDot();
		bdd.printSet();
		//bdd.printSetWithDomains();
		tree.redraw();
		

		return 0;
	}

	/*private void expandall( TreeItem t )
	{ 
		t.setExpanded(true);
		for (TreeItem t2 : t.getItems())
		{
			expandall(t2); 
		}
	}*/

	Image element = PlatformUI.getWorkbench().getSharedImages().getImage(
			ISharedImages.IMG_OBJ_ELEMENT);
	Image folder = PlatformUI.getWorkbench().getSharedImages().getImage(
			ISharedImages.IMG_OBJ_FOLDER);
	Image root = PlatformUI.getWorkbench().getSharedImages().getImage(
			ISharedImages.IMG_OBJS_INFO_TSK);

	private void dispstr( TreeItem x , BDD b , String s )
	{

		TreeItem tx = new TreeItem(x, SWT.NONE);
		//System.out.println("hashcode "+b.hashCode()); 
		tx.setExpanded(true);
		if (b.isOne())
		{

			tx.setText(s + "1");
			tx.setImage(element);
			return;

		}
		if (b.isZero())
		{
			
			tx.setText(s + "0");
			tx.setImage(element);
			return;

		}

		tx.setText(s + "" + cp.getDico().get(b.var()));
		tx.setImage(folder);
		dispstr(tx, b.low(), "0: ");
		dispstr(tx, b.high(), "1: ");
	}
}