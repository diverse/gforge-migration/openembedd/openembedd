package fr.inria.ctrte.bdd;

import java.util.ArrayList;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import fr.inria.ctrte.bdd.Cjeton.Code;

public class Cparser {

	ArrayList<String> ls = new ArrayList<String>();
	ArrayList<Code> lb = new ArrayList<Code>();

	ArrayList<String> dico = new ArrayList<String>();
	Cjeton root;
	
	public Cparser(String s) {
		parse(s);
		analyse();

	}

	private void parse(String s)
	{
		String s2 = s.toLowerCase();
		s += " ";
		int last = -1;
		for (int i = 0; i < s.length(); i++) {
			String lc =new String( s.substring(i, i + 1));
			int n = " \t\n\r".indexOf(lc);

			if (n != -1) {
				if (last != i - 1) {
					add(new String(s2.substring(last + 1, i)));
				}
				last = i;
			}
			n = ".+^!(),".indexOf(lc);
			if (n != -1) {
				if (last != i - 1) {
					add(new String(s2.substring(last + 1, i)));
				}
				last = i;
				add(lc);
			}
			
		}
	//	System.out.println(ls.size()+ " == "+ lb.size());
	}
	
	
	
	private void analyse()
	{
		ArrayList<Cjeton> lj = new ArrayList<Cjeton>();
		 root=new Cjeton();
		Cjeton courant=root;
		courant.setC(Code.lparen);
		lj.add(courant);
		boolean b=false;
		boolean isite=false;
		for (int i=0;i<lb.size();i++)
		{
			//System.out.println(lb.get(i)+"  "+ls.get(i));
			Cjeton cj=new Cjeton(ls.get(i),lb.get(i));
			switch (lb.get(i)) {
				case not:
					b=true;
					break;
				case lparen:
					cj = new Cjeton();// "(", Code.lparen
					cj.setNot(b);					
					b=false;
					cj.setC(Code.lparen);
					courant.add(cj);					
					courant= cj ;
					lj.add(courant);
				//	System.out.println("up" + lj.size());
					if (isite) courant.setIte(true);
					
					isite=false;
					break;
				case rparen:
					lj.remove(courant);
					courant=lj.get(lj.size()-1);
					b=false;
					break;	
				case ite:
					isite=true;
					break;
				case comma:
					break;
				default:
					courant.add(cj);
					cj.setNot(b);
					b=false;
					break;
			}			
		}
		//System.out.println("ana"+root.getArrayjeton().size());
		if (root.getArrayjeton().size()==1){
			root.setXcode(Code.lparen);
		}
		//System.out.println(root.toString());
		root.transform();
	}

	
	
	
	public void disp()
	{
		
	System.out.println("after transform : \n"+ root.toString());
	System.out.println("ligth : \n");
	int i=0;
	System.out.println(root.getXcode());
	for ( Cjeton jeton :root.getLight())
	{
		System.out.println("--->"+ i+"  " + jeton);
		i++;
	}
		
		
	}
	
	
	
	private boolean add(String s) { 

		ls.add(s);

		if (s.compareTo("(")==0) return  lb.add(Code.lparen);
		if (s.compareTo(")")==0) return lb.add(Code.rparen);

		if (s.compareTo(".")==0) return  lb.add(Code.and);
	//	if (s.compareTo("&")==0) return  lb.add(Code.and);
		if (s.compareTo("and")==0) return lb.add(Code.and);

		if (s.compareTo("+")==0) return  lb.add(Code.or);
		//if (s.compareTo("|")==0) return  lb.add(Code.or);
		if (s.compareTo("or")==0) return lb.add(Code.or);

		if (s.compareTo("^")==0) return  lb.add(Code.xor);
		if (s.compareTo("xor")==0) return lb.add(Code.xor	);

		if (s.compareTo("!")==0) return  lb.add(Code.not);
		if (s.compareTo("not")==0) return lb.add(Code.not);

		if (s.compareTo(",")==0) return lb.add(Code.comma);
		if (s.compareTo("ite")==0) return lb.add(Code.ite);
		if (dico.indexOf(s)==-1)
		{			
			dico.add(s);
		}
		return lb.add(Code.var);

	}

	public static void main(String arg[]) {
		Cparser cp=new Cparser( " (ite ( c,  ite ( b,  e ,f),a ) )") ;
		//
				//" (a+b )" );
				//"a.b+c.d+e.f+g.h+i.j+k.l+m.n+o.p+q.r+s.t+u.v");
		//	"x +a . (b )+ c. not (d and !e ) + b ^ j . i  ");
		cp.disp();
		//cp.builddico(cp.root);
		System.out.println("dico");
		for(String s:cp.dico)
		{
			System.out.println(cp.dico.indexOf(s)+" " +s);
		}
		
		BDD bd= cp.createBDD(BDDFactory.init(10000, 10000));
		bd.printDot();		
		bd.printSet();
		System.out.println(".."+ bd.nodeCount()+ " bd.scanSet() :" );
	//	System.out.println(bd.forAll(bd.getFactory().ithVar(1).not()));
		
		//System.out.println(bdi);
		for (int i:bd.varProfile())
		{ 
			System.out.print(i);
		}
		System.out.println();
		for (int i:bd.and(bd.getFactory().ithVar(0)).varProfile())
		{
			System.out.print(i);
		}
		System.out.println();
		for (int i:bd.and(bd.getFactory().ithVar(0).not()).varProfile())
		{
			System.out.print(i);
		}
		
	}
	
	public BDD createBDD( BDDFactory bdd)
	{
	int n=dico.size();
	if (bdd.varNum()<n)
		 bdd.setVarNum(n);
		return root.createBDD(bdd, dico);
	}

	public ArrayList<String> getDico()
	{
		return dico;
	}
}
