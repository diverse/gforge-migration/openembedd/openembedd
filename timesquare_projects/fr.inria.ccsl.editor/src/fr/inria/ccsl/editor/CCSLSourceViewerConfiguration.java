package fr.inria.ccsl.editor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.rules.BufferedRuleBasedScanner;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;

import fr.inria.ccsl.editor.scanner.CCSLPartitionScanner;
import fr.inria.ccsl.editor.util.CCSLTextAttributeProvider;

public class CCSLSourceViewerConfiguration extends SourceViewerConfiguration {
	static class SingleTokenScanner extends BufferedRuleBasedScanner {
		public SingleTokenScanner(TextAttribute attribute) {
			setDefaultReturnToken(new Token(attribute));
		}
	}
	

	public CCSLSourceViewerConfiguration()
	{
		super();		
	}


	@Override
	public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
		return CCSLEditorPlugin.CCSL_PARTITIONING;
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] {
				CCSLPartitionScanner.CCSL_SLCOMMENT,
				IDocument.DEFAULT_CONTENT_TYPE
		};
	}
	

	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		CCSLTextAttributeProvider provider= CCSLEditorPlugin.getDefault().getTextAttributeProvider();
		PresentationReconciler reconciler= new PresentationReconciler();
		reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));
		
		// Cr�e le damager/repairer pour le code
		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(CCSLEditorPlugin.getDefault().getCCSLCodeScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		// Cr�e les damagers/repairers pour les commentaires
		dr= new DefaultDamagerRepairer(
				new SingleTokenScanner(provider.getAttribute(CCSLTextAttributeProvider.SLCOMMENT_ATTRIBUTE))
									);
		reconciler.setDamager(dr, CCSLPartitionScanner.CCSL_SLCOMMENT);
		reconciler.setRepairer(dr, CCSLPartitionScanner.CCSL_SLCOMMENT);
		
		return reconciler; 
	}
	
	@Override
	public IReconciler getReconciler(ISourceViewer sourceViewer) {
		return super.getReconciler(sourceViewer);
	}
	
}