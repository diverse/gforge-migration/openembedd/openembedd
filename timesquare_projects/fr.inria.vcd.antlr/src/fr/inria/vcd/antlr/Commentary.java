/*
 * 
 * @author : Anthony Gaudino
 * 		update : Benoit Ferrero
 *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

import fr.inria.base.MyManager;
import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.command.IDecodeComment;
import fr.inria.vcd.model.comment.DecodeDictionary;

public class Commentary {
	String s;
	int time;
	Uposition up = null; 
	boolean status;
	boolean constraint;
	boolean hide;
	String st[] = null;
	String pure;

	public String getS() {
		return s;
	}

	public String getLocation() {
		return up.toString();
	}

	/*public Commentary(String s, int time) {
		super();
		this.s = s;
		this.time = time;
		up = new Uposition(time, Enumblocktype.SimulationTime);
		array();
		status = (st[0].equals("status"));
		constraint = (st[0].equals("constraint"));
		hide = (st[0].equals("hide"));
	}*/

	public Commentary(String s, Uposition upin) {
		super();
		this.s = s;
		this.up = upin;
		if (up == null)
			up = new Uposition(0, null);
		array();
		status = (st[0].startsWith("status"));
		constraint = (st[0].startsWith("constraint"));
		hide = (st[0].startsWith("hide"));

	}

	public ICommemtCommand convert() {
		try {
			IDecodeComment idc= DecodeDictionary.getDefault().getDecoder(st[0]);
			if (idc!=null)
			{
				ICommemtCommand x= idc.create(st, pure, time);
				return x;
			}		
			return null;
		} catch (Exception ex) {
			MyManager.printError(ex);	
			return null;
		}
	}

	private String[] array() {
		String cs = s.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll(
				"\t", " ");
		String r = null;
		while (!cs.equals(r)) {
			r = cs;
			cs = cs.replaceAll("  ", " ");
		}
		while (cs.charAt(0) == ' ') {
			cs = cs.substring(1);
		}
		pure = cs;
		st = cs.split(" ");	
		return st; 
	}

	@Override
	public String toString() {

		if (status) {
			return ("Status : " + s + " @ " + up);
		}
		if (constraint) {
			return ("Constraint : " + s + " @ " + up);
		}
		if (hide) {
			return ("Hide : " + s + " @ " + up);
		}
		return ("Other : " + s + " @ " + up);

	}

}
