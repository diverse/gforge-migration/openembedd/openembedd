/*
 * 
 * @author : Anthony Gaudino
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */



package fr.inria.vcd.antlr;

import java.io.IOException;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.ConstraintCommentVisitor;
import fr.inria.vcd.model.visitor.StringVisitor;
import fr.inria.vcd.model.visitor.VarCollector;

public class VcdTest {
	/**
	 * @param args
	 * @throws IOException
	 * @throws IOException
	 * @throws RecognitionException
	 */
	public static void main(String[] args) throws IOException,
			RecognitionException {
		// JFileChooser chooser = new JFileChooser();
		// int returnVal = chooser.showOpenDialog(null);
		// if(returnVal == JFileChooser.APPROVE_OPTION) {
		// ANTLRFileStream input = new
		// ANTLRFileStream(chooser.getSelectedFile());
		// ANTLRFileStream input = new
		// ANTLRFileStream(chooser.getSelectedFile().getAbsolutePath());
		ANTLRFileStream input = new ANTLRFileStream(
				"D:/Antlr/Antlr/Antlr/test.vcd");
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		//Tree tree = (Tree) parser.vcd_definitions().getTree();
		VCDDefinitions vcd = adaptor.getVcd();
		System.out.println(vcd.visit(new StringVisitor()));
		VarCollector vc = new VarCollector(vcd.getDecls());
		vcd.visit(vc);
		System.out.println(vcd.getConstraintCommentCommand());
		/*
		 * for (IVar var : vc) { System.out.println(var); }
		 */
		ConstraintCommentVisitor cv = new ConstraintCommentVisitor();
		vcd.visit(cv);
		System.out.println("Clock : " + cv.getClock());
		System.out.println("Function : " + cv.getFunction());
		for (int i = 0; i < cv.getReferenceClocks().size()-1; i++)
			System.out.println("Reference clocks : " + cv.getReferenceClocks().get(i));
		System.out.println("\n\n");
		System.out.println(vcd.getConstraintCommentCommand());
		/*
		 * System.out.println("\n\nTree:"+tree.toStringTree()); for(int i = 0; i<tree.getChildCount();
		 * i++) System.out.println("\t"+tree.getChild(i).toStringTree());
		 */
	}
}
