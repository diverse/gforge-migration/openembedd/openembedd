/*
 * 
 * @author : Anthony Gaudino
 * 
 *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package fr.inria.vcd.antlr.editors;


import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import fr.inria.base.MyManager;
import fr.inria.vcd.dialogs.HelpDialog;
import fr.inria.vcd.menu.VcdMenu;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

/**
 * Manages the installation/deinstallation of global actions for multi-page
 * editors. Responsible for the redirection of global actions to the active
 * editor. Multi-page contributor replaces the contributors for the individual
 * editors in the multi-page editor.
 */
public class VcdMultiPageEditorContributor extends  MultiPageEditorActionBarContributor 
{
	private VcdMultiPageEditor vdt=null;
	private IMenuManager vcdMenu;
	private IEditorPart globaleditorPart;
	private IMenuManager syncMenu;
	private IMenuManager findMenu;
	private Action orderAction;
	private Action showGhostAction;
	private Action hideGhostAction;
	private Action partialGhostAction;
	private Action findAction;
	private IMenuManager clockMenu;
	private IMenuManager optionsMenu;
	protected ConstraintCommentCommand comment;
	private boolean state = true;
	private HelpDialog dialog = null;

	/**
	 * Creates a multi-page contributor.
	 */
	public VcdMultiPageEditorContributor() {
		super();	
	
	//	createActions();
	}

	
	/**
	 * Returns the action registed with the given text editor.
	 * 
	 * @return IAction or null if editor is null.
	 */
	protected IAction getAction(ITextEditor editor, String actionID) {
		return (editor == null ? null : editor.getAction(actionID));
	}

	/*
	 * (non-JavaDoc) Method declared in
	 * AbstractMultiPageEditorActionBarContributor.
	 */

	
	@Override
	public void setActivePage(IEditorPart part) {	
		
		
		if (globaleditorPart == part)
		{
			//createActions();
			//return;
		}
		globaleditorPart = part; 

		IActionBars actionBars = getActionBars();
		if (actionBars == null)
		{
			if (part!=null)
			if (part.getEditorSite()!=null)			
			actionBars = part.getEditorSite().getActionBars();
		}
		if ((actionBars != null) )
		{ //&& (part instanceof ITextEditor) ) {
		
			ITextEditor editor = (part instanceof ITextEditor) ? (ITextEditor) part : null;

			actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(),
					getAction(editor, ITextEditorActionConstants.DELETE));
			actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),
					getAction(editor, ITextEditorActionConstants.UNDO));
			actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),
					getAction(editor, ITextEditorActionConstants.REDO));
			actionBars.setGlobalActionHandler(ActionFactory.CUT.getId(),
					getAction(editor, ITextEditorActionConstants.CUT));
			actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(),
					getAction(editor, ITextEditorActionConstants.COPY));
			actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(),
					getAction(editor, ITextEditorActionConstants.PASTE));
			actionBars.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(),
					getAction(editor, ITextEditorActionConstants.SELECT_ALL));
			if (part instanceof VcdMultiPageEditor)
				actionBars.setGlobalActionHandler(ActionFactory.REFRESH.getId(),
							((VcdMultiPageEditor) part).getRefreshAction());				
			
			actionBars.setGlobalActionHandler(ActionFactory.FIND.getId(),new FindActionEditor(part));
			actionBars.setGlobalActionHandler(IDEActionFactory.BOOKMARK.getId(), getAction(editor,IDEActionFactory.BOOKMARK.getId()));
			actionBars.updateActionBars();			
		}
		createActions();
	}

	public class FindActionEditor extends Action
	{
		private FindActionEditor(IEditorPart part)
		{
			super(ActionFactory.FIND.getId());
			this.part = part;
		}

		IEditorPart part;
		
		@Override
		public void run()
		{
			if (part instanceof VcdMultiPageEditor )
			{
				
				
				if (((VcdMultiPageEditor) part).getActivePageID()==0)
				{
					((VcdMultiPageEditor) part).getFindAction().run();
					return ;
				}
				if (((VcdMultiPageEditor) part).getActivePageID()==1)
				{
					ITextEditor itext=	((VcdMultiPageEditor) part).getTextEditor();
					itext.getAction( ITextEditorActionConstants.FIND).run();
				}
				return ;
			}
			
			//super.run();
		}
		
	}
	
	@Override
	public void setActiveEditor(IEditorPart part) {
		
			setActivePage(part) ;//editorPart);
		//createActions();
	}
	
	private void createActions() {
		try
		{
			IEditorPart editorPart=null;
			IWorkbenchWindow iww= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			if (iww!=null)				
			if (iww.getActivePage()!=null)	
			{
				editorPart= (iww.getActivePage().getActiveEditor());
				
			}
		if (editorPart instanceof VcdMultiPageEditor)
		{
			vdt = (VcdMultiPageEditor) editorPart;
			ToolBarAPi.getDef().setCourantvcd(vdt);
		}
	
		if (vdt==null)
			return ;
		VcdMenu vcdmenu = vdt.getVcdmenu();
		vdt.vmpec=this;
		orderAction = vdt.getOrderAction();
		clockMenu.removeAll();
		clockMenu.add(orderAction);
		
		
		// Add "Show all ghost" to "Option" menu.
		showGhostAction = vcdmenu.getShowGhostAction();
		optionsMenu.removeAll();
		optionsMenu.add(new Separator ( "ghost"));
		optionsMenu.add(new Separator ( "sync"));
		optionsMenu.appendToGroup("ghost",showGhostAction);
		
		// Add "Hide all ghost" to "Option" menu.
		//hideGhostAction = vdt.getHideGhostAction();
		//optionsMenu.add(hideGhostAction);
		
		hideGhostAction = vcdmenu.getHideGhostAction();
	    optionsMenu.appendToGroup("ghost",new ActionContributionItem (hideGhostAction) {
	           @Override
	           public boolean isVisible()
	           {
	        	   return getAction().isEnabled();
	           }  
	           });
		
		// Add "Partial" to "Option" menu.
		partialGhostAction = vcdmenu.getPartialGhostAction();
		optionsMenu.appendToGroup("ghost",new ActionContributionItem (partialGhostAction) {
	           @Override
	           public boolean isVisible()
	           {
	        	   return getAction().isEnabled();
	           }  
	           });
		
		syncMenu.removeAll();
		for(Action a : vdt.getSyncAction())
		{
			syncMenu.add(a);
		}
		optionsMenu.appendToGroup("sync",syncMenu);
		
		// Add "Find" to "Find" menu.
		findAction = vdt.getFindAction();
		findMenu.removeAll();
		findMenu.add(findAction);
	
		if(state){
			Action help = new Action("About"){
				@Override
				public boolean isEnabled()
				{
					return true;
				}

				@Override
				public void run() {
					if(dialog == null)
						dialog = new HelpDialog(){
							@Override
							public void close() {
								dialog = null;
								super.close();
								vdt.setFocus();
							}
					};
				}
			};
			vcdMenu.add(vdt.getRefreshAction());
			vcdMenu.add(help);
			
			state = false;
		}
		}
		catch (Exception e) {
			MyManager.printError(e ," Ex");
			return ;
		}
	}

	// Add "Vcd Editor" to tolls bar
	@Override
	public void contributeToMenu(IMenuManager manager) {
		vcdMenu = new MenuManager("&Vcd Editor")
		{
			@Override
			public boolean isDynamic() {
				return true;
			}
		};		
		manager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS, vcdMenu);
	
		updatemenu();	
	}

	public void updatemenu()
	{
		
		createClockMenu(vcdMenu);
		createOptionsMenu(vcdMenu);
		createfindMenu(vcdMenu);
		
	}
	
	public void createClockMenu(IMenuManager manager) {
		// Add "Clock" to "VCD Editor" menu.
		clockMenu = new MenuManager("&Clock");
		manager.add(clockMenu);
	}

	public void createOptionsMenu(IMenuManager manager) {
		// Add "Option" to "VCD Editor" menu.
		optionsMenu = new MenuManager("&Options");
		manager.add(optionsMenu);
		
		// Add "Synchronized" to "Option" menu.
		syncMenu = new MenuManager("&Synchronized");
		optionsMenu.add(syncMenu);
	}
	
	public void createfindMenu(IMenuManager manager) {
		// Add "Find" to "VCD Editor" menu.
		findMenu = new MenuManager("&Find");
		manager.add(findMenu);
	}


	@Override
	public void contributeToToolBar(IToolBarManager manager) {
		
		IContributionItem tb[]=ToolBarAPi.getDef().getZoomApi();
		for(IContributionItem a: tb)
		{
			manager.add(a);
		}
		manager.update(true);		
	}
	
	
}
