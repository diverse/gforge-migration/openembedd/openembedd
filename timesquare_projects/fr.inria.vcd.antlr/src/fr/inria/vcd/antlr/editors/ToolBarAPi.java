package fr.inria.vcd.antlr.editors;


import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IAction;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.vcd.IComboZoom;
import fr.inria.vcd.antlr.ActivatorVcdAntlr;
import fr.inria.vcd.antlr.action.ComboContributionItem;
import fr.inria.vcd.antlr.action.Zoomaction;
import fr.inria.vcd.listener.IVcdDiagram;

public class ToolBarAPi {

	/**
	 * @return the combozoom
	 */
	public final IComboZoom getCombozoom()
	{
		return combozoom;
	}

	static private ToolBarAPi def = null;
	IVcdDiagram ivcd = null;

	public static ToolBarAPi getDef()
	{
		if (def == null)
		{
			def = new ToolBarAPi();
		}
		return def;
	}

	public ToolBarAPi()
	{
		super();
	}

	private IVcdDiagram courantvcd = null;

	public IVcdDiagram getCourantvcd()
	{
		return courantvcd;
	}

	public void setCourantvcd( IVcdDiagram courantvcd )
	{
		this.courantvcd = courantvcd;
		combozoom.updateFocus();
	}

	private IAction[] zoomaction = null;
	private ContributionItem[] zoomaci = null;
	private IComboZoom combozoom =null;
	public ContributionItem[] getZoomApi()
	{
		try
		{
			if (zoomaction == null)
			{
				zoomaction = new IAction[3];
				zoomaci = new ContributionItem[4];

				zoomaction[0] = new Zoomaction(this, MyPluginManager.getImageDescriptor(
						ActivatorVcdAntlr.PLUGIN_ID, "icons/loupemoins.PNG"), "Zoom -", 0, this);
				zoomaction[1] = new Zoomaction(this, MyPluginManager.getImageDescriptor(
						ActivatorVcdAntlr.PLUGIN_ID, "icons/loupe1.png"), "Zoom 0", 1, this);
				zoomaction[2] = new Zoomaction(this, MyPluginManager.getImageDescriptor(
						ActivatorVcdAntlr.PLUGIN_ID, "icons/loupeplus.PNG"), "Zoom +", 2, this);

				for (int i = 0; i < 3; i++)
				{
					zoomaci[i] = new ActionContributionItem(zoomaction[i]);

				}
				
				ComboContributionItem c=new ComboContributionItem(this);
					zoomaci[3] =c;
					combozoom =c ;
			}
		} catch (Exception e)
		{
			MyManager.printError(e);
		}
		return zoomaci;
	}

}
