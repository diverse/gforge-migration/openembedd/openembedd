/*
 * 
 * @author : Anthony Gaudino
 * 
 *  		update : Benoit Ferrero
 *  
 * INRIA/I3S Aoste 
 * Copyright 2007-2008
 */

package fr.inria.vcd.antlr.editors;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.vcd.VcdZoom;
import fr.inria.vcd.antlr.action.ActionSync;
import fr.inria.vcd.antlr.view.ConstraintView;
import fr.inria.vcd.antlr.view.VcdDiagramTest;
import fr.inria.vcd.dialogs.FindAction;
import fr.inria.vcd.dialogs.OrderAction;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.listener.MouseClickListener;
import fr.inria.vcd.listener.MouseDraggedListener;
import fr.inria.vcd.listener.MouseMoveListenerVCD;
import fr.inria.vcd.listener.VcdKeyListener;
import fr.inria.vcd.listener.WheelListener;
import fr.inria.vcd.menu.Mode;
import fr.inria.vcd.menu.VcdMenu;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.FigureCanvasBase;
import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.command.ScopeCommand;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.visitor.Collector;
import fr.inria.vcd.model.visitor.TraceCollector;
import fr.inria.vcd.view.ConstraintsFactory;
import fr.inria.vcd.view.IVcdMultiPageEditor;
import fr.inria.vcd.view.Iupdate;
import fr.inria.vcd.view.MarkerFactory;
import fr.inria.vcd.view.VcdFactory;

/**
 * An example showing how to create a multi-page editor. This example has 3
 * pages:
 * <ul>
 * <li>page 0 vcd diagram
 * <li>page 1 vcd text
 * 
 * </ul>
 */
public class VcdMultiPageEditor extends MultiPageEditorPart implements
		IResourceChangeListener, IVcdMultiPageEditor, IVcdDiagram, Iupdate {

	private ConstraintsFactory constraintsf;
	private MarkerFactory markerFactory;
	private boolean ctrlKey = false;
	private TextEditor editor = null;
	private VcdFactory factory = null;
	private Action findAction;
	private Action refreshAction;
	private Action orderAction;
	private boolean ghostMode = true;
	private ArrayList<Action> syncAction = new ArrayList<Action>();
	private ArrayList<Action> intervalAction;
	private ArrayList<MenuItem> intervalItem;
	private ListConnections list;
	private double markerZoom = 1;
	private Shell shell;
	private boolean simulation = false;
	private TraceCollector tc;
	private double tcZoom = 1.0;
	private VCDDefinitions vcd;
	// private VcdMultiPageEditor vcdEditor = this;
	private double zoom = 1;
	// private org.eclipse.draw2d.geometry.Rectangle oldrc = null;
	private static VCDDefinitions vcdin = null;
	private static IFile file = null;
	protected VcdMultiPageEditorContributor vmpec = null;
	private VcdZoom vcdzoom = null;
	private FigureCanvasBase fcb = null;

	private fr.inria.vcd.menu.VcdMenu vcdmenu;

	public fr.inria.vcd.menu.VcdMenu getVcdmenu() {
		return vcdmenu;
	}

	/**
	 * Creates a multi-page editor example.
	 */
	public VcdMultiPageEditor() {
		super();
		//MyManager.println("Create Editor VCD");
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		list = createList();
		vcdzoom = new VcdZoom(list, this);
		fcb = new FigureCanvasBase(this);
		vcdmenu = new VcdMenu(this);
	}

	@Override
	public void setFocus() {
		IEditorActionBarContributor contributor = getEditorSite().getActionBarContributor();
		if ( contributor instanceof MultiPageEditorActionBarContributor) 
		{
			((MultiPageEditorActionBarContributor) contributor).setActivePage(this);
		}
		vcdzoom.setIczoom(	ToolBarAPi.getDef().getCombozoom());
		ToolBarAPi.getDef().setCourantvcd(this);
		ConstraintView.getCourant().setConstraint(vcd.getDecls());
		ConstraintView.getCourant().setFocus();
		
		super.setFocus();
	}

	@Override
	protected void setActivePage(int pageIndex) {
		try
		{			
			getFindAction();
			getOrderAction();	
			super.setActivePage(pageIndex);
			setFocus();
		}
		catch (Exception e)
		{
			MyManager.printError(e);
		}
	}

	public void addCtrlKeyListener() {
		fcb.getCanvas().addKeyListener(new VcdKeyListener(this));
	}

	public void addMouseClickListener() {
		fcb.getCanvas().addMouseListener(new MouseClickListener(list, this));
	}

	public void addMouseDraggedListener() {
		MouseDraggedListener listener = new MouseDraggedListener(list, this);
		fcb.getNames().addMouseListener(listener);
		fcb.getNames().addMouseMoveListener(listener);
	}

	public void addMouseMouveListener() {
		fcb.getCanvas().addMouseMoveListener(new MouseMoveListenerVCD(this));
	}

	public void addMouseWheelListener() {
		fcb.getCanvas().addMouseWheelListener(new WheelListener(this));
	}

	/***
	 * canvas AND names have same vertical scroll canvas AND scale have same
	 * horizontal scroll
	 */
	public void addScrollListener() {
		fcb.addScrollListener();
	}

	public void create() {
		shell = getContainer().getShell();
		addMouseMouveListener();
		addMouseClickListener();
		addMouseDraggedListener();
		addMouseWheelListener();
		addScrollListener();
		addCtrlKeyListener();
		setPartName(getEditorInput().getName());
	}

	public void createClockMenu(Menu menuBar) {
	}

	public void createFileMenu(Menu menuBar) {
	}

	public void createMenuBar() {
	}

	public void createOptionsMenu(Menu menuBar) {
	}

	public int getSizeFile(IFile f) {
		try {
			if (!f.exists())
				return 0;
			f.refreshLocal(0, null);
			InputStream is = f.getContents();
			int n = 0;
			while (is.read() != -1) {
				n++;
			}
			return n;
		} catch (Exception ex) {
			MyManager.printError(ex);
			return 0;
		}
	}

	public boolean isNewFile(IFile f) {
		return f.equals(file);
	}

	public void setSimulationAvance(int etape) {
		fcb.setSimulationAvance(etape);
	}

	/**
	 * Creates page 0 of the multi-page editor, which shows the sorted text.
	 */
	void createPage0() {
		// Shell shell = new Shell();
		try {
			simulation = false;

			IFile ifloc = ((FileEditorInput) getEditorInput()).getFile();

			if (!isNewFile(ifloc) && getSizeFile(ifloc) != 0) {
				MyManager.printOKln("VCD : Read " + ifloc.getName() );
				String editorname = getEditorInput().getName();
				InputStream is = ((FileEditorInput) getEditorInput())
						.getStorage().getContents();
				new VcdDiagramTest().read(this, is, editorname);

			} else {
				simulation = true;
				MyManager.printOKln("VCD : Simulation ");
				vcd = vcdin;
				if (vcd == null) {
					vcd = new VCDDefinitions();
					vcd.setScope(new ScopeCommand(ScopeType.module, "default"));
					
				}
				Collector.drawInEditor(this, vcd, "VCD viewer");
				vcd.setFactory(factory);
			}
		} catch (Exception ex) {
			MyManager.printError(ex, " babbabab");
			// MessageDialog.openError(shell, "Error in VCD viewer",
			// ex.toString());
			if (ex instanceof RuntimeException)
				throw (RuntimeException) ex;
			throw new RuntimeException(ex);
			// return ;
		}
		getFactory().setVcdZoom(vcdzoom);
		getFactory().setVcd(vcd);
		constraintsf = new ConstraintsFactory(list, this);
		for (ConstraintCommentCommand ccc: vcd.getConstraintCommentCommand() )
		{
		if (ccc.getIc()!=null)
		{
			ccc.getIc().setVcdFactory(factory);
			ccc.getIc().setList(list);
			ccc.getIc().setIcc(constraintsf);
		}
			
		}
		markerFactory = new MarkerFactory(list, this);
		fcb.makevcd(getContainer(), simulation);
		// fcb.getComposite().getVerticalBar().setEnabled(true);
		fcb.getComposite().getVerticalBar().setVisible(true);
		fcb.getComposite().getHorizontalBar().setVisible(true);
		addPage(0, fcb.getComposite());
		setPageText(0, "Vcd Viewer");
		create();			
	//	vcdzoom.applyZoom(); //TODo fix Bug
		fcb.scrollUpdate();	
		fcb.layoutall();
	}

	public ListConnections createList() {
		return new ListConnections(new ArrayList<Description>(),
				new ArrayList<IFigure>(),
				new HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>>());
	}

	public void refresh() {
		try {
			MyPluginManager.refreshWorkspace();

			file = null;
			IFile ifloc = ((FileEditorInput) getEditorInput()).getFile();
			ifloc.getParent().refreshLocal(0, null);
			if ( getPageCount()>0)
				removePage(0);
			if (getPageCount() == 1)
				removePage(0);
			//MyManager.println(""+ifloc.exists());		
			fcb = new FigureCanvasBase(this);
			createPage0();
			createPage1();
			fcb.getComposite().setFocus();
			vcdzoom.setIczoom(	ToolBarAPi.getDef().getCombozoom());
			ToolBarAPi.getDef().setCourantvcd(this);
			ConstraintView.getCourant().resetConstraint(vcd.getDecls());
			ConstraintView.getCourant().setFocus();
			
			// replace(0,"$date", "$Date ");
		} catch (Exception ex) {
			MyManager.printError(ex);
		}
	}

	public int replace(int pos, String src, String newtext) {
		try {
			IDocument id = editor.getDocumentProvider().getDocument(getEditorInput());
			FindReplaceDocumentAdapter frda = new FindReplaceDocumentAdapter(id);
			IRegion n = frda.find(pos, src, true, false, false, false);
			if (n != null) {
				frda.replace(newtext, false);

				return n.getOffset();
			}
		} catch (Exception ex) {
			MyManager.printError(ex);
		}	
		return 0;
	}

	/**
	 * Creates page 1 of the multi-page editor, which contains a text editor.
	 */
	void createPage1() {
		try {
			FileEditorInput iep = (FileEditorInput) getEditorInput();
			IFile ifloc = iep.getFile();
			if (!ifloc.exists())
				return;
			ifloc.setDerived(false);
			MyPluginManager.refreshWorkspace();
			editor = new TextEditor();
			int index = addPage(editor, iep);
			// new FileStoreEditorInput(
			// EFS.getStore(iep.getFile().getLocationURI()) ));
			setPageText(index, editor.getTitle());
		} catch (PartInitException ex) {
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", null, ex.getStatus());
			MyManager.printError(ex);
		} catch (Exception ex) {
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", ex.getMessage(), null);
			MyManager.printError(ex);
		}
		// editor.getDocumentProvider().
	}

	@Override
	protected void createPages() {
		//MyManager.println("Create Pages  ");
		createPage0();
		createPage1();
		setFocus();
		
		/*
		 * public boolean isEditable() { return false; } public boolean
		 * isEditorInputReadOnly() { return true; }
		 */
	}

	/**
	 * The <code>MultiPageEditorPart</code> implementation of this
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	@Override
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		ToolBarAPi.getDef().setCourantvcd(null);
		ConstraintView.getCourant().setConstraint(null);
		super.dispose();
	}

	/**
	 * Saves the multi-page editor's document.
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		getEditor(1).doSave(monitor);
		// getEditor(0).doSave(monitor);
		setPageText(1, editor.getTitle());
		setInput(editor.getEditorInput());
		removePage(0);
		MyPluginManager.refreshWorkspace();
		createPage0();
	}

	/**
	 * Saves the multi-page editor's document as another file. Also updates the
	 * text for page 0's tab, and updates this multi-page editor's input to
	 * correspond to the nested editor's.
	 */
	@Override
	public void doSaveAs() {
		// IEditorPart editor =
		getEditor(0).doSaveAs();
		editor.doSaveAs();
		setPageText(1, editor.getTitle());
		setInput(editor.getEditorInput());
		// removePage(0);
		// MyManager.refreshWorkspace();
		// createPage0();
		// ((FileEditorInput) getEditorInput()).
	}

	public FigureCanvas getCanvas() {
		return fcb.getCanvas();
	}

	public ConstraintsFactory getConstraintsFactory() {
		return constraintsf;
	}

	public MarkerFactory getMarkerFactory() {
		return markerFactory;
	}

	public VcdFactory getFactory() {
		return factory;
	}

	public Action getFindAction() { // TODO REMOVE
		if (findAction == null) {
			findAction = new FindAction(this);
		}
		findAction.setText("&Find");
		return findAction;
	}

	public Action getRefreshAction() { // TODO REMOVE
		if (refreshAction == null) {
			refreshAction = new Action() {

				@Override
				public void run() {
					refresh();
					super.run();
				}
			};
		}
		refreshAction.setText("&Refresh");
		return refreshAction;
	}

	public boolean haveGhost() {
		ArrayList<IVar> listVar = new ArrayList<IVar>();
		listVar.addAll(getTc().getSelectedClocks());
		listVar.addAll(getTc().getAllnames());
		for (IVar var : listVar) {
			if (var.getValueFactory() != null) {
				if (var.getValueFactory().haveGhostinclock() == true)
					return true;
			}
		}
		return false;
	}

	public ArrayList<Action> getIntervalAction() {
		return intervalAction;
	}

	public ArrayList<MenuItem> getIntervalItem() {
		return intervalItem;
	}

	public ListConnections getList() {
		return list;
	}

	public FigureCanvasBase getfcb() {
		return fcb;
	}

	public double getMarkerZoom() {
		return markerZoom;
	}

	public Shell getMyShell() {
		return shell;
	}

	public FigureCanvas getNames() {
		return fcb.getNames();
	}

	public FigureCanvas getScale() {
		return fcb.getScale();
	}

	public Action getOrderAction() {
		if (orderAction == null) {
			orderAction = new OrderAction(this);
			orderAction.setText("&Ordering ...");
			orderAction.setToolTipText("&Use to hide show and move clock");
		}
		return orderAction;
	}

	public ArrayList<Action> getSyncAction() {
		if (syncAction.isEmpty()) {
			setIntervalAction(new ArrayList<Action>());
			int i = 0;
			ArrayList<String> clocksnames = new ArrayList<String>();
			Color color = MyColorApi.colorLightGraySync();
			for (ConstraintCommentCommand cc : getVcd()
					.getConstraintCommentCommand()) {
				if ((i % 3) == 0)
					color = MyColorApi.colorLightGraySync();
				if ((i % 3) == 1)
					color = MyColorApi.colorLightBlueSync();
				if ((i % 3) == 2)
					color = MyColorApi.colorRedSync();
				if (cc.getFunction() != Function._synchronizeswith)
					continue;
				// if (clocksnames.contains(cc.getReferenceClocks().get(0)))
				// continue;
				clocksnames.add(cc.getClock());
				list.getListInterval().put(cc, new ArrayList<ConstraintsConnection>());
				list.firstClock1Put(cc, new ArrayList<IFigure>());
				list.firstClock2Put(cc, new ArrayList<IFigure>());
				list.lastClock1Put(cc, new ArrayList<IFigure>());
				list.lastClock2Put(cc, new ArrayList<IFigure>());
				syncAction.add(new ActionSync(this, cc, IAction.AS_CHECK_BOX));
				list.actionForCommentPut(syncAction.get(i), cc);
				list.actionColorPut(syncAction.get(i), color);
				syncAction.get(i).setToolTipText(
						"&Use to hide or show synchronized interval");
				i++;
			}
		}
		return syncAction;
	}

	public ToolBar getToolbar() {
		return fcb.getToolbar();
	}

	/**
	 * Method declared on IEditorPart
	 */
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}

	/**
	 * The <code>MultiPageEditorExample</code> implementation of this method
	 * checks that the input is an instance of <code>IFileEditorInput</code>.
	 */
	@Override
	public void init(IEditorSite site, IEditorInput editorInput)
			throws PartInitException {
		if (!(editorInput instanceof IFileEditorInput))
			throw new PartInitException(
					"Invalid Input: Must be IFileEditorInput");
		super.init(site, editorInput);
	}

	public boolean isCtrlKey() {
		return ctrlKey;
	}

	public boolean isGhostMode() {
		return ghostMode;
	}

	/**
	 * Method declared on IEditorPart.
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	public boolean isSimulation() {
		return simulation;
	}

	int activePage=0;
	
	/**
	 * @return the activePage
	 */
	public final int getActivePageID()
	{
		return activePage;
	}

	public final ITextEditor getTextEditor()
	{
		return editor; 
	}
	
	/**
	 * Calculates the contents of page 2 when the it is activated.
	 */
	@Override
	protected void pageChange(int newPageIndex) {
		activePage=newPageIndex;
		IEditorActionBarContributor contributor = getEditorSite()
				.getActionBarContributor();
		if (contributor != null
				&& contributor instanceof MultiPageEditorActionBarContributor) {
			((MultiPageEditorActionBarContributor) contributor)
					.setActivePage(this);
		}
		if (getEditor(newPageIndex) != null)
			super.pageChange(newPageIndex);
	}

	/**
	 * Closes all project files on project close.
	 */
	public void resourceChanged(final IResourceChangeEvent event) {
		if (event.getType() == IResourceChangeEvent.PRE_CLOSE) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					IWorkbenchPage[] pages = getSite().getWorkbenchWindow()
							.getPages();
					for (int i = 0; i < pages.length; i++) {
						if (((FileEditorInput) editor.getEditorInput())
								.getFile().getProject().equals(
										event.getResource())) {
							IEditorPart editorPart = pages[i].findEditor(editor
									.getEditorInput());
							pages[i].closeEditor(editorPart, true);
						}
					}
				}
			});
		}
	}
	
	

	public void setCtrlKey(boolean ctrlKey) {
		this.ctrlKey = ctrlKey;
	}

	public void setFactory(VcdFactory factory) {
		this.factory = factory;
	}

	public void setGhostMode(boolean ghostM) {
		if (ghostM) {
			getConstraintsFactory().showAllGhost();
			vcdmenu.setGhostSelected(Mode.show);
		} else {
			getConstraintsFactory().hideAllGhost();
			vcdmenu.setGhostSelected(Mode.hide);
		}
		for (IVar vr : getTc().getSelectedClocks()) {
			if (vr.getValueFactory() != null) {
				vr.getValueFactory().setIsGhostVisible(ghostM);
			}
		}
		this.ghostMode = ghostM;
	}

	public void setIntervalAction(ArrayList<Action> intervalAction) {
		this.intervalAction = intervalAction;
	}

	public void setIntervalItem(ArrayList<MenuItem> intervalItem) {
		this.intervalItem = intervalItem;
	}

	public void setMarkerZoom(double markerZoom) {
		this.markerZoom = markerZoom;
	}

	public void setSimulation(boolean simulation) {
		this.simulation = simulation;
	}

	public void setTc(TraceCollector tc) {
		this.tc = tc;
	}

	public TraceCollector getTc() {
		return tc;
	}

	public void setTcZoom(double tcZoom) {
		this.tcZoom = tcZoom;
	}

	public double getTcZoom() {
		return tcZoom;
	}

	public void setVcd(VCDDefinitions vcd) {
		this.vcd = vcd;
	}

	public VCDDefinitions getVcd() {
		return vcd;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public double getZoom() {

		return zoom;
	}

	public VcdZoom getVcdzoom() {
		return vcdzoom;
	}

	public int update(int n1, int n2, boolean b) {

		fcb.update(n1, n2, b);
		return 0;
	}

	public static VcdMultiPageEditor createNewEditor(VCDDefinitions vcdins,
			IFile f) {
		vcdin = vcdins;
		try {
			file = f;
			IWorkbenchWindow iw = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow();

			IEditorPart editorInput = IDE.openEditor(iw.getActivePage(), f);
			editorInput.getEditorSite().getPart().setFocus();

			VcdMultiPageEditor vmpe = (VcdMultiPageEditor) editorInput;
			vmpe.setGhostMode(true);
			vcdin = null;
			file = null;
			return vmpe;
		} catch (PartInitException ex) {
			MyManager.printError(ex, "PartInitException");
		} catch (Exception ex) {
			MyManager.printError(ex);
		} catch (Error ex) {
			MyManager.printError(ex);
		}
		return null;
	}
}