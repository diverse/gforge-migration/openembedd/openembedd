/*
 * 
 * @author : Anthony Gaudino
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.vcd.model.command.VarCommand;
import fr.inria.vcd.model.keyword.VarType;

public class VarTree extends CommonTree {
	private VarCommand var = null;
	private VarType type = null;
	private int size = -1;
	private String ident=null;
	
	public VarTree() {
		super();
	}

	public VarTree(CommonTree arg0) {
		super(arg0);
	}

	public VarTree(Token arg0) {
		super(arg0);
	}

	public VarCommand getVar() {
		return this.var;
	}

	@Override
	public void addChild(Tree arg0) {
		if(arg0.isNil()) return;
		if(arg0.getType() == VcdLexer.END) return;
		if (type == null) type = VarType.valueOf(arg0.getText());
		else if (size == -1) size = Integer.parseInt(arg0.getText());
		else if (ident == null) ident = arg0.getText();
		else var = new VarCommand(type, size, ident, arg0.getText());			
	}
}
