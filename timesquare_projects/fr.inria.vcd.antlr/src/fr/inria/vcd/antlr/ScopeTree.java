/*
 * 
 * @author : Anthony Gaudino
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.vcd.model.command.ScopeCommand;
import fr.inria.vcd.model.keyword.ScopeType;

public class ScopeTree extends CommonTree {
	private ScopeCommand scope = null;
	private ScopeType type = null;
	
	public ScopeTree(Token arg0) {
		super(arg0);
	}

	public ScopeCommand getScope() {
		return this.scope;
	}

	@Override
	public void addChild(Tree arg0) {
		if(arg0.isNil()) return;
		if(arg0 instanceof ScopeTree) {
			if (scope != null)
				scope.addChild(((ScopeTree)arg0).getScope());
		} else if (arg0 instanceof VarTree) {
			scope.addChild(((VarTree)arg0).getVar());
		} else {
			if (type == null) type = ScopeType.valueOf(arg0.getText());
			else {
				scope = new ScopeCommand(type, arg0.getText());	
			}
		}
	}
}
