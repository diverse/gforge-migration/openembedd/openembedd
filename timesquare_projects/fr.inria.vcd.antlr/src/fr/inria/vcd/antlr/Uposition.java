/*
 * 
 * @author : Benoit Ferrero
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

/**
 * @author Benoit Ferrero
 *
 */
public class Uposition {

	/**
	 * 
	 */
	public enum Enumblocktype{
		
		SimulationTime(0),
		DumpAll(1),
		DumpVars(2),
		DumpOn(3),
		DumpOff(4),
		Date(5),
		Scope(6),
		TimeScale(7),
		UpScope(8),
		Var(9),
		Version(10),
		EndDef(11);
		
		private Enumblocktype(int n) {
			this.n = n;
		}
		String s;
		int n;
		
	}
	
	int value;
	Enumblocktype block;
	boolean theend=false;
	
	public boolean isTheend() {
		return theend;
	}
	public void setTheend(boolean theend) { 
		this.theend = theend;
	}
	public Uposition(int value, Enumblocktype block) {
		super();
		this.value = value;
		this.block = block;
	}
	public int getValue() {
		return value;
	}
	public Enumblocktype getBlock() {
		return block;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public void setBlock_Value(Enumblocktype block,int value) {
		this.block = block;
		this.value = value;
	}
	@Override
	public String toString() {
		if (theend)
			return "< After : " +block +" >" ;
		return "<" +block +" "+ value+ ">";
	}
	
	
	
	
	
	

}
