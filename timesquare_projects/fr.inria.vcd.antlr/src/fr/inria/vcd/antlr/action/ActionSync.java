/**
 * 
 */
package fr.inria.vcd.antlr.action;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;

import fr.inria.base.MyManager;
import fr.inria.vcd.antlr.editors.VcdMultiPageEditor;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public final class ActionSync extends Action {
	/**
	 * 
	 */
	private VcdMultiPageEditor vcdMultiPageEditor;
	ConstraintCommentCommand cc;
	public ActionSync(VcdMultiPageEditor vcdMultiPageEditor, ConstraintCommentCommand cc, int style)
	{			
		super(cc.toString(), style);
		this.vcdMultiPageEditor = vcdMultiPageEditor;
		this.cc=cc;
	}

	@Override
	public void run() {
		try{
		
		if (isChecked()) {
			vcdMultiPageEditor.getConstraintsFactory().drawSyncInterval(
					vcdMultiPageEditor.getList().actionForCommentGet(this),
					vcdMultiPageEditor.getList().actionColorGet(this));
		}
		if (!isChecked()) {
			for (IFigure f : vcdMultiPageEditor.getList().getListInterval()
					.get(vcdMultiPageEditor.getList().actionForCommentGet(this))) {
				if (f!=null)
				if (vcdMultiPageEditor.getCanvas().getContents()== f.getParent())
					vcdMultiPageEditor.getCanvas().getContents().remove(f);
			}
			vcdMultiPageEditor.getList().getListInterval().get(
					vcdMultiPageEditor.getList().actionForCommentGet(this)).clear();
		}
		}
		catch (Throwable t)
		{
			MyManager.printError(t,"Erreur Sync");
		}
	}
}