/**
 * 
 */
package fr.inria.vcd.antlr.action;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.base.MyManager;
import fr.inria.vcd.IComboZoom;
import fr.inria.vcd.antlr.editors.ToolBarAPi;

public class ComboContributionItem extends ContributionItem  implements IComboZoom {

	List<Double> ardb= new ArrayList<Double>();
	
	Combo combo;
	ToolBarAPi tba;

	private ToolItem toolitem;
	
	public ComboContributionItem( 	ToolBarAPi tba)
	{			
		this.tba=tba;
		ardb.add(6.25);
		ardb.add(12.5);
		ardb.add(25.0);
		ardb.add(50.0);
		ardb.add(100.0);
		ardb.add(200.0);
	//	ardb.add(300.0);
		ardb.add(400.0);
		
	}
	protected Control createControl( Composite parent )
	{
		combo = new Combo(parent, SWT.DROP_DOWN);		
		combo.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected( SelectionEvent e )
			{
				widgetSelected(e);
			}

			public void widgetSelected( SelectionEvent e )
			{
			//	MyManager.println(" zoom " +((Combo)e.getSource()).getSelectionIndex());
				//MyManager.println(" zoom " +((Combo)e.getSource()).getText());
				try
				{
					if (tba.getCourantvcd()==null)
						return ;
					Double old=tba.getCourantvcd().getTcZoom();
					String s= ((Combo)e.getSource()).getText();
					if (s==null)
						return ;
					if (s.length()==0)
						return;
					Double d= Double.parseDouble(s)/100;
					if ((d <0.001)  ||(d >5000)) 
					{
						MyManager.println("Zoom : Out of order");
						return ;					
					}
					tba.getCourantvcd().setZoom(old / d);
					tba.getCourantvcd().setTcZoom(d);
					tba.getCourantvcd().getVcdzoom().applyZoom();
					
					//select(d*100);
					
				}
				catch (Exception ex) {
					MyManager.printError(ex);
				}
			}
		});
		combo.addFocusListener(new FocusListener() {
			public void focusGained( FocusEvent e )
			{
			
			}

			public void focusLost( FocusEvent e )
			{
				
			}
		});
		updateitem();		
		select(100.0);		
		if (toolitem != null)
			toolitem.setWidth(80);		
		return combo;
	}

	@Override
	public final void fill(Composite parent) {
		
		createControl(parent);
	}

	@Override
	public void fill( ToolBar parent , int index )
	{
		toolitem = new ToolItem(parent, SWT.SEPARATOR, index);
		Control control = createControl(parent);
		toolitem.setControl(control);
	}

	@Override
	public boolean isEnabled()
	{
		if (tba.getCourantvcd()==null)
			return false;
		return true;
	}
	
	
	/* (non-Javadoc)
	 * @see fr.inria.vcd.antlr.editors.IComboZoom#select(java.lang.Double)
	 */
	public int select(Double  d)
	{
		int n=0;
		n= ardb.indexOf(d);
		if (n!=-1)
		{
			combo.select(n);
			combo.setToolTipText("Zoom in "+d+" % for VCD");
			return 1;
		}
		n=0;
		for( double dl : ardb)
		{
		if (dl>d)
			break;
			n++;
		}
		ardb.add(n,d);
		updateitem();
		n= ardb.indexOf(d);			
		combo.select(n);
		combo.setToolTipText("Zoom in "+d+" % for VCD");
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.vcd.antlr.editors.IComboZoom#updateitem()
	 */
	public int updateitem()		
	{
		combo.removeAll();
		ArrayList<String> lst= new ArrayList<String>();
		for (Double d:ardb)
		{
			lst.add(d.toString());				
		}
		combo.setItems(lst.toArray(new String[]{}));
		return 0;
	}
	
	public int updateFocus()
	{
		if (combo==null)
			return  -1;		
		if (tba==null)
			return  -1;
		if (tba.getCourantvcd()==null)
			return  -1;
		if (combo.isDisposed())
				return -1;
		combo.removeAll();
		combo.setItems(tba.getCourantvcd().getVcdzoom().getZoomlist().arrayOf());
		combo.select(tba.getCourantvcd().getVcdzoom().getZoomlist().getIndice());
		return 0;
	}
}