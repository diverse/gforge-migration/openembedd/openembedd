/**
 * 
 */
package fr.inria.vcd.antlr.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyManager;
import fr.inria.vcd.antlr.editors.ToolBarAPi;

public class Zoomaction extends Action {
	
	/**
	 * 
	 */
	
	private final ToolBarAPi toolBarAPi;
	ImageDescriptor imagedesc;
	int type;
	String tooltilps;
	ToolBarAPi tba;

	public Zoomaction(ToolBarAPi toolBarAPi, ImageDescriptor imagedesc, String tooltilps, int type, ToolBarAPi tba)
	{
		super();
		this.toolBarAPi = toolBarAPi;
		this.imagedesc = imagedesc;
		setImageDescriptor(imagedesc);
		this.tooltilps = tooltilps;
		setToolTipText(tooltilps);
		this.type = type;
		this.tba = tba;

	}

	@Override
	public void run()
	{
		try
		{
		if (tba.getCourantvcd()==null)
			return ;
		setEnabled(false);
		double d = tba.getCourantvcd().getTcZoom();
		tba.getCourantvcd().setZoom(1);
		if (type == 1)
		{
		/*	if (d == 1.0)
			{
				setEnabled(true);
				return;
			}*/
			tba.getCourantvcd().getVcdzoom().setComp(0);
			tba.getCourantvcd().setZoom(1 / d);
			d = 1.0;
			toolBarAPi.getCombozoom().select(100.0);
		}

		if (type == 0)
		{
			d = d * 0.5;
			/*if (tba.getCourantvcd().getVcdzoom().getComp() <= -5)
			{
				setEnabled(true);
			//	toolBarAPi.getCombozoom().select(d*100);
				return;
			}*/
			tba.getCourantvcd().getVcdzoom().setComp(
					tba.getCourantvcd().getVcdzoom().getComp() - 1);
			tba.getCourantvcd().setZoom(0.5);
			
		}
		if (type == 2)
		{
			d = d * 2;
			/*if (tba.getCourantvcd().getVcdzoom().getComp() >= 10)
			{
				setEnabled(true);
				//toolBarAPi.getCombozoom().select(d*100);
				return;
			}*/
			tba.getCourantvcd().setZoom(2);
			tba.getCourantvcd().getVcdzoom().setComp(
					tba.getCourantvcd().getVcdzoom().getComp() + 1);
		}
		tba.getCourantvcd().setTcZoom(d);
		tba.getCourantvcd().getVcdzoom().applyZoom();
		//toolBarAPi.getCombozoom().select(d*100);
		setEnabled(true);
	}
	
	catch (Exception ex) {
		MyManager.printError(ex);
	}

}
}