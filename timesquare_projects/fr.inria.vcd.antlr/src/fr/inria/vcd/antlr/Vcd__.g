lexer grammar Vcd;
@members {
int time=-1;
ArrayList<Commentary> acm=new ArrayList<Commentary>();
Uposition up=null;
       
public int getTime()
{
	return time;  
}

public ArrayList<Commentary> getAcm() 
{
	return acm;
}
public int stringtime(String s)
{
	time=Integer.parseInt(s);
	up = new Uposition(time,Uposition.Enumblocktype.SimulationTime );
	return time;
}

public	int addcomment(String s)
{
	//System.out.println(time+   " " +s );
	acm.add(new Commentary(s,up));
	return 0;
}
	
}
@header {package fr.inria.vcd.antlr;}

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 106
SIMULATION_TIME
	:	'#' DECIMAL_NUMBER
		{ String s=getText();
		 stringtime(new String (s.substring(1))); }
		; //TODO time
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 112
VAR_TYPE	:	'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1'
		|	'tri'   | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor';
		
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 115
SCOPE_TYPE 	:	'begin' | 'fork' | 'function' |	'module' | 'task';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 117
TIME_UNIT  	:	's' | 'ms' | 'us' | 'ns' | 'ps'	| 'fs';

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 119
REFERENCE
	:	IDENT '[' DECIMAL_NUMBER ']' 
	|	IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 123
ENDDEFINITIONS
	:	'$enddefinitions' {  up=new Uposition( 0,Uposition.Enumblocktype.EndDef  )  ;  };
	
// status  constraint
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 127
COMMENT	:
	'$comment' .* '$end' 
	 {  
	 String s=getText();	
	 addcomment(new String ( s.substring(8, s.length()-4)));
	 }
	 { skip();
	 };
	 


	 
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 139
DATE	:	'$date'  	{  up=new Uposition( 0,Uposition.Enumblocktype.Date  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 140
SCOPE   :	'$scope' 	{  up=new Uposition( 0,Uposition.Enumblocktype.Scope  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 141
TIMESCALE:	'$timescale'  	{  up=new Uposition( 0,Uposition.Enumblocktype.TimeScale  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 142
UPSCOPE	:	'$upscope'    	{  up=new Uposition( 0,Uposition.Enumblocktype.UpScope  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 143
END	:	'$end'		{ if (up!=null) up.setTheend(true);  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 144
VAR	:	'$var'		{  up=new Uposition( 0,Uposition.Enumblocktype.Var  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 145
VERSION	:	'$version'	{  up=new Uposition( 0,Uposition.Enumblocktype.Version  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 146
DUMPALL	:	'$dumpall'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpAll  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 147
DUMPON	:	'$dumpon'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOn  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 148
DUMPOFF	:	'$dumpoff'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOff  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 149
DUMPVARS:	'$dumpvars'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpVars  )  ;  };
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 151
SCALAR_VALUE_CHANGE
	:	(BIT|VALUE) IDENTIFIER_CODE;

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 154
DECIMAL_NUMBER
	:	'-'? (BIT|DIGIT)+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 156
REAL_VECTOR 
	:	('r'|'R') '-'? (BIT|DIGIT)* '.' (BIT|DIGIT)+; 

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 159
BINARY_VECTOR	
	:	('b'|'B') (BIT|VALUE)+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 161
IDENT	:	(LETTER|'_') (LETTER|DIGIT|BIT|'_'|'@'|'.')*;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 162
VERSION_TEXT
	:	(LETTER|DIGIT|BIT) (LETTER|DIGIT|BIT|IDENT_CHAR|'.')+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 164
IDENTIFIER_CODE
	:	IDENT_CHAR (DIGIT | BIT | LETTER | IDENT_CHAR |'.' |'_')*;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 166
DIGIT   :	'2'..'9';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 167
VALUE	:	'x' | 'X' | 'z' | 'Z';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 168
BIT 	:	 '0' | '1';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 169
LETTER	:	'a'..'z' | 'A'..'Z';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 170
fragment
IDENT_CHAR  :	 '!'..'-' | '/' | ':'..'@' | '['..'^' | '`' | '{'..'~';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 173
WS	:	(' '|'\t'|
		(('\n'| '\r')  { /*newline();*/}   )
		)+ { skip(); };

