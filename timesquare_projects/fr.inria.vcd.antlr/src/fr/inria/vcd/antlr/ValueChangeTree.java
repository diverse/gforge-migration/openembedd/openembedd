/*
 * 
 * @author : Anthony Gaudino
 * 
 *    
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.vcd.model.BinaryVectorValueChange;
import fr.inria.vcd.model.RealVectorValueChange;
import fr.inria.vcd.model.ScalarValueChange;
import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.command.SimulationCommand;
import fr.inria.vcd.model.keyword.SimulationKeyword;

public class ValueChangeTree extends CommonTree {
	private SimulationCommand command;
	
	public ValueChangeTree(Token arg0) {
		super(arg0);
		String text = arg0.getText().substring(1);
		if(arg0.getType()==VcdLexer.SIMULATION_TIME ) {
			int time = Integer.parseInt(text);
			command = new SimulationCommand(time);
		} else {
			SimulationKeyword keyword = SimulationKeyword.valueOf(text);
			command = new SimulationCommand(keyword);
		}

	}
	public SimulationCommand getCommand() {
		return this.command;
	}
	
	@Override
	public void addChild(Tree arg0) {
		if(arg0.isNil()) return;	
		String text;
		ValueChange vc = null;
		switch(arg0.getType()) {
		case VcdLexer.SCALAR_VALUE_CHANGE:
			text = arg0.getText(); 
			vc = new ScalarValueChange(text.charAt(0), text.substring(1));
			break;
		case VcdLexer.RVECTOR:
			assert(arg0.getChild(0).getType() == VcdLexer.REAL_VECTOR);
			text = arg0.getChild(0).getText();
			vc = new RealVectorValueChange(text.substring(1), 
					  					arg0.getChild(1).getText());
			break;
		case VcdLexer.BVECTOR:
			assert(arg0.getChild(0).getType() == VcdLexer.BINARY_VECTOR);
			text = arg0.getChild(0).getText();
			vc = new BinaryVectorValueChange(text.substring(1), 
					  						arg0.getChild(1).getText()); 
			break;
		case VcdLexer.DUMPALL:
		case VcdLexer.DUMPOFF:
		case VcdLexer.DUMPON:
		case VcdLexer.DUMPVARS:
			assert(arg0 instanceof ValueChangeTree);
			ValueChangeTree child = (ValueChangeTree)arg0;
			
			command.merge(child.command);
			
			break;
		default:
				return;
		}
		command.addValueChange(vc);
	}
}
