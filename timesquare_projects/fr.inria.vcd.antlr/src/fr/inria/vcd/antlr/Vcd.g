grammar Vcd;

options {
  output = AST;
  ASTLabelType = CommonTree;
}

tokens { RVECTOR; BVECTOR; }

@parser::header {package fr.inria.vcd.antlr;}

@lexer::header {package fr.inria.vcd.antlr;} 


@lexer::members {
int time=-1;
ArrayList<Commentary> acm=new ArrayList<Commentary>();
Uposition up=null;
       
public int getTime()
{
	return time;  
}

public ArrayList<Commentary> getAcm() 
{
	return acm;
}
public int stringtime(String s)
{
	time=Integer.parseInt(s);
	up = new Uposition(time,Uposition.Enumblocktype.SimulationTime );
	return time;
}

public	int addcomment(String s)
{
	//System.out.println(time+   " " +s );
	acm.add(new Commentary(s,up));
	return 0;
}
	
} 




vcd_definitions
	:	vcd_declarations   simulation_command *;

vcd_declarations
	:	vcd_declaration_date vcd_declaration_version vcd_declaration_timescale? declaration_command* vcd_declaration_enddefinitions ;
	
simulation_command
	:	//vcd_declaration_comment!
	//|	
	SIMULATION_TIME^ (vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change)*;

vcd_simulation_dumpall
	:	DUMPALL^ value_change* END;
vcd_simulation_dumpvars
	:	DUMPVARS^ value_change* END;
vcd_simulation_dumpon
	:	DUMPON^ value_change* END;
vcd_simulation_dumpoff
	:	DUMPOFF^ value_change* END;
	
declaration_command
	:	vcd_declaration_scope^ declaration_command* vcd_declaration_upscope!
	//|	vcd_declaration_comment!
	|	vcd_declaration_vars;

vcd_declaration_enddefinitions
	:	ENDDEFINITIONS^ END;

//vcd_declaration_comment
//	:	COMMENT^ ;//.* END;
	
vcd_declaration_date
	:	DATE^ .+ END;

vcd_declaration_scope
	:	SCOPE^ SCOPE_TYPE IDENT END!;

vcd_declaration_timescale
	:	TIMESCALE^ DECIMAL_NUMBER TIME_UNIT END;

vcd_declaration_upscope
	:	UPSCOPE^ END!;

vcd_declaration_vars
	:	VAR^ VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE (REFERENCE|IDENT) END;	

vcd_declaration_version
	:	VERSION^ (~END )* END;

value_change
	: real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE;

real_vector_value_change
	:	REAL_VECTOR IDENTIFIER_CODE -> ^(RVECTOR REAL_VECTOR IDENTIFIER_CODE);

bin_vector_value_change
	:	BINARY_VECTOR IDENTIFIER_CODE -> ^(BVECTOR BINARY_VECTOR IDENTIFIER_CODE);

SIMULATION_TIME
	:	'#' DECIMAL_NUMBER
		{ String s=getText();
		 stringtime(new String (s.substring(1))); }
		; //TODO time
	
VAR_TYPE	:	'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1'
		|	'tri'   | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor';
		
SCOPE_TYPE 	:	'begin' | 'fork' | 'function' |	'module' | 'task';
	
TIME_UNIT  	:	's' | 'ms' | 'us' | 'ns' | 'ps'	| 'fs';

REFERENCE
	:	IDENT '[' DECIMAL_NUMBER ']' 
	|	IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']';
	
ENDDEFINITIONS
	:	'$enddefinitions' {  up=new Uposition( 0,Uposition.Enumblocktype.EndDef  )  ;  };
	
// status  constraint
COMMENT	:
	'$comment' .* '$end' 
	 {  
	 String s=getText();	
	 addcomment(new String ( s.substring(8, s.length()-4)));
	 }
	 { skip();
	 };
	 


	 
DATE	:	'$date'  	{  up=new Uposition( 0,Uposition.Enumblocktype.Date  )  ;  };
SCOPE   :	'$scope' 	{  up=new Uposition( 0,Uposition.Enumblocktype.Scope  )  ;  };
TIMESCALE:	'$timescale'  	{  up=new Uposition( 0,Uposition.Enumblocktype.TimeScale  )  ;  };
UPSCOPE	:	'$upscope'    	{  up=new Uposition( 0,Uposition.Enumblocktype.UpScope  )  ;  };
END	:	'$end'		{ if (up!=null) up.setTheend(true);  };
VAR	:	'$var'		{  up=new Uposition( 0,Uposition.Enumblocktype.Var  )  ;  };
VERSION	:	'$version'	{  up=new Uposition( 0,Uposition.Enumblocktype.Version  )  ;  };
DUMPALL	:	'$dumpall'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpAll  )  ;  };
DUMPON	:	'$dumpon'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOn  )  ;  };
DUMPOFF	:	'$dumpoff'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOff  )  ;  };
DUMPVARS:	'$dumpvars'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpVars  )  ;  };
	
SCALAR_VALUE_CHANGE
	:	(BIT|VALUE) IDENTIFIER_CODE;

DECIMAL_NUMBER
	:	'-'? (BIT|DIGIT)+;
REAL_VECTOR 
	:	('r'|'R') '-'? (BIT|DIGIT)* '.' (BIT|DIGIT)+; 

BINARY_VECTOR	
	:	('b'|'B') (BIT|VALUE)+;
IDENT	:	(LETTER|'_') (LETTER|DIGIT|BIT|'_'|'@'|'.')*;
VERSION_TEXT
	:	(LETTER|DIGIT|BIT) (LETTER|DIGIT|BIT|IDENT_CHAR|'.')+;
IDENTIFIER_CODE
	:	IDENT_CHAR (DIGIT | BIT | LETTER | IDENT_CHAR |'.' |'_')*;
DIGIT   :	'2'..'9';
VALUE	:	'x' | 'X' | 'z' | 'Z';
BIT 	:	 '0' | '1';
LETTER	:	'a'..'z' | 'A'..'Z';
fragment
IDENT_CHAR  :	 '!'..'-' | '/' | ':'..'@' | '['..'^' | '`' | '{'..'~';
	
WS	:	(' '|'\t'|
		(('\n'| '\r')  { /*newline();*/}   )
		)+ { skip(); };

