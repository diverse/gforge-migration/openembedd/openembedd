/*
 * 
 * @author : Anthony Gaudino
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.vcd.antlr.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import fr.inria.base.MyManager;
import fr.inria.vcd.antlr.view.VcdDiagramTest;

public class VCDViewerAction implements IObjectActionDelegate {

	/**
	 * Constructor for Action1.
	 */
	public VCDViewerAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		Shell shell = new Shell();
		try {
			new VcdDiagramTest().read(file.getContents(), file.getName());
		} catch (Exception ex) {
			MyManager.printError(ex);	
			MessageDialog.openError(shell, "Error in VCD viewer", ex.toString());
		}
	}
	private IFile file;

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	
	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof TreeSelection)) return;
		TreeSelection ts = (TreeSelection)selection;
		myLoad(ts.getFirstElement());
	}
	private void myLoad(Object o) {		
		if(!(o instanceof IFile)) return;
		this.file = (IFile)o;
	}
}
