// $ANTLR 3.0.1 D:\\SHELLEY\\Antlr\\Vcd.g 2009-01-15 14:13:09
package fr.inria.vcd.antlr;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

@SuppressWarnings("unused")

public class VcdParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RVECTOR", "BVECTOR", "SIMULATION_TIME", "DUMPALL", "END", "DUMPVARS", "DUMPON", "DUMPOFF", "ENDDEFINITIONS", "DATE", "SCOPE", "SCOPE_TYPE", "IDENT", "TIMESCALE", "DECIMAL_NUMBER", "TIME_UNIT", "UPSCOPE", "VAR", "VAR_TYPE", "IDENTIFIER_CODE", "REFERENCE", "VERSION", "SCALAR_VALUE_CHANGE", "REAL_VECTOR", "BINARY_VECTOR", "COMMENT", "BIT", "VALUE", "DIGIT", "LETTER", "IDENT_CHAR", "VERSION_TEXT", "WS"
    };
    public static final int REAL_VECTOR=27;
    public static final int VERSION_TEXT=35;
    public static final int LETTER=33;
    public static final int UPSCOPE=20;
    public static final int IDENT_CHAR=34;
    public static final int VAR_TYPE=22;
    public static final int REFERENCE=24;
    public static final int VERSION=25;
    public static final int VALUE=31;
    public static final int EOF=-1;
    public static final int TIME_UNIT=19;
    public static final int SCALAR_VALUE_CHANGE=26;
    public static final int DUMPOFF=11;
    public static final int ENDDEFINITIONS=12;
    public static final int SIMULATION_TIME=6;
    public static final int DUMPON=10;
    public static final int WS=36;
    public static final int SCOPE=14;
    public static final int BVECTOR=5;
    public static final int DUMPALL=7;
    public static final int DUMPVARS=9;
    public static final int TIMESCALE=17;
    public static final int IDENTIFIER_CODE=23;
    public static final int RVECTOR=4;
    public static final int IDENT=16;
    public static final int BIT=30;
    public static final int VAR=21;
    public static final int DIGIT=32;
    public static final int COMMENT=29;
    public static final int BINARY_VECTOR=28;
    public static final int END=8;
    public static final int DATE=13;
    public static final int SCOPE_TYPE=15;
    public static final int DECIMAL_NUMBER=18;

        public VcdParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();
    
    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    @Override
	public String[] getTokenNames() { return tokenNames; }
    @Override
	public String getGrammarFileName() { return "D:\\SHELLEY\\Antlr\\Vcd.g"; }


    public static class vcd_definitions_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_definitions
    // D:\\SHELLEY\\Antlr\\Vcd.g:48:1: vcd_definitions : vcd_declarations ( simulation_command )* ;
    public final vcd_definitions_return vcd_definitions() throws RecognitionException {
        vcd_definitions_return retval = new vcd_definitions_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        vcd_declarations_return vcd_declarations1 = null;

        simulation_command_return simulation_command2 = null;

        

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:49:2: ( vcd_declarations ( simulation_command )* )
            // D:\\SHELLEY\\Antlr\\Vcd.g:49:4: vcd_declarations ( simulation_command )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_vcd_declarations_in_vcd_definitions78);
            vcd_declarations1=vcd_declarations();
            _fsp--;
            
            adaptor.addChild(root_0, vcd_declarations1.getTree());
            // D:\\SHELLEY\\Antlr\\Vcd.g:49:23: ( simulation_command )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==SIMULATION_TIME) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:49:23: simulation_command
            	    {
            	    pushFollow(FOLLOW_simulation_command_in_vcd_definitions82);
            	    simulation_command2=simulation_command();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, simulation_command2.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_definitions

    public static class vcd_declarations_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declarations
    // D:\\SHELLEY\\Antlr\\Vcd.g:51:1: vcd_declarations : vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command )* vcd_declaration_enddefinitions ;
    public final vcd_declarations_return vcd_declarations() throws RecognitionException {
        vcd_declarations_return retval = new vcd_declarations_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        vcd_declaration_date_return vcd_declaration_date3 = null;

        vcd_declaration_version_return vcd_declaration_version4 = null;

        vcd_declaration_timescale_return vcd_declaration_timescale5 = null;

        declaration_command_return declaration_command6 = null;

        vcd_declaration_enddefinitions_return vcd_declaration_enddefinitions7 = null;

        

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:52:2: ( vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command )* vcd_declaration_enddefinitions )
            // D:\\SHELLEY\\Antlr\\Vcd.g:52:4: vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command )* vcd_declaration_enddefinitions
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_vcd_declaration_date_in_vcd_declarations93);
            vcd_declaration_date3=vcd_declaration_date();
            _fsp--;
            
            adaptor.addChild(root_0, vcd_declaration_date3.getTree());
            pushFollow(FOLLOW_vcd_declaration_version_in_vcd_declarations95);
            vcd_declaration_version4=vcd_declaration_version();
            _fsp--;
            
            adaptor.addChild(root_0, vcd_declaration_version4.getTree());
            // D:\\SHELLEY\\Antlr\\Vcd.g:52:49: ( vcd_declaration_timescale )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==TIMESCALE) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:52:49: vcd_declaration_timescale
                    {
                    pushFollow(FOLLOW_vcd_declaration_timescale_in_vcd_declarations97);
                    vcd_declaration_timescale5=vcd_declaration_timescale();
                    _fsp--;
                    
                    adaptor.addChild(root_0, vcd_declaration_timescale5.getTree());

                    }
                    break;

            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:52:76: ( declaration_command )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==SCOPE||LA3_0==VAR) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:52:76: declaration_command
            	    {
            	    pushFollow(FOLLOW_declaration_command_in_vcd_declarations100);
            	    declaration_command6=declaration_command();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, declaration_command6.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            pushFollow(FOLLOW_vcd_declaration_enddefinitions_in_vcd_declarations103);
            vcd_declaration_enddefinitions7=vcd_declaration_enddefinitions();
            _fsp--;
            
            adaptor.addChild(root_0, vcd_declaration_enddefinitions7.getTree());

            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declarations

    public static class simulation_command_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start simulation_command
    // D:\\SHELLEY\\Antlr\\Vcd.g:54:1: simulation_command : SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change )* ;
    public final simulation_command_return simulation_command() throws RecognitionException {
        simulation_command_return retval = new simulation_command_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token SIMULATION_TIME8=null;
        vcd_simulation_dumpall_return vcd_simulation_dumpall9 = null;

        vcd_simulation_dumpvars_return vcd_simulation_dumpvars10 = null;

        vcd_simulation_dumpon_return vcd_simulation_dumpon11 = null;

        vcd_simulation_dumpoff_return vcd_simulation_dumpoff12 = null;

        declaration_command_return declaration_command13 = null;

        value_change_return value_change14 = null;

        
        CommonTree SIMULATION_TIME8_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:55:2: ( SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change )* )
            // D:\\SHELLEY\\Antlr\\Vcd.g:57:2: SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change )*
            {
            root_0 = (CommonTree)adaptor.nil();

            SIMULATION_TIME8=(Token)input.LT(1);
            match(input,SIMULATION_TIME,FOLLOW_SIMULATION_TIME_in_simulation_command118); 
            SIMULATION_TIME8_tree = (CommonTree)adaptor.create(SIMULATION_TIME8);
            root_0 = (CommonTree)adaptor.becomeRoot(SIMULATION_TIME8_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:57:19: ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change )*
            loop4:
            do {
                int alt4=7;
                switch ( input.LA(1) ) {
                case DUMPALL:
                    {
                    alt4=1;
                    }
                    break;
                case DUMPVARS:
                    {
                    alt4=2;
                    }
                    break;
                case DUMPON:
                    {
                    alt4=3;
                    }
                    break;
                case DUMPOFF:
                    {
                    alt4=4;
                    }
                    break;
                case SCOPE:
                case VAR:
                    {
                    alt4=5;
                    }
                    break;
                case SCALAR_VALUE_CHANGE:
                case REAL_VECTOR:
                case BINARY_VECTOR:
                    {
                    alt4=6;
                    }
                    break;

                }

                switch (alt4) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:20: vcd_simulation_dumpall
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpall_in_simulation_command122);
            	    vcd_simulation_dumpall9=vcd_simulation_dumpall();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, vcd_simulation_dumpall9.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:45: vcd_simulation_dumpvars
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpvars_in_simulation_command126);
            	    vcd_simulation_dumpvars10=vcd_simulation_dumpvars();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, vcd_simulation_dumpvars10.getTree());

            	    }
            	    break;
            	case 3 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:71: vcd_simulation_dumpon
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpon_in_simulation_command130);
            	    vcd_simulation_dumpon11=vcd_simulation_dumpon();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, vcd_simulation_dumpon11.getTree());

            	    }
            	    break;
            	case 4 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:95: vcd_simulation_dumpoff
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpoff_in_simulation_command134);
            	    vcd_simulation_dumpoff12=vcd_simulation_dumpoff();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, vcd_simulation_dumpoff12.getTree());

            	    }
            	    break;
            	case 5 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:120: declaration_command
            	    {
            	    pushFollow(FOLLOW_declaration_command_in_simulation_command138);
            	    declaration_command13=declaration_command();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, declaration_command13.getTree());

            	    }
            	    break;
            	case 6 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:57:142: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_simulation_command142);
            	    value_change14=value_change();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, value_change14.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end simulation_command

    public static class vcd_simulation_dumpall_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_simulation_dumpall
    // D:\\SHELLEY\\Antlr\\Vcd.g:59:1: vcd_simulation_dumpall : DUMPALL ( value_change )* END ;
    public final vcd_simulation_dumpall_return vcd_simulation_dumpall() throws RecognitionException {
        vcd_simulation_dumpall_return retval = new vcd_simulation_dumpall_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token DUMPALL15=null;
        Token END17=null;
        value_change_return value_change16 = null;

        
        CommonTree DUMPALL15_tree=null;
        CommonTree END17_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:60:2: ( DUMPALL ( value_change )* END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:60:4: DUMPALL ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPALL15=(Token)input.LT(1);
            match(input,DUMPALL,FOLLOW_DUMPALL_in_vcd_simulation_dumpall153); 
            DUMPALL15_tree = (CommonTree)adaptor.create(DUMPALL15);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPALL15_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:60:13: ( value_change )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=SCALAR_VALUE_CHANGE && LA5_0<=BINARY_VECTOR)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:60:13: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpall156);
            	    value_change16=value_change();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, value_change16.getTree());

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            END17=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_simulation_dumpall159); 
            END17_tree = (CommonTree)adaptor.create(END17);
            adaptor.addChild(root_0, END17_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_simulation_dumpall

    public static class vcd_simulation_dumpvars_return extends ParserRuleReturnScope {
        CommonTree tree;
      
		@Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_simulation_dumpvars
    // D:\\SHELLEY\\Antlr\\Vcd.g:61:1: vcd_simulation_dumpvars : DUMPVARS ( value_change )* END ;
    public final vcd_simulation_dumpvars_return vcd_simulation_dumpvars() throws RecognitionException {
        vcd_simulation_dumpvars_return retval = new vcd_simulation_dumpvars_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token DUMPVARS18=null;
        Token END20=null;
        value_change_return value_change19 = null;

        
        CommonTree DUMPVARS18_tree=null;
        CommonTree END20_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:62:2: ( DUMPVARS ( value_change )* END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:62:4: DUMPVARS ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPVARS18=(Token)input.LT(1);
            match(input,DUMPVARS,FOLLOW_DUMPVARS_in_vcd_simulation_dumpvars167); 
            DUMPVARS18_tree = (CommonTree)adaptor.create(DUMPVARS18);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPVARS18_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:62:14: ( value_change )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=SCALAR_VALUE_CHANGE && LA6_0<=BINARY_VECTOR)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:62:14: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpvars170);
            	    value_change19=value_change();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, value_change19.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            END20=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_simulation_dumpvars173); 
            END20_tree = (CommonTree)adaptor.create(END20);
            adaptor.addChild(root_0, END20_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_simulation_dumpvars

    public static class vcd_simulation_dumpon_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_simulation_dumpon
    // D:\\SHELLEY\\Antlr\\Vcd.g:63:1: vcd_simulation_dumpon : DUMPON ( value_change )* END ;
    public final vcd_simulation_dumpon_return vcd_simulation_dumpon() throws RecognitionException {
        vcd_simulation_dumpon_return retval = new vcd_simulation_dumpon_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token DUMPON21=null;
        Token END23=null;
        value_change_return value_change22 = null;

        
        CommonTree DUMPON21_tree=null;
        CommonTree END23_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:64:2: ( DUMPON ( value_change )* END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:64:4: DUMPON ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPON21=(Token)input.LT(1);
            match(input,DUMPON,FOLLOW_DUMPON_in_vcd_simulation_dumpon181); 
            DUMPON21_tree = (CommonTree)adaptor.create(DUMPON21);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPON21_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:64:12: ( value_change )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=SCALAR_VALUE_CHANGE && LA7_0<=BINARY_VECTOR)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:64:12: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpon184);
            	    value_change22=value_change();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, value_change22.getTree());

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            END23=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_simulation_dumpon187); 
            END23_tree = (CommonTree)adaptor.create(END23);
            adaptor.addChild(root_0, END23_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_simulation_dumpon

    public static class vcd_simulation_dumpoff_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_simulation_dumpoff
    // D:\\SHELLEY\\Antlr\\Vcd.g:65:1: vcd_simulation_dumpoff : DUMPOFF ( value_change )* END ;
    public final vcd_simulation_dumpoff_return vcd_simulation_dumpoff() throws RecognitionException {
        vcd_simulation_dumpoff_return retval = new vcd_simulation_dumpoff_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token DUMPOFF24=null;
        Token END26=null;
        value_change_return value_change25 = null;

        
        CommonTree DUMPOFF24_tree=null;
        CommonTree END26_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:66:2: ( DUMPOFF ( value_change )* END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:66:4: DUMPOFF ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPOFF24=(Token)input.LT(1);
            match(input,DUMPOFF,FOLLOW_DUMPOFF_in_vcd_simulation_dumpoff195); 
            DUMPOFF24_tree = (CommonTree)adaptor.create(DUMPOFF24);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPOFF24_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:66:13: ( value_change )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=SCALAR_VALUE_CHANGE && LA8_0<=BINARY_VECTOR)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:66:13: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpoff198);
            	    value_change25=value_change();
            	    _fsp--;
            	    
            	    adaptor.addChild(root_0, value_change25.getTree());

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            END26=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_simulation_dumpoff201); 
            END26_tree = (CommonTree)adaptor.create(END26);
            adaptor.addChild(root_0, END26_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_simulation_dumpoff

    public static class declaration_command_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start declaration_command
    // D:\\SHELLEY\\Antlr\\Vcd.g:68:1: declaration_command : ( vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope | vcd_declaration_vars );
    public final declaration_command_return declaration_command() throws RecognitionException {
        declaration_command_return retval = new declaration_command_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        vcd_declaration_scope_return vcd_declaration_scope27 = null;

        declaration_command_return declaration_command28 = null;

        vcd_declaration_upscope_return vcd_declaration_upscope29 = null;

        vcd_declaration_vars_return vcd_declaration_vars30 = null;

        

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:69:2: ( vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope | vcd_declaration_vars )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==SCOPE) ) {
                alt10=1;
            }
            else if ( (LA10_0==VAR) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("68:1: declaration_command : ( vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope | vcd_declaration_vars );", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:69:4: vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_vcd_declaration_scope_in_declaration_command211);
                    vcd_declaration_scope27=vcd_declaration_scope();
                    _fsp--;
                    
                    root_0 = (CommonTree)adaptor.becomeRoot(vcd_declaration_scope27.getTree(), root_0);
                    // D:\\SHELLEY\\Antlr\\Vcd.g:69:27: ( declaration_command )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==SCOPE||LA9_0==VAR) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // D:\\SHELLEY\\Antlr\\Vcd.g:69:27: declaration_command
                    	    {
                    	    pushFollow(FOLLOW_declaration_command_in_declaration_command214);
                    	    declaration_command28=declaration_command();
                    	    _fsp--;
                    	    
                    	    adaptor.addChild(root_0, declaration_command28.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    pushFollow(FOLLOW_vcd_declaration_upscope_in_declaration_command217);
                    vcd_declaration_upscope29=vcd_declaration_upscope();
                    _fsp--;


                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:71:4: vcd_declaration_vars
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_vcd_declaration_vars_in_declaration_command225);
                    vcd_declaration_vars30=vcd_declaration_vars();
                    _fsp--;
                    
                    adaptor.addChild(root_0, vcd_declaration_vars30.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end declaration_command

    public static class vcd_declaration_enddefinitions_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_enddefinitions
    // D:\\SHELLEY\\Antlr\\Vcd.g:73:1: vcd_declaration_enddefinitions : ENDDEFINITIONS END ;
    public final vcd_declaration_enddefinitions_return vcd_declaration_enddefinitions() throws RecognitionException {
        vcd_declaration_enddefinitions_return retval = new vcd_declaration_enddefinitions_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token ENDDEFINITIONS31=null;
        Token END32=null;
        
        CommonTree ENDDEFINITIONS31_tree=null;
        CommonTree END32_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:74:2: ( ENDDEFINITIONS END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:74:4: ENDDEFINITIONS END
            {
            root_0 = (CommonTree)adaptor.nil();

            ENDDEFINITIONS31=(Token)input.LT(1);
            match(input,ENDDEFINITIONS,FOLLOW_ENDDEFINITIONS_in_vcd_declaration_enddefinitions234); 
            ENDDEFINITIONS31_tree = (CommonTree)adaptor.create(ENDDEFINITIONS31);
            root_0 = (CommonTree)adaptor.becomeRoot(ENDDEFINITIONS31_tree, root_0);

            END32=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_enddefinitions237); 
            END32_tree = (CommonTree)adaptor.create(END32);
            adaptor.addChild(root_0, END32_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_enddefinitions

    public static class vcd_declaration_date_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_date
    // D:\\SHELLEY\\Antlr\\Vcd.g:79:1: vcd_declaration_date : DATE ( . )+ END ;
    public final vcd_declaration_date_return vcd_declaration_date() throws RecognitionException {
        vcd_declaration_date_return retval = new vcd_declaration_date_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token DATE33=null;
        Token wildcard34=null;
        Token END35=null;
        
        CommonTree DATE33_tree=null;
        CommonTree wildcard34_tree=null;
        CommonTree END35_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:80:2: ( DATE ( . )+ END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:80:4: DATE ( . )+ END
            {
            root_0 = (CommonTree)adaptor.nil();

            DATE33=(Token)input.LT(1);
            match(input,DATE,FOLLOW_DATE_in_vcd_declaration_date250); 
            DATE33_tree = (CommonTree)adaptor.create(DATE33);
            root_0 = (CommonTree)adaptor.becomeRoot(DATE33_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:80:10: ( . )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==END) ) {
                    alt11=2;
                }
                else if ( ((LA11_0>=RVECTOR && LA11_0<=DUMPALL)||(LA11_0>=DUMPVARS && LA11_0<=WS)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:80:10: .
            	    {
            	    wildcard34=(Token)input.LT(1);
            	    matchAny(input); 
            	    wildcard34_tree = (CommonTree)adaptor.create(wildcard34);
            	    adaptor.addChild(root_0, wildcard34_tree);


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);

            END35=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_date256); 
            END35_tree = (CommonTree)adaptor.create(END35);
            adaptor.addChild(root_0, END35_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_date

    public static class vcd_declaration_scope_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_scope
    // D:\\SHELLEY\\Antlr\\Vcd.g:82:1: vcd_declaration_scope : SCOPE SCOPE_TYPE IDENT END ;
    public final vcd_declaration_scope_return vcd_declaration_scope() throws RecognitionException {
        vcd_declaration_scope_return retval = new vcd_declaration_scope_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token SCOPE36=null;
        Token SCOPE_TYPE37=null;
        Token IDENT38=null;
        Token END39=null;
        
        CommonTree SCOPE36_tree=null;
        CommonTree SCOPE_TYPE37_tree=null;
        CommonTree IDENT38_tree=null;
        CommonTree END39_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:83:2: ( SCOPE SCOPE_TYPE IDENT END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:83:4: SCOPE SCOPE_TYPE IDENT END
            {
            root_0 = (CommonTree)adaptor.nil();

            SCOPE36=(Token)input.LT(1);
            match(input,SCOPE,FOLLOW_SCOPE_in_vcd_declaration_scope265); 
            SCOPE36_tree = (CommonTree)adaptor.create(SCOPE36);
            root_0 = (CommonTree)adaptor.becomeRoot(SCOPE36_tree, root_0);

            SCOPE_TYPE37=(Token)input.LT(1);
            match(input,SCOPE_TYPE,FOLLOW_SCOPE_TYPE_in_vcd_declaration_scope268); 
            SCOPE_TYPE37_tree = (CommonTree)adaptor.create(SCOPE_TYPE37);
            adaptor.addChild(root_0, SCOPE_TYPE37_tree);

            IDENT38=(Token)input.LT(1);
            match(input,IDENT,FOLLOW_IDENT_in_vcd_declaration_scope270); 
            IDENT38_tree = (CommonTree)adaptor.create(IDENT38);
            adaptor.addChild(root_0, IDENT38_tree);

            END39=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_scope272); 

            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_scope

    public static class vcd_declaration_timescale_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_timescale
    // D:\\SHELLEY\\Antlr\\Vcd.g:85:1: vcd_declaration_timescale : TIMESCALE DECIMAL_NUMBER TIME_UNIT END ;
    public final vcd_declaration_timescale_return vcd_declaration_timescale() throws RecognitionException {
        vcd_declaration_timescale_return retval = new vcd_declaration_timescale_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token TIMESCALE40=null;
        Token DECIMAL_NUMBER41=null;
        Token TIME_UNIT42=null;
        Token END43=null;
        
        CommonTree TIMESCALE40_tree=null;
        CommonTree DECIMAL_NUMBER41_tree=null;
        CommonTree TIME_UNIT42_tree=null;
        CommonTree END43_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:86:2: ( TIMESCALE DECIMAL_NUMBER TIME_UNIT END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:86:4: TIMESCALE DECIMAL_NUMBER TIME_UNIT END
            {
            root_0 = (CommonTree)adaptor.nil();

            TIMESCALE40=(Token)input.LT(1);
            match(input,TIMESCALE,FOLLOW_TIMESCALE_in_vcd_declaration_timescale282); 
            TIMESCALE40_tree = (CommonTree)adaptor.create(TIMESCALE40);
            root_0 = (CommonTree)adaptor.becomeRoot(TIMESCALE40_tree, root_0);

            DECIMAL_NUMBER41=(Token)input.LT(1);
            match(input,DECIMAL_NUMBER,FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_timescale285); 
            DECIMAL_NUMBER41_tree = (CommonTree)adaptor.create(DECIMAL_NUMBER41);
            adaptor.addChild(root_0, DECIMAL_NUMBER41_tree);

            TIME_UNIT42=(Token)input.LT(1);
            match(input,TIME_UNIT,FOLLOW_TIME_UNIT_in_vcd_declaration_timescale287); 
            TIME_UNIT42_tree = (CommonTree)adaptor.create(TIME_UNIT42);
            adaptor.addChild(root_0, TIME_UNIT42_tree);

            END43=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_timescale289); 
            END43_tree = (CommonTree)adaptor.create(END43);
            adaptor.addChild(root_0, END43_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_timescale

    public static class vcd_declaration_upscope_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_upscope
    // D:\\SHELLEY\\Antlr\\Vcd.g:88:1: vcd_declaration_upscope : UPSCOPE END ;
    public final vcd_declaration_upscope_return vcd_declaration_upscope() throws RecognitionException {
        vcd_declaration_upscope_return retval = new vcd_declaration_upscope_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token UPSCOPE44=null;
        Token END45=null;
        
        CommonTree UPSCOPE44_tree=null;
        CommonTree END45_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:89:2: ( UPSCOPE END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:89:4: UPSCOPE END
            {
            root_0 = (CommonTree)adaptor.nil();

            UPSCOPE44=(Token)input.LT(1);
            match(input,UPSCOPE,FOLLOW_UPSCOPE_in_vcd_declaration_upscope298); 
            UPSCOPE44_tree = (CommonTree)adaptor.create(UPSCOPE44);
            root_0 = (CommonTree)adaptor.becomeRoot(UPSCOPE44_tree, root_0);

            END45=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_upscope301); 

            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_upscope

    public static class vcd_declaration_vars_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_vars
    // D:\\SHELLEY\\Antlr\\Vcd.g:91:1: vcd_declaration_vars : VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE ( REFERENCE | IDENT ) END ;
    public final vcd_declaration_vars_return vcd_declaration_vars() throws RecognitionException {
        vcd_declaration_vars_return retval = new vcd_declaration_vars_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token VAR46=null;
        Token VAR_TYPE47=null;
        Token DECIMAL_NUMBER48=null;
        Token IDENTIFIER_CODE49=null;
        Token set50=null;
        Token END51=null;
        
        CommonTree VAR46_tree=null;
        CommonTree VAR_TYPE47_tree=null;
        CommonTree DECIMAL_NUMBER48_tree=null;
        CommonTree IDENTIFIER_CODE49_tree=null;
        CommonTree set50_tree=null;
        CommonTree END51_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:92:2: ( VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE ( REFERENCE | IDENT ) END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:92:4: VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE ( REFERENCE | IDENT ) END
            {
            root_0 = (CommonTree)adaptor.nil();

            VAR46=(Token)input.LT(1);
            match(input,VAR,FOLLOW_VAR_in_vcd_declaration_vars311); 
            VAR46_tree = (CommonTree)adaptor.create(VAR46);
            root_0 = (CommonTree)adaptor.becomeRoot(VAR46_tree, root_0);

            VAR_TYPE47=(Token)input.LT(1);
            match(input,VAR_TYPE,FOLLOW_VAR_TYPE_in_vcd_declaration_vars314); 
            VAR_TYPE47_tree = (CommonTree)adaptor.create(VAR_TYPE47);
            adaptor.addChild(root_0, VAR_TYPE47_tree);

            DECIMAL_NUMBER48=(Token)input.LT(1);
            match(input,DECIMAL_NUMBER,FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_vars316); 
            DECIMAL_NUMBER48_tree = (CommonTree)adaptor.create(DECIMAL_NUMBER48);
            adaptor.addChild(root_0, DECIMAL_NUMBER48_tree);

            IDENTIFIER_CODE49=(Token)input.LT(1);
            match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_vcd_declaration_vars318); 
            IDENTIFIER_CODE49_tree = (CommonTree)adaptor.create(IDENTIFIER_CODE49);
            adaptor.addChild(root_0, IDENTIFIER_CODE49_tree);

            set50=(Token)input.LT(1);
            if ( input.LA(1)==IDENT||input.LA(1)==REFERENCE ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set50));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_vcd_declaration_vars320);    throw mse;
            }

            END51=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_vars326); 
            END51_tree = (CommonTree)adaptor.create(END51);
            adaptor.addChild(root_0, END51_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_vars

    public static class vcd_declaration_version_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start vcd_declaration_version
    // D:\\SHELLEY\\Antlr\\Vcd.g:94:1: vcd_declaration_version : VERSION (~ END )* END ;
    public final vcd_declaration_version_return vcd_declaration_version() throws RecognitionException {
        vcd_declaration_version_return retval = new vcd_declaration_version_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token VERSION52=null;
        Token set53=null;
        Token END54=null;
        
        CommonTree VERSION52_tree=null;
        CommonTree set53_tree=null;
        CommonTree END54_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:95:2: ( VERSION (~ END )* END )
            // D:\\SHELLEY\\Antlr\\Vcd.g:95:4: VERSION (~ END )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            VERSION52=(Token)input.LT(1);
            match(input,VERSION,FOLLOW_VERSION_in_vcd_declaration_version336); 
            VERSION52_tree = (CommonTree)adaptor.create(VERSION52);
            root_0 = (CommonTree)adaptor.becomeRoot(VERSION52_tree, root_0);

            // D:\\SHELLEY\\Antlr\\Vcd.g:95:13: (~ END )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=RVECTOR && LA12_0<=DUMPALL)||(LA12_0>=DUMPVARS && LA12_0<=WS)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:95:14: ~ END
            	    {
            	    set53=(Token)input.LT(1);
            	    if ( (input.LA(1)>=RVECTOR && input.LA(1)<=DUMPALL)||(input.LA(1)>=DUMPVARS && input.LA(1)<=WS) ) {
            	        input.consume();
            	        adaptor.addChild(root_0, adaptor.create(set53));
            	        errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_vcd_declaration_version340);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            END54=(Token)input.LT(1);
            match(input,END,FOLLOW_END_in_vcd_declaration_version346); 
            END54_tree = (CommonTree)adaptor.create(END54);
            adaptor.addChild(root_0, END54_tree);


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end vcd_declaration_version

    public static class value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start value_change
    // D:\\SHELLEY\\Antlr\\Vcd.g:97:1: value_change : ( real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE );
    public final value_change_return value_change() throws RecognitionException {
        value_change_return retval = new value_change_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token SCALAR_VALUE_CHANGE57=null;
        real_vector_value_change_return real_vector_value_change55 = null;

        bin_vector_value_change_return bin_vector_value_change56 = null;

        
        CommonTree SCALAR_VALUE_CHANGE57_tree=null;

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:98:2: ( real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE )
            int alt13=3;
            switch ( input.LA(1) ) {
            case REAL_VECTOR:
                {
                alt13=1;
                }
                break;
            case BINARY_VECTOR:
                {
                alt13=2;
                }
                break;
            case SCALAR_VALUE_CHANGE:
                {
                alt13=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("97:1: value_change : ( real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE );", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:98:4: real_vector_value_change
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_real_vector_value_change_in_value_change355);
                    real_vector_value_change55=real_vector_value_change();
                    _fsp--;
                    
                    adaptor.addChild(root_0, real_vector_value_change55.getTree());

                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:98:31: bin_vector_value_change
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_bin_vector_value_change_in_value_change359);
                    bin_vector_value_change56=bin_vector_value_change();
                    _fsp--;
                    
                    adaptor.addChild(root_0, bin_vector_value_change56.getTree());

                    }
                    break;
                case 3 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:98:57: SCALAR_VALUE_CHANGE
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    SCALAR_VALUE_CHANGE57=(Token)input.LT(1);
                    match(input,SCALAR_VALUE_CHANGE,FOLLOW_SCALAR_VALUE_CHANGE_in_value_change363); 
                    SCALAR_VALUE_CHANGE57_tree = (CommonTree)adaptor.create(SCALAR_VALUE_CHANGE57);
                    adaptor.addChild(root_0, SCALAR_VALUE_CHANGE57_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end value_change

    public static class real_vector_value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start real_vector_value_change
    // D:\\SHELLEY\\Antlr\\Vcd.g:100:1: real_vector_value_change : REAL_VECTOR IDENTIFIER_CODE -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE ) ;
    public final real_vector_value_change_return real_vector_value_change() throws RecognitionException {
        real_vector_value_change_return retval = new real_vector_value_change_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token REAL_VECTOR58=null;
        Token IDENTIFIER_CODE59=null;
        
        CommonTree REAL_VECTOR58_tree=null;
        CommonTree IDENTIFIER_CODE59_tree=null;
        RewriteRuleTokenStream stream_REAL_VECTOR=new RewriteRuleTokenStream(adaptor,"token REAL_VECTOR");
        RewriteRuleTokenStream stream_IDENTIFIER_CODE=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER_CODE");

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:101:2: ( REAL_VECTOR IDENTIFIER_CODE -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE ) )
            // D:\\SHELLEY\\Antlr\\Vcd.g:101:4: REAL_VECTOR IDENTIFIER_CODE
            {
            REAL_VECTOR58=(Token)input.LT(1);
            match(input,REAL_VECTOR,FOLLOW_REAL_VECTOR_in_real_vector_value_change372); 
            stream_REAL_VECTOR.add(REAL_VECTOR58);

            IDENTIFIER_CODE59=(Token)input.LT(1);
            match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_real_vector_value_change374); 
            stream_IDENTIFIER_CODE.add(IDENTIFIER_CODE59);

            
            // AST REWRITE
            // elements: IDENTIFIER_CODE, REAL_VECTOR
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);
            
            root_0 = (CommonTree)adaptor.nil();
            // 101:32: -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE )
            {
                // D:\\SHELLEY\\Antlr\\Vcd.g:101:35: ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(RVECTOR, "RVECTOR"), root_1);
                
                adaptor.addChild(root_1, stream_REAL_VECTOR.next());
                adaptor.addChild(root_1, stream_IDENTIFIER_CODE.next());
                
                adaptor.addChild(root_0, root_1);
                }
            
            }
            


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end real_vector_value_change

    public static class bin_vector_value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        @Override
		public Object getTree() { return tree; }
    };

    // $ANTLR start bin_vector_value_change
    // D:\\SHELLEY\\Antlr\\Vcd.g:103:1: bin_vector_value_change : BINARY_VECTOR IDENTIFIER_CODE -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE ) ;
    public final bin_vector_value_change_return bin_vector_value_change() throws RecognitionException {
        bin_vector_value_change_return retval = new bin_vector_value_change_return();
        retval.start = input.LT(1);
        
        CommonTree root_0 = null;

        Token BINARY_VECTOR60=null;
        Token IDENTIFIER_CODE61=null;
        
        CommonTree BINARY_VECTOR60_tree=null;
        CommonTree IDENTIFIER_CODE61_tree=null;
        RewriteRuleTokenStream stream_BINARY_VECTOR=new RewriteRuleTokenStream(adaptor,"token BINARY_VECTOR");
        RewriteRuleTokenStream stream_IDENTIFIER_CODE=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER_CODE");

        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:104:2: ( BINARY_VECTOR IDENTIFIER_CODE -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE ) )
            // D:\\SHELLEY\\Antlr\\Vcd.g:104:4: BINARY_VECTOR IDENTIFIER_CODE
            {
            BINARY_VECTOR60=(Token)input.LT(1);
            match(input,BINARY_VECTOR,FOLLOW_BINARY_VECTOR_in_bin_vector_value_change393); 
            stream_BINARY_VECTOR.add(BINARY_VECTOR60);

            IDENTIFIER_CODE61=(Token)input.LT(1);
            match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_bin_vector_value_change395); 
            stream_IDENTIFIER_CODE.add(IDENTIFIER_CODE61);

            
            // AST REWRITE
            // elements: IDENTIFIER_CODE, BINARY_VECTOR
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);
            
            root_0 = (CommonTree)adaptor.nil();
            // 104:34: -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE )
            {
                // D:\\SHELLEY\\Antlr\\Vcd.g:104:37: ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(BVECTOR, "BVECTOR"), root_1);
                
                adaptor.addChild(root_1, stream_BINARY_VECTOR.next());
                adaptor.addChild(root_1, stream_IDENTIFIER_CODE.next());
                
                adaptor.addChild(root_0, root_1);
                }
            
            }
            


            }

            retval.stop = input.LT(-1);
            
                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end bin_vector_value_change


 

    public static final BitSet FOLLOW_vcd_declarations_in_vcd_definitions78 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_simulation_command_in_vcd_definitions82 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_vcd_declaration_date_in_vcd_declarations93 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_vcd_declaration_version_in_vcd_declarations95 = new BitSet(new long[]{0x0000000000225000L});
    public static final BitSet FOLLOW_vcd_declaration_timescale_in_vcd_declarations97 = new BitSet(new long[]{0x0000000000205000L});
    public static final BitSet FOLLOW_declaration_command_in_vcd_declarations100 = new BitSet(new long[]{0x0000000000205000L});
    public static final BitSet FOLLOW_vcd_declaration_enddefinitions_in_vcd_declarations103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SIMULATION_TIME_in_simulation_command118 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpall_in_simulation_command122 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpvars_in_simulation_command126 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpon_in_simulation_command130 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpoff_in_simulation_command134 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_declaration_command_in_simulation_command138 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_value_change_in_simulation_command142 = new BitSet(new long[]{0x000000001C204E82L});
    public static final BitSet FOLLOW_DUMPALL_in_vcd_simulation_dumpall153 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpall156 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpall159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPVARS_in_vcd_simulation_dumpvars167 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpvars170 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpvars173 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPON_in_vcd_simulation_dumpon181 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpon184 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpon187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPOFF_in_vcd_simulation_dumpoff195 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpoff198 = new BitSet(new long[]{0x000000001C000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpoff201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vcd_declaration_scope_in_declaration_command211 = new BitSet(new long[]{0x0000000000304000L});
    public static final BitSet FOLLOW_declaration_command_in_declaration_command214 = new BitSet(new long[]{0x0000000000304000L});
    public static final BitSet FOLLOW_vcd_declaration_upscope_in_declaration_command217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vcd_declaration_vars_in_declaration_command225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ENDDEFINITIONS_in_vcd_declaration_enddefinitions234 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_enddefinitions237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATE_in_vcd_declaration_date250 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_date256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SCOPE_in_vcd_declaration_scope265 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_SCOPE_TYPE_in_vcd_declaration_scope268 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_IDENT_in_vcd_declaration_scope270 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_scope272 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TIMESCALE_in_vcd_declaration_timescale282 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_timescale285 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_TIME_UNIT_in_vcd_declaration_timescale287 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_timescale289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_UPSCOPE_in_vcd_declaration_upscope298 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_upscope301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_vcd_declaration_vars311 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_VAR_TYPE_in_vcd_declaration_vars314 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_vars316 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_vcd_declaration_vars318 = new BitSet(new long[]{0x0000000001010000L});
    public static final BitSet FOLLOW_set_in_vcd_declaration_vars320 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_vars326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VERSION_in_vcd_declaration_version336 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_set_in_vcd_declaration_version340 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_version346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_real_vector_value_change_in_value_change355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_bin_vector_value_change_in_value_change359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SCALAR_VALUE_CHANGE_in_value_change363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REAL_VECTOR_in_real_vector_value_change372 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_real_vector_value_change374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BINARY_VECTOR_in_bin_vector_value_change393 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_bin_vector_value_change395 = new BitSet(new long[]{0x0000000000000002L});

}