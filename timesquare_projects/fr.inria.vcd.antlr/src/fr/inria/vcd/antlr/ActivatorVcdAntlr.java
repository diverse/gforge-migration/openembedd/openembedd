/*
 * 
 * @author : Anthony Gaudino
 * 
 *  *  
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;



public class ActivatorVcdAntlr extends AbstractUIPlugin {

	public ActivatorVcdAntlr() {
		
	}
	
	
	
	
	public static final String PLUGIN_ID = "fr.inria.vcd.antlr";

	// The shared instance
	private static ActivatorVcdAntlr plugin;
	
	

	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ActivatorVcdAntlr getDefault() {
		return plugin;
	}

}
