/*
 * 
 * @author : Anthony Gaudino
 * 		Update Benoit Ferrero
 *   
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */



package fr.inria.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.Tree;

import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.command.DateCommand;
import fr.inria.vcd.model.command.ScopeCommand;
import fr.inria.vcd.model.command.TimeScaleCommand;
import fr.inria.vcd.model.command.VersionCommand;
import fr.inria.vcd.model.keyword.ScopeType;

public class VcdTreeAdaptor extends CommonTreeAdaptor {
	private int state = 0;
	//private boolean commentState = false;
	private VCDDefinitions vcd = new VCDDefinitions();
	private int level;
	private Tree tree;
	private int time = -1;
	private int pulse = -1;

	public int convertSimulationDate(Token arg0 )
	{
		try
		{
		return Integer.valueOf(arg0.getText().substring(1));
		}
		catch (Exception ex) {
		return 0;
		}
	}
	
	@Override
	public Tree create(Token arg0) {
		if (arg0 == null)
			return (Tree) super.create(arg0);
		switch (state) {
		case 0:
			state = arg0.getType();
			if (arg0.getType() == VcdLexer.SIMULATION_TIME  ) {
				vcd.setOffset(convertSimulationDate(arg0));
				ValueChangeTree vct = new ValueChangeTree(arg0);
				vcd.addSimulation(vct.getCommand());
				return vct;
			}
			if (arg0.getType() == VcdLexer.SCOPE) {
				level = 1;
				return tree = new ScopeTree(arg0);
			}
			if (arg0.getType() == VcdLexer.VAR)
				return tree = new VarTree(arg0);
			return tree = (Tree) super.create(arg0);
		case VcdLexer.DATE:
			if (arg0.getType() == VcdLexer.END) {
				StringBuffer buf = new StringBuffer();
				for (int i = 0; i < tree.getChildCount(); i++) {
					if (i > 0)
						buf.append(' ');
					buf.append(tree.getChild(i).getText());
				}
				vcd.addDefinition(new DateCommand(buf.toString()));
				state = 0;
			}
			break;
		case VcdLexer.VERSION:
			if (arg0.getType() == VcdLexer.END) {
				vcd.addDefinition(new VersionCommand(tree.getChild(0)
						.getText(), tree.getChild(1).getText()));
				state = 0;
			}
			break;
		case VcdLexer.TIMESCALE:
			if (arg0.getType() == VcdLexer.END) {
				vcd.addDefinition(new TimeScaleCommand(tree.getChild(0)
						.getText(), tree.getChild(1).getText()));
				state = 0;
			}
			break;
		case VcdLexer.SCOPE:
			if (arg0.getType() == VcdLexer.VAR)
				return new VarTree(arg0);
			if (arg0.getType() == VcdLexer.SCOPE) {
				level++;
				return new ScopeTree(arg0);
			}
			if (arg0.getType() == VcdLexer.UPSCOPE) {
				level--;
				if (level == 0) {
					state = 0;
					vcd.setScope(((ScopeTree) tree).getScope());
				}
			}
			break;
		case VcdLexer.SIMULATION_TIME:
			if (arg0.getType() == VcdLexer.SIMULATION_TIME) {
				if (time != -1) {
					int newtime= convertSimulationDate(arg0);
					if (pulse == -1)
						pulse = newtime - time;
					else if ( newtime - time < pulse)
						pulse = newtime- time;
				}
			//	vcd.setPulse(pulse);
				time = Integer.valueOf(arg0.getText().substring(1));
				ValueChangeTree vct = new ValueChangeTree(arg0);
				vcd.addSimulation(vct.getCommand());
				return vct;
			}
			if (arg0.getType() == VcdLexer.DUMPVARS
					|| arg0.getType() == VcdLexer.DUMPON
					|| arg0.getType() == VcdLexer.DUMPOFF
					|| arg0.getType() == VcdLexer.DUMPALL)
			{
				state = arg0.getType();		
			//	ValueChangeTree vct = new ValueChangeTree(arg0);
		//		vcd.setPulse(pulse);
				//vcd.addSimulation(vct.getCommand());
				return   new ValueChangeTree(arg0);
			}
			/*if(arg0.getType() == VcdLexer.COMMENT){
				state = arg0.getType();
				//commentState = true;
				return tree = (Tree) super.create(arg0);
			}*/
			break;
		case VcdLexer.DUMPALL:
		case VcdLexer.DUMPOFF: 
		case VcdLexer.DUMPON:
		case VcdLexer.DUMPVARS:
			if (arg0.getType() == VcdLexer.END) {
				this.state = VcdLexer.SIMULATION_TIME;
			}
			break;
		case VcdLexer.VAR:
			if (arg0.getType() == VcdLexer.END) {
				state = 0;
				if (scope == null) {
					scope = new ScopeCommand(ScopeType.module, null);
					vcd.setScope(scope);
				}
				scope.addChild(((VarTree) tree).getVar());
			}
			return (Tree) super.create(arg0);
		case VcdLexer.COMMENT:
		//	commentToken(arg0);
			
			break;
		default:		
			if (arg0.getType() == VcdLexer.END) {
				this.state = 0;
			}
		}
		return (Tree) super.create(arg0);
	}
	
	private ScopeCommand scope = null; // only used when no scope is defined

	public VCDDefinitions getVcd() {
		return vcd;
	}
	
	
	
	
	
}
