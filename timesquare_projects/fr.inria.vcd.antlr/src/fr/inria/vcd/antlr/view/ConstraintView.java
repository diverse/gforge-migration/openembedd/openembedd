package fr.inria.vcd.antlr.view;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.part.ViewPart;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.vcd.model.IDeclarationCommand;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

/**
 * This class/view allowed to list, display or hide the constraints in the
 * selected VCD
 * 
 * @author Xavier Seassau
 */
public class ConstraintView extends ViewPart {
	static private ConstraintView courant;
	static private ArrayList<IDeclarationCommand> localConstraint = null;
	private Composite parents = null;
	private Table table = null;
	private int size=0;

	public ConstraintView() {
		courant = this;
	}

	@Override
	public void createPartControl(Composite parent) {
		parents = parent;
		// create a Table to store and show data
		table = new Table(parent, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION| SWT.CHECK);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		table.setLayoutData(data);
		table.setHeaderVisible(true);
		TableColumn column = null;
		// add column with check box
		column = new TableColumn(table, SWT.CENTER, 0);
		column.setText("");
		column.setMoveable(false);
		column.setWidth(25);
		column.setResizable(false);
		// add column with Constraint Name
		column = new TableColumn(table, SWT.LEFT, 1);
		column.setText("Constraint Name");
		column.setMoveable(false);
		column.setWidth(200);
		table.addMouseListener(new mouseListener(table));
		draw();
	}
	
	private  class mouseListener implements MouseListener
	{
		private Table table;
		private ConstraintCommentCommand data=null;
		public mouseListener(Table table)
		{
			this.table=table;
		}

		public void mouseDown(MouseEvent e) 
		{
			//if it's a left click
			if(e.button==1)
			{
				TableItem tableItem=table.getItem(new Point(e.x,e.y));
				if (tableItem==null)
					return;
				data=(ConstraintCommentCommand)tableItem.getData();
				if(data==null)
				{
					MyManager.printlnError("ERROR DATA NULL");
					return;
				}
				if ( data.getIc()==null)
				{
					MyManager.printlnError("--> IC are  NULL :  ");
					return;
				}
				if(tableItem.getChecked()==true)
				{
					if(data.getIc().getIsConstraintVisible()==false)
					{
						int ret=data.getIc().drawConstraint();
						if(ret==-1)
						{
							tableItem.setChecked(false);
							MyManager.printlnError("ERROR : one or more cloks are hide");
						}
					}
				}
				else if(tableItem.getChecked()==false)
				{
					if(data.getIc().getIsConstraintVisible())
					{
						data.getIc().drawConstraint();
						data.getIc().setIsConstraintVisible(false);
					}				
				}
			}
		}

		public void mouseUp(MouseEvent e) 
		{
		}

		public void mouseDoubleClick(MouseEvent e) 
		{
		}
	}

	@Override
	public void setFocus() {
		if (table==null)
			return ;
		for( int i=0;i<table.getItemCount();i++)
		{
			TableItem tableItem=table.getItem(i);
			ConstraintCommentCommand data=(ConstraintCommentCommand) tableItem.getData();
			if (data!=null)
			if (data.getIc()!=null)
				tableItem.setChecked(data.getIc().getIsConstraintVisible());
		}
	}

	private int draw() {
		try {
			if (parents == null)
				return -1;
			if (table == null)
				return -1;
			table.removeAll();
			if (localConstraint==null)
				return -1;
			for (IDeclarationCommand var : localConstraint)
			{
				if (var instanceof ConstraintCommentCommand) {
					table.getColumn(1).pack();
					TableItem tableItem = new TableItem(table,	SWT.LEFT_TO_RIGHT);
					tableItem.setChecked(false);
					tableItem.setText(1, var.toString());
					tableItem.setData(var);
				}
			}
			table.getColumn(1).pack();
			table.redraw();
		} catch (Throwable t) {
			MyManager.printError(t);
		}
		return 0;
	}

	@Override
	public void dispose() {
		courant = null;
		super.dispose();
	}

	/**
	 * This method return the current view
	 * 
	 * @return ConstraintView
	 */
	public static ConstraintView getCourant() {
		if (courant == null) {
			try {

				courant = (ConstraintView) MyPluginManager.getCreateView("fr.inria.vcd.antlr.constraintview");
				MyPluginManager.getShowView("fr.inria.vcd.antlr.constraintview");
				courant.setFocus();
			} catch (Exception ex) {
				MyManager.printError(ex, "Constraint View");
			}
		}
		return courant;
	}

	/**
	 * This method return the table
	 * 
	 * @return Table
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * This method return the constraintText
	 * 
	 * @return String
	 */
	public ArrayList<IDeclarationCommand> getConstraint() {
		return localConstraint;
	}

	/**
	 * This method save all the constraints
	 * 
	 */
	public void setConstraint(ArrayList<IDeclarationCommand> constraint) {

		if (constraint==null)
		{
			size=0;
			localConstraint=null;
			draw();			
			return ;
		}
		int newSize= constraint.size();
		if (localConstraint != constraint || newSize !=size)
		{
			localConstraint = constraint;			
			draw();
			size=newSize; 
		}
	}
	
	public void resetConstraint(ArrayList<IDeclarationCommand> constraint) {

		if (constraint==null)
		{
			size=0;
			localConstraint=null;
			draw();			
			return ;
		}
		int newSize= constraint.size();
		
			localConstraint = constraint;			
			draw();
			size=newSize;
		
	}
}
