/*
 * 
 * @author : Anthony Gaudino
 * 
 *   
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */


package fr.inria.vcd.antlr.view;

import java.io.IOException;
import java.io.InputStream;
import org.antlr.runtime.ANTLRFileInputStream;
import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import fr.inria.vcd.antlr.Commentary;
import fr.inria.vcd.antlr.VcdLexer;
import fr.inria.vcd.antlr.VcdParser;
import fr.inria.vcd.antlr.VcdTreeAdaptor;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.Collector;

public class VcdDiagramTest {
	private void read(ANTLRStringStream input) throws RecognitionException {
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		parser.vcd_definitions();	// for init
		commentaryUpdate(lexer,adaptor);
		Collector.drawInWindow(adaptor.getVcd());
	}
	
	private void read(IVcdDiagram editor , ANTLRStringStream input) throws RecognitionException {
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		parser.vcd_definitions();	// for init
		commentaryUpdate(lexer,adaptor);	
		Collector.drawInEditor(editor,adaptor.getVcd());

	}
	
	private void commentaryUpdate(VcdLexer lexer,VcdTreeAdaptor adaptor )
	{
		for (Commentary cc: lexer.getAcm())
		{				
			adaptor.getVcd().addDefinition(cc.convert());			
		} 
	}
	
	public void read(InputStream file, String name) throws IOException, RecognitionException {
    	ANTLRFileInputStream input = new ANTLRFileInputStream(file, name);
    	read(input);
	}
	
	public void read(IVcdDiagram editor ,InputStream file, String name) throws IOException, RecognitionException {
    	ANTLRFileInputStream input = new ANTLRFileInputStream(file, name);
    	read(editor,input);
	}
	
	public void read(String fileName) throws IOException, RecognitionException {
    	ANTLRFileStream input = new ANTLRFileStream(fileName);		
    	read(input);
	}

	public void read(VCDDefinitions vcd)
	{
			Collector.drawInWindow(vcd);			
	}
	
	public static void main(String[] args) throws IOException, RecognitionException {
		Shell shell = new Shell();
		FileDialog fd = new FileDialog(shell, SWT.OPEN);
		fd.setFilterPath("D:/dev/antlr");
		String file = fd.open();
		if (file == null) return;
		new VcdDiagramTest().read(file);
	}
}
