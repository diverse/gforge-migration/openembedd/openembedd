// $ANTLR 3.0.1 D:\\SHELLEY\\Antlr\\Vcd.g 2009-01-15 14:13:09
package fr.inria.vcd.antlr;

import java.util.ArrayList;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;

public class VcdLexer extends Lexer {
    public static final int REAL_VECTOR=27;
    public static final int VERSION_TEXT=35;
    public static final int UPSCOPE=20;
    public static final int LETTER=33;
    public static final int IDENT_CHAR=34;
    public static final int VAR_TYPE=22;
    public static final int REFERENCE=24;
    public static final int VERSION=25;
    public static final int VALUE=31;
    public static final int Tokens=37;
    public static final int EOF=-1;
    public static final int TIME_UNIT=19;
    public static final int SCALAR_VALUE_CHANGE=26;
    public static final int DUMPOFF=11;
    public static final int ENDDEFINITIONS=12;
    public static final int SIMULATION_TIME=6;
    public static final int DUMPON=10;
    public static final int WS=36;
    public static final int SCOPE=14;
    public static final int BVECTOR=5;
    public static final int DUMPVARS=9;
    public static final int DUMPALL=7;
    public static final int TIMESCALE=17;
    public static final int IDENTIFIER_CODE=23;
    public static final int RVECTOR=4;
    public static final int IDENT=16;
    public static final int BIT=30;
    public static final int VAR=21;
    public static final int DIGIT=32;
    public static final int SCOPE_TYPE=15;
    public static final int DATE=13;
    public static final int END=8;
    public static final int BINARY_VECTOR=28;
    public static final int COMMENT=29;
    public static final int DECIMAL_NUMBER=18;
    
    int time=-1;
    ArrayList<Commentary> acm=new ArrayList<Commentary>();
    Uposition up=null;
           
    public int getTime()
    {
    	return time;  
    }
    
    public ArrayList<Commentary> getAcm() 
    {
    	return acm;
    }
    public int stringtime(String s)
    {
    	time=Integer.parseInt(s);
    	up = new Uposition(time,Uposition.Enumblocktype.SimulationTime );
    	return time;
    }
    
    public	int addcomment(String s)
    {
    	//System.out.println(time+   " " +s );
    	acm.add(new Commentary(s,up));
    	return 0;
    }
    	

    public VcdLexer() {;} 
    public VcdLexer(CharStream input) {
        super(input);
    }
    @Override
	public String getGrammarFileName() { return "D:\\SHELLEY\\Antlr\\Vcd.g"; }

    // $ANTLR start SIMULATION_TIME
    public final void mSIMULATION_TIME() throws RecognitionException {
        try {
            int _type = SIMULATION_TIME;
            // D:\\SHELLEY\\Antlr\\Vcd.g:107:2: ( '#' DECIMAL_NUMBER )
            // D:\\SHELLEY\\Antlr\\Vcd.g:107:4: '#' DECIMAL_NUMBER
            {
            match('#'); 
            mDECIMAL_NUMBER(); 
             String s=getText();
            		 stringtime(new String (s.substring(1))); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SIMULATION_TIME

    // $ANTLR start VAR_TYPE
    public final void mVAR_TYPE() throws RecognitionException {
        try {
            int _type = VAR_TYPE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:112:10: ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' )
            int alt1=17;
            switch ( input.LA(1) ) {
            case 'e':
                {
                alt1=1;
                }
                break;
            case 'i':
                {
                alt1=2;
                }
                break;
            case 'p':
                {
                alt1=3;
                }
                break;
            case 'r':
                {
                int LA1_4 = input.LA(2);

                if ( (LA1_4=='e') ) {
                    int LA1_8 = input.LA(3);

                    if ( (LA1_8=='a') ) {
                        int LA1_14 = input.LA(4);

                        if ( (LA1_14=='l') ) {
                            int LA1_18 = input.LA(5);

                            if ( (LA1_18=='t') ) {
                                alt1=5;
                            }
                            else {
                                alt1=4;}
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 14, input);

                            throw nvae;
                        }
                    }
                    else if ( (LA1_8=='g') ) {
                        alt1=6;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 8, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 4, input);

                    throw nvae;
                }
                }
                break;
            case 's':
                {
                int LA1_5 = input.LA(2);

                if ( (LA1_5=='u') ) {
                    int LA1_9 = input.LA(3);

                    if ( (LA1_9=='p') ) {
                        int LA1_16 = input.LA(4);

                        if ( (LA1_16=='p') ) {
                            int LA1_19 = input.LA(5);

                            if ( (LA1_19=='l') ) {
                                int LA1_28 = input.LA(6);

                                if ( (LA1_28=='y') ) {
                                    int LA1_29 = input.LA(7);

                                    if ( (LA1_29=='0') ) {
                                        alt1=7;
                                    }
                                    else if ( (LA1_29=='1') ) {
                                        alt1=8;
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 29, input);

                                        throw nvae;
                                    }
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 28, input);

                                    throw nvae;
                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 19, input);

                                throw nvae;
                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 16, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 9, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 5, input);

                    throw nvae;
                }
                }
                break;
            case 't':
                {
                int LA1_6 = input.LA(2);

                if ( (LA1_6=='r') ) {
                    int LA1_10 = input.LA(3);

                    if ( (LA1_10=='i') ) {
                        switch ( input.LA(4) ) {
                        case 'a':
                            {
                            alt1=10;
                            }
                            break;
                        case 'r':
                            {
                            alt1=12;
                            }
                            break;
                        case '1':
                            {
                            alt1=14;
                            }
                            break;
                        case '0':
                            {
                            alt1=13;
                            }
                            break;
                        case 'o':
                            {
                            alt1=11;
                            }
                            break;
                        default:
                            alt1=9;}

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 10, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 6, input);

                    throw nvae;
                }
                }
                break;
            case 'w':
                {
                switch ( input.LA(2) ) {
                case 'o':
                    {
                    alt1=17;
                    }
                    break;
                case 'a':
                    {
                    alt1=15;
                    }
                    break;
                case 'i':
                    {
                    alt1=16;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 7, input);

                    throw nvae;
                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("112:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:12: 'event'
                    {
                    match("event"); 


                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:22: 'integer'
                    {
                    match("integer"); 


                    }
                    break;
                case 3 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:34: 'parameter'
                    {
                    match("parameter"); 


                    }
                    break;
                case 4 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:48: 'real'
                    {
                    match("real"); 


                    }
                    break;
                case 5 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:57: 'realtime'
                    {
                    match("realtime"); 


                    }
                    break;
                case 6 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:70: 'reg'
                    {
                    match("reg"); 


                    }
                    break;
                case 7 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:78: 'supply0'
                    {
                    match("supply0"); 


                    }
                    break;
                case 8 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:112:90: 'supply1'
                    {
                    match("supply1"); 


                    }
                    break;
                case 9 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:5: 'tri'
                    {
                    match("tri"); 


                    }
                    break;
                case 10 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:15: 'triand'
                    {
                    match("triand"); 


                    }
                    break;
                case 11 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:26: 'trior'
                    {
                    match("trior"); 


                    }
                    break;
                case 12 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:36: 'trireg'
                    {
                    match("trireg"); 


                    }
                    break;
                case 13 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:47: 'tri0'
                    {
                    match("tri0"); 


                    }
                    break;
                case 14 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:56: 'tri1'
                    {
                    match("tri1"); 


                    }
                    break;
                case 15 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:65: 'wand'
                    {
                    match("wand"); 


                    }
                    break;
                case 16 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:74: 'wire'
                    {
                    match("wire"); 


                    }
                    break;
                case 17 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:113:83: 'wor'
                    {
                    match("wor"); 


                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VAR_TYPE

    // $ANTLR start SCOPE_TYPE
    public final void mSCOPE_TYPE() throws RecognitionException {
        try {
            int _type = SCOPE_TYPE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:115:13: ( 'begin' | 'fork' | 'function' | 'module' | 'task' )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 'b':
                {
                alt2=1;
                }
                break;
            case 'f':
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2=='u') ) {
                    alt2=3;
                }
                else if ( (LA2_2=='o') ) {
                    alt2=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("115:1: SCOPE_TYPE : ( 'begin' | 'fork' | 'function' | 'module' | 'task' );", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case 'm':
                {
                alt2=4;
                }
                break;
            case 't':
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("115:1: SCOPE_TYPE : ( 'begin' | 'fork' | 'function' | 'module' | 'task' );", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:115:15: 'begin'
                    {
                    match("begin"); 


                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:115:25: 'fork'
                    {
                    match("fork"); 


                    }
                    break;
                case 3 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:115:34: 'function'
                    {
                    match("function"); 


                    }
                    break;
                case 4 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:115:47: 'module'
                    {
                    match("module"); 


                    }
                    break;
                case 5 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:115:58: 'task'
                    {
                    match("task"); 


                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SCOPE_TYPE

    // $ANTLR start TIME_UNIT
    public final void mTIME_UNIT() throws RecognitionException {
        try {
            int _type = TIME_UNIT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:117:13: ( 's' | 'ms' | 'us' | 'ns' | 'ps' | 'fs' )
            int alt3=6;
            switch ( input.LA(1) ) {
            case 's':
                {
                alt3=1;
                }
                break;
            case 'm':
                {
                alt3=2;
                }
                break;
            case 'u':
                {
                alt3=3;
                }
                break;
            case 'n':
                {
                alt3=4;
                }
                break;
            case 'p':
                {
                alt3=5;
                }
                break;
            case 'f':
                {
                alt3=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("117:1: TIME_UNIT : ( 's' | 'ms' | 'us' | 'ns' | 'ps' | 'fs' );", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:15: 's'
                    {
                    match('s'); 

                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:21: 'ms'
                    {
                    match("ms"); 


                    }
                    break;
                case 3 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:28: 'us'
                    {
                    match("us"); 


                    }
                    break;
                case 4 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:35: 'ns'
                    {
                    match("ns"); 


                    }
                    break;
                case 5 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:42: 'ps'
                    {
                    match("ps"); 


                    }
                    break;
                case 6 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:117:49: 'fs'
                    {
                    match("fs"); 


                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end TIME_UNIT

    // $ANTLR start REFERENCE
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:120:2: ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:120:4: IDENT '[' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;
                case 2 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:121:4: IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(':'); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end REFERENCE

    // $ANTLR start ENDDEFINITIONS
    public final void mENDDEFINITIONS() throws RecognitionException {
        try {
            int _type = ENDDEFINITIONS;
            // D:\\SHELLEY\\Antlr\\Vcd.g:124:2: ( '$enddefinitions' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:124:4: '$enddefinitions'
            {
            match("$enddefinitions"); 

              up=new Uposition( 0,Uposition.Enumblocktype.EndDef  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ENDDEFINITIONS

    // $ANTLR start COMMENT
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:127:9: ( '$comment' ( . )* '$end' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:128:2: '$comment' ( . )* '$end'
            {
            match("$comment"); 

            // D:\\SHELLEY\\Antlr\\Vcd.g:128:13: ( . )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='$') ) {
                    int LA5_1 = input.LA(2);

                    if ( (LA5_1=='e') ) {
                        int LA5_3 = input.LA(3);

                        if ( (LA5_3=='n') ) {
                            int LA5_4 = input.LA(4);

                            if ( (LA5_4=='d') ) {
                                alt5=2;
                            }
                            else if ( ((LA5_4>='\u0000' && LA5_4<='c')||(LA5_4>='e' && LA5_4<='\uFFFE')) ) {
                                alt5=1;
                            }


                        }
                        else if ( ((LA5_3>='\u0000' && LA5_3<='m')||(LA5_3>='o' && LA5_3<='\uFFFE')) ) {
                            alt5=1;
                        }


                    }
                    else if ( ((LA5_1>='\u0000' && LA5_1<='d')||(LA5_1>='f' && LA5_1<='\uFFFE')) ) {
                        alt5=1;
                    }


                }
                else if ( ((LA5_0>='\u0000' && LA5_0<='#')||(LA5_0>='%' && LA5_0<='\uFFFE')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:128:13: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            match("$end"); 

              
            	 String s=getText();	
            	 addcomment(new String ( s.substring(8, s.length()-4)));
            	 
             skip();
            	 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMENT

    // $ANTLR start DATE
    public final void mDATE() throws RecognitionException {
        try {
            int _type = DATE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:139:6: ( '$date' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:139:8: '$date'
            {
            match("$date"); 

              up=new Uposition( 0,Uposition.Enumblocktype.Date  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DATE

    // $ANTLR start SCOPE
    public final void mSCOPE() throws RecognitionException {
        try {
            int _type = SCOPE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:140:9: ( '$scope' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:140:11: '$scope'
            {
            match("$scope"); 

              up=new Uposition( 0,Uposition.Enumblocktype.Scope  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SCOPE

    // $ANTLR start TIMESCALE
    public final void mTIMESCALE() throws RecognitionException {
        try {
            int _type = TIMESCALE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:141:10: ( '$timescale' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:141:12: '$timescale'
            {
            match("$timescale"); 

              up=new Uposition( 0,Uposition.Enumblocktype.TimeScale  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end TIMESCALE

    // $ANTLR start UPSCOPE
    public final void mUPSCOPE() throws RecognitionException {
        try {
            int _type = UPSCOPE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:142:9: ( '$upscope' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:142:11: '$upscope'
            {
            match("$upscope"); 

              up=new Uposition( 0,Uposition.Enumblocktype.UpScope  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end UPSCOPE

    // $ANTLR start END
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            // D:\\SHELLEY\\Antlr\\Vcd.g:143:5: ( '$end' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:143:7: '$end'
            {
            match("$end"); 

             if (up!=null) up.setTheend(true);  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end END

    // $ANTLR start VAR
    public final void mVAR() throws RecognitionException {
        try {
            int _type = VAR;
            // D:\\SHELLEY\\Antlr\\Vcd.g:144:5: ( '$var' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:144:7: '$var'
            {
            match("$var"); 

              up=new Uposition( 0,Uposition.Enumblocktype.Var  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VAR

    // $ANTLR start VERSION
    public final void mVERSION() throws RecognitionException {
        try {
            int _type = VERSION;
            // D:\\SHELLEY\\Antlr\\Vcd.g:145:9: ( '$version' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:145:11: '$version'
            {
            match("$version"); 

              up=new Uposition( 0,Uposition.Enumblocktype.Version  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VERSION

    // $ANTLR start DUMPALL
    public final void mDUMPALL() throws RecognitionException {
        try {
            int _type = DUMPALL;
            // D:\\SHELLEY\\Antlr\\Vcd.g:146:9: ( '$dumpall' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:146:11: '$dumpall'
            {
            match("$dumpall"); 

              up=new Uposition( 0,Uposition.Enumblocktype.DumpAll  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DUMPALL

    // $ANTLR start DUMPON
    public final void mDUMPON() throws RecognitionException {
        try {
            int _type = DUMPON;
            // D:\\SHELLEY\\Antlr\\Vcd.g:147:8: ( '$dumpon' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:147:10: '$dumpon'
            {
            match("$dumpon"); 

              up=new Uposition( 0,Uposition.Enumblocktype.DumpOn  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DUMPON

    // $ANTLR start DUMPOFF
    public final void mDUMPOFF() throws RecognitionException {
        try {
            int _type = DUMPOFF;
            // D:\\SHELLEY\\Antlr\\Vcd.g:148:9: ( '$dumpoff' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:148:11: '$dumpoff'
            {
            match("$dumpoff"); 

              up=new Uposition( 0,Uposition.Enumblocktype.DumpOff  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DUMPOFF

    // $ANTLR start DUMPVARS
    public final void mDUMPVARS() throws RecognitionException {
        try {
            int _type = DUMPVARS;
            // D:\\SHELLEY\\Antlr\\Vcd.g:149:9: ( '$dumpvars' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:149:11: '$dumpvars'
            {
            match("$dumpvars"); 

              up=new Uposition( 0,Uposition.Enumblocktype.DumpVars  )  ;  

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DUMPVARS

    // $ANTLR start SCALAR_VALUE_CHANGE
    public final void mSCALAR_VALUE_CHANGE() throws RecognitionException {
        try {
            int _type = SCALAR_VALUE_CHANGE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:152:2: ( ( BIT | VALUE ) IDENTIFIER_CODE )
            // D:\\SHELLEY\\Antlr\\Vcd.g:152:4: ( BIT | VALUE ) IDENTIFIER_CODE
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            mIDENTIFIER_CODE(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SCALAR_VALUE_CHANGE

    // $ANTLR start DECIMAL_NUMBER
    public final void mDECIMAL_NUMBER() throws RecognitionException {
        try {
            int _type = DECIMAL_NUMBER;
            // D:\\SHELLEY\\Antlr\\Vcd.g:155:2: ( ( '-' )? ( BIT | DIGIT )+ )
            // D:\\SHELLEY\\Antlr\\Vcd.g:155:4: ( '-' )? ( BIT | DIGIT )+
            {
            // D:\\SHELLEY\\Antlr\\Vcd.g:155:4: ( '-' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='-') ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:155:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:155:9: ( BIT | DIGIT )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DECIMAL_NUMBER

    // $ANTLR start REAL_VECTOR
    public final void mREAL_VECTOR() throws RecognitionException {
        try {
            int _type = REAL_VECTOR;
            // D:\\SHELLEY\\Antlr\\Vcd.g:157:2: ( ( 'r' | 'R' ) ( '-' )? ( BIT | DIGIT )* '.' ( BIT | DIGIT )+ )
            // D:\\SHELLEY\\Antlr\\Vcd.g:157:4: ( 'r' | 'R' ) ( '-' )? ( BIT | DIGIT )* '.' ( BIT | DIGIT )+
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:157:14: ( '-' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='-') ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // D:\\SHELLEY\\Antlr\\Vcd.g:157:14: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:157:19: ( BIT | DIGIT )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='0' && LA9_0<='9')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match('.'); 
            // D:\\SHELLEY\\Antlr\\Vcd.g:157:36: ( BIT | DIGIT )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='0' && LA10_0<='9')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end REAL_VECTOR

    // $ANTLR start BINARY_VECTOR
    public final void mBINARY_VECTOR() throws RecognitionException {
        try {
            int _type = BINARY_VECTOR;
            // D:\\SHELLEY\\Antlr\\Vcd.g:160:2: ( ( 'b' | 'B' ) ( BIT | VALUE )+ )
            // D:\\SHELLEY\\Antlr\\Vcd.g:160:4: ( 'b' | 'B' ) ( BIT | VALUE )+
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:160:14: ( BIT | VALUE )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='0' && LA11_0<='1')||LA11_0=='X'||LA11_0=='Z'||LA11_0=='x'||LA11_0=='z') ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end BINARY_VECTOR

    // $ANTLR start IDENT
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:161:7: ( ( LETTER | '_' ) ( LETTER | DIGIT | BIT | '_' | '@' | '.' )* )
            // D:\\SHELLEY\\Antlr\\Vcd.g:161:9: ( LETTER | '_' ) ( LETTER | DIGIT | BIT | '_' | '@' | '.' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:161:22: ( LETTER | DIGIT | BIT | '_' | '@' | '.' )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0=='.'||(LA12_0>='0' && LA12_0<='9')||(LA12_0>='@' && LA12_0<='Z')||LA12_0=='_'||(LA12_0>='a' && LA12_0<='z')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( input.LA(1)=='.'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='@' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end IDENT

    // $ANTLR start VERSION_TEXT
    public final void mVERSION_TEXT() throws RecognitionException {
        try {
            int _type = VERSION_TEXT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:163:2: ( ( LETTER | DIGIT | BIT ) ( LETTER | DIGIT | BIT | IDENT_CHAR | '.' )+ )
            // D:\\SHELLEY\\Antlr\\Vcd.g:163:4: ( LETTER | DIGIT | BIT ) ( LETTER | DIGIT | BIT | IDENT_CHAR | '.' )+
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // D:\\SHELLEY\\Antlr\\Vcd.g:163:23: ( LETTER | DIGIT | BIT | IDENT_CHAR | '.' )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='!' && LA13_0<='^')||(LA13_0>='`' && LA13_0<='~')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='^')||(input.LA(1)>='`' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VERSION_TEXT

    // $ANTLR start IDENTIFIER_CODE
    public final void mIDENTIFIER_CODE() throws RecognitionException {
        try {
            int _type = IDENTIFIER_CODE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:165:2: ( IDENT_CHAR ( DIGIT | BIT | LETTER | IDENT_CHAR | '.' | '_' )* )
            // D:\\SHELLEY\\Antlr\\Vcd.g:165:4: IDENT_CHAR ( DIGIT | BIT | LETTER | IDENT_CHAR | '.' | '_' )*
            {
            mIDENT_CHAR(); 
            // D:\\SHELLEY\\Antlr\\Vcd.g:165:15: ( DIGIT | BIT | LETTER | IDENT_CHAR | '.' | '_' )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>='!' && LA14_0<='~')) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end IDENTIFIER_CODE

    // $ANTLR start DIGIT
    public final void mDIGIT() throws RecognitionException {
        try {
            int _type = DIGIT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:166:9: ( '2' .. '9' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:166:11: '2' .. '9'
            {
            matchRange('2','9'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DIGIT

    // $ANTLR start VALUE
    public final void mVALUE() throws RecognitionException {
        try {
            int _type = VALUE;
            // D:\\SHELLEY\\Antlr\\Vcd.g:167:7: ( 'x' | 'X' | 'z' | 'Z' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:
            {
            if ( input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VALUE

    // $ANTLR start BIT
    public final void mBIT() throws RecognitionException {
        try {
            int _type = BIT;
            // D:\\SHELLEY\\Antlr\\Vcd.g:168:6: ( '0' | '1' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end BIT

    // $ANTLR start LETTER
    public final void mLETTER() throws RecognitionException {
        try {
            int _type = LETTER;
            // D:\\SHELLEY\\Antlr\\Vcd.g:169:8: ( 'a' .. 'z' | 'A' .. 'Z' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LETTER

    // $ANTLR start IDENT_CHAR
    public final void mIDENT_CHAR() throws RecognitionException {
        try {
            // D:\\SHELLEY\\Antlr\\Vcd.g:171:13: ( '!' .. '-' | '/' | ':' .. '@' | '[' .. '^' | '`' | '{' .. '~' )
            // D:\\SHELLEY\\Antlr\\Vcd.g:
            {
            if ( (input.LA(1)>='!' && input.LA(1)<='-')||input.LA(1)=='/'||(input.LA(1)>=':' && input.LA(1)<='@')||(input.LA(1)>='[' && input.LA(1)<='^')||input.LA(1)=='`'||(input.LA(1)>='{' && input.LA(1)<='~') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end IDENT_CHAR

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // D:\\SHELLEY\\Antlr\\Vcd.g:173:4: ( ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+ )
            // D:\\SHELLEY\\Antlr\\Vcd.g:173:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            {
            // D:\\SHELLEY\\Antlr\\Vcd.g:173:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            int cnt15=0;
            loop15:
            do {
                int alt15=4;
                switch ( input.LA(1) ) {
                case ' ':
                    {
                    alt15=1;
                    }
                    break;
                case '\t':
                    {
                    alt15=2;
                    }
                    break;
                case '\n':
                case '\r':
                    {
                    alt15=3;
                    }
                    break;

                }

                switch (alt15) {
            	case 1 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:173:7: ' '
            	    {
            	    match(' '); 

            	    }
            	    break;
            	case 2 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:173:11: '\\t'
            	    {
            	    match('\t'); 

            	    }
            	    break;
            	case 3 :
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:174:3: ( ( '\\n' | '\\r' ) )
            	    {
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:174:3: ( ( '\\n' | '\\r' ) )
            	    // D:\\SHELLEY\\Antlr\\Vcd.g:174:4: ( '\\n' | '\\r' )
            	    {
            	    if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }

            	     /*newline();*/

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);

             skip(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    @Override
	public void mTokens() throws RecognitionException {
        // D:\\SHELLEY\\Antlr\\Vcd.g:1:8: ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | COMMENT | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | DIGIT | VALUE | BIT | LETTER | WS )
        int alt16=30;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:10: SIMULATION_TIME
                {
                mSIMULATION_TIME(); 

                }
                break;
            case 2 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:26: VAR_TYPE
                {
                mVAR_TYPE(); 

                }
                break;
            case 3 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:35: SCOPE_TYPE
                {
                mSCOPE_TYPE(); 

                }
                break;
            case 4 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:46: TIME_UNIT
                {
                mTIME_UNIT(); 

                }
                break;
            case 5 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:56: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 6 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:66: ENDDEFINITIONS
                {
                mENDDEFINITIONS(); 

                }
                break;
            case 7 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:81: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 8 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:89: DATE
                {
                mDATE(); 

                }
                break;
            case 9 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:94: SCOPE
                {
                mSCOPE(); 

                }
                break;
            case 10 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:100: TIMESCALE
                {
                mTIMESCALE(); 

                }
                break;
            case 11 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:110: UPSCOPE
                {
                mUPSCOPE(); 

                }
                break;
            case 12 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:118: END
                {
                mEND(); 

                }
                break;
            case 13 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:122: VAR
                {
                mVAR(); 

                }
                break;
            case 14 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:126: VERSION
                {
                mVERSION(); 

                }
                break;
            case 15 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:134: DUMPALL
                {
                mDUMPALL(); 

                }
                break;
            case 16 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:142: DUMPON
                {
                mDUMPON(); 

                }
                break;
            case 17 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:149: DUMPOFF
                {
                mDUMPOFF(); 

                }
                break;
            case 18 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:157: DUMPVARS
                {
                mDUMPVARS(); 

                }
                break;
            case 19 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:166: SCALAR_VALUE_CHANGE
                {
                mSCALAR_VALUE_CHANGE(); 

                }
                break;
            case 20 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:186: DECIMAL_NUMBER
                {
                mDECIMAL_NUMBER(); 

                }
                break;
            case 21 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:201: REAL_VECTOR
                {
                mREAL_VECTOR(); 

                }
                break;
            case 22 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:213: BINARY_VECTOR
                {
                mBINARY_VECTOR(); 

                }
                break;
            case 23 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:227: IDENT
                {
                mIDENT(); 

                }
                break;
            case 24 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:233: VERSION_TEXT
                {
                mVERSION_TEXT(); 

                }
                break;
            case 25 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:246: IDENTIFIER_CODE
                {
                mIDENTIFIER_CODE(); 

                }
                break;
            case 26 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:262: DIGIT
                {
                mDIGIT(); 

                }
                break;
            case 27 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:268: VALUE
                {
                mVALUE(); 

                }
                break;
            case 28 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:274: BIT
                {
                mBIT(); 

                }
                break;
            case 29 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:278: LETTER
                {
                mLETTER(); 

                }
                break;
            case 30 :
                // D:\\SHELLEY\\Antlr\\Vcd.g:1:285: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA4 dfa4 = new DFA4(this);
    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA4_eotS =
        "\10\uffff";
    static final String DFA4_eofS =
        "\10\uffff";
    static final String DFA4_minS =
        "\1\101\2\56\1\55\2\60\2\uffff";
    static final String DFA4_maxS =
        "\3\172\2\71\1\135\2\uffff";
    static final String DFA4_acceptS =
        "\6\uffff\1\1\1\2";
    static final String DFA4_specialS =
        "\10\uffff}>";
    static final String[] DFA4_transitionS = {
            "\32\1\4\uffff\1\1\1\uffff\32\1",
            "\1\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32\2",
            "\1\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32\2",
            "\1\4\2\uffff\12\5",
            "\12\5",
            "\12\5\1\7\42\uffff\1\6",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        @Override
		public String getDescription() {
            return "119:1: REFERENCE : ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' );";
        }
    }
    static final String DFA16_eotS =
        "\1\uffff\1\27\4\35\1\51\10\35\1\27\1\35\1\103\1\27\1\103\3\35\2"+
        "\uffff\1\27\1\106\2\35\1\uffff\1\40\1\35\1\uffff\2\35\1\51\2\35"+
        "\1\40\2\35\1\uffff\6\35\1\130\1\51\2\35\1\51\1\35\2\51\3\134\7\27"+
        "\1\103\1\uffff\1\103\2\uffff\1\35\2\40\3\35\1\162\2\40\1\164\1\35"+
        "\1\162\3\35\1\162\1\35\1\uffff\3\35\1\uffff\6\134\11\27\1\35\1\40"+
        "\1\105\2\35\1\162\1\uffff\1\164\1\uffff\1\35\2\162\3\35\1\u0098"+
        "\2\162\1\35\1\u0098\2\35\2\134\1\105\1\27\1\u00a2\6\27\1\u00a9\1"+
        "\162\2\40\6\35\1\162\1\uffff\1\u0098\2\35\4\134\2\27\1\uffff\3\27"+
        "\1\u00bd\2\27\1\uffff\1\105\4\35\2\162\1\35\1\u0098\1\134\2\105"+
        "\7\27\1\uffff\1\u00d0\1\27\1\162\2\35\2\162\1\35\2\134\7\27\1\u00dd"+
        "\1\uffff\1\27\1\35\1\162\1\u0098\1\105\2\27\1\u00e4\2\27\1\u00e7"+
        "\1\u00e8\1\uffff\1\u00e9\1\162\1\27\1\uffff\2\27\1\uffff\1\27\1"+
        "\u00ed\3\uffff\2\27\1\u00f0\1\uffff\2\27\1\uffff\1\u00e1\3\27\1"+
        "\u00f6\1\uffff";
    static final String DFA16_eofS =
        "\u00f7\uffff";
    static final String DFA16_minS =
        "\1\11\1\55\15\41\1\143\2\41\1\60\3\41\1\56\2\uffff\1\60\3\41\1\uffff"+
        "\1\55\1\56\1\uffff\5\41\1\56\2\41\1\uffff\21\41\1\157\1\156\1\160"+
        "\1\151\1\141\1\143\1\141\1\41\1\uffff\1\41\2\uffff\1\41\2\60\4\41"+
        "\1\56\1\60\10\41\1\uffff\3\41\1\uffff\2\41\1\56\3\41\1\155\1\144"+
        "\1\163\2\155\1\164\1\157\2\162\1\41\1\55\4\41\1\uffff\1\41\1\uffff"+
        "\15\41\1\55\2\41\1\155\1\41\1\143\1\145\1\160\1\145\1\160\1\163"+
        "\2\41\2\60\7\41\1\uffff\3\41\2\60\2\41\2\145\1\uffff\1\157\1\163"+
        "\1\141\1\41\1\145\1\151\1\uffff\11\41\1\55\2\41\1\156\1\146\1\160"+
        "\1\143\1\141\1\154\1\146\1\uffff\1\41\1\157\6\41\2\60\1\164\1\151"+
        "\1\145\1\141\1\162\1\154\1\146\1\41\1\uffff\1\156\4\41\1\0\1\156"+
        "\1\41\1\154\1\163\2\41\1\uffff\2\41\1\0\1\uffff\1\0\1\151\1\uffff"+
        "\1\145\1\41\3\uffff\1\0\1\164\1\41\1\uffff\1\0\1\151\1\uffff\1\41"+
        "\1\157\1\156\1\163\1\41\1\uffff";
    static final String DFA16_maxS =
        "\1\176\1\71\15\176\1\166\2\176\1\71\3\176\1\172\2\uffff\1\71\3\176"+
        "\1\uffff\1\71\1\172\1\uffff\5\176\1\71\2\176\1\uffff\21\176\1\157"+
        "\1\156\1\160\1\151\1\165\1\143\1\145\1\176\1\uffff\1\176\2\uffff"+
        "\1\176\1\71\1\135\4\176\2\71\10\176\1\uffff\3\176\1\uffff\2\176"+
        "\1\172\3\176\1\155\1\144\1\163\2\155\1\164\1\157\2\162\1\176\1\71"+
        "\4\176\1\uffff\1\176\1\uffff\15\176\1\71\2\176\1\155\1\176\1\143"+
        "\1\145\1\160\1\145\1\160\1\163\2\176\1\71\1\135\7\176\1\uffff\3"+
        "\176\1\71\1\135\2\176\2\145\1\uffff\1\157\1\163\1\166\1\176\1\145"+
        "\1\151\1\uffff\11\176\1\71\2\176\1\156\1\146\1\160\1\143\1\141\1"+
        "\154\1\156\1\uffff\1\176\1\157\6\176\1\71\1\135\1\164\1\151\1\145"+
        "\1\141\1\162\1\154\1\146\1\176\1\uffff\1\156\4\176\1\ufffe\1\156"+
        "\1\176\1\154\1\163\2\176\1\uffff\2\176\1\ufffe\1\uffff\1\ufffe\1"+
        "\151\1\uffff\1\145\1\176\3\uffff\1\ufffe\1\164\1\176\1\uffff\1\ufffe"+
        "\1\151\1\uffff\1\176\1\157\1\156\1\163\1\176\1\uffff";
    static final String DFA16_acceptS =
        "\27\uffff\1\31\1\36\4\uffff\1\27\2\uffff\1\30\10\uffff\1\4\31\uffff"+
        "\1\24\1\uffff\1\5\1\1\21\uffff\1\26\3\uffff\1\23\25\uffff\1\2\1"+
        "\uffff\1\25\43\uffff\1\3\11\uffff\1\14\6\uffff\1\15\23\uffff\1\10"+
        "\22\uffff\1\11\14\uffff\1\20\3\uffff\1\7\2\uffff\1\13\2\uffff\1"+
        "\17\1\21\1\16\3\uffff\1\22\2\uffff\1\12\5\uffff\1\6";
    static final String DFA16_specialS =
        "\u00f7\uffff}>";
    static final String[] DFA16_transitionS = {
            "\2\30\2\uffff\1\30\22\uffff\1\30\2\27\1\1\1\17\10\27\1\22\1"+
            "\uffff\1\27\2\21\10\23\7\27\1\25\1\24\17\25\1\20\5\25\1\16\1"+
            "\25\1\16\4\27\1\26\1\27\1\25\1\11\2\25\1\2\1\12\2\25\1\3\3\25"+
            "\1\13\1\15\1\25\1\4\1\25\1\5\1\6\1\7\1\14\1\25\1\10\1\16\1\25"+
            "\1\16\4\27",
            "\1\31\2\uffff\12\32",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\25\34"+
            "\1\33\4\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\41\14\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\1\42\21"+
            "\34\1\43\7\34\4\40",
            "\14\40\1\46\1\47\1\40\12\45\6\40\33\34\1\36\3\40\1\37\1\40\4"+
            "\34\1\44\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\24\34"+
            "\1\50\5\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\1\53\20"+
            "\34\1\52\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\1\55\7"+
            "\34\1\54\5\34\1\56\13\34\4\40",
            "\15\40\1\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\36\3"+
            "\40\1\37\1\40\4\34\1\57\22\34\1\60\1\34\1\60\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\16\34"+
            "\1\62\3\34\1\61\1\34\1\63\5\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\16\34"+
            "\1\65\3\34\1\64\7\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\22\34"+
            "\1\66\7\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\22\34"+
            "\1\67\7\34\4\40",
            "\15\72\1\34\1\72\12\34\6\72\1\70\32\34\1\71\3\72\1\37\1\72\32"+
            "\34\4\72",
            "\1\73\1\77\1\74\15\uffff\1\100\1\76\1\75\1\101",
            "\14\40\1\46\1\47\1\40\12\45\6\40\33\34\1\36\3\40\1\37\1\40\32"+
            "\34\4\40",
            "\15\72\1\40\1\72\12\102\7\72\32\40\4\72\1\uffff\1\72\32\40\4"+
            "\72",
            "\12\104",
            "\17\40\12\102\45\40\1\uffff\37\40",
            "\15\40\1\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\36\3"+
            "\40\1\37\1\40\27\34\1\60\1\34\1\60\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\1\37\1\uffff\12\37\6\uffff\33\37\1\105\3\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "\12\32",
            "\17\27\12\32\105\27",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\107\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "",
            "\1\110\2\uffff\12\111",
            "\1\37\1\uffff\12\37\6\uffff\33\37\1\105\3\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\23\34"+
            "\1\112\6\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\113\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\1\114"+
            "\5\34\1\115\23\34\4\40",
            "\15\40\1\47\1\40\12\45\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\1\117\1\uffff\12\116",
            "\15\40\1\34\1\40\12\120\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\17\34"+
            "\1\121\12\34\4\40",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\10\34"+
            "\1\122\21\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\22\34"+
            "\1\123\7\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\124\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\125\14\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\126\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\6\34\1"+
            "\127\23\34\4\40",
            "\15\40\1\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\36\3"+
            "\40\1\37\1\40\27\34\1\60\1\34\1\60\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\131\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\132\14\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\3\34\1"+
            "\133\26\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\140\1\136\1\140\12\136\6\140\33\136\1\135\3\140\1\137\1"+
            "\140\32\136\4\140",
            "\14\140\1\141\2\140\12\142\45\140\1\uffff\37\140",
            "\76\140\1\uffff\37\140",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\150\23\uffff\1\147",
            "\1\151",
            "\1\153\3\uffff\1\152",
            "\17\40\12\102\45\40\1\uffff\37\40",
            "",
            "\17\27\12\104\105\27",
            "",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\154\14\34\4\40",
            "\12\111",
            "\12\111\1\155\42\uffff\1\156",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\157\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\1\160"+
            "\31\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\13\34"+
            "\1\161\16\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\1\117\1\uffff\12\116",
            "\12\163",
            "\15\40\1\34\1\40\12\120\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\17\34"+
            "\1\165\12\34\4\40",
            "\15\40\1\34\1\40\1\167\1\166\10\34\6\40\33\34\1\36\3\40\1\37"+
            "\1\40\1\170\15\34\1\172\2\34\1\171\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\12\34"+
            "\1\173\17\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\174\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\3\34\1"+
            "\175\26\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\10\34"+
            "\1\176\21\34\4\40",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\12\34"+
            "\1\177\17\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\2\34\1"+
            "\u0080\27\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\24\34"+
            "\1\u0081\5\34\4\40",
            "",
            "\14\140\1\141\2\140\12\142\45\140\1\uffff\37\140",
            "\15\140\1\136\1\140\12\136\6\140\33\136\1\135\3\140\1\137\1"+
            "\140\32\136\4\140",
            "\1\137\1\uffff\12\137\6\uffff\33\137\1\u0082\3\uffff\1\137\1"+
            "\uffff\32\137",
            "\76\140\1\uffff\37\140",
            "\17\140\12\142\45\140\1\uffff\37\140",
            "\17\140\12\142\1\u0083\42\140\1\u0084\1\140\1\uffff\37\140",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\23\34"+
            "\1\u008e\6\34\4\40",
            "\1\u008f\2\uffff\12\u0090",
            "\76\40\1\uffff\37\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\6\34\1"+
            "\u0091\23\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\14\34"+
            "\1\u0092\15\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\23\34"+
            "\1\u0093\6\34\4\40",
            "",
            "\17\40\12\163\45\40\1\uffff\37\40",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\13\34"+
            "\1\u0094\16\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\u0095\14\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u0096\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\u0097\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\u0099\14\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\23\34"+
            "\1\u009a\6\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\13\34"+
            "\1\u009b\16\34\4\40",
            "\1\u009c\2\uffff\12\u009d",
            "\14\140\1\u009e\2\140\12\u009f\45\140\1\uffff\37\140",
            "\76\140\1\134\37\140",
            "\1\u00a0",
            "\103\27\1\u00a1\32\27",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\136\27",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\12\u0090",
            "\12\u0090\43\uffff\1\u00aa",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u00ab\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u00ac\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\10\34"+
            "\1\u00ad\21\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\30\34"+
            "\1\u00ae\1\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\3\34\1"+
            "\u00af\26\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\6\34\1"+
            "\u00b0\23\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\10\34"+
            "\1\u00b1\21\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u00b2\25\34\4\40",
            "\12\u009d",
            "\12\u009d\1\u00b3\42\uffff\1\u00b4",
            "\17\140\12\u009f\45\140\1\uffff\37\140",
            "\17\140\12\u009f\43\140\1\u00b5\1\140\1\uffff\37\140",
            "\1\u00b6",
            "\1\u00b7",
            "",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00bb\15\uffff\1\u00bc\6\uffff\1\u00ba",
            "\136\27",
            "\1\u00be",
            "\1\u00bf",
            "",
            "\76\40\1\uffff\37\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\u00c0\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\23\34"+
            "\1\u00c1\6\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\14\34"+
            "\1\u00c2\15\34\4\40",
            "\15\40\1\34\1\40\1\u00c3\1\u00c4\10\34\6\40\33\34\1\36\3\40"+
            "\1\37\1\40\32\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\16\34"+
            "\1\u00c5\13\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\1\u00c6\2\uffff\12\u00c7",
            "\136\134",
            "\76\140\1\134\37\140",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce\7\uffff\1\u00cf",
            "",
            "\136\27",
            "\1\u00d1",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u00d2\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\4\34\1"+
            "\u00d3\25\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\15\34"+
            "\1\u00d4\14\34\4\40",
            "\12\u00c7",
            "\12\u00c7\43\uffff\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\u00dc",
            "\136\27",
            "",
            "\1\u00de",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\21\34"+
            "\1\u00df\10\34\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\136\134",
            "\41\u00e1\3\u00e2\1\u00e0\132\u00e2\uff80\u00e1",
            "\1\u00e3",
            "\136\27",
            "\1\u00e5",
            "\1\u00e6",
            "\136\27",
            "\136\27",
            "",
            "\136\27",
            "\15\40\1\34\1\40\12\34\6\40\33\34\1\36\3\40\1\37\1\40\32\34"+
            "\4\40",
            "\41\u00e1\3\u00e2\1\u00e0\100\u00e2\1\u00ea\31\u00e2\uff80\u00e1",
            "",
            "\41\u00e1\3\u00e2\1\u00e0\132\u00e2\uff80\u00e1",
            "\1\u00eb",
            "",
            "\1\u00ec",
            "\136\27",
            "",
            "",
            "",
            "\41\u00e1\3\u00e2\1\u00e0\111\u00e2\1\u00ee\20\u00e2\uff80\u00e1",
            "\1\u00ef",
            "\136\27",
            "",
            "\41\u00e1\3\u00e2\1\u00e0\77\u00e2\1\u00f1\32\u00e2\uff80\u00e1",
            "\1\u00f2",
            "",
            "\3\u00e2\1\u00e0\132\u00e2",
            "\1\u00f3",
            "\1\u00f4",
            "\1\u00f5",
            "\136\27",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        @Override
		public String getDescription() {
            return "1:1: Tokens : ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | COMMENT | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | DIGIT | VALUE | BIT | LETTER | WS );";
        }
    }
 

}