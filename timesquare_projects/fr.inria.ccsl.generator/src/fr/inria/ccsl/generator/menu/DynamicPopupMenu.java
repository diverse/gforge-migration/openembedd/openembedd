package fr.inria.ccsl.generator.menu;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

import code.Cumlcreator;
import code.Cumlelement;
import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ccsl.generator.actions.createClockref;
import fr.inria.ccsl.generator.actions.createFunction;
import fr.inria.ccsl.generator.actions.createMClock;
import fr.inria.ccsl.generator.actions.createRequirement;
import fr.inria.ccsl.generator.actions.createSClock;
import fr.inria.ccsl.generator.actions.createTrigger;
import fr.inria.ccsl.generator.actions.generateCCSL;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml;

public class DynamicPopupMenu extends CompoundContributionItem {

	protected IEditorPart	editorPart	= null;
	protected Element	selection	= null;
	private static SimulationStruct sim = null;

	public DynamicPopupMenu() {
		super();
	}

	public DynamicPopupMenu(String id) {
		super(id);
	}

	@Override
	protected IContributionItem[] getContributionItems()
	{
		try
		{
			selection = null;
			editorPart = MyPluginManager.getCourantEditor();		
			if (editorPart == null)
				return new IContributionItem[] {};

			if (!MyEditorManagerUml.getDefault().isUmlLikeEditor())
				return new IContributionItem[] {};

			selection = MyEditorManagerUml.getDefault().getdecodeCourant().getUmlElement();
			
			if (selection == null)
				return new IContributionItem[] {};
			
			return new IContributionItem[] { createMenu() };
		} catch (Throwable e)
		{
			MyManager.printError(e);
			return new IContributionItem[] {};
		}
	}
	
	@Override
	public boolean isVisible()
	{
		try
		{
			editorPart = MyPluginManager.getActiveEditor();

			if (editorPart == null)
				return false;

			if (!MyEditorManagerUml.getDefault().isUmlLikeEditor())
				return false;
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
	
	protected IContributionItem createFunction()
	{
		Action ac = new createFunction("Create function", null, sim);
		ac.setToolTipText("Create a function identifier for simulation structure");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem createClkRef()
	{
		Action ac = new createClockref("Create clock reference", null, sim);
		ac.setToolTipText("Create clock reference for simulation");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem createSClock(Element e)
	{
		Action ac = new createSClock("Create a single clock", null, sim, e);
		ac.setToolTipText("Create a clock for the selected port");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem createMClock(Element e)
	{
		Action ac = new createMClock("Create multiple clock", null, sim, e);
		ac.setToolTipText("Create clocks for the selected class");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem createTrigger()
	{
		Action ac = new createTrigger("Create trigger", null, sim);
		ac.setToolTipText("Create trigger for an ADLFunction");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem createRequirement(Element e)
	{
		Action ac = new createRequirement("Create requirement", null, sim, e);
		ac.setToolTipText("Create some requirement");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	protected IContributionItem generateCCSL()
	{
		Action ac = new generateCCSL("Generate CCSL file", null, sim);
		ac.setToolTipText("Generate the .ccsl file for this simulation");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;
	}
	
	public void view_trace(){
		if (selection.getAppliedStereotypes()!=null) {
			if (selection.getAppliedStereotypes().size()!=0) {
				System.out.println(selection.getAppliedStereotypes().get(0).getName());
				//System.out.println(selection.getAppliedStereotypes().get(0).getAllAttributes());
				//selection.setValue(selection.getAppliedStereotypes().get(0), "nominal", new String("plop"));
				//Cumlelement ele=Cumlcreator.getInstance().createUmlElement(selection);
				//Object o=ele.getAttributeStereotype("RepetitionRate", "reference");
				//System.out.println("..." + (o instanceof EObject));
				//System.out.println(o.toString());

				for(Property p : selection.getAppliedStereotypes().get(0).getAllAttributes()) {
					Stereotype s_strt = selection.getAppliedStereotypes().get(0);
					Object s_val = selection.getValue(s_strt, p.getName());
					if(p.getAppliedStereotypes().isEmpty())
						if(s_val!=null) {
							System.out.println(p.getName()
									+" - (Valeur)"+s_val
									+" ("+s_val.getClass().getSimpleName()+")");
							if(p.getName().equals("reference")) {
								//ADLFlowPortImpl p = null;
								System.out.println();
								System.out.println(s_val.getClass().getSuperclass());
								System.out.println();
								for(Field f : s_val.getClass().getSuperclass().getDeclaredFields())
									System.out.println(Modifier.toString(f.getModifiers())
											+" "+f.getType().getSimpleName()+" "+f.getName());
								System.out.println();
								for(Method m : s_val.getClass().getSuperclass().getDeclaredMethods()) {
									System.out.println(Modifier.toString(m.getModifiers())
											+" "+m.getReturnType().getSimpleName()+" "+m.getName());
									if(m.getName().equals("getBase_Port"))
										try {
											System.out.println(((Port)m.invoke(s_val, new Object[]{})).getName());
										} catch (IllegalArgumentException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IllegalAccessException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (InvocationTargetException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								}
								System.out.println();

								System.out.println();
							}
						}
						else
							System.out.println(p.getName()
									+" - (Valeur)"+s_val);
					else {
						Stereotype p_strt = p.getAppliedStereotypes().get(0);
						if(s_val!=null)
							System.out.println(p.getName()
									+" - (Stéréotype)"+p_strt.getName()
									+" - (Valeur)"+s_val
									+" ("+s_val.getClass().getSimpleName()+")");
						else
							System.out.println(p.getName()
									+" - (Stéréotype)"+p_strt.getName()
									+" - (Valeur)"+s_val);
					}
				}
			}
		}
	}
	
	public void view_trace2(){
		Cumlelement ele=Cumlcreator.getInstance().createUmlElement(selection);
		
		if (ele.isStereotyped()) {
			System.out.println(selection.getAppliedStereotypes().get(0).getName()+"   ...   "+ele.getMotherName());
		}
	}
	
	
	protected MenuManager createMenu() {
		try
		{
			if(sim == null) {
				IEditorInput input = MyPluginManager.getActiveEditor().getEditorInput();
				IFile file = ((IFileEditorInput)input).getFile();
				sim = new SimulationStruct(file);
			}	

			Cumlelement celem=Cumlcreator.getInstance().createUmlElement(selection);

			view_trace2();

			MenuManager mm = new MenuManager("Generator menu ...", "fr.inria.ccsl.generator.menu");

			mm.add(new Separator("utils"));
			mm.add(new Separator("clocks"));
			mm.add(new Separator("requirements"));
			mm.add(new Separator("generation"));
			
			mm.appendToGroup("clocks", createClkRef());
			mm.appendToGroup("utils", createFunction());
			
			if(sim.getFunctions().isEmpty() || sim.getFunctions().size()<1)
				return mm;

			if(selection instanceof Property) {
				if(celem.isStereotyped()) {
					if(celem.isStereotypedBy("ADLInFlowPort") || celem.isStereotypedBy("ADLOutFlowPort"))
						mm.appendToGroup("clocks", createSClock(selection));
					if(celem.isStereotypedBy("ADLFunctionPrototype")) {
						mm.appendToGroup("clocks", createMClock(selection));
						mm.appendToGroup("requirements", createRequirement(selection));
					}
				}
			}

			if(selection instanceof org.eclipse.uml2.uml.Class) {
				if(celem.isStereotyped()) {
					if(celem.isStereotypedBy("ADLFunctionType")) {
						mm.appendToGroup("clocks", createMClock(selection));
						mm.appendToGroup("requirements", createTrigger());
						mm.appendToGroup("requirements", createRequirement(selection));
					}
					if(celem.isStereotypedBy("FunctionalDevice")) {
						mm.appendToGroup("clocks", createMClock(selection));
						mm.appendToGroup("requirements", createRequirement(selection));
					}
				}
			}

			mm.appendToGroup("generation", generateCCSL());
			
			return mm;
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void fill( Menu menu , int index ){super.fill(menu, index);}

	public Element getSelection(){return selection;}
}