package fr.inria.ccsl.generator.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Element;

import fr.inria.ccsl.generator.dialogs.DSingleClock;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class createSClock extends Action {
	private SimulationStruct sim = null;
	private Element elem = null;
	
	public createSClock(String text, ImageDescriptor image, SimulationStruct s, Element e)
	{
		super(text, image);
		sim = s;
		elem = e;
	}		
	
	@Override
	public void run()
	{
		Shell shell = new Shell();
		DSingleClock dcf = new DSingleClock(shell, sim, elem);
		
		dcf.create();
		int r = dcf.open();
		
		if (r == Window.OK)
			dcf.apply();
	}
}
