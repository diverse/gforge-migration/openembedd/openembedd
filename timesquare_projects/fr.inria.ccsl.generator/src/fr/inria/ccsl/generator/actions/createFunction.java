package fr.inria.ccsl.generator.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import fr.inria.ccsl.generator.dialogs.DFunction;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class createFunction extends Action {
	private SimulationStruct sim = null;
	
	public createFunction(String text, ImageDescriptor image, SimulationStruct s)
	{
		super(text, image);
		sim = s;
	}		
	
	@Override
	public void run()
	{
		Shell shell = new Shell();
		DFunction dcf = new DFunction(shell, sim);
		
		dcf.create();
		int r = dcf.open();
		
		if (r == Window.OK)
			dcf.apply();
	}
}
