package fr.inria.ccsl.generator.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.inria.ccsl.generator.dialogs.DGeneration;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class generateCCSL extends Action {
	private SimulationStruct sim = null;
	
	public generateCCSL(String text, ImageDescriptor image, SimulationStruct s)
	{
		super(text, image);
		sim = s;
	}		
	
	@Override
	public void run()
	{
		Shell shell = new Shell();
		DGeneration dcf = new DGeneration(shell, sim.toString());
		
		dcf.create();
		int r = dcf.open();
		
		if (r == Window.OK)
			dcf.apply(sim);
	}
}