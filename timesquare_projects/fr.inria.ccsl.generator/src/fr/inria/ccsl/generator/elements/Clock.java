package fr.inria.ccsl.generator.elements;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class Clock {
	private FunctionRep ctxt;
	private String name;
	private ClockReference clkref;
	
	public Clock(String n, ClockReference r) {
		name = n;
		clkref = r;
	}
	
	public FunctionRep getCtxt() {return ctxt;}
	public String getName() {return name;}	
	public ClockReference getClkref() {return clkref;}	
	
	public void setClkref(ClockReference c) {clkref = c;}
	
	@Override
	public String toString() {
		return getName()+" isSubClockOf "+getClkref().getName()+";";
	}
}