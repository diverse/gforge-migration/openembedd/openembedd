package fr.inria.ccsl.generator.elements;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class RepetitionRate extends Requirement{
	private Clock reference;
	
	public RepetitionRate(FunctionRep ctxt, String name, Clock reference, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		super(ctxt, name, lower, upper, jitter, nominal);
		this.reference = reference;
	}
	
	public Clock getReference() {return reference;}
		
	public void setReference(Clock reference) {this.reference = reference;}

	@Override
	public String toString(){
		String str = getReference().getName()+" isPeriodicOn "+getCtxt().getCtxt().getClkref().getName();
		if(getNominal() != null)
			str += " period "+getNominal();
		
		str += ";";
		
		if(getJitter() != null)
			str += "\n"+getReference().getName()+" hasStability "+getJitter()+";";
		
		return str;
	}
}