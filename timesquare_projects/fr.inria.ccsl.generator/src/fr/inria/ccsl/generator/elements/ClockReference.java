package fr.inria.ccsl.generator.elements;

import java.text.NumberFormat;

public class ClockReference {
	private String name;
	private Double unit;

	public ClockReference(String n, Double u) {
		if(n == "" || n == null)
			name = "cc";
		else
			name = n;
		if(u == null)
			unit = 0.0001;
		else
			unit = u;
	}

	public ClockReference(String n) {
		if(n == "" || n == null)
			name = "cc";
		else
			name = n;
		unit = 0.0001;
	}
	
	public ClockReference() {
		name = "cc";
		unit = 0.0001;
	}
	
	public String getName() {return name;}	
	public Double getUnit() {return unit;}
	
	@Override
	public String toString() {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(10);
		String str = nf.format(getUnit());
		str = str.replace(',', '.');
		
		return getName()+" = idealClk discretizedBy "+str+";";
	}
}