package fr.inria.ccsl.generator.elements;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class DelayRequirement extends Requirement {
	private Clock from;
	private Clock until;

	public DelayRequirement(FunctionRep ctxt, String name, Integer lower, Integer upper, Integer jitter, Integer nominal, Clock f, Clock u) {
		super(ctxt, name, lower, upper, jitter, nominal);
		from = f;
		until = u;
	}

	public Clock getFrom() {return from;}
	public Clock getUntil() {return until;}
	
	@Override
	public String toString() {
		String str ="";
		
		str += getName()+" = oneShotConstraint "+getCtxt().getCtxt().getClkref().getName()+", "+getFrom().getName()+", "+getUntil().getName()+", uniform ("+(getUpper()-getJitter())+", "+getUpper()+");";
		
		return str;
	}
}