package fr.inria.ccsl.generator.elements;

import java.util.List;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class Synchronization extends Requirement {
	private Clock fclock;
	private List<Clock> clocks;
	private boolean isInputS;

public Synchronization(FunctionRep ctxt, String name, Integer lower, Integer upper, Integer jitter, Integer nominal, List<Clock> clocks, boolean isIS) {
		super(ctxt, name, lower, upper, jitter, nominal);
		if(isIS) {
			getCtxt().createClock(getName()+"0");
			this.fclock = getCtxt().getClock(getName()+"0");
		}
		else {
			getCtxt().createClock(getName()+"0");
			this.fclock = getCtxt().getClock(getName()+"0");
		}

		this.clocks = clocks;
		this.isInputS = isIS;
		createUndcls();
	}

	public Clock getFclock() {return fclock;}
	public List<Clock> getClocks() {return clocks;}
	public boolean getType() {return isInputS;} // InputSynchronization if true

	public void setFclock(Clock fclock) {this.fclock = fclock;}
	public void setClocks(List<Clock> clocks) {this.clocks = clocks;}
	public void setType(boolean isIS) {this.isInputS = isIS;}
	
	public void createUndcls() {
		getCtxt().createUndcl(getName()+"i");
		getCtxt().createUndcl(getName()+"s");
		int i = 1;
		for(Clock c : getClocks()) {
			getCtxt().createUndcl(getName()+i);
			i++;
		}
	}
	
	@Override
	public String toString() {
		String str = "";
		
		if(getClocks().size() > 1){
			if(getType()){
				str += getName()+"i = inf (";
				for(Clock c : getClocks())
					str += c.getName()+", ";
				str = str.substring(0, str.lastIndexOf(","));
				str += ");\n";
				str += getName()+"s = sup (";
				for(Clock c : getClocks())
					str += c.getName()+", ";
				str = str.substring(0, str.lastIndexOf(","));
				str += ");\n\n";
			}
			else {
				str += getName()+"i = inf (";
				for(Clock c : getClocks())
					str += c.getName()+", ";
				str = str.substring(0, str.lastIndexOf(","));
				str += ");\n";
				str += getName()+"s = sup (";
				for(Clock c : getClocks())
					str += c.getName()+", ";
				str = str.substring(0, str.lastIndexOf(","));
				str += ");\n\n";
			}
		} 
		
		int i = 1;
		
		for(Clock c : getClocks()) {
			str += getName()+i+" = oneShotConstraint "+getCtxt().getCtxt().getClkref().getName();
			str += ", "+getFclock().getName()+", "+c.getName()+", uniform(1, "+getUpper()+");\n";
			i++;
		}
		
		return str;
	}
}