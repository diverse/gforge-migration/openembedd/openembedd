package fr.inria.ccsl.generator.elements;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class Trigger extends Requirement{
	private ClockReference base;
	
	public Trigger(FunctionRep ctxt, String name, ClockReference b, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		super(ctxt, name, lower, upper, jitter, nominal);
		base = b;
	}
	
	public ClockReference getBase() {return base;}

	public String toString() {
		String str = getName()+" isPeriodicOn "+getBase().getName();
		
		if(getNominal() != null)
			str += " period "+getNominal();
		
		str += ";";
		
		return str;
	}
}
