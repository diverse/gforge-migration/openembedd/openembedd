package fr.inria.ccsl.generator.elements;

import fr.inria.ccsl.generator.utils.struct.FunctionRep;

public class Requirement {
	private FunctionRep ctxt;
	private String name;
	private Integer lower;
	private Integer upper;
	private Integer jitter;
	private Integer nominal;
	
	public Requirement(FunctionRep ctxt, String name, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		this.ctxt = ctxt;
		this.name = name;
		this.lower = lower;
		this.upper = upper;
		this.jitter = jitter;
		this.nominal = nominal;
	}
	
	public FunctionRep getCtxt() {return ctxt;}
	public String getName() {return name;}
	public Integer getLower() {return lower;}
	public Integer getUpper() {return upper;}
	public Integer getJitter() {return jitter;}
	public Integer getNominal() {return nominal;}
	
	public void setName(String name) {this.name = name;}
	public void setLower(Integer lower) {this.lower = lower;}
	public void setUpper(Integer upper) {this.upper = upper;}
	public void setJitter(Integer jitter) {this.jitter = jitter;}
	public void setNominal(Integer nominal) {this.nominal = nominal;}
}
