package fr.inria.ccsl.generator.utils.struct;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.PartInitException;

import fr.inria.ccsl.generator.elements.Clock;
import fr.inria.ccsl.generator.elements.ClockReference;

public class SimulationStruct {
	private IFile input;
	private ClockReference clkref;
	private LinkedHashMap<String, FunctionRep> functions;
	
	public SimulationStruct(IFile in) {
		input = in;
		functions = new LinkedHashMap<String, FunctionRep>();
	}
	
	public void clear(){
		clkref = null;
		functions.clear();
	}
	
	public IFile getInput() {return input;}
	public ClockReference getClkref() {return clkref;}
	public LinkedHashMap<String, FunctionRep> getFunctions() {return functions;}
	
	public void createClkRef() {
		clkref = new ClockReference("cc", 0.0001);
		for(Entry<String, FunctionRep> e : getFunctions().entrySet())
			for(Clock c : e.getValue().getClocks())
				c.setClkref(clkref);
	}
	
	public void createClkRef(String name, Double unit) {
		clkref = new ClockReference(name, unit);
		for(Entry<String, FunctionRep> e : getFunctions().entrySet())
			for(Clock c : e.getValue().getClocks())
				c.setClkref(clkref);
	}
	
	public boolean createFunction(String f) {
		if(getFunctions().containsKey(f))
			return false;
		getFunctions().put(f, new FunctionRep(this, f));
		return true;
	}
	
	public FunctionRep getFunction(String f) {
		if(getFunctions().containsKey(f))
			return getFunctions().get(f);
		else
			createFunction(f);
		return getFunction(f);
	}
	
	public String wHeader() {
		String res = "//\n";
		res += "// CCSL file of\n";
		res += "// "+getInput().getLocation()+"\n";
		res += "//\n";
		res += "// Generated with CCSL Generator (For EAST-ADL2)\n";
		res += "// @Author: A. Berger\n";
		res += "//\n\n";
		
		return res;
	}
	
	public String oBlock() { return "{\n\n"; }
	public String cBlock() { return "}"; }
	
	@Override
	public String toString() {
		String res = "";
		res += wHeader();
		res += oBlock();
		
		if(getClkref() == null)
			res += "// Warning: no ideal clock defined\n\n";
		else
			res += getClkref().toString()+"\n\n";
		
		for(Entry<String, FunctionRep> e : getFunctions().entrySet())
			res += e.getValue().toString()+"\n\n";
		
		res += cBlock();
		
		return res;
	}
	
	public void generateCCSLFile() {
		IProject project = getInput().getProject();
		IFolder folder = project.getFolder("CCSL");
		String nom = getInput().getName().substring(0, 
				getInput().getName().lastIndexOf(getInput().getFileExtension())-1);
		IFile ccsl = folder.getFile(nom+"_"+SimulationStruct.idGen()+".ccsl");
		
		try {
			if (!folder.exists()) 
				folder.create(IResource.NONE, true, null);
			if (!ccsl.exists()) {
				byte[] b = toString().getBytes();
				ccsl.create(new ByteArrayInputStream(b), IResource.NONE, null);
			}
		}catch(CoreException e) {
			e.printStackTrace();
		}
		
		try {
			org.eclipse.ui.ide.IDE.openEditor(org.eclipse.ui.PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage(), ccsl, true);
		} catch (PartInitException e) {
			e.printStackTrace();
		}

		clear();
	}
	
	@SuppressWarnings("deprecation")
	public static String idGen() {
		Date d = new Date();
		return d.toGMTString().replaceAll("\\s|:", "");
	}	
}