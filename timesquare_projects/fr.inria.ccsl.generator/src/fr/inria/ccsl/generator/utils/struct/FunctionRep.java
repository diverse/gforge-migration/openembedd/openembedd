package fr.inria.ccsl.generator.utils.struct;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import fr.inria.ccsl.generator.elements.Clock;
import fr.inria.ccsl.generator.elements.DelayRequirement;
import fr.inria.ccsl.generator.elements.RepetitionRate;
import fr.inria.ccsl.generator.elements.Requirement;
import fr.inria.ccsl.generator.elements.Synchronization;
import fr.inria.ccsl.generator.elements.Trigger;

public class FunctionRep {
	private SimulationStruct ctxt;
	private String name;
	private Trigger trigger;
	private LinkedList<Clock> clocks;
	private LinkedList<Requirement> requirements;
	private LinkedList<Clock> undeclared;
	
	public FunctionRep(SimulationStruct ctxt, String name){
		this.ctxt = ctxt;
		this.name = name;
		trigger = null;
		clocks = new LinkedList<Clock>();
		undeclared = new LinkedList<Clock>();
		requirements = new LinkedList<Requirement>();
	}
	
	public SimulationStruct getCtxt() {return ctxt;}
	public String getName() {return name;}
	public Trigger getTrigger() {return trigger;}
	public LinkedList<Clock> getClocks() {return clocks;}
	public LinkedList<Requirement> getRequirements() {return requirements;}
	public LinkedList<Clock> getUndcls() {return undeclared;}
	
	public void setName(String name){this.name = name;}
	public void setTrigger(Trigger trigger) {this.trigger = trigger;}
	public void setClocks(LinkedList<Clock> clocks) {this.clocks = clocks;}
	public void setRequirements(LinkedList<Requirement> requirements) {this.requirements = requirements;}

	public void createTrigger(String name, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		trigger = new Trigger(this, name, getCtxt().getClkref(), lower, upper, jitter, nominal);
		createUndcl(name);
	}
	
	public void createClock(String name) {
		Clock c = new Clock(name, getCtxt().getClkref());
		boolean insert = true;
		
		for(Clock cl : getClocks())
			if(cl.toString().equals(c.toString()))
				insert = false;
		
		if(insert)
			getClocks().add(c);
	}
	
	public void createUndcl(String name) {
		Clock c = new Clock(name, getCtxt().getClkref());
		boolean insert = true;
		
		for(Clock cl : getUndcls())
			if(cl.toString().equals(c.toString()))
				insert = false;
		
		if(insert)
			getUndcls().add(c);
	}
	
	public void createClocks(LinkedHashMap<Clock, Boolean> cs) {
		for(Entry<Clock, Boolean> e : cs.entrySet())
			if(e.getValue())
				createClock(e.getKey().getName());
	}
	
	public Clock getClock(String n) {
		Clock c = new Clock(n, getCtxt().getClkref());
		
		for(Clock cl : getClocks())
			if(cl.toString().equals(c.toString()))
				return cl;
		
		return null;
	}
	
	public Clock getUndcl(String n) {
		Clock c = new Clock(n, getCtxt().getClkref());
		
		for(Clock cl : getUndcls())
			if(cl.toString().equals(c.toString()))
				return cl;
		
		return null;
	}
	
	public void createRepetitionRate(String name, String clkn, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		if(getClock(clkn) == null)
			createClock(clkn);
		
		RepetitionRate r = new RepetitionRate(this, name, getClock(clkn), lower, upper, jitter, nominal);
		boolean insert = true;
		
		for(Requirement rr : getRequirements())
			if(rr instanceof RepetitionRate)
				if(((RepetitionRate) rr).toString().equals(r.toString()))
					insert = false;
		
		if(insert)
			getRequirements().add(r);
	}
	
	public void createDelayRequirement(String name, String fr, String unt, Integer lower, Integer upper, Integer jitter, Integer nominal) {
		Clock from, until;
		from = getClock(fr);
		until = getClock(unt);
		
		if(from == null) {
			createUndcl(fr);
			from = getUndcl(fr);
		}
		if(until == null) {
			createUndcl(unt);
			from = getUndcl(unt);
		}
		
		DelayRequirement r = new DelayRequirement(this, name, lower, upper, jitter, nominal, from, until);
		boolean insert = true;
		
		for(Requirement rr : getRequirements())
			if(rr instanceof DelayRequirement)
				if(((DelayRequirement) rr).toString().equals(r.toString()))
					insert = false;
		
		if(insert)
			getRequirements().add(r);
	}
	
	public void createSynchronization(String name, Integer lower, Integer upper, Integer jitter, Integer nominal, LinkedHashMap<Clock, Boolean> clocks, boolean isIS) {
		LinkedList<Clock> li = new LinkedList<Clock>();
		
		for(Entry<Clock, Boolean> e : clocks.entrySet())
			if(e.getValue())
				if(getClock(e.getKey().getName()) != null)
					li.add(getClock(e.getKey().getName()));
				else {
					createClock(e.getKey().getName());
					li.add(getClock(e.getKey().getName()));
				}
		
		Synchronization s = new Synchronization(this, name, lower, upper, jitter, nominal, li, isIS);
		boolean insert = true;
		
		for(Requirement r : getRequirements())
			if(r instanceof Synchronization)
				if(((Synchronization) r).toString().equals(s.toString()))
					insert = false;
		
		if(insert)
			getRequirements().add(s);
	}
	
	@Override
	public String toString() {
		String str = "// Fonction "+getName()+"\n//\n\n";
		
		for(Clock c : getClocks())
			str += c.toString()+"\n";
		
		str += "\n";
		
		if(getTrigger() != null)
			str += getTrigger().toString()+"\n";
		
		str += "\n";
		
		for(Requirement r : getRequirements())
			str += r.toString()+"\n";
		
		str += "\n// Fin de la fonction "+getName();
		
		return str;
	}
}
