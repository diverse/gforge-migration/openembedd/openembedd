package fr.inria.ccsl.generator.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class DFunction extends Dialog {
	private SimulationStruct sim;
	private String name;
	private Text namev;
	
	public DFunction(Shell shell, SimulationStruct s) {
		super(shell);
		sim = s;
	}
	
	public void apply() {
		sim.createFunction(name);
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("Function");
	}
	
	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		final int fid = id;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(fid == IDialogConstants.OK_ID) {
					name = namev.getText();
				}
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		if(id == IDialogConstants.CANCEL_ID) {
			GridData data = new GridData();
		    data.horizontalSpan = 2;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;
		    button.setLayoutData(data);
		}
		if(id == IDialogConstants.OK_ID) {
			GridData data = new GridData();
			data.widthHint = 50;
			data.horizontalAlignment = SWT.CENTER;
			button.setLayoutData(data);
		}
	    
		return button;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		createButtonsForButtonBar(parent);
		return parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		parent.setLayout(layout);
		
		Label l = new Label(parent, SWT.NONE);
		l.setText("Name");
		l.setSize(40, 10);
		GridData data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		namev = new Text(parent, SWT.BORDER);
		namev.setBackground(new Color(null, 200,200,200));
		namev.setSize(150, 10);
		data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 160;
		data.horizontalAlignment = SWT.LEFT;
		namev.setLayoutData(data);
		
		return parent;
	}
}
