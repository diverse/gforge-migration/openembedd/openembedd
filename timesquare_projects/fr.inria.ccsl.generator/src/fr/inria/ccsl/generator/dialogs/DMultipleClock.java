package fr.inria.ccsl.generator.dialogs;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.internal.impl.ClassImpl;

import code.Cumlcreator;
import code.Cumlelement;

import fr.inria.ccsl.generator.elements.Clock;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

@SuppressWarnings("restriction")
public class DMultipleClock extends Dialog {
	private SimulationStruct sim;
	private Element elem;
	private String fname;
	private Combo fnamev;
	private LinkedHashMap<Clock, Boolean> clocks = new LinkedHashMap<Clock, Boolean>();
	private LinkedHashMap<Clock, Button> sc = new LinkedHashMap<Clock, Button>();
	
	public DMultipleClock(Shell shell, SimulationStruct s, Element e) {
		super(shell);
		sim = s;
		elem = e;
	}

	public void apply() {
		sim.getFunction(fname).createClocks(clocks);
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("Clocks");
	}
	
	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		final int fint = id;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(fint == IDialogConstants.OK_ID) {
					fname = fnamev.getItem(fnamev.getSelectionIndex());
					for(Entry<Clock, Button> e : sc.entrySet())
						clocks.put(e.getKey(), e.getValue().getSelection());
				}
				
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		if(id == IDialogConstants.CANCEL_ID) {
			GridData data = new GridData();
		    data.horizontalSpan = 4;
		    data.widthHint = 60;
		    data.horizontalAlignment = SWT.CENTER;
		    button.setLayoutData(data);
		}
		if(id == IDialogConstants.OK_ID) {
			GridData data = new GridData();
			data.horizontalSpan = 2;
			data.widthHint = 60;
			data.horizontalAlignment = SWT.CENTER;
			button.setLayoutData(data);
		}
	    
		return button;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.widthHint = 10;
		b.setLayoutData(data);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);	
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 100;
		b.setLayoutData(data);
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		b.setLayoutData(data);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		createButtonsForButtonBar(parent);
		return parent;
	}
	
	public void chose(Composite parent) {
		for(Entry<Clock, Boolean> e : clocks.entrySet()) {
			Label b = new Label(parent, SWT.NONE);
			GridData data = new GridData();
			data.horizontalSpan = 2;
			data.widthHint = 20;
			b.setLayoutData(data);
			
			Button bt = new Button(parent, SWT.CHECK);
			if(e.getValue())
				bt.setSelection(true);
			data = new GridData();
			data.widthHint = 10;
			bt.setLayoutData(data);
			
			b = new Label(parent, SWT.NONE);
			data = new GridData();
			data.widthHint = 10;
			b.setLayoutData(data);

			Text name = new Text(parent, SWT.BORDER);
			name.setBackground(new Color(null, 200,200,200));
			name.setSize(180, 10);
			name.setText(e.getKey().getName());
			data = new GridData();
			data.horizontalSpan = 5;
			data.widthHint = 200;
			data.horizontalAlignment = SWT.LEFT;
			name.setLayoutData(data);
			
			sc.put(e.getKey(), bt);
		}
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 9;
		parent.setLayout(layout);
		
		Label name = new Label(parent, SWT.NONE);
		name.setText("Parent function");
		name.setSize(80, 10);
		GridData data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		name.setLayoutData(data);
		
		fnamev = new Combo(parent, SWT.READ_ONLY);
		fnamev.setSize(140, 10);
		for(String s : sim.getFunctions().keySet())
			fnamev.add(s);
		fnamev.select(0);
		data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fnamev.setLayoutData(data);
		
		name = new Label(parent, SWT.LEFT);
		name.setText("Select clocks : ");
		name.setSize(220, 10);
		data = new GridData();
		data.horizontalSpan = 9;
		data.widthHint = 240;
		data.horizontalAlignment = SWT.CENTER;
		name.setLayoutData(data);
		
		if(sim.getClkref() == null || sim.getClkref().toString()  == "-1")
			sim.createClkRef();
		
		EList<Property> l = ((ClassImpl)elem).getAttributes();		
		
		if(!l.isEmpty())
			for(Property p : l) {
				Cumlelement celem=Cumlcreator.getInstance().createUmlElement(p);
				if(celem.isStereotypedBy("ADLInFlowPort") || celem.isStereotypedBy("ADLOutFlowPort")) {
					String str = "";
					
					if(celem.isStereotypedBy("ADLInFlowPort"))
						str += "in";
					if(celem.isStereotypedBy("ADLOutFlowPort"))
						str += "out";
					
					str += p.getValue(p.getAppliedStereotypes().get(0), "name").toString();
					
					clocks.put(new Clock(str, sim.getClkref()), true);
				}
			}
		
		chose(parent);
		
		return parent;
	}
}
