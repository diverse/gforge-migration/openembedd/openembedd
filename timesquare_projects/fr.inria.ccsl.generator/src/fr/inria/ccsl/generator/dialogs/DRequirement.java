package fr.inria.ccsl.generator.dialogs;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

import code.Cumlcreator;
import code.Cumlelement;

import fr.inria.ccsl.generator.elements.Clock;
import fr.inria.ccsl.generator.utils.struct.FunctionRep;
import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class DRequirement extends Dialog {
	private SimulationStruct sim;
	private Element elem;
	private int type;
	private String fname, rname, sfromdr, suntildr;
	private Integer lower, upper, jitter, nominal;
	private boolean isis = false;
	private Combo fnamerrv, fnamedrv, fnamesyv, fromdr, untildr;
	private Text namerr, namedr, namesy;
	private Text lowerrr, lowerdr, lowersy;
	private Text upperrr, upperdr, uppersy;
	private Text jitterrr, jitterdr, jittersy;
	private Text nominalrr, nominaldr, nominalsy;
	private Button isisv;
	private LinkedHashMap<Clock, Boolean> clocks;
	private LinkedHashMap<Button, Clock> selclkrr, selclksy;
	private LinkedHashMap<Button, Text> abtrr, abtsy;
	
	public DRequirement(Shell shell, SimulationStruct s, Element e) {
		super(shell);
		sim = s;
		elem = e;
		type = -1;
		clocks = new LinkedHashMap<Clock, Boolean>();
		selclkrr = new LinkedHashMap<Button, Clock>();
		selclksy = new LinkedHashMap<Button, Clock>();
		abtrr = new LinkedHashMap<Button, Text>();
		abtsy = new LinkedHashMap<Button, Text>();
	}
	
	public void apply() {
		if(type == 2) {
			if(sim.getClkref() == null || sim.getClkref().toString()  == "-1")
				sim.createClkRef();
			
			for(Entry<Clock, Boolean> e : clocks.entrySet())
				if(e.getValue())
					sim.getFunction(fname).createRepetitionRate(rname, e.getKey().getName(), lower, upper, jitter, nominal);
		}
		if(type == 4) {
			if(sim.getClkref() == null || sim.getClkref().toString()  == "-1")
				sim.createClkRef();		
			
			sim.getFunction(fname).createDelayRequirement(rname, sfromdr, suntildr, lower, upper, jitter, nominal);
		}
		if(type == 6) {
			if(sim.getClkref() == null || sim.getClkref().toString()  == "-1")
				sim.createClkRef();
			
			for(Entry<Clock, Boolean> e : clocks.entrySet())
				if(e.getValue())
					sim.getFunction(fname).createSynchronization(rname, lower, upper, jitter, nominal, clocks, isis);
		}
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("Requirements");
	}
	
	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		final int fint = id;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		if(id == 2 || id == 4 || id == 6)
			button.setData(new Integer(IDialogConstants.OK_ID));
		else
			button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(fint == 2) {
					type = 2;
					fname = fnamerrv.getItem(fnamerrv.getSelectionIndex());
					rname = namerr.getText();
					if(lowerrr.getText() != "" && lowerrr.getText() != null)
						lower = Integer.parseInt(lowerrr.getText());
					if(upperrr.getText() != "" && upperrr.getText() != null)
						upper = Integer.parseInt(upperrr.getText());
					if(jitterrr.getText() != "" && jitterrr.getText() != null)
						jitter = Integer.parseInt(jitterrr.getText());
					if(nominalrr.getText() != "" && nominalrr.getText() != null)
						nominal = Integer.parseInt(nominalrr.getText());
					
					clocks.clear();
					
					for(Entry<Button, Clock> e : selclkrr.entrySet())
						if(e.getKey().getSelection())
							clocks.put(new Clock(((Text)abtrr.get(e.getKey())).getText(), sim.getClkref()), e.getKey().getSelection());
				}
				if(fint == 4) {
					type = 4;
					fname = fnamedrv.getItem(fnamerrv.getSelectionIndex());
					sfromdr = fromdr.getItem(fromdr.getSelectionIndex());
					suntildr = untildr.getItem(untildr.getSelectionIndex());
					rname = namedr.getText();
					if(lowerdr.getText() != "" && lowerdr.getText() != null)
						lower = Integer.parseInt(lowerdr.getText());
					if(upperdr.getText() != "" && upperdr.getText() != null)
						upper = Integer.parseInt(upperdr.getText());
					if(jitterdr.getText() != "" && jitterdr.getText() != null)
						jitter = Integer.parseInt(jitterdr.getText());
					if(nominaldr.getText() != "" && nominaldr.getText() != null)
						nominal = Integer.parseInt(nominaldr.getText());
				}
				if(fint == 6) {
					type = 6;
					rname = namesy.getText();
					fname = fnamesyv.getItem(fnamesyv.getSelectionIndex());
					isis = isisv.getSelection();
					if(lowersy.getText() != "" && lowersy.getText() != null)
						lower = Integer.parseInt(lowersy.getText());
					if(uppersy.getText() != "" && uppersy.getText() != null)
						upper = Integer.parseInt(uppersy.getText());
					if(jittersy.getText() != "" && jittersy.getText() != null)
						jitter = Integer.parseInt(jittersy.getText());
					if(nominalsy.getText() != "" && nominalsy.getText() != null)
						nominal = Integer.parseInt(nominalsy.getText());
					
					clocks.clear();
					
					for(Entry<Button, Clock> e : selclksy.entrySet())
						if(e.getKey().getSelection())
							clocks.put(new Clock(((Text)abtsy.get(e.getKey())).getText(), sim.getClkref()), e.getKey().getSelection());
				}
				
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		
		GridData data = new GridData();
		if(id == 1 || id == 3 || id == 5) {			
		    data.horizontalSpan = 2;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;		    
		}
		if(id == 2 || id == 4 || id == 6) {			
		    data.horizontalSpan = 4;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;		    
		}
		button.setLayoutData(data);
	    
		return button;
	}
	
	protected void createButtonsForButtonBar(Composite parent, int i) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 60;
		
		b.setLayoutData(data);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);	
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		
		b.setLayoutData(data);
		createButton(parent, (i+1), IDialogConstants.OK_LABEL, true);
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		
		b.setLayoutData(data);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		//createButtonsForButtonBar(parent);
		return parent;
	}
	
	protected Control createButtonBarb(Composite parent, int i) {
		createButtonsForButtonBar(parent, i);
		return parent;
	}
	
	private void chose(Composite parent, int ind) {
		boolean pair = false;
		for(Entry<Clock, Boolean> e : clocks.entrySet()) {
			if(!pair) {
				Label b = new Label(parent, SWT.NONE);
				GridData data = new GridData();
				data.horizontalSpan = 2;
				data.widthHint = 40;
				b.setLayoutData(data);

				Button bt = new Button(parent, SWT.CHECK);
				if(e.getValue())
					bt.setSelection(true);
				data = new GridData();
				data.widthHint = 10;
				bt.setLayoutData(data);

				b = new Label(parent, SWT.NONE);
				data = new GridData();
				data.widthHint = 10;
				b.setLayoutData(data);

				Text name = new Text(parent, SWT.BORDER);
				name.setBackground(new Color(null, 200,200,200));
				name.setSize(50, 10);
				name.setText(e.getKey().getName());
				data = new GridData();
				data.horizontalSpan = 2;
				data.widthHint = 50;
				data.horizontalAlignment = SWT.LEFT;
				name.setLayoutData(data);
				
				if(ind == 1) {
					selclkrr.put(bt, e.getKey());
					abtrr.put(bt, name);
				}
				
				if(ind == 3) {
					selclksy.put(bt, e.getKey());
					abtsy.put(bt, name);
				}
			}
			else {
				Label b = new Label(parent, SWT.NONE);
				GridData data = new GridData();
				data.widthHint = 30;
				b.setLayoutData(data);
				
				Button bt = new Button(parent, SWT.CHECK);
				if(e.getValue())
					bt.setSelection(true);
				data = new GridData();
				data.widthHint = 10;
				bt.setLayoutData(data);
				
				b = new Label(parent, SWT.NONE);
				data = new GridData();
				data.widthHint = 10;
				b.setLayoutData(data);

				Text name = new Text(parent, SWT.BORDER);
				name.setBackground(new Color(null, 200,200,200));
				name.setSize(50, 10);
				name.setText(e.getKey().getName());
				data = new GridData();
				data.horizontalSpan = 4;
				data.widthHint = 50;
				data.horizontalAlignment = SWT.LEFT;
				name.setLayoutData(data);
				
				b = new Label(parent, SWT.NONE);
				data = new GridData();
				data.horizontalSpan = 2;
				data.widthHint = 40;
				b.setLayoutData(data);
				
				if(ind == 1) {
					selclkrr.put(bt, e.getKey());
					abtrr.put(bt, name);
				}
				
				if(ind == 3) {
					selclksy.put(bt, e.getKey());
					abtsy.put(bt, name);
				}
			}
			
			pair = !pair;
		}
		if(pair) {
			Label b = new Label(parent, SWT.NONE);
			GridData data = new GridData();
			data.horizontalSpan = 9;
			data.widthHint = 140;
			b.setLayoutData(data);
		}
	}

	@SuppressWarnings("unchecked")
	private Control getTabRRControl(TabFolder tabFolder) {
	    Composite composite = new Composite(tabFolder, SWT.NONE);
	    GridLayout layout = new GridLayout();
		layout.numColumns = 15;
		composite.setLayout(layout);
		
		Label l = new Label(composite, SWT.NONE);
		l.setText("Parent function");
		l.setSize(80, 10);
		GridData data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fnamerrv = new Combo(composite, SWT.READ_ONLY);
		fnamerrv.setSize(140, 10);
		for(String s : sim.getFunctions().keySet())
			fnamerrv.add(s);
		fnamerrv.select(0);
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fnamerrv.setLayoutData(data);
		
		///////////////// Name
	    
	    l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Name");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		namerr = new Text(composite, SWT.NONE);
		namerr.setSize(180, 10);
		namerr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 180;
		namerr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Reference
		
		if(elem instanceof Property) {
			Cumlelement ele=Cumlcreator.getInstance().createUmlElement(elem);
			Object o=ele.getAttributeStereotype("ADLFunctionPrototype", "type");
			
			try {				
				EList li = (EList) o.getClass().getMethod("getFlowPort",new Class[]{}).invoke(o, new Object[]{});
				
				if(!li.isEmpty()){
					boolean a = false;
					for(Object p : li) {
						a = false;
						if(p.getClass().getSimpleName().equals("ADLInFlowPortImpl")
								|| p.getClass().getSimpleName().equals("ADLOutFlowPortImpl")) {
							String str = "";

							if(p.getClass().getSimpleName().equals("ADLInFlowPortImpl")){
								str += "in";
								a = true;
							}
							if(p.getClass().getSimpleName().equals("ADLOutFlowPortImpl"))
								str += "out";
							
							Port po = (Port) p.getClass().getMethod("getBase_Port", new Class[]{}).invoke(p, new Object[]{});
							str += po.getName();

							clocks.put(new Clock(str, sim.getClkref()), a);
						}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}			
		}
		
		if(elem instanceof org.eclipse.uml2.uml.Class) {
			if(sim.getClkref() == null || sim.getClkref().toString()  == "-1")
				sim.createClkRef();
			
			EList<Property> li = ((org.eclipse.uml2.uml.Class)elem).getAttributes();

			if(!li.isEmpty()){
				boolean a = false;
				for(Property p : li) {
					a = false;
					if(p.getAppliedStereotypes().get(0).getName().equals("ADLInFlowPort") || p.getAppliedStereotypes().get(0).getName().equals("ADLOutFlowPort")) {
						String str = "";

						if(p.getAppliedStereotypes().get(0).getName().equals("ADLInFlowPort")){
							str += "in";
							a = true;
						}
						if(p.getAppliedStereotypes().get(0).getName().equals("ADLOutFlowPort"))
							str += "out";

						str += p.getValue(p.getAppliedStereotypes().get(0), "name").toString();

						clocks.put(new Clock(str, sim.getClkref()), a);
					}
				}
			}
		}
		
		chose(composite, 1);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Lower & Upper
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Lower");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		lowerrr = new Text(composite, SWT.NONE);
		lowerrr.setSize(50, 10);
		lowerrr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		lowerrr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Upper");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		upperrr = new Text(composite, SWT.NONE);
		upperrr.setSize(50, 10);
		upperrr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		upperrr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Jitter & Nominal
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Jitter");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		jitterrr = new Text(composite, SWT.NONE);
		jitterrr.setSize(50, 10);
		jitterrr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		jitterrr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Nominal");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		nominalrr = new Text(composite, SWT.NONE);
		nominalrr.setSize(50, 10);
		nominalrr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		nominalrr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Button bar
		
		createButtonBarb(composite, 1);
		
	    return composite;
	  }
	
	private Control getTabDRControl(TabFolder tabFolder) {
		Composite composite = new Composite(tabFolder, SWT.NONE);
	    GridLayout layout = new GridLayout();
		layout.numColumns = 15;
		composite.setLayout(layout);
		
		///////////////// Space
		
		Label l = new Label(composite, SWT.NONE);
		GridData data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Parent function");
		l.setSize(80, 10);
		data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fnamedrv = new Combo(composite, SWT.READ_ONLY);
		fnamedrv.setSize(140, 10);
		for(String s : sim.getFunctions().keySet())
			fnamedrv.add(s);
		fnamedrv.select(0);
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fnamedrv.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Name
	    
	    l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Name");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		namedr = new Text(composite, SWT.NONE);
		namedr.setSize(180, 10);
		namedr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 180;
		namedr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// From et Until
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("From");
		l.setSize(80, 10);
		data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fromdr = new Combo(composite, SWT.READ_ONLY);
		fromdr.setSize(140, 10);
		for(Entry<String, FunctionRep> e : sim.getFunctions().entrySet()){
			for(Clock c : e.getValue().getClocks())
				fromdr.add(c.getName());
			for(Clock c : e.getValue().getUndcls())
				fromdr.add(c.getName());
		}			
		fromdr.select(0);
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fromdr.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Until");
		l.setSize(80, 10);
		data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		untildr = new Combo(composite, SWT.READ_ONLY);
		untildr.setSize(140, 10);
		for(Entry<String, FunctionRep> e : sim.getFunctions().entrySet()){
			for(Clock c : e.getValue().getClocks())
				untildr.add(c.getName());
			for(Clock c : e.getValue().getUndcls())
				untildr.add(c.getName());
		}		
		untildr.select(0);
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		untildr.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Lower & Upper
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Lower");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		lowerdr = new Text(composite, SWT.NONE);
		lowerdr.setSize(50, 10);
		lowerdr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		lowerdr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Upper");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		upperdr = new Text(composite, SWT.NONE);
		upperdr.setSize(50, 10);
		upperdr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		upperdr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Jitter & Nominal
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Jitter");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		jitterdr = new Text(composite, SWT.NONE);
		jitterdr.setSize(50, 10);
		jitterdr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		jitterdr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Nominal");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		nominaldr = new Text(composite, SWT.NONE);
		nominaldr.setSize(50, 10);
		nominaldr.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		nominaldr.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Button bar
		
		createButtonBarb(composite, 3);
		
		return composite;
	}
	
	private Control getTabSControl(TabFolder tabFolder) {
		Composite composite = new Composite(tabFolder, SWT.NONE);
	    GridLayout layout = new GridLayout();
		layout.numColumns = 15;
		composite.setLayout(layout);
		
		Label l = new Label(composite, SWT.NONE);
		l.setText("Parent function");
		l.setSize(80, 10);
		GridData data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fnamesyv = new Combo(composite, SWT.READ_ONLY);
		fnamesyv.setSize(140, 10);
		for(String s : sim.getFunctions().keySet())
			fnamesyv.add(s);
		fnamesyv.select(0);
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fnamesyv.setLayoutData(data);
		
		///////////////// Name
	    
	    l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Name");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		namesy = new Text(composite, SWT.NONE);
		namesy.setSize(180, 10);
		namesy.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 10;
		data.widthHint = 180;
		namesy.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Type
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		isisv = new Button(composite, SWT.RADIO);
		isisv.setSelection(true);
		data = new GridData();
		data.widthHint = 10;
		isisv.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Input");
		l.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		Button bt = new Button(composite, SWT.RADIO);
		data = new GridData();
		data.widthHint = 10;
		bt.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Output");
		l.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Reference
		
		chose(composite, 3);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Lower & Upper
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Lower");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		lowersy = new Text(composite, SWT.NONE);
		lowersy.setSize(50, 10);
		lowersy.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		lowersy.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Upper");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		uppersy = new Text(composite, SWT.NONE);
		uppersy.setSize(50, 10);
		uppersy.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		uppersy.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Jitter & Nominal
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Jitter");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		jittersy = new Text(composite, SWT.NONE);
		jittersy.setSize(50, 10);
		jittersy.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		jittersy.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		l.setText("Nominal");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		nominalsy = new Text(composite, SWT.NONE);
		nominalsy.setSize(50, 10);
		nominalsy.setBackground(new Color(null, 200,200,200));
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		nominalsy.setLayoutData(data);
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.widthHint = 10;
		l.setLayoutData(data);
		
		///////////////// Space
		
		l = new Label(composite, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 15;
		data.widthHint = 250;
		l.setLayoutData(data);
		
		///////////////// Button bar
		
		createButtonBarb(composite, 5);
		
		return composite;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		TabFolder c = new TabFolder(parent, SWT.TOP);
		c.setLayoutData(new GridData(GridData.FILL_BOTH));
	 
	    TabItem t = new TabItem(c, SWT.NONE);
	    t.setText("Repetition rate");
	    t.setControl(getTabRRControl(c));
	    
	    t = new TabItem(c, SWT.NONE);
	    t.setText("Delay requirement");
	    t.setControl(getTabDRControl(c));
	    
	    t = new TabItem(c, SWT.NONE);
	    t.setText("Synchronization");
	    t.setControl(getTabSControl(c));
		
		return parent;
	}
}