package fr.inria.ccsl.generator.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.uml2.uml.Element;

import code.Cumlcreator;
import code.Cumlelement;

import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class DSingleClock extends Dialog {
	private SimulationStruct sim;
	private Element elem;
	private String fname, clkname;
	private Combo fnamev;
	private Text namev;
	

	public DSingleClock(Shell shell, SimulationStruct s, Element e) {
		super(shell);
		sim = s;
		elem = e;
	}
	
	public void apply() {
		sim.getFunction(fname).createClock(clkname);
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("Clock");
	}
	
	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		final int fid = id;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(fid == IDialogConstants.OK_ID) {
					clkname = namev.getText();
					fname = fnamev.getItem(fnamev.getSelectionIndex());
				}
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		if(id == IDialogConstants.CANCEL_ID) {
			GridData data = new GridData();
		    data.horizontalSpan = 4;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;
		    button.setLayoutData(data);
		}
		if(id == IDialogConstants.OK_ID) {
			GridData data = new GridData();
			data.widthHint = 50;
			data.horizontalAlignment = SWT.CENTER;
			button.setLayoutData(data);
		}
	    
		return button;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 50;
		b.setLayoutData(data);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		createButtonsForButtonBar(parent);
		return parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 8;
		parent.setLayout(layout);
		
		Label l = new Label(parent, SWT.NONE);
		l.setText("Parent function");
		l.setSize(80, 10);
		GridData data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fnamev = new Combo(parent, SWT.READ_ONLY);
		fnamev.setSize(140, 10);
		for(String s : sim.getFunctions().keySet())
			fnamev.add(s);
		fnamev.select(0);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 150;
		data.horizontalAlignment = SWT.CENTER;
		fnamev.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Clock name : ");
		l.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 70;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		String str = "";
		
		Cumlelement celem=Cumlcreator.getInstance().createUmlElement(elem);
		
		if(celem.isStereotypedBy("ADLInFlowPort"))
			str += "in";
		if(celem.isStereotypedBy("ADLOutFlowPort"))
			str += "out";
		
		str += elem.getValue(elem.getAppliedStereotypes().get(0), "name").toString();
		
		namev = new Text(parent, SWT.BORDER);
		namev.setBackground(new Color(null, 200,200,200));
		namev.setSize(170, 10);
		namev.setText(str);
		data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 180;
		data.horizontalAlignment = SWT.LEFT;
		namev.setLayoutData(data);
		
		return parent;
	}
}