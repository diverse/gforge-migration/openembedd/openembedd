package fr.inria.ccsl.generator.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class DGeneration extends Dialog {
	private Text overview;
	private String ccsl;
	
	public DGeneration(Shell parentShell, String s) {
		super(parentShell);
		ccsl = s;
	}
	
	public void apply(SimulationStruct sim) {
		sim.generateCCSLFile();
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("CCSL Overview");
	}
	
	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		GridData data = new GridData();
		data.widthHint = 50;
		data.horizontalAlignment = SWT.CENTER;
		button.setLayoutData(data);
		
		return button;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.widthHint = 75;
		b.setLayoutData(data);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 100;
		b.setLayoutData(data);
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		createButtonsForButtonBar(parent);
		return parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 5;
		parent.setLayout(layout);
		
		overview = new Text(parent, SWT.MULTI|SWT.WRAP|SWT.V_SCROLL);
		overview.setEditable(false);
		overview.setText(ccsl);
		overview.setBackground(new Color(null, 200,200,200));
		overview.setSize(330, 480);
		GridData data = new GridData();
		data.horizontalSpan = 5;
		data.widthHint = 350;
		data.heightHint = 500;
		data.horizontalAlignment = SWT.CENTER;
		overview.setLayoutData(data);
		
		return parent;
	}
}