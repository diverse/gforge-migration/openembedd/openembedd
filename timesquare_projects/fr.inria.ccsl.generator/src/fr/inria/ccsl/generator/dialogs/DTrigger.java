package fr.inria.ccsl.generator.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.ccsl.generator.utils.struct.SimulationStruct;

public class DTrigger extends Dialog {
	private SimulationStruct sim;
	private String fname, tname;
	private Integer upper, lower, nominal, jitter;
	private Text namev, upperv, lowerv, nominalv, jitterv;
	private Combo fnamev;
	
	public DTrigger(Shell shell, SimulationStruct s) {
		super(shell);
		sim = s;
	}

	public void apply() {
		sim.getFunction(fname).createTrigger(tname, lower, upper, jitter, nominal);
	}
	
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText("Trigger");
	}
	
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		final int fid = id;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(fid == IDialogConstants.OK_ID) {
					tname = namev.getText();
					fname = fnamev.getItem(fnamev.getSelectionIndex());
					if(upperv.getText() != "" && upperv.getText() != null)
						upper = Integer.parseInt(upperv.getText());
					if(lowerv.getText() != "" && lowerv.getText() != null)
						lower = Integer.parseInt(lowerv.getText());
					if(nominalv.getText() != "" && nominalv.getText() != null)
						nominal = Integer.parseInt(nominalv.getText());
					if(jitterv.getText() != "" && jitterv.getText() != null)
						jitter = Integer.parseInt(jitterv.getText());
				}
				
				buttonPressed(((Integer) event.widget.getData()).intValue());
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		
		GridData data = new GridData();
		if(id == IDialogConstants.CANCEL_ID) {			
		    data.horizontalSpan = 2;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;		    
		}
		if(id == IDialogConstants.OK_ID) {			
		    data.horizontalSpan = 3;
		    data.widthHint = 50;
		    data.horizontalAlignment = SWT.CENTER;		    
		}
		button.setLayoutData(data);
	    
		return button;
	}
	
	protected void createButtonsForButtonBar(Composite parent) {
		Label b = new Label(parent, SWT.NONE);
		GridData data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);	
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 50;
		b.setLayoutData(data);
		
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		
		b = new Label(parent, SWT.NONE);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 50;
		b.setLayoutData(data);
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		createButtonsForButtonBar(parent);
		return parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 12;
		parent.setLayout(layout);
		
		Label l = new Label(parent, SWT.NONE);
		l.setText("Parent function");
		l.setSize(80, 10);
		GridData data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 100;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		fnamev = new Combo(parent, SWT.READ_ONLY);
		fnamev.setSize(180, 10);
		for(String s : sim.getFunctions().keySet())
			fnamev.add(s);
		fnamev.select(0);
		data = new GridData();
		data.horizontalSpan = 8;
		data.widthHint = 190;
		data.horizontalAlignment = SWT.CENTER;
		fnamev.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Name");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		namev = new Text(parent, SWT.BORDER);
		namev.setBackground(new Color(null, 200,200,200));
		namev.setSize(220, 10);
		data = new GridData();
		data.horizontalSpan = 9;
		data.widthHint = 230;
		data.horizontalAlignment = SWT.LEFT;
		namev.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Upper");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		upperv = new Text(parent, SWT.BORDER);
		upperv.setBackground(new Color(null, 200,200,200));
		upperv.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.RIGHT;
		upperv.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Lower");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		lowerv = new Text(parent, SWT.BORDER);
		lowerv.setBackground(new Color(null, 200,200,200));
		lowerv.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 70;
		data.horizontalAlignment = SWT.CENTER;
		lowerv.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Nominal");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 3;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.CENTER;
		l.setLayoutData(data);
		
		nominalv = new Text(parent, SWT.BORDER);
		nominalv.setBackground(new Color(null, 200,200,200));
		nominalv.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 60;
		data.horizontalAlignment = SWT.RIGHT;
		nominalv.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		data = new GridData();
		data.widthHint = 30;
		l.setLayoutData(data);
		
		l = new Label(parent, SWT.NONE);
		l.setText("Jitter");
		l.setSize(40, 10);
		data = new GridData();
		data.horizontalSpan = 2;
		data.widthHint = 40;
		l.setLayoutData(data);
		
		jitterv = new Text(parent, SWT.BORDER);
		jitterv.setBackground(new Color(null, 200,200,200));
		jitterv.setSize(50, 10);
		data = new GridData();
		data.horizontalSpan = 4;
		data.widthHint = 70;
		data.horizontalAlignment = SWT.CENTER;
		jitterv.setLayoutData(data);
		
		return parent;
	}
}
