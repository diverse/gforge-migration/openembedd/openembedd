/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;

import fr.inria.base.MyManager;

final public class MonLog {
	private PrintWriter pout, perr;

	static private HashMap<String, MonLog> cache = new HashMap<String, MonLog>();

	static public MonLog createLog(String id, Writer out, Writer err) {
		MonLog log = new MonLog(out, err);
		cache.put(id, log);
		return log;
	}

	static public MonLog getLog(String id) {
		MonLog log = cache.get(id);
		if (log == null) {
			try {
				log = createLog(id, new FileWriter(id + ".txt"),
						new FileWriter(id + ".err.txt"));
				cache.put(id, log);
			} catch (IOException ex) {
				MyManager.printError(ex);
			}
		}
		return log;
	}

	final public PrintWriter perr() {
		return perr;
	}

	private MonLog(Writer out, Writer err) {
		pout = new PrintWriter(out);
		if (out != err)
			perr = new PrintWriter(err);
		else
			perr = pout;
	}

	public void println(Object msg) {
		this.println(msg.toString());
	}

	public void println(String msg) {
		pout.println(msg);
	//	System.out.println(msg);
	}

	

	public void close() {
		pout.close();
		if (perr != pout)
			perr.close();
		cache.remove(this);
	}
}
