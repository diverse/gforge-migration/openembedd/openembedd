package fr.inria.ctrte.time.parser.packageexplorer;

import org.eclipse.ui.PlatformUI;

import fr.inria.base.MyManager;

public class PackageViewExtention {

	static PackageViewExtention eInstance = null;

	private PackageViewExtention() {
		super();
		create();
	}

	static public PackageViewExtention getInstance() {
		if (eInstance == null)
			eInstance = new PackageViewExtention();
		// eInstance.create();
		//eInstance.create();
		return eInstance;
	}

	private MyWindowListener mywl = null;

	private void create() {
		try
		{
		if (mywl == null) {
			mywl = new MyWindowListener();
			PlatformUI.getWorkbench().addWindowListener(mywl);
		}
		}catch (Exception e) {
			MyManager.printError(e, "Package View" );
		}
	}

}
