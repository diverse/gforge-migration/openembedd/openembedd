package fr.inria.ctrte.time.parser.packageexplorer;

import java.util.ArrayList;

public class VcdList {

	ArrayList<String> ls = new ArrayList<String>();

	private VcdList() {
		super();

	}

	private static VcdList vc = null;

	public static VcdList getVc() {
		if (vc == null)
			vc = new VcdList();
		return vc;
	}

	public int add(String s) {
		if (ls.contains(s))
			return 0;
		ls.add(s);
		return 0;
	}

	public int remove(String s) {
		if (ls.contains(s))
			ls.remove(s);
		return 0;
	}

	public boolean contains(String s) {
		return ls.contains(s);

	}
}
