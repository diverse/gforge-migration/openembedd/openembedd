/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser;

import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import fr.inria.base.MyPluginManager;

/**
 * The activator class controls the plug-in life cycle
 */
public class ActivatorTimeParser extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.inria.ctrte.time.parser";

	// The shared instance
	private static ActivatorTimeParser plugin;
	IConsole cv;

	/**
	 * The constructor
	 */

	public ActivatorTimeParser() {
		plugin = this;
	}

	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
	//	PackageViewExtention.getInstance();
		MyPluginManager.refreshWorkspace();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ActivatorTimeParser getDefault() {
		return plugin;
	}

	/*
	 * static public IViewPart getCCSLView() { try { IWorkbench iw
	 * =PlatformUI.getWorkbench(); if (iw==null) return null; return
	 * iw.getActiveWorkbenchWindow
	 * ().getActivePage().showView("fr.inria.ctrte.time.CCSLview");
	 * 
	 * } catch (Exception ex) { MyManager.printError(ex); }
	 * 
	 * }
	 */

}
