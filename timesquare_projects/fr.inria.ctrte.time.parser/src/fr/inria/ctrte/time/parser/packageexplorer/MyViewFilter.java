/**
 * 
 */
package fr.inria.ctrte.time.parser.packageexplorer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import fr.inria.base.MyManager;

final class MyViewFilter extends ViewerFilter {

	@Override
	public Object[] filter(Viewer viewer, Object parent, Object[] elements) {
		
		return super.filter(viewer, parent, elements);
	}

	@Override
	public Object[] filter(Viewer viewer, TreePath parentPath, Object[] elements) {
	
		return super.filter(viewer, parentPath, elements);
	}

	@Override
	public boolean isFilterProperty(Object element, String property) {
		
		return super.isFilterProperty(element, property);
	}

	private boolean checkvcd(IFile f) {
		try {
			//MyManager.println(f.getClass().getName());
			if (f.getParent() instanceof IFolder) {
				IPath ip = f.getLocation().removeFileExtension().addFileExtension("vcd");
			
				
				if (ip.toFile().exists()) {
					if (VcdList.getVc().contains(ip.toOSString()))
						return false;
					return true;
				}
				return false;
			}
			return false;
		} catch (Exception ex) {
			MyManager.printError(ex);
			return false;
		}
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		try {
			//MyManager.println(element.getClass().getName()	+" ----");
			/*for (  Class c: new JavaProjectAdapterFactory().getAdapterList())
			{
				MyManager.println(c.getName());
			}
			if (element instanceof org.eclipse.jdt.internal.core.CompilationUnit) {
				MyManager.println("coucou");
			}*/
			if (element instanceof IFile) {

				IFile ifile = (IFile) element;
				String s = ifile.getFileExtension();
				// checkvcd(ifile);
				// ((IFile)
				// element).createMarker("test").setAttribute("toto", true);
				if (s == null)
					return false;
				if (s.compareTo("err") == 0)
					return !checkvcd(ifile);
				if (s.compareTo("tra") == 0)
					return !checkvcd(ifile);
				if (s.compareTo("log") == 0)
					return !checkvcd(ifile);
				if (s.compareTo("out") == 0)
					return !checkvcd(ifile);
				if (s.compareTo("fire") == 0)
					return !checkvcd(ifile);
				if (s.compareTo("vcd") == 0) {
					
					
				}
			}
		} catch (Exception ex) {
			MyManager.printError(ex);
			return true;
		}
	

		return true;
	}
}