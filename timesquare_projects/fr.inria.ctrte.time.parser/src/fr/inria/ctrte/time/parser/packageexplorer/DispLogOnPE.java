package fr.inria.ctrte.time.parser.packageexplorer;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;

import fr.inria.base.MyManager;

public class DispLogOnPE implements IActionDelegate {

	public DispLogOnPE() {

	}

	private IFile file = null;
	boolean b = false;

	
	public void run(IAction action) {

		String s = file.getLocation().toOSString();
		if (b) {
			b = false;
			action.setChecked(b);
			VcdList.getVc().remove(s);
		
		} else {
			b = true;
			action.setChecked(b);
			VcdList.getVc().add(s);
		
		}

	

		try {

			IViewPart courant = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage().showView(
							"org.eclipse.jdt.ui.PackageExplorer");

			IPackagesViewPart ipvp = (IPackagesViewPart) courant;

			ipvp.getTreeViewer().refresh();

		} catch (Exception ex) {
			MyManager.printError(ex);
		}

	}

	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof TreeSelection))
			return;
		TreeSelection ts = (TreeSelection) selection;
		myLoad(ts.getFirstElement());
		action.setChecked(VcdList.getVc().contains(
				file.getLocation().toOSString()));
		b = VcdList.getVc().contains(file.getLocation().toOSString());
	}

	private void myLoad(Object o) {
		if (!(o instanceof IFile))
			return;
		this.file = (IFile) o;
	}

}
