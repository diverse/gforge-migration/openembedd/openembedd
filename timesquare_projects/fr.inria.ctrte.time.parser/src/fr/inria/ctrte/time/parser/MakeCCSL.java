/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Constraint;

import code.CmessageBox;
import code.Cumlcreator;
import fr.inria.base.MyManager;

public class MakeCCSL {

	private MonLog pw;

	// private int count = 0;

	public boolean not(boolean b) {
		return !b;
	}

	private void init() {
		try {
			pw = MonLog.createLog("time.parser", new FileWriter(
					"timeparser.txt"), new FileWriter("timeparser.err.txt"));
		} catch (IOException ex) {
			MyManager.printError(ex);
		}
	}

	int MessageBox(String s, String s1) {
		MessageDialog.openInformation(new Shell(), CmessageBox.pluginTime + s,
				s1);
		return 0;
	}

	boolean QuestionBox(String s, String s1) {
		return MessageDialog.openQuestion(new Shell(), CmessageBox.pluginTime
				+ s, s1);
	}

	public void parseConstraint(Constraint co) {
		init();
		Cumlcreator.getInstance().createUmlElement(co).makeParsable();
		pw.close();
	}

	int ErreurBox(String s, String s1) {
		MessageDialog.openError(new Shell(), CmessageBox.pluginTime + s, s1);
		return 0;
	}
}
