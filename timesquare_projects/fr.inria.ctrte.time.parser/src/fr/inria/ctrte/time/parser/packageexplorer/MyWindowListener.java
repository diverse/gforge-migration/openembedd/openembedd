/**
 * 
 */
package fr.inria.ctrte.time.parser.packageexplorer;

import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchWindow;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;

final class MyWindowListener implements IWindowListener {
	MyViewFilter mvl = null;
	

	public MyWindowListener() {
		super();
	
	}

	
	public void windowActivated(IWorkbenchWindow window) {
		try {
			IViewPart courant = MyPluginManager.getPackageExplorer();			
			if (courant==null)
				return ;
			IPackagesViewPart ipvp = (IPackagesViewPart) courant;

			if (mvl == null) {
				mvl = new MyViewFilter();
				ipvp.getTreeViewer().addFilter(mvl);
			}

		} catch (Exception ex) {
			MyManager.printError(ex);
		}

	}

	
	public void windowClosed(IWorkbenchWindow window) {

	}

	
	public void windowDeactivated(IWorkbenchWindow window) {

	}

	
	public void windowOpened(IWorkbenchWindow window) {

	}
}