package fr.inria.ctrte.time.parser;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import antlr.collections.AST;
import code.Cbase;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;

import grammaire.analyse.ASTMask;

public class CCSLview extends ViewPart {

	private Tree tree = null;
	// private Tree tree2=null;

	static public CCSLview courant = null;

	public CCSLview() {
		courant = this;
	}

	

	

	private final class TableListener implements Listener {
		Shell tip = null;
		Label label = null;

		
		public TableListener()
		{
			super();
		
		}


		public void handleEvent(Event event) {
			switch (event.type)
			{
			case SWT.Dispose:
			case SWT.KeyDown:
			case SWT.MouseMove:
			{
				if (tip == null)
					break;
				tip.dispose();
				tip = null;
				label = null;
				break;
			}
			case SWT.MouseHover: {
				TreeItem item = tree.getItem(new Point(event.x, event.y));
				if (item != null) {
					if (tip != null && !tip.isDisposed())
						tip.dispose();
					tip = new Shell(shell, SWT.ON_TOP | SWT.NO_FOCUS | SWT.TOOL);
					tip.setBackground(display
							.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
					FillLayout layout = new FillLayout();
					layout.marginWidth = 2;
					tip.setLayout(layout);
					label = new Label(tip, SWT.NONE);
					label.setForeground(display.getSystemColor(SWT.COLOR_INFO_FOREGROUND));
					label.setBackground(display.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
					label.setData("_TABLEITEM", item);
					if (item.getData() != null)
						label.setText(item.getData().toString());
					else
						label.setText(item.getText());
					label.addListener(SWT.MouseExit, labelListener);
					label.addListener(SWT.MouseDown, labelListener);
					Point size = tip.computeSize(SWT.DEFAULT, SWT.DEFAULT);
					Rectangle rect = item.getBounds(0);
					Point pt = tree.toDisplay(rect.x, rect.y);
					tip.setBounds(pt.x + 20, pt.y + 5, size.x, size.y);
					tip.setVisible(true);
				}
			}
			}
		}
	}

	private final class LabelListener implements Listener {
		public void handleEvent(Event event) {
			Label label = (Label) event.widget;
			Shell shell = label.getShell();
			switch (event.type) {
			case SWT.MouseDown:
				Event e = new Event();
				e.item = (TableItem) label.getData("_TABLEITEM");
				tree.setSelection(new TreeItem[] { (TreeItem) e.item });
				tree.notifyListeners(SWT.Selection, e);
				shell.dispose();
				tree.setFocus();
				break;
			case SWT.MouseExit:
				shell.dispose();
				break;
			}
		}
	}

	
	Composite x;

	@Override
	public void createPartControl(Composite parent) {
		shell = parent.getShell();
		display = shell.getDisplay();
		tree = new Tree(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		tree.addListener(SWT.Dispose, tableListener);
		tree.addListener(SWT.KeyDown, tableListener);
		tree.addListener(SWT.MouseMove, tableListener);
		tree.addListener(SWT.MouseHover, tableListener);
		Menu menu = new Menu(tree);
		MenuItem menitem = new MenuItem(menu, SWT.NONE);
		menitem.setText("Clear");
		menitem.addSelectionListener(new SelectionListener(){

			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);		
			}

			
			public void widgetSelected(SelectionEvent e) {
				tree.clearAll(false);
				tree.removeAll();
			}}); 
		tree.setMenu(menu);
		// (new Exception()).printStackTrace();
		/*
		 * TreeItem ti= new TreeItem(tree,0); ti.setText("no ast");
		 */
		/*
		 * tree2= new Tree(parent,SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		 */

		x = parent;
	}

	@Override
	public IViewSite getViewSite() {

		return super.getViewSite();
	}

	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {

		super.init(site, memento);
	}

	@Override
	public void saveState(IMemento memento) {

		super.saveState(memento);
	}

	protected int dispASTinternal(AST ast, TreeItem ti) 
	{
		/*
		 * if (ast.getType()==LangParserTokenTypes.ID ||
		 * ast.getType()==LangParserTokenTypes.PATH ) {
		 * 
		 * ti.setText("ID "+ast.getText());
		 * 
		 * } else
		 */
		{
			ti.setText(ast.getText());
			ti.setData("type "+ ast.getType());
		}
		AST l = ast.getFirstChild();
		if (l != null)
			ti.setImage(Cbase.IMG_openfolder);
		else
			ti.setImage(Cbase.IMG_file);
		while (l != null) {
			TreeItem ti2 = new TreeItem(ti, 0);
			dispASTinternal(l, ti2);
			l = l.getNextSibling();
		}

		return 0;
	}

	public int dispAST(ASTMask ast, String s) {

		return dispAST(ast.getA(), s);
	}

	public int dispAST(AST ast) 
	{
		return dispAST(ast, "");
	}

	public int dispAST(AST ast, String s) {
		if (tree == null)
			return 0;
		if (tree.isDisposed())
			return 0;
		TreeItem ti = new TreeItem(tree, 0);
		dispASTinternal(ast, ti);
		tree.redraw();
		return 0;
	}

	@Override
	public void setFocus() {
		if (tree != null)
			tree.setFocus();
		courant = this;

	}

	@Override
	public void dispose() {
		courant = null;
		super.dispose();
	}

	Listener labelListener = new LabelListener();

	Listener tableListener = new TableListener();

	Display display = null;
	Shell shell = null;

	public static CCSLview getCourant() {
		if (courant == null) {
			try {
				
				courant = (CCSLview) MyPluginManager.getCreateView("fr.inria.ctrte.time.CCSLview");
				MyPluginManager.getShowView("fr.inria.ctrte.time.CCSLview");
				courant.setFocus();
			} catch (Exception ex) {
				MyManager.printError(ex, "CCSL view");
			}
		}	
		return courant;
	}

	
}
