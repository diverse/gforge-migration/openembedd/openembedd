/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

import fr.inria.base.MyManager;

public class CmyStereotype extends CmyClass {
	// Cumlelement {

	private Stereotype stereo = null;

	public CmyStereotype(Stereotype stereo) {
		super(stereo);
		this.stereo = stereo;
	}

	public CmyProfile getProfile() {
		return Cumlcreator.getInstance().createUmlElement(stereo.getProfile());

	}

	public boolean IsprofileName(String prof) {
		return (getProfile().getName().compareTo(prof) == 0);
	}

	public boolean IsprofileQualifiedName(String prof) {

		return (getProfile().getQualifiedName().compareTo(prof) == 0);
	}

	public Clist ApplyTo() {

		Clist ls = new Clist();
		EList<Class> cc = stereo.getAllExtendedMetaclasses();// getExtendedMetaclasses
		// ();
		MyManager.println(CmessageBox.numberelement + cc.size());
		for (Element o2 : cc) {

			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}

		return ls;
	}

	@Override
	public Clist getAttributes() {
		EList<Property> cc = stereo.getAllAttributes();

		Clist ls = new Clist();
		for (Property o2 : cc) {

			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}
		return ls;
	}

	@Override
	public Stereotype getElement() {
		return stereo;
	}

	@Override
	public Image getIcone() {
		return Cbase.IMG_STEREOTYPE;
	}

}
