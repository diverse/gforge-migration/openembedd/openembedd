package code;

import code.Cbase.Enumtesttype;

public interface CmyClock extends CinterfaceUML {

	boolean isClockStereotype();

	boolean isMarteClock();

	Boolean isLogicalClock();

	CmyEnumerationLiteral getUnit();

	int setUnit( CmyEnumerationLiteral cmel );

	CmyEnumerationLiteral getStandard();

	int setStandard( CmyEnumerationLiteral standard );

	Enumtesttype ClockUnitValide();

	void ClockUnitAutoCorrection();

	String informationTimeClock();

	void applyClock();

	void setTypeClassifier( CmyClass c );

	CmyClass getTypeClassifier();

	CmyClass getClassifier();
	
	boolean isChronometricClcok(); //TODO rename
}