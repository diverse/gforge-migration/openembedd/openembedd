/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.HashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;

public class Cumlview {

	private final static Cumlview eInstance = new Cumlview();
	private static HashMap<String, Resource> hash = new HashMap<String, Resource>();
	protected static final ResourceSet RESOURCE_SET = new ResourceSetImpl();

	private static Package umlprim = null, martelib = null;
	private static Profile marteprof = null;

	protected Cumlview() {
		super();
		
	}

	public static Cumlview getEInstance() {
		return eInstance;
	}

	static public Resource loadfile(String f) {
		Resource r = null;
		if (f == null) {
			MyManager.println("f is null");
			return null;
		}
		try {
			r = hash.get(f);
		
			if (r == null) {
				URI uri = URI.createURI(f);
				r = RESOURCE_SET.getResource(uri, true);
				
				if (r != null)
					hash.put(f, r);
			}
		} catch (Exception e) {
			MyManager.println("<>" + f + "<>\nErreur  :" + e);
			// MessageBox.
			e.printStackTrace();
		}
		return r;
	}

	static public Resource loadMarteProfile() {
		return loadfile("pathmap://Papyrus_PROFILES/MARTE.profile.uml");
	}

	static public Resource loadMarteLibrary() {
		return loadfile("pathmap://Papyrus_PROFILES/MARTE_Library.library.uml");
	}

	static public Resource loadUmlPrimitiveLibrary() {
		return loadfile(UMLResource.UML_PRIMITIVE_TYPES_LIBRARY_URI);
	}

	static public Package getPackageUMLPrimitivitype() {
		if (umlprim == null)
			umlprim = (Package) EcoreUtil.getObjectByType(
					loadUmlPrimitiveLibrary().getContents(),
					UMLPackage.eINSTANCE.getPackage());

		return umlprim;
	}

	static public Type getStringType() {
		return (Type) getPackageUMLPrimitivitype().getMember("String");
	}

	static public Type getIntegerType() {
		return (Type) getPackageUMLPrimitivitype().getMember("Integer");
	}

	static public Package getPackageMarteLibrary() {
		if (martelib == null) {
			martelib = (Package) EcoreUtil.getObjectByType(loadMarteLibrary()
					.getContents(), UMLPackage.eINSTANCE.getPackage());
		}
	
		return martelib;
	}

	static public Profile getPackageMarteProfile() {
		if (marteprof == null) {
			marteprof = (Profile) EcoreUtil.getObjectByType(loadMarteProfile()
					.getContents(), UMLPackage.eINSTANCE.getPackage());

		}
	
		return marteprof;
	}

	static public InstanceSpecification getidealClock() {
		Package p = getPackageMarteLibrary();
		if (p != null) {
			InstanceSpecification s = (InstanceSpecification) ((Package) p
					.getOwnedMember("TimeLibrary"))
					.getOwnedMember("idealClock");
			if (s != null)
				return s;
			s = (InstanceSpecification) ((Package) p
					.getOwnedMember("TimeLibrary")).getOwnedMember("idealClk");
			if (s != null)
				return s;

		}
		return null;
	}

	static public Enumeration getTimeStandardKind() {

		Package p = getPackageMarteLibrary();
		if (p != null) {
			Enumeration e= (Enumeration) ((Package) p.getOwnedMember("TimeLibrary")) 
					.getOwnedMember("TimeStandardKind");
			if (e!=null)
				return e ;
			return (Enumeration) ((Package) p.getOwnedMember("TimeTypesLibrary")) 
			.getOwnedMember("TimeStandardKind");
		}
		return null;
	}

	static public Profile getTimeProfil() {
		Profile p = null;
		p = (Profile) ((Package) getPackageMarteProfile().getPackagedElement(
				"MARTE_Foundations")).getPackagedElement("Time");
		return p;
	}

	static public Profile getNFPsProfil() {
		Profile p = null;
		p = (Profile) ((Package) getPackageMarteProfile().getPackagedElement(
				"MARTE_Foundations")).getPackagedElement("NFPs");
		return p;
	}

	static public Profile getAllocProfil() {
		Profile p = null;
		p = (Profile) ((Package) getPackageMarteProfile().getPackagedElement(
				"MARTE_Foundations")).getPackagedElement("Alloc");
		return p;
	}

	static public Stereotype getClockConstraint() {

		Package p = getPackageMarteProfile();
		if (p != null) {

			
			return (Stereotype) getTimeProfil().getOwnedMember(	"ClockConstraint");
		}
		return null;

	}

	public static void touchModel(Element element) {
		NamedElement namedElement;
		if (element instanceof NamedElement) {
			namedElement = (NamedElement) element;
		} else {
			namedElement = element.getModel();
		}

		// Element name
		String name = "";
		if (namedElement.isSetName()) {
			name = namedElement.getName();
		}

		try { //TODO
			IEditorPart ie =MyPluginManager.getCourantEditor();
			if (ie instanceof IEditingDomainProvider)
			{
			IEditingDomainProvider activeEditor =(IEditingDomainProvider) ie;
			EditingDomain editingDomain = activeEditor.getEditingDomain();

			// Retrieve "Name" EStructuralFeature of NamedElement
			EStructuralFeature eNameSF = UMLPackage.eINSTANCE.getNamedElement()
					.getEStructuralFeature(UMLPackage.NAMED_ELEMENT__NAME);
			editingDomain.getCommandStack().execute(
					SetCommand.create(editingDomain, namedElement, eNameSF,
							name));
			}
		} catch (ClassCastException e) {
			return;
		} catch (Exception e) {
			MyManager.printError(e);
			return;
		}
	}

	static public int appliedNFPs(CmyPackage m) {
		if (m == null)
			return 0;
		if (m.getElement() == null)
			return 0;
		if (m.getProfiles(true).getElement("Time") == null) {
			m.getElement().applyProfile(Cumlview.getNFPsProfil());
			return 1;
		}
		return 0;
	}

	static public int appliedTime(CmyPackage m) {
		if (m == null)
			return 0;
		if (m.getElement() == null)
			return 0;
		if (m.getProfiles(true).getElement("Time") == null) {
			m.getElement().applyProfile(Cumlview.getTimeProfil());
			return 1;
		}
		return 0;
	}

	static public int appliedMARTE(CmyPackage m) {
		if (m.getProfiles(true).getElement("MARTE") == null) {
			m.getElement().applyProfile(Cumlview.getPackageMarteProfile());
			return 1;
		}
		return 0;
	}

	static public int appliedAlloc(CmyPackage m) {
		if (m.getProfiles(true).getElement("Alloc") == null) {
			m.getElement().applyProfile(Cumlview.getAllocProfil());
			return 1;
		}
		return 0;
	}

	static public int importUnitKind(CmyPackage m) {
		CmyPackage cp = new CmyPackage(Cumlview.getPackageMarteLibrary());
		Clist le = cp.getAllEnumeration().GetUnitEnumeration().getligth();
		Package ml = m.getElement(); // Model
		EList<PackageableElement> mi = ml.getImportedElements();
		for (Cumlelement o : le.lne) {
			CmyEnumeration e = (CmyEnumeration) o;
			if (!mi.contains(e.getElement()))

			{
				ml.createElementImport( e.getElement());
			}

		}

		return 0;
	}
}
