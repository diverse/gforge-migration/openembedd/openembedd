/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;

import fr.inria.base.MyManager;

public class CmyEnumeration extends Cumlelement {

	private Enumeration enumeration;

	public CmyEnumeration(Enumeration enumeration) {
		super(enumeration);
		this.enumeration = enumeration;
	}

	@Override
	public Enumeration getElement() {
		return enumeration;
	}

	public Clistenumerationliteral getEumerationliteral() {
		try {

			EList<EnumerationLiteral> cc = enumeration.getOwnedLiterals();
			Clistenumerationliteral ls = new Clistenumerationliteral();
			for (EnumerationLiteral o2 : cc) {
				ls.add(Cumlcreator.getInstance().createUmlElement(o2));
			}
			return ls;
		} catch (Exception e) {
			MyManager.println("xx " + e + " ");
			return null;
		}
	}

	public boolean isUnitEnumeration() {
		Clistenumerationliteral l = getEumerationliteral();
		for (CmyEnumerationLiteral el : l.getLneEnumrerationLiteral()) {
			if (el.IsStereotypeByUnit())
				return true;
		}
		return false;
	}

	public boolean has(String s) {
		try {
			return getEumerationliteral().contient(s);
		} catch (Exception e) {
			MyManager.print(" error Enumeration : " + s);
			return true;
		}
	}

	public boolean has(CmyEnumerationLiteral el) {
		// String s=el.getQualifiedName();
		// if (s.contains(enumeration.getQualifiedName())) return true;
		try {
			EnumerationLiteral lit = enumeration.getOwnedLiteral(el.getName());
			return (lit == el.getElement());
		} catch (Exception e) {
			MyManager.print("error Enumereation ::" + el.toString());
			return true;
		}

	}

	@Override
	public Image getIcone() {
		return Cbase.IMG_ENUMERATION;
	}
}
