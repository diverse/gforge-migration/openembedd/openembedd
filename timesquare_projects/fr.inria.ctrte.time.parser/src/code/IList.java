/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package code;

import org.eclipse.uml2.uml.Element;

public interface IList {

	public String[] toArrayString();

	public String[] toArrayStringExtends();

	public String[] toArrayStringAuto();

	public Object[] toArray();

	public Element get(int n);

	public Cumlelement getC(int n);

	public int size();

}
