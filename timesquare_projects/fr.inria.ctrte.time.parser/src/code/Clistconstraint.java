/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Element;

/**
 * @author Benoit Ferrero
 * 
 */
public class Clistconstraint extends Clist {

	/**
	 * 
	 */
	public Clistconstraint() {

	}

	/**
	 * @param ol
	 * @param s
	 */
	public Clistconstraint(Clist ol, String s) {
		super(); // ol, s);
		copie(ol);

	}

	public Clistconstraint(Clist ol) {
		super();
		copie(ol);

	}

	@Override
	protected boolean test(Element e) {
		return isUmlType(umlconstraint, e);
	}

	public List<CmyConstraint> getLneConstraint() {
		List<CmyConstraint> local = new ArrayList<CmyConstraint>();
		for (Cumlelement o1 : lne) {
			local.add((CmyConstraint) o1);
		}
		return local;
	}

	@Override
	public Clistconstraint getligth() {

		return (Clistconstraint) super.getligth();
	}

	/*
	 * 
	 * @see code.Clist#getElement(java.lang.String) use Covariance
	 */
	@Override
	public CmyConstraint getElement(String s) {
		return (CmyConstraint) super.getElement(s);
	}

	/*
	 * 
	 * @see code.Clist#getElementQualified(java.lang.String) use Covariance
	 */
	@Override
	public CmyConstraint getElementQualified(String s) {
		return (CmyConstraint) super.getElementQualified(s);
	}

	/*
	 * 
	 * @see code.Clist#getC(int) use Covariance
	 */
	@Override
	public CmyConstraint getC(int n) {
		return (CmyConstraint) super.getC(n);
	}

}
