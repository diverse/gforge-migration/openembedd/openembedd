/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

//import java.util.*;

import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Element;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.time.parser.ActivatorTimeParser;

public class Cbase {

	public enum EnumNature {
		eDiscrete, eDense;
		EnumNature() {
			
		}

		@Override
		public String toString() {
			switch (this) {
			case eDiscrete:
				return martediscrete;
			case eDense:
				return martedense;
			}
			return "<?>"; // unreachable code
		}
	}

	public enum Enumtesttype {
		eNoMarte, eUnitOk, eUnitPartialNok, eUnitNok, eNoClassifier, eNoUnit;
		Enumtesttype() {
			
		}

		@Override
		public String toString() {
			switch (this) {
			case eNoMarte:
				return "is not a Clock";
			case eUnitOk:
				return "is a correct Clock Unit";
			case eUnitPartialNok:
				return "incorrect Qualified Name for a Unit ";
			case eUnitNok:
				return "illegal Clock Unit";
			case eNoClassifier:
				return "missing Classifier";
			case eNoUnit:
				return "missing unit";
			}
			return "<?>"; // unreachable code
		}
	}

	static final public String nullstr = "";
	static final public String umlclass = "Class";
	static final public String umlconstraint = "Constraint";
	static final public String umlenumeration = "Enumeration";
	static final public String umlprofile = "Profile";
	static final public String umlstereotype = "Stereotype";
	static final public String umlmodel = "Model";
	static final public String umlinstance = "InstanceSpecification";
	static final public String umlenumerationliteral = "EnumerationLiteral";

	public final static String marteTimeprofilQualified = "MARTE::MARTE_Foundations::Time"; // or
	// "Time";
	// / TASK: for OpenEmbeDD use "MARTE::MARTE_Foundations::Time"
	// / for UsineLogicielle use "Time"

	public final static String marteTimeprofil = "Time";
	public final static String marteClockConstraint = "ClockConstraint";
	public final static String marteCCSL = "CCSL";
	public final static String marteClockType = "ClockType";
	public final static String marteClock = "Clock";
	public final static String marteNature = "nature";
	public final static String marteUnitfld = "unit";
	public final static String marteUnitst = "Unit";
	public final static String martediscrete = "discrete";
	public final static String martedense = "dense";
	public final static String martelogical = "isLogical";
	public final static String marteUnitType = "unitType";

	/**************************************/

	public static final String IMAGE_PATH_CCSL = "icons/CCSL/";

	public static final Image IMG_openfolder = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_CCSL
					+ "icon_OpenFolder.gif");
	public static final Image IMG_file = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_CCSL + "icon_File.gif");

	public static final String IMAGE_PATH_UML = "icons/UML/";
	public static final Image IMG_PROPERTY = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML + "Property.gif");
	public static final Image IMG_STEREOTYPE = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML + "Stereotype.gif");
	public static final Image IMG_ENUMERATION = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML + "Enumeration.gif");
	public static final Image IMG_PACKAGE = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML + "Package.gif");
	public static final Image IMG_CLASS = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML + "Class.gif");
	public static final Image IMG_INSTANCE = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, IMAGE_PATH_UML
					+ "InstanceSpecification.gif");
	public static final Image IMG_CLOCK = MyPluginManager.getImage(
			ActivatorTimeParser.PLUGIN_ID, "icons/clock.png");

	/***********************************************************************/

	public String qualifiedName(String s1, String s2) {
		return s1 + "::" + s2;
	}

	static public String reducename(String n) {
		//String c = new String(n);
		String d = n.substring(35);
		String e = new String(d.replace("Impl", ""));
		return e;
	}

	public boolean IsElement(Object o) {
		if (o instanceof Element)
			return true;
		return false;
	}

	public boolean isUmlClass(String s, Object o) {
		try {
			return (java.lang.Class.forName("org.eclipse.uml2.uml." + s)
					.isInstance(o));
		} catch (Exception e) {

			return false;
		}
	}

	public boolean isUmlType(String s, Object o) {
		return reducename(o.getClass().getName()).compareTo(s) == 0;
	}
}
