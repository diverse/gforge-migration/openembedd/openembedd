/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

import fr.inria.base.MyManager;

public class CmyClass extends Cumlelement {

	// org.eclipse.uml2.uml.Class
	private Class classe;

	// org.eclipse.uml2.uml.Class
	protected CmyClass(Class classe) {
		super(classe);
		this.classe = classe;

	}

	
	public CmyClass createClass(String name) {
		Class is = (Class) classe.createNestedClassifier(name, UMLPackage.Literals.CLASS);				
		if (is != null)
			return Cumlcreator.getInstance().createUmlElement(is);
		return null;
	} 
	
	public CmyProperty createProperty(CmyClass  c,String name) {
		Property is = (Property) classe.createOwnedAttribute(name, c.getElement());			
		if (is != null)
			return Cumlcreator.getInstance().createUmlElement(is);
		return null;
	} 
	
	public CmyProperty createProperty(CmyClass  c,String name , EClass ec) {
		Property is = (Property) classe.createOwnedAttribute(name, c.getElement(), ec);			
		if (is != null)
			return Cumlcreator.getInstance().createUmlElement(is);
		return null;
	} 
	
	// TimeModel
	public boolean isMarteClockType() {

		if (getStereotypes().contient(marteClockType)) {
			try {
				Clist l = getStereotypes().getElement(marteClockType)
						.getAttributes();
				for (Cumlelement o : l.getLne())
					getAttributeStereotype(marteClockType, o.getName());

			} catch (Exception ex) {
				MyManager.printError(ex);
				return false;
			}
			return true;
		}
		return false;
	}

	public Clist getAttributes() {
		EList<Property> cc = classe.getAllAttributes();

		Clist ls = new Clist();
		for (Property o2 : cc) {

			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}
		return ls;
	}

	public Clist getOperations() {
		EList<Operation> cc = classe.getAllOperations();
		Clist ls = new Clist();
		for (Operation o2 : cc) {
			ls.add(Cumlcreator.getInstance().createUmlElement(o2));
		}
		return ls;
	}

	// TimeModel
	public Boolean isLogicalClockType() {

		Object o = getAttributeStereotype(marteClockType, martelogical);
	
		if (o instanceof Boolean)
			return (Boolean) o;
		if (o instanceof String) {
			if (((String) o).compareTo("true") == 0)
				return true;
			if (((String) o).compareTo("false") == 0)
				return false;
			return null;
		}
		return null;

	}

	// TimeModel
	public Boolean isDenseClockType() {
		try {
			Object o = getAttributeStereotype(marteClockType, marteNature);		
			if (o instanceof NamedElement) {
				String s = ((NamedElement) o).getName();

				if (s.compareTo(martedense) == 0)
					return true;
				if (s.compareTo(martediscrete) == 0)
					return false;

			}

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	// TimeModel
	public EnumNature getClockTypeNatureEnum() {
		Object o = getAttributeStereotype(marteClockType, marteNature);
		if (o instanceof EnumerationLiteral) {
			String s = ((EnumerationLiteral) o).getName();
			if (s.compareTo(martediscrete) == 0)
				return EnumNature.eDiscrete;
			if (s.compareTo(martedense) == 0)
				return EnumNature.eDense;
			return null;

		}
		return null;
	}

	// TimeModel
	public String getClockTypeNatureString() {
		Object o = getAttributeStereotype(marteClockType, marteNature);
		if (o instanceof NamedElement)
			return ((NamedElement) o).getName();
		return null;
	}

	// TimeModel
	public CmyEnumeration getUnitType() {
		Object o = getAttributeStereotype("ClockType", "unitType");
		if (o instanceof Enumeration)
			return new CmyEnumeration((Enumeration) o);
		return null;

	}

	@Override
	public Class getElement() {
		return classe;
	}

	public String informationClock() {
		String s = "Class " + classe.getName() + "\t";
		if (isMarteClockType()) {
			s += "\n ClockType : \n";
			Boolean b = isLogicalClockType();
			if (b != null) {
				if (b)
					s += "      Logical ";
				else {
					s += "      Chronometric ";

					Boolean b2 = isDenseClockType();
					if (b2 != null) {
						if (b2)
							s += " Dense ";
						else
							s += " Discrete ";
					}
				}
				s += "\t\n";
			}
			CmyEnumeration cme = getUnitType();
			if (cme != null) {
				s += "      Unit Type : " + cme.getName() + "\t";
			}

		}
		return s;
	}

	/**
	 * Applique le stereoty ClockType s il n est pas present
	 * 
	 */
	public void applyClockType() {
		if (isMarteClockType())
			return;
		CmyPackage p = getRootPackage();

		if (p != null) {
			if (!p.getProfiles(true).contient("Time"))
				Cumlview.appliedTime(p);
			CmyProfile cmpr=p.getProfiles(true).getElement("Time");
		
			CmyStereotype cms = (CmyStereotype ) cmpr.allownedofE().getElement("ClockType");
			
			//cms.getElement().eSetDeliver(false);		//TODO deliver	
			classe.eSetDeliver(true);
			//classe.eResource().eSetDeliver(false);  //TODO deliver
			classe.applyStereotype(cms.getElement());
			return;
		}
		CmyProfile cmp = Cumlcreator.getInstance().createUmlElement(
				Cumlview.getTimeProfil());
		CmyStereotype cms = (CmyStereotype) cmp.getownedofE().getElement(
				"ClockType");
		//cms.getElement().eSetDeliver(false);		//TODO deliver	
		classe.eSetDeliver(true);
	//	classe.eResource().eSetDeliver(false);//TODO deliver
		classe.applyStereotype(cms.getElement());

	}

	public void setLogicalAttribut(Boolean b) {
		try
		{
		if (!isMarteClockType())
			return;
		if (b == null) {
			setStereotypeAttributValue(marteClockType, martelogical, null);
			return;
		}
		if (b == true) {
			setStereotypeAttributValue(marteClockType, martelogical, "true");
			return;
		}
		if (b == false) {
			setStereotypeAttributValue(marteClockType, martelogical, "false");
			return;
		}
		}
		catch (Exception e)
		{
			MyManager.println(e);
		}
	}

	public void setNatureAttribut(boolean nature) {
		if (!isMarteClockType())
			return;
		Clist le;
		try {
			le = ((CmyEnumeration) getRootPackage().getProfiles(true)
					.getElement("Time").getMembres().getElement(
							"TimeNatureKind")).getEumerationliteral();
		} catch (Exception e) {
			return;
		}
		Object o;
		if (nature == true)
			o = le.getElement("dense").getElement();
		else
			o = le.getElement("discrete").getElement();
		setStereotypeAttributValue(marteClockType, marteNature, o);
	}

	public void setUnitType(Enumeration e) {

		setStereotypeAttributValue("ClockType", "unitType", e);
	}

	@Override
	public Image getIcone() {
		if (isMarteClockType()) {
			return Cbase.IMG_CLOCK;
		}
		return Cbase.IMG_CLASS;
	}

	@Override
	public String getinfo(int x) {
		if (isMarteClockType()) {
			String s = "<<ClockType>> " + getName() + " :";
			if (isLogicalClockType())
				s += " (Logical ;";
			else if (isDenseClockType())
				s += " (Chronometric dense ;";
			else
				s += " (Chronometric discrete ;";
			if (getUnitType() != null)
				if (getUnitType().getName() != null)
					s += " Unit = " + getUnitType().getName() + " )";
			return s;
		}
		return super.getinfo(x);
	}

}
