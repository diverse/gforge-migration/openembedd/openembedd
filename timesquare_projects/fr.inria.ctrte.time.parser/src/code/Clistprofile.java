/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Element;

//import org.eclipse.uml2.uml.*;

public class Clistprofile extends Clist {
	boolean reduits;

	public Clistprofile() {
		super();

	}

	public Clistprofile(Clist l, boolean reduits) {
		super(l);
		this.reduits = reduits;
		// Copie(l);
	}

	@Override
	protected boolean test(Element e) {
		return isUmlType(umlprofile, e);
	}

	@Override
	public Clistprofile getligth() {
		Clistprofile l = new Clistprofile();
		for (Cumlelement o1 : lne) {

			// NamedElement o= o1.getNe();

			if (l.getLne().contains(o1) == false)
				l.add(o1);

		}
		return l;
	}

	public List<CmyProfile> getLneProfile() {
		List<CmyProfile> local = new ArrayList<CmyProfile>();
		for (Cumlelement o1 : lne) {
			local.add((CmyProfile) o1);
		}
		return local;
	}

	/*
	 * 
	 * @see code.Clist#getElement(java.lang.String) use Covariance
	 */
	@Override
	public CmyProfile getElement(String s) {
		return (CmyProfile) super.getElement(s);
	}

	/*
	 * 
	 * @see code.Clist#getElementQualified(java.lang.String) use Covariance
	 */
	@Override
	public CmyProfile getElementQualified(String s) {
		return (CmyProfile) super.getElementQualified(s);
	}

	/*
	 * 
	 * @see code.Clist#getC(int) use Covariance
	 */
	@Override
	public CmyProfile getC(int n) {
		return (CmyProfile) super.getC(n);
	}
}
