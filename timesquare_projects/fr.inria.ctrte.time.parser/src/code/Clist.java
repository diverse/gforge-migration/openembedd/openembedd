/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Element;

import fr.inria.base.MyManager;

public class Clist extends Cbase implements IList {

	protected boolean extend = false;
	protected List<Cumlelement> lne = new ArrayList<Cumlelement>();
	protected int nne = 0;
	protected boolean onoff = false;
	protected int signature = 0;
	protected String stype = null;
	protected String name = null;

	public Clist() {
	}

	public Clist(Clist ol) {
		copie(ol);
	}

	public Clist(Clist ol, String s) {
		copie(ol);
		name = s;
	}

	public Clist newextends() {
		// return new Clist();
		try {
			Object o = this.getClass().newInstance();
			return (Clist) o;
		} catch (Exception e) {

			return null;
		}
	}

	public int add(Cumlelement e) {
		if (test(e.getElement())) {
			lne.add(nne, e);
			return nne++;
		}
		return 0;
	}

	public int add(Element e) {
		if (test(e)) {
			lne.add(nne, Cumlcreator.getInstance().createUmlElement(e));
			return nne++;
		}
		return 0;
	}

	public int add(Element e, Element m) {
		if (test(e)) {
			lne.add(nne, Cumlcreator.getInstance().createUmlElement(e));
			return nne++;
		}
		return 0;

	}

	public Clist allownedofE() {
		Clist ls = new Clist();
		for (Cumlelement o1 : lne) {
			EList<Element> cc = o1.getElement().allOwnedElements();
			for (Element o : cc) {
				ls.add(Cumlcreator.getInstance().createUmlElement(o));
			}
		}
		return ls;
	}

	public boolean contient(String s) {
		for (Cumlelement o1 : lne) {

			if (o1.getName().compareTo(s) == 0)
				return true;
		}
		return false;
	}

	public boolean contient(Element e) {
		for (Cumlelement o1 : lne) {
			if (o1.getElement() == e)
				return true;
		}
		return false;
	}

	public boolean contientQualified(String s) {
		for (Cumlelement o1 : lne) {
			if (o1.getQualifiedName().compareTo(s) == 0)
				return true;
		}
		return false;
	}

	public void copie(Clist l) {
		if (l == null)
			return;
		for (Cumlelement o : l.getLne()) {
			add(o);
		}
	}

	public int disp() {
		for (Cumlelement o1 : lne) {
			MyManager.println(o1.getQualifiedName()+" " +o1.getXMIID());
		}

		MyManager.println();
		return 0;

	}

	public int found() {
		return 0;
	}

	public Element get(int n) // Named
	{
		if (lne.size() <= n) {
			return null;
		}
		return lne.get(n).getElement();
	}

	public Cumlelement getC(int n) {
		try {
			return lne.get(n);
		} catch (Exception e) {
			return null;
		}
	}

	public Cumlelement getElement(String s) {
		for (Cumlelement o1 : lne) {
			if (o1.getName() != null)
				if (o1.getName().compareTo(s) == 0)
					return o1;
		}
		return null;

	}

	public Cumlelement getElementQualified(String s) {
		for (Cumlelement o1 : lne) {
			if (o1.getQualifiedName() != null)
				if (o1.getQualifiedName().compareTo(s) == 0)
					return o1;
		}
		return null;

	}

	public Clist getFilterElementList(String s) {
		Clist l = newextends(); // new Clist();
		for (Cumlelement o1 : lne) {
			Element o = o1.getElement();
			if (isUmlClass(s, o))
				l.add(o1);

		}
		return l;
	}

	public Clist getFilterElementListStrict(String s) {
		Clist l = newextends(); // new Clist();
		for (Cumlelement o1 : lne) {
			Element o = o1.getElement();
			if (isUmlType(s, o))
				l.add(o);
		}
		return l;
	}

	public Clist getligth() {
		Clist l = newextends(); // new Clist();
		for (Cumlelement o1 : lne) {
			if (l.getLne().contains(o1) == false)
				l.add(o1);
		}
		return l;
	}

	public List<Cumlelement> getLne() {
		return lne;
	}

	public Clist getmemberofE() {
		Clist ls = new Clist(); // no

		for (Cumlelement o1 : lne) {
			Clist cc = o1.getMembres();
			if (cc != null)
				for (Cumlelement o2 : cc.getLne()) {
					Cumlelement uml = Cumlcreator.getInstance()
							.createUmlElement(o2.getElement());
					uml.setMother(o1.getElement());
					ls.add(uml);
				}

		}

		return ls;
	}

	public Clist getownedofE() {
		Clist ls = new Clist(); // no

		for (Cumlelement o1 : lne) {
			EList<Element> cc = o1.getElement().getOwnedElements();
			for (Element o2 : cc) {
				Cumlelement uml = Cumlcreator.getInstance()
						.createUmlElement(o2);
				uml.setMother(o1.getElement());
				ls.add(uml);
			}

		}

		return ls;
	}

	public int getSignature() {
		return signature;
	}

	public Cliststereotype getStererotypeofE() {
		Cliststereotype ls = new Cliststereotype();

		for (Cumlelement o1 : lne) {
			Cliststereotype cc7 = o1.getStereotypes();
			for (Cumlelement o2 : cc7.getLne()) {
				Cumlelement uml = Cumlcreator.getInstance().createUmlElement(
						o2.getElement());
				uml.setMother(o1.getElement());
				ls.add(uml);
			}
		}

		return ls;
	}

	public String getStype() {
		return stype;
	}

	public boolean isExtend() {
		return extend;
	}

	public Clist merge(Clist l) {
		Clist lc = new Clist(); // Check
		for (Cumlelement o : lne) {
			lc.add(o);
		}
		lc.copie(l);
		return lc;
	}

	public void setExtend(boolean extend) {
		this.extend = extend;
	}

	public void setSignature(int signature) {
		this.signature = signature;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public int size() {
		return lne.size();
	}

	protected boolean test(Element e) {

		if (e == null)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return CmessageBox.liste;
	}

	public Clist getStereotypedby(String s) {
		Clist l = newextends(); // new Clist();
		for (Cumlelement o1 : lne) {

			if (o1.getStereotypes().contient(s))
				l.add(o1);

		}
		return l;

	}

	public Clist getStereotypedbyQualified(String s) {
		Clist l = newextends(); // new Clist();
		for (Cumlelement o1 : lne) {

			if (o1.getStereotypes().contientQualified(s))
				l.add(o1);

		}
		return l;
	}

	public Object[] toArray() {
		return lne.toArray();
	}

	public String[] toArrayString() {
		try {
			int i = 0;
			String l[] = new String[lne.size()];
			for (Cumlelement c : lne) {
				l[i] = c.getName();
				i++;
			}

			return l;
		} catch (Exception e) {

			return null;
		}

	}

	public String[] toArrayStringExtends() {
		try {
			int i = 0;
			String l[] = new String[lne.size()];
			for (Cumlelement c : lne) {
				l[i] = c.getQualifiedName();
				i++;
			}

			return l;
		} catch (Exception e) {

			return null;
		}

	}

	public String[] toArrayStringAuto() {
		try {
			int i = 0, j = 0;
			Cumlelement[] liste = lne.toArray(new Cumlelement[] {});
			int n = liste.length;
			String l[] = new String[n];
			String l2[] = new String[n];
			for (i = 0; i < n; i++) {
				l[i] = liste[i].getName();
				l2[i] = l[i];
				for (j = 0; j < i; j++) {
					if (l2[i].compareTo(l2[j]) == 0) {
						l[i] = (liste[i]).getQualifiedName();
						l[j] = (liste[j]).getQualifiedName();
					}

				}
			}
			for (i = 0; i < n; i++) {
				if (l[i] == null) {
					l[i] = "";
				}
			}

			return l;
		} catch (Exception e) {

			MyManager.println("Exception Clist::toArrayStringAuto" + e);
			return null;
		}

	}

	public int indexOf(Element n) {
		int i = 0;
		for (Cumlelement e : lne) {
		
			if (e.getElement() == n)
				return i;
			if (e.getElement().equals(n))
				return i;

			i++;
		}
		return -1;

	}

	public int indexOf(Cumlelement uml) {
		int i = 0;
		Element n = uml.getElement();
		for (Cumlelement e : lne) {			
			if (e.getElement() == n)
				return i;
			if (e.getElement().equals(n))
				return i;

			i++;
		}
		return -1;

	}

}
