/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import fr.inria.base.MyManager;
import fr.inria.ctrte.time.parser.CCSLview;
import grammaire.LangLexer;
import grammaire.LangParser;
import grammaire.analyse.TransformAst;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

import antlr.BaseAST;
import antlr.collections.AST;

/**
 * @author Benoit Ferrero
 * 
 */
public class CmyConstraint extends Cumlelement {

	/**
	 * @param element
	 * @param mother
	 */
	//private boolean error = false;
	public String errorstr = "";
	private int ccslparsablerror = 0;

	private LangParser parserccsl = null;
	// private LangWalker walkerccsl =null;

	private Constraint cs;

	public HashMap<String, Integer> nameCount = null;

	public HashMap<String, String> qualifiedName2Name = null;

	/**
	 * @param element
	 */
	protected CmyConstraint(Constraint element) {
		super(element);
		cs = element;
	}

	@Override
	public Constraint getElement() {
		return cs;
	}

	public Clist getConstraintto() {
		// Constraint lne = (Constraint) element;
		EList<Element> le = cs.getConstrainedElements();
		Clist ls = new Clist();
		for (Element o2 : le) {
			Cumlelement uml = Cumlcreator.getInstance().createUmlElement(o2);
			uml.setMother(mother);
			ls.add(uml);

		}
		return ls;
	}

	public void clearListConstrainedElements() {
		cs.getConstrainedElements().clear();
	}

	public int setConstraintto(final CinterfaceUML l[]) {
		EList<Element> le = cs.getConstrainedElements();
		for (CinterfaceUML e : l) {
			le.add(e.getElement());
		}
		return 0;
	}

	public int build() {
		nameCount = new HashMap<String, Integer>();
		qualifiedName2Name = new HashMap<String, String>();
		Clist l = getConstraintto();
		for (Cumlelement uml : l.getLne()) {
			String s1 = uml.getName();
			String s2 = uml.getQualifiedName();
			Integer c;
			c = nameCount.get(s1);
			if (c == null) {

				c = new Integer(1);
				nameCount.put(s1, c);
			} else {
				Integer c1 = new Integer(c + 1);
				nameCount.put(s1, c1);
			}
			qualifiedName2Name.put(s2, s1);
			
		}
		return 0;
	}

	public int countRefId(String s1) {
		// s1 not is a path and haven t '.'
		Integer c;
		c = nameCount.get(s1);
		if (c == null)
			return 0;
		return c;
	}

	public EList<String> GetLanguage() {
		ValueSpecification specification = getSpecification();
		if (specification == null)
			return null;
		if ((specification instanceof OpaqueExpression) == false)
			return null;
		OpaqueExpression opaqExpr = (OpaqueExpression) specification;
		return opaqExpr.getLanguages();

	}

	public boolean isLanguage(String lg) {
		EList<String> l = GetLanguage();
		if (l == null)
			return false;
		for (String s : l) {
			if (s.compareTo(lg) == 0)
				return true;
		}
		return false;
	}

	public boolean hasLanguage() {
		EList<String> l = GetLanguage();
		if (l == null)
			return false;

		for (String s : l) {
			if (s.compareTo("") != 0)
				return true;
		}
		return false;
	}

	public ValueSpecification getSpecification() {
		return cs.getSpecification();
	}

	public boolean isCCSL() {
		return isLanguage(marteCCSL);
	}

	public boolean isTimeConstraint() {
		return isStereotypesByQualified(qualifiedName(marteTimeprofilQualified,
				marteClockConstraint));
	}

	public EList<String> getBodies() {
		ValueSpecification specification = getSpecification();
		if (specification == null)
			return null;
		if ((specification instanceof OpaqueExpression) == false)
			return null;
		OpaqueExpression opaqExpr = (OpaqueExpression) specification;
		return opaqExpr.getBodies();
	}

	public boolean isParsableCCSLConstainte() {
		if (!isTimeConstraint()) {
			ccslparsablerror = -5;
			return false;
		}
		if (getSpecification() == null) {
			ccslparsablerror = -4;
			return false;
		}
		if (!isCCSL()) {
			ccslparsablerror = -3;
			return false;
		}
		EList<String> l = getBodies();
		if (l.size() == 0) {
			ccslparsablerror = -2;
			return false;
		}
		if (l.get(0).length() == 0) {
			ccslparsablerror = -1;
			return false;
		}
		{
			ccslparsablerror = 1;
			return true;
		}
	}

	public int makeParsable() {
		try {
		
			if (isParsableCCSLConstainte()) {
				
				return 0;

			}
			//MyManager.println("..");  TODO remove
			Boolean test[] = new Boolean[7];
			test[0] = !isTimeConstraint();
			test[1] = (getSpecification() == null);
			if (test[1]) {
				test[2] = true;
				test[3] = true;
				test[4] = true;
				test[5] = true;
				test[6] = true;
			} else {
				test[2] = (getSpecification() instanceof OpaqueExpression);
				if (test[2]) {
					test[6] = hasLanguage();
					test[3] = !isCCSL();
					test[4] = (getBodies().size() == 0);
					if (test[4])
						test[5] = true;
					else
						test[5] = ((getBodies().get(0)).length() == 0);
				} else {
					test[3] = null;
					test[4] = null;
					test[5] = null;
				}
			}

			if (!test[2]) // <== (test[0] && !test[1] && !test[2] )
				return -1;

			if (!test[3] && test[6]) // bug
				return -1;

			if ((cs.getName() == null) || (cs.getName().compareTo("") == 0)) {
				MyManager.println("pas de nom");
				Shell shell = new Shell(SWT.BORDER | SWT.CLOSE);
				InputDialog id = new InputDialog(shell, "name ?",
						"give a name to", "", null);
				// IInputValidator
				id.create();
				id.open();
				String s = id.getValue();
				MyManager.println(s);
				if (s != null)
					cs.setName(s);
			}

			
			if (test[0]) {
				// CmyModel
				// m=Cumlcreator.getInstance().createUmlElement(element.
				// getModel());
				CmyPackage p = Cumlcreator.getInstance().createUmlElement(
						element).getRootPackage();

				Cumlview.appliedMARTE(p);
				Cumlview.appliedTime(p);
				Cumlelement x = p.getProfiles(true).getElement("Time");
				x.getElement().eSetDeliver(false);
				Cumlelement ster2 = x.allownedofE().getElement(
						"ClockConstraint");
				
				//ster2.getElement().eSetDeliver(false);		//TODO deliver	
				 cs.eSetDeliver(true);
				// cs.eResource().eSetDeliver(false);//TODO deliver
				cs.applyStereotype((Stereotype) ster2.getElement());
			}

			OpaqueExpression ope;
			if (test[1]) {
				ope = UMLFactory.eINSTANCE.createOpaqueExpression();
				cs.setSpecification(ope);
			} else {
				ope = (OpaqueExpression) cs.getSpecification();

			}

			if (test[3]) {
				List<String> lls = ope.getLanguages();
				lls.add("CCSL");
			}

			EList<String> bls = ope.getBodies();

			if (test[5]) {
				bls.add(0, "{  \n" + " }");
			}

		} catch (Exception e) {
			MyManager.println("erreur " + e);
			e.printStackTrace();
		}
		// Cumlview.touchModel(cs);
		return 0;
	}

	public int getCcslparsablerror() {
		return ccslparsablerror;
	}

	public String getCCSLBody(int n) {
		if (!isTimeConstraint())
			return null;
		if (getSpecification() == null)
			return null;
		
		if (!isCCSL())
			return null;
		
		EList<String> l = getBodies();
		if (l.size() == 0)
			return null;
		return l.get(0);
	}

	public int setCCSLBody(String s) {
		if (!isTimeConstraint())
			return -1;
		if (getSpecification() == null)
			return -1;
		
		if (!isCCSL())
			return -1;
		
		EList<String> l = getBodies();
		if (l.size() == 0)
			l.add(s);
		l.set(0, s);
		return 0;
	}

	// * n = bobyindex ( usually 0 )
	public boolean parseCCSL(int n) {
		if (ccslparsablerror <= 0)
			return false;
		String st = getCCSLBody(n);
		LangLexer lexer = new LangLexer(new StringReader(st));
		LangParser parser = new LangParser(lexer);
		parserccsl = parser;
		try {

			parserccsl.ccsl();
		} catch (Exception e) {
			// ((antlr.NoViableAltException) e).token
			errorstr = e.toString();
			// errorstr= parser.errorstr;
			MyManager.println("." + e);
			return false;
		}

		errorstr = parser.errorstr;
		return (parserccsl.errorNumber == 0);

	}

	public LangParser getParserccsl() {
		return parserccsl;
	}

	public AST getAST() {
		if (parserccsl == null)
			return null;
		return parserccsl.getAST();
	}

	private void astdisp(int n, AST a) {
		int i;
		for (i = 0; i < n; i++)
			MyManager.print("   ");
		MyManager.print(".<" + ((BaseAST) a).getText());
		MyManager.println(">");
		AST t = a.getFirstChild();

		while (t != null) {
			astdisp(n + 1, t);
			t = t.getNextSibling();
		}

	}

	public void dispast() {
		MyManager.println("ast");
		astdisp(0, getAST());

	}

	public void dispTreeWindows(int count) {
		AST a = getAST();
		// dispast();
		CCSLview.getCourant().dispAST( a);
		/*
		 * ASTFrame frame = new ASTFrame(CmessageBox.tree + getName(), a);
		 * frame.setBounds(50 + count 20, 110, 200, 350);
		 * frame.setVisible(true);
		 */
		return;
	}

	/*
	 * public boolean walkerCCSL() { walkerccsl = new LangWalker(); try {
	 * walkerccsl.ccsl((antlr.CommonAST) getAST());// } catch (Exception e) {
	 * e.printStackTrace(System.err); return false; } return true; }
	 * 
	 * public LangWalker getWalkerccsl() { //return walkerccsl; }
	 */

	public HashMap<String, Boolean> getlocalClock(HashMap<String, Boolean> hm) {
		if (parserccsl == null)
			return hm;
		return parserccsl.getDico().getClkdic().getlocalClock(hm);
	}

	public HashMap<String, Boolean> getboolean(HashMap<String, Boolean> hm) {
		if (parserccsl == null)
			return hm;
		return parserccsl.getDico().getBooldic().getnamebol(hm);

	}

	public int renameclock(HashMap<String, Boolean> hm) {

		if (parserccsl == null)
			return 0;
		for (String s : hm.keySet()) {
			if (hm.get(s).booleanValue()) {
				if (parserccsl.getDico().getClkdic().getVar(s) != null) {
					changename(s, s + "@" + cs.hashCode());
				}
			}
		}
		return 1;
	}

	public int changename(String old, String newb) {
		parserccsl.getDico().getClkdic().rename(old, newb);
		TransformAst.renameid(parserccsl.getAST(), old, newb);
		return 0;
	}

	public Cumlelement getContext() {
		NamedElement ne = cs.getContext();
		if (ne == null)
			return null;
		return Cumlcreator.getInstance().createUmlElement(ne);

	}

	public Cumlelement setContextwhithOwner() {

		Element ne = cs.getOwner();
		if (ne == null)
			return null;
		if (!(ne instanceof Namespace))
			return null;
		cs.setContext((Namespace) ne);

		return Cumlcreator.getInstance().createUmlElement(ne);

	}
}
