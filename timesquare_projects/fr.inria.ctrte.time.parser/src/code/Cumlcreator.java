/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;

import fr.inria.base.MyManager;

public class Cumlcreator extends Cbase {

	public static Cumlcreator instance = null;

	private Cumlcreator() {
		super();
	}

	public static Cumlcreator getInstance() {
		if (instance == null)
			instance = new Cumlcreator();
		return instance;
	}

	

	public Cumlelement createUmlElement(Element e) {
		
	/*	if (isUmlType("Class", e))
			return new CmyClass((Class) e);
		if (isUmlType("Property", e))
			return new CmyProperty((Property) e);
		if (isUmlType("Constraint", e))
			return new CmyConstraint((Constraint) e);
		if (isUmlType("Profile", e))
			return new CmyProfile((Profile) e);
		if (isUmlType("Model", e))
			return new CmyModel((Model) e);
		if (isUmlType("Package", e))
			return new CmyPackage((Package) e);
		if (isUmlType("InstanceSpecification", e))
			return new CmyInstance((InstanceSpecification) e);
		if (isUmlType("Stereotype", e))
			return new CmyStereotype((Stereotype) e);
		if (isUmlType("Enumeration", e))
			return new CmyEnumeration((Enumeration) e);
		if (isUmlType("EnumerationLiteral", e))
			return new CmyEnumerationLiteral((EnumerationLiteral) e);*/
		// choix par default		
		if (e instanceof Stereotype )
			return new CmyStereotype((Stereotype) e);	
		if (e instanceof Class)
			return new CmyClass((Class) e);
		if (e instanceof Property)
			return new CmyProperty((Property) e);		
		if ( e instanceof Model)	
			return new CmyModel((Model) e);
		if ( e instanceof Profile)
			return new CmyProfile((Profile) e);
		if (e instanceof Package)
			return new CmyPackage((Package) e);
		if ( e instanceof Enumeration)
			return new CmyEnumeration((Enumeration) e);
		if (e instanceof EnumerationLiteral)
			return new CmyEnumerationLiteral((EnumerationLiteral) e);
		if (e  instanceof InstanceSpecification)
			return new CmyInstance((InstanceSpecification) e);
		if ( e instanceof Constraint)
			return new CmyConstraint((Constraint) e);
		return new Cumlelement(e);
	}

	public CmyClass createUmlElement(Class e) // org.eclipse.uml2.uml.Class
	{
		if (e instanceof Stereotype)
			return new CmyStereotype((Stereotype) e);
		return new CmyClass(e);
	}

	public CmyConstraint createUmlElement(Constraint e) {
		return new CmyConstraint(e);

	}

	public CmyProfile createUmlElement(Profile e) {
		return new CmyProfile(e);

	}

	public CmyPackage createUmlElement(Package e) {
		if (e instanceof Model)
			return new CmyModel((Model) e);
		if (e instanceof Profile)
			return new CmyProfile((Profile) e);
		return new CmyPackage(e);

	}

	public CmyModel createUmlElement(Model e) {
		return new CmyModel(e);

	}

	public CmyInstance createUmlElement(InstanceSpecification e) {
		if (e instanceof EnumerationLiteral)
			return new CmyEnumerationLiteral((EnumerationLiteral) e);
		return new CmyInstance(e);

	}

	public CmyStereotype createUmlElement(org.eclipse.uml2.uml.Stereotype e) {
		return new CmyStereotype(e);

	}

	public CmyEnumeration createUmlElement(org.eclipse.uml2.uml.Enumeration e) {
		return new CmyEnumeration(e);

	}

	public CmyProperty createUmlElement(org.eclipse.uml2.uml.Property e) {
		return new CmyProperty( e);
	}
	
		
	public CmyEnumerationLiteral createUmlElement(
			org.eclipse.uml2.uml.EnumerationLiteral e) {
		return new CmyEnumerationLiteral(e);

	}

	
	public CmyProfile createProfil(URI urimodel) {
		CmyProfile prof = null;
		try {
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource2 = resourceSet.getResource(urimodel, true);
			Object object = EcoreUtil.getObjectByType(resource2.getContents(),
					UMLPackage.eINSTANCE.getProfile());
			if (object instanceof Profile) {
				prof = new CmyProfile((Profile) object);
			}
		} catch (WrappedException we) {
			MyManager.println(we.getMessage());
			MyManager.println(CmessageBox.errorLoading);
			// System.exit(1);
		}
		return prof;
	}

	/*
	 * public CmyProfile createProfil(Profile p) { return new CmyProfile(p); }
	 */

	/*
	 * public CmyModel createModel(Model e) { return new CmyModel(e); }
	 */

	public Cumlelement createElement(URI urimodel) {
		Cumlelement prof = null;
		try {
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource2 = resourceSet.getResource(urimodel, true);
			Object object = EcoreUtil.getObjectByType(resource2.getContents(),
					UMLPackage.eINSTANCE.getElement());
			MyManager.println(object);
			if (object instanceof Element) {
				prof = createUmlElement((Element) object);
			}
		} catch (WrappedException we) {
			MyManager.println(we.getMessage());
			MyManager.println(CmessageBox.errorLoading);
			// System.exit(1);
		}
		return prof;
	}

}
