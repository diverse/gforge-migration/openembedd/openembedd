/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Element;

public class Clistenumerationliteral extends Clist {

	public Clistenumerationliteral() {
		super();

	}

	public Clistenumerationliteral(Clist ol, String s) {
		super(ol, s);

	}

	public Clistenumerationliteral(Clist ol) {
		super(ol);

	}

	@Override
	protected boolean test(Element e) {
		return isUmlType(umlenumerationliteral, e);
	}

	public List<CmyEnumerationLiteral> getLneEnumrerationLiteral() {
		List<CmyEnumerationLiteral> local = new ArrayList<CmyEnumerationLiteral>();
		for (Cumlelement o1 : lne) {
			local.add((CmyEnumerationLiteral) o1);
		}
		return local;
	}

	@Override
	public Clistenumerationliteral getligth() {

		return (Clistenumerationliteral) super.getligth();
	}

	/*
	 * 
	 * @see code.Clist#getElement(java.lang.String) use Covariance
	 */
	@Override
	public CmyEnumerationLiteral getElement(String s) {
		return (CmyEnumerationLiteral) super.getElement(s);
	}

	/*
	 * 
	 * @see code.Clist#getElementQualified(java.lang.String) use Covariance
	 */
	@Override
	public CmyEnumerationLiteral getElementQualified(String s) {
		return (CmyEnumerationLiteral) super.getElementQualified(s);
	}

	/*
	 * 
	 * @see code.Clist#getC(int) use Covariance
	 */
	@Override
	public CmyEnumerationLiteral getC(int n) {
		return (CmyEnumerationLiteral) super.getC(n);
	}

}
