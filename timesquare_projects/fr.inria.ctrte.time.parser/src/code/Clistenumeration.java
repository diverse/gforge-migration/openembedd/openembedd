/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Element;

public class Clistenumeration extends Clist {

	public Clistenumeration() {
		super();

	}

	public Clistenumeration(Clist ol, String s) {
		super(ol, s);

	}

	public Clistenumeration(Clist ol) {
		super(ol);
	}

	@Override
	protected boolean test(Element e) {
		return isUmlType(umlenumeration, e);
	}

	public List<CmyEnumeration> getLneEnumreration() {
		List<CmyEnumeration> local = new ArrayList<CmyEnumeration>();
		for (Cumlelement o1 : lne) {
			local.add((CmyEnumeration) o1);
		}
		return local;
	}

	public Clistenumeration GetUnitEnumeration() {

		Clistenumeration l2 = new Clistenumeration();
		for (CmyEnumeration o : this.getLneEnumreration()) {
			boolean b = false;
			for (CmyEnumerationLiteral o2 : o.getEumerationliteral()
					.getLneEnumrerationLiteral()) {
				if (o2.getStereotypes().contient(marteUnitst)) {
					if (!b)
						l2.add(o);
					b = true;
				}
			}
		}
		return l2;
	}

	public Clistenumerationliteral GetUnitEnumerationLiterral() {

		return (Clistenumerationliteral) GetEnumerationLiterral()
				.getStereotypedby(marteUnitst);
	}

	public Clistenumerationliteral GetEnumerationLiterral() {

		Clistenumerationliteral l2 = new Clistenumerationliteral();
		for (CmyEnumeration o : this.getLneEnumreration()) {
			for (CmyEnumerationLiteral o2 : o.getEumerationliteral()
					.getLneEnumrerationLiteral()) {
				l2.add(o2);
			}
		}
		return l2;
	}

	@Override
	public Clistenumeration getligth() {

		return (Clistenumeration) super.getligth();
	}

	/*
	 * 
	 * @see code.Clist#getElement(java.lang.String) use Covariance
	 */
	@Override
	public CmyEnumeration getElement(String s) {
		return (CmyEnumeration) super.getElement(s);
	}

	/*
	 * 
	 * @see code.Clist#getElementQualified(java.lang.String) use Covariance
	 */
	@Override
	public CmyEnumeration getElementQualified(String s) {
		return (CmyEnumeration) super.getElementQualified(s);
	}

	/*
	 * 
	 * @see code.Clist#getC(int) use Covariance
	 */
	@Override
	public CmyEnumeration getC(int n) {
		return (CmyEnumeration) super.getC(n);
	}
}
