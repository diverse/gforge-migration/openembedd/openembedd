/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Stereotype;

public class Cliststereotype extends Clist {

	boolean reduits;

	public Cliststereotype(Clist l, boolean reduits) {
		super(l);
		this.reduits = reduits;
		// Copie(l);
	}

	public Cliststereotype(Clist l) {
		super(l);
		this.reduits = false;
		// Copie(l);
	}

	@Override
	protected boolean test(Element e) {
		return isUmlType(umlstereotype, e);
	}

	public Cliststereotype() {
		super();

	}

	public int add(NamedElement e) {
		if (isUmlType(umlstereotype, e)) {
			lne.add(nne, Cumlcreator.getInstance().createUmlElement(e));
			return nne++;
		}
		return 0;
	}

	@Override
	public Cliststereotype getligth() {
		Cliststereotype l = new Cliststereotype();
		for (Cumlelement o1 : lne) {
			NamedElement o = (NamedElement) o1.getElement();

			if (l.getLne().contains(o1) == false)
				l.add(o);

		}
		return l;
	}

	//
	public Clistprofile GetProfileList() {
		Clistprofile lp = new Clistprofile();
		for (Cumlelement o1 : lne) {
			// NamedElement o= ;

			Stereotype s = (Stereotype) o1.getElement();

			lp.add(s.getProfile());

		}
		return lp;

	}

	public List<CmyStereotype> getLneStereotype() {
		List<CmyStereotype> local = new ArrayList<CmyStereotype>();
		for (Cumlelement o1 : lne) {

			local.add((CmyStereotype) o1);

		}
		return local;
	}

	/*
	 * 
	 * @see code.Clist#getElement(java.lang.String) use Covariance
	 */
	@Override
	public CmyStereotype getElement(String s) {
		return (CmyStereotype) super.getElement(s);
	}

	/*
	 * 
	 * @see code.Clist#getElementQualified(java.lang.String) use Covariance
	 */
	@Override
	public CmyStereotype getElementQualified(String s) {
		return (CmyStereotype) super.getElementQualified(s);
	}

	/*
	 * 
	 * @see code.Clist#getC(int) use Covariance
	 */
	@Override
	public CmyStereotype getC(int n) {
		return (CmyStereotype) super.getC(n);
	}

}
