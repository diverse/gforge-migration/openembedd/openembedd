/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

public class CmessageBox {

	public static final String lines = "_________________________";
	public static final String pluginTime = "Plug-in Time:Contraint ";
	public static final String tree = "Tree of ";
	public static final String numberelement = "Number of elements ";
	public static final String liste = "Clist ___";
	public static final String fileNameTimeParser = "time.parser";
	// public static final String exception = "EXCEPTION: ";
	public static final String setValueIn = "SETVALUE IN ____";
	public static final String warning = "WARNING: ";
	public static final String error = "ERROR: ";
	public static final String errorLoading = "Error while loading...abort";

	public static final String noName = "<<No name>>";
	public static final String noNameQualified = "<<No qualified name>>";

	public static final String errorUnit1 = "Unknown Enumeration Litteral Unit";
	public static final String errorUnit3 = "Ambiguous Unit (Use QualifiedName instead) ";
	public static final String errorUnit2 = "contains non Unit enumerationliterals ";
	public static final String errorTime1 = "Time Profile is not applied";
	// public static final String askUnit1 =
	// "Corection Automatique \nEnumeration literal non valide\nName : Ok qualified Name: Nok"
	// ;

	public static final String localmessageBy = "and such that ";

	public static final String localmessage1 = " is locally defined and constrained  \n\t";
	public static final String localmessage2 = " is local";
	public static final String localmessage3 = " is constrained \n\t";
	public static final String localmessage4 = " is neither locally defined nor constrained       \n";
	public static final String localmessage5 = " has multiple local definitions";

	public static final String errorConstraint5 = "Not a Time::ClockConstraint  ==> No Parsing";
	public static final String errorConstraint4 = "empty constraint specification   ==> No Parsing";
	public static final String errorConstraint3 = "specification language is not CCSL  ==> No Parsing ";
	public static final String errorConstraint2 = "no specification body  ==> No Parsing";
	public static final String errorConstraint1 = "empty specification body ==> No Parsing ";
	public static final String errorConstraintUK = "Error Unknown";

	/*** Message for CmyInstance.informationTimeClock */
	public static String timeClockmessageLogical(String s, String s1, String s2) {
		return "Logical " + s + " << clock >>, Unit=" + s2 + " from " + s1
				+ "\n";
	}

	public static String timeClockmessageChronometric(String s, String s1,
			String s2) {
		return "Chronometric " + s + " << clock >>, Unit=" + s2 + " from " + s1
				+ "\n";
	}

	public static String timeClockmessageNoClockType() {
		return " <<clock>> whose type is not a <<clockType>> ";
	}

	public static String timeClockmessageNoClock() {
		return " <<clock>> missing ";
	}

}
