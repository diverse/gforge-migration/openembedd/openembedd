/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package code.view;

import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.uml2.uml.NamedElement;

import antlr.BaseAST;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.time.parser.ActivatorTimeParser;
import fr.inria.vcd.antlr.editors.VcdMultiPageEditor;
import fr.inria.wave.DictionaryPolitic;
import fr.inria.wave.VCDfile;

import grammaire.analyse.ASTMask;
import grammaire.analyse.Analysis;
import grammaire.analyse.TransformAst;

public class SimulationCall implements Runnable {
	// extends Thread {

	private BaseAST base = null;
	private Analysis ana = null;
	private NamedElement elem = null;
	private int length;
	private int mode;
	private int pulsed;
	private VCDfile vcdfile;
	//private VCDDefinitions vcd = null;
	private VcdMultiPageEditor vcdeditor = null;
	private IPath local = null;
	private String namefile = null;
	private boolean ghost = false;
	private String timescl = null;
	private IFile filesys= null;

	public SimulationCall(ASTMask ast, HashMap<String, Boolean> hmb, IFile file) {

		this.base = (BaseAST) TransformAst.revolveguard(ast.getA(), hmb);  
		(new ASTMask(base)).dispAst("resolve CCSL");		
		filesys=file;

	}

	public SimulationCall(ASTMask ast, HashMap<String, Boolean> hmb,
			NamedElement elem) {
		this.base = (BaseAST) TransformAst.revolveguard(ast.getA(), hmb);
		(new ASTMask(base)).dispAst("resolve UMLe");
		this.elem = elem;

	}
	
	public SimulationCall(ASTMask ast, HashMap<String, Boolean> hmb,
			NamedElement elem, IFile file) {
		this.base = (BaseAST) TransformAst.revolveguard(ast.getA(), hmb);
		(new ASTMask(base)).dispAst("resolve UML file");
		this.elem = elem;
		filesys=file;

	}
	

	public void setup(int l, int m, boolean p, boolean g, String timescale) {
		length = l;
		mode = m;
		pulsed = p ? 1 : 0;
		ghost = g;
		timescl = timescale;
	}

	public void run() {
		try {

			IPath ipath = null;
			MyManager.println("Simultaion go...");
			ipath = MyPluginManager.getIpath(ActivatorTimeParser.getDefault());
			//MyManager.println(ipath.toOSString());
			//
			generecode(ipath);
			//
			
			return;
		} catch (Exception e) {
			MyManager.println(e);
			return;
		} catch (Error e) {
			MyManager.println(e);
			return;
		}
	}

	
	private String getDirE()
	{
		int n = elem.eResource().getURI().segmentCount() - 1;
		String filename = elem.eResource().getURI().segment(n);
		filename = filename.substring(0, filename.length()
				- elem.eResource().getURI().fileExtension().length() - 1);
		MyManager.println(filename);
		return filename;
	}
	private String getNameE()
	{
		return  elem.eResource().getURI().toPlatformString(true);
	}
	
	private String getPackageE()
	{
		return  MyPluginManager.convertName(elem.getName());
	}
	private int generecode(IPath ipath2) {
		try {
			//MyManager.println("Start Simulation");
			String name = null;
			String s = MyManager.getDateTime();
			String filename =null;
			/** File Outpuit */
			if (elem!=null)
			{
				//UML simulation
				if (filesys==null)
				{
					name= getNameE();
				
				}
				else
				{
					name= filesys.getFullPath().toString();
					//filename="UMLRT";
				}
				filename = getDirE();	
				//MyManager.println(name + " " + s + " " + elem.getQualifiedName());
				String packagename =getPackageE();
				namefile ="Sim_" + packagename + "_" + s;
			}
			else
			{
				//Textutal simulation
				name= filesys.getFullPath().toString();			
				filename ="CCSL";
				String packagename =filesys.getName();
				namefile ="Sim_TEXTUAL_"+ packagename+"_"+ s;
			}
			IPath folder = MyPluginManager.getfolder(name);
			local = MyPluginManager.createdir(folder, "simul");
			local= MyPluginManager.createdir(local, filename);			
			
			IFile f = MyPluginManager.touchFile(local.append(namefile + ".vcd"));
			
			MyPluginManager.refreshWorkspace();
			/***/
			
			vcdfile = new VCDfile();
			vcdfile.openfile(local, namefile, true);
			if (timescl == null) {
				vcdfile.fileHeader(null, null);
			} else {
				int ln = timescl.indexOf(" ");
				String s1 = timescl.substring(0, ln);

				String s2 = timescl.substring(4, 6);
				if (s2.compareTo(" s") == 0)
					s2 = "s";

				vcdfile.fileHeader(s1, s2);
			}
			//vcd = vcdfile.getSc().getVcd();
			/** AST Charcutage */
			ana = new Analysis(vcdfile);
			BaseAST b = (BaseAST) TransformAst.transformAST(base); // give name to clockexpress 
			ana.found(b); // AST 2 MyFormat
			/***/
			MyManager.println("running");
			vcdfile.setPulse(pulsed);	// option pulse
			vcdfile.prerun();
			vcdeditor = VcdMultiPageEditor.createNewEditor( vcdfile.getSc().getVcd(), f);	//vcd		
			vcdeditor.setSimulation(true);
			vcdeditor.setSimulationAvance(1);
			vcdeditor.getTc().setPartial(true);
			vcdeditor.setGhostMode(ghost);

	
			vcdfile.setFirepol(DictionaryPolitic.getInstance().getCname(mode)); // Set Simulation Politic
			vcdfile.run(length, vcdeditor);
		
			vcdeditor.getTc().setPartial(false);
			if (vcdeditor.getMyShell() != null)
				vcdeditor.update(vcdfile.getSc().getsize() - 1, vcdfile.getSc()
						.getsize(), true);
			vcdfile.closefile();
			
			MyManager.println("Simulation End");
			vcdeditor.setSimulationAvance(6);
			vcdeditor.setFocus();
			vcdeditor.refresh();
		
			//MyPluginManager.refreshWorkspace();
			if (vcdfile.linearize(vcdeditor)==1)
			{
				vcdeditor.refresh();	
				vcdeditor.doSaveAs();
				vcdeditor.getEditorInput().getPersistable().saveState(null);
				MyPluginManager.refreshWorkspace();
			}

		} catch (Exception e) {
			MyManager.printError(e);
			vcdeditor.setSimulation(false);
			vcdeditor=null;
			//vcd=null;
			vcdfile=null;
			return -1;
		} catch (Error e) {
			MyManager.printError(e);
			//vcd=null;
			vcdfile=null;
			vcdeditor.setSimulation(false);
			vcdeditor=null;
			return -1; 
		}
		//vcd=null;
		vcdfile=null;
		vcdeditor.setSimulation(false);
		vcdeditor=null;
		return 1;
	}
}