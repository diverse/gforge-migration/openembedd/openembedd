/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

/**
 * @author Benoit Ferrero
 * 
 */
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.Stereotype;

import fr.inria.base.MyManager;

public class Cumlelement extends Cbase implements CinterfaceUML {
	protected Element element = null;

	protected Element mother = null;

	private int ctrl;

	private boolean extend = false;

	//private boolean b;

	// boolean ok=false;
	public static ResourceSet resourceSet = new ResourceSetImpl();

	protected Cumlelement(Element ne) {
		super();
		this.element = ne;	
	//	MyManager.println( ((XMLResourceImpl) element.eResource()).getEObjectToIDMap().get(element));
		if (ne != null)
			this.mother = ne.getOwner();
	}

	protected int getCtrl() {
		return ctrl;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getMembres()
	 */
	public Clist getMembres() {
		if (element instanceof Namespace) {
			Clist l = new Clist();

			EList<NamedElement> le = ((Namespace) element).getMembers();
			for (NamedElement e : le) {

				l.add(e, element);

			}
			return l;
		}
		return null;
	}

	
	public void addComment (String s)
	{
		Comment c=element.createOwnedComment();
		c.setBody(s);		
	}
	/* (non-Javadoc)
	 * @see code.CinterfaceUML#allownedofE()
	 */
	public Clist allownedofE() {
		Clist ls = new Clist();
		EList<Element> cc = element.allOwnedElements();
		for (Element o2 : cc) {

			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}
		return ls;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getownedofE()
	 */
	public Clist getownedofE() {

		Clist ls = new Clist();

		EList<Element> cc = element.getOwnedElements();
		// if (cc!=null)
		for (Element o2 : cc) {
			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}

		return ls;

	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getPackage()
	 */
	public CmyPackage getPackage() {
		Package p = element.getNearestPackage();
		if (p == null) {
			if (element instanceof Package)
				return Cumlcreator.getInstance().createUmlElement(
						((Package) element));
			return null;

		}
		return Cumlcreator.getInstance().createUmlElement(p);
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getRootPackage()
	 */
	public CmyPackage getRootPackage() {

		Package p = element.getNearestPackage();
		if (p == null) {
			if (element instanceof Package)
				return Cumlcreator.getInstance().createUmlElement(
						((Package) element));
			return null;
		}
		while (p.getOwner() != null) {
			p = p.getOwner().getNearestPackage();
		}
		return Cumlcreator.getInstance().createUmlElement(p);

	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getListPackageElement()
	 */
	public Clist getListPackageElement() {
		if ((element instanceof Namespace) == false)
			return null;
		EList<PackageableElement> cc = ((Namespace) element)
				.getImportedMembers();
		Clist ls = new Clist();
		ls.copie(allownedofE());
		for (PackageableElement o : cc) {
			ls.add(o);
		}
		return ls;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cumlelement)
			if (this.element.equals(((Cumlelement) obj).getElement())) // )
				return true;
		return false;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getElement()
	 */
	public Element getElement() {
		return element;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getStereotypes()
	 */
	public Cliststereotype getStereotypes() {
		return getStereotypes(true);

	}
	
	public boolean isStereotyped() {
		if(element.getAppliedStereotypes() == null || element.getAppliedStereotypes().size()==0)
			return false;
		else
			return true;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getStereotypes(boolean)
	 */
	public Cliststereotype getStereotypes(boolean b) {
		Cliststereotype l = new Cliststereotype();
		EList<Stereotype> le = element.getAppliedStereotypes();
		for (Stereotype e : le) {
			l.add(e, element);

		}
		return l;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#isExtend()
	 */
	public boolean isExtend() {
		return extend;
	}

	protected void setCtrl(int ctrl) {
		this.ctrl = ctrl;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#setExtend(boolean)
	 */
	public void setExtend(boolean extend) {
		this.extend = extend;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getMother()
	 */
	public Element getMother() {
		return mother;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#setMother(org.eclipse.uml2.uml.Element)
	 */
	public void setMother(Element mother) {
		this.mother = mother;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getRelativeName(java.lang.String)
	 */
	public String getRelativeName(String s) {
		
		//
		if (element instanceof NamedElement) {
			String se = getQualifiedName();
			if (se.startsWith(s)) {
				return se.substring(s.length());
			}
			return se;
		}
		if (element instanceof ElementImport) {

			return "." + getName();
		}
		return CmessageBox.noName;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getName()
	 */
	public String getName() {
		
		if (element instanceof NamedElement) {
			String s = ((NamedElement) element).getName();
			if (s == null)
				return "";
			return s;
		}
		if (element instanceof ElementImport) {
			// l.add(e);
			String s = ((ElementImport) element).getName();
			if (s == null)
				return "";
			return s;
		}
		if (element instanceof ProfileApplication) {
			// l.add(e);
			Profile p = ((ProfileApplication) element).getAppliedProfile();
			if (p == null)
				return "";
			String s = p.getName();
			if (s == null)
				return "";
			return s;
		}
		if (element instanceof PackageImport) {
			// l.add(e);
			String s = ((PackageImport) element).getImportedPackage().getName();
			if (s == null)
				return "";
			return s;
		}

		return CmessageBox.noName;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getQualifiedName()
	 */
	public String getQualifiedName() {
		if (element instanceof NamedElement) {
			String s = ((NamedElement) element).getQualifiedName();
			if (s == null)
				return "";
			return s;

		}
		if (element instanceof ElementImport) {
			String s = ((ElementImport) element).getName();
			if (s == null)
				return "";
			return s;
		}
		return CmessageBox.noNameQualified;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getQuotedName()
	 */
	public String getQuotedName() {
		if (element instanceof NamedElement) {
			return " `" + getName() + "' ";
		}
		return CmessageBox.noName;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getQuotedQualifiedName()
	 */
	public String getQuotedQualifiedName() {
		if (element instanceof NamedElement) {
			return " `" + getQualifiedName() + "' ";

		}
		return CmessageBox.noNameQualified;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getMotherName()
	 */
	public String getMotherName() {
		if (mother instanceof NamedElement) {
			return ((NamedElement) mother).getName();
		}
		return CmessageBox.noName;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getMotherQualifiedName()
	 */
	public String getMotherQualifiedName() {
		if (mother instanceof NamedElement) {
			return ((NamedElement) mother).getQualifiedName();
		}
		return CmessageBox.noNameQualified;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#isStereotypedBy(java.lang.String)
	 */
	public boolean isStereotypedBy(String s) {
		return getStereotypes().contient(s);
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#isStereotypesByQualified(java.lang.String)
	 */
	public boolean isStereotypesByQualified(String s) {
		return getStereotypes().contientQualified(s);
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#hasAttributeInStereotype(java.lang.String, java.lang.String)
	 */
	public boolean hasAttributeInStereotype(String stereoname, String name) {
		// Object o=null;
		if (getStereotypes().contient(stereoname)) {

			try {
				Stereotype stereo = getStereotypes().getElement(stereoname)
						.getElement();
				return element.hasValue(stereo, name);
				// return (o!=null);
			} catch (Exception ex) {
				MyManager.printError(ex);
				return false;
			}

		}

		return false;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getAttributeStereotype(java.lang.String, java.lang.String)
	 */
	public Object getAttributeStereotype(String stereoname, String name) {
		if (getStereotypes().contient(stereoname)) {
			try {
				Stereotype stereo = getStereotypes().getElement(stereoname)
						.getElement();
				return element.getValue(stereo, name);
			} catch (Exception ex) {
				MyManager.printError(ex);
				return null;
			}
		}
		MyManager.println("uk stereo");
		return null;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#setStereotypeAttributValue(java.lang.String, java.lang.String, java.lang.Object)
	 */
	public void setStereotypeAttributValue(String stereoname, String name,
			Object value) {
		if (getStereotypes().contient(stereoname)) {
			try {
				Stereotype stereo = getStereotypes().getElement(stereoname).getElement();
			
				//element.eContainer().eSetDeliver(false);//TODO deliver
			//	element.eSetDeliver(false);//TODO deliver
			//	stereo.eSetDeliver(false);//TODO deliver
			//	stereo.eContainer().eSetDeliver(false);//TODO deliver
				Object o= element.getValue(stereo, name);
				if (o instanceof EObject)
				{
					//((EObject) o).eSetDeliver(false);//TODO deliver
					
				}
				if (value instanceof EObject)
				{
					//((EObject) value).eSetDeliver(false);//TODO deliver
					
				}
				element.setValue(stereo, name, value);
				return;
			} catch (Exception ex) {
				MyManager.printError(ex, "< " + CmessageBox.setValueIn + " >");
				return;
			}
		}

		return;

	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getType()
	 */
	public String getType() {
		return element.eClass().getName();

	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getIcone()
	 */
	public Image getIcone() {
		if (element instanceof ElementImport) {
			ElementImport e = (ElementImport) element;
			return Cumlcreator.getInstance().createUmlElement(
					e.getImportedElement()).getIcone();

		}
		if (element instanceof PackageImport) {
			PackageImport pe = (PackageImport) element;
			return Cumlcreator.getInstance().createUmlElement(
					pe.getImportedPackage()).getIcone();

		}
		return null;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getinfo(int)
	 */
	public String getinfo(int x) {
		String s = getName();
		if ((x & 1) == 1)
			return "(" + getType() + ") " + s;
		return s;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#toString()
	 */
	@Override
	public String toString() {
		return getType() + " " + getQualifiedName();
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getModelof()
	 */
	public CmyModel getModelof() {
		if (element == null)
			return null;
		if (element.getModel() == null)
			return null;
		return new CmyModel(element.getModel());
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#setName(java.lang.String)
	 */
	public void setName(String name) {
		if (element instanceof NamedElement)
			((NamedElement) element).setName(name);
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#createConstraintRule(java.lang.String)
	 */
	public CmyConstraint createConstraintRule(String name) {
		if (element instanceof Namespace) {
			Constraint c = ((Namespace) element).createOwnedRule(name);
			if (c != null)
				return Cumlcreator.getInstance().createUmlElement(c);
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#createPackageImport(org.eclipse.uml2.uml.Package)
	 */
	public CmyPackage createPackageImport(Package p) {
		if (element instanceof Namespace) {
			PackageImport c = ((Namespace) element).createPackageImport(p);
			if (c != null)
				return Cumlcreator.getInstance().createUmlElement(
						c.getImportedPackage());

		}

		return null;

	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#createElementImport(org.eclipse.uml2.uml.Element, java.lang.String)
	 */
	public Cumlelement createElementImport(Element e, String s) {
		if ((e instanceof PackageableElement)) {
			if (element instanceof Namespace) {
				ElementImport i = ((Namespace) element)
						.createElementImport((PackageableElement) e);
				if (s != null)
					i.setAlias(s);
				return Cumlcreator.getInstance().createUmlElement(i);
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see code.CinterfaceUML#getPackageImport()
	 */
	public Clist getPackageImport() {
		Clist ls = new Clist();
		if (element instanceof Namespace) {
			EList<PackageImport> lpi = ((Namespace) element)
					.getPackageImports();
			for (PackageImport pi : lpi) {
				ls.add(pi.getImportedPackage());
			}

		}

		return ls;

	}

	public String getXMIID()
	{
		return  ((XMLResourceImpl) element.eResource()).getEObjectToIDMap().get(element); 
	}
	
}
