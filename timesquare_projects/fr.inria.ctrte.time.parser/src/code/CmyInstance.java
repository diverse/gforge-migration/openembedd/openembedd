/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.Stereotype;

import fr.inria.base.MyManager;

public class CmyInstance extends Cumlelement implements CmyClock {
	private InstanceSpecification instance;

	protected CmyInstance(InstanceSpecification instance) {
		super(instance);
		this.instance = instance;

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#isClockStereotype()
	 */
	public boolean isClockStereotype() {
		if (getStereotypes().contient(marteClock))
			return true;
		return false;
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#isMarteClock()
	 */
	public boolean isMarteClock() {
		if (isClockStereotype())
			if (getClassifier().isMarteClockType())
				return true;
		return false;
	}

	public CmyClass getClassifier() {
		try {
			EList<Classifier> l = instance.getClassifiers();
			if (l.size() == 0)
				return null;
			return new CmyClass((Class) l.get(0));
		} catch (Exception e) {
			MyManager.print("Instance not have a Classifier");
			e.printStackTrace();
			return null;

		}
	}

	public boolean isContientAttribut(String n) {

		CmyClass c = getClassifier();
		if (c == null)
			return false;
		return c.getAttributes().contient(n);
	} 

	public boolean isContientOperation(String n) {

		CmyClass c = getClassifier();
		if (c == null)
			return false;
		return c.getOperations().contient(n);
	}

	public Cumlelement getAttribut(String n) {

		CmyClass c = getClassifier();
		if (c == null)
			return null;
		return c.getAttributes().getElement(n);
	}

	public Cumlelement getOperation(String n) {

		CmyClass c = getClassifier();
		if (c == null)
			return null;
		return c.getOperations().getElement(n);
	}

	public Cumlelement getSlot(String n) {
		if (isContientAttribut(n) == false)
			return null;
		Clist l = getSlots();
		for (Cumlelement o : l.getLne()) {
			((Slot) o.getElement()).getDefiningFeature();
		}
		return null;
	}

	public Clist getSlots() {
		EList<Slot> cc = instance.getSlots();
		Clist ls = new Clist();
		for (Slot o2 : cc) {
			ls.add(Cumlcreator.getInstance().createUmlElement(o2));

		}
		return ls;
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#isLogicalClock()
	 */
	public Boolean isLogicalClock() {
		CmyClass c = getClassifier();
		if (c == null)
			return false;
		return c.isLogicalClockType();

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#getUnit()
	 */
	public CmyEnumerationLiteral getUnit() {
		Object o = getAttributeStereotype("Clock", "unit");
		
		if (o instanceof EnumerationLiteral)
			return new CmyEnumerationLiteral((EnumerationLiteral) o);
	
		// for profil Marte :: Time
		if (o instanceof org.eclipse.emf.ecore.impl.DynamicEObjectImpl) {
			org.eclipse.emf.ecore.impl.DynamicEObjectImpl x=((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o);
			if ( x.eCrossReferences()==null)
				return null;
			if (x.eCrossReferences().size()==0)
				return null;
			Object o2 = ((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o)	.eCrossReferences().get(0);
			
			if (o2 instanceof EnumerationLiteral) {
				
				return new CmyEnumerationLiteral((EnumerationLiteral) o2);
			}
			o2 = ((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o)
					.eCrossReferences().get(1);
			
			if (o2 instanceof EnumerationLiteral) {
				
				return new CmyEnumerationLiteral((EnumerationLiteral) o2);
			}
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#setUnit(code.CmyEnumerationLiteral)
	 */
	public int setUnit(CmyEnumerationLiteral cmel) {
		if (!isClockStereotype())
			return -1;
		if (getClassifier()==null)
				return -5;
		CmyEnumeration cme = getClassifier().getUnitType();
		if (cme == null)
			return -4;
		if (!cme.getQualifiedName().equals(cmel.getMotherQualifiedName())) {
			MyManager.println("Error  :" + cme.getQualifiedName() + " "
					+ (cmel.getMotherQualifiedName()));
			return -2;
		}
		Stereotype st = getStereotypes().getElement("Clock").getElement();	
		Property p = ( st.getAttributes().get(3));
		Stereotype stereo2 =  //cmel.getStereotypes().getElement("Unit").getElement() ;
			((Stereotype) p.getType());
		EnumerationLiteral ele = cmel.getElement();
		if (ele == null)
			return -3;
		EObject value = ele.getStereotypeApplication(stereo2);
	
		//setStereotypeAttributValue("Clock", "unit", null);
		//value.eSetDeliver(false);
		//value.eContainer().eSetDeliver(false);
		//cmel.getElement().eSetDeliver(false);  //TODO deliver
		setStereotypeAttributValue("Clock", "unit", value); 
		
		return 1;
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#getStandard()
	 */
	public CmyEnumerationLiteral getStandard() {
		Object o = getAttributeStereotype("Clock", "standard");

		if (o instanceof EnumerationLiteral)
			return new CmyEnumerationLiteral((EnumerationLiteral) o);

		return null;

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#setStandard(code.CmyEnumerationLiteral)
	 */
	public int setStandard(CmyEnumerationLiteral standard) {

		setStereotypeAttributValue("Clock", "standard", standard.getElement());

		return 0;

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#ClockUnitValide()
	 */
	public Enumtesttype ClockUnitValide() {
		try {
			if (!isMarteClock())
				return Enumtesttype.eNoMarte;
			CmyEnumerationLiteral el = getUnit();
			if (getClassifier() == null)
				return Enumtesttype.eNoClassifier;
			if (el == null)
				return Enumtesttype.eNoUnit;
			CmyEnumeration e = getClassifier().getUnitType();

			if (e == null)
				return Enumtesttype.eUnitNok;

			if (e.has(el) == true)
				return Enumtesttype.eUnitOk;

			if (e.has(el.getName())) {
				MyManager.println(CmessageBox.warning);
				return Enumtesttype.eUnitPartialNok;
			}
			MyManager.println(CmessageBox.error);
			return Enumtesttype.eUnitNok;
		} catch (Exception e) {
			
			MyManager.printError(e);
			return Enumtesttype.eUnitNok;
		}
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#ClockUnitAutoCorrection()
	 */
	public void ClockUnitAutoCorrection() {

		if (!isMarteClock())
			return;
		CmyEnumerationLiteral el = getUnit();
		CmyEnumeration e = getClassifier().getUnitType();
		if (e == null)
			return;
		if (el == null)
			return;
		if (e.has(el))
			return;
		if (e.has(el.getName())) {
			Cumlelement newel = e.getEumerationliteral().getElement(
					el.getName());
			setStereotypeAttributValue(marteClock, marteUnitfld, newel
					.getElement());
			return;
		}
		return;

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#informationTimeClock()
	 */
	public String informationTimeClock() {
		if (isMarteClock()) {
			String s = getClassifier().getClockTypeNatureString();
			String s1 = nullstr;
			String s2 = nullstr;
			Cumlelement uml = getClassifier().getUnitType();
			if (uml != null)
				s1 = uml.getName();
			Cumlelement uml2 = getUnit();
			if (uml2 != null)
				s2 = uml2.getQuotedName();
			Boolean b = isLogicalClock();
			if (b == null)
				return "";
			if (b)
				return CmessageBox.timeClockmessageLogical(s, s1, s2);
			return CmessageBox.timeClockmessageChronometric(s, s1, s2);
		}
		if (isClockStereotype())
			return CmessageBox.timeClockmessageNoClockType();
		return CmessageBox.timeClockmessageNoClock();

	}

	@Override
	public InstanceSpecification getElement() {
		return instance;
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#applyClock()
	 */
	public void applyClock() {
		if (isClockStereotype())// isMarteClock())
			return;
		CmyPackage p = getRootPackage();
		if (p != null) {
			if (!p.getProfiles(true).contient("Time"))
				Cumlview.appliedTime(p);
			CmyStereotype cms = (CmyStereotype) p.getProfiles(true)
					.getownedofE().getElement("Clock");
			//MyManager.println("1" + cms);
			//cms.getElement().eSetDeliver(false);	//TODO deliver		
			instance.eSetDeliver(true);
			//instance.eResource().eSetDeliver(false);//TODO deliver
			instance.applyStereotype(cms.getElement());
			return;
		}
		CmyProfile cmp = Cumlcreator.getInstance().createUmlElement(
				Cumlview.getTimeProfil());
		CmyStereotype cms = (CmyStereotype) cmp.getownedofE().getElement(
				"Clock");
		//MyManager.println("2" + cms);
		//cms.getElement().eSetDeliver(false);	//TODO deliver		
		instance.eSetDeliver(true);
		//instance.eResource().eSetDeliver(false);//TODO deliver
		instance.applyStereotype(cms.getElement());
		if (getClassifier()!=null)
		{
			if (getClassifier().isMarteClockType())
			{
				CmyStereotype cs = getClassifier().getStereotypes().getElement("ClockType");
				EObject eo = getClassifier().getElement().getStereotypeApplication(
						cs.getElement());

				setStereotypeAttributValue("Clock", "type", eo);
			}
		}
	}

	/* (non-Javadoc)
	 * @see code.CmyClock#setTypeClassifier(code.CmyClass)
	 */
	public void setTypeClassifier(CmyClass c) {
		if (c.isMarteClockType()) {

			Object o = getAttributeStereotype("Clock", "type");
		
			if (o instanceof org.eclipse.emf.ecore.impl.DynamicEObjectImpl) {
				org.eclipse.emf.ecore.impl.DynamicEObjectImpl dl = (org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o;
				if (dl.eCrossReferences() != null
						&& dl.eCrossReferences().size() >= 2) {
					EObject o2 = ((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o)
							.eCrossReferences().get(1);

					if (instance.getClassifiers().contains(o2)) {
						instance.getClassifiers().remove(o2);
					}
				}
			}
			// instance.getClassifiers().clear();
			if (!instance.getClassifiers().contains(c.getElement())) {
				c.getElement().eSetDeliver(false);
				//instance.eSetDeliver(false);  //TODO deliver
				instance.getClassifiers().add(0,c.getElement());
			}
			CmyStereotype cs = c.getStereotypes().getElement("ClockType");
			EObject eo = c.getElement().getStereotypeApplication(
					cs.getElement());

			setStereotypeAttributValue("Clock", "type", eo);

		}

	}

	/* (non-Javadoc)
	 * @see code.CmyClock#getTypeClassifier()
	 */
	public CmyClass getTypeClassifier() {
		Object o = getAttributeStereotype("Clock", "type");

		if (o instanceof Class)
			return Cumlcreator.getInstance().createUmlElement((Class) o);

		return null;
	}

	@Override
	public Image getIcone() {
		if (isClockStereotype()) {
			return Cbase.IMG_CLOCK;
		}
		return Cbase.IMG_INSTANCE;
	}

	@Override
	public String getinfo(int x) {
		if (isClockStereotype()) {
			String s = "<<Clock>> " + getName() + " :";
			if (getUnit() != null)
				s += "( unit :" + getUnit().getName();
			else
				s += "( unit is unknown ";
			if (isLogicalClock() == null || isLogicalClock() == true)
				s += " ";
			else
				s += "; std :" + getStandard().getName();
			s += " ) ";

			return s;
		}
		return super.getinfo(x);
	}
	
	
	public boolean isChronometricClcok()
	{
		if (getClassifier()!=null)
		{
			if ( getClassifier().isLogicalClockType() )
				if ( getClassifier().isLogicalClockType() == false)					
			{
					return true ;
			}
		}
			
			
		return false;
	}
	

}
