package code;

import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;

public interface CinterfaceUML {

	public Clist getMembres();

	public Clist allownedofE();

	public Clist getownedofE();

	public CmyPackage getPackage();

	public CmyPackage getRootPackage();

	public Clist getListPackageElement();

	public boolean equals( Object obj );

	public Element getElement();

	public Cliststereotype getStereotypes();

	public Cliststereotype getStereotypes( boolean b );

	public boolean isExtend();

	public void setExtend( boolean extend );

	public Element getMother();

	public void setMother( Element mother );

	public String getRelativeName( String s );

	public String getName();

	public String getQualifiedName();

	public String getQuotedName();

	public String getQuotedQualifiedName();

	public String getMotherName();

	public String getMotherQualifiedName();

	public boolean isStereotypedBy( String s );

	public boolean isStereotypesByQualified( String s );

	public boolean hasAttributeInStereotype( String stereoname , String name );

	public Object getAttributeStereotype( String stereoname , String name );

	public void setStereotypeAttributValue( String stereoname , String name , Object value );

	public String getType();

	public Image getIcone();

	public String getinfo( int x );

	public String toString();

	public CmyModel getModelof();

	public void setName( String name );

	public CmyConstraint createConstraintRule( String name );

	public CmyPackage createPackageImport( Package p );

	public Cumlelement createElementImport( Element e , String s );

	public Clist getPackageImport();

}