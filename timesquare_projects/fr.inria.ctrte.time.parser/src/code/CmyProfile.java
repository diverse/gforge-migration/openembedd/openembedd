/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

// import org.eclipse.emf.common.util.URI;
// import org.eclipse.emf.common.util.WrappedException;
// import org.eclipse.emf.ecore.resource.Resource;
// import org.eclipse.emf.ecore.resource.ResourceSet;
// import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
// import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.uml2.uml.Profile;

// import org.eclipse.uml2.uml.UMLPackage;
/**
 * @author Benoit Ferrero
 * 
 */
public class CmyProfile extends CmyPackage {

	private Profile profil = null;

	/**
	 * @param ne
	 */
	protected CmyProfile(Profile profil) {
		super(profil);
		this.profil = profil;
	}

	@Override
	public Profile getElement() {
		return profil;
	}

}
