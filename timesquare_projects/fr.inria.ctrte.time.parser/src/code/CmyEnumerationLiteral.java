/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

//import org.eclipse.uml2.uml.Element;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.EnumerationLiteral;

public class CmyEnumerationLiteral extends CmyInstance // Cumlelement
{

	private EnumerationLiteral literal;

	public CmyEnumerationLiteral(EnumerationLiteral literal) {
		super(literal);
		this.literal = literal;

	}

	public boolean IsStereotypeByUnit() {
		return isStereotypedBy(Cbase.marteUnitst);
	}

	@Override
	public EnumerationLiteral getElement() {
		return literal;
	}

	public String getInfo() {
		if (IsStereotypeByUnit()) {

			String s = " Unit " + getName() + "  ";
			String s2 = convertString(true);
			if (s2 != null)
				return s + s2;
			return s;
		}
		return getName();
	}

	public CmyEnumerationLiteral getBaseUnit() {
		if (IsStereotypeByUnit()) {
			Object o = getAttributeStereotype("Unit", "baseUnit");
			if (o == null)
				return null;
			EList<EObject> lst = ((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o)
					.eCrossReferences();
			if (lst == null || lst.size() == 0)
				return null;
			Object lo = lst.get(0);
			if (lo instanceof org.eclipse.emf.ecore.impl.DynamicEObjectImpl) {
				lo = ((org.eclipse.emf.ecore.impl.DynamicEObjectImpl) o)
						.eCrossReferences().get(1);
			}
			if (lo instanceof EnumerationLiteral) {
				return Cumlcreator.getInstance().createUmlElement(
						(EnumerationLiteral) lo);
			}
		}
		return null;
	}

	public String convertString() {
		return convertString(false);
	}

	public String convertString(boolean b) {
		if (IsStereotypeByUnit()) {
			Object o1, o2;
			String s = "";
			CmyEnumerationLiteral cme = getBaseUnit();

			o1 = getAttributeStereotype("Unit", "convFactor");
			o2 = getAttributeStereotype("Unit", "offsetFactor");
			if ((cme != null) && (o1 != null || o2 != null)) {
				try {
					if (b)
						s += " = ";
					if (o1 != null)
						s += o1.toString() + " * ";
					s += cme.getName();
					if (o2 != null)
						s += " + " + o2.toString();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return s;
		}
		return "";
	}

	@Override
	public String getinfo(int x) {
		if (IsStereotypeByUnit()) {
			if (literal.getEnumeration() != null) {
				String s1 = convertString(false);
				String s = "<<Unit>> " + literal.getEnumeration().getName()
						+ ":" + getName();
				if (s1.compareTo("") != 0) {
					s += "  < " + s1 + " > ";
				}
				return s;
			}
		}
		return super.getinfo(x);
	}

}
