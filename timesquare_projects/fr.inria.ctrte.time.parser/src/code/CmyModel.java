/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

//import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Model;

import fr.inria.ctrte.time.parser.MonLog;

/**
 * @author Benoit Ferrero
 * 
 */
public class CmyModel extends CmyPackage {
	private static MonLog logger = null;

	/**
	 * 
	 */

	private Model model = null;

	protected CmyModel(Model model2) {
		super(model2);
		this.model = model2;
		element = model2;
		if (logger == null)
			logger = MonLog.getLog(CmessageBox.fileNameTimeParser);
	}

	@Override
	public Model getElement() {
		return model;
	}
}
