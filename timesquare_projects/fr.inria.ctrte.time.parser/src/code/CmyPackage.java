/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

package code;

import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.UMLPackage;

public class CmyPackage extends Cumlelement {

	private Package packagesuml;

	public CmyPackage(Package packagesuml) {
		super(packagesuml);
		this.packagesuml = packagesuml;

	}

	public Clistenumeration getAllEnumeration() {
		Clist l = getListPackageElement(); // getownedofE();
		Clistenumeration l2 = new Clistenumeration(l);
		return l2;
	}

	public Clistenumeration getAllUnitEnumeration() {
		Clistenumeration l = getAllEnumeration();
		return l.GetUnitEnumeration();
	}

	public Clistenumerationliteral getAllEnumerationLiterral() {
		Clistenumeration l = getAllEnumeration();

		return l.GetEnumerationLiterral();
	}

	public Clistenumerationliteral getAllUnitEnumerationLiterral() {
		Clistenumeration l = getAllEnumeration();
		return l.GetUnitEnumerationLiterral();
	}

	public Clistenumeration getEnumeration() {
		Clist l = allownedofE(); // getownedofE();
		Clistenumeration l2 = new Clistenumeration(l);
		return l2;
	}

	public Clistenumeration getUnitEnumeration() {
		Clistenumeration l = getEnumeration();
		return l.GetUnitEnumeration();
	}

	public Clistenumerationliteral getUnitEnumerationLiterral() {
		Clistenumeration l = getEnumeration();
		return l.GetUnitEnumerationLiterral();
	}

	public Clistenumerationliteral getEnumerationLiterral() {
		Clistenumeration l = getEnumeration();
		return l.GetEnumerationLiterral();
	}

	public Clistprofile getProfiles(boolean b) {
		Clistprofile lp = new Clistprofile();
		if (packagesuml == null)
			return lp;
		if (b) {
			EList<Profile> le = packagesuml.getAllAppliedProfiles();

			for (Profile e : le) {

				lp.add(e, element);

			}
			return lp.getligth();
		}
		// else
		EList<ProfileApplication> le = packagesuml.getAllProfileApplications();
		for (ProfileApplication e : le) {

			lp.add(e.getAppliedProfile(), element);
		}
		return lp.getligth();
	}

	public Clist getElementlist() {
		Clist l = new Clist();
		EList<PackageableElement> le = packagesuml.getPackagedElements();
		for (PackageableElement e : le) {
			l.add(e, element);
		}
		return l;
	}

	public Clist getAllElementlist() {
		Clist l = new Clist();
		EList<Element> le = packagesuml.allOwnedElements();
		for (Element e : le) {

			l.add(e, element);

		}
		return l;
	}

	@Override
	public Package getElement() {
		return packagesuml;
	}

	public CmyInstance createInstance(String name) {
		InstanceSpecification is = (InstanceSpecification) packagesuml
				.createPackagedElement(name,
						UMLPackage.Literals.INSTANCE_SPECIFICATION);
		if (is != null)
			return Cumlcreator.getInstance().createUmlElement(is);
		return null;
	} 

	
	public CmyClass createClass(String name) {
		Class is = (Class) packagesuml
				.createPackagedElement(name,
						UMLPackage.Literals.CLASS);
		if (is != null)
			return Cumlcreator.getInstance().createUmlElement(is);
		return null;
	} 

	
	public CmyInstance createClockInstance(String s,CmyClass c ,CmyEnumerationLiteral cmel)
	{
		CmyInstance cmy = createInstance(s);
		cmy.applyClock();			
		cmy.setTypeClassifier(c );
		cmy.setUnit(cmel);
		return cmy;
	}
	
	public CmyClass createClockTypeClass(String name,CmyEnumeration cmel,Boolean isLogical, Boolean nature)
	{
		Class cuml = packagesuml.createOwnedClass(name, false);
		CmyClass cl =Cumlcreator.getInstance().createUmlElement(cuml);
		cl.applyClockType();
		if (cmel.isUnitEnumeration())
			cl.setUnitType(cmel.getElement());
		cl.setLogicalAttribut(isLogical);
		cl.setNatureAttribut(nature);		
		return cl;
	}
	
	
	@Override
	public Image getIcone() {
		return Cbase.IMG_PACKAGE;
	}

}
