/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element;

/**
 * @author Benoit Ferrero
 * 
 */
public class Coperator extends Celement {

	/*
	 * (non-Javadoc)
	 * 
	 * @see element.Celement#Apply()
	 */
	private EnumRel operateur;

	private String n;

	@Override
	public int apply(EnumType id) {

		return 0;
	}

	public Coperator(EnumRel operateur) {
		super();
		this.operateur = operateur;
	}

	public Coperator(String n) {
		super();
		this.n = n;
		sync();
	}

	public Coperator() {
		super();
		this.n = "undef";
		sync();
	}

	public int sync() {
		if (n.length() == 1) {
			if (n.compareTo("=") == 0) {
				operateur = EnumRel.eE;
				return 0;
			}
			if (n.compareTo("<") == 0) {
				operateur = EnumRel.eA;
				return 0;
			}
			if (n.compareTo(">") == 0) {
				operateur = EnumRel.eB;
				return 0;
			}
			return -1;
		}
		if (n.length() == 2) {
			if (n.compareTo("in") == 0) {
				operateur = EnumRel.eIN;
				return 0;
			}
			if (n.compareTo("<>") == 0) {
				operateur = EnumRel.eNE;
				return 0;
			}
			if (n.compareTo("<=") == 0) {
				operateur = EnumRel.eAE;
				return 0;
			}
			if (n.compareTo(">=") == 0) {
				operateur = EnumRel.eBE;
				return 0;
			}
			return -2;
		}
		operateur = EnumRel.eUK;
		return -3;
	}

	@Override
	public String getStr() {

		return "(" + n + ")";
	}

	@Override
	public String getStr2() {

		return "(" + n + " : " + operateur + ")";
	}

}
