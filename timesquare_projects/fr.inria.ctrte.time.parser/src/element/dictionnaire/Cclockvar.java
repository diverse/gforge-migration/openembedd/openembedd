/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import java.util.ArrayList;
import java.util.List;

class Point {

}

public class Cclockvar extends Cvariable2 {

	public Cclockvar(String name) {
		super(name);
		clockrelatif = new ArrayList<Cclockvar>();
	}

	private int markcode = -1;
	//private int rootlevel = 0;
//	private boolean islocal = false;
	private boolean rootclock = false;
//	private Boolean isinconstrutible = null;
////	private Boolean finite = null;
	//private Boolean dicretizable = null;

	public List<Cclockvar> clockrelatif = null;
	public List<Cclockvar> clockrelatifrequied = null;
	public List<Cclockvar> clockrelatifunrequied = null;
	public List<Cclockvar> filteredin = null;
	public List<Cclockvar> filteredout = null;
	public List<Cclockvar> equalclk = null;
	public List<Cclockvar> mysubclock = null; // filteredin
	public List<Cclockvar> mysuperclock = null; // filteredout
	public List<Cclockvar> inter = null; // non null
	public List<Cclockvar> nointer = null; // nulle
	// public List<Cclockvar> familly;

	public boolean empty = false;
	public boolean notempty = false;

	public boolean checkClock() {
		return true;
	}

	public void addInFilteringBy(Cclockvar c) {
	}

	public void addInFilteringSrc(Cclockvar c) {
	}

	public void addInInter(Cclockvar c) {
	}

	public void addInSuper(Cclockvar c) {
	}

	public void addInSub(Cclockvar c) {
	}

	public boolean isEmpty() {
		return empty;
	}

	public boolean isNotempty() {
		return notempty;
	}

	public List<Cclockvar> getInter() {
		return inter;
	}

	public List<Cclockvar> getNointer() {
		return nointer;
	}

	public boolean scanTop() {
		if (clockrelatif.size() == 1) {
			markcode = 0;
			return true;
		}
		markcode = -1;
		return false;
	}

	public boolean scan(int i) {
		int n = 0;
		if (markcode != -1)
			return true;
		for (Cclockvar c : clockrelatif) {
			if (c.getMarkcode() == -1)
				n++;
		}
		if (n <= 1) {
			markcode = i;
			return true;
		}
		return false;
	}

	public int getMarkcode() {
		return markcode;
	}

	public int solve() {
		return 0;
	}

	public boolean isRootclock() {
		return rootclock;
	}

	public void setRootclock(boolean rootclock) {
		this.rootclock = rootclock;
	}

}
