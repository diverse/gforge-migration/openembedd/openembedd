/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import element.Celement;

public class Cvariable extends Celement {

	public int assign = 0;

	public int local = 0;

	protected String name;

	boolean path;

	int refer = 0;

	public static final int N1 = 7;

	public static final int clkType = 1;

	public static final int instantType = 2;

	public static final int numberType = 3;

	public static final int booleanType = 4;

	public static final int binaryWordType = 5;

	public static final int unitType = 6;

	public static final String[] typeName = { "Unknown", "clkType",
			"instantType", "numberType", "booleanType", "binaryWordType",
			"unitType" };

	boolean type[] = null;

	// EnumType typevar;
	public Cvariable(String name) {
		super();
		type = new boolean[N1];
		clearType();
		this.name = name;
	}

	public void clearType() {
		int i;
		for (i = 0; i < N1; i++) {
			type[i] = false;
		}
		return;
	}

	public int setType(int d) {
		if (d < 0)
			return 0;
		if (d >= N1)
			return 0;

		type[d] = true;
		return 1;
	}

	public int addAssign() {
		return ++assign;
	}

	public int addLocal() {
		return ++local;
	}

	public int addref() {
		return ++refer;
	}

	@Override
	public int apply(EnumType id) {

		return 0;
	}

	public int getTypeInt() {
		boolean flag = false;
		int i;
		int p = 0;
		for (i = 1; i < N1; i++) {
			if (type[i]) {
				if (flag)
					return -1;
				flag = true;
				p = i;
			}
		}
		if (flag)
			return p;
		return 0;
	}

	public boolean checkIntegrity() {

		boolean flag = false;
		int i;
		for (i = 1; i < N1; i++) {
			if (type[i]) {
				if (flag)
					return false;
				flag = true;
			}
		}
		if (flag)
			return true;
		return true;
	}

	public String getTypeStr() {
		int n;
		n = getTypeInt();
		if (n == -1)
			return "type error";
		return typeName[n];
	}

	public int getAssign() {
		return assign;
	}

	public int getLocal() {
		return local;
	}

	public String getName() {
		return name;
	}

	public int getRefer() {
		return refer;
	}

	@Override
	public String getStr() {
		return name + ":\'" + getTypeStr() + "\'  " + " \tD:" + local + "  A :"
				+ assign + " R:" + refer + " .";
	}

	@Override
	public boolean isNumber() {
		return false;
	}

	public boolean isPath() {
		return path;
	}

	public boolean isType(int d) {
		if (d < 0)
			return false;
		if (d >= N1)
			return false;
		return (type[d]);
	}

	public boolean isType2(int d) {
		if (d < 0)
			return false;
		if (d >= N1)
			return false;
		return (getTypeInt() == d);
	}

	public void setPath(boolean path) {
		this.path = path;
	}

}
