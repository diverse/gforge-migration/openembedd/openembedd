/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import java.util.HashMap;

import element.Celement;
import fr.inria.base.MyManager;

/**
 * @author Benoit Ferrero
 * 
 */
public class Cdictionnaire extends Celement {

	@Override
	public int apply(EnumType id) {

		return 0;
	}

	@Override
	public String getStr() {

		return "Dictionnary ";
	}

	/**
	 * 
	 */
	public Cdictionnaire() {

		vars = new HashMap<String, Cvariable>();
	}

	public HashMap<String, Cvariable> vars = null;

	public Cvariable putVar(String n) {
		Cvariable c = null;
		c = vars.get(n);
		if (c == null) {
			c = new Cvariable(n);
			new Cvariable2(n);
			vars.put(n, c);
		}
		return c;
	}

	public Cvariable getVar(String n) {
		Cvariable c = null;
		c = vars.get(n);
		if (c == null) {
			return null;
		}
		return c;
	}

	public boolean isExiste(String n) {
		return vars.containsKey(n);
	}

	public int count() {
		return vars.size();
	}

	public int disp() {
		for (Cvariable value : vars.values()) {
			MyManager.println(value.getStr());

		}
		return 0;
	}

	public int disp2() {
		for (Cvariable value : vars.values()) {
			MyManager.println(value.getStr2());

		}
		return 0;
	}

	public HashMap<String, Cvariable> get() {
		return vars;
	}

	public Cdictionnaire getDicoType(int d) {
		if (d < 0)
			return null;
		if (d >= Cvariable.N1)
			return null;
		Cdictionnaire dic = new Cdictionnaire();
		for (String key : vars.keySet()) {
			Cvariable c = vars.get(key);
			if (c.isType(d)) {

				dic.vars.put(key, c);
			}

		}
		return dic;
	}

}
