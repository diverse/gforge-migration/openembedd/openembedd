/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import element.Celement;

public class Cvariable2 extends Celement {

	int posiligne, posiclo;

	// List<Csuiv> liste = null;

	public int local = 0;

	protected String name;

	protected String oldname = null;
	int path = 0;

	int refer = 0;

	// EnumType typevar;
	public Cvariable2(String name) {
		super();
		this.name = name;
		oldname = name;
		checkPath();
	}

	EnumType typevar = EnumType.eUk;

	public int setType(EnumType d) {
		typevar = d;
		return 0;
	}

	public int DefLocal() {
		return ++local;
	}

	public int Addref() {
		return ++refer;
	}

	@Override
	public int apply(EnumType id) {
		return 0;
	}

	public String getTypeStr() {
		return typevar.toString();
	}

	public int getLocal() {
		return local;
	}

	public String getName() {
		return name;
	}

	public int getRefer() {
		return refer;
	}

	@Override
	public String getStr() {
		return name + ":\'" + getTypeStr() + "\'  " + " \tD:" + local + " R:"
				+ refer + " .";

	}

	@Override
	public boolean isNumber() {
		return false;
	}

	public boolean isPath() {
		return path != 0;
	}

	public boolean isType(EnumType d) {

		return (typevar != d);
	}

	public int checkPath() {
		int n = name.length();
		int cpt = 0;
		int i = 0;
		for (i = 0; i < n; i++) {
			char c = name.charAt(i);
			if (c == '.')
				cpt++;
		}
		path = cpt;		
		return cpt;
	}

	public String getOldname() {
		return oldname;
	}

	protected void setName(String name) {
		this.name = oldname;
	}

}
