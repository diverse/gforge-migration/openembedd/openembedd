/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import java.util.HashMap;

import element.Celement;
import fr.inria.base.MyManager;

/**
 * @author Benoit Ferrero
 * 
 */
public class Csubdictionnaire extends Celement {

	String typename = null;

	EnumType t = EnumType.eUk;

	public HashMap<String, Cvariable2> vars = null;

	/**
	 * 
	 */
	public Csubdictionnaire(String typename, EnumType t) {
		this.typename = typename;
		this.t = t;
		vars = new HashMap<String, Cvariable2>();
	}

	@Override
	public int apply(EnumType id) {
		return 0;
	}

	public int count() {
		return vars.size();
	}

	public int disp() {
		for (Cvariable2 value : vars.values()) {
			MyManager.println(value.getStr());

		}
		return 0;
	}

	public int disp2() {
		for (Cvariable2 value : vars.values()) {
			MyManager.println(value.getStr2());
		}
		return 0;
	}

	public HashMap<String, Cvariable2> get() {
		return vars;
	}

	@Override
	public String getStr() {
		return "Dictionniary";
	}

	public Cvariable2 getVar(String n) {
		Cvariable2 c = null;
		c = vars.get(n);
		if (c == null) {
			return null;
		}
		return c;
	}

	public boolean IsExiste(String n) {
		return vars.containsKey(n);
	}

	public Cvariable2 putVar(String n) {
		Cvariable2 c = null;
		c = vars.get(n);
		if (c == null) {
			c = new Cvariable2(n);
			c.setType(t);
			vars.put(n, c);
		}
		return c;
	}

	public int size() {
		return vars.size();

	}

	public int rename(String old, String newb) {
		Cvariable2 c = null;
		c = vars.get(old);
		vars.remove(old);
		vars.put(newb, c);
		c.setName(newb);
		return 0;
	}

	public HashMap<String, Boolean> getnamebol(HashMap<String, Boolean> hm) {

		for (Cvariable2 cv : vars.values()) {

			String s = cv.getName();
			if (!hm.containsKey(s))
				hm.put(s, null);

		}
		return hm;
	}
}
