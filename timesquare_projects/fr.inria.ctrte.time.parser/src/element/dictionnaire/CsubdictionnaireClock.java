/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

import java.util.HashMap;

public class CsubdictionnaireClock extends Csubdictionnaire {

	public CsubdictionnaireClock(String typename, EnumType t) {
		super(typename, t);

	}

	@Override
	// public Cvariable2 putVar(String n)
	// use a Covariance
	public Cclockvar putVar(String n) {
		Cclockvar c = null;
		c = (Cclockvar) vars.get(n);
		if (c == null) {
			c = new Cclockvar(n);
			c.setType(t);
			vars.put(n, c);
		}
		return c;
	}

	public boolean checked() {
		int n = 0;

		boolean b = false;
		for (Cvariable2 cv : vars.values()) {
			if (((Cclockvar) cv).scanTop())
				b = true;
		}
		if (b == false)
			return false;
		// boolean b2=true;
		while (b) {
			b = false;
			// b2=false;
			for (Cvariable2 cv : vars.values()) {
				Cclockvar c = (Cclockvar) cv;
				if (c.getMarkcode() == -1) {
					// b2=true;
					if (c.scan(n))
						b = true;
				}

			}
			if (b == false)
				break;
			n++;
		}
		if (b == false)
			return false;
		return true;
	}

	/*
	 * public Object makeTreeClock() { // List<Cclockvar> rootClock=new
	 * ArrayList<Cclockvar>();
	 * 
	 * 
	 * return null; }
	 */

	public HashMap<String, Boolean> getlocalClock(HashMap<String, Boolean> hm) {

		for (Cvariable2 cv : vars.values()) {
			if ((cv).getLocal() > 0) {
				String s = cv.getName();
				if (hm.containsKey(s))
					hm.put(s, true);
				else
					hm.put(s, false);
			}
		}
		return hm;
	}

}
