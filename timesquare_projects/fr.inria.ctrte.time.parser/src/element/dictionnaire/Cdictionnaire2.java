/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element.dictionnaire;

// import java.util.HashMap;

import element.Celement;

/**
 * @author Benoit Ferrero
 * 
 */
public class Cdictionnaire2 extends Celement {

	Csubdictionnaire booldic = new Csubdictionnaire("boolean", EnumType.eBool);

	CsubdictionnaireClock clkdic = new CsubdictionnaireClock("clock",
			EnumType.eClk);

	Csubdictionnaire eventdic = new Csubdictionnaire("event", EnumType.eEvent);

	Csubdictionnaire instantdic = new Csubdictionnaire("instantduration", EnumType.eInstant);

	Csubdictionnaire numberdic = new Csubdictionnaire("number",	EnumType.eNumber);

	Csubdictionnaire unitdic = new Csubdictionnaire("unit", EnumType.eUnit);

	/**
	 * 
	 */
	public Cdictionnaire2() {
		super();
		// vars = new HashMap<String,Cvariable2>();
	}

	@Override
	public int apply(EnumType id) {
		return 0;
	}

	// public boolean IsExiste(String n) { return vars.containsKey(n);}
	public int count() {
		return clkdic.count() + booldic.count() + instantdic.count()
				+ numberdic.count() + unitdic.count() + eventdic.count();
	}

	public int disp() {

		clkdic.disp();
		booldic.disp();
		instantdic.disp();
		numberdic.disp();
		unitdic.disp();
		eventdic.disp();
		return 0;
	}

	public int disp2() {
		clkdic.disp2();
		booldic.disp2();
		instantdic.disp2();
		numberdic.disp2();
		unitdic.disp2();
		eventdic.disp2();
		return 0;
	}

	public Csubdictionnaire getBooldic() {
		return booldic;
	}

	public CsubdictionnaireClock getClkdic() {
		return clkdic;
	}

	public Csubdictionnaire getEventdic() {
		return eventdic;
	}

	public Csubdictionnaire getInstantdic() {
		return instantdic;
	}

	public Csubdictionnaire getNumberdic() {
		return numberdic;
	}

	public Csubdictionnaire getUnitdic() {
		return unitdic;
	}

	@Override
	public String getStr() {
		return "Dictionniary";
	}

	public Cvariable2 getVar(String n) {
		Cvariable2 c = null;
		c = clkdic.getVar(n);
		if (c != null)
			return c;
		c = booldic.getVar(n);
		if (c != null)
			return c;
		c = instantdic.getVar(n);
		if (c != null)
			return c;
		c = numberdic.getVar(n);
		if (c != null)
			return c;
		c = unitdic.getVar(n);
		if (c != null)
			return c;
		c = eventdic.getVar(n);
		if (c != null)
			return c;
		return c;
	}

	public Cvariable2 getVar(String n, EnumType d) {
		switch (d) {
		case eNumber:
			return numberdic.getVar(n);
		case eBool:
			return booldic.getVar(n);
		case eClk:
			return clkdic.getVar(n);
		case eInstant:
			return instantdic.getVar(n);
		case eBinaryword:
			return null;
		case eEvent:
			return eventdic.getVar(n);
		case eUnit:
			return unitdic.getVar(n);
		case eUk:
			return null;
		case eString:
			return null;
			// default : aucun cas
		default:
			return null;
		}
	}

}
