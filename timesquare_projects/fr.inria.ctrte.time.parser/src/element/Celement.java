/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element;

import element.dictionnaire.Cdictionnaire;

// import bword.*;
/**
 * @author Benoit Ferrero
 * 
 */

public abstract class Celement {

	public enum EnumRel {
		eA, eAE, eB, eBE, eE, eIN, eNE, eUK;
		EnumRel() {
			
		}
	}

	public enum EnumType {
		eBinaryword, eBool, eClk, eEvent, eInstant, eNumber, eString, eUk, eUnit;
		EnumType() {
			
		}
	}

	// indique si 2 instance avec les meme donne sont idantique
	protected boolean identique = false;

	//
	boolean clock = false;

	protected Cdictionnaire dico = null;

	EnumType en;

	int idnbr = -1;

	boolean instant = false;

	boolean intdouble = false;

	protected boolean interval = false;

	boolean logical = false;

	public Celement() {

		setEn(EnumType.eUk);

	}

	public int apply(EnumType id) {
		return 0;
	}

	boolean check() {
		return true;
	}

	public boolean checkEn() {
		return true;
	}

	public boolean compareEn(EnumType en) {
		return (this.en == en);
	}

	public int fctxxxx(Cdictionnaire dic, EnumType i) {
		setDico(dic);
		apply(i);
		refer();
		return 0;
	}

	public EnumType getEn() {
		return en;
	}

	/**
	 * @return the idnbr
	 */
	public int getIdnbr() {
		return idnbr;
	}

	public String getStr() {
		return "null";
	}

	public String getStr2() {
		return getStr();
	}

	public boolean isClock() {
		return clock;
	}

	public boolean isInstant() {
		return instant;
	}

	protected boolean isIntdouble() {
		return intdouble;
	}

	public boolean isKnow() {
		return ((this.en != EnumType.eUk));
	}

	protected boolean isLogical() {
		return logical;
	}

	public boolean isNumber() {
		return ((this.en == EnumType.eNumber));
	}

	public void refer() {
		
	}

	public void setClock(boolean clock) {
		this.clock = clock;
	}

	public void setDico(Cdictionnaire dico) {
		this.dico = dico;
	}

	protected void setEn(EnumType en) {
		this.en = en;
	}

	/**
	 * @param idnbr
	 *            the idnbr to set
	 */
	public void setIdnbr(int idnbr) {
		this.idnbr = idnbr;
	}

	public void setInstant(boolean instant) {
		this.instant = instant;
	}

	protected void setIntdouble(boolean intdouble) {
		this.intdouble = intdouble;
	}

	protected void setLogical(boolean logical) {
		this.logical = logical;
	}

	@Override
	public String toString() {
		return super.toString() + " -> " + getStr();
	}

	public Object getClock() {
		return null;
	}

	public boolean isIdentique() {
		return identique;
	}

}
