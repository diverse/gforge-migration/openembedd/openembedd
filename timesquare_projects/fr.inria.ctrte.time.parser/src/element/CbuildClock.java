/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package element;


public class CbuildClock extends Celement {

	//private int size;
	//private double tn[];
	private String name;
	/*private boolean isrootclock = true;
	private int levelresolve = 0;
	private int espacelevel = 0;*/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/*private List<CbuildClock> supercloc = null;
	private List<Object> superpatter = null;
	private List<CbuildClock> subclock = null;
	private List<Object> subpatter = null;*/

	public void AddSubClock(CbuildClock c, Object o) {
	}

	protected void AddSuperClock(CbuildClock c, Object o) {
	}

	public int size(int n) {
		return n;
	}

	public int sync() {
		return 0;
	}

	public int discretizeby(double d) {
		return 0;
	}

	public void markup() {
	}

	public void markdown() {
		
	}

	public Object get(int n) {
		return null;
	}

	public Object set(int n, Object o) {
		return null;
	}

	public CbuildClock MakeSubClock(Object o) {
		return null;
	}

	public void runtime() {
	}
	// uses boolean equation ??

}
