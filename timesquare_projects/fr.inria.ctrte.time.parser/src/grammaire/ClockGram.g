/*
	ClockLang.g
	v1: October 31, 2006
	v2 : 26 April 2007
	v3 : 04 May 2007
	v4 : 22 May 2007
	v5 : 11 June 2007
	v5.1 : 12 June 2007
	v5.2 : 14 June 2007
	v5.3 :  15 June 2007
	v5.4: 18 June 2007
*/


header {
/******************************  
* @author Benoit Ferrero
* ClockLang.g 
*
*
*********************************/
package grammaire ;
}

{
import element.dictionnaire.Cdictionnaire2;
import element.dictionnaire.Cvariable2;
@SuppressWarnings("unused")
 }
class LangParser extends Parser;





options {
	buildAST=true;
	interactive= true;
	k =4;  // ici kv
 	//ASTLabelType = "MyAST";    
	
	//
	//defaultErrorHandler=false;
}

tokens {
	BLOCK; // imaginary token
	REL;
	CLK_FUNC;
	CLK_FUNC2;
	CLK_REL;
	CLKG;
	CHRONONFP;
	CHRONONFPSTB;
	EXPR;
	BWEXPR;
	CLKEXPR;
	//CLKEXPR2;
	//SEMI=";";
	IF="if";
	COND;
	INT;
	REAL;
	LOG;
	LOGAND	;
	LOGOR;
	LOGXOR   ;
	LOGNOT ;
	LOGPAR ;
	LOGCOMP;
	ADDOP;
	INTERVALREAL ;
	MYLCURLY="{";
	MYRCURLY="}";
	LPAREN="(";
	RPAREN=")";
	LINTER="[";
	RINTER="]";
	ID;
	PATH;
	CLKDEF;
	ASSIGN ="=";
	CVS;
	FCVS;
	REF;
	DIESE="#";
	INST;
	INST_REL;
	RELINST;
	EGOP;
	STAT;
	GUARD;
	WRT;
	ATE;
	DUREXPR;
	INSTEXPR;
	INSTDEF;
	INSTREF1;
	INSTREF1b;
	INSTREF2;
	TIMEMUL;
	TIMEFUNC;
	TIMESPAN;
	TIMEINTER;
	EXPRADD;
	EXPRMUL;
	EXPRNUMBER;
	INTERPP;
	TIMESOMME;
	TIMEEXT;
	TUPLE;
	TUPLEEXT;
	STRICTLY="strictly" ;
	LITERAL_isDisjoint= "isDisjoint";
}

{
	Cdictionnaire2 dico2=new Cdictionnaire2();
	private	Cvariable2 tampon=null;
	int cpt=0;
	public Cdictionnaire2 getDico() { return dico2;}
	public int errorNumber=0;
	public String errorstr="";
	@Override
 	public void reportError(RecognitionException ex)
	{
		//TODO update error message
		errorNumber++;
		System.out.println("---> Error " +errorNumber+ "  "+ 	ex.toString());
		System.out.println("---> Error " + 	ex.getClass().getName() +" " +inputState.guessing);
		//if (ex instanceof antlr.NoViableAltException)
	//	System.out.println(((antlr.NoViableAltException) ex).token. getText()	);
		errorstr += "> Err" + errorNumber+"< "+ex.line +":" +ex.column +" >"+ex.getMessage() +" \n";
		//ex.printStackTrace(System.err);
		 super.reportError( ex)	;
	}


  
}

ccsl:  block ;



/*********************************************************************
*
*	Block
*
**********************************************************************/

block
	:	MYLCURLY!  (cStatement)*	 MYRCURLY!
		{
			//System.out.println("Block......................");
			#block = #([BLOCK, "BLOCK"], #block);
			}
	;
	/*exception
catch [ RecognitionException re ] {
                   	System.out.println("Error : block ");

                    reportError( re );
     }*/



/*********************************************************************
*
*	Statement
*
**********************************************************************/


/*	*  Statement + guard  */
cStatement :
statement  (guard  )
{#cStatement = #([STAT,"STAT"],#cStatement);cpt++;}
;/* exception
catch [ RecognitionException re ] {
                   	System.out.println("Error : statement ");

                    reportError( re );
     }*/


/*	*   guard */
guard : ("if"! (b:boolexpr1  )  ";"!
	{#guard = #([GUARD,"GUARD"],#guard);})
	 |
	 ( ";"!

	 )

	;

// /* System.out.println(s.getLine() );*/

/*	*  Statement  */
statement
	:
	 // !	b:block { statement_AST = #b; } 	|
	  clockdef
	//| 	clockId "="^   clockExpr  //expr //path //TODO


	|  (metaref "="^ ) =>
		metaref "="^ metaref  //clockExpr
	|  (clockRef "," clockRef "haveMaxDrift" ) =>
				 pcr:clk_rel_expr	{#statement = #([REL,"RELATION2"],#pcr);}
	|  (clockRef "," clockRef ("haveOffset"|   "haveSkew" | "haveDrift") ) =>
				chronoNFP		{#statement = #([CHRONONFP,"CHRONONFPEXP2"],#statement);}
	|  (clockRef "hasStability" )=>
				chronoNFP		{#statement = #([CHRONONFP,"CHRONONFPEXP"],#statement);}

	|	(instantref ("coincidentWith" | "precedes"| "strictly")  ) =>
	   cr3:instantrel 	{#statement = #([RELINST,"RELATION_INSTANT"],#cr3);} 	//	relinstant
	|	cr:clk_rel_expr	{#statement = #([REL,"RELATION"],#cr);}	//rel
	|   instantdef

;



/****/

metaref :
	(clockRef "by") => clockgroup
	| clockExpr
	;


/*********************************************************************
*
*	base commune
*
**********************************************************************/

pathe	:ID |PATH ;

unitId	: v:pathe {dico2.getUnitdic().putVar(#v.getText()) ; };
clockId	: v:pathe {dico2.getClkdic().putVar(#v.getText());    };
clockId2: v:pathe { tampon=dico2.getClkdic().putVar(#v.getText()); tampon.DefLocal();   };

timerId	: v:pathe {dico2.getClkdic().putVar(#v.getText());    };
timerId2: v:pathe { tampon=dico2.getClkdic().putVar(#v.getText()); tampon.DefLocal();   };

instantId : v:pathe {  dico2.getInstantdic().putVar(#v.getText());   };
instantId2: v:pathe { tampon=dico2.getInstantdic().putVar(#v.getText()); tampon.DefLocal();    };
numberId: v:pathe {  dico2.getNumberdic().putVar(#v.getText()); };
booleanId:  v:pathe { dico2.getBooldic().putVar(#v.getText());   };
eventid : v:pathe {dico2.getEventdic().putVar(#v.getText());  }   ;

interv : "[" | "]";//LINTER | RINTER ; topaze
interpp: "..";
prefixop :  "max" | "min" ;

/*********************************************************************
*
* Binary word
*
**********************************************************************/

bwexpr
	:   bwe:BW	{#bwexpr = #([BWEXPR,"BWEXPR_BW"],#bwexpr);}
	|	gbwe:GBW	{#bwexpr = #([BWEXPR,"BWEXPR_GBW"],#bwexpr);}
	;


/*********************************************************************
*
* Nombre reel COMMENT
*
**********************************************************************/

realexpr : interreal | number1 ;

realint : REAL | INT   ;

interreal :  (interv ) realint  ".."!  realint (interv  )
		{#interreal= #([INTERVALREAL,"INTERVALREAL"]) ; };


/*********************************************************************
*
* proba
*
**********************************************************************/

proba : "uniform"! "("! id1: INT ","! INT ")"! 
	{#proba= #([LITERAL_uniform,"uniform"], #id1) ; };

/*********************************************************************
*
*	Expression numerique
*
**********************************************************************/

number1 :  k:number2
		(
		(("+" | "-") number2 ) +
		{#number1= #([EXPRADD,"EXPRADD"],#k) ; }
		)?
		;

number2 :
	 k:number3
	(
	(("*" | "/" | "%") number3)+
	{#number2= #([EXPRMUL,"EXPRMUL"],#k) ; }
	)?
	;


signe: "+" | "-" ;

number3 : (k1:signe  realint ) {#number3= #([EXPRNUMBER,"number"],#k1) ; }
		| realint
		| ( "("! k:number1 ")"! ) {#number3= #([LOGPAR,"( )"],#k) ; }
		| numberId ;//Path;

/*********************************************************************
*
*	Expression logique
*
**********************************************************************/



boolexpr : l:boolexpr1
		{#boolexpr= #([LOG,"logique"],#l) ; };

boolexpr1 : l:boolexpr2
	( ("or"! boolexpr2) +
	{#boolexpr1= #([LOGOR,"OR"],#l) ; }
	)?
	;

boolexpr2 :l:boolexpr3
	(  ("xor"! boolexpr3 )+
	{#boolexpr2= #([LOGXOR,"XOR"],#l) ; }
	)?
	;

boolexpr3 :l:boolexpr4
	(
	("and"! boolexpr4 )+
	{#boolexpr3= #([LOGAND,"AND"],#l) ; }
	)?
	;


boolexpr4: ("not" l: boolexpr4)
	{#boolexpr4= #([LOGNOT,"not"],#l) ; }
				| boolexpr5 ;

boolexpr5:
		(number1  relOp ) => (c:compa )
		{#boolexpr5= #([LOGCOMP,"COMPA"],#c) ; }
		|
		( "("! l: boolexpr1   ")"! )
			{#boolexpr5= #([LOGPAR,"( )"],#l) ; }
		|	booleanId
		 | "true" | "false" //Path

		 ;

/*******************************************************************
*
*	Comparaison
*
*
*********************************************************************/

compa :  (number1)  relOp (number1 )  ;

relOp :
	 r:RELOP {#relOp = #([EGOP,"EGOP"],#r);  }
	  | a:ASSIGN	{#relOp = #([EGOP,"EGOP"],#a);  }
	  | i:"in" {#relOp = #([EGOP,"EGOP"],#i);  }
	  ;


/*********************************************************************
*
*   Clocked/Timed Value Specification
*
*
*********************************************************************/



extend :unitId "on"! clockId ;

durationexpr : d:diexpr//
	{#durationexpr= #([DUREXPR,"DUREXPR"],#d) ; };

instantexpr :  d:diexpr //timevaluespecifcation  (unitId "on"! clockId )?//
	{#instantexpr= #([INSTEXPR,"INSTEXPR"],#d) ; };


diexpr:

 ( MYLCURLY! 	t:time MYRCURLY! extend
 	{#diexpr= #([TIMEEXT,"TIMEEXT"],#t) ;
 		//System.out.println("ici2" +#t) ;
 		}
 		)
 	| (t2:time {
 		//System.out.println("ici" +#t2);
 		#diexpr= #t2;
 		 } )
 		 ;  //timevaluespecifcation // { }


/*********************************************************
*
*   DURATION /INSTANT
*
*
*********************************************************/


// IorD TIMESOMME
time : n:timeterm
	(
	( ("+" | "-") timeterm )+
		{ #time= #([TIMESOMME,"TIMESOMME"],#n) ; }
		)?
	 ;

// mult durataion
timeterm: (f:timefactor { #timeterm=#f; } )
		|  ( "mult"!  "("! n:number1 ","! timefactor ")"!						
							{#timeterm= #([TIMEMUL,"TIMEMUL"],#n) ; }
						 );

timefactor :  	timefunc
			   |
			  	( interv )=>
			 		 timeInterval
			   |	 timespan
			   |  ( tuple )=>tuple
			   | ("(" number2  ")" unitId)
			   	 	=> ("("! n1:number2 ")"! extend
			   	 	{#timefactor= #([TIMEEXT,"TIMEEXT"],#n1) ; } )
			   | ("(" number2  )
			   	 	=> ("("! number2 ")"! )
			   | 	  ( "("  ) =>
		 			("(" time ")")
		 	   |  (pathe ) => instantId
			   | ( realint unitId ) =>
				(	n:realint  extend
					{#timefactor= #([TIMEEXT,"TIMEEXT"],#n) ; } )
			  | ( pathe unitId ) =>
				(	n2:pathe  extend
					{#timefactor= #([TIMEEXT,"TIMEEXT"],#n2) ; } )
			  | (realint)=> realint

		 	//  |  number2 /* number2 */
		 	  ;

tuple: "("! n:tuplexpr ( ","! tuplexpr  ) *  ")"!
		{#tuple=#([TUPLE ,"TUPLE" ],#n);};

tuplexpr : (n:pathe "="! (
	 (boolexpr)=>
			boolexpr
 	|  	number1 )
 	)
 	 {#tuplexpr=#([TUPLEEXT ,"TUPLEEXT" ],#n);};

timefunc:   p:prefixop "("! time (","! time )* ")"!    // IorD
		{	#timefunc= #([TIMEFUNC,"TIMEFUNC"],#p) ; } ;

timeInterval: (i:interv  time  interpp!  time interv)
	{	#timeInterval= #([TIMEINTER,"TIMEINTER"],#i) ; } ;


timespan : "durationBetween"! "("! t:time ","!  time ")"!
	{	#timespan= #([TIMESPAN,"TIMESPAN"],#t) ; } ;




/*********************************************************
*
*  Clock expresion
*
*
*********************************************************/


clockExpr :
	(id1:clockRef (
			  	    "restrictedTo"! boolexpr
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_restrictedTo, "restrictedTo"],#id1);}
					|
   		 			"filteredBy"! bwexpr  //TODO  ADD weak
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_filteredBy, "filteredBy"],#id1);}
					|
					"discretizedBy"! number1  (("from"! REAL) ( "to"! REAL)?)?  // REALEXPR  //TODo
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_discretizedBy, "discretizedBy"],#id1);}
					|
					"restrictedToTrailing"!   number1  // INTEXPR TODO enlever  delayedBy , laisser  delayedFor
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_restrictedToTrailing, "restrictedToTrailing"],#id1);}
					|
					"delayedFor"! ( INT | proba  ) ( "on"! clockRef ) ?
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_delayedFor, "delayedFor"],#id1);}	
					|
					"followedBy"! clockRef //ID
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_followedBy, "followedBy"],#id1);}
					|
					"inter"! clockRef //ID
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_inter, "inter"],#id1);}
					|
					"union"! clockRef //ID
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_union, "union"],#id1);}
					|
					"minus"! clockRef //ID
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_minus, "minus"],#id1);}
			  		|
			  		// "sampleTo"! |  "sampledTo"! |
			moderator (	"sampledOn"! ) clockRef //ID //TODO FIX   and ADD striclty  :sampledTo : good
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_sampledOn, "sampledOn"],#id1);}
				 	|
				 	"restrictedToLeading"! INT  // INTEXPR
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_restrictedToLeading, "restrictedToLeading"],#id1);}
					|
					"restrictedToInterval"! "["! INT ".."! INT "]"! // INTEXPR
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_restrictedToInterval, "restrictedToInterval"],#id1);}


					|
				 	 //(""^)? //TODO fixedbug02
			{#clockExpr =#id1 /* #([CLK_FUNC2,"CLK_FUNC2"],#id1)*/ ;}	 // TODO 1002a
					 ))
	|
		"when"! id4:eventid //ID TODO)
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_when, "when"],#id4);}
	|
		"timerConstraint"!  id5:clockRef ","! clockRef ","! clockRef ","! clockRef ","!  ( INT | proba ) 
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_timerConstraint, "timerConstraint"],#id5);}
			//TODO FIX BUG
	|
		"oneShotConstraint"!  id6:clockRef ","! clockRef ","! clockRef ","!   ( INT | proba ) 
			{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_oneShotConstraint, "oneShotConstraint"],#id6);}
	| "sup" "("! id7:clockRef  (","! clockRef )+ ")"!
	{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_sup, "sup"],#id7);}
	| "inf" "("! id8:clockRef  (","! clockRef )+ ")"!
	{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_inf, "inf"],#id8);}
	| "sustain" id9:clockRef  "upto"! clockRef
	{#clockExpr = #([CLK_FUNC,"CLK_FUNC"],[LITERAL_sustain, "sustain"],#id9);} 
	;





/*********************************************************
*
*
*	TimerExpr
*
*
***********************/

timerExpr :  clockRef "countFor"! number1 "on"! clockRef ("until"! clockRef )?;


/*********************************************************
*
*   Clock Ref
*
*********************************************************/


clockRef
	:	id:clockId 		{#clockRef = #([CLKEXPR,"ID"],#id);} // ID //CLK_EXPR_ID
	|	( LPAREN! cef:clockExpr RPAREN! )
					{#clockRef = #([CLKEXPR,"CLK_EXPR_FUNC"],#cef);}
	;

 /*********************************************************
*
*   Chrono NFP
*
*********************************************************/

chronoNFP //ID ID
	 :  id:clockRef (
	    ( "hasStability"!  realint (wrt) ?  ) //ID
	 	{#chronoNFP= #([CHRONONFP,"CHRONONFP_STABILITY"],[LITERAL_hasStability, "hasStability"], #id	);}
	 	| (
	 		","! clockRef ((  "haveOffset"!    durationexpr  (wrt)   ?  (ate) ? )
	 		{#chronoNFP= #([CHRONONFP,"CHRONONFP_OFFSET"],[LITERAL_haveOffset, "haveOffset"], #id	);}
	 		|
	 		(  "haveSkew"!      number1   ( wrt)?  (ate) ? )
	 		{#chronoNFP= #([CHRONONFP,"CHRONONFP_SKEW"],[LITERAL_haveSkew, "haveSkew"], #id	);}
	 		|
	 	    ( "haveDrift"!  number1  (wrt) ?  (ate) ?  )
	 		{#chronoNFP= #([CHRONONFP,"CHRONONFP_DRIFT"], [LITERAL_haveDrift, "haveDrift"],#id	);}

	 	  )
	 	 ))
;

wrt : "wrt"! id:clockId //  Path
	{#wrt= #([WRT,"WRT"], #id	);}	 	;

ate:	"at"! id:instantexpr
	{#ate= #([ATE,"AT"], #id	);}	 	;
 



/*********************************************************
*
*   CLOCK RELATION
*
*********************************************************/

moderator: ("strictly" | 
	"leftWeakly" | "rightWeakly" | "weakly"  | 
	"nonStrictly"  | "leftNonStrictly" |  "rightNonStrictly" ) ? ; 

clk_rel_expr
	:
	 (clockgroup moderator ("alternatesWith" | "synchronizesWith" | "precedes" ))=>
	(
		id2:clockgroup moderator
		(
		(
			 "alternatesWith"! clockgroup
				{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_alternatesWith, "alternatesWith"],#id2);}
		)
		|
		(
			 "synchronizesWith"! clockgroup
				{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_synchronizesWith, "synchronizesWith"],#id2);}
		)
		|
		(	"precedes" ! clockgroup
		{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_precedes , "precedes"],#id2);}
			//TODO WARNING
		)

		)
	)
	|
	(

	id1:clockRef //ID
	( 	"isSubClockOf"! clockRef
		{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isSubClockOf, "isSubClockOf"],#id1);}
		|
		"isFinerThan"! clockRef
		{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isFinerThan, "isFinerThan"],#id1);}
		|
		 "isFasterThan"! clockRef ( "withIn" ! "["! INT  ".."! (INT | "*" ) "]"!    )?   
		{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isFasterThan, "isFasterThan"],#id1);}
		|
		"isCoarserThan"! clockRef
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isCoarserThan, "isCoarserThan"],#id1);}
		|
		"isSlowerThan"! clockRef  ( "withIn" ! "["! INT  ".."! INT "]"!    )?  //TODO INF
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isSlowerThan, "isSlowerThan"],#id1);}
		|
	 	"isPeriodicOn"! clockRef ("period"! ( number1| proba ) ("offset"! ( number1| proba ) )? )?  //realint  
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isPeriodicOn, "isPeriodicOn"],#id1);}
/*		|
		"isUnionOf"! clockRef "," clockRef
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isUnionOf, "isUnionOf"],#id1);}
		*/
		|	 "isSporadicOn"! clockRef  ("gap"! number1 )?   //realint
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isSporadicOn, "isSporadicOn"],#id1);}
		|	 DIESE! clockRef
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_isDisjoint, "isDisjoint"],#id1);}
		|	 "hasSameRateAs"! clockRef
			{#clk_rel_expr = #([CLK_REL,"CL K_REL"],[LITERAL_hasSameRateAs, "hasSameRateAs"],#id1);}
		|	","!  clockRef "haveMaxDrift"!  INT
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_haveMaxDrift, "haveMaxDrift"],#id1);}


		/*|	(st:"strictly")? "alternatesWith"! clockRef   //TODO  ADD strictly
			{#clk_rel_expr = #([CLK_REL,"CLK_REL"],[LITERAL_alternatesWith, "alternatesWith"],#id1);}	*/
		| "sync"! "("! INT  ","! INT  ")"! clockRef
			{#clk_rel_expr  = #([CLK_REL,"CLK_REL"],[LITERAL_sync, "sync"],#id1);}
			)

		)
	;




clockgroup :
(clockRef  "by")  =>
	(x:clockRef  "by"! number1   
{ #clockgroup = #([CLKG , "CLKG" ] , x );	} )
		| clockRef ;




 /*********************************************************
*
*   DEF
*
*********************************************************/

clockdef : "Clock"! clockId2  ("is"! clockExpr ) ?  //Path
	{#clockdef = #([CLKDEF,"CLK_DEF"],#clockdef);  } ;

timerdef : "Timer"! timerId2  ("is"! timerExpr ) ?  //Path
	{#timerdef = #([CLKDEF,"CLK_DEF"],#timerdef);  } ;

instantdef : "Instant"! instantId2 ("is"! instantref ) ?   //Path
	{#instantdef = #([INSTDEF,"INSTDEF"],#instantdef);  } ;

/*********************************************************
*
*  INSTANT Relation
*
*
*********************************************************/



instantrel : 	inst:instantref  (( "coincidentWith" instantref )
					{#instantrel= #([INST_REL,"INST_REL"],#inst) ; }
				| (("strictly" | "weakly" )? "iprecedes" instantref )  // TODO
				{#instantrel =#([INST_REL,"INST_REL"],#inst)  ;} );

/*
instant : id1:path  "at"! "("! INT ")"! //clockId "."!  // ID
				{#instant= #([INST,"INST"],#id1) ; }	;
*/

//number1 is int
instantref : ( clockRef (LINTER | "at" ) ) =>
			 ( clockRef (
						 (LINTER! number1 RINTER! )
						 | ("at"! "("! number1 ")"! )
									)   )
		{#instantref = #([INSTREF1,"INSTREF1"],#instantref);  }
			| instantId
		{#instantref = #([INSTREF1b,"INSTREF1b"],#instantref);  }
			|( "instantOf"! clockRef "suchThat"! boolexpr )
		{#instantref = #([INSTREF2,"INSTREF2"],#instantref);  };


//expr:  clockExpr ; //	{#expr = #([EXPR,"EXPR_CLK_FUNC"],#expr);} ; // TODO 1001a


/*
*******************************************************************************
*******************************************************************************
*******************************************************************************
*******************************************************************************
*******************************************************************************
*/



{ @SuppressWarnings({"unchecked", "unused" }) }
class LangLexer extends Lexer;

options {
	k = 2;  // ici kv

}
/*
{

//Hashtable<ANTLRHashString,Integer>  literals =new Hashtable<ANTLRHashString,Integer>();//<ANTLRHashString,Integer>
}*/

WS	:	(	' '
		|	'\t'
		|	( 	"\r\n"
			|	'\n'
			|	'\r'
			) { newline(); }
		)
		{ _ttype = Token.SKIP; }
	;

// Single-line comments
SL_COMMENT
	:	"//"
		(~('\n'|'\r'))* ('\n'|'\r'('\n')?)?
		{$setType(Token.SKIP); newline();}
	;

MUL :"*" ;

DIV :"/" ;

MODULO: "%" ;

protected
BIT
	:	( '0' | '1' )
	;

// do not define DOT before if you use it in BITV
// must be before INT
BW		// Binary Word concrete syntax
	:	'0'! 'b'!
			(	(BIT | "_"! )+ ( '(' (BIT | "_"! )+ ')'! )?
				|	'(' (BIT | "_"!  )+ ')'!
			)
	;

protected
GBV		// generalized bit vector
	:	 RBIT ( '.' RBIT )*
	;

protected
RBIT	// repeated bit
	:	(BIT ( '^' ('1'..'9') ('0'..'9')*  )? )| ("_"!)
	;

GBW		// Binary Word , generalized, concrete syntax
	:	'0'! 'B'!
			(	GBV ( '(' GBV ')'! )?
				|	'(' GBV ')'!
			)
	;

protected
DIGIT
	:	'0'..'9'
	;

//BOOLEANOP : "?" ;
	//: LOGA | LOGO |LOGN | LOGX ;

// hexadecimal digit (again, note it's protected!)
protected
HEX_DIGIT
	:	('0'..'9'|'A'..'F'|'a'..'f')
	;



ID
options {
	testLiterals = true;
}
	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*
	(
				(('.' | ("::")) ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*)+
				{ $setType(PATH); }

			)?
	;

// don't work if INT before BITV
// a numeric literal

INT	:	( '0'..'9' )+
		(
			(
				'.' { $setType(REAL); }
				('0'..'9')+ (EXPONENT)?
			)?
		|	EXPONENT { $setType(REAL); }
		)
	|	'0' ('x'|'X') (HEX_DIGIT)+
	;


//INFinity : "INF"
//;

// a couple protected methods to assist in matching floating point numbers
protected
EXPONENT
	:	('e'|'E') (ADDOP)? ('0'..'9')+
	;

//INTERV : '[' | ']';
//INTERPP :".." ;

LINTER : '[' ;

RINTER : ']';

LPAREN:	'('
	;

RPAREN:	')'
	;

MYLCURLY: '{'
	;

MYRCURLY:	'}'
	;

ASSIGN
	:	'='
	;

SEMI :';'
	;

COMA
	:	','
	;

DOT
	:	'.'
	("." { $setType(INTERPP); })?
	;

RELOP
	:  "<" | ">" | "<=" | ">=" |  "<>"
  	;

ADDOP : "+" | "-" ;

DIESE: "#" ;


