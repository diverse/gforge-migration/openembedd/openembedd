package grammaire;

import element.dictionnaire.Cdictionnaire2;
import grammaire.analyse.ASTMask;

import java.io.InputStream;
import java.io.StringReader;

public class TestId {

	public String input;
	LangLexer lexer=null;
	LangParser parser =null;
	
	public TestId(String input)
	{
		super();
		this.input = input;
		try {

			 lexer = new LangLexer(new StringReader(input));
			 parser = new LangParser(lexer);
			parser.pathe();
			
		} catch (Exception e) {
			parser=null;
		
		}
		catch (Error ex) {
			parser=null;
		}
	}
	
	
	public TestId(InputStream inputst)
	{
		super();
		
		try {

			 lexer = new LangLexer(inputst);
			 parser = new LangParser(lexer);
			parser.ccsl();
			
		} catch (Exception e) {
			parser=null;
		
		}
		catch (Error ex) {
			parser=null;
		}
	}
	public int getnbrerror()
	{
		if (parser==null)
			return -1;
		return parser.errorNumber;
	}
	
	public String getstrerror()
	{
		if (parser==null)
			return "null";
		return parser.errorstr;
	} 
	
	public Cdictionnaire2 getDico()
	{
		if (parser==null)
			return null;
		return parser.getDico();
	}
	
	public int dispAst(String s)
	{
	
		 getAstMask().dispAst(s);	
		return 0;
		
	}
	ASTMask mask=null;
	
	public ASTMask getAstMask()
	{
		if (mask==null)
			mask= new ASTMask(parser);
		return mask;
	}
	
}
