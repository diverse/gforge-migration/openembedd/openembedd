package grammaire;

import java.io.InputStream;
import java.io.StringReader;

import element.dictionnaire.Cdictionnaire2;

import fr.inria.base.MyManager;

import grammaire.analyse.ASTMask;

public class TestSyntax {

	public String input;
	LangLexer lexer=null;
	LangParser parser =null;
	boolean error=false;
	public TestSyntax(String input)
	{
		super();
		this.input = input;
		try {

			 lexer = new LangLexer(new StringReader(input));
			 parser = new LangParser(lexer);
			parser.ccsl();
			
		} 
		catch (Throwable ex) 
		{
			error=true;
			parser=null;
			//MyManager.printError(ex);
		}
	}
	
	
	public TestSyntax(InputStream inputst)
	{
		super();
		
		try {

			 lexer = new LangLexer(inputst);
			 parser = new LangParser(lexer);
			parser.ccsl();
			
		} catch (Throwable e) {			 
			error=true;
			parser=null;
			MyManager.printError(e);  
		}
	}
	public int getnbrerror()
	{
		if (parser==null)
			return -1;
		if ((error==true)&& (parser.errorNumber==0))
			return 1;
		return parser.errorNumber;
	}
	
	public String getstrerror()
	{
		if (parser==null)
			return "null";
		return parser.errorstr;
	} 
	
	public Cdictionnaire2 getDico()
	{
		if (parser==null)
			return null;
		return parser.getDico();
	}
	
	public int dispAst(String s)
	{
	
		 getAstMask().dispAst(s);	
		return 0;
		
	}
	ASTMask mask=null;
	
	public ASTMask getAstMask()
	{
		if (mask==null)
			mask= new ASTMask(parser);
		return mask;
	}
	
}
