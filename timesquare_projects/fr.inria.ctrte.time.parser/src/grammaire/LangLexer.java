// $ANTLR : "ClockGram.g" -> "LangLexer.java"$

/******************************  
* @author Benoit Ferrero
* ClockLang.g 
*
*
*********************************/
package grammaire ;

import java.io.InputStream;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.TokenStreamRecognitionException;
import antlr.CharStreamException;
import antlr.CharStreamIOException;
import antlr.ANTLRException;
import java.io.Reader;
import java.util.Hashtable;
import antlr.CharScanner;
import antlr.InputBuffer;
import antlr.ByteBuffer;
import antlr.CharBuffer;
import antlr.Token;
import antlr.CommonToken;
import antlr.RecognitionException;
import antlr.NoViableAltForCharException;
import antlr.MismatchedCharException;
import antlr.TokenStream;
import antlr.ANTLRHashString;
import antlr.LexerSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.SemanticException;
 @SuppressWarnings({"unchecked", "unused" }) 
public class LangLexer extends antlr.CharScanner implements LangParserTokenTypes, TokenStream
 {
public LangLexer(InputStream in) {
	this(new ByteBuffer(in));
}
public LangLexer(Reader in) {
	this(new CharBuffer(in));
}
public LangLexer(InputBuffer ib) {
	this(new LexerSharedInputState(ib));
}
public LangLexer(LexerSharedInputState state) {
	super(state);
	caseSensitiveLiterals = true;
	setCaseSensitive(true);
	literals = new Hashtable();
	literals.put(new ANTLRHashString("Clock", this), new Integer(148));
	literals.put(new ANTLRHashString("durationBetween", this), new Integer(101));
	literals.put(new ANTLRHashString("Instant", this), new Integer(151));
	literals.put(new ANTLRHashString(",", this), new Integer(71));
	literals.put(new ANTLRHashString("delayedFor", this), new Integer(108));
	literals.put(new ANTLRHashString("timerConstraint", this), new Integer(117));
	literals.put(new ANTLRHashString("]", this), new Integer(33));
	literals.put(new ANTLRHashString("hasSameRateAs", this), new Integer(146));
	literals.put(new ANTLRHashString("isPeriodicOn", this), new Integer(141));
	literals.put(new ANTLRHashString("hasStability", this), new Integer(76));
	literals.put(new ANTLRHashString("+", this), new Integer(86));
	literals.put(new ANTLRHashString("..", this), new Integer(80));
	literals.put(new ANTLRHashString("Timer", this), new Integer(150));
	literals.put(new ANTLRHashString("uniform", this), new Integer(85));
	literals.put(new ANTLRHashString("haveMaxDrift", this), new Integer(72));
	literals.put(new ANTLRHashString("gap", this), new Integer(145));
	literals.put(new ANTLRHashString("isSubClockOf", this), new Integer(135));
	literals.put(new ANTLRHashString("*", this), new Integer(88));
	literals.put(new ANTLRHashString("isFasterThan", this), new Integer(137));
	literals.put(new ANTLRHashString("until", this), new Integer(124));
	literals.put(new ANTLRHashString("alternatesWith", this), new Integer(133));
	literals.put(new ANTLRHashString("[", this), new Integer(32));
	literals.put(new ANTLRHashString("to", this), new Integer(106));
	literals.put(new ANTLRHashString("and", this), new Integer(93));
	literals.put(new ANTLRHashString("withIn", this), new Integer(138));
	literals.put(new ANTLRHashString("not", this), new Integer(94));
	literals.put(new ANTLRHashString(")", this), new Integer(31));
	literals.put(new ANTLRHashString("offset", this), new Integer(143));
	literals.put(new ANTLRHashString("from", this), new Integer(105));
	literals.put(new ANTLRHashString("nonStrictly", this), new Integer(130));
	literals.put(new ANTLRHashString("restrictedToTrailing", this), new Integer(107));
	literals.put(new ANTLRHashString("coincidentWith", this), new Integer(77));
	literals.put(new ANTLRHashString("when", this), new Integer(116));
	literals.put(new ANTLRHashString("countFor", this), new Integer(123));
	literals.put(new ANTLRHashString("(", this), new Integer(30));
	literals.put(new ANTLRHashString("isSporadicOn", this), new Integer(144));
	literals.put(new ANTLRHashString("restrictedToInterval", this), new Integer(115));
	literals.put(new ANTLRHashString("haveSkew", this), new Integer(74));
	literals.put(new ANTLRHashString("iprecedes", this), new Integer(152));
	literals.put(new ANTLRHashString("rightNonStrictly", this), new Integer(132));
	literals.put(new ANTLRHashString("oneShotConstraint", this), new Integer(118));
	literals.put(new ANTLRHashString("instantOf", this), new Integer(153));
	literals.put(new ANTLRHashString("leftNonStrictly", this), new Integer(131));
	literals.put(new ANTLRHashString("sync", this), new Integer(147));
	literals.put(new ANTLRHashString("isDisjoint", this), new Integer(69));
	literals.put(new ANTLRHashString("restrictedToLeading", this), new Integer(114));
	literals.put(new ANTLRHashString("}", this), new Integer(29));
	literals.put(new ANTLRHashString("at", this), new Integer(126));
	literals.put(new ANTLRHashString("synchronizesWith", this), new Integer(134));
	literals.put(new ANTLRHashString("is", this), new Integer(149));
	literals.put(new ANTLRHashString("weakly", this), new Integer(129));
	literals.put(new ANTLRHashString("restrictedTo", this), new Integer(102));
	literals.put(new ANTLRHashString("or", this), new Integer(91));
	literals.put(new ANTLRHashString("sustain", this), new Integer(121));
	literals.put(new ANTLRHashString("haveDrift", this), new Integer(75));
	literals.put(new ANTLRHashString("if", this), new Integer(15));
	literals.put(new ANTLRHashString("min", this), new Integer(82));
	literals.put(new ANTLRHashString("suchThat", this), new Integer(154));
	literals.put(new ANTLRHashString("xor", this), new Integer(92));
	literals.put(new ANTLRHashString("by", this), new Integer(79));
	literals.put(new ANTLRHashString("minus", this), new Integer(112));
	literals.put(new ANTLRHashString("strictly", this), new Integer(68));
	literals.put(new ANTLRHashString("%", this), new Integer(90));
	literals.put(new ANTLRHashString("sup", this), new Integer(119));
	literals.put(new ANTLRHashString("discretizedBy", this), new Integer(104));
	literals.put(new ANTLRHashString("period", this), new Integer(142));
	literals.put(new ANTLRHashString("union", this), new Integer(111));
	literals.put(new ANTLRHashString("{", this), new Integer(28));
	literals.put(new ANTLRHashString("=", this), new Integer(37));
	literals.put(new ANTLRHashString("inter", this), new Integer(110));
	literals.put(new ANTLRHashString("wrt", this), new Integer(125));
	literals.put(new ANTLRHashString("isCoarserThan", this), new Integer(139));
	literals.put(new ANTLRHashString("filteredBy", this), new Integer(103));
	literals.put(new ANTLRHashString("isFinerThan", this), new Integer(136));
	literals.put(new ANTLRHashString("sampledOn", this), new Integer(113));
	literals.put(new ANTLRHashString("precedes", this), new Integer(78));
	literals.put(new ANTLRHashString("false", this), new Integer(96));
	literals.put(new ANTLRHashString("#", this), new Integer(41));
	literals.put(new ANTLRHashString("rightWeakly", this), new Integer(128));
	literals.put(new ANTLRHashString("haveOffset", this), new Integer(73));
	literals.put(new ANTLRHashString("/", this), new Integer(89));
	literals.put(new ANTLRHashString("inf", this), new Integer(120));
	literals.put(new ANTLRHashString(";", this), new Integer(70));
	literals.put(new ANTLRHashString("max", this), new Integer(81));
	literals.put(new ANTLRHashString("on", this), new Integer(99));
	literals.put(new ANTLRHashString("isSlowerThan", this), new Integer(140));
	literals.put(new ANTLRHashString("in", this), new Integer(98));
	literals.put(new ANTLRHashString("upto", this), new Integer(122));
	literals.put(new ANTLRHashString("mult", this), new Integer(100));
	literals.put(new ANTLRHashString("true", this), new Integer(95));
	literals.put(new ANTLRHashString("-", this), new Integer(87));
	literals.put(new ANTLRHashString("followedBy", this), new Integer(109));
	literals.put(new ANTLRHashString("leftWeakly", this), new Integer(127));
}

public Token nextToken() throws TokenStreamException {
	Token theRetToken=null;
tryAgain:
	for (;;) {
		Token _token = null;
		int _ttype = Token.INVALID_TYPE;
		resetText();
		try {   // for char stream error handling
			try {   // for lexical error handling
				switch ( LA(1)) {
				case '\t':  case '\n':  case '\r':  case ' ':
				{
					mWS(true);
					theRetToken=_returnToken;
					break;
				}
				case '*':
				{
					mMUL(true);
					theRetToken=_returnToken;
					break;
				}
				case '%':
				{
					mMODULO(true);
					theRetToken=_returnToken;
					break;
				}
				case 'A':  case 'B':  case 'C':  case 'D':
				case 'E':  case 'F':  case 'G':  case 'H':
				case 'I':  case 'J':  case 'K':  case 'L':
				case 'M':  case 'N':  case 'O':  case 'P':
				case 'Q':  case 'R':  case 'S':  case 'T':
				case 'U':  case 'V':  case 'W':  case 'X':
				case 'Y':  case 'Z':  case '_':  case 'a':
				case 'b':  case 'c':  case 'd':  case 'e':
				case 'f':  case 'g':  case 'h':  case 'i':
				case 'j':  case 'k':  case 'l':  case 'm':
				case 'n':  case 'o':  case 'p':  case 'q':
				case 'r':  case 's':  case 't':  case 'u':
				case 'v':  case 'w':  case 'x':  case 'y':
				case 'z':
				{
					mID(true);
					theRetToken=_returnToken;
					break;
				}
				case '+':  case '-':
				{
					mADDOP(true);
					theRetToken=_returnToken;
					break;
				}
				case '[':
				{
					mLINTER(true);
					theRetToken=_returnToken;
					break;
				}
				case ']':
				{
					mRINTER(true);
					theRetToken=_returnToken;
					break;
				}
				case '(':
				{
					mLPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case ')':
				{
					mRPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case '{':
				{
					mMYLCURLY(true);
					theRetToken=_returnToken;
					break;
				}
				case '}':
				{
					mMYRCURLY(true);
					theRetToken=_returnToken;
					break;
				}
				case '=':
				{
					mASSIGN(true);
					theRetToken=_returnToken;
					break;
				}
				case ';':
				{
					mSEMI(true);
					theRetToken=_returnToken;
					break;
				}
				case ',':
				{
					mCOMA(true);
					theRetToken=_returnToken;
					break;
				}
				case '.':
				{
					mDOT(true);
					theRetToken=_returnToken;
					break;
				}
				case '<':  case '>':
				{
					mRELOP(true);
					theRetToken=_returnToken;
					break;
				}
				case '#':
				{
					mDIESE(true);
					theRetToken=_returnToken;
					break;
				}
				default:
					if ((LA(1)=='/') && (LA(2)=='/')) {
						mSL_COMMENT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='0') && (LA(2)=='b')) {
						mBW(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='0') && (LA(2)=='B')) {
						mGBW(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='/') && (true)) {
						mDIV(true);
						theRetToken=_returnToken;
					}
					else if (((LA(1) >= '0' && LA(1) <= '9')) && (true)) {
						mINT(true);
						theRetToken=_returnToken;
					}
				else {
					if (LA(1)==EOF_CHAR) {uponEOF(); _returnToken = makeToken(Token.EOF_TYPE);}
				else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				}
				if ( _returnToken==null ) continue tryAgain; // found SKIP token
				_ttype = _returnToken.getType();
				_ttype = testLiteralsTable(_ttype);
				_returnToken.setType(_ttype);
				return _returnToken;
			}
			catch (RecognitionException e) {
				throw new TokenStreamRecognitionException(e);
			}
		}
		catch (CharStreamException cse) {
			if ( cse instanceof CharStreamIOException ) {
				throw new TokenStreamIOException(((CharStreamIOException)cse).io);
			}
			else {
				throw new TokenStreamException(cse.getMessage());
			}
		}
	}
}

	public final void mWS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = WS;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case ' ':
		{
			match(' ');
			break;
		}
		case '\t':
		{
			match('\t');
			break;
		}
		case '\n':  case '\r':
		{
			{
			if ((LA(1)=='\r') && (LA(2)=='\n')) {
				match("\r\n");
			}
			else if ((LA(1)=='\n')) {
				match('\n');
			}
			else if ((LA(1)=='\r') && (true)) {
				match('\r');
			}
			else {
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			
			}
			newline();
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		_ttype = Token.SKIP;
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSL_COMMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SL_COMMENT;
		int _saveIndex;
		
		match("//");
		{
		_loop225:
		do {
			if ((_tokenSet_0.member(LA(1)))) {
				{
				match(_tokenSet_0);
				}
			}
			else {
				break _loop225;
			}
			
		} while (true);
		}
		{
		switch ( LA(1)) {
		case '\n':
		{
			match('\n');
			break;
		}
		case '\r':
		{
			match('\r');
			{
			if ((LA(1)=='\n')) {
				match('\n');
			}
			else {
			}
			
			}
			break;
		}
		default:
			{
			}
		}
		}
		_ttype = Token.SKIP; newline();
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMUL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MUL;
		int _saveIndex;
		
		match("*");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDIV(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DIV;
		int _saveIndex;
		
		match("/");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMODULO(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MODULO;
		int _saveIndex;
		
		match("%");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mBIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BIT;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case '0':
		{
			match('0');
			break;
		}
		case '1':
		{
			match('1');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBW(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BW;
		int _saveIndex;
		
		_saveIndex=text.length();
		match('0');
		text.setLength(_saveIndex);
		_saveIndex=text.length();
		match('b');
		text.setLength(_saveIndex);
		{
		switch ( LA(1)) {
		case '0':  case '1':  case '_':
		{
			{
			int _cnt236=0;
			_loop236:
			do {
				switch ( LA(1)) {
				case '0':  case '1':
				{
					mBIT(false);
					break;
				}
				case '_':
				{
					_saveIndex=text.length();
					match("_");
					text.setLength(_saveIndex);
					break;
				}
				default:
				{
					if ( _cnt236>=1 ) { break _loop236; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				}
				_cnt236++;
			} while (true);
			}
			{
			if ((LA(1)=='(')) {
				match('(');
				{
				int _cnt239=0;
				_loop239:
				do {
					switch ( LA(1)) {
					case '0':  case '1':
					{
						mBIT(false);
						break;
					}
					case '_':
					{
						_saveIndex=text.length();
						match("_");
						text.setLength(_saveIndex);
						break;
					}
					default:
					{
						if ( _cnt239>=1 ) { break _loop239; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
					}
					}
					_cnt239++;
				} while (true);
				}
				_saveIndex=text.length();
				match(')');
				text.setLength(_saveIndex);
			}
			else {
			}
			
			}
			break;
		}
		case '(':
		{
			match('(');
			{
			int _cnt241=0;
			_loop241:
			do {
				switch ( LA(1)) {
				case '0':  case '1':
				{
					mBIT(false);
					break;
				}
				case '_':
				{
					_saveIndex=text.length();
					match("_");
					text.setLength(_saveIndex);
					break;
				}
				default:
				{
					if ( _cnt241>=1 ) { break _loop241; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				}
				_cnt241++;
			} while (true);
			}
			_saveIndex=text.length();
			match(')');
			text.setLength(_saveIndex);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mGBV(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = GBV;
		int _saveIndex;
		
		mRBIT(false);
		{
		_loop244:
		do {
			if ((LA(1)=='.')) {
				match('.');
				mRBIT(false);
			}
			else {
				break _loop244;
			}
			
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mRBIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RBIT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '0':  case '1':
		{
			{
			mBIT(false);
			{
			if ((LA(1)=='^')) {
				match('^');
				{
				matchRange('1','9');
				}
				{
				_loop250:
				do {
					if (((LA(1) >= '0' && LA(1) <= '9'))) {
						matchRange('0','9');
					}
					else {
						break _loop250;
					}
					
				} while (true);
				}
			}
			else {
			}
			
			}
			}
			break;
		}
		case '_':
		{
			{
			_saveIndex=text.length();
			match("_");
			text.setLength(_saveIndex);
			}
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mGBW(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = GBW;
		int _saveIndex;
		
		_saveIndex=text.length();
		match('0');
		text.setLength(_saveIndex);
		_saveIndex=text.length();
		match('B');
		text.setLength(_saveIndex);
		{
		switch ( LA(1)) {
		case '0':  case '1':  case '_':
		{
			mGBV(false);
			{
			if ((LA(1)=='(')) {
				match('(');
				mGBV(false);
				_saveIndex=text.length();
				match(')');
				text.setLength(_saveIndex);
			}
			else {
			}
			
			}
			break;
		}
		case '(':
		{
			match('(');
			mGBV(false);
			_saveIndex=text.length();
			match(')');
			text.setLength(_saveIndex);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mDIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DIGIT;
		int _saveIndex;
		
		matchRange('0','9');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mHEX_DIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = HEX_DIGIT;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			matchRange('0','9');
			break;
		}
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':
		{
			matchRange('A','F');
			break;
		}
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':
		{
			matchRange('a','f');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mID(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ID;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':  case 'g':  case 'h':
		case 'i':  case 'j':  case 'k':  case 'l':
		case 'm':  case 'n':  case 'o':  case 'p':
		case 'q':  case 'r':  case 's':  case 't':
		case 'u':  case 'v':  case 'w':  case 'x':
		case 'y':  case 'z':
		{
			matchRange('a','z');
			break;
		}
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':  case 'G':  case 'H':
		case 'I':  case 'J':  case 'K':  case 'L':
		case 'M':  case 'N':  case 'O':  case 'P':
		case 'Q':  case 'R':  case 'S':  case 'T':
		case 'U':  case 'V':  case 'W':  case 'X':
		case 'Y':  case 'Z':
		{
			matchRange('A','Z');
			break;
		}
		case '_':
		{
			match('_');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		_loop261:
		do {
			switch ( LA(1)) {
			case 'a':  case 'b':  case 'c':  case 'd':
			case 'e':  case 'f':  case 'g':  case 'h':
			case 'i':  case 'j':  case 'k':  case 'l':
			case 'm':  case 'n':  case 'o':  case 'p':
			case 'q':  case 'r':  case 's':  case 't':
			case 'u':  case 'v':  case 'w':  case 'x':
			case 'y':  case 'z':
			{
				matchRange('a','z');
				break;
			}
			case 'A':  case 'B':  case 'C':  case 'D':
			case 'E':  case 'F':  case 'G':  case 'H':
			case 'I':  case 'J':  case 'K':  case 'L':
			case 'M':  case 'N':  case 'O':  case 'P':
			case 'Q':  case 'R':  case 'S':  case 'T':
			case 'U':  case 'V':  case 'W':  case 'X':
			case 'Y':  case 'Z':
			{
				matchRange('A','Z');
				break;
			}
			case '_':
			{
				match('_');
				break;
			}
			case '0':  case '1':  case '2':  case '3':
			case '4':  case '5':  case '6':  case '7':
			case '8':  case '9':
			{
				matchRange('0','9');
				break;
			}
			default:
			{
				break _loop261;
			}
			}
		} while (true);
		}
		{
		if ((LA(1)=='.'||LA(1)==':')) {
			{
			int _cnt269=0;
			_loop269:
			do {
				if ((LA(1)=='.'||LA(1)==':')) {
					{
					switch ( LA(1)) {
					case '.':
					{
						match('.');
						break;
					}
					case ':':
					{
						{
						match("::");
						}
						break;
					}
					default:
					{
						throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
					}
					}
					}
					{
					switch ( LA(1)) {
					case 'a':  case 'b':  case 'c':  case 'd':
					case 'e':  case 'f':  case 'g':  case 'h':
					case 'i':  case 'j':  case 'k':  case 'l':
					case 'm':  case 'n':  case 'o':  case 'p':
					case 'q':  case 'r':  case 's':  case 't':
					case 'u':  case 'v':  case 'w':  case 'x':
					case 'y':  case 'z':
					{
						matchRange('a','z');
						break;
					}
					case 'A':  case 'B':  case 'C':  case 'D':
					case 'E':  case 'F':  case 'G':  case 'H':
					case 'I':  case 'J':  case 'K':  case 'L':
					case 'M':  case 'N':  case 'O':  case 'P':
					case 'Q':  case 'R':  case 'S':  case 'T':
					case 'U':  case 'V':  case 'W':  case 'X':
					case 'Y':  case 'Z':
					{
						matchRange('A','Z');
						break;
					}
					case '_':
					{
						match('_');
						break;
					}
					default:
					{
						throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
					}
					}
					}
					{
					_loop268:
					do {
						switch ( LA(1)) {
						case 'a':  case 'b':  case 'c':  case 'd':
						case 'e':  case 'f':  case 'g':  case 'h':
						case 'i':  case 'j':  case 'k':  case 'l':
						case 'm':  case 'n':  case 'o':  case 'p':
						case 'q':  case 'r':  case 's':  case 't':
						case 'u':  case 'v':  case 'w':  case 'x':
						case 'y':  case 'z':
						{
							matchRange('a','z');
							break;
						}
						case 'A':  case 'B':  case 'C':  case 'D':
						case 'E':  case 'F':  case 'G':  case 'H':
						case 'I':  case 'J':  case 'K':  case 'L':
						case 'M':  case 'N':  case 'O':  case 'P':
						case 'Q':  case 'R':  case 'S':  case 'T':
						case 'U':  case 'V':  case 'W':  case 'X':
						case 'Y':  case 'Z':
						{
							matchRange('A','Z');
							break;
						}
						case '_':
						{
							match('_');
							break;
						}
						case '0':  case '1':  case '2':  case '3':
						case '4':  case '5':  case '6':  case '7':
						case '8':  case '9':
						{
							matchRange('0','9');
							break;
						}
						default:
						{
							break _loop268;
						}
						}
					} while (true);
					}
				}
				else {
					if ( _cnt269>=1 ) { break _loop269; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt269++;
			} while (true);
			}
			_ttype = PATH;
		}
		else {
		}
		
		}
		_ttype = testLiteralsTable(_ttype);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mINT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = INT;
		int _saveIndex;
		
		if ((LA(1)=='0') && (LA(2)=='X'||LA(2)=='x')) {
			match('0');
			{
			switch ( LA(1)) {
			case 'x':
			{
				match('x');
				break;
			}
			case 'X':
			{
				match('X');
				break;
			}
			default:
			{
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			}
			}
			{
			int _cnt280=0;
			_loop280:
			do {
				if ((_tokenSet_1.member(LA(1)))) {
					mHEX_DIGIT(false);
				}
				else {
					if ( _cnt280>=1 ) { break _loop280; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt280++;
			} while (true);
			}
		}
		else if (((LA(1) >= '0' && LA(1) <= '9')) && (true)) {
			{
			int _cnt272=0;
			_loop272:
			do {
				if (((LA(1) >= '0' && LA(1) <= '9'))) {
					matchRange('0','9');
				}
				else {
					if ( _cnt272>=1 ) { break _loop272; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt272++;
			} while (true);
			}
			{
			if ((LA(1)=='E'||LA(1)=='e')) {
				mEXPONENT(false);
				_ttype = REAL;
			}
			else {
				{
				if ((LA(1)=='.')) {
					match('.');
					_ttype = REAL;
					{
					int _cnt276=0;
					_loop276:
					do {
						if (((LA(1) >= '0' && LA(1) <= '9'))) {
							matchRange('0','9');
						}
						else {
							if ( _cnt276>=1 ) { break _loop276; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
						}
						
						_cnt276++;
					} while (true);
					}
					{
					if ((LA(1)=='E'||LA(1)=='e')) {
						mEXPONENT(false);
					}
					else {
					}
					
					}
				}
				else {
				}
				
				}
			}
			
			}
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mEXPONENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EXPONENT;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case 'e':
		{
			match('e');
			break;
		}
		case 'E':
		{
			match('E');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		switch ( LA(1)) {
		case '+':  case '-':
		{
			mADDOP(false);
			break;
		}
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		int _cnt285=0;
		_loop285:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				matchRange('0','9');
			}
			else {
				if ( _cnt285>=1 ) { break _loop285; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			
			_cnt285++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mADDOP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ADDOP;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '+':
		{
			match("+");
			break;
		}
		case '-':
		{
			match("-");
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLINTER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LINTER;
		int _saveIndex;
		
		match('[');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRINTER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RINTER;
		int _saveIndex;
		
		match(']');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LPAREN;
		int _saveIndex;
		
		match('(');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RPAREN;
		int _saveIndex;
		
		match(')');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMYLCURLY(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MYLCURLY;
		int _saveIndex;
		
		match('{');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMYRCURLY(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MYRCURLY;
		int _saveIndex;
		
		match('}');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mASSIGN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ASSIGN;
		int _saveIndex;
		
		match('=');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSEMI(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SEMI;
		int _saveIndex;
		
		match(';');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCOMA(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = COMA;
		int _saveIndex;
		
		match(',');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDOT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DOT;
		int _saveIndex;
		
		match('.');
		{
		if ((LA(1)=='.')) {
			match(".");
			_ttype = INTERPP;
		}
		else {
		}
		
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRELOP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RELOP;
		int _saveIndex;
		
		if ((LA(1)=='<') && (LA(2)=='=')) {
			match("<=");
		}
		else if ((LA(1)=='>') && (LA(2)=='=')) {
			match(">=");
		}
		else if ((LA(1)=='<') && (LA(2)=='>')) {
			match("<>");
		}
		else if ((LA(1)=='<') && (true)) {
			match("<");
		}
		else if ((LA(1)=='>') && (true)) {
			match(">");
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDIESE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DIESE;
		int _saveIndex;
		
		match("#");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { -9217L, -1L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 287948901175001088L, 541165879422L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	
	}
