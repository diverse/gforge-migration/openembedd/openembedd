package grammaire.analyse;

import fr.inria.ctrte.time.parser.CCSLview;
import grammaire.LangParser;

import java.util.List;

import antlr.collections.AST;
import code.CmyConstraint;

public class ASTMask {

	AST a;

	public ASTMask(AST a) {
		super();
		this.a = a;
	}

	public ASTMask(List<CmyConstraint> arrayconst) {
		super();
		this.a = TransformAst.merge(arrayconst);
	}

	public AST getA() {
		return a;
	}

	public int dispAst(String s) {
		return CCSLview.getCourant().dispAST(a, s);
	}
	
	public ASTMask(LangParser parser ) {
		super();
		this.a = parser.getAST();
	}

}
