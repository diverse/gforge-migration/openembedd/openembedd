/*
 *
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste
 * Copyright  2007-2008
 */
package grammaire.analyse;

import java.util.HashMap;

import antlr.BaseAST;
import antlr.collections.AST;
import element.dictionnaire.CsubdictionnaireClock;

import fr.inria.base.MyManager;
import fr.inria.wave.Clock;
import fr.inria.wave.VCDfile;
import fr.inria.wave.constraint.Alternates;
import fr.inria.wave.constraint.Disjoint;
import fr.inria.wave.constraint.EqualClock;
import fr.inria.wave.constraint.FilteringBy;
import fr.inria.wave.constraint.InfFaster;
import fr.inria.wave.constraint.Inter;
import fr.inria.wave.constraint.Minus;
import fr.inria.wave.constraint.MultiInf;
import fr.inria.wave.constraint.MultiSup;
import fr.inria.wave.constraint.OneShot;
import fr.inria.wave.constraint.PeriodicOn;
import fr.inria.wave.constraint.Precedes;
import fr.inria.wave.constraint.PureDelay;
import fr.inria.wave.constraint.RestrictToInterval;
import fr.inria.wave.constraint.SampleTo;
import fr.inria.wave.constraint.SubClock;
import fr.inria.wave.constraint.SubLower;
import fr.inria.wave.constraint.Sustain;
import fr.inria.wave.constraint.Sync;
import fr.inria.wave.constraint.TimerOn;
import fr.inria.wave.constraint.Union;
import fr.inria.wave.data.BW;
import fr.inria.wave.data.bw.BinaryWord;
import fr.inria.wave.data.bw.GBinaryWord;
import fr.inria.wave.enumeration.Moderator;
import fr.inria.wave.proba.IDParam;
import fr.inria.wave.proba.Uniform;

import grammaire.LangParserTokenTypes;

public class Analysis {
	// public List<Action> action = new ArrayList<Action>();
	public HashMap<String, String> bsw = new HashMap<String, String>();
	public HashMap<String, Clock> clockid = new HashMap<String, Clock>();
	CsubdictionnaireClock cdclk = null;

	VCDfile vc = null;

	public class GroupBy {
		String s;
		int n;
		AST ast;

		public String getS() {
			return s;
		}

		public int getN() {
			return n;
		}

		public GroupBy(AST ast) {
			super();
			this.ast = ast;

			s = "noclock@";
			n = 1;
			if (ast == null) {
				return;
			}
			

			if (ast.getType() == LangParserTokenTypes.CLKG) {
				s = ast.getFirstChild().getFirstChild().getText();
				n = Integer.parseInt(ast.getFirstChild().getNextSibling()
						.getText());
			}
			if (ast.getType() == LangParserTokenTypes.CLKEXPR) {
				AST a = ast.getFirstChild();
				if (a.getType() == LangParserTokenTypes.ID)
					s = a.getText();
				if (a.getType() == LangParserTokenTypes.PATH)
					s = a.getText();
				n = 1;
			}
			if (ast.getType() == LangParserTokenTypes.ID) {
				s = ast.getFirstChild().getText();
				n = 1;
			}
			if (ast.getType() == LangParserTokenTypes.PATH) {
				s = ast.getFirstChild().getText();
				n = 1;
			}
		}

	}

	public Moderator getmodeModerator(AST ast) {
		if (ast.getType() == LangParserTokenTypes.STRICTLY)
			return Moderator.strictly;
		if (ast.getType() == LangParserTokenTypes.LITERAL_leftWeakly)
			return Moderator.leftWeakly;
		if (ast.getType() == LangParserTokenTypes.LITERAL_rightWeakly)
			return Moderator.rightWeakly;
		if (ast.getType() == LangParserTokenTypes.LITERAL_leftNonStrictly)
			return Moderator.leftWeakly;
		if (ast.getType() == LangParserTokenTypes.LITERAL_rightNonStrictly )
			return Moderator.rightWeakly;
		if (ast.getType() == LangParserTokenTypes.LITERAL_weakly)
			return Moderator.weakly;
		return Moderator.weakly;
	}

	public Analysis(VCDfile vc) {
		this.vc = vc;
	}

	private int foundinternalBW(AST a, String sid) {

		AST x = a.getFirstChild();		
		int n = x.getType();
		String s = x.getText();
	
		BinaryWord bw = null;
		if (n == LangParserTokenTypes.GBW) {
			bw = GBinaryWord.parseGBinaryWord(s);
			// bw = gbw ; // new BinaryWord(gbw.getPrefix(), gbw.getPeriod());

		} else {
			bw = BinaryWord.parseBinaryWord(s);		

		}
		bsw.put(sid, bw.getBody());	
		return 0;

	}

	private BW getBW(AST a) {
		BinaryWord bw = null;
		AST x = a.getFirstChild();	
		int n = x.getType();
		String s = x.getText();
		if (n == LangParserTokenTypes.GBW) {
			GBinaryWord gbw = GBinaryWord.parseGBinaryWord(s);

		
			return gbw;

		}
		bw = BinaryWord.parseBinaryWord(s);
			
		return bw;
		
	}

	public Clock addClock(String s) {
		Clock n = clockid.get(s);
		if (n != null)
			return n;
		try {
			n = vc.createClock(s);
			clockid.put(s, n);
			return n;
		} catch (Exception ex) {
			MyManager.printError(ex, "never run ");
			MyManager.println("ERROR 123456"); // never run
			return null;
		}

	}

	private AST[] gettable(AST a) {

		AST ast = a.getFirstChild();
		if (ast == null)
			return null;
		int n = 0; // README bug fixed; ( --->tb[tb.length-1]==null, reduire
					// taille tableau)
		while (ast != null) {
			ast = ast.getNextSibling();
			n++;
		}
		AST tb[] = new AST[n];
		ast = a.getFirstChild();
		n = 0;
		while (ast != null) {
			tb[n] = ast;
			ast = ast.getNextSibling();
			n++;
		}
		// remove
		// :taile n+1 ;
		// avec tb[n]= null;
		return tb;

	}

	private String getidexprname(AST a, String sid) {
		if (a.getType() == LangParserTokenTypes.CLKEXPR) {
			AST ast = a.getFirstChild();
			if (ast.getType() == LangParserTokenTypes.ID)
				return ast.getText();
			if (ast.getType() == LangParserTokenTypes.PATH)
				return ast.getText();
			return sid;
		}
		return sid; // +a.getText()+a.getType();
	}

	private int getint(AST a) {
		if (a.getType() == LangParserTokenTypes.INT) {
			// AST ast = a.getFirstChild();
			return Integer.parseInt(a.getText());

		}
		return 0; // +a.getText()+a.getType();
	}
	
	private Clock[] getlist(AST a[], int n, String  sid)
	{
		int size= a.length-n;
		Clock c[]= new Clock[size];
		for (int i=0; i<size;i++)
			c[i]=addClock(getidexprname(a[i+n], sid + 2 + '_'));
		return c;
	}
	
	private IDParam getUniform(AST a) throws Exception  {
		if (a.getType() == LangParserTokenTypes.LITERAL_uniform) {
			// AST ast = a.getFirstChild();
			int x=getint(a.getFirstChild());
			int y=getint(a.getFirstChild().getNextSibling());
			MyManager.println("testunifom :"+x +" " + y);
			return new Uniform(x,y);
		}
		return null; // +a.getText()+a.getType();
	}

	private int foundinternal(AST tb[], int id, String s, String s1)
			throws Exception {
		foundinternal(tb[id], s + (id + 1) + "_", s1);
		return 0;
	}

	int earliest = -1;
	int lastest = -1;

	private int foundinternalclkRel(AST a, String sid, String s2)
			throws Exception {

		Clock id1 = null;
		Clock id2 = null;
		AST tb[] = gettable(a);
		String ps1 = getidexprname(tb[1], sid + 2 + '_');
		String ps2 = getidexprname(tb[2], sid + 3 + '_');
		// String ps3;

		GroupBy g1 = null, g2 = null;
		Moderator md;
		clkrelswitch: switch (tb[0].getType()) {
		case LangParserTokenTypes.LITERAL_alternatesWith:
			// id1 = addClock(ps1);
			g1 = new GroupBy(tb[1]);
			if (tb.length == 4) {
				md = getmodeModerator(tb[2]);
				g2 = new GroupBy(tb[3]);
			} else {
				md = Moderator.strictly;
				g2 = new GroupBy(tb[2]);
			}		
			id1 = addClock(g1.getS());
			id2 = addClock(g2.getS());
			vc.add(new Alternates(id1, id2, g1.getN(), g2.getN(), md));
			// action.add(new Alternative(id1, id2,b));
			break clkrelswitch;
		
		case LangParserTokenTypes.LITERAL_isSubClockOf:
			// id1 = addClock(ps1);
			g1 = new GroupBy(tb[1]);			
			g2 = new GroupBy(tb[2]);
			

			
			id1 = addClock(g1.getS());
			id2 = addClock(g2.getS());
			vc.add(new SubClock(id2,id1));
			// action.add(new Alternative(id1, id2,b));
			break clkrelswitch;
			
		case LangParserTokenTypes.LITERAL_precedes:
			// id1 = addClock(ps1);
			g1 = new GroupBy(tb[1]);
			if (tb.length == 4) {
				md = getmodeModerator(tb[2]);
				g2 = new GroupBy(tb[3]);
			} else {
				md = Moderator.strictly;
				g2 = new GroupBy(tb[2]);
			}

			MyManager.println("precedes" +g1.getS() + " " + g2.getS() + " " + md);
			id1 = addClock(g1.getS());
			id2 = addClock(g2.getS());
			vc.add(new Precedes(id1, id2, g1.getN(), g2.getN(), md));
			// action.add(new Alternative(id1, id2,b));
			break clkrelswitch;

		case LangParserTokenTypes.LITERAL_isCoarserThan:
			id1 = addClock(ps1);
			id2 = addClock(ps2);
			vc.add(new SubClock(id1, id2));
			// action.add(new SubClocking(id1, id2));
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_isFinerThan:
			id1 = addClock(ps1);
			id2 = addClock(ps2);
			vc.add(new SubClock(id2, id1));
			// action.add(new SubClocking(id2, id1));
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_isFasterThan:
			id1 = addClock(ps1);
			id2 = addClock(ps2);
			
			if (tb.length == 3) {
				earliest = 0;
				lastest = -1;
			} else {
				earliest = getint(tb[3]);				
				lastest = getint(tb[4]);
			}
			// action.add(new Cfaster(id1, id2,earliest,lastest));
			// missing
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_isSlowerThan:
			id1 = addClock(ps1);
			id2 = addClock(ps2);
			
			if (tb.length == 3) {
				earliest = 0;
				lastest = -1;
			} else {
				earliest = getint(tb[3]);				
				lastest = getint(tb[4]);
			}
			// action.add(new Cfaster(id2, id1,earliest,lastest));
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_isDisjoint:
			id1 = addClock(ps1);
			id2 = addClock(ps2);
			vc.add(new Disjoint(id1,id2));
			break;
		case LangParserTokenTypes.LITERAL_isPeriodicOn:
			id1 = addClock(ps1);
			id2 = addClock(ps2);			
			if (tb.length == 5) {	
				int offset=0 ;
				if (tb[4].getType()==LangParserTokenTypes.INT)	
					offset= getint(tb[4]);
				if (tb[4].getType()==LangParserTokenTypes.LITERAL_uniform)	
					offset =getUniform(tb[4]).getNextValue();
				
				if (tb[3].getType()==LangParserTokenTypes.INT)		
					vc.add(new PeriodicOn (id2,id1 , getint(tb[3]),offset ));
				if (tb[3].getType()==LangParserTokenTypes.LITERAL_uniform)
					vc.add(new PeriodicOn (id2,id1 , getUniform(tb[3]),offset ));
					
			}
			
			
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_sync:
			// z1 = tb[2].getNextSibling();
			// z2 = tb[3].getNextSibling();
			String ps4 = getidexprname(tb[4], sid + 4 + '_');
			id1 = addClock(ps1);
			id2 = addClock(ps4);
			// action.add(new Sync(id1, id2, getint(tb[2]), getint(tb[3])));
			vc.add(new Sync(id1, id2, getint(tb[2]), getint(tb[3]),
					Moderator.strictly));
			
			break clkrelswitch;
		case LangParserTokenTypes.LITERAL_synchronizesWith:
			g1 = new GroupBy(tb[1]);

			if (tb.length == 4) {
				g2 = new GroupBy(tb[3]);
				md = getmodeModerator(tb[2]);
			} else {
				g2 = new GroupBy(tb[2]);
				md = Moderator.weakly;
			}
			id1 = addClock(g1.getS());
			id2 = addClock(g2.getS());			
			vc.add(new Sync(id1, id2, g1.getN(), g2.getN(), md));

			break clkrelswitch;
			
		default:
			System.out.println("test64" + tb[0].getType() );
		}
		foundinternal(tb, 1, sid, s2);
		foundinternal(tb, 2, sid, s2);
		return 1;

	}

	private int foundinternalclkFunc(AST a, String sid, String s2)
			throws Exception {
		// int tpe = a.getType();
		AST tb[] = gettable(a);
		int n;
	
		Clock id1 = null;
		Clock id2 = null;
		Clock id3 = null;		
		Clock id4 = null, id5 = null;
		Clock id6 =null;
		String ps1 = null;
		String ps2 = null;
		String ps3 = null;
		String ps4 = null;
	
		clkfuncswitch: switch (a.getFirstChild().getType()) {
		case LangParserTokenTypes.LITERAL_filteredBy:

			ps1 = getidexprname(tb[1], sid + 2 + '_');
			ps2 = getidexprname(tb[2], sid + 3 + '_');		
			id1 = addClock(s2);// sub
			id2 = addClock(ps1); // super
			BW bw;
			bw = getBW(tb[2]);
			// action.add(new Filtering(id1, id2, ps2));
			vc.add(new FilteringBy(id2, id1, bw));
			if (tb[1].getFirstChild().getType() != LangParserTokenTypes.ID) {
				foundinternal(tb[1], sid + "2_", s2);
				MyManager.println("call ref");
			}
			// Binary word
			foundinternal(tb[2], sid + "3_", s2);
			break clkfuncswitch;
		case LangParserTokenTypes.LITERAL_discretizedBy:
			
			vc.discretizeBy(addClock(s2), tb[2].getText());
			break clkfuncswitch;
		case LangParserTokenTypes.LITERAL_delayedFor:
			ps1 = getidexprname(tb[1], sid + 2 + '_');		
			if (tb.length==3)
				ps3=ps1;
			else
				ps3=getidexprname(tb[3], sid + 3 + '_');
			
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps3); 			
			if (tb[2].getType()==LangParserTokenTypes.INT)	
				vc.add(new PureDelay(id3, id2, id1,getint(tb[2])));
			if (tb[2].getType()==LangParserTokenTypes.LITERAL_uniform)	
				vc.add(new PureDelay(id3, id2, id1,getUniform(tb[2])));
			break;
		case LangParserTokenTypes.LITERAL_followedBy:
			break;
		case LangParserTokenTypes.LITERAL_sustain :	
			ps1 = getidexprname(tb[1], sid + 2 + '_');		
			ps2 = getidexprname(tb[2], sid + 2 + '_');		
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps2);
			vc.add(new Sustain(id1,id2,id3));
			break;
		case LangParserTokenTypes.LITERAL_inf :	
			ps1 = getidexprname(tb[1], sid + 2 + '_');		
			ps2 = getidexprname(tb[2], sid + 2 + '_');	
			if (tb.length==3)
			{
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps2);
			vc.add(new InfFaster(id1,id2,id3));
			}
			else
			{
				id1 = addClock(s2);
				vc.add(new MultiInf(id1,getlist(tb, 1 ,sid))); 
			}
			break;
		case LangParserTokenTypes.LITERAL_sup :	
			ps1 = getidexprname(tb[1], sid + 2 + '_');		
			ps2 = getidexprname(tb[2], sid + 2 + '_');	
			if (tb.length==3)
			{
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps2);
			vc.add(new SubLower(id1,id2,id3));
			}
			else
			{
				id1 = addClock(s2);
				vc.add(new MultiSup(id1,getlist(tb, 1 ,sid)));
			}
			break;
		case LangParserTokenTypes.LITERAL_union:
			ps1 = getidexprname(tb[1], sid + 2 + '_');			
			ps3=getidexprname(tb[2], sid + 3 + '_');			
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps3); 
			vc.add(new Union(id1,id2,id3));
			break;
		case LangParserTokenTypes.LITERAL_inter:
			ps1 = getidexprname(tb[1], sid + 2 + '_');			
			ps3=getidexprname(tb[2], sid + 3 + '_');			
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps3); 
			vc.add(new Inter(id1,id2,id3));
			break;
		case LangParserTokenTypes.LITERAL_minus:
			ps1 = getidexprname(tb[1], sid + 2 + '_');			
			ps3=getidexprname(tb[2], sid + 3 + '_');			
			id1 = addClock(s2);
			id2 = addClock(ps1); 
			id3= addClock(ps3); 
			vc.add(new Minus(id1,id2,id3));
			break;
			
		case LangParserTokenTypes.LITERAL_restrictedToTrailing:
			ps1 = getidexprname(tb[1], sid + 2 + '_');
			n = getint(tb[2]);
		
			id1 = addClock(s2);
			id2 = addClock(ps1);
			MyManager.println("**********************");
			// action.add(new CDelayedfor(id1, id2, n));
			vc.add(new RestrictToInterval(id2, id1, n, Integer.MAX_VALUE));
			if (tb[1].getFirstChild().getType() != LangParserTokenTypes.ID) {
				foundinternal(tb[1], sid + "2_", s2);
				MyManager.println("call ref");
			}
			break;
		case LangParserTokenTypes.LITERAL_restrictedToLeading:
			ps1 = getidexprname(tb[1], sid + 2 + '_');
			n = getint(tb[2]);			
			id1 = addClock(s2);
			id2 = addClock(ps1);
			MyManager.println("**********************");
			vc.add(new RestrictToInterval(id2, id1, 0, n));
			// action.add(new CshortenTo(id1, id2, n));
			if (tb[1].getFirstChild().getType() != LangParserTokenTypes.ID) {
				foundinternal(tb[1], sid + "2_", s2);
				MyManager.println("call ref");
			}

			break;
		case LangParserTokenTypes.LITERAL_restrictedToInterval:
			ps1 = getidexprname(tb[1], sid + 2 + '_');
			n = getint(tb[2]);
			// z2 = z.getNextSibling();
			int n2 = getint(tb[3]);			
			id1 = addClock(s2);
			id2 = addClock(ps1);
		
			vc.add(new RestrictToInterval(id2, id1, n, n2));
			// action.add(new CrestrictTointerval(id1, id2, n, n2));
			if (tb[1].getFirstChild().getType() != LangParserTokenTypes.ID) {
				foundinternal(tb[1], sid + "2_", s2);
				MyManager.println("call ref");
			}

			break;
		//case LangParserTokenTypes.LITERAL_
			
			//break;
		case LangParserTokenTypes.LITERAL_sampledOn: 
			foundinternal(tb[1], sid + "2_", s2);		
			Moderator md= Moderator.strictly;
			if (tb.length==4)
			{	
				md=getmodeModerator(tb[2]);
			
				foundinternal(tb[3], sid + "4_", s2);
				ps3 = getidexprname(tb[3], sid + 4 + '_');
			
			} else {
			
				foundinternal(tb[2], sid + "3_", s2);
				ps3 = getidexprname(tb[2], sid + 3 + '_');
				
			}
			id3 = addClock(ps3);
			id1 = addClock(s2);
			id2 = addClock(getidexprname(tb[1], sid + 2 + '_'));
			// id3 = addClock(getidexprname(tb[2], sid + 3 + '_'));
			vc.add(new SampleTo(id3, id2, id1, md.is_strictly()));
			// action.add(new Sampleto(id1, id3, id2,b));

			break clkfuncswitch;	

		case LangParserTokenTypes.LITERAL_timerConstraint:
			/*	*/

		
			ps1 = getidexprname(tb[1], sid + 2 + '_');
			ps2 = getidexprname(tb[2], sid + 3 + '_');
			ps3 = getidexprname(tb[3], sid + 4 + '_');
			ps4 = getidexprname(tb[4], sid + 5 + '_');
		
			id1 = addClock(s2);
			id2 = addClock(ps1);
			id3 = addClock(ps2);
			id4 = addClock(ps3);
			id5 = addClock(ps4);			
			if (tb[5].getType()==LangParserTokenTypes.INT)		
			vc.add(new TimerOn(id1, id2, id3, id4, id5,  getint(tb[5])));
			if (tb[5].getType()==LangParserTokenTypes.LITERAL_uniform)		
				vc.add(new TimerOn(id1, id2, id3, id4, id5,  getUniform(tb[5])));
			
			// action.add(new CtimerOn(id1,id2,id3,id4,id5,y));
			break;
		case LangParserTokenTypes.LITERAL_oneShotConstraint:
			/*	*/

		
			ps1 = getidexprname(tb[1], sid + 2 + '_');
			ps2 = getidexprname(tb[2], sid + 3 + '_');
			ps3 = getidexprname(tb[3], sid + 4 + '_');

			
			id1 = addClock(s2);
			id2 = addClock(ps1);
			id3 = addClock(ps2);
			id6 = addClock(ps3);

			if (tb[4].getType()==LangParserTokenTypes.INT)		
				vc.add(new OneShot(id1, id2, id3, id6, getint(tb[4])));
			if (tb[4].getType()==LangParserTokenTypes.LITERAL_uniform)
				vc.add(new OneShot(id1, id2, id3, id6, getUniform(tb[4])));
			// action.add(new ConeShoot(id1,id2,id3,id6,y));
			break;
		default:
			if (tb[1] != null)
				foundinternal(tb[1], sid + "2_", s2);
		}
		return 0;
	}

	/**
	 * 
	 * @param a
	 * @param sid
	 * @param s2
	 * @return
	 */
	boolean flag=false;
	private int foundinternal(AST a, String sid, String s2) throws Exception {

		int i = 1;
	
	
		int tpe = a.getType();
		AST x = a.getFirstChild();
		mainswitch: switch (tpe) {
		case LangParserTokenTypes.CLKDEF:
			 
			if (x.getNextSibling() != null)
			{
				if (x.getNextSibling().getType()== 14 )
				{
					Clock id1 = addClock(x.getNextSibling().getFirstChild().getText());
					Clock id2= addClock(x.getFirstChild().getText());
					System.out.println("Assign" +a.getText() +" s1:"+sid+" s2:"+s2);
					vc.add( new EqualClock(id1, id2)); 
				}
				else
					foundinternal(x.getNextSibling(), sid + 2 + "_", x.getText());
			}
			
			break mainswitch;
		case LangParserTokenTypes.ASSIGN:	
			//System.out.println("--"+x.getNextSibling().getType());
			if (x.getNextSibling().getType()== 14 )
			{
				Clock id1 = addClock(x.getNextSibling().getFirstChild().getText());
				Clock id2= addClock(x.getFirstChild().getText());
				System.out.println("Assign" +a.getText() +" s1:"+sid+" s2:"+s2);
				vc.add( new EqualClock(id1, id2)); 
			}
			else
			foundinternal(x.getNextSibling(), sid + "2_1_", x.getFirstChild().getText()); // PATCH
			
			break mainswitch;
		case LangParserTokenTypes.CLK_FUNC:
			foundinternalclkFunc(a, sid, s2); 
			break mainswitch;
		case LangParserTokenTypes.CLK_REL:
			foundinternalclkRel(a, sid, s2);
			break mainswitch;
		case LangParserTokenTypes.BWEXPR:
			foundinternalBW(a, sid);
			break mainswitch;
		case LangParserTokenTypes.ID :
			if (s2!=null && flag)
			{
				
				Clock id1 = addClock(a.getText());
				Clock id2= addClock(s2);
				//System.out.println("bug........................" +a.getText() +" s1:"+sid+" s2:"+s2);
				vc.add( new EqualClock(id1, id2)); 
			}
			//System.out.println("..........................." +a.getText() +" s1:"+sid+" s2:"+s2);
			break;
		default: {
			AST t = a.getFirstChild();
			while (t != null) {
				String s = s2;
				if (t.getType() == LangParserTokenTypes.CLK_FUNC)
					s = sid;
				foundinternal(t, sid + i + "_", s);
				i++;
				t = t.getNextSibling();
			}
		}
		}
		flag=false;
		return 0;
	}

	public int found(BaseAST a) {
		try {
			BaseAST bs = a;
			foundinternal(bs, "id_", null);
		//	MyManager.println("--------------------------");
			//Set<String> sets = bsw.keySet();
			/*for (String s : sets) {
				//MyManager.println("< < " + s + " > : < " + bsw.get(s) + " >");
			}*/
		} catch (Exception ex) {
			MyManager.printError(ex);
		}
		return 0;
	}

}
