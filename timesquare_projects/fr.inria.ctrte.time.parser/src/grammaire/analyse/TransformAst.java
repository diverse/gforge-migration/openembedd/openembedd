/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package grammaire.analyse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import antlr.collections.AST;
import code.CmyConstraint;
import grammaire.LangParserTokenTypes;
import grammaire.MyAST;

public class TransformAst {

	static int count = 0;
	static boolean fullresolve = true;

	public static String getlocalname() {
		return "__local" + count++;
	}

	public static AST transformAST(AST t) {
		AST x[] = TransformAst.toarray(t);
		ArrayList<AST> tbl = new ArrayList<AST>();
		for (AST i : x) {
			AST tx[] = TransformAst.transformArray(i, 0);
			for (AST j : tx)
				tbl.add(j);
		}

		AST x2[] = tbl.toArray(new AST[0]);
		AST m = TransformAst.merge(x2);
		return m;
	}

	public static boolean isId(AST t) {
		return (t.getType() == LangParserTokenTypes.ID)
				|| (t.getType() == LangParserTokenTypes.PATH);
	}

	public static boolean isIdEx(AST t) {		
		if (t.getType() == LangParserTokenTypes.CLKEXPR)
			if (t.getText().compareTo("ID") == 0)
				return true;

		return (t.getType() == LangParserTokenTypes.ID)
				|| (t.getType() == LangParserTokenTypes.PATH);
	}

	public static AST[] transformArray(AST tbe) {
		return null;
	}

	public static AST creenodeid(String s) {
		AST t;
		t = new MyAST();
		t.setText("ID");
		t.setType(LangParserTokenTypes.CLKEXPR);
		AST t2 = new MyAST();
		t2.setText(s);
		t2.setType(LangParserTokenTypes.ID);
		t.setFirstChild(t2);
		return t;
	}

	public static AST creenode(String s, int n) {
		AST t;
		t = new MyAST();
		t.setText(s);
		t.setType(n);
		return t;
	}

	public static AST[] transformArray(AST tbe, int i) {
		AST guard = null;
		AST test[] = toarray(tbe);
		if (test.length >= 2) {
			if (test[1] != null)
				if (test[1].getType() == LangParserTokenTypes.GUARD) {
					guard = test[1];
				}
		}

		//
		AST t[] = new AST[2];
		t[0] = cloneAST(tbe, false);
		t[1] = null;
		AST tb = tbe.getFirstChild();
		if (tb.getType() == LangParserTokenTypes.ASSIGN) {
			
			AST lt[] = toarray(tb);
			boolean b0 = isIdEx(lt[0]);
			boolean b1 = isIdEx(lt[1]);
		
			if (b0 == false) {
				if (b1 == true) {
					// inverse
					AST x = lt[0];
					AST y = lt[1];
					AST ass = t[0].getFirstChild();
					ass.setFirstChild(y);
					y.setNextSibling(x);
					x.setNextSibling(null);
				} else {
					String name = tbe.getText();
					AST x = lt[0];
					AST y = lt[1];
					AST ass = creenode("=", LangParserTokenTypes.ASSIGN);
					String localname = getlocalname();
					AST idl = creenodeid(localname);
					AST idl2 = creenodeid(localname);

					x.setNextSibling(null);
					y.setNextSibling(null);
					t[0].getFirstChild().setFirstChild(idl);
					t[0].setText(name + "_1");
					idl.setNextSibling(y);
					t[1] = creenode(name + "_2", LangParserTokenTypes.STAT);
					t[1].setFirstChild(ass);
					ass.setFirstChild(idl2);
					idl2.setNextSibling(y);

					ass.setNextSibling(cloneAST(guard, false));

					// introduire
				}
			}
		}
		ArrayList<AST> arast = new ArrayList<AST>();
		for (AST ot : t) {
			arast.add(ot);
			String s = "";
			if (ot != null) {
				s = ot.getText();
			}
			transformArray2(arast, ot, s, 0, guard);
		}
		return arast.toArray(new AST[1]);
	}

	private static int transformArray2(ArrayList<AST> lst, AST a, String name,
			int n, AST guard) {

		// CLKEXPR,
		if (a == null)
			return 0;
		AST t = a.getFirstChild();

		AST lt = null;
		while (t != null) {
			AST ta = t;
			if (t.getType() == LangParserTokenTypes.CLKEXPR) {
			
				if (t.getText().compareTo("CLK_EXPR_FUNC") == 0) {
					
					String localname = getlocalname();
					AST idl = creenodeid(localname);
					AST idl2 = creenodeid(localname);
					
					if (lt != null) {
						lt.setNextSibling(idl);
						idl.setNextSibling(t.getNextSibling());
						t = lt;
					} else {
						a.setFirstChild(idl);
						idl.setNextSibling(t.getNextSibling());
					}
					AST ass = creenode("=", LangParserTokenTypes.ASSIGN);
					AST node = creenode(name + "_" + n,
							LangParserTokenTypes.STAT);
					node.setFirstChild(ass);
					ass.setFirstChild(idl2);
					idl2.setNextSibling(cloneAST(ta.getFirstChild(), true));
					ass.setNextSibling(cloneAST(guard, true));
					lst.add(node);
				}
			}
			transformArray2(lst, ta, name + "_" + n, 0, guard);
			lt = t;
			t = t.getNextSibling();
			n++;

		}
		return 0;
	}

	public static AST merge(ArrayList<AST> arrayast) {
		AST t[] = arrayast.toArray(new AST[arrayast.size()]);
		return merge(t);

	}

	public static AST merge(List<CmyConstraint> arrayconst) {
		AST nextast = null;
		int j = 0;
		ArrayList<AST> arrayast = new ArrayList<AST>();
		for (CmyConstraint c : arrayconst) {
			nextast = c.getAST();
			j = 0;
			if ((c.errorstr.compareTo("") == 0) && (nextast != null)) {

				AST tb[] = TransformAst.toarray(nextast);
				for (AST t : tb) {
					t.setText("STAT" + "_" + j + "@" + c.hashCode());
					arrayast.add(t);
					j++;
				}
			}
		}

		return TransformAst.merge(arrayast);

	}

	public static AST merge(AST tb[]) {
		int n = tb.length;
		AST r = null;
		AST p = new MyAST();
		p.setText("BLOCK");
		p.setType(LangParserTokenTypes.BLOCK);
		// p.setFirstChild(cloneAST(tb[0], true));
		r = p;
		AST x = null;
		for (int i = n - 1; i >= 0; i--)	
		{
			
			x = cloneAST(tb[i], true);
			if (x != null) {
				AST y = r.getFirstChild();

				r.setFirstChild(x);
				x.setNextSibling(y);

			}

		}
		return r;
	}

	public static AST[] toarray(AST t) {
		ArrayList<AST> tbl = new ArrayList<AST>();
		// AST tb[]= null;
		AST local;
		int i = 0;
		t = t.getFirstChild();
		while (t != null) {
			local = cloneAST(t, false);
			local.setNextSibling(null);
			tbl.add(local);
			if (local.getFirstChild() != null) {
				if (local.getType() == LangParserTokenTypes.STAT)
					local.setText(local.getText() + i);
			}
			t = t.getNextSibling();
			i++;
		}		
		return tbl.toArray(new AST[tbl.size()]);
	}

	public static AST cloneAST(AST t, boolean b) {
		MyAST nt = new MyAST();
		if (t == null)
			return null;
		nt.setType(t.getType());
		nt.setText(t.getText());
		if (t.getFirstChild() != null) {
			nt.setFirstChild(cloneAST(t.getFirstChild(), true));
		}
		if (b == true) {
			if (t.getNextSibling() != null)
				nt.setNextSibling(cloneAST(t.getNextSibling(), true));
		} else
			nt.setNextSibling(null);
		return nt;
	}

	static public AST renameid(AST a, String old, String newb) {
		AST t = a.getFirstChild();
		while (t != null) {
			renameid(t, old, newb);
			t = t.getNextSibling();
		}
		if ((a.getType() == LangParserTokenTypes.ID)
				|| a.getType() == LangParserTokenTypes.PATH) {
			if (a.getText().compareTo(old) == 0)
				a.setText(newb);
		}
		return a;
	}

	static public AST revolveguard(AST t, HashMap<String, Boolean> hmb) {
		fullresolve = true;
		AST tb[] = toarray(t);
		for (int i = 0; i < tb.length; i++) {
			int n = internalguard(tb[i], hmb);
			if (n == 0)
				tb[i] = null;
			if (n == 2)
				fullresolve = false;
		}
		return merge(tb);
	}

	static private int internalguard(AST st, HashMap<String, Boolean> hmb) {
		AST t1 = st.getFirstChild();
		if (t1 == null)
			return -1;
		AST t2 = t1.getNextSibling();
		if (t2 == null)
			return 1;
		if (t2.getType() != LangParserTokenTypes.GUARD)
			return -1;
		int x = evalguard(t2.getFirstChild(), hmb);
		if (x == 1)
			t1.setNextSibling(null);
		/*
		 * if (x==2) st.setFirstChild(null);
		 */
		return x;
	}

	static private int evalguard(AST st, HashMap<String, Boolean> hmb) {
		AST t = null;
		int n = st.getType();
		int y, x, r;
		switch (n) {
		case LangParserTokenTypes.LITERAL_true:
			return 1;
		case LangParserTokenTypes.LITERAL_false:
			return 0;
		case LangParserTokenTypes.ID:
		case LangParserTokenTypes.PATH:
			Boolean b = hmb.get(st.getText());
			if (b == null)
				return 2;
			if (b == true)
				return 1;
			if (b == false)
				return 0;
		case LangParserTokenTypes.LOGNOT:
			y = evalguard(st.getFirstChild(), hmb);
			if (y == 2)
				return 2;
			return 1 - y;
		case LangParserTokenTypes.LOGOR:
			t = st.getFirstChild();
			r = 0;
			x = 0;
			while (t != null) {
				y = evalguard(t, hmb);
				if (y == 2)
					x = 1;
				if (y == 1)
					r = 1;
				t = t.getNextSibling();
			}
			if (r == 1)
				return 1;
			if (x == 1)
				return 2;
			return 0;

		case LangParserTokenTypes.LOGAND:
			t = st.getFirstChild();
			r = 1;
			x = 0;
			while (t != null) {
				y = evalguard(t, hmb);
				if (y == 2)
					x = 1;
				if (y == 0)
					r = 0;
				t = t.getNextSibling();
			}
			if (r == 0)
				return 0;
			if (x == 1)
				return 2;
			return 1;

		case LangParserTokenTypes.LOGXOR:
			t = st.getFirstChild();
			r = 0;
			x = 0;
			while (t != null) {
				y = evalguard(t, hmb);
				if (y == 2)
					x = 1;
				if (y == 1)
					r = 1 - r;
				t = t.getNextSibling();
			}

			if (x == 1)
				return 2;
			return r;
		}

		//
		//
		return 0;
	}

}
