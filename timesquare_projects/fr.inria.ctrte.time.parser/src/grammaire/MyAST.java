package grammaire;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;

public class MyAST extends CommonAST {

	public MyAST(Token arg0)
	{
		super(arg0);
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 8299717605894932736L;
	public MyAST()
	{
		super();
		
	}
	

	
	
	
	
	Object data;
	/* (non-Javadoc)
	 * @see antlr.CommonAST#getText()
	 */
	@Override
	public String getText()
	{
		// TODO Auto-generated method stub
		return super.getText();
	}
	/* (non-Javadoc)
	 * @see antlr.CommonAST#getType()
	 */
	@Override
	public int getType()
	{
		// TODO Auto-generated method stub
		return super.getType();
	}
	/* (non-Javadoc)
	 * @see antlr.CommonAST#setText(java.lang.String)
	 */
	@Override
	public void setText( String arg0 )
	{
		// TODO Auto-generated method stub
		super.setText(arg0);
	}
	/* (non-Javadoc)
	 * @see antlr.CommonAST#setType(int)
	 */
	@Override
	public void setType( int arg0 )
	{
		// TODO Auto-generated method stub
		super.setType(arg0);
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#getColumn()
	 */
	@Override
	public int getColumn()
	{
		// TODO Auto-generated method stub
		return super.getColumn();
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#getFirstChild()
	 */
	@Override
	public AST getFirstChild()
	{
		// TODO Auto-generated method stub
		return super.getFirstChild();
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#getLine()
	 */
	@Override
	public int getLine()
	{
		// TODO Auto-generated method stub
		return super.getLine();
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#getNextSibling()
	 */
	@Override
	public AST getNextSibling()
	{
		// TODO Auto-generated method stub
		return super.getNextSibling();
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#getNumberOfChildren()
	 */
	@Override
	public int getNumberOfChildren()
	{
		// TODO Auto-generated method stub
		return super.getNumberOfChildren();
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#setFirstChild(antlr.collections.AST)
	 */
	@Override
	public void setFirstChild( AST arg0 )
	{
		// TODO Auto-generated method stub
		super.setFirstChild(arg0);
	}
	/* (non-Javadoc)
	 * @see antlr.BaseAST#setNextSibling(antlr.collections.AST)
	 */
	@Override
	public void setNextSibling( AST arg0 )
	{
		// TODO Auto-generated method stub
		super.setNextSibling(arg0);
	}
	/**
	 * @return the data
	 */
	public final Object getData()
	{
		
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public final void setData( Object data )
	{
		this.data = data;
	}
	
	
}
