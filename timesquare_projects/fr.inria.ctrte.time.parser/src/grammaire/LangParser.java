// $ANTLR : "ClockGram.g" -> "LangParser.java"$

/******************************  
* @author Benoit Ferrero
* ClockLang.g 
*
*
*********************************/
package grammaire ;

import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.collections.AST;
import java.util.Hashtable;
import antlr.ASTFactory;
import antlr.ASTPair;
import antlr.collections.impl.ASTArray;

import element.dictionnaire.Cdictionnaire2;
import element.dictionnaire.Cvariable2;
@SuppressWarnings("unused")
 
public class LangParser extends antlr.LLkParser       implements LangParserTokenTypes
 {

	Cdictionnaire2 dico2=new Cdictionnaire2();
	private	Cvariable2 tampon=null;
	int cpt=0;
	public Cdictionnaire2 getDico() { return dico2;}
	public int errorNumber=0;
	public String errorstr="";
	@Override
 	public void reportError(RecognitionException ex)
	{
		//TODO update error message
		errorNumber++;
		System.out.println("---> Error " +errorNumber+ "  "+ 	ex.toString());
		System.out.println("---> Error " + 	ex.getClass().getName() +" " +inputState.guessing);
		//if (ex instanceof antlr.NoViableAltException)
	//	System.out.println(((antlr.NoViableAltException) ex).token. getText()	);
		errorstr += "> Err" + errorNumber+"< "+ex.line +":" +ex.column +" >"+ex.getMessage() +" \n";
		//ex.printStackTrace(System.err);
		 super.reportError( ex)	;
	}


  

protected LangParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public LangParser(TokenBuffer tokenBuf) {
  this(tokenBuf,4);
}

protected LangParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public LangParser(TokenStream lexer) {
  this(lexer,4);
}

public LangParser(ParserSharedInputState state) {
  super(state,4);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

	public final void ccsl() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST ccsl_AST = null;
		
		try {      // for error handling
			block();
			astFactory.addASTChild(currentAST, returnAST);
			ccsl_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = ccsl_AST;
	}
	
/*********************************************************************
*
*	Block
*
**********************************************************************/
	public final void block() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST block_AST = null;
		
		try {      // for error handling
			match(MYLCURLY);
			{
			_loop4:
			do {
				if ((_tokenSet_1.member(LA(1)))) {
					cStatement();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop4;
				}
				
			} while (true);
			}
			match(MYRCURLY);
			if ( inputState.guessing==0 ) {
				block_AST = (AST)currentAST.root;
				
							//System.out.println("Block......................");
							block_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(BLOCK,"BLOCK")).add(block_AST));
							
				currentAST.root = block_AST;
				currentAST.child = block_AST!=null &&block_AST.getFirstChild()!=null ?
					block_AST.getFirstChild() : block_AST;
				currentAST.advanceChildToEnd();
			}
			block_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = block_AST;
	}
	
/*********************************************************************
*
*	Statement
*
**********************************************************************/
	public final void cStatement() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST cStatement_AST = null;
		
		try {      // for error handling
			statement();
			astFactory.addASTChild(currentAST, returnAST);
			{
			guard();
			astFactory.addASTChild(currentAST, returnAST);
			}
			if ( inputState.guessing==0 ) {
				cStatement_AST = (AST)currentAST.root;
				cStatement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(STAT,"STAT")).add(cStatement_AST));cpt++;
				currentAST.root = cStatement_AST;
				currentAST.child = cStatement_AST!=null &&cStatement_AST.getFirstChild()!=null ?
					cStatement_AST.getFirstChild() : cStatement_AST;
				currentAST.advanceChildToEnd();
			}
			cStatement_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = cStatement_AST;
	}
	
	public final void statement() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST statement_AST = null;
		AST pcr_AST = null;
		AST cr3_AST = null;
		AST cr_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_Clock:
			{
				clockdef();
				astFactory.addASTChild(currentAST, returnAST);
				statement_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_Instant:
			{
				instantdef();
				astFactory.addASTChild(currentAST, returnAST);
				statement_AST = (AST)currentAST.root;
				break;
			}
			default:
				boolean synPredMatched13 = false;
				if (((_tokenSet_3.member(LA(1))) && (_tokenSet_4.member(LA(2))) && (_tokenSet_5.member(LA(3))) && (_tokenSet_6.member(LA(4))))) {
					int _m13 = mark();
					synPredMatched13 = true;
					inputState.guessing++;
					try {
						{
						metaref();
						match(ASSIGN);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched13 = false;
					}
					rewind(_m13);
inputState.guessing--;
				}
				if ( synPredMatched13 ) {
					metaref();
					astFactory.addASTChild(currentAST, returnAST);
					AST tmp3_AST = null;
					tmp3_AST = astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp3_AST);
					match(ASSIGN);
					metaref();
					astFactory.addASTChild(currentAST, returnAST);
					statement_AST = (AST)currentAST.root;
				}
				else {
					boolean synPredMatched15 = false;
					if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_7.member(LA(2))) && (_tokenSet_8.member(LA(3))) && (_tokenSet_9.member(LA(4))))) {
						int _m15 = mark();
						synPredMatched15 = true;
						inputState.guessing++;
						try {
							{
							clockRef();
							match(71);
							clockRef();
							match(LITERAL_haveMaxDrift);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched15 = false;
						}
						rewind(_m15);
inputState.guessing--;
					}
					if ( synPredMatched15 ) {
						clk_rel_expr();
						pcr_AST = (AST)returnAST;
						astFactory.addASTChild(currentAST, returnAST);
						if ( inputState.guessing==0 ) {
							statement_AST = (AST)currentAST.root;
							statement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(REL,"RELATION2")).add(pcr_AST));
							currentAST.root = statement_AST;
							currentAST.child = statement_AST!=null &&statement_AST.getFirstChild()!=null ?
								statement_AST.getFirstChild() : statement_AST;
							currentAST.advanceChildToEnd();
						}
						statement_AST = (AST)currentAST.root;
					}
					else {
						boolean synPredMatched18 = false;
						if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_10.member(LA(2))) && (_tokenSet_11.member(LA(3))) && (_tokenSet_12.member(LA(4))))) {
							int _m18 = mark();
							synPredMatched18 = true;
							inputState.guessing++;
							try {
								{
								clockRef();
								match(71);
								clockRef();
								{
								switch ( LA(1)) {
								case LITERAL_haveOffset:
								{
									match(LITERAL_haveOffset);
									break;
								}
								case LITERAL_haveSkew:
								{
									match(LITERAL_haveSkew);
									break;
								}
								case LITERAL_haveDrift:
								{
									match(LITERAL_haveDrift);
									break;
								}
								default:
								{
									throw new NoViableAltException(LT(1), getFilename());
								}
								}
								}
								}
							}
							catch (RecognitionException pe) {
								synPredMatched18 = false;
							}
							rewind(_m18);
inputState.guessing--;
						}
						if ( synPredMatched18 ) {
							chronoNFP();
							astFactory.addASTChild(currentAST, returnAST);
							if ( inputState.guessing==0 ) {
								statement_AST = (AST)currentAST.root;
								statement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CHRONONFP,"CHRONONFPEXP2")).add(statement_AST));
								currentAST.root = statement_AST;
								currentAST.child = statement_AST!=null &&statement_AST.getFirstChild()!=null ?
									statement_AST.getFirstChild() : statement_AST;
								currentAST.advanceChildToEnd();
							}
							statement_AST = (AST)currentAST.root;
						}
						else {
							boolean synPredMatched20 = false;
							if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_10.member(LA(2))) && (_tokenSet_11.member(LA(3))) && (_tokenSet_12.member(LA(4))))) {
								int _m20 = mark();
								synPredMatched20 = true;
								inputState.guessing++;
								try {
									{
									clockRef();
									match(LITERAL_hasStability);
									}
								}
								catch (RecognitionException pe) {
									synPredMatched20 = false;
								}
								rewind(_m20);
inputState.guessing--;
							}
							if ( synPredMatched20 ) {
								chronoNFP();
								astFactory.addASTChild(currentAST, returnAST);
								if ( inputState.guessing==0 ) {
									statement_AST = (AST)currentAST.root;
									statement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CHRONONFP,"CHRONONFPEXP")).add(statement_AST));
									currentAST.root = statement_AST;
									currentAST.child = statement_AST!=null &&statement_AST.getFirstChild()!=null ?
										statement_AST.getFirstChild() : statement_AST;
									currentAST.advanceChildToEnd();
								}
								statement_AST = (AST)currentAST.root;
							}
							else {
								boolean synPredMatched23 = false;
								if (((_tokenSet_13.member(LA(1))) && (_tokenSet_14.member(LA(2))) && (_tokenSet_15.member(LA(3))) && (_tokenSet_16.member(LA(4))))) {
									int _m23 = mark();
									synPredMatched23 = true;
									inputState.guessing++;
									try {
										{
										instantref();
										{
										switch ( LA(1)) {
										case LITERAL_coincidentWith:
										{
											match(LITERAL_coincidentWith);
											break;
										}
										case LITERAL_precedes:
										{
											match(LITERAL_precedes);
											break;
										}
										case STRICTLY:
										{
											match(STRICTLY);
											break;
										}
										default:
										{
											throw new NoViableAltException(LT(1), getFilename());
										}
										}
										}
										}
									}
									catch (RecognitionException pe) {
										synPredMatched23 = false;
									}
									rewind(_m23);
inputState.guessing--;
								}
								if ( synPredMatched23 ) {
									instantrel();
									cr3_AST = (AST)returnAST;
									astFactory.addASTChild(currentAST, returnAST);
									if ( inputState.guessing==0 ) {
										statement_AST = (AST)currentAST.root;
										statement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(RELINST,"RELATION_INSTANT")).add(cr3_AST));
										currentAST.root = statement_AST;
										currentAST.child = statement_AST!=null &&statement_AST.getFirstChild()!=null ?
											statement_AST.getFirstChild() : statement_AST;
										currentAST.advanceChildToEnd();
									}
									statement_AST = (AST)currentAST.root;
								}
								else if ((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_7.member(LA(2))) && (_tokenSet_8.member(LA(3))) && (_tokenSet_9.member(LA(4)))) {
									clk_rel_expr();
									cr_AST = (AST)returnAST;
									astFactory.addASTChild(currentAST, returnAST);
									if ( inputState.guessing==0 ) {
										statement_AST = (AST)currentAST.root;
										statement_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(REL,"RELATION")).add(cr_AST));
										currentAST.root = statement_AST;
										currentAST.child = statement_AST!=null &&statement_AST.getFirstChild()!=null ?
											statement_AST.getFirstChild() : statement_AST;
										currentAST.advanceChildToEnd();
									}
									statement_AST = (AST)currentAST.root;
								}
							else {
								throw new NoViableAltException(LT(1), getFilename());
							}
							}}}}}
						}
						catch (RecognitionException ex) {
							if (inputState.guessing==0) {
								reportError(ex);
								recover(ex,_tokenSet_17);
							} else {
							  throw ex;
							}
						}
						returnAST = statement_AST;
					}
					
	public final void guard() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST guard_AST = null;
		AST b_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case IF:
			{
				{
				match(IF);
				{
				boolexpr1();
				b_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				}
				match(70);
				if ( inputState.guessing==0 ) {
					guard_AST = (AST)currentAST.root;
					guard_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(GUARD,"GUARD")).add(guard_AST));
					currentAST.root = guard_AST;
					currentAST.child = guard_AST!=null &&guard_AST.getFirstChild()!=null ?
						guard_AST.getFirstChild() : guard_AST;
					currentAST.advanceChildToEnd();
				}
				}
				guard_AST = (AST)currentAST.root;
				break;
			}
			case 70:
			{
				{
				match(70);
				}
				guard_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = guard_AST;
	}
	
	public final void boolexpr1() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr1_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			boolexpr2();
			l_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_or:
			{
				{
				int _cnt66=0;
				_loop66:
				do {
					if ((LA(1)==LITERAL_or)) {
						match(LITERAL_or);
						boolexpr2();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt66>=1 ) { break _loop66; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt66++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					boolexpr1_AST = (AST)currentAST.root;
					boolexpr1_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGOR,"OR")).add(l_AST)) ;
					currentAST.root = boolexpr1_AST;
					currentAST.child = boolexpr1_AST!=null &&boolexpr1_AST.getFirstChild()!=null ?
						boolexpr1_AST.getFirstChild() : boolexpr1_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case IF:
			case RPAREN:
			case ASSIGN:
			case STRICTLY:
			case 70:
			case 71:
			case LITERAL_coincidentWith:
			case LITERAL_weakly:
			case LITERAL_iprecedes:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			boolexpr1_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_18);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr1_AST;
	}
	
/*********************************************************
*
*   DEF
*
*********************************************************/
	public final void clockdef() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockdef_AST = null;
		
		try {      // for error handling
			match(LITERAL_Clock);
			clockId2();
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_is:
			{
				match(LITERAL_is);
				clockExpr();
				astFactory.addASTChild(currentAST, returnAST);
				break;
			}
			case IF:
			case 70:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			if ( inputState.guessing==0 ) {
				clockdef_AST = (AST)currentAST.root;
				clockdef_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CLKDEF,"CLK_DEF")).add(clockdef_AST));
				currentAST.root = clockdef_AST;
				currentAST.child = clockdef_AST!=null &&clockdef_AST.getFirstChild()!=null ?
					clockdef_AST.getFirstChild() : clockdef_AST;
				currentAST.advanceChildToEnd();
			}
			clockdef_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = clockdef_AST;
	}
	
/****/
	public final void metaref() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST metaref_AST = null;
		
		try {      // for error handling
			boolean synPredMatched26 = false;
			if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_19.member(LA(2))) && (_tokenSet_20.member(LA(3))) && (_tokenSet_21.member(LA(4))))) {
				int _m26 = mark();
				synPredMatched26 = true;
				inputState.guessing++;
				try {
					{
					clockRef();
					match(LITERAL_by);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched26 = false;
				}
				rewind(_m26);
inputState.guessing--;
			}
			if ( synPredMatched26 ) {
				clockgroup();
				astFactory.addASTChild(currentAST, returnAST);
				metaref_AST = (AST)currentAST.root;
			}
			else if ((_tokenSet_3.member(LA(1))) && (_tokenSet_22.member(LA(2))) && (_tokenSet_23.member(LA(3))) && (_tokenSet_24.member(LA(4)))) {
				clockExpr();
				astFactory.addASTChild(currentAST, returnAST);
				metaref_AST = (AST)currentAST.root;
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		returnAST = metaref_AST;
	}
	
/*********************************************************
*
*   Clock Ref
*
*********************************************************/
	public final void clockRef() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockRef_AST = null;
		AST id_AST = null;
		AST cef_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case ID:
			case PATH:
			{
				clockId();
				id_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					clockRef_AST = (AST)currentAST.root;
					clockRef_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CLKEXPR,"ID")).add(id_AST));
					currentAST.root = clockRef_AST;
					currentAST.child = clockRef_AST!=null &&clockRef_AST.getFirstChild()!=null ?
						clockRef_AST.getFirstChild() : clockRef_AST;
					currentAST.advanceChildToEnd();
				}
				clockRef_AST = (AST)currentAST.root;
				break;
			}
			case LPAREN:
			{
				{
				match(LPAREN);
				clockExpr();
				cef_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(RPAREN);
				}
				if ( inputState.guessing==0 ) {
					clockRef_AST = (AST)currentAST.root;
					clockRef_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CLKEXPR,"CLK_EXPR_FUNC")).add(cef_AST));
					currentAST.root = clockRef_AST;
					currentAST.child = clockRef_AST!=null &&clockRef_AST.getFirstChild()!=null ?
						clockRef_AST.getFirstChild() : clockRef_AST;
					currentAST.advanceChildToEnd();
				}
				clockRef_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_26);
			} else {
			  throw ex;
			}
		}
		returnAST = clockRef_AST;
	}
	
	public final void clk_rel_expr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clk_rel_expr_AST = null;
		AST id2_AST = null;
		AST id1_AST = null;
		
		try {      // for error handling
			boolean synPredMatched179 = false;
			if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_27.member(LA(2))) && (_tokenSet_8.member(LA(3))) && (_tokenSet_28.member(LA(4))))) {
				int _m179 = mark();
				synPredMatched179 = true;
				inputState.guessing++;
				try {
					{
					clockgroup();
					moderator();
					{
					switch ( LA(1)) {
					case LITERAL_alternatesWith:
					{
						match(LITERAL_alternatesWith);
						break;
					}
					case LITERAL_synchronizesWith:
					{
						match(LITERAL_synchronizesWith);
						break;
					}
					case LITERAL_precedes:
					{
						match(LITERAL_precedes);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
				}
				catch (RecognitionException pe) {
					synPredMatched179 = false;
				}
				rewind(_m179);
inputState.guessing--;
			}
			if ( synPredMatched179 ) {
				{
				clockgroup();
				id2_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				moderator();
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LITERAL_alternatesWith:
				{
					{
					match(LITERAL_alternatesWith);
					clockgroup();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_alternatesWith,"alternatesWith")).add(id2_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					}
					break;
				}
				case LITERAL_synchronizesWith:
				{
					{
					match(LITERAL_synchronizesWith);
					clockgroup();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_synchronizesWith,"synchronizesWith")).add(id2_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					}
					break;
				}
				case LITERAL_precedes:
				{
					{
					match(LITERAL_precedes);
					clockgroup();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_precedes,"precedes")).add(id2_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				clk_rel_expr_AST = (AST)currentAST.root;
			}
			else if ((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_29.member(LA(2))) && (_tokenSet_30.member(LA(3))) && (_tokenSet_31.member(LA(4)))) {
				{
				clockRef();
				id1_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LITERAL_isSubClockOf:
				{
					match(LITERAL_isSubClockOf);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isSubClockOf,"isSubClockOf")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isFinerThan:
				{
					match(LITERAL_isFinerThan);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isFinerThan,"isFinerThan")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isFasterThan:
				{
					match(LITERAL_isFasterThan);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_withIn:
					{
						match(LITERAL_withIn);
						match(LINTER);
						AST tmp20_AST = null;
						tmp20_AST = astFactory.create(LT(1));
						astFactory.addASTChild(currentAST, tmp20_AST);
						match(INT);
						match(80);
						{
						switch ( LA(1)) {
						case INT:
						{
							AST tmp22_AST = null;
							tmp22_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp22_AST);
							match(INT);
							break;
						}
						case 88:
						{
							AST tmp23_AST = null;
							tmp23_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp23_AST);
							match(88);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						match(RINTER);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isFasterThan,"isFasterThan")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isCoarserThan:
				{
					match(LITERAL_isCoarserThan);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isCoarserThan,"isCoarserThan")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isSlowerThan:
				{
					match(LITERAL_isSlowerThan);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_withIn:
					{
						match(LITERAL_withIn);
						match(LINTER);
						AST tmp29_AST = null;
						tmp29_AST = astFactory.create(LT(1));
						astFactory.addASTChild(currentAST, tmp29_AST);
						match(INT);
						match(80);
						AST tmp31_AST = null;
						tmp31_AST = astFactory.create(LT(1));
						astFactory.addASTChild(currentAST, tmp31_AST);
						match(INT);
						match(RINTER);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isSlowerThan,"isSlowerThan")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isPeriodicOn:
				{
					match(LITERAL_isPeriodicOn);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_period:
					{
						match(LITERAL_period);
						{
						switch ( LA(1)) {
						case INT:
						case REAL:
						case LPAREN:
						case ID:
						case PATH:
						case 86:
						case 87:
						{
							number1();
							astFactory.addASTChild(currentAST, returnAST);
							break;
						}
						case LITERAL_uniform:
						{
							proba();
							astFactory.addASTChild(currentAST, returnAST);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						{
						switch ( LA(1)) {
						case LITERAL_offset:
						{
							match(LITERAL_offset);
							{
							switch ( LA(1)) {
							case INT:
							case REAL:
							case LPAREN:
							case ID:
							case PATH:
							case 86:
							case 87:
							{
								number1();
								astFactory.addASTChild(currentAST, returnAST);
								break;
							}
							case LITERAL_uniform:
							{
								proba();
								astFactory.addASTChild(currentAST, returnAST);
								break;
							}
							default:
							{
								throw new NoViableAltException(LT(1), getFilename());
							}
							}
							}
							break;
						}
						case IF:
						case 70:
						{
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isPeriodicOn,"isPeriodicOn")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_isSporadicOn:
				{
					match(LITERAL_isSporadicOn);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_gap:
					{
						match(LITERAL_gap);
						number1();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isSporadicOn,"isSporadicOn")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case DIESE:
				{
					match(DIESE);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_isDisjoint,"isDisjoint")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_hasSameRateAs:
				{
					match(LITERAL_hasSameRateAs);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CL K_REL")).add(astFactory.create(LITERAL_hasSameRateAs,"hasSameRateAs")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case 71:
				{
					match(71);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					match(LITERAL_haveMaxDrift);
					AST tmp42_AST = null;
					tmp42_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp42_AST);
					match(INT);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_haveMaxDrift,"haveMaxDrift")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_sync:
				{
					match(LITERAL_sync);
					match(LPAREN);
					AST tmp45_AST = null;
					tmp45_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp45_AST);
					match(INT);
					match(71);
					AST tmp47_AST = null;
					tmp47_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp47_AST);
					match(INT);
					match(RPAREN);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clk_rel_expr_AST = (AST)currentAST.root;
						clk_rel_expr_AST  = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_REL,"CLK_REL")).add(astFactory.create(LITERAL_sync,"sync")).add(id1_AST));
						currentAST.root = clk_rel_expr_AST;
						currentAST.child = clk_rel_expr_AST!=null &&clk_rel_expr_AST.getFirstChild()!=null ?
							clk_rel_expr_AST.getFirstChild() : clk_rel_expr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				clk_rel_expr_AST = (AST)currentAST.root;
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = clk_rel_expr_AST;
	}
	
/*********************************************************
*
*   Chrono NFP
*
*********************************************************/
	public final void chronoNFP() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST chronoNFP_AST = null;
		AST id_AST = null;
		
		try {      // for error handling
			clockRef();
			id_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_hasStability:
			{
				{
				match(LITERAL_hasStability);
				realint();
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LITERAL_wrt:
				{
					wrt();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				case IF:
				case 70:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				if ( inputState.guessing==0 ) {
					chronoNFP_AST = (AST)currentAST.root;
					chronoNFP_AST= (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CHRONONFP,"CHRONONFP_STABILITY")).add(astFactory.create(LITERAL_hasStability,"hasStability")).add(id_AST));
					currentAST.root = chronoNFP_AST;
					currentAST.child = chronoNFP_AST!=null &&chronoNFP_AST.getFirstChild()!=null ?
						chronoNFP_AST.getFirstChild() : chronoNFP_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case 71:
			{
				{
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LITERAL_haveOffset:
				{
					{
					match(LITERAL_haveOffset);
					durationexpr();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_wrt:
					{
						wrt();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					case LITERAL_at:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case LITERAL_at:
					{
						ate();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
					if ( inputState.guessing==0 ) {
						chronoNFP_AST = (AST)currentAST.root;
						chronoNFP_AST= (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CHRONONFP,"CHRONONFP_OFFSET")).add(astFactory.create(LITERAL_haveOffset,"haveOffset")).add(id_AST));
						currentAST.root = chronoNFP_AST;
						currentAST.child = chronoNFP_AST!=null &&chronoNFP_AST.getFirstChild()!=null ?
							chronoNFP_AST.getFirstChild() : chronoNFP_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_haveSkew:
				{
					{
					match(LITERAL_haveSkew);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_wrt:
					{
						wrt();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					case LITERAL_at:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case LITERAL_at:
					{
						ate();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
					if ( inputState.guessing==0 ) {
						chronoNFP_AST = (AST)currentAST.root;
						chronoNFP_AST= (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CHRONONFP,"CHRONONFP_SKEW")).add(astFactory.create(LITERAL_haveSkew,"haveSkew")).add(id_AST));
						currentAST.root = chronoNFP_AST;
						currentAST.child = chronoNFP_AST!=null &&chronoNFP_AST.getFirstChild()!=null ?
							chronoNFP_AST.getFirstChild() : chronoNFP_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_haveDrift:
				{
					{
					match(LITERAL_haveDrift);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_wrt:
					{
						wrt();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					case LITERAL_at:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case LITERAL_at:
					{
						ate();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
					if ( inputState.guessing==0 ) {
						chronoNFP_AST = (AST)currentAST.root;
						chronoNFP_AST= (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CHRONONFP,"CHRONONFP_DRIFT")).add(astFactory.create(LITERAL_haveDrift,"haveDrift")).add(id_AST));
						currentAST.root = chronoNFP_AST;
						currentAST.child = chronoNFP_AST!=null &&chronoNFP_AST.getFirstChild()!=null ?
							chronoNFP_AST.getFirstChild() : chronoNFP_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			chronoNFP_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = chronoNFP_AST;
	}
	
	public final void instantref() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantref_AST = null;
		
		try {      // for error handling
			boolean synPredMatched213 = false;
			if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_32.member(LA(2))))) {
				int _m213 = mark();
				synPredMatched213 = true;
				inputState.guessing++;
				try {
					{
					clockRef();
					{
					switch ( LA(1)) {
					case LINTER:
					{
						match(LINTER);
						break;
					}
					case LITERAL_at:
					{
						match(LITERAL_at);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
				}
				catch (RecognitionException pe) {
					synPredMatched213 = false;
				}
				rewind(_m213);
inputState.guessing--;
			}
			if ( synPredMatched213 ) {
				{
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LINTER:
				{
					{
					match(LINTER);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					match(RINTER);
					}
					break;
				}
				case LITERAL_at:
				{
					{
					match(LITERAL_at);
					match(LPAREN);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					match(RPAREN);
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				if ( inputState.guessing==0 ) {
					instantref_AST = (AST)currentAST.root;
					instantref_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INSTREF1,"INSTREF1")).add(instantref_AST));
					currentAST.root = instantref_AST;
					currentAST.child = instantref_AST!=null &&instantref_AST.getFirstChild()!=null ?
						instantref_AST.getFirstChild() : instantref_AST;
					currentAST.advanceChildToEnd();
				}
				instantref_AST = (AST)currentAST.root;
			}
			else if ((LA(1)==ID||LA(1)==PATH) && (_tokenSet_33.member(LA(2)))) {
				instantId();
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					instantref_AST = (AST)currentAST.root;
					instantref_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INSTREF1b,"INSTREF1b")).add(instantref_AST));
					currentAST.root = instantref_AST;
					currentAST.child = instantref_AST!=null &&instantref_AST.getFirstChild()!=null ?
						instantref_AST.getFirstChild() : instantref_AST;
					currentAST.advanceChildToEnd();
				}
				instantref_AST = (AST)currentAST.root;
			}
			else if ((LA(1)==LITERAL_instantOf)) {
				{
				match(LITERAL_instantOf);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(LITERAL_suchThat);
				boolexpr();
				astFactory.addASTChild(currentAST, returnAST);
				}
				if ( inputState.guessing==0 ) {
					instantref_AST = (AST)currentAST.root;
					instantref_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INSTREF2,"INSTREF2")).add(instantref_AST));
					currentAST.root = instantref_AST;
					currentAST.child = instantref_AST!=null &&instantref_AST.getFirstChild()!=null ?
						instantref_AST.getFirstChild() : instantref_AST;
					currentAST.advanceChildToEnd();
				}
				instantref_AST = (AST)currentAST.root;
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_33);
			} else {
			  throw ex;
			}
		}
		returnAST = instantref_AST;
	}
	
/*********************************************************
*
*  INSTANT Relation
*
*
*********************************************************/
	public final void instantrel() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantrel_AST = null;
		AST inst_AST = null;
		
		try {      // for error handling
			instantref();
			inst_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_coincidentWith:
			{
				{
				AST tmp61_AST = null;
				tmp61_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp61_AST);
				match(LITERAL_coincidentWith);
				instantref();
				astFactory.addASTChild(currentAST, returnAST);
				}
				if ( inputState.guessing==0 ) {
					instantrel_AST = (AST)currentAST.root;
					instantrel_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INST_REL,"INST_REL")).add(inst_AST)) ;
					currentAST.root = instantrel_AST;
					currentAST.child = instantrel_AST!=null &&instantrel_AST.getFirstChild()!=null ?
						instantrel_AST.getFirstChild() : instantrel_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case STRICTLY:
			case LITERAL_weakly:
			case LITERAL_iprecedes:
			{
				{
				{
				switch ( LA(1)) {
				case STRICTLY:
				{
					AST tmp62_AST = null;
					tmp62_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp62_AST);
					match(STRICTLY);
					break;
				}
				case LITERAL_weakly:
				{
					AST tmp63_AST = null;
					tmp63_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp63_AST);
					match(LITERAL_weakly);
					break;
				}
				case LITERAL_iprecedes:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				AST tmp64_AST = null;
				tmp64_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp64_AST);
				match(LITERAL_iprecedes);
				instantref();
				astFactory.addASTChild(currentAST, returnAST);
				}
				if ( inputState.guessing==0 ) {
					instantrel_AST = (AST)currentAST.root;
					instantrel_AST =(AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INST_REL,"INST_REL")).add(inst_AST))  ;
					currentAST.root = instantrel_AST;
					currentAST.child = instantrel_AST!=null &&instantrel_AST.getFirstChild()!=null ?
						instantrel_AST.getFirstChild() : instantrel_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			instantrel_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = instantrel_AST;
	}
	
	public final void instantdef() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantdef_AST = null;
		
		try {      // for error handling
			match(LITERAL_Instant);
			instantId2();
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_is:
			{
				match(LITERAL_is);
				instantref();
				astFactory.addASTChild(currentAST, returnAST);
				break;
			}
			case IF:
			case 70:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			if ( inputState.guessing==0 ) {
				instantdef_AST = (AST)currentAST.root;
				instantdef_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INSTDEF,"INSTDEF")).add(instantdef_AST));
				currentAST.root = instantdef_AST;
				currentAST.child = instantdef_AST!=null &&instantdef_AST.getFirstChild()!=null ?
					instantdef_AST.getFirstChild() : instantdef_AST;
				currentAST.advanceChildToEnd();
			}
			instantdef_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = instantdef_AST;
	}
	
	public final void clockgroup() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockgroup_AST = null;
		AST x_AST = null;
		
		try {      // for error handling
			boolean synPredMatched197 = false;
			if (((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_34.member(LA(2))) && (_tokenSet_35.member(LA(3))) && (_tokenSet_36.member(LA(4))))) {
				int _m197 = mark();
				synPredMatched197 = true;
				inputState.guessing++;
				try {
					{
					clockRef();
					match(LITERAL_by);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched197 = false;
				}
				rewind(_m197);
inputState.guessing--;
			}
			if ( synPredMatched197 ) {
				{
				clockRef();
				x_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(LITERAL_by);
				number1();
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					clockgroup_AST = (AST)currentAST.root;
					clockgroup_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CLKG,"CLKG")).add(x_AST));	
					currentAST.root = clockgroup_AST;
					currentAST.child = clockgroup_AST!=null &&clockgroup_AST.getFirstChild()!=null ?
						clockgroup_AST.getFirstChild() : clockgroup_AST;
					currentAST.advanceChildToEnd();
				}
				}
				clockgroup_AST = (AST)currentAST.root;
			}
			else if ((LA(1)==LPAREN||LA(1)==ID||LA(1)==PATH) && (_tokenSet_37.member(LA(2))) && (_tokenSet_38.member(LA(3))) && (_tokenSet_21.member(LA(4)))) {
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				clockgroup_AST = (AST)currentAST.root;
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_39);
			} else {
			  throw ex;
			}
		}
		returnAST = clockgroup_AST;
	}
	
/*********************************************************
*
*  Clock expresion
*
*
*********************************************************/
	public final void clockExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockExpr_AST = null;
		AST id1_AST = null;
		AST id4_AST = null;
		AST id5_AST = null;
		AST id6_AST = null;
		AST id7_AST = null;
		AST id8_AST = null;
		AST id9_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LPAREN:
			case ID:
			case PATH:
			{
				{
				clockRef();
				id1_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				{
				switch ( LA(1)) {
				case LITERAL_restrictedTo:
				{
					match(LITERAL_restrictedTo);
					boolexpr();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_restrictedTo,"restrictedTo")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_filteredBy:
				{
					match(LITERAL_filteredBy);
					bwexpr();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_filteredBy,"filteredBy")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_discretizedBy:
				{
					match(LITERAL_discretizedBy);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					{
					switch ( LA(1)) {
					case LITERAL_from:
					{
						{
						match(LITERAL_from);
						AST tmp72_AST = null;
						tmp72_AST = astFactory.create(LT(1));
						astFactory.addASTChild(currentAST, tmp72_AST);
						match(REAL);
						}
						{
						switch ( LA(1)) {
						case LITERAL_to:
						{
							match(LITERAL_to);
							AST tmp74_AST = null;
							tmp74_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp74_AST);
							match(REAL);
							break;
						}
						case IF:
						case RPAREN:
						case ASSIGN:
						case 70:
						{
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						break;
					}
					case IF:
					case RPAREN:
					case ASSIGN:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_discretizedBy,"discretizedBy")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_restrictedToTrailing:
				{
					match(LITERAL_restrictedToTrailing);
					number1();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_restrictedToTrailing,"restrictedToTrailing")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_delayedFor:
				{
					match(LITERAL_delayedFor);
					{
					switch ( LA(1)) {
					case INT:
					{
						AST tmp77_AST = null;
						tmp77_AST = astFactory.create(LT(1));
						astFactory.addASTChild(currentAST, tmp77_AST);
						match(INT);
						break;
					}
					case LITERAL_uniform:
					{
						proba();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case LITERAL_on:
					{
						match(LITERAL_on);
						clockRef();
						astFactory.addASTChild(currentAST, returnAST);
						break;
					}
					case IF:
					case RPAREN:
					case ASSIGN:
					case 70:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_delayedFor,"delayedFor")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_followedBy:
				{
					match(LITERAL_followedBy);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_followedBy,"followedBy")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_inter:
				{
					match(LITERAL_inter);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_inter,"inter")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_union:
				{
					match(LITERAL_union);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_union,"union")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_minus:
				{
					match(LITERAL_minus);
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_minus,"minus")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case STRICTLY:
				case LITERAL_sampledOn:
				case LITERAL_leftWeakly:
				case LITERAL_rightWeakly:
				case LITERAL_weakly:
				case LITERAL_nonStrictly:
				case LITERAL_leftNonStrictly:
				case LITERAL_rightNonStrictly:
				{
					moderator();
					astFactory.addASTChild(currentAST, returnAST);
					{
					match(LITERAL_sampledOn);
					}
					clockRef();
					astFactory.addASTChild(currentAST, returnAST);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_sampledOn,"sampledOn")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_restrictedToLeading:
				{
					match(LITERAL_restrictedToLeading);
					AST tmp85_AST = null;
					tmp85_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp85_AST);
					match(INT);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_restrictedToLeading,"restrictedToLeading")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case LITERAL_restrictedToInterval:
				{
					match(LITERAL_restrictedToInterval);
					match(LINTER);
					AST tmp88_AST = null;
					tmp88_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp88_AST);
					match(INT);
					match(80);
					AST tmp90_AST = null;
					tmp90_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp90_AST);
					match(INT);
					match(RINTER);
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_restrictedToInterval,"restrictedToInterval")).add(id1_AST));
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				case IF:
				case RPAREN:
				case ASSIGN:
				case 70:
				{
					if ( inputState.guessing==0 ) {
						clockExpr_AST = (AST)currentAST.root;
						clockExpr_AST =id1_AST /* #([CLK_FUNC2,"CLK_FUNC2"],#id1)*/ ;
						currentAST.root = clockExpr_AST;
						currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
							clockExpr_AST.getFirstChild() : clockExpr_AST;
						currentAST.advanceChildToEnd();
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_when:
			{
				match(LITERAL_when);
				eventid();
				id4_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_when,"when")).add(id4_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_timerConstraint:
			{
				match(LITERAL_timerConstraint);
				clockRef();
				id5_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				{
				switch ( LA(1)) {
				case INT:
				{
					AST tmp98_AST = null;
					tmp98_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp98_AST);
					match(INT);
					break;
				}
				case LITERAL_uniform:
				{
					proba();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_timerConstraint,"timerConstraint")).add(id5_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_oneShotConstraint:
			{
				match(LITERAL_oneShotConstraint);
				clockRef();
				id6_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				{
				switch ( LA(1)) {
				case INT:
				{
					AST tmp103_AST = null;
					tmp103_AST = astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp103_AST);
					match(INT);
					break;
				}
				case LITERAL_uniform:
				{
					proba();
					astFactory.addASTChild(currentAST, returnAST);
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_oneShotConstraint,"oneShotConstraint")).add(id6_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_sup:
			{
				AST tmp104_AST = null;
				tmp104_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp104_AST);
				match(LITERAL_sup);
				match(LPAREN);
				clockRef();
				id7_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				{
				int _cnt150=0;
				_loop150:
				do {
					if ((LA(1)==71)) {
						match(71);
						clockRef();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt150>=1 ) { break _loop150; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt150++;
				} while (true);
				}
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_sup,"sup")).add(id7_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_inf:
			{
				AST tmp108_AST = null;
				tmp108_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp108_AST);
				match(LITERAL_inf);
				match(LPAREN);
				clockRef();
				id8_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				{
				int _cnt152=0;
				_loop152:
				do {
					if ((LA(1)==71)) {
						match(71);
						clockRef();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt152>=1 ) { break _loop152; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt152++;
				} while (true);
				}
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_inf,"inf")).add(id8_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_sustain:
			{
				AST tmp112_AST = null;
				tmp112_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp112_AST);
				match(LITERAL_sustain);
				clockRef();
				id9_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(LITERAL_upto);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					clockExpr_AST = (AST)currentAST.root;
					clockExpr_AST = (AST)astFactory.make( (new ASTArray(3)).add(astFactory.create(CLK_FUNC,"CLK_FUNC")).add(astFactory.create(LITERAL_sustain,"sustain")).add(id9_AST));
					currentAST.root = clockExpr_AST;
					currentAST.child = clockExpr_AST!=null &&clockExpr_AST.getFirstChild()!=null ?
						clockExpr_AST.getFirstChild() : clockExpr_AST;
					currentAST.advanceChildToEnd();
				}
				clockExpr_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_40);
			} else {
			  throw ex;
			}
		}
		returnAST = clockExpr_AST;
	}
	
/*********************************************************************
*
*	base commune
*
**********************************************************************/
	public final void pathe() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST pathe_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case ID:
			{
				AST tmp114_AST = null;
				tmp114_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp114_AST);
				match(ID);
				pathe_AST = (AST)currentAST.root;
				break;
			}
			case PATH:
			{
				AST tmp115_AST = null;
				tmp115_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp115_AST);
				match(PATH);
				pathe_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_41);
			} else {
			  throw ex;
			}
		}
		returnAST = pathe_AST;
	}
	
	public final void unitId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST unitId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getUnitdic().putVar(v_AST.getText()) ;
			}
			unitId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_42);
			} else {
			  throw ex;
			}
		}
		returnAST = unitId_AST;
	}
	
	public final void clockId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getClkdic().putVar(v_AST.getText());
			}
			clockId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_43);
			} else {
			  throw ex;
			}
		}
		returnAST = clockId_AST;
	}
	
	public final void clockId2() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST clockId2_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				tampon=dico2.getClkdic().putVar(v_AST.getText()); tampon.DefLocal();
			}
			clockId2_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_44);
			} else {
			  throw ex;
			}
		}
		returnAST = clockId2_AST;
	}
	
	public final void timerId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timerId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getClkdic().putVar(v_AST.getText());
			}
			timerId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = timerId_AST;
	}
	
	public final void timerId2() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timerId2_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				tampon=dico2.getClkdic().putVar(v_AST.getText()); tampon.DefLocal();
			}
			timerId2_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_45);
			} else {
			  throw ex;
			}
		}
		returnAST = timerId2_AST;
	}
	
	public final void instantId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getInstantdic().putVar(v_AST.getText());
			}
			instantId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_46);
			} else {
			  throw ex;
			}
		}
		returnAST = instantId_AST;
	}
	
	public final void instantId2() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantId2_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				tampon=dico2.getInstantdic().putVar(v_AST.getText()); tampon.DefLocal();
			}
			instantId2_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_44);
			} else {
			  throw ex;
			}
		}
		returnAST = instantId2_AST;
	}
	
	public final void numberId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST numberId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getNumberdic().putVar(v_AST.getText());
			}
			numberId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_47);
			} else {
			  throw ex;
			}
		}
		returnAST = numberId_AST;
	}
	
	public final void booleanId() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST booleanId_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getBooldic().putVar(v_AST.getText());
			}
			booleanId_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_48);
			} else {
			  throw ex;
			}
		}
		returnAST = booleanId_AST;
	}
	
	public final void eventid() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST eventid_AST = null;
		AST v_AST = null;
		
		try {      // for error handling
			pathe();
			v_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				dico2.getEventdic().putVar(v_AST.getText());
			}
			eventid_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_40);
			} else {
			  throw ex;
			}
		}
		returnAST = eventid_AST;
	}
	
	public final void interv() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST interv_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LINTER:
			{
				AST tmp116_AST = null;
				tmp116_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp116_AST);
				match(LINTER);
				interv_AST = (AST)currentAST.root;
				break;
			}
			case RINTER:
			{
				AST tmp117_AST = null;
				tmp117_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp117_AST);
				match(RINTER);
				interv_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_49);
			} else {
			  throw ex;
			}
		}
		returnAST = interv_AST;
	}
	
	public final void interpp() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST interpp_AST = null;
		
		try {      // for error handling
			AST tmp118_AST = null;
			tmp118_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp118_AST);
			match(80);
			interpp_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_50);
			} else {
			  throw ex;
			}
		}
		returnAST = interpp_AST;
	}
	
	public final void prefixop() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST prefixop_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_max:
			{
				AST tmp119_AST = null;
				tmp119_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp119_AST);
				match(LITERAL_max);
				prefixop_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_min:
			{
				AST tmp120_AST = null;
				tmp120_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp120_AST);
				match(LITERAL_min);
				prefixop_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_51);
			} else {
			  throw ex;
			}
		}
		returnAST = prefixop_AST;
	}
	
/*********************************************************************
*
* Binary word
*
**********************************************************************/
	public final void bwexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST bwexpr_AST = null;
		Token  bwe = null;
		AST bwe_AST = null;
		Token  gbwe = null;
		AST gbwe_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case BW:
			{
				bwe = LT(1);
				bwe_AST = astFactory.create(bwe);
				astFactory.addASTChild(currentAST, bwe_AST);
				match(BW);
				if ( inputState.guessing==0 ) {
					bwexpr_AST = (AST)currentAST.root;
					bwexpr_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(BWEXPR,"BWEXPR_BW")).add(bwexpr_AST));
					currentAST.root = bwexpr_AST;
					currentAST.child = bwexpr_AST!=null &&bwexpr_AST.getFirstChild()!=null ?
						bwexpr_AST.getFirstChild() : bwexpr_AST;
					currentAST.advanceChildToEnd();
				}
				bwexpr_AST = (AST)currentAST.root;
				break;
			}
			case GBW:
			{
				gbwe = LT(1);
				gbwe_AST = astFactory.create(gbwe);
				astFactory.addASTChild(currentAST, gbwe_AST);
				match(GBW);
				if ( inputState.guessing==0 ) {
					bwexpr_AST = (AST)currentAST.root;
					bwexpr_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(BWEXPR,"BWEXPR_GBW")).add(bwexpr_AST));
					currentAST.root = bwexpr_AST;
					currentAST.child = bwexpr_AST!=null &&bwexpr_AST.getFirstChild()!=null ?
						bwexpr_AST.getFirstChild() : bwexpr_AST;
					currentAST.advanceChildToEnd();
				}
				bwexpr_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_40);
			} else {
			  throw ex;
			}
		}
		returnAST = bwexpr_AST;
	}
	
/*********************************************************************
*
* Nombre reel COMMENT
*
**********************************************************************/
	public final void realexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST realexpr_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LINTER:
			case RINTER:
			{
				interreal();
				astFactory.addASTChild(currentAST, returnAST);
				realexpr_AST = (AST)currentAST.root;
				break;
			}
			case INT:
			case REAL:
			case LPAREN:
			case ID:
			case PATH:
			case 86:
			case 87:
			{
				number1();
				astFactory.addASTChild(currentAST, returnAST);
				realexpr_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = realexpr_AST;
	}
	
	public final void interreal() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST interreal_AST = null;
		
		try {      // for error handling
			{
			interv();
			astFactory.addASTChild(currentAST, returnAST);
			}
			realint();
			astFactory.addASTChild(currentAST, returnAST);
			match(80);
			realint();
			astFactory.addASTChild(currentAST, returnAST);
			{
			interv();
			astFactory.addASTChild(currentAST, returnAST);
			}
			if ( inputState.guessing==0 ) {
				interreal_AST = (AST)currentAST.root;
				interreal_AST= (AST)astFactory.make( (new ASTArray(1)).add(astFactory.create(INTERVALREAL,"INTERVALREAL"))) ;
				currentAST.root = interreal_AST;
				currentAST.child = interreal_AST!=null &&interreal_AST.getFirstChild()!=null ?
					interreal_AST.getFirstChild() : interreal_AST;
				currentAST.advanceChildToEnd();
			}
			interreal_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = interreal_AST;
	}
	
/*********************************************************************
*
*	Expression numerique
*
**********************************************************************/
	public final void number1() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST number1_AST = null;
		AST k_AST = null;
		
		try {      // for error handling
			number2();
			k_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case 86:
			case 87:
			{
				{
				int _cnt52=0;
				_loop52:
				do {
					if ((LA(1)==86||LA(1)==87)) {
						{
						switch ( LA(1)) {
						case 86:
						{
							AST tmp122_AST = null;
							tmp122_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp122_AST);
							match(86);
							break;
						}
						case 87:
						{
							AST tmp123_AST = null;
							tmp123_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp123_AST);
							match(87);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						number2();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt52>=1 ) { break _loop52; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt52++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					number1_AST = (AST)currentAST.root;
					number1_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EXPRADD,"EXPRADD")).add(k_AST)) ;
					currentAST.root = number1_AST;
					currentAST.child = number1_AST!=null &&number1_AST.getFirstChild()!=null ?
						number1_AST.getFirstChild() : number1_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case EOF:
			case IF:
			case RPAREN:
			case RINTER:
			case ASSIGN:
			case STRICTLY:
			case 70:
			case 71:
			case LITERAL_coincidentWith:
			case LITERAL_precedes:
			case LITERAL_or:
			case LITERAL_xor:
			case LITERAL_and:
			case RELOP:
			case LITERAL_in:
			case LITERAL_on:
			case LITERAL_from:
			case LITERAL_wrt:
			case LITERAL_at:
			case LITERAL_leftWeakly:
			case LITERAL_rightWeakly:
			case LITERAL_weakly:
			case LITERAL_nonStrictly:
			case LITERAL_leftNonStrictly:
			case LITERAL_rightNonStrictly:
			case LITERAL_alternatesWith:
			case LITERAL_synchronizesWith:
			case LITERAL_offset:
			case LITERAL_iprecedes:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			number1_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_52);
			} else {
			  throw ex;
			}
		}
		returnAST = number1_AST;
	}
	
	public final void realint() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST realint_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case REAL:
			{
				AST tmp124_AST = null;
				tmp124_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp124_AST);
				match(REAL);
				realint_AST = (AST)currentAST.root;
				break;
			}
			case INT:
			{
				AST tmp125_AST = null;
				tmp125_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp125_AST);
				match(INT);
				realint_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_53);
			} else {
			  throw ex;
			}
		}
		returnAST = realint_AST;
	}
	
/*********************************************************************
*
* proba
*
**********************************************************************/
	public final void proba() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST proba_AST = null;
		Token  id1 = null;
		AST id1_AST = null;
		
		try {      // for error handling
			match(LITERAL_uniform);
			match(LPAREN);
			id1 = LT(1);
			id1_AST = astFactory.create(id1);
			astFactory.addASTChild(currentAST, id1_AST);
			match(INT);
			match(71);
			AST tmp129_AST = null;
			tmp129_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp129_AST);
			match(INT);
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				proba_AST = (AST)currentAST.root;
				proba_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LITERAL_uniform,"uniform")).add(id1_AST)) ;
				currentAST.root = proba_AST;
				currentAST.child = proba_AST!=null &&proba_AST.getFirstChild()!=null ?
					proba_AST.getFirstChild() : proba_AST;
				currentAST.advanceChildToEnd();
			}
			proba_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_54);
			} else {
			  throw ex;
			}
		}
		returnAST = proba_AST;
	}
	
	public final void number2() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST number2_AST = null;
		AST k_AST = null;
		
		try {      // for error handling
			number3();
			k_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case 88:
			case 89:
			case 90:
			{
				{
				int _cnt57=0;
				_loop57:
				do {
					if (((LA(1) >= 88 && LA(1) <= 90))) {
						{
						switch ( LA(1)) {
						case 88:
						{
							AST tmp131_AST = null;
							tmp131_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp131_AST);
							match(88);
							break;
						}
						case 89:
						{
							AST tmp132_AST = null;
							tmp132_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp132_AST);
							match(89);
							break;
						}
						case 90:
						{
							AST tmp133_AST = null;
							tmp133_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp133_AST);
							match(90);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						number3();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt57>=1 ) { break _loop57; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt57++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					number2_AST = (AST)currentAST.root;
					number2_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EXPRMUL,"EXPRMUL")).add(k_AST)) ;
					currentAST.root = number2_AST;
					currentAST.child = number2_AST!=null &&number2_AST.getFirstChild()!=null ?
						number2_AST.getFirstChild() : number2_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case EOF:
			case IF:
			case RPAREN:
			case RINTER:
			case ASSIGN:
			case STRICTLY:
			case 70:
			case 71:
			case LITERAL_coincidentWith:
			case LITERAL_precedes:
			case 86:
			case 87:
			case LITERAL_or:
			case LITERAL_xor:
			case LITERAL_and:
			case RELOP:
			case LITERAL_in:
			case LITERAL_on:
			case LITERAL_from:
			case LITERAL_wrt:
			case LITERAL_at:
			case LITERAL_leftWeakly:
			case LITERAL_rightWeakly:
			case LITERAL_weakly:
			case LITERAL_nonStrictly:
			case LITERAL_leftNonStrictly:
			case LITERAL_rightNonStrictly:
			case LITERAL_alternatesWith:
			case LITERAL_synchronizesWith:
			case LITERAL_offset:
			case LITERAL_iprecedes:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			number2_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		returnAST = number2_AST;
	}
	
	public final void number3() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST number3_AST = null;
		AST k1_AST = null;
		AST k_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case 86:
			case 87:
			{
				{
				signe();
				k1_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				realint();
				astFactory.addASTChild(currentAST, returnAST);
				}
				if ( inputState.guessing==0 ) {
					number3_AST = (AST)currentAST.root;
					number3_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EXPRNUMBER,"number")).add(k1_AST)) ;
					currentAST.root = number3_AST;
					currentAST.child = number3_AST!=null &&number3_AST.getFirstChild()!=null ?
						number3_AST.getFirstChild() : number3_AST;
					currentAST.advanceChildToEnd();
				}
				number3_AST = (AST)currentAST.root;
				break;
			}
			case INT:
			case REAL:
			{
				realint();
				astFactory.addASTChild(currentAST, returnAST);
				number3_AST = (AST)currentAST.root;
				break;
			}
			case LPAREN:
			{
				{
				match(LPAREN);
				number1();
				k_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(RPAREN);
				}
				if ( inputState.guessing==0 ) {
					number3_AST = (AST)currentAST.root;
					number3_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGPAR,"( )")).add(k_AST)) ;
					currentAST.root = number3_AST;
					currentAST.child = number3_AST!=null &&number3_AST.getFirstChild()!=null ?
						number3_AST.getFirstChild() : number3_AST;
					currentAST.advanceChildToEnd();
				}
				number3_AST = (AST)currentAST.root;
				break;
			}
			case ID:
			case PATH:
			{
				numberId();
				astFactory.addASTChild(currentAST, returnAST);
				number3_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_47);
			} else {
			  throw ex;
			}
		}
		returnAST = number3_AST;
	}
	
	public final void signe() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST signe_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case 86:
			{
				AST tmp136_AST = null;
				tmp136_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp136_AST);
				match(86);
				signe_AST = (AST)currentAST.root;
				break;
			}
			case 87:
			{
				AST tmp137_AST = null;
				tmp137_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp137_AST);
				match(87);
				signe_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_56);
			} else {
			  throw ex;
			}
		}
		returnAST = signe_AST;
	}
	
/*********************************************************************
*
*	Expression logique
*
**********************************************************************/
	public final void boolexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			boolexpr1();
			l_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				boolexpr_AST = (AST)currentAST.root;
				boolexpr_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOG,"logique")).add(l_AST)) ;
				currentAST.root = boolexpr_AST;
				currentAST.child = boolexpr_AST!=null &&boolexpr_AST.getFirstChild()!=null ?
					boolexpr_AST.getFirstChild() : boolexpr_AST;
				currentAST.advanceChildToEnd();
			}
			boolexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_18);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr_AST;
	}
	
	public final void boolexpr2() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr2_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			boolexpr3();
			l_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_xor:
			{
				{
				int _cnt70=0;
				_loop70:
				do {
					if ((LA(1)==LITERAL_xor)) {
						match(LITERAL_xor);
						boolexpr3();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt70>=1 ) { break _loop70; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt70++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					boolexpr2_AST = (AST)currentAST.root;
					boolexpr2_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGXOR,"XOR")).add(l_AST)) ;
					currentAST.root = boolexpr2_AST;
					currentAST.child = boolexpr2_AST!=null &&boolexpr2_AST.getFirstChild()!=null ?
						boolexpr2_AST.getFirstChild() : boolexpr2_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case IF:
			case RPAREN:
			case ASSIGN:
			case STRICTLY:
			case 70:
			case 71:
			case LITERAL_coincidentWith:
			case LITERAL_or:
			case LITERAL_weakly:
			case LITERAL_iprecedes:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			boolexpr2_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_57);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr2_AST;
	}
	
	public final void boolexpr3() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr3_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			boolexpr4();
			l_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_and:
			{
				{
				int _cnt74=0;
				_loop74:
				do {
					if ((LA(1)==LITERAL_and)) {
						match(LITERAL_and);
						boolexpr4();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt74>=1 ) { break _loop74; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt74++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					boolexpr3_AST = (AST)currentAST.root;
					boolexpr3_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGAND,"AND")).add(l_AST)) ;
					currentAST.root = boolexpr3_AST;
					currentAST.child = boolexpr3_AST!=null &&boolexpr3_AST.getFirstChild()!=null ?
						boolexpr3_AST.getFirstChild() : boolexpr3_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case IF:
			case RPAREN:
			case ASSIGN:
			case STRICTLY:
			case 70:
			case 71:
			case LITERAL_coincidentWith:
			case LITERAL_or:
			case LITERAL_xor:
			case LITERAL_weakly:
			case LITERAL_iprecedes:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			boolexpr3_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_58);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr3_AST;
	}
	
	public final void boolexpr4() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr4_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_not:
			{
				{
				AST tmp140_AST = null;
				tmp140_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp140_AST);
				match(LITERAL_not);
				boolexpr4();
				l_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				}
				if ( inputState.guessing==0 ) {
					boolexpr4_AST = (AST)currentAST.root;
					boolexpr4_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGNOT,"not")).add(l_AST)) ;
					currentAST.root = boolexpr4_AST;
					currentAST.child = boolexpr4_AST!=null &&boolexpr4_AST.getFirstChild()!=null ?
						boolexpr4_AST.getFirstChild() : boolexpr4_AST;
					currentAST.advanceChildToEnd();
				}
				boolexpr4_AST = (AST)currentAST.root;
				break;
			}
			case INT:
			case REAL:
			case LPAREN:
			case ID:
			case PATH:
			case 86:
			case 87:
			case LITERAL_true:
			case LITERAL_false:
			{
				boolexpr5();
				astFactory.addASTChild(currentAST, returnAST);
				boolexpr4_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_48);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr4_AST;
	}
	
	public final void boolexpr5() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolexpr5_AST = null;
		AST c_AST = null;
		AST l_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_true:
			{
				AST tmp141_AST = null;
				tmp141_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp141_AST);
				match(LITERAL_true);
				boolexpr5_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_false:
			{
				AST tmp142_AST = null;
				tmp142_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp142_AST);
				match(LITERAL_false);
				boolexpr5_AST = (AST)currentAST.root;
				break;
			}
			default:
				boolean synPredMatched79 = false;
				if (((_tokenSet_59.member(LA(1))) && (_tokenSet_60.member(LA(2))) && (_tokenSet_61.member(LA(3))) && (_tokenSet_62.member(LA(4))))) {
					int _m79 = mark();
					synPredMatched79 = true;
					inputState.guessing++;
					try {
						{
						number1();
						relOp();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched79 = false;
					}
					rewind(_m79);
inputState.guessing--;
				}
				if ( synPredMatched79 ) {
					{
					compa();
					c_AST = (AST)returnAST;
					astFactory.addASTChild(currentAST, returnAST);
					}
					if ( inputState.guessing==0 ) {
						boolexpr5_AST = (AST)currentAST.root;
						boolexpr5_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGCOMP,"COMPA")).add(c_AST)) ;
						currentAST.root = boolexpr5_AST;
						currentAST.child = boolexpr5_AST!=null &&boolexpr5_AST.getFirstChild()!=null ?
							boolexpr5_AST.getFirstChild() : boolexpr5_AST;
						currentAST.advanceChildToEnd();
					}
					boolexpr5_AST = (AST)currentAST.root;
				}
				else if ((LA(1)==LPAREN) && (_tokenSet_63.member(LA(2))) && (_tokenSet_64.member(LA(3))) && (_tokenSet_65.member(LA(4)))) {
					{
					match(LPAREN);
					boolexpr1();
					l_AST = (AST)returnAST;
					astFactory.addASTChild(currentAST, returnAST);
					match(RPAREN);
					}
					if ( inputState.guessing==0 ) {
						boolexpr5_AST = (AST)currentAST.root;
						boolexpr5_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(LOGPAR,"( )")).add(l_AST)) ;
						currentAST.root = boolexpr5_AST;
						currentAST.child = boolexpr5_AST!=null &&boolexpr5_AST.getFirstChild()!=null ?
							boolexpr5_AST.getFirstChild() : boolexpr5_AST;
						currentAST.advanceChildToEnd();
					}
					boolexpr5_AST = (AST)currentAST.root;
				}
				else if ((LA(1)==ID||LA(1)==PATH) && (_tokenSet_48.member(LA(2))) && (_tokenSet_66.member(LA(3))) && (_tokenSet_67.member(LA(4)))) {
					booleanId();
					astFactory.addASTChild(currentAST, returnAST);
					boolexpr5_AST = (AST)currentAST.root;
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_48);
			} else {
			  throw ex;
			}
		}
		returnAST = boolexpr5_AST;
	}
	
	public final void relOp() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST relOp_AST = null;
		Token  r = null;
		AST r_AST = null;
		Token  a = null;
		AST a_AST = null;
		Token  i = null;
		AST i_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case RELOP:
			{
				r = LT(1);
				r_AST = astFactory.create(r);
				astFactory.addASTChild(currentAST, r_AST);
				match(RELOP);
				if ( inputState.guessing==0 ) {
					relOp_AST = (AST)currentAST.root;
					relOp_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EGOP,"EGOP")).add(r_AST));
					currentAST.root = relOp_AST;
					currentAST.child = relOp_AST!=null &&relOp_AST.getFirstChild()!=null ?
						relOp_AST.getFirstChild() : relOp_AST;
					currentAST.advanceChildToEnd();
				}
				relOp_AST = (AST)currentAST.root;
				break;
			}
			case ASSIGN:
			{
				a = LT(1);
				a_AST = astFactory.create(a);
				astFactory.addASTChild(currentAST, a_AST);
				match(ASSIGN);
				if ( inputState.guessing==0 ) {
					relOp_AST = (AST)currentAST.root;
					relOp_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EGOP,"EGOP")).add(a_AST));
					currentAST.root = relOp_AST;
					currentAST.child = relOp_AST!=null &&relOp_AST.getFirstChild()!=null ?
						relOp_AST.getFirstChild() : relOp_AST;
					currentAST.advanceChildToEnd();
				}
				relOp_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_in:
			{
				i = LT(1);
				i_AST = astFactory.create(i);
				astFactory.addASTChild(currentAST, i_AST);
				match(LITERAL_in);
				if ( inputState.guessing==0 ) {
					relOp_AST = (AST)currentAST.root;
					relOp_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EGOP,"EGOP")).add(i_AST));
					currentAST.root = relOp_AST;
					currentAST.child = relOp_AST!=null &&relOp_AST.getFirstChild()!=null ?
						relOp_AST.getFirstChild() : relOp_AST;
					currentAST.advanceChildToEnd();
				}
				relOp_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_59);
			} else {
			  throw ex;
			}
		}
		returnAST = relOp_AST;
	}
	
/*******************************************************************
*
*	Comparaison
*
*
*********************************************************************/
	public final void compa() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST compa_AST = null;
		
		try {      // for error handling
			{
			number1();
			astFactory.addASTChild(currentAST, returnAST);
			}
			relOp();
			astFactory.addASTChild(currentAST, returnAST);
			{
			number1();
			astFactory.addASTChild(currentAST, returnAST);
			}
			compa_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_48);
			} else {
			  throw ex;
			}
		}
		returnAST = compa_AST;
	}
	
/*********************************************************************
*
*   Clocked/Timed Value Specification
*
*
*********************************************************************/
	public final void extend() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST extend_AST = null;
		
		try {      // for error handling
			unitId();
			astFactory.addASTChild(currentAST, returnAST);
			match(LITERAL_on);
			clockId();
			astFactory.addASTChild(currentAST, returnAST);
			extend_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = extend_AST;
	}
	
	public final void durationexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST durationexpr_AST = null;
		AST d_AST = null;
		
		try {      // for error handling
			diexpr();
			d_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				durationexpr_AST = (AST)currentAST.root;
				durationexpr_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(DUREXPR,"DUREXPR")).add(d_AST)) ;
				currentAST.root = durationexpr_AST;
				currentAST.child = durationexpr_AST!=null &&durationexpr_AST.getFirstChild()!=null ?
					durationexpr_AST.getFirstChild() : durationexpr_AST;
				currentAST.advanceChildToEnd();
			}
			durationexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_69);
			} else {
			  throw ex;
			}
		}
		returnAST = durationexpr_AST;
	}
	
	public final void diexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST diexpr_AST = null;
		AST t_AST = null;
		AST t2_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case MYLCURLY:
			{
				{
				match(MYLCURLY);
				time();
				t_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(MYRCURLY);
				extend();
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					diexpr_AST = (AST)currentAST.root;
					diexpr_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEEXT,"TIMEEXT")).add(t_AST)) ;
							//System.out.println("ici2" +#t) ;
							
					currentAST.root = diexpr_AST;
					currentAST.child = diexpr_AST!=null &&diexpr_AST.getFirstChild()!=null ?
						diexpr_AST.getFirstChild() : diexpr_AST;
					currentAST.advanceChildToEnd();
				}
				}
				diexpr_AST = (AST)currentAST.root;
				break;
			}
			case INT:
			case REAL:
			case LPAREN:
			case LINTER:
			case RINTER:
			case ID:
			case PATH:
			case LITERAL_max:
			case LITERAL_min:
			case LITERAL_mult:
			case LITERAL_durationBetween:
			{
				{
				time();
				t2_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					diexpr_AST = (AST)currentAST.root;
					
							//System.out.println("ici" +#t2);
							diexpr_AST= t2_AST;
							
					currentAST.root = diexpr_AST;
					currentAST.child = diexpr_AST!=null &&diexpr_AST.getFirstChild()!=null ?
						diexpr_AST.getFirstChild() : diexpr_AST;
					currentAST.advanceChildToEnd();
				}
				}
				diexpr_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_69);
			} else {
			  throw ex;
			}
		}
		returnAST = diexpr_AST;
	}
	
	public final void instantexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST instantexpr_AST = null;
		AST d_AST = null;
		
		try {      // for error handling
			diexpr();
			d_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				instantexpr_AST = (AST)currentAST.root;
				instantexpr_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(INSTEXPR,"INSTEXPR")).add(d_AST)) ;
				currentAST.root = instantexpr_AST;
				currentAST.child = instantexpr_AST!=null &&instantexpr_AST.getFirstChild()!=null ?
					instantexpr_AST.getFirstChild() : instantexpr_AST;
				currentAST.advanceChildToEnd();
			}
			instantexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = instantexpr_AST;
	}
	
/*********************************************************
*
*   DURATION /INSTANT
*
*
*********************************************************/
	public final void time() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST time_AST = null;
		AST n_AST = null;
		
		try {      // for error handling
			timeterm();
			n_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case 86:
			case 87:
			{
				{
				int _cnt96=0;
				_loop96:
				do {
					if ((LA(1)==86||LA(1)==87)) {
						{
						switch ( LA(1)) {
						case 86:
						{
							AST tmp148_AST = null;
							tmp148_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp148_AST);
							match(86);
							break;
						}
						case 87:
						{
							AST tmp149_AST = null;
							tmp149_AST = astFactory.create(LT(1));
							astFactory.addASTChild(currentAST, tmp149_AST);
							match(87);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						timeterm();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						if ( _cnt96>=1 ) { break _loop96; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt96++;
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					time_AST = (AST)currentAST.root;
					time_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMESOMME,"TIMESOMME")).add(n_AST)) ;
					currentAST.root = time_AST;
					currentAST.child = time_AST!=null &&time_AST.getFirstChild()!=null ?
						time_AST.getFirstChild() : time_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case IF:
			case MYRCURLY:
			case RPAREN:
			case LINTER:
			case RINTER:
			case 70:
			case 71:
			case 80:
			case LITERAL_wrt:
			case LITERAL_at:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			time_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_70);
			} else {
			  throw ex;
			}
		}
		returnAST = time_AST;
	}
	
	public final void timeterm() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timeterm_AST = null;
		AST f_AST = null;
		AST n_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case INT:
			case REAL:
			case LPAREN:
			case LINTER:
			case RINTER:
			case ID:
			case PATH:
			case LITERAL_max:
			case LITERAL_min:
			case LITERAL_durationBetween:
			{
				{
				timefactor();
				f_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				if ( inputState.guessing==0 ) {
					timeterm_AST = (AST)currentAST.root;
					timeterm_AST=f_AST;
					currentAST.root = timeterm_AST;
					currentAST.child = timeterm_AST!=null &&timeterm_AST.getFirstChild()!=null ?
						timeterm_AST.getFirstChild() : timeterm_AST;
					currentAST.advanceChildToEnd();
				}
				}
				timeterm_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_mult:
			{
				{
				match(LITERAL_mult);
				match(LPAREN);
				number1();
				n_AST = (AST)returnAST;
				astFactory.addASTChild(currentAST, returnAST);
				match(71);
				timefactor();
				astFactory.addASTChild(currentAST, returnAST);
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					timeterm_AST = (AST)currentAST.root;
					timeterm_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEMUL,"TIMEMUL")).add(n_AST)) ;
					currentAST.root = timeterm_AST;
					currentAST.child = timeterm_AST!=null &&timeterm_AST.getFirstChild()!=null ?
						timeterm_AST.getFirstChild() : timeterm_AST;
					currentAST.advanceChildToEnd();
				}
				}
				timeterm_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = timeterm_AST;
	}
	
	public final void timefactor() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timefactor_AST = null;
		AST n1_AST = null;
		AST n_AST = null;
		AST n2_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_max:
			case LITERAL_min:
			{
				timefunc();
				astFactory.addASTChild(currentAST, returnAST);
				timefactor_AST = (AST)currentAST.root;
				break;
			}
			case LINTER:
			case RINTER:
			{
				timeInterval();
				astFactory.addASTChild(currentAST, returnAST);
				timefactor_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_durationBetween:
			{
				timespan();
				astFactory.addASTChild(currentAST, returnAST);
				timefactor_AST = (AST)currentAST.root;
				break;
			}
			default:
				boolean synPredMatched104 = false;
				if (((LA(1)==LPAREN) && (LA(2)==ID||LA(2)==PATH) && (LA(3)==ASSIGN))) {
					int _m104 = mark();
					synPredMatched104 = true;
					inputState.guessing++;
					try {
						{
						tuple();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched104 = false;
					}
					rewind(_m104);
inputState.guessing--;
				}
				if ( synPredMatched104 ) {
					tuple();
					astFactory.addASTChild(currentAST, returnAST);
					timefactor_AST = (AST)currentAST.root;
				}
				else {
					boolean synPredMatched106 = false;
					if (((LA(1)==LPAREN) && (_tokenSet_59.member(LA(2))) && (_tokenSet_71.member(LA(3))) && (_tokenSet_71.member(LA(4))))) {
						int _m106 = mark();
						synPredMatched106 = true;
						inputState.guessing++;
						try {
							{
							match(LPAREN);
							number2();
							match(RPAREN);
							unitId();
							}
						}
						catch (RecognitionException pe) {
							synPredMatched106 = false;
						}
						rewind(_m106);
inputState.guessing--;
					}
					if ( synPredMatched106 ) {
						{
						match(LPAREN);
						number2();
						n1_AST = (AST)returnAST;
						astFactory.addASTChild(currentAST, returnAST);
						match(RPAREN);
						extend();
						astFactory.addASTChild(currentAST, returnAST);
						if ( inputState.guessing==0 ) {
							timefactor_AST = (AST)currentAST.root;
							timefactor_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEEXT,"TIMEEXT")).add(n1_AST)) ;
							currentAST.root = timefactor_AST;
							currentAST.child = timefactor_AST!=null &&timefactor_AST.getFirstChild()!=null ?
								timefactor_AST.getFirstChild() : timefactor_AST;
							currentAST.advanceChildToEnd();
						}
						}
						timefactor_AST = (AST)currentAST.root;
					}
					else {
						boolean synPredMatched109 = false;
						if (((LA(1)==LPAREN) && (_tokenSet_59.member(LA(2))) && (_tokenSet_71.member(LA(3))) && (_tokenSet_72.member(LA(4))))) {
							int _m109 = mark();
							synPredMatched109 = true;
							inputState.guessing++;
							try {
								{
								match(LPAREN);
								number2();
								}
							}
							catch (RecognitionException pe) {
								synPredMatched109 = false;
							}
							rewind(_m109);
inputState.guessing--;
						}
						if ( synPredMatched109 ) {
							{
							match(LPAREN);
							number2();
							astFactory.addASTChild(currentAST, returnAST);
							match(RPAREN);
							}
							timefactor_AST = (AST)currentAST.root;
						}
						else {
							boolean synPredMatched112 = false;
							if (((LA(1)==LPAREN) && (_tokenSet_50.member(LA(2))) && (_tokenSet_73.member(LA(3))) && (_tokenSet_74.member(LA(4))))) {
								int _m112 = mark();
								synPredMatched112 = true;
								inputState.guessing++;
								try {
									{
									match(LPAREN);
									}
								}
								catch (RecognitionException pe) {
									synPredMatched112 = false;
								}
								rewind(_m112);
inputState.guessing--;
							}
							if ( synPredMatched112 ) {
								{
								AST tmp158_AST = null;
								tmp158_AST = astFactory.create(LT(1));
								astFactory.addASTChild(currentAST, tmp158_AST);
								match(LPAREN);
								time();
								astFactory.addASTChild(currentAST, returnAST);
								AST tmp159_AST = null;
								tmp159_AST = astFactory.create(LT(1));
								astFactory.addASTChild(currentAST, tmp159_AST);
								match(RPAREN);
								}
								timefactor_AST = (AST)currentAST.root;
							}
							else {
								boolean synPredMatched115 = false;
								if (((LA(1)==ID||LA(1)==PATH) && (_tokenSet_68.member(LA(2))))) {
									int _m115 = mark();
									synPredMatched115 = true;
									inputState.guessing++;
									try {
										{
										pathe();
										}
									}
									catch (RecognitionException pe) {
										synPredMatched115 = false;
									}
									rewind(_m115);
inputState.guessing--;
								}
								if ( synPredMatched115 ) {
									instantId();
									astFactory.addASTChild(currentAST, returnAST);
									timefactor_AST = (AST)currentAST.root;
								}
								else {
									boolean synPredMatched117 = false;
									if (((LA(1)==INT||LA(1)==REAL) && (LA(2)==ID||LA(2)==PATH))) {
										int _m117 = mark();
										synPredMatched117 = true;
										inputState.guessing++;
										try {
											{
											realint();
											unitId();
											}
										}
										catch (RecognitionException pe) {
											synPredMatched117 = false;
										}
										rewind(_m117);
inputState.guessing--;
									}
									if ( synPredMatched117 ) {
										{
										realint();
										n_AST = (AST)returnAST;
										astFactory.addASTChild(currentAST, returnAST);
										extend();
										astFactory.addASTChild(currentAST, returnAST);
										if ( inputState.guessing==0 ) {
											timefactor_AST = (AST)currentAST.root;
											timefactor_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEEXT,"TIMEEXT")).add(n_AST)) ;
											currentAST.root = timefactor_AST;
											currentAST.child = timefactor_AST!=null &&timefactor_AST.getFirstChild()!=null ?
												timefactor_AST.getFirstChild() : timefactor_AST;
											currentAST.advanceChildToEnd();
										}
										}
										timefactor_AST = (AST)currentAST.root;
									}
									else {
										boolean synPredMatched120 = false;
										if (((LA(1)==ID||LA(1)==PATH) && (LA(2)==ID||LA(2)==PATH))) {
											int _m120 = mark();
											synPredMatched120 = true;
											inputState.guessing++;
											try {
												{
												pathe();
												unitId();
												}
											}
											catch (RecognitionException pe) {
												synPredMatched120 = false;
											}
											rewind(_m120);
inputState.guessing--;
										}
										if ( synPredMatched120 ) {
											{
											pathe();
											n2_AST = (AST)returnAST;
											astFactory.addASTChild(currentAST, returnAST);
											extend();
											astFactory.addASTChild(currentAST, returnAST);
											if ( inputState.guessing==0 ) {
												timefactor_AST = (AST)currentAST.root;
												timefactor_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEEXT,"TIMEEXT")).add(n2_AST)) ;
												currentAST.root = timefactor_AST;
												currentAST.child = timefactor_AST!=null &&timefactor_AST.getFirstChild()!=null ?
													timefactor_AST.getFirstChild() : timefactor_AST;
												currentAST.advanceChildToEnd();
											}
											}
											timefactor_AST = (AST)currentAST.root;
										}
										else {
											boolean synPredMatched123 = false;
											if (((LA(1)==INT||LA(1)==REAL) && (_tokenSet_68.member(LA(2))))) {
												int _m123 = mark();
												synPredMatched123 = true;
												inputState.guessing++;
												try {
													{
													realint();
													}
												}
												catch (RecognitionException pe) {
													synPredMatched123 = false;
												}
												rewind(_m123);
inputState.guessing--;
											}
											if ( synPredMatched123 ) {
												realint();
												astFactory.addASTChild(currentAST, returnAST);
												timefactor_AST = (AST)currentAST.root;
											}
										else {
											throw new NoViableAltException(LT(1), getFilename());
										}
										}}}}}}}}
									}
									catch (RecognitionException ex) {
										if (inputState.guessing==0) {
											reportError(ex);
											recover(ex,_tokenSet_68);
										} else {
										  throw ex;
										}
									}
									returnAST = timefactor_AST;
								}
								
	public final void timefunc() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timefunc_AST = null;
		AST p_AST = null;
		
		try {      // for error handling
			prefixop();
			p_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			match(LPAREN);
			time();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop134:
			do {
				if ((LA(1)==71)) {
					match(71);
					time();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop134;
				}
				
			} while (true);
			}
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				timefunc_AST = (AST)currentAST.root;
					timefunc_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEFUNC,"TIMEFUNC")).add(p_AST)) ;
				currentAST.root = timefunc_AST;
				currentAST.child = timefunc_AST!=null &&timefunc_AST.getFirstChild()!=null ?
					timefunc_AST.getFirstChild() : timefunc_AST;
				currentAST.advanceChildToEnd();
			}
			timefunc_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = timefunc_AST;
	}
	
	public final void timeInterval() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timeInterval_AST = null;
		AST i_AST = null;
		
		try {      // for error handling
			{
			interv();
			i_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			time();
			astFactory.addASTChild(currentAST, returnAST);
			interpp();
			time();
			astFactory.addASTChild(currentAST, returnAST);
			interv();
			astFactory.addASTChild(currentAST, returnAST);
			}
			if ( inputState.guessing==0 ) {
				timeInterval_AST = (AST)currentAST.root;
					timeInterval_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMEINTER,"TIMEINTER")).add(i_AST)) ;
				currentAST.root = timeInterval_AST;
				currentAST.child = timeInterval_AST!=null &&timeInterval_AST.getFirstChild()!=null ?
					timeInterval_AST.getFirstChild() : timeInterval_AST;
				currentAST.advanceChildToEnd();
			}
			timeInterval_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = timeInterval_AST;
	}
	
	public final void timespan() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timespan_AST = null;
		AST t_AST = null;
		
		try {      // for error handling
			match(LITERAL_durationBetween);
			match(LPAREN);
			time();
			t_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			match(71);
			time();
			astFactory.addASTChild(currentAST, returnAST);
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				timespan_AST = (AST)currentAST.root;
					timespan_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TIMESPAN,"TIMESPAN")).add(t_AST)) ;
				currentAST.root = timespan_AST;
				currentAST.child = timespan_AST!=null &&timespan_AST.getFirstChild()!=null ?
					timespan_AST.getFirstChild() : timespan_AST;
				currentAST.advanceChildToEnd();
			}
			timespan_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = timespan_AST;
	}
	
	public final void tuple() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST tuple_AST = null;
		AST n_AST = null;
		
		try {      // for error handling
			match(LPAREN);
			tuplexpr();
			n_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop126:
			do {
				if ((LA(1)==71)) {
					match(71);
					tuplexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop126;
				}
				
			} while (true);
			}
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				tuple_AST = (AST)currentAST.root;
				tuple_AST=(AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TUPLE,"TUPLE")).add(n_AST));
				currentAST.root = tuple_AST;
				currentAST.child = tuple_AST!=null &&tuple_AST.getFirstChild()!=null ?
					tuple_AST.getFirstChild() : tuple_AST;
				currentAST.advanceChildToEnd();
			}
			tuple_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		returnAST = tuple_AST;
	}
	
	public final void tuplexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST tuplexpr_AST = null;
		AST n_AST = null;
		
		try {      // for error handling
			{
			pathe();
			n_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			match(ASSIGN);
			{
			boolean synPredMatched131 = false;
			if (((_tokenSet_63.member(LA(1))) && (_tokenSet_75.member(LA(2))) && (_tokenSet_76.member(LA(3))) && (_tokenSet_77.member(LA(4))))) {
				int _m131 = mark();
				synPredMatched131 = true;
				inputState.guessing++;
				try {
					{
					boolexpr();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched131 = false;
				}
				rewind(_m131);
inputState.guessing--;
			}
			if ( synPredMatched131 ) {
				boolexpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((_tokenSet_59.member(LA(1))) && (_tokenSet_78.member(LA(2))) && (_tokenSet_72.member(LA(3))) && (_tokenSet_79.member(LA(4)))) {
				number1();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			}
			if ( inputState.guessing==0 ) {
				tuplexpr_AST = (AST)currentAST.root;
				tuplexpr_AST=(AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(TUPLEEXT,"TUPLEEXT")).add(n_AST));
				currentAST.root = tuplexpr_AST;
				currentAST.child = tuplexpr_AST!=null &&tuplexpr_AST.getFirstChild()!=null ?
					tuplexpr_AST.getFirstChild() : tuplexpr_AST;
				currentAST.advanceChildToEnd();
			}
			tuplexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_80);
			} else {
			  throw ex;
			}
		}
		returnAST = tuplexpr_AST;
	}
	
/*********************************************************
*
*   CLOCK RELATION
*
*********************************************************/
	public final void moderator() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST moderator_AST = null;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case STRICTLY:
			{
				AST tmp171_AST = null;
				tmp171_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp171_AST);
				match(STRICTLY);
				break;
			}
			case LITERAL_leftWeakly:
			{
				AST tmp172_AST = null;
				tmp172_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp172_AST);
				match(LITERAL_leftWeakly);
				break;
			}
			case LITERAL_rightWeakly:
			{
				AST tmp173_AST = null;
				tmp173_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp173_AST);
				match(LITERAL_rightWeakly);
				break;
			}
			case LITERAL_weakly:
			{
				AST tmp174_AST = null;
				tmp174_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp174_AST);
				match(LITERAL_weakly);
				break;
			}
			case LITERAL_nonStrictly:
			{
				AST tmp175_AST = null;
				tmp175_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp175_AST);
				match(LITERAL_nonStrictly);
				break;
			}
			case LITERAL_leftNonStrictly:
			{
				AST tmp176_AST = null;
				tmp176_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp176_AST);
				match(LITERAL_leftNonStrictly);
				break;
			}
			case LITERAL_rightNonStrictly:
			{
				AST tmp177_AST = null;
				tmp177_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp177_AST);
				match(LITERAL_rightNonStrictly);
				break;
			}
			case LITERAL_precedes:
			case LITERAL_sampledOn:
			case LITERAL_alternatesWith:
			case LITERAL_synchronizesWith:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			moderator_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_81);
			} else {
			  throw ex;
			}
		}
		returnAST = moderator_AST;
	}
	
/*********************************************************
*
*
*	TimerExpr
*
*
***********************/
	public final void timerExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timerExpr_AST = null;
		
		try {      // for error handling
			clockRef();
			astFactory.addASTChild(currentAST, returnAST);
			match(LITERAL_countFor);
			number1();
			astFactory.addASTChild(currentAST, returnAST);
			match(LITERAL_on);
			clockRef();
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_until:
			{
				match(LITERAL_until);
				clockRef();
				astFactory.addASTChild(currentAST, returnAST);
				break;
			}
			case EOF:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			timerExpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = timerExpr_AST;
	}
	
	public final void wrt() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST wrt_AST = null;
		AST id_AST = null;
		
		try {      // for error handling
			match(LITERAL_wrt);
			clockId();
			id_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				wrt_AST = (AST)currentAST.root;
				wrt_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(WRT,"WRT")).add(id_AST));
				currentAST.root = wrt_AST;
				currentAST.child = wrt_AST!=null &&wrt_AST.getFirstChild()!=null ?
					wrt_AST.getFirstChild() : wrt_AST;
				currentAST.advanceChildToEnd();
			}
			wrt_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_82);
			} else {
			  throw ex;
			}
		}
		returnAST = wrt_AST;
	}
	
	public final void ate() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST ate_AST = null;
		AST id_AST = null;
		
		try {      // for error handling
			match(LITERAL_at);
			instantexpr();
			id_AST = (AST)returnAST;
			astFactory.addASTChild(currentAST, returnAST);
			if ( inputState.guessing==0 ) {
				ate_AST = (AST)currentAST.root;
				ate_AST= (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(ATE,"AT")).add(id_AST));
				currentAST.root = ate_AST;
				currentAST.child = ate_AST!=null &&ate_AST.getFirstChild()!=null ?
					ate_AST.getFirstChild() : ate_AST;
				currentAST.advanceChildToEnd();
			}
			ate_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = ate_AST;
	}
	
	public final void timerdef() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST timerdef_AST = null;
		
		try {      // for error handling
			match(LITERAL_Timer);
			timerId2();
			astFactory.addASTChild(currentAST, returnAST);
			{
			switch ( LA(1)) {
			case LITERAL_is:
			{
				match(LITERAL_is);
				timerExpr();
				astFactory.addASTChild(currentAST, returnAST);
				break;
			}
			case EOF:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			if ( inputState.guessing==0 ) {
				timerdef_AST = (AST)currentAST.root;
				timerdef_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(CLKDEF,"CLK_DEF")).add(timerdef_AST));
				currentAST.root = timerdef_AST;
				currentAST.child = timerdef_AST!=null &&timerdef_AST.getFirstChild()!=null ?
					timerdef_AST.getFirstChild() : timerdef_AST;
				currentAST.advanceChildToEnd();
			}
			timerdef_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
		returnAST = timerdef_AST;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"BLOCK",
		"REL",
		"CLK_FUNC",
		"CLK_FUNC2",
		"CLK_REL",
		"CLKG",
		"CHRONONFP",
		"CHRONONFPSTB",
		"EXPR",
		"BWEXPR",
		"CLKEXPR",
		"\"if\"",
		"COND",
		"INT",
		"REAL",
		"LOG",
		"LOGAND",
		"LOGOR",
		"LOGXOR",
		"LOGNOT",
		"LOGPAR",
		"LOGCOMP",
		"ADDOP",
		"INTERVALREAL",
		"\"{\"",
		"\"}\"",
		"\"(\"",
		"\")\"",
		"\"[\"",
		"\"]\"",
		"ID",
		"PATH",
		"CLKDEF",
		"\"=\"",
		"CVS",
		"FCVS",
		"REF",
		"\"#\"",
		"INST",
		"INST_REL",
		"RELINST",
		"EGOP",
		"STAT",
		"GUARD",
		"WRT",
		"ATE",
		"DUREXPR",
		"INSTEXPR",
		"INSTDEF",
		"INSTREF1",
		"INSTREF1b",
		"INSTREF2",
		"TIMEMUL",
		"TIMEFUNC",
		"TIMESPAN",
		"TIMEINTER",
		"EXPRADD",
		"EXPRMUL",
		"EXPRNUMBER",
		"INTERPP",
		"TIMESOMME",
		"TIMEEXT",
		"TUPLE",
		"TUPLEEXT",
		"\"strictly\"",
		"\"isDisjoint\"",
		"\";\"",
		"\",\"",
		"\"haveMaxDrift\"",
		"\"haveOffset\"",
		"\"haveSkew\"",
		"\"haveDrift\"",
		"\"hasStability\"",
		"\"coincidentWith\"",
		"\"precedes\"",
		"\"by\"",
		"\"..\"",
		"\"max\"",
		"\"min\"",
		"BW",
		"GBW",
		"\"uniform\"",
		"\"+\"",
		"\"-\"",
		"\"*\"",
		"\"/\"",
		"\"%\"",
		"\"or\"",
		"\"xor\"",
		"\"and\"",
		"\"not\"",
		"\"true\"",
		"\"false\"",
		"RELOP",
		"\"in\"",
		"\"on\"",
		"\"mult\"",
		"\"durationBetween\"",
		"\"restrictedTo\"",
		"\"filteredBy\"",
		"\"discretizedBy\"",
		"\"from\"",
		"\"to\"",
		"\"restrictedToTrailing\"",
		"\"delayedFor\"",
		"\"followedBy\"",
		"\"inter\"",
		"\"union\"",
		"\"minus\"",
		"\"sampledOn\"",
		"\"restrictedToLeading\"",
		"\"restrictedToInterval\"",
		"\"when\"",
		"\"timerConstraint\"",
		"\"oneShotConstraint\"",
		"\"sup\"",
		"\"inf\"",
		"\"sustain\"",
		"\"upto\"",
		"\"countFor\"",
		"\"until\"",
		"\"wrt\"",
		"\"at\"",
		"\"leftWeakly\"",
		"\"rightWeakly\"",
		"\"weakly\"",
		"\"nonStrictly\"",
		"\"leftNonStrictly\"",
		"\"rightNonStrictly\"",
		"\"alternatesWith\"",
		"\"synchronizesWith\"",
		"\"isSubClockOf\"",
		"\"isFinerThan\"",
		"\"isFasterThan\"",
		"\"withIn\"",
		"\"isCoarserThan\"",
		"\"isSlowerThan\"",
		"\"isPeriodicOn\"",
		"\"period\"",
		"\"offset\"",
		"\"isSporadicOn\"",
		"\"gap\"",
		"\"hasSameRateAs\"",
		"\"sync\"",
		"\"Clock\"",
		"\"is\"",
		"\"Timer\"",
		"\"Instant\"",
		"\"iprecedes\"",
		"\"instantOf\"",
		"\"suchThat\"",
		"WS",
		"SL_COMMENT",
		"MUL",
		"DIV",
		"MODULO",
		"BIT",
		"GBV",
		"RBIT",
		"DIGIT",
		"HEX_DIGIT",
		"EXPONENT",
		"SEMI",
		"COMA",
		"DOT"
	};
	
	protected void buildTokenTypeASTClassMap() {
		tokenTypeToASTClassMap=null;
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 52613349376L, 283726776524341248L, 42991616L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 53150220288L, 283726776524341248L, 42991616L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 52613349376L, 283726776524341248L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { 190052302848L, -8935148532650704880L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { 196495147008L, -8646918148966580080L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { 196495179776L, -8646915888756784944L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = { 2251636604928L, -8939645260330385264L, 867327L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = { 54761226240L, -8935148532638138352L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = { 2258079481856L, -8646918148849090096L, 1015807L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = { 52613349376L, 283726776524345472L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = { 54761226240L, -8935148532650737648L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = { 59056226304L, -6341075139752878384L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = { 52613349376L, 0L, 33554432L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = { 56908316672L, 4895412794951737360L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = { 54761226240L, -8935148532638154736L, 117440543L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	private static final long[] mk_tokenSet_16() {
		long[] data = { 67646160896L, -4035232130421751600L, 33554463L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_16 = new BitSet(mk_tokenSet_16());
	private static final long[] mk_tokenSet_17() {
		long[] data = { 32768L, 64L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_17 = new BitSet(mk_tokenSet_17());
	private static final long[] mk_tokenSet_18() {
		long[] data = { 139586469888L, 8400L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_18 = new BitSet(mk_tokenSet_18());
	private static final long[] mk_tokenSet_19() {
		long[] data = { 190052335616L, 283726776524374080L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_19 = new BitSet(mk_tokenSet_19());
	private static final long[] mk_tokenSet_20() {
		long[] data = { 55298097152L, -8935148525121961968L, 42991647L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_20 = new BitSet(mk_tokenSet_20());
	private static final long[] mk_tokenSet_21() {
		long[] data = { 2395518435330L, -4035232103712362288L, 17644543L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_21 = new BitSet(mk_tokenSet_21());
	private static final long[] mk_tokenSet_22() {
		long[] data = { 190052335616L, -8935148532650737584L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_22 = new BitSet(mk_tokenSet_22());
	private static final long[] mk_tokenSet_23() {
		long[] data = { 197032050688L, -8646918148966580016L, 42991647L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_23 = new BitSet(mk_tokenSet_23());
	private static final long[] mk_tokenSet_24() {
		long[] data = { 2396055306242L, -4035229870329368368L, 60636159L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_24 = new BitSet(mk_tokenSet_24());
	private static final long[] mk_tokenSet_25() {
		long[] data = { 137438986240L, 64L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_25 = new BitSet(mk_tokenSet_25());
	private static final long[] mk_tokenSet_26() {
		long[] data = { 2342904692738L, -2589576657685651504L, 68124671L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_26 = new BitSet(mk_tokenSet_26());
	private static final long[] mk_tokenSet_27() {
		long[] data = { 52613349376L, -8939645260330385392L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_27 = new BitSet(mk_tokenSet_27());
	private static final long[] mk_tokenSet_28() {
		long[] data = { 59056226304L, -8646918148849090352L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_28 = new BitSet(mk_tokenSet_28());
	private static final long[] mk_tokenSet_29() {
		long[] data = { 2251636604928L, 283726776524341376L, 867200L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_29 = new BitSet(mk_tokenSet_29());
	private static final long[] mk_tokenSet_30() {
		long[] data = { 54760833024L, -8935148532650737648L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_30 = new BitSet(mk_tokenSet_30());
	private static final long[] mk_tokenSet_31() {
		long[] data = { 2258079481856L, -8646918148966579760L, 1015711L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_31 = new BitSet(mk_tokenSet_31());
	private static final long[] mk_tokenSet_32() {
		long[] data = { 56908316672L, 4895412794951729152L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_32 = new BitSet(mk_tokenSet_32());
	private static final long[] mk_tokenSet_33() {
		long[] data = { 32768L, 8272L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_33 = new BitSet(mk_tokenSet_33());
	private static final long[] mk_tokenSet_34() {
		long[] data = { 52613349376L, 283726776524374016L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_34 = new BitSet(mk_tokenSet_34());
	private static final long[] mk_tokenSet_35() {
		long[] data = { 54761226240L, -8935148532638154736L, 31L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_35 = new BitSet(mk_tokenSet_35());
	private static final long[] mk_tokenSet_36() {
		long[] data = { 196495179776L, -8646918148849090352L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_36 = new BitSet(mk_tokenSet_36());
	private static final long[] mk_tokenSet_37() {
		long[] data = { 190052335616L, -8939645260330418096L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_37 = new BitSet(mk_tokenSet_37());
	private static final long[] mk_tokenSet_38() {
		long[] data = { 55298097152L, -8935148525121945584L, 42991743L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_38 = new BitSet(mk_tokenSet_38());
	private static final long[] mk_tokenSet_39() {
		long[] data = { 137438986240L, -9223372036854759344L, 127L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_39 = new BitSet(mk_tokenSet_39());
	private static final long[] mk_tokenSet_40() {
		long[] data = { 139586469888L, 64L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_40 = new BitSet(mk_tokenSet_40());
	private static final long[] mk_tokenSet_41() {
		long[] data = { 2403571105794L, -283731388249538608L, 87031807L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_41 = new BitSet(mk_tokenSet_41());
	private static final long[] mk_tokenSet_42() {
		long[] data = { 0L, 34359738368L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_42 = new BitSet(mk_tokenSet_42());
	private static final long[] mk_tokenSet_43() {
		long[] data = { 2352031498242L, -283733648459309104L, 68124671L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_43 = new BitSet(mk_tokenSet_43());
	private static final long[] mk_tokenSet_44() {
		long[] data = { 32768L, 64L, 2097152L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_44 = new BitSet(mk_tokenSet_44());
	private static final long[] mk_tokenSet_45() {
		long[] data = { 2L, 0L, 2097152L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_45 = new BitSet(mk_tokenSet_45());
	private static final long[] mk_tokenSet_46() {
		long[] data = { 15569289216L, 6917529027653738704L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_46 = new BitSet(mk_tokenSet_46());
	private static final long[] mk_tokenSet_47() {
		long[] data = { 148176404482L, -2305840748991323952L, 16810111L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_47 = new BitSet(mk_tokenSet_47());
	private static final long[] mk_tokenSet_48() {
		long[] data = { 139586469888L, 939532496L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_48 = new BitSet(mk_tokenSet_48());
	private static final long[] mk_tokenSet_49() {
		long[] data = { 68183031810L, 6917529233812553920L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_49 = new BitSet(mk_tokenSet_49());
	private static final long[] mk_tokenSet_50() {
		long[] data = { 65498644480L, 206158823424L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_50 = new BitSet(mk_tokenSet_50());
	private static final long[] mk_tokenSet_51() {
		long[] data = { 1073741824L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_51 = new BitSet(mk_tokenSet_51());
	private static final long[] mk_tokenSet_52() {
		long[] data = { 148176404482L, -2305840749121347376L, 16810111L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_52 = new BitSet(mk_tokenSet_52());
	private static final long[] mk_tokenSet_53() {
		long[] data = { 204547850242L, -2305840748991258416L, 16810111L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_53 = new BitSet(mk_tokenSet_53());
	private static final long[] mk_tokenSet_54() {
		long[] data = { 139586469888L, 34359738432L, 32768L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_54 = new BitSet(mk_tokenSet_54());
	private static final long[] mk_tokenSet_55() {
		long[] data = { 148176404482L, -2305840749108764464L, 16810111L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_55 = new BitSet(mk_tokenSet_55());
	private static final long[] mk_tokenSet_56() {
		long[] data = { 393216L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_56 = new BitSet(mk_tokenSet_56());
	private static final long[] mk_tokenSet_57() {
		long[] data = { 139586469888L, 134226128L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_57 = new BitSet(mk_tokenSet_57());
	private static final long[] mk_tokenSet_58() {
		long[] data = { 139586469888L, 402661584L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_58 = new BitSet(mk_tokenSet_58());
	private static final long[] mk_tokenSet_59() {
		long[] data = { 52613742592L, 12582912L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_59 = new BitSet(mk_tokenSet_59());
	private static final long[] mk_tokenSet_60() {
		long[] data = { 190052696064L, 25899827200L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_60 = new BitSet(mk_tokenSet_60());
	private static final long[] mk_tokenSet_61() {
		long[] data = { 192200179712L, 25899827200L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_61 = new BitSet(mk_tokenSet_61());
	private static final long[] mk_tokenSet_62() {
		long[] data = { 192200212480L, 26839359696L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_62 = new BitSet(mk_tokenSet_62());
	private static final long[] mk_tokenSet_63() {
		long[] data = { 52613742592L, 7528775680L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_63 = new BitSet(mk_tokenSet_63());
	private static final long[] mk_tokenSet_64() {
		long[] data = { 192200179712L, 34355544064L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_64 = new BitSet(mk_tokenSet_64());
	private static final long[] mk_tokenSet_65() {
		long[] data = { 192200212480L, 34355552464L, 16777218L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_65 = new BitSet(mk_tokenSet_65());
	private static final long[] mk_tokenSet_66() {
		long[] data = { 2404645240834L, -6863479242800L, 127893503L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_66 = new BitSet(mk_tokenSet_66());
	private static final long[] mk_tokenSet_67() {
		long[] data = { 2404913676290L, -6631429505072L, 127893503L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_67 = new BitSet(mk_tokenSet_67());
	private static final long[] mk_tokenSet_68() {
		long[] data = { 15569289216L, 6917529027653730496L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_68 = new BitSet(mk_tokenSet_68());
	private static final long[] mk_tokenSet_69() {
		long[] data = { 32768L, 6917529027641081920L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_69 = new BitSet(mk_tokenSet_69());
	private static final long[] mk_tokenSet_70() {
		long[] data = { 15569289216L, 6917529027641147584L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_70 = new BitSet(mk_tokenSet_70());
	private static final long[] mk_tokenSet_71() {
		long[] data = { 54761226240L, 130023424L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_71 = new BitSet(mk_tokenSet_71());
	private static final long[] mk_tokenSet_72() {
		long[] data = { 68183031808L, 6917529027771171008L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_72 = new BitSet(mk_tokenSet_72());
	private static final long[] mk_tokenSet_73() {
		long[] data = { 67646128128L, 206171406336L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_73 = new BitSet(mk_tokenSet_73());
	private static final long[] mk_tokenSet_74() {
		long[] data = { 205621985280L, 6917529268289732800L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_74 = new BitSet(mk_tokenSet_74());
	private static final long[] mk_tokenSet_75() {
		long[] data = { 192200179712L, 34355544192L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_75 = new BitSet(mk_tokenSet_75());
	private static final long[] mk_tokenSet_76() {
		long[] data = { 205621985280L, 6917529061996691648L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_76 = new BitSet(mk_tokenSet_76());
	private static final long[] mk_tokenSet_77() {
		long[] data = { 205890420736L, 7201256044679856320L, 42991616L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_77 = new BitSet(mk_tokenSet_77());
	private static final long[] mk_tokenSet_78() {
		long[] data = { 54761226240L, 130023552L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_78 = new BitSet(mk_tokenSet_78());
	private static final long[] mk_tokenSet_79() {
		long[] data = { 205890420736L, 7201256017970528448L, 42991616L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_79 = new BitSet(mk_tokenSet_79());
	private static final long[] mk_tokenSet_80() {
		long[] data = { 2147483648L, 128L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_80 = new BitSet(mk_tokenSet_80());
	private static final long[] mk_tokenSet_81() {
		long[] data = { 0L, 562949953437696L, 96L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_81 = new BitSet(mk_tokenSet_81());
	private static final long[] mk_tokenSet_82() {
		long[] data = { 32768L, 4611686018427387968L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_82 = new BitSet(mk_tokenSet_82());
	
	}
