/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyrigth 2007-2008
 */
package grammaire.test; 

import java.io.FileInputStream;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import antlr.BaseAST;
import antlr.collections.AST;
import antlr.debug.misc.ASTFrame;
import code.view.SimulationCall;

import element.dictionnaire.CsubdictionnaireClock;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.Collector;
import fr.inria.vcd.view.Iupdate;
import fr.inria.vcd.view.VcdDiagram;
import fr.inria.wave.VCDfile;
import fr.inria.wave.fire.SimFireRandom;
import grammaire.LangLexer;
import grammaire.LangParser;
//import grammaire.LangWalker;
import grammaire.analyse.Analysis;
import grammaire.analyse.TransformAst;

class Ctestgram// extends Thread 
{
	Ctestgram() {
		;
	}

	static CsubdictionnaireClock cdclk = null;
	//Process p = null;



	public static void main(String[] args) {
		System.out.println("Grammar testing");
		LangParser parser = null;
		LangLexer lexer = null;
		AST m=null;
		Analysis ana = new Analysis(null);
		try {
			lexer = new LangLexer(new FileInputStream( 	"src/grammaire/test/test11b.in"));
			
			parser = new LangParser(lexer);
			parser.ccsl();
		
			System.out.println("\nParsing:");
			antlr.CommonAST a = (antlr.CommonAST) parser.getAST();
		
			System.out.println(a.toStringList());
		
			ASTFrame frame = new ASTFrame("The tree", a);
			System.out.println("\nThe Tree");
			frame.setVisible(true);

			//LangWalker walker = new LangWalker();
		//	walker.block(a); // walk tree
			System.out.println("\nWalking done\nDeclaration:");
			//Cliste run = walker.getTree();
			//run.disp();
			//run.makeListevar();
			//System.out.println("Number of walker variables :"		+ run.getDico().count());
			// run.GetDico().Disp();
			System.out.println("------------------------------");
			parser.getDico().getClkdic().disp();
			cdclk = parser.getDico().getClkdic();
			System.out.println("Number of parser variables :"
					+ parser.getDico().count());
			parser.getDico().disp();
		
			 m =TransformAst.transformAST(a);
			ASTFrame frame2;
			frame2 = new ASTFrame("The tree 3", m);			
			frame2.setVisible(true);
			
			//ana.found(a);
		} catch (Exception e) {
			System.out.println("..exception: " + e + "\n" + parser.errorNumber);

			e.printStackTrace(System.out);
		}
		Process p;
		System.out.println("Error :\n" + parser.errorstr);
		SimulationCall s;
		try {
	
			vcdfile = new VCDfile();
			vcdfile.openfile("resultsimul");
			vcdfile.fileHeader("1 ns");
			ana = new Analysis(vcdfile);
			BaseAST b=(BaseAST )TransformAst.transformAST(m);			
			ana.found(b);
			
			vcdfile.setPulse(1);
			System.out.println("runing");
			
			
			Collector.block = false;
			Collector c = new Collector();

			vcd = vcdfile.getSc().getVcd();
			vcdfile.prerun();
			Collector.drawInWindow(c.collect(vcd), "VCD viewer", c);			
			
			win = c.vcddiagram;
			win.setSimulation(true);
			((VcdDiagram) win).getTc().partial = true;

			vcdfile.setFirepol(new SimFireRandom());
			
			vcdfile.run(100, win);
			((VcdDiagram) win).getTc().partial = false;
		//	if (((VcdDiagram) win).getShell()!=null)
		//		win.update(vcdfile.getSc().getsize() -1,	vcdfile.getSc().getsize(), true);
			vcdfile.closefile();
			System.out.println();
			Shell shell=((VcdDiagram) win).getShell();
			Display display=((VcdDiagram) win).getShell().getDisplay();
			
			
			
			while (!shell.isDisposed ()) {
				if (!display.readAndDispatch ()) display.sleep ();
			}
			display.dispose ();
			System.out.println("\nok");
	
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		

		System.out.print("\nok");
	}


	
	static VCDfile vcdfile;
	static VCDDefinitions vcd = null;
	static Iupdate win = null;
	
}
