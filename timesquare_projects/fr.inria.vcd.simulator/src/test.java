/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.Collector;
import fr.inria.vcd.view.Iupdate;
import fr.inria.vcd.view.VcdDiagram;
import fr.inria.wave.Clock;
import fr.inria.wave.VCDfile;
import fr.inria.wave.constraint.Alternates;
import fr.inria.wave.constraint.FilteringBy;
import fr.inria.wave.constraint.InfFaster;
import fr.inria.wave.constraint.Inter;
import fr.inria.wave.constraint.Meet;
import fr.inria.wave.constraint.Minus;
import fr.inria.wave.constraint.OneShot;
import fr.inria.wave.constraint.SampleTo;
import fr.inria.wave.constraint.SubClock;
import fr.inria.wave.constraint.SubLower;
import fr.inria.wave.constraint.Sustain;
import fr.inria.wave.constraint.Sync;
import fr.inria.wave.constraint.TimerOn;
import fr.inria.wave.constraint.Union;
import fr.inria.wave.data.BW;
import fr.inria.wave.data.bw.BinaryWord;
import fr.inria.wave.data.bw.GBinaryWord;
import fr.inria.wave.data.bw.Smooth;
import fr.inria.wave.enumeration.Moderator;
import fr.inria.wave.fire.SimFireManual;
import fr.inria.wave.fire.SimFireRandom;

public class test {

	public static void main(String[] args) {

		try {
			
			 //test1();
			// test2();
			 //test3();
			 test4();			 
			testABRO();
			//test4();

		} catch (Exception e) {
			System.out.println("ERROR" + e);
			e.printStackTrace();
		}

	}

	static Iupdate disp(VCDfile v) {
		// Shell shell=new Shell();
		try {

			// VcdDiagramTest window = new VcdDiagramTest();

			System.out.println("runing");

			// vcdfile.run(length);
			Collector.block = false;
			Collector c = new Collector();
			VCDDefinitions vcd = v.getSc().getVcd();
			Iupdate win = null;
			Collector.drawInWindow(c.collect(vcd), "VCD viewer", c);
			win = c.vcddiagram;
			// ((VcdDiagram)win).getTc().partial=false;
			// win.update();
			win.setSimulation(true);
			((VcdDiagram) win).getTc().setPartial ( true);
			// read(v.getSc().getVcd());
			System.out.println(v.getSc().getVcd());
			
			return win;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;

	}

	static void test1() throws Exception {
		Clock days, sundays, newMoon, newMoon2, fullMoon, vEquinox, easter, easterMoon, delay14;
		VCDfile v = new VCDfile();
		v.openfile("eastertest");
		v.fileHeader("1","ns");
		// ClockList cl= v.getCl();
		days = v.createClock("days");
		sundays = v.createClock("sundays");
		newMoon = v.createClock("newmoon");
		newMoon2 = v.createClock("newmoon2");
		delay14 = v.createClock("Delay14");
		fullMoon = v.createClock("fulMoon");
		// fullMoon2 =v.createClock("fulMoon2");
		vEquinox = v.createClock("VEquinox");
		easterMoon = v.createClock("EasterMoon");

		easter = v.createClock("Easter");

		v.locked();

		BW b = new BinaryWord("0", "1000000");
		BW b1 = new Smooth(673, 2953, 100);

		BW b2 = new GBinaryWord("0^6", "1.0^29"); // 0^6(1.0^29)
		// BW b3=new GBinaryWord("0^20","1.0^29");
		BW b4 = new GBinaryWord("0^20", "1.0^365");
		// int i;
		b.check(10000);
		b1.check(20000);
		// b2.check(20000);
		// b4.check(20000);
		v.setPulse(1);
		v.add(new FilteringBy(days, sundays, b));
		v.add(new FilteringBy(days, newMoon, b2));
		v.add(new FilteringBy(days, newMoon2, b1));
		// v.add(new FilteringBy(days,fullMoon2,b3));
		v.add(new OneShot(delay14, days, newMoon2, fullMoon, 14));
		v.add(new FilteringBy(days, vEquinox, b4));
		// v.add(new SampleTo(fullMoon,vEquinox,easterMoon,true));
		v.add(new SampleTo(fullMoon, vEquinox, easterMoon, false));
		v.add(new SampleTo(sundays, easterMoon, easter, true));
		// v.add(new Alternates(easterMoon,easter,1));
		// v.add(new Alternates(vEquinox,easterMoon,0));
		v.locked();
		v.prerun();
		v.setFirepol(new SimFireManual());
		v.getFirepol().startSimul();
		
		Iupdate x = disp(v);
		v.run(100, x);
		v.statistique();
		v.closefile();
		x.setSimulation(false);		
		x.setSimulationAvance(6);
		x.getTc().setPartial(false);
		waitclose(x,v);
	}

	static void test2() throws Exception {
		Clock clk1, clk3;
		Clock clk2; // ,clk4,clk5;
		VCDfile v = new VCDfile();
		v.openfile("test_2");
		v.fileHeader("1","ns");
		// ClockList cl= v.getCl();
		clk1 = v.createClock("clk1");
		clk2 = v.createClock("clk2");
		clk3 = v.createClock("clk3");
		// clk4 =v.createClock("clk4");
		// clk5 =v.createClock("clk5");
		v.locked();
		v.setPulse(1);
		BW b = new BinaryWord("1", "000010000000000");
		BW b2 = new BinaryWord("1", "000010000000000");
		v.add(new Alternates(clk3, clk1, Moderator.weakly));
		v.add(new Sync(clk2, clk1, 3, 3, Moderator.strictly));
		v.add(new Sync(clk1, clk2, 5, 5, Moderator.strictly));
		v.add(new Meet(clk1, clk2, b, b2));
		// v.add(new RestrictToInterval(clk2,clk5,5,10));
		// v.add(new RestrictToInterval(clk2,clk4,2,6));
		v.setFirepol(new SimFireRandom());
		v.getFirepol().startSimul();
		v.prerun();
		Iupdate x = disp(v);
		v.run(100, x);
		v.statistique();
		v.closefile();
		x.setSimulation(false);
		waitclose(x,v);
	}

	static void test3() throws Exception

	{
		Clock supclk, clk1, da, t1s, t1e, t2s, t1, no1;
		// t2e,t3s,t3e,ds,t2,t3; //,no2,no3;
		VCDfile v = new VCDfile();
		v.openfile("test_3");
		v.fileHeader("1","ns");
		// ClockList cl= v.getCl();
		clk1 = v.createClock("clk1");
		supclk = v.createClock("supclk");
		da = v.createClock("da");
		// ds =v.createClock("ds");
		t1s = v.createClock("t1s");
		t1e = v.createClock("t1e");
		t2s = v.createClock("t2s");
		// t2e =v.createClock("t2e");
		// t3s =v.createClock("t3s");
		// t3e =v.createClock("t3e");
		t1 = v.createClock("task__1");
		// t2 =v.createClock("task__2");
		// t3 =v.createClock("task__3");
		no1 = v.createClock("no1");
		// no2 =v.createClock("no2");
		// no3 =v.createClock("no3");
		v.locked();
		v.setPulse(1);
		v.add(new FilteringBy(supclk, clk1, new BinaryWord("0", "00000001")));
		v.add(new FilteringBy(supclk, da, new GBinaryWord("0^5", "1.0^35")));
		v.add(new SampleTo(clk1, da, t1s, true));
		// v.add(new SampleTo(clk1,t3e,ds,true));
		// v.add(new Alternates(da, ds,1));
		// v.add(new SubClock(clk1,t1s));
		// v.add(new SubClock(clk1,t2s));
		// v.add(new SubClock(clk1,t3s));

		v.add(new SampleTo(clk1, t1e, t2s, true));
		// v.add(new SampleTo(clk1,t2e,t3s,true));

		// v.add(new Alternates(t1s, t1e,1));
		// v.add(new Alternates(t2s, t2e,1));
		// v.add(new Alternates(t3s, t3e,1));

		// v.add(new Alternates(t1e, t2s,1));
		// v.add(new Alternates(t2e, t3s,1));
		v.add(new FilteringBy(supclk, no1, new GBinaryWord("0.0^49.1.0^36.1","0")));

		v.add(new TimerOn(t1, supclk, t1s, no1, t1e, 12));
		// v.add(new TimerOn(t2,supclk,t2s,Clockdeath.getClockdeath(),t2e,9));
		// v.add(new TimerOn(t3,supclk,t3s,Clockdeath.getClockdeath(),t3e,7));

		// v.add(new Alternates(t1e, t3s,1));
		v.setFirepol(new SimFireRandom());
		v.getFirepol().startSimul();
		v.prerun();
		Iupdate x = disp(v);
		v.run(200, x);
		v.statistique();
		v.closefile();
		x.setSimulation(false);
		waitclose(x,v);

	}

	static void test4() throws Exception {
		Clock clk1, clk2, clk3, clk3b, clk, clk4, smooth, clk5,top ,stop,sustain, sampled;
		VCDfile v = new VCDfile();
		v.openfile("test_4");
		v.fileHeader("1","ns");
		// ClockList cl= v.getCl();
		v.createClock("xcb");
		clk1 = v.createClock("clk1");
		clk2 = v.createClock("clk2");
		clk3 = v.createClock("speed");
		clk3b = v.createClock("slow");
		clk4 = v.createClock("inter");
		clk5 = v.createClock("minus");
		clk = v.createClock("clk");
		top = v.createClock("top");
		stop = v.createClock("stop");
		sustain=v.createClock("sustain");
		smooth = v.createClock("smooth");
		sampled = v.createClock("sampled");
		
		v.add(new FilteringBy(clk, clk1, new BinaryWord("", "01")));
		v.add(new FilteringBy(clk, clk2, new BinaryWord("", "001")));
		v.add(new FilteringBy(clk, smooth, new Smooth(0, 12, 5)));
		v.add(new Union(v.createClock("union"), clk1, clk2));
		v.add(new Inter(clk4, clk1, clk2));
		v.add(new InfFaster(clk3, clk1, clk2));
		v.add(new SubLower(clk3b, clk1, clk2));
		v.add(new Minus(clk5,clk1, clk2));
		v.add(new FilteringBy(clk, top, new BinaryWord("0000", "100000000000000000")));
		v.add(new FilteringBy(clk, stop, new BinaryWord("0000", "00000000010000000")));
		v.add(new SubClock(clk,sustain));
		v.add(new Sustain(sustain,top,stop));
		v.add(new SampleTo(clk4,clk5,sampled,false));
		v.setPulse(1);
		v.locked();
		v.setFirepol(new SimFireRandom());
		//v.setFirepol(new SimFireManual());
		// v.add(new SubClock(clk3,clk1));
		// v.add(new SubClock(clk3,clk2));
		//v.getFirepol().startsimul();
		v.prerun();
		Iupdate x = disp(v);
		v.run(154, x);
		v.statistique();
		v.closefile();
		x.setSimulation(false);
		waitclose(x,v);

	}

	static void waitclose(Iupdate x, VCDfile vcdfile)
	{
		x.setSimulationAvance(6);
		((VcdDiagram) x).setFocus();
		x.getTc().setPartial(false);
		if (x.getMyShell() != null)
			x.update(vcdfile.getSc().getsize() - 1, vcdfile.getSc()
					.getsize(), true);
		Shell loopShell=((VcdDiagram) x).getShell(); 
		Display	display=loopShell.getDisplay();
		while (loopShell != null && !loopShell.isDisposed()) {
			try {
				if (!display.readAndDispatch()) 
				{
					display.sleep();
				}
			} catch (Throwable e) {
				break;
			}
		}
		((VcdDiagram) x).close();
	}
	
	static void testABRO() throws Exception {
		Clock a, b, r, o, ra, rb, rab, fs, time,o1,o2; // ,clk;
		VCDfile v = new VCDfile();
		v.openfile("testABRO");
		v.fileHeader("1","ns");		
		a = v.createClock("A");
		b = v.createClock("B");
		r = v.createClock("R");
		o1 = v.createClock("O1");
		o2 = v.createClock("O2");
		o = v.createClock("O");
		ra = v.createClock("ra");
		rb = v.createClock("rb");
		rab = v.createClock("union");
		//ab = v.createClock("aandb");
		fs = v.createClock("first");
		time = v.createClock("timer");
	
		// v.add(new FilteringBy(clk,a,new
		// BinaryWord("100000000100000001000","0")));  
		// v.add(new FilteringBy(clk,b,new
		// BinaryWord("000010000000010010000","0")));
		// v.add(new FilteringBy(clk,r,new
		// BinaryWord("000100010010000000010","0")));

		v.add(new SampleTo(a, r, ra, true));
		v.add(new SampleTo(b, r, rb, true));	
		v.add(new Union(rab, ra, rb));
		v.add(new Inter(o2, ra, rb));
		v.add(new SampleTo(rab, r, fs, false));
		v.add(new TimerOn(time, rab, fs,  r, o1, 1));
		v.add(new Union(o, o1, o2));
		 
		v.locked();
		v.setPulse(1);
		// v.add(new SubClock(clk3,clk1));
		// v.add(new SubClock(clk3,clk2));
		v.setFirepol(new SimFireManual());
		//v.getFirepol().startsimul();
		v.prerun();

		Iupdate x = disp(v);		
		v.run(54, x);
		v.statistique();
		v.closefile();
		waitclose(x,v);

	}
}
