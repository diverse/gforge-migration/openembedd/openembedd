/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.dialog;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;



public class CbooleanCombo extends Composite {
	private CCombo c;

	private Label lb;

	private String[] lst = new String []{"X" , "0", "1" };
	private String name = "";
	private String tooltip = "";
	private int value = 0;
	private int oldvalue=0;
	private int id;
	private ISimulationDialog isd=null; 
	public int getId()
	{
		return id;
	}

	private TypeValue tpvalue;
	public enum TypeValue {
		T(1, "1"), F(0, "0"), X(2, "X"), x(3, "x");
		private String s;
		private int n;
 
		private TypeValue(int n, String s) {
			this.s = s;
			this.n = n;
		} 

		@Override
		public String toString() {
			return s;
		}
 
		protected int getN()
		{
			return n;
		}
		
	}


	@Override
	public void dispose() {
		c.dispose();
		lb.dispose();
		
	}

	public CbooleanCombo(Composite parent, String namein, String tooltipin, int idin, ISimulationDialog inisd) {
		super(parent, SWT.FILL  );  
		this.name = namein;
		this.tooltip = tooltipin;
		id=idin;
		isd=inisd;
		make();		
		setSelectCombo(0);
	}

	

	@Override
	public void addListener(int eventType, Listener listener) {
		c.addListener(eventType, listener);

	}

	public int make() {

		
		c = new CCombo(this, SWT.FILL  | SWT.READ_ONLY | SWT.FLAT | SWT.TAB | SWT.NO_REDRAW_RESIZE);
		c.setCapture(false);
		c.setToolTipText(tooltip);					
		c.setSize(40, 20);
		c.setLocation(05, 05);	
		c.setBackground(new Color(null, 255, 255, 255));
		c.setItems(lst);
		c.select(0);
		c.addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			
			public void widgetSelected(SelectionEvent e) {
				oldvalue=value;
				value = ((CCombo) e.getSource()).getSelectionIndex();							
				if (isd!=null) isd.modifyClock(id,value, oldvalue);
			}
		});
		lb = new Label(this, SWT.FILL| SWT.RESIZE);
		lb.setText(name);
		lb.setSize(110, 20);
		lb.setLocation(50, 05);
		value = 0;
		return 1;

	}

	public void removeAll() {
		c.removeAll();
	}

	@Override
	public void setEnabled(boolean enabled) {

		super.setEnabled(enabled);
		c.setEnabled(enabled);
		lb.setEnabled(enabled);
	}



	

	public void updatelabel(String s) {

	}

	public void updateTooltips(String s) {

	}

	public int getValue() {
		return value;
	}

	public String getValueString()
	{		
		return null;
	}

	public Object getValueObject() {		
		return null;
	}
	
	public void setEnableCombo(boolean b) 
	{
		lb.setEnabled(b);
		c.setEnabled(b);
	}

	public void setSelectCombo(int n) {
		c.select(n);
	}

	public TypeValue getTpvalue()
	{
		return tpvalue;
	}

	public void setTpvalue( TypeValue tpvalue )
	{
		switch (tpvalue) {
			case T:
				c.select(2);
				value=2;
				lb.setForeground(ColorConstants.darkGreen);
				c.setEnabled(false);
				break;
			case F:
				c.select(1);
				value=1;
				c.setEnabled(false);
				lb.setForeground(ColorConstants.darkGreen);
				break;
			case X:
				c.select(0);
				value=0;
				c.setEnabled(true);
				lb.setForeground(ColorConstants.black);
				break;
			case x:
				c.select(0);
				value=0;
				c.setEnabled(true);
				lb.setForeground(ColorConstants.lightBlue);
				break;
			default:
				break;
		}
		this.tpvalue = tpvalue;
	}

	public void addCombo(String string) {
		c.add(string);

	}

	public void removeCombo(int id) {
		c.remove(id);

	}

	public void addSelectionListenerCombo(SelectionListener listener) {
		c.addSelectionListener(listener);

	}

	@Override
	public String getToolTipText() {

		return c.getToolTipText();
	}

	@Override
	public void setToolTipText(String string) {

		c.setToolTipText(string);
	}


	@Override
	public void pack() {

		lb.pack();
		c.pack();
	}

	@Override
	public void redraw() {
		lb.redraw();
		c.redraw();
	}

	@Override
	public void update() {
		lb.update();
		c.update();
		super.update();
	}

	protected void setValue( int value )
	{ 
		this.value = value;
		c.select(value);		
	}

	
}
