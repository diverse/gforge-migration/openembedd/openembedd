/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.wave.dialog;

import java.util.ArrayList;
import java.util.List;

import net.sf.javabdd.BDD;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import fr.inria.base.MyManager;
import fr.inria.wave.Clock;

public class SimInterDialog extends Dialog implements ISimulationDialog {

	public class MyButton2 implements Listener {

		int pr;

		MyButton2(int n)
		{
			pr = n;
		}

	
		public void handleEvent( Event event )
		{
			postid = pr;
			event.doit = true;
		}

	}

	private BDD solution;
	private BDD input;

	public BDD getSolution()
	{
		return solution;
	}

	public void setInput( BDD input )
	{
		solution = input.getFactory().one();
		this.input = input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.inria.wave.dialog.SimulationDialog#modifyClock(int)
	 */

	public int modifyClock( int id , int value , int oldvalue )
	{
		MyManager.println("--id " + id + " " + value);

		solution = input.getFactory().one();
		for (CbooleanCombo cb : cbc)
		{
			int n = cb.getValue();
			int x = cb.getTpvalue().getN();
			//MyManager.println("les" + cb + " ; " + n + " ; " + x);
			if (x >= 2)
			{
				switch (n) {
					case 1:
						solution = solution.and(input.getFactory().ithVar(cb.getId()).not());
						break;
					case 2:
						solution = solution.and(input.getFactory().ithVar(cb.getId()));
						break;
					default:
						break;
				}
			}

		}
		//MyManager.println("next :" + solution);
		BDD result = input.and(solution);
		MyManager.println("valid V1 : ?" + !result.isZero());
		if (result.isZero())
		{
			MyManager.println("ERRROR .........................................");
		} else
		{
			if (value!=0)
				validatation(id);
		}

		return 0;
	}

	public int validatation(int id) 
	{
	for( CbooleanCombo cb:cbc)
	{
		if ( cb.getId()==id)
			continue;
		int n=cb.getValue();
		int x=cb.getTpvalue().getN();
	//	MyManager.println("les" +cb +" ; " + n + " ; "+ x);
		if ((x>=2) &&  (n==0))
		{
			BDD var=input.getFactory().ithVar(cb.getId());
			BDD test=solution.and(var.not()).and(input);
			if (test.isZero())
			{
				cb.setValue(2);
			}
			test=solution.and(var).and(input);
			if (test.isZero())
			{
				cb.setValue(1);
			}
			
		} 
	
	}
	return 0;
	}

	public List<CbooleanCombo> cbc = new ArrayList<CbooleanCombo>();
	Label lb = null;
	public int postid = -1;

	public SimInterDialog(Shell parentShell)
	{
		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.CLIP_CHILDREN | SWT.CLIP_SIBLINGS);

		setBlockOnOpen(false);
	}

	@Override
	protected Control createDialogArea( Composite parent )
	{

		// Group composite = new Group(parent);
		parent.setLayout(new GridLayout(1, true));
		Label lb = new Label(parent, SWT.FILL);
		lb.setText("Choose Clock :");

		Group composite = new Group(parent, SWT.NONE);
		composite.setLayout(new GridLayout(4, true));
		MyManager.println("-->" + lclk + " " + lclk.size());
		for (Clock c : lclk)
		{
			cbc.add(new CbooleanCombo(composite, c.getName(0), "", c.id, this));
		}
		composite.pack();
		return parent;
	}

	Button random;
	//Button force0;
	//Button force1;
	Button next; 
	
	@Override
	protected void createButtonsForButtonBar( Composite parent )
	{
		
		random = createButton(parent, 1025, "Random", false);
		random.addListener(SWT.Selection, new MyButton2(0));
		/*force0 = createButton(parent, 1024, "Force 0", true);
		force0.addListener(SWT.Selection, new MyButton2(0));
		force1 = createButton(parent, 1024, "Force 1", true);
		force1.addListener(SWT.Selection, new MyButton2(0));*/
		next = createButton(parent, 1024, "Next", true);
		next.addListener(SWT.Selection, new MyButton2(1));

	}

	List<Clock> lclk;

	public void setLclock( List<Clock> lclkin )
	{
		this.lclk = lclkin;
		MyManager.println("-->" + lclk + " " + lclk.size());

	}

	@Override
	public void create()
	{

		super.create();
	}

}
