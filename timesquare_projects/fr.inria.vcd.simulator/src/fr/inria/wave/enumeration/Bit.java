/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.enumeration;

import fr.inria.vcd.model.Value;
import fr.inria.wave.VCDfile;

public enum Bit {

	F("F", "0", "bZ ", 0), T("T", "1", "bX ", 1), Z("Z", "Z", "Z ", 2), X("X",
			"X", "X ", 3);
	// L("L", "bZ ", 4), H("H", "bX ", 5);

	String s;
	String vcd;
	String vcd2;
	int n;
	protected static int conflict = 0;

	private Bit(String arg0, String vcdcode, String vcdcode2, int arg1) {
		s = arg0;
		n = arg1;
		vcd = vcdcode;
		vcd2 = vcdcode2;
	}

	@Override
	public String toString() {
		return s;
	}

	public Bit ressolve(Bit b1) {
		conflict = 0;
		Bit result;
		switch (this) {
		case F:
			switch (b1) {
			case F:
				result = F;
				break;
			case T:
				conflict = 1;
				result = X;
				break;
			/*
			 * case L: result = L; break; case H: result = H; // is this
			 * sensible? break;
			 */
			default:
				conflict = 1;
				result = b1;
			}
			break;
		case T:
			switch (b1) {
			case T:
				// no conflict
				result = T;
				break;
			case F:
				conflict = 1;
				result = X;
				break;
			/*
			 * case L: result = L; // is it sensible? break; case H: result = H;
			 * break;
			 */
			default:
				conflict = 1;
				result = b1;
			}
			break;
		case Z:
			result = Z; // default
			switch (b1) {
			case Z:
				// no conflict
				break;
			case X:
				result = X;
				// no break
			default:
				conflict = 1;
			}
			break;
		case X:
			result = X; // absorbant
			switch (b1) {
			case X:
				// no conflict
				break;
			default:
				conflict = 1;
			}
			break;
		/*
		 * case L: switch (b1) { case X: case Z: conflict = 1; result = b1;
		 * break; default: result = L; } break; case H: switch (b1) { case X:
		 * case Z: conflict = 1; result = b1; break; default: result = H; }
		 * break;
		 */
		default:
			// should never get here

			VCDfile.getCourant().msgOut("illegal Bit in BIT_Resolve");
			VCDfile.getCourant().trace("illegal Bit in BIT_Resolve");
			VCDfile.getCourant().errorMsg("illegal Bit in BIT_Resolve", 90);
			return null;
		}

		return result;
	}

	/**
	 * @return the vcd
	 */
	/*
	 * public String getVcd() { return vcd; }
	 */

	public String getVcd(boolean istimer) {
		if (istimer)
			return vcd2;
		return vcd;
	}

	public boolean is0or1() {
		if (n <= 1)
			return true;
		return false;
	}

	public Value convertValue(boolean istimer) {
		if (istimer) {
			if (this == F)
				return Value._z;
			if (this == T)
				return Value._x;
		}
		if (this == F)
			return Value._0;
		if (this == T)
			return Value._1;
		if (this == Z)
			return Value._z;
		// if (this==X)
		return Value._x;

	}

	public String convertString(boolean istimer) {
		if (istimer) {
			if (this == F)
				return "Z";
			if (this == T)
				return "X";
		}
		if (this == F)
			return "0";
		if (this == T)
			return "1";
		if (this == Z)
			return "Z";
		// if (this==X)
		return "X";

	}
}
