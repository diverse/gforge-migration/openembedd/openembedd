/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.enumeration;

public enum ClockKind {

	CK_unknown("unknown", 0), CK_clock("clock", 1), CK_timer("timer", 2), CK_illegal(
			"illegal", 3);

	String s;
	int n;

	private ClockKind(String arg0, int arg1) {
		s = arg0;
		n = arg1;
	}

	@Override
	public String toString() {
		return s;
	}

	public ClockKind resolve(ClockKind x) {
		switch (n) {
		case 0:
			return x;

		case 1:
			if (x.n <= 1)
				return CK_clock;
			return CK_illegal;

		case 2:
			if ((x.n == 0) || (x.n == 2))
				return CK_timer;
			return CK_illegal;

		case 3:
			return CK_illegal;

		}
		return CK_unknown;
	}

	public String gettype() {
		switch (this) {
		case CK_unknown:
			return "unknown ";

		case CK_clock:
			return "wire ";

		case CK_timer:
			return "tri ";
		case CK_illegal:
			return "null ";
		}
		return null;

	}

}
