/**
 * 
 */
package fr.inria.wave.enumeration;

/**
 * @author Benoit Ferrero
 *
 */
public enum Moderator {
	strictly(0," strictly ",false), 
	leftWeakly(1," leftWeakly ",true),
	rightWeakly(2," rightWeakly ",true),
	weakly(3," weakly ",false);  	
	
	private Moderator(int n, String name, boolean ext) {
		this.n = n;
		this.name = name;
		extens=ext;
	}
	
	private final int n;
	private final String name;
	private final boolean extens;
	
	public int getN() {
		return n;
	}
	public String getName() {
		return name;
	}
	public boolean isExtens() {
		return extens;
	}
	
	public boolean is_leftWeakly()
	{
		if (n==3) return true; //weakly
		if (n==1) return true; //leftWeakly
		return false;
	}	

	public boolean is_rightWeakly()
	{
		if (n==3) return true; //weakly
		if (n==2) return true; //rightWeakly
		return false;
	}
	
	public boolean is_strictly()
	{
		return (n==0);
	}
}
