/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.enumeration;

import fr.inria.wave.VCDfile;

public enum ClockStatus {
	CS_unknown("unknown", 0), CS_forbidden("forbidden", 1), CS_finished(
			"finished", 2), CS_disabled("disabled", 3), CS_enabled("enabled", 4), CS_fireable(
			"fireable", 5), CS_mandatory("mandatory", 6);

	String s;
	int n;
	private static int conflict = 0;

	private ClockStatus(String arg0, int arg1) {
		s = arg0;
		n = arg1;
	}

	@Override
	public String toString() {
		return s;
	}

	public ClockStatus resolve(ClockStatus cs) {
		ClockStatus result;
		conflict = 0;
		switch (this) {
		case CS_unknown:
			result = cs;
			break;
		case CS_forbidden:
			result = CS_forbidden; // absorbant
			switch (cs) {
			case CS_forbidden:
				break;
			default:
				conflict = 1;
			}
			break;
		case CS_finished:
			result = CS_finished; // default
			switch (cs) {
			case CS_finished:
				// no conflict
				break;
			case CS_forbidden:
				result = CS_forbidden;
				// no break
			default:
				conflict = 1;
			}
			break;
		case CS_disabled:
			switch (cs) {
			case CS_forbidden:
			case CS_finished:
				conflict = 1;
				result = cs;
				break;
			case CS_unknown:
				result = CS_disabled;
				break;
			default:
				result = cs;
			}
			break;
		case CS_enabled:
			switch (cs) {
			case CS_forbidden:
			case CS_finished:
				conflict = 1;
				result = cs;
				break;
			case CS_unknown:
			case CS_disabled:
				result = CS_enabled;
				break;
			default:
				result = cs;
			}
			break;
		case CS_fireable:
			switch (cs) {
			case CS_forbidden:
			case CS_finished:
			case CS_disabled:
				conflict = 1;
				result = cs;
				break;
			case CS_unknown:
			case CS_enabled:
				result = CS_fireable;
				break;
			default:
				result = cs;
			}
			break;
		case CS_mandatory:
			switch (cs) {
			case CS_forbidden:
			case CS_finished:
				conflict = 1;
				result = cs;
				break;
			default:
				result = CS_mandatory;
			}
			break;
		default:
			// should never get here
			VCDfile.getCourant().trace(
					"illegal ClockStatus in ClockStatus_Resolve");
			VCDfile.getCourant().msgOut(
					"illegal ClockStatus in ClockStatus_Resolve");
			VCDfile.getCourant().errorMsg(
					"illegal ClockStatus in ClockStatus_Resolve", 80);

			return CS_forbidden;
		}

		
		return result;

	}

	/**
	 * @return the conflict
	 */
	static public int getConflict() {
		return conflict;
	}

}
