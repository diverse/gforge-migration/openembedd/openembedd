/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.enumeration;

public enum ActionKind {

	AK_none("none", 0), AK_load("load", 1), AK_dec("dec", 2);

	String s;
	int n;

	private ActionKind(String arg0, int arg1) {
		s = arg0;
		n = arg1;
	}

	@Override
	public String toString() {
		return s;
	}
}
