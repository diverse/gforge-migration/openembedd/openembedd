/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class RestrictToInterval extends Constraint {

	private int from, to;
	private Clock superc = null;
	private Clock sub = null;

	public RestrictToInterval(Clock superc, Clock sub, int from, int to)
			throws Exception {
		super();
		this.superc = superc;
		this.sub = sub;
		this.from = from;
		this.to = to;
		if (sub == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		superc.setKind(ClockKind.CK_clock);
		sub.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {
		// int nbClocks;
		int idxSup;
		idxSup = (int) superc.getIdx() + 1;
		if (idxSup < from) {
			forbidBDD(sub);
			//linkTo(sub, superc, from);
		} else {
			if (idxSup <= to)
				coincidenceBDD(superc, sub);
			else
				//dieCstr(sub);
				forbidBDD(sub);
				
		}

		return 0;
	}

}
