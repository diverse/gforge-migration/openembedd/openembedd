/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.proba.IDParam;

/**
 * @author Benoit Ferrero
 * 
 */
public class OneShot extends Constraint {

	protected Clock timer, superc, start, timeout;
	protected int duration=0;
	protected IDParam random=null; 
	protected int _reste;
	protected int _lastclkid;
	protected boolean _running;
	protected int _laststart;

	public OneShot(Clock timer, Clock superc, Clock start, Clock timeout,
			int duration) throws Exception {
		super();
		this.timer = timer;
		this.superc = superc;
		this.start = start;

		this.timeout = timeout;
		this.duration = duration;
		if (timer == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
		if (start == null)
			throw new Exception("null");

		if (timeout == null)
			throw new Exception("null");
		if (duration < 0)
			throw new Exception("negative");
		_reste = 0;
		_lastclkid = 0;
		_running = false;
		_laststart = 0;
	}
	
	public OneShot(Clock timer, Clock superc, Clock start, Clock timeout,
			IDParam rand) throws Exception {
		super();
		this.timer = timer;
		this.superc = superc;
		this.start = start;

		this.timeout = timeout;
		this.random = rand ;
		if (timer == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
		if (start == null)
			throw new Exception("null");

		if (timeout == null)
			throw new Exception("null");
		if (rand==null)
			throw new Exception("null");
		_reste = 0;
		_lastclkid = 0;
		_running = false;
		_laststart = 0;
	}


	@Override
	public int check() {

		timer.setKind(ClockKind.CK_timer);
		superc.setKind(ClockKind.CK_clock);
		start.setKind(ClockKind.CK_clock);
		timeout.setKind(ClockKind.CK_clock);
		return 0;
	}

	/**
	 * @see wave.constraint.Constraint#first()
	 */
	@Override
	public int first() {
		timer.forceBit(Bit.F); // BIT.L
		//forbidBDD(timeout);
		//forbidBDD(timer);
		
		return 0;
	}

	public void startBDD()
	{
		forbidBDD(timeout);
		coincidenceBDD(start,timer );
	}
	
	public void runBDD()
	{
		present(timer);		
		forbidBDD(timeout);	
	}
	
	public void endBDD()
	{			
		coincidenceBDD(superc, timeout);
		xorBDD(superc, timer);	
	}
	
	protected void start()
	{
		if (duration!=0)
			_reste = duration ;
		else
			_reste= random.getNextValue();
		_laststart = (int) start.getIdx();
		_lastclkid = (int) superc.getIdx();
		_running=true;	
	}
	
	protected boolean isStarted()
	{
		if ((start.getIdx() != _laststart)) 
		{
			start();
		return true;
		}
		return false ;  
	}
	
	
	
	@Override
	public int run() {	
		boolean flag=false;	
		if ( _running )
		{
				
			if (start.getIdx() != _laststart)
			{		
			_laststart = (int) start.getIdx();
			flag =true;
			}
			if (_reste == 0)
			{
				_running = false;
				if (flag)
				{			
					start();
					runBDD();
					return 0;
				}	
				startBDD();
				return 0;
			}
			if (_lastclkid != superc.getIdx()) 
			{
				_reste--;	
				_lastclkid = (int) superc.getIdx();					
			}
			if (_reste==1)
			{
				endBDD();
				return 0 ;
			}
			if (_reste==0)
			{
				if (flag)
				{	
					start();
					runBDD();
					return 0;
				}	
				startBDD();
				return 0 ;
			}
			runBDD();
			return 0;
		}	
		
		if (isStarted())
		{
			if (_reste==1)
			{
				endBDD();	
				return 0;
			}			
			runBDD();
			return 0;
		}	
		startBDD();
		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		if (n != 0)
			return null;
		String s = "oneShot";
		if (duration!=0)
		return new ConstraintCommentCommand(s, timer.getName(0),
				Function._oneshoton, convert(new String[] { superc.getName(0),
						start.getName(0), timeout.getName(0), "" + duration }));
		return new ConstraintCommentCommand(s, timer.getName(0),
				Function._oneshoton, convert(new String[] { superc.getName(0),
						start.getName(0), timeout.getName(0), "" + random }));
	}

}
