/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.comment.ConstraintDecodeComment;
import fr.inria.wave.Clock;
import fr.inria.wave.data.BW;
import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.enumeration.ClockKind;

public class FilteringBy extends Constraint {

	//PARAM
	private BW bw;
	private Clock superc = null;
	private Clock sub = null;
	//DATA
	
	//CODE
	public FilteringBy(Clock superc, Clock sub, BW bw) throws Exception {
		super();
		this.superc = superc;
		this.sub = sub;
		this.bw = bw;
		if (bw == null)
			throw new Exception("null");
		if (sub == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		superc.setKind(ClockKind.CK_clock);
		sub.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {

		//int egIdx;

		int idxsup = (int) superc.getIdx() + 1; // consider next instant
		Bit b = bw.kthBit(idxsup);

		switch (b) {
		case Z: // no longer filtering
			getSim().log("**** " + getSim().getDate() + 1 + "  " + superc.name 	+ " is finished. No future tick expected.\n");		
			forbidBDD(sub);			
			break;
		case T:
			coincidenceBDD(superc, sub);
			break;
		case F:		
			forbidBDD(sub);			
			break;
		default:
			// nothing to do			
		}

		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		if (n != 0)
			return null;
		/*String s = sub.name + " = " + superc.name + " " + Function._filteredby
				+ " " + bw.toString();
		ConstraintCommentCommand cc= new ConstraintCommentCommand(s, sub.name, Function._filteredby,
				convert(new String[] { superc.name, bw.toString() }));
		cc.setIc(  new ConstraintFilteredBy());*/
		return ConstraintDecodeComment.filteredBy(superc.getName(0), sub.getName(0), bw.toString());
	}

}
