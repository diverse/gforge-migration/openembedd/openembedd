/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class Union extends Constraint {
	private Clock union, clka, clkb;

	/**
	 * @param union = clka Union clkb
	 * @param clka
	 * @param clkb
	 */
	public Union(Clock union, Clock clka, Clock clkb) {
		super();
		this.union = union;
		this.clka = clka;
		this.clkb = clkb;
	}

	@Override
	public int check() {
		union.setKind(ClockKind.CK_clock);
		clka.setKind(ClockKind.CK_clock);
		clkb.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {
		unionBDD(union, clka, clkb);
	
		return super.run();
	}

}
