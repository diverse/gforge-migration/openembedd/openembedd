/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.constraint.ConstraintSync;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.Moderator;

public class Sync extends Constraint {

	private Clock left = null;
	private Clock right = null;
	private int alpha, beta;
	private Moderator md;

	public Sync(Clock left, Clock right, int idr, int idl, Moderator mdin)
			throws Exception {
		super();
		this.left = left;
		this.right = right;
		this.alpha = idr;
		this.beta = idl;
		if (mdin == null)
			this.md = Moderator.strictly;
		else
			this.md = mdin;
		if (alpha == 0)
			throw new Exception("alpha : 0");
		if (beta == 0)
			throw new Exception("beta  : 0");
		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");
	}

	public Moderator getMd() {
		return md;
	}

	@Override
	public int check() {

		super.check();
		left.setKind(ClockKind.CK_clock);
		right.setKind(ClockKind.CK_clock);

		return 0;
	}

	@Override
	public int run() {

		int idxLeft, idxRight;
		//int q;// ,r;
		idxLeft = (int) left.getIdx() ; // consider next instant
		idxRight = (int) right.getIdx() ;
		int ql =idxLeft / alpha;
		int rl= idxLeft % alpha;
		int qr =idxRight / beta;
		int rr= idxRight % beta;
		
		if (ql<qr)
		{
			if (md.is_leftWeakly())
			{
				if (rl==alpha-1 && rr==0)
					implyBDD(right, left);
				else
					forbidBDD(right);
			}
			else
			{
			forbidBDD(right);
			}
			
		}
		if (ql==qr)
		{
			//nothing
			
		}
		if (ql>qr)
		{
			if (md.is_rightWeakly())
			{
				if (rr==beta-1 && rl==0)
					implyBDD(left,right);
				else
					forbidBDD(left);
			}
			else
			{
			forbidBDD(left);
			}
		}
		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		String s = left.name + " by " + alpha + " " + " synchronizesWith "
				+ right.name + " by " + beta;
		if (n >= 1)
			return null;
	/*	if (n == 1)
			return new ConstraintCommentCommand(s, left.name,
					Function._synchronizeswith, convert(new String[] {
							right.name, "" + alpha, "" + beta }));*/
		ConstraintCommentCommand cc= new ConstraintCommentCommand(s, right.name,
				Function._synchronizeswith, convert(new String[] { left.name,
						"" + beta, "" + alpha }));
		 cc.setIc(  new ConstraintSync());
			return cc;
	}
}
