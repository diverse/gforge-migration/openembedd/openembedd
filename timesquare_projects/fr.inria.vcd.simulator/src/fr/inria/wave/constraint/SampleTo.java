/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.comment.ConstraintDecodeComment;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class SampleTo extends Constraint {

	private boolean strict;

	private Clock sub;
	private Clock superc = null;
	private Clock trigger = null;

	// BitSet toupdate;
	public SampleTo(Clock superc, Clock trigger, Clock sub, boolean strict) {
		super();
		//	System.out.println("---->>" +superc.getName(0)+" " +trigger.getName(0) +" " +sub.getName(0) + " " +strict);
		//System.out.println("---->>" +superc.id+" " +trigger.id +" " +sub.id+ " " +strict);
		this.superc = superc;
		this.trigger = trigger;
		this.strict = strict;
		this.sub = sub;

	}

	@Override
	public int check() {

		super.check();
		superc.setKind(ClockKind.CK_clock);
		trigger.setKind(ClockKind.CK_clock);
		sub.setKind(ClockKind.CK_clock);

		return 0;
	}

	

	@Override
	public ConstraintCommentCommand localcomment(int n) {

		if (n != 0)
			return null;
		// return null;
		
		return ConstraintDecodeComment.sampledOn(superc.getName(0), sub.getName(0), trigger.getName(0), !strict);
	}

	@Override
	public int run() {
		if (trigger == superc) {
			int idxsup = (int) superc.getIdx() + 1;
			if ((strict) && (idxsup == 1))
			{
				//linkTo(sub, superc, 2);				;
				 forbidBDD(sub);
			}
			else
			{
				coincidenceBDD(sub, superc );//, (int) sub.getIdx() + 1, idxsup + 1);
			
			}
			return 0;
		}
		if (strict) // strict triger av superc
		{ 
			long udt = trigger.getUniversaldate();
			long udc = sub.getUniversaldate();			
			if (udt > 0 && (udt >= udc)) {
				// triger apres sub
				coincidenceBDD(superc, sub) ;//, (int) superc.getIdx() + 1, (int) sub.getIdx() + 1);
				
			} 
			else {
				// triger avtn sub
				// after(superc,sub,(int)superc.getIdx()+1);
			//	linkTo(sub, superc, (int) superc.getIdx() + 2);
				//sim.bddAdd(sub.getVarclock().not());
				
				// after(trigger, sub,(int)trigger.getIdx()+1);
				 forbidBDD(sub);
			}

			return 0;
		}

		long udt = trigger.getUniversaldate();
		long udc = sub.getUniversaldate();
		if (udt != 0 && (udt > udc)) {
			coincidenceBDD(sub,superc);
			//coincidence(sub, superc, (int) sub.getIdx() + 1, (int) superc	.getIdx() + 1);
			
			// forbidBDD(sub);
		} else {
			
			interBDD(sub, trigger, superc);
			
			
		}
		return 0;

	}

}
