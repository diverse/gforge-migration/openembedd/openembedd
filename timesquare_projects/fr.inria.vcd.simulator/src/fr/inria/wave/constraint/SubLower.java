/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class SubLower extends Constraint {
	private Clock left, first, second;

	public SubLower(Clock left, Clock first, Clock second) throws Exception {
		super();
		this.left = left;
		this.first = first;
		this.second = second;
		if (left == null)
			throw new Exception("null");
		if (first == null)
			throw new Exception("null");
		if (second == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		left.setKind(ClockKind.CK_clock);
		first.setKind(ClockKind.CK_clock);
		second.setKind(ClockKind.CK_clock);
		return super.check();
	}

	@Override
	public int run() {

		if (first == second) {
			//int idxLeft, idxFirst;
		//	idxLeft = (int) left.getIdx() + 1; // consider next instant
			//idxFirst = (int) first.getIdx() + 1;
			coincidenceBDD(left, first);
			// in this case idxLeft = idxFirst
			return 0;
		}

		// usual case
		int idxLeft = (int) left.getIdx() + 1;
		int idxFirst = (int) first.getIdx() + 1;
		int idxSecond = (int) second.getIdx() + 1;

		// note that idxLeft <= idxFirst, idxSecond
		if (idxLeft < idxFirst) {
			// first has already occurs
			if (idxLeft == idxSecond)
				coincidenceBDD(left, second);
			// else: impossible
		} else {
			// idxLeft == idxFirst
			if (idxLeft < idxSecond)
				coincidenceBDD(left, first);
			else {

				interBDD(left, first, second);

			}
		}
		return 0;
		// return super.run();
	}

}
