/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.Moderator;

public class Precedes extends Constraint {

	private Clock left = null;
	private Clock right = null;
	int alpha = 1;
	int beta = 1;
	boolean extend = false;
	Moderator md;

	public Precedes(Clock left , Clock right, Moderator mdin) throws Exception {
		super();
		extend = false;
		this.left = left;
		this.right = right;		
		md = mdin;
		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");
	}

	public Precedes(Clock left, Clock right, int a, int b, Moderator mdin)
			throws Exception {
		super();
		this.left = left;
		this.right = right;
		md = mdin;	
		alpha = a;
		beta = b;
		if (a != 1 || b != 1) {
			extend = true;
		} else {
			extend = false;
		}

		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		left.setKind(ClockKind.CK_clock);
		right.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {

		int idxRight = (int) right.getIdx() + 1;
		int idxLeft = (int) left.getIdx() + 1;		
		if (beta==1 && alpha==1)
		{
			if (md.is_strictly() )
			{
				if (idxRight==idxLeft)
					forbidBDD(right);
				return 0;
			}
			if (idxRight==idxLeft)	
				//	forbidBDD(right);
				implyBDD( right,left);
				
			return 0;
		}
	//	int r=0;
		int q=0;
		int lastleft ;
		int firstrigth ;
		if (alpha ==1)
		{
			lastleft= idxLeft;
			q=idxLeft-1;
			//r=0;
		}
		else
		{
			//r= (idxLeft ) %alpha ; 
			q=( idxLeft-1) / alpha;
			lastleft= ( q ) * alpha ;
		}
		firstrigth= (q ) *beta +1;	
		
		if (md.is_strictly())
		{
			if (idxRight == firstrigth )	
			{			
				
					forbidBDD(right);			
			}
			return 0;
		}
		if (idxRight == firstrigth &&  lastleft!= idxLeft)	
		{		
			forbidBDD(right);			
		}
		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		String s1 = left.name + " " + Function._precedes + " " + right.name +" "+md +" "+ alpha +" "+ beta ;
	//	String s2 = right.name + " " + Function._precedes + " " + left.name;
		if (n >= 1)
			return null;
		/*if (n == 1)
			return new ConstraintCommentCommand(s1, left.name,
					Function._precedes, convert(new String[] { right.name }));*/
		return new ConstraintCommentCommand(s1, right.name, Function._precedes,
				convert(new String[] { left.name , ""+ alpha , ""+beta}));
		// return right.name + " alternate  " + left.name;

	}

}
