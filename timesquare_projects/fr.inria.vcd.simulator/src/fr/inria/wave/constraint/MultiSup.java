package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;

public class MultiSup extends CompositeConstraint {
	private Clock left ;
	private Clock listscr[];	 
	private Clock interal[];
	
	
	public MultiSup(Clock left, Clock[] listscr) throws Exception
	{
		super();
		this.left = left;
		this.listscr = listscr;
		if (listscr.length<2) throw new Exception("Size to short");
	}

	@Override
	public int run( Simulation sim , int n ) throws Exception
	{
	
		int size=listscr.length-2;
		interal = new Clock[size ];
		for (int i=0; i<size ;i++)
		{
		 	interal[i]=sim.getLc().createClock("__internalclock_" + n+"_"+i, true);
		 	interal[i].setHiden(true);
		 }
		array.add(new SubLower(interal[0] , listscr[0], listscr[1]));
	  	for (int i=1; i<size ;i++)
		{
	  		array.add(new SubLower(interal[i] , interal[i-1], listscr[i+1]));
			
		}
	  	array.add(new SubLower(left , interal[size-1], listscr[size+1]));
		return 0;
		
	}
	
	
}
