/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class SubClock extends Constraint {

	private Clock superc = null;
	private Clock sub = null;

	public SubClock(Clock superc, Clock sub) throws Exception {
		super();
		this.superc = superc;
		this.sub = sub;
		if (sub == null)
			throw new Exception("SubClock arg sub(0) is null");
		if (superc == null)
			throw new Exception("SubClock arg  super (1) is null");
	}

	@Override
	public int check() {

		super.check();
		superc.setKind(ClockKind.CK_clock);
		sub.setKind(ClockKind.CK_clock);

		return 0;
	}

	@Override
	public int run() {
		return implyBDD(sub ,superc);
	//	return linkTo(sub, superc, 0);
	}
}
