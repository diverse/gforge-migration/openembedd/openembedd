/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class Minus extends Constraint {
	private Clock minus, clka, clkb;

	/**
	 * @param minus = clka - clkb;
	 * @param clka
	 * @param clkb
	 */
	public Minus(Clock minus, Clock clka, Clock clkb) throws Exception {
		super();
		this.minus = minus;
		this.clka = clka;
		this.clkb = clkb;
		if (minus == null)
			throw new Exception("null");
		if (clka == null)
			throw new Exception("null");
		if (clkb == null)
			throw new Exception("null");
	}

	@Override
	public int check() {
		minus.setKind(ClockKind.CK_clock);
		clka.setKind(ClockKind.CK_clock);
		clkb.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {
		minusBDD(minus, clka, clkb);
		return 0;
	}

}
