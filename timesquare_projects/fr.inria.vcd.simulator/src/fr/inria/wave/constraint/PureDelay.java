package fr.inria.wave.constraint;

import java.util.TreeSet;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.proba.IDParam;

public class PureDelay extends Constraint {

	
	
	private Clock sup;
	private Clock trig;
	private Clock timeout;
	private int duration=0 ;
	private IDParam dure=null; 
	int lastid=-1;
	private TreeSet<Integer> t= new TreeSet<Integer>();

	
	public PureDelay( Clock supin,Clock trigin, Clock timeoutin, int durationin) throws Exception {
		super();
		this.duration = durationin+1;
		this.sup = supin;
		this.timeout = timeoutin;
		this.trig = trigin;

		if (sup == null)
			throw new Exception("null");
		if (timeout == null)
			throw new Exception("null");
		if (trig == null)
			throw new Exception("null");
		
	;
	
		if (duration <=0)
			throw new RuntimeException("duration <0 ");
		
		
		
	}
	public PureDelay( Clock supin,Clock trigin, Clock timeoutin, IDParam dure) throws Exception {
		super();
		this.dure=dure;
		this.sup = supin;
		this.timeout = timeoutin;
		this.trig = trigin;

		if (sup == null)
			throw new Exception("null");
		if (timeout == null)
			throw new Exception("null");
		if (trig == null)
			throw new Exception("null");
		
		
		if (dure ==null)
			throw new RuntimeException("IDParam == null ");
	}
	
	
	@Override
	public int check() {
		super.check();
		sup.setKind(ClockKind.CK_clock);
		trig.setKind(ClockKind.CK_clock);
		timeout.setKind(ClockKind.CK_clock);
		
		return 0;
	}

	
	

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		if (n==0)
		{
			return super.localcomment(n);
		}
		return null;
	}


	private int ltr=0, lti=0, lsup=0;
	
	int next=-1;
	@Override
	public int run() {
		int  a,b,c;
		a=(int) sup.getIdx();
		b= (int)trig.getIdx();
		c= (int ) timeout.getIdx();
		if (a!=lsup)
		{
			if (c!=lti)
			{
				//t.pollFirst(); //TODO change and verify
				t.remove(t.first()); 
			}
			
		}
		if (b!=ltr)
		{		
			int newdate=0;
			if (duration!=0)
			{
				newdate=lsup+duration;
			}
			if (dure!=null)
			{
				newdate=lsup+dure.getNextValue();
			}
			t.add(newdate);
		
		}	
		if (!t.isEmpty() && (a+1 )==t.first())
		{
			//coincidence(sup, timeout, a+1, c+1);
			coincidenceBDD(sup, timeout);
		}
		else
		{
			forbidBDD(timeout);
			//linkTo(timeout, sup, time[0]);
		}
		
		lsup=a;
		ltr=b;
		lti=c;
		
		return super.run();
	}

	

}
