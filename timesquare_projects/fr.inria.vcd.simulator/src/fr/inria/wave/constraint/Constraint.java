/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import java.util.ArrayList;

import net.sf.javabdd.BDD;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;

public abstract class Constraint {
	private BDD bddlocal;
	private Simulation sim = null;
	private int idcst = -1;

	public int getIdcst() {
		return idcst;
	}

	public Constraint() {
		super();
	}

	public int check() {
		return 1;
	}

	public int first() {
		return 0;
	}

	public int run() {

		return 1;
	}

	protected final Simulation getSim() {
		return sim;
	}

	public void setSim(Simulation simul, int n) throws Exception {
		if (sim==null)
		{
			this.sim = simul;
			idcst = n;
		}		
	}
	
	public void cleanBDD(){
		bddlocal=sim.bddf.one();
	}
	
	/**
	 * @return the bddlocal
	 */
	public BDD getBddlocal()
	{
		return bddlocal;
	}
	
	protected final void bdd2sim(BDD b){
		bddlocal=bddlocal.and(b);
		sim.bddAdd(b);
	}

	public ConstraintCommentCommand localcomment(int n) {
		return null;
	}

	public Clock[] getClock() {
		return null;
	}

	final public String comment() {
		int n = 0;
		String r = "";		
		String s = "";
		String s1 = "";
		ConstraintCommentCommand cs = null;
		do {
			cs = localcomment(n++);
			if (cs == null)
				break;

			r += "$comment constraint " + cs.toString() + " $end";

			if (s1.compareTo(s) == 0)
				break;
			s1 = s;
		} while (true ); 
		return r;
	}

	final public ArrayList<String> convert(String ts[]) {
		ArrayList<String> l = new ArrayList<String>();
		for (String s : ts) {
			l.add(s);
		}
		return l;
	}
	
	final protected int unionBDD(Clock union, Clock a, Clock b) {
		BDD unionbdd;
		BDD bddu=union.getVarclock();
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();
		unionbdd = bddu.biimp(bdda.or(bddb));
		bdd2sim( unionbdd);
		return 0;	
	}
	
	final protected int interBDD(Clock inter, Clock a, Clock b) {
		BDD interbdd;
		BDD bddu=inter.getVarclock();
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();
		interbdd = bddu.biimp(bdda.and(bddb));
		bdd2sim( interbdd);
		return 0;	
	}
	
	final protected int minusBDD(Clock minus, Clock a, Clock b) {
		BDD minusbdd;
		BDD bddu=minus.getVarclock();
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();
		BDD inter=bdda.and(bddb);
		BDD unionbdd=inter.xor(bddu);
		minusbdd = bdda.biimp(unionbdd);
		//unionbdd.free();
		bdd2sim( minusbdd);		
		return 0;	
	}
	
	final protected int coincidenceBDD( Clock a, Clock b) {
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();
		
		bdd2sim( bdda.biimp(bddb));
		return 0;	
	}
	
	final protected int xorBDD( Clock a, Clock b){		
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();		
		bdd2sim( bdda.xor(bddb));
		return 0;	
	}
	
	final protected int disjointBDD( Clock a, Clock b) {		
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();		
		bdd2sim( bdda.and(bddb).not());
		return 0;	
	}
	
	final protected int implyBDD( Clock a, Clock b) {		
		BDD bdda=a.getVarclock();
		BDD bddb=b.getVarclock();		
		bdd2sim( bdda.imp(bddb));
		return 0;	
	}
	
	final protected void forbidBDD(Clock clk) {
		bdd2sim(clk.getVarclock().not());
	}
	
	final protected void present(Clock clk) {
		bdd2sim(clk.getVarclock());
	}
}
