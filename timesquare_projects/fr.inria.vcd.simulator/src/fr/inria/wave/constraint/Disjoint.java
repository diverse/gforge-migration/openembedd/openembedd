/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class Disjoint extends Constraint {

	//PARAM
	private Clock left = null;
	private Clock right = null;
	//DATA
	
	//CODE
	public Disjoint(Clock left, Clock right) throws Exception {
		super();
		this.left = left;
		this.right = right;
		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		left.setKind(ClockKind.CK_clock);
		right.setKind(ClockKind.CK_clock);

		return 0;
	}

	@Override
	public int run() {
		return disjointBDD(left,right);
		//return exclude(left, right);
	}

}
