package fr.inria.wave.constraint;

import java.util.ArrayList;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Simulation;

public abstract class CompositeConstraint  {
	protected ArrayList<Constraint> array=new  ArrayList<Constraint>(); 
	protected int localid = 0;	
	
	public CompositeConstraint(){
		super();		
	}
	
	/**
	 * @return the array
	 */
	public final ArrayList<Constraint> getArray(Simulation sim,int n) throws Exception{
		if ( array.size()==0)
		{			
			run(sim,n);
		}
		return array;
	}

	public abstract int run(Simulation sim,int n) throws Exception ;

	public ConstraintCommentCommand localcomment(int n) {	
		return null;
	}
}
