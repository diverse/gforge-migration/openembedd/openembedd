/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class Sustain extends Constraint {

	private Clock sustain, top, stop;

	/**
	 *  sustainclk = sustainclk top upto stopclk;
	 * @param sustainclk
	 * @param topclk
	 * @param stopclk
	 * @throws Exception
	 */
	public Sustain(Clock sustainclk, Clock topclk, Clock stopclk) throws Exception {
		super();
		this.sustain = sustainclk;// stutter
		this.top = topclk; // trigger  
		this.stop = stopclk; // killer
		if (sustain == null)
			throw new Exception("null");
		if (top == null)
			throw new Exception("null");
		if (stop == null)
			throw new Exception("null");
	}

	@Override
	public int check() {
		sustain.setKind(ClockKind.CK_clock);
		top.setKind(ClockKind.CK_clock);
		stop.setKind(ClockKind.CK_clock);
		return super.check();
	}

	
	@Override
	public int run() {
		if (top.getUniversaldate() <= stop.getUniversaldate())
		{
			//forbidBDD(sustain);
			//linkTo(a, b, 0);
			implyBDD(sustain, top);			
		}
		disjointBDD(sustain,stop);
		return 0;
	} 


	@Override
	public ConstraintCommentCommand localcomment( int n )
	{
		if (n>0) return null;
		String s = sustain.getName(0) + " = sustains " +  top.getName(0)+ " upto " + stop.getName(0) ;
	
		return new ConstraintCommentCommand(s, sustain.getName(0),
				Function._sustains, convert(new String[] { top.getName(0),
						stop.getName(0) }));

	}
	
	
}