/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.Moderator;

public class Alternates extends Constraint {

	//PARAM
	private Clock left = null;
	private Clock right = null;
	int alpha = 1;
	int beta = 1;
	boolean extend = false;
	Moderator md;
	//DATA
	
	//CODE
	public Alternates(Clock right, Clock left, Moderator mdin) throws Exception {
		super();
		extend = false;
		this.left = left;
		this.right = right;
		md = mdin;
		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");		
	}

	public Alternates(Clock right, Clock left, int a, int b, Moderator mdin)
			throws Exception {
		super();
		this.left = left;
		this.right = right;
		md = mdin;
		alpha = a;
		beta = b;
		if (a != 1 || b != 1) {
			extend = true;
		} else {
			extend = false;
		}

		if (left == null)
			throw new Exception("null");
		if (right == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		left.setKind(ClockKind.CK_clock);
		right.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {
		int idxLeft, idxRight;
		//int egIdx;
		// constrained = right
		idxRight = (int) right.getIdx() + 1; 
		idxLeft = (int) left.getIdx() + 1;// consider next instant		
		if ((beta == 1)&& (alpha==1 ) )
		{
			if (idxRight<idxLeft)
			{
				forbidBDD(left);
				return 0;
			}			
			if (idxRight>idxLeft)
			{
				forbidBDD(right);
				return 0;
			}
			switch (md) {
				case strictly:
				case rightWeakly:
					forbidBDD(left);
					break;
				case leftWeakly:				
				case weakly:
					implyBDD(left ,right);
			}
			return 0;
		}
			//TODO	
		/*if (beta == 1)		
				{
			
			egIdx = idxRight * alpha;
			if (md.is_leftWeakly())
				weakAfter(right, left, egIdx);
			else
				//after(right, left, egIdx);
				forbidBDD(right);

		} else {

			// left[k*alpha] op1 right[(k-1)*beta+1]
			if ((idxRight % beta) == 1) {
				int q = idxRight / beta; // q = k-1
				egIdx = (q + 1) * alpha;
				if (md.is_leftWeakly())
					weakAfter(right, left, egIdx);
				else
					//after(right, left, egIdx);
					forbidBDD(right);
			}
		}

		// constrained = left
		idxLeft = (int) left.getIdx() + 1; // consider next instant
		if (alpha == 1) {
			// right[k*beta] op2 left[k+1]
			// every occurrence of right is constrained but the first one
			if (idxLeft > 1)
			{
				egIdx = (idxLeft - 1) * beta;
				if (md.is_rightWeakly())
					weakAfter(left, right, egIdx);
				else
					//after(left, right, egIdx);
					forbidBDD(left);

			}
		} else {

			if ((idxLeft % alpha) == 1) {
				int q = idxLeft / alpha;
				egIdx = q * beta;
				if (md.is_rightWeakly())
					weakAfter(left, right, egIdx);
				else
					//after(left, right, egIdx);
					forbidBDD(left);

			}
		}*/

		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		//String s1 = left.name + " " + Function._alternateswith + " " + right.name;
		String s2 = right.name + " " + Function._alternateswith + " " + left.name;
		if (n >= 1)
			return null;
		/*if (n == 1)
			return new ConstraintCommentCommand(s1, left.name,
					Function._alternateswith,
					convert(new String[] { right.name }));*/
		return new ConstraintCommentCommand(s2, right.name,
				Function._alternateswith, convert(new String[] { left.name }));
		// return right.name + " alternate  " + left.name;

	}

}


