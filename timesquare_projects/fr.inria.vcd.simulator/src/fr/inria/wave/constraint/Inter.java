/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;

public class Inter extends Constraint {
	//PARAM
	private Clock inter, clka, clkb;
	//DATA
	
	//CODE
	/**
	 * @param inter = clka inter clkb
	 * @param clka
	 * @param clkb
	 */
	public Inter(Clock inter, Clock clka, Clock clkb) throws Exception {
		super();
		this.inter = inter;
		this.clka = clka;
		this.clkb = clkb;
		if (inter == null)
			throw new Exception("null");
		if (clka == null)
			throw new Exception("null");
		if (clkb == null)
			throw new Exception("null");
	}

	@Override
	public int check() {
		inter.setKind(ClockKind.CK_clock);
		clka.setKind(ClockKind.CK_clock);
		clkb.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public int run() {
		interBDD(inter, clka, clkb);
		return 0;
	}

}
