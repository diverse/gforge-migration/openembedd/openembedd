/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.BW;

public class Meet extends CompositeConstraint {

	private BW bw1;
	private BW bw2;
	private Clock c1 = null;
	private Clock c2 = null;
	private Clock x = null;
	private FilteringBy f1 = null, f2 = null;

	public Meet(Clock c1, Clock c2, BW bw1, BW bw2) throws Exception {
		super();
		this.c1 = c1;
		this.c2 = c2;
		this.bw1 = bw1;
		this.bw2 = bw2;
		if (bw1 == null)
			throw new Exception("null");
		if (bw2 == null)
			throw new Exception("null");
		if (c1 == null)
			throw new Exception("null");
		if (c2 == null)
			throw new Exception("null");

	}

	/**
	 * @see wave.constraint.Constraint#setSim(wave.Simulation, int)
	 */
	@Override
	public int run(Simulation sim, int n) throws Exception {
		
		x = sim.getLc().createClock("__internalclock_" + n, true);
		f1 = new FilteringBy(c1, x, bw1);
		f2 = new FilteringBy(c2, x, bw2);
		
		array.add(f1);
		array.add(f2);
		return 0;
	}

	

	

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		if (n == 0)
			return f1.localcomment(0);
		if (n == 1)
			return f2.localcomment(0);

		return null;
	}

}
