/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;


import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.comment.ConstraintDecodeComment;
import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.proba.IDParam;

public class PeriodicOn extends Constraint {

	
	private Clock superc = null;
	private Clock sub = null;
	private int per=0;	
	private int off=0;
	private IDParam idper=null;
	int nextdate=0;
	public PeriodicOn(Clock superc, Clock sub, int period , int offset) throws Exception {
		super();
		this.superc = superc;
		this.sub = sub;
		per=period;
		off=offset;
		if (period == 0)
			throw new Exception("period 0");
		if (sub == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
	}
	
	public PeriodicOn(Clock superc, Clock sub, IDParam idperiod , int offset) throws Exception {
		super();
		this.superc = superc;
		this.sub = sub;
		idper=idperiod;
		off=offset;
		if (idper == null)
			throw new Exception("period 0");
		if (sub == null)
			throw new Exception("null");
		if (superc == null)
			throw new Exception("null");
	}

	@Override
	public int check() {

		super.check();
		superc.setKind(ClockKind.CK_clock);
		sub.setKind(ClockKind.CK_clock);
		return 0;
	}

	
	/* (non-Javadoc)
	 * @see fr.inria.wave.constraint.Constraint#first()
	 */
	@Override
	public int first()
	{
	
		 super.first();
		 nextdate = off+1 ;
		 return 0;
	}

	@Override
	public int run() {

	

		int idxsup = (int) superc.getIdx() ; // consider next instant
		if (idxsup==nextdate  )
		{
			if (per!=0)
				nextdate = idxsup  +per ;
			else
				nextdate = idxsup  +idper.getNextValue();
		
		}
		if (nextdate==idxsup+1)
			coincidenceBDD(superc, sub);
		else	
			forbidBDD(sub);		
		return 0;
	}

	@Override
	public ConstraintCommentCommand localcomment(int n) {
		if (n != 0)
			return null;		
		return ConstraintDecodeComment.isperiodicOn(sub.getName(0), superc.getName(0));
		
	}

}
