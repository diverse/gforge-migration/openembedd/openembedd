/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.constraint;

import fr.inria.wave.Clock;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.proba.IDParam;

/**
 * @author Benoit Ferrero
 * 
 */
public class TimerOn extends OneShot {

	private Clock stop;

	private long _laststop = -1;

	public TimerOn(Clock timer, Clock superc, Clock start, Clock stop,
			Clock timeout, int duration) throws Exception {
		super(timer, superc, start, timeout, duration);

		this.stop = stop;

		if (stop == null)
			throw new Exception("null");

	}
	
	public TimerOn(Clock timer, Clock superc, Clock start, Clock stop,
			Clock timeout, IDParam rand) throws Exception {
		super(timer, superc, start, timeout, rand);

		this.stop = stop;

		if (stop == null)
			throw new Exception("null");

	}

	@Override
	public int check() {

		super.check();
		stop.setKind(ClockKind.CK_clock);
		return 0;
	}

	@Override
	public void startBDD()
	{
		forbidBDD(timeout);
		minusBDD(timer ,start,stop ); // timer=  start -stop 
		
	}
	
	@Override
	public void runBDD()
	{			
		xorBDD(timer, stop);		
		forbidBDD(timeout);	
	}
	
	@Override
	public void endBDD()
	{			
		minusBDD(timeout ,superc, stop);
		disjointBDD(superc, timer);	
		disjointBDD(stop, timer);	
	}
	  
	
	@Override
	public int run() {

		long n = stop.getIdx();
		if (n != _laststop) {
			_laststop = n;
			if (_running) {
				_running = false;
				_reste = 0;
				timer.timedata.timeAbort ++;				
				startBDD();
				return 0 ;
			}
			if (start.getIdx() != _laststart)
				_laststart = (int) start.getIdx();
			
			startBDD();
			return 0;
		}
		super.run();

		return 0;
	}

}
