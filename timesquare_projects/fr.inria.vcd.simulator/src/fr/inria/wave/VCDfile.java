/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;

import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.widgets.Display;

import fr.inria.base.MyManager;
import fr.inria.vcd.model.BinaryVectorValueChange;
import fr.inria.vcd.model.Value;
import fr.inria.vcd.model.command.TimeScaleCommand;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.comment.HideDecodeComment;
import fr.inria.vcd.view.Iupdate;
import fr.inria.wave.constraint.CompositeConstraint;
import fr.inria.wave.constraint.Constraint;
import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.ClockStatus;
import fr.inria.wave.fire.Imode;

public class VCDfile {
	private static VCDfile courant;
	public class LocalPrintStream {
		//public PrintStream tra = null;
		//public PrintStream fou = null;
		public PrintStream vcd = null;
		public PrintStream fire = null;
		private PrintStream _stdout = null;
		//private PrintStream _stderr = null;
		
		public int printstd(String s){
			MyManager.println(s);
			if (_stdout!=null)
			_stdout.print(s);
		return 0;	
		}
		
		public int printlnstd(String s){
			MyManager.println(s);
			if (_stdout!=null)
			_stdout.println(s);
			return 0;	
		}
		
		public int printlnerr(String s){
			//if (_stderr!=null)
				//_stderr.println(s);
			return 0;	
		}
	}

	private class LocalFile{
		public File ft = null;
		public File fl = null;
		public File fv = null;
		public File fr = null;
	}
	public static class VersionSim {
		static final int majVersion = 2;
		static final int minVersion = 100;
	}
	
	//private ClockList cl = null;
	private Simulation sim = null;
	private boolean bit = false;
	private ScoreBoard sc = null;
	private LocalPrintStream local = null;
//	private VCDDefinitions vcdin = null;
	private LocalFile localfile = null;
	
	public Imode getFirepol() {
		return sim.getFirepol();
	}

	public void setFirepol(Imode firepol) {
		sim.setFirepol(firepol);
	}

	public VCDfile() {
		sim = new Simulation();
	//	cl = new ClockList(sim);
		sim.setVc(this);
		sim.setLc( new ClockList(sim));
		courant = this;		
		local = new LocalPrintStream();
		localfile = new LocalFile();
		local._stdout = null; //System.out;
		//local._stderr = null;//System.err;	
	}

	/*public VCDfile(VCDDefinitions vi) {
		vcdin = vi;
		sim = new Simulation();
		//cl = new ClockList(sim);
		sim.setVc(this);
		sim.setLc( new ClockList(sim));
		
		courant = this;
		local._stdout =null;// System.out;
	//	local._stderr =null;// System.err;		
	}*/
	
	/**
	 * 
	 * @param f
	 * @param s
	 * @param b if true redirectiob des flux erreur et standartd vers (s).out et (s)err 
	 * @return
	 * @throws Exception
	 */
	public int openfile(IPath f, String s, boolean b) throws Exception {
		try {
			//localfile.ft = f.append(s + ".tra").toFile();						
			//localfile.fl = f.append(s + ".log").toFile();						
			localfile.fv = f.append(s + ".vcd").toFile();						
			localfile.fr = f.append(s + ".fire").toFile();
			openprintstream();	
			if (b) 
			{
				setStdout(new PrintStream(f.append(s + ".out").toFile()));
				setStderr(null ); //new PrintStream(f.append(s + ".err").toFile()));			
			}
			sc = ScoreBoard.getScoreboard(s, null); //vcdin);
		} catch (Exception e) {
			e.printStackTrace();
			local.printlnstd("ERREUR VCDFILE01:"+e);
			throw e;
		}
		return 0;
	}

	public int openfile(String s) throws Exception {
		try {
			//localfile.ft = new File(s + ".tra");
			//localfile.fl = new File(s + ".log");
			localfile.fv = new File(s + ".vcd");
			localfile.fr = new File(s + ".fire");
			openprintstream();
			sc = ScoreBoard.getScoreboard(s);
		//	vcdin=sc.getVcd(); 
		} catch (Exception e) {
			e.printStackTrace();
			local.printlnstd("ERREUR VCDFILE01:"+e);
			throw e;
		}
		return 0;
	}
	
	private int openprintstream() throws Exception{		
		//local.tra = new PrintStream(localfile.ft);
		//local.fou = new PrintStream(localfile.fl);
		local.vcd = new PrintStream(localfile.fv);
		local.fire = new PrintStream(localfile.fr);
		return 0; 
	}
	
	public int fileHeader(String s1, String s) {
		// strftime (buffer,80,"%B %d, %Y %H:%M:%S",timeinfo);
		Date d = new Date();
		local.vcd.println("$date");
		local.vcd.println("\t" + d);
		endtag();
		local.vcd.println("$version");
		local.vcd.println("\tCLOCK-SIMULATOR " + VersionSim.majVersion + "." + VersionSim.minVersion);
		endtag();
		if ((s1!=null) && (s!=null)) 
		{
		local.vcd.println("$timescale");
		local.vcd.println("\t" + s1 + " " + s);	
		endtag();
		getSc().getVcd().addDefinition(new TimeScaleCommand(s1, s));
		}
		return 0;
	}

	public void reportProgress(PrintStream f, String st) {
		f.println("(Progress) " + st);
	}

	public int closefile() {
	//	local.tra.close();
		//local.fou.close();
		local.vcd.close();
		if (local._stdout!=null)
			local._stdout.close();
		//if (local._stderr!=null)
			//local._stderr.close();
		local.fire.close();
		return 0;
	}

	@Override
	protected void finalize() throws Throwable {
		closefile();
		super.finalize();
	}

	public void trace(String s) {
		//local.tra.println(s);
	}

	public void msgOut(String s) {
		//local.fou.println(s);
	}

	public int errorMsg(String s, int num) {
		local.printlnerr(s);
		return num;
	}

	public int enddef() {
		local.vcd.print("$enddefinitions ");
		endtag();
		return 1;
	}

	public int endtag() {
		local.vcd.println("$end");
		local.vcd.println();
		return 0;
	}

	public int date(long dte) {
		local.vcd.println("#" + dte+" ");
		return 0;
	}

	public int defvar(Clock clk) {
		if (clk.getKind() != ClockKind.CK_unknown) {
			local.vcd.println("$var " + clk.getKind().gettype() + " 1 "
					+ clk.pcode + " " + clk.name + " $end");
			// if (clk.id!=0)
			sc.addVar(clk.name, clk.pcode);	
			if (clk.isHiden())
			{
				//TODO adding hide
				sc.getVcd().addDefinition( HideDecodeComment.createHide(clk.getName(0)));
				local.vcd.println("$comment hide " + clk.getName(0) + " $end") ;
			}
		}
		return 0;
	}

	/**
	 * @return the cl
	 */
/*	private ClockList getCl() {
		return sim.getLc();
	}*/

	public int dumpvar() {
		date(0);
		sim.inctime(0,true );
		local.vcd.println("$dumpvars "); 
		for (Clock c : sim.getLc().listeallclockwithdc) {
			if (c.isClock())
				local.vcd.println("Z" + c.pcode);
			if (c.isTimer())
				local.vcd.println("bZ " + c.pcode);
			local.printlnstd(c + " " + c.name + " " + c.getKind() + c.pcode);
		}
		endtag();
	/*	date(0);
		for (Clock c : cl.listeallclockwithdc) {
			if (c.isClock())
				local.vcd.println("Z" + c.pcode);
			if (c.isTimer())
				local.vcd.println("bZ " + c.pcode);
			local.printlnstd(c + " " + c.name + " " + c.kind + c.pcode);
		}*/
		for (Clock c : sim.getLc().listeallclockwithdc)
			if (c.isClock() || c.isTimer())
				sc.addScalarValueChange(Value._z, c.pcode);
		return 0;
	}

	public int writeext(long dte){ 
		date(dte );
		sim.inctime((int )dte,false );
		for (Clock c : sim.getLc().listeallclockwithdc) {
			// if (c.isChanged())
			local.vcd.println(c.write());
			if ((c.getPresence() == Bit.Z) && c.isClock() && c.id != 0)
				c.setStatus(ClockStatus.CS_finished);
		}
		sc.tick((int) dte );
		for (Clock c : sim.getLc().listeallclockwithoutdc) {
			if (c.isClock())
				sc.addScalarValueChange(c.getPresence().convertValue(false),c.pcode);
			if (c.isTimer()) {
				sc.add(new BinaryVectorValueChange(c.getPresence().convertString(true), c.pcode));
			}
		}
		return 0;
	}
	
	public int write(long dte) {
		date(dte * 10);
		local.vcd.flush();		
		sim.inctime((int )dte*10, true);
		for (Clock c : sim.getLc().listeallclockwithdc) {
			// if (c.isChanged())
			local.vcd.println(c.write());
			if ((c.getPresence() == Bit.Z) && c.isClock() && c.id != 0)
				c.setStatus(ClockStatus.CS_finished);
		}
		sc.tick((int) dte * 10);
		for (Clock c : sim.getLc().listeallclockwithoutdc) {
			if (c.isClock())
				sc.addScalarValueChange(c.getPresence().convertValue(false),c.pcode);
			if (c.isTimer()) {
				sc.add(new BinaryVectorValueChange(c.getPresence().convertString(true), c.pcode));
			}
		}
		if (sim.getPulse() == 1) {
			date(dte * 10 + sim.getPulseWidth());
			sim.inctime((int )dte*10 + sim.getPulseWidth(), false);
			sc.tick((int) dte * 10 + sim.getPulseWidth());
			for (Clock c : sim.getLc().listeallclockwithoutdc) {
				if (c.isClock())
					if (c.getPresence().is0or1()) {
						local.vcd.println("0" + c.pcode);
						sc.addScalarValueChange(Value._0, c.pcode);
					}
				if (c.getPresence() == Bit.X) {
					local.vcd.println("0" + c.pcode);
					sc.addScalarValueChange(Value._0, c.pcode);
				}
			}
		}
		return 0;
	}

	public int finalvcd(long dte) {
		date(dte*10);
		for (Clock c : sim.getLc().listeallclockwithdc) {
			if (c.isClock())
				local.vcd.println("Z" + c.pcode);
			if (c.isTimer())
				local.vcd.println("bZ " + c.pcode);
			// c.status=ClockStatus.CS_unknown;
		}
		sc.tick((int) dte * 10); 
		sim.finishtime((int) dte * 10);
		for (Clock c : sim.getLc().listeallclockwithdc)
			if (c.isClock() || c.isTimer())
				sc.addScalarValueChange(Value._z, c.pcode);
		return 0;
	}

	public int add(Constraint a) {
		return sim.add(a);
	}

	public int add(CompositeConstraint c) {		
		return sim.add(c);
	}
	
	public int discretizeBy(Clock c, String time){
		return sim.discretizeBy(c, time);
	}
	/**
	 * @return the courant
	 */
	public static VCDfile getCourant() {
		return courant;
	}

	/**
	 * @return the sim
	 */
	protected Simulation getSim() {
		return sim;
	}

	public void setPulse(int pulse) {
		sim.setPulse(pulse);
	}

	public void locked() {
		sim.getLc().locked();
	}

	public Clock createClock(String name) throws Exception {
		return sim.getLc().createClock(name);
	}

	public int prerun() throws Exception {
		int n = 0;
		for (Constraint c : getSim().getConslist()) {
			c.setSim(getSim(), n++);
			c.check();					
		}
		sim.getLc().locked();
		for (Clock c : sim.getLc().listeallclockwithdc) {
			if (c.isIllegal())
				throw new Exception("Clock illegal " + c.name);
			if (c.id != 0)
				defvar(c);
		}
		sim.getLc().buildsubliste();
		sim.getLc().generateBDDclock();
		enddef();
		//dumpvar();
		for (Constraint c : getSim().getConslist()) {
			c.first();
			local.vcd.println(c.comment());
			ConstraintCommentCommand cc = null;
			int ln = 0;
			do {
				cc = c.localcomment(ln++);
				if (cc == null)
					break;
				sc.getVcd().addDefinition(cc);
				
				// getCommentCommand().add(cc);
				//local.printlnstd("---> " + cc); //TODO Color
			} while (true) ; //cc != null);

		}
		local.vcd.println();
		for (Object o : sc.getVcd().getConstraintCommentCommand()) {
			local.printlnstd(o.toString());
		}
		bit = true;
		return 0;
	}

	public int run(int duration) throws Exception {
		return run(duration, null);

	}

	public int run(int duration, Iupdate up) throws Exception {
		// if (!cl.isLock()) throw new Exception ("nNo lock for run");
		if (up != null)
			up.setSimulationAvance(2);
		if (!bit)
			prerun();
		dumpvar();
		getFirepol().startSimul();
		int n, ln;
		ln = 0;
		Display display = null;
		if (up != null) {
			if (up.getMyShell() != null)
				display = up.getMyShell().getDisplay();
		}
		
		for (Clock c : sim.getLc().listeClock)		
				c.setPresence(Bit.F);
		writeext(5);
		int pos;
		boucle: for (pos = 1; pos <= duration; pos++) {

			sim.step();
			write(pos );
			n = sim.getVc().getSc().getsize();

			if (up != null) {
				try {
					int avc = pos * 5 / (duration + 1) + 2;
					up.setSimulationAvance(avc);
					up.update(ln, n, false);

					// ((Window ) up).getShell().getDisplay().readAndDispatch();

					display.update();					
					if (!display.readAndDispatch())
						display.sleep();
				} catch (Exception e) {
					e.printStackTrace();
					up = null;
				}
				if (up == null || up.getMyShell() == null) {
					up = null;
					break boucle;
					// throw new Exception("killed" );
				}
			}
			ln = n;
		}
		finalvcd((pos + 1) ); // duration
		// finalvcd(duration *10);  
		getFirepol().endSimul();
		return 0;
	}
	/*public PrintStream getStdout() {
		return local._stdout;
	}
	public PrintStream getStderr() {
		return local._stderr;
	}
	*/
	public void setStdout(PrintStream stdout) {
		this.local._stdout = stdout;
	}

	public void setStderr(PrintStream stderr) {
		//this.local._stderr = stderr;
	}

	public int statistique() {
		local.printlnstd("\nRapport :");
		for (Clock c : sim.getLc().listeallclockwithdc) {
			if (c.isClock())
				local.printlnstd("Final " + c.name + " " + c.getIdx());
			if (c.isTimer())
				local.printlnstd("Final Timer " + c.name + " :" + c.timedata.started
						+ " " + c.timedata.timeAbort);
		}
		return 0;
	}

	public ScoreBoard getSc() {
		return sc;
	}

	public LocalPrintStream getLocal(){
		return local;
	}
	
	public int linearize (Iupdate up){
		sim.linearize(up);
		return 0;
	}
}

/*
 * check
 * 
 * 
 * 
 * date = 10upto; for (i=1;i<nbClock;i++) { if (isIn(pTimers,i))
 * strcpy(var_type,"bZ "); else strcpy(var_type,"Z");
 * fprintf(vcd,"%s%s\n",var_type,pCode[i]); } date += 10;
 * fprintf(vcd,"#%d\n",date);
 */