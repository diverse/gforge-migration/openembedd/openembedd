package fr.inria.wave.proba;

import java.util.Random;

import fr.inria.base.MyManager;

public class Uniform implements IDParam {

	int min;
	int max;
	public Uniform(int min, int max) throws Exception
	{
		MyManager.println("Create" +max+" "+min+ " " +hashCode());
		this.min=min;
		this.max=max;
		if (max-min<0)
		{
			throw new Exception("max- min <0");
		}
	}
	int current=0;
	/* (non-Javadoc)
	 * @see fr.inria.wave.proba.IDParam#getCurrent()
	 */
	
	public int getCurrent()
	{
		
		return current;
	}

	Random rand= new Random();
	/* (non-Javadoc)
	 * @see fr.inria.wave.proba.IDParam#getNextParam()
	 */
	
	public int getNextValue()
	{
		//MyManager.println(max+" "+min+ " " +hashCode());
		current = min +rand.nextInt(max-min+1);
		return current;
	}

	
}
