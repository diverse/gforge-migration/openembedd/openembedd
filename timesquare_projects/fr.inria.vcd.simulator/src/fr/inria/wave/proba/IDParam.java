package fr.inria.wave.proba;

public interface IDParam {

	//public void setValue(int n , int value ) ;
	
	public int getCurrent();
	public int getNextValue();
	
}
