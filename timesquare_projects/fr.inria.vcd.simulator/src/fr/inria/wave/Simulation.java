/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave;

import java.util.ArrayList;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import fr.inria.base.MyManager;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.Iupdate;
import fr.inria.wave.constraint.CompositeConstraint;
import fr.inria.wave.constraint.Constraint;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;
import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.fire.Imode;

public class Simulation {

	private int choix;
	private ArrayList<Constraint> conslist;
	private ArrayList<CompositeConstraint> compositelist;
	private long date = 1;
	private int emptyfireableset = 0;
	private TriStateSet callfirable;
	private TriStateSet presencefirable;
	private Imode firepol = null;
	private ClockList lc;	
	private int nbClocks;
	private int pulse;
	private int pulseWidth = 1;	
	private VCDfile vc;	
	private Linear line=null;
	private BDD band=null;
	public int linearid=-1;
	public int max4linear=0;
	public ArrayList<Linear> stepint=new ArrayList<Linear>();
	public int basetime=0;	
	public int count=0;
	
	public class LinearPoint 
	{
		public int time;
		public boolean b;
		public LinearPoint(int time, boolean b) {
			super();
			this.time = time;
			this.b = b;
		}		
	}
	
	public class Linear
	{
		public ArrayList<LinearPoint> lp= new ArrayList<LinearPoint>();
		int time;
		public Linear(int time) {
			super();
			this.time = time;
		}
		int count=0;
		
	}
	
	public void inctime(int time, boolean b)
	{
		if (linearid==-1)
			return;
		Clock c=lc.get(linearid);
		if ((c.getPresence()==Bit.T && b)|| time == 0)
		{
			if (max4linear<count ) max4linear=count; 
			Linear l= new Linear(time);
			stepint.add(l);
			line=l;
			count=0;
		}
		else
		{
			if (line!=null)
			{
				line.lp.add(new LinearPoint(time,b));
				line.count++;
			}
			count++;  
		}
	}
	
	public void finishtime(int time)
	{
		if (linearid!=-1)
		{	
	
		
			if (max4linear<count ) max4linear=count; 
			if (line!=null)
			{
				line.lp.add(new LinearPoint(time,false));
				line.count++;
			}
		}
		/*for(Linear i:stepint)
		{
			//System.out.println(i.count);
		}*/
	}
	
	public int linearize (Iupdate up)
	{
		if (linearid==-1)
			return 0;
		int n=max4linear;
		System.out.println("nbr "+n);
		int n10=(int ) Math.pow(10, Math.floor( Math.log10(n)+1 ));
		System.out.println("nbr 10 : "+n10);
		int cpt=0;
		int ptr=0;
		for(Linear l:stepint)
		{
			System.out.println("test "+l.time +"  " +cpt*n10);
			ptr=up.replace(ptr, "#"+l.time, "#"+(cpt*n10) +" $comment change $end"); 
			int j=1;
			int base =cpt*n10;
			int delta=0;
			for (LinearPoint lp:l.lp)
			{				
				if (lp.b)
				 delta = ( j * n10 ) / l.count ;
				else
				 delta++;
				int value=base+delta;
				System.out.println("test   "+lp.time +"  " +value);
				ptr=up.replace(ptr, "#"+lp.time, "#"+value );
				j++;
			}
			cpt++;
		}
		return 1;
	}
	
	public BDDFactory bddf=BDDFactory.init(20000, 20000);
	/**
	 *
	 */
	public Simulation() {
		super();
		conslist = new ArrayList<Constraint>();
		compositelist= new ArrayList<CompositeConstraint>();
		//mat = null;
	}

	public int add(Constraint cs) {
		conslist.add(cs);		
		return 0;
	}

	public int add(CompositeConstraint c) 
	{
		try
		{
		compositelist.add(c);
		int n=conslist.size();
		ArrayList<Constraint> local= c.getArray(this, n);
		for (Constraint lc:local)
		{	
			lc.setSim(this, n);
			conslist.add(lc);	
		}
		}
		catch (Exception ex) {
			MyManager.printError(ex ,"here have ex  :" );
		}
		return 0;
	}

	public void createData() 
	{
		int x = lc.getCountclock();
		nbClocks = x;	
		callfirable = new TriStateSet(x);		
		presencefirable= new TriStateSet(x);		
	}

	public final int dispmessage(String s, int num) {
		msgOut(s);
		trace(s);
		return errorMsg(s, num);
	}

	public int discretizeBy(Clock c, String time)
	{
		System.out.println(c+"test disctretize " + time);
		linearid=c.id;
		basetime= (int ) Math.log10( Double.parseDouble(time)) ;
		System.out.println(basetime);
		return 0;
		
	}
	
	public final int errorMsg(String s, int num) {
		return vc.errorMsg(s, num);
	}

	/**
	 * @return the date
	 */
	public long getDate() {
		return date;
	}

	public Imode getFirepol() {
		return firepol;
	}

	/**
	 * @return the lc
	 */
	public ClockList getLc() {
		return lc;
	}
	/*private SimuMatrix getMat() {
		return mat;
	}*/

	/**
	 * @return the pulse
	 */
	public int getPulse() {
		return pulse;
	}
	/*private int initfired() {
		for (Clock c : lc.listeallclockwithdc) //
		{

			if (c.getStatus() == ClockStatus.CS_fireable) {
				if (c.isIsfired())
					fireable.set(c.id, true);
				println("fireable " + c.name + " " + c);
			} else {
				// fireable.set(c.id, false);
				println("not fireable " + c.name + " " + c + " " + c.id);
			}
		}
		return 0;
	}*/

	public final void log(String s) {
		vc.msgOut(s);
	}
	
	public void bddAdd(BDD b)
	{
		if (band!=null)
		{
			//BDD old=band;
			band=band.and(b);
			if (band.isZero())
			{				
				//(new Exception()).printStackTrace();		
				
			}
		//	old.free();
		}
		else
		{
			System.out.println("---> " +b );
			band=b.and(bddf.one());
		}
	} 

	public final void msgOut(String s) {
		vc.msgOut(s);
	}

	public int print(String s) {
		vc.getLocal().printstd(s);
		return 0;
	}

	public int println() {
		vc.getLocal().printlnstd("");
		return 0;
	}

	public int println(String s) {
		vc.getLocal().printlnstd(s);
		return 0;
	}

	private int reportProgress(String s) {
		vc.errorMsg(s, 0);
		return 0;
	}

	public void setFirepol(Imode firepolin) {
		this.firepol = firepolin;
		getFirepol().init(this);
	}

	/**
	 * @param lc
	 *            the lc to set
	 */
	protected void setLc(ClockList lc) {
		this.lc = lc;
	}

	/**
	 * @param pulse
	 *            the pulse to set
	 */
	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	/**
	 * @param vc
	 *            the vc to set
	 */
	protected void setVc(VCDfile vc) {
		this.vc = vc;
	}
	/*private int sim_CheckFireable()
	{	
		
		fireable.clear();
		initfired();	
		return 0;
	}*/

	public int sim_Fire() {
		choix = 0;
		callfirable.clearX();
		presencefirable.clearX();
		//fireable.clear();
		try
		{
			if (band.isZero()) 
			{
				callfirable.clear0();
				presencefirable.clear0();
				choix=-1;
			}
			else
			choix = firepol.fireDone(presencefirable, callfirable,band);			
			
		} catch (Exception e) {
			MyManager.printError(e);
		}
		//trace("Fireable set at " + date + ": " + fireable.toString());	
		/*if (choix < 0) 
		{
			System.out.println("ERROR ------------------------------------------------------------------");
		}*/
		if (emptyfireableset == 1) {
			//fireable.clear();
			trace("No Clock to Fire");
			log("@" + date + ": no clocks to fire ");
			//toUpdate.clear();
		} else {
			reportProgress(">>Sim_fire : n=" + choix);
			//toUpdate.set2bitset(mat.getimplyline(choix));
			trace("firing choice : n=" + choix + " " );
			//+ toUpdate.toString());		
			//println(" : ->" + choix);
		}
		log("@" + (date + 1) + "firing : ");
		trace("@" + (date + 1) + "firing : ");
		for (Clock c : lc.listeClock)
		{
				if (callfirable.get(c.id) == TriState.T) // update
				{
					c.setPresence(Bit.T);
					c.setUniversaldate(date);
					c.incIdx();
				} else 
					if ((c.getPresence() != Bit.Z)) 
					{
					if (presencefirable.get(c.id)!=TriState.F)
					{
						c.setPresence(Bit.X);						
					} else
					{
						c.setPresence(Bit.F);
					}
				}
		}
		for (Clock timer : lc.listeTimer) 
		{
			if (callfirable.get(timer.id) == TriState.T) 		
				timer.setPresence(Bit.T);
			else
				timer.setPresence(Bit.F);
		}
		date++;
		//mat.setdiagonal();
		//mat.resetCstr();
		return 0;
	}

	public int step() {
		band =lc.getOrclock();	 
		//bddf.one();
		/*for (Clock c : lc.listeallclockwithdc)
			c.setIsfired(true);*/
		reportProgress(">>>>>>>> Simulation step " + date);
		reportProgress("Phase 1: constraint analysis");
		trace("Phase 1: constraint analysis");
		
		for (Constraint c : conslist) 
		{
			c.cleanBDD();
			c.run();			
		}		
		if (band.isZero())
		{
			MyManager.printlnError("ERROR SYMULATION Deadlock .............");
			//System.out.println("Ctrs -->BDD =0 " + err.toString() );
			MyManager.println("test1 ");
			BDD test= lc.getOrclock();
			for (Constraint c : conslist)
			{
				BDD l=c.getBddlocal();				
				test=test.and(l);
				MyManager.println(c.toString());
				ConstraintCommentCommand s=null;
				int n=0;
				while ((s=c.localcomment(n))!=null)
				{
					MyManager.println("\t"+s.toString());
					n++;
				}
				//MyManager.println(test.toString());  
				//MyManager.println(l.toString());				  
				if (test.isZero())
				{
					break;
				}
				
			}
			/*MyManager.println("test2 ");
			for (Constraint c : conslist)
			{
				BDD test= band.getFactory().one() ; //   lc.getOrclock();
				boolean b=true;
				for (Constraint c2 : conslist)
				{
					BDD l=c2.getBddlocal();
					if (c!=c2)
					{
					test=test.and(l);
					if (test.isZero())
					{
						b=false;
					}
					}
				}
				String mess=null;
				if (b)				
					mess= ("Ctr -->BDD =0  :" + c.toString() );				
				else
					mess =("Ctr not lock"  + c.toString() );	
				MyManager.println(mess);  
				System.out.println(mess);
				
			}*/
			MyManager.printlnError("");
		}
		trace("Phase 2: clock enabling analysis");
		//sim_CheckFireable();
		reportProgress("Phase 3: clock firing");
		sim_Fire();		
		//vc.getLocal().fire.print(band.pathCount()+" " +band.nodeCount()+" "+ band.isOne() +" "+ band.isZero()+" " );
		vc.getLocal().fire.println("@"+date+":"+presencefirable+":"+callfirable+";");
		return 0;
	}
	/*
	public int UVset(int k, int u) { previousdat[k] = u; return 0; }
	public BitSet getToUpdate() { return toUpdate; }
	*/

	public void trace(String s) {
		vc.trace(s);
	}

	/*private void tryForceClockStatus(int i, ClockStatus cs) {
		Clock c = lc.get(i);
		c.tryForceClockStatus(cs);
	}
	
	public DParam getDparam() {
		return dparam;
	}

	public Histo getHisto() {
		return histo;
	}*/

	/**
	 * @return the nbClocks
	 */
	public int getNbClocks() {
		return nbClocks;
	}

	public ArrayList<Constraint> getConslist() {
		return conslist;
	}

	public int getPulseWidth() {
		return pulseWidth;
	}

	public VCDfile getVc() {
		return vc;
	}
	
	/*
	public void computeBitSet()
	{
		if (band.pathCount()==1.0)
		{
		}
	}*/
}