/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright 2007-2008
 */
package fr.inria.wave;

import net.sf.javabdd.BDD;

import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.ClockStatus;

public class Clock {
	// private boolean changed = false;

	public final int id;
	public final String name;
	public final String pcode;

	public class TimerData 
	{
		public int started = 0;
		public int timeFinish = 0;
		public int timeAbort = 0;
	}

	private long idx;
	private boolean isPrivate;
	private ClockKind kind = ClockKind.CK_unknown;
	private long universalDate = 0;
	// public string xmiid;
	public TimerData timedata;

	private BDD varClock = null;
	private Bit current, previous;
	private Bit finished;
	private Bit presence;
	private boolean hiden = false;
	private Simulation sim;
	private ClockStatus status = ClockStatus.CS_unknown;

	@Override
	public String toString()
	{
		return "Clock " + id + " " + name + " " + status;
	}

	/**
	 * @param name
	 * @param n
	 */
	protected Clock(String name, int n)
	{
		super();
		this.name = name;
		id = n;
		pcode = "!" + n;
		finished = Bit.T;
		previous = Bit.Z;
		presence = Bit.Z;
		// ecs=new ExtraCstr();
		status = ClockStatus.CS_unknown;
		kind = ClockKind.CK_unknown;
		// condAct = new CondAction();
		idx = 0;
		// timerAction = false;
		isPrivate = false;
		// isFired = true;
	}

	protected Clock(String name, int n, boolean b)
	{
		super();
		this.name = name;
		id = n;
		pcode = "!" + n;
		finished = Bit.T;
		previous = Bit.Z;
		presence = Bit.Z;
		// ecs=new ExtraCstr();
		status = ClockStatus.CS_unknown;
		kind = ClockKind.CK_unknown;
		// condAct = new CondAction();
		idx = 0;
		// timerAction = false;
		isPrivate = b;

	}

	/**
	 * @return the current
	 */
	public Bit getCurrent()
	{
		return current;
	}

	/**
	 * @return the finished
	 */
	public Bit getFinished()
	{
		return finished;
	}

	/**
	 * @param finished
	 *            the finished to set
	 */
	public void setFinished( Bit finished )
	{
		this.finished = finished;
	}

	/**
	 * @return the presence
	 */
	public Bit getPresence()
	{
		return presence;
	}

	/**
	 * @return the previous
	 */
	public Bit getPrevious()
	{
		return previous;
	}

	/**
	 * @return the sim
	 */
	protected Simulation getSim()
	{
		return sim;
	}

	/**
	 * @return the changed
	 */
	/*
	 * public boolean isChanged() { return changed; }
	 */

	public boolean isClock()
	{
		return kind == ClockKind.CK_clock;
	}

	public boolean isIllegal()
	{
		return kind == ClockKind.CK_illegal;
	}

	public boolean isTimer()
	{
		return kind == ClockKind.CK_timer;
	}

	public boolean isUnKnow()
	{
		return kind == ClockKind.CK_unknown;
	}

	/**
	 * @return the changed changed <= false;
	 */
	/*
	 * public boolean resetChanged() { boolean b = changed; changed = false;
	 * return b; }
	 */

	/**
	 * @param changed
	 *            the changed to set
	 */
	/*
	 * public void setChanged() { this.changed = true; }
	 */

	public void setCurrent()
	{
		current = presence;
	}

	/**
	 * @param current
	 *            the current to set
	 */
	public void setCurrent( Bit current )
	{
		this.current = current;
	}

	public ClockKind getKind()
	{
		return kind;
	}

	public void setKind( ClockKind kd )
	{
		kind = kind.resolve(kd);
		idx = 0;
	}

	/**
	 * @param presence
	 *            the presence to set
	 */
	public void setPresence( Bit presence )
	{
		this.presence = presence;
	}

	/**
	 * @param previous
	 *            the previous to set
	 */
	public void setPrevious( Bit previous )
	{
		this.previous = previous;
	}

	/**
	 * @param sim
	 *            the sim to set
	 */
	protected void setSim( Simulation sim )
	{
		this.sim = sim;
	}

	public void forceBit( Bit b )
	{
		presence = b;
	}

	public void tryForceBit( Bit b )
	{
		// enum ClockStatus oldB, newB;
		Bit oldB, newB;

		oldB = presence;
		newB = oldB.ressolve(b);
		/*
		 * if (Bit_Resolve(oldB,B,&newB)) { // forcing denied
		 * sprintf(buffer,"conflicting Bit on state %s (%s vs. %s)\n",
		 * cs->id,Bit_Str[oldB],Bit_Str[B]); fprintf(ftra,buffer);
		 * fprintf(stderr,buffer); fprintf(flog,buffer); }
		 */
		presence = newB; // new status set (error sets X)
	}

	public void tryForceClockStatus( ClockStatus cs )
	{
		ClockStatus oldCs, newCs;
		oldCs = this.status;
		newCs = oldCs.resolve(cs);
		if (ClockStatus.getConflict() == 1)
		{
			sim.dispmessage("conflicting ClockStatus on state " + name + " (" + oldCs + " vs. "
					+ cs + ")\n", 0);
		}
		status = newCs;
	}

	public Bit updateBit( Bit b )
	{
		previous = current;
		current = b;
		return previous;
	}

	public String write()
	{
		if (presence == Bit.X)
			return presence.getVcd(isTimer()) + pcode + "\n$comment status F " + pcode + " $end";

		return presence.getVcd(isTimer()) + pcode;
	}

	public String getName( int x )
	{
		if (x == 0)
			return name;
		if (name.length() >= x)
			return name;
		String s = name;
		int i;
		for (i = name.length(); i < x; i++)
		{
			s += " ";
		}
		return s;
	}

	public ClockStatus getStatus()
	{
		return status;
	}

	public void setStatus( ClockStatus status )
	{
		this.status = status;
	}

	public long getIdx()
	{
		return idx;
	}

	/*
	 * protected void setIdx( long idx ) { this.idx = idx; }
	 */

	public void incIdx()
	{
		idx++;
	}

	public long getUniversaldate()
	{
		return universalDate;
	}

	public void setUniversaldate( long universaldate )
	{
		this.universalDate = universaldate;
	}

	/*
	 * public boolean isTimeraction() { return timerAction; }
	 * 
	 * public void setTimeraction(boolean timeraction) { this.timerAction =
	 * timeraction; }
	 */

	protected boolean isPrivate()
	{
		return isPrivate;
	}

	/*
	 * public boolean isIsfired() { return isFired; }
	 * 
	 * public void setIsfired(boolean isfired) { this.isFired = isfired; }
	 */

	public BDD getVarclock()
	{
		return varClock;
	}

	public void createBdd()
	{
		varClock = sim.bddf.ithVar(id);
	}

	/**
	 * @return the hiden
	 */
	public final boolean isHiden()
	{
		return hiden;
	}

	/**
	 * @param hiden
	 *            the hiden to set
	 */
	public final void setHiden( boolean hidenin )
	{
		hiden = hidenin; // TODO fix bug;
	}
}