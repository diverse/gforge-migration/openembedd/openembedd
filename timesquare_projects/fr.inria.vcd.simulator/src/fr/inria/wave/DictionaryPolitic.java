package fr.inria.wave;

import java.util.ArrayList;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.inria.base.MyManager;
import fr.inria.wave.fire.SimBase;
import fr.inria.wave.fire.SimFireRandom;



public class DictionaryPolitic {

	static private DictionaryPolitic instance;

	static public DictionaryPolitic getInstance() {
		if (instance==null)
			instance = new DictionaryPolitic();
		return instance;
	}

	private DictionaryPolitic() {
		super();	
		init();
		disp();
	}
	
	private ArrayList<String > lname = new ArrayList<String>();
	private ArrayList<String > lcomment = new ArrayList<String>();
	
	public ArrayList<String> getLname() {
		return lname;
	}

	public String[] getLnameArray() {
		return lname.toArray(new String []{});
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<Class > lclass = new ArrayList<Class >();
	
	public int getSize()
	{
		return lclass.size();
	}
	
	public  String getLname(int i)
	{
		try
		{
			return lname.get(i);
		}
		catch (Throwable t) {
			return  null;
		}
	}
	
	public  String getComment(int i)
	{
		try
		{
			return lcomment.get(i);
		}
		catch (Throwable t) {
			return  " ";
		}
	}
	
	public SimBase getCname(int i) {
		try
		{
		return  (SimBase) lclass.get(i).newInstance();
		}
		catch (Throwable e) {
			MyManager.printlnError("--->>  Politic by default used  :  Random");
		return new SimFireRandom();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Class getClass(int i) {
		try
		{
		return   lclass.get(i);
		}
		catch (Throwable e) {
			MyManager.printlnError("--->>  Politic by default used  :  Random");
		return null;
		}
	}
	private void init()
	{
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] extensions = reg.getConfigurationElementsFor("fr.inria.vcd.simulation.politicSimulation");

		for (IConfigurationElement ice : extensions)
		{
			instanceone(ice);
		}
	}
	
	
	

	@SuppressWarnings("unchecked")
	private void instanceone( IConfigurationElement ice )
	{

		try
		{
			String plugin = ice.getDeclaringExtension().getNamespaceIdentifier();
			Bundle b = Platform.getBundle(plugin);
			String nameclass = ice.getAttribute("class");
			Class c=b.loadClass(nameclass);		
			String name= ice.getAttribute("name");
			String comment= ice.getAttribute("comment");
			if (comment==null) { comment="";}
			lclass.add(c );
			lname.add(name);
			lcomment.add(comment);
			
		} catch (Exception e)
		{
			MyManager.printError(e);

		} catch (Error e)
		{
			MyManager.printError(e);

		}

	}
	
	public void disp()
	{
		MyManager.println("Politic of Simulation :");
		for ( String s : lname)
		{
			MyManager.println("\t" + s);
		}
	}
	
}
