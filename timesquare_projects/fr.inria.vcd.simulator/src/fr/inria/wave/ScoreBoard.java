/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave;

//package fr.inria.signal.capture;

import java.util.HashMap;

import fr.inria.vcd.model.ScalarValueChange;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.Value;
import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.command.ScopeCommand;
import fr.inria.vcd.model.command.SimulationCommand;
import fr.inria.vcd.model.command.VarCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.keyword.VarType;
import fr.inria.vcd.model.visitor.TraceCollector;
import fr.inria.vcd.model.visitor.VarCollector;
import fr.inria.vcd.view.VcdFactory;

final public class ScoreBoard {
	private static final HashMap<String, ScoreBoard> map = new HashMap<String, ScoreBoard>();
	private VCDDefinitions vcd;
	private VcdFactory factory = new VcdFactory();
	private ScopeCommand scope;

	private SimulationCommand sim = null;
	private VarCollector vc;
	private TraceCollector tc;

	private ScoreBoard() { // int step
		// this.step = step;
		// vcd
		vcd = new VCDDefinitions();
		vcd.setScope(scope = new ScopeCommand(ScopeType.module, "top"));
		vcd.addSimulation(sim = new SimulationCommand(0)); // step
		vc = new VarCollector(vcd.getDecls());
		vcd.visit(vc);
		tc = new TraceCollector(vc, factory);		
		vcd.visit(tc);
		vcd.setFactory(factory);
	}

	private ScoreBoard(VCDDefinitions vcdin) { // int step
		// this.step = step;
		// vcd
		vcd = vcdin;
		if (vcd == null)
			vcd = new VCDDefinitions();
		vcd.setScope(scope = new ScopeCommand(ScopeType.module, "default"));
		vcd.addSimulation(sim = new SimulationCommand(0)); // step
		vc = new VarCollector(vcd.getDecls());
		vcd.visit(vc);
		tc = new TraceCollector(vc, factory);		
		vcd.visit(tc);
		vcd.setFactory(factory);
	}

	public void start() {
		for (String id : changes.keySet())
			changes.put(id, -1);
	}

	public void tick(int tick) {

		// tick++;
		vcd.addSimulation(sim = new SimulationCommand(tick));// tick *step
	}

	public int getsize() {
		return vcd.getSims().size();
	}

	private int nbVar = 0;

	/** @return id */
	public String addVar(String name, String id) {
		// String id = "*"+name+nbVar;
		scope.addChild(new VarCommand(VarType.integer, 1, id, name));
		nbVar++;
		// changes.put(id, tick);
		return id;
	}

	private HashMap<String, Integer> changes = new HashMap<String, Integer>();

	public void addScalarValueChange(Value v, String id) {

		// changes.put(id, tick);
		add(v, id);
	}

	private void add(Value v, String id) {
		sim.addValueChange(new ScalarValueChange(v, id));
	}

	public void add(ValueChange v) {
		sim.addValueChange(v);
	}

	static public ScoreBoard getScoreboard(String id) {
		ScoreBoard res = map.get(id);
		// if (res == null)
		{
			res = new ScoreBoard();
			map.put(id, res);
		}
		return res;
	}

	static public ScoreBoard getScoreboard(String id, VCDDefinitions vcd) {
		ScoreBoard res = map.get(id);
		// if (res == null)
		{
			res = new ScoreBoard(vcd);
			map.put(id, res);
		}
		return res;
	}

	public VCDDefinitions getVcd() {
		return vcd;
	}
}
