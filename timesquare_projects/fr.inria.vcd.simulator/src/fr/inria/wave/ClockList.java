/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave;

import java.util.ArrayList;

import net.sf.javabdd.BDD;
import fr.inria.wave.enumeration.ClockKind;

public class ClockList {
	/** List of Clock and Timer with Clockdeath */
	protected ArrayList<Clock> listeallclockwithdc;
	/** List of Clock and Timer without Clockdeath */
	protected ArrayList<Clock> listeallclockwithoutdc;
	/** List of Clock  with Clockdeath */
	protected ArrayList<Clock> listeClock;
	/** List of Clock  without Clockdeath */
	protected ArrayList<Clock> listeClockwodc;
	/** List of  Timer */
	protected ArrayList<Clock> listeTimer;

	private int countclock;
	private int Clocknumber;
	private int n = 1;
	private boolean lock = false;
	private Simulation sim;

	/**
	 * 
	 */
	protected ClockList(Simulation sime) {
		super();
		sim = sime;
		listeallclockwithdc = new ArrayList<Clock>();
		listeallclockwithoutdc = new ArrayList<Clock>();
		listeClock = new ArrayList<Clock>();
		listeClockwodc = new ArrayList<Clock>();
		listeTimer = new ArrayList<Clock>();
		Clock c = new Clockdeath();
		c.setSim(sim);
		listeallclockwithdc.add(c);

	}

	public Clock createClock(String name) throws Exception {
		if (lock) {
			throw new Exception("creation verrouiller : " + name);
		}
		Clock c = new Clock(name, n);
		c.setSim(sim);
		n++;
		listeallclockwithdc.add(c);
		listeallclockwithoutdc.add(c);
		return c;
	}

	public Clock createClock(String name, boolean b) throws Exception {
		if (lock) {
			throw new Exception("creation verrouiller : " + name);
		}
		Clock c = new Clock(name, n); //TODO ,b);
		c.setSim(sim);
		n++;
		listeallclockwithdc.add(c);
		listeallclockwithoutdc.add(c);
		return c;
	}

	public boolean checkclock() {
		for (Clock c : listeallclockwithdc) {
			if (c.getKind() == ClockKind.CK_illegal)
				return false;

		}
		return true;

	}

	/**
	 * @return the lock
	 */
	public boolean isLock() {

		return lock;
	}

	/**
	 * 
	 */
	public void locked() {
		sim.println("Clock " + n);
		sim.createData();
		this.lock = true;
	}

	/**
	 * @return the sim
	 */
	protected Simulation getSim() {
		return sim;
	}

	/**
	 * @param sim
	 *            the sim to set
	 */
	protected void setSim(Simulation sim) {
		this.sim = sim;
	}

	public Clock get(int i) {
		return listeallclockwithdc.get(i);
	} 

	public int getCountclock() {
		countclock = n;
		return countclock;
	}

	public ArrayList<Clock> getListeAllclockwithdc() {
		return listeallclockwithdc;
	}

	public ArrayList<Clock> getListeAllclockwithoutdc() {
		return listeallclockwithoutdc;
	}

	public int buildsubliste() { 
		for (Clock c : listeallclockwithdc) {
			if (c.isClock()) {
				listeClock.add(c);
				if (c.id != 0)
					listeClockwodc.add(c);
			}
			if (c.isTimer())
				listeTimer.add(c);

		}

		return 0;
	}

	public ArrayList<Clock> getListeClockwodc() {
		return listeClockwodc;
	}

	public ArrayList<Clock> getListeTimer() { 
		return listeTimer;
	}

	private BDD orclock=null;
	
	
	public BDD getOrclock()
	{
		//orclock.not().printSet();
		return orclock;		
	}

	public void generateBDDclock()
	{
		 orclock = sim.bddf.zero();
		if (n> sim.bddf.varNum())
			sim.bddf.setVarNum(n);
		for (Clock c : listeallclockwithdc)
		{
			c.createBdd();
			if (c.isClock()&& c.id!=0)
			{
				orclock=orclock.or(c.getVarclock());
			}
				
		}
	}

	public int getClocknumber()
	{
		Clocknumber=listeClock.size();		
		return Clocknumber;
	}
	
	public int getClocknumberwodc()
	{
		Clocknumber=listeClockwodc.size();		
		return Clocknumber;
	}
}
