/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave;

import fr.inria.wave.enumeration.Bit;
import fr.inria.wave.enumeration.ClockKind;
import fr.inria.wave.enumeration.ClockStatus;

public class Clockdeath extends Clock {

	static Clockdeath clockdeath;

	/**
	 * @param name
	 */
	public Clockdeath() {
		super("deathClock", 0);
		clockdeath = this;
		super.setKind( ClockKind.CK_clock ); // initialization
		super.setStatus(ClockStatus.CS_finished); // initialization
		super.setPresence( Bit.Z ); // initialization
		super.setFinished( Bit.Z);
		// universaldate=0;
	}

	@Override
	public void setStatus(ClockStatus status) {
		// throw new RuntimeException("non");
	}

	/**
	 * @return the clockdeath
	 */
	public static Clockdeath getClockdeath() {
		return clockdeath;
	}

	@Override
	public void tryForceBit(Bit b) {

	}

	@Override
	public void setFinished(Bit finished) {

	}

	@Override
	public void setPresence(Bit presence) {
		// if (presence!=Bit.Z)
		// throw new Error("DC are "+presence );
	}

	

	@Override
	public void forceBit(Bit b) {

	}

	@Override
	public boolean isClock() {

		return true;
	}

	@Override
	public boolean isIllegal() {

		return false;
	}

	@Override
	public boolean isTimer() {

		return false;
	}

	@Override
	public boolean isUnKnow() {

		return false;
	}

	@Override //TODO
	public void setKind(ClockKind kd) {
		if (kd!=ClockKind.CK_clock ||kd!=ClockKind.CK_unknown  )
			 throw new Error("DC are "+kd );
	}
}
