/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.fire;

import java.util.Random;

import net.sf.javabdd.BDD;
import fr.inria.base.MyManager;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;

public class SimFireRandom extends SimBase implements Imode {

	private Random 	rand = new Random();

	public SimFireRandom() {
		super();
	}

	@Override
	public int endSimul() {
		return 0;
	}
	
	private int generaterandom(TriStateSet s2, BDD bdd)	
	{
		BDD bdd0, bdd1;
		if (bdd.isOne())
			return 0;	
		if (bdd.isZero())
		{
			MyManager.println("DUMP++++");
			return 0;
		}
		bdd0=bdd.low();
		bdd1=bdd.high();
		int var= bdd.var();
		if (bdd0.isZero())
		{
			s2.set(var,TriState.T);
			return generaterandom(s2, bdd1);
		}
		if (bdd1.isZero())
		{
			s2.set(var,TriState.F);
			return generaterandom(s2, bdd0);
		}
		int 	rd = (int) (rand.nextDouble() * 2);
		if (rd==0)
		{
			s2.set(var,TriState.T);
			return generaterandom(s2, bdd1);
		}
		
		s2.set(var,TriState.F);
		return generaterandom(s2, bdd0);		
		//return 0;
	}
	
	@Override
	public int fireDone(TriStateSet s, TriStateSet s2, BDD bddin) {
		if (bddin.isZero())
		{
			MyManager.println("ZERO : " +getSim().getDate());
			for( Clock c:getSim().getLc().getListeAllclockwithdc())
			{
				MyManager.println( c.getName(0)+"  "+c.getIdx() +" " +c.getUniversaldate() +" " );
			}
			MyManager.println();
			return 0;
		}
		/*MyManager.println("One : " +sim.getDate());
		for( Clock c:sim.getLc().getListeAllclockwithdc())
		{
			MyManager.println( c.getName(0)+"  "+c.getIdx() +" " +c.getUniversaldate() +" " +c.getCounter());
		}*/
		super.fireDone(s, s2, bddin);
		BDD bdd=bddin.restrict(getRestrict());		
		generaterandom(s2, bdd);	
		int tmp[] =bdd.varProfile();
		for (int i=0;i<s2.getSize();i++)
		{
			if (s2.get(i)==TriState.X)
			{
				if (rand.nextBoolean())				
					s2.set(i,TriState.T);
				else
					s2.set(i,TriState.F);
			}
			if (tmp[i]==0 && s.get(i)==TriState.X)
			{
				s.set(i,TriState.x);
			}
		}
		return 0;
	}

	@Override
	public int liste() {
		return 0;
	}

	@Override
	public int startSimul() {
		return 0;
	}

	@Override
	public int init(Simulation s) {
		return super.init(s);
	}
}