/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.fire;

import net.sf.javabdd.BDD;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;

public interface Imode {

	public boolean getError();

	public int setSize(int n);

	public int getSize();

	public int fireDone(TriStateSet force, TriStateSet choix, BDD bdd);   

	public int liste();

	public int startSimul();

	public int endSimul();

	public int init(Simulation s);

}
