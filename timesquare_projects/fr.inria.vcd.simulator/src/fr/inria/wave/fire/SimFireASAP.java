/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.fire;

import java.util.Random;

import net.sf.javabdd.BDD;
import fr.inria.base.MyManager;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;

public class SimFireASAP extends SimBase implements Imode {

	private Random 	rand = new Random();
	
	public SimFireASAP() {
		super();
	}
	
	@Override
	public int endSimul() {
		return 0;
	}
	
	private class MyTree {
		private MyTree b0=null;
		private MyTree b1=null;
		private int countZero;
		private int count;
		private int var;
		private boolean zero=false;
		private boolean one=false;
		
		
		public MyTree(MyTree b0, MyTree b1, boolean zero ,boolean one, int var, int czero, int c){
			super();
			this.b0 = b0;
			this.b1 = b1;
			this.one = one;
			this.var = var;
			this.countZero = czero;
			this.zero=zero;
			this.count=c;
		}
		
		@Override
		public String toString() {
			return ""+( b0!=null) +" " +(b1!=null) +" " +one + " " +var +" " +countZero + " " +count ;
		}
	}
	
	private MyTree generateAsap( BDD bdd)	
	{
		BDD bdd0, bdd1;
		if (bdd.isOne())
		{		
			MyTree t=new  MyTree(null,null,false,true,-1,0,1);				
			return t;
		}
		if (bdd.isZero())
		{
			MyTree t=new  MyTree(null,null,true ,false,-1,1000,0);		
			return t;
		}
		bdd0=bdd.low();
		bdd1=bdd.high();		
		MyTree t0= generateAsap( bdd0);		
		MyTree t1= generateAsap( bdd1);
		int n0=t0.countZero+1;
		int n1= t1.countZero;
		int c0=t0.count;
		int c1=t1.count;
		int n=(n0<n1) ? n0 : n1 ;
		int c=0;
		if ((n1<n0) || t0.zero )
		{
			t0=null;
			n0=0;
			c0=0;
			c=c1;
		}
		else
		if ((n1>n0) || t1.zero )
		{
			t1=null;
			n1=0;
			c1=0;
			c=c0;
		}
		else
		if (n1==n0)
		{
			 c=c1+c0;
			 
		}
		MyTree t= new MyTree(t0,t1,false, false,bdd.var(),n,c);	
		return t;		
	}
	
	private int applySolution(MyTree t, TriStateSet s,int rd)
	{
		if (t.one)
			return 0;
		if (t.var==-1)
			return 0;
		if (t.b0==null && t.b1==null)
			return 0;
		if (t.b0==null)
		{
			s.set(t.var,TriState.T);
			applySolution(t.b1,s,rd);
			return 0;
		}
		if (t.b1==null)
		{
			s.set(t.var,TriState.F);
			applySolution(t.b0,s,rd);
			return 0;
		}
		int n=t.b0.count;
		if (rd<n)
		{
			s.set(t.var,TriState.F);
			applySolution(t.b0,s,rd);
		}
		else
		{
			s.set(t.var,TriState.T);
			applySolution(t.b1,s,rd-n);
		}
		return 0;
	}
	
	@Override
	public int fireDone(TriStateSet s, TriStateSet s2, BDD bddin) {
		if (bddin.isZero())
		{
			MyManager.println("ZERO : " +getSim().getDate());
			for( Clock c:getSim().getLc().getListeAllclockwithdc())
			{
				MyManager.println( c.getName(0)+"  "+c.getIdx() +" " +c.getUniversaldate() +" " );
			}
			MyManager.println(); 
			return 0;
		}	
		super.fireDone(s, s2, bddin);
		BDD bdd=bddin.restrict(getRestrict());		
		MyTree t=generateAsap( bdd);	
		int n=t.count;
		int x= rand.nextInt(n);
		applySolution(t, s2, x);
		for (int i=0;i<s2.getSize();i++)
		{
			if (s2.get(i)==TriState.X)
			{
				//MyManager.println("XXX");
				s2.set(i,TriState.T);
			}
		}
		return 0;
	}

	@Override
	public int liste() {
		return 0;
	}

	@Override
	public int startSimul() {
		return 0;
	}

	@Override
	public int init(Simulation s) {
		return super.init(s);
	}
}