package fr.inria.wave.fire;

import java.util.Random;

import net.sf.javabdd.BDD;
import fr.inria.base.MyManager;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;

/**
 * 
 * @author Xavier Seassau
 *
 */
public class SimFireMin extends SimBase implements Imode {

	private Random 	rand = new Random();
	
	public SimFireMin(){
		super();
	}
	
	@Override
	public int endSimul() {
		return 0;
	}
	
	/**
	 * 
	 * @param TriStateSet
	 * @param BDD
	 * @return int
	 */
	private int generateRandom(TriStateSet s2, BDD bdd)	
	{
		BDD bdd0, bdd1;
		if (bdd.isOne())
			return 0;	
		if (bdd.isZero())
		{
			MyManager.println("DUMP++++");
			return 0;
		}
		bdd0=bdd.low();
		bdd1=bdd.high();
		int var= bdd.var();
		if (bdd0.isZero())
		{
			s2.set(var,TriState.T);
			return generateRandom(s2, bdd1);
		}
		if (bdd1.isZero())
		{
			s2.set(var,TriState.F);
			return generateRandom(s2, bdd0);
		}
		int rd = (int) (rand.nextDouble() * 2);
		if (rd==0)
		{
			s2.set(var,TriState.T);
			return generateRandom(s2, bdd1);
		}
		s2.set(var,TriState.F);
		return generateRandom(s2, bdd0);		
	}

	/**
	 * 
	 * @param  BDD
	 * @return BDD
	 */
	private BDD generateMinimumBDD(BDD bdd)
	{
		if (bdd.isOne() || bdd.isZero() ) 
			return bdd;
		int var = bdd.var();
		BDD bddVar = bdd.getFactory().ithVar(var);
		BDD branch0, branch1;
		branch0 = bdd.low();
		branch1 = bdd.high();	
		branch1 = branch1.and(branch1.and(branch0).not());
		branch1 = generateMinimumBDD(branch1);
		branch0 = generateMinimumBDD(branch0);
		return bddVar.ite(branch1, branch0);
	}
	
	@Override
	public int fireDone(TriStateSet force, TriStateSet choix, BDD bddin) {
		if (bddin.isZero())
		{
			MyManager.println("ZERO : " +getSim().getDate());
			for( Clock c:getSim().getLc().getListeAllclockwithdc())
			{
				MyManager.println( c.getName(0)+"  "+c.getIdx() +" " +c.getUniversaldate() +" " );
			}
			MyManager.println(); 
			return 0;
		}
		super.fireDone(force, choix, bddin);
		BDD bdd=bddin.restrict(getRestrict());
		bdd= generateMinimumBDD(bdd);	
		generateRandom(choix,bdd);
		for (int i=0;i<choix.getSize();i++)
		{
			if (choix.get(i)==TriState.X)
			{
				choix.set(i,TriState.F); 
			}
		}
		return 0;
	}

	@Override
	public int init(Simulation s) {
		return super.init(s);
	}
}