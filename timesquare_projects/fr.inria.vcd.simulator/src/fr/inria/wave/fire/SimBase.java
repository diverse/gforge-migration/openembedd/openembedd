/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.fire;

import java.util.ArrayList;
import java.util.List;

import net.sf.javabdd.BDD;
import fr.inria.wave.Clock;
import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;

public class SimBase implements Imode {
	
	private BDD restrict = null;
	private Simulation sim;
	private boolean error = false;
	private List<Clock> lclk = new ArrayList<Clock>();
	private int size;
	
	public Simulation getSim() {
		return sim;
	}
	
	public void setSim(Simulation sim) {
		this.sim = sim;
	}
	
	public BDD getRestrict() {
		return restrict;
	}
	
	public List<Clock> getLclk() {
		return lclk;
	}
	
	public SimBase() {
		super();
	}
	
	public int init(Simulation s) {
		setSim(s);
		return 0;
	}
	
	public int endSimul() {
		return 0;
	}
	
	public int fireDone(TriStateSet s, TriStateSet s2, BDD bdd) {  
		s2.clearX();
		s.clearX();
		s2.set(0,TriState.F);		
		s.set(0,TriState.F);
		restrict= sim.bddf.one();		
		for( Clock c:sim.getLc().getListeAllclockwithoutdc())
		{
			BDD clk=sim.bddf.ithVar(c.id);
			BDD bone=bdd.restrict(clk);
			BDD bzero=bdd.restrict(clk.not());
			if (!bone.isZero() && bzero.isZero() )
			{				
				s.set(c, TriState.T);
				s2.set(c, TriState.T);
				restrict=restrict.and(clk);
			}
			else
			if (!bzero.isZero() && bone.isZero())
			{				
				s.set(c, TriState.F);
				s2.set(c, TriState.F);
				restrict=restrict.and(clk.not());
			}	
			/*bone.free();
			bzero.free();
			clk.free();*/
		}
		return 0;
	}
	
	public int getSize() {
		return size;
	}
	
	public int liste() {
		return 0; 
	}
	
	public int setSize(int n) {
		size = n;
		return 0;
	}
	
	public int startSimul() {
		for (Clock c : sim.getLc().getListeAllclockwithoutdc()) {
			lclk.add(c);
		}
		return 0;
	}
	
	public boolean getError() {
		return error;
	}
}