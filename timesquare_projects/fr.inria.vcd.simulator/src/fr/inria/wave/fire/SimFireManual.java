/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.fire;

import net.sf.javabdd.BDD;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.inria.wave.Simulation;
import fr.inria.wave.data.TriStateSet;
import fr.inria.wave.data.TriStateSet.TriState;
import fr.inria.wave.dialog.CbooleanCombo;
import fr.inria.wave.dialog.SimInterDialog;
import fr.inria.wave.dialog.CbooleanCombo.TypeValue;

public class SimFireManual extends SimBase {

	//private BDD local;
	private SimFireRandom sr = null;
	private SimInterDialog sid = null;
	private Display display ;

	public SimFireManual() {
		sr = new SimFireRandom();
	}

	@Override  
	public int endSimul() {
		sid.close();
		sr.endSimul();
		return super.endSimul();
	}

	@Override
	public int init(Simulation s) {
		setSim(s);
		sr.init(s);				
		return 0;
	}
	
	@Override
	public int fireDone(TriStateSet s, TriStateSet s2, BDD bddin) {
		super.fireDone(s, s2, bddin);
		BDD bdd=bddin.restrict(getRestrict());		
		int tmp[] =bdd.varProfile();
		for (int i=0;i<s2.getSize();i++)
		{
			if (tmp[i]==0 && s.get(i)==TriState.X)
			{
				s.set(i,TriState.x);
			}
		}
		if (sid.getShell() != null) {
			 display = sid.getShell().getDisplay();	
			for (CbooleanCombo lb:sid.cbc) {
				int i= lb.getId(); 				
				switch (s.get(i))
				 {
					 case T : lb.setTpvalue(TypeValue.T) ; break;
					 case F : lb.setTpvalue(TypeValue.F) ; break;					
					 case x : lb.setTpvalue(TypeValue.x) ; break;  
					 case X : lb.setTpvalue(TypeValue.X) ; break;  
					 default: 
				 }				
				lb.getParent().update();
			}
			sid.setInput(bdd);
			display.update();
			sid.postid = -1;
			while (sid.postid == -1) {
				if (sid.getShell() != null) {
					if (!display.readAndDispatch())
						display.sleep();
				} else
					break;
			}
		} else {
			return sr.fireDone(s, s2,bddin);
		}
	
		if (sid.postid == 0 ) {
			return sr.fireDone(s, s2,bddin);
		}
		else
		{
			if (sid.postid==1)
			{
				for (CbooleanCombo lb:sid.cbc) {
					if (lb.getValue()==2)
						s2.set(lb.getId(),TriState.T );
					else
						s2.set(lb.getId(),TriState.F );
				}
			}
		}
		return sid.postid;
	}

	@Override
	public int getSize() {
		return super.getSize();
	}

	@Override
	public int liste() {
		return super.liste();
	}
	
	@Override
	public int startSimul() {

		 super.startSimul();
		sid = new SimInterDialog(new Shell());
		sid.setLclock(getLclk());
		sid.create();
		sid.getShell().setActive();
		sid.getShell().getDisplay().update();
		sid.open();
		//sid.lbut.get(0).setVisible(false);
		return 0;
	}
}