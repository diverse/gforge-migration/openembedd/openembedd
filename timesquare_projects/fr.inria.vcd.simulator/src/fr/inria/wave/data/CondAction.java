/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

import fr.inria.wave.Clock;
import fr.inria.wave.Clockdeath;
import fr.inria.wave.enumeration.ActionKind;

public class CondAction {
	@Override
	public String toString() {

		return "CD : " + actkind + " D: " + decrement.name + " L:"
				+ loader.name + " int " + count;
	}

	ActionKind actkind;
	Clock decrement;
	Clock loader;
	int count;

	public CondAction() {
		super();
		actkind = ActionKind.AK_none;
		decrement = Clockdeath.getClockdeath();
		loader = Clockdeath.getClockdeath();
		count = 0;
	}

	public ActionKind getActkind() {
		return actkind;
	}

	public void setActkind(ActionKind actkind) {
		this.actkind = actkind;
	}

	public Clock getDecrement() {
		return decrement;
	}

	public void setDecrement(Clock decrement) {
		this.decrement = decrement;
	}

	public Clock getLoader() {
		return loader;
	}

	public void setLoader(Clock loader) {
		this.loader = loader;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setAll(ActionKind actkind, Clock decrement, Clock loader,
			int count) {

		this.actkind = actkind;
		this.decrement = decrement;
		this.loader = loader;
		this.count = count;
	}

}
