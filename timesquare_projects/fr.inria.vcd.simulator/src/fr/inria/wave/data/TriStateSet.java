/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

import fr.inria.wave.Clock;

public class TriStateSet {
	private int size;
	private TriState b[];

	public enum TriState {
		T(1, "1"), F(0, "0"), X(2, "X"), Z(3, "Z"), x(4, "x");
		String s;
		int n;

		private TriState(int n, String s) {
			this.s = s;
			this.n = n;
		} 

		@Override
		public String toString() {
			return s;
		}

	}

	public int cardianal(TriState s) {
		int n = 0;
		for (int i = 0; i < size; i++) {
			if (b[i] == s)
				n++;
		}
		return n;

	}


	public TriStateSet(int size) {
		super();
		this.size = size;
		b = new TriState[size];
		clearX();
	}

	public TriStateSet clearX() {
		for (int i = 0; i < size; i++) {
			b[i] = TriState.X;
		}
		return this;
	}

	public TriStateSet clear0() {
		for (int i = 0; i < size; i++) {
			b[i] = TriState.F;
		}
		return this;
	}
	
	public TriStateSet set(int i, TriState bin) {

		if (i >= size)
			return null;
		if (i < 0)
			return null;
		b[i] = bin;
		return this;
	}

	public TriState get(int i) {

		if (i >= size)
			return null;
		if (i < 0)
			return null;
		return b[i];
	}

	public void set2bitset(TriStateSet bs) {
		for (int i = 0; i < size; i++)
			b[i] = bs.b[i];
	}

	public TriStateSet set(Clock c, TriState bin) {

		b[c.id] = bin;
		return this;
	}

	public TriState get(Clock c) {

		return b[c.id];
	}

	@Override
	public String toString() {

		String s = "";
		for (TriState bl : b)
			s += bl.toString();
		return s;

	}

	public int getSize() {
		return size;
	}

	public BitSet convert2Bitset() {
		BitSet b = new BitSet(size);
		for (int i = 0; i < size; i++) {
			b.set(i, get(i) == TriState.T);
		}
		return b;
	}

}
