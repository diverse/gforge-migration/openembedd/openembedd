/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data.bw;

import fr.inria.wave.data.BW;
import fr.inria.wave.enumeration.Bit;

public class Smooth extends BW {
	long d, p;
	long r;
	long base;

	// ,f,df;

	public Smooth(long d, long p, long base) {
		super();
		this.d = d;
		this.p = p;
		r = p - d - 1;
		// (p-d)*base;
		// (d*base) %p;//(p-d)/base; //p-d%p

		this.base = base;
		if (base == 0)
			throw new Error();

		// f=1/p;
		// df=d/p;
	}

	@Override
	public long Clear() {

		return super.Clear();
	}

	@Override
	public Bit kthBit(int x) {

		// double r=(1-df)+f*(x-1);
		// double rc =Math.ceil(r)-r;

		long rc = (r + base * (x)) % p;
		if (rc < base)
			return Bit.T;
		return Bit.F;
	}

	@Override
	public long kthOne(int x) {

		long rc = ((d + p * (x - 1)) / base) + 1;
		// (d+p*(x) )/ base;

		return rc;
	}

	@Override
	public long oneUpTok(int x) {

		// x--;
		// x--;

		// if( x* base<d) return 0;
		long rc = (r + base * (x)) / p;

		return rc;// Math.round(Math.ceil(r));
	}

	@Override
	public String toString() {

		return "BW ";
	}

}
