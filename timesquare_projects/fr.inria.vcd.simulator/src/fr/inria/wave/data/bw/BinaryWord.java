/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data.bw;

import fr.inria.wave.data.BW;
import fr.inria.wave.enumeration.Bit;
import gnu.lists.BitVector;

public class BinaryWord extends BW {
	private BitVector prefix, period;
	protected CBitvector prefix2, periode2;
	int pfone, pflen, prone, prlen;

	public BinaryWord(String prefix, String period) {
		this.prefix = getBitVector(prefix);
		this.period = getBitVector(period);
		build();
	}

	public BinaryWord(BitVector prefix, BitVector period) {
		super();
		this.prefix = prefix;
		this.period = period;
		build();
	}

	BitVector getBitVector(String s) {
		char[] pcTab = s.toCharArray();
		boolean[] pTab = new boolean[pcTab.length];
		for (int i = 0; i < pcTab.length; i++) {
			pTab[i] = pcTab[i] != '0';
		}
		return new BitVector(pTab);
	}

	protected String toString(BitVector bv) {
		char[] tab = new char[bv.size];
		for (int i = 0; i < bv.size; i++) {
			if (bv.booleanAt(i))
				tab[i] = '1';
			else
				tab[i] = '0';
		}
		return new String(tab);
	}

	protected String getHeader() {
		return "0b";
	}

	@Override
	public String toString() {
		return getHeader() + toString(prefix) + "(" + toString(period) + ")";
	}

	static public BinaryWord parseBinaryWord(String s) {
		int i = s.indexOf('(');
		if (i == -1)
			return new BinaryWord(s, "");
		return new BinaryWord(s.substring(0, i), s.substring(i + 1));
	}

	public BitVector getPeriod() {
		return period;
	}

	public BitVector getPrefix() {
		return prefix;
	}

	public String getBody() {

		return toString(prefix) + "(" + toString(period) + ")";
	}

	public int build() {
		pflen = prefix.size();
		prlen = period.size();
		prefix2 = new CBitvector(prefix);
		periode2 = new CBitvector(period);
		pfone = prefix2.getOnes();
		prone = periode2.getOnes();

		return 0;
	}

	@Override
	public long kthOne(int k) {
		try {
			if (pfone >= k) {

				return prefix2.getnOnes(k) + 1;
			}

			int q, r;
			if (prone == 0) {
				return Integer.MAX_VALUE; // infinite

			}
			q = ( k - 1 - pfone) / prone;
			r = ( k - 1 - pfone) % prone;

			return pflen + q * prlen + periode2.getnOnes(r + 1) + 1;
		} catch (Exception e) {
			e.printStackTrace();
			return Integer.MAX_VALUE;
		}
	}

	@Override
	public long oneUpTok(int k) {
		if (k == 0)
			return 0;
		if (pflen == k)
			return pfone;
		if (pflen > k) {
			int n = 0;
			int i = 0;
			for (i = 1; i <= k; i++) {
				if (prefix.booleanAtBuffer(i - 1))
					n++;
			}
			return n;
		}
		if (prone == 0)
			return prlen;
		int q, r;
		q = (k - pflen) / prlen;
		r = (k - pflen) % prlen;
		int n = 0;
		int i = 0;
		for (i = 1; i <= r; i++) {
			if (period.booleanAtBuffer(i - 1))
				n++;
		}
		return pfone + q * prone + n;
	}

	@Override
	public Bit kthBit(int x) {
		if (x <= 0)
			return null;
		if (x <= pflen) {
			if (prefix.booleanAtBuffer(x - 1))
				return Bit.T;
			return Bit.F;
		}
		if (prlen == 0)
			return Bit.Z;
		int r = (x - pflen - 1) % prlen;
		if (period.booleanAtBuffer(r))
			return Bit.T;
		return Bit.F;
	}

}
