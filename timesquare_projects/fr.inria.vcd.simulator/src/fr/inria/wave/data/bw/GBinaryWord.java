/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data.bw;

import java.util.ArrayList;
import java.util.StringTokenizer;

import gnu.lists.BitVector;

public class GBinaryWord extends BinaryWord {
	public GBinaryWord(String prefix, String period) {
		super(prefix, period);
	}

	public GBinaryWord(BitVector prefix, BitVector period) {
		super(prefix, period);

	}

	// discard the leading 0B
	@Override
	BitVector getBitVector(String s) {
		StringTokenizer st = new StringTokenizer(s, ".");
		ArrayList<Boolean> bList = new ArrayList<Boolean>();
		while (st.hasMoreElements()) {
			String e = st.nextToken();

			int iPow = e.indexOf('^');
			boolean bit = e.charAt(0) == '0' ? false : true;
			int pow = 1;
			if (iPow != -1)
				pow = Integer.parseInt(e.substring(iPow + 1));
			for (int i = 0; i < pow; i++)
				bList.add(bit);
		}

		boolean[] bTab = new boolean[bList.size()];
		for (int i = 0; i < bList.size(); i++)
			bTab[i] = bList.get(i);

		return new BitVector(bTab);
	}

	@Override
	protected String getHeader() {
		return "0B";
	}

	@Override
	protected String toString(BitVector bv) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		boolean previous = false;

		for (Object b : bv) {
			boolean bit = (Boolean) b;
			if (count == 0) {
				sb.append(bit ? '1' : '0');
				previous = bit;
				count = 1;
			} else if (previous != bit) {
				if (count > 1)
					sb.append('^').append(count).append('.').append(
							bit ? '1' : '0');
				// count=0; CA - October 31
				count = 1;
				previous = bit;
			} else
				count++;
		}
		if (count > 1) {
			sb.append('^').append(count);
		}

		return sb.toString();
	}

	static public GBinaryWord parseGBinaryWord(String s) {
		int i = s.indexOf('(');
		if (i == -1)
			return new GBinaryWord(s, "");
		return new GBinaryWord(s.substring(0, i), s.substring(i + 1));
	}

}
