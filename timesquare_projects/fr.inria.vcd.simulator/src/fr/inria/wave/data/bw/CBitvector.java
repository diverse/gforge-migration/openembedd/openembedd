/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data.bw;

import gnu.lists.BitVector;

public class CBitvector {
	BitVector b;
	int size;
	int tb[];
	int ones;

	/**
	 * @param b
	 */
	public CBitvector(BitVector b) {
		super();
		this.b = b;
		size = b.size();
		int i, n;
		n = 0;
		for (i = 0; i < size; i++) {
			if (b.booleanAtBuffer(i) == true)
				n++;
		}
		tb = new int[n];
		ones = n;
		n = 0;
		for (i = 0; i < size; i++) {
			if (b.booleanAtBuffer(i) == true) {
				tb[n++] = i;
			}
		}
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @return the ones
	 */
	public int getOnes() {
		return ones;
	}

	public int getnOnes(int n) throws Exception {
		if (n <= 0)
			throw new Exception("N ones <=0");
		if (n > ones)
			throw new Exception("N ones >size");
		return tb[n - 1];
	}

	public int get(int n) {
		if (n <= 0)
			return -2;
		if (n > size)
			return -1;
		if (b.booleanAtBuffer(n))
			return 1;
		return 0;
	}

}
