/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

import java.util.ArrayList;

import fr.inria.wave.Clock;

public class Fireable {
	boolean isfire;
	Clock clk;
	int poids;
	ArrayList<Fireable> coincidende;
	ArrayList<Fireable> impfireable;
	ArrayList<Fireable> onefireable;
	ArrayList<Fireable> orfireable;
	TriStateSet applied;

	/**
	 * 
	 */
	public Fireable(Clock clk, int n) {
		super();
		this.clk = clk;
		applied = new TriStateSet(n);
		applied.clearX();
		coincidende = new ArrayList<Fireable>();
		impfireable = new ArrayList<Fireable>();
		onefireable = new ArrayList<Fireable>();
		orfireable = new ArrayList<Fireable>();
	}

	public TriStateSet getClock() {

		return applied;
	}

}
