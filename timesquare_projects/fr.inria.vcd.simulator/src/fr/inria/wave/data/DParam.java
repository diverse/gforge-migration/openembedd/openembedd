/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

import java.util.Random;

public class DParam {

	public enum DLaw {
		DLconst, DLuniform, DLusers
	}

	// Recurrence laws
	public enum RLaw {
		RLnone, RLsigma, RLpi, RLusers
	}

	String id;
	DLaw law;
	int current;
	double mean;
	int nominal; // for constant
	int initial; // for the recurrence
	int low, high; // for DLuniform
	Histo histo; // for DLusers
	int seedRandom, prevRandom; // for random generation
	// for recurrence law, if any
	RLaw rec;
	Random rand;

	public DParam(String id, DLaw law, RLaw rec) {
		super();
		this.id = id;
		this.law = law;
		this.rec = rec;
		rand = new Random();
	}

	public void setUsers(Histo pb, int ini) {
		setInitial(ini);
		setCurrent(ini);
		setHisto(pb);
		setMean(pb.getMeans());

	}

	public int Dp_getnextvalue() throws Exception {
		int n = 0;

		int newvalue = 0;

		switch (law) {
		case DLconst:
			newvalue = nominal;
			break;
		case DLuniform:
			if (low >= high)
				new Exception("DPnextvalue 3");
			newvalue = DL_uniform(low, high);
			break;
		case DLusers:

			if (histo == null)
				new Exception("DPnextvalue 4");
			newvalue = DL_users(histo);
			break;
		default:
			throw new Exception("DPnextvalue 5");// should not be reached
		}

		if (rec == RLaw.RLnone) {
			n = newvalue;
			current = n;

			return n;
		}

		n = recurFunc(current, newvalue);
		current = n;

		return n;
	}

	public int DL_users(Histo ph) throws Exception {
		int rd, i, k, p, nb;
		int sizes[] = ph.getSizes();
		int values[] = ph.getValue();

		nb =  ph.getSize();
		if (nb == 1)
			return values[0]; // no random
		p =  ph.getPopulation();
		// srand(*previousRandom);
		rd = (int) (Math.random() * p);
		// *previousRandom = (unsigned) rd;
		if (p == 0) {

			throw new Exception("population=0 in DL_users");
		}
		for (i = 0, k = sizes[0]; i < nb - 1; i++) {
			if (rd < k)
				return values[i];
			k += sizes[i + 1];
		}
		return values[nb - 1];

	}

	public int recurFunc(int x, int y) {

		return 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DLaw getLaw() {
		return law;
	}

	public void setLaw(DLaw law) {
		this.law = law;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public int getNominal() {
		return nominal;
	}

	public void setNominal(int nominal) {
		this.nominal = nominal;
	}

	public int getInitial() {
		return initial;
	}

	public void setInitial(int initial) {
		this.initial = initial;
	}

	public int getLow() {
		return low;
	}

	public void setLow(int low) {
		this.low = low;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public Histo getHisto() {
		return histo;
	}

	public void setHisto(Histo histo) {
		this.histo = histo;
	}

	public int getSeedRandom() {
		return seedRandom;
	}

	public void setSeedRandom(int seedRandom) {
		this.seedRandom = seedRandom;
	}

	public int getPrevRandom() {
		return prevRandom;
	}

	public void setPrevRandom(int prevRandom) {
		this.prevRandom = prevRandom;
	}

	public RLaw getRec() {
		return rec;
	}

	public void setRec(RLaw rec) {
		this.rec = rec;
	}

	public int DL_uniform(int low, int high) {
		int rd;
		// srand(*previousRandom);
		// Math.random()

		rd = (int) (rand.nextDouble() * (1 + high - low)) + low;

		// getRandom();
		// *previousRandom = (unsigned) rd;
		return rd;
		// ( low + rd%(high-low+1) );
	}

}
