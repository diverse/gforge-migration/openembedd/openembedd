/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

import fr.inria.wave.enumeration.Bit;

public class BW {

	public BW() {

	}

	public long Clear() {
		return 0;
	}

	public long kthOne(int x) {

		return 0;
	}

	public long oneUpTok(int x) {

		return 0;
	}

	public Bit kthBit(int x) {

		return Bit.Z;
	}

	@Override
	public String toString() {

		return "";
	}

	public final int check(int n) throws Error {
		long i, x = 0;
		for (i = 1; i < n; i++) {
			Bit b = kthBit((int) i);

			if (b == Bit.Z)
				return 0;
			if (b == Bit.T) {

				x++;
				long k = kthOne((int) x);
				// x++;
				if (k != i)
					throw new Error("Check BW 1 " + i + " " + k + " " + x);
			}

			long r = oneUpTok((int) i);
			if (r != x) {

				throw new Error("Check BW 2 " + i + " " + r + " " + x);
			}

		}

		return 0;
	}

}
