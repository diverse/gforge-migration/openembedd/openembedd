/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

public class BitSet {
	private int size;

	private boolean b[];

	public BitSet(int size) {
		super();
		this.size = size;
		b = new boolean[size];
	}

	public void clear() {
		for (int i = 0; i < size; i++) {
			b[i] = false;
		}
	}

	public void set(int i, boolean bin) {

		if (i >= size)
			return;
		if (i < 0)
			return;
		b[i] = bin;
	}

	public void set2bitset(BitSet bs) {
		for (int i = 0; i < size; i++)
			b[i] = bs.b[i];
	}

	public boolean get(int i) {

		if (i >= size)
			return false;
		if (i < 0)
			return false;
		return b[i];
	}

	@Override
	public String toString() {

		String s = "";
		for (boolean bl : b)
			s += (bl) ? '1' : '0';
		return s;

	}

	public int getSize() {
		return size;
	}

	public BitSet not() {
		BitSet b = new BitSet(size);
		for (int i = 0; i < size; i++) {
			b.set(i, !get(i));
		}
		return b;
	}

	public BitSet or(BitSet c) {
		if (size != c.getSize())
			return null;
		BitSet b = new BitSet(size);
		for (int i = 0; i < size; i++) {
			b.set(i, get(i) || c.get(i));
		}
		return b;
	}

	public BitSet and(BitSet c) {
		if (size != c.getSize())
			return null;
		BitSet b = new BitSet(size);
		for (int i = 0; i < size; i++) {
			b.set(i, get(i) && c.get(i));
		}
		return b;
	}

	public int getone() {
		int n = 0;
		for (int i = 0; i < size; i++) {
			if (get(i))
				n++;
		}
		return n;
	}

	public boolean comp(BitSet c) {
		if (size != c.getSize())
			return false;
		for (int i = 0; i < size; i++) {
			if (get(i) != c.get(i))
				return false;

		}
		return true;

	}

}
