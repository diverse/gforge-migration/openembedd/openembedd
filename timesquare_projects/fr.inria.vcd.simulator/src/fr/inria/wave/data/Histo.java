/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.wave.data;

public class Histo {

	int size, population, sizemax;
	int value[], sizes[];

	double means;

	public Histo(int sizein) {
		super();
		this.sizemax = sizein;
		value = new int[sizemax];
		sizes = new int[sizemax];
		means = 1.0;
	}

	public int clear() {
		population = 0;
		return 0;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public double getMeans() {
		return means;
	}

	public void setMeans(double means) {
		this.means = means;
	}

	public void setValue(int id, int v) {
		this.value[id] = v;
	}

	public void setSizes(int id, int v) {
		this.sizes[id] = v;
	}

	public int[] getValue() {
		return value;
	}

	public int[] getSizes() {
		return sizes;
	}

}
