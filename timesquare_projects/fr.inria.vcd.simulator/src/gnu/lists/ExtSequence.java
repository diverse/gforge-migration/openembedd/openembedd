/*******************************************************************************
 * Copyright (c) 2001, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
// Copyright (c) 2003  Per M.A. Bothner and Brainfood Inc.
// This is free software;  for terms and warranty disclaimer see ./COPYING.
package gnu.lists;

/**
 * Abstract helper class for Sequences that use an ExtPosition. That is
 * sequences where it is inefficient to represent a position just using a Pos
 * int.
 */

public abstract class ExtSequence extends AbstractSequence {
	@Override
	public int copyPos(int ipos) {
		if (ipos <= 0)
			return ipos;
		return PositionManager.manager.register(PositionManager
				.getPositionObject(ipos).copy());
	}

	@Override
	protected void releasePos(int ipos) {
		if (ipos > 0)
			PositionManager.manager.release(ipos);
	}

	@Override
	protected boolean isAfterPos(int ipos) {
		if (ipos <= 0)
			return ipos < 0;
		return (PositionManager.getPositionObject(ipos).ipos & 1) != 0;
	}

	@Override
	protected int nextIndex(int ipos) {
		return ipos == -1 ? size() : ipos == 0 ? 0 : PositionManager
				.getPositionObject(ipos).nextIndex();
	}
}
