/*******************************************************************************
 * Copyright (c) 2001, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
// Copyright (c) 2003  Per M.A. Bothner.
// This is free software;  for terms and warranty disclaimer see ./COPYING.
package gnu.lists;

/**
 * A SeqPosition for sequences that need more than a Pos int for a position. For
 * such sequences, a Pos int is an index into a PositionManager, which manages a
 * table of ExtPositions, which may contain more state than a regular
 * SeqPosition does.
 */

public class ExtPosition extends SeqPosition {
	/**
	 * Index into PositionManager.positions, if >= 0. This is used if we need a
	 * single Pos integer for this position.
	 */
	int position = -1;

	@Override
	public int getPos() {
		if (position < 0)
			position = PositionManager.manager.register(this);
		return position;
	}

	@Override
	public void setPos(AbstractSequence seq, int ipos) {
		throw seq.unsupported("setPos");
	}

	@Override
	public final boolean isAfter() {
		return (ipos & 1) != 0;
	}

	@Override
	public void release() {
		if (position >= 0)
			PositionManager.manager.release(position);
		sequence = null;
	}
}
