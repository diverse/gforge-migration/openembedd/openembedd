/*******************************************************************************
 * Copyright (c) 2001, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
// Copyright (c) 2001, 2002, 2003  Per M.A. Bothner and Brainfood Inc.
// This is free software;  for terms and warranty disclaimer see ./COPYING.
package gnu.lists;

/**
 * Implements a stable sequence with sticky positions. I.e if you have a
 * position, it gets automatically updated after insertions and deletions.
 */

public class StableVector extends GapVector {
	/**
	 * This array maps from the exported ipos values (indexes in the positions
	 * array) to the ipos of the underlying SimpleVector base. The first two
	 * elements are reserved for START_POSITION and END_POSITION. Unused
	 * elements in positions are chained together in a free list headed by the
	 * 'free' variable.
	 */
	protected int[] positions;

	/**
	 * The head of the free elements in position, if they are chained. We need
	 * track of available elements in the positions array in two ways: In
	 * unchained mode, there is no free list per se. Instead an index i is
	 * available if positions[i]==FREE_POSITION. This modemakes it easy to loop
	 * over all positions, ignores the unused ones. In chained mode, there is a
	 * free list and if index i is available, then positions[i] is the next
	 * available index, with -1 if there is none. Unchained mode is indicated by
	 * free==-2. In chained mode, free is the first element in the free list, or
	 * -1 if the free list is empty. The main virtue of this convention is that
	 * we don't need a separate list or array for the free list. But we should
	 * get rid of the unchained mode, at least. .
	 */
	protected int free;

	/** An invalid value for an in-use element of positions. */
	protected static final int FREE_POSITION = -1 << 1;

	/** Put all free elements in positions in a chain starting with free. */
	protected void chainFreelist() {
		free = -1;
		for (int i = positions.length; --i > END_POSITION;) {
			int pos = positions[i];
			if (pos == FREE_POSITION) {
				positions[i] = free;
				free = i;
			}
		}
	}

	/** Set all free elements in positions to FREE_POSITION. */
	protected void unchainFreelist() {
		for (int i = free; i >= 0;) {
			int next = positions[i];
			positions[i] = FREE_POSITION;
			i = next;
		}
		free = -2;
	}

	/**
	 * Index in positions for the start position. positions[START_POSITION] is
	 * always 0.
	 */
	static final int START_POSITION = 0;

	/**
	 * Index in positions for the end position. positions[END] is always
	 * (size()<<1)+1.
	 */
	static final int END_POSITION = 1;

	@Override
	public int startPos() {
		return START_POSITION;
	}

	@Override
	public int endPos() {
		return END_POSITION;
	}

	public StableVector(SimpleVector base) {
		super(base);
		positions = new int[16];
		positions[START_POSITION] = 0;
		positions[END_POSITION] = (base.getBufferLength() << 1) | 1;
		free = -1;
		for (int i = positions.length; --i >= 2;) {
			positions[i] = free;
			free = i;
		}
	}

	protected StableVector() {
	}

	protected int allocPositionIndex() {
		if (free == -2)
			chainFreelist();
		if (free < 0) {
			int oldLength = positions.length;
			int[] tmp = new int[2 * oldLength];
			System.arraycopy(positions, 0, tmp, 0, oldLength);
			for (int i = 2 * oldLength; --i >= oldLength;) {
				tmp[i] = free;
				free = i;
			}
			positions = tmp;
		}
		int pos = free;
		free = positions[free];
		return pos;
	}

	@Override
	public int createPos(int index, boolean isAfter) {
		if (index == 0 && !isAfter)
			return START_POSITION;
		else if (isAfter && index == size())
			return END_POSITION;
		if (index > gapStart || (index == gapStart && isAfter))
			index += gapEnd - gapStart;
		int ipos = allocPositionIndex();
		positions[ipos] = (index << 1) | (isAfter ? 1 : 0);
		return ipos;
	}

	@Override
	protected boolean isAfterPos(int ipos) {
		return (positions[ipos] & 1) != 0;
	}

	@Override
	public boolean hasNext(int ipos) {
		int ppos = positions[ipos];
		int index = ppos >>> 1;
		if (index >= gapStart)
			index += gapEnd - gapStart;
		return index < base.getBufferLength();
	}

	@Override
	public int nextPos(int ipos) {
		int ppos = positions[ipos];
		int index = ppos >>> 1;
		if (index >= gapStart)
			index += gapEnd - gapStart;
		if (index >= base.getBufferLength()) {
			releasePos(ipos);
			return 0;
		}
		if (ipos == 0)
			ipos = createPos(0, true);
		positions[ipos] = ppos | 1;
		return ipos;
	}

	@Override
	public int nextIndex(int ipos) {
		int index = positions[ipos] >>> 1;
		if (index > gapStart)
			index -= gapEnd - gapStart;
		return index;
	}

	@Override
	public void releasePos(int ipos) {
		if (ipos >= 2) {
			if (free == -2)
				chainFreelist();
			positions[ipos] = free;
			free = ipos;
		}
	}

	@Override
	public int copyPos(int ipos) {
		if (ipos > END_POSITION) {
			int i = allocPositionIndex();
			positions[i] = positions[ipos];
			ipos = i;
		}
		return ipos;
	}

	@Override
	public void fillPosRange(int fromPos, int toPos, Object value) {
		fillPosRange(positions[fromPos], positions[toPos], value);
	}

	@Override
	protected void shiftGap(int newGapStart) {
		int oldGapStart = gapStart;
		int delta = newGapStart - oldGapStart;
		int low, high, adjust;
		if (delta > 0) {
			low = gapEnd;
			high = low + delta;
			adjust = (oldGapStart - low) << 1;
			// The position corresponding to the new endGap should be adjusted
			// only if it has the isAfter (low-order) bit is clear. Hence the
			// -1.
			low = low << 1;
			high = (high << 1) - 1;
		} else if (newGapStart == oldGapStart)
			return;
		else // newGapStart < gapStart:
		{
			// Positions at the newgapStart should be adjust only if isAfter.
			low = (newGapStart << 1) + 1;
			high = oldGapStart << 1;
			adjust = (gapEnd - oldGapStart) << 1;
		}
		super.shiftGap(newGapStart);

		adjustPositions(low, high, adjust);
	}

	/**
	 * Add a delta to all positions elements that point into a given range.
	 * Assume x==positions[i], then if (unsigned)x>=(unsigned)low && (unsigned)x
	 * <= (unsigned)high, then add delta to positions[i]. Using unsigned
	 * comparisons allows us to compare ipos values, which include both the
	 * index and the isAfter low-order bit.
	 */
	protected void adjustPositions(int low, int high, int delta) {
		if (free >= 0)
			unchainFreelist();

		// Invert the high-order bit, because:
		// (unsigned) X > (unsigned) Y
		// iff (int) (X^0x80000000) > (int) (Y^0x80000000)
		low = low ^ 0x80000000;
		high = high ^ 0x80000000;

		for (int i = positions.length; --i > START_POSITION;) {
			int pos = positions[i];
			if (pos != FREE_POSITION) {
				int index = pos ^ 0x80000000;
				if (index >= low && index <= high)
					positions[i] = pos + delta;
			}
		}
	}

	@Override
	protected void gapReserve(int size) {
		int oldGapEnd = gapEnd;
		int oldLength = base.getBufferLength();
		super.gapReserve(size);
		int newLength = base.getBufferLength();
		adjustPositions(oldGapEnd << 1, (newLength << 1) | 1,
				(newLength - oldLength) << 1);
	}

	@Override
	protected int addPos(int ipos, Object value) {
		int ppos = positions[ipos];
		int index = ppos >>> 1;
		if (index >= gapStart)
			index += gapEnd - gapStart;
		// Force positions[ipos] to have the isAfter property.
		if ((ppos & 1) == 0) {
			if (ipos == 0)
				ipos = createPos(0, true);
			else
				positions[ipos] = ppos | 1;
		}
		add(index, value);
		return ipos;
	}

	@Override
	protected void removePosRange(int ipos0, int ipos1) {
		super.removePosRange(positions[ipos0], positions[ipos1]);

		// adjust positions in gap
		int low = gapStart;
		int high = gapEnd;
		if (free >= 0)
			unchainFreelist();
		for (int i = positions.length; --i > START_POSITION;) {
			int pos = positions[i];
			if (pos != FREE_POSITION) {
				int index = pos >> 1;
				boolean isAfter = (pos & 1) != 0;
				if (isAfter) {
					if (index >= low && index < high)
						positions[i] = (gapEnd << 1) | 1;
				} else {
					if (index > low && index <= high)
						positions[i] = (gapStart << 1);
				}
			}
		}
	}

	@Override
	public void consumePosRange(int iposStart, int iposEnd, Consumer out) {
		super.consumePosRange(positions[iposStart], positions[iposEnd], out);
	}
}
