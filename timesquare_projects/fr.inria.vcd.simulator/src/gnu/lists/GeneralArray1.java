/*******************************************************************************
 * Copyright (c) 2001, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
// Copyright (c) 2001, 2002  Per M.A. Bothner and Brainfood Inc.
// This is free software;  for terms and warranty disclaimer see ./COPYING.
package gnu.lists;

@SuppressWarnings("unchecked")
public class GeneralArray1 extends GeneralArray implements Sequence {
	@Override
	public int createPos(int index, boolean isAfter) {
		return (index << 1) | (isAfter ? 1 : 0);
	}

	@Override
	protected int nextIndex(int ipos) {
		return ipos == -1 ? size() : ipos >>> 1;
	}

	@Override
	public void consumePosRange(int iposStart, int iposEnd, Consumer out) {
		if (out.ignoring())
			return;
		int it = iposStart;
		while (!equals(it, iposEnd)) {
			if (!hasNext(it))
				throw new RuntimeException();
			base.consume(offset + strides[0] * (it >>> 1), 1, out);
			it = nextPos(it);
		}
	}
}
