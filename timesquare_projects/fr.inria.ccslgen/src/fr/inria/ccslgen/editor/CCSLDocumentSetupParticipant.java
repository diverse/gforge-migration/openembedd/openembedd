
package fr.inria.ccslgen.editor;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.DefaultPartitioner;

import fr.inria.ccslgen.CCSLEditorPlugin;
import fr.inria.ccslgen.editor.scanner.CCSLPartitionScanner;

@SuppressWarnings("deprecation")
public class CCSLDocumentSetupParticipant implements IDocumentSetupParticipant {

	
	// Call by Reflection
	public CCSLDocumentSetupParticipant()
	{
		super();
	
	}

	public void setup(IDocument document) {
		if (document instanceof IDocumentExtension3) {
			IDocumentExtension3 extension3= (IDocumentExtension3) document;
			
			IDocumentPartitioner partitioner= new DefaultPartitioner(
						CCSLEditorPlugin.getDefault().getCCSLPartitionScanner(),
						CCSLPartitionScanner.CCSL_PARTITION_TYPES);
			
			extension3.setDocumentPartitioner(CCSLEditorPlugin.CCSL_PARTITIONING, partitioner);
			
			partitioner.connect(document);
		}

	}
}