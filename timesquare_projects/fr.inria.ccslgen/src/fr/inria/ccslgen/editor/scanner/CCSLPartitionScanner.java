package fr.inria.ccslgen.editor.scanner;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

public class CCSLPartitionScanner extends RuleBasedPartitionScanner {
	public final static String CCSL_SLCOMMENT = "__ccsl_sl_comment";

	public final static String[] CCSL_PARTITION_TYPES =
		new String[] {
			CCSL_SLCOMMENT
		};

	public CCSLPartitionScanner() {
		super();

		IToken ccslSLComment = new Token(CCSL_SLCOMMENT);

		IPredicateRule[] rules = new IPredicateRule[] {
				new EndOfLineRule("//", ccslSLComment, '\0', true),
			};
		
		setPredicateRules(rules);
	}
}
