package fr.inria.ccslgen.editor.scanner;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWhitespaceDetector;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import fr.inria.ccslgen.editor.util.CCSLTextAttributeProvider;

public class CCSLCodeScanner extends RuleBasedScanner {
	private static final Character[] fgOperators = new Character[] {
		'+',		'-',		'*',		'/',		'%',
		'=',		'#',		
	};
	
	// Il manque le ".."
	//
	private static final Character[] fgSymboles = new Character[] {
		'{',	'}',	'(',	')',	'[',	']',
		';',	',',	
	};
	
	private static final String[] fgCodes = new String[] {
		"filteredBy",		"discretizedBy",		"restrictedTo",		"delayedFor",		"restrictedToTrailing",
		"followedBy",		"durationBetween",		"coincidentWith",	"sampledOn",		"timerConstraint",
		"alternatesWith",	"weakly",				"rightWeakly",		"leftWeakly",		"oneShotConstraint",
		"nonStrictly",		"leftNonStrictly",		"rightNonStrictly",	"isSubClockOf",		"synchronizesWith",
		"isFinerThan",		"isFasterThan",			"isCoarserThan",	"isSlowerThan",		"restrictedToLeading",
		"isPeriodicOn",		"isSporadicOn",			"hasSameRateAs",	"iprecedes",		"restrictedToInterval",
		"precedes",			"instantOf",			"suchThat",			"sustain",			"strictly",
		"isDisjoint",		"haveMaxDrift",			"haveOffset",		"haveSkew",			"haveDrift",
		"hasStability",		"withIn",				"period",			"offset",			"sync",
		"gap",				"Clock",				"Timer",			"Instant",			"countFor",
		"is",				"min",					"max",				"inf",				"sup",
		"if",				"from",					"to",				"when",				"at",
		"true",				"false",				"in",				"on",				"mult",
		"uniform",			"minus",				"by",				"upto",				"until",
		"wrt",				"or",					"xor",				"and",				"not",
		"inter",			"union",
	};
	
	static class CCSLWordDetector implements IWordDetector {
		public boolean isWordPart(char c) {
		    return (Character.isLetterOrDigit(c));
		}
		
		public boolean isWordStart(char c) {
			return (Character.isLetter(c) || c == '#');
		}
	}
	
	public CCSLCodeScanner(CCSLTextAttributeProvider provider) {
		List<IRule> rules = new ArrayList<IRule>();
		
		IToken opcode 		= new Token(provider.getAttribute(CCSLTextAttributeProvider.CODE_ATTRIBUTE));
		IToken undefined 	= new Token(provider.getAttribute(CCSLTextAttributeProvider.DEFAULT_ATTRIBUTE));
				
		rules.add(new WhitespaceRule(new IWhitespaceDetector() {
		    public boolean isWhitespace(char c) {
                return Character.isWhitespace(c);
            }
		}));		
				
		WordRule wr = new WordRule(new CCSLWordDetector(), undefined);
		
		for(Character c : fgSymboles)
			wr.addWord(c.toString(), opcode);
		
		for(Character c : fgOperators)
			wr.addWord(c.toString(), opcode);
		
		for(String str : fgCodes)
			wr.addWord(str, opcode);
		
		rules.add(wr);
		
		IRule[] param = new IRule[rules.size()];
		rules.toArray(param);
		setRules(param);
	}
}
