package fr.inria.ccslgen.editor;

import org.eclipse.ui.editors.text.TextEditor;

public class CCSLEditor extends TextEditor {
	@Override
	protected void initializeEditor() {
		super.initializeEditor();
		setSourceViewerConfiguration(new CCSLSourceViewerConfiguration());
	}
}
