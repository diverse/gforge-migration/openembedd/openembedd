package fr.inria.ccslgen.editor.util;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

public abstract class CCSLConstantStyles {
	private static RGB CCSL_CODE_COLOR = new RGB(128, 0, 128);
	private static RGB CCSL_SLCOMMENT_COLOR = new RGB(0, 128, 0);
	private static RGB CCSL_DEFAULT_COLOR = new RGB(0, 0, 0);
	
	private static int CCSL_CODE_FONT = SWT.BOLD;
	private static int CCSL_SLCOMMENT_FONT = SWT.NORMAL;
	private static int CCSL_DEFAULT_FONT = SWT.NORMAL;
	
	
	public static TextAttribute getCCSLCodeStyle() {
		return new TextAttribute(new Color(Display.getCurrent(), CCSL_CODE_COLOR), null, CCSL_CODE_FONT);
	}
	
	public static TextAttribute getCCSLSLCommentStyle() {
		return new TextAttribute(new Color(Display.getCurrent(), CCSL_SLCOMMENT_COLOR), null, CCSL_SLCOMMENT_FONT);
	}
	
	public static TextAttribute getCCSLDefaultStyle() {
		return new TextAttribute(new Color(Display.getCurrent(), CCSL_DEFAULT_COLOR), null, CCSL_DEFAULT_FONT);
	}
	
}