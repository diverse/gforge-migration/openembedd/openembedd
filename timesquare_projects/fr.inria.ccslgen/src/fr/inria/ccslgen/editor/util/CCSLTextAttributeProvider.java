package fr.inria.ccslgen.editor.util;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.text.TextAttribute;

public class CCSLTextAttributeProvider {
	public static final String CODE_ATTRIBUTE = "__ccsl_code_attribute";
	public static final String SLCOMMENT_ATTRIBUTE = "__ccsl_slcomment_attribute";
	public static final String DEFAULT_ATTRIBUTE = "__ccsl_default_attribute";
	
	private Map<String, TextAttribute> fAttributes = new HashMap<String, TextAttribute>();

	public CCSLTextAttributeProvider() {
		fAttributes.put(CODE_ATTRIBUTE, CCSLConstantStyles.getCCSLCodeStyle());
		fAttributes.put(SLCOMMENT_ATTRIBUTE, CCSLConstantStyles.getCCSLSLCommentStyle());
		fAttributes.put(DEFAULT_ATTRIBUTE, CCSLConstantStyles.getCCSLDefaultStyle());				
	}
	
	public TextAttribute getAttribute(String type) {
		TextAttribute attr = (TextAttribute)fAttributes.get(type);
		if(attr == null) {
			attr = (TextAttribute) fAttributes.get(DEFAULT_ATTRIBUTE);
		}
		return attr;
	}
}
