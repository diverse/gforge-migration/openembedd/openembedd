package fr.inria.ccslgen;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import fr.inria.ccslgen.editor.scanner.CCSLCodeScanner;
import fr.inria.ccslgen.editor.scanner.CCSLPartitionScanner;
import fr.inria.ccslgen.editor.util.CCSLTextAttributeProvider;

public class CCSLEditorPlugin extends AbstractUIPlugin {
	
	public static String CCSL_PARTITIONING = "__ccsl_partitioning";
	public static final String PLUGIN_ID = "fr.inria.ccslgen";
	
	private static CCSLEditorPlugin plugin;
	
	private ResourceBundle resourceBundle;
	
	private CCSLPartitionScanner fPartitionScanner;	
	
	private CCSLCodeScanner fCCSLCodeScanner;
	
	private CCSLTextAttributeProvider fCCSLTextAttributeProvider;
	
	public CCSLEditorPlugin() {
		super();
		
		plugin = this;
		try {
			resourceBundle = ResourceBundle.getBundle("fr.inria.ccslgen.CCSLEditorPluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}

	public static CCSLEditorPlugin getDefault() {
		return plugin;
	}

	public static String getResourceString(String key) {
		ResourceBundle bundle = CCSLEditorPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
		catch (NullPointerException e) {
			return "";
		}
	}

	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}
	
	public CCSLPartitionScanner getCCSLPartitionScanner() {
		if(fPartitionScanner == null) {
			fPartitionScanner = new CCSLPartitionScanner();
		}
		return fPartitionScanner;
	}
	
	public CCSLTextAttributeProvider getTextAttributeProvider() {
		if(fCCSLTextAttributeProvider == null) {
			fCCSLTextAttributeProvider = new CCSLTextAttributeProvider();
		}
		return fCCSLTextAttributeProvider;
	}
	
	public CCSLCodeScanner getCCSLCodeScanner() {
		if(fCCSLCodeScanner == null) {
			fCCSLCodeScanner = new CCSLCodeScanner(getTextAttributeProvider());
		}
		return fCCSLCodeScanner;
	}
}
