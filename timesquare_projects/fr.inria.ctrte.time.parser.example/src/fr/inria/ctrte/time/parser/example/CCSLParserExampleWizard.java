package fr.inria.ctrte.time.parser.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openembedd.wizards.AbstractNewExampleWizard;
import org.openembedd.wizards.AbstractNewExampleWizard.ProjectDescriptor;

public class CCSLParserExampleWizard extends AbstractNewExampleWizard
{
	protected Collection<ProjectDescriptor> getProjectDescriptors()
	{
		// We need the statements example to be unzipped along with the
		// EMF library example model, edit and editor examples
		List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>(1);
		projects.add(new ProjectDescriptor("fr.inria.ctrte.time.parser.example",
			"zip/fr.inria.ctrte.time.parser.demo.zip", "fr.inria.ctrte.time.parser.demo"));
		return projects;
	}
}
