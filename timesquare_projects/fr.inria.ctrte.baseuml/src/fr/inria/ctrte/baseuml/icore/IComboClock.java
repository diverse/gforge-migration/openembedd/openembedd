package fr.inria.ctrte.baseuml.icore;



public interface IComboClock {

	public String getValueString();
	public void setSelectCombo(int n);
	public int getValue();

	//boolean isMissing();
}