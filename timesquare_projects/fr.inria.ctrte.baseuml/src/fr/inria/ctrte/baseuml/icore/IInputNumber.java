package fr.inria.ctrte.baseuml.icore;

public interface IInputNumber {

	public String getValue();

	//String getValue2();

	public boolean isMissing();
}