/**
 * 
 */
package fr.inria.ctrte.baseuml.listener;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class MyListernerID implements Listener {

	public MyListernerID() {
		super();
	}

	public void handleEvent(Event e) {
		String string = e.text;
		char[] chars = new char[string.length()];
		string.getChars(0, chars.length, chars, 0);
		for (int i = 0; i < chars.length; i++) {
			if (!('0' <= chars[i] && chars[i] <= '9')) 
			if (!('a' <= chars[i] && chars[i] <= 'z')) 
			if (!('A' <= chars[i] && chars[i] <= 'Z')) 
			if (!('_' == chars[i] || chars[i] == '@')) 
			{
				e.doit = false;
				return;
			}
		}
	}
}