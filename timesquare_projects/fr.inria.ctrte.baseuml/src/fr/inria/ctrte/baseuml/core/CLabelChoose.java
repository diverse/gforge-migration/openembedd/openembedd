/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

public class CLabelChoose extends Composite {
	private CCombo c;
	private Label l;
	// private String name = "";
	// private String tooltip = "";
	// public Clist li=null;
	private int value = -1;
	// private Group group = null;
	private Composite parents;
	private String choose[];

	public int getValue() {
		return value;
	}

	@Override
	public void dispose() {

		c.dispose();
		l.dispose();
		parents.dispose();
		super.dispose();
	}

	public CLabelChoose(Composite parent, String namein, String tooltipin,
			String choose[]) {
		super(parent, SWT.NONE);
		// this.name=namein;
		// this.tooltip=tooltipin;

		// parents = new Group(parent, SWT.READ_ONLY);
		this.choose = choose;
		parents = this;
		setSize(520, 55);
		setLayout(null);// new FillLayout ());
		parents.setLayout(null);
		// setSize(520, 35);
		int n = choose.length;
		// group=this;
		// parents=parent;
		parent.setSize(400, 40);
		l = new Label(parents, SWT.READ_ONLY);
		l.setText(namein);
		l.setSize(80, 20);
		l.setLocation(10, 15);
		l.setToolTipText(tooltipin);

		Listener listener = new Listener() {
			public void handleEvent(Event e) {
				Control[] children = parents.getChildren();
				for (int i = 0; i < children.length; i++) {
					Control child = children[i];
					if ((child instanceof Button)
							&& (child.getStyle() & SWT.RADIO) != 0) {
						if (e.widget != child) {
							((Button) child).setSelection(false);

						} else {

							value = i - 1;
						}

					}
					((Button) e.widget).setSelection(true);
				}

			}
		};
		for (int i = 0; i < n; i++) {
			Button button = new Button(parents, SWT.RADIO);
			button.setText(choose[i]);
			button.setLocation(90 + 100 * i, 15);
			button.setSize(100, 20);
			// button.setToolTipText(tooltipin);
			button.addListener(SWT.Selection, listener);
			if (i == 0)
				button.setSelection(true);
		}

		value = 0;
	}

	public void removeAll() {
		c.removeAll();
	}

	/*
	 * public void setlist(Clist l) { li=l; if (li!=null)
	 * c.setItems(li.toArrayString()); else c.setItems( new String[]{ }); }
	 */

	@Override
	public void addListener(int eventType, Listener listener) {
		c.addListener(eventType, listener);

	}

	public void updatelabel(String s) {

	}

	public void updateTooltips(String s) {

	}

	public String getSelection() {
		return choose[value];
	}

	public int getSelectionValue() {
		return value;
	}

}
