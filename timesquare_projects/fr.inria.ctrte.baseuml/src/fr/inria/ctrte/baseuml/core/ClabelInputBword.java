/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import fr.inria.ctrte.baseuml.icore.IInputBword;
import fr.inria.ctrte.baseuml.listener.MyListernerBinary;

public class ClabelInputBword extends ClabelInput implements IInputBword {


	protected String  inp2 = "";


	protected Text tx2 = null; 
	
	
	@Override
	public void dispose() {		
		tx2.dispose();	
		super.dispose();
	}

	public ClabelInputBword(Composite parent, int style, String name) {
		super(parent, style, name,110,100);
		init2();
	}
	
	
	
	

	protected void init2() {
		

		tx.addListener(SWT.Verify, new MyListernerBinary());
		tx.setToolTipText(" Initialisation ");
		tx2 = new Text(this, SWT.NONE);
		tx2.setToolTipText(" Periodic ");
		tx2.setSize(250, 20);
		tx2.setLocation(230, 05);
		tx2.setEditable(true);
		tx2.setEnabled(true);
		tx2.setText("");
		tx2.addModifyListener(new ModifyListener() { 
			public void modifyText(ModifyEvent e) {				
				inp2 = tx2.getText();
				if (icb!=null)
					icb.validation();
			}
		});		
		tx2.addListener(SWT.Verify, new MyListernerBinary());
		
	}

	// tx.get(value();
	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.core.IInputBword#getValue()
	 */
	@Override
	public String getValue() {
		String inputs = "0b" + input  + "(" + inp2 + ")";
		return inputs;

	}
	

}
