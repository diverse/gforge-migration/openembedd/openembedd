/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.uml2.uml.Element;

import code.IList;
import fr.inria.base.MyManager;
import fr.inria.ctrte.baseuml.icore.ICallback4Box;
import fr.inria.ctrte.baseuml.icore.IToolTips;

public class Clabelcombo extends Composite {
	private CCombo c;

	private Label lb;
	protected IList li = null;
	private String[] lst = null;
	private String name = "";
	private String tooltip = "";
	private int value = 0;
	private ICallback4Box icb=null;
	private IToolTips itt=null;


	@Override
	public void dispose() {
		c.dispose();
		lb.dispose();
		// super.dispose();
	}

	public Clabelcombo(Composite parent, String namein, String tooltipin,	IList lin) {
		super(parent, SWT.FILL  );  //READ_ONLY | SWT.FLAT);
		this.name = namein;
		this.tooltip = tooltipin;
		make();
		setlist(lin);
		setSelectCombo(0);
	}

	public Clabelcombo(Composite parent, String namein, String tooltipin,
			String[] lste) {
		super(parent, SWT.FILL );
		this.name = namein;
		this.tooltip = tooltipin;

		make();
		setlist(lste);
		setSelectCombo(0);
	}

	@Override
	public void addListener(int eventType, Listener listener) {
		c.addListener(eventType, listener);

	}

	public int make() {

		lb = new Label(this, SWT.FILL);
		lb.setText(name);
		lb.setSize(110, 20);
		lb.setLocation(10, 05);
		c = new CCombo(this, SWT.FILL  | SWT.READ_ONLY | SWT.FLAT | SWT.TAB);
		c.setCapture(false);
		c.setToolTipText(tooltip);	
		c.select(0);				
		c.setSize(250, 20);
		c.setLocation(120, 05);	
		c.setBackground(new Color(null, 255, 255, 255));
		c.addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			// @Override
			public void widgetSelected(SelectionEvent e) {
 
				value = ((CCombo) e.getSource()).getSelectionIndex();
				if (value != -1) {
					if (li != null) {
						Object o = li.getC(value);
						String s;
						if (itt!=null)
						{
							s= itt.getNewToolTips(o);
							if (s!=null)
								c.setToolTipText(s);
						}
						
						if (icb!=null)
							icb.validation();
					} else {

					}
				}

			}
		});
		value = 0;
		return 1;

	}

	public void removeAll() {
		c.removeAll();
	}

	@Override
	public void setEnabled(boolean enabled) {

		super.setEnabled(enabled);
		c.setEnabled(enabled);
		lb.setEnabled(enabled);
	}

	public void setlist(IList lc) {
		li = lc;
		if (li != null)
			c.setItems(li.toArrayStringAuto());
		else
			c.setItems(new String[] {});
	}

	public void setlist(String[] la) {
		lst = la;

		try {
			if (la != null) {
				c.setItems(la);
			} else
				c.setItems(new String[] { "." });
		} catch (Exception ex) {
			MyManager.printError(ex);
		}

	}

	public void updatelabel(String s) {

	}

	public void updateTooltips(String s) {

	}

	public int getValue() {
		return value;
	}

	public String getValueString() {
		try {
			if (li != null)
				return li.getC(value).getName();
			if (lst != null)
				return lst[value];
			return "";
		} catch (Exception ex) {
			MyManager.printError(ex);
			return "";
		}

	}

	public Object getValueObject() {
		try {
			if (li != null)
				return li.getC(value);
			if (lst != null)
				return lst[value];
			return null;
		} catch (Exception ex) {
			MyManager.printError(ex);
			return null;
		}

	}
	
	public Element getOnlst(int n) {
		return li.get(n);

	}

	public Object getCOnlst(int n) {
		return li.getC(n);

	}

	public void setEnableCombo(boolean b) {
		lb.setEnabled(b);
		c.setEnabled(b);
	}

	public void setSelectCombo(int n) {
		try {
			if (n >= c.getItemCount() || n < 0) {
				return;
			}
			value = n;
			c.select(n);
			if (li == null)
				return;
			Object o = li.getC(n);
			String s;
			if (itt!=null)
			{
				s= itt.getNewToolTips(o);
				if (s!=null)
					c.setToolTipText(s);
			}
		} catch (Exception ex) {
			MyManager.printError(ex);
		}

	}

	public void addCombo(String string) {
		c.add(string);

	}

	public void removeCombo(int id) {
		c.remove(id);

	}

	public void addSelectionListenerCombo(SelectionListener listener) {
		c.addSelectionListener(listener);

	}

	@Override
	public String getToolTipText() {

		return c.getToolTipText();
	}

	@Override
	public void setToolTipText(String string) {

		c.setToolTipText(string);
	}

/*	public String computeNewtoolTip(Object o) {
		return c.getToolTipText();
	}*/

	/*public int actionSelected(Object o) {
		return 0;
	}*/

	@Override
	public void pack() {

		lb.pack();
		c.pack();
	}

	@Override
	public void redraw() {
		lb.redraw();
		c.redraw();
	}

	@Override
	public void update() {
		lb.update();
		c.update();
		super.update();
	}

	public ICallback4Box getIcb() {
		return icb;
	}

	public void setIcb( ICallback4Box icb ) {
		this.icb = icb;
	}

	public IToolTips getItt()
	{
		return itt;
	}

	public void setItt( IToolTips itt )
	{
		this.itt = itt;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setSize(int, int)
	 */
	@Override
	public void setSize( int width , int height )
	{		
		super.setSize(width, height);
		lb.setSize(110, 20);
		lb.setLocation(10, 05);				
		c.setSize(width-125, 20);
		c.setLocation(120, 05);	
		
	}

	
	
}
