/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import fr.inria.ctrte.baseuml.listener.MyListernerID;

public class ClabelInputID extends ClabelInput {

	

	

	public ClabelInputID(Composite parent, int style, String name) 
	{
		super(parent, style, name);
	}

	
	
	


	@Override
	protected void verify() {
		tx.addListener(SWT.Verify, new MyListernerID());
		super.verify();
	}



	@Override
	public String getValue() {
		if (input == null)
			return "0";
		if (input.compareTo("") == 0 ) 
			return "0";
		return input;
	}

	

	
	
}
