/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.inria.base.MyManager;

public class ClabelChooseInput extends Composite {

	private Composite parents;
	private String input = "";

	private Label lb = null;
	private Text tx = null;
	private CCombo c;
	private int value = -1;

	@Override
	public void dispose() {
		c.dispose();
		lb.dispose();
		tx.dispose();
		parents.dispose();
		super.dispose();
	}

	public ClabelChooseInput(Composite parent, int style, String name,
			String choose[]) {
		super(parent, style);

		// name=names;
		// parents = new Group(parent, SWT.READ_ONLY);

		lb = new Label(this, SWT.NONE);
		lb.setText(name);
		lb.setSize(50, 20);
		lb.setLocation(10, 05);

		c = new CCombo(this, SWT.READ_ONLY);

		c.select(0);
		c.setSize(100, 20);
		c.setLocation(70, 05);
		c.setBackground(new Color(null, 255, 255, 255));
		c.addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			
			public void widgetSelected(SelectionEvent e) {

				value = ((CCombo) e.getSource()).getSelectionIndex();

			}
		});
		value = 0;
		setlist(choose);

		tx = new Text(this, SWT.NONE);
		tx.setToolTipText(" ");
		tx.setSize(200, 20);
		tx.setLocation(190, 05);
		tx.setEditable(true);
		tx.setEnabled(true);
		tx.setText("");
		tx.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				input = tx.getText();
			}
		});

	}

	public int getValue() {
		return value;
	}

	public void setlist(String[] la) {
		try {
			if (la != null) {
				c.setItems(la);
			} else
				c.setItems(new String[] { "." });
		} catch (Exception ex) {
			MyManager.printError(ex);
		}

	}

	public String getInput() {
		return input;
	}

}
