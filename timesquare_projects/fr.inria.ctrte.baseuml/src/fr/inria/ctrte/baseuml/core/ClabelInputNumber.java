/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.baseuml.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import fr.inria.ctrte.baseuml.icore.IInputNumber;
import fr.inria.ctrte.baseuml.listener.MyListernerInt;

public class ClabelInputNumber extends ClabelInput implements IInputNumber {

	

	

	public ClabelInputNumber(Composite parent, int style, String name) 
	{
		super(parent, style, name);
	}

	
	 
	


	@Override
	protected void verify() {
		tx.addListener(SWT.Verify, new MyListernerInt());
		super.verify();
	}



	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.core.IInputNumber#getValue()
	 */
	@Override
	public String getValue() {
		if (input == null)
			return "0";
		if (input.compareTo("") == 0 ) 
			return "0";
		return input;
	}

	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.icore.IInputNumber#isMissing()
	 */
	
	public boolean isMissing()
	{	
		if (input == null)
			return true;
		if (input.compareTo("") == 0 ) 
			return true;
		return false;
	}
	
	

}
