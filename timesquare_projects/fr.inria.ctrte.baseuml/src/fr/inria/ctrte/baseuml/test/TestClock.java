package fr.inria.ctrte.baseuml.test;


import fr.inria.ctrte.baseuml.icore.IComboClock;

public class TestClock implements IComboClock {

	
	private int n;
	private boolean haveby;
	
	public int getValue()
	{
		
		return n;
	}

	
	public TestClock(int n, boolean haveby)
	{
		super();
		this.haveby=haveby;
		this.n = n;
	}


	
	public String getValueString()
	{
		if (haveby)
			return "testclock"+n+" by 2 ";
		return "testclock"+n+" ";
	}

	
	public void setSelectCombo( int n )
	{
		

	}

}
