package fr.inria.ctrte.baseuml.test;

import fr.inria.ctrte.baseuml.icore.IInputNumber;

public class TestNumber implements IInputNumber {

	
	public TestNumber()
	{
		super();		
	}


	public String getValue()
	{
	
		return "12";
	}


	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.icore.IInputNumber#isMissing()
	 */
	
	public boolean isMissing()
	{	
		return false;
	}
	
	

}
