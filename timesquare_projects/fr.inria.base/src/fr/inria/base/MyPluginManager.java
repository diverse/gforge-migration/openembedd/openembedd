package fr.inria.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;

public class MyPluginManager {

	
	private static ImageRegistry imageRegistry = null;
	private static final String DEFAULT_IMAGE = "icons/default.gif";
	/**
	 * 
	 * @param plugin
	 *            : name of plugin where is a image
	 * @param key
	 *            : relative path of the image
	 * @return Image
	 */
	public static Image getImage( String plugin , String key )
	{

		if (imageRegistry == null)
		{
			Display display = Display.getDefault();
			imageRegistry = new ImageRegistry(display);
		}

		String rkey = plugin + "@" + key;
		Image image = imageRegistry.get(rkey);
		if (image == null)
		{
			ImageDescriptor desc = AbstractUIPlugin.imageDescriptorFromPlugin(plugin, key);
			imageRegistry.put(rkey, desc);
			image = imageRegistry.get(rkey);
		}
		if ((image == null) && !key.equals(DEFAULT_IMAGE))
		{
			image = getImage(ActivatorBase.PLUGIN_ID, DEFAULT_IMAGE);
		}
		return image;
	}

	/**
	 * 
	 * @param plugin
	 *            : name of plugin where is a image
	 * @param key
	 *            : relative path of the image
	 * @return ImageDescriptor.createFromImage( MyManager.getImage(plugin,
	 *         key));
	 */
	public static ImageDescriptor getImageDescriptor( String plugin , String key )
	{
		// 
		if (imageRegistry == null)
		{
			Display display = Display.getDefault();
			imageRegistry = new ImageRegistry(display);
		}

		String rkey = plugin + "@" + key;
		ImageDescriptor desc = imageRegistry.getDescriptor(rkey);
		if (desc == null)
		{
			desc = AbstractUIPlugin.imageDescriptorFromPlugin(plugin, key);
			imageRegistry.put(rkey, desc);
			desc = imageRegistry.getDescriptor(rkey);
		}
		if ((desc == null) && !key.equals(DEFAULT_IMAGE))
		{
			desc = getImageDescriptor(ActivatorBase.PLUGIN_ID, DEFAULT_IMAGE);
		}
		return desc;
	}


	
	static public Image getEditorIcon( String name )
	{
		Image image = null;
		IConfigurationElement ice = plugin.get(name);
		if (ice == null)
			return null;
		String s = ice.getAttribute("icon");
		String sp = ice.getContributor().getName();
		image = getImage(sp, s);
		return image;
	}
	private static List<String> list = null;
	private static HashMap<String, IConfigurationElement> plugin = null;
	
	static public List<String> listingEditor( boolean refresh )
	{
		if (list == null || refresh)
		{
			IExtensionRegistry reg = Platform.getExtensionRegistry();
			IConfigurationElement[] extensions = reg.getConfigurationElementsFor("org.eclipse.ui.editors");
			list = new ArrayList<String>();
			plugin = new HashMap<String, IConfigurationElement>();
			for (IConfigurationElement ice : extensions)
			{
				String editor = ice.getAttribute("class");
				list.add(editor);
				plugin.put(editor, ice);
			}
		}
		return list;
	}
	
	public static IFileStore getIFileStore(URL url ) throws URISyntaxException
	{		
		return EFS.getLocalFileSystem().getStore(url.toURI());
	}
	
	public static IFileStore getIFileStore(URI uri )
	{
		return EFS.getLocalFileSystem().getStore(uri);
	}
	
	public static IFileStore getIFileStore(IPath ph  )
	{
		return EFS.getLocalFileSystem().getStore(ph);
	}

	public static URL getInstallUrl( Bundle bundle , String path )
	{
		try
		{		
			URL url=bundle.getEntry(path); 
		//	URL url = FileLocator.find(bundle, new Path(path), null);	
			url = FileLocator.resolve(FileLocator.toFileURL(url));
			MyManager.println(FileLocator.toFileURL(url).getFile());
			String ls=url.toString();
			String nls=ls.replaceAll(" ", "%20");
			if (nls.compareTo(ls)!=0)
			{
				url = new URL(nls);
			}
			return url;
		} catch (Exception e)
		{
			MyManager.printError(e);
			return null;
		}
	}

	public static IPath getIpath( AbstractUIPlugin plug )
	{
		return plug.getStateLocation();
	}

	static public IViewPart getShowView( String strid )
	{
		try
		{
			IWorkbench iw = PlatformUI.getWorkbench();
			if (iw == null)
				return null;
			if  (iw.getActiveWorkbenchWindow()==null)
				return null;
			if (iw.getActiveWorkbenchWindow().getActivePage()==null)
				return null;
			return iw.getActiveWorkbenchWindow().getActivePage().showView(strid);
	
		} catch (Exception e)
		{
			MyManager.printError(e);
			return null;
		} catch (Error e)
		{
			MyManager.printError(e);
			return null;
		}
	}

	static public IViewPart getCreateView( String strid )
	{
		try
		{
			IWorkbench iw = PlatformUI.getWorkbench();
			if (iw == null)
				return null;
			return iw.getViewRegistry().find(strid).createView();
		} catch (Exception e)
		{
			MyManager.printError(e);
			return null;
		}
	}

	static public IViewPart getPackageExplorer()
	{
		try
		{
			return getShowView("org.eclipse.jdt.ui.PackageExplorer");
		} catch (Exception e)
		{
			return null;
		} catch (Error e)
		{
			return null;
		}
	}

	/**
	 * 
	 * 
	 * @return Ifile to file f create File is not existe
	 * 
	 */
	
	public static IFile touchFile( IPath path )
	{
		try
		{
			if (!path.toFile().exists())
			{				
				File f = path.toFile();
				OutputStream is = new FileOutputStream(f);
				is.flush();
				is.close();
			}
			refreshWorkspace();
			IPath workspacepath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
			IPath base = new Path(path.toOSString().substring(workspacepath.toOSString().length()));
			return ResourcesPlugin.getWorkspace().getRoot().getFile(base);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static int copy(IFileStore src, IFileStore dst,int options, IProgressMonitor monitor) throws CoreException
	{ 
		src.copy(dst, options, monitor);
		return 0;
	}

	public static IPath getfolder( String namefile )
	{
		try
		{
			IPath p = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(namefile);
			if (p.toFile().isDirectory())
				return p;
			// if (p.toFile().isFile())
			return p.append("..");
		} catch (Exception e)
		{
			MyManager.printError(e);
			return null;
		}
	}

	/*
	 * 
	 * 
	 */
	public static IPath createdir( IPath p , String folder )
	{
		try
		{
			// IFileStore base =
			EFS.getLocalFileSystem().getStore(p.append(folder)).mkdir(EFS.OVERWRITE, null);
			// base.fetchInfo().
			return p.append(folder);
		} catch (Exception e)
		{
			MyManager.printError(e);
		}
		return null;
	}

	public static String convertName( String name )
	{
	
		if (name == null)
			return "";
		if (name.contains("::"))
		{
			name = name.replaceAll("::", "_");
		}
		return name;
	
	}

	/**
	 * Gets the selection.
	 * 
	 * @return the selection
	 */
	static public ISelection getSelection()
	{
		IWorkbenchWindow iww = getActiveWorkbench();
		if (iww == null)
			return null;
		ISelectionService iss = iww.getSelectionService();
		if (iss == null)
			return null;
		return iss.getSelection();
	}

	static public IWorkspaceRoot getWorkspaceRoot()
	{
		try
		{
			return ResourcesPlugin.getWorkspace().getRoot();
		} catch (Exception e)
		{
			return null;
		}
	}

	/***
	 * 
	 */
	public static void refreshWorkspace()
	{
		try
		{
			getWorkspaceRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
			if (getWorkspaceRoot().getProject() != null)
				getWorkspaceRoot().getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (Exception e)
		{
			MyManager.printError(e);
		}
	}

	/**
	 * Gets the active workbench.
	 * 
	 * @return the active workbench
	 */
	static public IWorkbenchWindow getActiveWorkbench()
	{
		try
		{
			return PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		} catch (Exception e)
		{
			MyManager.printError(e);
			return null;
		}
	
	}

	static public IEditorPart getCourantEditor()
	{
		//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().
		// getActiveEditor();
		IWorkbenchWindow iww = getActiveWorkbench();
		if (iww == null)
			return null;
		// iww.addPageListener(new IPageListener(){});
		
		IWorkbenchPage iwp = iww.getActivePage();
		if (iwp == null)
			return null;		
		IEditorPart editorPart = (iwp.getActiveEditor());		
		return editorPart; // editorPart;
	}

	static public IEditorPart getActiveEditor()
	{
		try
		{
			IWorkbenchWindow iww = getActiveWorkbench();
			if (iww == null)
				return null;
			// iww.addPageListener(new IPageListener(){});
	
			IWorkbenchPage iwp = iww.getActivePage();
			if (iwp == null)
				return null;
			IEditorPart editorPart = (iwp.getActiveEditor());
			if (editorPart == null)
				return null;
			if (iwp.getActivePartReference() == null)
				return null;
			String s1 = iwp.getActivePartReference().getId();
			if (editorPart.getEditorSite() == null)
				return null;
			String s2 = editorPart.getEditorSite().getId();
			if (!s1.equals(s2))
			{
				return null;
			}
	
			return editorPart; // editorPart;
		} catch (Exception e)
		{
			return null;
		}
	}

	static public String getPluginVersion( String plug )
	{
		return getPluginData(plug,"Bundle-Version");
	}

	static public String getPluginVendor( String plug )
	{
		return getPluginData(plug,"Bundle-Vendor");
	}

	static public String getPluginDescription( String plug )
	{
		return getPluginData(plug,"Bundle-Description");
	}

	static public String getPluginData(String plug, String label )
	{
		try
		{
			Bundle bdl = Platform.getBundle(plug);
			if (bdl == null)
				return "";
			if (bdl.getHeaders() == null)
				return "";
			if (bdl.getHeaders().get(label)==null)
				return "";
			return bdl.getHeaders().get(label).toString();
		} catch (Exception e)
		{
	
		}
		return "" ;
	}
	
	
}
