package fr.inria.base;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ElementList<T> implements IStructuredContentProvider  {


	private ArrayList<T> elements = new ArrayList<T>();

	public ElementList()
	{
		
	}	
	
	public ArrayList<T> getElements()
	{
		return elements;
	}

	public void addElement( T element )
	{
		elements.add(elements.size(), element);
	}

	public void removeElement( T element )
	{
		elements.remove(element);		
	}

	public int moveElementUp( T element )
	{	
		int index = elements.indexOf(element);
		if (index > 0)
		{
			elements.remove(element);
			elements.add(index - 1, element);
		}	
		return index;
	}

	
	public int moveElementDown( T element )
	{		
		int index = elements.indexOf(element);
		if (index >= 0 && index < elements.size() - 1)
		{
			elements.remove(element);
			elements.add(index + 1, element);
		}		
		return index;
	}


	public boolean contains( Object element )
	{
		return elements.contains(element);
	}

	
	
	public int size()
	{
		return elements.size();
	}
	
	public boolean isEmpty()
	{
		return elements.isEmpty();
	}

	/***/
	
	public void inputChanged( Viewer v , Object oldInput , Object newInput )
	{			
	}

	
	public void dispose()
	{
		
	}

	public Object[] getElements( Object parent )
	{
		return elements.toArray();
	}		
}
