/*
 * 
 */
package fr.inria.base;


import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 */
public class ActivatorBase extends AbstractUIPlugin {

	/** The Constant PLUGIN_ID. */
	public static final String PLUGIN_ID = "fr.inria.base";

	// The shared instance
	/** The plugin. */  
	private static ActivatorBase plugin;

	/**
	 * The constructor.
	 */
	public ActivatorBase()
	{
	
	}
	
	
	


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start( BundleContext context ) throws Exception
	{
		super.start(context);
		plugin = this;
		
		

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop( BundleContext context ) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the shared instance
	 */
	public static ActivatorBase getDefault()
	{
		return plugin;
	}

}
