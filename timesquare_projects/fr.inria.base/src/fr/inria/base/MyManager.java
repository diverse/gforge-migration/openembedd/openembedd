package fr.inria.base;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * @author Benoit Ferrero
 * 
 */
public class MyManager {

	
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy_MMdd_HHmmss");
	

	static private MessageConsole mc = null;
	
	static private MessageConsoleStream iostd = null;
	static private MessageConsoleStream ioerr = null;
	static private MessageConsoleStream iook = null;

	static private PrintStream consolestd = null;
	static private PrintStream consoleerr = null;
	static private PrintStream consoleok = null;
	
	static private MessageConsole mcstack = null;
	static private MessageConsoleStream iostack = null;
	static private PrintStream consolestack = null;
	
	static
	{
		mc = new MessageConsole("Default Console", null);
		mcstack = new MessageConsole("Stack Error", null);	
		
		
		iostd = mc.newMessageStream();	
		ioerr = mc.newMessageStream();	
		iook = mc.newMessageStream();		
		
		iostd.setActivateOnWrite(false);
		ioerr.setActivateOnWrite(false);
		iook.setActivateOnWrite(false);
		
		consolestd = new PrintStream(iostd);
		consoleerr = new PrintStream(ioerr);
		consoleok = new PrintStream(iook);
		
	
		iostack = mcstack.newMessageStream();						
		consolestack = new PrintStream(iostack);
		iostack.setActivateOnWrite(false);
		iostd.setActivateOnWrite(false);
	
		
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { mcstack ,mc});	
		//ConsolePlugin.getDefault().getConsoleManager().
		iostd.setColor(new Color(null, 0,0,0));
		ioerr.setColor(new Color(null, 255,0,0));
		iook.setColor(new Color(null, 0,0,255));
		mc.activate();
		
	}

	private MyManager()
	{
		super();

	}

	static public int println( String s )
	{
		consolestd.println(s);
		return 1;
	}

	static public int print( String s )
	{
		consolestd.print(s);
		return 1;
	}

	static public int println( Object o )
	{
		consolestd.println(o);
		return 1;
	}

	static public int printOKln( String  s )
	{
		try
		{
		consolestd.flush();
		ioerr.flush();
		consoleok.println(s);
		consoleok.flush();
		ioerr.flush();
		}
		catch(Throwable t)
		{
			
		}
		return 1;
	}

	static public int println()
	{
		consolestd.println();
		return 1;
	}

	
	static public int printWhere() 
	{
		Exception e= new Exception();
		consolestd.println(e.getStackTrace()[1]);
		System.err.println(e.getStackTrace()[1]);
		return 1;
	}
	
	static public int printWhere(String s) 
	{
		Exception e= new Exception();
		consolestd.println(e.getStackTrace()[1] +" : " +s);
		System.err.println(e.getStackTrace()[1] +" : " +s);
		return 1;
	}

	static public int printWhere(int n ,String s) 
	{
		Exception e= new Exception();
		consolestd.println(e.getStackTrace()[1] +" : " +s);
		System.err.println(e.getStackTrace()[1] +" : " +s);
		n =  n < e.getStackTrace().length-2  ? n  : e.getStackTrace().length-2;
		for (int i=1;i<n;i++)
		{
			consolestd.println("\t" +e.getStackTrace()[1+i]);		
			System.err.println("\t"+ e.getStackTrace()[1+i]);
		}
		return 1;
	}
	
	static public int flush()
	{
		consolestd.flush();
		return 1;
	}

	static public int printlnError(  String s )
	{
		try
		{
		consolestd.flush();
		ioerr.flush();
		consoleerr.println(s);
		consoleerr.flush();
		ioerr.flush();
		}
		catch (Exception ex) {
		
		}
		return 1;
	}
	static public int printError( Throwable t , String s )
	{
		try
		{
			t.printStackTrace(consolestack);
			t.printStackTrace();
			consolestd.flush();
			ioerr.flush();
			if (t instanceof Error)
				consoleerr.println("ERROR " + s + " :" + t);
			if (t instanceof Exception)
				consoleerr.println("EXCEPTION " + s + " :" + t);
			consoleerr.flush();
			ioerr.flush();

		} catch (Exception ex)
		{

		}
		return 1;
	}

	static public int printError( Throwable t )
	{

		return printError(t, "");
	}

	/***
	 * 
	 * @return Date/Time in String
	 */
	public static String getDateTime()
	{
		return dateFormat.format(new Date());
	}
	
}
