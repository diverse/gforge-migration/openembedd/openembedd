/*
 * 
 */
package fr.inria.base;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;

public class BasedDialog<T> extends Dialog
{

	protected String							possibleText	= "Choose Element";
	protected String							selectedText	= "Selected Element(s)";

	/** The title. */
	protected String							title			= "Windows";

	// buttons

	private Button								addButton;
	private Button								removeButton;
	private Button								upButton;
	private Button								downButton;

	protected Table								tableSource;
	protected Table								tableSelect;

	protected MyTableViewer<T>					tvselect;
	protected MyTableViewer<T>					tvsource;

	protected LabelProvider						labelProvider;
	private DecoratedContentProposalProvider	decoratedContentProposalProvider;

	protected ElementList<T>					elselect		= new ElementList<T>();
	protected ElementList<T>					elsource		= new ElementList<T>();

	protected Label								labelSource;

	protected Label								labelSelect;

	private static final Image					IMG_UP			= MyPluginManager.getImage(ActivatorBase.PLUGIN_ID,
																	"icons/ArrowUp.gif");
	private static final Image					IMG_DOWN		= MyPluginManager.getImage(ActivatorBase.PLUGIN_ID,
																	"icons/ArrowDown.gif");
	private static final Image					IMG_LEFT		= MyPluginManager.getImage(ActivatorBase.PLUGIN_ID,
																	"icons/ArrowLeft.gif");
	private static final Image					IMG_RIGHT		= MyPluginManager.getImage(ActivatorBase.PLUGIN_ID,
																	"icons/ArrowRight.gif");

	/** value that will be available after dialog is closed. */
	protected Object							value			= null;

	BasedDialog<T>								ob;

	public BasedDialog(Shell parentShell, String possibleTextin, String selectedTextin, String titlein)
	{
		super(parentShell);
		ob = this;
		setShellStyle(SWT.RESIZE | super.getShellStyle());
		title = titlein;
		if (possibleTextin != null)
		{
			possibleText = possibleTextin;
		}
		if (selectedTextin != null)
		{
			selectedText = selectedTextin;
		}

		decoratedContentProposalProvider = new DecoratedContentProposalProvider();
		labelProvider = new LabelProvider();

	}

	/**
	 * Creates the buttons for button bar.
	 * 
	 * @param parent
	 * 
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{
		// remove default button = OK (enter pressed)
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent)
	{
		getShell().setMinimumSize(300, 250);
		getShell().setText(title);

		Composite comp = (Composite) super.createDialogArea(parent);
		comp.setLayout(new FormLayout());
		createElements(comp);
		setLayoutData();
		setTableViewers();

		return comp;
	}

	protected final void setUpdown(boolean updownin)
	{
		updown = updownin;
	}

	/**
	 * Sets the table viewers.
	 */
	private void setTableViewers()
	{
		tvsource = new MyTableViewer<T>(tableSource);
		tvselect = new MyTableViewer<T>(tableSelect);

		tvsource.setInput(elsource);
		tvselect.setInput(elselect);

		tableSource.addMouseListener(new AddRemoveItemListener(tvsource, tvselect));
		tableSelect.addMouseListener(new AddRemoveItemListener(tvselect, tvsource));

		addButton.addMouseListener(new AddRemoveButtonListener(tvsource, tvselect));
		removeButton.addMouseListener(new AddRemoveButtonListener(tvselect, tvsource));
		if (updown)
		{
			upButton.addMouseListener(new UpButtonListener());
			downButton.addMouseListener(new DownButtonListener());
		}
	}

	/**
	 * Create elements in the dialog.
	 * 
	 * @param comp
	 *        the parent composite
	 */
	private void createElements(Composite comp)
	{
		// buttons
		addButton = new Button(comp, SWT.CENTER);
		addButton.setImage(IMG_RIGHT);

		removeButton = new Button(comp, SWT.CENTER);
		removeButton.setImage(IMG_LEFT);

		if (updown)
		{
			upButton = new Button(comp, SWT.CENTER);
			upButton.setImage(IMG_UP);

			downButton = new Button(comp, SWT.CENTER);
			downButton.setImage(IMG_DOWN);
		}
		// texts
		labelSource = new Label(comp, SWT.CENTER);
		labelSource.setText(possibleText);

		labelSelect = new Label(comp, SWT.CENTER);
		labelSelect.setText(selectedText);

		tableSource = new Table(comp, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER);
		tableSelect = new Table(comp, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER);

	}

	/**
	 * Sets the layout data.
	 */
	private void setLayoutData()
	{
		// set data to the left elements: possible elements
		// Label: possible elements title
		FormData data;
		data = new FormData();
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		labelSource.setLayoutData(data);

		// buttons
		// add button
		data = new FormData();
		data.top = new FormAttachment(40, 0);
		data.right = new FormAttachment(50, -(ITabbedPropertyConstants.HSPACE) / 2);
		addButton.setLayoutData(data);

		// right button
		data = new FormData();
		data.top = new FormAttachment(addButton, ITabbedPropertyConstants.VSPACE, SWT.BOTTOM);
		data.right = new FormAttachment(50, -(ITabbedPropertyConstants.HSPACE) / 2);
		removeButton.setLayoutData(data);

		// text assist field

		// List: possible elements list
		data = new FormData();
		data.height = 200;
		data.width = 300;
		data.top = new FormAttachment(labelSource, ITabbedPropertyConstants.VSPACE);
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);

		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);

		data.right = new FormAttachment(addButton, -ITabbedPropertyConstants.HSPACE);
		tableSource.setLayoutData(data);

		// Right part
		// Label: selected elements title

		data = new FormData();
		data.top = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.left = new FormAttachment(addButton, ITabbedPropertyConstants.HSPACE);
		labelSelect.setLayoutData(data);

		// Label: selected elements list
		data = new FormData();
		data.left = new FormAttachment(addButton, ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(labelSelect, ITabbedPropertyConstants.VSPACE);
		if (updown)
			data.right = new FormAttachment(upButton, -ITabbedPropertyConstants.HSPACE);
		else
			data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);

		data.width = 300;
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);
		tableSelect.setLayoutData(data);

		if (updown)
		{
			// up button
			data = new FormData();
			data.top = new FormAttachment(40, 0);
			data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
			upButton.setLayoutData(data);
			// down button
			data = new FormData();
			data.top = new FormAttachment(upButton, ITabbedPropertyConstants.HSPACE, SWT.BOTTOM);
			data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
			downButton.setLayoutData(data);
		}
	}

	private boolean	updown	= true;

	/**
	 * Sets the focus.
	 */
	public void setFocus()
	{
		tableSource.setFocus();
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	/**
	 * Returns the default label provider.
	 * 
	 * @return the default label provider
	 */
	protected ILabelProvider getLabelProvider()
	{
		if (labelProvider == null)
		{
			labelProvider = new LabelProvider();
		}
		return labelProvider;
	}

	/**
	 * Returns the default content proposal provider which is added to the Text area (Singleton pattern).
	 * 
	 * @return the content proposal provider which is added to the Text area
	 */
	protected IContentProposalProvider getContentProposalProvider()
	{
		return decoratedContentProposalProvider;
	}

	/**
	 * Checks if the element is selectable or not.
	 * 
	 * @param text
	 *        the text
	 * 
	 * @return <code>true</code> if the element is selectable
	 */
	protected boolean isSelectableElement(String text)
	{
		return false;
	}

	/**
	 * Returns the list of selected elements.
	 * 
	 * @return the list of selected elements
	 */
	public ArrayList<T> getSelectedElements()
	{
		return elselect.getElements();
	}

	/**
	 * Returns the item on which a mouse event happened.
	 * 
	 * @param event
	 *        the mouse event that happened on the item
	 * 
	 * @return the retrieved item
	 */
	protected TableItem getItem(MouseEvent event)
	{
		TableItem[] items = ((Table) event.getSource()).getSelection();
		if (items.length > 0)
		{
			return items[0];
		}
		return null;
	}

	protected class AddRemoveItemListener implements MouseListener
	{

		/**
		 * The Constructor.
		 */
		MyTableViewer<T>	t1;
		MyTableViewer<T>	t2;

		public AddRemoveItemListener(MyTableViewer<T> t1i, MyTableViewer<T> t2i)
		{
			t1 = t1i;
			t2 = t2i;
		}

		/**
		 * Mouse double click.
		 * 
		 * @param e
		 *        the e
		 */
		@SuppressWarnings("unchecked")
		public void mouseDoubleClick(MouseEvent e)
		{
			if (getItem(e) == null)
				return;
			Object o = getItem(e).getData();
			T element = (T) o;
			t1.removeE(element);
			t2.addE(element);
		}

		/**
		 * Mouse down.
		 * 
		 * @param e
		 *        the e
		 */
		public void mouseDown(MouseEvent e)
		{}

		public void mouseUp(MouseEvent e)
		{

		}
	}

	protected class AddRemoveButtonListener implements MouseListener
	{

		MyTableViewer<T>	tvin;
		MyTableViewer<T>	tvout;

		public AddRemoveButtonListener(MyTableViewer<T> _tvin, MyTableViewer<T> _tvout)
		{

			tvin = _tvin;
			tvout = _tvout;
		}

		public void mouseDoubleClick(MouseEvent e)
		{}

		public void mouseDown(MouseEvent e)
		{}

		@SuppressWarnings("unchecked")
		public void mouseUp(MouseEvent e)
		{
			IStructuredSelection selection = (IStructuredSelection) tvin.getSelection();
			Iterator<T> it = selection.iterator();
			while (it.hasNext())
			{
				T t = it.next();
				tvin.removeE(t);
				tvout.addE(t);
			}
		}
	}

	protected class UpButtonListener implements MouseListener
	{

		public UpButtonListener()
		{}

		public void mouseDoubleClick(MouseEvent e)
		{}

		public void mouseDown(MouseEvent e)
		{}

		public void mouseUp(MouseEvent e)
		{
			tvselect.upsel();
		}
	}

	protected class DownButtonListener implements MouseListener
	{

		public DownButtonListener()
		{}

		public void mouseDoubleClick(MouseEvent e)
		{}

		public void mouseDown(MouseEvent e)
		{}

		public void mouseUp(MouseEvent e)
		{
			tvselect.downsel();
		}
	}

	final public class DecoratedContentProposalProvider implements IContentProposalProvider
	{

		public IContentProposal[] getProposals(String contents, int position)
		{

			return new IContentProposal[0];
		}
	}

	class MyTableViewer<TT> extends TableViewer
	{
		/**
		 * Content provider for the possible elements list.
		 */

		ElementList<TT>	el;

		public MyTableViewer(Table table)
		{
			super(table);
			setLabelProvider(ob.getLabelProvider());
		}

		public void setInput(ElementList<TT> elin)
		{
			el = elin;
			setContentProvider(el);
			super.setInput(el);
		}

		public void down(TT element)
		{
			el.moveElementDown(element);
			refresh();
		}

		public void up(TT element)
		{
			el.moveElementUp(element);

			refresh();
		}

		public void addE(TT element)
		{
			el.addElement(element);

			refresh();
		}

		public void removeE(TT element)
		{
			el.removeElement(element);

			refresh();
		}

		/**
		 * @return the el
		 */
		protected final ElementList<TT> getEl()
		{
			return el;
		}

		@SuppressWarnings("unchecked")
		public void upsel()
		{
			IStructuredSelection selection = (IStructuredSelection) getSelection();
			Iterator<TT> it = selection.iterator();
			while (it.hasNext())
			{
				TT element = it.next();
				up(element);
			}
		}

		@SuppressWarnings("unchecked")
		public void downsel()
		{
			IStructuredSelection selection = (IStructuredSelection) getSelection();
			Iterator<TT> it = selection.iterator();
			while (it.hasNext())
			{
				TT element = it.next();
				down(element);
			}
		}

	}
}
