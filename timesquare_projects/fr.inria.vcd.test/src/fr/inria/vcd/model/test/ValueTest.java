package fr.inria.vcd.model.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.vcd.model.Value;

public class ValueTest {

	@Test
	public void testGetValue() {
		for (Value v : Value.values()) {
			String t = v.toString();
			assertTrue(Value.fromString(t) == v);
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFromCharY() {
		Value.fromChar('y');
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFromStringY() {
		Value.fromString("Y");
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFromString() {
		Value.fromString("0X");
	}
	
	@Test
	public void testCaseInsensitive() {
		assertTrue(Value.fromString("X") == Value._x);
		assertTrue(Value.fromString("Z") == Value._z);
	}

}
