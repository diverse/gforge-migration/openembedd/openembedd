package fr.inria.vcd.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.inria.vcd.model.ValueChange;

public class ValueChangeTest {
	static String[] tbs = {
		"0 !", "1 #", "x @", "X !", "z #", "Z @", // scalar
		"b10 !", "bX10 #", "bZX0 @", "b0X10 !", // binary
		"B10 !", "BX10 #", "BZX0 @", "B0X10 !", // Binary
		"r2.5 !", "r-4.253 #", "R4.75 @" // real
	};
	
	@Test
	public void testFromString() {
		for(String tb : tbs) {
			ValueChange vc = ValueChange.fromString(tb);
			assertEquals(vc.toString(), tb.toLowerCase());
		}
		assertEquals(ValueChange.fromString("R.126 !").toString(), "r0.126 !");
	}
}
