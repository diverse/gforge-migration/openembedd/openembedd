package fr.inria.vcd.model.test;

import org.junit.Test;

import fr.inria.vcd.model.IdentifierCode;

public class IdentifierCodeTest {

	@Test(expected=IllegalArgumentException.class)
	public void testLowChar() {
		IdentifierCode.fromString(""+(char)32);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGreatChar() {
		IdentifierCode.fromString(""+(char)167);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testWrongString() {
		IdentifierCode.fromString("toto");
	}	
}
