package fr.inria.vcd.model.test;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for fr.inria.vcd.model.test");
		//$JUnit-BEGIN$
		suite.addTest(new JUnit4TestAdapter(IdentifierCodeTest.class));
		suite.addTest(new JUnit4TestAdapter(ValueChangeTest.class));
		suite.addTest(new JUnit4TestAdapter(ValueTest.class));		
		//$JUnit-END$
		return suite;
	}

}
