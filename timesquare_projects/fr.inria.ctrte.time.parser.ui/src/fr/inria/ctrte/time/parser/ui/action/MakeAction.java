/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser.ui.action;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.uml2.uml.Constraint;

import fr.inria.ctrte.time.parser.ui.work.MakeCCSL;

public class MakeAction implements IObjectActionDelegate {
	private MakeCCSL makeccsl = new MakeCCSL();

	/**
	 * Constructor for Action1.
	 */
	public MakeAction() {
		super();

	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if (constraint == null)
			return;
		makeccsl.parseConstraint(constraint);

	}

	private Constraint constraint = null;

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		constraint = null;
		if (selection instanceof StructuredSelection) {
			constraint = (Constraint) ((StructuredSelection) selection)
					.getFirstElement();
		}

	}

}
