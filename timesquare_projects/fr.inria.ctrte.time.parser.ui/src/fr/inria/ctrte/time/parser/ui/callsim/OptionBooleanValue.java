/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser.ui.callsim;

import java.util.HashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.inria.base.MyManager;



public class OptionBooleanValue extends Dialog {

	Label text = null;

	public class Mylistener implements SelectionListener {
		String id;
		private HashMap<String, Boolean> hmb;

		public Mylistener(String id, HashMap<String, Boolean> hmbi) {
			super();
			this.id = id;
			hmb = hmbi;
			MyManager.println("id " + id);
		}

		
		public void widgetDefaultSelected(SelectionEvent e) {

			widgetSelected(e);
		}

		
		public void widgetSelected(SelectionEvent e) {
			int value = ((Combo) e.getSource()).getSelectionIndex();
			MyManager.println(">>> " + value + " " + id);
			if (value == 0) {
				hmb.put(id, null);
			}
			if (value == 1) {
				hmb.put(id, true);
			}
			if (value == 2) {
				hmb.put(id, false);
			}
		}

	}

	String value[] = { "undefine", "true", "false" };
	HashMap<String, Boolean> hmb;

	public OptionBooleanValue(Shell parentShell, HashMap<String, Boolean> list) {
		super(parentShell);
		hmb =  list;

	}

	@Override
	protected Control createButtonBar(Composite parent) {

		return super.createButtonBar(parent);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		/*
		 * bar.cancel = createButton(parent, IDialogConstants.CANCEL_ID,
		 * IDialogConstants.CANCEL_LABEL, false);
		 */
		//bar.ok = 
			createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, false);

	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		text = new Label(parent, SWT.FILL);
		text.setText("Choose value for boolean");
		Group g = new Group(parent, SWT.NONE);
		for (String s : hmb.keySet()) {
			Label l = new Label(g, SWT.NONE);
			Combo c = new Combo(g, SWT.READ_ONLY);
			g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			g.setLayout(new GridLayout(2, true));
			c.setItems(value);

			c.setBackground(new Color(null, new RGB(255, 255, 255)));
			c.setVisibleItemCount(3);
			l.setText(s);
			Boolean b = hmb.get(s);
			if (b == null)
				c.select(0);
			else if (b == true)
				c.select(1);
			else
				c.select(2);

			c.addSelectionListener(new Mylistener(s, hmb));
		}
		return super.createDialogArea(parent);
	}

	

}
