package fr.inria.ctrte.time.parser.ui.ccsl;

/*import grammaire.LangLexer;
import grammaire.LangParser;
import grammaire.analyse.ASTMask;*/

import fr.inria.base.MyManager;
import fr.inria.ctrte.time.parser.view.SimulationCCSLViewPart;
import grammaire.TestSyntax;

import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class SimulationCCSLfile implements  IObjectActionDelegate {

	public SimulationCCSLfile()
	{
		
	}
 
	
//	LangLexer lexer=null;  
	//LangParser parser =null;
	

		
	

	
	public void run( IAction action )
	{	
		
		try
		{
		TestSyntax ts=null;	
		ts=new TestSyntax(file.getContents());
		ts.getDico().disp();
		//MyManager.println("...");	
		ts.dispAst("ccsl simulation");
		if (ts.getnbrerror()>0)
		{
			MyManager.printlnError("Error Parsing :");
			MyManager.printlnError(ts.getstrerror());
			HashMap<String, Boolean>  hmb= new HashMap<String, Boolean> ();
			ts.getDico().getBooldic().getnamebol(hmb);
			SimulationCCSLViewPart.getCourant().configure(file.getName(),ts.getstrerror(), 1, file, ts.getAstMask(), hmb);		  	
			return ;
		}
		HashMap<String, Boolean>  hmb= new HashMap<String, Boolean> ();
		ts.getDico().getBooldic().getnamebol(hmb);
		SimulationCCSLViewPart.getCourant().configure(file.getName(), "__..", 0, file, ts.getAstMask(), hmb);
		}catch (Throwable ex) {
			ex.printStackTrace();		
			MyManager.printlnError("Error Parsing :");
			
			HashMap<String, Boolean>  hmb= new HashMap<String, Boolean> ();
			
			SimulationCCSLViewPart.getCourant().configure(file.getName(),"ERROR" , 1, file, null, hmb);		  	
			return ;
		}
		
	}

	private IFile file;

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	
	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof TreeSelection)) return;
		TreeSelection ts = (TreeSelection)selection;
		myLoad(ts.getFirstElement());
	}
	private void myLoad(Object o) {		
		if(!(o instanceof IFile)) return;
		this.file = (IFile)o;
		MyManager.println(file);
	}



	
	public void setActivePart( IAction action , IWorkbenchPart targetPart )
	{
		
		
	}

	
}
