package fr.inria.ctrte.time.parser.ui.ccsl;

/*import grammaire.LangLexer;
import grammaire.LangParser;
import grammaire.analyse.ASTMask;*/

import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.uml2.uml.Package;

import fr.inria.base.MyManager;
import fr.inria.ctrte.time.parser.ui.work.MonParser;
import fr.inria.ctrte.time.parser.view.SimulationCCSLViewPart;

public class SimulationUMLfile implements  IObjectActionDelegate {

	public SimulationUMLfile()
	{		
	}
 
	
//	LangLexer lexer=null;  
	//LangParser parser =null;
	

		
	

	
	public void run( IAction action )
	{	
		//TODO gestion uml
		try
		{
			MyManager.printOKln("Run simulation :" + file);
			
			
			IPath ph = file.getProject().getLocation().append("..").append(file.getFullPath().toString());
			URI uri = URI.createFileURI(ph.toOSString());
			
			Resource resource = new ResourceSetImpl().getResource(uri, true);			
		//	System.out.println(resource);
		//	System.out.println(resource.getContents());
			Object o= resource.getContents().get(0);		
			MyManager.println(o);
			if (o instanceof Package)
			{
				MonParser p = (new MonParser());
				p.setFile(file);
				p.parse((Package ) o,false);
			}
		
	
		//	UMLFactory.eINSTANCE. 
		}catch (Throwable ex) {
			ex.printStackTrace();		
			MyManager.printlnError("Error Parsing :");
			
			HashMap<String, Boolean>  hmb= new HashMap<String, Boolean> ();
			
			SimulationCCSLViewPart.getCourant().configure(file.getName(),"ERROR" , 1, file, null, hmb);		  	
			return ;
		}
		
	}

	private IFile file;

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	
	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof TreeSelection)) return;
		TreeSelection ts = (TreeSelection)selection;
		myLoad(ts.getFirstElement());		
	}
	private void myLoad(Object o) {	
		file=null;
		if(!(o instanceof IFile)) return;
			this.file = (IFile)o;
	
	}



	
	public void setActivePart( IAction action , IWorkbenchPart targetPart )
	{
		
		
	}

	
}
