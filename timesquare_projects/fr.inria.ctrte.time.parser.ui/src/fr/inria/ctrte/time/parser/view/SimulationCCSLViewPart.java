package fr.inria.ctrte.time.parser.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.uml2.uml.NamedElement;

import code.view.SimulationCall;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.core.Ccheckbox;
import fr.inria.ctrte.baseuml.core.ClabelInput;
import fr.inria.ctrte.baseuml.core.ClabelInputNumber;
import fr.inria.ctrte.baseuml.core.Clabelcombo;
import fr.inria.ctrte.time.parser.ui.ActivatorTimeUI;
import fr.inria.ctrte.time.parser.ui.callsim.OptionBooleanValue;
import fr.inria.wave.DictionaryPolitic;

import grammaire.analyse.ASTMask;

public class SimulationCCSLViewPart extends ViewPart {

	final static public String VIEWID = "fr.inria.ctrte.time.simview";
	private static SimulationCCSLViewPart courant = null;

	/***
	 * 
	 * @author Benoit Ferrero
	 * 
	 *         Memorise l'etat ( ouvert/ ferme ) de la bar
	 * 
	 */
	protected final class ExpandBarLsitener implements ExpandListener {

		public void itemExpanded(ExpandEvent e) {
			mybar.eb.redraw();
			mybar.eb.update();
			mybar.setitem(e.item, true);
		}

		
		public void itemCollapsed(ExpandEvent e) {
			mybar.eb.redraw();
			mybar.eb.update();
			mybar.setitem(e.item, false);
		}
	}

	protected final class SimulationButtonListener implements Listener {
		public void handleEvent(Event e) {
			
			try {
				_simul.simul.setEnabled(false);
				if( modeUML && ((elem.eResource()==null) ||	(elem.eResource().getURI()==null)))
				{
					MessageDialog.openError(_simul.simul.getShell(), "Error", "perte de ref");
					_simul.simul.setEnabled(true);
					return ;					
				}
				boolean b = _simul.tpl.getSelection();
				boolean g = _simul.ghost.getSelection();
				MyManager.println("Option Simulation :\n{ ");
				MyManager.println("\tLength _simul: " + _simul.cn.getValue() + ";" );
				MyManager.println("\tPolitic : " + _simul.lcpolsim.getValue() + "  ;\n\tpulse " + b +";" );
				MyManager.println("\tghost " + g +" ; \n}");
				MyManager.flush();

				/*  Simulation length */
				int l = 100; 
					l = Integer.parseInt(_simul.cn.getValue());
				if (l > 20000)
					l = 20000;
				if (l == 0)
					l = 100;
				/****/
				if (modeUML)
					s = new SimulationCall(mask, hmb, elem);
				else
				if (modeUMLFile )
					s = new SimulationCall(mask, hmb, elem,file);
				else
					s = new SimulationCall(mask, hmb, file	);
				//_simul.ltime.getValueString();
				String tmp=null;
				if (_simul.ltime.getValue()!=0)
					tmp=_simul.ltime.getValueString();				
				s.setup(l, _simul.lcpolsim.getValue(), b, g,tmp);

				s.run();
				
				
				
			} catch (Throwable ex) {
				MyManager.printError(ex);
		
			}
			_simul.simul.setEnabled(true);
			//e.notify();
		}
	}

	
	protected final class EditbooleanValueListener implements Listener {
		public void handleEvent(Event e) {
			OptionBooleanValue d = new OptionBooleanValue(display
					.getActiveShell(), hmb);
			d.create();
			d.open();
			_simul.l.setText(computebol());
			_simul.l.getParent().update();
			_simul.l.getParent().redraw();

		}
	}


	static public class _image {
		final static public Image imageparseerr = MyPluginManager.getImage(
				ActivatorTimeUI.PLUGIN_ID, "icons/redcircle.PNG");
		final static public Image imagewarning = MyPluginManager.getImage(
				ActivatorTimeUI.PLUGIN_ID, "icons /orangecircle.png");
		final static public Image imageok = MyPluginManager.getImage(
				ActivatorTimeUI.PLUGIN_ID, "icons/greencircle.PNG");
		final static public Image imageunparse = MyPluginManager.getImage(
				ActivatorTimeUI.PLUGIN_ID, "icons/error.PNG");

		final static public Image tbimage[] = new Image[] { imageok,
				imagewarning, imageparseerr, imageunparse };
	}

	public class _expand {
		public ExpandBar eb = null;
		public ExpandItem item2 = null;
		public ExpandItem item1 = null;
		public ExpandItem item0 = null;
		public boolean b[] = new boolean[] { false, true, false };

		public void reset() {
			b = new boolean[] { false, true, false };
		}

		public void resetleak() {
			b [1]=true;
			if (_constraint.lb.size() != tb[0])
				b[0]=true;
			
			
		
	
		}
		
		public void dispose() {
			if (item0!=null)
			if (item0.getControl() != null)
				item0.getControl().dispose();
			if (item1!=null)
			if (item1.getControl() != null)
				item1.getControl().dispose();
			if (item2!=null)
			if (item2.getControl() != null)
				item2.getControl().dispose();
			if (eb!=null)
				eb.dispose();
		}

		public ExpandItem createItem(int n, String s, Composite c, Image i) {
			ExpandItem item = new ExpandItem(eb, SWT.FILL, n);
			item.setText(s);
			item.setControl(c);
			if (c != null)
				item.setHeight(c.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			item.setImage(i);
			return item;
		}

		public void setFocus() {
			if (eb != null)
				if (!eb.isDisposed())
					eb.setFocus();
			if (item1 != null)
				if (!item1.isDisposed())
					if (item1.getControl() != null)
						if (!item1.getControl().isDisposed())
							item1.getControl().setFocus();

		}

		public void setitem(Object e, boolean b) {
			if (e == mybar.item0)
				mybar.b[0] = b;
			if (e == mybar.item1)
				mybar.b[1] = b;
			if (e == mybar.item2)
				mybar.b[2] = b;
		}

		public void expands() {
			if (b[0])
				item0.setExpanded(true);
			if (b[1])
				item1.setExpanded(true);
			if (b[2])
				item2.setExpanded(true);
		}

		public _expand() {
		}

	}

	static public class _simul {
		static public ClabelInput cname = null;
		static public ClabelInputNumber cn = null;
		static public Clabelcombo lcpolsim = null;
		static public Clabelcombo ltime = null;
		static public Ccheckbox tpl = null;
		static public Ccheckbox ghost = null;

		static public Label l;
		static public Button boloption;
		static public Button simul = null;
	}

	static public class _simulvalue {
		static public int step = 100;
		static public int mode = 0;
		static public boolean pulse;
		static public String name = "_simul";

	}



	private _expand mybar = new _expand();
	boolean modeUML= true;
	boolean modeUMLFile= false;
	boolean modeCCSL= false;
	private IFile file;

	static public class _constraint {
		static public List<String> nameconstraint;
		static public List<String> l2;
		static public List<Integer> lb;
	}
	private Display display = Display.getDefault();
	private ASTMask mask;
	private int n = 0;
	private CTabFolder folder = null;
	private int tb[] = new int[] { 0, 0, 0, 0 };
	private NamedElement elem;
	private SimulationCall s = null;
	static private Composite composite = null;
	private HashMap<String, Boolean> hmb = null;

	// ScrolledComposite scrolledcomposite;

	public void setHmb(HashMap<String, Boolean> hmb) {
		this.hmb = hmb;
	}

	public SimulationCCSLViewPart() {
		courant = this;
	//	MyManager.println("<<<SimView>>>");

	}

	@Override
	protected void finalize() throws Throwable {
		MyManager.println("Finalize " + this);
		super.finalize();
	}

	public int clear() {

		if (composite==null)
			return 0 ;
		if (composite.isDisposed())
			return 0;
		for (Control c : composite.getChildren()) {

			c.dispose();

		}
		return 0;
	}

	public int drawcmd() {
		if ((_constraint.nameconstraint != null) && (_constraint.l2 != null)) {
			clear();
			createsimuloption();
			update();
		}
		return 0;
	}

	public int update() {
		composite.redraw();
		return 0;
	}

	@Override
	public void createPartControl(Composite parent) {
		
		//MyManager.println("parent  " +parent );
		composite = parent;		
		drawcmd();
	}

	@Override
	public void setFocus() {

		courant = this;
		try {
			mybar.setFocus();
			if (composite != null)
				if (!composite.isDisposed())
					composite.setFocus();
		} catch (Exception e) {
			MyManager.printError(e);
		}

	}

	
	
	public void redraw() {
		clear();
		drawcmd();
		update();
		
	}

	
	
	
	@Override
	public void dispose() {
		MyManager.println("Disposed");
		courant = null;
		composite.dispose();
		mybar.dispose();
		_constraint.l2=null;
		_constraint.lb=null;
		_constraint.nameconstraint=null;
		
		super.dispose();
	}

	public static SimulationCCSLViewPart getCourant() {
		if (courant == null)
		{
			try {
				MyPluginManager.getShowView(VIEWID);
				courant = (SimulationCCSLViewPart) MyPluginManager.getCreateView(VIEWID);
				MyPluginManager.getShowView(VIEWID);
				courant.setFocus();
			} catch (Exception ex) {
				MyManager.printError(ex, "Simulation View");
			}
		}		
		return courant;
	}

	private void booleanoption(Composite gr) {
		Group gr3 = null;
		gr3 = new Group(gr, SWT.NONE);
		gr3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gr3.setText("Boolean 4 guard");
		gr3.setLayout(new GridLayout(2, false));
		_simul.boloption = createButton(gr3, "   Edit   ");
		_simul.l = new Label(gr3, SWT.NONE);
		_simul.l.setText(computebol());
		_simul.l.setEnabled(true);
		_simul.boloption.addListener(SWT.Selection,
				new EditbooleanValueListener());
	}

	public Image getIconInformation() {
		return display.getSystemImage(SWT.ICON_INFORMATION);
	}

	public void createsimuloption() {

		if (composite==null )
			return ;
		if (composite.isDisposed())
			return ; 
	/*	if (_constraint.nameconstraint==null)
			return;
		if (_constraint.lb==null)
			return;*/
		if (file==null)
			return;
		if (elem==null && modeUML)
			return ;
		mybar.eb = new ExpandBar(composite, SWT.V_SCROLL | SWT.H_SCROLL);
		mybar.eb.addExpandListener(new ExpandBarLsitener());

		Composite composite2 = new Composite(mybar.eb, SWT.FILL);
		dispinfoconstraint(composite2);
		String id = "Constraint information :"; 
		if (elem!=null)	
			id+=elem.getName();

		Composite gr = new Composite(mybar.eb, SWT.FILL);
		dispsimulation(gr);
		Composite gr2 = new Composite(mybar.eb, SWT.FILL);
		dispinfo(gr2);
		String m="Source information :";
		if (modeUML) m+=" UML" ; else m+=" texte";
		
		mybar.item2 = mybar.createItem(0, m, gr2,getIconInformation());
		mybar.item0 = mybar.createItem(1, id, composite2, getIconInformation());
		mybar.item1 = mybar.createItem(2, "Simulation option", gr,	getIconInformation());

		mybar.expands();

		mybar.eb.setSpacing(5);
		Color c=ColorConstants.menuBackground ;
		c= new Color(c.getDevice(), c.getRed()-20, c.getGreen()-20, c.getBlue()-20 );
		mybar.eb.setBackground(c);//lightGray);
		mybar.eb.setForeground(ColorConstants.menuForeground);
		mybar.eb.redraw();
		mybar.eb.layout();
		mybar.eb.setEnabled(true);
		mybar.eb.pack();		
		Point p = composite.getSize();
		mybar.eb.setSize(p);

	}

	public String computebol() {
		String s = "";
		for (String ls : hmb.keySet()) {
			String m = "";
			Boolean bl = hmb.get(ls);
			if (bl == null) {
				m = "X";
			} else {
				if (bl == true)
					m = "1";
				else
					m = "0";
			}

			s += ls + ":" + m + ", ";
		}
		return new String (s.substring(0, s.length() - 2));
	}

	protected Button createButton(Composite parent, String label) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		return button;
	}

	
	
	public static String unit[]=new  String[]{ " s" , "ms" ,"us" ,"ns" , "ps" , "fs" };
	public static String unitlist[]= createListe();
	
	static protected String [] createListe()
	{		
		String x[]=new String[3*6+1];
		x[0]="tick" ;
		for (int i=0;i<6; i++)
		{
			x[1+i*3]= "100 " +unit[i];
			x[2+i*3]= "10  " +unit[i];
			x[3+i*3]= "1   " +unit[i];
		}
		return x;
	}
	
	public int dispsimulation(Composite gr) {
		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd1.minimumHeight = 100;
		gd1.minimumWidth = 250;
		gr.setLayoutData(gd1);
		GridLayout gl = new GridLayout(1, true);
		gr.setLayout(gl);
		_simul.cname = new ClabelInput(gr, SWT.FILL, "Name simulation");
		int touch = 1;
		if (hmb != null && hmb.size() != 0) {
			booleanoption(gr);
		}
		Group gr2 = new Group(gr, SWT.FILL);
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);

		gr2.setLayoutData(gd);
		_simul.cn = new ClabelInputNumber(gr2, SWT.FILL, "length");
		_simul.cn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		_simul.lcpolsim = new Clabelcombo(gr2, "Mode", "...", 
				DictionaryPolitic.getInstance().getLnameArray()	);
		_simul.lcpolsim.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		_simul.ltime=new Clabelcombo(gr2, "Time Scale", "...",unitlist);
		_simul.ltime.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		_simul.tpl = new Ccheckbox(gr2, "Option :  ", "Pulse ", true);
		_simul.tpl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		_simul.ghost = new Ccheckbox(gr2, "Option :  ", "Ghost ", false);
		_simul.ghost.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,	false));
		

		gr2.setLayout(new GridLayout(touch, true));

		_simul.simul = createButton(gr, "Simulation");
		_simul.simul.addListener(SWT.Selection, new SimulationButtonListener());
		if (haveerror)
		{
			_simul.simul.setEnabled(false);
		}
		gr.setBackground(ColorConstants.menuBackground);
		gr2.layout();
		gr2.pack();
		gr.layout();
		gr.pack();

		return 0;
	}

	public int dispinfo(Composite gr2) {
		Label l = new Label(gr2, SWT.FILL);
		if ((file==null) || (elem==null && modeUML))
			return 0;
		l.setText("Project  :  " + file.getProject().getName());
		l = new Label(gr2, SWT.FILL);
		l.setText("Folder     :  " + file.getParent().getName());
		l = new Label(gr2, SWT.FILL);
		l.setText("File     :  " + file.getName());
		l = new Label(gr2, SWT.FILL);
		if (modeUML)
			l.setText("Package  :  " + elem.getName());
		else
			l.setText("CCSL File");
			
		gr2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		gr2.setLayout(new GridLayout(1, true));
		gr2.layout();
		gr2.pack();
		return 0;
	}

	private Color mycolor() {
		Color color = display.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
		Color color2 = new Color(null, color.getRed() * 7 / 8,
				color.getGreen() * 7 / 8, color.getBlue() * 7 / 8);
		return color2;
	}

	private  int dispinfoconstraint(Composite basecmp) {
		folder = new CTabFolder(basecmp, SWT.NONE);
		basecmp.setLayout(new GridLayout(1, false));
		folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		folder.setUnselectedImageVisible(true);
		folder.setUnselectedCloseVisible(false);
		folder.setMaximized(true);

		folder.setBackground(mycolor());

		GridLayout layout = new GridLayout(1, true);
		{
			layout.marginLeft = 5;
			layout.marginRight = 5;
			layout.marginBottom = 5;
			layout.marginTop = 5;
		}
		GridLayout layout2 = new GridLayout(1, false);
		{
			layout2.marginLeft = 5;
			layout2.marginRight = 5;
			layout2.marginBottom = 5;
			layout2.marginTop = 5;
		}

		for (int i = 0; i < n; i++) {
			onglet(i, layout, layout2, basecmp);
			
		}
		basecmp.setLayout(layout2);
		folder.setSelection(0);

		basecmp.setBackground(ColorConstants.menuBackground);
		basecmp.layout();
		basecmp.pack();

		return 0;

	}

	private void onglet(int i, GridLayout layout, GridLayout layout2,
			Composite composite2) {
		Group parentsTab = new Group(folder, SWT.FILL);
		parentsTab.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		CTabItem item = new CTabItem(folder, SWT.FILL);
		item.setText("(" + i + ") ");
		Label label = new Label(parentsTab, SWT.FILL);
		String l = _constraint.nameconstraint.get(i);
		parentsTab.setLayout(layout);
		if (l.startsWith("__MARTE__CCSL") == true)
			label.setText("Constraint " + i + ": <" + l + ">");
		else
			label.setText("Constraint " + i + ": " + l);
		label.setLocation(10, 10);
		Text text = new Text(parentsTab, SWT.H_SCROLL | SWT.MULTI | SWT.HORIZONTAL | SWT.V_SCROLL | SWT.VERTICAL);
		text.setText(_constraint.l2.get(i));
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		text.setLocation(10, 40);

		//composite2.setLayout(layout2);
		item.setControl(parentsTab);
		if (_constraint.lb.get(i) < 4)
			item.setImage(_image.tbimage[_constraint.lb.get(i)]);
	} 

	public int view() {
		MyPluginManager.getShowView("fr.inria.ctrte.time.simview");

		return 0;
	}

	boolean haveerror=false;
	public void configure(List<String> inl1, List<String> inl2, List<Integer> inlb,
			NamedElement ele, ASTMask maskin, IFile f,HashMap<String, Boolean> hmb,boolean element)
	{
		haveerror=false;
	clear();
	setup(inl1,inl2,inlb,ele,maskin,f,element );
	setHmb(hmb);
	drawcmd();
	update();
	view();
	setFocus(); 
	
	}
	
	public void configure(String name,String message , int code, IFile f, ASTMask maskin,HashMap<String, Boolean> hmb)
	{
		haveerror=false;
		if (code!=0)
			haveerror=true;
		clear();
		setup(name,message,code,maskin,f);
		setHmb(hmb);
		drawcmd();
		update();
		view();
		setFocus(); 
	}
	
	private void setup(List<String> inl1, List<String> inl2, List<Integer> inlb,
			NamedElement ele, ASTMask maskin, IFile f ,boolean element ) {
		file = f;
		modeUML= element;
		 modeUMLFile= ! element;
		 modeCCSL= false;
		_constraint.nameconstraint = inl1;
		_constraint.l2 = inl2;
		_constraint.lb = inlb;
		n = _constraint.nameconstraint.size();
		elem = ele;
		mask = maskin;
		for (int i = 0; i < 4; i++)
			tb[i] = 0;
		for (int x : _constraint.lb)
			tb[x]++;
		mybar.resetleak();
		
	}
	
	


	private void setup(String name,String message  , int  inlb,	 ASTMask maskin, IFile f) {
		file = f;
		modeUML= false;
		 modeUMLFile= false;
		 modeCCSL= true;
		(_constraint.nameconstraint = new ArrayList<String>()).add(name);
		(_constraint.l2 =  new ArrayList<String>()).add(message);
		(_constraint.lb =  new ArrayList<Integer>()).add(inlb);
		n = 1;
		elem = null;
		mask = maskin;
		for (int i = 0; i < 4; i++)
			tb[i] = 0;
		for (int x : _constraint.lb)
			tb[x]++;
		mybar.resetleak();
		
	}
	
}

/*
 * 
 * switch (lb.get(i)) { case 3: item.setImage(_image.imageunparse); break; // -2
 * case 2: item.setImage(_image.imageparseerr); break;// -1 case 0:
 * item.setImage(_image.imageok); break; case 1:
 * item.setImage(_image.imagewarning); break; }
 */

