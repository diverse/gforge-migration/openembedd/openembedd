/*
 * 
 * @author : Benoit Ferrero
 * INRIA/I3S Aoste 
 * Copyright  2007-2008
 */
package fr.inria.ctrte.time.parser.ui.work;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.WindowManager;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Package;

import code.Cbase;
import code.Clist;
import code.Clistconstraint;
import code.Clistenumeration;
import code.Clistenumerationliteral;
import code.CmessageBox;
import code.CmyClock;
import code.CmyConstraint;
import code.CmyEnumeration;
import code.CmyEnumerationLiteral;
import code.CmyPackage;
import code.CmyProfile;
import code.Cumlcreator;
import code.Cumlelement;
import element.dictionnaire.Csubdictionnaire;
import element.dictionnaire.Cvariable2;

import fr.inria.base.MyManager;
import fr.inria.ctrte.time.parser.view.SimulationCCSLViewPart;

import grammaire.analyse.ASTMask;

public class MonParser {
	// static Shell shell = null;
	
	WindowManager w = null;
	private boolean modelparsing = false;
	private int count = 0;
	public List<String> l1 = new ArrayList<String>(); // name
	public List<String> l2 = new ArrayList<String>(); // message 
	public List<Integer> lb = new ArrayList<Integer>(); // codeerror
	public List<CmyConstraint> lcs = new ArrayList<CmyConstraint>();
	int errorparse = 0;
	int errorunparse = 0;
	int warningclock = 0;
	ASTMask mask = null;
	public ArrayList<Thread> th = new ArrayList<Thread>();
	IFile file = null;

	/*
	 * public boolean not(boolean b) { return !b; }
	 */

	public IFile getFile() {
		return file;
	}

	public void setFile(IFile file) {
		this.file = file;
	}

	public boolean flagerror = false;

	private void init() {
	
	}

	int MessageBox(String s, String s1) {
		MessageDialog.openInformation(new Shell(), CmessageBox.pluginTime + s,
				s1);
		return 0;
	}

	int ErreurBox(String s, String s1) {
		MessageDialog.openError(new Shell(), CmessageBox.pluginTime + s, s1);
		return 0;
	}

	boolean QuestionBox(String s, String s1) {
		return MessageDialog.openQuestion(new Shell(), CmessageBox.pluginTime
				+ s, s1);
	}

	public void CheckUnit(CmyClock c) {

		// Element e = c.getElement();
		Cumlelement.Enumtesttype t = c.ClockUnitValide();
		switch (t) {
		case eUnitPartialNok:
			ErreurBox(c.getName(), CmessageBox.error + c.getQuotedName()
					+ CmessageBox.errorUnit3);

			break;
		case eUnitNok:
			ErreurBox(c.getName(), CmessageBox.error + c.getQuotedName()
					+ CmessageBox.errorUnit1);
			break;
		case eUnitOk:
			break; // nothing
		default:
			
		}
	}

	String CheckClock(CmyClock c) {
		CheckUnit(c);
		return c.informationTimeClock();
	}

	String rapportValueLocal(Cvariable2 value, CmyConstraint o1) {
		String localy = value.getName();
		{
			Cumlelement uml = o1.getConstraintto().getElement(value.getName());
			if (uml != null) {
				flagerror = true;
				localy += CmessageBox.localmessage1;
				if (uml instanceof CmyClock) {
					localy += CmessageBox.localmessageBy
							+ CheckClock((CmyClock) uml);
				}
			} else
				localy += CmessageBox.localmessage2;
		}
		return localy + "\n";
	}

	String rapportValueExtern(Cvariable2 value, CmyConstraint o1) {
		String localy = value.getName();

		Cumlelement uml = o1.getConstraintto().getElement(value.getName());

		if (uml != null) {
			localy += CmessageBox.localmessage3;
			if (uml instanceof CmyClock) {
				localy += CmessageBox.localmessageBy
						+ CheckClock((CmyClock) uml);
			}

		} else {
			localy += CmessageBox.localmessage4;
			flagerror = true;
		}
		return localy;
	}

	String rapportValueMultiLocal(Cvariable2 value, CmyConstraint o1) {
		String localy = value.getName();
		localy += CmessageBox.localmessage5;// " est defini plusieurs fois
		// localement ";
		flagerror = true;
		return localy + "\n";
	}

	String rapportClk(Csubdictionnaire dicoclk, CmyConstraint o1) {
		String rapport = "";// CmessageBox.
		for (Cvariable2 value : dicoclk.get().values()) {
			switch (value.getLocal()) {
			case 1:
				rapport += rapportValueLocal(value, o1);
				break;
			case 0:
				rapport += rapportValueExtern(value, o1);
				break;
			default:
				rapport += rapportValueMultiLocal(value, o1);
			}
		}

		return rapport;
	}

	String rapportBoolean(Csubdictionnaire dicobool) {
		if (dicobool.size() == 0)
			return "";
		String rapport = "\nBoolean : ";
		for (Cvariable2 value : dicobool.get().values()) {
			rapport += value.getName() + " ";
		}
		return rapport + "\n";
	}

	public void parse(Package m) { // TODO , IFile f
		init();
		modelparsing = true;
		this.parseInternal(Cumlcreator.getInstance().createUmlElement(m),true);
	//	pw.close();
	}
	
	public void parse(Package m, boolean element) { // TODO , IFile f
		init();
		modelparsing = true;
		this.parseInternal(Cumlcreator.getInstance().createUmlElement(m),false);
	//	pw.close();
	}

	public void Enumreationcheck(Clistenumeration le) {
		for (CmyEnumeration en : le.getLneEnumreration()) {
			boolean b = false;
			Clistenumerationliteral lel = en.getEumerationliteral();
			for (CmyEnumerationLiteral enl : lel.getLneEnumrerationLiteral()) {
				if (!enl.isStereotypedBy(Cbase.marteUnitst)) {
					b = true;
				}
			}
			if (b) {
				MyManager.println(CmessageBox.error + en.getQualifiedName()
						+ CmessageBox.errorUnit2);
			}
		}
		return;
	}

	private HashMap<String, Boolean> listboolean() {
		HashMap<String, Boolean> bol = new HashMap<String, Boolean>();
		for (CmyConstraint c : lcs) {
			c.getboolean(bol);
		}
		return bol;

	}

	private void parseInternal(CmyPackage m,boolean element) {
	
		Clistconstraint cc4 = new Clistconstraint(m.allownedofE());
		CmyProfile timeprofil = m.getProfiles(true).getElement(
				Cbase.marteTimeprofil);
		if (timeprofil == null) {
			MessageBox("", CmessageBox.errorTime1);
			return;
		}
		

		Clistenumeration le = m.getAllUnitEnumeration();
		Enumreationcheck(le);

		count = 0;
		for (CmyConstraint o1 : cc4.getLneConstraint()) {
			if (o1.isTimeConstraint()) {

				parseConstraint(o1);
				count++;
			}
		}
	
		// int n = l1.size();
		Clist lc = new Clist();
		for (CmyConstraint cons : lcs) {
			lc = lc.merge(cons.getConstraintto());
		}
		lc.getligth().disp();

		

		// dispglobalAST();
		try {
			buildconstaint();			
			HashMap<String, Boolean> hmb = listboolean();		
			SimulationCCSLViewPart.getCourant().configure(l1, l2, lb,m.getElement(), mask, getFile(),hmb,element);
		

			
		} catch (Exception ex) {
			MyManager.printError(ex);

		} catch (Error ex) {
			MyManager.printError(ex);
			

		}

	}

	public void parseConstraint(Constraint co) {
		init();
		modelparsing = false;
		parseConstraint(Cumlcreator.getInstance().createUmlElement(co));
		//pw.close();
	}

	private int parseConstraint(CmyConstraint o1) {
		if (modelparsing == true)
			lcs.add(o1);
		if (!o1.isParsableCCSLConstainte()) {
			// if error , disp message
			String l = null;
			switch (o1.getCcslparsablerror()) {
			case -5:
				l = CmessageBox.errorConstraint5;
				break;
			case -4:
				l = CmessageBox.errorConstraint4;
				break;
			case -3:
				l = CmessageBox.errorConstraint3;
				break;
			case -2:
				l = CmessageBox.errorConstraint2;
				break;
			case -1:
				l = CmessageBox.errorConstraint1;
				break;
			default:
				// nothing : unreachable code
				l = CmessageBox.errorConstraintUK;
				break;
			}
			if (modelparsing == true) {
				// parsing of model
				errorunparse++;
				l1.add(o1.getName());
				l2.add("Error:\n\t" + l);
				lb.add(3);
				return 0;
			} 
				// parsing of one constraint
				return ErreurBox(o1.getName(), l);

			
		}
	//	pw.println("Language  :\t" + o1.GetLanguage());
		boolean b = o1.parseCCSL(0);
		if (b) {
			flagerror = false;
			// o1.walkerCCSL();
			o1.build();
			String rapport = "";
			rapport += rapportClk(o1.getParserccsl().getDico().getClkdic(), o1);
			rapport += rapportBoolean(o1.getParserccsl().getDico().getBooldic());

			if (modelparsing == false)
				o1.dispTreeWindows(0);
			if (modelparsing == true) {
				l1.add(o1.getName());
				l2.add(rapport);
				if (flagerror == true) {
					lb.add(1);
					warningclock++;
				} else

					lb.add(0);

			} else {
				MessageBox(o1.getName(), rapport);
			}
		} else {
			errorparse++;
			if (modelparsing == true) {
				l1.add(o1.getName());
				l2.add("CCSL Parsing Error:\n" + o1.errorstr);
				lb.add(2);

			} else {
				ErreurBox(o1.getName(), "CCSL Parsing Error \n" + o1.errorstr);
			}
		
		}

		return 0;
	}

	public void buildconstaint() {

		// boolean b = true;
		// int i = 0;

		HashMap<String, Boolean> hm = new HashMap<String, Boolean>();
		for (CmyConstraint c : lcs) {

			c.getlocalClock(hm);
		}
		for (CmyConstraint c : lcs) {

			c.renameclock(hm);
		}
		mask = new ASTMask(lcs);
		mask.dispAst("Simulation");
	}

}
