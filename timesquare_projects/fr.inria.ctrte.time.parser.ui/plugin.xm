<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.2"?>
<plugin>

 <extension  point="org.eclipse.ui.popupMenus">
      <objectContribution
            id="fr.inria.ctrte.time.parser.contribution1"
            objectClass="org.eclipse.uml2.uml.Constraint">
         <menu
               id="fr.inria.ctrte.menu"
               label="CT-RTE"
               >
            <separator name="separator"/>
         </menu>
         <action
               class="fr.inria.ctrte.time.parser.ui.action.LocalAction"
               enablesFor="1"
               id="org.odf.ctrte.time.parser.newAction"
               label="ClockConstraint Parser"
               menubarPath="fr.inria.ctrte.menu/separator"/>
         <action
               class="fr.inria.ctrte.time.parser.ui.action.MakeAction"
               enablesFor="1"
               id="fr.inria.ctrte.time.parser.action1"
               label="MakeCCSL"
               menubarPath="fr.inria.ctrte.menu/separator">
         </action>
      </objectContribution>
      <objectContribution
            id="fr.inria.ctrte.time.parser.contribution2"
            objectClass="org.eclipse.uml2.uml.Package">
         <action
               class="fr.inria.ctrte.time.parser.ui.action.GlobalAction"
               enablesFor="1"
               id="fr.inria.ctrte.time.parser.newAction"
               label="CCSL parser"
               menubarPath="fr.inria.ctrte.menu/separator"/>
         <menu
               id="fr.inria.ctrte.menu"
               label="CT-RTE">
            <separator name="separator"/>
         </menu>
      </objectContribution>
   </extension> 
</plugin>
