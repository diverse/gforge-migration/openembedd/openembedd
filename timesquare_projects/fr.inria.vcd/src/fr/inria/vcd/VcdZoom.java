/**
 * 
 */
package fr.inria.vcd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.base.MyManager;
import fr.inria.vcd.figure.Balise;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.figure.ExtendFigure;

/**
 * @author Benoit Ferrero
 * 
 */
public class VcdZoom implements ScaleZoom {

	/**
	 * 
	 */
	private double size = 1.0;
	private int delta = 0;
	private IVcdDiagram vdt;
	private ListConnections list;
	private IComboZoom iczoom;
	// private boolean sens = false;
	private int comp;

	private int lastpoint = 0;
	// private boolean state = true;
	// private int begin = 0;

	private IFigure fig;
	private Dimension dim;
	public ZoomList zoomlist;

	public VcdZoom(ListConnections listin, IVcdDiagram vdtin)
	{
		vdt = vdtin;
		list = listin;
		zoomlist = new ZoomList();
	}

	// ------------------------------------------------------------------

	public int getDelta()
	{
		return delta;
	}

	public ListConnections getList()
	{
		return list;
	}

	public void setList( ListConnections list )
	{
		this.list = list;
	}

	public void setDelta( int delta )
	{
		this.delta = delta;
	}

	public double getSize()
	{
		return size;
	}

	public void setSize( double sizein )
	{
		if (sizein <= 0.0)
			return;
		// sens = (sizein >= size);
		this.size = sizein;
	}

	public void scale( int n )
	{
		if (n == 0)
		{
			// sens = true;
			return;
		}

		if (n > 0)
		{
			size = size * (1 << n);
			// sens = true;
			return;
		}

		size = size / (1 << (-n));
		// sens = false;

	}

	public void scaleration( double f )
	{
		size = size * f;
	}

	private boolean lock = false;

	/**
	 * @return the lock
	 */
	public final boolean isLock()
	{
		return lock;
	}

	public void applyZoom()
	{
		lock = true;
		try
		{
			int sel = 	vdt.getfcb().getCanvas().getHorizontalBar().getSelection(); 

			fig = vdt.getCanvas().getContents();
			dim = vdt.getCanvas().getContents().getSize();
			/*
			 * if (getComp() <= 0) //size<=1.0) applyZoomless(); else
			 */
			double delta=1.0f;		
			delta = vdt.getZoom();
			int n=	zoomlist.selectValue(vdt.getTcZoom() * 100);
			if (n==-1)
			{
				lock = false;
				return ;
			}
			applyZoommore();
			
			if (vdt.getScale() != null)
			{
				vdt.getFactory().zoomscale(vdt.getTcZoom());
				vdt.getScale().redraw();
				vdt.getScale().layout();
				vdt.getScale().getParent().layout();

			}
			vdt.setMarkerZoom(vdt.getTcZoom());
			vdt.getMarkerFactory().hideMarkerFireable();
			vdt.getfcb().getCanvas().layout();
			vdt.getfcb().getCanvas().update();
			
			vdt.getfcb().getComposite().layout();
			vdt.getfcb().getComposite().update();
			vdt.getfcb().scrollUpdate();
			zoomlist.selectValue(vdt.getTcZoom() * 100);
			if (iczoom != null)
				iczoom.select(vdt.getTcZoom() * 100);
		
			vdt.getCanvas().scrollToX( (int )(sel*delta));
			vdt.getScale().scrollToX( (int ) (sel*delta));
		
			
		} catch (Throwable e)
		{
			MyManager.printError(e, " VCDZOOM ");
		}
		lock = false;

	}

	public double getTime( int time )
	{
		return 0.0;
	}

	public int computeposition( int t )
	{
		int r = (int) ((t) * vdt.getTcZoom());
		// MyManager.println(t +" --> " +r + "  " + vdt.getTcZoom());
		return r;
	}

	private void applyZoommore()
	{
		// state = true;
		// begin = 0;
		lastpoint = 0;
		ArrayList<Balise> listBalise = new ArrayList<Balise>();

		for (Object obj : vdt.getCanvas().getContents().getChildren())
		{
			Panel panel = null;
			if (obj instanceof Panel)
			{
				panel = (Panel) obj;
			} else
				continue;
			panel.setValid(false);
			for (Object f : panel.getChildren())
			{
				if (f instanceof IFigure)
				{
					if (f instanceof ExtendFigure)
					{
						((ExtendFigure) f).mycompute();
					}
					if (f instanceof Balise)
					{
						listBalise.add((Balise) f);
					} 
					else
					{
						int tmp = ((IFigure) f).getBounds().right();
						if (tmp > lastpoint)
							lastpoint = tmp;
					}
				}
			}
			panel.setValid(true);
		}
		fig.setPreferredSize(new Dimension(lastpoint, fig.getPreferredSize().height));
		for (Object obj : vdt.getCanvas().getContents().getChildren())
		{
			Panel panel = null;
			if (obj instanceof Panel)
			{
				panel = (Panel) obj;
				panel.setPreferredSize(fig.getPreferredSize().width,
						panel.getPreferredSize().height);
			}
		}
		for (Balise balise : listBalise)
		{
			balise.update();
		}
		if (vdt.getIntervalItem() != null)
			for (MenuItem menuitem : vdt.getIntervalItem())
			{
				if (menuitem.getSelection())
				{
					ConstraintCommentCommand cc = list.getMenuForComment().get(menuitem);
					Color color = list.menuForColorGet(menuitem);
					vdt.getConstraintsFactory().redrawSyncInterval(cc, color);
				}
			}
		// if(!vdt.isGhostMode())
		// vdt.getConstraintsFactory().hideGhost();
		vdt.getCanvas().redraw();
		vdt.getCanvas().getParent().layout();
		vdt.getCanvas().getContents().setPreferredSize(
				new Dimension(vdt.getCanvas().getContents().getPreferredSize().width,
						dim.height - 5));
	}

	public void setComp( int comp )
	{
		this.comp = comp;
	}

	public int getComp()
	{
		return comp;
	}

	/**
	 * @return the iczoom
	 */
	public final IComboZoom getIczoom()
	{
		return iczoom;
	}

	/**
	 * @param iczoom
	 *            the iczoom to set
	 */
	public final void setIczoom( IComboZoom iczoom )
	{
		this.iczoom = iczoom;
	}

	public class ZoomList {

		private ZoomList()
		{
			super();
			ardb.add(6.25);
			ardb.add(12.5);
			ardb.add(25.0);
			ardb.add(50.0);
			ardb.add(100.0);
			ardb.add(200.0);
			ardb.add(400.0);
			indice = ardb.indexOf(100.0);
		}

		private int indice = 0;

		/**
		 * @return the indice
		 */
		public final int getIndice()
		{
			return indice;
		}

		private List<Double> ardb = new ArrayList<Double>();

		public int selectValue( Double d )
		{
			if ((d < 0.001) || (d > 5000))
			{
				return -1;
			}
			int n = 0;
		//	MyManager.println(d);
			n = ardb.indexOf(d);
			if (n != -1)
			{
				indice = n;
				return n;
			}
			n = 0;
			for (double dl : ardb)
			{
				if (dl > d)
					break;
				n++;
			}
			ardb.add(n, d);
			n = ardb.indexOf(d);
			indice = n;
			return n;
		}

		public String[] arrayOf()
		{
			ArrayList<String> lst = new ArrayList<String>();
			for (Double d : ardb)
			{
				lst.add(d.toString());
			}
			return lst.toArray(new String[] {});
		}
	}

	/**
	 * @return the zoomlist
	 */
	public final ZoomList getZoomlist()
	{
		return zoomlist;
	}
}
