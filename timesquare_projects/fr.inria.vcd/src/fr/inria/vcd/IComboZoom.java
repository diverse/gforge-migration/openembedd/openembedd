package fr.inria.vcd;

public interface IComboZoom {

	int updateitem();

	int select( Double d );
	
	int updateFocus();

}