package fr.inria.vcd.menu;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import fr.inria.vcd.dialogs.FindAction;
import fr.inria.vcd.listener.IVcdDiagram;

public class VcdMenu {
	IVcdDiagram ivcd;
	Action findAction,refreshAction,showGhostAction,hideGhostAction,partialGhostAction;
	boolean ghostMode;
	
	public VcdMenu(IVcdDiagram ivcd) {
		super();
		this.ivcd = ivcd;
		showGhostAction = new GhostAction(ivcd, "&Show all ghost", IAction.AS_RADIO_BUTTON,0);
		showGhostAction.setToolTipText("&Use to show ghost instants");	
		hideGhostAction = new GhostAction(ivcd, "&Hide all ghost", IAction.AS_RADIO_BUTTON ,1);
		hideGhostAction.setToolTipText("&Use to hide ghost instants");
		partialGhostAction = new GhostAction(ivcd, "&Partial ...", IAction.AS_RADIO_BUTTON,2);
		partialGhostAction.setToolTipText("&Use to show or hide ghost instants");
		findAction = new FindAction(ivcd);		
		findAction.setText("&Find");
		setGhostSelected(Mode.hide);
	}
	
	public Action getFindAction() {
		return findAction;
	}
	
	public Action getRefreshAction() {
		if (refreshAction == null) {
			refreshAction = new Action(){
				@Override 
				public void run() {
					ivcd.refresh();
					super.run();
				}
			};
		}		
		refreshAction.setText("&Refresh");
		return refreshAction;
	}
	
	public Action getShowGhostAction()
	{		
		showGhostAction.setEnabled(ivcd.haveGhost());
		return showGhostAction;
	}
	
	public Action getHideGhostAction()
	{	
		hideGhostAction.setEnabled(ivcd.haveGhost());
		return hideGhostAction;
	}
	
	public Action getPartialGhostAction()
	{
		partialGhostAction.setEnabled(ivcd.haveGhost());
		return partialGhostAction;
	}
	
	public void setGhostSelected(Mode n)
	{
		if (n==null)
			return ;
		showGhostAction.setChecked(n.isShow());
		hideGhostAction.setChecked(n.isHide());
		partialGhostAction.setChecked(n.isPartial());
	}
	
	public Mode  getGhostSelected()
	{
		if(showGhostAction.isChecked())    return Mode.show;
		if(hideGhostAction.isChecked())    return Mode.hide;
		if(partialGhostAction.isChecked()) return Mode.partial;
		return null;
	}
	
	public void getGhostAction() 
	{
		if (getGhostSelected()==null)
			setGhostSelected(Mode.hide);
	}
}