/**
 * 
 */
package fr.inria.vcd.menu;

import org.eclipse.jface.action.Action;

import fr.inria.base.MyManager;
import fr.inria.vcd.dialogs.PartialDialog;
import fr.inria.vcd.listener.IVcdDiagram;

//action of "Show Ghost","Hide Ghost" and "Partial..."
public class GhostAction extends Action {
	private final IVcdDiagram vcdMultiPageEditor;
	final int n;
	final String labelactive ;
	boolean checked;
	public GhostAction(IVcdDiagram vcdMultiPageEditor, String text, int style, int  n) {
		super(text, style);
		labelactive =text;
		this.vcdMultiPageEditor = vcdMultiPageEditor;
		this.n=n;
		checked=false;
		super.setChecked(checked); 
	}
	
	@Override
	public void run() {
		try
		{
			VcdMenu vcdMenu = vcdMultiPageEditor.getVcdmenu();		
			switch (n) 
			{
			case 0:
				//case of show ghost
				vcdMultiPageEditor.setGhostMode(true);
				break;
			case 1:
				//case of hide ghost
				vcdMultiPageEditor.setGhostMode(false);
				break;
			case 2:		
				//case of partial
				Mode oldsel=vcdMenu.getGhostSelected();		
				if(vcdMenu.getPartialGhostAction().isChecked())
				{
					//create a dialog box to choice which clock with ghost to show or hide
					PartialDialog partialDialog=new PartialDialog(
							vcdMultiPageEditor.getMyShell(), "Clock with ghost hide","Clock with ghost show", vcdMultiPageEditor ,".");
					//show dialog box
					int ret=partialDialog.open();
					//if "OK"
					if(ret== org.eclipse.jface.window.Window.OK)
					{
						//call method apply of partialDialog
						partialDialog.apply();
					}
					//if "CANCEL"
					if(ret== org.eclipse.jface.window.Window.CANCEL)
					{
						vcdMenu.setGhostSelected(oldsel);
					}
				}
				break;
			default:
				break;
			}
		}
		catch (Throwable e) {
			MyManager.printError(e, "Ghost run");
		}
		vcdMultiPageEditor.setFocus();
	}

	@Override
	public void setChecked(boolean checked) {
		this.checked=checked;
		if (isEnabled())
			super.setChecked(checked);
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (enabled)	
		{			
			setText(labelactive);
			super.setChecked(checked);
		}
		else
		{			
			setText("No ghost on VcdDiagram");
			super.setChecked(false);
		}
		super.setEnabled(enabled);
	}
}