package fr.inria.vcd.menu;

public enum Mode {
	show(0) , hide(1),partial(2);
	int n;

	private Mode(int n) {
		this.n = n;
	}
	
	public boolean isHide()
	{
		return this==hide;
	}
	
	public boolean isShow()
	{
		return this==show;
	}
	
	public boolean isPartial()
	{
		return this==partial;
	}
}
