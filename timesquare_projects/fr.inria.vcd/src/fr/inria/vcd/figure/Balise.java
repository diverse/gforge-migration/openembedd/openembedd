package fr.inria.vcd.figure;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;

public class Balise extends RectangleFigure {
	
	public enum Linkto
	{
		tl("tl",0) , tr("tr",1) , bl("bl",2), br("br",3);
		String s;
		int n;
		private Linkto( String s, int n)
		{
			this.n = n;
			this.s = s;
		}
		
	}
	
	private IFigure anchor = null;
	private String param = null;
	
	public IFigure getAnchor() {
		return anchor;
	}
	public void setAnchor(IFigure anchor) {
		this.anchor = anchor;
	}
	public String getParam() {
		return param;
	}
	
	/*public void setParam(String param) {
		this.param = param;
	}*/
	
	private Linkto linkTo ;

	/**
	 * @return the linkTo
	 */
	public final Linkto getLinkTo()
	{
		return linkTo;
	}
	/**
	 * @param linkTo the linkTo to set
	 */
	public final void setLinkTo( Linkto linkTo )
	{
		this.linkTo = linkTo;
	}

	
	public int update()
	{
		
		IFigure anchor = getAnchor();
		if (linkTo==null)
			return 0;
		switch (linkTo) {
			case tl:	
		{
			getBounds().x = anchor.getBounds().getTopLeft().x - 2;
			getBounds().y = anchor.getBounds().getTopLeft().y - 4;
		}
			break;
		case tr:
		{
			getBounds().x = anchor.getBounds().getTopRight().x + 2;
			getBounds().y = anchor.getBounds().getTopRight().y - 4;
		}
		break ;
		case bl :
		
		{
			getBounds().x = anchor.getBounds().getBottomLeft().x - 2;
			getBounds().y = anchor.getBounds().getBottomLeft().y + 5;
		}
		break;
		case br:
	
		{
			getBounds().x = anchor.getBounds().getBottomRight().x + 2;
			getBounds().y = anchor.getBounds().getBottomRight().y + 5;
		}
		break;
		}
		return 0;
	}
	
}
