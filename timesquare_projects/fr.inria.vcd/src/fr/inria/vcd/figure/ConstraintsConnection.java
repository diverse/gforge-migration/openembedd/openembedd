package fr.inria.vcd.figure;

import org.eclipse.draw2d.PolylineConnection;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public class ConstraintsConnection extends PolylineConnection {
	private ConstraintCommentCommand comment ;
	private boolean global=false;
	
	public ConstraintCommentCommand getComment() {
		return comment;
	}

	public void setComment(ConstraintCommentCommand comment) {
		this.comment = comment;
	}

	public boolean isGlobal() {
		return global;
	}

	public void setGlobal(boolean global) {
		this.global = global;
	}
	
	public int redraw()
	{
		return 0;
	}
}
