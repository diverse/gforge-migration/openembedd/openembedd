package fr.inria.vcd.listener;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;

import fr.inria.vcd.VcdZoom;

public class WheelListener implements MouseWheelListener {
	//private ListConnections list;
	private IVcdDiagram vdt;
	//private int begin = 0;
	//private int compteur = 0;
//	boolean up = false;
	//boolean down = false;

	//public WheelListener(ListConnections list, IVcdDiagram vdt)
	public WheelListener( IVcdDiagram vdt){
		//this.list = list;
		this.vdt = vdt;

	}


	public void mouseScrolled(MouseEvent e) {
		if (vdt.isSimulation())
			return;
		if (!vdt.isCtrlKey())
			return;
		
		VcdZoom vcdzoom= vdt.getVcdzoom();
		if (vcdzoom.isLock())
			return ;
		int compteur =vcdzoom.getComp();
		vdt.setZoom(1);
		if (e.count >= 0)	
		{
			if (compteur == 10)
				return;
			if (e.count!=0)
			{
			vdt.setZoom(2);
			vdt.setTcZoom(vdt.getTcZoom() * 2);

			compteur++;
			}
		} else {
			if (compteur == -5)
				return;
			vdt.setZoom(0.5);
			vdt.setTcZoom(vdt.getTcZoom() * 0.5);	
			compteur--;
		}
	
		vcdzoom.setComp(compteur);		
		vcdzoom.applyZoom();
	}

}
