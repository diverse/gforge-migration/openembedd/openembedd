package fr.inria.vcd.listener;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.vcd.dialogs.SelectClocksDialog;
import fr.inria.vcd.menu.Mode;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.figure.ExtendFigure;

public class MouseDraggedListener implements MouseListener, MouseMoveListener {
	
	//listener of "Hide" in the contextMenu 
	public class SelectedHide implements SelectionListener {
		private IVar variable;
		private IVcdDiagram vdt;
		public SelectedHide(IVar var,IVcdDiagram vdt)
		{
			variable=var;
			this.vdt=vdt;
		}
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}
		public void widgetSelected(SelectionEvent e) {
			//obtain hide clock and show clock
			ArrayList<IVar> hideClock= vdt.getTc().getAllnames();
			ArrayList<IVar> showClock= vdt.getTc().getSelectedClocks();
			hideClock.add(variable);
			showClock.remove(variable);
			//call apply on SelectClocksDialog
			SelectClocksDialog scd=new SelectClocksDialog(vdt.getMyShell(),hideClock,showClock,"","" ,"");
			scd.apply(vdt);
		}
	}
	
	//listener of "Show Ghost" in the contextMenu 
	public class SelectedShowGhost implements SelectionListener {
		private int index;
		private IVcdDiagram vdt;
		public SelectedShowGhost(int index,IVcdDiagram vdt)
		{
			this.index=index;
			this.vdt=vdt;
		}
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		public void widgetSelected(SelectionEvent e) {
			vdt.getConstraintsFactory().showGhost(index);
			if(isAllShow())
				vdt.getVcdmenu().setGhostSelected(Mode.show);
			else
				vdt.getVcdmenu().setGhostSelected(Mode.partial);
		}
		private boolean isAllShow()
		{
			for(IVar var:vdt.getTc().getSelectedClocks())
			{
				if(var.getValueFactory().haveGhostinclock())
				{
					if(!var.getValueFactory().getIsGhostVisible())
						return false;
				}
			}
			return true;
		}
	}
	
	//listener of "Hide Ghost" in the contextMenu 
	public class SelectedHideGhost implements SelectionListener {
		private int index;
		private IVcdDiagram vdt;
		public SelectedHideGhost(int index,IVcdDiagram vdt)
		{
			this.index=index;
			this.vdt=vdt;
		}
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		public void widgetSelected(SelectionEvent e) {
			vdt.getConstraintsFactory().hideGhost(index);
			if(isAllHide())
				vdt.getVcdmenu().setGhostSelected(Mode.hide);
			else
				vdt.getVcdmenu().setGhostSelected(Mode.partial);
		}
		private boolean isAllHide()
		{
			for(IVar var:vdt.getTc().getSelectedClocks())
			{
				if(var.getValueFactory().haveGhostinclock())
				{
					if(var.getValueFactory().getIsGhostVisible())
						return false;
				}
			}
			return true;
		}
	}

	private ListConnections list;
	private IVcdDiagram vdt;
	private IFigure fig;
	private boolean state  = false;
	private int indexlabel = -1;
	private int indexWave  = -1;

	public MouseDraggedListener(ListConnections list, IVcdDiagram vdt) {
		this.list = list;
		this.vdt = vdt;
	}


	public void mouseDoubleClick(MouseEvent e) {

	}

	
	public void mouseDown(MouseEvent e) {
		//offset of vertical scrollBar
		int dy = vdt.getNames().getVerticalBar().getSelection();
		//select name clock
		fig = vdt.getNames().getContents().findFigureAt(e.x, e.y+dy);
		
		//if it's a left click
		if(e.button==1)
		{
			if (fig instanceof Label) {
				IFigure rect = fig.getParent();
				state = true;
				indexlabel = vdt.getNames().getContents().getChildren().indexOf(rect);
			}
		}
		//if it's a right click
		else if (e.button==3)
		{
			
			if (fig instanceof Label) 
			{
				//create contextMenu
				Menu contextMenu=new Menu(vdt.getMyShell(),SWT.POP_UP); 
				
				//add "Ghost" to contextMenu
				MenuItem ghostItem = new MenuItem(contextMenu, SWT.CASCADE);
				ghostItem.setText("Ghost");
				//add "Hide" to contextMenu
				MenuItem hideItem = new MenuItem(contextMenu, SWT.CASCADE);
				hideItem.setText("Hide Clock");
				
				//set "Ghost" like subMenu
				Menu subMenuGhost = new Menu(contextMenu);
				ghostItem.setMenu(subMenuGhost);
				//add "Show Ghost" to "Ghost"
				MenuItem showGhostItem = new MenuItem(subMenuGhost,SWT.CASCADE);
				showGhostItem.setText("Show Ghost");
				//add "Hide Ghost" to "Ghost"
				MenuItem hideGhostItem = new MenuItem(subMenuGhost,SWT.CASCADE);
				hideGhostItem.setText("Hide Ghost");
				
				//find and save IVar and index of the name
				IFigure rect = fig.getParent();	
				indexlabel = vdt.getNames().getContents().getChildren().indexOf(rect);
				IVar var=vdt.getTc().getSelectedClocks().get(indexlabel);
				
				//add listener to "Hide"
				hideItem.addSelectionListener(new SelectedHide(var,vdt));
				
				//if selected clock have ghost
				if(var.getValueFactory().haveGhostinclock()==true)
				{
					//"Ghost" is enable
					ghostItem.setEnabled(true);
					//select clock wave
					IFigure fig2= vdt.getCanvas().getContents().findFigureAt(e.x+130, e.y+dy);
					indexWave=vdt.getCanvas().getContents().getChildren().indexOf(fig2);
					if (indexWave==-1)
					{
						indexWave=vdt.getCanvas().getContents().getChildren().indexOf(fig2.getParent());
					}
					//add listener to "Show Ghost"
					showGhostItem.addSelectionListener(new SelectedShowGhost(indexWave,vdt));
					//add listener to "Hide Ghost"
					hideGhostItem.addSelectionListener(new SelectedHideGhost(indexWave,vdt));
				}
				else
					//"Ghost" is not enable
					ghostItem.setEnabled(false);
				//add contextMenu to the shell
				vdt.getMyShell().setMenu(contextMenu);
				//set contextMenu is visible
				contextMenu.setVisible(true);
			}
		}
	}
	
	public void mouseMove(MouseEvent e) {		
		int dx = vdt.getNames().getVerticalBar().getSelection();
		if (state) {
			fig.getParent().setLocation(
					new Point(fig.getParent().getBounds().x, e.y+dx ));
		}
	}

	
	public void mouseUp(MouseEvent es) {
		if(es.button==1)
		{
			int index = -1;
			int verticalmargin = 5;
			state=false;
			index = (fig.getParent().getBounds().y - verticalmargin)
					/ (fig.getParent().getBounds().height + verticalmargin);
			if(index > vdt.getNames().getContents().getChildren().size()-1)
				index = vdt.getNames().getContents().getChildren().size()-1;
			if(index < 0)
				index = 0;
			RectangleFigure rf1 = (RectangleFigure) vdt.getNames().getContents()
					.getChildren().get(indexlabel);
			IVar var = vdt.getTc().getSelectedClocks().get(indexlabel);
			vdt.getNames().getContents().remove(rf1);
			vdt.getTc().getSelectedClocks().remove(var);
			vdt.getNames().getContents().add(rf1, index);
			vdt.getTc().getSelectedClocks().add(index,var);
			vdt.getNames().getContents().getLayoutManager().layout(vdt.getNames().getContents());
			
			alternateWaves(indexlabel, index);
			
			vdt.getNames().getContents().getLayoutManager().layout(vdt.getNames().getContents());
			fig=null;  
			indexlabel=-1;
			if (vdt.getIntervalItem()==null)
			{
				return ;
			}
			for(MenuItem menuitem : vdt.getIntervalItem()){
				if(menuitem.getSelection()){
					ConstraintCommentCommand cc = list.getMenuForComment().get(menuitem);
					Color color = list.menuForColorGet(menuitem);
					vdt.getConstraintsFactory().redrawSyncInterval(cc,color);
				}
			}
			vdt.getCanvas().redraw();
		}
	}

	public void alternateWaves(int indexlabel, int index) 
	{		
		Panel p1 = (Panel) vdt.getCanvas().getContents().getChildren().get(		indexlabel);
		vdt.getCanvas().getContents().remove(p1);
		vdt.getCanvas().getContents().add(p1, index);
		vdt.getCanvas().getContents().getLayoutManager().layout(vdt.getCanvas().getContents());
		int n=vdt.getNames().getContents().getChildren().size();
		for(int i = 0 ; i < n ; i++){
			Panel p2 = (Panel) vdt.getCanvas().getContents().getChildren().get(i);
			for (Object o : p2.getChildren()) {
				IFigure f = (IFigure) o;			
				if (f instanceof ExtendFigure)
				{
					((ExtendFigure) f).setTr(new Point(0,i*40+25));
					((ExtendFigure) f).mycompute();
				}
			}
		}
	
		
	}
}
