package fr.inria.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import fr.inria.vcd.dialogs.FindDialog;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.MyColorApi;

public class VcdFindButtonListener implements SelectionListener {
	private ListConnections list;
	private IVcdDiagram vdt;
	private IFigure poly;
	int reponse;
	int x;

	public VcdFindButtonListener(ListConnections list, IVcdDiagram vdt) {
		this.list = list;
		this.vdt = vdt;
	}

	public void widgetDefaultSelected(SelectionEvent e) {
	}

	public void widgetSelected(SelectionEvent e) {
		FindDialog inputDialog = new FindDialog(Display.getCurrent()
				.getActiveShell(), "Find", "Clock name", "Instance", "", vdt);
		reponse = inputDialog.open();
		if (reponse == Window.OK) {
			Description d = new Description(inputDialog.getClock(), inputDialog
					.getInstance(),-1);
			for (Object obj : vdt.getCanvas().getContents().getChildren()) {
				Panel panel = null;
				if (obj instanceof Panel)
					panel = (Panel) obj;
				else
					continue;
				for (Object f : panel.getChildren()) {
					poly = (IFigure) f;
					Description descr = vdt.getFactory()
							.getFigureForDescription().get(poly);
					if (descr == null )
						continue;
					if (d.equals(descr)) {
						x = poly.getBounds().x;
						poly.setForegroundColor(MyColorApi.colorRedFind());
						vdt.getCanvas().scrollTo(x - (vdt.getCanvas().getSize().x / 2), 0);
						list.getListFind().add(poly);
						return;
					}
				}
			}
			MessageBox messageBox = new MessageBox(Display.getCurrent()
					.getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setText("Warning");
			messageBox.setMessage("Figure introuvable !");
			messageBox.open();
		}
	}
}
