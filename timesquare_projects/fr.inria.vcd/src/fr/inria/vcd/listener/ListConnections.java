package fr.inria.vcd.listener;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public class ListConnections {
	private ArrayList<Description> listDescriptions = new ArrayList<Description>();
	private ArrayList<IFigure> listConstraints = new ArrayList<IFigure>();
	private HashMap<ConstraintCommentCommand,ArrayList<ConstraintsConnection>> listInterval = new HashMap<ConstraintCommentCommand,ArrayList<ConstraintsConnection>>();
	private ArrayList<IFigure> listFind = new ArrayList<IFigure>();
	private HashMap<ConstraintCommentCommand,ArrayList<IFigure>> firstclock1 = new HashMap<ConstraintCommentCommand,ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand,ArrayList<IFigure>> firstclock2 = new HashMap<ConstraintCommentCommand,ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand,ArrayList<IFigure>> lastclock1 = new HashMap<ConstraintCommentCommand,ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand,ArrayList<IFigure>> lastclock2 = new HashMap<ConstraintCommentCommand,ArrayList<IFigure>>();
	private HashMap<MenuItem,ConstraintCommentCommand> menuforcomment = new HashMap<MenuItem,ConstraintCommentCommand>();
	private HashMap<MenuItem,Color> menuforcolor = new HashMap<MenuItem,Color>();
	private ArrayList<IFigure> listcoincidence = new ArrayList<IFigure>();
	private ArrayList<IFigure> listGhost = new ArrayList<IFigure>(); 
	private HashMap<Action,ConstraintCommentCommand> actionforcomment = new HashMap<Action,ConstraintCommentCommand>();
	private HashMap<Action,Color> actionforcolor = new HashMap<Action,Color>();
	
	
	public ListConnections(ArrayList<Description> listDesciptions,
			ArrayList<IFigure> listSync, HashMap<ConstraintCommentCommand,ArrayList<ConstraintsConnection>> listInterval) {
		super();
		this.listDescriptions = listDesciptions;
		this.listConstraints = listSync;
		this.listInterval = listInterval;
	}

	
	public void clearAll(){		
		listDescriptions.clear();
		listFind.clear();
		listConstraints.clear();
		firstclock1.clear();
		firstclock2.clear();
		lastclock1.clear();
		lastclock2.clear();
	}
	
	public void clear(){
		listConstraints.clear();
	}
	
	public void clearInterval(ConstraintCommentCommand cc){
		if (listInterval.get(cc)!=null)
		listInterval.get(cc).clear();
		if (lastclock1.get(cc)!=null)
			lastclock1.get(cc).clear();
		if (firstclock1.get(cc)!=null)
			firstclock1.get(cc).clear();
		if (lastclock2.get(cc)!=null)
			lastclock2.get(cc).clear();
		if (firstclock2.get(cc)!=null)
			firstclock2.get(cc).clear();
	}
	
	public Color actionColorGet(Action ac)
	{
		return actionforcolor.get(ac);
	}
	
	public void actionColorPut(Action ac, Color c)
	{
		 actionforcolor.put(ac,c);
	}
	
	public ConstraintCommentCommand actionForCommentGet(Action ac)
	{
		return actionforcomment.get(ac);
	}
	
	public void actionForCommentPut(Action ac, ConstraintCommentCommand cc)
	{
		actionforcomment.put(ac,cc);
	}
	
	public ArrayList<IFigure> firstClock1Get(ConstraintCommentCommand cc)
	{
		return firstclock1.get(cc);
	}
	
	public void firstClock1Put(ConstraintCommentCommand cc, ArrayList<IFigure> list)
	{
		firstclock1.put(cc,list);
	}
	
	public ArrayList<IFigure> firstClock2Get(ConstraintCommentCommand cc)
	{
		return firstclock2.get(cc);
	}
	
	public void firstClock2Put(ConstraintCommentCommand cc, ArrayList<IFigure> list)
	{
		firstclock2.put(cc,list);
	}
	
	public ArrayList<IFigure> lastClock1Get(ConstraintCommentCommand cc)
	{
		return lastclock1.get(cc);
	}
	
	public void lastClock1Put(ConstraintCommentCommand cc, ArrayList<IFigure> list)
	{
		lastclock1.put(cc,list);
	}
	
	public ArrayList<IFigure> lastClock2Get(ConstraintCommentCommand cc)
	{
		return lastclock2.get(cc);
	}
	
	public void lastClock2Put(ConstraintCommentCommand cc, ArrayList<IFigure> list)
	{
		lastclock2.put(cc,list);
	}
	
	public ArrayList<IFigure> getListCoincidence()
	{
		return listcoincidence;
	}
	
	public void listGhostAdd(IFigure e)
	{
		listGhost.add(e);
	}
	
	public ArrayList<IFigure> getListFind()
	{
		return listFind;
	}
	
	public ArrayList<Description> getListDescription()
	{
		return listDescriptions;
	}
	
	public void menuForColorPut(MenuItem mi,Color c)
	{
		menuforcolor.put(mi,c);
	}
	
	public Color menuForColorGet(MenuItem mi)
	{
		return menuforcolor.get(mi);
	}
	
	
	// TODO get / put  /keyset ??
	public HashMap<MenuItem,ConstraintCommentCommand> getMenuForComment()
	{
		return menuforcomment;
	}
	
	public HashMap<ConstraintCommentCommand,ArrayList<ConstraintsConnection>> getListInterval()
	{
		return listInterval;
	}
	
	public ArrayList<IFigure> getListConstraints()
	{
		return listConstraints;
	}
}
