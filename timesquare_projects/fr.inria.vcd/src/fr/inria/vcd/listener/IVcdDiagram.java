package fr.inria.vcd.listener;

import java.util.ArrayList;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.vcd.VcdZoom;
import fr.inria.vcd.menu.VcdMenu;
import fr.inria.vcd.model.FigureCanvasBase;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.TraceCollector;
import fr.inria.vcd.view.ConstraintsFactory;
import fr.inria.vcd.view.MarkerFactory;
import fr.inria.vcd.view.VcdFactory;

public interface IVcdDiagram {

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#addMouseClickListener()
	 */
	public abstract void addMouseClickListener();

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#addMouseDraggedListener()
	 */
	public abstract void addMouseDraggedListener();

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#addMouseMouveListener()
	 */
	public abstract void addMouseMouveListener();

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#createClockMenu(org.eclipse.swt.widgets.Menu)
	 */
	public abstract void createClockMenu(Menu menuBar);

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#createFileMenu(org.eclipse.swt.widgets.Menu)
	 */
	public abstract void createFileMenu(Menu menuBar);

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#createMenuBar()
	 */
	public abstract void createMenuBar();

	/**
	 * @see fr.inria.vcd.antlr.view.IVcdDiagramTest#createOptionsMenu(org.eclipse.swt.widgets.Menu)
	 */
	public abstract void createOptionsMenu(Menu menuBar);

	public abstract FigureCanvas getCanvas();

	public abstract ConstraintsFactory getConstraintsFactory();
	
	public abstract MarkerFactory getMarkerFactory();

	public abstract VcdFactory getFactory();

	public abstract ArrayList<MenuItem> getIntervalItem();
	
	public abstract ArrayList<Action> getIntervalAction();

	public abstract ListConnections getList();

	public abstract double getMarkerZoom();  

	public abstract Shell getMyShell();

	public abstract FigureCanvas getNames();
	
	public abstract FigureCanvas getScale();
	
	public abstract TraceCollector getTc();
	
	public abstract double getTcZoom();
	
	public abstract ToolBar getToolbar();
	
	public abstract VCDDefinitions getVcd();

	public abstract double getZoom();
	
	public abstract boolean isCtrlKey(); 
	
	public abstract boolean isGhostMode();

	public abstract boolean isSimulation();
	
	public abstract void setCtrlKey(boolean ctrlKey);

	public abstract void setGhostMode(boolean ghostMode);
	
	public abstract void setMarkerZoom(double markerZoom);

	public abstract void setSimulation(boolean simulation);
	
	public abstract void setSimulationAvance(int etape);
	
	public abstract void setTcZoom(double tcZoom);

	public abstract void setZoom(double zoom);
	
	public abstract VcdZoom getVcdzoom();
	public abstract void setFocus();
	
	public abstract FigureCanvasBase getfcb();
	

	public abstract void setTc(TraceCollector tc);
	
	public abstract void setVcd(VCDDefinitions vcd);
	
	public abstract void setFactory(VcdFactory factory);
	
	public abstract void refresh();
	
	public abstract boolean haveGhost();
	
	
/*	public abstract Action getterShowGhostAction();
	
	
	public abstract Action getterHideGhostAction();
	
	
	public abstract Action getterPartialGhostAction();*/
	
	public abstract VcdMenu getVcdmenu();

}