package fr.inria.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;

import fr.inria.base.MyManager;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.MyColorApi;

public class MouseClickListener implements MouseListener {
	private ListConnections list;
	private IVcdDiagram vdt;
	private int xscroll;
	private int yscroll;

	public MouseClickListener(ListConnections list, IVcdDiagram vdt) {
		this.list = list;
		this.vdt = vdt;
	}

	private void buttonDown1(MouseEvent e) {
		try {
			IFigure fig = vdt.getCanvas().getContents().findFigureAt(e.x + xscroll, e.y + yscroll);
			Description descr;
			if (fig == null)
				return;
			if (fig instanceof Label)
				descr = vdt.getFactory().getFigureForDescription().get(	fig.getParent());
			else
				descr = vdt.getFactory().getFigureForDescription().get(fig);

			if (descr == null) 
			{
				return;
			}
			if (!list.getListFind().isEmpty()) {
				if (list.getListFind().contains(fig)) {
					list.getListFind().remove(fig);
					fig.setForegroundColor(MyColorApi.colorLightGreenFirable());
				}
			}
			if (list.getListDescription().isEmpty()) {
				if (fig instanceof Label)
					list.getListDescription().add(vdt.getFactory()
							.getFigureForDescription().get(fig.getParent()));
				else
					list.getListDescription().add(vdt.getFactory()
							.getFigureForDescription().get(fig));
			} else {
				if (!list.getListDescription().contains(descr))
					list.getListDescription().add(vdt.getFactory()
							.getFigureForDescription().get(fig));
				else
					list.getListDescription().remove(descr);
			}
			vdt.getConstraintsFactory().drawConstraints(fig);
		} catch (Exception ex) {
			MyManager.printError(ex);
		}
	}

	private void buttonDown2(MouseEvent e) {
	}

	private void buttonDown3(MouseEvent e) {
		try {		
			vdt.getMarkerFactory().showMarker(e.x, xscroll);
			vdt.getMarkerFactory().showFireable(e.x + xscroll);
		} catch (Exception ex) {
			MyManager.printError(ex);
		}
	}

	public void mouseDown(MouseEvent e) {
		xscroll = vdt.getCanvas().getHorizontalBar().getSelection();
		yscroll = vdt.getCanvas().getVerticalBar().getSelection();
		switch (e.button) {
		case 1:
			buttonDown1(e); //left click
			break;
		case 2:
			buttonDown2(e); //middle click
			break;
		case 3:
			buttonDown3(e); //right click
			break;

		default:
			break;
		}

	}

	public void mouseDoubleClick(MouseEvent e) {
	}

	public void mouseUp(MouseEvent e) {
		// nothing
	}
}
