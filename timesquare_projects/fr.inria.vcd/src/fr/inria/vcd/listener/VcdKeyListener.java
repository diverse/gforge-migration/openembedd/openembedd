package fr.inria.vcd.listener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

public class VcdKeyListener implements KeyListener{
	private IVcdDiagram vdt;
	
	public VcdKeyListener(IVcdDiagram vdt){
		this.vdt = vdt;
	}

	
	public void keyPressed(KeyEvent e) {
		
		if(e.keyCode == SWT.CONTROL)
			vdt.setCtrlKey(true);
	}

	public void keyReleased(KeyEvent e) {
		
		if(e.keyCode == SWT.CONTROL)
			vdt.setCtrlKey(false);
	}

}
