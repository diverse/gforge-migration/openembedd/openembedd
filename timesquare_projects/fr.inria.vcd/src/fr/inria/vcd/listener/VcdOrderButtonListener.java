package fr.inria.vcd.listener;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import fr.inria.vcd.dialogs.SelectClocksDialog;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public class VcdOrderButtonListener implements SelectionListener{
	private ListConnections list;
	private IVcdDiagram vdt;
	
	public VcdOrderButtonListener(ListConnections list, IVcdDiagram vdt) {
		this.list = list;
		this.vdt = vdt;
	}

	public void widgetDefaultSelected(SelectionEvent e){}
	
	public void widgetSelected(SelectionEvent e) {
		Shell shell = new Shell();
		SelectClocksDialog d = new SelectClocksDialog(
				shell, vdt.getTc().getAllnames(), vdt.getTc().getSelectedClocks(), "All clocks", "Selected clocks",
		"Selected clocks dialog");
		d.create();
		int ret = d.open();
		if (ret == Window.OK) {
			vdt.getCanvas().getContents().getChildren().clear();
			vdt.getNames().getContents().getChildren().clear();
			list.clear();
			vdt.getTc().setAllClocks(d.getSrc());
			vdt.getTc().constructClock(vdt.getFactory(), d.added(), vdt.getTcZoom());
			vdt.getFactory().getNameforfigures().clear();
		//	vdt.getTc().setZoom(vdt.getTcZoom());
			vdt.getVcd().visit(vdt.getTc());
			vdt.getNames().setContents(vdt.getFactory().getNames());
			vdt.getCanvas().setContents(vdt.getFactory().getBack());
			vdt.getNames().getParent().layout();
			vdt.getCanvas().getContents().validate();
			vdt.getConstraintsFactory().redrawConstraints();
			for(MenuItem menuitem :list.getMenuForComment().keySet())
			{
				if(menuitem.getSelection())
				{
					ConstraintCommentCommand cc =list.getMenuForComment().get(menuitem);
					Color color = list.menuForColorGet(menuitem);
					vdt.getConstraintsFactory().drawSyncInterval(cc,color);
				}
			}
			if(d.added().size() <= 1){
				vdt.getMyShell().setSize(vdt.getMyShell().getSize().x,200);
			}
			else{
				vdt.getMyShell().setSize(vdt.getMyShell().getSize().x,d.added().size()*45 + 200);
			}
			if(!vdt.isGhostMode())
				vdt.getConstraintsFactory().hideAllGhost();
		}
	}

	
}
