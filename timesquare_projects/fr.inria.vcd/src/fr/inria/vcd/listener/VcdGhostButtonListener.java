package fr.inria.vcd.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.MenuItem;

public class VcdGhostButtonListener  implements SelectionListener {
	private IVcdDiagram vdt;
	
	public VcdGhostButtonListener(IVcdDiagram vdt) {
		this.vdt = vdt;
	}
	
	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);
	}
	
	public void widgetSelected(SelectionEvent e) {
		
		MenuItem menuitem = (MenuItem)e.getSource();
		if(!menuitem.getSelection()){
			vdt.setGhostMode(false);
		}
		else{
			vdt.setGhostMode(true);
		}
	}
}
