/**
 * 
 */
package fr.inria.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.model.Description;

public class MouseMoveListenerVCD implements MouseMoveListener 
{
	/**
	 * 
	 */

	private IVcdDiagram vcd=null;
	
	
	public MouseMoveListenerVCD( IVcdDiagram vcd) {
		super();
		
		this.vcd = vcd;
	}



	public void mouseMove(MouseEvent e) {		
		int xscroll = vcd.getfcb().getCanvas().getHorizontalBar().getSelection();
		int yscroll = vcd.getfcb().getCanvas().getVerticalBar().getSelection();
		IFigure fig = vcd.getfcb().getCanvas().getContents().findFigureAt(e.x + xscroll,
				e.y + yscroll);
		if (fig != null) {
			if (fig instanceof PolylineConnection
					|| fig instanceof Label) {
				vcd.getConstraintsFactory().showConstraint(fig);
				if (fig instanceof PolylineConnection) {
					Description descr = vcd.getFactory()
							.getFigureForDescription().get(fig);
					if (descr != null)
						if (descr.getDescription() != null) {
							if (descr.getIndex() > 0) {
								vcd.getfcb().getCanvas().setToolTipText(descr.getName()
										+ "[" + descr.getIndex() + "]");
								// constraintsf.showCoincidence(fig);
							}
						}
				}
				if (fig instanceof ConstraintsConnection) {
					ConstraintsConnection poly = (ConstraintsConnection) fig;
					if (poly.getComment() != null) {
						ToolItem item;
						if (0 >= vcd.getToolbar().getItems().length) {
							item = new ToolItem(this.vcd.getToolbar(), SWT.PUSH);
						}
						item = vcd.getToolbar().getItems()[0];
						item.setText(poly.getComment().toString());
					}
				}
			} else {
				vcd.getfcb().getCanvas().setToolTipText(null);
				vcd.getConstraintsFactory().hideConstraints();
				vcd.getConstraintsFactory().hideCoincidence();
			}
		} 
	}
}