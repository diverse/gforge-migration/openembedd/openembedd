package fr.inria.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public class VcdIntervalButtonListener implements SelectionListener{
	private ListConnections list;
	private IVcdDiagram vdt;
	private ConstraintCommentCommand cc;
	
	public VcdIntervalButtonListener(ListConnections list, IVcdDiagram vdt,
			ConstraintCommentCommand cc) {
		super();
		this.list = list;
		this.vdt = vdt;
		this.cc = cc;
	}

	/*public VcdIntervalButtonListener(ListConnections list, IVcdDiagram vdt) {
		super();
		this.list = list;
		this.vdt = vdt;
	}*/

	public void widgetDefaultSelected(SelectionEvent e){}
	
	public void widgetSelected(SelectionEvent e) {
		MenuItem menuitem = (MenuItem)e.getSource();
			if(menuitem.getSelection()){
				vdt.getConstraintsFactory().drawSyncInterval(cc,list.menuForColorGet(menuitem));
			}
			if(!menuitem.getSelection()){
				for(IFigure f : list.getListInterval().get(cc)){
					vdt.getCanvas().getContents().remove(f);
				}
				list.getListInterval().get(cc).clear();
			}
	}
}




