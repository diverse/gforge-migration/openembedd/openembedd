package fr.inria.vcd.view;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYAnchor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;

import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.model.comment.StatusCommentCommand;

public class MarkerFactory {

	private int height = -1;
	private PolylineConnection marker = null;
	private PolylineConnection marker2 = null;
	private Label time = new Label();
		
	private IVcdDiagram vdt;
	
	public MarkerFactory(ListConnections list, IVcdDiagram vdt) {
		super();
		//this.list = list;
		this.vdt = vdt;		
		vdt.getFactory().setMarkerFactory(this);
	}
	
	private int oldfirable=-1;
	private int oldmarker=-1;

	public void hideMarkerFireable()
	{
		if (oldfirable!=-1)
			showFireable(oldfirable);
		if (oldmarker!=-1)
			showMarker(oldmarker, 0);
	}
	
	public void showFireable(int x) {
		int height = vdt.getCanvas().getContents().getSize().height;
		for (Object obj : vdt.getNames().getContents().getChildren()) {
			if (obj instanceof RectangleFigure) {
				RectangleFigure rf = (RectangleFigure) obj;
				Label l = (Label) rf.getChildren().get(0);
				l.setForegroundColor(MyColorApi.colorWhiteText());
			}
		}
		if (x==oldfirable)
		{
			oldfirable=-1;
			return ;  
		}
		oldfirable=x;
		IFigure oldfig = null;
		for (int y = 0; y < height; y++) {
			IFigure fig = vdt.getCanvas().getContents().findFigureAt(x, y);
			if (oldfig != null) {
				if (oldfig.equals(fig))
					continue;
			}
			Description descr = vdt.getFactory().getFigureForDescription().get(
					fig);
			String clockName = new String();
			if (descr != null) {
				for (StatusCommentCommand cc : vdt.getVcd()
						.getStatusCommentCommand()) {
					if (cc.getTime().equals(
							String.valueOf(descr.getTime()
									- vdt.getVcd().getPulse()))) {
						for (IVar var : vdt.getTc().getSelectedClocks()) {
							if (var.getIdentiferCode().equals(cc.getIdent())) {
								clockName = var.getName();
								break;
							}
						}					
						for (Object obj : vdt.getNames().getContents()
								.getChildren()) {
							if (obj instanceof RectangleFigure) {
								RectangleFigure rf = (RectangleFigure) obj;
								Label l = (Label) rf.getChildren().get(0);
								if (l.getText().equals(clockName))
									l.setForegroundColor(MyColorApi.colorDarkGreenGhostFireable());
								else
									continue;
							}
						}
						clockName = "";
						continue;
					}
				}
				if (clockName.equals("")) {
					clockName = descr.getName();
					for (Object obj : vdt.getNames().getContents()
							.getChildren()) {
						if (obj instanceof RectangleFigure) {
							RectangleFigure rf = (RectangleFigure) obj;
							Label l = (Label) rf.getChildren().get(0);
							if (l.getText().equals(clockName))
								l.setForegroundColor(MyColorApi.colorLightGreenFirable());

							else
								continue;
						}
					}
				}
				oldfig = fig;
			}
		}
	}

	public void showMarker(int x, int xscroll) {		
		String timeUnit= "";
		int factor =1;
		if (vdt.getVcd().getTimeScaleCommand()!= null)
		{
			timeUnit= vdt.getVcd().getTimeScaleCommand().getUnit().toString(); 
			factor=  vdt.getVcd().getTimeScaleCommand().getNumber();
		}
		if (height == -1)
			height = vdt.getCanvas().getContents().getSize().height;
		if (marker != null)
		{
			vdt.getCanvas().getContents().remove(marker);
			vdt.getScale().getContents().remove(marker2);
			if (x==oldmarker)
			{
				oldmarker=-1;
				marker=null;
				marker2=null;
				return ;
			}			
		}
		oldmarker=x;
		
		double zoom = vdt.getMarkerZoom();
		int offset = vdt.getVcd().getOffset();
	
	//	boolean dezoom = false;
		//if (vdt.getTcZoom() < 1)
			//dezoom = true;		
		double value=1.0;
		value = ( (x -5 ) +offset  +xscroll ) / zoom * factor;
		time.setText(value + " " + timeUnit);	
		time.setForegroundColor(MyColorApi.colorRedTime());		
		time.setOpaque(true);		
		marker = constructConnection(MyColorApi.colorRedTimeXor(), new Point(x, 0), new Point(x, height));
		marker.setOutlineXOR(true);
		marker.setLineWidth(1);
		PolygonDecoration decoration = new PolygonDecoration();
		PointList decorationPointList = new PointList(); 
		decorationPointList.addPoint(-2, 1);
		decorationPointList.addPoint(-4, 3);
		decorationPointList.addPoint(-4, 16);
		decorationPointList.addPoint(-2, 19);
		decorationPointList.addPoint(0, 16);
		decorationPointList.addPoint(0, 3);
		decorationPointList.addPoint(-2, 1);
		decoration.setTemplate(decorationPointList);
		decoration.setLayoutManager(new BorderLayout());
		decoration.setForegroundColor(MyColorApi.colorBlack());
		marker2=constructConnection(MyColorApi.colorRedTime(), new Point(x, 30), new Point(x, 50));
		marker2.setSourceDecoration(decoration);		
		if (vdt.getScale().getContents().getChildren().contains(time))
			vdt.getScale().getContents().remove(time);
		vdt.getCanvas().layout(); 
		vdt.getCanvas().getContents().add(marker);
		vdt.getScale().getContents().add(marker2);
		decoration.add(time, BorderLayout.CENTER);

	}
	
	private ConstraintsConnection constructConnection(Color color, Point f1,Point f2)
	{
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(1);
		c.setOpaque(true);
		XYAnchor sourceAnchor = new XYAnchor(f1);
		XYAnchor targetAnchor = new XYAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		return c;
	}
	
	
}
