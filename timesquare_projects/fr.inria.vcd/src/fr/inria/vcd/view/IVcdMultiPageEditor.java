package fr.inria.vcd.view;

import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.TraceCollector;

public interface IVcdMultiPageEditor {

	public abstract void setTc(TraceCollector tc);
	
	public abstract void setVcd(VCDDefinitions vcd);
	
	public abstract void setFactory(VcdFactory factory);
}