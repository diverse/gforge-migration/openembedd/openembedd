package fr.inria.vcd.view;

import org.eclipse.swt.widgets.Shell;

import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.visitor.TraceCollector;

public interface Iupdate {

	public abstract int update(int nl, int n,boolean  b);	
	public abstract boolean isSimulation();
	public abstract void setSimulation(boolean simulation);
	public abstract void setSimulationAvance(int etape);	
	public abstract TraceCollector getTc();
	public abstract Shell getMyShell();	
	public abstract VCDDefinitions getVcd();
	 public abstract 	 void  refresh();
public abstract int replace(int pos, String src, String newtext );


}