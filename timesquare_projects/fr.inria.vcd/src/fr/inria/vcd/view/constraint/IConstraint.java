package fr.inria.vcd.view.constraint;

import org.eclipse.draw2d.IFigure;

import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.IConstructContraint;
import fr.inria.vcd.view.VcdFactory;

public interface IConstraint {

	/**
	 * @return the vcdFactory
	 */
	VcdFactory getVcdFactory();

	/**
	 * @param vcdFactory the vcdFactory to set
	 */
	void setVcdFactory( VcdFactory vcdFactory );

	/**
	 * @return the cc
	 */
	public ConstraintCommentCommand getCc();

	/**
	 * @param cc the cc to set
	 */
	public void setCc( ConstraintCommentCommand cc );

	/**
	 * @return the list
	 */
	public ListConnections getList();

	/**
	 * @param list the list to set
	 */
	public void setList( ListConnections list );

	int draw( IFigure currentfig , String currentclock );
	
	/**
	 * @return the number of tick of the clock
	 */
	int drawConstraint( );
	
	boolean getIsConstraintVisible();
	
	void setIsConstraintVisible(boolean b);

	/**
	 * @return the icc
	 */
	public IConstructContraint getIcc();

	/**
	 * @param icc the icc to set
	 */
	public void setIcc( IConstructContraint icc );
	
	public  boolean haveAllClockVisible();
	

}