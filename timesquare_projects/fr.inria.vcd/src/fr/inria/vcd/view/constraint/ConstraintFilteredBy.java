package fr.inria.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.base.MyManager;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.MyColorApi;

public class ConstraintFilteredBy extends AbsConstraint implements IConstraint {

	public ConstraintFilteredBy()
	{
		super();
		testn=1;
		
	}

	

	/**
	 * @see fr.inria.vcd.view.constraint.IConstraint#draw(org.eclipse.draw2d.IFigure, java.lang.String)
	 */

	public int draw( IFigure currentfig , String currentclock )
	{
		boolean state =true;
		if (cc.getReferenceClocks().get(0).equals(currentclock))
			state=false;
		ArrayList<IFigure> figures = null;
		if (state)
			figures = vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0));
		else
			figures = vcdFactory.getNameforfigures().get(cc.getClock());
		IFigure dest = null;
		boolean state2 = false;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x == ((PolylineConnection) currentfig).getLocation().x)
			{
				dest = figs;
				state2 = true;
				break;
			}
		}
		if (!state2)
			return 0;
		ConstraintsConnection poly = null;
		if (state)
			poly = icc.constructConnection(MyColorApi.colorRedCoincidence(), dest, currentfig);
		else
			poly = icc.constructConnection(MyColorApi.colorRedCoincidence(), currentfig, dest);
		poly.setComment(cc);
		poly.setGlobal(isGlobal);
		icc.addToList(list.getListConstraints(), poly);
	
		return 0;
	}

	
	public int drawConstraint() {
		try
		{
			isGlobal=true;
			String nameClock=cc.getClock();
			if(haveAllClockVisible())
			{
				for(IFigure fig : vcdFactory.getNameforfigures().get(nameClock))
				{
					draw(fig, nameClock);
				}
			}
			else return -1;
			IVcdDiagram vdt=vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints())
			{
				Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
						true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible=true;
			isGlobal=false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		}
		catch(Throwable t){
			MyManager.printError(t);
			isGlobal=false;
			return -1;
		}
	}


	
	
}
