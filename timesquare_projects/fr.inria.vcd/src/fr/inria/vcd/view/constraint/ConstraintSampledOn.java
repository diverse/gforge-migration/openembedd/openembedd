package fr.inria.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.base.MyManager;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.MyColorApi;

public class ConstraintSampledOn  extends AbsConstraint implements IConstraint {

	public ConstraintSampledOn()
	{
		super();
		testn=2;
	}

	
	boolean weakly =false;

	/**
	 * @see fr.inria.vcd.view.constraint.IConstraint#draw(org.eclipse.draw2d.IFigure, java.lang.String)
	 */
	int c1=-1, c2a=-1, c2b=-1, c3=1;
	
	String clk1=null;
	String clk2=null;
	String clk3=null;
	ArrayList<IFigure> figures1 = null;
	ArrayList<IFigure> figures2 = null;
	ArrayList<IFigure> figures3	= null;
	/**
	 * @return the weakly
	 */
	public final boolean isWeakly()
	{
		return weakly;
	}

	/**
	 * @param weakly the weakly to set
	 */
	public final void setWeakly( boolean weakly )
	{
		this.weakly = weakly;
	}

	
	public int draw( IFigure currentfig , String currentclock )
	{	
		try
		{
			c1=c2a=c2b=c3=-1; 
			int cas=0;
			 clk1=cc.getClock();
			 clk2=cc.getReferenceClocks().get(0);
			 clk3=cc.getReferenceClocks().get(1);
			 figures1 = vcdFactory.getNameforfigures().get(clk1);
			 figures2 = vcdFactory.getNameforfigures().get(clk2);
			 figures3	= vcdFactory.getNameforfigures().get(clk3);
	 		if (currentclock.equals(clk1)) cas=1;
			if (currentclock.equals(clk2)) cas=2;
			if (currentclock.equals(clk3)) cas=3;
			
			int x= ((PolylineConnection) currentfig).getLocation().x;
			if (weakly)
			{
			switch ( cas) {
				case 1:
					c1= vcdFactory.getNameforfigures().get(clk1).indexOf(currentfig);
					c2a= icc.lastBefore(clk2,x );
					c2b= icc.synchrone(clk2, x);
					c3= icc.lastBefore(clk3, x);    
					break;				
				case 2:
					c2b= vcdFactory.getNameforfigures().get(clk2).indexOf(currentfig);
					c2a=c2b-1;
					c1= icc.synchrone(clk1, x);			
					c3= icc.lastBefore(clk3, x);				
					break;
				case 3:
					c3= vcdFactory.getNameforfigures().get(clk3).indexOf(currentfig);
					c2a= icc.lastBeforeweak(clk2,x );
					c2b= icc.firstAfter(clk2,x);
					c1= icc.firstAfter(clk1,x);
					break;				
				default:
					break;
			}
			}
			else
			{
				switch ( cas) {
					case 1:
						c1= vcdFactory.getNameforfigures().get(clk1).indexOf(currentfig);
						c2a= icc.lastBefore(clk2,x );
						c2b= icc.synchrone(clk2, x);
						c3= icc.lastBefore(clk3, x);    
						break;				
					case 2:
						c2b= vcdFactory.getNameforfigures().get(clk2).indexOf(currentfig);
						c2a=c2b-1;
						c1= icc.synchrone(clk1, x);			
						c3= icc.lastBefore(clk3, x);				
						break;
					case 3:
						c3= vcdFactory.getNameforfigures().get(clk3).indexOf(currentfig);
						c2a= icc.lastBefore(clk2,x );
						c2b= icc.firstAfterStrict(clk2,x);					
						c1= icc.firstAfter(clk1,x);
						break;				
					default:
						break;
				}
			}
			displaySync();
			return 0;
		}
		catch(Exception e)
		{
			return-1;
		}
	}

	private int displaySync()
	{
	
		if (c1!=-1 && c2b!=-1)
		{
			ConstraintsConnection poly =icc.constructConnection(MyColorApi.colorRedCoincidence(),
					figures1.get(c1), figures2.get(c2b));			 
			poly.setComment(cc);
			poly.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly);
		}		
		if (c3!=-1 && c2a!=-1&& c1!=-1)
		{
			
			ConstraintsConnection poly2 = icc.constructDashConnection(MyColorApi.colorWhiteArrow(),
					figures2.get(c2a), figures3.get(c3));
			poly2.setComment(cc);
			poly2.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly2);
		}
		if (c3!=-1 && c2b!=-1 && c1!=-1)
		{
			
			ConstraintsConnection poly2 = icc.constructDashConnection(MyColorApi.colorWhiteArrow(),
					figures3.get(c3), figures2.get(c2b));
			poly2.setComment(cc);
			poly2.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly2);
		}
		return 0;
	}

	
	public int drawConstraint() {
		try
		{
			isGlobal=true;
			String nameClock=cc.getClock();
			if(haveAllClockVisible())
			{
				for(IFigure fig : vcdFactory.getNameforfigures().get(nameClock))
				{
					draw(fig, nameClock);
				}
			}
			else return -1;
			IVcdDiagram vdt=vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints())
			{
				Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
						true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible=true;
			isGlobal=false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		}
		catch(Throwable t){
			MyManager.printError(t);
			isGlobal=false;
			return -1;
		}
	}

	
}
