package fr.inria.vcd.view.constraint;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.base.MyManager;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.MyColorApi;

public class ConstraintAlternates extends AbsConstraint implements IConstraint {

	public ConstraintAlternates()
	{
		super();
		testn=1;
	}

	/**
	 * @see fr.inria.vcd.view.constraint.IConstraint#draw(org.eclipse.draw2d.IFigure, java.lang.String)
	 */
	
	public int draw( IFigure currentfig , String currentclock )
	{
		boolean state =true;
		if (cc.getReferenceClocks().get(0).equals(currentclock))
			state=false;
		int index = vcdFactory.getNameforfigures().get(currentclock).indexOf(currentfig);
		IFigure dest = null;
		if (state)
			if (index <=vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0)).size())
			{
				dest = vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0)).get(index);
			}
			else
			{
				return 0;
			}
		else
		{
			if (index <= vcdFactory.getNameforfigures().get(cc.getClock()).size())
			{
				dest = vcdFactory.getNameforfigures().get(cc.getClock()).get(index);
			} else
			{
				return 0;
			}
		}
		ConstraintsConnection poly = null;
		if (((PolylineConnection) currentfig).getLocation().x == ((PolylineConnection) dest)
				.getLocation().x)
		{
			poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), currentfig, dest);
			poly.setComment(cc);
			poly.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly);
			IFigure dest2 = null;
			if (index < vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0))
					.size() - 1)
			{
				if (state)
					dest2 = vcdFactory.getNameforfigures()
							.get(cc.getReferenceClocks().get(0)).get(index + 1);
				else
					dest2 = vcdFactory.getNameforfigures().get(cc.getClock()).get(index + 1);
				poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), currentfig, dest2);
				poly.setComment(cc);
				poly.setGlobal(isGlobal);
				icc.addToList(list.getListConstraints(), poly);
			}
			return 0;
		}
		if (((PolylineConnection) currentfig).getLocation().x > ((PolylineConnection) dest)
				.getLocation().x)
		{
			poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), dest, currentfig);
			poly.setComment(cc);
			poly.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly);
			IFigure dest2 = null;
			if (index <= vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0))
					.size())
			{
				if (state)
				{
					if (index + 2 <= vcdFactory.getNameforfigures().get(
							cc.getReferenceClocks().get(0)).size())
					{
						dest2 = vcdFactory.getNameforfigures().get(
								cc.getReferenceClocks().get(0)).get(index + 1);
					} else
						return 0;
				} else
				{
					if (index + 2 <= vcdFactory.getNameforfigures().get(
							cc.getReferenceClocks().get(0)).size())
					{
						dest2 = vcdFactory.getNameforfigures().get(cc.getClock()).get(
								index + 1);
					} else
						return 0;
					poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), currentfig, dest2);
					poly.setComment(cc);
					poly.setGlobal(isGlobal);
					icc.addToList(list.getListConstraints(), poly);
				}
			}
			return 0;
		}
		poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), currentfig, dest);
		poly.setComment(cc);
		poly.setGlobal(isGlobal);
		icc.addToList(list.getListConstraints(), poly);
		IFigure dest2 = null;
		if (index >= 1)
		{
			if (state)
				dest2 = vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0))
						.get(index - 1);
			else
				dest2 = vcdFactory.getNameforfigures().get(cc.getClock()).get(index - 1);
			poly = icc.constructDashConnection(MyColorApi.colorWhiteArrow(), dest2, currentfig);
			poly.setComment(cc);
			poly.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly);
		}
		return 0;
	} 
    

	public int drawConstraint() 
	{
		try
		{
			isGlobal=true;
			String nameClock=cc.getClock();
			if(haveAllClockVisible())
			{
				for(IFigure fig : vcdFactory.getNameforfigures().get(nameClock))
				{
					draw(fig, nameClock);
				}
			}
			else return -1;
			IVcdDiagram vdt=vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints())
			{
				Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
						true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible=true;
			isGlobal=false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		}
		catch(Throwable t){
			isGlobal=false;
			MyManager.printError(t);
			return -1;
		}
	}

}
