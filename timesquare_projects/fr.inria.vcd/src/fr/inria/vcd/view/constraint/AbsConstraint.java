package fr.inria.vcd.view.constraint;

import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.view.IConstructContraint;
import fr.inria.vcd.view.VcdFactory;

public abstract class AbsConstraint implements IConstraint {

	
	protected VcdFactory vcdFactory=null;
	protected ConstraintCommentCommand cc=null;
	protected ListConnections list;
	protected IConstructContraint icc;
	protected boolean isConstraintVisible=false;
	protected boolean isGlobal=false;
	
	/**
	 * @see fr.inria.vcd.view.constraint.IConstraint#getVcdFactory()
	 */
	public final VcdFactory getVcdFactory()
	{
		return vcdFactory;
	}

	/** 
	 * @see fr.inria.vcd.view.constraint.IConstraint#setVcdFactory(fr.inria.vcd.view.VcdFactory)
	 */
	public final void setVcdFactory( VcdFactory vcdFactory )
	{
		this.vcdFactory = vcdFactory;
	}
	
	/**
	 * @return the cc
	 */
	public final ConstraintCommentCommand getCc()
	{
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public final void setCc( ConstraintCommentCommand cc )
	{
		this.cc = cc;
	}

	/**
	 * @return the list
	 */
	public final ListConnections getList()
	{
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public final void setList( ListConnections list )
	{
		this.list = list;
	}

	/**
	 * @return the icc
	 */
	public final IConstructContraint getIcc()
	{
		return icc;
	}

	/**
	 * @param icc the icc to set
	 */
	public final void setIcc( IConstructContraint icc )
	{
		this.icc = icc;
	}


	public final boolean getIsConstraintVisible() {
		return isConstraintVisible;
	}

	public final  void setIsConstraintVisible(boolean b) {
		isConstraintVisible=b;
	}
	
	protected int testn=1;
	/**
	 * 
	 * @return false if getClock or get(0) are equals to  null else return true
	 */
	public final boolean haveAllClockVisible()
	{
		if(vcdFactory.getNameforfigures().get(cc.getClock())==null)
			return false;
		if(vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0))==null)
			return false;
		if (testn>=2)
			if(vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(1))==null)
				return false;
		return true;
	}
	
	

}
