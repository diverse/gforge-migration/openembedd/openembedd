package fr.inria.vcd.view;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYAnchor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.vcd.figure.Balise;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.figure.Balise.Linkto;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;

public class ConstraintsFactory implements IConstructContraint {

	private ListConnections listed;	
	private IVcdDiagram vdt;

	public ConstraintsFactory(ListConnections list, IVcdDiagram vdt)
	{
		super();
		this.listed = list;
		this.vdt = vdt;
		vdt.getFactory().setConstraintsFactory(this);
		
	}

	
	public void addToList( ArrayList<IFigure> listSync , ConstraintsConnection poly )
	{
		// TODO exception getComment()==null
		ConstraintsConnection polyc = poly;
		Iterator<IFigure> it = listSync.iterator();
		boolean state = true;
		IFigure startAnchor1 = null;
		IFigure startAnchor2 = null;
		IFigure endAnchor1 = null;
		IFigure endAnchor2 = null;
		while (it.hasNext())
		{
			ConstraintsConnection pc = (ConstraintsConnection) it.next();
			if (pc.isGlobal()!= poly.isGlobal()) continue;
			if (pc.getComment()!= poly.getComment()) continue;
			if (pc.getTargetAnchor() != null && polyc.getSourceAnchor() != null
					&& polyc.getTargetAnchor() != null && pc.getSourceAnchor() != null)
			{
				startAnchor1 = pc.getSourceAnchor().getOwner();
				startAnchor2 = polyc.getSourceAnchor().getOwner();
				endAnchor1 = pc.getTargetAnchor().getOwner();
				endAnchor2 = polyc.getTargetAnchor().getOwner();
				if (startAnchor1 instanceof PolylineConnection
						&& startAnchor2 instanceof PolylineConnection
						&& endAnchor1 instanceof PolylineConnection
						&& endAnchor2 instanceof PolylineConnection)
				{
					if (((PolylineConnection) startAnchor1).getPoints().getBounds().equals(
							((PolylineConnection) startAnchor2).getPoints().getBounds()))
					{
						if (((PolylineConnection) endAnchor1).getPoints().getBounds().equals(
								((PolylineConnection) endAnchor2).getPoints().getBounds()))
						{
							state = false;
							listSync.remove(pc);
							if (vdt.getCanvas().getContents().getChildren().contains(pc))
								vdt.getCanvas().getContents().remove(pc); 
							Rectangle bounds = pc.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof RectangleFigure
						&& startAnchor2 instanceof RectangleFigure
						&& endAnchor1 instanceof RectangleFigure
						&& endAnchor2 instanceof RectangleFigure)
				{
					Rectangle rect1 = new Rectangle(((RectangleFigure) startAnchor1).getBounds());
					Rectangle rect2 = new Rectangle(((RectangleFigure) endAnchor1).getBounds());
					if (rect1.expand(5, 5).intersects(((RectangleFigure) startAnchor2).getBounds()))
					{
						if (rect2.expand(5, 5).intersects(
								((RectangleFigure) endAnchor2).getBounds()))
						{
							state = false;
							listSync.remove(pc);
							if (vdt.getCanvas().getContents().getChildren().contains(pc))
								vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof PolylineConnection
						&& startAnchor2 instanceof PolylineConnection
						&& endAnchor1 instanceof RectangleFigure
						&& endAnchor2 instanceof RectangleFigure)
				{
					if (((PolylineConnection) startAnchor1).getPoints().getBounds().equals(
							((PolylineConnection) startAnchor2).getPoints().getBounds()))
					{
						Rectangle rect = new Rectangle(((RectangleFigure) endAnchor1).getBounds());
						if (rect.expand(5, 5)
								.intersects(((RectangleFigure) endAnchor2).getBounds()))
						{
							state = false;
							listSync.remove(pc);
							if (vdt.getCanvas().getContents().getChildren().contains(pc))
								vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof RectangleFigure
						&& startAnchor2 instanceof RectangleFigure
						&& endAnchor1 instanceof PolylineConnection
						&& endAnchor2 instanceof PolylineConnection)
				{
					Rectangle rect = new Rectangle(((RectangleFigure) startAnchor2).getBounds());
					if (rect.expand(5, 5).intersects(((RectangleFigure) startAnchor1).getBounds()))
					{
						if (((PolylineConnection) endAnchor1).getPoints().getBounds().equals(
								((PolylineConnection) endAnchor2).getPoints().getBounds()))
						{
							state = false;
							listSync.remove(pc);
							if (vdt.getCanvas().getContents().getChildren().contains(pc))
								vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							break;
						}
					}
				}
			} else
			{
				if (polyc.getPoints().size() > 2)
				{
					for (int i = 0; i < pc.getPoints().size(); i++)
					{
						if (i != pc.getPoints().size() - 1)
						{
							if (!pc.getPoints().getPoint(i).equals(polyc.getPoints().getPoint(i)))
								break;

							continue;
						}
						state = false;
						listSync.remove(pc);
						vdt.getCanvas().getContents().remove(pc);
						Rectangle bounds = pc.getBounds();
						vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
								true);
						break;
					}
				}
			}
		}
		if (state)
			listSync.add(poly);
	}
	
	
	public ConstraintsConnection constructConnection( Color color , IFigure f1 , IFigure f2 )
	{
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
		PointList decorationPointList = new PointList();
		decorationPointList.addPoint(-2, 0);
		decorationPointList.addPoint(-1, 1);
		decorationPointList.addPoint(0, 0);
		decorationPointList.addPoint(-1, -1);
		decoration.setTemplate(decorationPointList);
		c.setTargetDecoration(decoration);
		c.setSourceDecoration(decoration);

		return c;
	}

	
	public ConstraintsConnection constructConnection( Color color , IFigure f1 , Point f2 )
	{
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		XYAnchor targetAnchor = new XYAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		return c;
	}

	/**
	 * @see fr.inria.vcd.view.IConstructContraint#constructDashConnection(org.eclipse.swt.graphics.Color,
	 *      org.eclipse.draw2d.IFigure, org.eclipse.draw2d.IFigure)
	 */
	
	public ConstraintsConnection constructDashConnection( Color color , IFigure f1 , IFigure f2 )
	{
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineStyle(Graphics.LINE_DASH);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		if (f1 != null && f2 != null)
			if (f1.getBounds() != null && f2.getBounds() != null)
				if (f1.getBounds().x == f2.getBounds().x)
				{
					decoration.setForegroundColor(color);
					decoration.setBackgroundColor(MyColorApi.colorBlack());
				}
		c.setTargetDecoration(decoration);

		return c;
	}

	
	public ConstraintsConnection constructPolylineConnection( Color color , Point pt1 , Point pt2 )
	{
		ConstraintsConnection poly = new ConstraintsConnection();
		poly.setPoints(new PointList());
		poly.setForegroundColor(color);
		poly.setOutlineXOR(true);
		poly.setLineWidth(2);
		poly.setOpaque(true);
		poly.addPoint(pt1);
		poly.addPoint(pt2);
		return poly;
	}

	/***
	 * 
	 * @return  index == date of (clk define by name)   
	 *   
	 */
	
	public int synchrone(String name, int date)
	{
	
		ArrayList<IFigure> figures = vdt.getFactory().getNameforfigures().get(name);
		int n=0;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x == date )
			{				
				return n;
			}
			n++;
		}
		return -1;
		
	}	
	
	
	/***
	 * 
	 * @return  premier index  >=  of (clk define by name) /   
	 *   
	 */
	
	public int firstAfter(String name, int date)
	{
	
		ArrayList<IFigure> figures = vdt.getFactory().getNameforfigures().get(name);
		int n=0;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x >= date)
			{				
				return n;
			}
			n++;
		}
		return -1;
		
	}
	
	/***
	 * 
	 * @return  premier index  >  of (clk define by name) /   
	 *   
	 */
	
	public int firstAfterStrict(String name, int date)  
	{
		ArrayList<IFigure> figures = vdt.getFactory().getNameforfigures().get(name);
		int n=0;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x > date)
			{				
				return n;
			}
			n++;
		}
		return -1;
	}
	
	/***
	 * 
	 * @return  dernier index  <  of (clk define by name) /   
	 *   
	 */
	
	public int lastBefore(String name, int px)
	{
		ArrayList<IFigure> figures = vdt.getFactory().getNameforfigures().get(name);
		int n=0;
		int k=-1;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x < px)
			{		
				k=n;				
			}
			n++;
		}
		return k;
	}
	
	public int lastBeforeweak(String name, int px)
	{
		ArrayList<IFigure> figures = vdt.getFactory().getNameforfigures().get(name);
		int n=0;
		int k=-1;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x <= px)
			{		
				k=n;				
			}
			n++;
		}
		return k;
	}
	
	public boolean drawConstraints( IFigure fig )
	{
		if (fig != null)
		{
			Description descr;
			if (fig instanceof Label)
				descr = vdt.getFactory().getFigureForDescription().get(fig.getParent());
			else
				descr = vdt.getFactory().getFigureForDescription().get(fig);
			if (descr == null)
				return false;
			String currentclock = descr.getName();
			if (fig instanceof PolylineConnection || fig instanceof Label)
			{
				for (ConstraintCommentCommand cc : vdt.getVcd().getConstraintCommentCommand())
				{
					if (cc.getClock().equals(currentclock))
					{
						switch (cc.getFunction()) {
							case _filteredby:
							case _synchronizeswith:
							case _sampledon:
							case _sustains:	
							case _alternateswith:
							case _isperiodicon:
								if (cc.getIc() != null)
								{
									//cc.getIc().setCc(cc);
									//cc.getIc().setList(listed);
									//cc.getIc().setVcdFactory(vdt.getFactory());
									//cc.getIc().setIcc(this);
									if (cc.getIc().haveAllClockVisible())
										cc.getIc().draw(fig, currentclock);
								}
								break;
							/*case _periodicon:
								filterByFunction(fig, currentclock, cc, MyColorApi
										.colorRedCoincidence(), true);
								break;*/
							case _restrictedto:
								filterByFunction(fig, currentclock, cc, MyColorApi
										.colorRedCoincidence(), true);
								break;
							/*case _alternateswith:
								alternateFunction(fig, currentclock, cc, null, true);
								break;		*/			
							case _oneshoton:
								oneShotConstraintFunction(fig, currentclock, cc, null, true);
								break;
							case _union:
								unionFunction(fig, currentclock, cc, null, true);
								break;
							case _precedes:
								precedesFunction(fig, currentclock, cc, MyColorApi
										.colorBluePrecedes(), true);
								break;
							default:
						}
						for (IFigure f : listed.getListConstraints())
						{
							Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
							vdt.getCanvas().getContents().add(f);
							Rectangle bounds = f.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							vdt.getCanvas().getContents().setPreferredSize(dim);
						}
					}
					if (cc.getReferenceClocks().contains(currentclock))
					{
						switch (cc.getFunction()) {
							case _filteredby:
							case _synchronizeswith:
							case _sampledon:
							case _sustains:
							case _alternateswith:
							case _isperiodicon:
								if (cc.getIc() != null)
								{
									//cc.getIc().setCc(cc);
									//cc.getIc().setList(listed);
									//cc.getIc().setVcdFactory(vdt.getFactory());
									//cc.getIc().setIcc(this);
									if (cc.getIc().haveAllClockVisible())
										cc.getIc().draw(fig, currentclock);
								}
								
								break;
						/*	case _periodicon:
								filterByFunction(fig, currentclock, cc, MyColorApi
										.colorRedCoincidence(), false);
								break;*/
							case _restrictedto:
								filterByFunction(fig, currentclock, cc, MyColorApi
										.colorRedCoincidence(), false);
								break;
						/*	case _alternateswith:
								alternateFunction(fig, currentclock, cc, null, false);
								break;*/
							case _union:
								unionFunction(fig, currentclock, cc, null, false);
								break;
							case _precedes:
								precedesFunction(fig, currentclock, cc, MyColorApi
										.colorBluePrecedes(), false);
								break;
							default: 
								break;
						}
						for (IFigure f : listed.getListConstraints())
						{
							Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
							vdt.getCanvas().getContents().add(f);
							Rectangle bounds = f.getBounds();
							vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
									true);
							vdt.getCanvas().getContents().setPreferredSize(dim);
						}
					}
				}
				if (fig.getForegroundColor().equals(MyColorApi.colorLightGreenFirable()))
				{
					// fig.setForegroundColor(MyColorApi.colorred());
				} else
				{
					fig.setForegroundColor(MyColorApi.colorLightGreenFirable());
				}
			}

		}
		return true;
	}

	public void redrawSyncInterval( ConstraintCommentCommand cc , Color color )
	{
		for (IFigure f : listed.getListInterval().get(cc))
		{
			vdt.getCanvas().getContents().remove(f);
		}
		listed.getListInterval().get(cc).clear();
		Iterator<IFigure> it = listed.lastClock1Get(cc).iterator();
		Iterator<IFigure> ot = listed.lastClock2Get(cc).iterator();
		while (it.hasNext() && ot.hasNext())
		{
			Point pt1 = ((PolylineConnection) it.next()).getPoints().getPoint(3);
			Point pt2 = ((PolylineConnection) ot.next()).getPoints().getPoint(3);
			ConstraintsConnection poly = null;
			if (pt1.x > pt2.x)
			{
				if (pt1.y > pt2.y)
				{
					poly = constructPolylineConnection(color, new Point(pt1.x + 5, pt1.y + 15),
							new Point(pt1.x + 5, pt2.y - 15));
				} else
				{
					poly = constructPolylineConnection(color, new Point(pt1.x + 5, pt1.y - 15),
							new Point(pt1.x + 5, pt2.y + 15));
				}
				listed.getListInterval().get(cc).add(poly);
				vdt.getCanvas().getContents().add(poly);
			} else
			{
				if (pt2.y > pt1.y)
				{
					poly = constructPolylineConnection(color, new Point(pt2.x + 5, pt2.y + 15),
							new Point(pt2.x + 5, pt1.y - 15));
				} else
				{
					poly = constructPolylineConnection(color, new Point(pt2.x + 5, pt2.y - 15),
							new Point(pt2.x + 5, pt1.y + 15));
				}
				listed.getListInterval().get(cc).add(poly);
				vdt.getCanvas().getContents().add(poly);
			}
		}
	}


	public void drawSyncInterval( ConstraintCommentCommand cc , Color color )
	{
		for (IFigure f : listed.getListInterval().get(cc))
		{
			if (f!=null)
			if (vdt.getCanvas().getContents()== f.getParent())
				vdt.getCanvas().getContents().remove(f);
		}
		Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
		listed.clearInterval(cc);
		String clock1 = null;
		String clock2 = null;
		clock1 = cc.getClock();
		clock2 = cc.getReferenceClocks().get(0);
		int occurencesclock1 = Integer.valueOf(cc.getReferenceClocks().get(1));
		int occurencesclock2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		if (vdt.getFactory().getNameforfigures().get(clock1) == null)
			return;
		if (vdt.getFactory().getNameforfigures().get(clock2) == null)
			return;
		listed.lastClock1Get(cc).clear();
		listed.lastClock2Get(cc).clear();
		for (int i = 0; i < vdt.getFactory().getNameforfigures().get(clock1).size(); i++)
		{
			if ((i + 1) % occurencesclock1 == 0)
			{
				listed.lastClock1Get(cc).add(vdt.getFactory().getNameforfigures().get(clock1).get(i));
				listed.firstClock1Get(cc).add(	vdt.getFactory().getNameforfigures().get(clock1).get(	i - (occurencesclock1 - 1)));
			}
		}
		for (int i = 0; i < vdt.getFactory().getNameforfigures().get(clock2).size(); i++)
		{
			if ((i + 1) % occurencesclock2 == 0)
			{
				listed.lastClock2Get(cc).add(vdt.getFactory().getNameforfigures().get(clock2).get(i));
				listed.firstClock2Get(cc).add(	vdt.getFactory().getNameforfigures().get(clock2).get(i - (occurencesclock2 - 1)));
			}
		}
		Iterator<IFigure> it = listed.lastClock1Get(cc).iterator();
		Iterator<IFigure> ot = listed.lastClock2Get(cc).iterator();
		while (it.hasNext() && ot.hasNext())
		{
			Point pt1 = ((PolylineConnection) it.next()).getPoints().getPoint(3);
			Point pt2 = ((PolylineConnection) ot.next()).getPoints().getPoint(3);
			ConstraintsConnection poly = null;
			if (pt1.x > pt2.x)
			{
				if (pt1.y > pt2.y)
				{
					poly = constructPolylineConnection(color, new Point(pt1.x + 5, pt1.y + 15),	new Point(pt1.x + 5, pt2.y - 15));
				} else
				{
					poly = constructPolylineConnection(color, new Point(pt1.x + 5, pt1.y - 15),	new Point(pt1.x + 5, pt2.y + 15));
				}
				
			} else
			{
				if (pt2.y > pt1.y)
				{
					poly = constructPolylineConnection(color, new Point(pt2.x + 5, pt2.y + 15), new Point(pt2.x + 5, pt1.y - 15));
				} else
				{
					poly = constructPolylineConnection(color, new Point(pt2.x + 5, pt2.y - 15),	new Point(pt2.x + 5, pt1.y + 15));
				}
				
			}
			listed.getListInterval().get(cc).add(poly);
			if (poly!=null)
				vdt.getCanvas().getContents().add(poly);
			
		
		}
		
		vdt.getCanvas().getContents().setPreferredSize(dim);
	}

	public IVcdDiagram getVdt()
	{
		return vdt;
	}

	public void hideCoincidence()
	{
		for (IFigure figs : listed.getListCoincidence())
		{
			figs.setForegroundColor(MyColorApi.colorLightGreenFirable());
		}
		listed.getListCoincidence().clear();
	}

	public void hideConstraints()
	{
		if (vdt.getToolbar().getItems().length == 0)
			return;
		for (ToolItem item : vdt.getToolbar().getItems())
		{
			item.setText("");
		}
	}

	public void addhiddenGhost( RectangleFigure rect )
	{
		RectangleFigure temp = new RectangleFigure();
		PolylineConnection poly = new PolylineConnection();
		PointList pl = new PointList();
		poly.setPoints(pl);
		temp.setOpaque(false);
		temp.setFill(true);
		poly.setOpaque(false);
		poly.setFill(true);
		Rectangle bounds = rect.getBounds();
		temp.setForegroundColor(MyColorApi.colorBlack());
		temp.setBackgroundColor(MyColorApi.colorBlack());
		poly.setForegroundColor(MyColorApi.colorLightGreenFirable());
		poly.setBackgroundColor(MyColorApi.colorLightGreenFirable());
		temp.setBounds(bounds);
		pl.addPoint(new Point(bounds.x, bounds.y + bounds.height - 1));
		pl.addPoint(new Point(bounds.x + bounds.width, bounds.y + bounds.height - 1));
		poly.setPoints(pl);
		if (rect.getParent() != null)
		{
			rect.getParent().add(temp);
			rect.getParent().add(poly);
		}
		listed.listGhostAdd(temp);
		listed.listGhostAdd(poly);

	}

	public void hideGhost(int index)
	{
		try
		{
		IVar var=vdt.getTc().getSelectedClocks().get(index);
		var.getValueFactory().setGhostVisible(false);
		}
		catch (Exception e){}
		
	}
	
	public void hideAllGhost()
	{
		ArrayList<IVar> listVar=vdt.getTc().getAllnames();
		ArrayList<IVar> listVar2= vdt.getTc().getSelectedClocks();
		for(IVar var : listVar)
		{
			if (var.getValueFactory()!=null)
			var.getValueFactory().setGhostVisible(false);
		}
		for(IVar var : listVar2)
		{
			if (var.getValueFactory()!=null)
			var.getValueFactory().setGhostVisible(false);
		}
	}

	public void redrawConstraints()
	{
		ArrayList<IFigure> temp = new ArrayList<IFigure>();
		for (Object obj : vdt.getCanvas().getContents().getChildren())
		{
			Panel panel = null;
			if (obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			for (Object f : panel.getChildren())
			{
				IFigure poly = (IFigure) f;
				for (Description d : listed.getListDescription())
				{
					Description descr = vdt.getFactory().getFigureForDescription().get(poly);
					if (descr == null || d == null)
						continue;
					if (d.equals(descr))
					{
						temp.add(poly);
					}
				}
			}
		}
		for (IFigure fig : temp)
		{
			vdt.getConstraintsFactory().drawConstraints(fig);
		}
	}

	public void showConstraint( IFigure fig )
	{
		if (fig != null)
		{
			if (fig instanceof PolylineConnection || fig instanceof Label)
			{
				Description descr = null;
				if (fig instanceof PolylineConnection)
					descr = vdt.getFactory().getFigureForDescription().get(fig);
				if (fig instanceof Label)
					descr = vdt.getFactory().getFigureForDescription().get(fig.getParent());
				if (descr == null)
					return;
				String currentclock = descr.getName();
				int i = 0;
				for (ConstraintCommentCommand cc : vdt.getVcd().getConstraintCommentCommand())
				{
					if (cc.getClock().equals(currentclock)
							|| cc.getReferenceClocks().contains(currentclock))
					{
						if (vdt.getToolbar() == null)
							return;
						for (ToolItem item : vdt.getToolbar().getItems())
						{
							if (item.getText().equals(cc.toString()))
								return;
						}
						String fonction = "";
						switch (cc.getFunction()) {
							default:
								fonction = cc.toString();
						}
						ToolItem item;
						if (i >= vdt.getToolbar().getItems().length)
						{
							item = new ToolItem(vdt.getToolbar(), SWT.PUSH);
						}
						item = vdt.getToolbar().getItems()[i];
						item.setText(fonction);
						i++;
					}
				}
			}
		}
	}

	public void showGhost(int index)
	{
		try
		{
			IVar var=vdt.getTc().getSelectedClocks().get(index);	
			var.getValueFactory().setGhostVisible(true);
		}
		catch (Exception e){
			
		}
	}
	
	public void showAllGhost()
	{
		ArrayList<IVar> listVar=vdt.getTc().getAllnames();
		ArrayList<IVar> listVar2= vdt.getTc().getSelectedClocks();
		for(IVar var : listVar)
		{
			if (var.getValueFactory()!=null)
				var.getValueFactory().setGhostVisible(true);
		}
		for(IVar var : listVar2)
		{
			if (var.getValueFactory()!=null)
				var.getValueFactory().setGhostVisible(true);
		}
	}

	private void filterByFunction( IFigure currentfig , String currentclock ,
			ConstraintCommentCommand cc , Color color , boolean state )
	{
		ArrayList<IFigure> figures = null;
		if (state)
			figures = vdt.getFactory().getNameforfigures().get(cc.getReferenceClocks().get(0));
		else
			figures = vdt.getFactory().getNameforfigures().get(cc.getClock());
		IFigure dest = null;
		boolean state2 = false;
		for (IFigure figs : figures)
		{
			if (((PolylineConnection) figs).getLocation().x == ((PolylineConnection) currentfig)
					.getLocation().x)
			{
				dest = figs;
				state2 = true;
				break;
			}
		}
		if (!state2)
			return;
		ConstraintsConnection poly = null;
		if (state)
			poly = constructConnection(MyColorApi.colorRedCoincidence(), dest, currentfig);
		else
			poly = constructConnection(MyColorApi.colorRedCoincidence(), currentfig, dest);
		poly.setComment(cc);
		addToList(listed.getListConstraints(), poly);
	}

	private void oneShotConstraintFunction( IFigure currentfig , String currentclock ,
			ConstraintCommentCommand cc , Color colornull , boolean b )
	{ // b is unused
		String firstclock = cc.getReferenceClocks().get(0);
		String secondtclock = cc.getReferenceClocks().get(1);
		String thirdclock = cc.getReferenceClocks().get(2);
		int delay = Integer.valueOf(cc.getReferenceClocks().get(3));
		ArrayList<IFigure> figsoffirstclock = vdt.getFactory().getNameforfigures().get(firstclock);
		ArrayList<IFigure> figsofsecondtclock = vdt.getFactory().getNameforfigures().get(
				secondtclock);
		ArrayList<IFigure> figsofthirdclock = vdt.getFactory().getNameforfigures().get(thirdclock);
		int j = 0;
		boucle: for (IFigure fig : figsoffirstclock)
		{
			j++;
			if (fig.getBounds().x == ((Polygon) currentfig.getParent()).getPoints().getPoint(1).x)
			{
				for (int i = j - 1; i < j + delay; i++)
				{
					if (figsoffirstclock.get(i).getForegroundColor().equals(
							MyColorApi.colorLightGreenFirable()))
					{
						figsoffirstclock.get(i)
								.setForegroundColor(MyColorApi.colorRedCoincidence());
					} else
					{
						figsoffirstclock.get(i).setForegroundColor(
								MyColorApi.colorLightGreenFirable());
					}
				}
				break boucle;
			}
		}
		boucle2: for (IFigure fig : figsofsecondtclock)
		{
			if (fig.getBounds().x == ((Polygon) currentfig.getParent()).getPoints().getPoint(1).x)
			{
				Point pt = ((Polygon) currentfig.getParent()).getPoints().getPoint(1);
				ConstraintsConnection poly = constructConnection(MyColorApi.colorRedCoincidence(),
						fig, pt);
				addToList(listed.getListConstraints(), poly);
				break boucle2;
			}
		}
		boucle3: for (IFigure fig : figsofthirdclock)
		{

			if (fig.getBounds().x == ((Polygon) currentfig.getParent()).getPoints().getPoint(4).x)
			{
				Point pt = ((Polygon) currentfig.getParent()).getPoints().getPoint(4);
				ConstraintsConnection poly = constructConnection(MyColorApi.colorRedCoincidence(),
						fig, pt);
				addToList(listed.getListConstraints(), poly);
				break boucle3;
			}
		}

	}

	private void precedesFunction( IFigure currentfig , String currentclock ,
			ConstraintCommentCommand cc , Color color , boolean state )
	{
		int startindex = -1;
		int stopindex = -1;
		int startindex2 = -1;
		int stopindex2 = -1;
		ArrayList<IFigure> clk1 = vdt.getFactory().getNameforfigures().get(cc.getClock());
		ArrayList<IFigure> clk2 = vdt.getFactory().getNameforfigures().get(
				cc.getReferenceClocks().get(0));
		int index = 0;
		// vdt.getFactory().getNameforfigures().get(currentclock).indexOf(
		// currentfig) + 1;
		int coeff = Integer.valueOf(cc.getReferenceClocks().get(1));
		int coeff2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		int group = 0;

		if (state)
		{
			index = clk1.indexOf(currentfig) + 1;
			group = (index - 1) / coeff;

		} else
		{
			index = clk2.indexOf(currentfig) + 1;
			group = (index - 1) / coeff2;
		}

		startindex = group * coeff;
		stopindex = (group + 1) * coeff - 1;
		startindex2 = group * coeff2;
		stopindex2 = (group + 1) * coeff2 - 1;

		IFigure start = clk1.get(startindex);
		IFigure stop = clk1.get(stopindex);
		IFigure start2 = clk2.get(startindex2);
		IFigure stop2 = clk2.get(stopindex2);

		ArrayList<ConstraintsConnection> conn = constructPacket(color, start, stop);
		ArrayList<ConstraintsConnection> conn2 = constructPacket(color, start2, stop2);

		for (ConstraintsConnection c : conn)
		{
			addToList(listed.getListConstraints(), c);
		}
		for (ConstraintsConnection c : conn2)
		{
			addToList(listed.getListConstraints(), c);
		}
		/*
		 * ConstraintsConnection poly =
		 * constructDashConnection(MyColorApi.colorWhiteArrow(), currentfig,
		 * conn.get(0).getSourceAnchor().getOwner()); poly.setComment(cc);
		 * addToList(list.listConstraints, poly);
		 */

		ConstraintsConnection poly2 = constructDashConnection(MyColorApi.colorWhiteArrow(), stop,
				start2);
		// poly2.setComment(cc);
		addToList(listed.getListConstraints(), poly2);

	}
	
	private Balise createBalise( int x , int y )
	{
		Balise rect = new Balise();
		rect.setOpaque(false);
		rect.setFill(true);
		rect.setForegroundColor(MyColorApi.colorBlack());
		rect.setBackgroundColor(MyColorApi.colorBlack());
		rect.setBounds(new Rectangle(x, y, 1, 1));
		return rect;
	}

	public ArrayList<ConstraintsConnection> constructPacket( Color color , IFigure f1 , IFigure f2 )
	{
		ArrayList<ConstraintsConnection> res = new ArrayList<ConstraintsConnection>();
		Balise[] tab = new Balise[4];
		tab[0] = createBalise(f1.getBounds().getTopLeft().x - 2, f1.getBounds().getTopLeft().y - 4);
		tab[0].setAnchor(f1);
		tab[0].setLinkTo(Linkto.tl);
		tab[1] = createBalise(f2.getBounds().getTopRight().x + 2,
				f2.getBounds().getTopRight().y - 4);
		tab[1].setAnchor(f2);
		tab[1].setLinkTo(Linkto.tr);
		tab[2] = createBalise(f2.getBounds().getBottomRight().x + 2, f2.getBounds()
				.getBottomRight().y + 5);
		tab[2].setAnchor(f2);
		tab[2].setLinkTo(Linkto.br);
		tab[3] = createBalise(f1.getBounds().getBottomLeft().x - 2,
				f1.getBounds().getBottomLeft().y + 5);
		tab[3].setAnchor(f1);
		tab[3].setLinkTo(Linkto.bl);
		for (int i = 0; i < 4; i++)
		{
			for (Object obj : f1.getParent().getChildren())
			{
				if (obj instanceof Balise)
				{
					Balise rect = (Balise) obj;
					if (tab[i].getBounds().equals(rect.getBounds()))
					{
						tab[i] = rect;
					}
				}
			}
		}
		f1.getParent().add(tab[0]);
		f1.getParent().add(tab[1]);
		f1.getParent().add(tab[2]);
		f1.getParent().add(tab[3]);
		ChopboxAnchor targetAnchor;
		for (int j = 0; j < 4; j++)
		{
			ConstraintsConnection poly = new ConstraintsConnection();
			poly.setOpaque(false);
			poly.setLineWidth(2);
			poly.setForegroundColor(color);
			ChopboxAnchor sourceAnchor = new ChopboxAnchor(tab[j]);
			if (j == 3)
			{
				targetAnchor = new ChopboxAnchor(tab[0]);
			} else
				targetAnchor = new ChopboxAnchor(tab[j + 1]);
			poly.setSourceAnchor(sourceAnchor);
			poly.setTargetAnchor(targetAnchor);
			res.add(poly);
		}

		return res;
	}

	private void unionFunction( IFigure currentfig , String currentclock ,
			ConstraintCommentCommand cc , Color colornull , boolean state )
	{
		ArrayList<IFigure> figures1 = null;
		ArrayList<IFigure> figures2 = null;
		if (state)
		{
			figures1 = vdt.getFactory().getNameforfigures().get(cc.getReferenceClocks().get(0));
			figures2 = vdt.getFactory().getNameforfigures().get(cc.getReferenceClocks().get(1));
		} else
			figures1 = vdt.getFactory().getNameforfigures().get(cc.getClock());
		IFigure dest1 = null;
		IFigure dest2 = null;
		boolean state2 = false;
		boolean state3 = false;
		for (IFigure figs : figures1)
		{
			if (((PolylineConnection) figs).getLocation().x == ((PolylineConnection) currentfig)
					.getLocation().x)
			{
				dest1 = figs;
				state2 = true;
				break;
			}
		}
		if (state)
		{
			for (IFigure figs : figures2)
			{
				if (((PolylineConnection) figs).getLocation().x == ((PolylineConnection) currentfig)
						.getLocation().x)
				{
					dest2 = figs;
					state3 = true;
					break;
				}
			}
		}
		if (!state2 && !state3)
			return;
		if (state2)
		{
			ConstraintsConnection poly1 = constructConnection(MyColorApi.colorRedCoincidence(),
					dest1, currentfig);
			poly1.setComment(cc);
			addToList(listed.getListConstraints(), poly1);
		}
		if (state3)
		{
			ConstraintsConnection poly2 = constructConnection(MyColorApi.colorRedCoincidence(),
					dest2, currentfig);
			poly2.setComment(cc);
			addToList(listed.getListConstraints(), poly2);
		}
	}
	/* Arrow Motor */
	public int getDateOf( String name , int n )
	{
		return 0;
	}
}