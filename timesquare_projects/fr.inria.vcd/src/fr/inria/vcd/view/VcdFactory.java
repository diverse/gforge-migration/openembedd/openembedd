package fr.inria.vcd.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.base.MyManager;
import fr.inria.vcd.ScaleZoom;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.preferences.ColorSelection;
import fr.inria.vcd.view.figure.ExtendFigure;

final public class VcdFactory {
	final private int height_2 = 10, height = height_2 << 1, dY = 5, nameWidth = 0;
	/**
	 * @return the height_2
	 */
	public final int getHeight_2()
	{
		return height_2;
	}

	/**
	 * @return the nameWidth
	 */
	public final int getNameWidth()
	{
		return nameWidth;
	}

	/**
	 * @return the height
	 */
	public final int getHeight()
	{
		return height;
	}



	final private int y = 5;

	private int timemax = 0;
//	private int zero = 0;
	private Polyline p = null;
	private ScaleZoom vcdZoom = null;
	private IVcdDiagram vcddia=null;

	//private double zoom;

	private final class ToolTipRectangle extends RectangleFigure {
		//private int n=0;
		private IVar variable=null;
		private Label l= null;
		//private boolean b=true;
		public ToolTipRectangle(IVar variable) {
			super();
			int n= variable.getName().length()+8 ;
			
			this.variable = variable;		
			this.setOpaque(false);
			this.setLineStyle(Graphics.LINE_DOT);
			this.setForegroundColor(MyColorApi.colorYellowTooltip());
			this.setBounds(new Rectangle(0,0, 8*n+10, 58));
			l= new Label();
			l.setText("default ");
			//l.setBounds(new Rectangle(1, 1, 98, 43));			
			l.setBounds(new Rectangle(5, 3, 8*n, 53));
			l.setForegroundColor(MyColorApi.colorTextToolTip());
			l.setOpaque(true);
			//this.setBounds(new Rectangle(new Point(0,0)  ,l.getSize()));
			this.add(l);		
		}

		@Override
		public boolean isVisible() {		
			return super.isVisible();
		}

		@Override
		public void setBounds(Rectangle rect) {
			super.setBounds(rect);
		}
		
		
		

		@Override
		public void repaint() {
			if (l!=null)
			{
				if (variable!=null)
				{
					String s=null;
					s= "Name: "+variable.getName() + "\n"; 	
					if (variable.getValueFactory()!=null)
					{
						s+= "Nb of tic: "+variable.getValueFactory().getEdge()+ "\n";
						s+= "Ghost: "+variable.getValueFactory().haveGhostinclock();
						if(variable.getValueFactory().haveGhostinclock())
							s+="\nNb of Ghost:"+variable.getValueFactory().getNbGhost();
					} 
					l.setText(s);
					l.getPreferredSize();
					l.setSize(l.getPreferredSize());
					this.setSize(l.getPreferredSize().expand(10, 6));
					
					//this.setBounds(new Rectangle(new Point(0,0)  ,l.getSize()));					
					l.repaint();
				}
				else
				{
				l.setText("nothing");
				}
			}
			super.repaint();
		}

		@Override
		public void setVisible(boolean visible) {
			
			super.setVisible(visible);
		}
	}

	public class TimeLineElement {
		private Polyline figure = null;
		private int indice = 0;
		private int ind = 0;
		//private int  factor = 0;
		private int y2=0;
		private Label time = null;
		private Polygon poly=null;
		public TimeLineElement( int ind,int  f ,int y) {			
			this.figure =  new PolylineConnection();
			figure.setPoints(new PointList());
			this.ind =ind; 
			indice =  (ind *f) ;
			//factor =f;
			y2=y;
			figure.addPoint(new Point(ind + 5, 5)); 
			figure.addPoint(new Point(ind + 5, y2));
			figure.setForegroundColor(MyColorApi.colorWhiteRule() );
			figure.setVisible(true);
			figure.setOpaque(false);
		}

		public void inittime() 
		{
			time = new Label();
			time.setText("" + indice);		
			time.setBounds(new Rectangle (ind+3,10,100,20));
			time.setVisible(true);
			time.setBackgroundColor(MyColorApi.colorBlack());
			time.setForegroundColor(MyColorApi.colorRedRule());
			time.setOpaque(true);	
			poly = new Polygon();
			poly.setLayoutManager(new BorderLayout());
			poly.setOpaque(false);
			poly.setForegroundColor(MyColorApi.colorBlack());
			poly.addPoint(new Point(ind+3,10));
			poly.addPoint(new Point(ind+103,10));
			poly.addPoint(new Point(ind+103,30));
			poly.addPoint(new Point(ind+3,30));
			poly.add(time, BorderLayout.CENTER);			
		}

		public void clear() 
		{
			figure.erase();
			time.erase();
			poly.erase();
			time=null;
			poly=null;
			indice = 0;
			figure = null;

		}

		@Override
		protected void finalize() throws Throwable {
			clear();
			super.finalize();
		}
		
		public void scale(double x)		
		{
			try
			{
			PointList pl=  figure.getPoints();
			int lx = (int) (ind * x) + 5;
			Point p = pl.getPoint(0);
			p.x = lx;
			pl.setPoint(p, 0);
			p = pl.getPoint(1);
			p.x = lx;
			pl.setPoint(p, 1);
			figure.setPoints(pl);
			figure.repaint();
			if (poly==null)
			{
				if (time!=null)
					time.setLocation(new Point(lx,0));	
				return ;
			}
			
			lx++;
			/**  modification atomic de la liste des point */
			 pl=  poly.getPoints();			
			pl.setPoint(new Point(lx,10),0);
			pl.setPoint(new Point(lx+100,10),1);
			pl.setPoint(new Point(lx+100,30),2);
			pl.setPoint(new Point(lx,30),3);
			poly.setPoints(pl);
			time.setLocation(new Point(lx,10));	
			poly.repaint();
			
			
			}
			catch (Exception e) {
				MyManager.printError(e);
			}
		}
		public void setVisibleText(boolean b)
		{
			if (poly!=null)
			{
				poly.setVisible(b);
			}
		}

	}

	private ArrayList<TimeLineElement> timeline = new ArrayList<TimeLineElement>();
	private TimeLineElement endtle=null;
	private Panel scale;
	private Panel back;
	private Panel namesP;
	private Panel picture;

	private Rectangle bounds = new Rectangle();
	private HashMap<IFigure, Description> figurefordescription = new HashMap<IFigure, Description>();
	private HashMap<String, ArrayList<IFigure>> nameforfigures = new HashMap<String, ArrayList<IFigure>>();
	private HashMap<String, Panel> nameforpanel = new HashMap<String, Panel>();
	private ArrayList<RectangleFigure> ghostList = new ArrayList<RectangleFigure>();

	private ConstraintsFactory constraintsFactory;
	private MarkerFactory markerFactory;

	private VCDDefinitions vcd=null;
	public VcdFactory() {
		GridLayout layout = null;
		layout = new GridLayout(1, true);
		layout.marginHeight=25;		
		
		namesP = new Panel();
		namesP.setBackgroundColor(MyColorApi.colorBlack());
		namesP.setOpaque(true);
		namesP.setLayoutManager(layout);

		back = new Panel();
		back.setBackgroundColor(MyColorApi.colorBlack());
		back.setOpaque(true);
		layout = new GridLayout(1, true);
		layout.marginHeight=25;		
		back.setLayoutManager(layout);

		layout = new GridLayout(1, true);
		layout.marginHeight = 1;
		layout.verticalSpacing = 0; // 0 for design
		layout.horizontalSpacing=0;
		scale = new Panel();
		scale.setBackgroundColor(MyColorApi.colorBlack());
		scale.setOpaque(false);
		scale.setLayoutManager(layout);
		scale.setSize(-1, 35);
		createRule();

		layout = new GridLayout(1, true);
		layout.marginWidth = 0;
		layout.horizontalSpacing=0;
		picture = new Panel();
		picture.setBackgroundColor(MyColorApi.colorBlack());
		picture.setOpaque(false);
		picture.setLayoutManager(layout);
		picture.setSize(120, 35);
	}

	void add(Figure f, Description description, boolean b) {
		if (f==null)
			return ;
		//TODO update 
		bounds = bounds.getUnion(f.getBounds());
		back.getLayoutManager().layout(back);

		if (f instanceof PolylineConnection) 
		{
			PolylineConnection fig = (PolylineConnection) f;
			fig.getPoints().translate(nameforpanel.get(description.getName()).getLocation());
			if  ( f instanceof ExtendFigure)
			{
				((ExtendFigure) f).setTr(nameforpanel.get(description.getName()).getLocation());
				((ExtendFigure) f).mycompute();
			}
		}
		
		if (f instanceof Polygon)
		{
			Polygon poly = (Polygon) f;
			poly.getPoints().translate(	nameforpanel.get(description.getName()).getLocation());
			for (Object l : poly.getChildren()) {
				if (l instanceof Label)
					((Label) l).setBounds(((Label) l).getBounds().translate(
							nameforpanel.get(description.getName())
									.getLocation()));
			}
			poly.setBounds(poly.getBounds().translate(	nameforpanel.get(description.getName()).getLocation()));
			if  ( f instanceof ExtendFigure)
			{
				((ExtendFigure) f).setTr(nameforpanel.get(description.getName()).getLocation());
				((ExtendFigure) f).mycompute();
			}
		}
		
		if (f instanceof RectangleFigure) {
			RectangleFigure rect = (RectangleFigure) f;
			rect.setBounds(rect.getBounds().translate(
					nameforpanel.get(description.getName()).getLocation()));
			if  ( f instanceof ExtendFigure)
			{
				((ExtendFigure) f).setTr(nameforpanel.get(description.getName()).getLocation());
				((ExtendFigure) f).mycompute();
			}

		}
		nameforpanel.get(description.getName()).add(f);
		
		if (description.getIndex() != -1) {
			nameforfigures.put(description.getName(), description.getFigures());
			figurefordescription.put(f, description);
		}
		if (f instanceof RectangleFigure) {
		//	RectangleFigure rect2 = (RectangleFigure) f;
			if (b) {
				if (constraintsFactory != null)
					if (constraintsFactory.getVdt() != null)
						if (!constraintsFactory.getVdt().isGhostMode())
						{
							//rect2.setVisible(false);
						}
			}
		}
	}

	public VcdValueFactory getVcdValueFactory(IVar var, String name, double zoom)  //TODO rename
	{
		Panel p = new Panel();
		p.setOpaque(true);
		p.setBackgroundColor(MyColorApi.colorBlack());
		p.setPreferredSize(new Dimension(2000, height + dY + 10));
		
		nameforpanel.put(name, p);
		back.add(p);
		Label l = new Label(name); 
		
		RectangleFigure rectangle = new RectangleFigure();
		int size= ColorSelection.getDefault().getWidthName()-10; // 90;
		rectangle.setOpaque(false);
		rectangle.setLineStyle(Graphics.LINE_DOT);
		rectangle.setForegroundColor(MyColorApi.colorLightGrayClockNameContour());
		rectangle.setBounds(new Rectangle(0, 0, size, height + dY + 10));
		l.setBounds(new Rectangle(1, 1, size-4, height + dY + 5));
		l.setForegroundColor(MyColorApi.colorWhiteText());
		l.setOpaque(true);
		rectangle.add(l);
		namesP.add(rectangle);
		RectangleFigure tiprectangle = new ToolTipRectangle(var);
		rectangle.setToolTip(tiprectangle); 
		VcdValueFactory out = new VcdValueFactory(name, this, y, zoom);		
		var.setValueFactory(out);
		// y += height + dY;
		return out;
	}

	public IFigure getNames() {
		for (Object p : namesP.getChildren()) {
			if (p instanceof Panel) {
				((Panel) p).setPreferredSize(bounds.getSize().width, height
						+ dY + 10); 				
			}
		} 
		return namesP;
	}

	public Panel getScale() {
		return scale;
	}

	public Panel getBack() {
		// bounds.expand(0, 1);
		
		for (Object p : back.getChildren()) {
			if (p instanceof Panel) {
				((Panel) p).setPreferredSize(bounds.getSize().width, height
						+ dY + 10); 				
			}
		} 
		
		return back;
	}

	public int getLength()
	{
		return bounds.getSize().width;
	}
	public int getHeigth()
	{
		int n=0;
		for (Object p : back.getChildren()) {
			if (p instanceof Panel) {
				n++;
			}
		}
		return (n+1)*40+40;//old *35+ 50 40.40
	}
	
	public Panel getPicture() {
		return picture;
	}

	public HashMap<IFigure, Description> getFigureForDescription() {
		return figurefordescription;
	}

	public HashMap<String, ArrayList<IFigure>> getNameforfigures() {
		return nameforfigures;
	}

	public ArrayList<RectangleFigure> getGhostList() {
		return ghostList;
	}

	public HashMap<String, Panel> getNameforpanel() {
		return nameforpanel;
	}

	public ArrayList<RectangleFigure> addGhost(RectangleFigure e) {
		ghostList.add(e);
		if (constraintsFactory != null) {
			if (constraintsFactory.getVdt() != null)
				if (!constraintsFactory.getVdt().isGhostMode())
					constraintsFactory.addhiddenGhost(e);
		}
		return ghostList;
	}

	public ConstraintsFactory getConstraintsFactory() {
		return constraintsFactory;
	}

	public void setConstraintsFactory(ConstraintsFactory constraintsFactory) {
		this.constraintsFactory = constraintsFactory;
	}

	public MarkerFactory getMarkerFactory() {
		return markerFactory;
	}

	public void setMarkerFactory(MarkerFactory markerFactory) {
		this.markerFactory = markerFactory;
	}
	 
	
	
	public void setVcd( VCDDefinitions vcd ) 
	{
		this.vcd = vcd;
	}

	public int modifyRuleSize(int n) {  
		if (n > timemax) 
		{
			int f=1;		
			if (vcd!=null)
			{
				if (vcd.getTimeScaleCommand()!=null)
				f =vcd.getTimeScaleCommand().getNumber();
			
			}
			if (p != null)
			{
				p.setPoint(new Point(n + 5 +20, 5), 1);				
				int r = timemax % 10;		
				boolean b=false;
				int bmin ;
				if (timemax==0)
					bmin=0;
				else
					bmin= timemax - r+10 ;
				for (int i = bmin ; i <= n; i += 10) {					
					b = false;
					int lc = i % 500;
					TimeLineElement tle=null;
					switch (lc) {
					case 0:
						
						tle = new TimeLineElement( i,f,27); 
						b = true;
						break;
					case 100:
					case 200:
					case 300:
					case 400:
						tle = new TimeLineElement( i,f,18); 
						
						b = true;
						break;
					default:
						tle = new TimeLineElement( i,f,11); 						
						break;
					}

					
					
					if (b)
					{						
					 tle.inittime();
					 scale.add(tle.poly);
					 endtle=tle;
					}
					scale.add(tle.figure);
					timeline.add(tle);					
						
					
				}
			}

			timemax = n;
		}
		return 0;
	}

	private int createRule() {
		p = new Polyline();// Connection();//Connection();
		p.setPoints(new PointList());
		p.addPoint(new Point(5, 5));
		p.addPoint(new Point(100, 5));
		p.setForegroundColor(MyColorApi.colorWhiteRule());
		p.setVisible(true);
		scale.add(p);

		return 0;
	}

	public void zoomscale(double x) {	
		for (TimeLineElement tle : timeline) {
			tle.scale(x);		
		}
		if (endtle!=null)
			endtle.setVisibleText(true);
		if (p != null)
			p.setPoint(new Point((int) (timemax * x) + 5, 5), 1);
		scale.setPreferredSize((int) (timemax * x) + 10, 35);
		scale.repaint();
	}

	public ScaleZoom getVcdZoom() {
		return vcdZoom;
	}

	public void setVcdZoom(ScaleZoom vcdZoom) {
		this.vcdZoom = vcdZoom;
	}

	public VCDDefinitions getVcd() {
		return vcd;
	}

	public IVcdDiagram getVcddia() {
		return vcddia;
	}

	public void setVcddia(IVcdDiagram vcddia) {
		this.vcddia = vcddia;
	}

	
}
