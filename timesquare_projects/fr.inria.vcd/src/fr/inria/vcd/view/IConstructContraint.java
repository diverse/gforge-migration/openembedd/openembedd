package fr.inria.vcd.view;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.Color;

import fr.inria.vcd.figure.ConstraintsConnection;

public interface IConstructContraint 
{

	public ConstraintsConnection constructDashConnection( Color color , IFigure f1 , IFigure f2 );
	public void addToList( ArrayList<IFigure> listSync , ConstraintsConnection poly );
	public  ConstraintsConnection constructConnection( Color color , IFigure f1 , IFigure f2 );
	public ConstraintsConnection constructPolylineConnection( Color color , Point pt1 , Point pt2 );
	public ConstraintsConnection constructConnection( Color color , IFigure f1 , Point f2 );
	public ArrayList<ConstraintsConnection> constructPacket( Color color , IFigure f1 , IFigure f2 );
	
	public int synchrone(String name, int px);
	public int firstAfter(String name, int px);
	public int firstAfterStrict(String name, int px);
	public int lastBefore(String name, int px);
	public int lastBeforeweak(String name, int px);
}