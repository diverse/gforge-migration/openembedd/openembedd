package fr.inria.vcd.view;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.base.MyManager;
import fr.inria.vcd.VcdZoom;
import fr.inria.vcd.figure.ConstraintsConnection;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.listener.ListConnections;
import fr.inria.vcd.listener.MouseClickListener;
import fr.inria.vcd.listener.MouseDraggedListener;
import fr.inria.vcd.listener.VcdFindButtonListener;
import fr.inria.vcd.listener.VcdGhostButtonListener;
import fr.inria.vcd.listener.VcdIntervalButtonListener;
import fr.inria.vcd.listener.VcdKeyListener;
import fr.inria.vcd.listener.VcdOrderButtonListener;
import fr.inria.vcd.listener.WheelListener;
import fr.inria.vcd.menu.VcdMenu;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.FigureCanvasBase;
import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.visitor.TraceCollector;

public class VcdDiagram extends ApplicationWindow implements IVcdDiagram , Iupdate { //TODO Update ??
	
	protected FigureCanvas canvas;
	protected Composite composite;
	protected ConstraintsFactory constraintsf; 
	private MarkerFactory markerFactory;
	protected boolean ctrlKey = false;
	protected VcdFactory factory = new VcdFactory();
	protected boolean ghostMode = true;
	protected ArrayList<MenuItem> intervalItem;
	protected ListConnections list;
	protected double markerZoom = 1;
	protected FigureCanvas names;
	private Rectangle oldrc=null;
	protected Shell shell;
	protected boolean simulation = false;
	protected TraceCollector tc;
	protected double tcZoom = 1;
	protected String title;
	protected ToolBar toolbar;
	protected VCDDefinitions vcd;
	protected double zoom = 1;

	
	
	
	public FigureCanvasBase getfcb() {
		return null;
	}

	/*public VcdDiagram(Shell parentShell) {
		super(parentShell);
		this.list = new ListConnections(new ArrayList<Description>(),new ArrayList<IFigure>(),new HashMap<ConstraintCommentCommand,ArrayList<IFigure>>());
		this.constraintsf = new ConstraintsFactory(list,this);
		this.markerFactory =new MarkerFactory(list, this);
		vcdzoom =new VcdZoom(list, this);
		//this.vcdzoom.setList(list);
	}*/

	public VcdDiagram(Shell parentShell,String title,VcdFactory factory) {
		super(parentShell);
		this.list = new ListConnections(new ArrayList<Description>(),new ArrayList<IFigure>(),new HashMap<ConstraintCommentCommand,ArrayList<ConstraintsConnection>>()); 
		this.title = title;
		this.factory = factory;
		this.constraintsf = new ConstraintsFactory(list,this);
		this.markerFactory =new MarkerFactory(list, this);
	}
	
	public void addCtrlKeyListener(){
		canvas.addKeyListener(new VcdKeyListener(this));
	}
	
	public void addMouseClickListener() {
		canvas.addMouseListener(new MouseClickListener(list,this));
	}

	
	public ArrayList<Action> getIntervalAction() {
		return null;
	}

	public void  refresh()
	{
		
	}
	public void addMouseDraggedListener(){
		MouseDraggedListener listener = new MouseDraggedListener(list,this);
		names.addMouseListener(listener);
		names.addMouseMoveListener(listener);
	}

	public void addMouseMouveListener() {
		canvas.addMouseMoveListener(new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {
				int xscroll = canvas.getHorizontalBar().getSelection();
				int yscroll = canvas.getVerticalBar().getSelection();
				IFigure fig = canvas.getContents().findFigureAt(e.x+xscroll, e.y+yscroll);
				if (fig != null) {
					if (fig instanceof PolylineConnection || fig instanceof Label) {
						constraintsf.showConstraint(fig);
						if(fig instanceof PolylineConnection){
							Description descr = factory.getFigureForDescription().get(fig);
							if (descr != null)
								if (descr.getDescription() != null) {
									if(descr.getIndex() > 0){
										canvas.setToolTipText(descr.getName() + "["+ descr.getIndex() + "]");
										//constraintsf.showCoincidence(fig);
									}
								}
							}
						if(fig instanceof ConstraintsConnection){
							ConstraintsConnection poly = (ConstraintsConnection) fig;
							if(poly.getComment() != null){
								ToolItem item;
								if (0 >= getToolbar().getItems().length) {
									item = new ToolItem(getToolbar(), SWT.PUSH);
								}
								item = getToolbar().getItems()[0];
								item.setText(poly.getComment().toString());
							}
						}
					}else{
						canvas.setToolTipText(null);
						constraintsf.hideConstraints();
						constraintsf.hideCoincidence();
					}
				}
			}
		});
	}


	public void addMouseWheelListener(){
		canvas.addMouseWheelListener(new WheelListener(this));
	}
	

	public void addScrollListener(){
		canvas.getVerticalBar().addSelectionListener(new SelectionListener(){
			
			public void widgetDefaultSelected(SelectionEvent e) {
				
				getNames().scrollToY(getCanvas().getVerticalBar().getSelection());
			}
			
			public void widgetSelected(SelectionEvent e) {
				
				getNames().scrollToY(getCanvas().getVerticalBar().getSelection());
			}
		});
	}
	
	@Override
	public boolean close() {
		
		getCanvas().setContents(null);
		getNames().setContents(null);
		getCanvas().dispose();
		getNames().dispose();	
		
		MyManager.println("Closed");
		composite.dispose();
		list.getListDescription().clear();
		list.getListConstraints().clear();		
		Runtime.getRuntime().runFinalization();	
		
		return super.close();
	}

	@Override
	public void create() {
		super.create();
		shell = this.getShell();
		shell.setSize(Toolkit.getDefaultToolkit().getScreenSize().width,getCanvas().getSize().y+140);
		addMouseMouveListener();
		addMouseClickListener();
		addMouseDraggedListener();
		addMouseWheelListener();
		addScrollListener();
		addCtrlKeyListener();
		createMenuBar();
	}

	public void createClockMenu(Menu menuBar) {
		// Clock menu.
		MenuItem clockMenu;
		clockMenu = new MenuItem(menuBar, SWT.CASCADE);
		clockMenu.setText("Clock");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		clockMenu.setMenu(menu);

		// Clock -> Order.
		MenuItem subItem = new MenuItem(menu, SWT.NULL);
		subItem.setText("Order");
		subItem.addSelectionListener(new VcdOrderButtonListener(list,this));
	}

	@Override
	protected Control createContents(Composite parent) {
		getShell().setText(title);
		
		composite = new Group(parent, SWT.NONE);
		names = new FigureCanvas(composite, SWT.NONE);
		toolbar = new ToolBar(composite, SWT.NONE);
		
		composite.setBackground(MyColorApi.colorMenuBackground());
		composite.setLayout(new FormLayout());
		
		FormData canvasData = new FormData();
		canvasData.left = new FormAttachment(names);
		canvasData.right = new FormAttachment(100,0);
		canvasData.bottom =   new FormAttachment(toolbar);
		canvasData.top = new FormAttachment(0,0);
		
		canvas = new FigureCanvas(composite, SWT.NONE);
		canvas.setBackground(MyColorApi.colorBlack());
		canvas.setScrollBarVisibility(FigureCanvas.AUTOMATIC);
		canvas.setLayoutData(canvasData);
		canvas.setContents(factory.getBack());
		
		FormData namesData = new FormData();
		namesData.left = new FormAttachment(0,0);
		namesData.bottom = new FormAttachment(toolbar);
		namesData.top = new FormAttachment(0,0);
		namesData.width = 100;
		
		names.setLayoutData(namesData);
		names.setContents(factory.getNames());
		names.setScrollBarVisibility(FigureCanvas.NEVER);

		
		FormData toolbarData = new FormData();
		toolbarData.left = new FormAttachment(0,0);
		toolbarData.right = new FormAttachment(100,0);
		toolbarData.bottom = new FormAttachment(100,0);
		toolbarData.height=25;
		
		toolbar.setLayoutData(toolbarData);

		composite.setSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		return composite;
	}
	
	
	
	
	public FigureCanvas getScale() {		
		return null;
	}

	public void createFileMenu(Menu menuBar) {
		// File menu.
		MenuItem fileMenu;
		fileMenu = new MenuItem(menuBar, SWT.CASCADE);
		fileMenu.setText("File");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		fileMenu.setMenu(menu);
		
		/*MenuItem printItem = new MenuItem(menu, SWT.NULL);
		printItem.setText("Print");
		printItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				/*PrintDialog printd = new PrintDialog(getShell());
				//Image image = new Image(composite.getDisplay(), new Rectangle(0,0,2000,2000));
				Image image = composite.get
				PrinterData  data = printd.open();
				
				if(data!=null)
			    {
			        Printer p = new Printer(data);
			        p.startJob("PrintJob");    
			        p.startPage();
			        GC g = new GC(composite);
			        g.copyArea(image, 0, 0);
			        GC gcPrint = new GC(p);
			        gcPrint.drawImage(image,0, 0);
			        p.endPage();
			        gcPrint.dispose();
			        p.endJob();
			        p.dispose();
			    }*/
		/*	}*/
		/*});*/

		MenuItem exitItem = new MenuItem(menu, SWT.NULL);
		exitItem.setText("Exit");
		class ExitListener implements SelectionListener {
			private VcdDiagram vdt;
			
			public ExitListener(VcdDiagram vdt) {
				super();
				this.vdt = vdt;
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e); 
				
			}
			
			public void widgetSelected(SelectionEvent e) {
				vdt.close();
			}
		}
		exitItem.addSelectionListener(new ExitListener(this));
	}

	public void createFindMenu(Menu menuBar) {
		MenuItem findMenu;
		findMenu = new MenuItem(menuBar, SWT.CASCADE);
		findMenu.setText("Find");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		findMenu.setMenu(menu);

		MenuItem subItem = new MenuItem(menu, SWT.NULL);
		subItem.setText("Find");
		subItem.addSelectionListener(new VcdFindButtonListener(list,this));
	}
	
	public void createMenuBar() {
		Menu menuBar = new Menu(this.getShell(), SWT.BAR);
		this.getShell().setMenuBar(menuBar);
		// create each header and subMenu for the menuBar
		createFileMenu(menuBar);
		createClockMenu(menuBar);
		createOptionsMenu(menuBar);
		createFindMenu(menuBar);
	}	
	
	public void createOptionsMenu(Menu menuBar) {
		// Options menu.
		MenuItem optionMenu;
		optionMenu = new MenuItem(menuBar, SWT.CASCADE);
		optionMenu.setText("Options");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		optionMenu.setMenu(menu);
		
		MenuItem ghostItem1 = new MenuItem(menu, SWT.CASCADE | SWT.CHECK);
		ghostItem1.setText("Show Ghost");
		ghostItem1.setSelection(true);
		ghostItem1.addSelectionListener(new VcdGhostButtonListener(this));
		
		MenuItem syncItem = new MenuItem(menu, SWT.CASCADE);
		syncItem.setText("Synchronized");
		Menu syncMenu = new Menu(menu);
		syncItem.setMenu(syncMenu);
		syncItem.setEnabled(false);
		for(ConstraintCommentCommand cc : vcd.getConstraintCommentCommand()){
			if(cc.getFunction() == Function._synchronizeswith){
				syncItem.setEnabled(true);
				break;
			}
		}
		if(syncItem.isEnabled()){
			intervalItem = new ArrayList<MenuItem>();
			int i = 0;
			ArrayList<String> clocksnames = new ArrayList<String>();
			Color color = MyColorApi.colorLightGraySync();
			for(ConstraintCommentCommand cc : vcd.getConstraintCommentCommand()){
				if(i == 0) color = MyColorApi.colorLightGraySync();
				if(i == 1) color = MyColorApi.colorLightBlueSync();
				if(i == 2) color = MyColorApi.colorRedSync();
				if(i == 3) i=0;
				if(cc.getFunction() != Function._synchronizeswith) continue;
				if(clocksnames.contains(cc.getReferenceClocks().get(0))) continue;
				clocksnames.add(cc.getClock());
				list.getListInterval().put(cc, new ArrayList<ConstraintsConnection>());
				list.firstClock1Put(cc, new ArrayList<IFigure>());
				list.firstClock2Put(cc, new ArrayList<IFigure>());
				list.lastClock1Put(cc, new ArrayList<IFigure>());
				list.lastClock2Put(cc, new ArrayList<IFigure>());
				MenuItem item = new MenuItem(syncMenu, SWT.CHECK);
				item.setText(cc.toString());
				item.addSelectionListener(new VcdIntervalButtonListener(list,this,cc));
				list.getMenuForComment().put(item,cc);
				list.menuForColorPut(item,color);
				intervalItem.add(item);
				i++;
			}
		}
	}
	
	public FigureCanvas getCanvas() {
		return canvas;
	}

	public ConstraintsFactory getConstraintsFactory() {
		return constraintsf;
	}

	
	
	
	public MarkerFactory getMarkerFactory()
	{
		
		return markerFactory;
	}

	public VcdFactory getFactory() {
		return factory;
	}

	/**
	 * @param factory the factory to set
	 */
	public final void setFactory( VcdFactory factory )
	{
		this.factory = factory;
	}

	public ArrayList<MenuItem> getIntervalItem() {
		return intervalItem;
	}

	public ListConnections getList() {
		return list;
	}

	public double getMarkerZoom() {
		return markerZoom;
	}

	public Shell getMyShell() {
		return shell;
	}

	public FigureCanvas getNames() {
		return names;
	}

	public TraceCollector getTc() {
		return tc;
	}

	public double getTcZoom() {
		return tcZoom;
	}

	public ToolBar getToolbar() {
		return toolbar;
	}
	
	public VCDDefinitions getVcd() {
		return vcd;
	}
	
	public double getZoom() {
		return zoom;
	}

	
	
	public boolean isCtrlKey() {
		
		return ctrlKey;
	}

	public boolean isGhostMode() {
		return ghostMode;
	}

	public boolean isSimulation() {
		return simulation;
	}

	
	public void setCtrlKey(boolean ctrlKey) {
		this.ctrlKey = ctrlKey;
	}
	public void setGhostMode(boolean ghostMode) {
		this.ghostMode = ghostMode;
	}

	public void setMarkerZoom(double markerZoom) {
		this.markerZoom = markerZoom;
	}

	public void setSimulation(boolean simulation) {
		this.simulation = simulation;
	}

	public void setTc(TraceCollector tc) {
		this.tc = tc;
	}

	public void setTcZoom(double tcZoom) {
		this.tcZoom = tcZoom;
	}

	public void setVcd(VCDDefinitions vcd) {
		this.vcd = vcd;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public  void setSimulationAvance(int etape)
	{
		
	}
	
	public int update(int n1, int n2,boolean  b)
	{

				//getFactory().initFactory();
		ArrayList<IVar> listvar = new ArrayList<IVar>();
		listvar.addAll(getTc().getSelectedClocks());
		getTc().setZoom(getTcZoom());
		getVcd().visit(getTc(),n1,n2,b);
		//getNames().setContents(getFactory().getNames());
		getCanvas().setContents(getFactory().getBack());
		
		int x1=-1, x2=-1, y1=-1,y2=-1;
		
		org.eclipse.draw2d.geometry.Rectangle rc=null;
		for(Object obj : getCanvas().getContents().getChildren())
		{
			Panel panel = null;
			if(obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			org.eclipse.draw2d.geometry.Rectangle  r=null;
			//Object f= panel.getChildren().get(panel.getChildren().size()-1);
			for(Object f : panel.getChildren())
			{
				if(f instanceof IFigure)
				{
					IFigure poly = (IFigure)f;
				r=poly.getBounds();
				if (rc==null ) rc=r ; 
				else
					rc=rc.getUnion(r); 
					
				}
				
			}
		
		}
		

		org.eclipse.draw2d.geometry.Rectangle rc2 =null;
		if (oldrc==null)
		{
			rc2=rc;
		}
		else
			rc2=new org.eclipse.draw2d.geometry.Rectangle(oldrc.getTopRight(), rc.getBottomRight());

		org.eclipse.draw2d.geometry.Rectangle rc3 =null;
		
		
		oldrc=rc;
		
		for(Object obj : getCanvas().getContents().getChildren())
		{
			Panel panel = null;
			if(obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			org.eclipse.draw2d.geometry.Rectangle  r=null;
			//Object f= panel.getChildren().get(panel.getChildren().size()-1);
			for(Object f : panel.getChildren())
			{
				if(f instanceof IFigure)
				{
					IFigure poly = (IFigure)f;
				r=poly.getBounds();
				if (r.intersects(rc2))
				if (rc3==null ) rc3=r ; 
				else
					rc3=rc3.getUnion(r);  
				}
				
			}
		
		}
		if (rc3!=null)
		{
			x1=rc3.x;
			x2=rc3.width;
			y1=rc3.y;
			y2=rc3.height;
			getCanvas().layout();
			for(MenuItem menuitem :list.getMenuForComment().keySet()){
				if(menuitem.getSelection()){
					ConstraintCommentCommand cc = list.getMenuForComment().get(menuitem);
					Color color = list.menuForColorGet(menuitem);
					getConstraintsFactory().drawSyncInterval(cc,color);
				}
			}
			getCanvas().redraw(x1,y1,x2,y2, false);
		}

		return 0;
	}
	
	private VcdZoom vcdzoom=null;
	public VcdZoom getVcdzoom() {
		return vcdzoom;
	}
	
	public void setFocus()
	{
		if (shell!=null)
			if (!shell.isDisposed())
				shell.setFocus();
	}
	
	public int replace(int pos, String src, String newtext )
	{
		return 0;
	}

	public boolean haveGhost() {
		
		return true;
	}

	
	public VcdMenu getVcdmenu() {
	
		return null;
	}
	
}
