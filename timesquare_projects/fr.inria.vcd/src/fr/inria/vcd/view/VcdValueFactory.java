package fr.inria.vcd.view;

import java.util.ArrayList;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;

import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.Value;
import fr.inria.vcd.view.figure.Draw0;
import fr.inria.vcd.view.figure.Draw1;
import fr.inria.vcd.view.figure.DrawTimer;
import fr.inria.vcd.view.figure.DrawX;
import fr.inria.vcd.view.figure.DrawZ;
import fr.inria.vcd.view.figure.ExtendFigure;
import fr.inria.vcd.view.figure.ExtendFigure.Level;

final public class VcdValueFactory {
	

	private VcdFactory vcd;
	private int x, y, compteur = 0;
	private String clockName;
	private ArrayList<IFigure> figures= new ArrayList<IFigure>();
	private ArrayList<DrawX> ghostFigures= new ArrayList<DrawX >(); 
	private boolean isGhostVisible;
	private int time = 0;
	private int edge=0;
	private boolean lastvalueisx =false;
	private boolean ghostinclock =false;
	private int nbGhost=0;
	private Value oldvalue=null;
	private Value currentvalue=null;
	private ExtendFigure precedent;

	public boolean getIsGhostVisible()
	{
		return isGhostVisible;
	}
	
	public void setIsGhostVisible(boolean b)
	{
		isGhostVisible=b;
	}
	
	public int getNbGhost()
	{
		return nbGhost;
	}

	public boolean haveGhostinclock() {
		return ghostinclock;
	}

	public void setZoom(double zoom) {
	//	this.zoom = zoom;  // TODO remove zoom?
	}

	/*VcdValueFactory(String clockName, VcdFactory vcdFactory, int y) {
		super();	
		this.vcd = vcdFactory;
		this.y = y;
		this.x = vcdFactory.getNameWidth();
		this.clockName = clockName;
	}*/
	
	VcdValueFactory(String clockName, VcdFactory vcdFactory, int y,double zoom) {
		super();		
		this.vcd = vcdFactory;
		this.y = y;
		this.x = vcdFactory.getNameWidth();
		this.clockName = clockName;

	}

	public void build(Object value, int length) {
		boolean b;
		Description d;
		Figure f;
		if (value instanceof Value) {
			b=ghostundisp((Value) value);
			d=getDescription((Value)value,length);
			f=get((Value) value, length);
			lastvalueisx = ((Value) value==Value._x );
			
		} else {
			if (value==null)
				return ;
			b=ghostundisp(value.toString());
			d=getDescription(value.toString(),length);
			f=get(value.toString(), length);
			lastvalueisx = value.toString().equals("x");
		}		
		vcd.add(f,d,b);
		
	}

	public void build(Object value, int t , int previoustime) {
		this.time = t;
		int length = time - previoustime;
		boolean b;
		Description d;
		Figure f;
		if (value instanceof Value) {
			f=get((Value) value, length);
			 b=ghostundisp((Value) value);
			 d=getDescription((Value)value,time);
			lastvalueisx = ((Value) value==Value._x );
		} else {
			if (value==null)
				return ;
			f=get(value.toString(), length);
			 b=ghostundisp(value.toString());
			 d=getDescription(value.toString(),time);
			lastvalueisx = value.toString().equals("x");
		}
		
		
			vcd.add(f,d,b );
			
		
		
	}
	
	public boolean ghostundisp(Value value)
	{
		switch (value) {
		case _0:
		case _1:
		case _z:
			return false;
		
		
		case _x:
			return true;
		
			
		}
		return false;
	}

	public boolean ghostundisp(String value)
	{
		if (value.equals("x")) 
			return true;		
		return false;
	}
	
	public int getEdge() {
		return edge;
	}

	private Description getDescription(Value value, int time) {
		switch (value) {
		
		case _1:
			return new Description(clockName,compteur,String.valueOf(compteur),figures,time);
		case _0:
			return new Description(clockName,-1,"-1",figures,time);
		case _x:			
		case _z:
			return new Description(clockName,time);
		}
		return null;
	}

	private Description getDescription(String value, int time) {
		
			return new Description(clockName,-1,figures,time);
		
	}

	private Figure get(Value value, int length) {
		Value tmp = currentvalue ;
		currentvalue= value;
		switch (value) {
		case _0:
			oldvalue =tmp;
			return getZero(length);
		case _1:
			compteur++;
			Figure fig = getOne(length);			
			figures.add(fig);
			oldvalue =tmp;
			return fig;
		case _x:
			//oldvalue =tmp;
			Figure figx=getX(length);		   
			return figx;
		case _z:
			oldvalue =tmp;
			return getZ(length);
		}		
		return null;
	}

	private Figure get(String value, int length) {
		if (value.equals("z"))		
			return getZ(length);
		DrawTimer drawtime= new DrawTimer(vcd,x,y,length, "Active");
		if (precedent !=null )
			drawtime.setPrecedeLevel(precedent.getLevel());
		drawtime.mycompute();
		/*Label label = new Label("Active"); //TODO value or count
		label.setBounds(new Rectangle( (int) (x*zoom ), y, (int )(length * zoom), vcd.getHeight()));
		Polygon poly = new Polygon();
		poly.setLayoutManager(new BorderLayout());
		poly.setOpaque(false);
		poly.setForegroundColor(MyColorApi.colorTimer());
		poly.addPoint(new Point(x* zoom + 2, y + vcd.getHeight()));
		poly.addPoint(new Point(x* zoom, y + vcd.getHeight_2()));
		poly.addPoint(new Point(x* zoom + 2, y));
		x += length;
		poly.addPoint(new Point(x* zoom - 2, y));
		poly.addPoint(new Point(x* zoom, y + vcd.getHeight_2()));
		poly.addPoint(new Point(x* zoom - 2, y + vcd.getHeight() ));
		poly.add(label,BorderLayout.CENTER);*/
		x += length;
		precedent = drawtime ;
		return drawtime;  
	}

	private Figure getOne(int length) {	
		Draw1 dr1= new Draw1(vcd,x,y,length);
		if (precedent !=null )
			dr1.setPrecedeLevel(precedent.getLevel());
		dr1.setoldValue(lastvalueisx ? 1 : 0);
		dr1.mycompute();		
		x += length;	
		edge++;	
		precedent=dr1; //poly				
		return dr1; // poly
	}

	private Figure getZero(int length) {
		Draw0 dr0= new Draw0(vcd,x,y,length);
		if (precedent !=null )
			dr0.setPrecedeLevel(precedent.getLevel());
		dr0.setoldValue(oldvalue==Value._z ? 1 : 0 );
		dr0.mycompute();	
		precedent=dr0 ; 
		x += length;
		return dr0 ;
	}

	private Figure getZ(int length) {
		if (precedent!=null)
		{
			precedent.setFuturLevel(Level.z);
			//Point p=precedent.getPoints().getLastPoint(); //TODO postdraw
			//precedent.addPoint(new Point(p.x, p.y + coef* vcd.getHeight_2()));
			precedent.mycompute();
			precedent.repaint();
		}		
		DrawZ drz= new DrawZ(vcd,x,y,length);
		if (precedent !=null )
			drz.setPrecedeLevel(precedent.getLevel());
		drz.setoldValue(oldvalue==Value._z ? 1 : 0 );
		drz.mycompute();	
		x += length;
		precedent = drz;
		return drz;
	}

	private  Figure getX(int length) {
		DrawX drawX = new DrawX(vcd,x,y,length);	
		if (precedent !=null )
			drawX.setPrecedeLevel(precedent.getLevel());
		x += length;		
		drawX.setGhostvisible(isGhostVisible); //TODO
		ghostinclock =true;
		nbGhost++;
		ghostFigures.add(drawX);	
		precedent = drawX;
		return drawX;
	}
	
	public void setGhostVisible(boolean b)
	{
		for (DrawX f: ghostFigures)
		{
			f.setGhostvisible(b);
			f.mycompute();
			isGhostVisible=b;
		}
	}
} 
