package fr.inria.vcd.view.figure;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.view.VcdFactory;

public class DrawX extends Polygon implements ExtendFigure {

	private boolean ghostvisible;
	private int length;
	int oldvalue = 0;
	Point tr = new Point(0, 0);

	private VcdFactory vcd;

	private int x;

	private int y;

	public DrawX(VcdFactory vcd, int x, int y, int length)
	{
		super();
		this.vcd = vcd;
		this.y = y;
		this.x = x;
		this.length = length;
		ghostvisible = true;
	}

	public int getLength()
	{

		return length;
	}

	public Point getTr()
	{
		return tr;
	}

	public boolean isGhost()
	{
		return true;
	}

	public boolean isGhostvisible()
	{
		return ghostvisible;
	}

	Rectangle rect = null;

	Polyline polyline = null;

	public int mycompute()
	{
		// setValid(false);
		if (ghostvisible)
		{
			setForegroundColor(MyColorApi.colorGhostClock());
			setBackgroundColor(MyColorApi.colorGhostClock());
		} else
		{
			setForegroundColor(MyColorApi.colorClock());
			setBackgroundColor(MyColorApi.colorBlack());
		}
		setLayoutManager(new BorderLayout());
		setOpaque(false);
		setPoints(new PointList());
		// setFill(false);

		int x2 = x + length;
		int mx = x;
		int mx2 = x2;
		if (vcd.getVcdZoom() != null)
		{
			mx = vcd.getVcdZoom().computeposition(x);
			mx2 = vcd.getVcdZoom().computeposition(x2);
		}
		addPoint(new Point(mx, y + vcd.getHeight()).translate(tr));
		if (ghostvisible)
		{
			addPoint(new Point(mx, y).translate(tr));
			addPoint(new Point(mx2, y).translate(tr));
		}
		addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
		if (futurLevel==Level.z)
		{
			addPoint(new Point(mx2, y + vcd.getHeight_2()).translate(tr));
			addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
		}
		setFill(true);
		// setValid(true);
		if (ghostvisible)
		{
			if (polyline != null)
				remove(polyline);
			polyline = new Polyline();
			polyline.setPoints(new PointList());
			polyline.setForegroundColor(MyColorApi.colorClock());
			polyline.addPoint(new Point(mx, y + vcd.getHeight()).translate(tr));
			polyline.addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));		
			polyline.setOpaque(false);
			add(polyline);
		}		
		return 0;
	}

	public void setGhostvisible( boolean ghostvisible )
	{
		this.ghostvisible = ghostvisible;
	
	}

	public void setLength( int length )
	{
		this.length = length;

	}

	public void setoldValue( int n )
	{
		oldvalue = n;

	}

	public void setTr( Point tr )
	{
		this.tr = tr;
	}

	public Level getLevel()
	{
		return Level.x;
	}

	/**
	 * @return the precedeLevel
	 */
	public final Level getPrecedeLevel()
	{
		return precedeLevel;
	}

	/**
	 * @param precedeLevel
	 *            the precedeLevel to set
	 */
	public final void setPrecedeLevel( Level precedeLevel )
	{
		this.precedeLevel = precedeLevel;
	}

	/**
	 * @return the futurLevel
	 */
	public final Level getFuturLevel()
	{
		return futurLevel;
	}

	/**
	 * @param futurLevel
	 *            the futurLevel to set
	 */
	public final void setFuturLevel( Level futurLevel )
	{
		this.futurLevel = futurLevel;
	}

	Level precedeLevel;
	Level futurLevel;

}
