package fr.inria.vcd.view.figure;

import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.view.VcdFactory;

public class Draw1 extends PolylineConnection implements ExtendFigure {

	private int length;
	int oldvalue = 0;
	Point tr = new Point(0, 0);
	private final VcdFactory vcd;

	private final int x;

	private final int y;

	public Draw1(VcdFactory vcd, int x, int y, int length)
	{
		super();
		this.vcd = vcd;
		this.x = x;
		this.y = y;
		this.length = length;
	}

	public int getLength()
	{

		return length;
	}

	public Point getTr()
	{
		return tr;
	}

	public boolean isGhost()
	{
		return false;
	}

	public int mycompute()
	{

		double zoom = 1.0;

		// PointList pl = getPoints();

		int begin = 0;
		// if (pl!=null)
		// begin= pl.getPoint(0).getDifference(new Point(0, 0)).height;

		setPoints(new PointList());
		removeAllPoints();
		setOpaque(false);
		setForegroundColor(MyColorApi.colorClock());
		int mx = (int) (x * zoom);
		if (vcd.getVcdZoom() != null)
		{
			mx = vcd.getVcdZoom().computeposition(x);
		}
		if (precedeLevel == Level.l0)
			addPoint(new Point(mx, begin + y + vcd.getHeight()).translate(tr));
		if (precedeLevel==Level.z)
			addPoint(new Point(mx, y + vcd.getHeight_2()).translate(tr));
	
		addPoint(new Point(mx, begin + y).translate(tr));
		int x2 = x + length;
		int mx2 = (int) (x2 * zoom);
		if (vcd.getVcdZoom() != null)
		{
			mx2 = vcd.getVcdZoom().computeposition(x2);
		}  
		
		addPoint(new Point(mx2, begin + y).translate(tr));
	//	
		if (futurLevel==Level.z)
			addPoint(new Point(mx2, y + vcd.getHeight_2()).translate(tr));
		else
			addPoint(new Point(mx2, begin + y + vcd.getHeight()).translate(tr));
		// MyManager.println(x + " " + x2 + " " + y + " " + begin + " " +
		// vcd.getHeight());
		return 0;
	}

	public void setLength( int length )
	{
		this.length = length;

	}

	public void setoldValue( int n )
	{
		oldvalue = n;

	}

	public void setTr( Point tr )
	{
		this.tr = tr;
	}

	public Level getLevel()
	{
		return Level.l1;
	}

	/**
	 * @return the precedeLevel
	 */
	public final Level getPrecedeLevel()
	{
		return precedeLevel;
	}

	/**
	 * @param precedeLevel
	 *            the precedeLevel to set
	 */
	public final void setPrecedeLevel( Level precedeLevel )
	{
		this.precedeLevel = precedeLevel;
	}

	/**
	 * @return the futurLevel
	 */
	public final Level getFuturLevel()
	{
		return futurLevel;
	}

	/**
	 * @param futurLevel
	 *            the futurLevel to set
	 */
	public final void setFuturLevel( Level futurLevel )
	{
		this.futurLevel = futurLevel;
	}

	Level precedeLevel;
	Level futurLevel;
}
