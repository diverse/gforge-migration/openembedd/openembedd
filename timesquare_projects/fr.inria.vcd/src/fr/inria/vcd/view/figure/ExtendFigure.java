package fr.inria.vcd.view.figure;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;

public interface ExtendFigure extends IFigure {
	public enum Level {
		z("z", 0), u("u", 1), x("x", 2), l1("l1", 3), l0("l0", 4), time("time", 5), lnull("lnull",
				6);
		int n;
		String s;

		private Level(String arg0, int arg1)
		{
			s = arg0;
			n = arg1;
		}
	};

	public int getLength();

	public Point getTr();

	public boolean isGhost();

	public int mycompute();

	public void setLength( int length );

	public void setoldValue( int n );

	public void setTr( Point tr );

	public Level getLevel();

	public Level getPrecedeLevel();

	public Level getFuturLevel();

	public void setPrecedeLevel( Level l );

	public void setFuturLevel( Level l );
}
