package fr.inria.vcd.view.figure;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.vcd.model.MyColorApi;
import fr.inria.vcd.view.VcdFactory;

public class DrawTimer extends Polygon implements ExtendFigure {

	private String labelvalue = "Active";
	private int length;
	int oldvalue = 0;
	Point tr = new Point(0, 0);
	private VcdFactory vcd;

	private int x;

	private int y;

	public DrawTimer(VcdFactory vcd, int x, int y, int length, String label)
	{
		super();
		this.vcd = vcd;
		this.x = x;
		this.y = y;
		this.length = length;
		labelvalue = label;
	}

	public String getLabelvalue()
	{
		return labelvalue;
	}

	public int getLength()
	{

		return length;
	}

	public Point getTr()
	{
		return tr;
	}

	public boolean isGhost()
	{
		return false;
	}

	Label label = null;

	public int mycompute()
	{
		// TODO
		double zoom = 1.0;
		int x2 = x + length;
		int mx = x;
		int mx2 = x2;
		if (vcd.getVcdZoom() != null)
		{
			mx = vcd.getVcdZoom().computeposition(x);
			mx2 = vcd.getVcdZoom().computeposition(x2);
		}
		setPoints(new PointList());
		setLayoutManager(new BorderLayout());
		setOpaque(false);
		setForegroundColor(MyColorApi.colorTimer());
		addPoint(new Point(mx + 2, y + vcd.getHeight()).translate(tr));
		addPoint(new Point(mx, y + vcd.getHeight_2()).translate(tr));
		addPoint(new Point(mx + 2, y).translate(tr));

		addPoint(new Point(mx2 - 2, y).translate(tr));
		addPoint(new Point(mx2, y + vcd.getHeight_2()).translate(tr));
		addPoint(new Point(mx2 - 2, y + vcd.getHeight()).translate(tr));
		if (label == null)
		{
			label = new Label("Active");
			// remove(label);
		}

		add(label, BorderLayout.CENTER);

		label.setBounds(new Rectangle(new Point(mx, y).translate(tr), new Dimension(
				(int) (length * zoom), vcd.getHeight())));

		return 0;
	}

	public void setLabelvalue( String labelvalue )
	{
		this.labelvalue = labelvalue;
	}

	public void setLength( int length )
	{
		this.length = length;

	}

	public void setoldValue( int n )
	{
		oldvalue = n;

	}

	public void setTr( Point tr )
	{
		this.tr = tr;
	}

	public Level getLevel()
	{
		return Level.time;
	}

	/**
	 * @return the precedeLevel
	 */
	public final Level getPrecedeLevel()
	{
		return precedeLevel;
	}

	/**
	 * @param precedeLevel
	 *            the precedeLevel to set
	 */
	public final void setPrecedeLevel( Level precedeLevel )
	{
		this.precedeLevel = precedeLevel;
	}

	/**
	 * @return the futurLevel
	 */
	public final Level getFuturLevel()
	{
		return futurLevel;
	}

	/**
	 * @param futurLevel
	 *            the futurLevel to set
	 */
	public final void setFuturLevel( Level futurLevel )
	{
		this.futurLevel = futurLevel;
	}

	Level precedeLevel;
	Level futurLevel;

}
