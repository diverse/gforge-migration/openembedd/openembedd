package fr.inria.vcd.preferences;

import java.util.ArrayList;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.Color;

import fr.inria.vcd.VcdActivator;

public class ColorSelection {

	public class ColorData 
	{
		public String name;
		public String title;
		public String defvalue;	
		public ColorData(String name, String title, String defvalue)
		{
			super();
			this.name = name;
			this.title = title;
			this.defvalue = defvalue;
		}	
		
		public Color getColor()
		{
			if (store!=null)
				return new Color(null,PreferenceConverter.getColor(store, name));
			try
			{
				
				int r=Integer.parseInt(defvalue.substring(0,defvalue.indexOf(",")));
				String s=defvalue.substring(defvalue.indexOf(",")+1);
				int g=Integer.parseInt(s.substring(0,s.indexOf(",")));
				s=s.substring(s.indexOf(",")+1);
				int b=Integer.parseInt(s);			
				Color c=new Color(null,r,g,b);
				return c;
			}
			catch(Exception e)
			{ 
				e.printStackTrace();
				
			}
			return new Color(null,0,0,0);
		}
	}
	
	private ArrayList<ColorData> arrayColor = new ArrayList<ColorData>();

	private ColorSelection()
	{
		super();
		if (VcdActivator.getDefault()!=null)
			store = VcdActivator.getDefault().getPreferenceStore();	
		arrayColor.add(0, new ColorData("background","background","0,0,0" ));
		arrayColor.add(1, new ColorData("text","text","255,255,255" ));
		arrayColor.add(2, new ColorData("clock","clocks","0,255,0" ));
		arrayColor.add(3, new ColorData("ghost","ghosts","0,127,0" ));
		arrayColor.add(4, new ColorData("time","time scale","255,0,0" ));
		arrayColor.add(5, new ColorData("whiteArrow","precedence","255,255,255" ));
		arrayColor.add(6, new ColorData("coincidence","coincidence","255,0,0" ));
		
	}
	
	IPreferenceStore store =null;
	static ColorSelection c;
	public static ColorSelection getDefault()
	{
		if (c==null)
		{
			c=new ColorSelection();			
		}
		return c;
	}

	public void init()
	{			
		if (store!=null)
		{
		for (ColorData cd :  arrayColor )
		{
			store.setDefault(cd.name, cd.defvalue);
		}
		store.setDefault(PreferenceConstants.P_namesize, 150);
		}
	}
	
	public Color convertColor(String s)
	{
		return new Color (null, PreferenceConverter.getColor(store, s));
	}
	
	public int getWidthName()
	{
		return store.getInt(PreferenceConstants.P_namesize);	
	}
	
	public Color convertColor(int n)
	{
		if (n>=arrayColor.size())
			return new Color(null,0,0,0);
		return arrayColor.get(n).getColor();
	}

	public void setArrayColor( ArrayList<ColorData> arrayColor )
	{
		this.arrayColor = arrayColor;
	}

	public ArrayList<ColorData> getArrayColor()
	{
		return arrayColor;
	}
	
}
