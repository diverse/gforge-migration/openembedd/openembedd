package fr.inria.vcd.preferences;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import fr.inria.vcd.VcdActivator;
import fr.inria.vcd.preferences.ColorSelection.ColorData;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class VCDeditorPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public VCDeditorPreferencePage() {
		super(GRID);
		setPreferenceStore(VcdActivator.getDefault().getPreferenceStore());
		setDescription("VCD editor : Select  Color");
	
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */

	
	public void createFieldEditors() {
		
		for (ColorData cd :  ColorSelection.getDefault().getArrayColor() )
		{			
			addField( new ColorFieldEditor(cd.name,cd.title, getFieldEditorParent())  );
		}
		IntegerFieldEditor ife=new IntegerFieldEditor(PreferenceConstants.P_namesize ,"Width of the column name",
				getFieldEditorParent());
		ife.setValidRange(115, 300);
		addField( ife);
	}
	

	
	
	
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}