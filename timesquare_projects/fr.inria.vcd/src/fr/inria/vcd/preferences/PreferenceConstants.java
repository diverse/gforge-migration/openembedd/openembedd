package fr.inria.vcd.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_PATH = "xpathPreference";

	public static final String P_BOOLEAN = "wbooleanPreference";

	public static final String P_CHOICE = "wchoicePreference";

	public static final String P_STRING = "xtest";
	public static final String P_STRING2 = "xcolor";
	public static final String P_STRING3 = "xcolor1";
	public static final String P_STRING4 = "xcolor2";
	public static final String P_STRING5 = "xcolor3";
	public static final String P_namesize = "namesize";
	
}
