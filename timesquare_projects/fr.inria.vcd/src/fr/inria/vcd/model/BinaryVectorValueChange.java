package fr.inria.vcd.model;

public final class BinaryVectorValueChange extends VectorValueChange {
	private String value;
	private Value[] values;
	public BinaryVectorValueChange(String value, String ident) {
		super('b', ident);
		check(value.toLowerCase());
	}
	@Override
	public void setValue(Object value){
	}
	private void check(String value) {
		char[] buf = value.toCharArray();
		this.values = new Value[buf.length];
		for(int i =0; i<buf.length; i++)
			this.values[i]=Value.fromChar(buf[i]);
		this.value = value;
	}

	@Override
	public Object getValue() {
		return this.value;
	}

}
