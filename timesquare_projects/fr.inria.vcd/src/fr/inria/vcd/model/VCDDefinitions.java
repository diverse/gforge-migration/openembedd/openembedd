package fr.inria.vcd.model;

import java.util.ArrayList;

import fr.inria.vcd.model.command.ScopeCommand;
import fr.inria.vcd.model.command.TimeScaleCommand;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.model.comment.StatusCommentCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;
import fr.inria.vcd.model.visitor.IScopeVisitor;
import fr.inria.vcd.model.visitor.ISimulationVisitor;
import fr.inria.vcd.model.visitor.IVcdVisitor;
import fr.inria.vcd.view.VcdFactory;

final public class VCDDefinitions {
	
	public VCDDefinitions()
	{
		super();
		
	}

	
	private ScopeCommand scope = null;
	private ArrayList<IDeclarationCommand> decls = new ArrayList<IDeclarationCommand>();
	private int pulse = -1;
	private  ArrayList<ISimulationCommand> sims = new ArrayList<ISimulationCommand>();
	private int offset = -1;
	
	
	
	public void setScope(ScopeCommand scope) {
		this.scope = scope;
	}
	
	public void addDefinition(IDeclarationCommand decl) {
		this.decls.add(decl);
		if (decl instanceof ConstraintCommentCommand)
		{
			if (((ConstraintCommentCommand) decl).getIc()!=null)
			{
				if (factory!=null)
					((ConstraintCommentCommand) decl).getIc().setVcdFactory(factory);
			}
		}
		
	}
	
	VcdFactory factory=null;
	
	/**
	 * @param factory the factory to set
	 */
	public final void setFactory( VcdFactory factory )
	{
		this.factory = factory;
	}

	public void addSimulation(ISimulationCommand sim) {
		this.sims.add(sim);
	}
	public Object visit(IVcdVisitor vcd) {
		IDeclarationVisitor dVisitor = vcd.getDeclarationVisitor();
		if (dVisitor != null) 
			visit(dVisitor);
		
		IScopeVisitor scVisitor = vcd.getScopeVisitor();
		if (scVisitor != null)
			visit(scVisitor);

		if (dVisitor != null)
			dVisitor.visitEndDefinitions();			

		ISimulationVisitor siVisitor = vcd.getSimulationVisitor();
		if (siVisitor != null)
			visit(siVisitor);

		return vcd;
	}
	public void visit(IDeclarationVisitor visitor) {
		for(IDeclarationCommand dc : decls)
			dc.visit(visitor);		
	}
	public void visit(IScopeVisitor visitor) {
		if (scope==null) throw new RuntimeException("VCD without scope");
		scope.visit(visitor);
	}
	public void visit(ISimulationVisitor visitor) {
		for(ISimulationCommand sc : sims) {
			sc.visit(visitor);
		}
		visitor.endSimulation();
	}

	public void visit(ISimulationVisitor visitor, int n1, int n2,boolean  b) {
		int n=sims.size();
		
		n2= n2< n ? n2 : n;
		for(int i=n1;i<n2;i++) {
			sims.get(i).visit(visitor);
		}
		if (b)
			visitor.endSimulation();
	}
	
	public ArrayList<StatusCommentCommand> getStatusCommentCommand() {
		ArrayList<StatusCommentCommand> commands = new ArrayList<StatusCommentCommand>();
		for(IDeclarationCommand cc : decls){
			if(cc instanceof StatusCommentCommand)
				commands.add((StatusCommentCommand)cc);
		}
		return commands;
	}
	
	public ArrayList<ConstraintCommentCommand> getConstraintCommentCommand() {
		ArrayList<ConstraintCommentCommand> commands = new ArrayList<ConstraintCommentCommand>();
		for(IDeclarationCommand cc : decls){
			if(cc instanceof ConstraintCommentCommand)
				commands.add((ConstraintCommentCommand)cc);
		}
		return commands;
	}
	
	public TimeScaleCommand getTimeScaleCommand() {
		for(IDeclarationCommand cc : decls){
			if(cc instanceof TimeScaleCommand)
				return (TimeScaleCommand)cc;
		}
		return null;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the sims
	 */
	public ArrayList<IDeclarationCommand> getDecls()
	{
		return decls;
	}

	/**
	 * @return the sims
	 */
	public ArrayList<ISimulationCommand> getSims()
	{
		return sims;
	}
	
	
}
