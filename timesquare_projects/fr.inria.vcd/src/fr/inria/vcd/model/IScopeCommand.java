package fr.inria.vcd.model;

import fr.inria.vcd.model.visitor.IScopeVisitor;

public interface IScopeCommand {
	public void visit(IScopeVisitor visitor);
}
