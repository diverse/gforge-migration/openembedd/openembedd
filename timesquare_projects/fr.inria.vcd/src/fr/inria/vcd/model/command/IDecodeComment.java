package fr.inria.vcd.model.command;

import fr.inria.vcd.model.ICommemtCommand;

public interface IDecodeComment 
{
	
	public ICommemtCommand create(String st[] , String comment, int time );
}
