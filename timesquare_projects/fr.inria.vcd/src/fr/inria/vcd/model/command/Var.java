package fr.inria.vcd.model.command;

import fr.inria.vcd.model.IVar;
import fr.inria.vcd.view.VcdValueFactory;

final public class Var implements IVar {
    private VarCommand varC;
    private String parent;
    
	public Var(VarCommand varC, String parent) {
		super();
		this.varC = varC;
		this.parent = parent;
	}

	
	public String getName() {
		return varC.getName();
	}

	
	public String getQualifiedName() {
		return parent+":"+this.getName();
	}

	
	public int getSize() {
		return varC.getSize();
	}

	@Override	
	public String toString() {
		return "var "+getSize()+" "+getQualifiedName();
	}

	
	public String getIdentiferCode() {
		return varC.getIdentifierCode();
	}
	
	private VcdValueFactory vvf;
	
	
	public VcdValueFactory getValueFactory()
	{
		return vvf;
	}
	
	
	public void setValueFactory(VcdValueFactory vin)
	{
		vvf=vin;
		
	}

	private boolean visibleByDefault=true;

	public boolean isVisibleByDefault()
	{
		
		return visibleByDefault;
	}


	public void setVisibleByDefault( boolean f )
	{		
		visibleByDefault=f;
	}
	
	
	
}
