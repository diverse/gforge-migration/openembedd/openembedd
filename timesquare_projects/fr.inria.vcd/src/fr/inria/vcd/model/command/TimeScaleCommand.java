package fr.inria.vcd.model.command;

import fr.inria.vcd.model.IDeclarationCommand;
import fr.inria.vcd.model.keyword.TimeUnit;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;

final public class TimeScaleCommand implements IDeclarationCommand {
	private int number;
	private TimeUnit unit;
	
	public TimeScaleCommand(String number, String unit) {
		try
		{
		this.number = Integer.parseInt(number);
		this.unit = TimeUnit.valueOf(unit);
		} 
		catch ( Exception ex)		
		{
			this.number=1;
			this.unit= TimeUnit.s;
		}
	}
	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitTimeScale(number, unit);
	}
	public int getNumber() {
		return number;
	}
	public TimeUnit getUnit() {
		return unit;
	}
}
