package fr.inria.vcd.model.command;

import java.util.ArrayList;
import java.util.Iterator;

import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.keyword.SimulationKeyword;
import fr.inria.vcd.model.visitor.ISimulationVisitor;

public class SimulationCommand implements fr.inria.vcd.model.ISimulationCommand {
	private ArrayList<ValueChange> changes = new ArrayList<ValueChange>();
	private int time;
	private SimulationKeyword keyword; // null if normal dump

	public SimulationCommand(int time) {
		this.time = time;
		this.keyword = null;	
	}

	public SimulationCommand(SimulationKeyword keyword) {
		this.time = 0;		//TODO
		this.keyword = keyword;
	}

	public void merge(SimulationCommand other) {
		this.keyword = other.keyword;
		this.changes = other.changes;		
	}

	public void addValueChange(ValueChange vc) {
		changes.add(vc);
	}


	public Iterator<ValueChange> iterator() {
		return changes.iterator();
	}

	
	public void visit(ISimulationVisitor visitor) {
		visitor.visitTime(time);
		visitor.visitKeyword(keyword);
		for (ValueChange vc : changes)
			visitor.visitValueChange(vc);
		visitor.visitEnd(keyword);
	}

	public int getTime() {
		return time;
	} 
	
	
}
