package fr.inria.vcd.model.command;

import java.util.Date;

import fr.inria.vcd.model.IDeclarationCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;

final public class DateCommand implements IDeclarationCommand {
	private String date;
	
	public DateCommand(String date) {
		this.date = date;
	}
	public DateCommand() {
		this(new Date().toString());
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitDate(date);
	}
}
