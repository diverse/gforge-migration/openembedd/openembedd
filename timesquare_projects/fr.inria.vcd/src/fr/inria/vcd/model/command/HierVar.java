package fr.inria.vcd.model.command;

import fr.inria.vcd.model.IVar;
import fr.inria.vcd.view.VcdValueFactory;

final public class HierVar implements IVar {
	private IVar child;
	private String parent;
	
	public HierVar(IVar child, String parent) {
		super();
		this.child = child;
		this.parent = parent;
	}

	
	public String getName() {
		return child.getName();
	}

	
	public String getQualifiedName() {
		return parent+":"+child.getQualifiedName();
	}

	
	public int getSize() {
		return child.getSize();
	}
	@Override
	public String toString() {
		return "var "+getSize()+" "+getQualifiedName();
	}

	
	public String getIdentiferCode() {
		return this.child.getIdentiferCode();
	}
	

	public VcdValueFactory getValueFactory()
	{
		return null;
	}
	
	
	public void setValueFactory(VcdValueFactory v)
	{
		
		
	}

	
	public boolean isVisibleByDefault()
	{
	
		return true;
	}

	
	public void setVisibleByDefault( boolean f )
	{
		
		//TODO setter
	}
	
	
		
}
