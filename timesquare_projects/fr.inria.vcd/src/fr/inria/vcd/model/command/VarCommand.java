package fr.inria.vcd.model.command;

import fr.inria.vcd.model.IScopeCommand;
import fr.inria.vcd.model.keyword.VarType;
import fr.inria.vcd.model.visitor.IScopeVisitor;

final public class VarCommand implements IScopeCommand {
	private VarType type;
	private int size;
	private String ident, ref;
	
	public VarCommand(VarType type, int size, String ident, String ref) {
		this.type = type;
		this.size = size;
		this.ident = ident;
		this.ref = ref;
	}

	public String getName() {
		return this.ref;
	}

	public int getSize() {
		return this.size;
	}

	
	public void visit(IScopeVisitor visitor) {
		visitor.visitVar(this, type, ident);		
	}
	
	public String getIdentifierCode() {
		return this.ident;
	}
}
