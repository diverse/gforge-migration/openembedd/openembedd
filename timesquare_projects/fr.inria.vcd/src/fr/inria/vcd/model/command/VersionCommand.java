package fr.inria.vcd.model.command;

import fr.inria.vcd.model.IDeclarationCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;

final public class VersionCommand implements IDeclarationCommand {
	private String text, task;
	
	public VersionCommand(String text, String task) {
		this.text = text;
		this.task = task;
	}
	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitVersion(text, task);
	}
}
