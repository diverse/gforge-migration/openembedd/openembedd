package fr.inria.vcd.model.command;

import java.util.ArrayList;

import fr.inria.vcd.model.IScopeCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.visitor.IScopeVisitor;

public class ScopeCommand implements IScopeCommand {
	private ScopeType type;
	private String name;
	private ArrayList<IScopeCommand> children;
	
	public ScopeCommand(ScopeType type, String name) {
		super();
		this.type = type;
		this.name = name;
		this.children = new ArrayList<IScopeCommand>();
	}

	public void addChild(IScopeCommand child) {
		this.children.add(child);
	}

	public void visit(IScopeVisitor visitor) {
		visitor.visitScope(type, name);
		for(IScopeCommand sc : children)
			sc.visit(visitor);
		visitor.visitUpscope();
	}
}
