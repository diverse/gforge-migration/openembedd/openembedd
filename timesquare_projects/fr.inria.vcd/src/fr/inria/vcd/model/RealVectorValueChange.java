package fr.inria.vcd.model;

public final class RealVectorValueChange extends VectorValueChange {
	private double value;
	
	public RealVectorValueChange(String value, String ident) {
		this(Double.valueOf(value), ident);
	}
	public RealVectorValueChange(double value, String ident) {
		super('r', ident);
		this.value = value;	
	}
	@Override
	public Object getValue() {
		return this.value;
	}

	@Override
	public void setValue(Object value){
	}
}
