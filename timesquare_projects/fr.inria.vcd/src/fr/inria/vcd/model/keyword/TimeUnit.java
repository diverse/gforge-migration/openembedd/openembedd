package fr.inria.vcd.model.keyword;

public enum TimeUnit {
	s, ms, us, ns, ps, fs, tick ;
}
