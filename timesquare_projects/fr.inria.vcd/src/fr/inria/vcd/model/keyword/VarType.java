package fr.inria.vcd.model.keyword;

public enum VarType {
	event, integer, parameter, real, realtime, reg, supply0, supply1, time, 
	tri, triand, trior, trireg, tri0, tri1, wand, wire, wor;
}
