package fr.inria.vcd.model.keyword;

public enum DeclarationKeyword {
	comment, date, enddefinitions, scope, 
	timescale, upscope, var, version;
}
