package fr.inria.vcd.model.keyword;

public enum SimulationKeyword {
  dumpall, dumpoff, dumpon, dumpvars;
}
