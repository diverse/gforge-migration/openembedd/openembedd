package fr.inria.vcd.model.keyword;

public enum ScopeType {
	begin, fork, function, module, task;
}
