package fr.inria.vcd.model.comment;

import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.command.IDecodeComment;

public class StatusDecodeComment implements IDecodeComment {

	
	
	public StatusDecodeComment()
	{
		super();
		
	}

	
	public ICommemtCommand create( String[] st , String comment, int time  )
	{
		if (st.length<3) return null;
		String func = new String();
		String ident = new String();
		func = st[1];
		ident = st[2];
		return new StatusCommentCommand(func, ident, String.valueOf(time));		
	}

}
