package fr.inria.vcd.model.comment;

import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.command.IDecodeComment;

public class HideDecodeComment implements IDecodeComment {

	
	
	public HideDecodeComment()
	{
		super();	
	}

	
	public ICommemtCommand create( String[] st , String comment , int time )
	{
		if (st.length<2) return null;	
		return new HideCommand(st[1]);		
	}
	
	public static  HideCommand createHide(String clock)	
	{
		
		HideCommand hc = new HideCommand(clock);		
		return hc; 
	
	}
	
	

}
