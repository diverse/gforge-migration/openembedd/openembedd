package fr.inria.vcd.model.comment;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.command.IDecodeComment;
import fr.inria.vcd.view.constraint.ConstraintAlternates;
import fr.inria.vcd.view.constraint.ConstraintFilteredBy;
import fr.inria.vcd.view.constraint.ConstraintSampledOn;
import fr.inria.vcd.view.constraint.ConstraintSustains;
import fr.inria.vcd.view.constraint.ConstraintSync;
import fr.inria.vcd.view.constraint.IConstraint;

public class ConstraintDecodeComment implements IDecodeComment {

	
	public ConstraintDecodeComment()
	{
		super();		
	}

	
	public ConstraintCommentCommand create( String[] st , String commentin , int time )
	{
		try
		{
		IConstraint ic=null;
		String comment = commentin.substring(10);
		String clock = new String();
		Function function = null;
		ArrayList<String> clocks = new ArrayList<String>();
		//ArrayList<Integer> number = new ArrayList<Integer>();
		if (st.length <=2)
		{
			return null;
		}
		
		if (st[2].toLowerCase().equals(
				Function._alternateswith.toString())) {
			function = Function._alternateswith;
			clock = st[1];
			clocks.add(st[3]);
			ic =  new ConstraintAlternates();
		}
		if (st[2].toLowerCase().equals(
				Function._isperiodicon.toString())  ||  (st[2].toLowerCase().equals("periodicon"))) {
			function = Function._isperiodicon;
			clock = st[1];
			clocks.add(st[3]);
			ic = new ConstraintFilteredBy();
		}
		
		if (st[3].toLowerCase().equals(
				Function._alternateswith.toString())) {
			function = Function._alternateswith;
			clock = st[1];
			clocks.add(st[4]);
			ic =  new ConstraintAlternates();
		}

		if (st[3].toLowerCase().equals(Function._sustains.toString())) {
			function = Function._sustains;
			clock = st[1];
			clocks.add(st[4]);
			clocks.add(st[6]);
			ic=new ConstraintSustains();
		}
		if (st[2].toLowerCase().equals(Function._precedes.toString())) {
			function = Function._precedes;
			clock = st[1];
			clocks.add(st[3]);
			clocks.add(st[5]);
			clocks.add(st[6]);
			//clocks.add(st[5]);
		}
		if (st[3].toLowerCase().equals(Function._precedes.toString())) {
			function = Function._precedes;
			clock = st[1];
			clocks.add(st[4]);
			clocks.add(st[6]);
			clocks.add(st[7]);
			//clocks.add(st[6]);
		}
		
		if (st.length >= 5) {
			if (st[4].toLowerCase().equals(
					Function._sampledon.toString())) {
				function = Function._sampledon;
				clock = st[1];
				clocks.add(st[3]);
				clocks.add(st[5]);
				ic =  new ConstraintSampledOn();
			}
			if (st[3].toLowerCase().equals(
					Function._oneshoton.toString())) {
				function = Function._oneshoton;
				clock = st[1];
				clocks.add(st[4]);
				clocks.add(st[6]);
				clocks.add(st[8]); 
				clocks.add(st[10]); //TODO correction
			}
			if (st[4].toLowerCase().equals(
					Function._restrictedto.toString())) {
				function = Function._restrictedto;
				clock = st[1];
				clocks.add(st[3]);
			}
			
			if (st[4].toLowerCase().equals(
					Function._filteredby.toString())) {
				function = Function._filteredby;
				clock = st[1];
				clocks.add(st[3]);
				ic =  new ConstraintFilteredBy();
			}

			if (st[4].toLowerCase().equals(
					Function._delayedfor.toString())) {
				function = Function._delayedfor;
				clock = st[1];
				clocks.add(st[3]);
				// clocks.add(st[5]);
				clocks.add(st[7]);
			}
			if (st[4].toLowerCase().equals(
					Function._synchronizeswith.toString())) {
				function = Function._synchronizeswith;
				clock = st[1];
				clocks.add(st[5]);
				clocks.add(st[3]);
				clocks.add(st[7]);
				ic =  new ConstraintSync();
			}

		}
		if (st.length >= 6) {
			if (st[5].toLowerCase().equals(
					Function._sampledon.toString())) {
				function = Function._sampledon;
				clock = st[1];
				clocks.add(st[3]);
				clocks.add(st[6]);
				ic =  new ConstraintSampledOn();
				((ConstraintSampledOn )ic).setWeakly( !st[4].toLowerCase().equals("strictly"));
			}
			if (st[5].toLowerCase().equals(
					Function._synchronizeswith.toString())) {
				function = Function._synchronizeswith;
				clock = st[1];
				clocks.add(st[6]);
				clocks.add(st[3]);
				clocks.add(st[8]);
				ic =  new ConstraintSync();
			}
		}
		if (function != null)
		{
			ConstraintCommentCommand cc=new ConstraintCommentCommand(comment,clock,function,clocks);
			cc.setIc(ic);
			if (ic!=null)
				ic.setCc(cc);
			return  cc;
		}
		}
		catch (Exception ex) {
			
		}
		return null;
	}  
 
	
	public static ConstraintCommentCommand filteredBy(String superc, String sub, String bw)
	{
		IConstraint ic=null;		
		String clock = new String();
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = sub +" = " + superc +" filteredBy " +bw ; 
		clock = sub;
		clocks.add(superc);
		ic =  new ConstraintFilteredBy();
		ConstraintCommentCommand cc=new ConstraintCommentCommand(comment,clock,Function._filteredby,clocks);
		cc.setIc(ic);
		ic.setCc(cc);
		return  cc;
	}
	
	
	public static  ConstraintCommentCommand synchronize(String left, String right, int a, int b)
	{
		IConstraint ic=null;		
		String clock = new String();
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = left +" by " + a +" synchronizeswith " +right + " by " + b ; 
		clock = left;		
		clocks.add(right);
		clocks.add(""+a);
		clocks.add(""+b);
		ic =  new ConstraintSync();
		ConstraintCommentCommand cc=new ConstraintCommentCommand(comment,clock,Function._synchronizeswith,clocks);
		cc.setIc(ic);
		ic.setCc(cc);
		return  cc;
	}
	 
	
	public static  ConstraintCommentCommand isperiodicOn(String left, String right)
	{
		IConstraint ic=null;		
		String clock = new String();
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = left +" isPeriodicOn " +right  ; 
		clock = left;		
		clocks.add(right);		
		ic =  new ConstraintFilteredBy();
		ConstraintCommentCommand cc=new ConstraintCommentCommand(comment,clock,Function._isperiodicon,clocks);
		cc.setIc(ic);
		ic.setCc(cc);
		return  cc;
	}
	
	
	public static ConstraintCommentCommand sampledOn(String superc, String sub, String trig , boolean weakly) //TODO
	{
		ConstraintSampledOn ic=null;		
		String clock = new String();
		ArrayList<String> clocks = new ArrayList<String>();
		String mod= weakly ? " weakly " : " strictly " ;
 		String comment = sub +" = " + superc +mod +" sampledOn " +trig ; 
		clock = sub;
		clocks.add(superc);
		ic =  new ConstraintSampledOn();
		ic.setWeakly(weakly);
		ConstraintCommentCommand cc=new ConstraintCommentCommand(comment,clock,Function._filteredby,clocks);
		cc.setIc(ic);
		ic.setCc(cc);
		return  cc;
	}
}
