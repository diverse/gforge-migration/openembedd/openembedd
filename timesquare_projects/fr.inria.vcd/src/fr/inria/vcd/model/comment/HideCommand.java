package fr.inria.vcd.model.comment;

import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;

public final class HideCommand implements ICommemtCommand {

	
	private String name;
	
	
	public String getName()
	{
		return name;
	}


	public HideCommand(String name)
	{
		super();
		this.name = name;
	}


	
	public void visit( IDeclarationVisitor visitor )
	{
		
	}


	@Override
	public String toString()
	{
		return "Hide Clock :"+ name  ;   
	}


	public boolean isActive() {
	
		return false;
	}


	public void setActive(boolean active) {
	
		
	}
	

}
