package fr.inria.vcd.model.comment;

import java.util.HashMap;

import fr.inria.vcd.model.command.IDecodeComment;

public class DecodeDictionary {

	private HashMap<String , IDecodeComment> hmsdc = new HashMap<String, IDecodeComment>();
	
	private static DecodeDictionary decode ;

	/**
	 * @return the decode
	 */
	public static final DecodeDictionary getDefault()
	{
		if (decode==null)
			decode= new DecodeDictionary();
		return decode;
	}

	private DecodeDictionary()
	{
		super();
		hmsdc.put("hide" , new HideDecodeComment());
		hmsdc.put("status" , new StatusDecodeComment());
		hmsdc.put("constraint" , new ConstraintDecodeComment());
	}
	
	public IDecodeComment getDecoder(String s)
	{
		return hmsdc.get(s.toLowerCase());
	}
	
	
}
