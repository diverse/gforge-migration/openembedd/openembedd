package fr.inria.vcd.model.comment;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;
import fr.inria.vcd.view.constraint.IConstraint;

final public class ConstraintCommentCommand implements ICommemtCommand {
	private String comment;
	private String clock;
	private Function function;
	private ArrayList<String> referenceClocks;
	private IConstraint ic=null;

	public ConstraintCommentCommand(String comment,String clock,Function function, ArrayList<String> referenceClocks) {
		this.comment = comment;
		this.clock = clock;
		this.function = function;
		this.referenceClocks = referenceClocks;
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment(clock, function, referenceClocks);
	}

	@Override
	public String toString(){
		return comment;
	}

	public String getClock() {
		return clock.trim();
	}

	public ArrayList<String> getReferenceClocks() {
		return referenceClocks;
	}

	public Function getFunction() {
		return function;
	}

	/**
	 * @return the ic
	 */
	public final IConstraint getIc()
	{
		return ic;
	}

	/**
	 * @param ic the ic to set
	 */
	public final void setIc( IConstraint ic )
	{
		this.ic = ic;
	}

	public boolean isActive() {
		return false;
	}

	public void setActive(boolean active) {
	}
	
	
}