package fr.inria.vcd.model.comment;

import fr.inria.vcd.model.ICommemtCommand;
import fr.inria.vcd.model.visitor.IDeclarationVisitor;

final public class StatusCommentCommand implements ICommemtCommand {
	private String func;
	private String ident;
	private String time;

	public StatusCommentCommand(String func, String ident, String time) {
		this.func = func;
		this.ident = ident;
		this.time = time;
	}


	public String getFunc() {
		return func;
	}


	public String getIdent() {
		return ident;
	}


	public String getTime() {
		return time;
	}

	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitStatusComment(func, ident, time);
	}

	
	@Override
	public String toString(){
		return func + " " + ident + " " + time;
	}


	public boolean isActive() {
		
		return false;
	}


	public void setActive(boolean active) {
		
		
	}
}
