package fr.inria.vcd.model;

public interface ICommemtCommand extends IDeclarationCommand {
	
	public boolean isActive();
	public void setActive(boolean active);
}
