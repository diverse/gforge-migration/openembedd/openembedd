package fr.inria.vcd.model;

abstract class VectorValueChange extends ValueChange {
	private char base;
	
	protected VectorValueChange(char base, String ident) {
		super(ident);
		assert(base=='b' || base=='r') : "invalid base";
		this.base = base;
	}

	@Override
	final public String toString() {
		return base+(getValue()+" ")+super.toString();
	}	
}
