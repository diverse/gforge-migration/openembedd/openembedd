package fr.inria.vcd.model;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;

public class Description {
	private String name;
	private int index = -1;
	private String description;
	private ArrayList<IFigure> figures = new ArrayList<IFigure>();
	private int time = -1;

	public Description() {
	}
	
	

	public Description(String name, int index, ArrayList<IFigure> figures, int time) {
		super();
		this.name = name;
		this.index = index;
		this.figures = figures;
		this.time = time;
	}



	public Description(String name, int index, String description,
			ArrayList<IFigure> figures,int time) {
		this.name = name;
		this.index = index;
		this.description = description;
		this.figures = figures;
		this.time = time;
	}
	
	@Override
	public boolean equals(Object obj) {
		Description descr = (Description) obj; 
		if(descr.name.equals(name) && descr.description.equals(description))
			return true;
		
			return false;
	}

	public Description(String name, String description,
			ArrayList<IFigure> figures,int time) {
		this.name = name;
		this.description = description;
		this.figures = figures;
		this.time = time;
	}

	public Description(String name, String description,int time) {
		this.name = name;
		this.description = description;
		this.time = time;
	}

	public Description(String name,int time) {
		super();
		this.name = name;
		this.time = time;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString(){
		return name + " " + description;
	}

	public ArrayList<IFigure> getFigures() {
		return figures;
	}

	public int getIndex() {
		return index;
	}



	public int getTime() {
		return time;
	}
}
