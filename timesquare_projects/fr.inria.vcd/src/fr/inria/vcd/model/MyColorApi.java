/**
 * 
 */
package fr.inria.vcd.model;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import fr.inria.vcd.preferences.ColorSelection;

/**
 * @author bferrero
 *
 */
public class MyColorApi {

	
	
	public static Color colorMenuBackground()
	{
		return ColorConstants.menuBackground ;
	}	
	
		
	public static Color colorBlack()
	{
		return ColorSelection.getDefault().convertColor(0) ;			
	}

	public static Color colorTextToolTip()
	{
		return ColorConstants.black;		
	}
	
	public static Color colorWhiteArrow()
	{
		
		return ColorSelection.getDefault().convertColor(5); // ColorConstants.white;
		
	}
	
	public static Color colorWhiteRule()
	{
		
		return ColorSelection.getDefault().convertColor(1) ;
		
	}
	public static Color colorWhiteText()
	{
		return ColorSelection.getDefault().convertColor(1) ;	
	}
	
	public static Color colorRedRule()
	{
		return ColorConstants.red;
	}
	
	public static Color colorRedFind()	
	{
		return ColorConstants.red;
	}
	
	public static Color colorRedTime()	
	{
		return ColorSelection.getDefault().convertColor(4) ;	//ColorConstants.red;
	}
	
	public static Color colorRedTimeXor()	
	{
		RGB rbg= ColorSelection.getDefault().convertColor(4).getRGB();
		RGB rbg2= ColorSelection.getDefault().convertColor(0).getRGB();		
		return new Color (null, rbg.red ^ rbg2.red,rbg.green ^ rbg2.green ,rbg.blue ^ rbg2.blue ) ;	//ColorConstants.red;
	}
	public static Color colorRedCoincidence()
	{
		return ColorConstants.red;
	}
	
	public static Color colorRedSync()
	{
		return ColorConstants.red;
	}
	
	public static Color colorGhostClock()
	{
		
			return ColorSelection.getDefault().convertColor(3) ;	 
			//ColorConstants.darkGreen;
		
	}
	
	public static Color colorClock()
	{
		
			return ColorSelection.getDefault().convertColor(2) ;	
			//ColorConstants.lightGreen;
		
	}
	
	public static Color colorTimer()
	{
		
			return ColorConstants.lightGreen;
		
	}

	public static Color colorLightGraySync()
	{
		return ColorConstants.lightGray;
	}
	
	public static Color colorLightGrayClockNameContour()
	{
		return ColorConstants.lightGray;
	}	
	
	public static Color colorLightBlueSync() 
	{
		return ColorConstants.lightBlue;
	}
		
	public static Color colorYellowZ()
	{
		
		return ColorConstants.yellow;
		
	}
	
	public static Color colorYellowTooltip()
	{
		
		return ColorConstants.yellow;
		
	}
	
	public static Color colorBluePrecedes()
	{
		return ColorConstants.blue;
	}

	public static Color colorBlueSustain()
	{
		return ColorConstants.blue;
	}

	public static Color colorDarkGreenGhostFireable()
	{
		
			return ColorConstants.darkGreen;
		
	}

	public static Color colorLightGreenFirable()
	{		
			return ColorConstants.lightGreen;		
	}
	
	public final static Color myBrown =new Color(null, 0x80,0x40,0);
}


