package fr.inria.vcd.model;

import fr.inria.vcd.view.VcdValueFactory;

public interface IVar {
	public String getName();
	public String getQualifiedName();
	public int getSize();
	public String getIdentiferCode();
	public VcdValueFactory getValueFactory();
	public void setValueFactory(VcdValueFactory v); 
	public boolean isVisibleByDefault();
	public void setVisibleByDefault(boolean f); 
	
}
