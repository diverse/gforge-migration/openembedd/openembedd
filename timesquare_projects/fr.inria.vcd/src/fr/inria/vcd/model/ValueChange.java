package fr.inria.vcd.model;


public abstract class ValueChange {
	private String identifierCode;	
	protected ValueChange(String ident) {
		this.identifierCode = ident;
	}
	public abstract Object getValue();
	public abstract void setValue(Object value);
	@Override
	public String toString() {
		return identifierCode;
	}
	
	@Override
	final public boolean equals(Object o) {
		if(!(o instanceof ValueChange)) return false;
		if (this == o) return true;
		if(this.identifierCode == ((ValueChange)o).identifierCode) return true;
		return false;
	}
	
	static public ValueChange fromString(String v) {
		v = v.trim();
		int iSpace= v.indexOf(' ');
		if (iSpace==-1) {
			return new ScalarValueChange(Value.fromChar(v.charAt(0)),
										 v.substring(1));
		}
		String value = v.substring(0, iSpace);
		String ident = v.substring(iSpace+1);
		return create(value, ident);
	}
	static private ValueChange create(String value, String code) {
		char c = value.charAt(0);
		value = value.substring(1);
		switch(c) {
		case 'b':case'B': 
			return new BinaryVectorValueChange(value.substring(1), code);
		case 'r':case'R': 
			return new RealVectorValueChange(value.substring(1), code);
		default:
				throw new IllegalArgumentException(value+" "+code);
		}
	}
	final public String getIdentifierCode() {
		return identifierCode;
	}
}
