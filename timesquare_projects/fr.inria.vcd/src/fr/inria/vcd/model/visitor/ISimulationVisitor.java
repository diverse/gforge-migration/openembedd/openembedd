package fr.inria.vcd.model.visitor;

import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.keyword.SimulationKeyword;

public interface ISimulationVisitor {
	void visitTime(int time);
	void visitKeyword(SimulationKeyword keyword);
	void visitValueChange(ValueChange value);
	void visitEnd(SimulationKeyword keyword);
	void endSimulation();
}
