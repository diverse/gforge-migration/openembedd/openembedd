package fr.inria.vcd.model.visitor;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.keyword.TimeUnit;

public interface IDeclarationVisitor {
	void visitDate(String date);
	void visitVersion(String text, String task);
	void visitTimeScale(int number, TimeUnit unit);
	void visitEndDefinitions();
	void visitStatusComment(String func, String ident,String time);
	void visitConstraintComment(String clock, Function function, ArrayList<String> referenceClocks);
}