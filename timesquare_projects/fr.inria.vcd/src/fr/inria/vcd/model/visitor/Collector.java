package fr.inria.vcd.model.visitor;

import org.eclipse.swt.widgets.Shell;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.VCDDefinitions;
import fr.inria.vcd.view.Iupdate;
import fr.inria.vcd.view.VcdDiagram;
import fr.inria.vcd.view.VcdFactory;

public class Collector  {

	private TraceCollector tc ;
	private VCDDefinitions vcd ;
	public Iupdate vcddiagram=null;
	static public boolean block=true; 
	
	public VcdFactory collect(VCDDefinitions vcd) {
		VcdFactory factory = new VcdFactory();		
		VarCollector vc = new VarCollector(vcd.getDecls());
		vcd.visit(vc);
		factory.setVcd(vcd);
		tc = new TraceCollector(vc, factory);
	
		vcd.visit(tc);
		this.vcd = vcd;
		
		return factory;
	}

	public static void drawInWindow(VCDDefinitions vcd) {
		drawInWindow(vcd, "VCD viewer");
	}
	
	public static void drawInWindow(VCDDefinitions vcd, String name) {
		Collector co = new Collector();
		drawInWindow(co.collect(vcd), name, co);
	}
	
	public static void drawInWindow(VcdFactory factory, String name,Collector co) {		
		Shell shell = new Shell(); 
		VcdDiagram window = new VcdDiagram(shell, name,factory);
		window.setTc(co.tc);
		window.setVcd(co.vcd);
		factory.setVcddia(window);
		window.setFactory(factory);
		window.setBlockOnOpen(block);
		co.vcddiagram=window;	
		window.create();
		window.getShell().setActive();		
		window.open();
	}
	
	public static void drawInEditor(IVcdDiagram editor ,VCDDefinitions vcd) {
		drawInEditor(editor,vcd, "VCD viewer");
	}

	public static void drawInEditor(IVcdDiagram editor,VCDDefinitions vcd, String name) {
		Collector co = new Collector();
		VcdFactory factory= co.collect(vcd);		
		drawInEditor(editor,factory, name, co);
		
		
	}

	public static void drawInEditor(IVcdDiagram editor, VcdFactory factory, String name,Collector co) {	
		editor.setTc(co.tc);
		editor.setVcd(co.vcd);
		editor.setFactory(factory);
		factory.setVcddia(editor);
		co.vcddiagram=(Iupdate) editor;	
		
	}
	
}
