package fr.inria.vcd.model.visitor;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.keyword.TimeUnit;

final public class TimeScaleVisitor implements IDeclarationVisitor {
	private int timescale;
	private TimeUnit unit;

	public int getTimescale() {
		return timescale;
	}

	public TimeUnit getUnit() {
		return unit;
	}

	
	public void visitDate(String date) {
	}

	
	public void visitEndDefinitions() {
	}

	
	public void visitVersion(String text, String task) {
	}

	
	public void visitConstraintComment(String comment,Function function, ArrayList<String> clocks) {
	}

	
	public void visitStatusComment(String func, String ident, String time) {
		
		
	}

	
	public void visitTimeScale(int number, TimeUnit unit) {
		this.timescale = number;
		this.unit = unit;
	}
}
