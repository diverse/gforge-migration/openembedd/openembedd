package fr.inria.vcd.model.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import fr.inria.vcd.model.IDeclarationCommand;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.command.HierVar;
import fr.inria.vcd.model.command.Var;
import fr.inria.vcd.model.command.VarCommand;
import fr.inria.vcd.model.comment.HideCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.keyword.VarType;

public class VarCollector implements IScopeVisitor, Iterable<IVar> {
	private ArrayList<IVar> vars = new ArrayList<IVar>();
	private LinkedList<String> names = new LinkedList<String>();
	private String parent = null;
	private ArrayList<IDeclarationCommand> list;
	
	
	public VarCollector(ArrayList<IDeclarationCommand> list)
	{
		super();
		this.list = list;
	}

	
	public void visitScope(ScopeType type, String name) {
		if (parent != null)
			names.add(parent);
		parent = name;
	}

	
	public void visitUpscope() {
		if (names.isEmpty())
			this.parent = null;
		else
			this.parent = names.removeLast();
	}

	
	public void visitVar(VarCommand command, VarType type, String ref) {
		IVar var = new Var(command, parent);
		if (!names.isEmpty()) {
			ListIterator<String> it = names.listIterator(names.size() - 1);
			while (it.hasPrevious())
				var = new HierVar(var, it.previous());
		}	
		for(IDeclarationCommand icc: list)
		{
			if (icc instanceof HideCommand)
			{
				String s= ((HideCommand) icc).getName();
				if (s.equals(var.getName()))
					var.setVisibleByDefault(false);
			}
		}
		vars.add(var);
	}

	
	public Iterator<IVar> iterator() {
		return vars.iterator();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(IVar var : this){
			sb.append(var.toString()+"\n");
		}
		return sb.toString();
	}
}
