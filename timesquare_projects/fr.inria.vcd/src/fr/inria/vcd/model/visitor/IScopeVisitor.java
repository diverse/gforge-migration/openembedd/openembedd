package fr.inria.vcd.model.visitor;

import fr.inria.vcd.model.command.VarCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.keyword.VarType;

public interface IScopeVisitor {
	void visitVar(VarCommand command, VarType type, String ref);
	void visitScope(ScopeType type, String name);
	void visitUpscope();
}
