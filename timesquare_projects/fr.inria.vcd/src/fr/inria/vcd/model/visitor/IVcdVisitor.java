package fr.inria.vcd.model.visitor;

public interface IVcdVisitor {
	IDeclarationVisitor getDeclarationVisitor();
	IScopeVisitor getScopeVisitor();
	ISimulationVisitor getSimulationVisitor();
}
