package fr.inria.vcd.model.visitor;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.command.VarCommand;
import fr.inria.vcd.model.keyword.ScopeType;
import fr.inria.vcd.model.keyword.SimulationKeyword;
import fr.inria.vcd.model.keyword.TimeUnit;
import fr.inria.vcd.model.keyword.VarType;


final public class StringVisitor implements IVcdVisitor {
	private StringBuffer sb = new StringBuffer();

	@Override
	public String toString() {
		return sb.toString();
	}

	class MyDeclarationVisitor implements IDeclarationVisitor {
		
		public void visitDate(String date) {
			sb.append("$date ").append(date).append(" $end\n");		
		}

		
		public void visitEndDefinitions() {
			sb.append("$enddefinitions $end\n");
		}

		
		public void visitVersion(String text, String task) {
			sb.append("$version ").append(text).append(' ').append(task).append(" $end\n");
		}
		
		
		public void visitConstraintComment(String clock, Function function,ArrayList<String> clocks) {
			sb.append("$comment\n").append(clock+" ").append(function+" ");
			for(String str : clocks)
				sb.append(str + " ");
			sb.append("\n$end\n");
		}

		
		public void visitStatusComment(String func, String ident, String time) {
		}

		
		public void visitTimeScale(int number, TimeUnit unit) {
			sb.append("$timescale ").append(number).append(' ').append(unit).append(" $end\n");
		}
	}

	
	public IDeclarationVisitor getDeclarationVisitor() {
		return new MyDeclarationVisitor();
	}

	class MyScopeVisitor implements IScopeVisitor {
		
		public void visitScope(ScopeType type, String name) {
			sb.append("$scope ").append(type).append(' ').append(name).append(" $end\n");
		}

		
		public void visitVar(VarCommand command, VarType type, String ident) {
			sb.append("$var ").append(type).append(' ').append(command.getSize()).append(' ').append(ident).append(' ').append(command.getName()).append(" $end\n");		
		}

		
		public void visitUpscope() {
			sb.append("$upscope $end\n");
		}	
	}
		
	
	public IScopeVisitor getScopeVisitor() {
		return new MyScopeVisitor();
	}

	private class MySimulationVisitor implements ISimulationVisitor {

		
		public void visitTime(int time) {
			sb.append('#').append(time).append('\n');
		}

		
		public void visitKeyword(SimulationKeyword keyword) {
			if (keyword != null) {
				sb.append('$').append(keyword).append('\n');
			}
		}

		
		public void visitValueChange(ValueChange value) {
			sb.append(value).append('\n');
		}

		
		public void visitEnd(SimulationKeyword keyword) {
			if (keyword != null) sb.append("$end\n");		
		}

		
		public void endSimulation() {
			// nothing
		}		
	}

	
	public ISimulationVisitor getSimulationVisitor() {
		return new MySimulationVisitor();
	}
}
