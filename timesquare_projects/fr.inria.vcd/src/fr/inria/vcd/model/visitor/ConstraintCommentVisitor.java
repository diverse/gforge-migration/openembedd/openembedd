package fr.inria.vcd.model.visitor;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.keyword.TimeUnit;

final public class ConstraintCommentVisitor implements IDeclarationVisitor {
	private String clock;
	private Function function;
	private ArrayList<String> referenceClocks;

	
	public void visitDate(String date) {
	}

	
	public void visitEndDefinitions() {
	}

	
	public void visitVersion(String text, String task) {
	}

	
	public void visitConstraintComment(String clock, Function function, ArrayList<String> referenceClocks) {
		this.clock = clock;
		this.function = function;
		this.referenceClocks = referenceClocks;
		
	}

	
	public void visitStatusComment(String func, String ident,String time) {
	}
	
	
	public void visitTimeScale(int number, TimeUnit unit) {
	}

	public String getClock() {
		return clock;
	}

	public ArrayList<String> getReferenceClocks() {
		return referenceClocks;
	}

	public Function getFunction() {
		return function;
	}
}