package fr.inria.vcd.model.visitor;

import java.util.ArrayList;

import fr.inria.vcd.model.Function;
import fr.inria.vcd.model.keyword.TimeUnit;

final public class StatusCommentVisitor implements IDeclarationVisitor {
//	private String func;
//	private String ident;
//	private String time;

	
	public void visitDate(String date) {
	}

	
	public void visitEndDefinitions() {
	}

	
	public void visitVersion(String text, String task) {
	}

	
	public void visitStatusComment(String func, String ident, String time) {
//		this.func = func;
//		this.ident = ident;
//		this.time = time;
		
	}
	
	
	public void visitConstraintComment(String clock, Function function, ArrayList<String> referenceClocks) {
	}

	
	public void visitTimeScale(int number, TimeUnit unit) {
	} 
}