package fr.inria.vcd.model.visitor;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.ValueChange;
import fr.inria.vcd.model.keyword.SimulationKeyword;
import fr.inria.vcd.view.VcdFactory;
import fr.inria.vcd.view.VcdValueFactory;

public class TraceCollector implements ISimulationVisitor {
	private int time =-1;// -1;
	private boolean partial =false; 
	private HashMap<String, TraceData> hmtdata = new HashMap<String, TraceData>();
	private ArrayList<IVar> selectedClocks = new ArrayList<IVar>();
	private ArrayList<IVar> allClocks = new ArrayList<IVar>();	
	private double zoom = 1;
	private VcdFactory fact=null; 
	
	public TraceCollector(Iterable<IVar> vars, VcdFactory factory) {
		super();
		fact=factory;	
		int i=0;		
		hmtdata.clear();
		for (IVar var : vars) {
			
			
			if (var.isVisibleByDefault())
			{				
				selectedClocks.add(var); 	
				TraceData td=new TraceData(factory.getVcdValueFactory(var,var.getName(), zoom));
				hmtdata.put(var.getIdentiferCode(), td);	
			}
			else
			{
				allClocks.add(var);
			}
		
			
							
			i++;
		}
		
		for (IVar var : selectedClocks) {
			
			//data.put(var.getIdentiferCode(), new TraceData(factory.getVcdValueFactory(var,var.getName(), zoom)));
			
			getAllnames().remove(var);
			
		}
	}

	public void constructClock(VcdFactory factory, ArrayList<IVar> listvar,	double zoom) {

		hmtdata.clear();
		selectedClocks.clear();
		selectedClocks = listvar;
		for (IVar var : selectedClocks) {
			hmtdata.put(var.getIdentiferCode(), new TraceData(factory
					.getVcdValueFactory(var,var.getName(),zoom)));
			getAllnames().remove(var);
		}
	}
	
	
	
	
	
	public void visitTime(int time) {

		this.time = time;  
		fact.modifyRuleSize(time);
	}

	
	public void visitKeyword(SimulationKeyword keyword) {
		// nothing
	}

	
	public void visitValueChange(ValueChange valueChange) {
		if (valueChange==null)
			return ;
		TraceData td = hmtdata.get(valueChange.getIdentifierCode());
		if (td == null)
			return;
		Object value = valueChange.getValue();
		if (td.value != null) {
			if (td.value.equals(value))
				return;
			td.factory.setZoom(zoom);					
			td.factory.build(td.value, time,td.previousTime);
		}		
		td.value = value;
		td.previousTime = time;
	}

	
	public void visitEnd(SimulationKeyword keyword) {
		// nothing
	}

	
	public void endSimulation() {
		if (partial) {
			//for (TraceData td : data.values()) {
				//td.factory.setZoom(zoom);
				//td.factory.build(td.value, time - td.previousTime);
			//	}
		} else {
			for (TraceData td : hmtdata.values()) { //TODO ...
				td.factory.setZoom(zoom);
				if (td.value!=null)
				td.factory.build(td.value, time + 20 - td.previousTime);
			}
		}

	}

	public ArrayList<IVar> getSelectedClocks() {
		return selectedClocks;
	}

	public ArrayList<IVar> getAllnames() {
		return allClocks;
	}

	public void setAllClocks(ArrayList<IVar> allClocks) {
		this.allClocks = allClocks;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public boolean isPartial() {
		return partial;
	} 

	public void setPartial(boolean partial) {
		this.partial = partial;
	}
	
	public VcdValueFactory getDataFactory(String s)
	{
		TraceData td =hmtdata.get(s);
			if (td==null) return null;
		return td.factory;
	}
}

class TraceData 
{
	protected Object value;
	protected VcdValueFactory factory;
	protected int previousTime=0;

	TraceData(VcdValueFactory factory) {
		this.factory = factory;
		previousTime=0;
		value=null;
	}

	@Override
	public String toString() {
		return previousTime + "," + value;
	}
}
