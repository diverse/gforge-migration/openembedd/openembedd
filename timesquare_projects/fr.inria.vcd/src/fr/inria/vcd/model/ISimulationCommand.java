package fr.inria.vcd.model;

import fr.inria.vcd.model.visitor.ISimulationVisitor;

public interface ISimulationCommand extends Iterable<ValueChange> {
	public void visit(ISimulationVisitor visitor);
	public int getTime();
}
