/**
 * 
 */
package fr.inria.vcd.model;

import java.util.ArrayList;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.ScrollPaneSolver;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.base.MyPluginManager;
import fr.inria.vcd.VcdActivator;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.vcd.preferences.ColorSelection;

public final class FigureCanvasBase
{
	public final class ResizeListener implements Listener {
		public void handleEvent( Event event )
		{				
			scrollUpdate();	
		}
	}

	private IVcdDiagram me=null;
	private FigureCanvas canvas=null;
	private FigureCanvas names=null;
	private FigureCanvas scale=null;
	private FigureCanvas picture=null;
	private ToolBar toolbar=null;
	private Composite composite=null;
	
	private ImageFigure picture_im=null;
	
	public FigureCanvasBase( IVcdDiagram me) {			
		super();
		this.me=me;
	}
	
	
	public void makevcd(Composite p, boolean simulation	)
			{
				init(p,simulation);		
				positionpicture();
				positionscale();		
				positioncanvas();		
				positionnames();
				positiontoolbar();					 
			}
	
	private void init(Composite p, boolean simulation	)
	{
			composite = new Composite (p,  SWT.FILL | SWT.V_SCROLL | SWT.H_SCROLL);
			canvas = new FigureCanvas(composite,SWT.NONE );
			names = new FigureCanvas(composite, SWT.NONE);		
			scale = new FigureCanvas(composite, SWT.NONE);	
			picture = new FigureCanvas(composite, SWT.NONE);	
			toolbar = new ToolBar(composite, SWT.NONE);	
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
			composite.setBackground(MyColorApi.colorMenuBackground());
			composite.setLayout(new FormLayout());
			picture_im= new ImageFigure(MyPluginManager.getImage(VcdActivator.PLUGIN_ID, "icons/simulation.png"));	
			me.getFactory().getPicture().add(picture_im);
			picture_im.setVisible(simulation);
			
	}
	
	private void positioncanvas()
	{
		FormData canvasData = new FormData();
		canvasData.left = new FormAttachment(names);  
		canvasData.right = new FormAttachment(100, 0);
		canvasData.bottom = new FormAttachment(toolbar);
		canvasData.top = new FormAttachment(picture);			
		canvas.setBackground(MyColorApi.colorBlack());
		canvas.setScrollBarVisibility(FigureCanvas.NEVER );
		canvas.setLayoutData(canvasData);		
		canvas.setContents(me.getFactory().getBack());
	}
	
	private void positionnames()
	{
		FormData namesData = new FormData();
		namesData.left = new FormAttachment(0, 0);
		namesData.bottom = new FormAttachment(toolbar);
		namesData.top = new FormAttachment(picture);
		namesData.width = ColorSelection.getDefault().getWidthName();
		names.setBackground(MyColorApi.colorBlack());
		names.setLayoutData(namesData);
		names.setContents(me.getFactory().getNames());	
		names.setScrollBarVisibility(FigureCanvas.NEVER);		
	}
	
	private void positionscale()
	{
		FormData scaleData = new FormData();
		scaleData.left = new FormAttachment(picture);
		scaleData.right = new FormAttachment(100, 0);
		scaleData.top = new FormAttachment(0, 0);
		scaleData.height = 50;
		scale.setLayoutData(scaleData);
		scale.setBackground(MyColorApi.colorBlack());
		scale.setScrollBarVisibility(FigureCanvas.NEVER	);		
		scale.setContents(me.getFactory().getScale());
		me.getFactory().getScale().setSize(-1, 35);			
		scale.setSize(-1, 50);
	}
	
	private void positionpicture()
	{
		FormData pictureData = new FormData();
		pictureData.left = new FormAttachment(0, 0);		
		pictureData.top = new FormAttachment(0, 0);
		pictureData.height = 50;
		pictureData.width=ColorSelection.getDefault().getWidthName();
		picture.setLayoutData(pictureData);
		picture.setBackground(MyColorApi.colorBlack() );
		picture.setScrollBarVisibility(FigureCanvas.NEVER	);		
		picture.setContents(me.getFactory().getPicture());
	}
	
	private void positiontoolbar()
	{
		FormData toolbarData = new FormData();
		toolbarData.left = new FormAttachment(0, 0);
		toolbarData.right = new FormAttachment(100, 0);
		toolbarData.bottom = new FormAttachment(100, 0);
		toolbarData.height = 25;
		toolbar.setLayoutData(toolbarData);
	}
	
	public void layoutall()
	{
		canvas.layout();
		names.layout();
		scale.layout();
		picture.layout();
		toolbar.layout();
		composite.layout();
	}

	public FigureCanvas getCanvas() {
		return canvas;
	}

	public FigureCanvas getNames() {
		return names;
	}

	public FigureCanvas getScale() {
		return scale;
	}

	public FigureCanvas getPicture() {
		return picture;
	}

	public ToolBar getToolbar() {
		return toolbar;
	}

	public Composite getComposite() {
		return composite;
	}
	
	public  void setSimulationAvance(int etape)
	{
		if (picture_im==null)
			return ;
		String s= VcdActivator.PLUGIN_ID ;
		
		switch (etape)
		{
		
		case 0: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation.png")); break;
		case 1: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation2.png")); break;
		case 2: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation3.png")); break;
		case 3: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation4.png")); break;
		case 4: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation5.png")); break;
		case 5: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation6.png")); break;
		case 6: picture_im.setImage(MyPluginManager.getImage(s, "icons/simulation7.png")); break;
		}
	}
	
	private org.eclipse.draw2d.geometry.Rectangle oldrc = null;
	
	
	public void scrollUpdate()
	{
		ScrollBar sbrscr =  getCanvas().getHorizontalBar();
		ScrollBar sbrdst =getComposite().getHorizontalBar();
		sbrdst.setIncrement(sbrscr.getIncrement());
		sbrdst.setMaximum(sbrscr.getMaximum());
		sbrdst.setMinimum(sbrscr.getMinimum() );
		sbrdst.setSelection(sbrscr.getSelection());
		sbrdst.setPageIncrement(sbrscr.getPageIncrement());
		sbrdst.setThumb(sbrscr.getThumb());
		
		sbrscr =  getCanvas().getVerticalBar();
		sbrdst =getComposite().getVerticalBar();
		sbrdst.setIncrement(sbrscr.getIncrement());
		sbrdst.setMaximum(sbrscr.getMaximum());
		sbrdst.setMinimum(sbrscr.getMinimum() );
		sbrdst.setSelection(sbrscr.getSelection());
		sbrdst.setPageIncrement(sbrscr.getPageIncrement());
		sbrdst.setThumb(sbrscr.getThumb());
		ScrollPaneSolver.Result result;
		FigureCanvas fc= getCanvas();
		result = ScrollPaneSolver.solve(new Rectangle(fc.getBounds()).setLocation(0, 0),
			fc.getViewport(),
			FigureCanvas.AUTOMATIC,
			FigureCanvas.AUTOMATIC,
			0,
			0 );
		
		
			if (getComposite().getHorizontalBar().getVisible() != result.showH)
				getComposite().getHorizontalBar().setVisible(result.showH);
			if (getComposite().getVerticalBar().getVisible() != result.showV)
				getComposite().getVerticalBar().setVisible(result.showV);
			Rectangle r = new Rectangle(fc.getClientArea());
			r.setLocation(0, 0);
			fc.getLightweightSystem().getRootFigure().setBounds(r);
			//FigureCanvas fc= getNa();
	}
	
	public int update(int n1, int n2, boolean b) 
	{
		if (me.getCanvas().isDisposed())
			return 0 ;
		scrollUpdate();
		ArrayList<IVar> listvar = new ArrayList<IVar>();
		listvar.addAll(me.getTc().getSelectedClocks());
		me.getTc().setZoom(me.getTcZoom());
		me.getVcd().visit(me.getTc(), n1, n2, b);	
		getCanvas().setContents(me.getFactory().getBack());    

		int x1 = -1, x2 = -1, y1 = -1, y2 = -1;

		org.eclipse.draw2d.geometry.Rectangle rc = null;
		for (Object obj :me.getCanvas().getContents().getChildren()) {
			Panel panel = null;
			if (obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			org.eclipse.draw2d.geometry.Rectangle r = null;		
			for (Object f : panel.getChildren()) {
				if (f instanceof IFigure) {
					IFigure poly = (IFigure) f;
					r = poly.getBounds();
					if (rc == null)
						rc = r;
					else
						rc = rc.getUnion(r);
				}
			}
		}
		org.eclipse.draw2d.geometry.Rectangle rc2 = null;
		if (oldrc == null) 
		{
			rc2 = rc;
		} 
		else
			rc2 = new org.eclipse.draw2d.geometry.Rectangle(oldrc.getTopRight(), rc.getBottomRight());
		org.eclipse.draw2d.geometry.Rectangle rc3 = null;
		oldrc = rc;
		for (Object obj : me.getCanvas().getContents().getChildren()) {
			Panel panel = null;
			if (obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			org.eclipse.draw2d.geometry.Rectangle r = null;
			// Object f= panel.getChildren().get(panel.getChildren().size()-1);
			for (Object f : panel.getChildren()) {
				if (f instanceof IFigure) {
					IFigure poly = (IFigure) f;
					r = poly.getBounds();
					if (r.intersects(rc2))
						if (rc3 == null)
							rc3 = r;
						else
							rc3 = rc3.getUnion(r);
				}
			}
		}
		if (rc3 != null) {
			x1 = rc3.x;
			x2 = rc3.width;
			y1 = rc3.y;
			y2 = rc3.height;
			me.getCanvas().layout();
			for (MenuItem menuitem : me.getList().getMenuForComment().keySet()) {
				if (menuitem.getSelection()) {
					ConstraintCommentCommand cc = me.getList().getMenuForComment()
							.get(menuitem);
					Color color = me.getList().menuForColorGet(menuitem);
					me.getConstraintsFactory().drawSyncInterval(cc, color);
				}
			}
			me.getCanvas().redraw(x1, y1, x2, y2, false);
		}
		me.getCanvas().layout();
		return 0;
	}
	
	public int addScrollListener()
	{
		getCanvas().getVerticalBar().addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
			
			public void widgetSelected(SelectionEvent e) {
				int y=getCanvas().getVerticalBar().getSelection();
				getNames().scrollToY(y);
				scrollUpdate();
			}
		});
		/**Mouse Scrool name -> canvas*/
		getNames().getVerticalBar().addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
			
			public void widgetSelected(SelectionEvent e) {
				int y=getNames().getVerticalBar().getSelection();
				getCanvas().scrollToY(y);
				scrollUpdate();
			}
		});
		
		getCanvas().getHorizontalBar().addSelectionListener(new SelectionListener() 
		{
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
			
			public void widgetSelected(SelectionEvent e) 
			{				
				getScale().scrollToX(getCanvas().getHorizontalBar().getSelection());
				scrollUpdate();	
			}
		});
		getComposite().addListener(SWT.Resize,new ResizeListener());
		getCanvas().addListener(SWT.Resize,new ResizeListener());
		getScale().addListener(SWT.Resize,new ResizeListener());
		
		getComposite().getVerticalBar().addSelectionListener(new SelectionListener() {	
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		
			public void widgetSelected(SelectionEvent e) {
				int y=getComposite().getVerticalBar().getSelection();
				getNames().scrollToY(y);
				getCanvas().scrollToY(y);
				scrollUpdate();	
			}
		});
		getComposite().getHorizontalBar().addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		
			public void widgetSelected(SelectionEvent e) 
			{	
				int x=getComposite().getHorizontalBar().getSelection();
				getScale().scrollToX(x); 
				getCanvas().scrollToX(x);
				scrollUpdate();	
			}
		});
		return 0;
	}
}