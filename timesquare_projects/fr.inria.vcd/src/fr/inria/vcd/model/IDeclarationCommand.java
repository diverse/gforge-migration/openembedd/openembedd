package fr.inria.vcd.model;

import fr.inria.vcd.model.visitor.IDeclarationVisitor;

public interface IDeclarationCommand {
	void visit(IDeclarationVisitor visitor);
}
