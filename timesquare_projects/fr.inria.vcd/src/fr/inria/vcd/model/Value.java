package fr.inria.vcd.model;

public enum Value {
	_0, _1, _x, _z;

	@Override
	public String toString() { return super.toString().substring(1); }
	public static Value fromChar(char c) {
		c=Character.toLowerCase(c);
		for(Value v : values()) {
			if(v.toString().charAt(0)==c) return v;
		}
		throw new IllegalArgumentException("Not a Value "+c);
	}
	public static Value fromString(String s) {
		return valueOf("_"+s.toLowerCase());
	}
}
