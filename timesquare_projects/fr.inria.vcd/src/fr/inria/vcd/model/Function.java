package fr.inria.vcd.model;

public enum Function {
	_filteredby , _alternateswith , _synchronizeswith, _sampledon, _oneshoton, _sustains , _delayedfor, _precedes, _union, _isperiodicon,_restrictedto;
	
	@Override
	public String toString() { return super.toString().substring(1); }
	
	public static Function fromString(String s) 	
	{
		if (s.compareTo("periodicon")==0)
			return _isperiodicon ;		
	 return valueOf("_"+s.toLowerCase());
	}
}
