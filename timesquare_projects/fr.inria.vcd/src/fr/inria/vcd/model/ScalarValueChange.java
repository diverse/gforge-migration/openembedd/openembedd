package fr.inria.vcd.model;

final public class ScalarValueChange extends ValueChange {
	private Value value;
	public ScalarValueChange(char v, String ident) {
		this(Value.fromChar(v), ident);
	}
	public ScalarValueChange(String v, String ident) {
		this(Value.fromString(v), ident);
	}
	public ScalarValueChange(Value v, String ident) {
		super(ident);
		this.value = v;	
	}
	@Override
	public Object getValue() {
		return value;
	}
	@Override
	public String toString() {
		return value+super.toString();
	}
	
	@Override
	public void setValue(Object value){
		this.value = (Value)value;
	}
}
