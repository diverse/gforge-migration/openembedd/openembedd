package fr.inria.vcd.model;

public final class IdentifierCode {
	static private byte BEGIN = 33, END=126;
	static private IdentifierCode[] identifiers;
	static {
		identifiers = new IdentifierCode[END-BEGIN+1];		                                 
		for(int i=BEGIN; i<=END; i++)
			identifiers[i-BEGIN] = new IdentifierCode(i);
	}
	
	static private int current = BEGIN;
	private int value;
	private IdentifierCode(int value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return ""+(char)value;
	}
	public static IdentifierCode fromString(String v) {
		if(v.length()!=1) throw new IllegalArgumentException("Not an IdentifierCode:"+v);
		char c = v.charAt(0);
		if(c<BEGIN || c>END) throw new IllegalArgumentException("IdentifierCode:"+v+" not in RANGE["+BEGIN+".."+END+"]");
		return identifiers[c-BEGIN];
	}
	public static IdentifierCode getNext() {
		assert(current>END) : "Invalid identifier";
		return identifiers[current++ - BEGIN];
	}
}
