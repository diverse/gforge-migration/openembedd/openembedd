package fr.inria.vcd;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;




public class VcdActivator extends AbstractUIPlugin {

	
	public static final String PLUGIN_ID = "fr.inria.vcd";

	// The shared instance
	private static VcdActivator plugin;
	
	public VcdActivator() {
		
	}

	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static VcdActivator getDefault() 
	{		
		return plugin;
	}


}
