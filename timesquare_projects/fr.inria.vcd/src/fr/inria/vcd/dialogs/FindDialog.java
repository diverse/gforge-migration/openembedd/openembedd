package fr.inria.vcd.dialogs;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.IVar;

public class FindDialog extends Dialog {
	private IVcdDiagram vdt;
	private String title;
	private String message;
	private String value = "";
	private String value2 = "";
	private Button okButton;
	private Text text;
	private CCombo combo;
	private Text text2;
	private Text errorMessageText;
	private String errorMessage;

	private String message2;
	private int[] clockedge=null;
	private int max =0;
	public class ListernerInt implements Listener {

		public ListernerInt() {
			super();
		}

		
		public void handleEvent(Event e) {
			String string = e.text;
			
			char[] chars = new char[string.length()];
			string.getChars(0, chars.length, chars, 0);			
			for (int i = 0; i < chars.length; i++) {
				if (!('0' <= chars[i] && chars[i] <= '9')) {
					e.doit = false;
					return;	
				}
			}
			
			if (combo.getSelectionIndex()!=-1)
			{
				okButton.setEnabled(true);
				
			}
			else
			{
			setErrorMessage("Warning : no clock selected ",ColorConstants.darkBlue);
			}
			
			
		}
	}
	
	
	public FindDialog(Shell parentShell, String dialogTitle,
			String dialogMessage, String dialogMessage2,
			String initialValue,  IVcdDiagram vdt) {
		super(parentShell);
		this.title = dialogTitle;
		message = dialogMessage;
		message2 = dialogMessage2;
		if (initialValue == null) {
			value = "";
		} else {
			value = initialValue;
		}
		
		this.vdt = vdt;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			value = combo.getText();
			value2 = text2.getText();
		} else {
			value = null;
			value2 = null;
		}
		super.buttonPressed(buttonId);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID,	IDialogConstants.OK_LABEL, true);
		okButton.setEnabled(false);
		createButton(parent, IDialogConstants.CANCEL_ID,	IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridData data = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
				| GridData.VERTICAL_ALIGN_CENTER);
		data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
		if (message != null) {
			Label label = new Label(composite, SWT.WRAP);
			label.setText(message);
			label.setLayoutData(data);
			label.setFont(parent.getFont());
		}
		combo = new CCombo(composite, SWT.NONE | SWT.READ_ONLY);
		combo.setBackground(new Color(null, 255, 255, 255));
		int size=vdt.getTc().getSelectedClocks().size();
		String[] clocknames = new String[size];
		clockedge= new int[size];
		int i = 0;
		for (IVar var : vdt.getTc().getSelectedClocks()) {
			int n=0;
			if (var.getValueFactory()!=null)
				n=var.getValueFactory().getEdge();
			clocknames[i] = var.getName();
			clockedge[i]=n;
			i++;
		}
		combo.setItems(clocknames);
		combo.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		combo.addSelectionListener(new SelectionListener() {
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			
			public void widgetSelected(SelectionEvent e) {
				validateInput();
			
			}
		});
		if (message2 != null) {
			Label label = new Label(composite, SWT.WRAP);
			label.setText(message2);
			data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
			label.setLayoutData(data);
			label.setFont(parent.getFont());
		}
		text2 = new Text(composite, SWT.SINGLE | SWT.BORDER);
		text2.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL 	| GridData.HORIZONTAL_ALIGN_FILL));
		text2.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				
				 validateInput();
			
				
				//validateInput();
			
			}
		});
		text2.addListener(SWT.Verify, new ListernerInt());
		errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.WRAP);
		errorMessageText.setLayoutData(new GridData(
				GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		setErrorMessage(errorMessage,ColorConstants.black);

		applyDialogFont(composite);

		return composite;
	}

	protected Button getOkButton() {
		return okButton;
	}

	protected Text getText() {
		return text;
	}

	
	public String getClock() {
		return value;
	}

	public String getInstance() {
		return value2;
	}

	public String getMessage2() {
		return message2;
	}

	public void setMessage2(String message2) {
		this.message2 = message2;
	}

	protected void validateInput() {
		
		if (combo.getSelectionIndex()!=-1)
		{
			
		
			int sel=combo.getSelectionIndex();			
			if ((sel!=-1) && (clockedge!=null)  && (sel < clockedge.length )) 
			{
			max= clockedge[sel];
	
			setErrorMessage("Clock  "+ combo.getItem(sel ) +" have  " + max + "  edge(s) ", ColorConstants.black);
			}
			else
			{
				max=0;
			}		
			
			
			String s=text2.getText();
			if (s==null || s.equals(""))
			{
			okButton.setEnabled(false);
			}
			else
			{	
				int n=Integer.parseInt(s);  
				if (( n<max+1)&& (n!=0))
				{
					okButton.setEnabled(true);
					setErrorMessage("Clock  "+ combo.getItem(sel ) +" have  " + max + "  edge(s) ",ColorConstants.black);
				}
				else
				{
					okButton.setEnabled(false);  
					setErrorMessage("ERROR : Clock  "+ combo.getItem(sel ) +" have  " + max + "  edge(s) ",ColorConstants.red);
				}
			}
			
		}
		else
		{
			setErrorMessage("Warning : no clock selected ",ColorConstants.darkBlue);
			okButton.setEnabled(false);
		}
	
		
	}

	public void setErrorMessage(String errorMessage, Color c) {
		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			errorMessageText.setText(errorMessage == null ? " \n "
					: errorMessage);
			boolean hasError = errorMessage != null
					&& (StringConverter.removeWhiteSpaces(errorMessage))
							.length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);
			errorMessageText.setForeground(c);
			errorMessageText.getParent().update();
			/*Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				//button.setEnabled(errorMessage == null);
			}*/
		}
	}
}
