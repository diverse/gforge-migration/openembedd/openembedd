package fr.inria.vcd.dialogs;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import fr.inria.base.BasedDialog;
import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.menu.Mode;
import fr.inria.vcd.model.IVar;
import fr.inria.vcd.model.visitor.TraceCollector;

//DialogBox open by the action of "Partial..."
public class PartialDialog extends BasedDialog<IVar>{
	private IVcdDiagram ivcd;
	public PartialDialog(Shell parentShell, String possibleText,String selectedText,IVcdDiagram ivcd ,String title ) {
		super(parentShell, possibleText, selectedText,title);
		this.ivcd=ivcd;
		setUpdown(false);		
		TraceCollector tc=ivcd.getTc();
		labelProvider = new MyLabelProvider();	
		//for each hide clock 
		for(IVar var : tc.getAllnames())
		{
			if (var.getValueFactory()!=null)
			{
				if(var.getValueFactory().haveGhostinclock())
				{
					if(var.getValueFactory().getIsGhostVisible()==true)
					{
						elselect.addElement(var);
					}
					else
					{
						elsource.addElement(var);
					}
				}
			}
		}
		//for each show clock 
		for(IVar var : tc.getSelectedClocks())
		{
			if(var.getValueFactory().haveGhostinclock())
			{
				if(var.getValueFactory().getIsGhostVisible()==true)
				{
					elselect.addElement(var);
				}
				else
				{
					elsource.addElement(var);
				}
			}
		}
	}
	
	
	
	
	
	
	
	protected class MyLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public Image getImage(Object element) 
		{
			return null;
		}


		@Override
		public String getText(Object o) {
			if ((o instanceof IVar)) 		
				return ((IVar) o).getName(); 
			//TODO nbr ghost				
			return o.toString();
		}

		public Image getColumnImage(Object element, int columnIndex) 
		{
			return null;
		}
	
		public String getColumnText(Object element, int columnIndex) 
		{
			if (columnIndex == 0) 
			{
				return getText(element);
			} 
			else if (columnIndex == 1) 
			{
				return "toto" ;
			}
			return "not the right column index";
		}
	}
	
	//for apply the change
	public int apply()
	{
		//if all clock are show
		if(elsource.isEmpty())
		{
			ivcd.getVcdmenu().setGhostSelected(Mode.show);
			ivcd.setGhostMode(true);
			return 0;
		}
		//if all clock are hide
		if(elselect.isEmpty())
		{
			ivcd.getVcdmenu().setGhostSelected(Mode.hide);
			ivcd.setGhostMode(false);
			return 1;
		}
		//Default usage
		for(IVar var: elselect.getElements())
		{
			var.getValueFactory().setGhostVisible(true);
		}
		for(IVar var: elsource.getElements())
		{
			var.getValueFactory().setGhostVisible(false);
		}
		ivcd.getVcdmenu().setGhostSelected(Mode.partial);
		return 2;
	}
}