/**
 * 
 */
package fr.inria.vcd.dialogs;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.inria.base.MyManager;
import fr.inria.vcd.listener.IVcdDiagram;

public class OrderAction extends Action {
	/**
	 * 
	 */
	private final IVcdDiagram ivcddiagram;

	/**
	 * @param ivcddiagram
	 */
	public OrderAction(IVcdDiagram ivcd) {
		this.ivcddiagram = ivcd;
	}

	@Override
	public void run() {
		try{
			
		
		Shell shell = new Shell();
		SelectClocksDialog d = new SelectClocksDialog(shell,
				ivcddiagram.getTc().getAllnames(), ivcddiagram.getTc().getSelectedClocks(),
				"Others clocks", "Displayed clocks","Selected clocks dialog");
		d.create();
		int ret = d.open();
		if (ret == Window.OK) {
			
			d.apply(ivcddiagram); 
			ivcddiagram.setFocus();
		}
	}
		catch (Exception ex) {
			MyManager.printError(ex);
		}
	
	}
}