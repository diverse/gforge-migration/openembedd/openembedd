package fr.inria.vcd.dialogs;

import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Panel;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.inria.base.MyPluginManager;
import fr.inria.vcd.VcdActivator;

public class HelpDialog  {
	private Shell sShell = null;  //  @jve:decl-index=0:visual-constraint="0,0"
	private FigureCanvas canvas = null;
	private Label title = null;
	private Label author = null;
	private Label author2 = null;
	private Label version = null;
	private Composite composite = null;
	private Label help = null;
	private Button button1 = null;
	private boolean resize = false;
	private Label HelpContents = null;
	private Label explains = null;
	private Label inria = null;
	private Label date = null;
	private static int size = 511;
	private Image logo = null;
	private Button close = null;
	private Button helpbutton2 = null;
	/**
	 * This method initializes canvas	
	 *
	 */
	private void createCanvas() {
		canvas = new FigureCanvas(sShell, SWT.NONE);
		canvas.setBackground(ColorConstants.white);
		logo = MyPluginManager.getImage(VcdActivator.PLUGIN_ID ,"icons/logo.jpg");	
		logo.type = SWT.BITMAP;
		
		Panel p = new Panel(){
			
			@Override
			public void paint(Graphics graphics) {
				graphics.drawImage(logo,new org.eclipse.draw2d.geometry.Point(0,0));
			}
		};
		
		p.setPreferredSize(logo.getBounds().width,logo.getBounds().height);
		canvas.setContents(p);
		canvas.setBounds(new Rectangle(3, 31, 252, 153));
	}

	/**
	 * This method initializes composite	
	 *
	 */
	private void createComposite() {
		composite = new Composite(sShell, SWT.NONE);
		composite.setBackground(ColorConstants.white);
		composite.setLayout(null);
		composite.setBounds(new Rectangle(0, 290, 253, 80));
		help = new Label(composite, SWT.NONE);
		help.setBackground(ColorConstants.white);
		help.setBounds(new Rectangle(6, 46, 130, 19));
		help.setText("	        Help contents");
		button1 = new Button(composite, SWT.ARROW | SWT.RIGHT);
		button1.setBounds(new Rectangle(161, 46, 26, 16));
		helpbutton2 = new Button(composite,SWT.ARROW | SWT.LEFT);
		helpbutton2.setBounds(new Rectangle(161, 46, 26, 16));
		helpbutton2.setVisible(false);
		helpbutton2.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				
				sShell.setSize(255, sShell.getSize().y);
				close.setBounds(new Rectangle(105, 370, 38, 23));
				button1.setVisible(true);
				helpbutton2.setVisible(false);
				resize = false;
			}
		});
		button1.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				if(!resize){
					sShell.setSize(size, sShell.getSize().y);
					close.setBounds(new Rectangle(230, 370, 38, 23));
					button1.setVisible(false);
					helpbutton2.setVisible(true);
					resize = true;
				}
			}
		});
	}

	/**
	 * @param args
	 */

	public void close(){
		sShell.close();
		
	}
	
	public HelpDialog(){		
		createSShell();
		sShell.open();
	}
	
	
	
	
	
	/**
	 * This method initializes sShell
	 */
	private void createSShell() {
		//println( Platform.getBundle("fr.inria.vcd").getHeaders().get("Bundle-Version"));
		//println( Platform.getBundle("fr.inria.vcd").getHeaders().get("Bundle-Vendor"));
		sShell = new Shell(SWT.NO_REDRAW_RESIZE | SWT.APPLICATION_MODAL | SWT.CENTER);
		sShell.setBackground(ColorConstants.white);
		sShell.setText("About vcd viewer editor");
		createCanvas();
		sShell.setLayout(null);
		sShell.setSize(new Point(256, 410));	
		Rectangle r=sShell.getDisplay().getClientArea();
		 //r=sShell.getDisplay().getBounds();
		Point p=new Point ((r.width-256)/2 , (r.height-410)/2 );
		sShell.setLocation(p);
		title = new Label(sShell, SWT.NONE);
		title.setBackground(ColorConstants.white);
		FontData myFontData = new FontData("VcdFont",12,SWT.BOLD);
		Font myFont = new Font(sShell.getDisplay(),myFontData);
		title.setFont(myFont);
		title.setText("         VCD VIEWER EDITOR");
		title.setBounds(new Rectangle(1, 9, 251, 27));
		inria = new Label(sShell, SWT.NONE);
		inria.setBackground(ColorConstants.white);
		inria.setText("                            Aoste INRIA/I3S");		
		inria.setBounds(new Rectangle(0, 185, 252, 26));
		author = new Label(sShell, SWT.NONE);
		author.setBackground(ColorConstants.white);
		author.setText("                      Author:");
		author.setBounds(new Rectangle(0, 215, 140, 46));
		
		author2 = new Label(sShell, SWT.NONE);
		author2.setBackground(ColorConstants.white);
		author2.setText("Anthony Gaudino\nBenoit Ferrero");
		author2.setBounds(new Rectangle(140, 215, 100, 46));
		
		date = new Label(sShell, SWT.NONE);
		date.setBackground(ColorConstants.white);
		date.setText("                           Date: 30/10/2008");
		date.setBounds(new Rectangle(0, 275, 252, 26));
		
		version = new Label(sShell, SWT.NONE);
		version.setBackground(ColorConstants.white);
		//version.setText("                            Version: 1.0.0.0");
		version.setText("                            Version: "+Platform.getBundle("fr.inria.vcd").getHeaders().get("Bundle-Version"));
		version.setBounds(new Rectangle(0, 303, 252, 26));
		createComposite();
		HelpContents = new Label(sShell, SWT.NONE);
		HelpContents.setBackground(ColorConstants.white);
		HelpContents.setBounds(new Rectangle(316, 9, 291, 25));
		HelpContents.setFont(myFont);
		HelpContents.setText("  HELP CONTENTS");
		explains = new Label(sShell, SWT.NONE);
		explains.setBackground(ColorConstants.white);
		explains.setBounds(new Rectangle(269, 75, 303, 77));
		explains.setText("Left click: Allow to show clocks constraints\n\n     Right click: Allow to show time bar\n\n     Crtl + Wheelbutton: Allow to zoom ");
		
		close = new Button(sShell, SWT.NONE);
		close.setBounds(new Rectangle(100, 370, 38, 23));
		close.setText("Close");
		close.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				
				close();
			}
		});
	}

}
