/**
 * 
 */
package fr.inria.vcd.dialogs;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import fr.inria.vcd.listener.IVcdDiagram;
import fr.inria.vcd.model.Description;
import fr.inria.vcd.model.MyColorApi;

public class FindAction extends Action {
	
	/**
	 * 
	 */ 
	private final IVcdDiagram ivcddiagram;

	public FindAction(IVcdDiagram ivcd) {
		super();
		this.ivcddiagram = ivcd;		
	}

	@Override
	public void run() {
		FindDialog inputDialog = new FindDialog(Display	.getCurrent().getActiveShell(), "Find",	"Clock name", "Instance", "",  ivcddiagram);
		int reponse;
		reponse = inputDialog.open();
		if (reponse == Window.OK) {
			Description d = new Description(inputDialog.getClock(),
					inputDialog.getInstance(), -1);
			for (Object obj : ivcddiagram.getCanvas().getContents()
					.getChildren()) {
				Panel panel = null;
				if (obj instanceof Panel)
					panel = (Panel) obj;
				else
					continue;
				for (Object f : panel.getChildren()) {
					IFigure poly = (IFigure) f;
					Description descr = ivcddiagram.getFactory()
							.getFigureForDescription().get(poly);
					if (descr == null ) //|| d == null)
						continue;
					if (d.equals(descr)) {
						int x;
						x = poly.getBounds().x;
						poly.setForegroundColor(MyColorApi.colorRedFind());
						ivcddiagram.getCanvas().scrollToX( x - (this.ivcddiagram.getCanvas().getSize().x / 2));
						ivcddiagram.getScale().scrollToX( x - (this.ivcddiagram.getScale().getSize().x / 2));
						ivcddiagram.getMarkerFactory().hideMarkerFireable(); //TODO
						ivcddiagram.getList().getListFind().add(poly);
					    ivcddiagram.setFocus(); // add
						return;
					}
				}
			}
			MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR| SWT.OK);
			messageBox.setText("Warning");   
			messageBox.setMessage("Figure introuvable !");
			messageBox.open();
		}
		 ivcddiagram.setFocus();
	}
}