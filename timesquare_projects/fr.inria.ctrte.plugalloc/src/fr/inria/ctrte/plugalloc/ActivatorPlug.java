/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 */
public class ActivatorPlug extends AbstractUIPlugin {

	/** The Constant PLUGIN_ID. */
	public static final String		PLUGIN_ID	= "fr.inria.ctrte.plugalloc";

	/** The plugin. */
	private static ActivatorPlug	plugin;

	/**
	 * The constructor.
	 */

	public ActivatorPlug()
	{
		super();
		plugin = this;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */

	MyEditorManagerUml		memu	= null;
	ConstraintDictionnary	c		= null;

	@Override
	public void start( BundleContext context ) throws Exception
	{
		super.start(context);
		c = ConstraintDictionnary.getDefault();
		// c.scanconstraint();
		// c.add(new AlternatesDialog());
		// c.add(new PrecedesDialog());
		// c.add(new DisjointDialog());
		// c.add(new FilterByDialog());
		// c.add(new FollowedByDialog());
		// c.add(new InterDialog());
		// c.add(new MinusDialog());
		// c.add(new PeriodicDialog());
		// c.add(new RestrictDialog());
		// c.add(new SampledDialog());
		// c.add(new SubClocking());
		// c.add(new SustainDialog());
		// c.add(new SyncDialog());
		// c.add(new TimerDialog());
		// c.add(new UnionDialog());

		memu = MyEditorManagerUml.getDefault();
		// memu.scaneditor();

		// memu.add(org.eclipse.uml2.uml.editor.presentation.UMLEditor.class,
		// new UmlDecode("DefaultUMLeditor"));
		// memu.add(
		// "fr.obeo.acceleo.gen.ui.editors.reflective.AcceleoReflectiveEditor",
		// new UmlDecode("DefaultUMLeditor"));
		// memu.add("com.cea.papyrus.core.editor.MultiDiagramEditor", new
		// PapyrusUmlDecode());
		// memu.add("org.eclipse.uml2.diagram.clazz.part.UMLDiagramEditor",new
		// GraphUmlDecode());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop( BundleContext context ) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the shared instance
	 */
	public static ActivatorPlug getDefault()
	{
		return plugin;
	}

}
