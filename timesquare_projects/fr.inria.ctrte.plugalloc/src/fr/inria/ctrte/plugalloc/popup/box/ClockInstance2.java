/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.box;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import code.CinterfaceUML;
import code.Clist;
import code.Clistenumerationliteral;
import code.CmyClass;
import code.CmyClock;
import code.CmyEnumeration;
import code.CmyEnumerationLiteral;
import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.baseuml.core.ClabelInput;
import fr.inria.ctrte.baseuml.core.ClabelInputID;
import fr.inria.ctrte.baseuml.core.Clabelcombo;
import fr.inria.ctrte.baseuml.icore.ICallback4Box;
import fr.inria.ctrte.baseuml.icore.IToolTips;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import grammaire.TestId;

/**
 * Generic dialog that allows user to add/remove items to an element, with a
 * field with code assist to choose element.
 */
public class ClockInstance2 extends Dialog  implements ICallback4Box
{



	public class UnitTooltips implements IToolTips
	{
		
		
		public UnitTooltips()
		{
			super();
			
		}

		
		public String getNewToolTips( Object o )
		{
			if (o instanceof CmyEnumerationLiteral)
				return ((CmyEnumerationLiteral) o).getInfo();
			return null;
		}
	}
	

	
	public class ClockTooltips implements IToolTips
	{
		
		
		public ClockTooltips()
		{
			super();
			
		}

		
		public String getNewToolTips( Object o )
		{
			if (o instanceof CmyClass)
			{
				return ((CmyClass) o).informationClock();
			}
			return null;
		}
	}

	final static private String		clas_defaut_tooltips	= "Choose a Clock type Classifier";
	final static private String		clas_name				= "Classifier";

	/**	 */
	private Clabelcombo				cl2						= null;
	/**	 */
	private Clabelcombo				cl3						= null;
	/** The clas. */
	private Clabelcombo				clas					= null;
	/** The linp. */
	private ClabelInput				linp					= null;
	/**	 */
	private Button					ok						= null;
	//private Button 					cancel 					= null;

	private CmyClass				classif					= null;

	private boolean					isdense					= false;

	private boolean					islogical				= false;

	private Clistenumerationliteral	le						= null;
	private Clistenumerationliteral	local					= null;
	private Clist					lst						= null;
	private CinterfaceUML			uml						= null;
	private CmyPackage				puml					= null;
	private CmyClock				cuml					= null;
	/** The value. */
	protected Object				value					= null;
	private Clist					clist					= null;
	final boolean					b;

	/**
	 * Instantiates a new clock instance2.
	 * 
	 * @param parentShell
	 *            the parent shell
	 * @param uml
	 *            the uml
	 */
	public ClockInstance2(Shell parentShell, CmyPackage umlin)
	{ 
		super(parentShell);
		b = false;
		setShellStyle(super.getShellStyle());
		this.uml = umlin;
		puml = umlin;
		lst = uml.getRootPackage().getAllElementlist().getStereotypedby("ClockType");
		lst.add(Cumlview.getidealClock());
	}

	public ClockInstance2(Shell parentShell, CmyClock umlin)
	{
		super(parentShell);
		b = true;
		setShellStyle(super.getShellStyle());
		this.uml =   umlin;
		cuml = umlin;
		lst = uml.getRootPackage().getAllElementlist().getStereotypedby("ClockType");
		lst.add(Cumlview.getidealClock());
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar( Composite parent )
	{

		//cancel = 
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL,	false);

		ok = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		// super.createButtonsForButtonBar(parent);
		ok.setEnabled(false);
	} 

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea( Composite parent )
	{
		Composite composite = parent;
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		clist = uml.getRootPackage().getAllElementlist().getStereotypedby("ClockType");

		le = Cumlcreator.getInstance().createUmlElement(Cumlview.getTimeStandardKind())
				.getEumerationliteral();

		linp = new ClabelInputID(composite, SWT.NONE, "name:");
		linp.setIcb(this);
		clas = new Clabelcombo(composite, clas_name, clas_defaut_tooltips, clist);
		clas.setItt(new ClockTooltips());
		clas.setIcb(this);
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		parent.setLayout(null);
		parent.setSize(520, 355);
		parent.setLayout(new GridLayout(1, true));  
		local = null;
		if (b && cuml.getClassifier() != null)
		{
			if (cuml.getClassifier().getUnitType() != null)
				local = cuml.getClassifier().getUnitType().getEumerationliteral();
		}
		cl2 = new Clabelcombo(parent, "Unit", "Choose a Unit of Clock", local);
		
		cl2.setItt(new UnitTooltips());
		cl3 = new Clabelcombo(parent, "Standard", "Choose Standard of Clock", le);
		cl3.setEnabled(false);
		cl3.setEnableCombo(false);
		cl3.setSelectCombo(0);
		if (b && (cuml.getClassifier() != null))
		{

			linp.setText(cuml.getName());			
			//linp.setTextEditable(false);
			linp.setTextBackground(org.eclipse.draw2d.ColorConstants.white);

			clas.setSelectCombo(lst.indexOf(cuml.getClassifier()));

			if (cuml.getUnit() != null && local != null)
				cl2.setSelectCombo(local.indexOf(cuml.getUnit()));
			if (cuml.getStandard() != null)
				cl3.setSelectCombo(le.indexOf(cuml.getStandard()));
			if (cuml.isChronometricClcok() );
			{
				cl3.setEnabled(true);
				cl3.setEnableCombo(true);
			}
			cl2.redraw();
			cl3.redraw();

		}
		parent.redraw();
		return parent;
	}

	// @Override
	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	public boolean isIsdense()
	{
		return isdense;
	}

	public boolean isIslogical()
	{
		return islogical;
	}

	/**
	 * Update classifier.
	 * 
	 * @return the int
	 */
	public int updateClassifier()
	{
		// cl2.removeAll();
		try
		{

			// general.folder.changed(null);
			
			classif= (CmyClass ) clas.getValueObject();
			clas.setToolTipText(classif.informationClock());
			CmyEnumeration cme = classif.getUnitType();
			if (cme == null)
				local = new Clistenumerationliteral();
			else
				local = cme.getEumerationliteral();
			cl2.setlist(local);
			cl2.setEnabled(true);
			cl2.setEnableCombo(true);
			cl2.setSelectCombo(0);

			islogical = classif.isLogicalClockType();
			isdense = classif.isDenseClockType();
			// tab2.base.setDisabledImage(null)
			if (islogical == false)
			{
				cl3.setEnabled(true);
				cl3.setEnableCombo(true);

			} else
			{
				cl3.setEnabled(false);
				cl3.setEnableCombo(false);
			}
			cl2.redraw();
			cl3.redraw();
			TestId ti= new TestId(linp.getValue());
			if (ok!=null)
			{
			if (ti.getnbrerror()!=0)
			{
				ok.setEnabled(false);
			}
			else
			{
				ok.setEnabled(true);	
			}
			}

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}
		return 0;
	}

	/**
	 * Apply.
	 * 
	 * @return the int
	 */
	public int apply( BaseDecode bd )
	{
		try
		{		
			//TODO rewrite
			if (!b)
			{
				cuml = puml.createInstance(linp.getValue());
			}
			cuml.applyClock();
			cuml.setName(linp.getValue());
			cuml.setTypeClassifier((CmyClass) clist.getC(clas.getValue()));
			cuml.setStandard(le.getC(cl3.getValue()));
			if (local != null)
				cuml.setUnit(local.getC(cl2.getValue()));
			bd.notifymodification();

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}
		return 0;
	}

	
	public int validation()
	{		
		updateClassifier();
		return 0;
	}
	
	

}
