/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputNumber;

/**
 * The Class RestrictDialog.
 */
public class RestrictDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new restrict dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public RestrictDialog()
	{
		super("Restriction");

	}

	/** The linp1. */
	private IInputNumber	linp1	= null;

	/** The linp2. */
	private IInputNumber	linp2	= null;

	// private Label info=null;
	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Super Clock", "Clock");

		clcls = createclock("Sub Clock", "Clock");

		linp1 = createInputNumber("within  ", false); // TODO Within change box
		// new ClabelInputNumber(getComposite(), SWT.NONE, "start ");
		// position(linp1);
		linp2 = createInputNumber("stop ", false);
		// new ClabelInputNumber(getComposite(), SWT.NONE, "stop");
		// position(linp2);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{
		// restrictedToTrailing
		// restrictedToLeading
		// restrictedToInterval
		String s = clcls.getValueString() + " = " + clcl.getValueString();
		s += " restrictedToInterval [" + linp1.getValue() + " .. " + linp2.getValue() + " ]";
		String s1 = " { \n" + s + " ; \n}\n"; 

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Synchrone;
	}

	@Override
	protected int validate()
	{
		return 0;
	}

}
