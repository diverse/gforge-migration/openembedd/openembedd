/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class SampledDialog.
 */
public class SampledDialog extends BaseConstraintApi {

	/** The trig. */
	private IComboClock	clcl, trig;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new sampled dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public SampledDialog()
	{
		super("Sampling");		
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Super Clock", "Clock");

		clcls = createclock("Sub Clock", "Clock");

		trig = createclock("Trigger ", "Clock");

		createModifierSingleton(false);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + clcls.getValueString() + " = " + trig.getValueString()
				+ getModifierselected() + " sampledOn " + clcl.getValueString() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Mixed;
	}

	@Override
	public ImageDescriptor geticone()
	{

		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/sampled16.png");
	}

	@Override
	protected int validate()
	{
		
		return 0;
	}

}
