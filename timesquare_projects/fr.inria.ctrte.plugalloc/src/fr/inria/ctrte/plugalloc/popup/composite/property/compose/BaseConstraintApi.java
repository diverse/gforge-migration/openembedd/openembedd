/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import junit.framework.Assert;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.junit.Test;

import code.Cbase;
import code.Clist;
import code.CmyClock;
import element.dictionnaire.Cvariable2;
import fr.inria.ctrte.baseuml.core.Ccheckbox;
import fr.inria.ctrte.baseuml.core.ClabelInputBword;
import fr.inria.ctrte.baseuml.core.ClabelInputNumber;
import fr.inria.ctrte.baseuml.core.Clabelcombo;
import fr.inria.ctrte.baseuml.core.ClabelcomboClock;
import fr.inria.ctrte.baseuml.icore.ICallback4Box;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputBword;
import fr.inria.ctrte.baseuml.icore.IInputNumber;
import fr.inria.ctrte.baseuml.test.TestBword;
import fr.inria.ctrte.baseuml.test.TestClock;
import fr.inria.ctrte.baseuml.test.TestNumber;
import fr.inria.ctrte.plugalloc.popup.composite.property.IDialogError;
import fr.inria.ctrte.plugalloc.popup.work.Cinputrelation2;
import grammaire.TestSyntax;

/**
 * The Class BaseConstraintApi.
 */
public abstract class BaseConstraintApi implements ICallback4Box {


	/** The name. */
	private final String			name;
	private String					comment				= "";

	/** The c. */
	private Composite				basec;

	/** The active. */
	private boolean					active;  

	/** The listelementuml. */
	private Clist					listelementuml;

	/** The modere. */
	private Clabelcombo				modere				= null;

	private Ccheckbox				groupcheck			= null;
	/** The moderatorkeywork. */

	private List<ClabelcomboClock>	lscomboclockext;
	private List<ClabelcomboClock>	lscomboclock;
	private int						p					= 0;
	protected boolean				haveerror			= false;
	protected int					colorcoderror		= 0;
	protected String				messagestr			= "" ; //not yet implemented";
	protected boolean other=false ;

	/*private class ValidateListener implements Listener {
		@Override
		public void handleEvent( Event event )
		{

			validation();
		}
	}*/
	int localMaxClockMumber=0;
	
	/**
	 * @param localMaxClockMumber the number maximun of local clock .
	 */
	protected final void setLocalMaxClockMumber( int localMaxClockMumber )
	{
		this.localMaxClockMumber = localMaxClockMumber;
	}

	private final class Ccheckbox4Group extends Ccheckbox {
		private Ccheckbox4Group(Composite parent, String namein, String tooltipin, boolean b)
		{
			super(parent, namein, tooltipin, b);
		}

		@Override
		protected int computenewvalue( boolean b )
		{

			for (ClabelcomboClock c : lscomboclockext)
			{
				c.setExtendby(b);
				c.redraw();
				c.update();
			}
			return 0;
		}
	}

	/**
	 * The Enum typeConstrain.
	 */
	public enum TypeConstrain {

		Synchrone(0, "Synchronous"), Asynchrone(1, "Asynchronous"), Mixed(2, "Mixed"),
		NFPs(3,"NFPs"),	Miscellaneous(4, "Miscellaneous");

		private int		n;
		private String	s;

		/**
		 * Instantiates a new type constrain.
		 * 
		 * @param id
		 * @param name
		 */
		TypeConstrain(int id, String name)
		{
			n = id;
			s = name;
		}

		/**
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return s;
		}

		/**
		 * Gets the iD.
		 * 
		 * @return the iD
		 */
		public int getID()
		{
			return n;
		}

	}
	
	public enum Modifier {

		strictly(0, " strictly "), 
		leftNonStrictly(1, " leftNonStrictly " ), 
		rightNonStrictly(2, " rightNonStrictly "), 
		nonStrictly(3," nonStrictly ");

		private int		n;
		private String	s;

		/**
		 * Instantiates a new type constrain.
		 * 
		 * @param id
		 * @param name
		 */
		Modifier(int id, String name)
		{
			n = id;
			s = name;
		}

		/**
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString()
		{
			return s;
		}

		/**
		 * Gets the iD.
		 * 
		 * @return the iD
		 */
		public int getID()
		{
			return n;
		}

	}
	/**
	 * Draw.
	 * 
	 * @param c
	 *            the c
	 * 
	 * @return the int
	 */
	protected IDialogError	idge	= null;

	/**
	 * Instantiates a new base constraint api.
	 * 
	 * @param name
	 *            the name
	 * @param ls
	 *            the ls
	 */
	protected BaseConstraintApi(String name)
	{
		super();
		this.name = name;
		this.basec = null;
		listelementuml = null;
		lscomboclockext = new ArrayList<ClabelcomboClock>();
		lscomboclock = new ArrayList<ClabelcomboClock>();
	}

	protected abstract void mydraw();

	

	abstract protected int validate();

	/**
	 * Gets the contrainttext.
	 * 
	 * @return the contrainttext <b>abstract</b>
	 */
	public abstract String getContraintText();

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	abstract public TypeConstrain getType();

	/**
	 * Gets the icone.
	 * 
	 * @return the icone <b>abstract</b>
	 */
	public ImageDescriptor geticone()
	{
		return ImageDescriptor.createFromImage(Cbase.IMG_PROPERTY);
	}

	/**
	 * Make.
	 * 
	 * @return the cinputrelation2
	 */
	public final Cinputrelation2 make()
	{
		String s1 = getContraintText();
		Cinputrelation2 x = new Cinputrelation2(s1, getListClock());
		return x;
	}

	/**
	 * Sets the active.
	 * 
	 * @param b
	 *            the b
	 * 
	 * @return the int
	 */
	public final int setActive( boolean b )
	{
		active = b;
		basec.setVisible(b);
		basec.pack();
		return 0;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	final public String getName()
	{
		return name;
	}

	final public String getComment()
	{
		return comment;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return true, if is active
	 */
	final public boolean isActive()
	{
		return active;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{

		return name;
	}

	/**
	 * Gets the le.
	 * 
	 * @return the listelementuml
	 */
	public final Clist getLe()
	{
		return listelementuml;
	}

	/**
	 * Sets the le.
	 * 
	 * @param le
	 *            the le
	 */
	public final void setLe( Clist le )
	{
		this.listelementuml = le;
	}

	/**
	 * Gets the moderatorselected.
	 * 
	 * @return the moderatorselected
	 */
	public final Modifier getModifierselected()
	{
		if (test)
		{
			if (testmoderate==1)
			{
				//System._err.println("\t<<Dont Cant modifier without create >>");
				Assert.fail("Dont Cant modifier without create ");
			}
			return mdtest;
		}
		if (modere == null)
		{
			
			throw new  RuntimeException("Dont Cant modifier without create ");
		}
		int v= modere.getValue();
		if (modifiermode)
		{
			return Modifier.values()[v];
		}
		if (v==0)
			return Modifier.strictly;
		return Modifier.nonStrictly;
		
		
	}

	/**
	 * 
	 * @return
	 */
	public final int validation()
	{

		validate();
		if (idge != null)
		{
			idge.setError(messagestr, !haveerror, colorcoderror);
		}
		return 0;
	}

	/**
	 * @return the other
	 */
	public final  boolean isOther()
	{
		return other || !isNative();
	}

	/**
	 * @param other the other to set
	 */
	public final void  setOther( boolean other )
	{
		this.other = other;
	}

	/**
	 * 
	 * @param c
	 * @param idg
	 * @return
	 */
	final public int draw( Composite c , IDialogError idg )
	{
		if (test)
		{
			mydraw();
			return 0;
		}
		n=0;
		haveerror = false;
		colorcoderror = 0;
		messagestr = "test";
		idge = idg;
		modere = null;
		groupcheck = null;
		this.basec = c;
		lscomboclock.clear();
		lscomboclockext.clear();
		p = 0;
		/*****/
		mydraw();
		/***/
		boolean b = false;
		for (ClabelcomboClock l : lscomboclockext)
		{
			if (l.isActiveGroup())
			{
				b = true;
				break;
			}
		}
		if (b)
			createCheckboxGroupSingleton();
		return 0;
	}

	protected final void setComment( String comment )
	{
		if (comment == null)
			comment = "";
		this.comment = comment;
	}

	/**
	 * Gets the clock.
	 * 
	 * @param n
	 *          
	 * 
	 * @return the clock
	 */
	protected final CmyClock getClock( int n )
	{
		return (CmyClock) listelementuml.getC(n);
	}


	/**
	 * Gets the clock name.
	 * 
	 * @param n
	 *            the n
	 * 
	 * @return the clock name
	 */


	private final int position( Control c )
	{
		c.setVisible(true);
		c.setLocation(10, 10 + 30 * p);
		p++;
		return 0;
	}

	protected final IInputNumber createInputNumber( String name, boolean optional )
	{
		if (test)
			return new TestNumber();
		ClabelInputNumber ci = new ClabelInputNumber(basec, SWT.NONE, name);
		ci.setIcb(this);
		position(ci);
		return ci;
	}

	protected final IInputBword createInputBword( String name )
	{
		if (test)
			return new TestBword();
		ClabelInputBword ci = new ClabelInputBword(basec, SWT.NONE, name);
		ci.setIcb(this);
		position(ci);
		return ci;
	}

	protected final IComboClock createclockExtend( String name , String tooltip )
	{
		if (test)
			return new TestClock(n++,true);
		ClabelcomboClock l = new ClabelcomboClock(basec, name, tooltip, listelementuml, true);
		l.setIcb(this);
		position(l);
		l.setSelectCombo(0);
		lscomboclockext.add(l);
		lscomboclock.add(l);
		return l;
	}

	protected final IComboClock createclock( String name , String tooltip )
	{
		if (test)
			return new TestClock(n++,false);
		return createclock(name, tooltip, false);
	}

	protected final IComboClock createclock( String name , String tooltip , boolean optional )
	{
		if (test)
			return new TestClock(n++,false);
		ClabelcomboClock l = new ClabelcomboClock(basec, name, tooltip, listelementuml, false);
		l.setIcb(this);
		position(l);
		l.setSelectCombo(0);
		lscomboclock.add(l);
		return l;
	}

	/**
	 * Moderator.
	 * 
	 * @param c
	 *            the c
	 * @param b
	 *            the b
	 * 
	 * @return the clabelcombo
	 */
	boolean modifiermode=false;
	protected final void  createModifierSingleton( boolean b )
	{
		modifiermode=b;
		if (test)
		{
			if (b)
				testmoderate=4;
			else
				testmoderate=2;
			return ;
		}
		if (modere != null)
		{

			return ;
		}
		Clabelcombo tmp = null;
		tmp = new Clabelcombo(basec, "Modifier", "", (String[]) null);
		tmp.removeCombo(0);
		tmp.addCombo(Modifier.strictly.toString());
		if (b)
		{
			tmp.addCombo(Modifier.leftNonStrictly.toString());
			tmp.addCombo(Modifier.rightNonStrictly.toString());
		} 			
		tmp.addCombo(Modifier.nonStrictly.toString());
		tmp.setVisible(true);
		tmp.setSelectCombo(0);
		modere = tmp;
		modere.setIcb(this);
		position(tmp);
		return ;
	}

	private final Ccheckbox createCheckboxGroupSingleton()
	{
		if (groupcheck != null)
		{
			return groupcheck;
		}
		Ccheckbox x = new Ccheckbox4Group(basec, "Option", "group", false);
		x.setIcb(this);
		position(x);
		groupcheck = x;
		return x;
	}

	protected final CmyClock[] getListClock()
	{
		List<CmyClock> l = new ArrayList<CmyClock>();
		for (ClabelcomboClock clcc : lscomboclock)
		{
			CmyClock ci = clcc.getValueObject();
			if ((ci != null) && (!l.contains(ci)))
				l.add(ci);
		}
		return l.toArray(new CmyClock[] {});

	}
	
	public final boolean isNative()
	{
		String s=getClass().getName();
		n = s.lastIndexOf(".");
		if (n==-1)
			return false;
		String sub = s.substring(0, n);
		if (sub.equals("fr.inria.ctrte.plugalloc.popup.composite.property.compose")) return true ;
		return false;
	}
	
	
	public final String localclock ( int id)
	{
		if (id<localMaxClockMumber && id >=0)
			return "local"+id;
		throw new RuntimeException( "id >= localMaxClockMumber");
	}
	
	/** Test API check grammar */
	int n=0;
	private  boolean test=false;
	private  int testmoderate=1;
	Modifier mdtest=null;
	
	
	
	@Test
	final public void testGrammar()
	{		
		try
		{
		System.out.println("Test Grammar : " + getClass().getSimpleName());		
		test=true;
		mydraw();		
		switch (testmoderate) {
			case 1: // no moderator
				internaltest1(null);
				break;
			case 2: //  moderator strictly / nonStricly				
				internaltest1(Modifier.strictly);
				internaltest1(Modifier.nonStrictly);
				break;
			case 4:// all  moderator 
				internaltest1(Modifier.strictly);
				internaltest1(Modifier.leftNonStrictly);
				internaltest1(Modifier.rightNonStrictly);
				internaltest1(Modifier.nonStrictly);
				break;
			default:
				break;
		}		
		}
		catch ( Exception e)
		{
			Assert.fail(getClass().getName() + " failed grammar test " + e);
			
		}	
	}
	
	
	final private void internaltest1(Modifier md)
	{	
		mdtest=md;
		System.out.println("\tModifier : " + mdtest ) ;
		String s= getContraintText();			
		Assert.assertNotNull("Constraint", s);
		TestSyntax ts= new TestSyntax(s);
		if (ts.getnbrerror()!=0)
			{
			System.out.println(s);
			Assert.assertEquals(getClass().getName() +": "+mdtest + " failed :" +ts.getnbrerror(),0,ts.getnbrerror() );					
			}			
		for (Entry<String, Cvariable2> es: ts.getDico().getClkdic().get().entrySet())
		{
			System.out.println(es.getKey() +" "+( es.getValue().local!=0));
		}
		
	}
	
	@Test
	final public void testType()
	{
		if (getType()==null)
			Assert.fail(getClass().getName() + " failed type test" );
	}
	

}
