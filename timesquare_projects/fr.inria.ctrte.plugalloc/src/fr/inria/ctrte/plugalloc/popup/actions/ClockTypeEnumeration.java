/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.actions;

/**
 * The Enum ClockTypeEnumeration.
 */
public enum ClockTypeEnumeration {

	/** The Logical p. */
	LogicalP(true, Base.Package, "Create Logical Clock on Package"),

	/** The Chrono p. */
	ChronoP(false, Base.Package, "Create Chronometric Clock on Package"),

	/** The Logical c. */
	LogicalC(true, Base.Classe, "Create Logical Clock "),

	/** The Chrono c. */
	ChronoC(false, Base.Classe, "Create Chronometric Clock "),

	/** The Stereo logical. */
	StereoLogical(true, Base.Classe, false, "Applied Stereotype (ClockTime:logical) on Class"),

	/** The Stereo chrono. */
	StereoChrono(false, Base.Classe, false, "Applied Stereotype (ClockTime:chronometric) on Class"),

	/** The Stereo. */
	StereoC(null, Base.Classe, false, "Applied Stereotype on Class"),

	/** new Sterotype on */
	StereoP(null, Base.Package, true, "Applied Stereotype on new Class Package");

	/** The logical. */
	private Boolean	logical	= null;

	/** The base. */
	private Base	base	= null;

	/** The newc. */
	private boolean	newc;

	/** The info. */
	private String	info	= null;

	/**
	 * The Enum Base.
	 */
	public enum Base {

		/** The Package. */
		Package("Package", 0),

		/** The Classe. */
		Classe("Classe", 1);

		/** The s. */
		String	s;

		/** The n. */
		int		n;

		/**
		 * Instantiates a new base.
		 * 
		 * @param s
		 *            the s
		 * @param n
		 *            the n
		 */
		private Base(String s, int n)
		{
			this.s = s;
			this.n = n;

		}

	}

	/**
	 * Instantiates a new clock type enumeration.
	 * 
	 * @param logical
	 *            the logical
	 * @param base
	 *            the base
	 * @param inf
	 *            the inf
	 */
	private ClockTypeEnumeration(Boolean logical, Base base, String inf)
	{
		this.logical = logical;
		this.base = base;
		newc = true;
		info = inf;
	}

	/**
	 * Instantiates a new clock type enumeration.
	 * 
	 * @param logical
	 *            the logical
	 * @param base
	 *            the base
	 * @param create
	 *            the create
	 * @param inf
	 *            the inf
	 */
	private ClockTypeEnumeration(Boolean logical, Base base, boolean create, String inf)
	{
		this.logical = logical;
		this.base = base;
		newc = create;
		info = inf;
	}

	/**
	 * Gets the logical.
	 * 
	 * @return the logical
	 */
	public Boolean getLogical()
	{
		return logical;
	}

	/**
	 * Gets the base.
	 * 
	 * @return the base
	 */
	public Base getBase()
	{
		return base;
	}

	/**
	 * Checks if is newc.
	 * 
	 * @return true, if is newc
	 */
	public boolean isNewc()
	{
		return newc;
	}

	/**
	 * Checks if is stereotype.
	 * 
	 * @return true, if is stereotype
	 */
	public boolean isStereotype()
	{
		return !newc;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString()
	{
		if (info != null)
			return info;
		return super.toString();
	}

}
