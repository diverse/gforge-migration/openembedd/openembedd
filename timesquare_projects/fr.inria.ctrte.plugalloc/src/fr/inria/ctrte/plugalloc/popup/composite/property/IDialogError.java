package fr.inria.ctrte.plugalloc.popup.composite.property;

public interface IDialogError {

	public void setError( String message , boolean error , int value );
}
