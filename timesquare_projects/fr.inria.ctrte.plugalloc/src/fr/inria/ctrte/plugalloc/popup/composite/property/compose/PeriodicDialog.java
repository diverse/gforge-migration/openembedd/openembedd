/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputNumber;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class PeriodicDialog.
 */
public class PeriodicDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new periodic dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public PeriodicDialog()
	{
		super("Periodic");

	}

	/** The linp1. */
	private IInputNumber	linp1	= null;

	/** The linp2. */
	private IInputNumber	linp2	= null;

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Super Clock", "Clock");
		clcls = createclock("Sub Clock", "Clock");
		linp1 = createInputNumber("period ",true);
		// new ClabelInputNumber(getComposite(), SWT.NONE, "period ");
		// position(linp1);
		linp2 = createInputNumber("offset",true);
		// new ClabelInputNumber(getComposite(), SWT.NONE, "offset");
		// position(linp2);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String offset = "";
		String periode = "";
		
	
		if (!linp1.isMissing())
		{
			periode = " period " + linp1.getValue();
			if (!linp2.isMissing())
			{
				offset = " offset " + linp2.getValue(); 
			}
		}

		String s1 = " { \n" + clcls.getValueString() + " isPeriodicOn " + clcl.getValueString()
				+ periode + offset + " ; \n}\n"; 

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Synchrone;
	}

	@Override
	public ImageDescriptor geticone()
	{

		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/periodic.png");
	}

	@Override
	protected int validate()
	{
		
		return 0;
	}

}
