/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.box;

import java.util.ArrayList;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Package;

import code.Clist;
import code.CmyPackage;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.baseuml.core.ClabelInput;
import fr.inria.ctrte.baseuml.core.Clabelcombo;
import fr.inria.ctrte.plugalloc.popup.actions.ClockTypeEnumeration;
import fr.inria.ctrte.plugalloc.popup.work.Ccreateclock;
import fr.inria.ctrte.plugalloc.popup.work.Iclock;
import fr.inria.ctrte.plugalloc.popup.work.StereotypeClock;

/**
 * Generic dialog that allows user to add/remove items to an element, with a
 * field with code assist to choose element.
 */
public final class ClockTypeInstance2 extends Dialog {

	/** The cl1b. */
	private Clabelcombo				cl1b	= null;

	/** The cl3. */
	private Clabelcombo				cl3		= null;

	/** The mylst. */
	private Clist					mylst	= null;

	/** The ls. */
	private ClabelInput				ls		= null;

	/** The info. */
	private ClockTypeEnumeration	info;

	/** The uml. */
	private Cumlelement				uml		= null;

	/** The value. */
	protected Object				value	= null;

	/** The lint. */
	private java.util.List<Integer>	lint	= new ArrayList<Integer>();

	/** The lstr. */
	private java.util.List<String>	lstr	= new ArrayList<String>();

	/**
	 * Instantiates a new clock type instance2.
	 * 
	 * @param parentShell
	 *            the parent shell
	 * @param uml
	 *            the uml
	 * @param n
	 *            the n
	 * @param cte
	 *            the cte
	 */
	public ClockTypeInstance2(Shell parentShell, Cumlelement uml, int n, ClockTypeEnumeration cte)
	{
		super(parentShell);
		setShellStyle(super.getShellStyle());
		this.uml = uml;
		info = cte;
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar( Composite parent )
	{
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);

	}

	/**
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell( Shell shell )
	{
		super.configureShell(shell);
		shell.setText(info.toString());
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea( Composite parent )
	{

		GridLayout layout = new GridLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		layout.verticalSpacing = 10;
		layout.horizontalSpacing = 10;

		parent.setLayout(layout);

		parent.setLayoutData(new GridData(GridData.FILL));

		ls = new ClabelInput(parent, SWT.NONE, "name :");
		if (!info.isNewc() && (uml.getName() != ""))
		{
			ls.setText(uml.getName());
			ls.setTextEditable(false);
			ls.setTextBackground(ColorConstants.white);
		}

		if (info.getLogical() == null)
		{
			cl1b = new Clabelcombo(parent, "Type & Nature", "Nature : dense or discrete",
					new String[] { "Logical Clock", "Chronometric Clock discrete",
							"Chronometric Clock dense" });
			cl1b.setSelectCombo(0);
		} else if (info.getLogical() == false)
		{
			cl1b = new Clabelcombo(parent, "Nature", "Nature : dense or discrete", new String[] {
					"discrete", "dense" });
			cl1b.setSelectCombo(0);
		}
		initmylst();
		initUnitKind(parent);
		mylst.disp();

		return parent;
	}

	/**
	 * Initmylst.
	 * 
	 * @return the int
	 */
	private int initmylst()
	{
		CmyPackage cmp = uml.getModelof();
		if (cmp == null)
		{
			cmp = uml.getRootPackage();
			if (cmp == null)
				return 0;
		}
		mylst = cmp.getAllEnumeration().GetUnitEnumeration();
		if (mylst.size() == 0)
		{

			Cumlview.importUnitKind(cmp);
			mylst = cmp.getAllEnumeration().GetUnitEnumeration();

		}
		return 0;
	}

	/**
	 * Inits the unit kind.
	 * 
	 * @param parent
	 *            the parent
	 * 
	 * @return the int
	 */
	private int initUnitKind( Composite parent )
	{
		String s[] = mylst.toArrayStringAuto();
		cl3 = new Clabelcombo(parent, "Unit Kind", "Choose a UnitKind ", s);
		int x = 0;
		for (int i = 0; i < s.length; i++)
		{
			if (s[i].compareTo("TimeUnitKind") == 0)
			{
				x = i;
			}
			if (s[i].compareTo("MARTE_Library::MeasurementUnits::TimeUnitKind") == 0)
			{
				x = i;
				break;
			}
		}
		cl3.setSelectCombo(x);
		return 0;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	/** The stag. */
	public final String	stag[]	= new String[] { "resolAttrib", "offsetAttrib", "maxValAttrib" };

	/**
	 * Apply.
	 * 
	 * @return the int
	 */
	public int apply()
	{
		String name = ls.getValue();
		Iclock c;
		try
		{
			if (info.isNewc())
				c = new Ccreateclock(name, (Package) uml.getElement(), typelogical(), nature(),
						mylst.get(cl3.getValue()));
			else
				c = new StereotypeClock(name, (Class) uml.getElement(), typelogical(), nature(),
						(Enumeration) mylst.get(cl3.getValue()));

			int n = lint.size();
			for (int i = 0; i < n; i++)
			{
				c.setpropertyvalue(stag[lint.get(i)], lstr.get(i));
			}
		} catch (Exception ex)
		{
			MyManager.printError(ex);

		}
		return 0;
	}

	/**
	 * Typelogical
	 * 
	 * @return true, if successful
	 */
	private boolean typelogical()
	{
		if (info.getLogical() != null)
			return info.getLogical();		
		if (cl1b.getValue() == 0)
			return true;
		return false;

	}

	/**
	 * Nature
	 * 
	 * @return the boolean
	 */
	private Boolean nature()
	{
		if (cl1b == null)
			return false;
		if (info.getLogical() == null)
			return (cl1b.getValue() == 2);
		if (info.getLogical() == false)
			return (cl1b.getValue() == 1);

		return false;
	}

}
