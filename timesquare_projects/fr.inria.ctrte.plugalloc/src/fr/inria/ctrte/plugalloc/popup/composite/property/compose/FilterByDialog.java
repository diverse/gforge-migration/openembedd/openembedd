/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputBword;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class FilterByDialog.
 */
public class FilterByDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new filter by dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public FilterByDialog()
	{
		super("Filtering");

	}

	/** The linp1. */
	private IInputBword	linp1	= null;

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Super Clock", "Clock");
		clcls = createclock("Sub Clock", "Clock");
		linp1 = createInputBword("Filter");
		// new ClabelInputBword(getComposite(), SWT.NONE, "Filtering by:");
		// position(linp1);

	}


	@Override
	public String getContraintText()
	{
		String s1 = " { \n" + clcls.getValueString() + " = "
				+ clcl.getValueString() +  " filteredBy " + linp1.getValue() + " ; \n}\n";		
		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Synchrone;
	}
	
	
	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/filter.png");
	}

	@Override
	protected int validate()
	{
		if (clcl.getValue() == clcls.getValue() || (linp1.getValue().indexOf("1")==-1) )
		{
			haveerror = true;
		
			messagestr = "Invalid specification : super must be different from sub  and filter must not be null  ";
		}
		else
		{
		
			haveerror = false;
			messagestr = " Ok";
		}
		return 0;
	}

}
