/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.work;

import code.Cumlelement;

/**
 * The Interface Iclock.
 */
public interface Iclock {

	/**
	 * Setpropertyvalue.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * 
	 * @return the int
	 */
	public abstract int setpropertyvalue( String name , String value );

	/**
	 * Creates the operation.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return the int
	 */
	public abstract int createOperation( String name );
	
	public abstract  Cumlelement getElement();
	

}