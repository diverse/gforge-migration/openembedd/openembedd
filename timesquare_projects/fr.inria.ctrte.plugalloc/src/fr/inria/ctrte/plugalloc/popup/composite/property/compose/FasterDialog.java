/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputNumber;

/**
 * The Class RestrictDialog.
 */
public class FasterDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new restrict dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public FasterDialog()
	{
		super("FasterThan");

	}

	/** The linp1. */
	@SuppressWarnings("unused")
	private IInputNumber	linp1	= null;

	/** The linp2. */
	@SuppressWarnings("unused")
	private IInputNumber	linp2	= null;

	// private Label info=null;
	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Super Clock", "Clock");
		clcls = createclock("Sub Clock", "Clock");
		linp1 = createInputNumber("within  ", false);// TODO Within change box		
		linp2 = createInputNumber("stop ", false);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{
		// restrictedToTrailing
		// restrictedToLeading
		// restrictedToInterval
		String s = clcls.getValueString()  +
		" isFasterThan "+ clcl.getValueString() ; //+ " within" + linp1.getValue() + " .. " + linp2.getValue() + " ";
		String s1 = " { \n" + s + " ; \n}\n"; // TODO within add

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Synchrone;
	}

	@Override
	protected int validate()
	{
		return 0;
	}

}
