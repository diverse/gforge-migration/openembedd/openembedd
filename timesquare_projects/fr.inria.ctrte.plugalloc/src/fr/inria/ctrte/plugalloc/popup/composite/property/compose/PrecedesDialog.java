/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class AlternatesDialog.
 */
public class PrecedesDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new alternates dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public PrecedesDialog()
	{
		super("Precedence");

	}

	/** The linp1. */
//	private ClabelInputBword	linp1	= null;

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclockExtend("First", "Clock");

		clcls = createclockExtend("Second", "Clock");

		clcls.setSelectCombo(0);

		createModifierSingleton(false);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{
		String s1 = " { \n" + clcl.getValueString() + getModifierselected() + " precedes "
				+ clcls.getValueString() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Asynchrone;
	}

	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/precedence.png");
	}
	
	@Override
	protected int validate()
	{
		
		return 0;
	}

	/*
	 * @Override public ImageDescriptor geticone() {
	 * 
	 * return ImageDescriptor.createFromImage(ImageManager.getImage(
	 * ActivatorPlug.PLUGIN_ID, "icons/alternates.png")); }
	 */

}
