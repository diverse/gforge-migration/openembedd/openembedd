/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class MinusDialog.
 */
public class InfDialog extends BaseConstraintApi {

	/** The a. */
	private IComboClock	a;

	/** The b. */
	private IComboClock	b;

	/** The c. */
	private IComboClock	c;

	/**
	 * Instantiates a new minus dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public InfDialog()
	{
		super("Inf");    

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{
		a = createclock("result", "Clock");
		b = createclock("b Clock", "Clock");
		c = createclock("c Clock", "Clock");  

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + a.getValueString() + " = inf ( " + b.getValueString()
				+ " , " + c.getValueString() + " ); \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Mixed;
	}

	@Override
	protected int validate()
	{
		
		return 0;
	}
	
	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/inf.png");
	}
}
