/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.actions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

import code.Clist;
import code.Cliststereotype;
import code.CmessageBox;
import code.CmyPackage;
import code.CmyStereotype;
import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.popup.box.MyDialog;

// import com.sun.jndi.url.dns.dnsURLContext;

/**
 * The Class AbstrationPlatform.
 */
public class AbstrationPlatform implements IObjectActionDelegate {

	// private JFrame frame=null;
	/** The sel. */
	private Abstraction		sel				= null;
	// private Cumlelement cli = null, sur = null;
	/** The sh. */
	private Shell			sh				= null;

	/** The d. */
	private MyDialog		d				= null;

	/** The c. */
	private Clist			c				= null;

	/** The s. */
	private Clist			s				= null;

	/** The clientapplied. */
	private Clist			clientapplied	= null;

	/** The supplierapplied. */
	private Clist			supplierapplied	= null;

	/** The lc. */
	private EList<Element>	lc;

	/** The ls. */
	private EList<Element>	ls;

	/**
	 * Message box.
	 * 
	 * @param s
	 *            the s
	 * @param s1
	 *            the s1
	 * 
	 * @return the int
	 */
	int MessageBox( String s , String s1 )
	{
		MessageDialog.openInformation(new Shell(), CmessageBox.pluginTime + s, s1);
		return 0;
	}

	/**
	 * Gets the supplier stereotype list allocate from.
	 * 
	 * @param e
	 *            the e
	 * 
	 * @return the supplier stereotype list allocate from
	 */
	@SuppressWarnings("unchecked")
	public EList<Element> getSupplierStereotypeListAllocateFrom( Cumlelement e )
	{

		return (EList<Element>) e.getAttributeStereotype("ExecutionPlatformAllocationEnd",
				"allocatedFrom");
	}

	/**
	 * Gets the client stereotype list allocate to.
	 * 
	 * @param e
	 *            the e
	 * 
	 * @return the client stereotype list allocate to
	 */
	@SuppressWarnings("unchecked")
	public EList<Element> getClientStereotypeListAllocateTo( Cumlelement e )
	{

		return (EList<Element>) e.getAttributeStereotype("ApplicationAllocationEnd", "allocatedTo");
	}

	/**
	 * Gets the supplier stereotype list allocate from.
	 * 
	 * @param e
	 *            the e
	 * 
	 * @return the supplier stereotype list allocate from
	 */
	public EList<Element> getSupplierStereotypeListAllocateFrom( Element e )
	{

		return getSupplierStereotypeListAllocateFrom(Cumlcreator.getInstance().createUmlElement(e));
	}

	/**
	 * Gets the client stereotype list allocate to.
	 * 
	 * @param e
	 *            the e
	 * 
	 * @return the client stereotype list allocate to
	 */
	public EList<Element> getClientStereotypeListAllocateTo( Element e )
	{

		return getClientStereotypeListAllocateTo(Cumlcreator.getInstance().createUmlElement(e));
	}

	/**
	 * Clientchange.
	 * 
	 * @return the int
	 */
	public int clientchange()
	{
		d = new MyDialog(sh, "sel", c, clientapplied, "Applicable Application Allocation: ",
				"Applied Application Allocation : ","");
		d.open();
		if (d.getReturnCode() == Window.OK)
		{
			for (Cumlelement e : d.remoded().getLne())
			{
				EList<Element> local = getClientStereotypeListAllocateTo(e);

				sel.getClients().remove(e.getElement());
				for (Cumlelement sup : supplierapplied.getLne())
				{
					getSupplierStereotypeListAllocateFrom(sup).remove(e.getElement());
					local.remove(sup.getElement());
				}

			}
			for (Cumlelement e : d.added().getLne())
			{
				sel.getClients().add((NamedElement) e.getElement());
				EList<Element> local = getClientStereotypeListAllocateTo(e);

				for (Cumlelement sup : supplierapplied.getLne())
				{

					if (getSupplierStereotypeListAllocateFrom(sup).contains(e.getElement()) == false)
						getSupplierStereotypeListAllocateFrom(sup).add(e.getElement());
					// if (local.contains(sup.getElement()) == false)
					local.add(sup.getElement());
				}
			}
		}
		return 0;
	}

	/**
	 * Supplierchange.
	 * 
	 * @return the int
	 */
	public int supplierchange()
	{
		d = new MyDialog(sh, "sel", s, supplierapplied, " Execution platform : ",
				" Execution platform : ","");
		d.open();

		if (d.getReturnCode() == Window.OK)
		{
			for (Cumlelement e : d.remoded().getLne())
			{
				sel.getSuppliers().remove(e.getElement());
				EList<Element> local = getSupplierStereotypeListAllocateFrom(e);
				for (Cumlelement cli : clientapplied.getLne())
				{
					getClientStereotypeListAllocateTo(cli).remove(e.getElement());
					local.remove(cli.getElement());
				}

			}
			for (Cumlelement e : d.added().getLne())
			{
				sel.getSuppliers().add((NamedElement) e.getElement());
				EList<Element> local = getSupplierStereotypeListAllocateFrom(e);
				clientapplied.disp();
				for (Cumlelement cli : clientapplied.getLne())
				{
					if (getClientStereotypeListAllocateTo(cli).contains(e.getElement()) == false)
						getClientStereotypeListAllocateTo(cli).add(e.getElement());
					// if (local.contains(cli.getElement())==false)
					local.add(cli.getElement());

				}

			}
		}
		return 0;
	}

	/**
	 * Listing.
	 * 
	 * @param m
	 *            the m
	 * 
	 * @return the int
	 */
	public int listing( CmyPackage m )
	{
		c = new Clist();
		s = new Clist();
		clientapplied = new Clist();
		supplierapplied = new Clist();
		for (NamedElement e : sel.getClients())
		{
			clientapplied.add(e);
		}
		for (NamedElement e : sel.getSuppliers())
		{
			supplierapplied.add(e);
		}

		for (Cumlelement e : m.getMembres().getLne())
		{
			if (e.isStereotypedBy("ApplicationAllocationEnd"))
			{
				if (clientapplied.contient(e.getElement()) == false)
					c.add(e);
			}
			if (e.isStereotypedBy("ExecutionPlatformAllocationEnd"))
			{
				if (supplierapplied.contient(e.getElement()) == false)
					s.add(e);
			}
		}
		return 0;
	}

	/**
	 * Sync.
	 * 
	 * @return the int
	 */
	public int sync()
	{
		for (Cumlelement c1 : clientapplied.getLne())
		{

			lc = getClientStereotypeListAllocateTo(c1);
			if (lc != null)
				for (Cumlelement s1 : supplierapplied.getLne())
				{
					Element se = s1.getElement();
					if (lc.contains(se) == false)
					{
						lc.add(se);
					}
				}
		}
		for (Cumlelement s1 : supplierapplied.getLne())

		{

			ls = getSupplierStereotypeListAllocateFrom(s1);
			if (ls != null)
				for (Cumlelement c1 : clientapplied.getLne())
				{
					Element ce = c1.getElement();
					if (ls.contains(ce) == false)
					{
						ls.add(ce);
					}
				}
		}
		return 0;
	}

	/**
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction,
	 *      org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart( IAction action , IWorkbenchPart targetPart )
	{
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run( IAction action )
	{
		try
		{

			if (sel == null)
				return;
			Cumlelement abstraction = Cumlcreator.getInstance().createUmlElement(sel);
			Cumlview.appliedAlloc(abstraction.getRootPackage());
			if (abstraction.isStereotypedBy("Allocate") == false)
			{
				MessageBox("Allocation", "Allocate stereoptype missing");
				MyManager.print("Allocate stereoptype missing");
				return;
			}
			// CmyModel m = (CmyModel)
			// Cumlcreator.getInstance().createUmlElement( sel.getModel());
			CmyPackage m = abstraction.getRootPackage();
			sh = new Shell();

			// m.getMembres().disp();
			Cliststereotype ls = m.getownedofE().getStererotypeofE().getligth();
			// ls.disp();

			CmyStereotype ap = ls.getElement("ApplicationAllocationEnd");
			CmyStereotype ep = ls.getElement("ExecutionPlatformAllocationEnd");

			if ((ap != null) && (ep != null))
			{				
				listing(m);
				sync();

				clientchange();
				supplierchange();
			} else
			{

				if (ap != null)
				{
					MessageBox("Allocation", "Missing :\nNamedElement with Execution Stereotype ");
				} else
				{
					// ApplicationAllocationEnd
					if (ep == null)
						MessageBox(
								"Allocation",
								"Missing :\n1) NamedElement with Application Stereotype\n2) NamedElement with Execution Stereotype ");
					else
						MessageBox("Allocation",
								"Missing :\nNamedElement with Application Stereotype ");

				}

			}
		}

		catch (Exception ex)
		{
			MyManager.printError(ex);
		}
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged( IAction action , ISelection selection )
	{
		sel = null;

		if (!(selection instanceof StructuredSelection))
			return;
		Object o = ((StructuredSelection) selection).getFirstElement();

		if (!(o instanceof Abstraction))
			return;
		sel = (Abstraction) o;
		// action.setEnabled(!action.isEnabled());
		// action.notify();
	}

}
