/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class FollowedByDialog.
 */
public class FollowedByDialog extends BaseConstraintApi {

	/** The a. */
	private IComboClock	a;

	/** The b. */
	private IComboClock	b;

	/** The c. */
	private IComboClock	c;

	/**
	 * Instantiates a new followed by dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public FollowedByDialog()
	{
		super("Follow");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		a = createclock("Resulting Clock ", "Clock");

		b = createclock("First Clock ", "Clock");

		c = createclock("Second Clock ", "Clock");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + a.getValueString() + " = " + b.getValueString()
				+ " followedBy " + c.getValueString() + "; \n}\n";

		return s1; 
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Asynchrone;
	}

	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/follow.png");
	}
	
	@Override
	protected int validate()
	{
		
		return 0;
	}
}
