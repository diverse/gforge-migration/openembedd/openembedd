/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */

package fr.inria.ctrte.plugalloc.popup.box;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.uml2.uml.NamedElement;

import code.Clist;
import code.Cumlelement;
import fr.inria.base.BasedDialog;
import fr.inria.base.MyManager;

/**
 * The Class MyDialog.
 */
public class MyDialog extends BasedDialog<Cumlelement>
// ChooseSetAssistedDialog
{

	// private Clist src=null;
	/** The cli. */
	private Clist	cli	= null;

	// private boolean f=true;

	/**
	 * Instantiates a new my dialog.
	 * 
	 * @param parentShell
	 *            the parent shell
	 * @param base
	 *            the base
	 * @param a
	 *            the a
	 * @param b
	 *            the b
	 * @param st1
	 *            the st1
	 * @param st2
	 *            the st2
	 */
	public MyDialog(Shell parentShell, String base, Clist a, Clist b, String st1, String st2,String title)
	{
		super(parentShell, "Applicable " + st1, "Applied " + st2 , title);
		// src=a;
		cli = b;
		
		labelProvider = new MyLabelProvider();
		
		if (a.size() == 0)
		{
			// f=false;
			
		}

		for (Cumlelement o : b.getLne())
			elselect.addElement(o);
		for (Cumlelement o : a.getLne())
			elsource.addElement(o);

	}

	/**
	 * @see fr.inria.base.BasedDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea( Composite parent )
	{
		Control composite = super.createDialogArea(parent);
		setLayoutData2();
		return composite;
	}

	

	/**
	 * Sets the layout data2.
	 */
	private void setLayoutData2()
	{

		((FormData) tableSelect.getLayoutData()).width = 300;
		((FormData) tableSource.getLayoutData()).bottom = new FormAttachment(100,
				-ITabbedPropertyConstants.VSPACE);
	}

	/**
	 * @see fr.inria.base.BasedDialog#isSelectableElement(java.lang.String)
	 */
	@Override
	protected boolean isSelectableElement( String text )
	{

		
		Iterator<Cumlelement> it = elsource.getElements().iterator();
		while (it.hasNext())
		{
			NamedElement element = (NamedElement) it.next().getElement();
			if (text.equalsIgnoreCase(element.getName())
					|| text.equalsIgnoreCase(element.getQualifiedName()))
			{
				return true;
			}
		}
		return false;
	}



	/**
	 * The Class MyLabelProvider.
	 */
	protected class MyLabelProvider extends LabelProvider implements ITableLabelProvider {

		/**
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public Image getImage( Object element )
		{
			if (element instanceof Cumlelement)
			{
				return ((Cumlelement) element).getIcone();
			}
		
			return null;
		}

		/**
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText( Object o )
		{
			try
			{
				if ((o instanceof Cumlelement))
				{
					return ((Cumlelement) o).getinfo(1);
				}
				return o.toString();
			} catch (Exception ex)
			{
				MyManager.printError(ex);
				return ex.getMessage();
			}
		}

		// @Override
		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
		 *      int)
		 */
		public Image getColumnImage( Object element , int columnIndex )
		{
			if (columnIndex == 0)
			{
				return getImage(element);
			}

			return null;
		}

		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
		 *      int)
		 */
		public String getColumnText( Object element , int columnIndex )
		{

			if (columnIndex == 0)
			{
				return getText(element);
			}		
			return "";
		}
	}

	/**
	 * Remoded.
	 * 
	 * @return the clist
	 */
	public Clist remoded()
	{

		ArrayList<Cumlelement> l = getSelectedElements();
		Clist la = new Clist();
		for (Cumlelement o : cli.getLne())
		{

			if (l.contains(o) == false)
			{
				la.add(o);
			}

		}
		return la;
	}

	/**
	 * Added.
	 * 
	 * @return the clist
	 */
	public Clist added()
	{

		ArrayList<Cumlelement> l = getSelectedElements();
		Clist la = new Clist();
		for (Cumlelement o : l)
		{
			if (cli.getLne().contains(o) == false)
			{
				la.add( o);
			}

		}
		return la;
	}

}