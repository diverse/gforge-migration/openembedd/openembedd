/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.work;

import code.Clistconstraint;
import code.CmyClock;
import code.CmyPackage;

/**
 * The Class Cinputrelation2.
 */
public class Cinputrelation2 {

	/** The liste. */
	private int					n		= 0;
	private CmyClock	liste[]	= null;

	/** The constraint. */
	private String		constraint;

	/**
	 * Instantiates a new cinputrelation2.
	 * 
	 * @param c
	 *            the c
	 * @param l
	 *            the l
	 */
	public Cinputrelation2(String c, CmyClock[] l)
	{
		super();
		try
		{
			CmyPackage cmp = l[0].getRootPackage();
			Clistconstraint cl = new Clistconstraint(cmp.getRootPackage().getownedofE());
			n = cl.size() + 1;
		} catch (Exception ex)
		{
			// TODO disp or not disp excep
			// MyManager.printError(ex);
			n = 0;
		}
		liste = l;
		constraint = c;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{

		return constraint;
	}

	/**
	 * Gets the liste.
	 * 
	 * @return the liste
	 */
	public CmyClock[] getListe()
	{
		return liste;
	}

	/**
	 * Gets the constraint.
	 * 
	 * @return the constraint
	 */
	public String getConstraint()
	{
		return constraint;
	}

	/**
	 * Createnameconstaint.
	 * 
	 * @return the string
	 */
	public String createnameconstaint()
	{
		String s = "__MARTE__CCSLrela__" + n;
		for (CmyClock c : liste)
		{
			s += "__" + c.getName();
		}

		return s;
	}

}
