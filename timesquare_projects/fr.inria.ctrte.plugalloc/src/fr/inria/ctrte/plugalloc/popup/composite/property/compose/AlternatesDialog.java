/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class AlternatesDialog.
 */
public class AlternatesDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new alternates dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public AlternatesDialog()
	{
		super("Alternation");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclockExtend("Clock a", "Clock");
		clcls = createclockExtend("Clock b", "Clock");		

		createModifierSingleton(true);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{
		String s1 = " { \n" + clcl.getValueString() + getModifierselected() + " alternatesWith "
				+ clcls.getValueString() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Asynchrone;
	}

	@Override
	public ImageDescriptor geticone()
	{
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/alternates.png");
	}

	@Override
	protected int validate()
	{
		if (clcl.getValue() == clcls.getValue())
		{
			haveerror = true;			
			messagestr = " Alternation : clock a!=b   ";
		} else
		{			
			haveerror = false;
			messagestr = " Alternation : Ok";
		}

		return 0;
	}

}
