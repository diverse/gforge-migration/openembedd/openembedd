/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.work;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;

import code.CmyClass;
import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;

/**
 * The Class Ccreateclock.
 */
public class Ccreateclock implements Iclock {

	/** The name. */
	private String							name;

	/** The puml. */
	private org.eclipse.uml2.uml.Package	puml;

	/** The is logical. */
	private Boolean							isLogical;

	/** The nature. */
	private Boolean							nature;

	/** The time unit kind. */
	private Object							timeUnitKind;

	/** The cuml. */
	private org.eclipse.uml2.uml.Class		cuml;

	/** The el. */
	private Cumlelement						el;

	/** The st. */
	private Stereotype						st	= null;

	/** The op. */
	private Operation						op	= null;

	/**
	 * Instantiates a new ccreateclock.
	 * 
	 * @param name
	 *            the name
	 * @param own
	 *            the own
	 * @param isLogical
	 *            the is logical
	 * @param nature
	 *            the nature
	 * @param timeUnitKind
	 *            the time unit kind
	 */
	public Ccreateclock(String name, org.eclipse.uml2.uml.Package own, Boolean isLogical,
			Boolean nature, Object timeUnitKind)
	{
		super();
		this.name = name;
		this.puml = own;
		this.isLogical = isLogical;
		this.nature = nature;
		this.timeUnitKind = timeUnitKind;
		compute();
		Cumlview.touchModel(puml);
	}

	

	/**
	 * @return the el
	 */
	public final CmyClass getElement()
	{
		return cl;
	}


CmyClass cl;
	/**
	 * Compute.
	 */
	protected void compute()
	{
		// TODo
		cuml = puml.createOwnedClass(this.name, false);
		cl =Cumlcreator.getInstance().createUmlElement(cuml);
		el = Cumlcreator.getInstance().createUmlElement(puml);
		st = (Stereotype) el.getPackage().getProfiles(true).getElement("Time").getMembres()
				.getElement("ClockType").getElement();
	
	//	MyManager.println(cuml.getApplicableStereotype("MARTE::MARTE_Foundations::Time::ClockType").toString());
		
		CmyClass el2 = Cumlcreator.getInstance().createUmlElement(cuml);
		el2.applyClockType();
		el2.setLogicalAttribut(isLogical);
//		Clist le = ((CmyEnumeration) el.getRootPackage().getProfiles(true).getElement("Time")
		//		.getMembres().getElement("TimeNatureKind")).getEumerationliteral();
	//	MyManager.println(le.getElement("dense").getElement() + " >>  " + nature);

		el2.setNatureAttribut(nature);		

		cuml.setValue(st, "unitType", this.timeUnitKind);
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.work.Iclock#setpropertyvalue(java.lang.String,
	 *      java.lang.String)
	 */
	public int setpropertyvalue( String name , String value )
	{
		try
		{

			Type type = null;
			type = Cumlview.getStringType();
			EClass ecs = UMLPackage.eINSTANCE.getLiteralString();

			Property p = cuml.createOwnedAttribute("__Marte_" + name, type);

			PackageImport ei = UMLFactory.eINSTANCE.createPackageImport();
			ei.setImportedPackage(null);

			ValueSpecification vl = p.createDefaultValue("value", type, ecs);
			((LiteralString) vl).setValue(value);
			cuml.setValue(st, name, p);
		} catch (Exception ex)
		{
			MyManager.printError(ex);

		}
		return 0;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.work.Iclock#createOperation(java.lang.String)
	 */
	public int createOperation( String name )
	{
		Type t = Cumlview.getIntegerType();
		EList<Type> el = new BasicEList<Type>();
		el.add(t);
		EList<String> es = new BasicEList<String>();
		es.add("indice");
		op = cuml.createOwnedOperation("__Marte_" + name, es, el, t);
		cuml.setValue(st, "getTime", op);
		return 0;
	}

}
