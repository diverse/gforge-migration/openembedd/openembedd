/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputNumber;

/**
 * The Class TimerDialog.
 */
public class TimerDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The tm. */
	private IComboClock	clcls, t, tm;

	/**
	 * Instantiates a new timer dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public TimerDialog()
	{
		super("Timer");

	}

	private IInputNumber	linp2	= null;

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		t = createclock("Timer ", "Clock");

		clcl = createclock("Super Clock", "Clock");

		clcls = createclock("Start Clock", "Clock");

		tm = createclock("TimeOut Clock", "Clock");

		linp2 = createInputNumber("duration ", false);
		// new ClabelInputNumber(getComposite(), SWT.NONE, "duration ");
		// position(linp2);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + t.getValueString() + "= oneShotConstraint " + clcl.getValueString()
				+ " , " + clcls.getValueString() + " ," + tm.getValueString() + " , "
				+ linp2.getValue() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Mixed;
	}

	@Override
	protected int validate()
	{
		
		return 0;
	}
}
