/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class SyncDialog.
 */
public class SyncDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new sync dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public SyncDialog()
	{
		super("Synchronization");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{
		clcl = createclockExtend("left", "Clock");
		clcls = createclockExtend("right", "Clock");
		createModifierSingleton(true);
		// createCheckboxGroupSingleton();

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + clcl.getValueString() + getModifierselected() + " synchronizesWith "
				+ clcls.getValueString() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Asynchrone;
	}

	@Override
	public ImageDescriptor geticone()
	{
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/sync.png");
	}

	@Override
	protected int validate()
	{
		return 0;
	}
}
