/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.work;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EDataTypeImpl;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;

import code.CmyClass;
import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;

/**
 * The Class StereotypeClock.
 */
public class StereotypeClock implements Iclock {
	// private String name;



	/** The cuml. */
	private org.eclipse.uml2.uml.Class	cuml;

	/** The el. */
	private CmyClass					el;

	// private Boolean isLogical;
	/** The nature. */
	private Boolean						nature;

	/** The op. */
	private Operation					op	= null;

	/** The st. */
	// private Stereotype st = null;
	/** The time unit kind. */
	private Object						timeUnitKind;

	/**
	 * Instantiates a new stereotype clock.
	 * 
	 * @param name
	 *            the name
	 * @param cls
	 *            the cls
	 * @param isLogical
	 *            the is logical
	 * @param nature
	 *            the nature
	 * @param timeUnitKind
	 *            the time unit kind
	 */
	public StereotypeClock(String name, org.eclipse.uml2.uml.Class cls, Boolean isLogical,
			boolean nature, Enumeration timeUnitKind)
	{
		super();
		cuml = cls;
		this.nature = nature;
		this.timeUnitKind = timeUnitKind;

		el = Cumlcreator.getInstance().createUmlElement(cuml);
		el.setName(name);
		el.applyClockType();
		el.setLogicalAttribut(isLogical);
		el.setNatureAttribut(nature);
		el.setUnitType(timeUnitKind);

		// Cumlview.touchModel(cuml);
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.work.Iclock#createOperation(java.lang.String)
	 */
	public int createOperation( String name )
	{
		Type t = Cumlview.getIntegerType();
		EList<Type> el = new BasicEList<Type>();
		el.add(t);
		EList<String> es = new BasicEList<String>();
		es.add("indice");
		op = cuml.createOwnedOperation("__Marte_" + name, es, el, t);
		Stereotype st = null;
		cuml.setValue(st, "getTime", op);
		return 0;
	}

	/**
	 * Gets the nature.
	 * 
	 * @return the nature
	 */
	public Boolean getNature()
	{
		return nature;
	}

	/* (non-Javadoc)
	 * @see fr.inria.ctrte.plugalloc.popup.work.Iclock#getEl()
	 */
	public Cumlelement getElement()
	{		
		return el; //TODO ...
	}
	
	/**
	 * Gets the time unit kind.
	 * 
	 * @return the time unit kind
	 */
	public Object getTimeUnitKind()
	{
		return timeUnitKind;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.work.Iclock#setpropertyvalue(java.lang.String,
	 *      java.lang.String)
	 */
	public int setpropertyvalue( String name , String value )
	{
		try
		{
			MyManager.println("--------------------------------------------------------");
			Type type = null;
			MyManager.println(UMLFactory.eINSTANCE.getUMLPackage().getString().eContainer());
			MyManager.println(UMLFactory.eINSTANCE.createLiteralString().getType());
			EDataTypeImpl datatype = (EDataTypeImpl) UMLFactory.eINSTANCE.getUMLPackage()
					.getString().eContainer().eContents().get(2);
			MyManager.println(datatype);
			type = Cumlview.getStringType();
			MyManager.println("........................ " + type);
			EClass ecs = UMLPackage.eINSTANCE.getLiteralString();

			MyManager.println(UMLFactory.eINSTANCE.createLiteralString());
			Property p = cuml.createOwnedAttribute("__Marte_" + name, type);

			PackageImport ei = UMLFactory.eINSTANCE.createPackageImport();
			ei.setImportedPackage(null);

			MyManager.println(type.getQualifiedName() + "\n" + type.toString());
			MyManager.println(((EDataTypeImpl) UMLFactory.eINSTANCE.eCrossReferences().get(0)
					.eContents().get(2)).getInstanceClass());

			MyManager.println("toto " + type.getOwner() + " ");

			ValueSpecification vl = p.createDefaultValue("value", type, ecs);
			((LiteralString) vl).setValue(value);
			Stereotype st = null;
			cuml.setValue(st, name, p); 
		} catch (Exception ex)
		{
			MyManager.printError(ex);

		}
		return 0;
	}

}
