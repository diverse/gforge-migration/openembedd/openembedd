/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.baseuml.icore.IInputNumber;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class UnionDialog.
 */
public class DelayedFor extends BaseConstraintApi {

	/** The a. */
	private IComboClock			a;

	/** The b. */
	private IComboClock			b;

	/** The c. */
	private IComboClock			c;

	private IInputNumber	linp1	= null;

	/**
	 * Instantiates a new union dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public DelayedFor()
	{
		super("DelayedFor");
		setComment("Apply a delay ");
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		a = createclock("Result", "Clock");
		b = createclock("Trigger", "Clock");
		linp1 = createInputNumber("delay", false);
		c = createclock("Reference", "Clock");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n" + a.getValueString() + " = " + b.getValueString()
				+ " delayedFor  " + linp1.getValue() + " on " + c.getValueString()
				+ "; \n}\n";
		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Mixed;
	}

	@Override
	protected int validate()
	{
		if ((a.getValue() == b.getValue()) || (a.getValue() == c.getValue()) )
		{
			haveerror = true;			
			messagestr = " Delayed : clock a!=b   ";
		} else
		{			
			haveerror = false;
			messagestr = " Delayed : Ok";
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#geticone()
	 */
	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/delay.png");
	}

	
}
