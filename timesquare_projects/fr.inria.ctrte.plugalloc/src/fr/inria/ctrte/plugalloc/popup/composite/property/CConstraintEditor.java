/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;

import code.CinterfaceUML;
import code.Clist;
import code.CmyPackage;
import code.Cumlcreator;
import fr.inria.ctrte.baseuml.core.Clabelcombo;
import fr.inria.ctrte.plugalloc.ConstraintDictionnary;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;
import fr.inria.ctrte.plugalloc.popup.work.Cinputrelation2;

/**
 * The Class CConstraintEditor.
 */
public class CConstraintEditor extends Dialog implements IDialogError {

	private final class SelectionListenerCombo implements SelectionListener {
		public void widgetDefaultSelected( SelectionEvent e )
		{
			widgetSelected(e);
		}

		public void widgetSelected( SelectionEvent e )
		{
			int n = ((CCombo) e.getSource()).getSelectionIndex();

			if (n != -1)
			{
				old = courant;
				courant = liste.get(n);
				old.setActive(false);
				courant.setActive(true);
				courant.validation();

			}
			getShell().setImage(courant.geticone().createImage());
		}
	}

	/** The clci. */
	private Clabelcombo						clci;
	// private Group group = null;
	/** The groupbs. */
	private Group							groupbs		= null;

	/** The le. */
	private Clist							le;

	/** The cpt. */
	private int								cpt			= 0;
	// private Shell shell;
	// int size = 0;
	// String localliste[] = null;
	/** The courant. */
	private BaseConstraintApi				old			= null, courant = null;
	private Label							messerror	= null;
	/** The liste. */
	private ArrayList<BaseConstraintApi>	liste		= new ArrayList<BaseConstraintApi>();

	/**
	 * Instantiates a new c relative clock2.
	 * 
	 * @param parentShell
	 *            the parent shell
	 * @param l
	 *            the l
	 */
	public CConstraintEditor(Shell parentShell, Clist l)
	{

		super(parentShell);
		le = l;
		// shell = parentShell;
		setShellStyle(SWT.RESIZE | super.getShellStyle());

	}

	
	public void setError( String message , boolean error , int value )
	{
		if (ok != null)
			ok.setEnabled(error);
		if (messerror != null)
		{
			messerror.setText(message);
			messerror.pack();

		}
		// groupbs.pack(true);

	}

	Button	ok;

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar( Composite parent )
	{
		ok = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		courant.validation();
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea( Composite parent )
	{
		getShell().setText("Clock Constraint" );
		Composite comp = (Composite) super.createDialogArea(parent);

		clci = new Clabelcombo(comp, "constraint", "constraint", new String[] {});
		clci.addSelectionListenerCombo(new SelectionListenerCombo());
		groupbs = new Group(comp, SWT.NONE);
		// groupbs.setLayout(new GridLayout(1, true));
		groupbs.setLayoutData(new GridData(GridData.FILL));
		groupbs.setText("parameters");
		groupbs.setVisible(true);

		le.disp();
		for (BaseConstraintApi bca : ConstraintDictionnary.getDefault().getlist())
		{
			bca.setLe(le);
			adding(bca);

		}
		clci.setSelectCombo(0);
		for (BaseConstraintApi bca : liste)
		{
			bca.setActive(false);

		}
		liste.get(0).setActive(true);
		courant = liste.get(0);

		groupbs.pack();
		messerror = new Label(comp, SWT.RIGHT | SWT.FILL);

		messerror.setText("not yet implemented");
		messerror.pack();
		getShell().setImage(courant.geticone().createImage());
		courant.validation();

		return comp;
	}

	/**
	 * Adding.
	 * 
	 * @param fb
	 *            the fb
	 * 
	 * @return the int
	 */
	protected int adding( BaseConstraintApi fb )
	{
		Composite p = createGroup(groupbs, true, "" + cpt++);
		fb.draw(p, this);
		p.pack();
		liste.add(fb);
		clci.addCombo(fb.getName());

		return 0;
	}

	/**
	 * Creates the group.
	 * 
	 * @param c
	 *            the c
	 * @param b
	 *            the b
	 * @param s
	 *            the s
	 * 
	 * @return the group
	 */
	protected Composite createGroup( Composite c , boolean b , String s )
	{
		Composite gl = new Composite(c, SWT.NONE);
		gl.setLocation(10, 20);
		gl.setSize(500, 100);
		//gl.setText("");
		gl.setVisible(b);
		gl.setLayout(new GridLayout(1, true));
		gl.setLayoutData(new GridData(GridData.FILL_BOTH));
		gl.pack();
		return gl;
	}

	/**
	 * Gets the relation.
	 * 
	 * @return the relation
	 */
	public Cinputrelation2 getRelation()
	{

		return courant.make();
	}

	/**
	 * Apply.
	 * 
	 * @param cmp
	 *            the cmp
	 */
	public void apply( CmyPackage cmp )
	{
		Cinputrelation2 c = getRelation();
		String namecst = c.createnameconstaint();	
		Constraint cs = cmp.getElement().createOwnedRule(namecst);

		Cumlcreator.getInstance().createUmlElement(cs).makeParsable();
		((OpaqueExpression) cs.getSpecification()).getBodies().set(0, c.getConstraint());
		for (CinterfaceUML e : c.getListe())
		{
			cs.getConstrainedElements().add(e.getElement());
		}
	}

}
