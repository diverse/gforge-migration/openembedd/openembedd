/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import fr.inria.ctrte.baseuml.icore.IComboClock;

/**
 * The Class SustainDialog.
 */
public class SustainDialog extends BaseConstraintApi {

	/** The trig. */
	private IComboClock	clcl, trig;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new sustain dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public SustainDialog()
	{
		super("Sustain ");
		other=true;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("a Clock", "Clock");

		clcls = createclock("b Clock", "Clock");

		trig = createclock("c Clock", "Clock");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{

		String s1 = " { \n"
				+ clcls.getValueString() 
				+ " = sustain " + clcl.getValueString() + " upto " + trig.getValueString()
				+ " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Asynchrone;
	}

	@Override
	protected int validate()
	{
		
		return 0;
	}

}
