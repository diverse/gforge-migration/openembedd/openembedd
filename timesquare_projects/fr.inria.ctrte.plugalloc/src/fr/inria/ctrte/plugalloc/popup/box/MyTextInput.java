/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright 2007-2008
 */

package fr.inria.ctrte.plugalloc.popup.box;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.VerticalRuler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.base.MyManager;
import fr.inria.ctrte.baseuml.core.LabelEditorDialog;
import grammaire.TestSyntax;

/**
 * The Class MyTextInput.
 */
public class MyTextInput extends LabelEditorDialog {

	// private String s="";

	/** The parameter label document. */
	private Document parameterLabelDocument;

	/**
	 * Instantiates a new my text input.
	 * 
	 * @param parentShell
	 *            the parent shell
	 * @param base
	 *            the base
	 */
	public MyTextInput(Shell parentShell, String base)
	{
		super(parentShell, "constraint", base, null);
		// s=base;

	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea( Composite parent )
	{

		Group composite = new Group(parent, SWT.RESIZE);
		composite.setText("Specification");
		GridLayout layout = new GridLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		layout.verticalSpacing = 10;
		layout.horizontalSpacing = 10;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		validator = new IInputValidator() {

			public String isValid( String newText )
			{

				TestSyntax ts = new TestSyntax(newText);
				if (ts.getnbrerror() != 0)
				{

					return ts.getstrerror();
				}

				MyManager.println("Constraint Update ::" + newText + " \n");
				return "";
			}
		};

		Composite viewerGroup = new Composite(composite, SWT.RESIZE);
		FillLayout viewerLayout = new FillLayout();
		viewerGroup.setLayout(viewerLayout);
		GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL
				| GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		data.widthHint = 450;
		data.heightHint = 200;
		viewerGroup.setLayoutData(data);
		
		viewer = new SourceViewer(viewerGroup, new VerticalRuler(10),  SWT.BORDER | SWT.FILL_EVEN_ODD | SWT.MULTI
				| SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		
		// viewer.getDocument().
		
		parameterLabelDocument = new Document();
		parameterLabelDocument.set(value);
		parameterLabelDocument.addDocumentListener(new DocumentListener());
	
		viewer.setDocument(parameterLabelDocument);
		viewer.setSelectedRange(0, 100); 
		viewer.setRangeIndication(0, 100, true);		
	


		//CCSLSourceViewerConfiguration ccsl=new CCSLSourceViewerConfiguration();		 	
	//	viewer.configure(ccsl); 
		
		errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.MULTI | SWT.V_SCROLL);
		errorMessageText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		errorMessageText.setBackground(errorMessageText.getDisplay().getSystemColor(
				SWT.COLOR_WIDGET_BACKGROUND));

		setErrorMessage(errorMessage);
		errorMessageText.setText("..\n\n");
		errorMessageText.pack();
	
		applyDialogFont(composite);
		return composite;

	}

	private class DocumentListener implements IDocumentListener {

		public void documentAboutToBeChanged( DocumentEvent event )
		{
		}

		public void documentChanged( DocumentEvent event )
		{
			validateInput();
		}

	}

}