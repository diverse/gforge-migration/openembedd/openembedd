/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.popup.composite.property.compose;

import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.baseuml.icore.IComboClock;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class DisjointDialog.
 */
public class DisjointDialog extends BaseConstraintApi {

	/** The clcl. */
	private IComboClock	clcl;

	/** The clcls. */
	private IComboClock	clcls;

	/**
	 * Instantiates a new disjoint dialog.
	 * 
	 * @param ls
	 *            the ls
	 */
	public DisjointDialog()
	{
		super("Disjoint");

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#draw(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void mydraw()
	{

		clcl = createclock("Clock", "Clock");

		clcl.setSelectCombo(0);

		clcls = createclock("Clock", "Clock");

		clcls.setSelectCombo(0);

	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#make()
	 */
	@Override
	public String getContraintText()
	{
		String s1 = " { \n" + clcl.getValueString() + " # "
				+ clcls.getValueString() + " ; \n}\n";

		return s1;
	}

	/**
	 * @see fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi#getType()
	 */
	@Override
	public TypeConstrain getType()
	{
		return TypeConstrain.Miscellaneous;
	}

	@Override
	public ImageDescriptor geticone()
	{
		
		return MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID, "icons/disjoint.png");
	}
	
	@Override
	protected int validate()
	{
		if (clcl.getValue() == clcls.getValue())
		{
			haveerror = true;			
			messagestr = " Disjoint : clock a!=b   ";
		} else
		{			
			haveerror = false;
			messagestr = " Disjoint : Ok";
		}
		return 0;
	}
}
