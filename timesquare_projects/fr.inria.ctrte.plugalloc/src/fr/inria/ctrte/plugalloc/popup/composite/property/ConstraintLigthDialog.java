/**
 * 
 */
package fr.inria.ctrte.plugalloc.popup.composite.property;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import code.CmyConstraint;
import code.CmyPackage;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;
import fr.inria.ctrte.plugalloc.popup.work.Cinputrelation2;

public final class ConstraintLigthDialog extends Dialog implements IDialogError {

	/**
	 * 
	 */


	public void setError( String message , boolean error , int value )
	{
		ok.setEnabled(error);
		if (messerror != null)
		{
			messerror.setText(message);
			messerror.pack();
		}

	}

	BaseConstraintApi	bca;
	Button				ok;

	public ConstraintLigthDialog(Shell parentShell, BaseConstraintApi bca)
	{
		super(parentShell);
		parentShell.setMinimumSize(400, 100);
		this.bca = bca;
	}

	@Override
	protected void createButtonsForButtonBar( Composite parent )
	{
		ok = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		bca.validation();
	}

	@Override
	protected Control createDialogArea( Composite parent )
	{
		getShell().setText(bca.getName());
		if (bca.geticone() != null)
		{
			getShell().setImage(bca.geticone().createImage());
		}
		GridLayout gl = null;
		gl = new GridLayout(1, true);
		gl.marginBottom = 5;
		gl.marginTop = 5;
		gl.marginLeft = 10;
		gl.marginRight = 10;
		gl.marginHeight = 1;
		gl.marginWidth = 1;
		parent.setLayout(gl);
		bca.draw(parent, this);
		messerror = new Label(parent, SWT.FILL);
		messerror.setText("not yet implemented");
		
		return parent;
	}

	Label	messerror	= null;

	public void apply( CmyPackage cmp )
	{
		Cinputrelation2 c = bca.make();
		String namecst = c.createnameconstaint();
		CmyConstraint cmc = cmp.createConstraintRule(namecst);
		cmc.makeParsable();
		cmc.setCCSLBody(c.getConstraint());
		cmc.setConstraintto(c.getListe());
	}
}