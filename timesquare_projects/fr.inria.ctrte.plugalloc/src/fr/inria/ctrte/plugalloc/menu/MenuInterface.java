package fr.inria.ctrte.plugalloc.menu;

/***
 * 
 * 
 * @author Benoit Ferrero
 * 
 *         interface for{@link DynamicMenu} and {@link DynamicPopupMenu}
 * 
 */

public interface MenuInterface {

	/**
	 * @return the selection
	 */
	public abstract Object getSelection();

}