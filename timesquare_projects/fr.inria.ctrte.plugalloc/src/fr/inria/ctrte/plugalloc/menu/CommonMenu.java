package fr.inria.ctrte.plugalloc.menu;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.actions.CompoundContributionItem;

import fr.inria.ctrte.plugalloc.ConstraintDictionnary;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml;
import fr.inria.ctrte.plugalloc.action.constraint.ConstraintLigth_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi.TypeConstrain;



public abstract class CommonMenu extends CompoundContributionItem {

	public CommonMenu()
	{
		super();		
	}

	public CommonMenu(String id)
	{
		super(id);	
	}

	/**
	 * Creates the sub menu constraint.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return the menu manager
	 */
	protected IContributionItem[] createSubMenuConstraint( )
	{
		
		
		
		try
		{
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		MenuManager tb[]=new MenuManager[ TypeConstrain.values().length];
		MenuManager tb2[]=new MenuManager[ TypeConstrain.values().length];
	//	MenuManager tb[]=new MenuManager[ TypeConstrain.values().length];
		for (Object o : TypeConstrain.values())
		{
			//sb.add(new Separator(o.toString()));  
			MenuManager mm=new MenuManager(o.toString());			
			tb[((TypeConstrain)o).getID()] =mm;
			MenuManager sub=new MenuManager("Others");
			mm.add(sub);
			tb2[((TypeConstrain)o).getID()] =sub;
		}

		for (BaseConstraintApi bca : ConstraintDictionnary.getDefault().getlist())
		{

			//sb.appendToGroup(bca.getType().toString(), new ConstraintLigth_Action(bca, bd));
			ConstraintLigth_Action cla=new ConstraintLigth_Action(bca, bd);
			if (bca.isOther())
				tb2[bca.getType().getID()].add(cla);
			else
				tb[bca.getType().getID()].add(cla);
		}
		return tb;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}
	
}
