package fr.inria.ctrte.plugalloc.menu.submenu;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.MenuManager;

import fr.inria.ctrte.plugalloc.action.clock.ClockType_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.actions.ClockTypeEnumeration;

public class ClockTypeSubMenu {

	private MenuManager	mm;
	private BaseDecode	bd;

	public ClockTypeSubMenu(MenuManager mm, BaseDecode bd)
	{
		super();
		this.bd = bd;
		this.mm = mm;
	}

	public int createClocktypeMenu()
	{
		Action ac = null;
	/*	ac = new ClockType_Action("Apply Stereotype Chronometric ClockType", null,
				ClockTypeEnumeration.StereoChrono, bd);
		ActionContributionItem aci = new ActionContributionItem(ac);
		mm.appendToGroup("clock", aci);
		ac = new ClockType_Action("Apply Stereotype Logical ClockType", null,
				ClockTypeEnumeration.StereoLogical, bd);
		aci = new ActionContributionItem(ac);
		mm.appendToGroup("clock", aci);*/
		ac = new ClockType_Action("Apply Stereotype  ClockType", null, ClockTypeEnumeration.StereoC, bd);
		ActionContributionItem aci = new ActionContributionItem(ac);
		mm.appendToGroup("clock", aci);
		return 0;
	}

}
