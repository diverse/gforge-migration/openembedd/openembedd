package fr.inria.ctrte.plugalloc.menu.submenu;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;

import code.CmyClass;
import code.CmyClock;
import code.CmyEnumeration;
import code.CmyEnumerationLiteral;
import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.action.clock.UnitSelect_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

public class ClockSubMenu {

	protected final class ActionSelectUnit extends Action {
		CmyEnumerationLiteral	cl;
		CmyClock				c;
		boolean					b;

		protected ActionSelectUnit(String text, CmyEnumerationLiteral cl, CmyClock c, boolean b,
				BaseDecode bd)
		{
			super(text, IAction.AS_RADIO_BUTTON);
			this.cl = cl;
			this.b = b;
			this.c = c;
		}

		@Override
		public void run()
		{
			if (b == false)
			{

				bd.getUmlElement();
				CmyPackage cmp = Cumlcreator.getInstance().createUmlElement(
						bd.getUmlElement().getNearestPackage());
				Cumlview.appliedTime(cmp);
				c.setUnit(cl);
				bd.notifymodification();

			}
		}

	}

	protected final class ActionSelectKind extends Action {
		CmyEnumerationLiteral	cl;
		CmyClock				c;
		boolean					b;

		protected ActionSelectKind(String text, CmyEnumerationLiteral cl, CmyClock c, boolean b,
				BaseDecode bd)
		{
			super(text, IAction.AS_RADIO_BUTTON);
			this.cl = cl;
			this.b = b;
			this.c = c;
		}

		@Override
		public void run()
		{
			if (b == false)
			{

				bd.getUmlElement();
				CmyPackage cmp = Cumlcreator.getInstance().createUmlElement(
						bd.getUmlElement().getNearestPackage());
				Cumlview.appliedTime(cmp);
				c.setStandard(cl);
				bd.notifymodification();

			}
		}

	}

	private MenuManager	mm;
	private BaseDecode	bd;

	public ClockSubMenu(MenuManager mm, BaseDecode bd)
	{
		super();
		this.bd = bd;
		this.mm = mm;
	}

	public int createClockMenu()
	{
		if (!(bd.getUmlElement() instanceof org.eclipse.uml2.uml.EnumerationLiteral))
		{
			_createClockMenu();
		}
		return 0;
	}

	private int _createClockMenu()
	{
		Cumlelement ce= Cumlcreator.getInstance().createUmlElement(	bd.getUmlElement());
		MyManager.println(ce.getClass().getName()+" "+ce.getElement());
		CmyClock ci = (CmyClock) ce;
		if (!ci.isClockStereotype())
		{
			ActionContributionItem aci = new ActionContributionItem(new Action(
					"Applied Clock Stereotype") {
			});
			mm.appendToGroup("clock", aci);
		}

		Action action = new UnitSelect_Action("Configure Clock", null, bd);
		ActionContributionItem aci = new ActionContributionItem(action);
		mm.appendToGroup("clock", aci);

		CmyClass cl = ci.getClassifier();
		if (cl != null && cl.isMarteClockType() && ci.isClockStereotype())
		{

			CmyEnumeration cme = cl.getUnitType();
			if (cme != null)
			{
				MenuManager mml = new MenuManager("Select unit");
				mm.appendToGroup("clock", mml);
				CmyEnumerationLiteral unit = ci.getUnit();
				boolean x = false;
				for (Cumlelement cmel : cme.getEumerationliteral().getLne())
				{
					String s = "" + cmel.getName();
					boolean b = false;
					if (unit != null)
					{
						b = (unit.getName().equals(cmel.getName()));
						s += "@     " + ((CmyEnumerationLiteral) cmel).convertString();
					}
					x = !x;
					Action ac = new ActionSelectUnit(s, (CmyEnumerationLiteral) cmel, ci, b, bd);
					ac.setChecked(b);

					mml.add(ac);

				}

			}

			if (cl.isLogicalClockType().booleanValue())
			{
				Action ac = new Action("Select TAI") {};
				ac.setEnabled(false);
				mm.appendToGroup("clock", ac);

			} else
			{
				MenuManager mml = new MenuManager("Select TAI");
				mm.appendToGroup("clock", mml);
				mml.setVisible(true);
				CmyEnumeration cme2 = Cumlcreator.getInstance().createUmlElement(Cumlview.getTimeStandardKind());
				if (cme2!=null)
				if (cme2.getEumerationliteral()!=null)
				for (Cumlelement cme2l : cme2.getEumerationliteral().getLne())  
				{
					boolean b = false;
					if (ci.getStandard() != null)
						b = (ci.getStandard().getName().equals(cme2l.getName()));
					Action ace = new ActionSelectKind(cme2l.getName(),
							(CmyEnumerationLiteral) cme2l, ci, b, bd);
					ace.setChecked(b);
					mml.add(ace);
				}

			}

		}
		return 0;
	}

}
