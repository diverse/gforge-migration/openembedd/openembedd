/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.menu;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.ActivatorPlug;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml;
import fr.inria.ctrte.plugalloc.action.About_Action;
import fr.inria.ctrte.plugalloc.action.ApplyProfile_Action;
import fr.inria.ctrte.plugalloc.action.clock.ClockType_Action;
import fr.inria.ctrte.plugalloc.action.clock.Clock_Action;
import fr.inria.ctrte.plugalloc.action.constraint.CCSL_Action;
import fr.inria.ctrte.plugalloc.action.constraint.ConstraintFull_Action;
import fr.inria.ctrte.plugalloc.action.constraint.ContextContraint_Action;
import fr.inria.ctrte.plugalloc.action.constraint.MakeCCSL_Action;
import fr.inria.ctrte.plugalloc.action.constraint.ParserCCSL_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.menu.submenu.ClockSubMenu;
import fr.inria.ctrte.plugalloc.menu.submenu.ClockTypeSubMenu;
import fr.inria.ctrte.plugalloc.popup.actions.ClockTypeEnumeration;

/**
 * The Class DynamicMenu.
 */
public class DynamicPopupMenu extends CommonMenu implements MenuInterface {

	protected IEditorPart	editorPart	= null;

	/**
	 * Instantiates a new dynamic menu.
	 */
	public DynamicPopupMenu()
	{
		super();
	}

	/**
	 * Instantiates a new dynamic popup menu.
	 * 
	 * @param id
	 * 
	 */
	public DynamicPopupMenu(String id)
	{
		super(id);

	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#isDynamic()
	 */
	@Override
	public boolean isDynamic()
	{
		return true;
	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#isDirty()
	 */
	@Override
	public boolean isDirty()
	{
		return true;
	}

	/** The selection. */
	protected Element	selection	= null;

	/**
	 * Gets the no um leditor mmenu.
	 * 
	 * @return the no um leditor mmenu
	 */
	protected IContributionItem[] getnoUMLeditorMmenu()
	{
		Action ac = new About_Action("About");
		ac.setToolTipText("About");
		IContributionItem m1 = new ActionContributionItem(ac);
		return new IContributionItem[] { m1 };
	}

	

	/**
	 * Applied profile item.
	 * 
	 * @return the i contribution item
	 */
	protected IContributionItem appliedProfilItem()
	{
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		Action ac = new ApplyProfile_Action("Apply Marte Profile : Time", null, bd);
		ac.setToolTipText("Apply a Marte profil on ...");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;

	}

	/**
	 * 
	 * 
	 * @return sub menu for class
	 */
	protected void addPackageMenu( MenuManager mm )
	{

		ImageDescriptor id = MyPluginManager.getImageDescriptor(ActivatorPlug.PLUGIN_ID,
				"icons/clock.png");
		ClockType_Action ac;
		/******/
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		ac = new ClockType_Action("Make a Chronometric ClockType", id, ClockTypeEnumeration.ChronoC, bd);
		// ac.setElement((Element) selection);
		/******/
		generateItemForClockTypeAction(mm, ac, "make");
		ac = new ClockType_Action("Make a Logical ClockType", id, ClockTypeEnumeration.LogicalC, bd);
		// ac.setElement((Element) selection);
		generateItemForClockTypeAction(mm, ac, "make");
		/******/
		ac = new ClockType_Action("Make a ClockType", id, ClockTypeEnumeration.StereoP, bd);
		// ac.setElement((Element) selection);
		generateItemForClockTypeAction(mm, ac, "make");
		/******/
		Clock_Action acs = new Clock_Action("Make a Clock", id, bd);
		generateItemForClockTypeAction(mm, acs, "makeclock");
		/******/
		Action action = new ConstraintFull_Action("Constraint ", id, bd);
		generateItemForClockTypeAction(mm, action, "constraint");
		/****/
		action = new CCSL_Action("Simulator / Parser CCSL", id, bd);
		generateItemForClockTypeAction(mm, action, "first");
	}

	protected void addConstraintMenu( MenuManager mm )
	{

		// ImageDescriptor id=
		// ImageDescriptor.createFromImage(ImageManager.getImage
		// (ActivatorPlug.PLUGIN_ID, "icons/clock.png"));
		// Action ac;
		/*
		 * mm.add( new MakeCCSL());
		 */
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		Action action = new MakeCCSL_Action("Make CCSL", null, bd);
		generateItemForClockTypeAction(mm, action, "constraint");
		action = new ParserCCSL_Action("Parser CCSL", null, bd);
		generateItemForClockTypeAction(mm, action, "constraint");
		action = new ContextContraint_Action("Edit Constraint", null, bd);
		generateItemForClockTypeAction(mm, action, "constraint");
	}

	// fr.inria.ctrte.time.parser.ui.action.MakeAction

	/**
	 * @see org.eclipse.jface.action.ContributionItem#isVisible()
	 */
	@Override
	public boolean isVisible()
	{
		try
		{
			//editorPart = MyManager.getCourantEditor();			
			editorPart = MyPluginManager.getActiveEditor();

			if (editorPart == null)
				return false;

			if (!MyEditorManagerUml.getDefault().isUmlLikeEditor())
				return false;
			return true;
		} catch (Exception e)
		{
			return false;
		}
		// return super.isVisible();
	}

	protected void generateItemForClockTypeAction( MenuManager mm , Action ac , String group )
	{
		ActionContributionItem aci = new ActionContributionItem(ac);
		mm.appendToGroup(group, aci);
	}

	ClockSubMenu		csm		= null;
	ClockTypeSubMenu	ctsm	= null;

	/***
	 * 
	 * @return Marte Menu
	 */
	protected MenuManager createMarteMenu()
	{
		// ImageDescriptor id = ImageDescriptor.createFromImage(ImageManager
		// .getImage(ActivatorPlug.PLUGIN_ID, "icons/clock.png"));
		// pas icone en 3.3
		MenuManager mm = new MenuManager("Marte Menu ...", "fr.inria.ctrte.menu"); // "fr.inria.ctrte.menu"
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		// )
		try
		{
			mm.add(new Separator("first"));
			mm.add(new Separator("make"));
			mm.add(new Separator("clock"));
			mm.add(new Separator("makeclock"));
			mm.add(new Separator("constraint"));
			mm.add(new Separator("other"));

			mm.appendToGroup("first", appliedProfilItem());
			// mm.appendToGroup("first", createClockItem());
			csm = null;
			if (selection instanceof Package)
			{
				addPackageMenu(mm);
			}
			if (selection instanceof Constraint)
			{
				addConstraintMenu(mm);
			}
			if (selection instanceof InstanceSpecification)
			{
				csm = (new ClockSubMenu(mm, bd));
				csm.createClockMenu();
			}
			if (selection instanceof Property)
			{
				csm = (new ClockSubMenu(mm, bd));
				csm.createClockMenu();
			}
			if (selection instanceof Class)
			{
				ctsm = (new ClockTypeSubMenu(mm, bd));
				ctsm.createClocktypeMenu();
			}

			for( IContributionItem it:createSubMenuConstraint())
			mm.appendToGroup("constraint",it );
			// IContributionItem info= new ActionContributionItem(new Action
			// ("Info"){});
			// mm.appendToGroup("other", info);
			Action ac = new About_Action("About");
			ac.setToolTipText("About");
			//*new ActionContributionItem(ac);
			mm.appendToGroup("other", new ActionContributionItem(ac));

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}
		return mm;
	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	@Override
	protected IContributionItem[] getContributionItems()
	{

		try
		{
			selection = null;
			editorPart = MyPluginManager.getCourantEditor();		
			if (editorPart == null)
				return new IContributionItem[] {};

			if (!MyEditorManagerUml.getDefault().isUmlLikeEditor())
				return new IContributionItem[] {};

			selection = MyEditorManagerUml.getDefault().getdecodeCourant().getUmlElement();

			if (selection == null)
				return new IContributionItem[] {};
			return new IContributionItem[] { createMarteMenu() };
		} catch (Throwable e)
		{
			MyManager.printError(e);
			return new IContributionItem[] {};
		}

	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#fill(org.eclipse.swt.widgets.Menu,
	 *      int)
	 */
	@Override
	public void fill( Menu menu , int index )
	{

		super.fill(menu, index);
	}

	/*
	 * @see fr.inria.ctrte.plugalloc.menu.MenuInterface#getSelection()
	 */
	public Element getSelection()
	{
		return selection;
	}

}
