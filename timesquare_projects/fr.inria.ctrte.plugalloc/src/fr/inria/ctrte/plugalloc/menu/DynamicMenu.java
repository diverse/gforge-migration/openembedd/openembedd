/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.menu;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Element;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml;
import fr.inria.ctrte.plugalloc.action.About_Action;
import fr.inria.ctrte.plugalloc.action.ApplyProfile_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

/**
 * The Class DynamicMenu.
 */
public class DynamicMenu extends CommonMenu implements MenuInterface {

	/**
	 * Instantiates a new dynamic menu.
	 */
	public DynamicMenu()
	{
		super();
		
	}

	/**
	 * Instantiates a new dynamic menu.
	 * 
	 * @param id
	 *            the id
	 */
	public DynamicMenu(String id)
	{
		super(id);

	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#isDynamic()
	 */
	@Override
	public boolean isDynamic()
	{
		return true;
	}

	@Override
	public boolean isVisible()
	{
		IEditorPart editorPart = MyPluginManager.getActiveEditor();
		if (editorPart == null)
			return false;
		if (MyEditorManagerUml.getDefault().isUmlLikeEditor())
		{
			return true ;
		}
		return true;
	}  

	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.ContributionItem#isEnabled()
	 */
	@Override
	public boolean isEnabled()
	{
		/*IEditorPart editorPart = MyManager.getActiveEditor();
		if (editorPart == null)
			return false;
		if (MyEditorManagerUml.getDefault().isUmlLikeEditor())
		{*/
			return true ;
		/*}
		return false;*/
	//	return true; // super.isEnabled(); 
	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#isDirty()
	 */
	@Override
	public boolean isDirty()
	{
		return true;
	}

	/** The selection. */
	protected Element	selection	= null;

	/**
	 * @return the selection
	 */
	public Element getSelection()
	{
		return selection;
	}

	/**
	 * Gets the no um leditor mmenu.
	 * 
	 * @return the no um leditor mmenu
	 */
	protected IContributionItem[] getnoUMLeditorMmenu()
	{
		Action ac = new About_Action("About");
		ac.setToolTipText("About");
		IContributionItem m1 = new ActionContributionItem(ac);
		return new IContributionItem[] {  m1};
	}

	/**
	 * Creates the sub menu constraint.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return the menu manager
	 */

	/**
	 * Applied profil item.
	 * 
	 * @return the i contribution item
	 */
	private IContributionItem appliedProfilItem()
	{
		BaseDecode bd = MyEditorManagerUml.getDefault().getdecodeCourant();
		Action ac = new ApplyProfile_Action("Apply Marte Profile : Time", null, bd);
		ac.setToolTipText("Apply a Marte profile on ...");
		IContributionItem sb2 = new ActionContributionItem(ac);
		sb2.setVisible(true);
		return sb2;

	}

	@Override
	protected IContributionItem[] getContributionItems()
	{
		MenuManager mm = new MenuManager("Marte Menu");
		for( IContributionItem it:getContributionItems2())
			mm.add(it );
		return new IContributionItem[] {mm  };
	}
	
	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	
	protected IContributionItem[] getContributionItems2()
	{

		try
		{
			selection = null;
			IEditorPart editorPart = MyPluginManager.getActiveEditor();
			if (editorPart == null)
				return new IContributionItem[] {  };

			if (!MyEditorManagerUml.getDefault().isUmlLikeEditor())
			{
				return new IContributionItem[] {  };
			}

			selection = MyEditorManagerUml.getDefault().getdecodeCourant().getUmlElement();
			Action ac = new About_Action("About");
			ac.setToolTipText("About");
			IContributionItem m1 = new ActionContributionItem(ac);			
			if (selection == null)
			{
				return new IContributionItem[] { m1 }; // mode
			}
			MenuManager mm = new MenuManager("Constraint");
			for( IContributionItem it:createSubMenuConstraint())
				mm.add(it );

			IContributionItem sb2 = appliedProfilItem();

			return new IContributionItem[] {  sb2, mm,m1 }; // mode
		} catch (Throwable e)
		{
			MyManager.printError(e);
			return new IContributionItem[] {};
		}

	}

	/**
	 * @see org.eclipse.ui.actions.CompoundContributionItem#fill(org.eclipse.swt.widgets.Menu,
	 *      int)
	 */
	@Override
	public void fill( Menu menu , int index )
	{

		super.fill(menu, index);
	}

}
