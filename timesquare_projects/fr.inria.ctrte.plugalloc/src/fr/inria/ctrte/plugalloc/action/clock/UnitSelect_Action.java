/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.action.clock;



import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;

import code.CmyClock;
import code.Cumlcreator;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.box.ClockInstance2;

/**
 * The Class UnitSelect_Action.
 */
public class UnitSelect_Action extends Action {

	/** Constructor for Action1. */

	boolean	notstereo	= false;

	public boolean isntstereo()
	{
		return notstereo;
	}

	/**
	 * Instantiates a new unit select.
	 */
	BaseDecode	bd	= null;

	public UnitSelect_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		this.bd = bd;
		Element e=	bd.getUmlElement();

		 InstanceSpecification	instance	= null;
		 Property proprety=null;
		if( e instanceof InstanceSpecification)
		{
		instance = (InstanceSpecification) e;		
		 umlins = Cumlcreator.getInstance().createUmlElement(instance);
		}
		if( e instanceof Property)
		{
			proprety = (Property) e;		
		 umlins = Cumlcreator.getInstance().createUmlElement(proprety);
		}
	}
	CmyClock umlins=null ; 
	@Override
	public void run()
	{
		try
		{
			if (umlins == null)
				return;
		

			if (umlins.isClockStereotype() == false)
			{

				// MessageDialog.openInformation(new Shell(),
				// "Unit Selection ","Clock Stereotype missing");

				notstereo = true;

			} else
			{
			

			}
			
			Shell shell = new Shell();
			ClockInstance2 d = new ClockInstance2(shell, umlins);
			// DialogInstance d = new DialogInstance(shell, umlins, mode);
			d.create();
			int r = d.open();
			if (r == Window.OK)
			{
				d.apply(bd);
			}

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
