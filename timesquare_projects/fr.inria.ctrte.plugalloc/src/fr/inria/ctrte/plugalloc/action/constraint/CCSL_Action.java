package fr.inria.ctrte.plugalloc.action.constraint;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.uml2.uml.Package;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.time.parser.ui.work.MonParser;

public class CCSL_Action extends Action {

	Package	packages	= null;

	public CCSL_Action()
	{
		super();

	}

	BaseDecode	bdc	= null;
	IFile		f	= null;

	public CCSL_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		try
		{
			f =  (((FileEditorInput) MyPluginManager.getActiveEditor().getEditorInput()).getFile());
		} catch (Exception e)
		{
			// nothing
		}
		packages = (Package) bd.getUmlElement();
		bdc = bd;
	}

	@Override
	public void run()
	{
		try
		{
			MonParser p = (new MonParser());
			p.setFile(f);
			p.parse(packages);
			// bdc.notifymodification();
		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
