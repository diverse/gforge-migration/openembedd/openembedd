/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.action.constraint;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;

import code.Clist;
import code.CmyPackage;
import code.Cumlview;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.composite.property.ConstraintLigthDialog;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;


/**
 * The Class ConstraintAction.
 */
public class ConstraintLigth_Action extends Action {

	/** The bca. */
	BaseConstraintApi	bca		= null;

	/** The data. */
	private Element		data	= null;
	private BaseDecode	bce;

	/**
	 * Instantiates a new constraint action.
	 * 
	 * @param bca
	 *            the bca
	 * @param element
	 *            the element
	 */
	public ConstraintLigth_Action(BaseConstraintApi bca, BaseDecode bd)
	{
		super(bca.getName());
		this.bca = bca;
		data = bd.getUmlElement();
		setImageDescriptor(bca.geticone());
		setDescription(bca.toString());
		bce = bd;
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		CmyPackage cmp = new CmyPackage((Package) data);
		Clist lclock = cmp.getAllElementlist().getStereotypedby("Clock");
		lclock.add(Cumlview.getidealClock());
		bca.setLe(lclock);
		ConstraintLigthDialog d = new ConstraintLigthDialog(new Shell(), bca);
		int r = d.open();
		if (r == Window.OK)
		{
			d.apply(cmp);
			bce.notifymodification();
		}

	}

}
