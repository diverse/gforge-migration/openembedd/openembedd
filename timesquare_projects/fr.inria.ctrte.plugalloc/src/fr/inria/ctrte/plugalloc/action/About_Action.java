/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.action;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.graphics.Image;

import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.ActivatorPlug;

/**
 * The Class About_Action.
 */
public class About_Action extends Action {

	public static Image	imgred	= MyPluginManager.getImage(ActivatorPlug.PLUGIN_ID, "icons/redcircle.png");

	/*
	 * static Image imggreen =
	 * MyManager.getImage(ActivatorPlug.PLUGIN_ID,"icons/greencircle.png");
	 */

	/**
	 * Instantiates a new about action.
	 * 
	 * @param text
	 *            the text
	 */
	public About_Action(String text)
	{
		super(text);
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		//Shell shell = new Shell();
		MyPluginManager.getShowView("fr.inria.ctrte.plugalloc.info");
		//AboutDialog d = new AboutDialog(shell);
		//d.create();
		//Point p = d.getShell().getSize();
		//Rectangle r = d.getShell().getDisplay().getBounds();		
		//Point p2 = new Point(r.x + r.width / 2 - p.x / 2, r.y + r.height / 2 - p.y / 2);
		//d.getShell().setLocation(p2);
		//d.open();

	}
}
