/**
 * 
 */
package fr.inria.ctrte.plugalloc.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

/**
 * 
 * @author Benoit Ferrero
 * 
 * 
 *         The name as "_" because
 *         "org.eclipse.uml2.uml.editor.actions.ApplyProfileAction"
 */

public final class ApplyProfile_Action extends Action {
	/**
	 * 
	 */

	BaseDecode	bd;

	public ApplyProfile_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		this.bd = bd;
	}

	@Override
	public void run()
	{
		super.run();
		try
		{
			if (bd == null)
				return;
			Cumlview.loadMarteLibrary();
			Cumlview.loadMarteProfile();
			Cumlview.loadUmlPrimitiveLibrary();
			// Cumlview.touchModel(p);

			CmyPackage cmp = Cumlcreator.getInstance().createUmlElement(bd.getUmlElement())
					.getPackage();
			Cumlview.appliedTime(cmp);
			bd.notifymodification();
		} catch (Exception ex)
		{
			MyManager.printError(ex);

		} catch (Error ex)
		{
			MyManager.printError(ex);
		}
	}

}