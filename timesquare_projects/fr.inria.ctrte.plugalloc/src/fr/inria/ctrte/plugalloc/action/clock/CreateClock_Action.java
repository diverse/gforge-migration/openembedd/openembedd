package fr.inria.ctrte.plugalloc.action.clock;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Package;

import code.Cumlcreator;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.box.ClockInstance2;

public class CreateClock_Action extends Action {

	Package		packages	= null;	

	BaseDecode	bd;

	public CreateClock_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		this.bd = bd;

		packages = (Package) bd.getUmlElement();
	}

	@Override
	public void run()
	{
		try
		{

			Shell shell = new Shell();
			if (packages == null)
				MyManager.print("null is null" + bd);
			Cumlview.appliedTime(Cumlcreator.getInstance().createUmlElement(packages));
			ClockInstance2 d = new ClockInstance2(shell, Cumlcreator.getInstance()
					.createUmlElement(packages));
			d.create();
			int r = d.open();

			//
			if (r == Window.OK)
				d.apply(bd);

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
