package fr.inria.ctrte.plugalloc.action.constraint;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.uml2.uml.Constraint;

import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.time.parser.ui.work.MonParser;

public class ParserCCSL_Action extends Action {

	Constraint	constraint	= null;

	public ParserCCSL_Action()
	{
		super();

	}

	BaseDecode	bdc	= null;

	public ParserCCSL_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		this.constraint = (Constraint) bd.getUmlElement();
		bdc = bd;
	}

	@Override
	public void run()
	{
		try
		{
			(new MonParser()).parseConstraint(constraint);
			// Cumlview.touchModel(constraint);
		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
