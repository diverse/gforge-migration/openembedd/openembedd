package fr.inria.ctrte.plugalloc.action.clock;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.box.ClockInstance2;

public class Clock_Action extends Action {

	BaseDecode	bd;
	CmyPackage	cmp	= null;

	public Clock_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		this.bd = bd;
		cmp = (CmyPackage) Cumlcreator.getInstance().createUmlElement(bd.getUmlElement());
	}

	@Override
	public void run()
	{
		try
		{

			Shell shell = new Shell();
			if (cmp == null)
				MyManager.print("null is null" + bd);
			Cumlview.appliedTime(cmp);
			ClockInstance2 d = new ClockInstance2(shell, cmp);
			d.create();
			int r = d.open();

			//
			if (r == Window.OK)
				d.apply(bd);

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
