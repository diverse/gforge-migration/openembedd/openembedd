package fr.inria.ctrte.plugalloc.action.constraint;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Package;

import code.Clist;
import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.composite.property.CConstraintEditor;

public class ConstraintFull_Action extends Action {

	/** The data. */
	Package		packages	= null;

	BaseDecode	bdc			= null;

	public ConstraintFull_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);

		bdc = bd;
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		try
		{

			Shell shell = new Shell();
			this.packages = (Package) bdc.getUmlElement();
			CmyPackage cmp = Cumlcreator.getInstance().createUmlElement(packages);
			Cumlview.appliedTime(cmp.getModelof());
			Clist lclock = cmp.getAllElementlist().getStereotypedby("Clock");
			lclock.add(Cumlview.getidealClock());
			CConstraintEditor d = new CConstraintEditor(shell, lclock);
			d.create();
			int r = d.open();
			if (r == Window.OK)
			{
				d.apply(cmp);

			}
			bdc.notifymodification();
		} catch (Exception ex)
		{
			MyManager.printError(ex);

		}

	}

}
