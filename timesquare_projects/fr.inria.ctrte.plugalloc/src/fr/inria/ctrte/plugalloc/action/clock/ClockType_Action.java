package fr.inria.ctrte.plugalloc.action.clock;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;

import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.actions.ClockTypeEnumeration;
import fr.inria.ctrte.plugalloc.popup.box.ClockTypeInstance2;

public class ClockType_Action extends Action {

	ClockTypeEnumeration	cte;
	BaseDecode				basedecode	= null;

	public ClockType_Action(String text, ImageDescriptor image, ClockTypeEnumeration ctei,
			BaseDecode bd)
	{
		super(text, image);
		basedecode = bd;
		setElement(basedecode.getUmlElement());
		cte = ctei;
	}

	/** The element. */
	private Element	element		= null;

	/** The packages. */
	private Package	packages	= null;

	/** The classe. */
	private Class	classe		= null;

	/**
	 * @return the element
	 */
	public Element getElement()
	{
		return element;
	}

	/**
	 * @return the packages
	 */
	public Package getPackages()
	{
		return packages;
	}

	/**
	 * @return the classe
	 */
	public Class getClasse()
	{
		return classe;
	}

	/**
	 * @param element
	 *            the element to set
	 */
	private void setElement( Element element )
	{
		this.element = element;
		if (element instanceof Class)
		{
			classe = (Class) element;
		} else
		{
			classe = null;
		}
		if (element instanceof Package)
		{
			packages = (Package) element;
		} else
		{
			packages = null;
		}
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		if (element == null)
			return;
		try
		{
			Shell shell = new Shell();

			if (element != null)
				Cumlview.appliedTime(Cumlcreator.getInstance().createUmlElement(element)
						.getPackage());

			int n = 0;
			Cumlelement el = Cumlcreator.getInstance().createUmlElement(element);
			ClockTypeInstance2 d = new ClockTypeInstance2(shell, el, n, cte);
			d.create();
			int r = d.open();

			//
			if (r == Window.OK)
				d.apply();

		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

}
