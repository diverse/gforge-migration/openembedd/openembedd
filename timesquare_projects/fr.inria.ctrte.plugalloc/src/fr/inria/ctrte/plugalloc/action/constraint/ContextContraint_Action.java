/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.action.constraint;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.NamedElement;

import code.Clist;
import code.CmyConstraint;
import code.CmyPackage;
import code.Cumlcreator;
import code.Cumlelement;
import code.Cumlview;
import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.box.MyDialog;
import fr.inria.ctrte.plugalloc.popup.box.MyTextInput;

// "MARTE_Library::TimeLibrary::idealClock"

/**
 * The Class ContextContraint_Action.
 */
public class ContextContraint_Action extends Action {

	/** The constraint. */
	// private Constraint constraint = null;
	/** Constructor for Action1. */
	private Clist	m		= null;

	/** The l. */
	private Clist	l		= null;

	CmyConstraint	cmc		= null;
	Cumlelement		cnsp	= null;
	/**
	 * Instantiates a new context contraint.
	 */
	BaseDecode		bdc		= null;

	public ContextContraint_Action(String text, ImageDescriptor image, BaseDecode bd)
	{
		super(text, image);
		cmc = Cumlcreator.getInstance().createUmlElement((Constraint) bd.getUmlElement());
		bdc = bd;
	}

	/**
	 * Dialogint.
	 */
	private void dialogint()
	{
		MyDialog d = new MyDialog(new Shell(), "constraint", l, m, " Constaint element: ",
				"Constraint Element : " , "");
		d.open();
		d.remoded().disp();
		d.added().disp();
		if (Window.OK == d.getReturnCode())
		{

			cmc.clearListConstrainedElements();
			cmc.setConstraintto(d.getSelectedElements().toArray(new Cumlelement[] {}));
		}

	}

	private Clist listeclock( Cumlelement el )
	{
		l = new Clist();

		for (Cumlelement le : el.allownedofE().getLne())
		{

			if (le.getElement() instanceof ElementImport)
			{
				MyManager.println("IE " + ((ElementImport) le.getElement()).getName());
				NamedElement pe = ((ElementImport) le.getElement()).getImportedElement();
				if (pe != null)
				{
					if (pe.getQualifiedName().compareTo("MARTE_Library::TimeLibrary::idealClock") == 0)
					{
						MyManager.println("IE " + pe.getQualifiedName());
						idealclkfound = true;
					}
					if (pe.getQualifiedName().compareTo("MARTE_Library::TimeLibrary::idealClk") == 0)
					{
						MyManager.println("IE " + pe.getQualifiedName());
						idealclkfound = true;
					}
				}
			}

			if (m.contient(le.getElement()) == false)
				l.add(le);

		}
		return l;
	}

	/**
	 * Listpossiblecontraint.
	 */
	private boolean	idealclkfound	= false;

	private void listpossiblecontraint()
	{
		idealclkfound = false;

		listeclock(cnsp);

		if (idealclkfound == false)
		{
			MyManager.println("MARTE_Library::TimeLibrary::idealClock n est pas importe");

			boolean c = false;
			// PackageImport fd = null;
			CmyPackage cmp = null;
			Clist lp = cnsp.getPackageImport();
			if (lp.size() != 0)
			{
				Cumlelement pi = lp.getElementQualified("MARTE_Library::TimeLibrary");
				if (pi != null)
				{
					cmp = (CmyPackage) pi;
					c = true;
				}
			}
			if (c == false)
			{
				cmp = cnsp.createPackageImport(Cumlview.getPackageMarteLibrary());

			}

			MyManager.println("fd " + cmp.getElement().getMember("idealClock"));
			MyManager.println("fd " + cmp.getElement().getMember("idealClk"));

			Cumlelement m = cnsp.getRootPackage();
			m.getListPackageElement().disp();
			NamedElement value = cmp.getElement().getMember("idealClock");
			if (value == null)
				value = cmp.getElement().getMember("idealClk");
			l.getLne().add(0, cnsp.createElementImport(value, "impidealclk"));

		}

	}

	/**
	 * Run.
	 * 
	 * @param action
	 *            the action
	 * 
	 * @see IActionDelegate#run(IAction)
	 */
	@Override
	public void run()
	{
		try
		{
			if (cmc.getElement() == null)
				return;
			cnsp = cmc.getContext();
			Cumlview.appliedTime(cmc.getRootPackage());
			if (cnsp == null)
			{
				MyManager.print("choose a context : null");
				cnsp = cmc.setContextwhithOwner();
				if (cnsp == null)
				{
					return;
				}
			}
			m = cmc.getConstraintto();
			listpossiblecontraint();
			dialogint();
			setconstraint();
			bdc.notifymodification();
		} catch (Exception ex)
		{
			MyManager.printError(ex);
		}

	}

	/**
	 * Setconstraint.
	 * 
	 * @return the int
	 */
	private int setconstraint()
	{
		try
		{
		Shell shell = new Shell();
		String s = "";
		if (cmc.getElement().getSpecification() != null)
		{
			s = cmc.getElement().getSpecification().stringValue();
		}
		MyTextInput t = new MyTextInput(shell, s);
		t.open();
		if (t.getValue() != null)			
			MyManager.print(t.getValue());

		if (t.getReturnCode() == Window.OK)
		{
			cmc.makeParsable();
			cmc.setCCSLBody(t.getValue());
			bdc.notifymodification();
		}
		}
		catch (Exception ex) {
			MyManager.printError(ex);
		}
		catch (Error ex) {
			MyManager.printError(ex);
		}
		return 1;
	}

}
