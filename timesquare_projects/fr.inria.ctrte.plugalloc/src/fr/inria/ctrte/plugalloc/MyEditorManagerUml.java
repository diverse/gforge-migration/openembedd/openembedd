/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IEditorPart;
import org.osgi.framework.Bundle;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

/**
 * The Class MyEditorManagerUml.
 * 
 * @author Benoit Ferrero
 */
public class MyEditorManagerUml {

	/** The liste. */
	protected HashMap<String, EditorControl>	liste	= new HashMap<String, EditorControl>();

	/** The def. */
	static private MyEditorManagerUml			def		= null;

	/**
	 * Instantiates a new my editor manager uml.
	 */
	private MyEditorManagerUml()
	{

	}

	/**
	 * Gets the default.
	 * 
	 * @return the default
	 */
	public static MyEditorManagerUml getDefault()
	{
		if (def == null)
		{
			def = new MyEditorManagerUml();
			def.scaneditor();
		}
		return def;
	}

	/**
	 * Checks if is uml like editor.
	 * 
	 * @return true, if is uml like editor
	 */
	public boolean isUmlLikeEditor()
	{
		IEditorPart iep = MyPluginManager.getCourantEditor();
		//MyManager.println(iep);
		return liste.containsKey(iep.getClass().getName());
	}

	/**
	 * Gets the convert util.
	 * 
	 * @return the convert util
	 */
	public Object getConvertUtil()
	{

		return null;
	}

	/**
	 * 
	 */
	@SuppressWarnings("all")
	public int add( Class c , EditorControl o )
	{
		liste.put(c.getName(), o);
		return 0;
	}

	/**
	 * 
	 */
	public int add( String s , EditorControl o )
	{

		liste.put(s, o);
		return 0;
	}

	/**
	 * Gets the list uml editor.
	 * 
	 * @return the list uml editor
	 */
	public ArrayList<String> getlistUMLEditorString()
	{
		ArrayList<String> albca = new ArrayList<String>(liste.keySet());
		return albca;
	}

	public ArrayList<EditorControl> getlistUMLEditor()
	{
		ArrayList<EditorControl> albca = new ArrayList<EditorControl>(liste.values());
		return albca;
	}

	/**
	 * Gets the decodeur.
	 * 
	 * @param s
	 * 
	 * @return the decodeur
	 */
	public BaseDecode getdecodeur( String s )
	{
		return liste.get(s).getBd();
	}

	/**
	 * 
	 * @return BaseDecode Class associate to a courant editor
	 */
	public BaseDecode getdecodeCourant()
	{
		return liste.get(MyPluginManager.getCourantEditor().getClass().getName()).getBd();
	}

	public void scaneditor()
	{
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] extensions = reg
				.getConfigurationElementsFor("fr.inria.ctrte.plugalloc.UML_editor");

		for (IConfigurationElement ice : extensions)
		{
			instanceone(ice);
		}
	}

	// @SuppressWarnings({ "deprecation", "unchecked" })



	private void instanceone( IConfigurationElement ice )
	{
		try
		{
			EditorControl ec = new EditorControl(ice);

			add(ec.getEditor(), ec);
		}catch (ClassNotFoundException e) {
			MyManager.printlnError("Editor not found :");
		} 
		catch (Exception e)
		{
			MyManager.printError(e);

		} catch (Error e)
		{
			MyManager.printError(e);

		}

	}

	public class EditorControl {
		String					editor		= null;
		String					plugin		= null;
		String					providerid	= null;
		String					decodeur	= null;
		IConfigurationElement	ice			= null;
		BaseDecode				bd			= null;
		Bundle					b			= null;
		boolean					extention	= true;

		public EditorControl(IConfigurationElement icein) throws Exception, Error
		{
			super();
			ice = icein;
			editor = ice.getAttribute("editor");
			plugin = ice.getDeclaringExtension().getNamespaceIdentifier();
			b = Platform.getBundle(plugin);
			decodeur = ice.getAttribute("Decodeur");
			bd = (BaseDecode) b.loadClass(decodeur).newInstance();
			providerid = b.getHeaders().get("Bundle-Vendor").toString();
		}

		public String getEditor()
		{
			return editor;
		}

		public String getPlugin()
		{
			return plugin;
		}

		public String getProviderid()
		{
			return providerid;
		}

		public String getDecodeur()
		{
			return decodeur;
		}

		public IConfigurationElement getIce()
		{
			return ice;
		}

		public BaseDecode getBd()
		{
			return bd;
		}

		public String info()
		{
			List<String> ls = MyPluginManager.listingEditor(false);
			String s = null;

			if (ls.contains(editor))
			{
				s = "<< Editor : " + editor + "\n>>";
			} else
			{
				s = "<<  Editor  " + editor + ": missing editor ; \n>>";
			}

			/*
			 * s+="\n"; String s2= "<< Extension :\n" + "     Decodeur    : "+
			 * decodeur + "\n" + "     in Plugin   : " +plugin +"\n"+
			 * "        Provider : " +providerid +"\n"+ ">>";
			 */
			return s + " \n";// +s2;
		}
	}

}
