/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.inria.base.MyManager;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml.EditorControl;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;

/**
 * The Class ConstraintDictionnary.
 */
@SuppressWarnings("all")
public class ConstraintDictionnary {

	/** The constraint. */
	private static ConstraintDictionnary	constraint	= null;

	/**
	 * Instantiates a new constraint dictionnary.
	 */
	protected ConstraintDictionnary()
	{

	}

	/**
	 * Gets the default.
	 * 
	 * @return the default
	 */
	public static ConstraintDictionnary getDefault()
	{
		if (constraint == null)
		{
			constraint = new ConstraintDictionnary();
			constraint.scanconstraint();
		}
		return constraint;
	}

	/** The liste. */
	protected HashMap<Class, BaseConstraintApi>	liste	= new HashMap<Class, BaseConstraintApi>();

	private ArrayList<Class>					keylist	= new ArrayList<Class>();

	/**
	 * 
	 * 
	 * @param o
	 * 
	 * 
	 * @return the int
	 */
	public int add( BaseConstraintApi o )
	{
		if (o == null)
			return 0;
		if (liste.get(o.getClass()) == null)
		{
			keylist.add(o.getClass());
		}
		liste.put(o.getClass(), o);

		return 0;
	}

	/**
	 * Gets the list.
	 * 
	 * @return the list
	 */
	protected HashMap<Class, BaseConstraintApi> getListe()
	{
		return liste;
	}

	/**
	 * Gets the list.
	 * 
	 * @return the list of value
	 * 
	 *         same ArrayList<BaseConstraintApi> albca = new
	 *         ArrayList<BaseConstraintApi>(liste.value()); with a same order
	 *         value
	 * 
	 */
	public ArrayList<BaseConstraintApi> getlist()
	{

		ArrayList<BaseConstraintApi> albca = new ArrayList<BaseConstraintApi>();
		for (Class c : keylist)
		{
			albca.add(liste.get(c));
		}
		return albca;

	}

	public void scanconstraint()
	{
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] extensions = reg.getConfigurationElementsFor("fr.inria.ctrte.plugalloc.ConstraintWizard");

		for (IConfigurationElement ice : extensions)
		{
			instanceone(ice);
		}
	}

	private void instanceone( IConfigurationElement ice )
	{

		try
		{
			String plugin = ice.getDeclaringExtension().getNamespaceIdentifier();
			Bundle b = Platform.getBundle(plugin);
			String nameclass = ice.getAttribute("class");
			BaseConstraintApi bca = (BaseConstraintApi) b.loadClass(nameclass).newInstance();
			add(bca);
		} catch (Exception e)
		{
			MyManager.printError(e);

		} catch (Error e)
		{
			MyManager.printError(e);

		}

	}
}
