/*
 * 
 * @author : Benoit Ferrero INRIA/I3S Aoste Copyright  2007-2008
 */
package fr.inria.ctrte.plugalloc.decode;

import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Element;

/**
 * The Class BaseDecode.
 * 
 * @author Benoit Ferrero
 */
public class BaseDecode {

	/**
	 * Instantiates a new base decode.
	 */
	IEditorPart	iep	= null;

	public BaseDecode()
	{
		this("noname");
	}

	Element	element	= null;

	/**
	 * @return the element
	 */
	/*
	 * public Element getElement() { return element; }
	 */

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/** The name. */
	private String	name	= null;

	/**
	 * Instantiates a new base decode.
	 * 
	 * @param name
	 *            the name
	 */
	public BaseDecode(String name)
	{
		super();
		this.name = name;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{

		return name;
	}

	public Element getUmlElement()
	{
		return null;
	}

	public int notifymodification()
	{

		return 0;
	}

	/*
	 * private String pluginid=null; private String providerplugin=null;
	 * 
	 * public String getPluginid() { return pluginid; }
	 * 
	 * public void setPluginid(String pluginid) { this.pluginid = pluginid; }
	 * 
	 * public String getProviderplugin() { return providerplugin; }
	 * 
	 * public void setProviderplugin(String providerplugin) {
	 * this.providerplugin = providerplugin; }
	 */

}
