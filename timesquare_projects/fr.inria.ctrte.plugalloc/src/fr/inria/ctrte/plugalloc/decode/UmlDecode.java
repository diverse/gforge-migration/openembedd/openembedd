package fr.inria.ctrte.plugalloc.decode;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.uml2.uml.Element;

import code.Cumlview;

import fr.inria.base.MyPluginManager;

public class UmlDecode extends BaseDecode {

	Element	uml	= null;

	public UmlDecode()
	{
		super("DefaultUmlEditor");

	}

	public UmlDecode(String s)
	{
		super("uml " + s);

	}

	@Override
	public Element getUmlElement()
	{
		ISelection selection = MyPluginManager.getSelection();
		if (selection instanceof StructuredSelection)
		{
			Object o = ((StructuredSelection) selection).getFirstElement();
			if (o instanceof Element)
			{
				uml = (Element) o;
				uml.eSetDeliver(true);
				return uml;

			}
			uml = null;
		}
		return null;
	}

	@Override
	public int notifymodification()
	{
		Cumlview.touchModel(uml);
		return 0;
	}

}
