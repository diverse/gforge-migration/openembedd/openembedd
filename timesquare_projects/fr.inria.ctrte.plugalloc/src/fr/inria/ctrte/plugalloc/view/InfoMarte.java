package fr.inria.ctrte.plugalloc.view;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.pde.internal.ui.search.ShowDescriptionAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.part.ViewPart;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.ActivatorPlug;
import fr.inria.ctrte.plugalloc.ConstraintDictionnary;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml;
import fr.inria.ctrte.plugalloc.MyEditorManagerUml.EditorControl;
import fr.inria.ctrte.plugalloc.action.About_Action;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;
import fr.inria.wave.DictionaryPolitic;


@SuppressWarnings("restriction")
public class InfoMarte extends ViewPart {

	public InfoMarte()
	{

	}



	private Table table1 = null;
	private Table table2 = null;
	private Table table3 = null;
	private Tree tree4 = null;
	private Table table5 = null;
	
	private Composite tab1 = null;
	private Composite tab2 = null;

	private TabFolder folder = null;

	TabItem item4, item2, item0, item1, item3;

	@Override
	public void createPartControl( Composite parent )
	{
		try
		{
			folder = new TabFolder(parent, SWT.CENTER | SWT.FILL | SWT.TAB | SWT.BORDER);
			item4 = createTabItem(folder,"( TimeSquare ) " , tablTSQ(folder)) ;
			item2 = createTabItem(folder,"( Constraint wizard ) ",tablconstraint(folder));	
			item0 = createTabItem(folder,"( Plugin )"	,tablplug(folder)); 		
			item1 = createTabItem(folder,"( Editor )",tableditor(folder));		
			item3 = createTabItem(folder,"( PathMap UML  )",	tablprofil(folder));			
			item3 = createTabItem(folder,"( Politic Simulation )",tablpolitic(folder));
			folder.pack();		
			folder.setSelection(0);
			
		} catch (Throwable t)
		{
			MyManager.printError(t);
		}

	}

	private TableColumn createTableColumn( Table table , int swt , int index , String label ,
			int size , boolean move , boolean resize )
	{
		TableColumn column = null;
		column = new TableColumn(table, swt, index);
		column.setText(label);
		column.setMoveable(move);
		column.setResizable(resize);
		if (size != -1)
			column.setWidth(size);
		return column;
	}
	
	private TabItem createTabItem( TabFolder fd , String s, Composite ctrl)
	{
		TabItem item= new TabItem(fd, SWT.RIGHT_TO_LEFT);
		
		item.setText(s);
		item.setControl(ctrl);
		return item;
	}
	
	private void layout(Composite c)
	{
		GridLayout gl = null;
		gl = new GridLayout(1, true);
		gl.marginBottom = 4;
		gl.marginTop = 4;
		gl.marginLeft = 7;
		gl.marginRight = 7;
		gl.marginHeight = 2;
		gl.marginWidth = 2;
		c.setLayout(gl);
	}
	
	private Table createTable(Composite composite)
	{
		Table lctable = new Table(composite, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION | SWT.FILL);

	GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
	data.heightHint = 150;
	data.widthHint = 600;
	lctable.setLayoutData(data);
	lctable.setLinesVisible(true);
	lctable.setHeaderVisible(true);
		return lctable;
	}

	private final class FileumlFilter implements FilenameFilter {
		public boolean accept( File dir , String name )
		{
			return (name.endsWith(".uml"));
		}
	}

	public final class SortListener implements Listener {
		public void handleEvent( Event e )
		{
			TableColumn sortColumn = table2.getSortColumn();

			TableColumn column = (TableColumn) e.widget;
			int n = 0;
			int index = 0;
			for (TableColumn tc : table2.getColumns())
			{
				if (tc == column)
					index = n;
				n++;
			}
			int dir = table2.getSortDirection();
			if (sortColumn == column)
			{
				dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
			} else
			{
				table2.setSortColumn(column);
				dir = SWT.UP;
			}
			sortConstraint(index, dir == SWT.UP);
			table2.setSortDirection(dir);
		}
	}

	public class ListenerLink implements Listener {

		private ListenerLink()
		{
			super();
		}

		public void handleEvent( Event event )
		{
			try
			{
			
				if ("constraint".compareTo(event.text) == 0)
				{
					tab1.setFocus();
					folder.setSelection(1);							
					
				}
				if ("constraintdef".compareTo(event.text) == 0)
				{
														
					openSchemaHtml("fr.inria.ctrte.plugalloc.ConstraintWizard");							
					
				}
				if ("editor".compareTo(event.text) == 0)
				{
					tab2.setFocus();
					folder.setSelection(3);		
					
					
				}
				if ("editordef".compareTo(event.text) == 0)
				{					
					openSchemaHtml("fr.inria.ctrte.plugalloc.UML_editor");	
					
				}
				if ("politic".compareTo(event.text) == 0)
				{
					tab2.setFocus();
					folder.setSelection(5);						
				}
				if ("politicdef".compareTo(event.text) == 0)
				{				
					openSchemaHtml("fr.inria.vcd.simulation.politicSimulation");						
				}
				if ("site".compareTo(event.text) == 0)
				{

					IWorkbench iw = PlatformUI.getWorkbench();						
					IWebBrowser wb = iw.getBrowserSupport().createBrowser("TimeSquare");
					wb.openURL(new URL("http://www-sop.inria.fr/aoste/dev/time_square"));

				}
				if ("vcd".compareTo(event.text) == 0)
				{
					IWorkbench iw = PlatformUI.getWorkbench();
					iw.getHelpSystem()
					.displayHelpResource("/fr.inria.vcd.help/html/Vcd_Viewer.htm");

				}

				if ("ccsl".compareTo(event.text) == 0)
				{
					IWorkbench iw = PlatformUI.getWorkbench();
					iw.getHelpSystem().displayHelpResource(
					"/fr.inria.ctrte.time.doc/html/marteTimeModelDeliverableImage.htm");

				}

				if ("bug".compareTo(event.text) == 0)
				{
					IWorkbench iw = PlatformUI.getWorkbench();
					IWebBrowser wb = iw.getBrowserSupport().createBrowser("TimeSquare");
					wb.openURL(new URL("http://www-sop.inria.fr/aoste/?r=9&s=30&tsq=7"));

				}

			} catch (Throwable ex)
			{
				MyManager.printError(ex);
			}

		}
	}
	
	
	public void openSchemaHtml(String name)
	{
		new ShowDescriptionAction(name).run();		
		
	}

	
	private Composite tablTSQ( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		Composite tab0 = xparent;
		layout(xparent);
		
		Link link = new Link(xparent, SWT.NONE);
		String text = "TimeSquare define a Extension Point :\n"
			+ "    <a href=\"constraint\">fr.inria.ctrte.plugalloc.ConstraintWizard</a> <a href=\"constraintdef\">( definition )</a> \n"
			+ "    <a href=\"editor\">fr.inria.ctrte.plugalloc.UML_editor</a> <a href=\"editordef\">( definition )</a>\n"
			+ "    <a href=\"politic\">fr.inria.vcd.simulation.politicSimulation</a> <a href=\"politicdef\">( definition )</a>\n";
		link.setText(text);
		link.setVisible(true);
		link.addListener(SWT.Selection, new ListenerLink());
		Link link2 = new Link(xparent, SWT.NONE);
		text = "Offical Web site : <a href=\"site\">www-sop.inria.fr/aoste/dev/time_square</a> \n";
		link2.setText(text);
		link2.setVisible(true);
		link2.addListener(SWT.Selection, new ListenerLink());
		Link link3 = new Link(xparent, SWT.NONE);
		text = "Help : \n" + "      <a href=\"vcd\">VCD Viewer Help</a> \n"
		+ "      <a href=\"ccsl\">CCSL</a> \n";
		link3.setText(text);
		link3.setVisible(true);
		link3.addListener(SWT.Selection, new ListenerLink());
		Link link4 = new Link(xparent, SWT.NONE);
		text = "<a href=\"bug\">Bug Report On web Site</a> \n";

		link4.setText(text);
		link4.setVisible(true);
		link4.addListener(SWT.Selection, new ListenerLink());
		return tab0;
	}

	private Composite tableditor( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		tab1 = xparent;
		layout(xparent);	
		Label l;		
		l = new Label(xparent, SWT.FILL);
		l.setText("List of editor UML detected");
		table1 = createTable(xparent);
			
	

		TableColumn column = null;
		column = createTableColumn(table1, SWT.CENTER, 0, "", 25, false, true);
		column = createTableColumn(table1, SWT.LEFT, 1, "Editor UML", -1, false, true);
		column.setToolTipText("name of editor UML with Marte Support");
		column = createTableColumn(table1, SWT.LEFT, 2, "Decoder", -1, false, true);
		column.setToolTipText("object name for Marte support");

		List<String> ls = MyPluginManager.listingEditor(true);
		for (EditorControl ed : MyEditorManagerUml.getDefault().getlistUMLEditor())
		{
			TableItem item = new TableItem(table1, SWT.NONE);
			String s = ed.getEditor();
			item.setText(1, s);
			if (ls.contains(s))
			{
				item.setImage(0, MyPluginManager.getEditorIcon(s));
			} else
			{
				item.setImage(0, About_Action.imgred);
			}
			item.setData(ed.info());
			BaseDecode o = ed.getBd();
			if (o != null)
				item.setText(2, o.getName());
			else
				item.setText(2, "Not Found ");

		}

		table1.setToolTipText(null);
		table1.setSize(250, 100);

		for (int i = 0; i < table1.getColumnCount(); i++)
		{
			table1.getColumn(i).pack();
		}
		table1.pack();
		return tab1;
	}

	private Composite tablconstraint( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		tab2 = xparent;
		layout(xparent);
		Label l = new Label(xparent, SWT.FILL);
		l.setText("List of Wizard Constraint");
		table2 = createTable(xparent);

		Listener sortListener = new SortListener();

		TableColumn column = null;
		column = createTableColumn(table2, SWT.CENTER, 0, " ", 25, false, false);
		column.addListener(SWT.Selection, sortListener);

		column = createTableColumn(table2, SWT.LEFT, 1, "   Constraint Name    ", 175, false, true);
		column.addListener(SWT.Selection, sortListener);

		column = createTableColumn(table2, SWT.LEFT, 2, "Type", 150, false, false);
		column.addListener(SWT.Selection, sortListener);
		table2.setSortColumn(column);
		column.addListener(SWT.Selection, sortListener);

		column = createTableColumn(table2, SWT.LEFT, 3, "Native", 60, false, false);
		column.addListener(SWT.Selection, sortListener);

		column = createTableColumn(table2, SWT.LEFT, 4, "   Information     ", 300, false, true);
		column.addListener(SWT.Selection, sortListener);

		for (BaseConstraintApi bca : ConstraintDictionnary.getDefault().getlist())
		{
			TableItem item = new TableItem(table2, SWT.NONE);
			item.setData(bca);
			updateItemConstraint(item);
		}
		sortConstraint(1, false);
		table2.setToolTipText(null);
		table2.setSize(250, 100);
		for (int i = 0; i < table2.getColumnCount(); i++)
		{
			// table2.getColumn(i).pack();
			table2.computeSize(-1, -1);
		}
		table2.pack(true);
		return tab2;
	}

	private void sortConstraint( int n , boolean b )
	{
		// MyManager.println("sort " +n +"  ->" +b );
		int max = table2.getItemCount();
		for (int i = 0; i < max; i++)
			for (int j = 0; j < max; j++)
			{
				if (i != j)
				{
					BaseConstraintApi n1 = ((BaseConstraintApi) table2.getItem(i).getData());
					BaseConstraintApi n2 = ((BaseConstraintApi) table2.getItem(j).getData());
					if (n == 1)
					{
						if (b && (n1.getName().compareTo(n2.getName()) < 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						}

						if (!b && (n1.getName().compareTo(n2.getName()) > 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						}
					}
					if (n == 2)
					{
						if (b && (n1.getType().toString().compareTo(n2.getType().toString()) < 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						} else if (!b
								&& (n1.getType().toString().compareTo(n2.getType().toString()) > 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						}
					}
					if (n == 3)
					{
						if (b && (!n1.isNative() && n2.isNative()))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						} else if (!b && (n1.isNative() && !n2.isNative()))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						}
					}
					if (n == 4)
					{
						if (b
								&& (n1.getComment().toString()
										.compareTo(n2.getComment().toString()) < 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						} else if (!b
								&& (n1.getComment().toString()
										.compareTo(n2.getComment().toString()) > 0))
						{
							table2.getItem(i).setData(n2);
							table2.getItem(j).setData(n1);
						}
					}

				}
			}
		for (TableItem item : table2.getItems())
		{
			updateItemConstraint(item);
		}
	}

	private void updateItemConstraint( TableItem item )
	{
		try
		{
		BaseConstraintApi bca = (BaseConstraintApi) item.getData();
		String name = bca.getName();
		item.setImage(0, bca.geticone().createImage());
		item.setText(1, "    " + name + "   ");
		item.setText(2, "    " + bca.getType().toString() + "   ");
		item.setText(3, "" + bca.isNative());
		item.setText(4, "    " + bca.getComment() + "   ");
		}
		catch ( Throwable t)
		{
			
		}
	}

	static final String featplug[] = new String[] { "fr.inria.ctrte.supporteditor",
		"fr.inria.ctrte.time.feature", "fr.inria.vcd.feature", "fr.inria.ctrte.bdd.feature" };

	static final String pluglst[] = new String[] { "fr.inria.base", "fr.inria.ctrte.baseuml",
		"fr.inria.ctrte.extendeditor", "fr.inria.ctrte.plugalloc", "fr.inria.ctrte.time.doc",
		"fr.inria.ctrte.time.parser", "fr.inria.ctrte.time.parser.ui", "fr.inria.vcd",
		"fr.inria.vcd.antlr", "fr.inria.vcd.help", "fr.inria.vcd.simulator",
		"fr.inria.ctrte.bdd", "fr.inria.ccsl.generator", "fr.inria.ccsl.editor",
		"fr.inria.ctrte.dev.constraint", "fr.inria.ctrte.demo" };

	private Composite tablplug( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		Composite tab3 = xparent;
		layout(xparent);
		Label l = new Label(xparent, SWT.FILL);
		l.setText("List of plugin");
		table3 = createTable(xparent);
		
		createTableColumn(table3, SWT.LEFT, 0, "   Plugin   ", 350, false, true);
		createTableColumn(table3, SWT.LEFT, 1, "   Version      ", 250, false, true);
		 createTableColumn(table3, SWT.LEFT, 2, "   Provider      ", 250, false, true);
		createTableColumn(table3, SWT.LEFT, 3, "   Information      ", 400, false, true);
		
		for (String s : pluglst)
		{
			TableItem item = new TableItem(table3, SWT.NONE);
			item.setText(0, s);
			item.setText(1, " " + MyPluginManager.getPluginVersion(s) + "   ");
			item.setText(2, " " + MyPluginManager.getPluginVendor(s) + "  ");
			item.setText(3, " " + MyPluginManager.getPluginDescription(s) + "  ");

		}
		for (int i = 0; i < table3.getColumnCount(); i++)
		{
			table3.getColumn(i).pack();
			table3.computeSize(-1, -1);
		}
		table3.pack();
		return tab3;
	}

	private Composite tablprofil( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		Composite tab4 = xparent;
		layout(xparent);
		Label l = new Label(xparent, SWT.FILL);
		l.setText("List Profil UML");
		tree4 = new Tree(xparent, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.heightHint = 150;
		data.widthHint = 600;
		tree4.setLayoutData(data);
		tree4.setLinesVisible(true);
		tree4.setHeaderVisible(true);
		TreeColumn column = null;
			
		column = new TreeColumn(tree4, SWT.CENTER, 0);
		column.setText("");
		column.setMoveable(false);
		column.setWidth(65);
		

		column = new TreeColumn(tree4, SWT.LEFT, 1);
		column.setText(" Identifiant ");
		column.setMoveable(false);
		column.setWidth(350);

		column = new TreeColumn(tree4, SWT.LEFT, 2);
		column.setText(" Plugin ");
		column.setMoveable(false);
		column.setWidth(100);

		column = new TreeColumn(tree4, SWT.LEFT, 3);
		column.setText(" Plugin version ");
		column.setMoveable(false);
		column.setWidth(100);

		for (URI key : URIConverter.URI_MAP.keySet())
		{
			TreeItem item = new TreeItem(tree4, SWT.NONE);
			item.setImage(0, MyPluginManager.getImage(ActivatorPlug.PLUGIN_ID,
			"icons/file/icon_File.gif"));

			item.setText(1, " " + key.toString() + "   ");
			// item.setText(2, " " + URIConverter.URI_MAP.get(key) + "  ");
			String name = URIConverter.URI_MAP.get(key).segments()[1];
			String version = MyPluginManager.getPluginVersion(name);
			if (version.compareTo("") != 0)
			{
				item.setText(2, " " + name + " ");
				item.setText(3, " " + version + "  ");
			}
			URI uri = URIConverter.URI_MAP.get(key);
			if (uri.isPlatformPlugin())
			{
				try
				{

					// PlatformUI.getWorkbench()
					// FileLocator.
					String s1 = uri.toPlatformString(false).substring(1);
					int n = s1.indexOf("/");
					String s2 = s1.substring(0, n);
					String s3 = s1.substring(n + 1);
					if (Platform.getBundle(s2) != null)
					{

						URL url = Platform.getBundle(s2).getEntry(s3);
						IPath jrePath = new Path(FileLocator.toFileURL(url).getFile());
						if (jrePath.toFile().isDirectory())
						{
							item.setImage(0, MyPluginManager.getImage(ActivatorPlug.PLUGIN_ID,
							"icons/file/icon_OpenFolder.gif"));

							String ls[] = jrePath.toFile().list(new FileumlFilter());
							for (String s : ls)
							{
								TreeItem item2 = new TreeItem(item, SWT.NONE);
								item2.setText(1, s);
								item2.setData(jrePath);
								item2.setImage(0, MyPluginManager.getImage(ActivatorPlug.PLUGIN_ID,
								"icons/file/icon_File.gif"));

							}

						}

					}
				} catch (Exception ex)
				{
					// TODO: handle exception
				}
			}
			
		
			
		}

		for (int i = 1; i < tree4.getColumnCount(); i++)
		{
			tree4.getColumn(i).pack();
		}
		tree4.pack();
		return tab4;
	}

	private Composite tablpolitic( Composite inparent )
	{
		Composite xparent = new Composite(inparent, SWT.FILL);
		Composite tab5 = xparent;
		layout(xparent);
		Label l = new Label(xparent, SWT.FILL);
		l.setText("List of Politic of simulation");
		table5 = createTable(xparent);
		
		createTableColumn(table5, SWT.LEFT, 0, "   Politic Name   ", 350, false, true);
		createTableColumn(table5, SWT.LEFT, 1, "   Comment      ", 250, false, true);
		createTableColumn(table5, SWT.LEFT, 2, "   Class      ", 250, false, true);

		

		int n = DictionaryPolitic.getInstance().getSize();
		for (int i = 0; i < n; i++)
		{
			TableItem item = new TableItem(table5, SWT.NONE);
			item.setText(0, DictionaryPolitic.getInstance().getLname(i));
			item.setText(1, DictionaryPolitic.getInstance().getComment(i));
			item.setText(2, DictionaryPolitic.getInstance().getClass(i).getName());
		}

		for (int i = 0; i < table5.getColumnCount(); i++)
		{
			table5.getColumn(i).pack();
			table5.computeSize(-1, -1);
		}
		table5.pack();
		return tab5;
	}

	@Override
	public void setFocus() 
	{
		folder.setFocus();
	}


}
