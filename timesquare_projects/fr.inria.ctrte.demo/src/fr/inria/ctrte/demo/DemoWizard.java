package fr.inria.ctrte.demo;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;

public class DemoWizard extends BasicNewProjectResourceWizard implements INewWizard {

	public DemoWizard()
	{
		
	}

	protected WizardNewProjectCreationPage mainPage;

	
	public void addPages()
	{
		mainPage = new WizardNewProjectCreationPage("CCSL DEMO Project page");
		mainPage.setDescription("Create a new CCSL DEMO project");
		mainPage.setTitle("CCSL DEMO project :");
		mainPage.setMessage("Hello");
		addPage(mainPage);

	}

	@Override
	public boolean performFinish()
	{
		try
		{
			newProject = createProject();
			if (newProject == null)
				return true;
			updatePerspective();
			selectAndReveal(newProject, MyPluginManager.getActiveWorkbench());
			//
		} catch (Throwable ex)
		{

		}
		return true;
	}

	public void init( IWorkbench workbench , IStructuredSelection selection )
	{
		super.init(workbench, selection);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard#
	 * getNewProject()
	 */

	protected IProject newProject;
	 IProject newProjectHandle=null;
	protected IProject createProject()
	{
		if (newProject != null)
			return newProject;

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		if (mainPage==null)
			return newProject;
		  newProjectHandle = mainPage.getProjectHandle();
		final IProjectDescription description = workspace.newProjectDescription(newProjectHandle
				.getName());
		if (!mainPage.useDefaults())
			description.setLocation(mainPage.getLocationPath());

		WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
			protected void execute( IProgressMonitor monitor ) throws CoreException
			{
				createProject(description, newProjectHandle, monitor);
			}
		};

		try
		{
			getContainer().run(true, true, operation);
		} catch (InterruptedException e)
		{
			return null;
		} catch (InvocationTargetException e)
		{
			MessageDialog.openError(getShell(), "Cannot create a CCSL demo project", e
					.getTargetException().getMessage());
			return null;
		}

		return newProjectHandle;
	}

	protected void createProject( IProjectDescription description , IProject projectHandle ,
			IProgressMonitor monitor ) throws CoreException , OperationCanceledException
			{
		try
		{
			monitor.beginTask("Create Project", 200);
			projectHandle.create(description, new SubProgressMonitor(monitor, 100));
			CopyCCSLFile ccslfile= new CopyCCSLFile(projectHandle);
			ccslfile.create();
		//	projectHandle.getLocation().append("CCSL")
			//monitor.done();
			if (monitor.isCanceled())
				throw new OperationCanceledException();
			projectHandle.open(new SubProgressMonitor(monitor, 100));

		} catch (Throwable ex)
		{
			ex.printStackTrace();
			MyManager.printError(ex);
			ex.printStackTrace();
		} finally
		{
			monitor.done();
		}
			}

}
