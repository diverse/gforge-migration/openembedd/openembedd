package fr.inria.ctrte.demo;

import java.net.URL;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;

import code.CinterfaceUML;
import code.CmyClass;
import code.CmyConstraint;
import code.CmyEnumeration;
import code.CmyEnumerationLiteral;
import code.CmyInstance;
import code.CmyModel;
import code.CmyProperty;
import code.Cumlcreator;
import code.Cumlview;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.popup.work.Ccreateclock;



public class CopyCCSLFile {

	IProject project;

	public CopyCCSLFile(IProject project)
	{
		super();
		this.project = project;
	}

	public void create()
	{
		try
		{
			MyManager.printOKln("Create Project "+ project.getName()+ " !! " );			
			MyPluginManager.createdir(project.getLocation(), "DOC");
			MyManager.printOKln("Create Doc Folder  : [OK] " );
			copyfileDOC("easter.pdf");			
			MyPluginManager.createdir(project.getLocation(), "CCSL");
			MyManager.printOKln("Create CCSL Folder : [OK] ");			
			copyfileCCSL("easter.ccsl");
			copyfileCCSL("test.ccsl");
			copyfileCCSL("test4.ccsl");
			MyManager.printOKln("Copy file CCSL     : [OK] ");			
			MyPluginManager.createdir(project.getLocation(), "UML");
			MyManager.printOKln("Create UML Folder  : [OK] ");			
			createEasterUML();
			MyManager.printOKln("Create UML Easter Example : [OK]");			
			createImageUML();
			MyManager.printOKln("Create UML Image Example  : [OK]");
			MyPluginManager.refreshWorkspace();
		} catch (Throwable t)
		{
			MyManager.printError(t, "chat alors");
		}
	}

	public void copyfileCCSL( String s )
	{
		try
		{
			URL url = MyPluginManager.getInstallUrl(Activator.getDefault().getBundle(), "/ressource/" + s);		
			IPath ph = project.getLocation().append("CCSL").append(s);
			MyPluginManager.copy(MyPluginManager.getIFileStore(url), MyPluginManager.getIFileStore(ph),EFS.OVERWRITE, null);
		} catch (Throwable e)
		{
			MyManager.printError(e, "Fail copy file :" + s);
		}
	}

	public void copyfileDOC( String s )
	{
		try
		{
			URL url = MyPluginManager.getInstallUrl(Activator.getDefault().getBundle(), "/ressource/" + s);		
			IPath ph = project.getLocation().append("DOC").append(s);
			MyPluginManager.copy(MyPluginManager.getIFileStore(url), MyPluginManager.getIFileStore(ph),
					EFS.OVERWRITE, null);
		} catch (Throwable e)
		{
			MyManager.printError(e, "Fail copy file :" + s);
		}
	}
	
	public void createEasterUML()
	{
		try
		{
			IPath ph = project.getLocation().append("UML").append("easter.uml");
			URI uri = URI.createFileURI(ph.toOSString());
			Resource resource = new ResourceSetImpl().createResource(uri);
			Model model = UMLFactory.eINSTANCE.createModel();
			model.setName("Easter");

			resource.getContents().add(model);
			CmyModel mymodel = Cumlcreator.getInstance().createUmlElement(model);
			mymodel.addComment("Example easter ");
			Cumlview.appliedMARTE(mymodel);  
			Cumlview.appliedNFPs(mymodel);
			Cumlview.appliedTime(mymodel);
			Cumlview.importUnitKind(mymodel);
			CmyEnumeration cme = mymodel.getAllEnumeration().GetUnitEnumeration().getElement(
					"TimeUnitKind");
			CmyEnumerationLiteral cmel = cme.getEumerationliteral().getElement("tick");
			resource.save(null);
			CmyClass c =mymodel.createClockTypeClass("clockdays", cme, false, false); 
				//new Ccreateclock("clockdays", mymodel.getElement(), false, false, cme
					//.getElement());
			// ----
			CmyInstance days = mymodel.createClockInstance("days", c, cmel);
			CmyInstance sunday = mymodel.createClockInstance("sunday", c, cmel);
			CmyInstance newmoon = mymodel.createClockInstance("newmoon", c, cmel);
			CmyInstance vEquinox = mymodel.createClockInstance("vEquinox", c, cmel);
			CmyInstance fullmoon = mymodel.createClockInstance("fullmoon", c, cmel);
			CmyInstance eastermoon = mymodel.createClockInstance("eastermoon", c, cmel);
			CmyInstance easter = mymodel.createClockInstance("easter", c, cmel);
			// ----
			CmyConstraint ccs = mymodel.createConstraintRule("c1");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { days, sunday });
			ccs.setCCSLBody("{ \n" +
					" sunday isPeriodicOn days period 7 offset 1;\n"+
					" //sunday = days filteredBy 0b0(1_000_000) ; \n" +
					"}");
			// ----
			ccs = mymodel.createConstraintRule("c2");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { days, newmoon });
			ccs.setCCSLBody("{newmoon = days filteredBy 0B0^6(1.0^29);}");
			// ---
			ccs = mymodel.createConstraintRule("c3");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { days, vEquinox });
			ccs.setCCSLBody("{vEquinox = days filteredBy 0B0^20(1.0^365) ;}");
			// ---
			ccs = mymodel.createConstraintRule("c4");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { fullmoon, vEquinox, eastermoon });
			ccs.setCCSLBody("{eastermoon =vEquinox weakly sampledOn fullmoon ;}");

			ccs = mymodel.createConstraintRule("c5");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { easter, sunday, eastermoon });
			ccs.setCCSLBody("{easter =eastermoon strictly sampledOn sunday ;}");

			ccs = mymodel.createConstraintRule("c6");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { days, newmoon, fullmoon });
			ccs.setCCSLBody("{\n" + " Clock delay14 ;\n"
					+ "delay14= oneShotConstraint days, newmoon , fullmoon ,14;\n" + "}");

			resource.save(null);

		} catch (Throwable e)
		{
			MyManager.printError(e, "Fail create Easter.uml !!");
		}
	}

	public void createImageUML()
	{
		try
		{
			IPath ph = project.getLocation().append("UML").append("image.uml");
			URI uri = URI.createFileURI(ph.toOSString());
			Resource resource = new ResourceSetImpl().createResource(uri);
			Model model = UMLFactory.eINSTANCE.createModel();
			model.setName("Appli_SD");
			resource.getContents().add(model);
			CmyModel mymodel = Cumlcreator.getInstance().createUmlElement(model);
			Cumlview.appliedMARTE(mymodel);
			Cumlview.appliedNFPs(mymodel);
			Cumlview.appliedTime(mymodel);
			Cumlview.importUnitKind(mymodel);
			CmyEnumeration cme = mymodel.getAllEnumeration().GetUnitEnumeration().getElement(
					"LogicalTimeUnit");
			CmyEnumerationLiteral cmel = cme.getEumerationliteral().getElement("tick");
			// ....
			cmel.convertString();
			CmyClass cimage = mymodel.createClass("Image");
			CmyClass cline = mymodel.createClass("Line");
			CmyClass cpixel = mymodel.createClass("Pixel");
			CmyClass cword = mymodel.createClass("Word");
			CmyClass cimageappli = mymodel.createClass("ImageApplication");
			CmyClass cfilter = cimageappli.createClass("Filter");

			CmyClass cclockedsignal = new Ccreateclock("ClockedSignal", mymodel.getElement(), true,
					false, cme.getElement()).getElement();
			CmyClass cclockedpixel = new Ccreateclock("ClockedPixel", mymodel.getElement(), true,
					false, cme.getElement()).getElement();
			CmyClass cclockedword = new Ccreateclock("ClockedWord", mymodel.getElement(), true,
					false, cme.getElement()).getElement();
			cimage.getElement().createAssociation(true, AggregationKind.COMPOSITE_LITERAL,
					"end1Name", 10, 10, cline.getElement(), true, AggregationKind.COMPOSITE_LITERAL,
					"end2Name", 1, 1);

			cline.getElement().createAssociation(true, AggregationKind.COMPOSITE_LITERAL,
					"end1Name", 8, 8, cpixel.getElement(), true, AggregationKind.COMPOSITE_LITERAL,
					"end2Name", 1, 1);
			cword.getElement().createAssociation(true, AggregationKind.COMPOSITE_LITERAL,
					"end1Name", 4, 4, cpixel.getElement(), true, AggregationKind.NONE_LITERAL,
					"end2Name", 1, 1);
  
			cclockedpixel.createProperty(cpixel, "p");
			cclockedword.createProperty(cword, "w");

			CmyProperty cmp1 = cfilter.createProperty(cclockedsignal, "pReady",
					UMLPackage.Literals.PORT);
			CmyProperty cmp2 = cfilter.createProperty(cclockedsignal, "pEoL",
					UMLPackage.Literals.PORT);
			CmyProperty cmp3 = cfilter.createProperty(cclockedpixel, "pOutPixel",
					UMLPackage.Literals.PORT);
			CmyProperty cmp4 = cfilter.createProperty(cclockedword, "pInWord",
					UMLPackage.Literals.PORT);
			cmp1.applyClock();
			cmp2.applyClock();
			cmp3.applyClock();
			cmp4.applyClock();
			cmp1.setUnit(cmel);
			cmp2.setUnit(cmel);
			cmp3.setUnit(cmel);
			cmp4.setUnit(cmel);
			resource.save(null);
			//
			CmyConstraint ccs = mymodel.createConstraintRule("c1");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { cmp1, cmp4 });
			ccs.setCCSLBody(" { pReady strictly  alternatesWith pInWord ; }");
			//
			ccs = mymodel.createConstraintRule("c2");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { cmp3, cmp2 });
			ccs.setCCSLBody(" { pEoL = pOutPixel filteredBy 0b(0000_0001) ;  }");
			//
			ccs = mymodel.createConstraintRule("c3");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { cmp3, cmp4 });
			ccs.setCCSLBody(" { pInWord by 1  strictly  precedes pOutPixel by 4  ;  }");
			//
			ccs = mymodel.createConstraintRule("c4");
			ccs.makeParsable();
			ccs.setConstraintto(new CinterfaceUML[] { cmp3, cmp4 });
			ccs.setCCSLBody(" { pInWord by 2  rightNonStrictly  synchronizesWith pOutPixel by 8  ;  }");
			resource.save(null);
		} catch (Throwable e)
		{
			MyManager.printError(e, "Fail create Image.uml !!");
		}
	}
}
