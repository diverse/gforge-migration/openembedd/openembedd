package fr.inria.ctrte.demo;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

public class DemoWizard extends BasicNewProjectResourceWizard  implements INewWizard {

	public DemoWizard()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean performFinish()
	{
		
		return true;
	}

	public void init( IWorkbench workbench , IStructuredSelection selection )
	{
		

	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard#getNewProject()
	 */
	@Override
	public IProject getNewProject()
	{
		return super.getNewProject();
	}
	
	

}






/**
* An <code>org.eclipse.ui.newWizards</code> extension which creates a new
* project in the workspace. The implementation inherits most of its functionality
* from <code>BasicNewProjectResourceWizard</code> class.
*
* @author not attributable
* @version $Revision: 1.1 $ $Date: 2008-12-10 16:20:24 $
*/
/*
public class NewWSMOProjectWizard extends BasicNewProjectResourceWizard implements INewWizard {

       protected WizardNewProjectCreationPage mainPage;
       protected IProject newProject;

   private class NSWizardPage extends WizardPage {

       private Text nsField;

       public NSWizardPage() {
           super("");
       }

       public void createControl(Composite parent) {
           Group comp = new Group(parent, SWT.NONE);
           comp.setLayout(new GridLayout(1, false));
           new Label(comp, SWT.NONE).setText("Namespace IRI:");

           nsField = new Text(comp, SWT.BORDER);
           nsField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
           nsField.addModifyListener(new ModifyListener() {
               public void modifyText(ModifyEvent e) {
                   if (nsField.getText().length()  > 0) {
                       try {
                           WSMORuntime.getRuntime().getWsmoFactory().createIRI(nsField.getText());
                           setErrorMessage(null);
                       }
                       catch(Throwable ex) {
                           setErrorMessage(ex.getMessage());
                       }
                   }
                   else {
                       setErrorMessage(null);
                   }
               }
           });

           setControl(comp);
           setTitle("New WSMO Project");
           setDescription("Supply a default namespace for the project (optional)");
       }

       public String getNSText() {
           return nsField.getText();
       }
   }
   private NSWizardPage nsPage;

       public void addPages() {
               mainPage = new WizardNewProjectCreationPage("WSMO Project page");
               mainPage.setDescription("Create a new WSMO project");
       mainPage.setTitle("New WSMO project");
       addPage(mainPage);

       nsPage = new NSWizardPage();
       addPage(nsPage);
       }

       public void dispose() {
               mainPage.dispose();
       nsPage.dispose();
               super.dispose();
       }

       public boolean performFinish() {
               if (nsPage.getErrorMessage() != null) {
           if (false == MessageDialog.openConfirm(getShell(),
                   "Invalid Namespace",
                   "The supplied project default namespace is invalid!"
                   +"\nContinue anyway ?")) {
               return false;
           }
       }
       newProject = createNewWSMOProject();
               if (newProject == null)
                       return false;

               updatePerspective();
               selectAndReveal(newProject);

       try {
           String nsText = nsPage.getNSText();
           WSMORuntime.getRuntime().getWsmoFactory().createIRI(nsText);
           newProject.setPersistentProperty(
                   ProjectNamespaceAction.PROJECT_NAMESPACE_NAME,
                   nsText);
       }
       catch(Throwable ex) {} // do nothing
               return true;
       }

       protected IProject createNewWSMOProject() {
               if (newProject != null)
                       return newProject;

               IWorkspace workspace = ResourcesPlugin.getWorkspace();
               final IProject newProjectHandle = mainPage.getProjectHandle();
               final IProjectDescription description = workspace
                               .newProjectDescription(newProjectHandle.getName());
               if (!mainPage.useDefaults())
                       description.setLocation(mainPage.getLocationPath());

               WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
                       protected void execute(IProgressMonitor monitor)
                                       throws CoreException {
                               createWSMOProject(description, newProjectHandle, monitor);
                       }
               };

               try {
                       getContainer().run(true, true, operation);
               }
               catch (InterruptedException e) {
                       return null;
               }
               catch (InvocationTargetException e) {
                       MessageDialog.openError(getShell(), "Cannot create a new WSMO project",
                                       e.getTargetException().getMessage());
                       return null;
               }

               return newProjectHandle;
       }

       protected void createWSMOProject(IProjectDescription description, IProject projectHandle,
                       IProgressMonitor monitor) throws CoreException, OperationCanceledException {
               try {
                       monitor.beginTask("", 2000);
                       projectHandle.create(description, new SubProgressMonitor(monitor, 1000));
                       if (monitor.isCanceled())
                               throw new OperationCanceledException();
                       projectHandle.open(new SubProgressMonitor(monitor, 1000));
               }
               finally {
                       monitor.done();
               }
       }
}*/


