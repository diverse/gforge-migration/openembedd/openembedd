package fr.inria.ctrte.plugalloc.test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;
import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;

public class AllTests {

	public static Test suite()
	{
		TestSuite suite = new TestSuite("Test for fr.inria.ctrte.plugalloc");

		/*JUnit4TestAdapter ju=null;
		ju=new JUnit4TestAdapter(FilterByDialog.class);
		suite.addTest(ju);		
		ju=new JUnit4TestAdapter(SustainDialog.class);
		suite.addTest(ju);	
		ju=new JUnit4TestAdapter(AlternatesDialog.class);
		suite.addTest(ju);		*/
		for (JUnit4TestAdapter ju : liste())
		{
			suite.addTest(ju);

		}
		
	
		

		return suite;
	}

	/*
	 * return a array of  JUnit4TestAdapter
	 * contruit par
	 *  new JUnit4TestAdapter(xxx.class);  //ligne 92
	 *  ou xxx est une classe de 
	 *  fr.inria.ctrte.plugalloc.popup.composite.property.compose 
	 *  du plug in 
	 *  fr.inria.ctrte.plugalloc
	 * 
	 * 	qui est file de ( BaseConstraintApi )
	 */

	public static JUnit4TestAdapter[] liste()
	{
		/**
		 *
		 */
		ArrayList<JUnit4TestAdapter> listjunit = new ArrayList<JUnit4TestAdapter>();
		Class<? extends BaseConstraintApi> c = BaseConstraintApi.class;
		URL url = c.getProtectionDomain().getCodeSource().getLocation();

		try
		{

			Package p = c.getPackage();
			String s = p.getName();
			String filename = url.getFile();

			int n = 0;
			while ((n = s.indexOf(".")) != -1)
			{
				String tmp = s.substring(0, n);
				s = s.substring(n + 1);

				filename += "/" + tmp;
			}

			filename += "/" + s;
			File f = new File(filename);
			//	System.out.println(filename+" "+f.exists());			
			for (String list : f.list())
			{

				if (list.endsWith(".class") && list.indexOf("$") == -1)
				{
					list = list.substring(0, list.length() - 6);
					//System.out.println(list);
					if (!list.equals("BaseConstraintApi"))
					{
						try
						{
							Class<? extends Object> d = c.getClassLoader().loadClass(
									p.getName() + "." + list);

							if (d != null)
							{
								JUnit4TestAdapter ju = new JUnit4TestAdapter(d);
								listjunit.add(ju);
							}
						} catch (Exception ex)
						{
							System.out.println(ex);
						}
					}
				}
			}
		} catch (Exception ex)
		{
			System.out.println(ex);
		}
		return listjunit.toArray(new JUnit4TestAdapter[] {});

	}

}
/*
 * 
 testClass(FilterByDialog.class);
 testClass(SustainDialog.class);
 testClass(AlternatesDialog.class);
 */

