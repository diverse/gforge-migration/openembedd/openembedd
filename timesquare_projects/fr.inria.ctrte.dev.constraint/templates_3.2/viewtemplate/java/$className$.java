package $packageName$;


//import fr.inria.ctrte.baseuml.icore.IComboClock;
//import fr.inria.ctrte.baseuml.icore.IInputBword;

import fr.inria.ctrte.plugalloc.popup.composite.property.compose.BaseConstraintApi;

public class $className$ extends BaseConstraintApi { 
		
		public $className$()
		{
			super("$viewName$");
			// setLocalMaxClockMumber( int  );  1
			setComment("$message$");
		}

		//IComboClock a,b;
		//IInputBword ba, bb;
		
		@Override
		protected void mydraw() {		
		/*	a=createclock("a", "a");
			ba=createInputBword("mask a");
			b=createclock("b", "b");
			bb=createInputBword("mask b");
			createModifierSingleton(false);*/			
		}

		@Override
		public String getContraintText() 
		{	
			String s="";
			/* s="Clock " + localclock(0)+" is "+ a.getValueString() + " filteredBy " + ba.getValue() +" ;\n" + 
			localclock(0) +"  = " +b.getValueString() + " filteredBy " + bb.getValue() + ";" ;
			if (getModifierselected()==Modifier.strictly )
			{
				s+= " ("+a.getValueString() +" minus "+	localclock(0)+" ) # ("+b.getValueString()+" minus "+	localclock(0)+" ) ;"	 ;
			}*/
			return "{ "+ s + "  } ";
		}

		@Override
		public TypeConstrain getType() {
			
			return  $type$;
		}
		
		@Override
		protected int validate() {	
			return 0;
		}
		
		
		
	}
