package fr.inria.ctrte.dev.constraint;


import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.pde.core.plugin.IPluginBase;
import org.eclipse.pde.core.plugin.IPluginElement;
import org.eclipse.pde.core.plugin.IPluginExtension;
import org.eclipse.pde.core.plugin.IPluginModelFactory;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.OptionTemplateSection;
import org.eclipse.pde.ui.templates.PluginReference;
import org.eclipse.pde.ui.templates.TemplateOption;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.time.parser.ActivatorTimeParser;

public class SimpleViewTemplateSection extends OptionTemplateSection {

	private static final String KEY_CLASS_NAME = "className";
	private static final String KEY_VIEW_NAME = "viewName";
	private static final String KEY_MESSAGE_NAME = "message";
	//private static final String KEY_IMP_MESSAGE_NAME = "importantMessage";
	private static final String KEY_TYPE= "type";
	private String packageName = null;
	public SimpleViewTemplateSection() {
		super();
		
		setPageCount(1);
		createOptions();
	}
	
	private void createOptions() {
		//addOption(KEY_PACKAGE_NAME, "Package Name ", getFormattedPackageName(id), 0);
		addOption(KEY_CLASS_NAME, "Class Name ", "MyClassBox", 0);
		addOption(KEY_VIEW_NAME, "Name", "MyLabel", 0);
		addOption(KEY_MESSAGE_NAME, "Message", "information about ....", 0);
		//addOption(KEY_IMP_MESSAGE_NAME, "Important?", false, 0);
		addComboChoiceOption(KEY_TYPE, "type of constraint", new String[][]
		                                                                  {
				{ "TypeConstrain.Synchrone" , "Synchrone"}, {"TypeConstrain.Asynchrone" ,"Asynchrone"} ,
				{ "TypeConstrain.Mixed","Mixed" } , { "TypeConstrain.NFPs","NFPs" } 
				,{"TypeConstrain.Miscellaneous","Miscellaneous" }
		         }		                                                                 
		      , "", 0);
	

	}
	
	public void addPages(Wizard wizard) {
		//System.out.println(	" ... " +IHelpContextIds.TEMPLATE_NEW_WIZARD);
		//  IHelpContextIds.TEMPLATE_NEW_WIZARD ="org.eclipse.pde.doc.user.template_new_wizard"
		WizardPage page = createPage(0, "org.eclipse.pde.doc.user.template_new_wizard");
		page.setTitle("Simple View Template");
		page.setDescription("Creates a simple view");
		wizard.addPage(page);
		markPagesAdded();
	}
	
	/**
	 * This is the folder relative to your install url and template directory
	 * where the templates are stored.
	 */
	public String getSectionId() {
		return "viewtemplate";
	}
	
	protected void initializeFields(IFieldData data) {
		if (data!=null)
		{
		String id = data.getId();	
		
		initializeOption(KEY_PACKAGE_NAME, getFormattedPackageName(id));
		this.packageName = getFormattedPackageName(id);
		}
	}

	/**
	 * Validate your options here!
	 */
	public void validateOptions(TemplateOption changed) {
		super.validateOptions(changed); 
	//	System.out.println(	changed );
	}


	protected void updateModel(IProgressMonitor monitor) throws CoreException {
		try
		{
		IPluginBase plugin = model.getPluginBase();
		IPluginModelFactory factory = model.getPluginFactory();
        		
		// org.eclipse.core.runtime.applications
		 
		IPluginExtension extension = createExtension("fr.inria.ctrte.plugalloc.ConstraintWizard", true);

		IPluginElement element = factory.createElement(extension);
		element.setName("ConstraintWizard");	
		element.setAttribute("name", getStringOption(KEY_VIEW_NAME));
		String  pack= getStringOption(KEY_PACKAGE_NAME);
		if (pack==null)
			pack=plugin.getId();
		String fullClassName = 	pack+"."+getStringOption(KEY_CLASS_NAME);		
		element.setAttribute("class", fullClassName);
		extension.add(element);
		boolean b=false;
		for (IPluginExtension ipe:	plugin.getExtensions())
		{
			if (ipe.getPoint()!=null)
			if (ipe.getPoint().equals("fr.inria.ctrte.plugalloc.ConstraintWizard"))
				b=true;
		}
		if (b==false)
			plugin.add(extension);
		copyfile(  );
	

	
	
		IJavaProject javaProject = JavaCore.create(project);
		
		IClasspathEntry icpetb[]=  javaProject.getRawClasspath();
		System.out.println("---");
		b=false;
		IClasspathEntry newicpetb[] = new IClasspathEntry[icpetb.length+1];
		int n=0;
		for (IClasspathEntry icpe:icpetb)
		{
			//System.out.println(icpe);
			//System.out.println( "\t\t"+icpe.getPath() +" " +icpe.getContentKind()+ " " + icpe.getEntryKind());
			if (icpe.getEntryKind()== org.eclipse.jdt.core.IClasspathEntry.CPE_LIBRARY)
			{
				if  (icpe.getPath().removeFileExtension().lastSegment().equals("antlr-2.7.7"))
				{
					b=true;
				}
			}
			newicpetb[n]=icpetb[n];
			n++;
		}
		if (b==false)
		{
		IClasspathEntry newcpe=JavaCore.newLibraryEntry(project.getFullPath().append("antlr-2.7.7.jar"),null,null);
		newicpetb[n] =newcpe;	
		javaProject.setRawClasspath(newicpetb, null);		
		}
		MyPluginManager.refreshWorkspace();
		//MyManager.refreshWorkspace(); // TODO Test
		}
		catch (Throwable ex) {
			MyManager.printError(ex);
		}
		
	}

	
	
	
	public void copyfile(  )
	{
		try
		{
			URL url = MyPluginManager.getInstallUrl(ActivatorTimeParser.getDefault().getBundle(), "/antlr-2.7.7.jar");
			IPath ph = project.getLocation().append("").append("antlr-2.7.7.jar");			
			MyPluginManager.copy(MyPluginManager.getIFileStore(url), MyPluginManager.getIFileStore(ph),
					EFS.OVERWRITE, null);
		} catch (Throwable e)
		{
			MyManager.printError(e, "echec file : antlr-2.7.7.jar");
		}
	}
	
	public String getUsedExtensionPoint() {
		return "fr.inria.ctrte.plugalloc.ConstraintWizard";
	}

	/**
	 * The location of your plugin supplying the template content
	 */
	protected URL getInstallURL()  {
		try
		{
		URL url= Activator.getDefault().getBundle().getEntry("/");
		url = FileLocator.resolve(FileLocator.toFileURL(url));
		String ls=url.toString();
		String nls=ls.replaceAll(" ", "%20");
		if (nls.compareTo(ls)!=0)
		{
			url = new URL(nls);
		}
		return url;
		}
		catch (Exception ex) {
			
		}
		return null;
	}

	protected ResourceBundle getPluginResourceBundle() {
		return Platform.getResourceBundle(Activator.getDefault().getBundle());
	}

	/**
	 * You can use this method to add files relative to your section id
	 */
	public String[] getNewFiles() {
		return new String[0];
	}
	
	public IPluginReference[] getDependencies(String schemaVersion) {
		try
		{
		ArrayList<PluginReference> result = new ArrayList<PluginReference>();

		result.add(new PluginReference("org.eclipse.ui.views", null, 0)); //$NON-NLS-1$
        result.add(new PluginReference("org.eclipse.core.runtime", null, 0)); //$NON-NLS-1$
        result.add(new PluginReference("org.eclipse.ui", null, 0)); //$NON-NLS-1$
        result.add(new PluginReference("fr.inria.ctrte.time.parser", null, 0)); 
        result.add(new PluginReference("fr.inria.ctrte.baseuml", null, 0)); 
        result.add(new PluginReference("fr.inria.ctrte.plugalloc", null, 0)); 
        result.add(new PluginReference("org.junit4", null, 0)); 
		return (IPluginReference[]) result.toArray(
				new IPluginReference[result.size()]);
		}
		catch (Throwable ex) {
			MyManager.println("Import Plugin Fail");
		}
		return 	new IPluginReference[]{};
	}
	
	public boolean isDependentOnParentWizard() {
		return true;
	}
	
	public int getNumberOfWorkUnits() {
		return super.getNumberOfWorkUnits() + 1;
	}
	
	public String getStringOption(String name) {
		if (name.equals(KEY_PACKAGE_NAME)) {
			return packageName;
		}
		return super.getStringOption(name);
	}
	
	protected String getFormattedPackageName(String id){
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < id.length(); i++) {
			char ch = id.charAt(i);
			if (buffer.length() == 0) {
				if (Character.isJavaIdentifierStart(ch))
					buffer.append(Character.toLowerCase(ch));
			} else {
				if (Character.isJavaIdentifierPart(ch) || ch == '.')
					buffer.append(ch);
			}
		}
		return buffer.toString().toLowerCase(Locale.ENGLISH);
	}
	

	public String getDescription() {
		return "Creates a simple view";
	}

	public String getLabel() {
		return "Simple View";
	}

}
