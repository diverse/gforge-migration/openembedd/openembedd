package fr.inria.ctrte.extendeditor.decode;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.uml2.uml.Element;

import com.cea.papyrus.core.editpart.IUMLElementEditPart;

import fr.inria.base.MyManager;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

public class PapyrusUmlDecode extends BaseDecode {

	public PapyrusUmlDecode() {
		super("Papyrus");

	}

	@Override
	public String getName() {

		return super.getName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Element getUmlElement() {
		try
		{
		ISelection is=MyPluginManager.getSelection();	
		if (is instanceof StructuredSelection) {
			StructuredSelection ss = (StructuredSelection) is;			
			Object o=ss.getFirstElement();
			if ( o instanceof IUMLElementEditPart)
			{
				IUMLElementEditPart ped= 	(( IUMLElementEditPart ) o);			
			return ped.getUmlElement();
			}
			
		}
		}
		catch (Exception e) {
			MyManager.printError(e);
			
		}
		catch (Error e) {
			MyManager.printError(e);
			
		}
		return super.getUmlElement();
	}

	@Override
	public int notifymodification() {

		return super.notifymodification();
	}

	@Override
	public String toString() {

		return super.toString();
	}



}
