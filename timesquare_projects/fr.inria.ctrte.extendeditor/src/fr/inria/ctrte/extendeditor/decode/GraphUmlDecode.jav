package fr.inria.ctrte.extendeditor.decode;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.transaction.impl.TransactionChangeRecorder;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.uml2.uml.Element;

import code.Cumlview;
import fr.inria.base.MyPluginManager;
import fr.inria.ctrte.plugalloc.decode.BaseDecode;

public class GraphUmlDecode extends BaseDecode {

	public GraphUmlDecode() {
		super("Graphic UML editor");

	}

	@Override
	public Element getUmlElement() {
		ISelection selection = MyPluginManager.getSelection();
		if (selection instanceof StructuredSelection) {
			
			Object o = ((StructuredSelection) selection).getFirstElement();
			
			if (o instanceof org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart) {
				org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart x = ((org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart) o);
			
				//x.getEditingDomain().getResourceSet().eSetDeliver(false);				
				//((org.eclipse.ui.part.FileEditorInput) MyManager.getActiveEditor().getEditorInput())
				
			
				Object o2 = x.resolveSemanticElement();
				x.getNotationView().eSetDeliver(false);
				x.deactivate();
				x.enableEditMode();
				if (o2 instanceof Element) {
					uml = (Element) o2;	
					//uml.eSetDeliver(false);
					for ( Adapter eo:  uml.eAdapters())
					{
						if (eo instanceof TransactionChangeRecorder )
						{
							((TransactionChangeRecorder) eo).endRecording();
						}
					}						
					return uml;
				}
				

						
			}
		}
		uml = null;
		return uml ;//null;
	}

	private Element uml;

	@Override
	public int notifymodification() {

	
		ISelection selection = MyPluginManager.getSelection();
		if (selection instanceof StructuredSelection) {
			
			Object o = ((StructuredSelection) selection).getFirstElement();
			
			if (o instanceof org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart) {
				org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart x = ((org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart) o);
			
				x.getEditingDomain().getResourceSet().eSetDeliver(true);
				Object o2 = x.resolveSemanticElement();
				if (o2 instanceof Element) {
					uml = (Element) o2;	
					uml.eSetDeliver(true);	
					uml.notifyAll();
				}
			}
		}
						
		MyPluginManager.refreshWorkspace();
			
		
		Cumlview.touchModel(uml);
		return 0;
	}

}
