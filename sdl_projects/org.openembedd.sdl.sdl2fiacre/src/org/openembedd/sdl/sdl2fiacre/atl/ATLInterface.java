/******************************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Christophe Le Camus (CS), Marion Feau (CS), Guillaume Jolly (CS), Sébastien Gabel (CS) Petre Bazavan
 * (AEIC), Vincent Combet (CS) - initial API and implementation
 *******************************************************************************************/
package org.openembedd.sdl.sdl2fiacre.atl;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.openembedd.sdl.sdl2fiacre.Constants;

/**
 * Class that calls the transformation onto the ATL Interface on the selected file
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
public class ATLInterface
{
	/**
	 * Instance of the current class
	 */
	private static ATLInterface	instance	= null;
	/**
	 * Name of the ATL compiled transformation
	 */
	private final static String	atlTransfo	= "SDL2FIACRE.asm";
	/**
	 * The model handler used for the transformation
	 */
	private AtlEMFModelHandler	modelHandler;
	/**
	 * The URL to the transformation resource file
	 */
	private URL					transfoResource;
	/**
	 * The UML metamodel
	 */
	private ASMEMFModel			umlMM;
	/**
	 * The Fiacre metamodel
	 */
	private ASMEMFModel			fcrMM;

	/**
	 * This method returns the instance of the ATL interface (there is only one instance of this object)
	 * 
	 * @return the instance of the ATLInterface
	 */
	public static ATLInterface getInstance()
	{
		if (instance == null)
			instance = new ATLInterface();
		return instance;
	}

	/**
	 * Initialize the metamodel and transformation resources and update the parameter
	 * 
	 * @param models
	 *        the map that will be updated with resource elements
	 */
	private void init(Map<String, Object> models)
	{
		modelHandler = (AtlEMFModelHandler) AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);
		transfoResource = ATLInterface.class.getResource(atlTransfo);

		umlMM = (ASMEMFModel) modelHandler
				.loadModel(Constants.UML_MM_NAME, modelHandler.getMof(), Constants.UML_MM_URI);
		fcrMM = (ASMEMFModel) modelHandler.loadModel(Constants.FIACRE_MM_NAME, modelHandler.getMof(),
			Constants.FIACRE_MM_URI);

		models.put(Constants.UML_MM_NAME, umlMM);
		models.put(Constants.FIACRE_MM_NAME, fcrMM);
	}

	/**
	 * Call the SDL->Fiacre transformation onto the <code>inFilePath</code> and store the result in the
	 * <code>outFilePath</code> file.
	 * 
	 * @param inFilePath
	 *        The path to the input UML2 file (profiled with SDL)
	 * @param outFilePath
	 *        The path to the output Fiacre file
	 */
	public void transform(String inFilePath, String outFilePath)
	{
		try
		{
			Map<String, Object> models = new HashMap<String, Object>();
			init(models);

			// get/create models
			ASMEMFModel inModel = (ASMEMFModel) modelHandler.loadModel("IN", umlMM, URI.createFileURI(inFilePath));
			ASMEMFModel outModel = (ASMEMFModel) modelHandler.newModel("OUT", URI.createFileURI(outFilePath)
					.toFileString(), fcrMM);
			// load models
			models.put("IN", inModel);
			models.put("OUT", outModel);
			// add the continue after errors options
			Map<String, String> options = new HashMap<String, String>();
			options.put("continueAfterError", "true");
			// launch
			AtlLauncher.getDefault().launch(transfoResource, Collections.EMPTY_MAP, models, Collections.EMPTY_MAP,
				Collections.EMPTY_LIST, options);

			modelHandler.saveModel(outModel, outFilePath, false);
			dispose(models);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Dispose all objects loaded in the parameter
	 * 
	 * @param models
	 *        the map that contains resource elements
	 */
	private void dispose(Map<String, Object> models)
	{
		for (Object model : models.values())
			((ASMEMFModel) model).dispose();
	}
}
