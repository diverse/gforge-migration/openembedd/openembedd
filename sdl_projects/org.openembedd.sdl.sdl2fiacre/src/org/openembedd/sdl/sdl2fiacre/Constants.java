/******************************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christophe Le Camus (CS), Marion Feau (CS), Guillaume Jolly (CS), Sébastien Gabel (CS) 
 *    Petre Bazavan (AEIC), Vincent Combet (CS) - initial API and implementation
 *******************************************************************************************/
package org.openembedd.sdl.sdl2fiacre;

/**
 * Constants used by other classes of this plug-in
 * @author Christian Brunette - OpenEmbeDD integration team
 */
public interface Constants
{
	/**
	 * Uri of the UML2 metamodel
	 */
	public final static String	UML_MM_URI		= "uri:http://www.eclipse.org/uml2/2.1.0/UML";

	/**
	 * Uri of the Fiacre metamodel
	 */
	public final static String	FIACRE_MM_URI	= "uri:http://org.topcased.fiacre";

	/**
	 * Name of the UML2 metamodel in the ATL transformation file
	 */
	public final static String	UML_MM_NAME		= "SDL";

	/**
	 * Name of the Fiacre metamodel in the ATL transformation file
	 */
	public final static String	FIACRE_MM_NAME	= "FIACRE";

	/**
	 * Extension of UML2 file
	 */
	public final static String	UML_EXT			= "uml";

	/**
	 * Extension of Fiacre file
	 */
	public final static String	FIACRE_EXT		= "fiacre";
}
