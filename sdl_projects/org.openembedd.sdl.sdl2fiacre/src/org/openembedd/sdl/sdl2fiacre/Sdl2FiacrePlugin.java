/******************************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christophe Le Camus (CS), Marion Feau (CS), Guillaume Jolly (CS), Sébastien Gabel (CS) 
 *    Petre Bazavan (AEIC), Vincent Combet (CS) - initial API and implementation
 *******************************************************************************************/
package org.openembedd.sdl.sdl2fiacre;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
public class Sdl2FiacrePlugin extends AbstractUIPlugin
{

	// The plug-in ID
	public static final String		PLUGIN_ID	= "org.openembedd.sdl.sdl2fiacre";

	// The shared instance
	private static Sdl2FiacrePlugin	plugin;

	/**
	 * The constructor
	 */
	public Sdl2FiacrePlugin()
	{}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Sdl2FiacrePlugin getDefault()
	{
		return plugin;
	}

}
