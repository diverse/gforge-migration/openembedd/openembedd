/******************************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Christophe Le Camus (CS), Marion Feau (CS), Guillaume Jolly (CS), Sébastien Gabel (CS) Petre Bazavan
 * (AEIC), Vincent Combet (CS) - initial API and implementation
 *******************************************************************************************/
package org.openembedd.sdl.sdl2fiacre.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.openembedd.sdl.sdl2fiacre.Constants;
import org.openembedd.sdl.sdl2fiacre.atl.ATLInterface;

/**
 * Action that call the SDL->Fiacre transformation on the selected file (if it is a uml model).
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 */
public class Sdl2FiacreAction implements IObjectActionDelegate
{
	/**
	 * Selected UML file on which the transformation is called
	 */
	private IFile	srcFile;
	/**
	 * Shell from which this action is called
	 */
	private Shell	shell;

	/**
	 * Constructor
	 */
	public Sdl2FiacreAction()
	{
		this.srcFile = null;
	}

	/**
	 * Get the shell that activate this action
	 * 
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
		shell = targetPart.getSite().getShell();
	}

	/**
	 * Execute the UML2 to SynDEx transformation on the selected file
	 * 
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action)
	{
		if (srcFile != null)
		{
			IPath srcPath = srcFile.getLocation();
			IPath tgtPath = srcPath.removeFileExtension().addFileExtension(Constants.FIACRE_EXT);

			ATLInterface.getInstance().transform(srcPath.toOSString(), tgtPath.toOSString());
			try
			{
				srcFile.getParent().refreshLocal(2, null);
			}
			catch (CoreException e)
			{
				e.printStackTrace();
			}
			MessageDialog.openInformation(shell, "SDL2Fiacre Plug-in", "The transformation SDL->Fiacre was executed.");
		}
		else
		{
			System.out.println("No file was selected!");
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		if (selection instanceof StructuredSelection)
		{
			// the selection should be a single *.uml file
			if (((StructuredSelection) selection).size() == 1)
			{
				IFile tmp = (IFile) ((StructuredSelection) selection).getFirstElement();
				if (Constants.UML_EXT.equals(tmp.getFileExtension()))
					srcFile = tmp;
				else
					srcFile = null;
			}
			else
				srcFile = null;
		}
	}

}
