<?xml version="1.0" encoding="UTF-8"?>
<feature
      id="org.topcased.aadl.behavior"
      label="AADL behavior annex"
      version="0.5.2"
      provider-name="Topcased">

   <description>
      This feature implements the behavioral annex for AADL. The annex
conforms to the documentation version 1.5 provided by the gforge
website. New features (hierarchical states) are not documented
yet but described by examples.
It contains the following elements:
- the metamodel of the annex
- an extension to the AADL textual editor that recognizes annex
keywords
- a parser which analyses &quot;behavior_specification&quot; annexes and
makes some static analysis
(declaration of variables, references to AADL identifiers, type
checking)
- the &quot;behavior&quot; package containing predefined types
- the &quot;behavior_properties&quot; AADL property set containing predefined
properties.
CHANGES
Version 0.5.0
- compatible with eclipse 3.3 - topcased 1.0.0
- requires OSATE &gt;= 1.5.0
Version 0.4.0
- hierarchical states are now supported
- the behavior can depend on AADL modes: modes can be split into
annex states
Version 0.3.0
- abstract data types are now supported. They must be identifed
by the Abstract property.
Data subprograms with one &apos;out&apos; parameter with the enclosing
classifier as type are constructors. For each constructor c of
the data type dt, legal expressions are:
- dt.c(...) constructs an element of the type
- dt.c?(x)  checks if x has been constructed using c
- dt.c&apos;a(x) returns the value given to argument when x has been
build using constructor c
- if and loop constructs have been added to the action language
- the syntax for accessing &apos;count&apos; and &apos;fresh&apos; port information
has been changed to
&lt;port identifier&gt;&apos;count
&lt;port identifier&gt;&apos;fresh
- the metamodel has been updated to support the new features
   </description>

   <copyright>
      Copyright (c) 2005, 2009 Topcased contributors. All rights reserved.
   </copyright>

   <license url="license_agreement.html">
      TOPCASED Software User Agreement
September 23, 2005
Usage Of Content
THE TOPCASED PROJECT MAKES AVAILABLE SOFTWARE, DOCUMENTATION,
INFORMATION AND/OR OTHER MATERIALS FOR OPEN SOURCE PROJECTS (COLLECTIVELY
&quot;CONTENT&quot;). USE OF THE CONTENT IS GOVERNED BY THE TERMS AND CONDITIONS
OF THIS AGREEMENT AND/OR THE TERMS AND CONDITIONS OF LICENSE
AGREEMENTS OR NOTICES INDICATED OR REFERENCED BELOW. BY USING
THE CONTENT, YOU AGREE THAT YOUR USE OF THE CONTENT IS GOVERNED
BY THIS AGREEMENT AND/OR THE TERMS AND CONDITIONS OF ANY APPLICABLE
LICENSE AGREEMENTS OR NOTICES INDICATED OR REFERENCED BELOW.
IF YOU DO NOT AGREE TO THE TERMS AND CONDITIONS OF THIS AGREEMENT
AND THE TERMS AND CONDITIONS OF ANY APPLICABLE LICENSE AGREEMENTS
OR NOTICES INDICATED OR REFERENCED BELOW, THEN YOU MAY NOT USE
THE CONTENT.
Applicable Licenses
Unless otherwise indicated, all Content made available by the
TOPCASED project is provided to you under the terms and conditions
of the Eclipse Public License Version 1.0 (&quot;EPL&quot;). A copy of
the EPL is provided with this Content and is also available at
http://www.eclipse.org/legal/epl-v10.html. For purposes of the
EPL, &quot;Program&quot; will mean the Content.
Content includes, but is not limited to, source code, object
code, documentation and other files maintained in the TOPCASED
CVS repository (&quot;Repository&quot;) in CVS modules (&quot;Modules&quot;) and
made available as downloadable archives (&quot;Downloads&quot;).
* Content may be structured and packaged into modules to facilitate
delivering, extending, and upgrading the Content. Typical modules
may include plug-ins (&quot;Plug-ins&quot;), plug-in fragments (&quot;Fragments&quot;),
and features (&quot;Features&quot;).
* Each Plug-in or Fragment may be packaged as a sub-directory
or
JAR (Java™ ARchive) in a directory named &quot;plugins&quot;.
* A Feature is a bundle of one or more Plug-ins and/or Fragments
and associated material. Each Feature may be packaged as a sub-directory
in a directory named &quot;features&quot;. Within a Feature, files named
&quot;feature.xml&quot; may contain a list of the names and version numbers
of the Plug-ins and/or Fragments associated with that Feature.
* Features may also include other Features (&quot;Included Features&quot;).
Within a Feature, files named &quot;feature.xml&quot; may contain a list
of the names and version numbers of Included Features.
The terms and conditions governing Plug-ins and Fragments should
be contained in files named &quot;about.html&quot; (&quot;Abouts&quot;). The terms
and conditions governing Features and Included Features should
be contained in files named &quot;license.html&quot; (&quot;Feature Licenses&quot;).
Abouts and Feature Licenses may be located in any directory of
a Download or Module including, but not limited to the following
locations:
* The top-level (root) directory
* Plug-in and Fragment directories
* Inside Plug-ins and Fragments packaged as JARs
* Sub-directories of the directory named &quot;src&quot; of certain Plug-ins
* Feature directories
Note: if a Feature made available by the TOPCASED project is
installed using the Eclipse Update Manager, you must agree to
a license (&quot;Feature Update License&quot;) during the installation
process. If the Feature contains Included Features, the Feature
Update License should either provide you with the terms and conditions
governing the Included Features or inform you where you can locate
them. Feature Update Licenses may be found in the &quot;license&quot; property
of files named &quot;feature.properties&quot; found within a Feature. Such
Abouts, Feature Licenses, and Feature Update Licenses contain
the terms and conditions (or references to such terms and conditions)
that govern your use of the associated Content in that directory.
THE ABOUTS, FEATURE LICENSES, AND FEATURE UPDATE LICENSES MAY
REFER TO THE EPL OR OTHER LICENSE AGREEMENTS, NOTICES OR TERMS
AND CONDITIONS. SOME OF THESE OTHER LICENSE AGREEMENTS MAY INCLUDE
(BUT ARE NOT LIMITED TO):
* Common Public License Version 1.0 (available at http://www.eclipse.org/legal/cpl-v10.html)
* Apache Software License 1.1 (available at http://www.apache.org/licenses/LICENSE)
* Apache Software License 2.0 (available at http://www.apache.org/licenses/LICENSE-2.0)
IT IS YOUR OBLIGATION TO READ AND ACCEPT ALL SUCH TERMS AND CONDITIONS
PRIOR TO USE OF THE CONTENT. If no About, Feature License, or
Feature Update License is provided, please contact the TOPCASED
project to determine what terms and conditions govern that particular
Content.
Cryptography
Content may contain encryption software. The country in which
you are currently may have restrictions on the import, possession,
and use, and/or re-export to another country, of encryption software.
BEFORE using any encryption software, please check the country&apos;s
laws, regulations and policies concerning the import, possession,
or use, and re-export of encryption software, to see if this
is permitted.
   </license>

   <url>
      <update label="Topcased" url="http://topcased-mm.gforge.enseeiht.fr/release/update-site/"/>
   </url>

   <requires>
      <import plugin="org.eclipse.core.runtime"/>
      <import plugin="org.eclipse.emf.ecore.edit"/>
      <import plugin="edu.cmu.sei.aadl.model" version="1.4.0" match="greaterOrEqual"/>
      <import plugin="org.eclipse.emf.edit.ui"/>
      <import plugin="org.eclipse.ui.ide"/>
      <import plugin="edu.cmu.sei.aadl.model.edit"/>
      <import plugin="org.eclipse.emf.edit"/>
      <import plugin="org.junit"/>
      <import plugin="org.eclipse.core.resources"/>
      <import plugin="org.eclipse.emf.ecore.xmi"/>
      <import plugin="org.eclipse.emf.ecore"/>
      <import plugin="org.topcased.modeler"/>
      <import plugin="org.eclipse.ui"/>
      <import plugin="org.eclipse.ui.editors"/>
      <import plugin="org.eclipse.jface.text"/>
      <import plugin="org.eclipse.ui.workbench.texteditor"/>
      <import plugin="edu.cmu.sei.aadl.texteditor"/>
      <import plugin="org.eclipse.emf.ecore.editor"/>
      <import plugin="org.eclipse.emf.mapping.ecore2xml"/>
      <import plugin="edu.cmu.sei.aadl.annex"/>
      <import plugin="edu.cmu.sei.osate.pluginsupport" version="1.4.1" match="greaterOrEqual"/>
      <import plugin="edu.cmu.sei.osate.workspace"/>
   </requires>

   <plugin
         id="org.topcased.aadl.behavior"
         download-size="0"
         install-size="0"
         version="0.5.2"
         unpack="false"/>

   <plugin
         id="org.topcased.aadl.behavior.texteditor"
         download-size="0"
         install-size="0"
         version="0.5.2"
         unpack="false"/>

   <plugin
         id="org.topcased.aadl.behavior.parser"
         download-size="0"
         install-size="0"
         version="0.5.2"
         unpack="false"/>

</feature>
