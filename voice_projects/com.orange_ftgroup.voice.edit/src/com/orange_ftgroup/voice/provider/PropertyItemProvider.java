/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyItemProvider.java,v 1.1 2008-04-10 15:47:17 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.Property;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.Property} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertyItemProvider
	extends ValueContainerItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIsCompositePropertyDescriptor(object);
			addIsReadOnlyPropertyDescriptor(object);
			addIsDerivedPropertyDescriptor(object);
			addIsIdPropertyDescriptor(object);
			addVisibilityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Is Composite feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsCompositePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_isComposite_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_isComposite_feature", "_UI_Property_type"),
				 VoicePackage.Literals.PROPERTY__IS_COMPOSITE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Read Only feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReadOnlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_isReadOnly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_isReadOnly_feature", "_UI_Property_type"),
				 VoicePackage.Literals.PROPERTY__IS_READ_ONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Derived feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsDerivedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_isDerived_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_isDerived_feature", "_UI_Property_type"),
				 VoicePackage.Literals.PROPERTY__IS_DERIVED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_isId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_isId_feature", "_UI_Property_type"),
				 VoicePackage.Literals.PROPERTY__IS_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Visibility feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVisibilityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_visibility_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_visibility_feature", "_UI_Property_type"),
				 VoicePackage.Literals.PROPERTY__VISIBILITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.PROPERTY__DEFAULT);
		}
		return childrenFeatures;
	}

	/**
	 * This returns Property.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Property"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((Property)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Property_type") :
			getString("_UI_Property_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Property.class)) {
			case VoicePackage.PROPERTY__IS_COMPOSITE:
			case VoicePackage.PROPERTY__IS_READ_ONLY:
			case VoicePackage.PROPERTY__IS_DERIVED:
			case VoicePackage.PROPERTY__IS_ID:
			case VoicePackage.PROPERTY__VISIBILITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VoicePackage.PROPERTY__DEFAULT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createCharacterValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createCharstringValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createIdent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createParenthesisExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createListExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createFeatureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createInformalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createDigitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createDTMFValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createPropertyExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createCollectionLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.PROPERTY__DEFAULT,
				 VoiceFactory.eINSTANCE.createNewExpression()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
