/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateMachineItemProvider.java,v 1.1 2008-04-10 15:47:20 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.StateMachine;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.StateMachine} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class StateMachineItemProvider
	extends BehaviorItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.STATE_MACHINE__NODE);
			childrenFeatures.add(VoicePackage.Literals.STATE_MACHINE__TRANSITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StateMachine.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/StateMachine"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		return getString("_UI_StateMachine_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StateMachine.class)) {
			case VoicePackage.STATE_MACHINE__NODE:
			case VoicePackage.STATE_MACHINE__TRANSITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createSubDialogState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createWaitState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createAnyState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createListState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createDecisionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createInitialNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createJunctionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createDiversionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createHistoryState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createNextNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createStopNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__NODE,
				 VoiceFactory.eINSTANCE.createReturnNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.STATE_MACHINE__TRANSITION,
				 VoiceFactory.eINSTANCE.createTransition()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
