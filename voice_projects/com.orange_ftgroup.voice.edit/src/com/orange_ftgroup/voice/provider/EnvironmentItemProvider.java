/**
 * <copyright>
 * </copyright>
 *
 * $Id: EnvironmentItemProvider.java,v 1.1 2008-04-10 15:47:19 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.Environment} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EnvironmentItemProvider
	extends NamedElementItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__OWNED_GRAMMAR);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__DEFINED_SERVICE);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__REFERENCED_SERVICE);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT);
			childrenFeatures.add(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_OPERATION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Environment.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Environment"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((Environment)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Environment_type") :
			getString("_UI_Environment_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Environment.class)) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__OWNED_GRAMMAR,
				 VoiceFactory.eINSTANCE.createGrammar()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__DEFINED_SERVICE,
				 VoiceFactory.eINSTANCE.createService()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__DEFINED_SERVICE,
				 VoiceFactory.eINSTANCE.createVoiceService()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__REFERENCED_SERVICE,
				 VoiceFactory.eINSTANCE.createService()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__REFERENCED_SERVICE,
				 VoiceFactory.eINSTANCE.createVoiceService()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createDataType()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createEnumerationType()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createChoiceType()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createListType()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_TYPE,
				 VoiceFactory.eINSTANCE.createEntity()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createDTMF()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createSystemInput()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createReject()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createInactivity()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createConcept()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createExternalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createAnyDtmf()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createAnyDigit()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createRecording()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createAbstractDTMF()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_EVENT,
				 VoiceFactory.eINSTANCE.createMessageEnd()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.ENVIRONMENT__PREDEFINED_OPERATION,
				 VoiceFactory.eINSTANCE.createOperation()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCreateChildText(Object owner, Object feature, Object child, Collection selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VoicePackage.Literals.ENVIRONMENT__DEFINED_SERVICE ||
			childFeature == VoicePackage.Literals.ENVIRONMENT__REFERENCED_SERVICE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
