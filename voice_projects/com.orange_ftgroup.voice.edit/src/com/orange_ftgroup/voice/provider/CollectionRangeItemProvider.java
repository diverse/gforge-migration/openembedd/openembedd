/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollectionRangeItemProvider.java,v 1.1 2008-04-10 15:47:19 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.CollectionRange;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.CollectionRange} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CollectionRangeItemProvider
	extends CollectionLiteralPartItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionRangeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.COLLECTION_RANGE__FIRST);
			childrenFeatures.add(VoicePackage.Literals.COLLECTION_RANGE__LAST);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CollectionRange.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CollectionRange"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((CollectionRange)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CollectionRange_type") :
			getString("_UI_CollectionRange_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CollectionRange.class)) {
			case VoicePackage.COLLECTION_RANGE__FIRST:
			case VoicePackage.COLLECTION_RANGE__LAST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createCharacterValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createCharstringValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createIdent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createParenthesisExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createListExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createFeatureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createInformalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createDigitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createDTMFValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createPropertyExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createCollectionLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__FIRST,
				 VoiceFactory.eINSTANCE.createNewExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createCharacterValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createCharstringValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createIdent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createParenthesisExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createListExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createFeatureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createInformalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createDigitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createDTMFValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createPropertyExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createCollectionLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.COLLECTION_RANGE__LAST,
				 VoiceFactory.eINSTANCE.createNewExpression()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCreateChildText(Object owner, Object feature, Object child, Collection selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VoicePackage.Literals.COLLECTION_RANGE__FIRST ||
			childFeature == VoicePackage.Literals.COLLECTION_RANGE__LAST;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
