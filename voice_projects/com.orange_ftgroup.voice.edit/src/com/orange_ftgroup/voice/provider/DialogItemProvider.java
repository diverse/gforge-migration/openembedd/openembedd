/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogItemProvider.java,v 1.1 2008-04-10 15:47:19 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.Dialog} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DialogItemProvider
	extends SignatureItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DialogItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAccessedFunctionalityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Accessed Functionality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAccessedFunctionalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Dialog_accessedFunctionality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Dialog_accessedFunctionality_feature", "_UI_Dialog_type"),
				 VoicePackage.Literals.DIALOG__ACCESSED_FUNCTIONALITY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__NODE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__TRANSITION);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__MESSAGE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__CONCEPT);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__MESSAGE_PART);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__EXTERNAL_EVENT);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__CONDITION);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__VARIABLE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__SUB_DIALOG);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__GLOBAL_VARIABLE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__REFERENCED_VARIABLE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__REFERENCED_MESSAGE);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__OPERATION);
			childrenFeatures.add(VoicePackage.Literals.DIALOG__OWNED_GRAMMAR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Dialog.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Dialog"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((Dialog)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Dialog_type") :
			getString("_UI_Dialog_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Dialog.class)) {
			case VoicePackage.DIALOG__NODE:
			case VoicePackage.DIALOG__TRANSITION:
			case VoicePackage.DIALOG__MESSAGE:
			case VoicePackage.DIALOG__CONCEPT:
			case VoicePackage.DIALOG__MESSAGE_PART:
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
			case VoicePackage.DIALOG__CONDITION:
			case VoicePackage.DIALOG__VARIABLE:
			case VoicePackage.DIALOG__SUB_DIALOG:
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
			case VoicePackage.DIALOG__OPERATION:
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createSubDialogState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createWaitState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createAnyState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createListState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createDecisionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createInitialNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createJunctionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createDiversionNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createHistoryState()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createNextNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createStopNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__NODE,
				 VoiceFactory.eINSTANCE.createReturnNode()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__TRANSITION,
				 VoiceFactory.eINSTANCE.createTransition()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__MESSAGE,
				 VoiceFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__CONCEPT,
				 VoiceFactory.eINSTANCE.createConcept()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__MESSAGE_PART,
				 VoiceFactory.eINSTANCE.createFixPart()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__MESSAGE_PART,
				 VoiceFactory.eINSTANCE.createVariablePart()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__MESSAGE_PART,
				 VoiceFactory.eINSTANCE.createSilencePart()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__EXTERNAL_EVENT,
				 VoiceFactory.eINSTANCE.createExternalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__CONDITION,
				 VoiceFactory.eINSTANCE.createMessageElementCondition()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__VARIABLE,
				 VoiceFactory.eINSTANCE.createVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__SUB_DIALOG,
				 VoiceFactory.eINSTANCE.createDialog()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__GLOBAL_VARIABLE,
				 VoiceFactory.eINSTANCE.createVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_VARIABLE,
				 VoiceFactory.eINSTANCE.createVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_MESSAGE,
				 VoiceFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createDTMF()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createSystemInput()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createReject()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createInactivity()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createConcept()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createExternalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createAnyDtmf()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createAnyDigit()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createRecording()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createAbstractDTMF()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT,
				 VoiceFactory.eINSTANCE.createMessageEnd()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__OPERATION,
				 VoiceFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.DIALOG__OWNED_GRAMMAR,
				 VoiceFactory.eINSTANCE.createGrammar()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCreateChildText(Object owner, Object feature, Object child, Collection selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VoicePackage.Literals.DIALOG__MESSAGE ||
			childFeature == VoicePackage.Literals.DIALOG__REFERENCED_MESSAGE ||
			childFeature == VoicePackage.Literals.DIALOG__CONCEPT ||
			childFeature == VoicePackage.Literals.DIALOG__REFERENCED_INPUT_EVENT ||
			childFeature == VoicePackage.Literals.DIALOG__EXTERNAL_EVENT ||
			childFeature == VoicePackage.Literals.DIALOG__VARIABLE ||
			childFeature == VoicePackage.Literals.DIALOG__GLOBAL_VARIABLE ||
			childFeature == VoicePackage.Literals.DIALOG__REFERENCED_VARIABLE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
