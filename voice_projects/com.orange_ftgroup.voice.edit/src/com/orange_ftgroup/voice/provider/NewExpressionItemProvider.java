/**
 * <copyright>
 * </copyright>
 *
 * $Id: NewExpressionItemProvider.java,v 1.1 2008-04-10 15:47:19 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.NewExpression;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.NewExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NewExpressionItemProvider
	extends ExpressionItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_NewExpression_class_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_NewExpression_class_feature", "_UI_NewExpression_type"),
				 VoicePackage.Literals.NEW_EXPRESSION__CLASS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT);
		}
		return childrenFeatures;
	}

	/**
	 * This returns NewExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/NewExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((NewExpression)object).getDescription();
		return label == null || label.length() == 0 ?
			getString("_UI_NewExpression_type") :
			getString("_UI_NewExpression_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(NewExpression.class)) {
			case VoicePackage.NEW_EXPRESSION__ARGUMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createCharacterValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createCharstringValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createIdent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createParenthesisExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createListExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createFeatureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createInformalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createDigitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createDTMFValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createPropertyExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createCollectionLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.NEW_EXPRESSION__ARGUMENT,
				 VoiceFactory.eINSTANCE.createNewExpression()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
