/**
 * <copyright>
 * </copyright>
 *
 * $Id: UnaryExpressionItemProvider.java,v 1.1 2008-04-10 15:47:19 vmahe Exp $
 */
package com.orange_ftgroup.voice.provider;


import com.orange_ftgroup.voice.UnaryExpression;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.orange_ftgroup.voice.UnaryExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UnaryExpressionItemProvider
	extends ExpressionItemProvider
	implements	
		IEditingDomainItemProvider,	
		IStructuredItemContentProvider,	
		ITreeItemContentProvider,	
		IItemLabelProvider,	
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addOperationNamePropertyDescriptor(object);
			addOperationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Operation Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UnaryExpression_operationName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UnaryExpression_operationName_feature", "_UI_UnaryExpression_type"),
				 VoicePackage.Literals.UNARY_EXPRESSION__OPERATION_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UnaryExpression_operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UnaryExpression_operation_feature", "_UI_UnaryExpression_type"),
				 VoicePackage.Literals.UNARY_EXPRESSION__OPERATION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND);
		}
		return childrenFeatures;
	}

	/**
	 * This returns UnaryExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UnaryExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText(Object object) {
		String label = ((UnaryExpression)object).getOperationName();
		return label == null || label.length() == 0 ?
			getString("_UI_UnaryExpression_type") :
			getString("_UI_UnaryExpression_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UnaryExpression.class)) {
			case VoicePackage.UNARY_EXPRESSION__OPERATION_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VoicePackage.UNARY_EXPRESSION__OPERAND:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds to the collection of {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing all of the children that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createCharacterValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createCharstringValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createIdent()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createParenthesisExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createListExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createFeatureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createInformalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createDigitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createDTMFValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createPropertyExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createCollectionLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(VoicePackage.Literals.UNARY_EXPRESSION__OPERAND,
				 VoiceFactory.eINSTANCE.createNewExpression()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return VoiceEditPlugin.INSTANCE;
	}

}
