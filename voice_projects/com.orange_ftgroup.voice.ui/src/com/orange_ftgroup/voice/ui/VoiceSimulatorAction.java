
package com.orange_ftgroup.voice.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.kermeta.interpreter.api.Interpreter;
import org.kermeta.interpreter.api.InterpreterMode;

import fr.irisa.triskell.eclipse.console.EclipseConsole;
import fr.irisa.triskell.eclipse.console.IOConsole;

public class VoiceSimulatorAction implements IObjectActionDelegate {
	
	/** Path to the model file */
	private IFile voiceModelFile = null;

	/**
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// Nothing to do
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action){
		
		if (voiceModelFile != null) {
			IOConsole console = new EclipseConsole("Voice model simulation");
	        
			Interpreter interpreter;
			
			try {
				interpreter = new Interpreter("platform:/plugin/com.orange_ftgroup.voice.simulator/src/kermeta/VoiceSimulator.kmt",
												InterpreterMode.RUN, null);
				interpreter.setStreams(console);       
				interpreter.setEntryPoint("voice::VoiceSimulator", "main");
				
				String[] parameters = new String[1];
				parameters[0] = "platform:/resource" + voiceModelFile.getFullPath().toString();
				interpreter.setParameters(parameters);
				
			    interpreter.launch();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println ("No model");
		}
	}

	/**
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		IStructuredSelection structured = (IStructuredSelection) selection;
		Object object = structured.getFirstElement();
		if (object instanceof IAdaptable) {
			object = ((IAdaptable) object).getAdapter(IResource.class);
		}
		// Retrieves the path of the model file
		if (object != null && object instanceof IResource) {
			IResource res = (IResource) object;
			if (res instanceof IFile) {
				voiceModelFile = (IFile) res;
			} else {
				voiceModelFile = null;
			}
		}
	}
}

