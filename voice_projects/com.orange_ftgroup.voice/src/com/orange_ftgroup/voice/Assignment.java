/**
 * <copyright>
 * </copyright>
 *
 * $Id: Assignment.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Assignment#getValueContainer <em>Value Container</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Assignment#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends Action {
	/**
	 * Returns the value of the '<em><b>Value Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Container</em>' reference.
	 * @see #setValueContainer(ValueContainer)
	 * @see com.orange_ftgroup.voice.VoicePackage#getAssignment_ValueContainer()
	 * @model
	 * @generated
	 */
	ValueContainer getValueContainer();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Assignment#getValueContainer <em>Value Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Container</em>' reference.
	 * @see #getValueContainer()
	 * @generated
	 */
	void setValueContainer(ValueContainer value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getAssignment_Value()
	 * @model containment="true"
	 * @generated
	 */
	Expression getValue();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Assignment#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Expression value);

} // Assignment