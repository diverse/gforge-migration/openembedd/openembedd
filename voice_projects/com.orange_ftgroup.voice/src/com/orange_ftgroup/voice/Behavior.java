/**
 * <copyright>
 * </copyright>
 *
 * $Id: Behavior.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getBehavior()
 * @model abstract="true"
 * @generated
 */
public interface Behavior extends EObject {
} // Behavior