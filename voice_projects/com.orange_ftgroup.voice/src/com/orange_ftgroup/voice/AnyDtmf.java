/**
 * <copyright>
 * </copyright>
 *
 * $Id: AnyDtmf.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Any Dtmf</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getAnyDtmf()
 * @model
 * @generated
 */
public interface AnyDtmf extends DtmfInput {
} // AnyDtmf