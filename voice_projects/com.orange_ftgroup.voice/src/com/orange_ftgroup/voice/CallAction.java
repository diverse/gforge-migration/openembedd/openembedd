/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallAction.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.CallAction#getCallExpression <em>Call Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getCallAction()
 * @model
 * @generated
 */
public interface CallAction extends Action {
	/**
	 * Returns the value of the '<em><b>Call Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Expression</em>' containment reference.
	 * @see #setCallExpression(CallExpression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getCallAction_CallExpression()
	 * @model containment="true"
	 * @generated
	 */
	CallExpression getCallExpression();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.CallAction#getCallExpression <em>Call Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call Expression</em>' containment reference.
	 * @see #getCallExpression()
	 * @generated
	 */
	void setCallExpression(CallExpression value);

} // CallAction