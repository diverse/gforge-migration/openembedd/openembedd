/**
 * <copyright>
 * </copyright>
 *
 * $Id: Functionality.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Functionality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Functionality#getAccessDialog <em>Access Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Functionality#getParticipant <em>Participant</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getFunctionality()
 * @model
 * @generated
 */
public interface Functionality extends PackageableElement {
	/**
	 * Returns the value of the '<em><b>Access Dialog</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Dialog#getAccessedFunctionality <em>Accessed Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Dialog</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Dialog</em>' reference.
	 * @see #setAccessDialog(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getFunctionality_AccessDialog()
	 * @see com.orange_ftgroup.voice.Dialog#getAccessedFunctionality
	 * @model opposite="accessedFunctionality"
	 * @generated
	 */
	Dialog getAccessDialog();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Functionality#getAccessDialog <em>Access Dialog</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Dialog</em>' reference.
	 * @see #getAccessDialog()
	 * @generated
	 */
	void setAccessDialog(Dialog value);

	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Actor}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Actor#getFunctionality <em>Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getFunctionality_Participant()
	 * @see com.orange_ftgroup.voice.Actor#getFunctionality
	 * @model type="com.orange_ftgroup.voice.Actor" opposite="Functionality"
	 * @generated
	 */
	EList getParticipant();

} // Functionality