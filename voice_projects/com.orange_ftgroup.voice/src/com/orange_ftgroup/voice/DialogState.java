/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogState.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dialog State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.DialogState#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDialogState()
 * @model abstract="true"
 * @generated
 */
public interface DialogState extends DialogNode {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference.
	 * @see #setEntry(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialogState_Entry()
	 * @model containment="true"
	 * @generated
	 */
	ActionSequence getEntry();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.DialogState#getEntry <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry</em>' containment reference.
	 * @see #getEntry()
	 * @generated
	 */
	void setEntry(ActionSequence value);

} // DialogState