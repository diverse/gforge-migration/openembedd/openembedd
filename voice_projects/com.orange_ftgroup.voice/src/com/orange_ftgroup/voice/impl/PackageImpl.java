/**
 * <copyright>
 * </copyright>
 *
 * $Id: PackageImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.PackageableElement;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.PackageImpl#getOwnedElement <em>Owned Element</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.PackageImpl#getUsedElement <em>Used Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PackageImpl extends PackageableElementImpl implements com.orange_ftgroup.voice.Package {
	/**
	 * The cached value of the '{@link #getOwnedElement() <em>Owned Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedElement()
	 * @generated
	 * @ordered
	 */
	protected EList ownedElement = null;

	/**
	 * The cached value of the '{@link #getUsedElement() <em>Used Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsedElement()
	 * @generated
	 * @ordered
	 */
	protected EList usedElement = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOwnedElement() {
		if (ownedElement == null) {
			ownedElement = new EObjectContainmentEList(PackageableElement.class, this, VoicePackage.PACKAGE__OWNED_ELEMENT);
		}
		return ownedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getUsedElement() {
		if (usedElement == null) {
			usedElement = new EObjectResolvingEList(PackageableElement.class, this, VoicePackage.PACKAGE__USED_ELEMENT);
		}
		return usedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.PACKAGE__OWNED_ELEMENT:
				return ((InternalEList)getOwnedElement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.PACKAGE__OWNED_ELEMENT:
				return getOwnedElement();
			case VoicePackage.PACKAGE__USED_ELEMENT:
				return getUsedElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.PACKAGE__OWNED_ELEMENT:
				getOwnedElement().clear();
				getOwnedElement().addAll((Collection)newValue);
				return;
			case VoicePackage.PACKAGE__USED_ELEMENT:
				getUsedElement().clear();
				getUsedElement().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.PACKAGE__OWNED_ELEMENT:
				getOwnedElement().clear();
				return;
			case VoicePackage.PACKAGE__USED_ELEMENT:
				getUsedElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.PACKAGE__OWNED_ELEMENT:
				return ownedElement != null && !ownedElement.isEmpty();
			case VoicePackage.PACKAGE__USED_ELEMENT:
				return usedElement != null && !usedElement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PackageImpl