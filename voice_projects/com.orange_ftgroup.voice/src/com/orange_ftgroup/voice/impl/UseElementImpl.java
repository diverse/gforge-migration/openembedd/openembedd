/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseElementImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.MessagePart;
import com.orange_ftgroup.voice.UseElement;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.UseElementImpl#getUsedMessagePart <em>Used Message Part</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UseElementImpl extends MessageElementImpl implements UseElement {
	/**
	 * The cached value of the '{@link #getUsedMessagePart() <em>Used Message Part</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsedMessagePart()
	 * @generated
	 * @ordered
	 */
	protected MessagePart usedMessagePart = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.USE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessagePart getUsedMessagePart() {
		if (usedMessagePart != null && usedMessagePart.eIsProxy()) {
			InternalEObject oldUsedMessagePart = (InternalEObject)usedMessagePart;
			usedMessagePart = (MessagePart)eResolveProxy(oldUsedMessagePart);
			if (usedMessagePart != oldUsedMessagePart) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.USE_ELEMENT__USED_MESSAGE_PART, oldUsedMessagePart, usedMessagePart));
			}
		}
		return usedMessagePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessagePart basicGetUsedMessagePart() {
		return usedMessagePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsedMessagePart(MessagePart newUsedMessagePart) {
		MessagePart oldUsedMessagePart = usedMessagePart;
		usedMessagePart = newUsedMessagePart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.USE_ELEMENT__USED_MESSAGE_PART, oldUsedMessagePart, usedMessagePart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.USE_ELEMENT__USED_MESSAGE_PART:
				if (resolve) return getUsedMessagePart();
				return basicGetUsedMessagePart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.USE_ELEMENT__USED_MESSAGE_PART:
				setUsedMessagePart((MessagePart)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.USE_ELEMENT__USED_MESSAGE_PART:
				setUsedMessagePart((MessagePart)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.USE_ELEMENT__USED_MESSAGE_PART:
				return usedMessagePart != null;
		}
		return super.eIsSet(featureID);
	}

} //UseElementImpl