/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessageImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.ActionSequence;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.MessageElement;
import com.orange_ftgroup.voice.VisibilityKind;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.MessageImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.MessageImpl#getBody <em>Body</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.MessageImpl#getMessageElement <em>Message Element</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.MessageImpl#getActionSpecification <em>Action Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MessageImpl extends SignatureImpl implements Message {
	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final VisibilityKind VISIBILITY_EDEFAULT = VisibilityKind.PUBLIC_LITERAL;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected VisibilityKind visibility = VISIBILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getBody() <em>Body</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected static final String BODY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected String body = BODY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMessageElement() <em>Message Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageElement()
	 * @generated
	 * @ordered
	 */
	protected EList messageElement = null;

	/**
	 * The cached value of the '{@link #getActionSpecification() <em>Action Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionSpecification()
	 * @generated
	 * @ordered
	 */
	protected ActionSequence actionSpecification = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityKind getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(VisibilityKind newVisibility) {
		VisibilityKind oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.MESSAGE__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBody(String newBody) {
		String oldBody = body;
		body = newBody;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.MESSAGE__BODY, oldBody, body));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getMessageElement() {
		if (messageElement == null) {
			messageElement = new EObjectContainmentEList(MessageElement.class, this, VoicePackage.MESSAGE__MESSAGE_ELEMENT);
		}
		return messageElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSequence getActionSpecification() {
		return actionSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActionSpecification(ActionSequence newActionSpecification, NotificationChain msgs) {
		ActionSequence oldActionSpecification = actionSpecification;
		actionSpecification = newActionSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.MESSAGE__ACTION_SPECIFICATION, oldActionSpecification, newActionSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActionSpecification(ActionSequence newActionSpecification) {
		if (newActionSpecification != actionSpecification) {
			NotificationChain msgs = null;
			if (actionSpecification != null)
				msgs = ((InternalEObject)actionSpecification).eInverseRemove(this, VoicePackage.ACTION_SEQUENCE__MESSAGE, ActionSequence.class, msgs);
			if (newActionSpecification != null)
				msgs = ((InternalEObject)newActionSpecification).eInverseAdd(this, VoicePackage.ACTION_SEQUENCE__MESSAGE, ActionSequence.class, msgs);
			msgs = basicSetActionSpecification(newActionSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.MESSAGE__ACTION_SPECIFICATION, newActionSpecification, newActionSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				if (actionSpecification != null)
					msgs = ((InternalEObject)actionSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.MESSAGE__ACTION_SPECIFICATION, null, msgs);
				return basicSetActionSpecification((ActionSequence)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.MESSAGE__MESSAGE_ELEMENT:
				return ((InternalEList)getMessageElement()).basicRemove(otherEnd, msgs);
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				return basicSetActionSpecification(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.MESSAGE__VISIBILITY:
				return getVisibility();
			case VoicePackage.MESSAGE__BODY:
				return getBody();
			case VoicePackage.MESSAGE__MESSAGE_ELEMENT:
				return getMessageElement();
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				return getActionSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.MESSAGE__VISIBILITY:
				setVisibility((VisibilityKind)newValue);
				return;
			case VoicePackage.MESSAGE__BODY:
				setBody((String)newValue);
				return;
			case VoicePackage.MESSAGE__MESSAGE_ELEMENT:
				getMessageElement().clear();
				getMessageElement().addAll((Collection)newValue);
				return;
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				setActionSpecification((ActionSequence)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.MESSAGE__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case VoicePackage.MESSAGE__BODY:
				setBody(BODY_EDEFAULT);
				return;
			case VoicePackage.MESSAGE__MESSAGE_ELEMENT:
				getMessageElement().clear();
				return;
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				setActionSpecification((ActionSequence)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.MESSAGE__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case VoicePackage.MESSAGE__BODY:
				return BODY_EDEFAULT == null ? body != null : !BODY_EDEFAULT.equals(body);
			case VoicePackage.MESSAGE__MESSAGE_ELEMENT:
				return messageElement != null && !messageElement.isEmpty();
			case VoicePackage.MESSAGE__ACTION_SPECIFICATION:
				return actionSpecification != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (visibility: ");
		result.append(visibility);
		result.append(", body: ");
		result.append(body);
		result.append(')');
		return result.toString();
	}

} //MessageImpl