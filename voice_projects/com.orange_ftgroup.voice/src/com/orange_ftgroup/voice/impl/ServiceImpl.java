/**
 * <copyright>
 * </copyright>
 *
 * $Id: ServiceImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getOwnedGrammar <em>Owned Grammar</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getSubService <em>Sub Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getOfferedFunctionality <em>Offered Functionality</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getEntityPackage <em>Entity Package</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ServiceImpl#getFunctionalityPackage <em>Functionality Package</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceImpl extends NamedElementImpl implements Service {
	/**
	 * The cached value of the '{@link #getOwnedGrammar() <em>Owned Grammar</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedGrammar()
	 * @generated
	 * @ordered
	 */
	protected EList ownedGrammar = null;

	/**
	 * The cached value of the '{@link #getSubService() <em>Sub Service</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubService()
	 * @generated
	 * @ordered
	 */
	protected EList subService = null;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected EList parent = null;

	/**
	 * The cached value of the '{@link #getOfferedFunctionality() <em>Offered Functionality</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOfferedFunctionality()
	 * @generated
	 * @ordered
	 */
	protected EList offeredFunctionality = null;

	/**
	 * The cached value of the '{@link #getEntityPackage() <em>Entity Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityPackage()
	 * @generated
	 * @ordered
	 */
	protected com.orange_ftgroup.voice.Package entityPackage = null;

	/**
	 * The cached value of the '{@link #getFunctionalityPackage() <em>Functionality Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionalityPackage()
	 * @generated
	 * @ordered
	 */
	protected com.orange_ftgroup.voice.Package functionalityPackage = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.SERVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOwnedGrammar() {
		if (ownedGrammar == null) {
			ownedGrammar = new EObjectContainmentWithInverseEList(Grammar.class, this, VoicePackage.SERVICE__OWNED_GRAMMAR, VoicePackage.GRAMMAR__SERVICE);
		}
		return ownedGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSubService() {
		if (subService == null) {
			subService = new EObjectWithInverseResolvingEList.ManyInverse(Service.class, this, VoicePackage.SERVICE__SUB_SERVICE, VoicePackage.SERVICE__PARENT);
		}
		return subService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getParent() {
		if (parent == null) {
			parent = new EObjectWithInverseResolvingEList.ManyInverse(Service.class, this, VoicePackage.SERVICE__PARENT, VoicePackage.SERVICE__SUB_SERVICE);
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOfferedFunctionality() {
		if (offeredFunctionality == null) {
			offeredFunctionality = new EObjectResolvingEList(Functionality.class, this, VoicePackage.SERVICE__OFFERED_FUNCTIONALITY);
		}
		return offeredFunctionality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public com.orange_ftgroup.voice.Package getEntityPackage() {
		return entityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntityPackage(com.orange_ftgroup.voice.Package newEntityPackage, NotificationChain msgs) {
		com.orange_ftgroup.voice.Package oldEntityPackage = entityPackage;
		entityPackage = newEntityPackage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.SERVICE__ENTITY_PACKAGE, oldEntityPackage, newEntityPackage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityPackage(com.orange_ftgroup.voice.Package newEntityPackage) {
		if (newEntityPackage != entityPackage) {
			NotificationChain msgs = null;
			if (entityPackage != null)
				msgs = ((InternalEObject)entityPackage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.SERVICE__ENTITY_PACKAGE, null, msgs);
			if (newEntityPackage != null)
				msgs = ((InternalEObject)newEntityPackage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.SERVICE__ENTITY_PACKAGE, null, msgs);
			msgs = basicSetEntityPackage(newEntityPackage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.SERVICE__ENTITY_PACKAGE, newEntityPackage, newEntityPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public com.orange_ftgroup.voice.Package getFunctionalityPackage() {
		return functionalityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionalityPackage(com.orange_ftgroup.voice.Package newFunctionalityPackage, NotificationChain msgs) {
		com.orange_ftgroup.voice.Package oldFunctionalityPackage = functionalityPackage;
		functionalityPackage = newFunctionalityPackage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE, oldFunctionalityPackage, newFunctionalityPackage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionalityPackage(com.orange_ftgroup.voice.Package newFunctionalityPackage) {
		if (newFunctionalityPackage != functionalityPackage) {
			NotificationChain msgs = null;
			if (functionalityPackage != null)
				msgs = ((InternalEObject)functionalityPackage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE, null, msgs);
			if (newFunctionalityPackage != null)
				msgs = ((InternalEObject)newFunctionalityPackage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE, null, msgs);
			msgs = basicSetFunctionalityPackage(newFunctionalityPackage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE, newFunctionalityPackage, newFunctionalityPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicAdd(otherEnd, msgs);
			case VoicePackage.SERVICE__SUB_SERVICE:
				return ((InternalEList)getSubService()).basicAdd(otherEnd, msgs);
			case VoicePackage.SERVICE__PARENT:
				return ((InternalEList)getParent()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicRemove(otherEnd, msgs);
			case VoicePackage.SERVICE__SUB_SERVICE:
				return ((InternalEList)getSubService()).basicRemove(otherEnd, msgs);
			case VoicePackage.SERVICE__PARENT:
				return ((InternalEList)getParent()).basicRemove(otherEnd, msgs);
			case VoicePackage.SERVICE__ENTITY_PACKAGE:
				return basicSetEntityPackage(null, msgs);
			case VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE:
				return basicSetFunctionalityPackage(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				return getOwnedGrammar();
			case VoicePackage.SERVICE__SUB_SERVICE:
				return getSubService();
			case VoicePackage.SERVICE__PARENT:
				return getParent();
			case VoicePackage.SERVICE__OFFERED_FUNCTIONALITY:
				return getOfferedFunctionality();
			case VoicePackage.SERVICE__ENTITY_PACKAGE:
				return getEntityPackage();
			case VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE:
				return getFunctionalityPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				getOwnedGrammar().addAll((Collection)newValue);
				return;
			case VoicePackage.SERVICE__SUB_SERVICE:
				getSubService().clear();
				getSubService().addAll((Collection)newValue);
				return;
			case VoicePackage.SERVICE__PARENT:
				getParent().clear();
				getParent().addAll((Collection)newValue);
				return;
			case VoicePackage.SERVICE__OFFERED_FUNCTIONALITY:
				getOfferedFunctionality().clear();
				getOfferedFunctionality().addAll((Collection)newValue);
				return;
			case VoicePackage.SERVICE__ENTITY_PACKAGE:
				setEntityPackage((com.orange_ftgroup.voice.Package)newValue);
				return;
			case VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE:
				setFunctionalityPackage((com.orange_ftgroup.voice.Package)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				return;
			case VoicePackage.SERVICE__SUB_SERVICE:
				getSubService().clear();
				return;
			case VoicePackage.SERVICE__PARENT:
				getParent().clear();
				return;
			case VoicePackage.SERVICE__OFFERED_FUNCTIONALITY:
				getOfferedFunctionality().clear();
				return;
			case VoicePackage.SERVICE__ENTITY_PACKAGE:
				setEntityPackage((com.orange_ftgroup.voice.Package)null);
				return;
			case VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE:
				setFunctionalityPackage((com.orange_ftgroup.voice.Package)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.SERVICE__OWNED_GRAMMAR:
				return ownedGrammar != null && !ownedGrammar.isEmpty();
			case VoicePackage.SERVICE__SUB_SERVICE:
				return subService != null && !subService.isEmpty();
			case VoicePackage.SERVICE__PARENT:
				return parent != null && !parent.isEmpty();
			case VoicePackage.SERVICE__OFFERED_FUNCTIONALITY:
				return offeredFunctionality != null && !offeredFunctionality.isEmpty();
			case VoicePackage.SERVICE__ENTITY_PACKAGE:
				return entityPackage != null;
			case VoicePackage.SERVICE__FUNCTIONALITY_PACKAGE:
				return functionalityPackage != null;
		}
		return super.eIsSet(featureID);
	}

} //ServiceImpl