/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConceptImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Concept;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.ConceptImpl#getGrammar <em>Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConceptImpl extends InputEventImpl implements Concept {
	/**
	 * The cached value of the '{@link #getGrammar() <em>Grammar</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrammar()
	 * @generated
	 * @ordered
	 */
	protected EList grammar = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConceptImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.CONCEPT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGrammar() {
		if (grammar == null) {
			grammar = new EObjectResolvingEList(Grammar.class, this, VoicePackage.CONCEPT__GRAMMAR);
		}
		return grammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.CONCEPT__GRAMMAR:
				return getGrammar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.CONCEPT__GRAMMAR:
				getGrammar().clear();
				getGrammar().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.CONCEPT__GRAMMAR:
				getGrammar().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.CONCEPT__GRAMMAR:
				return grammar != null && !grammar.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConceptImpl