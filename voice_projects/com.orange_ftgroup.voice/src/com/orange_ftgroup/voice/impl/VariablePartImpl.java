/**
 * <copyright>
 * </copyright>
 *
 * $Id: VariablePartImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.ActionSequence;
import com.orange_ftgroup.voice.DiffusionType;
import com.orange_ftgroup.voice.Expression;
import com.orange_ftgroup.voice.VariablePart;
import com.orange_ftgroup.voice.VisibilityKind;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.VariablePartImpl#getFormat <em>Format</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.VariablePartImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.VariablePartImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.VariablePartImpl#getActionSpecification <em>Action Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariablePartImpl extends MessagePartImpl implements VariablePart {
	/**
	 * The default value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected static final DiffusionType FORMAT_EDEFAULT = DiffusionType.DIGITS_LITERAL;

	/**
	 * The cached value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected DiffusionType format = FORMAT_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final VisibilityKind VISIBILITY_EDEFAULT = VisibilityKind.PUBLIC_LITERAL;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected VisibilityKind visibility = VISIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Expression value = null;

	/**
	 * The cached value of the '{@link #getActionSpecification() <em>Action Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionSpecification()
	 * @generated
	 * @ordered
	 */
	protected ActionSequence actionSpecification = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariablePartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.VARIABLE_PART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiffusionType getFormat() {
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormat(DiffusionType newFormat) {
		DiffusionType oldFormat = format;
		format = newFormat == null ? FORMAT_EDEFAULT : newFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__FORMAT, oldFormat, format));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityKind getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(VisibilityKind newVisibility) {
		VisibilityKind oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Expression newValue, NotificationChain msgs) {
		Expression oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__VALUE, oldValue, newValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Expression newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VARIABLE_PART__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VARIABLE_PART__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSequence getActionSpecification() {
		return actionSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActionSpecification(ActionSequence newActionSpecification, NotificationChain msgs) {
		ActionSequence oldActionSpecification = actionSpecification;
		actionSpecification = newActionSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION, oldActionSpecification, newActionSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActionSpecification(ActionSequence newActionSpecification) {
		if (newActionSpecification != actionSpecification) {
			NotificationChain msgs = null;
			if (actionSpecification != null)
				msgs = ((InternalEObject)actionSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION, null, msgs);
			if (newActionSpecification != null)
				msgs = ((InternalEObject)newActionSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION, null, msgs);
			msgs = basicSetActionSpecification(newActionSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION, newActionSpecification, newActionSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.VARIABLE_PART__VALUE:
				return basicSetValue(null, msgs);
			case VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION:
				return basicSetActionSpecification(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.VARIABLE_PART__FORMAT:
				return getFormat();
			case VoicePackage.VARIABLE_PART__VISIBILITY:
				return getVisibility();
			case VoicePackage.VARIABLE_PART__VALUE:
				return getValue();
			case VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION:
				return getActionSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.VARIABLE_PART__FORMAT:
				setFormat((DiffusionType)newValue);
				return;
			case VoicePackage.VARIABLE_PART__VISIBILITY:
				setVisibility((VisibilityKind)newValue);
				return;
			case VoicePackage.VARIABLE_PART__VALUE:
				setValue((Expression)newValue);
				return;
			case VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION:
				setActionSpecification((ActionSequence)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.VARIABLE_PART__FORMAT:
				setFormat(FORMAT_EDEFAULT);
				return;
			case VoicePackage.VARIABLE_PART__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case VoicePackage.VARIABLE_PART__VALUE:
				setValue((Expression)null);
				return;
			case VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION:
				setActionSpecification((ActionSequence)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.VARIABLE_PART__FORMAT:
				return format != FORMAT_EDEFAULT;
			case VoicePackage.VARIABLE_PART__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case VoicePackage.VARIABLE_PART__VALUE:
				return value != null;
			case VoicePackage.VARIABLE_PART__ACTION_SPECIFICATION:
				return actionSpecification != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (format: ");
		result.append(format);
		result.append(", visibility: ");
		result.append(visibility);
		result.append(')');
		return result.toString();
	}

} //VariablePartImpl