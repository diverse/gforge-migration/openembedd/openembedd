/**
 * <copyright>
 * </copyright>
 *
 * $Id: EnvironmentImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.InputEvent;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.Type;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getOwnedGrammar <em>Owned Grammar</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getDefinedService <em>Defined Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getReferencedService <em>Referenced Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getPredefinedType <em>Predefined Type</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getPredefinedEvent <em>Predefined Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.EnvironmentImpl#getPredefinedOperation <em>Predefined Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EnvironmentImpl extends NamedElementImpl implements Environment {
	/**
	 * The cached value of the '{@link #getOwnedGrammar() <em>Owned Grammar</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedGrammar()
	 * @generated
	 * @ordered
	 */
	protected EList ownedGrammar = null;

	/**
	 * The cached value of the '{@link #getDefinedService() <em>Defined Service</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinedService()
	 * @generated
	 * @ordered
	 */
	protected EList definedService = null;

	/**
	 * The cached value of the '{@link #getReferencedService() <em>Referenced Service</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedService()
	 * @generated
	 * @ordered
	 */
	protected EList referencedService = null;

	/**
	 * The cached value of the '{@link #getPredefinedType() <em>Predefined Type</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredefinedType()
	 * @generated
	 * @ordered
	 */
	protected EList predefinedType = null;

	/**
	 * The cached value of the '{@link #getPredefinedEvent() <em>Predefined Event</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredefinedEvent()
	 * @generated
	 * @ordered
	 */
	protected EList predefinedEvent = null;

	/**
	 * The cached value of the '{@link #getPredefinedOperation() <em>Predefined Operation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredefinedOperation()
	 * @generated
	 * @ordered
	 */
	protected EList predefinedOperation = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOwnedGrammar() {
		if (ownedGrammar == null) {
			ownedGrammar = new EObjectContainmentWithInverseEList(Grammar.class, this, VoicePackage.ENVIRONMENT__OWNED_GRAMMAR, VoicePackage.GRAMMAR__ENVIRONMENT);
		}
		return ownedGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getDefinedService() {
		if (definedService == null) {
			definedService = new EObjectContainmentEList(Service.class, this, VoicePackage.ENVIRONMENT__DEFINED_SERVICE);
		}
		return definedService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getReferencedService() {
		if (referencedService == null) {
			referencedService = new EObjectContainmentEList(Service.class, this, VoicePackage.ENVIRONMENT__REFERENCED_SERVICE);
		}
		return referencedService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPredefinedType() {
		if (predefinedType == null) {
			predefinedType = new EObjectContainmentEList(Type.class, this, VoicePackage.ENVIRONMENT__PREDEFINED_TYPE);
		}
		return predefinedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPredefinedEvent() {
		if (predefinedEvent == null) {
			predefinedEvent = new EObjectContainmentEList(InputEvent.class, this, VoicePackage.ENVIRONMENT__PREDEFINED_EVENT);
		}
		return predefinedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPredefinedOperation() {
		if (predefinedOperation == null) {
			predefinedOperation = new EObjectContainmentEList(Operation.class, this, VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION);
		}
		return predefinedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicRemove(otherEnd, msgs);
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
				return ((InternalEList)getDefinedService()).basicRemove(otherEnd, msgs);
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
				return ((InternalEList)getReferencedService()).basicRemove(otherEnd, msgs);
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
				return ((InternalEList)getPredefinedType()).basicRemove(otherEnd, msgs);
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
				return ((InternalEList)getPredefinedEvent()).basicRemove(otherEnd, msgs);
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				return ((InternalEList)getPredefinedOperation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				return getOwnedGrammar();
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
				return getDefinedService();
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
				return getReferencedService();
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
				return getPredefinedType();
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
				return getPredefinedEvent();
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				return getPredefinedOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				getOwnedGrammar().addAll((Collection)newValue);
				return;
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
				getDefinedService().clear();
				getDefinedService().addAll((Collection)newValue);
				return;
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
				getReferencedService().clear();
				getReferencedService().addAll((Collection)newValue);
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
				getPredefinedType().clear();
				getPredefinedType().addAll((Collection)newValue);
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
				getPredefinedEvent().clear();
				getPredefinedEvent().addAll((Collection)newValue);
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				getPredefinedOperation().clear();
				getPredefinedOperation().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				return;
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
				getDefinedService().clear();
				return;
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
				getReferencedService().clear();
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
				getPredefinedType().clear();
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
				getPredefinedEvent().clear();
				return;
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				getPredefinedOperation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.ENVIRONMENT__OWNED_GRAMMAR:
				return ownedGrammar != null && !ownedGrammar.isEmpty();
			case VoicePackage.ENVIRONMENT__DEFINED_SERVICE:
				return definedService != null && !definedService.isEmpty();
			case VoicePackage.ENVIRONMENT__REFERENCED_SERVICE:
				return referencedService != null && !referencedService.isEmpty();
			case VoicePackage.ENVIRONMENT__PREDEFINED_TYPE:
				return predefinedType != null && !predefinedType.isEmpty();
			case VoicePackage.ENVIRONMENT__PREDEFINED_EVENT:
				return predefinedEvent != null && !predefinedEvent.isEmpty();
			case VoicePackage.ENVIRONMENT__PREDEFINED_OPERATION:
				return predefinedOperation != null && !predefinedOperation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EnvironmentImpl