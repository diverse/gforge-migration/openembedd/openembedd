/**
 * <copyright>
 * </copyright>
 *
 * $Id: ParenthesisExpressionImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Expression;
import com.orange_ftgroup.voice.ParenthesisExpression;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parenthesis Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.ParenthesisExpressionImpl#getInnerExpression <em>Inner Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ParenthesisExpressionImpl extends ExpressionImpl implements ParenthesisExpression {
	/**
	 * The cached value of the '{@link #getInnerExpression() <em>Inner Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression innerExpression = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParenthesisExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.PARENTHESIS_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInnerExpression() {
		return innerExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInnerExpression(Expression newInnerExpression, NotificationChain msgs) {
		Expression oldInnerExpression = innerExpression;
		innerExpression = newInnerExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION, oldInnerExpression, newInnerExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnerExpression(Expression newInnerExpression) {
		if (newInnerExpression != innerExpression) {
			NotificationChain msgs = null;
			if (innerExpression != null)
				msgs = ((InternalEObject)innerExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION, null, msgs);
			if (newInnerExpression != null)
				msgs = ((InternalEObject)newInnerExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION, null, msgs);
			msgs = basicSetInnerExpression(newInnerExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION, newInnerExpression, newInnerExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION:
				return basicSetInnerExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION:
				return getInnerExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION:
				setInnerExpression((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION:
				setInnerExpression((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.PARENTHESIS_EXPRESSION__INNER_EXPRESSION:
				return innerExpression != null;
		}
		return super.eIsSet(featureID);
	}

} //ParenthesisExpressionImpl