/**
 * <copyright>
 * </copyright>
 *
 * $Id: GrammarImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grammar</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#isIsComputed <em>Is Computed</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getFormalism <em>Formalism</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getContent <em>Content</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getService <em>Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getDialog <em>Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.GrammarImpl#getDynamicDefinition <em>Dynamic Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GrammarImpl extends NamedElementImpl implements Grammar {
	/**
	 * The default value of the '{@link #isIsComputed() <em>Is Computed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsComputed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_COMPUTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsComputed() <em>Is Computed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsComputed()
	 * @generated
	 * @ordered
	 */
	protected boolean isComputed = IS_COMPUTED_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalism() <em>Formalism</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalism()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMALISM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalism() <em>Formalism</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalism()
	 * @generated
	 * @ordered
	 */
	protected String formalism = FORMALISM_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected String language = LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDynamicDefinition() <em>Dynamic Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicDefinition()
	 * @generated
	 * @ordered
	 */
	protected Operation dynamicDefinition = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GrammarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.GRAMMAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsComputed() {
		return isComputed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsComputed(boolean newIsComputed) {
		boolean oldIsComputed = isComputed;
		isComputed = newIsComputed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__IS_COMPUTED, oldIsComputed, isComputed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalism() {
		return formalism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalism(String newFormalism) {
		String oldFormalism = formalism;
		formalism = newFormalism;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__FORMALISM, oldFormalism, formalism));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguage(String newLanguage) {
		String oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__LANGUAGE, oldLanguage, language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnvironment() {
		if (eContainerFeatureID != VoicePackage.GRAMMAR__ENVIRONMENT) return null;
		return (Environment)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironment(Environment newEnvironment, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newEnvironment, VoicePackage.GRAMMAR__ENVIRONMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironment(Environment newEnvironment) {
		if (newEnvironment != eInternalContainer() || (eContainerFeatureID != VoicePackage.GRAMMAR__ENVIRONMENT && newEnvironment != null)) {
			if (EcoreUtil.isAncestor(this, newEnvironment))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newEnvironment != null)
				msgs = ((InternalEObject)newEnvironment).eInverseAdd(this, VoicePackage.ENVIRONMENT__OWNED_GRAMMAR, Environment.class, msgs);
			msgs = basicSetEnvironment(newEnvironment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__ENVIRONMENT, newEnvironment, newEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service getService() {
		if (eContainerFeatureID != VoicePackage.GRAMMAR__SERVICE) return null;
		return (Service)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetService(Service newService, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newService, VoicePackage.GRAMMAR__SERVICE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setService(Service newService) {
		if (newService != eInternalContainer() || (eContainerFeatureID != VoicePackage.GRAMMAR__SERVICE && newService != null)) {
			if (EcoreUtil.isAncestor(this, newService))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newService != null)
				msgs = ((InternalEObject)newService).eInverseAdd(this, VoicePackage.SERVICE__OWNED_GRAMMAR, Service.class, msgs);
			msgs = basicSetService(newService, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__SERVICE, newService, newService));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog getDialog() {
		if (eContainerFeatureID != VoicePackage.GRAMMAR__DIALOG) return null;
		return (Dialog)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDialog(Dialog newDialog, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDialog, VoicePackage.GRAMMAR__DIALOG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDialog(Dialog newDialog) {
		if (newDialog != eInternalContainer() || (eContainerFeatureID != VoicePackage.GRAMMAR__DIALOG && newDialog != null)) {
			if (EcoreUtil.isAncestor(this, newDialog))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDialog != null)
				msgs = ((InternalEObject)newDialog).eInverseAdd(this, VoicePackage.DIALOG__OWNED_GRAMMAR, Dialog.class, msgs);
			msgs = basicSetDialog(newDialog, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__DIALOG, newDialog, newDialog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getDynamicDefinition() {
		if (dynamicDefinition != null && dynamicDefinition.eIsProxy()) {
			InternalEObject oldDynamicDefinition = (InternalEObject)dynamicDefinition;
			dynamicDefinition = (Operation)eResolveProxy(oldDynamicDefinition);
			if (dynamicDefinition != oldDynamicDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.GRAMMAR__DYNAMIC_DEFINITION, oldDynamicDefinition, dynamicDefinition));
			}
		}
		return dynamicDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetDynamicDefinition() {
		return dynamicDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicDefinition(Operation newDynamicDefinition) {
		Operation oldDynamicDefinition = dynamicDefinition;
		dynamicDefinition = newDynamicDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.GRAMMAR__DYNAMIC_DEFINITION, oldDynamicDefinition, dynamicDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetEnvironment((Environment)otherEnd, msgs);
			case VoicePackage.GRAMMAR__SERVICE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetService((Service)otherEnd, msgs);
			case VoicePackage.GRAMMAR__DIALOG:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDialog((Dialog)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				return basicSetEnvironment(null, msgs);
			case VoicePackage.GRAMMAR__SERVICE:
				return basicSetService(null, msgs);
			case VoicePackage.GRAMMAR__DIALOG:
				return basicSetDialog(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				return eInternalContainer().eInverseRemove(this, VoicePackage.ENVIRONMENT__OWNED_GRAMMAR, Environment.class, msgs);
			case VoicePackage.GRAMMAR__SERVICE:
				return eInternalContainer().eInverseRemove(this, VoicePackage.SERVICE__OWNED_GRAMMAR, Service.class, msgs);
			case VoicePackage.GRAMMAR__DIALOG:
				return eInternalContainer().eInverseRemove(this, VoicePackage.DIALOG__OWNED_GRAMMAR, Dialog.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__IS_COMPUTED:
				return isIsComputed() ? Boolean.TRUE : Boolean.FALSE;
			case VoicePackage.GRAMMAR__FORMALISM:
				return getFormalism();
			case VoicePackage.GRAMMAR__LANGUAGE:
				return getLanguage();
			case VoicePackage.GRAMMAR__CONTENT:
				return getContent();
			case VoicePackage.GRAMMAR__LOCATION:
				return getLocation();
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				return getEnvironment();
			case VoicePackage.GRAMMAR__SERVICE:
				return getService();
			case VoicePackage.GRAMMAR__DIALOG:
				return getDialog();
			case VoicePackage.GRAMMAR__DYNAMIC_DEFINITION:
				if (resolve) return getDynamicDefinition();
				return basicGetDynamicDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__IS_COMPUTED:
				setIsComputed(((Boolean)newValue).booleanValue());
				return;
			case VoicePackage.GRAMMAR__FORMALISM:
				setFormalism((String)newValue);
				return;
			case VoicePackage.GRAMMAR__LANGUAGE:
				setLanguage((String)newValue);
				return;
			case VoicePackage.GRAMMAR__CONTENT:
				setContent((String)newValue);
				return;
			case VoicePackage.GRAMMAR__LOCATION:
				setLocation((String)newValue);
				return;
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				setEnvironment((Environment)newValue);
				return;
			case VoicePackage.GRAMMAR__SERVICE:
				setService((Service)newValue);
				return;
			case VoicePackage.GRAMMAR__DIALOG:
				setDialog((Dialog)newValue);
				return;
			case VoicePackage.GRAMMAR__DYNAMIC_DEFINITION:
				setDynamicDefinition((Operation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__IS_COMPUTED:
				setIsComputed(IS_COMPUTED_EDEFAULT);
				return;
			case VoicePackage.GRAMMAR__FORMALISM:
				setFormalism(FORMALISM_EDEFAULT);
				return;
			case VoicePackage.GRAMMAR__LANGUAGE:
				setLanguage(LANGUAGE_EDEFAULT);
				return;
			case VoicePackage.GRAMMAR__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case VoicePackage.GRAMMAR__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				setEnvironment((Environment)null);
				return;
			case VoicePackage.GRAMMAR__SERVICE:
				setService((Service)null);
				return;
			case VoicePackage.GRAMMAR__DIALOG:
				setDialog((Dialog)null);
				return;
			case VoicePackage.GRAMMAR__DYNAMIC_DEFINITION:
				setDynamicDefinition((Operation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.GRAMMAR__IS_COMPUTED:
				return isComputed != IS_COMPUTED_EDEFAULT;
			case VoicePackage.GRAMMAR__FORMALISM:
				return FORMALISM_EDEFAULT == null ? formalism != null : !FORMALISM_EDEFAULT.equals(formalism);
			case VoicePackage.GRAMMAR__LANGUAGE:
				return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
			case VoicePackage.GRAMMAR__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case VoicePackage.GRAMMAR__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case VoicePackage.GRAMMAR__ENVIRONMENT:
				return getEnvironment() != null;
			case VoicePackage.GRAMMAR__SERVICE:
				return getService() != null;
			case VoicePackage.GRAMMAR__DIALOG:
				return getDialog() != null;
			case VoicePackage.GRAMMAR__DYNAMIC_DEFINITION:
				return dynamicDefinition != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isComputed: ");
		result.append(isComputed);
		result.append(", formalism: ");
		result.append(formalism);
		result.append(", language: ");
		result.append(language);
		result.append(", content: ");
		result.append(content);
		result.append(", location: ");
		result.append(location);
		result.append(')');
		return result.toString();
	}

} //GrammarImpl