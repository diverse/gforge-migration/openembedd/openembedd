/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Concept;
import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.DialogNode;
import com.orange_ftgroup.voice.ExternalEvent;
import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.InputEvent;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.MessagePart;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.Transition;
import com.orange_ftgroup.voice.Variable;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dialog</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getNode <em>Node</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getAccessedFunctionality <em>Accessed Functionality</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getTransition <em>Transition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getConcept <em>Concept</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getMessagePart <em>Message Part</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getExternalEvent <em>External Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getSubDialog <em>Sub Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getGlobalVariable <em>Global Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getReferencedVariable <em>Referenced Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getReferencedMessage <em>Referenced Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getReferencedInputEvent <em>Referenced Input Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogImpl#getOwnedGrammar <em>Owned Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DialogImpl extends SignatureImpl implements Dialog {
	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected EList node = null;

	/**
	 * The cached value of the '{@link #getAccessedFunctionality() <em>Accessed Functionality</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessedFunctionality()
	 * @generated
	 * @ordered
	 */
	protected Functionality accessedFunctionality = null;

	/**
	 * The cached value of the '{@link #getTransition() <em>Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransition()
	 * @generated
	 * @ordered
	 */
	protected EList transition = null;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList message = null;

	/**
	 * The cached value of the '{@link #getConcept() <em>Concept</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConcept()
	 * @generated
	 * @ordered
	 */
	protected EList concept = null;

	/**
	 * The cached value of the '{@link #getMessagePart() <em>Message Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessagePart()
	 * @generated
	 * @ordered
	 */
	protected EList messagePart = null;

	/**
	 * The cached value of the '{@link #getExternalEvent() <em>External Event</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalEvent()
	 * @generated
	 * @ordered
	 */
	protected EList externalEvent = null;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected EList condition = null;

	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected EList variable = null;

	/**
	 * The cached value of the '{@link #getSubDialog() <em>Sub Dialog</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubDialog()
	 * @generated
	 * @ordered
	 */
	protected EList subDialog = null;

	/**
	 * The cached value of the '{@link #getGlobalVariable() <em>Global Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalVariable()
	 * @generated
	 * @ordered
	 */
	protected EList globalVariable = null;

	/**
	 * The cached value of the '{@link #getReferencedVariable() <em>Referenced Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedVariable()
	 * @generated
	 * @ordered
	 */
	protected EList referencedVariable = null;

	/**
	 * The cached value of the '{@link #getReferencedMessage() <em>Referenced Message</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedMessage()
	 * @generated
	 * @ordered
	 */
	protected EList referencedMessage = null;

	/**
	 * The cached value of the '{@link #getReferencedInputEvent() <em>Referenced Input Event</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedInputEvent()
	 * @generated
	 * @ordered
	 */
	protected EList referencedInputEvent = null;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected EList operation = null;

	/**
	 * The cached value of the '{@link #getOwnedGrammar() <em>Owned Grammar</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedGrammar()
	 * @generated
	 * @ordered
	 */
	protected EList ownedGrammar = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DialogImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.DIALOG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getNode() {
		if (node == null) {
			node = new EObjectContainmentWithInverseEList(DialogNode.class, this, VoicePackage.DIALOG__NODE, VoicePackage.DIALOG_NODE__OWNER);
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Functionality getAccessedFunctionality() {
		if (accessedFunctionality != null && accessedFunctionality.eIsProxy()) {
			InternalEObject oldAccessedFunctionality = (InternalEObject)accessedFunctionality;
			accessedFunctionality = (Functionality)eResolveProxy(oldAccessedFunctionality);
			if (accessedFunctionality != oldAccessedFunctionality) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, oldAccessedFunctionality, accessedFunctionality));
			}
		}
		return accessedFunctionality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Functionality basicGetAccessedFunctionality() {
		return accessedFunctionality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessedFunctionality(Functionality newAccessedFunctionality, NotificationChain msgs) {
		Functionality oldAccessedFunctionality = accessedFunctionality;
		accessedFunctionality = newAccessedFunctionality;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, oldAccessedFunctionality, newAccessedFunctionality);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessedFunctionality(Functionality newAccessedFunctionality) {
		if (newAccessedFunctionality != accessedFunctionality) {
			NotificationChain msgs = null;
			if (accessedFunctionality != null)
				msgs = ((InternalEObject)accessedFunctionality).eInverseRemove(this, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, Functionality.class, msgs);
			if (newAccessedFunctionality != null)
				msgs = ((InternalEObject)newAccessedFunctionality).eInverseAdd(this, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, Functionality.class, msgs);
			msgs = basicSetAccessedFunctionality(newAccessedFunctionality, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, newAccessedFunctionality, newAccessedFunctionality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTransition() {
		if (transition == null) {
			transition = new EObjectContainmentWithInverseEList(Transition.class, this, VoicePackage.DIALOG__TRANSITION, VoicePackage.TRANSITION__OWNER);
		}
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getMessage() {
		if (message == null) {
			message = new EObjectContainmentEList(Message.class, this, VoicePackage.DIALOG__MESSAGE);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConcept() {
		if (concept == null) {
			concept = new EObjectContainmentEList(Concept.class, this, VoicePackage.DIALOG__CONCEPT);
		}
		return concept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getMessagePart() {
		if (messagePart == null) {
			messagePart = new EObjectContainmentEList(MessagePart.class, this, VoicePackage.DIALOG__MESSAGE_PART);
		}
		return messagePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getExternalEvent() {
		if (externalEvent == null) {
			externalEvent = new EObjectContainmentEList(ExternalEvent.class, this, VoicePackage.DIALOG__EXTERNAL_EVENT);
		}
		return externalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getCondition() {
		if (condition == null) {
			condition = new EObjectContainmentEList(MessageElementCondition.class, this, VoicePackage.DIALOG__CONDITION);
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getVariable() {
		if (variable == null) {
			variable = new EObjectContainmentEList(Variable.class, this, VoicePackage.DIALOG__VARIABLE);
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSubDialog() {
		if (subDialog == null) {
			subDialog = new EObjectContainmentEList(Dialog.class, this, VoicePackage.DIALOG__SUB_DIALOG);
		}
		return subDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGlobalVariable() {
		if (globalVariable == null) {
			globalVariable = new EObjectContainmentEList(Variable.class, this, VoicePackage.DIALOG__GLOBAL_VARIABLE);
		}
		return globalVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getReferencedVariable() {
		if (referencedVariable == null) {
			referencedVariable = new EObjectContainmentEList(Variable.class, this, VoicePackage.DIALOG__REFERENCED_VARIABLE);
		}
		return referencedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getReferencedMessage() {
		if (referencedMessage == null) {
			referencedMessage = new EObjectContainmentEList(Message.class, this, VoicePackage.DIALOG__REFERENCED_MESSAGE);
		}
		return referencedMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getReferencedInputEvent() {
		if (referencedInputEvent == null) {
			referencedInputEvent = new EObjectContainmentEList(InputEvent.class, this, VoicePackage.DIALOG__REFERENCED_INPUT_EVENT);
		}
		return referencedInputEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOperation() {
		if (operation == null) {
			operation = new EObjectContainmentEList(Operation.class, this, VoicePackage.DIALOG__OPERATION);
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOwnedGrammar() {
		if (ownedGrammar == null) {
			ownedGrammar = new EObjectContainmentWithInverseEList(Grammar.class, this, VoicePackage.DIALOG__OWNED_GRAMMAR, VoicePackage.GRAMMAR__DIALOG);
		}
		return ownedGrammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				return ((InternalEList)getNode()).basicAdd(otherEnd, msgs);
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				if (accessedFunctionality != null)
					msgs = ((InternalEObject)accessedFunctionality).eInverseRemove(this, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, Functionality.class, msgs);
				return basicSetAccessedFunctionality((Functionality)otherEnd, msgs);
			case VoicePackage.DIALOG__TRANSITION:
				return ((InternalEList)getTransition()).basicAdd(otherEnd, msgs);
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				return ((InternalEList)getNode()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				return basicSetAccessedFunctionality(null, msgs);
			case VoicePackage.DIALOG__TRANSITION:
				return ((InternalEList)getTransition()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__MESSAGE:
				return ((InternalEList)getMessage()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__CONCEPT:
				return ((InternalEList)getConcept()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__MESSAGE_PART:
				return ((InternalEList)getMessagePart()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
				return ((InternalEList)getExternalEvent()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__CONDITION:
				return ((InternalEList)getCondition()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__VARIABLE:
				return ((InternalEList)getVariable()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__SUB_DIALOG:
				return ((InternalEList)getSubDialog()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
				return ((InternalEList)getGlobalVariable()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
				return ((InternalEList)getReferencedVariable()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
				return ((InternalEList)getReferencedMessage()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
				return ((InternalEList)getReferencedInputEvent()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__OPERATION:
				return ((InternalEList)getOperation()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				return ((InternalEList)getOwnedGrammar()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				return getNode();
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				if (resolve) return getAccessedFunctionality();
				return basicGetAccessedFunctionality();
			case VoicePackage.DIALOG__TRANSITION:
				return getTransition();
			case VoicePackage.DIALOG__MESSAGE:
				return getMessage();
			case VoicePackage.DIALOG__CONCEPT:
				return getConcept();
			case VoicePackage.DIALOG__MESSAGE_PART:
				return getMessagePart();
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
				return getExternalEvent();
			case VoicePackage.DIALOG__CONDITION:
				return getCondition();
			case VoicePackage.DIALOG__VARIABLE:
				return getVariable();
			case VoicePackage.DIALOG__SUB_DIALOG:
				return getSubDialog();
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
				return getGlobalVariable();
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
				return getReferencedVariable();
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
				return getReferencedMessage();
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
				return getReferencedInputEvent();
			case VoicePackage.DIALOG__OPERATION:
				return getOperation();
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				return getOwnedGrammar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				getNode().clear();
				getNode().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				setAccessedFunctionality((Functionality)newValue);
				return;
			case VoicePackage.DIALOG__TRANSITION:
				getTransition().clear();
				getTransition().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__CONCEPT:
				getConcept().clear();
				getConcept().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__MESSAGE_PART:
				getMessagePart().clear();
				getMessagePart().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
				getExternalEvent().clear();
				getExternalEvent().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__VARIABLE:
				getVariable().clear();
				getVariable().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__SUB_DIALOG:
				getSubDialog().clear();
				getSubDialog().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
				getGlobalVariable().clear();
				getGlobalVariable().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
				getReferencedVariable().clear();
				getReferencedVariable().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
				getReferencedMessage().clear();
				getReferencedMessage().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
				getReferencedInputEvent().clear();
				getReferencedInputEvent().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__OPERATION:
				getOperation().clear();
				getOperation().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				getOwnedGrammar().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				getNode().clear();
				return;
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				setAccessedFunctionality((Functionality)null);
				return;
			case VoicePackage.DIALOG__TRANSITION:
				getTransition().clear();
				return;
			case VoicePackage.DIALOG__MESSAGE:
				getMessage().clear();
				return;
			case VoicePackage.DIALOG__CONCEPT:
				getConcept().clear();
				return;
			case VoicePackage.DIALOG__MESSAGE_PART:
				getMessagePart().clear();
				return;
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
				getExternalEvent().clear();
				return;
			case VoicePackage.DIALOG__CONDITION:
				getCondition().clear();
				return;
			case VoicePackage.DIALOG__VARIABLE:
				getVariable().clear();
				return;
			case VoicePackage.DIALOG__SUB_DIALOG:
				getSubDialog().clear();
				return;
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
				getGlobalVariable().clear();
				return;
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
				getReferencedVariable().clear();
				return;
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
				getReferencedMessage().clear();
				return;
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
				getReferencedInputEvent().clear();
				return;
			case VoicePackage.DIALOG__OPERATION:
				getOperation().clear();
				return;
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				getOwnedGrammar().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.DIALOG__NODE:
				return node != null && !node.isEmpty();
			case VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY:
				return accessedFunctionality != null;
			case VoicePackage.DIALOG__TRANSITION:
				return transition != null && !transition.isEmpty();
			case VoicePackage.DIALOG__MESSAGE:
				return message != null && !message.isEmpty();
			case VoicePackage.DIALOG__CONCEPT:
				return concept != null && !concept.isEmpty();
			case VoicePackage.DIALOG__MESSAGE_PART:
				return messagePart != null && !messagePart.isEmpty();
			case VoicePackage.DIALOG__EXTERNAL_EVENT:
				return externalEvent != null && !externalEvent.isEmpty();
			case VoicePackage.DIALOG__CONDITION:
				return condition != null && !condition.isEmpty();
			case VoicePackage.DIALOG__VARIABLE:
				return variable != null && !variable.isEmpty();
			case VoicePackage.DIALOG__SUB_DIALOG:
				return subDialog != null && !subDialog.isEmpty();
			case VoicePackage.DIALOG__GLOBAL_VARIABLE:
				return globalVariable != null && !globalVariable.isEmpty();
			case VoicePackage.DIALOG__REFERENCED_VARIABLE:
				return referencedVariable != null && !referencedVariable.isEmpty();
			case VoicePackage.DIALOG__REFERENCED_MESSAGE:
				return referencedMessage != null && !referencedMessage.isEmpty();
			case VoicePackage.DIALOG__REFERENCED_INPUT_EVENT:
				return referencedInputEvent != null && !referencedInputEvent.isEmpty();
			case VoicePackage.DIALOG__OPERATION:
				return operation != null && !operation.isEmpty();
			case VoicePackage.DIALOG__OWNED_GRAMMAR:
				return ownedGrammar != null && !ownedGrammar.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DialogImpl