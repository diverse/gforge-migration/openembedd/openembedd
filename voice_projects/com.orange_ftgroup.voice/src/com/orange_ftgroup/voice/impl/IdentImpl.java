/**
 * <copyright>
 * </copyright>
 *
 * $Id: IdentImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Ident;
import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ident</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.IdentImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.IdentImpl#getValueContainer <em>Value Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IdentImpl extends ValueExpressionImpl implements Ident {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValueContainer() <em>Value Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueContainer()
	 * @generated
	 * @ordered
	 */
	protected ValueContainer valueContainer = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.IDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.IDENT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueContainer getValueContainer() {
		if (valueContainer != null && valueContainer.eIsProxy()) {
			InternalEObject oldValueContainer = (InternalEObject)valueContainer;
			valueContainer = (ValueContainer)eResolveProxy(oldValueContainer);
			if (valueContainer != oldValueContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.IDENT__VALUE_CONTAINER, oldValueContainer, valueContainer));
			}
		}
		return valueContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueContainer basicGetValueContainer() {
		return valueContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueContainer(ValueContainer newValueContainer) {
		ValueContainer oldValueContainer = valueContainer;
		valueContainer = newValueContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.IDENT__VALUE_CONTAINER, oldValueContainer, valueContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.IDENT__VALUE:
				return getValue();
			case VoicePackage.IDENT__VALUE_CONTAINER:
				if (resolve) return getValueContainer();
				return basicGetValueContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.IDENT__VALUE:
				setValue((String)newValue);
				return;
			case VoicePackage.IDENT__VALUE_CONTAINER:
				setValueContainer((ValueContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.IDENT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case VoicePackage.IDENT__VALUE_CONTAINER:
				setValueContainer((ValueContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.IDENT__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case VoicePackage.IDENT__VALUE_CONTAINER:
				return valueContainer != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //IdentImpl