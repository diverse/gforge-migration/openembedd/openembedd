/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConditionalElementImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.ConditionalElement;
import com.orange_ftgroup.voice.MessageElement;
import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.ConditionalElementImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ConditionalElementImpl#getThenPart <em>Then Part</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.ConditionalElementImpl#getElsePart <em>Else Part</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConditionalElementImpl extends MessageElementImpl implements ConditionalElement {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected MessageElementCondition condition = null;

	/**
	 * The cached value of the '{@link #getThenPart() <em>Then Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenPart()
	 * @generated
	 * @ordered
	 */
	protected EList thenPart = null;

	/**
	 * The cached value of the '{@link #getElsePart() <em>Else Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElsePart()
	 * @generated
	 * @ordered
	 */
	protected EList elsePart = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionalElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.CONDITIONAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageElementCondition getCondition() {
		if (condition != null && condition.eIsProxy()) {
			InternalEObject oldCondition = (InternalEObject)condition;
			condition = (MessageElementCondition)eResolveProxy(oldCondition);
			if (condition != oldCondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.CONDITIONAL_ELEMENT__CONDITION, oldCondition, condition));
			}
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageElementCondition basicGetCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(MessageElementCondition newCondition) {
		MessageElementCondition oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.CONDITIONAL_ELEMENT__CONDITION, oldCondition, condition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getThenPart() {
		if (thenPart == null) {
			thenPart = new EObjectContainmentEList(MessageElement.class, this, VoicePackage.CONDITIONAL_ELEMENT__THEN_PART);
		}
		return thenPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getElsePart() {
		if (elsePart == null) {
			elsePart = new EObjectContainmentEList(MessageElement.class, this, VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART);
		}
		return elsePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.CONDITIONAL_ELEMENT__THEN_PART:
				return ((InternalEList)getThenPart()).basicRemove(otherEnd, msgs);
			case VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART:
				return ((InternalEList)getElsePart()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.CONDITIONAL_ELEMENT__CONDITION:
				if (resolve) return getCondition();
				return basicGetCondition();
			case VoicePackage.CONDITIONAL_ELEMENT__THEN_PART:
				return getThenPart();
			case VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART:
				return getElsePart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.CONDITIONAL_ELEMENT__CONDITION:
				setCondition((MessageElementCondition)newValue);
				return;
			case VoicePackage.CONDITIONAL_ELEMENT__THEN_PART:
				getThenPart().clear();
				getThenPart().addAll((Collection)newValue);
				return;
			case VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART:
				getElsePart().clear();
				getElsePart().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.CONDITIONAL_ELEMENT__CONDITION:
				setCondition((MessageElementCondition)null);
				return;
			case VoicePackage.CONDITIONAL_ELEMENT__THEN_PART:
				getThenPart().clear();
				return;
			case VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART:
				getElsePart().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.CONDITIONAL_ELEMENT__CONDITION:
				return condition != null;
			case VoicePackage.CONDITIONAL_ELEMENT__THEN_PART:
				return thenPart != null && !thenPart.isEmpty();
			case VoicePackage.CONDITIONAL_ELEMENT__ELSE_PART:
				return elsePart != null && !elsePart.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConditionalElementImpl