/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceFactoryImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.AbstractDTMF;
import com.orange_ftgroup.voice.ActionSequence;
import com.orange_ftgroup.voice.Actor;
import com.orange_ftgroup.voice.AnyDigit;
import com.orange_ftgroup.voice.AnyDtmf;
import com.orange_ftgroup.voice.AnyState;
import com.orange_ftgroup.voice.Assignment;
import com.orange_ftgroup.voice.BinaryExpression;
import com.orange_ftgroup.voice.BooleanValue;
import com.orange_ftgroup.voice.CallAction;
import com.orange_ftgroup.voice.CallExpression;
import com.orange_ftgroup.voice.CharacterValue;
import com.orange_ftgroup.voice.CharstringValue;
import com.orange_ftgroup.voice.ChoiceType;
import com.orange_ftgroup.voice.CollectionItem;
import com.orange_ftgroup.voice.CollectionLiteralExpression;
import com.orange_ftgroup.voice.CollectionRange;
import com.orange_ftgroup.voice.Concept;
import com.orange_ftgroup.voice.ConditionalElement;
import com.orange_ftgroup.voice.ConditionalExpression;
import com.orange_ftgroup.voice.Constraint;
import com.orange_ftgroup.voice.DTMF;
import com.orange_ftgroup.voice.DTMFKind;
import com.orange_ftgroup.voice.DTMFValue;
import com.orange_ftgroup.voice.DataType;
import com.orange_ftgroup.voice.DecisionNode;
import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.DiffusionType;
import com.orange_ftgroup.voice.DigitKind;
import com.orange_ftgroup.voice.DigitValue;
import com.orange_ftgroup.voice.DiversionNode;
import com.orange_ftgroup.voice.Entity;
import com.orange_ftgroup.voice.EnumValue;
import com.orange_ftgroup.voice.EnumerationItem;
import com.orange_ftgroup.voice.EnumerationType;
import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.ExternalEvent;
import com.orange_ftgroup.voice.FeatureExpression;
import com.orange_ftgroup.voice.FixPart;
import com.orange_ftgroup.voice.FloatValue;
import com.orange_ftgroup.voice.ForEachElement;
import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.HistoryState;
import com.orange_ftgroup.voice.Ident;
import com.orange_ftgroup.voice.IfThenElse;
import com.orange_ftgroup.voice.Inactivity;
import com.orange_ftgroup.voice.InformalExpression;
import com.orange_ftgroup.voice.InitialNode;
import com.orange_ftgroup.voice.IntegerValue;
import com.orange_ftgroup.voice.JunctionNode;
import com.orange_ftgroup.voice.ListExpression;
import com.orange_ftgroup.voice.ListState;
import com.orange_ftgroup.voice.ListType;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.MessageEnd;
import com.orange_ftgroup.voice.NewExpression;
import com.orange_ftgroup.voice.NextNode;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.Parameter;
import com.orange_ftgroup.voice.ParameterDirectionKind;
import com.orange_ftgroup.voice.ParenthesisExpression;
import com.orange_ftgroup.voice.Play;
import com.orange_ftgroup.voice.Property;
import com.orange_ftgroup.voice.PropertyExpression;
import com.orange_ftgroup.voice.Recording;
import com.orange_ftgroup.voice.Reject;
import com.orange_ftgroup.voice.ReturnAction;
import com.orange_ftgroup.voice.ReturnNode;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.SilencePart;
import com.orange_ftgroup.voice.StateMachine;
import com.orange_ftgroup.voice.StopNode;
import com.orange_ftgroup.voice.SubDialogState;
import com.orange_ftgroup.voice.SystemInput;
import com.orange_ftgroup.voice.Tag;
import com.orange_ftgroup.voice.Transition;
import com.orange_ftgroup.voice.Trigger;
import com.orange_ftgroup.voice.UnaryExpression;
import com.orange_ftgroup.voice.Uninterpreted;
import com.orange_ftgroup.voice.UseElement;
import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.Variable;
import com.orange_ftgroup.voice.VariablePart;
import com.orange_ftgroup.voice.VisibilityKind;
import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.VoicePackage;
import com.orange_ftgroup.voice.VoiceService;
import com.orange_ftgroup.voice.WaitState;
import com.orange_ftgroup.voice.While;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VoiceFactoryImpl extends EFactoryImpl implements VoiceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VoiceFactory init() {
		try {
			VoiceFactory theVoiceFactory = (VoiceFactory)EPackage.Registry.INSTANCE.getEFactory("http:///voice.ecore"); 
			if (theVoiceFactory != null) {
				return theVoiceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VoiceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoiceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VoicePackage.PARAMETER: return createParameter();
			case VoicePackage.OPERATION: return createOperation();
			case VoicePackage.CONSTRAINT: return createConstraint();
			case VoicePackage.PACKAGE: return createPackage();
			case VoicePackage.DATA_TYPE: return createDataType();
			case VoicePackage.TAG: return createTag();
			case VoicePackage.PROPERTY: return createProperty();
			case VoicePackage.CLASS: return createClass();
			case VoicePackage.BINARY_EXPRESSION: return createBinaryExpression();
			case VoicePackage.UNARY_EXPRESSION: return createUnaryExpression();
			case VoicePackage.CHARACTER_VALUE: return createCharacterValue();
			case VoicePackage.INTEGER_VALUE: return createIntegerValue();
			case VoicePackage.FLOAT_VALUE: return createFloatValue();
			case VoicePackage.BOOLEAN_VALUE: return createBooleanValue();
			case VoicePackage.CHARSTRING_VALUE: return createCharstringValue();
			case VoicePackage.IDENT: return createIdent();
			case VoicePackage.PARENTHESIS_EXPRESSION: return createParenthesisExpression();
			case VoicePackage.CONDITIONAL_EXPRESSION: return createConditionalExpression();
			case VoicePackage.LIST_EXPRESSION: return createListExpression();
			case VoicePackage.CALL_EXPRESSION: return createCallExpression();
			case VoicePackage.ENUMERATION_TYPE: return createEnumerationType();
			case VoicePackage.ENUMERATION_ITEM: return createEnumerationItem();
			case VoicePackage.CHOICE_TYPE: return createChoiceType();
			case VoicePackage.LIST_TYPE: return createListType();
			case VoicePackage.VARIABLE: return createVariable();
			case VoicePackage.INFORMAL_EXPRESSION: return createInformalExpression();
			case VoicePackage.ACTION_SEQUENCE: return createActionSequence();
			case VoicePackage.DIGIT_VALUE: return createDigitValue();
			case VoicePackage.DTMF_VALUE: return createDTMFValue();
			case VoicePackage.FEATURE_EXPRESSION: return createFeatureExpression();
			case VoicePackage.PROPERTY_EXPRESSION: return createPropertyExpression();
			case VoicePackage.COLLECTION_RANGE: return createCollectionRange();
			case VoicePackage.COLLECTION_LITERAL_EXPRESSION: return createCollectionLiteralExpression();
			case VoicePackage.COLLECTION_ITEM: return createCollectionItem();
			case VoicePackage.ENUM_VALUE: return createEnumValue();
			case VoicePackage.STATE_MACHINE: return createStateMachine();
			case VoicePackage.NEW_EXPRESSION: return createNewExpression();
			case VoicePackage.VALUE_CONTAINER: return createValueContainer();
			case VoicePackage.DIALOG: return createDialog();
			case VoicePackage.SUB_DIALOG_STATE: return createSubDialogState();
			case VoicePackage.TRANSITION: return createTransition();
			case VoicePackage.TRIGGER: return createTrigger();
			case VoicePackage.WAIT_STATE: return createWaitState();
			case VoicePackage.ANY_STATE: return createAnyState();
			case VoicePackage.LIST_STATE: return createListState();
			case VoicePackage.DECISION_NODE: return createDecisionNode();
			case VoicePackage.INITIAL_NODE: return createInitialNode();
			case VoicePackage.JUNCTION_NODE: return createJunctionNode();
			case VoicePackage.DIVERSION_NODE: return createDiversionNode();
			case VoicePackage.HISTORY_STATE: return createHistoryState();
			case VoicePackage.NEXT_NODE: return createNextNode();
			case VoicePackage.STOP_NODE: return createStopNode();
			case VoicePackage.RETURN_NODE: return createReturnNode();
			case VoicePackage.ASSIGNMENT: return createAssignment();
			case VoicePackage.PLAY: return createPlay();
			case VoicePackage.UNINTERPRETED: return createUninterpreted();
			case VoicePackage.IF_THEN_ELSE: return createIfThenElse();
			case VoicePackage.WHILE: return createWhile();
			case VoicePackage.RETURN_ACTION: return createReturnAction();
			case VoicePackage.CALL_ACTION: return createCallAction();
			case VoicePackage.DTMF: return createDTMF();
			case VoicePackage.REJECT: return createReject();
			case VoicePackage.INACTIVITY: return createInactivity();
			case VoicePackage.CONCEPT: return createConcept();
			case VoicePackage.EXTERNAL_EVENT: return createExternalEvent();
			case VoicePackage.ANY_DTMF: return createAnyDtmf();
			case VoicePackage.ANY_DIGIT: return createAnyDigit();
			case VoicePackage.RECORDING: return createRecording();
			case VoicePackage.ABSTRACT_DTMF: return createAbstractDTMF();
			case VoicePackage.MESSAGE_END: return createMessageEnd();
			case VoicePackage.SYSTEM_INPUT: return createSystemInput();
			case VoicePackage.MESSAGE: return createMessage();
			case VoicePackage.FIX_PART: return createFixPart();
			case VoicePackage.VARIABLE_PART: return createVariablePart();
			case VoicePackage.CONDITIONAL_ELEMENT: return createConditionalElement();
			case VoicePackage.SILENCE_PART: return createSilencePart();
			case VoicePackage.USE_ELEMENT: return createUseElement();
			case VoicePackage.MESSAGE_ELEMENT_CONDITION: return createMessageElementCondition();
			case VoicePackage.FOR_EACH_ELEMENT: return createForEachElement();
			case VoicePackage.GRAMMAR: return createGrammar();
			case VoicePackage.VOICE_SERVICE: return createVoiceService();
			case VoicePackage.SERVICE: return createService();
			case VoicePackage.FUNCTIONALITY: return createFunctionality();
			case VoicePackage.ACTOR: return createActor();
			case VoicePackage.ENTITY: return createEntity();
			case VoicePackage.ENVIRONMENT: return createEnvironment();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case VoicePackage.PARAMETER_DIRECTION_KIND:
				return createParameterDirectionKindFromString(eDataType, initialValue);
			case VoicePackage.VISIBILITY_KIND:
				return createVisibilityKindFromString(eDataType, initialValue);
			case VoicePackage.DTMF_KIND:
				return createDTMFKindFromString(eDataType, initialValue);
			case VoicePackage.DIGIT_KIND:
				return createDigitKindFromString(eDataType, initialValue);
			case VoicePackage.DIFFUSION_TYPE:
				return createDiffusionTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case VoicePackage.PARAMETER_DIRECTION_KIND:
				return convertParameterDirectionKindToString(eDataType, instanceValue);
			case VoicePackage.VISIBILITY_KIND:
				return convertVisibilityKindToString(eDataType, instanceValue);
			case VoicePackage.DTMF_KIND:
				return convertDTMFKindToString(eDataType, instanceValue);
			case VoicePackage.DIGIT_KIND:
				return convertDigitKindToString(eDataType, instanceValue);
			case VoicePackage.DIFFUSION_TYPE:
				return convertDiffusionTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint createConstraint() {
		ConstraintImpl constraint = new ConstraintImpl();
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public com.orange_ftgroup.voice.Package createPackage() {
		PackageImpl package_ = new PackageImpl();
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType createDataType() {
		DataTypeImpl dataType = new DataTypeImpl();
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tag createTag() {
		TagImpl tag = new TagImpl();
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public com.orange_ftgroup.voice.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryExpression createBinaryExpression() {
		BinaryExpressionImpl binaryExpression = new BinaryExpressionImpl();
		return binaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryExpression createUnaryExpression() {
		UnaryExpressionImpl unaryExpression = new UnaryExpressionImpl();
		return unaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CharacterValue createCharacterValue() {
		CharacterValueImpl characterValue = new CharacterValueImpl();
		return characterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatValue createFloatValue() {
		FloatValueImpl floatValue = new FloatValueImpl();
		return floatValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CharstringValue createCharstringValue() {
		CharstringValueImpl charstringValue = new CharstringValueImpl();
		return charstringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ident createIdent() {
		IdentImpl ident = new IdentImpl();
		return ident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParenthesisExpression createParenthesisExpression() {
		ParenthesisExpressionImpl parenthesisExpression = new ParenthesisExpressionImpl();
		return parenthesisExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalExpression createConditionalExpression() {
		ConditionalExpressionImpl conditionalExpression = new ConditionalExpressionImpl();
		return conditionalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListExpression createListExpression() {
		ListExpressionImpl listExpression = new ListExpressionImpl();
		return listExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallExpression createCallExpression() {
		CallExpressionImpl callExpression = new CallExpressionImpl();
		return callExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationType createEnumerationType() {
		EnumerationTypeImpl enumerationType = new EnumerationTypeImpl();
		return enumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationItem createEnumerationItem() {
		EnumerationItemImpl enumerationItem = new EnumerationItemImpl();
		return enumerationItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceType createChoiceType() {
		ChoiceTypeImpl choiceType = new ChoiceTypeImpl();
		return choiceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListType createListType() {
		ListTypeImpl listType = new ListTypeImpl();
		return listType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable createVariable() {
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformalExpression createInformalExpression() {
		InformalExpressionImpl informalExpression = new InformalExpressionImpl();
		return informalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSequence createActionSequence() {
		ActionSequenceImpl actionSequence = new ActionSequenceImpl();
		return actionSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitValue createDigitValue() {
		DigitValueImpl digitValue = new DigitValueImpl();
		return digitValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DTMFValue createDTMFValue() {
		DTMFValueImpl dtmfValue = new DTMFValueImpl();
		return dtmfValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureExpression createFeatureExpression() {
		FeatureExpressionImpl featureExpression = new FeatureExpressionImpl();
		return featureExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyExpression createPropertyExpression() {
		PropertyExpressionImpl propertyExpression = new PropertyExpressionImpl();
		return propertyExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionRange createCollectionRange() {
		CollectionRangeImpl collectionRange = new CollectionRangeImpl();
		return collectionRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionLiteralExpression createCollectionLiteralExpression() {
		CollectionLiteralExpressionImpl collectionLiteralExpression = new CollectionLiteralExpressionImpl();
		return collectionLiteralExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionItem createCollectionItem() {
		CollectionItemImpl collectionItem = new CollectionItemImpl();
		return collectionItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine createStateMachine() {
		StateMachineImpl stateMachine = new StateMachineImpl();
		return stateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewExpression createNewExpression() {
		NewExpressionImpl newExpression = new NewExpressionImpl();
		return newExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueContainer createValueContainer() {
		ValueContainerImpl valueContainer = new ValueContainerImpl();
		return valueContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog createDialog() {
		DialogImpl dialog = new DialogImpl();
		return dialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubDialogState createSubDialogState() {
		SubDialogStateImpl subDialogState = new SubDialogStateImpl();
		return subDialogState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trigger createTrigger() {
		TriggerImpl trigger = new TriggerImpl();
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitState createWaitState() {
		WaitStateImpl waitState = new WaitStateImpl();
		return waitState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyState createAnyState() {
		AnyStateImpl anyState = new AnyStateImpl();
		return anyState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListState createListState() {
		ListStateImpl listState = new ListStateImpl();
		return listState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionNode createDecisionNode() {
		DecisionNodeImpl decisionNode = new DecisionNodeImpl();
		return decisionNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialNode createInitialNode() {
		InitialNodeImpl initialNode = new InitialNodeImpl();
		return initialNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JunctionNode createJunctionNode() {
		JunctionNodeImpl junctionNode = new JunctionNodeImpl();
		return junctionNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiversionNode createDiversionNode() {
		DiversionNodeImpl diversionNode = new DiversionNodeImpl();
		return diversionNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryState createHistoryState() {
		HistoryStateImpl historyState = new HistoryStateImpl();
		return historyState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NextNode createNextNode() {
		NextNodeImpl nextNode = new NextNodeImpl();
		return nextNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StopNode createStopNode() {
		StopNodeImpl stopNode = new StopNodeImpl();
		return stopNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnNode createReturnNode() {
		ReturnNodeImpl returnNode = new ReturnNodeImpl();
		return returnNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment createAssignment() {
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Play createPlay() {
		PlayImpl play = new PlayImpl();
		return play;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uninterpreted createUninterpreted() {
		UninterpretedImpl uninterpreted = new UninterpretedImpl();
		return uninterpreted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfThenElse createIfThenElse() {
		IfThenElseImpl ifThenElse = new IfThenElseImpl();
		return ifThenElse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public While createWhile() {
		WhileImpl while_ = new WhileImpl();
		return while_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnAction createReturnAction() {
		ReturnActionImpl returnAction = new ReturnActionImpl();
		return returnAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction createCallAction() {
		CallActionImpl callAction = new CallActionImpl();
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DTMF createDTMF() {
		DTMFImpl dtmf = new DTMFImpl();
		return dtmf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reject createReject() {
		RejectImpl reject = new RejectImpl();
		return reject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Inactivity createInactivity() {
		InactivityImpl inactivity = new InactivityImpl();
		return inactivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Concept createConcept() {
		ConceptImpl concept = new ConceptImpl();
		return concept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalEvent createExternalEvent() {
		ExternalEventImpl externalEvent = new ExternalEventImpl();
		return externalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyDtmf createAnyDtmf() {
		AnyDtmfImpl anyDtmf = new AnyDtmfImpl();
		return anyDtmf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyDigit createAnyDigit() {
		AnyDigitImpl anyDigit = new AnyDigitImpl();
		return anyDigit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Recording createRecording() {
		RecordingImpl recording = new RecordingImpl();
		return recording;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractDTMF createAbstractDTMF() {
		AbstractDTMFImpl abstractDTMF = new AbstractDTMFImpl();
		return abstractDTMF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEnd createMessageEnd() {
		MessageEndImpl messageEnd = new MessageEndImpl();
		return messageEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInput createSystemInput() {
		SystemInputImpl systemInput = new SystemInputImpl();
		return systemInput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message createMessage() {
		MessageImpl message = new MessageImpl();
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPart createFixPart() {
		FixPartImpl fixPart = new FixPartImpl();
		return fixPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariablePart createVariablePart() {
		VariablePartImpl variablePart = new VariablePartImpl();
		return variablePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalElement createConditionalElement() {
		ConditionalElementImpl conditionalElement = new ConditionalElementImpl();
		return conditionalElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SilencePart createSilencePart() {
		SilencePartImpl silencePart = new SilencePartImpl();
		return silencePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseElement createUseElement() {
		UseElementImpl useElement = new UseElementImpl();
		return useElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageElementCondition createMessageElementCondition() {
		MessageElementConditionImpl messageElementCondition = new MessageElementConditionImpl();
		return messageElementCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachElement createForEachElement() {
		ForEachElementImpl forEachElement = new ForEachElementImpl();
		return forEachElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Grammar createGrammar() {
		GrammarImpl grammar = new GrammarImpl();
		return grammar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoiceService createVoiceService() {
		VoiceServiceImpl voiceService = new VoiceServiceImpl();
		return voiceService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service createService() {
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Functionality createFunctionality() {
		FunctionalityImpl functionality = new FunctionalityImpl();
		return functionality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Actor createActor() {
		ActorImpl actor = new ActorImpl();
		return actor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entity createEntity() {
		EntityImpl entity = new EntityImpl();
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment createEnvironment() {
		EnvironmentImpl environment = new EnvironmentImpl();
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterDirectionKind createParameterDirectionKindFromString(EDataType eDataType, String initialValue) {
		ParameterDirectionKind result = ParameterDirectionKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertParameterDirectionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityKind createVisibilityKindFromString(EDataType eDataType, String initialValue) {
		VisibilityKind result = VisibilityKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVisibilityKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DTMFKind createDTMFKindFromString(EDataType eDataType, String initialValue) {
		DTMFKind result = DTMFKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDTMFKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitKind createDigitKindFromString(EDataType eDataType, String initialValue) {
		DigitKind result = DigitKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDigitKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiffusionType createDiffusionTypeFromString(EDataType eDataType, String initialValue) {
		DiffusionType result = DiffusionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDiffusionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoicePackage getVoicePackage() {
		return (VoicePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static VoicePackage getPackage() {
		return VoicePackage.eINSTANCE;
	}

} //VoiceFactoryImpl
