/**
 * <copyright>
 * </copyright>
 *
 * $Id: EnumerationItemImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.EnumerationItem;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EnumerationItemImpl extends NamedElementImpl implements EnumerationItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.ENUMERATION_ITEM;
	}

} //EnumerationItemImpl