/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogNodeImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.DialogNode;
import com.orange_ftgroup.voice.Transition;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dialog Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogNodeImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogNodeImpl#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.DialogNodeImpl#getIncoming <em>Incoming</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class DialogNodeImpl extends NamedElementImpl implements DialogNode {
	/**
	 * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoing()
	 * @generated
	 * @ordered
	 */
	protected EList outgoing = null;

	/**
	 * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncoming()
	 * @generated
	 * @ordered
	 */
	protected EList incoming = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DialogNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.DIALOG_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog getOwner() {
		if (eContainerFeatureID != VoicePackage.DIALOG_NODE__OWNER) return null;
		return (Dialog)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(Dialog newOwner, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOwner, VoicePackage.DIALOG_NODE__OWNER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(Dialog newOwner) {
		if (newOwner != eInternalContainer() || (eContainerFeatureID != VoicePackage.DIALOG_NODE__OWNER && newOwner != null)) {
			if (EcoreUtil.isAncestor(this, newOwner))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, VoicePackage.DIALOG__NODE, Dialog.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.DIALOG_NODE__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOutgoing() {
		if (outgoing == null) {
			outgoing = new EObjectWithInverseResolvingEList(Transition.class, this, VoicePackage.DIALOG_NODE__OUTGOING, VoicePackage.TRANSITION__ORIGIN);
		}
		return outgoing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIncoming() {
		if (incoming == null) {
			incoming = new EObjectWithInverseResolvingEList(Transition.class, this, VoicePackage.DIALOG_NODE__INCOMING, VoicePackage.TRANSITION__TARGET);
		}
		return incoming;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOwner((Dialog)otherEnd, msgs);
			case VoicePackage.DIALOG_NODE__OUTGOING:
				return ((InternalEList)getOutgoing()).basicAdd(otherEnd, msgs);
			case VoicePackage.DIALOG_NODE__INCOMING:
				return ((InternalEList)getIncoming()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				return basicSetOwner(null, msgs);
			case VoicePackage.DIALOG_NODE__OUTGOING:
				return ((InternalEList)getOutgoing()).basicRemove(otherEnd, msgs);
			case VoicePackage.DIALOG_NODE__INCOMING:
				return ((InternalEList)getIncoming()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				return eInternalContainer().eInverseRemove(this, VoicePackage.DIALOG__NODE, Dialog.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				return getOwner();
			case VoicePackage.DIALOG_NODE__OUTGOING:
				return getOutgoing();
			case VoicePackage.DIALOG_NODE__INCOMING:
				return getIncoming();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				setOwner((Dialog)newValue);
				return;
			case VoicePackage.DIALOG_NODE__OUTGOING:
				getOutgoing().clear();
				getOutgoing().addAll((Collection)newValue);
				return;
			case VoicePackage.DIALOG_NODE__INCOMING:
				getIncoming().clear();
				getIncoming().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				setOwner((Dialog)null);
				return;
			case VoicePackage.DIALOG_NODE__OUTGOING:
				getOutgoing().clear();
				return;
			case VoicePackage.DIALOG_NODE__INCOMING:
				getIncoming().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.DIALOG_NODE__OWNER:
				return getOwner() != null;
			case VoicePackage.DIALOG_NODE__OUTGOING:
				return outgoing != null && !outgoing.isEmpty();
			case VoicePackage.DIALOG_NODE__INCOMING:
				return incoming != null && !incoming.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DialogNodeImpl