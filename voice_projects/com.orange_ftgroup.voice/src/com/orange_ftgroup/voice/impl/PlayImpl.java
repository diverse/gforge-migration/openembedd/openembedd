/**
 * <copyright>
 * </copyright>
 *
 * $Id: PlayImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Expression;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.Play;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Play</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.PlayImpl#isInterruptible <em>Interruptible</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.PlayImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.PlayImpl#getMessageArgument <em>Message Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PlayImpl extends ActionImpl implements Play {
	/**
	 * The default value of the '{@link #isInterruptible() <em>Interruptible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterruptible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INTERRUPTIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInterruptible() <em>Interruptible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterruptible()
	 * @generated
	 * @ordered
	 */
	protected boolean interruptible = INTERRUPTIBLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected Message message = null;

	/**
	 * The cached value of the '{@link #getMessageArgument() <em>Message Argument</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageArgument()
	 * @generated
	 * @ordered
	 */
	protected EList messageArgument = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlayImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.PLAY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInterruptible() {
		return interruptible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptible(boolean newInterruptible) {
		boolean oldInterruptible = interruptible;
		interruptible = newInterruptible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.PLAY__INTERRUPTIBLE, oldInterruptible, interruptible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getMessage() {
		if (message != null && message.eIsProxy()) {
			InternalEObject oldMessage = (InternalEObject)message;
			message = (Message)eResolveProxy(oldMessage);
			if (message != oldMessage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.PLAY__MESSAGE, oldMessage, message));
			}
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(Message newMessage) {
		Message oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.PLAY__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getMessageArgument() {
		if (messageArgument == null) {
			messageArgument = new EObjectContainmentEList(Expression.class, this, VoicePackage.PLAY__MESSAGE_ARGUMENT);
		}
		return messageArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.PLAY__MESSAGE_ARGUMENT:
				return ((InternalEList)getMessageArgument()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.PLAY__INTERRUPTIBLE:
				return isInterruptible() ? Boolean.TRUE : Boolean.FALSE;
			case VoicePackage.PLAY__MESSAGE:
				if (resolve) return getMessage();
				return basicGetMessage();
			case VoicePackage.PLAY__MESSAGE_ARGUMENT:
				return getMessageArgument();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.PLAY__INTERRUPTIBLE:
				setInterruptible(((Boolean)newValue).booleanValue());
				return;
			case VoicePackage.PLAY__MESSAGE:
				setMessage((Message)newValue);
				return;
			case VoicePackage.PLAY__MESSAGE_ARGUMENT:
				getMessageArgument().clear();
				getMessageArgument().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.PLAY__INTERRUPTIBLE:
				setInterruptible(INTERRUPTIBLE_EDEFAULT);
				return;
			case VoicePackage.PLAY__MESSAGE:
				setMessage((Message)null);
				return;
			case VoicePackage.PLAY__MESSAGE_ARGUMENT:
				getMessageArgument().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.PLAY__INTERRUPTIBLE:
				return interruptible != INTERRUPTIBLE_EDEFAULT;
			case VoicePackage.PLAY__MESSAGE:
				return message != null;
			case VoicePackage.PLAY__MESSAGE_ARGUMENT:
				return messageArgument != null && !messageArgument.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (interruptible: ");
		result.append(interruptible);
		result.append(')');
		return result.toString();
	}

} //PlayImpl