/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceServiceImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.VoicePackage;
import com.orange_ftgroup.voice.VoiceService;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.VoiceServiceImpl#getMainDialog <em>Main Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.VoiceServiceImpl#getExtraDialog <em>Extra Dialog</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VoiceServiceImpl extends ServiceImpl implements VoiceService {
	/**
	 * The cached value of the '{@link #getMainDialog() <em>Main Dialog</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainDialog()
	 * @generated
	 * @ordered
	 */
	protected Dialog mainDialog = null;

	/**
	 * The cached value of the '{@link #getExtraDialog() <em>Extra Dialog</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraDialog()
	 * @generated
	 * @ordered
	 */
	protected EList extraDialog = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VoiceServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.VOICE_SERVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog getMainDialog() {
		return mainDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainDialog(Dialog newMainDialog, NotificationChain msgs) {
		Dialog oldMainDialog = mainDialog;
		mainDialog = newMainDialog;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.VOICE_SERVICE__MAIN_DIALOG, oldMainDialog, newMainDialog);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainDialog(Dialog newMainDialog) {
		if (newMainDialog != mainDialog) {
			NotificationChain msgs = null;
			if (mainDialog != null)
				msgs = ((InternalEObject)mainDialog).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VOICE_SERVICE__MAIN_DIALOG, null, msgs);
			if (newMainDialog != null)
				msgs = ((InternalEObject)newMainDialog).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.VOICE_SERVICE__MAIN_DIALOG, null, msgs);
			msgs = basicSetMainDialog(newMainDialog, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.VOICE_SERVICE__MAIN_DIALOG, newMainDialog, newMainDialog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getExtraDialog() {
		if (extraDialog == null) {
			extraDialog = new EObjectContainmentEList(Dialog.class, this, VoicePackage.VOICE_SERVICE__EXTRA_DIALOG);
		}
		return extraDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.VOICE_SERVICE__MAIN_DIALOG:
				return basicSetMainDialog(null, msgs);
			case VoicePackage.VOICE_SERVICE__EXTRA_DIALOG:
				return ((InternalEList)getExtraDialog()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.VOICE_SERVICE__MAIN_DIALOG:
				return getMainDialog();
			case VoicePackage.VOICE_SERVICE__EXTRA_DIALOG:
				return getExtraDialog();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.VOICE_SERVICE__MAIN_DIALOG:
				setMainDialog((Dialog)newValue);
				return;
			case VoicePackage.VOICE_SERVICE__EXTRA_DIALOG:
				getExtraDialog().clear();
				getExtraDialog().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.VOICE_SERVICE__MAIN_DIALOG:
				setMainDialog((Dialog)null);
				return;
			case VoicePackage.VOICE_SERVICE__EXTRA_DIALOG:
				getExtraDialog().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.VOICE_SERVICE__MAIN_DIALOG:
				return mainDialog != null;
			case VoicePackage.VOICE_SERVICE__EXTRA_DIALOG:
				return extraDialog != null && !extraDialog.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VoiceServiceImpl