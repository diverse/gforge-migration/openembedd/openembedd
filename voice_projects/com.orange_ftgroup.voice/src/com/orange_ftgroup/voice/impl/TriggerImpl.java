/**
 * <copyright>
 * </copyright>
 *
 * $Id: TriggerImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.InputEvent;
import com.orange_ftgroup.voice.Trigger;
import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.TriggerImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.TriggerImpl#getArgHandle <em>Arg Handle</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TriggerImpl extends ModelElementImpl implements Trigger {
	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected InputEvent event = null;

	/**
	 * The cached value of the '{@link #getArgHandle() <em>Arg Handle</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgHandle()
	 * @generated
	 * @ordered
	 */
	protected EList argHandle = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.TRIGGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputEvent getEvent() {
		if (event != null && event.eIsProxy()) {
			InternalEObject oldEvent = (InternalEObject)event;
			event = (InputEvent)eResolveProxy(oldEvent);
			if (event != oldEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.TRIGGER__EVENT, oldEvent, event));
			}
		}
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputEvent basicGetEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(InputEvent newEvent) {
		InputEvent oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.TRIGGER__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getArgHandle() {
		if (argHandle == null) {
			argHandle = new EObjectResolvingEList(ValueContainer.class, this, VoicePackage.TRIGGER__ARG_HANDLE);
		}
		return argHandle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.TRIGGER__EVENT:
				if (resolve) return getEvent();
				return basicGetEvent();
			case VoicePackage.TRIGGER__ARG_HANDLE:
				return getArgHandle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.TRIGGER__EVENT:
				setEvent((InputEvent)newValue);
				return;
			case VoicePackage.TRIGGER__ARG_HANDLE:
				getArgHandle().clear();
				getArgHandle().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.TRIGGER__EVENT:
				setEvent((InputEvent)null);
				return;
			case VoicePackage.TRIGGER__ARG_HANDLE:
				getArgHandle().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.TRIGGER__EVENT:
				return event != null;
			case VoicePackage.TRIGGER__ARG_HANDLE:
				return argHandle != null && !argHandle.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TriggerImpl