/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallActionImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.CallAction;
import com.orange_ftgroup.voice.CallExpression;
import com.orange_ftgroup.voice.VoicePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.CallActionImpl#getCallExpression <em>Call Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallActionImpl extends ActionImpl implements CallAction {
	/**
	 * The cached value of the '{@link #getCallExpression() <em>Call Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallExpression()
	 * @generated
	 * @ordered
	 */
	protected CallExpression callExpression = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.CALL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallExpression getCallExpression() {
		return callExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallExpression(CallExpression newCallExpression, NotificationChain msgs) {
		CallExpression oldCallExpression = callExpression;
		callExpression = newCallExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.CALL_ACTION__CALL_EXPRESSION, oldCallExpression, newCallExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallExpression(CallExpression newCallExpression) {
		if (newCallExpression != callExpression) {
			NotificationChain msgs = null;
			if (callExpression != null)
				msgs = ((InternalEObject)callExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VoicePackage.CALL_ACTION__CALL_EXPRESSION, null, msgs);
			if (newCallExpression != null)
				msgs = ((InternalEObject)newCallExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VoicePackage.CALL_ACTION__CALL_EXPRESSION, null, msgs);
			msgs = basicSetCallExpression(newCallExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.CALL_ACTION__CALL_EXPRESSION, newCallExpression, newCallExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.CALL_ACTION__CALL_EXPRESSION:
				return basicSetCallExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.CALL_ACTION__CALL_EXPRESSION:
				return getCallExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.CALL_ACTION__CALL_EXPRESSION:
				setCallExpression((CallExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.CALL_ACTION__CALL_EXPRESSION:
				setCallExpression((CallExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.CALL_ACTION__CALL_EXPRESSION:
				return callExpression != null;
		}
		return super.eIsSet(featureID);
	}

} //CallActionImpl