/**
 * <copyright>
 * </copyright>
 *
 * $Id: FunctionalityImpl.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice.impl;

import com.orange_ftgroup.voice.Actor;
import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.VoicePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functionality</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.impl.FunctionalityImpl#getAccessDialog <em>Access Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.impl.FunctionalityImpl#getParticipant <em>Participant</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FunctionalityImpl extends PackageableElementImpl implements Functionality {
	/**
	 * The cached value of the '{@link #getAccessDialog() <em>Access Dialog</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessDialog()
	 * @generated
	 * @ordered
	 */
	protected Dialog accessDialog = null;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected EList participant = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return VoicePackage.Literals.FUNCTIONALITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog getAccessDialog() {
		if (accessDialog != null && accessDialog.eIsProxy()) {
			InternalEObject oldAccessDialog = (InternalEObject)accessDialog;
			accessDialog = (Dialog)eResolveProxy(oldAccessDialog);
			if (accessDialog != oldAccessDialog) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, oldAccessDialog, accessDialog));
			}
		}
		return accessDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dialog basicGetAccessDialog() {
		return accessDialog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessDialog(Dialog newAccessDialog, NotificationChain msgs) {
		Dialog oldAccessDialog = accessDialog;
		accessDialog = newAccessDialog;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, oldAccessDialog, newAccessDialog);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessDialog(Dialog newAccessDialog) {
		if (newAccessDialog != accessDialog) {
			NotificationChain msgs = null;
			if (accessDialog != null)
				msgs = ((InternalEObject)accessDialog).eInverseRemove(this, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, Dialog.class, msgs);
			if (newAccessDialog != null)
				msgs = ((InternalEObject)newAccessDialog).eInverseAdd(this, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, Dialog.class, msgs);
			msgs = basicSetAccessDialog(newAccessDialog, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VoicePackage.FUNCTIONALITY__ACCESS_DIALOG, newAccessDialog, newAccessDialog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getParticipant() {
		if (participant == null) {
			participant = new EObjectWithInverseResolvingEList.ManyInverse(Actor.class, this, VoicePackage.FUNCTIONALITY__PARTICIPANT, VoicePackage.ACTOR__FUNCTIONALITY);
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				if (accessDialog != null)
					msgs = ((InternalEObject)accessDialog).eInverseRemove(this, VoicePackage.DIALOG__ACCESSED_FUNCTIONALITY, Dialog.class, msgs);
				return basicSetAccessDialog((Dialog)otherEnd, msgs);
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				return ((InternalEList)getParticipant()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				return basicSetAccessDialog(null, msgs);
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				return ((InternalEList)getParticipant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				if (resolve) return getAccessDialog();
				return basicGetAccessDialog();
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				return getParticipant();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				setAccessDialog((Dialog)newValue);
				return;
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				getParticipant().clear();
				getParticipant().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				setAccessDialog((Dialog)null);
				return;
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				getParticipant().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VoicePackage.FUNCTIONALITY__ACCESS_DIALOG:
				return accessDialog != null;
			case VoicePackage.FUNCTIONALITY__PARTICIPANT:
				return participant != null && !participant.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FunctionalityImpl