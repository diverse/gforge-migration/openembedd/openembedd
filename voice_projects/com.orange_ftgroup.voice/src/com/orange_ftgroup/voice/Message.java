/**
 * <copyright>
 * </copyright>
 *
 * $Id: Message.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Message#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Message#getBody <em>Body</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Message#getMessageElement <em>Message Element</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Message#getActionSpecification <em>Action Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends Signature {
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.VisibilityKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @see #setVisibility(VisibilityKind)
	 * @see com.orange_ftgroup.voice.VoicePackage#getMessage_Visibility()
	 * @model
	 * @generated
	 */
	VisibilityKind getVisibility();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Message#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(VisibilityKind value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' attribute.
	 * @see #setBody(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getMessage_Body()
	 * @model
	 * @generated
	 */
	String getBody();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Message#getBody <em>Body</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' attribute.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(String value);

	/**
	 * Returns the value of the '<em><b>Message Element</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessageElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Element</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getMessage_MessageElement()
	 * @model type="com.orange_ftgroup.voice.MessageElement" containment="true"
	 * @generated
	 */
	EList getMessageElement();

	/**
	 * Returns the value of the '<em><b>Action Specification</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.ActionSequence#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Specification</em>' containment reference.
	 * @see #setActionSpecification(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getMessage_ActionSpecification()
	 * @see com.orange_ftgroup.voice.ActionSequence#getMessage
	 * @model opposite="Message" containment="true"
	 * @generated
	 */
	ActionSequence getActionSpecification();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Message#getActionSpecification <em>Action Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Specification</em>' containment reference.
	 * @see #getActionSpecification()
	 * @generated
	 */
	void setActionSpecification(ActionSequence value);

} // Message