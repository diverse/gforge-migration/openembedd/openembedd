/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateMachine.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.StateMachine#getNode <em>Node</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.StateMachine#getTransition <em>Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getStateMachine()
 * @model
 * @generated
 */
public interface StateMachine extends Behavior {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.DialogNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getStateMachine_Node()
	 * @model type="com.orange_ftgroup.voice.DialogNode" containment="true"
	 * @generated
	 */
	EList getNode();

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getStateMachine_Transition()
	 * @model type="com.orange_ftgroup.voice.Transition" containment="true"
	 * @generated
	 */
	EList getTransition();

} // StateMachine