/**
 * <copyright>
 * </copyright>
 *
 * $Id: IfThenElse.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Then Else</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.IfThenElse#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.IfThenElse#getThenPart <em>Then Part</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.IfThenElse#getElsePart <em>Else Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getIfThenElse()
 * @model
 * @generated
 */
public interface IfThenElse extends Action {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getIfThenElse_Condition()
	 * @model containment="true"
	 * @generated
	 */
	Expression getCondition();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.IfThenElse#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Expression value);

	/**
	 * Returns the value of the '<em><b>Then Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Part</em>' containment reference.
	 * @see #setThenPart(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getIfThenElse_ThenPart()
	 * @model containment="true"
	 * @generated
	 */
	ActionSequence getThenPart();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.IfThenElse#getThenPart <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Part</em>' containment reference.
	 * @see #getThenPart()
	 * @generated
	 */
	void setThenPart(ActionSequence value);

	/**
	 * Returns the value of the '<em><b>Else Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Part</em>' containment reference.
	 * @see #setElsePart(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getIfThenElse_ElsePart()
	 * @model containment="true"
	 * @generated
	 */
	ActionSequence getElsePart();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.IfThenElse#getElsePart <em>Else Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Part</em>' containment reference.
	 * @see #getElsePart()
	 * @generated
	 */
	void setElsePart(ActionSequence value);

} // IfThenElse