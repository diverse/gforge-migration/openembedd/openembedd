/**
 * <copyright>
 * </copyright>
 *
 * $Id: Variable.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Variable#isIsShared <em>Is Shared</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends TypedElement, ValueContainer {
	/**
	 * Returns the value of the '<em><b>Is Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Shared</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Shared</em>' attribute.
	 * @see #setIsShared(boolean)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVariable_IsShared()
	 * @model
	 * @generated
	 */
	boolean isIsShared();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Variable#isIsShared <em>Is Shared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Shared</em>' attribute.
	 * @see #isIsShared()
	 * @generated
	 */
	void setIsShared(boolean value);

} // Variable