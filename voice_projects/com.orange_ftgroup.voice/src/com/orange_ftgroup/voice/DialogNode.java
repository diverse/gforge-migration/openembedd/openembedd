/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogNode.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dialog Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.DialogNode#getOwner <em>Owner</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.DialogNode#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.DialogNode#getIncoming <em>Incoming</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDialogNode()
 * @model abstract="true"
 * @generated
 */
public interface DialogNode extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Dialog#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialogNode_Owner()
	 * @see com.orange_ftgroup.voice.Dialog#getNode
	 * @model opposite="node"
	 * @generated
	 */
	Dialog getOwner();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.DialogNode#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Dialog value);

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Transition}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Transition#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialogNode_Outgoing()
	 * @see com.orange_ftgroup.voice.Transition#getOrigin
	 * @model type="com.orange_ftgroup.voice.Transition" opposite="origin"
	 * @generated
	 */
	EList getOutgoing();

	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Transition}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialogNode_Incoming()
	 * @see com.orange_ftgroup.voice.Transition#getTarget
	 * @model type="com.orange_ftgroup.voice.Transition" opposite="target"
	 * @generated
	 */
	EList getIncoming();

} // DialogNode