/**
 * <copyright>
 * </copyright>
 *
 * $Id: SilencePart.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Silence Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.SilencePart#getDuration <em>Duration</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getSilencePart()
 * @model
 * @generated
 */
public interface SilencePart extends MessagePart {
	/**
	 * Returns the value of the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' containment reference.
	 * @see #setDuration(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getSilencePart_Duration()
	 * @model containment="true"
	 * @generated
	 */
	Expression getDuration();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.SilencePart#getDuration <em>Duration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' containment reference.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Expression value);

} // SilencePart