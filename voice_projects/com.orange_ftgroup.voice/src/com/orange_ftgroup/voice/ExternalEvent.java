/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExternalEvent.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getExternalEvent()
 * @model
 * @generated
 */
public interface ExternalEvent extends InputEvent {
} // ExternalEvent