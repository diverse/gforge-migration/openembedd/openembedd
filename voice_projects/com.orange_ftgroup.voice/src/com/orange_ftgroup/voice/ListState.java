/**
 * <copyright>
 * </copyright>
 *
 * $Id: ListState.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ListState#isExcluded <em>Excluded</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ListState#getState <em>State</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getListState()
 * @model
 * @generated
 */
public interface ListState extends DialogState {
	/**
	 * Returns the value of the '<em><b>Excluded</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Excluded</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Excluded</em>' attribute.
	 * @see #setExcluded(boolean)
	 * @see com.orange_ftgroup.voice.VoicePackage#getListState_Excluded()
	 * @model default="false"
	 * @generated
	 */
	boolean isExcluded();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ListState#isExcluded <em>Excluded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Excluded</em>' attribute.
	 * @see #isExcluded()
	 * @generated
	 */
	void setExcluded(boolean value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.DialogState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getListState_State()
	 * @model type="com.orange_ftgroup.voice.DialogState"
	 * @generated
	 */
	EList getState();

} // ListState