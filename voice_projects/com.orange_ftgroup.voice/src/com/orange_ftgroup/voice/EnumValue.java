/**
 * <copyright>
 * </copyright>
 *
 * $Id: EnumValue.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.EnumValue#getValue <em>Value</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.EnumValue#getLiteralDefinition <em>Literal Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getEnumValue()
 * @model
 * @generated
 */
public interface EnumValue extends ValueExpression {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnumValue_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.EnumValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Literal Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal Definition</em>' reference.
	 * @see #setLiteralDefinition(EnumerationItem)
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnumValue_LiteralDefinition()
	 * @model
	 * @generated
	 */
	EnumerationItem getLiteralDefinition();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.EnumValue#getLiteralDefinition <em>Literal Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal Definition</em>' reference.
	 * @see #getLiteralDefinition()
	 * @generated
	 */
	void setLiteralDefinition(EnumerationItem value);

} // EnumValue