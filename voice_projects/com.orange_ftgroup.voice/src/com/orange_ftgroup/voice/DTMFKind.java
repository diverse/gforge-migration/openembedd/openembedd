/**
 * <copyright>
 * </copyright>
 *
 * $Id: DTMFKind.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DTMF Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoicePackage#getDTMFKind()
 * @model
 * @generated
 */
public final class DTMFKind extends AbstractEnumerator {
	/**
	 * The '<em><b>Zero</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Zero</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZERO_LITERAL
	 * @model name="zero"
	 * @generated
	 * @ordered
	 */
	public static final int ZERO = 0;

	/**
	 * The '<em><b>One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ONE_LITERAL
	 * @model name="one"
	 * @generated
	 * @ordered
	 */
	public static final int ONE = 1;

	/**
	 * The '<em><b>Two</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Two</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TWO_LITERAL
	 * @model name="two"
	 * @generated
	 * @ordered
	 */
	public static final int TWO = 2;

	/**
	 * The '<em><b>Three</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Three</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THREE_LITERAL
	 * @model name="three"
	 * @generated
	 * @ordered
	 */
	public static final int THREE = 3;

	/**
	 * The '<em><b>For</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>For</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FOR_LITERAL
	 * @model name="for"
	 * @generated
	 * @ordered
	 */
	public static final int FOR = 4;

	/**
	 * The '<em><b>Five</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Five</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIVE_LITERAL
	 * @model name="five"
	 * @generated
	 * @ordered
	 */
	public static final int FIVE = 5;

	/**
	 * The '<em><b>Six</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Six</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIX_LITERAL
	 * @model name="six"
	 * @generated
	 * @ordered
	 */
	public static final int SIX = 6;

	/**
	 * The '<em><b>Seven</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Seven</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SEVEN_LITERAL
	 * @model name="seven"
	 * @generated
	 * @ordered
	 */
	public static final int SEVEN = 7;

	/**
	 * The '<em><b>Eigth</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Eigth</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EIGTH_LITERAL
	 * @model name="eigth"
	 * @generated
	 * @ordered
	 */
	public static final int EIGTH = 8;

	/**
	 * The '<em><b>Nine</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nine</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NINE_LITERAL
	 * @model name="nine"
	 * @generated
	 * @ordered
	 */
	public static final int NINE = 9;

	/**
	 * The '<em><b>Star</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Star</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STAR_LITERAL
	 * @model name="star"
	 * @generated
	 * @ordered
	 */
	public static final int STAR = 10;

	/**
	 * The '<em><b>Pound</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pound</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POUND_LITERAL
	 * @model name="pound"
	 * @generated
	 * @ordered
	 */
	public static final int POUND = 11;

	/**
	 * The '<em><b>Zero</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ZERO
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind ZERO_LITERAL = new DTMFKind(ZERO, "zero", "zero");

	/**
	 * The '<em><b>One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ONE
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind ONE_LITERAL = new DTMFKind(ONE, "one", "one");

	/**
	 * The '<em><b>Two</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TWO
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind TWO_LITERAL = new DTMFKind(TWO, "two", "two");

	/**
	 * The '<em><b>Three</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THREE
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind THREE_LITERAL = new DTMFKind(THREE, "three", "three");

	/**
	 * The '<em><b>For</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOR
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind FOR_LITERAL = new DTMFKind(FOR, "for", "for");

	/**
	 * The '<em><b>Five</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIVE
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind FIVE_LITERAL = new DTMFKind(FIVE, "five", "five");

	/**
	 * The '<em><b>Six</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIX
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind SIX_LITERAL = new DTMFKind(SIX, "six", "six");

	/**
	 * The '<em><b>Seven</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SEVEN
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind SEVEN_LITERAL = new DTMFKind(SEVEN, "seven", "seven");

	/**
	 * The '<em><b>Eigth</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EIGTH
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind EIGTH_LITERAL = new DTMFKind(EIGTH, "eigth", "eigth");

	/**
	 * The '<em><b>Nine</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NINE
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind NINE_LITERAL = new DTMFKind(NINE, "nine", "nine");

	/**
	 * The '<em><b>Star</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STAR
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind STAR_LITERAL = new DTMFKind(STAR, "star", "star");

	/**
	 * The '<em><b>Pound</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POUND
	 * @generated
	 * @ordered
	 */
	public static final DTMFKind POUND_LITERAL = new DTMFKind(POUND, "pound", "pound");

	/**
	 * An array of all the '<em><b>DTMF Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DTMFKind[] VALUES_ARRAY =
		new DTMFKind[] {
			ZERO_LITERAL,
			ONE_LITERAL,
			TWO_LITERAL,
			THREE_LITERAL,
			FOR_LITERAL,
			FIVE_LITERAL,
			SIX_LITERAL,
			SEVEN_LITERAL,
			EIGTH_LITERAL,
			NINE_LITERAL,
			STAR_LITERAL,
			POUND_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>DTMF Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DTMF Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DTMFKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DTMFKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DTMF Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DTMFKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DTMFKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DTMF Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DTMFKind get(int value) {
		switch (value) {
			case ZERO: return ZERO_LITERAL;
			case ONE: return ONE_LITERAL;
			case TWO: return TWO_LITERAL;
			case THREE: return THREE_LITERAL;
			case FOR: return FOR_LITERAL;
			case FIVE: return FIVE_LITERAL;
			case SIX: return SIX_LITERAL;
			case SEVEN: return SEVEN_LITERAL;
			case EIGTH: return EIGTH_LITERAL;
			case NINE: return NINE_LITERAL;
			case STAR: return STAR_LITERAL;
			case POUND: return POUND_LITERAL;
		}
		return null;	
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DTMFKind(int value, String name, String literal) {
		super(value, name, literal);
	}

} //DTMFKind
