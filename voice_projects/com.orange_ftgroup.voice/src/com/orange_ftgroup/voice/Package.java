/**
 * <copyright>
 * </copyright>
 *
 * $Id: Package.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Package#getOwnedElement <em>Owned Element</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Package#getUsedElement <em>Used Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends PackageableElement {
	/**
	 * Returns the value of the '<em><b>Owned Element</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.PackageableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Element</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getPackage_OwnedElement()
	 * @model type="com.orange_ftgroup.voice.PackageableElement" containment="true"
	 * @generated
	 */
	EList getOwnedElement();

	/**
	 * Returns the value of the '<em><b>Used Element</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.PackageableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Element</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getPackage_UsedElement()
	 * @model type="com.orange_ftgroup.voice.PackageableElement"
	 * @generated
	 */
	EList getUsedElement();

} // Package