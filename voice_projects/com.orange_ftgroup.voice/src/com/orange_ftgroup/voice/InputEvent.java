/**
 * <copyright>
 * </copyright>
 *
 * $Id: InputEvent.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.InputEvent#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getInputEvent()
 * @model abstract="true"
 * @generated
 */
public interface InputEvent extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getInputEvent_Parameter()
	 * @model type="com.orange_ftgroup.voice.Parameter" containment="true"
	 * @generated
	 */
	EList getParameter();

} // InputEvent