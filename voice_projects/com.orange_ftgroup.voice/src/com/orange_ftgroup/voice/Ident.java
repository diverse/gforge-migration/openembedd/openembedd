/**
 * <copyright>
 * </copyright>
 *
 * $Id: Ident.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ident</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Ident#getValue <em>Value</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Ident#getValueContainer <em>Value Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getIdent()
 * @model
 * @generated
 */
public interface Ident extends ValueExpression {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getIdent_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Ident#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Value Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Container</em>' reference.
	 * @see #setValueContainer(ValueContainer)
	 * @see com.orange_ftgroup.voice.VoicePackage#getIdent_ValueContainer()
	 * @model
	 * @generated
	 */
	ValueContainer getValueContainer();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Ident#getValueContainer <em>Value Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Container</em>' reference.
	 * @see #getValueContainer()
	 * @generated
	 */
	void setValueContainer(ValueContainer value);

} // Ident