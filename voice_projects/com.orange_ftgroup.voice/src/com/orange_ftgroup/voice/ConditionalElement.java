/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConditionalElement.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ConditionalElement#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ConditionalElement#getThenPart <em>Then Part</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ConditionalElement#getElsePart <em>Else Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getConditionalElement()
 * @model
 * @generated
 */
public interface ConditionalElement extends MessageElement {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' reference.
	 * @see #setCondition(MessageElementCondition)
	 * @see com.orange_ftgroup.voice.VoicePackage#getConditionalElement_Condition()
	 * @model
	 * @generated
	 */
	MessageElementCondition getCondition();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ConditionalElement#getCondition <em>Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(MessageElementCondition value);

	/**
	 * Returns the value of the '<em><b>Then Part</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessageElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Part</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getConditionalElement_ThenPart()
	 * @model type="com.orange_ftgroup.voice.MessageElement" containment="true"
	 * @generated
	 */
	EList getThenPart();

	/**
	 * Returns the value of the '<em><b>Else Part</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessageElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Part</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getConditionalElement_ElsePart()
	 * @model type="com.orange_ftgroup.voice.MessageElement" containment="true"
	 * @generated
	 */
	EList getElsePart();

} // ConditionalElement