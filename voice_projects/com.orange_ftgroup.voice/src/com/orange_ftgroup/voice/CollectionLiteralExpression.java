/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollectionLiteralExpression.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Literal Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.CollectionLiteralExpression#getPart <em>Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getCollectionLiteralExpression()
 * @model
 * @generated
 */
public interface CollectionLiteralExpression extends ValueExpression {
	/**
	 * Returns the value of the '<em><b>Part</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.CollectionLiteralPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getCollectionLiteralExpression_Part()
	 * @model type="com.orange_ftgroup.voice.CollectionLiteralPart" containment="true"
	 * @generated
	 */
	EList getPart();

} // CollectionLiteralExpression