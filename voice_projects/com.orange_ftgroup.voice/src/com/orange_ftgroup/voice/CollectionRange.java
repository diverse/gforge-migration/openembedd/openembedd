/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollectionRange.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.CollectionRange#getFirst <em>First</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.CollectionRange#getLast <em>Last</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getCollectionRange()
 * @model
 * @generated
 */
public interface CollectionRange extends CollectionLiteralPart {
	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #setFirst(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getCollectionRange_First()
	 * @model containment="true"
	 * @generated
	 */
	Expression getFirst();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.CollectionRange#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(Expression value);

	/**
	 * Returns the value of the '<em><b>Last</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last</em>' containment reference.
	 * @see #setLast(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getCollectionRange_Last()
	 * @model containment="true"
	 * @generated
	 */
	Expression getLast();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.CollectionRange#getLast <em>Last</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last</em>' containment reference.
	 * @see #getLast()
	 * @generated
	 */
	void setLast(Expression value);

} // CollectionRange