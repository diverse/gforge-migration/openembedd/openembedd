/**
 * <copyright>
 * </copyright>
 *
 * $Id: Action.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Action#getText <em>Text</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Action#getActionSequence <em>Action Sequence</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getAction_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Action#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Action Sequence</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.ActionSequence#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Sequence</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Sequence</em>' container reference.
	 * @see #setActionSequence(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getAction_ActionSequence()
	 * @see com.orange_ftgroup.voice.ActionSequence#getAction
	 * @model opposite="action"
	 * @generated
	 */
	ActionSequence getActionSequence();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Action#getActionSequence <em>Action Sequence</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Sequence</em>' container reference.
	 * @see #getActionSequence()
	 * @generated
	 */
	void setActionSequence(ActionSequence value);

} // Action