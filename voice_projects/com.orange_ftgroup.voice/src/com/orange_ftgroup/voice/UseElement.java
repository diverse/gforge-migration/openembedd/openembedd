/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseElement.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.UseElement#getUsedMessagePart <em>Used Message Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getUseElement()
 * @model
 * @generated
 */
public interface UseElement extends MessageElement {
	/**
	 * Returns the value of the '<em><b>Used Message Part</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Message Part</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Message Part</em>' reference.
	 * @see #setUsedMessagePart(MessagePart)
	 * @see com.orange_ftgroup.voice.VoicePackage#getUseElement_UsedMessagePart()
	 * @model
	 * @generated
	 */
	MessagePart getUsedMessagePart();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.UseElement#getUsedMessagePart <em>Used Message Part</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Used Message Part</em>' reference.
	 * @see #getUsedMessagePart()
	 * @generated
	 */
	void setUsedMessagePart(MessagePart value);

} // UseElement