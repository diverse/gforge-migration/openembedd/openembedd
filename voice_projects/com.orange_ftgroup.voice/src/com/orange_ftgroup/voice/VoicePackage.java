/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoicePackage.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoiceFactory
 * @model kind="package"
 * @generated
 */
public interface VoicePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "voice";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///voice.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "voice";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VoicePackage eINSTANCE = com.orange_ftgroup.voice.impl.VoicePackageImpl.init();

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ModelElementImpl <em>Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ModelElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getModelElement()
	 * @generated
	 */
	int MODEL_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT__TAG = 1;

	/**
	 * The number of structural features of the '<em>Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.NamedElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__TEXT = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TypedElementImpl <em>Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TypedElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTypedElement()
	 * @generated
	 */
	int TYPED_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.PackageableElementImpl <em>Packageable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.PackageableElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPackageableElement()
	 * @generated
	 */
	int PACKAGEABLE_ELEMENT = 33;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Packageable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TypeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getType()
	 * @generated
	 */
	int TYPE = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__DESCRIPTION = PACKAGEABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__TAG = PACKAGEABLE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__NAME = PACKAGEABLE_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ValueContainerImpl <em>Value Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ValueContainerImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getValueContainer()
	 * @generated
	 */
	int VALUE_CONTAINER = 48;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_CONTAINER__DESCRIPTION = TYPED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_CONTAINER__TAG = TYPED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_CONTAINER__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_CONTAINER__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The number of structural features of the '<em>Value Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_CONTAINER_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ParameterImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DESCRIPTION = VALUE_CONTAINER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TAG = VALUE_CONTAINER__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = VALUE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = VALUE_CONTAINER__TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DIRECTION = VALUE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = VALUE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.SignatureImpl <em>Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.SignatureImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSignature()
	 * @generated
	 */
	int SIGNATURE = 31;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__DESCRIPTION = TYPED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__TAG = TYPED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__FORMAL_PARAMETER = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.OperationImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DESCRIPTION = SIGNATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__TAG = SIGNATURE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = SIGNATURE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__TYPE = SIGNATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__FORMAL_PARAMETER = SIGNATURE__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__VARIABLE = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__BEHAVIOR = SIGNATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ConstraintImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 7;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__DESCRIPTION = PACKAGEABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__TAG = PACKAGEABLE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__NAME = PACKAGEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__SPECIFICATION = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.PackageImpl <em>Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.PackageImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPackage()
	 * @generated
	 */
	int PACKAGE = 8;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__DESCRIPTION = PACKAGEABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__TAG = PACKAGEABLE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__NAME = PACKAGEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__OWNED_ELEMENT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Used Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE__USED_ELEMENT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FEATURE_COUNT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DataTypeImpl <em>Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DataTypeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__DESCRIPTION = TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__TAG = TYPE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__NAME = TYPE__NAME;

	/**
	 * The number of structural features of the '<em>Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TagImpl <em>Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TagImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTag()
	 * @generated
	 */
	int TAG = 10;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG__VALUE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.PropertyImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 11;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DESCRIPTION = VALUE_CONTAINER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__TAG = VALUE_CONTAINER__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = VALUE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__TYPE = VALUE_CONTAINER__TYPE;

	/**
	 * The feature id for the '<em><b>Is Composite</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IS_COMPOSITE = VALUE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IS_READ_ONLY = VALUE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Derived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IS_DERIVED = VALUE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IS_ID = VALUE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VISIBILITY = VALUE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Default</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DEFAULT = VALUE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = VALUE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ClassImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getClass_()
	 * @generated
	 */
	int CLASS = 12;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__DESCRIPTION = TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__TAG = TYPE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__NAME = TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__ATTRIBUTE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__OPERATION = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.BinaryExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__OPERATION_NAME = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LEFT_OPERAND = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RIGHT_OPERAND = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__OPERATION = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.UnaryExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 14;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERATION_NAME = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERATION = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ValueExpressionImpl <em>Value Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ValueExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getValueExpression()
	 * @generated
	 */
	int VALUE_EXPRESSION = 15;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The number of structural features of the '<em>Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CharacterValueImpl <em>Character Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CharacterValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCharacterValue()
	 * @generated
	 */
	int CHARACTER_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Character Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.IntegerValueImpl <em>Integer Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.IntegerValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIntegerValue()
	 * @generated
	 */
	int INTEGER_VALUE = 17;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.FloatValueImpl <em>Float Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.FloatValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFloatValue()
	 * @generated
	 */
	int FLOAT_VALUE = 18;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.BooleanValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 19;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CharstringValueImpl <em>Charstring Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CharstringValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCharstringValue()
	 * @generated
	 */
	int CHARSTRING_VALUE = 20;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARSTRING_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARSTRING_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARSTRING_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARSTRING_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Charstring Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARSTRING_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.IdentImpl <em>Ident</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.IdentImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIdent()
	 * @generated
	 */
	int IDENT = 21;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT__VALUE_CONTAINER = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ident</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENT_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ParenthesisExpressionImpl <em>Parenthesis Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ParenthesisExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParenthesisExpression()
	 * @generated
	 */
	int PARENTHESIS_EXPRESSION = 22;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Inner Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__INNER_EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parenthesis Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ConditionalExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConditionalExpression()
	 * @generated
	 */
	int CONDITIONAL_EXPRESSION = 23;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__CONDITION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__THEN_PART = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__ELSE_PART = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Conditional Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ListExpressionImpl <em>List Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ListExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListExpression()
	 * @generated
	 */
	int LIST_EXPRESSION = 24;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__ELEMENT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>List Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.FeatureExpressionImpl <em>Feature Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.FeatureExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFeatureExpression()
	 * @generated
	 */
	int FEATURE_EXPRESSION = 38;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXPRESSION__SOURCE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CallExpressionImpl <em>Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CallExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCallExpression()
	 * @generated
	 */
	int CALL_EXPRESSION = 25;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__DESCRIPTION = FEATURE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__TAG = FEATURE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__TEXT = FEATURE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__SOURCE = FEATURE_EXPRESSION__SOURCE;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__OPERATION_NAME = FEATURE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ARGUMENT = FEATURE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__OPERATION = FEATURE_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION_FEATURE_COUNT = FEATURE_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.EnumerationTypeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumerationType()
	 * @generated
	 */
	int ENUMERATION_TYPE = 26;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE__DESCRIPTION = DATA_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE__TAG = DATA_TYPE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE__NAME = DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Item</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE__ITEM = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumeration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.EnumerationItemImpl <em>Enumeration Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.EnumerationItemImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumerationItem()
	 * @generated
	 */
	int ENUMERATION_ITEM = 27;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_ITEM__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_ITEM__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_ITEM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Enumeration Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_ITEM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ChoiceTypeImpl <em>Choice Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ChoiceTypeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getChoiceType()
	 * @generated
	 */
	int CHOICE_TYPE = 28;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE__DESCRIPTION = DATA_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE__TAG = DATA_TYPE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE__NAME = DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE__ATTRIBUTE = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Choice Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ListTypeImpl <em>List Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ListTypeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListType()
	 * @generated
	 */
	int LIST_TYPE = 29;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_TYPE__DESCRIPTION = DATA_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_TYPE__TAG = DATA_TYPE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_TYPE__NAME = DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Content Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_TYPE__CONTENT_TYPE = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>List Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.VariableImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 30;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__DESCRIPTION = TYPED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__TAG = TYPED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Is Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__IS_SHARED = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.InformalExpressionImpl <em>Informal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.InformalExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInformalExpression()
	 * @generated
	 */
	int INFORMAL_EXPRESSION = 32;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMAL_EXPRESSION__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMAL_EXPRESSION__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMAL_EXPRESSION__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMAL_EXPRESSION__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Informal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMAL_EXPRESSION_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ActionSequenceImpl <em>Action Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ActionSequenceImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getActionSequence()
	 * @generated
	 */
	int ACTION_SEQUENCE = 34;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SEQUENCE__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SEQUENCE__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SEQUENCE__ACTION = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SEQUENCE__MESSAGE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Action Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_SEQUENCE_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ActionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 35;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TEXT = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_SEQUENCE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DigitValueImpl <em>Digit Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DigitValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDigitValue()
	 * @generated
	 */
	int DIGIT_VALUE = 36;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIGIT_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIGIT_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIGIT_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIGIT_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Digit Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIGIT_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DTMFValueImpl <em>DTMF Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DTMFValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMFValue()
	 * @generated
	 */
	int DTMF_VALUE = 37;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DTMF Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.PropertyExpressionImpl <em>Property Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.PropertyExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPropertyExpression()
	 * @generated
	 */
	int PROPERTY_EXPRESSION = 39;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION__DESCRIPTION = FEATURE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION__TAG = FEATURE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION__TEXT = FEATURE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION__SOURCE = FEATURE_EXPRESSION__SOURCE;

	/**
	 * The feature id for the '<em><b>Referred Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION__REFERRED_PROPERTY = FEATURE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Property Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_EXPRESSION_FEATURE_COUNT = FEATURE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CollectionLiteralPartImpl <em>Collection Literal Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CollectionLiteralPartImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionLiteralPart()
	 * @generated
	 */
	int COLLECTION_LITERAL_PART = 43;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_PART__DESCRIPTION = TYPED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_PART__TAG = TYPED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_PART__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_PART__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The number of structural features of the '<em>Collection Literal Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_PART_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CollectionRangeImpl <em>Collection Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CollectionRangeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionRange()
	 * @generated
	 */
	int COLLECTION_RANGE = 40;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__DESCRIPTION = COLLECTION_LITERAL_PART__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__TAG = COLLECTION_LITERAL_PART__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__NAME = COLLECTION_LITERAL_PART__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__TYPE = COLLECTION_LITERAL_PART__TYPE;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__FIRST = COLLECTION_LITERAL_PART_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Last</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE__LAST = COLLECTION_LITERAL_PART_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Collection Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_RANGE_FEATURE_COUNT = COLLECTION_LITERAL_PART_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CollectionLiteralExpressionImpl <em>Collection Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CollectionLiteralExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionLiteralExpression()
	 * @generated
	 */
	int COLLECTION_LITERAL_EXPRESSION = 41;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_EXPRESSION__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_EXPRESSION__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_EXPRESSION__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_EXPRESSION__PART = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Collection Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_LITERAL_EXPRESSION_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CollectionItemImpl <em>Collection Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CollectionItemImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionItem()
	 * @generated
	 */
	int COLLECTION_ITEM = 42;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM__DESCRIPTION = COLLECTION_LITERAL_PART__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM__TAG = COLLECTION_LITERAL_PART__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM__NAME = COLLECTION_LITERAL_PART__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM__TYPE = COLLECTION_LITERAL_PART__TYPE;

	/**
	 * The feature id for the '<em><b>Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM__ITEM = COLLECTION_LITERAL_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Collection Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ITEM_FEATURE_COUNT = COLLECTION_LITERAL_PART_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.EnumValueImpl <em>Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.EnumValueImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumValue()
	 * @generated
	 */
	int ENUM_VALUE = 44;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__DESCRIPTION = VALUE_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__TAG = VALUE_EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__TEXT = VALUE_EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__VALUE = VALUE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Literal Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__LITERAL_DEFINITION = VALUE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_FEATURE_COUNT = VALUE_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.BehaviorImpl <em>Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.BehaviorImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBehavior()
	 * @generated
	 */
	int BEHAVIOR = 45;

	/**
	 * The number of structural features of the '<em>Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.StateMachineImpl <em>State Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.StateMachineImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getStateMachine()
	 * @generated
	 */
	int STATE_MACHINE = 46;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__NODE = BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TRANSITION = BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_FEATURE_COUNT = BEHAVIOR_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.NewExpressionImpl <em>New Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.NewExpressionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNewExpression()
	 * @generated
	 */
	int NEW_EXPRESSION = 47;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION__TAG = EXPRESSION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION__TEXT = EXPRESSION__TEXT;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION__CLASS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION__ARGUMENT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>New Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DialogImpl <em>Dialog</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DialogImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialog()
	 * @generated
	 */
	int DIALOG = 49;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__DESCRIPTION = SIGNATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__TAG = SIGNATURE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__NAME = SIGNATURE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__TYPE = SIGNATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__FORMAL_PARAMETER = SIGNATURE__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__NODE = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Accessed Functionality</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__ACCESSED_FUNCTIONALITY = SIGNATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__TRANSITION = SIGNATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Message</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__MESSAGE = SIGNATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Concept</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__CONCEPT = SIGNATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Message Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__MESSAGE_PART = SIGNATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__EXTERNAL_EVENT = SIGNATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__CONDITION = SIGNATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__VARIABLE = SIGNATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Sub Dialog</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__SUB_DIALOG = SIGNATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Global Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__GLOBAL_VARIABLE = SIGNATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Referenced Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__REFERENCED_VARIABLE = SIGNATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Referenced Message</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__REFERENCED_MESSAGE = SIGNATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Referenced Input Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__REFERENCED_INPUT_EVENT = SIGNATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__OPERATION = SIGNATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG__OWNED_GRAMMAR = SIGNATURE_FEATURE_COUNT + 15;

	/**
	 * The number of structural features of the '<em>Dialog</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 16;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DialogNodeImpl <em>Dialog Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DialogNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialogNode()
	 * @generated
	 */
	int DIALOG_NODE = 52;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__OWNER = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__OUTGOING = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE__INCOMING = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dialog Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_NODE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DialogStateImpl <em>Dialog State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DialogStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialogState()
	 * @generated
	 */
	int DIALOG_STATE = 51;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__DESCRIPTION = DIALOG_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__TAG = DIALOG_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__NAME = DIALOG_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__OWNER = DIALOG_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__OUTGOING = DIALOG_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__INCOMING = DIALOG_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE__ENTRY = DIALOG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dialog State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIALOG_STATE_FEATURE_COUNT = DIALOG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.SubDialogStateImpl <em>Sub Dialog State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.SubDialogStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSubDialogState()
	 * @generated
	 */
	int SUB_DIALOG_STATE = 50;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__DESCRIPTION = DIALOG_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__TAG = DIALOG_STATE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__NAME = DIALOG_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__OWNER = DIALOG_STATE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__OUTGOING = DIALOG_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__INCOMING = DIALOG_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__ENTRY = DIALOG_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>Called</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__CALLED = DIALOG_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE__ARGUMENT = DIALOG_STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Dialog State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_DIALOG_STATE_FEATURE_COUNT = DIALOG_STATE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TransitionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 53;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Is Else</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__IS_ELSE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Effect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EFFECT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRIGGER = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GUARD = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__OWNER = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ORIGIN = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TriggerImpl <em>Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TriggerImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTrigger()
	 * @generated
	 */
	int TRIGGER = 54;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__EVENT = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Arg Handle</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__ARG_HANDLE = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.TransientNodeImpl <em>Transient Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.TransientNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTransientNode()
	 * @generated
	 */
	int TRANSIENT_NODE = 55;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__DESCRIPTION = DIALOG_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__TAG = DIALOG_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__NAME = DIALOG_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__OWNER = DIALOG_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__OUTGOING = DIALOG_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE__INCOMING = DIALOG_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Transient Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSIENT_NODE_FEATURE_COUNT = DIALOG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.WaitStateImpl <em>Wait State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.WaitStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getWaitState()
	 * @generated
	 */
	int WAIT_STATE = 56;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__DESCRIPTION = DIALOG_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__TAG = DIALOG_STATE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__NAME = DIALOG_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__OWNER = DIALOG_STATE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__OUTGOING = DIALOG_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__INCOMING = DIALOG_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__ENTRY = DIALOG_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__DELAY = DIALOG_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Grammar</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE__GRAMMAR = DIALOG_STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Wait State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATE_FEATURE_COUNT = DIALOG_STATE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.AnyStateImpl <em>Any State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.AnyStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyState()
	 * @generated
	 */
	int ANY_STATE = 57;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__DESCRIPTION = DIALOG_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__TAG = DIALOG_STATE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__NAME = DIALOG_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__OWNER = DIALOG_STATE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__OUTGOING = DIALOG_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__INCOMING = DIALOG_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE__ENTRY = DIALOG_STATE__ENTRY;

	/**
	 * The number of structural features of the '<em>Any State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_STATE_FEATURE_COUNT = DIALOG_STATE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ListStateImpl <em>List State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ListStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListState()
	 * @generated
	 */
	int LIST_STATE = 58;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__DESCRIPTION = DIALOG_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__TAG = DIALOG_STATE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__NAME = DIALOG_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__OWNER = DIALOG_STATE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__OUTGOING = DIALOG_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__INCOMING = DIALOG_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__ENTRY = DIALOG_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>Excluded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__EXCLUDED = DIALOG_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE__STATE = DIALOG_STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>List State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_STATE_FEATURE_COUNT = DIALOG_STATE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DecisionNodeImpl <em>Decision Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DecisionNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDecisionNode()
	 * @generated
	 */
	int DECISION_NODE = 59;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__DESCRIPTION = TRANSIENT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__TAG = TRANSIENT_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__NAME = TRANSIENT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__OWNER = TRANSIENT_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__OUTGOING = TRANSIENT_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__INCOMING = TRANSIENT_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE__CONDITION = TRANSIENT_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Decision Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_NODE_FEATURE_COUNT = TRANSIENT_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.InitialNodeImpl <em>Initial Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.InitialNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInitialNode()
	 * @generated
	 */
	int INITIAL_NODE = 60;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__DESCRIPTION = TRANSIENT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__TAG = TRANSIENT_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__NAME = TRANSIENT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__OWNER = TRANSIENT_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__OUTGOING = TRANSIENT_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE__INCOMING = TRANSIENT_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Initial Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_NODE_FEATURE_COUNT = TRANSIENT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.JunctionNodeImpl <em>Junction Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.JunctionNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getJunctionNode()
	 * @generated
	 */
	int JUNCTION_NODE = 61;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__DESCRIPTION = TRANSIENT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__TAG = TRANSIENT_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__NAME = TRANSIENT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__OWNER = TRANSIENT_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__OUTGOING = TRANSIENT_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE__INCOMING = TRANSIENT_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Junction Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_NODE_FEATURE_COUNT = TRANSIENT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.FinalNodeImpl <em>Final Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.FinalNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFinalNode()
	 * @generated
	 */
	int FINAL_NODE = 64;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__DESCRIPTION = TRANSIENT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__TAG = TRANSIENT_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__NAME = TRANSIENT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__OWNER = TRANSIENT_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__OUTGOING = TRANSIENT_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE__INCOMING = TRANSIENT_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Final Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_NODE_FEATURE_COUNT = TRANSIENT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DiversionNodeImpl <em>Diversion Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DiversionNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDiversionNode()
	 * @generated
	 */
	int DIVERSION_NODE = 62;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__DESCRIPTION = FINAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__TAG = FINAL_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__NAME = FINAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__OWNER = FINAL_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__OUTGOING = FINAL_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__INCOMING = FINAL_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__TARGET = FINAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE__ARGUMENT = FINAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Diversion Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVERSION_NODE_FEATURE_COUNT = FINAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.HistoryStateImpl <em>History State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.HistoryStateImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getHistoryState()
	 * @generated
	 */
	int HISTORY_STATE = 63;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__DESCRIPTION = DIALOG_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__TAG = DIALOG_STATE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__NAME = DIALOG_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__OWNER = DIALOG_STATE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__OUTGOING = DIALOG_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__INCOMING = DIALOG_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE__ENTRY = DIALOG_STATE__ENTRY;

	/**
	 * The number of structural features of the '<em>History State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_STATE_FEATURE_COUNT = DIALOG_STATE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.NextNodeImpl <em>Next Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.NextNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNextNode()
	 * @generated
	 */
	int NEXT_NODE = 65;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__DESCRIPTION = FINAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__TAG = FINAL_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__NAME = FINAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__OWNER = FINAL_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__OUTGOING = FINAL_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE__INCOMING = FINAL_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Next Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_NODE_FEATURE_COUNT = FINAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.StopNodeImpl <em>Stop Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.StopNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getStopNode()
	 * @generated
	 */
	int STOP_NODE = 66;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__DESCRIPTION = FINAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__TAG = FINAL_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__NAME = FINAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__OWNER = FINAL_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__OUTGOING = FINAL_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE__INCOMING = FINAL_NODE__INCOMING;

	/**
	 * The number of structural features of the '<em>Stop Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_NODE_FEATURE_COUNT = FINAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ReturnNodeImpl <em>Return Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ReturnNodeImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReturnNode()
	 * @generated
	 */
	int RETURN_NODE = 67;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__DESCRIPTION = FINAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__TAG = FINAL_NODE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__NAME = FINAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__OWNER = FINAL_NODE__OWNER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__OUTGOING = FINAL_NODE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__INCOMING = FINAL_NODE__INCOMING;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE__EXPRESSION = FINAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Return Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_NODE_FEATURE_COUNT = FINAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.AssignmentImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 68;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Value Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__VALUE_CONTAINER = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__VALUE = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.PlayImpl <em>Play</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.PlayImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPlay()
	 * @generated
	 */
	int PLAY = 69;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Interruptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__INTERRUPTIBLE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__MESSAGE = ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__MESSAGE_ARGUMENT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Play</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY_FEATURE_COUNT = ACTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.UninterpretedImpl <em>Uninterpreted</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.UninterpretedImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUninterpreted()
	 * @generated
	 */
	int UNINTERPRETED = 70;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED__BODY = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Uninterpreted</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINTERPRETED_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.IfThenElseImpl <em>If Then Else</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.IfThenElseImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIfThenElse()
	 * @generated
	 */
	int IF_THEN_ELSE = 71;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__CONDITION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__THEN_PART = ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE__ELSE_PART = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Then Else</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_ELSE_FEATURE_COUNT = ACTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.WhileImpl <em>While</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.WhileImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getWhile()
	 * @generated
	 */
	int WHILE = 72;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__CONDITION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE__BODY = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>While</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ReturnActionImpl <em>Return Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ReturnActionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReturnAction()
	 * @generated
	 */
	int RETURN_ACTION = 73;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__EXPRESSION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Return Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.CallActionImpl <em>Call Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.CallActionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCallAction()
	 * @generated
	 */
	int CALL_ACTION = 74;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__DESCRIPTION = ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__TAG = ACTION__TAG;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__TEXT = ACTION__TEXT;

	/**
	 * The feature id for the '<em><b>Action Sequence</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__ACTION_SEQUENCE = ACTION__ACTION_SEQUENCE;

	/**
	 * The feature id for the '<em><b>Call Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__CALL_EXPRESSION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.InputEventImpl <em>Input Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.InputEventImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInputEvent()
	 * @generated
	 */
	int INPUT_EVENT = 75;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__PARAMETER = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DtmfInputImpl <em>Dtmf Input</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DtmfInputImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDtmfInput()
	 * @generated
	 */
	int DTMF_INPUT = 86;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_INPUT__DESCRIPTION = INPUT_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_INPUT__TAG = INPUT_EVENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_INPUT__NAME = INPUT_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_INPUT__PARAMETER = INPUT_EVENT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Dtmf Input</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_INPUT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.DTMFImpl <em>DTMF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.DTMFImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMF()
	 * @generated
	 */
	int DTMF = 76;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF__DESCRIPTION = DTMF_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF__TAG = DTMF_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF__NAME = DTMF_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF__PARAMETER = DTMF_INPUT__PARAMETER;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF__KEY = DTMF_INPUT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DTMF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DTMF_FEATURE_COUNT = DTMF_INPUT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.SystemInputImpl <em>System Input</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.SystemInputImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSystemInput()
	 * @generated
	 */
	int SYSTEM_INPUT = 87;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_INPUT__DESCRIPTION = INPUT_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_INPUT__TAG = INPUT_EVENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_INPUT__NAME = INPUT_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_INPUT__PARAMETER = INPUT_EVENT__PARAMETER;

	/**
	 * The number of structural features of the '<em>System Input</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_INPUT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.RejectImpl <em>Reject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.RejectImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReject()
	 * @generated
	 */
	int REJECT = 77;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT__DESCRIPTION = SYSTEM_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT__TAG = SYSTEM_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT__NAME = SYSTEM_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT__PARAMETER = SYSTEM_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Reject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT_FEATURE_COUNT = SYSTEM_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.InactivityImpl <em>Inactivity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.InactivityImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInactivity()
	 * @generated
	 */
	int INACTIVITY = 78;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INACTIVITY__DESCRIPTION = SYSTEM_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INACTIVITY__TAG = SYSTEM_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INACTIVITY__NAME = SYSTEM_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INACTIVITY__PARAMETER = SYSTEM_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Inactivity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INACTIVITY_FEATURE_COUNT = SYSTEM_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ConceptImpl <em>Concept</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ConceptImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConcept()
	 * @generated
	 */
	int CONCEPT = 79;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT__DESCRIPTION = INPUT_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT__TAG = INPUT_EVENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT__NAME = INPUT_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT__PARAMETER = INPUT_EVENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>Grammar</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT__GRAMMAR = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Concept</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ExternalEventImpl <em>External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ExternalEventImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getExternalEvent()
	 * @generated
	 */
	int EXTERNAL_EVENT = 80;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT__DESCRIPTION = INPUT_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT__TAG = INPUT_EVENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT__NAME = INPUT_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT__PARAMETER = INPUT_EVENT__PARAMETER;

	/**
	 * The number of structural features of the '<em>External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.AnyDtmfImpl <em>Any Dtmf</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.AnyDtmfImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyDtmf()
	 * @generated
	 */
	int ANY_DTMF = 81;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DTMF__DESCRIPTION = DTMF_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DTMF__TAG = DTMF_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DTMF__NAME = DTMF_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DTMF__PARAMETER = DTMF_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Any Dtmf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DTMF_FEATURE_COUNT = DTMF_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.AnyDigitImpl <em>Any Digit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.AnyDigitImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyDigit()
	 * @generated
	 */
	int ANY_DIGIT = 82;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DIGIT__DESCRIPTION = DTMF_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DIGIT__TAG = DTMF_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DIGIT__NAME = DTMF_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DIGIT__PARAMETER = DTMF_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Any Digit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_DIGIT_FEATURE_COUNT = DTMF_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.RecordingImpl <em>Recording</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.RecordingImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getRecording()
	 * @generated
	 */
	int RECORDING = 83;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORDING__DESCRIPTION = SYSTEM_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORDING__TAG = SYSTEM_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORDING__NAME = SYSTEM_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORDING__PARAMETER = SYSTEM_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Recording</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORDING_FEATURE_COUNT = SYSTEM_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.AbstractDTMFImpl <em>Abstract DTMF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.AbstractDTMFImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAbstractDTMF()
	 * @generated
	 */
	int ABSTRACT_DTMF = 84;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DTMF__DESCRIPTION = DTMF_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DTMF__TAG = DTMF_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DTMF__NAME = DTMF_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DTMF__PARAMETER = DTMF_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Abstract DTMF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DTMF_FEATURE_COUNT = DTMF_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.MessageEndImpl <em>Message End</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.MessageEndImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageEnd()
	 * @generated
	 */
	int MESSAGE_END = 85;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_END__DESCRIPTION = SYSTEM_INPUT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_END__TAG = SYSTEM_INPUT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_END__NAME = SYSTEM_INPUT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_END__PARAMETER = SYSTEM_INPUT__PARAMETER;

	/**
	 * The number of structural features of the '<em>Message End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_END_FEATURE_COUNT = SYSTEM_INPUT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.MessageImpl <em>Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.MessageImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessage()
	 * @generated
	 */
	int MESSAGE = 88;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__DESCRIPTION = SIGNATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TAG = SIGNATURE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__NAME = SIGNATURE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TYPE = SIGNATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__FORMAL_PARAMETER = SIGNATURE__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__VISIBILITY = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__BODY = SIGNATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__MESSAGE_ELEMENT = SIGNATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Action Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ACTION_SPECIFICATION = SIGNATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.MessagePartImpl <em>Message Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.MessagePartImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessagePart()
	 * @generated
	 */
	int MESSAGE_PART = 89;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART__DESCRIPTION = SIGNATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART__TAG = SIGNATURE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART__NAME = SIGNATURE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART__TYPE = SIGNATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART__FORMAL_PARAMETER = SIGNATURE__FORMAL_PARAMETER;

	/**
	 * The number of structural features of the '<em>Message Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_PART_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.FixPartImpl <em>Fix Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.FixPartImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFixPart()
	 * @generated
	 */
	int FIX_PART = 90;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__DESCRIPTION = MESSAGE_PART__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__TAG = MESSAGE_PART__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__NAME = MESSAGE_PART__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__TYPE = MESSAGE_PART__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__FORMAL_PARAMETER = MESSAGE_PART__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__CONTENT = MESSAGE_PART_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART__FORMAT = MESSAGE_PART_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Fix Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PART_FEATURE_COUNT = MESSAGE_PART_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.VariablePartImpl <em>Variable Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.VariablePartImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVariablePart()
	 * @generated
	 */
	int VARIABLE_PART = 91;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__DESCRIPTION = MESSAGE_PART__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__TAG = MESSAGE_PART__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__NAME = MESSAGE_PART__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__TYPE = MESSAGE_PART__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__FORMAL_PARAMETER = MESSAGE_PART__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__FORMAT = MESSAGE_PART_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__VISIBILITY = MESSAGE_PART_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__VALUE = MESSAGE_PART_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Action Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART__ACTION_SPECIFICATION = MESSAGE_PART_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Variable Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_PART_FEATURE_COUNT = MESSAGE_PART_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.MessageElementImpl <em>Message Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.MessageElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageElement()
	 * @generated
	 */
	int MESSAGE_ELEMENT = 94;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The number of structural features of the '<em>Message Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ConditionalElementImpl <em>Conditional Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ConditionalElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConditionalElement()
	 * @generated
	 */
	int CONDITIONAL_ELEMENT = 92;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT__DESCRIPTION = MESSAGE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT__TAG = MESSAGE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT__CONDITION = MESSAGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT__THEN_PART = MESSAGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT__ELSE_PART = MESSAGE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Conditional Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_ELEMENT_FEATURE_COUNT = MESSAGE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.SilencePartImpl <em>Silence Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.SilencePartImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSilencePart()
	 * @generated
	 */
	int SILENCE_PART = 93;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__DESCRIPTION = MESSAGE_PART__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__TAG = MESSAGE_PART__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__NAME = MESSAGE_PART__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__TYPE = MESSAGE_PART__TYPE;

	/**
	 * The feature id for the '<em><b>Formal Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__FORMAL_PARAMETER = MESSAGE_PART__FORMAL_PARAMETER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART__DURATION = MESSAGE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Silence Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SILENCE_PART_FEATURE_COUNT = MESSAGE_PART_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.UseElementImpl <em>Use Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.UseElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUseElement()
	 * @generated
	 */
	int USE_ELEMENT = 95;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_ELEMENT__DESCRIPTION = MESSAGE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_ELEMENT__TAG = MESSAGE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Used Message Part</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_ELEMENT__USED_MESSAGE_PART = MESSAGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Use Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_ELEMENT_FEATURE_COUNT = MESSAGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.MessageElementConditionImpl <em>Message Element Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.MessageElementConditionImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageElementCondition()
	 * @generated
	 */
	int MESSAGE_ELEMENT_CONDITION = 96;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_CONDITION__DESCRIPTION = MODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_CONDITION__TAG = MODEL_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_CONDITION__VISIBILITY = MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_CONDITION__CONDITION_EXPRESSION = MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Message Element Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_ELEMENT_CONDITION_FEATURE_COUNT = MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ForEachElementImpl <em>For Each Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ForEachElementImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getForEachElement()
	 * @generated
	 */
	int FOR_EACH_ELEMENT = 97;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT__DESCRIPTION = MESSAGE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT__TAG = MESSAGE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Iterator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT__ITERATOR = MESSAGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT__SOURCE = MESSAGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT__BODY = MESSAGE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>For Each Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_ELEMENT_FEATURE_COUNT = MESSAGE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.GrammarImpl <em>Grammar</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.GrammarImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getGrammar()
	 * @generated
	 */
	int GRAMMAR = 98;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Is Computed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__IS_COMPUTED = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Formalism</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__FORMALISM = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__LANGUAGE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__CONTENT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__ENVIRONMENT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Service</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__SERVICE = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Dialog</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__DIALOG = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Dynamic Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR__DYNAMIC_DEFINITION = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Grammar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ServiceImpl <em>Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ServiceImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getService()
	 * @generated
	 */
	int SERVICE = 100;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__OWNED_GRAMMAR = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sub Service</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__SUB_SERVICE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__PARENT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Offered Functionality</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__OFFERED_FUNCTIONALITY = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Entity Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__ENTITY_PACKAGE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Functionality Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__FUNCTIONALITY_PACKAGE = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.VoiceServiceImpl <em>Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.VoiceServiceImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVoiceService()
	 * @generated
	 */
	int VOICE_SERVICE = 99;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__DESCRIPTION = SERVICE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__TAG = SERVICE__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__NAME = SERVICE__NAME;

	/**
	 * The feature id for the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__OWNED_GRAMMAR = SERVICE__OWNED_GRAMMAR;

	/**
	 * The feature id for the '<em><b>Sub Service</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__SUB_SERVICE = SERVICE__SUB_SERVICE;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__PARENT = SERVICE__PARENT;

	/**
	 * The feature id for the '<em><b>Offered Functionality</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__OFFERED_FUNCTIONALITY = SERVICE__OFFERED_FUNCTIONALITY;

	/**
	 * The feature id for the '<em><b>Entity Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__ENTITY_PACKAGE = SERVICE__ENTITY_PACKAGE;

	/**
	 * The feature id for the '<em><b>Functionality Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__FUNCTIONALITY_PACKAGE = SERVICE__FUNCTIONALITY_PACKAGE;

	/**
	 * The feature id for the '<em><b>Main Dialog</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__MAIN_DIALOG = SERVICE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extra Dialog</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE__EXTRA_DIALOG = SERVICE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOICE_SERVICE_FEATURE_COUNT = SERVICE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.FunctionalityImpl <em>Functionality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.FunctionalityImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFunctionality()
	 * @generated
	 */
	int FUNCTIONALITY = 101;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY__DESCRIPTION = PACKAGEABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY__TAG = PACKAGEABLE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY__NAME = PACKAGEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Access Dialog</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY__ACCESS_DIALOG = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY__PARTICIPANT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Functionality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONALITY_FEATURE_COUNT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.ActorImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 102;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__DESCRIPTION = PACKAGEABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TAG = PACKAGEABLE_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAME = PACKAGEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Functionality</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__FUNCTIONALITY = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = PACKAGEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.EntityImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 103;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__DESCRIPTION = CLASS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__TAG = CLASS__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__NAME = CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ATTRIBUTE = CLASS__ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__OPERATION = CLASS__OPERATION;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = CLASS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.impl.EnvironmentImpl <em>Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.impl.EnvironmentImpl
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnvironment()
	 * @generated
	 */
	int ENVIRONMENT = 104;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__DESCRIPTION = NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__TAG = NAMED_ELEMENT__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__OWNED_GRAMMAR = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Defined Service</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__DEFINED_SERVICE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Referenced Service</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__REFERENCED_SERVICE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Predefined Type</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__PREDEFINED_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Predefined Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__PREDEFINED_EVENT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Predefined Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__PREDEFINED_OPERATION = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.ParameterDirectionKind <em>Parameter Direction Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.ParameterDirectionKind
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParameterDirectionKind()
	 * @generated
	 */
	int PARAMETER_DIRECTION_KIND = 105;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.VisibilityKind <em>Visibility Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVisibilityKind()
	 * @generated
	 */
	int VISIBILITY_KIND = 106;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.DTMFKind <em>DTMF Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.DTMFKind
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMFKind()
	 * @generated
	 */
	int DTMF_KIND = 107;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.DigitKind <em>Digit Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.DigitKind
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDigitKind()
	 * @generated
	 */
	int DIGIT_KIND = 108;

	/**
	 * The meta object id for the '{@link com.orange_ftgroup.voice.DiffusionType <em>Diffusion Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDiffusionType()
	 * @generated
	 */
	int DIFFUSION_TYPE = 109;


	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Element</em>'.
	 * @see com.orange_ftgroup.voice.ModelElement
	 * @generated
	 */
	EClass getModelElement();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.ModelElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.orange_ftgroup.voice.ModelElement#getDescription()
	 * @see #getModelElement()
	 * @generated
	 */
	EAttribute getModelElement_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ModelElement#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tag</em>'.
	 * @see com.orange_ftgroup.voice.ModelElement#getTag()
	 * @see #getModelElement()
	 * @generated
	 */
	EReference getModelElement_Tag();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see com.orange_ftgroup.voice.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.orange_ftgroup.voice.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see com.orange_ftgroup.voice.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Expression#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see com.orange_ftgroup.voice.Expression#getText()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_Text();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Element</em>'.
	 * @see com.orange_ftgroup.voice.TypedElement
	 * @generated
	 */
	EClass getTypedElement();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.TypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see com.orange_ftgroup.voice.TypedElement#getType()
	 * @see #getTypedElement()
	 * @generated
	 */
	EReference getTypedElement_Type();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see com.orange_ftgroup.voice.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see com.orange_ftgroup.voice.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Parameter#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see com.orange_ftgroup.voice.Parameter#getDirection()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Direction();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Operation#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variable</em>'.
	 * @see com.orange_ftgroup.voice.Operation#getVariable()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Variable();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Operation#getBehavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Behavior</em>'.
	 * @see com.orange_ftgroup.voice.Operation#getBehavior()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Behavior();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see com.orange_ftgroup.voice.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Constraint#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Specification</em>'.
	 * @see com.orange_ftgroup.voice.Constraint#getSpecification()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_Specification();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Package <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package</em>'.
	 * @see com.orange_ftgroup.voice.Package
	 * @generated
	 */
	EClass getPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Package#getOwnedElement <em>Owned Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Element</em>'.
	 * @see com.orange_ftgroup.voice.Package#getOwnedElement()
	 * @see #getPackage()
	 * @generated
	 */
	EReference getPackage_OwnedElement();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Package#getUsedElement <em>Used Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Used Element</em>'.
	 * @see com.orange_ftgroup.voice.Package#getUsedElement()
	 * @see #getPackage()
	 * @generated
	 */
	EReference getPackage_UsedElement();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type</em>'.
	 * @see com.orange_ftgroup.voice.DataType
	 * @generated
	 */
	EClass getDataType();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Tag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tag</em>'.
	 * @see com.orange_ftgroup.voice.Tag
	 * @generated
	 */
	EClass getTag();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Tag#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.Tag#getValue()
	 * @see #getTag()
	 * @generated
	 */
	EAttribute getTag_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see com.orange_ftgroup.voice.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Property#isIsComposite <em>Is Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Composite</em>'.
	 * @see com.orange_ftgroup.voice.Property#isIsComposite()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_IsComposite();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Property#isIsReadOnly <em>Is Read Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Read Only</em>'.
	 * @see com.orange_ftgroup.voice.Property#isIsReadOnly()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_IsReadOnly();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Property#isIsDerived <em>Is Derived</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Derived</em>'.
	 * @see com.orange_ftgroup.voice.Property#isIsDerived()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_IsDerived();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Property#isIsId <em>Is Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Id</em>'.
	 * @see com.orange_ftgroup.voice.Property#isIsId()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_IsId();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Property#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see com.orange_ftgroup.voice.Property#getVisibility()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Visibility();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Property#getDefault <em>Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default</em>'.
	 * @see com.orange_ftgroup.voice.Property#getDefault()
	 * @see #getProperty()
	 * @generated
	 */
	EReference getProperty_Default();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see com.orange_ftgroup.voice.Class
	 * @generated
	 */
	EClass getClass_();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Class#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute</em>'.
	 * @see com.orange_ftgroup.voice.Class#getAttribute()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Attribute();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Class#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.Class#getOperation()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Operation();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see com.orange_ftgroup.voice.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.BinaryExpression#getOperationName <em>Operation Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation Name</em>'.
	 * @see com.orange_ftgroup.voice.BinaryExpression#getOperationName()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EAttribute getBinaryExpression_OperationName();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.BinaryExpression#getLeftOperand <em>Left Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Operand</em>'.
	 * @see com.orange_ftgroup.voice.BinaryExpression#getLeftOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_LeftOperand();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.BinaryExpression#getRightOperand <em>Right Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Operand</em>'.
	 * @see com.orange_ftgroup.voice.BinaryExpression#getRightOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_RightOperand();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.BinaryExpression#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.BinaryExpression#getOperation()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Operation();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see com.orange_ftgroup.voice.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.UnaryExpression#getOperationName <em>Operation Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation Name</em>'.
	 * @see com.orange_ftgroup.voice.UnaryExpression#getOperationName()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EAttribute getUnaryExpression_OperationName();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.UnaryExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see com.orange_ftgroup.voice.UnaryExpression#getOperand()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Operand();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.UnaryExpression#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.UnaryExpression#getOperation()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Operation();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ValueExpression <em>Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Expression</em>'.
	 * @see com.orange_ftgroup.voice.ValueExpression
	 * @generated
	 */
	EClass getValueExpression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CharacterValue <em>Character Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Character Value</em>'.
	 * @see com.orange_ftgroup.voice.CharacterValue
	 * @generated
	 */
	EClass getCharacterValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.CharacterValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.CharacterValue#getValue()
	 * @see #getCharacterValue()
	 * @generated
	 */
	EAttribute getCharacterValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value</em>'.
	 * @see com.orange_ftgroup.voice.IntegerValue
	 * @generated
	 */
	EClass getIntegerValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.IntegerValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.IntegerValue#getValue()
	 * @see #getIntegerValue()
	 * @generated
	 */
	EAttribute getIntegerValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value</em>'.
	 * @see com.orange_ftgroup.voice.FloatValue
	 * @generated
	 */
	EClass getFloatValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.FloatValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.FloatValue#getValue()
	 * @see #getFloatValue()
	 * @generated
	 */
	EAttribute getFloatValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see com.orange_ftgroup.voice.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.BooleanValue#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.BooleanValue#isValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CharstringValue <em>Charstring Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Charstring Value</em>'.
	 * @see com.orange_ftgroup.voice.CharstringValue
	 * @generated
	 */
	EClass getCharstringValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.CharstringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.CharstringValue#getValue()
	 * @see #getCharstringValue()
	 * @generated
	 */
	EAttribute getCharstringValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Ident <em>Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ident</em>'.
	 * @see com.orange_ftgroup.voice.Ident
	 * @generated
	 */
	EClass getIdent();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Ident#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.Ident#getValue()
	 * @see #getIdent()
	 * @generated
	 */
	EAttribute getIdent_Value();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Ident#getValueContainer <em>Value Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Container</em>'.
	 * @see com.orange_ftgroup.voice.Ident#getValueContainer()
	 * @see #getIdent()
	 * @generated
	 */
	EReference getIdent_ValueContainer();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ParenthesisExpression <em>Parenthesis Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parenthesis Expression</em>'.
	 * @see com.orange_ftgroup.voice.ParenthesisExpression
	 * @generated
	 */
	EClass getParenthesisExpression();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ParenthesisExpression#getInnerExpression <em>Inner Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inner Expression</em>'.
	 * @see com.orange_ftgroup.voice.ParenthesisExpression#getInnerExpression()
	 * @see #getParenthesisExpression()
	 * @generated
	 */
	EReference getParenthesisExpression_InnerExpression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ConditionalExpression <em>Conditional Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Expression</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalExpression
	 * @generated
	 */
	EClass getConditionalExpression();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ConditionalExpression#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalExpression#getCondition()
	 * @see #getConditionalExpression()
	 * @generated
	 */
	EReference getConditionalExpression_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ConditionalExpression#getThenPart <em>Then Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Part</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalExpression#getThenPart()
	 * @see #getConditionalExpression()
	 * @generated
	 */
	EReference getConditionalExpression_ThenPart();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ConditionalExpression#getElsePart <em>Else Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Part</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalExpression#getElsePart()
	 * @see #getConditionalExpression()
	 * @generated
	 */
	EReference getConditionalExpression_ElsePart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ListExpression <em>List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Expression</em>'.
	 * @see com.orange_ftgroup.voice.ListExpression
	 * @generated
	 */
	EClass getListExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ListExpression#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element</em>'.
	 * @see com.orange_ftgroup.voice.ListExpression#getElement()
	 * @see #getListExpression()
	 * @generated
	 */
	EReference getListExpression_Element();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CallExpression <em>Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Expression</em>'.
	 * @see com.orange_ftgroup.voice.CallExpression
	 * @generated
	 */
	EClass getCallExpression();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.CallExpression#getOperationName <em>Operation Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation Name</em>'.
	 * @see com.orange_ftgroup.voice.CallExpression#getOperationName()
	 * @see #getCallExpression()
	 * @generated
	 */
	EAttribute getCallExpression_OperationName();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.CallExpression#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument</em>'.
	 * @see com.orange_ftgroup.voice.CallExpression#getArgument()
	 * @see #getCallExpression()
	 * @generated
	 */
	EReference getCallExpression_Argument();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.CallExpression#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.CallExpression#getOperation()
	 * @see #getCallExpression()
	 * @generated
	 */
	EReference getCallExpression_Operation();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.EnumerationType <em>Enumeration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Type</em>'.
	 * @see com.orange_ftgroup.voice.EnumerationType
	 * @generated
	 */
	EClass getEnumerationType();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.EnumerationType#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Item</em>'.
	 * @see com.orange_ftgroup.voice.EnumerationType#getItem()
	 * @see #getEnumerationType()
	 * @generated
	 */
	EReference getEnumerationType_Item();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.EnumerationItem <em>Enumeration Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Item</em>'.
	 * @see com.orange_ftgroup.voice.EnumerationItem
	 * @generated
	 */
	EClass getEnumerationItem();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ChoiceType <em>Choice Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice Type</em>'.
	 * @see com.orange_ftgroup.voice.ChoiceType
	 * @generated
	 */
	EClass getChoiceType();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ChoiceType#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute</em>'.
	 * @see com.orange_ftgroup.voice.ChoiceType#getAttribute()
	 * @see #getChoiceType()
	 * @generated
	 */
	EReference getChoiceType_Attribute();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ListType <em>List Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Type</em>'.
	 * @see com.orange_ftgroup.voice.ListType
	 * @generated
	 */
	EClass getListType();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.ListType#getContentType <em>Content Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Content Type</em>'.
	 * @see com.orange_ftgroup.voice.ListType#getContentType()
	 * @see #getListType()
	 * @generated
	 */
	EReference getListType_ContentType();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see com.orange_ftgroup.voice.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Variable#isIsShared <em>Is Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Shared</em>'.
	 * @see com.orange_ftgroup.voice.Variable#isIsShared()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_IsShared();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signature</em>'.
	 * @see com.orange_ftgroup.voice.Signature
	 * @generated
	 */
	EClass getSignature();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Signature#getFormalParameter <em>Formal Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Formal Parameter</em>'.
	 * @see com.orange_ftgroup.voice.Signature#getFormalParameter()
	 * @see #getSignature()
	 * @generated
	 */
	EReference getSignature_FormalParameter();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.InformalExpression <em>Informal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Informal Expression</em>'.
	 * @see com.orange_ftgroup.voice.InformalExpression
	 * @generated
	 */
	EClass getInformalExpression();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.InformalExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.InformalExpression#getValue()
	 * @see #getInformalExpression()
	 * @generated
	 */
	EAttribute getInformalExpression_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.PackageableElement <em>Packageable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Packageable Element</em>'.
	 * @see com.orange_ftgroup.voice.PackageableElement
	 * @generated
	 */
	EClass getPackageableElement();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ActionSequence <em>Action Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Sequence</em>'.
	 * @see com.orange_ftgroup.voice.ActionSequence
	 * @generated
	 */
	EClass getActionSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ActionSequence#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see com.orange_ftgroup.voice.ActionSequence#getAction()
	 * @see #getActionSequence()
	 * @generated
	 */
	EReference getActionSequence_Action();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.ActionSequence#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Message</em>'.
	 * @see com.orange_ftgroup.voice.ActionSequence#getMessage()
	 * @see #getActionSequence()
	 * @generated
	 */
	EReference getActionSequence_Message();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see com.orange_ftgroup.voice.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Action#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see com.orange_ftgroup.voice.Action#getText()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Text();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.Action#getActionSequence <em>Action Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Action Sequence</em>'.
	 * @see com.orange_ftgroup.voice.Action#getActionSequence()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_ActionSequence();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DigitValue <em>Digit Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Digit Value</em>'.
	 * @see com.orange_ftgroup.voice.DigitValue
	 * @generated
	 */
	EClass getDigitValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.DigitValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.DigitValue#getValue()
	 * @see #getDigitValue()
	 * @generated
	 */
	EAttribute getDigitValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DTMFValue <em>DTMF Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DTMF Value</em>'.
	 * @see com.orange_ftgroup.voice.DTMFValue
	 * @generated
	 */
	EClass getDTMFValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.DTMFValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.DTMFValue#getValue()
	 * @see #getDTMFValue()
	 * @generated
	 */
	EAttribute getDTMFValue_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.FeatureExpression <em>Feature Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Expression</em>'.
	 * @see com.orange_ftgroup.voice.FeatureExpression
	 * @generated
	 */
	EClass getFeatureExpression();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.FeatureExpression#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see com.orange_ftgroup.voice.FeatureExpression#getSource()
	 * @see #getFeatureExpression()
	 * @generated
	 */
	EReference getFeatureExpression_Source();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.PropertyExpression <em>Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Expression</em>'.
	 * @see com.orange_ftgroup.voice.PropertyExpression
	 * @generated
	 */
	EClass getPropertyExpression();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.PropertyExpression#getReferredProperty <em>Referred Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referred Property</em>'.
	 * @see com.orange_ftgroup.voice.PropertyExpression#getReferredProperty()
	 * @see #getPropertyExpression()
	 * @generated
	 */
	EReference getPropertyExpression_ReferredProperty();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CollectionRange <em>Collection Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection Range</em>'.
	 * @see com.orange_ftgroup.voice.CollectionRange
	 * @generated
	 */
	EClass getCollectionRange();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.CollectionRange#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see com.orange_ftgroup.voice.CollectionRange#getFirst()
	 * @see #getCollectionRange()
	 * @generated
	 */
	EReference getCollectionRange_First();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.CollectionRange#getLast <em>Last</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Last</em>'.
	 * @see com.orange_ftgroup.voice.CollectionRange#getLast()
	 * @see #getCollectionRange()
	 * @generated
	 */
	EReference getCollectionRange_Last();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CollectionLiteralExpression <em>Collection Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection Literal Expression</em>'.
	 * @see com.orange_ftgroup.voice.CollectionLiteralExpression
	 * @generated
	 */
	EClass getCollectionLiteralExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.CollectionLiteralExpression#getPart <em>Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Part</em>'.
	 * @see com.orange_ftgroup.voice.CollectionLiteralExpression#getPart()
	 * @see #getCollectionLiteralExpression()
	 * @generated
	 */
	EReference getCollectionLiteralExpression_Part();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CollectionItem <em>Collection Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection Item</em>'.
	 * @see com.orange_ftgroup.voice.CollectionItem
	 * @generated
	 */
	EClass getCollectionItem();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.CollectionItem#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Item</em>'.
	 * @see com.orange_ftgroup.voice.CollectionItem#getItem()
	 * @see #getCollectionItem()
	 * @generated
	 */
	EReference getCollectionItem_Item();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CollectionLiteralPart <em>Collection Literal Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection Literal Part</em>'.
	 * @see com.orange_ftgroup.voice.CollectionLiteralPart
	 * @generated
	 */
	EClass getCollectionLiteralPart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Value</em>'.
	 * @see com.orange_ftgroup.voice.EnumValue
	 * @generated
	 */
	EClass getEnumValue();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.EnumValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.EnumValue#getValue()
	 * @see #getEnumValue()
	 * @generated
	 */
	EAttribute getEnumValue_Value();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.EnumValue#getLiteralDefinition <em>Literal Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Literal Definition</em>'.
	 * @see com.orange_ftgroup.voice.EnumValue#getLiteralDefinition()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_LiteralDefinition();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Behavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavior</em>'.
	 * @see com.orange_ftgroup.voice.Behavior
	 * @generated
	 */
	EClass getBehavior();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine</em>'.
	 * @see com.orange_ftgroup.voice.StateMachine
	 * @generated
	 */
	EClass getStateMachine();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.StateMachine#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Node</em>'.
	 * @see com.orange_ftgroup.voice.StateMachine#getNode()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Node();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.StateMachine#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transition</em>'.
	 * @see com.orange_ftgroup.voice.StateMachine#getTransition()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Transition();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.NewExpression <em>New Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Expression</em>'.
	 * @see com.orange_ftgroup.voice.NewExpression
	 * @generated
	 */
	EClass getNewExpression();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.NewExpression#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class</em>'.
	 * @see com.orange_ftgroup.voice.NewExpression#getClass_()
	 * @see #getNewExpression()
	 * @generated
	 */
	EReference getNewExpression_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.NewExpression#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument</em>'.
	 * @see com.orange_ftgroup.voice.NewExpression#getArgument()
	 * @see #getNewExpression()
	 * @generated
	 */
	EReference getNewExpression_Argument();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ValueContainer <em>Value Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Container</em>'.
	 * @see com.orange_ftgroup.voice.ValueContainer
	 * @generated
	 */
	EClass getValueContainer();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Dialog <em>Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dialog</em>'.
	 * @see com.orange_ftgroup.voice.Dialog
	 * @generated
	 */
	EClass getDialog();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Node</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getNode()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Node();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Dialog#getAccessedFunctionality <em>Accessed Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Accessed Functionality</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getAccessedFunctionality()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_AccessedFunctionality();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transition</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getTransition()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Transition();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getMessage()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Message();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getConcept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concept</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getConcept()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Concept();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getMessagePart <em>Message Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Part</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getMessagePart()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_MessagePart();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getExternalEvent <em>External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>External Event</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getExternalEvent()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_ExternalEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getCondition()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variable</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getVariable()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Variable();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getSubDialog <em>Sub Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Dialog</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getSubDialog()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_SubDialog();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getGlobalVariable <em>Global Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Variable</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getGlobalVariable()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_GlobalVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getReferencedVariable <em>Referenced Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Referenced Variable</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getReferencedVariable()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_ReferencedVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getReferencedMessage <em>Referenced Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Referenced Message</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getReferencedMessage()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_ReferencedMessage();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getReferencedInputEvent <em>Referenced Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Referenced Input Event</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getReferencedInputEvent()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_ReferencedInputEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operation</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getOperation()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_Operation();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Dialog#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Grammar</em>'.
	 * @see com.orange_ftgroup.voice.Dialog#getOwnedGrammar()
	 * @see #getDialog()
	 * @generated
	 */
	EReference getDialog_OwnedGrammar();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.SubDialogState <em>Sub Dialog State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Dialog State</em>'.
	 * @see com.orange_ftgroup.voice.SubDialogState
	 * @generated
	 */
	EClass getSubDialogState();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.SubDialogState#getCalled <em>Called</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Called</em>'.
	 * @see com.orange_ftgroup.voice.SubDialogState#getCalled()
	 * @see #getSubDialogState()
	 * @generated
	 */
	EReference getSubDialogState_Called();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.SubDialogState#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument</em>'.
	 * @see com.orange_ftgroup.voice.SubDialogState#getArgument()
	 * @see #getSubDialogState()
	 * @generated
	 */
	EReference getSubDialogState_Argument();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DialogState <em>Dialog State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dialog State</em>'.
	 * @see com.orange_ftgroup.voice.DialogState
	 * @generated
	 */
	EClass getDialogState();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.DialogState#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entry</em>'.
	 * @see com.orange_ftgroup.voice.DialogState#getEntry()
	 * @see #getDialogState()
	 * @generated
	 */
	EReference getDialogState_Entry();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DialogNode <em>Dialog Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dialog Node</em>'.
	 * @see com.orange_ftgroup.voice.DialogNode
	 * @generated
	 */
	EClass getDialogNode();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.DialogNode#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see com.orange_ftgroup.voice.DialogNode#getOwner()
	 * @see #getDialogNode()
	 * @generated
	 */
	EReference getDialogNode_Owner();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.DialogNode#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see com.orange_ftgroup.voice.DialogNode#getOutgoing()
	 * @see #getDialogNode()
	 * @generated
	 */
	EReference getDialogNode_Outgoing();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.DialogNode#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see com.orange_ftgroup.voice.DialogNode#getIncoming()
	 * @see #getDialogNode()
	 * @generated
	 */
	EReference getDialogNode_Incoming();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see com.orange_ftgroup.voice.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Transition#isIsElse <em>Is Else</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Else</em>'.
	 * @see com.orange_ftgroup.voice.Transition#isIsElse()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_IsElse();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Transition#getEffect <em>Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Effect</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getEffect()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Effect();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Transition#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trigger</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getTrigger()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Trigger();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Transition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getGuard()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Guard();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.Transition#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getOwner()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Owner();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Transition#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Origin</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getOrigin()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Origin();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see com.orange_ftgroup.voice.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trigger</em>'.
	 * @see com.orange_ftgroup.voice.Trigger
	 * @generated
	 */
	EClass getTrigger();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Trigger#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see com.orange_ftgroup.voice.Trigger#getEvent()
	 * @see #getTrigger()
	 * @generated
	 */
	EReference getTrigger_Event();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Trigger#getArgHandle <em>Arg Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Arg Handle</em>'.
	 * @see com.orange_ftgroup.voice.Trigger#getArgHandle()
	 * @see #getTrigger()
	 * @generated
	 */
	EReference getTrigger_ArgHandle();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.TransientNode <em>Transient Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transient Node</em>'.
	 * @see com.orange_ftgroup.voice.TransientNode
	 * @generated
	 */
	EClass getTransientNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.WaitState <em>Wait State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait State</em>'.
	 * @see com.orange_ftgroup.voice.WaitState
	 * @generated
	 */
	EClass getWaitState();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.WaitState#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see com.orange_ftgroup.voice.WaitState#getDelay()
	 * @see #getWaitState()
	 * @generated
	 */
	EAttribute getWaitState_Delay();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.WaitState#getGrammar <em>Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Grammar</em>'.
	 * @see com.orange_ftgroup.voice.WaitState#getGrammar()
	 * @see #getWaitState()
	 * @generated
	 */
	EReference getWaitState_Grammar();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.AnyState <em>Any State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Any State</em>'.
	 * @see com.orange_ftgroup.voice.AnyState
	 * @generated
	 */
	EClass getAnyState();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ListState <em>List State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List State</em>'.
	 * @see com.orange_ftgroup.voice.ListState
	 * @generated
	 */
	EClass getListState();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.ListState#isExcluded <em>Excluded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Excluded</em>'.
	 * @see com.orange_ftgroup.voice.ListState#isExcluded()
	 * @see #getListState()
	 * @generated
	 */
	EAttribute getListState_Excluded();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.ListState#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>State</em>'.
	 * @see com.orange_ftgroup.voice.ListState#getState()
	 * @see #getListState()
	 * @generated
	 */
	EReference getListState_State();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DecisionNode <em>Decision Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decision Node</em>'.
	 * @see com.orange_ftgroup.voice.DecisionNode
	 * @generated
	 */
	EClass getDecisionNode();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.DecisionNode#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.DecisionNode#getCondition()
	 * @see #getDecisionNode()
	 * @generated
	 */
	EReference getDecisionNode_Condition();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.InitialNode <em>Initial Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial Node</em>'.
	 * @see com.orange_ftgroup.voice.InitialNode
	 * @generated
	 */
	EClass getInitialNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.JunctionNode <em>Junction Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Junction Node</em>'.
	 * @see com.orange_ftgroup.voice.JunctionNode
	 * @generated
	 */
	EClass getJunctionNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DiversionNode <em>Diversion Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diversion Node</em>'.
	 * @see com.orange_ftgroup.voice.DiversionNode
	 * @generated
	 */
	EClass getDiversionNode();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.DiversionNode#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see com.orange_ftgroup.voice.DiversionNode#getTarget()
	 * @see #getDiversionNode()
	 * @generated
	 */
	EReference getDiversionNode_Target();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.DiversionNode#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument</em>'.
	 * @see com.orange_ftgroup.voice.DiversionNode#getArgument()
	 * @see #getDiversionNode()
	 * @generated
	 */
	EReference getDiversionNode_Argument();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.HistoryState <em>History State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History State</em>'.
	 * @see com.orange_ftgroup.voice.HistoryState
	 * @generated
	 */
	EClass getHistoryState();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.FinalNode <em>Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Final Node</em>'.
	 * @see com.orange_ftgroup.voice.FinalNode
	 * @generated
	 */
	EClass getFinalNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.NextNode <em>Next Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Next Node</em>'.
	 * @see com.orange_ftgroup.voice.NextNode
	 * @generated
	 */
	EClass getNextNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.StopNode <em>Stop Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stop Node</em>'.
	 * @see com.orange_ftgroup.voice.StopNode
	 * @generated
	 */
	EClass getStopNode();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ReturnNode <em>Return Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Node</em>'.
	 * @see com.orange_ftgroup.voice.ReturnNode
	 * @generated
	 */
	EClass getReturnNode();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ReturnNode#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see com.orange_ftgroup.voice.ReturnNode#getExpression()
	 * @see #getReturnNode()
	 * @generated
	 */
	EReference getReturnNode_Expression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see com.orange_ftgroup.voice.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Assignment#getValueContainer <em>Value Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Container</em>'.
	 * @see com.orange_ftgroup.voice.Assignment#getValueContainer()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_ValueContainer();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Assignment#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.Assignment#getValue()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_Value();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Play <em>Play</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Play</em>'.
	 * @see com.orange_ftgroup.voice.Play
	 * @generated
	 */
	EClass getPlay();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Play#isInterruptible <em>Interruptible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interruptible</em>'.
	 * @see com.orange_ftgroup.voice.Play#isInterruptible()
	 * @see #getPlay()
	 * @generated
	 */
	EAttribute getPlay_Interruptible();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Play#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Message</em>'.
	 * @see com.orange_ftgroup.voice.Play#getMessage()
	 * @see #getPlay()
	 * @generated
	 */
	EReference getPlay_Message();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Play#getMessageArgument <em>Message Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Argument</em>'.
	 * @see com.orange_ftgroup.voice.Play#getMessageArgument()
	 * @see #getPlay()
	 * @generated
	 */
	EReference getPlay_MessageArgument();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Uninterpreted <em>Uninterpreted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uninterpreted</em>'.
	 * @see com.orange_ftgroup.voice.Uninterpreted
	 * @generated
	 */
	EClass getUninterpreted();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Uninterpreted#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Body</em>'.
	 * @see com.orange_ftgroup.voice.Uninterpreted#getBody()
	 * @see #getUninterpreted()
	 * @generated
	 */
	EAttribute getUninterpreted_Body();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.IfThenElse <em>If Then Else</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Then Else</em>'.
	 * @see com.orange_ftgroup.voice.IfThenElse
	 * @generated
	 */
	EClass getIfThenElse();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.IfThenElse#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.IfThenElse#getCondition()
	 * @see #getIfThenElse()
	 * @generated
	 */
	EReference getIfThenElse_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.IfThenElse#getThenPart <em>Then Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Part</em>'.
	 * @see com.orange_ftgroup.voice.IfThenElse#getThenPart()
	 * @see #getIfThenElse()
	 * @generated
	 */
	EReference getIfThenElse_ThenPart();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.IfThenElse#getElsePart <em>Else Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Part</em>'.
	 * @see com.orange_ftgroup.voice.IfThenElse#getElsePart()
	 * @see #getIfThenElse()
	 * @generated
	 */
	EReference getIfThenElse_ElsePart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.While <em>While</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While</em>'.
	 * @see com.orange_ftgroup.voice.While
	 * @generated
	 */
	EClass getWhile();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.While#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.While#getCondition()
	 * @see #getWhile()
	 * @generated
	 */
	EReference getWhile_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.While#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see com.orange_ftgroup.voice.While#getBody()
	 * @see #getWhile()
	 * @generated
	 */
	EReference getWhile_Body();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ReturnAction <em>Return Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Action</em>'.
	 * @see com.orange_ftgroup.voice.ReturnAction
	 * @generated
	 */
	EClass getReturnAction();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ReturnAction#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see com.orange_ftgroup.voice.ReturnAction#getExpression()
	 * @see #getReturnAction()
	 * @generated
	 */
	EReference getReturnAction_Expression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Action</em>'.
	 * @see com.orange_ftgroup.voice.CallAction
	 * @generated
	 */
	EClass getCallAction();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.CallAction#getCallExpression <em>Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Call Expression</em>'.
	 * @see com.orange_ftgroup.voice.CallAction#getCallExpression()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_CallExpression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.InputEvent <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Event</em>'.
	 * @see com.orange_ftgroup.voice.InputEvent
	 * @generated
	 */
	EClass getInputEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.InputEvent#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see com.orange_ftgroup.voice.InputEvent#getParameter()
	 * @see #getInputEvent()
	 * @generated
	 */
	EReference getInputEvent_Parameter();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DTMF <em>DTMF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DTMF</em>'.
	 * @see com.orange_ftgroup.voice.DTMF
	 * @generated
	 */
	EClass getDTMF();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.DTMF#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see com.orange_ftgroup.voice.DTMF#getKey()
	 * @see #getDTMF()
	 * @generated
	 */
	EAttribute getDTMF_Key();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Reject <em>Reject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reject</em>'.
	 * @see com.orange_ftgroup.voice.Reject
	 * @generated
	 */
	EClass getReject();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Inactivity <em>Inactivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inactivity</em>'.
	 * @see com.orange_ftgroup.voice.Inactivity
	 * @generated
	 */
	EClass getInactivity();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Concept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept</em>'.
	 * @see com.orange_ftgroup.voice.Concept
	 * @generated
	 */
	EClass getConcept();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Concept#getGrammar <em>Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Grammar</em>'.
	 * @see com.orange_ftgroup.voice.Concept#getGrammar()
	 * @see #getConcept()
	 * @generated
	 */
	EReference getConcept_Grammar();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ExternalEvent <em>External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Event</em>'.
	 * @see com.orange_ftgroup.voice.ExternalEvent
	 * @generated
	 */
	EClass getExternalEvent();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.AnyDtmf <em>Any Dtmf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Any Dtmf</em>'.
	 * @see com.orange_ftgroup.voice.AnyDtmf
	 * @generated
	 */
	EClass getAnyDtmf();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.AnyDigit <em>Any Digit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Any Digit</em>'.
	 * @see com.orange_ftgroup.voice.AnyDigit
	 * @generated
	 */
	EClass getAnyDigit();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Recording <em>Recording</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Recording</em>'.
	 * @see com.orange_ftgroup.voice.Recording
	 * @generated
	 */
	EClass getRecording();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.AbstractDTMF <em>Abstract DTMF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract DTMF</em>'.
	 * @see com.orange_ftgroup.voice.AbstractDTMF
	 * @generated
	 */
	EClass getAbstractDTMF();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.MessageEnd <em>Message End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message End</em>'.
	 * @see com.orange_ftgroup.voice.MessageEnd
	 * @generated
	 */
	EClass getMessageEnd();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.DtmfInput <em>Dtmf Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dtmf Input</em>'.
	 * @see com.orange_ftgroup.voice.DtmfInput
	 * @generated
	 */
	EClass getDtmfInput();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.SystemInput <em>System Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Input</em>'.
	 * @see com.orange_ftgroup.voice.SystemInput
	 * @generated
	 */
	EClass getSystemInput();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message</em>'.
	 * @see com.orange_ftgroup.voice.Message
	 * @generated
	 */
	EClass getMessage();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Message#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see com.orange_ftgroup.voice.Message#getVisibility()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Visibility();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Message#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Body</em>'.
	 * @see com.orange_ftgroup.voice.Message#getBody()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Body();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Message#getMessageElement <em>Message Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Element</em>'.
	 * @see com.orange_ftgroup.voice.Message#getMessageElement()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_MessageElement();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Message#getActionSpecification <em>Action Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action Specification</em>'.
	 * @see com.orange_ftgroup.voice.Message#getActionSpecification()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_ActionSpecification();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.MessagePart <em>Message Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Part</em>'.
	 * @see com.orange_ftgroup.voice.MessagePart
	 * @generated
	 */
	EClass getMessagePart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.FixPart <em>Fix Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fix Part</em>'.
	 * @see com.orange_ftgroup.voice.FixPart
	 * @generated
	 */
	EClass getFixPart();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.FixPart#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see com.orange_ftgroup.voice.FixPart#getContent()
	 * @see #getFixPart()
	 * @generated
	 */
	EAttribute getFixPart_Content();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.FixPart#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see com.orange_ftgroup.voice.FixPart#getFormat()
	 * @see #getFixPart()
	 * @generated
	 */
	EAttribute getFixPart_Format();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.VariablePart <em>Variable Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Part</em>'.
	 * @see com.orange_ftgroup.voice.VariablePart
	 * @generated
	 */
	EClass getVariablePart();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.VariablePart#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see com.orange_ftgroup.voice.VariablePart#getFormat()
	 * @see #getVariablePart()
	 * @generated
	 */
	EAttribute getVariablePart_Format();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.VariablePart#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see com.orange_ftgroup.voice.VariablePart#getVisibility()
	 * @see #getVariablePart()
	 * @generated
	 */
	EAttribute getVariablePart_Visibility();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.VariablePart#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see com.orange_ftgroup.voice.VariablePart#getValue()
	 * @see #getVariablePart()
	 * @generated
	 */
	EReference getVariablePart_Value();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.VariablePart#getActionSpecification <em>Action Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action Specification</em>'.
	 * @see com.orange_ftgroup.voice.VariablePart#getActionSpecification()
	 * @see #getVariablePart()
	 * @generated
	 */
	EReference getVariablePart_ActionSpecification();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ConditionalElement <em>Conditional Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Element</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalElement
	 * @generated
	 */
	EClass getConditionalElement();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.ConditionalElement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Condition</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalElement#getCondition()
	 * @see #getConditionalElement()
	 * @generated
	 */
	EReference getConditionalElement_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ConditionalElement#getThenPart <em>Then Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Then Part</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalElement#getThenPart()
	 * @see #getConditionalElement()
	 * @generated
	 */
	EReference getConditionalElement_ThenPart();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ConditionalElement#getElsePart <em>Else Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Else Part</em>'.
	 * @see com.orange_ftgroup.voice.ConditionalElement#getElsePart()
	 * @see #getConditionalElement()
	 * @generated
	 */
	EReference getConditionalElement_ElsePart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.SilencePart <em>Silence Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Silence Part</em>'.
	 * @see com.orange_ftgroup.voice.SilencePart
	 * @generated
	 */
	EClass getSilencePart();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.SilencePart#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Duration</em>'.
	 * @see com.orange_ftgroup.voice.SilencePart#getDuration()
	 * @see #getSilencePart()
	 * @generated
	 */
	EReference getSilencePart_Duration();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.MessageElement <em>Message Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Element</em>'.
	 * @see com.orange_ftgroup.voice.MessageElement
	 * @generated
	 */
	EClass getMessageElement();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.UseElement <em>Use Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Element</em>'.
	 * @see com.orange_ftgroup.voice.UseElement
	 * @generated
	 */
	EClass getUseElement();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.UseElement#getUsedMessagePart <em>Used Message Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Used Message Part</em>'.
	 * @see com.orange_ftgroup.voice.UseElement#getUsedMessagePart()
	 * @see #getUseElement()
	 * @generated
	 */
	EReference getUseElement_UsedMessagePart();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.MessageElementCondition <em>Message Element Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Element Condition</em>'.
	 * @see com.orange_ftgroup.voice.MessageElementCondition
	 * @generated
	 */
	EClass getMessageElementCondition();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.MessageElementCondition#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see com.orange_ftgroup.voice.MessageElementCondition#getVisibility()
	 * @see #getMessageElementCondition()
	 * @generated
	 */
	EAttribute getMessageElementCondition_Visibility();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.MessageElementCondition#getConditionExpression <em>Condition Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition Expression</em>'.
	 * @see com.orange_ftgroup.voice.MessageElementCondition#getConditionExpression()
	 * @see #getMessageElementCondition()
	 * @generated
	 */
	EReference getMessageElementCondition_ConditionExpression();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.ForEachElement <em>For Each Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Each Element</em>'.
	 * @see com.orange_ftgroup.voice.ForEachElement
	 * @generated
	 */
	EClass getForEachElement();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ForEachElement#getIterator <em>Iterator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iterator</em>'.
	 * @see com.orange_ftgroup.voice.ForEachElement#getIterator()
	 * @see #getForEachElement()
	 * @generated
	 */
	EReference getForEachElement_Iterator();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.ForEachElement#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see com.orange_ftgroup.voice.ForEachElement#getSource()
	 * @see #getForEachElement()
	 * @generated
	 */
	EReference getForEachElement_Source();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.ForEachElement#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Body</em>'.
	 * @see com.orange_ftgroup.voice.ForEachElement#getBody()
	 * @see #getForEachElement()
	 * @generated
	 */
	EReference getForEachElement_Body();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Grammar <em>Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grammar</em>'.
	 * @see com.orange_ftgroup.voice.Grammar
	 * @generated
	 */
	EClass getGrammar();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Grammar#isIsComputed <em>Is Computed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Computed</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#isIsComputed()
	 * @see #getGrammar()
	 * @generated
	 */
	EAttribute getGrammar_IsComputed();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Grammar#getFormalism <em>Formalism</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formalism</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getFormalism()
	 * @see #getGrammar()
	 * @generated
	 */
	EAttribute getGrammar_Formalism();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Grammar#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getLanguage()
	 * @see #getGrammar()
	 * @generated
	 */
	EAttribute getGrammar_Language();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Grammar#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getContent()
	 * @see #getGrammar()
	 * @generated
	 */
	EAttribute getGrammar_Content();

	/**
	 * Returns the meta object for the attribute '{@link com.orange_ftgroup.voice.Grammar#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getLocation()
	 * @see #getGrammar()
	 * @generated
	 */
	EAttribute getGrammar_Location();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.Grammar#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Environment</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getEnvironment()
	 * @see #getGrammar()
	 * @generated
	 */
	EReference getGrammar_Environment();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.Grammar#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Service</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getService()
	 * @see #getGrammar()
	 * @generated
	 */
	EReference getGrammar_Service();

	/**
	 * Returns the meta object for the container reference '{@link com.orange_ftgroup.voice.Grammar#getDialog <em>Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Dialog</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getDialog()
	 * @see #getGrammar()
	 * @generated
	 */
	EReference getGrammar_Dialog();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Grammar#getDynamicDefinition <em>Dynamic Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dynamic Definition</em>'.
	 * @see com.orange_ftgroup.voice.Grammar#getDynamicDefinition()
	 * @see #getGrammar()
	 * @generated
	 */
	EReference getGrammar_DynamicDefinition();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.VoiceService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service</em>'.
	 * @see com.orange_ftgroup.voice.VoiceService
	 * @generated
	 */
	EClass getVoiceService();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.VoiceService#getMainDialog <em>Main Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main Dialog</em>'.
	 * @see com.orange_ftgroup.voice.VoiceService#getMainDialog()
	 * @see #getVoiceService()
	 * @generated
	 */
	EReference getVoiceService_MainDialog();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.VoiceService#getExtraDialog <em>Extra Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extra Dialog</em>'.
	 * @see com.orange_ftgroup.voice.VoiceService#getExtraDialog()
	 * @see #getVoiceService()
	 * @generated
	 */
	EReference getVoiceService_ExtraDialog();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Service <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service</em>'.
	 * @see com.orange_ftgroup.voice.Service
	 * @generated
	 */
	EClass getService();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Service#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Grammar</em>'.
	 * @see com.orange_ftgroup.voice.Service#getOwnedGrammar()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_OwnedGrammar();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Service#getSubService <em>Sub Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Service</em>'.
	 * @see com.orange_ftgroup.voice.Service#getSubService()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_SubService();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Service#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Parent</em>'.
	 * @see com.orange_ftgroup.voice.Service#getParent()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Parent();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Service#getOfferedFunctionality <em>Offered Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Offered Functionality</em>'.
	 * @see com.orange_ftgroup.voice.Service#getOfferedFunctionality()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_OfferedFunctionality();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Service#getEntityPackage <em>Entity Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity Package</em>'.
	 * @see com.orange_ftgroup.voice.Service#getEntityPackage()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_EntityPackage();

	/**
	 * Returns the meta object for the containment reference '{@link com.orange_ftgroup.voice.Service#getFunctionalityPackage <em>Functionality Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Functionality Package</em>'.
	 * @see com.orange_ftgroup.voice.Service#getFunctionalityPackage()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_FunctionalityPackage();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Functionality <em>Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functionality</em>'.
	 * @see com.orange_ftgroup.voice.Functionality
	 * @generated
	 */
	EClass getFunctionality();

	/**
	 * Returns the meta object for the reference '{@link com.orange_ftgroup.voice.Functionality#getAccessDialog <em>Access Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Access Dialog</em>'.
	 * @see com.orange_ftgroup.voice.Functionality#getAccessDialog()
	 * @see #getFunctionality()
	 * @generated
	 */
	EReference getFunctionality_AccessDialog();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Functionality#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participant</em>'.
	 * @see com.orange_ftgroup.voice.Functionality#getParticipant()
	 * @see #getFunctionality()
	 * @generated
	 */
	EReference getFunctionality_Participant();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see com.orange_ftgroup.voice.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the reference list '{@link com.orange_ftgroup.voice.Actor#getFunctionality <em>Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Functionality</em>'.
	 * @see com.orange_ftgroup.voice.Actor#getFunctionality()
	 * @see #getActor()
	 * @generated
	 */
	EReference getActor_Functionality();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see com.orange_ftgroup.voice.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for class '{@link com.orange_ftgroup.voice.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment</em>'.
	 * @see com.orange_ftgroup.voice.Environment
	 * @generated
	 */
	EClass getEnvironment();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Grammar</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getOwnedGrammar()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_OwnedGrammar();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getDefinedService <em>Defined Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Defined Service</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getDefinedService()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_DefinedService();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getReferencedService <em>Referenced Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Referenced Service</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getReferencedService()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_ReferencedService();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getPredefinedType <em>Predefined Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Predefined Type</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getPredefinedType()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_PredefinedType();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getPredefinedEvent <em>Predefined Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Predefined Event</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getPredefinedEvent()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_PredefinedEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.orange_ftgroup.voice.Environment#getPredefinedOperation <em>Predefined Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Predefined Operation</em>'.
	 * @see com.orange_ftgroup.voice.Environment#getPredefinedOperation()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_PredefinedOperation();

	/**
	 * Returns the meta object for enum '{@link com.orange_ftgroup.voice.ParameterDirectionKind <em>Parameter Direction Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Parameter Direction Kind</em>'.
	 * @see com.orange_ftgroup.voice.ParameterDirectionKind
	 * @generated
	 */
	EEnum getParameterDirectionKind();

	/**
	 * Returns the meta object for enum '{@link com.orange_ftgroup.voice.VisibilityKind <em>Visibility Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Visibility Kind</em>'.
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @generated
	 */
	EEnum getVisibilityKind();

	/**
	 * Returns the meta object for enum '{@link com.orange_ftgroup.voice.DTMFKind <em>DTMF Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DTMF Kind</em>'.
	 * @see com.orange_ftgroup.voice.DTMFKind
	 * @generated
	 */
	EEnum getDTMFKind();

	/**
	 * Returns the meta object for enum '{@link com.orange_ftgroup.voice.DigitKind <em>Digit Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Digit Kind</em>'.
	 * @see com.orange_ftgroup.voice.DigitKind
	 * @generated
	 */
	EEnum getDigitKind();

	/**
	 * Returns the meta object for enum '{@link com.orange_ftgroup.voice.DiffusionType <em>Diffusion Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Diffusion Type</em>'.
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @generated
	 */
	EEnum getDiffusionType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VoiceFactory getVoiceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ModelElementImpl <em>Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ModelElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getModelElement()
		 * @generated
		 */
		EClass MODEL_ELEMENT = eINSTANCE.getModelElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_ELEMENT__DESCRIPTION = eINSTANCE.getModelElement_Description();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_ELEMENT__TAG = eINSTANCE.getModelElement_Tag();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.NamedElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION__TEXT = eINSTANCE.getExpression_Text();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TypedElementImpl <em>Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TypedElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTypedElement()
		 * @generated
		 */
		EClass TYPED_ELEMENT = eINSTANCE.getTypedElement();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_ELEMENT__TYPE = eINSTANCE.getTypedElement_Type();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TypeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ParameterImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__DIRECTION = eINSTANCE.getParameter_Direction();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.OperationImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__VARIABLE = eINSTANCE.getOperation_Variable();

		/**
		 * The meta object literal for the '<em><b>Behavior</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__BEHAVIOR = eINSTANCE.getOperation_Behavior();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ConstraintImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__SPECIFICATION = eINSTANCE.getConstraint_Specification();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.PackageImpl <em>Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.PackageImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPackage()
		 * @generated
		 */
		EClass PACKAGE = eINSTANCE.getPackage();

		/**
		 * The meta object literal for the '<em><b>Owned Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE__OWNED_ELEMENT = eINSTANCE.getPackage_OwnedElement();

		/**
		 * The meta object literal for the '<em><b>Used Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE__USED_ELEMENT = eINSTANCE.getPackage_UsedElement();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DataTypeImpl <em>Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DataTypeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDataType()
		 * @generated
		 */
		EClass DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TagImpl <em>Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TagImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTag()
		 * @generated
		 */
		EClass TAG = eINSTANCE.getTag();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAG__VALUE = eINSTANCE.getTag_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.PropertyImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Is Composite</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__IS_COMPOSITE = eINSTANCE.getProperty_IsComposite();

		/**
		 * The meta object literal for the '<em><b>Is Read Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__IS_READ_ONLY = eINSTANCE.getProperty_IsReadOnly();

		/**
		 * The meta object literal for the '<em><b>Is Derived</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__IS_DERIVED = eINSTANCE.getProperty_IsDerived();

		/**
		 * The meta object literal for the '<em><b>Is Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__IS_ID = eINSTANCE.getProperty_IsId();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VISIBILITY = eINSTANCE.getProperty_Visibility();

		/**
		 * The meta object literal for the '<em><b>Default</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY__DEFAULT = eINSTANCE.getProperty_Default();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ClassImpl <em>Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ClassImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getClass_()
		 * @generated
		 */
		EClass CLASS = eINSTANCE.getClass_();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__ATTRIBUTE = eINSTANCE.getClass_Attribute();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__OPERATION = eINSTANCE.getClass_Operation();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.BinaryExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operation Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EXPRESSION__OPERATION_NAME = eINSTANCE.getBinaryExpression_OperationName();

		/**
		 * The meta object literal for the '<em><b>Left Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LEFT_OPERAND = eINSTANCE.getBinaryExpression_LeftOperand();

		/**
		 * The meta object literal for the '<em><b>Right Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RIGHT_OPERAND = eINSTANCE.getBinaryExpression_RightOperand();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__OPERATION = eINSTANCE.getBinaryExpression_Operation();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.UnaryExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operation Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EXPRESSION__OPERATION_NAME = eINSTANCE.getUnaryExpression_OperationName();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__OPERAND = eINSTANCE.getUnaryExpression_Operand();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__OPERATION = eINSTANCE.getUnaryExpression_Operation();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ValueExpressionImpl <em>Value Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ValueExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getValueExpression()
		 * @generated
		 */
		EClass VALUE_EXPRESSION = eINSTANCE.getValueExpression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CharacterValueImpl <em>Character Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CharacterValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCharacterValue()
		 * @generated
		 */
		EClass CHARACTER_VALUE = eINSTANCE.getCharacterValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_VALUE__VALUE = eINSTANCE.getCharacterValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.IntegerValueImpl <em>Integer Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.IntegerValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIntegerValue()
		 * @generated
		 */
		EClass INTEGER_VALUE = eINSTANCE.getIntegerValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE__VALUE = eINSTANCE.getIntegerValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.FloatValueImpl <em>Float Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.FloatValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFloatValue()
		 * @generated
		 */
		EClass FLOAT_VALUE = eINSTANCE.getFloatValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_VALUE__VALUE = eINSTANCE.getFloatValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.BooleanValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBooleanValue()
		 * @generated
		 */
		EClass BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_VALUE__VALUE = eINSTANCE.getBooleanValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CharstringValueImpl <em>Charstring Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CharstringValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCharstringValue()
		 * @generated
		 */
		EClass CHARSTRING_VALUE = eINSTANCE.getCharstringValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARSTRING_VALUE__VALUE = eINSTANCE.getCharstringValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.IdentImpl <em>Ident</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.IdentImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIdent()
		 * @generated
		 */
		EClass IDENT = eINSTANCE.getIdent();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENT__VALUE = eINSTANCE.getIdent_Value();

		/**
		 * The meta object literal for the '<em><b>Value Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENT__VALUE_CONTAINER = eINSTANCE.getIdent_ValueContainer();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ParenthesisExpressionImpl <em>Parenthesis Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ParenthesisExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParenthesisExpression()
		 * @generated
		 */
		EClass PARENTHESIS_EXPRESSION = eINSTANCE.getParenthesisExpression();

		/**
		 * The meta object literal for the '<em><b>Inner Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARENTHESIS_EXPRESSION__INNER_EXPRESSION = eINSTANCE.getParenthesisExpression_InnerExpression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ConditionalExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConditionalExpression()
		 * @generated
		 */
		EClass CONDITIONAL_EXPRESSION = eINSTANCE.getConditionalExpression();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_EXPRESSION__CONDITION = eINSTANCE.getConditionalExpression_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_EXPRESSION__THEN_PART = eINSTANCE.getConditionalExpression_ThenPart();

		/**
		 * The meta object literal for the '<em><b>Else Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_EXPRESSION__ELSE_PART = eINSTANCE.getConditionalExpression_ElsePart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ListExpressionImpl <em>List Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ListExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListExpression()
		 * @generated
		 */
		EClass LIST_EXPRESSION = eINSTANCE.getListExpression();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_EXPRESSION__ELEMENT = eINSTANCE.getListExpression_Element();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CallExpressionImpl <em>Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CallExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCallExpression()
		 * @generated
		 */
		EClass CALL_EXPRESSION = eINSTANCE.getCallExpression();

		/**
		 * The meta object literal for the '<em><b>Operation Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_EXPRESSION__OPERATION_NAME = eINSTANCE.getCallExpression_OperationName();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EXPRESSION__ARGUMENT = eINSTANCE.getCallExpression_Argument();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EXPRESSION__OPERATION = eINSTANCE.getCallExpression_Operation();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.EnumerationTypeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumerationType()
		 * @generated
		 */
		EClass ENUMERATION_TYPE = eINSTANCE.getEnumerationType();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUMERATION_TYPE__ITEM = eINSTANCE.getEnumerationType_Item();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.EnumerationItemImpl <em>Enumeration Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.EnumerationItemImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumerationItem()
		 * @generated
		 */
		EClass ENUMERATION_ITEM = eINSTANCE.getEnumerationItem();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ChoiceTypeImpl <em>Choice Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ChoiceTypeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getChoiceType()
		 * @generated
		 */
		EClass CHOICE_TYPE = eINSTANCE.getChoiceType();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHOICE_TYPE__ATTRIBUTE = eINSTANCE.getChoiceType_Attribute();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ListTypeImpl <em>List Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ListTypeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListType()
		 * @generated
		 */
		EClass LIST_TYPE = eINSTANCE.getListType();

		/**
		 * The meta object literal for the '<em><b>Content Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_TYPE__CONTENT_TYPE = eINSTANCE.getListType_ContentType();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.VariableImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Is Shared</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__IS_SHARED = eINSTANCE.getVariable_IsShared();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.SignatureImpl <em>Signature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.SignatureImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSignature()
		 * @generated
		 */
		EClass SIGNATURE = eINSTANCE.getSignature();

		/**
		 * The meta object literal for the '<em><b>Formal Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNATURE__FORMAL_PARAMETER = eINSTANCE.getSignature_FormalParameter();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.InformalExpressionImpl <em>Informal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.InformalExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInformalExpression()
		 * @generated
		 */
		EClass INFORMAL_EXPRESSION = eINSTANCE.getInformalExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMAL_EXPRESSION__VALUE = eINSTANCE.getInformalExpression_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.PackageableElementImpl <em>Packageable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.PackageableElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPackageableElement()
		 * @generated
		 */
		EClass PACKAGEABLE_ELEMENT = eINSTANCE.getPackageableElement();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ActionSequenceImpl <em>Action Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ActionSequenceImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getActionSequence()
		 * @generated
		 */
		EClass ACTION_SEQUENCE = eINSTANCE.getActionSequence();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_SEQUENCE__ACTION = eINSTANCE.getActionSequence_Action();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_SEQUENCE__MESSAGE = eINSTANCE.getActionSequence_Message();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ActionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__TEXT = eINSTANCE.getAction_Text();

		/**
		 * The meta object literal for the '<em><b>Action Sequence</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__ACTION_SEQUENCE = eINSTANCE.getAction_ActionSequence();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DigitValueImpl <em>Digit Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DigitValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDigitValue()
		 * @generated
		 */
		EClass DIGIT_VALUE = eINSTANCE.getDigitValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIGIT_VALUE__VALUE = eINSTANCE.getDigitValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DTMFValueImpl <em>DTMF Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DTMFValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMFValue()
		 * @generated
		 */
		EClass DTMF_VALUE = eINSTANCE.getDTMFValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DTMF_VALUE__VALUE = eINSTANCE.getDTMFValue_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.FeatureExpressionImpl <em>Feature Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.FeatureExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFeatureExpression()
		 * @generated
		 */
		EClass FEATURE_EXPRESSION = eINSTANCE.getFeatureExpression();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_EXPRESSION__SOURCE = eINSTANCE.getFeatureExpression_Source();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.PropertyExpressionImpl <em>Property Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.PropertyExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPropertyExpression()
		 * @generated
		 */
		EClass PROPERTY_EXPRESSION = eINSTANCE.getPropertyExpression();

		/**
		 * The meta object literal for the '<em><b>Referred Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_EXPRESSION__REFERRED_PROPERTY = eINSTANCE.getPropertyExpression_ReferredProperty();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CollectionRangeImpl <em>Collection Range</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CollectionRangeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionRange()
		 * @generated
		 */
		EClass COLLECTION_RANGE = eINSTANCE.getCollectionRange();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION_RANGE__FIRST = eINSTANCE.getCollectionRange_First();

		/**
		 * The meta object literal for the '<em><b>Last</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION_RANGE__LAST = eINSTANCE.getCollectionRange_Last();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CollectionLiteralExpressionImpl <em>Collection Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CollectionLiteralExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionLiteralExpression()
		 * @generated
		 */
		EClass COLLECTION_LITERAL_EXPRESSION = eINSTANCE.getCollectionLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION_LITERAL_EXPRESSION__PART = eINSTANCE.getCollectionLiteralExpression_Part();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CollectionItemImpl <em>Collection Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CollectionItemImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionItem()
		 * @generated
		 */
		EClass COLLECTION_ITEM = eINSTANCE.getCollectionItem();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION_ITEM__ITEM = eINSTANCE.getCollectionItem_Item();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CollectionLiteralPartImpl <em>Collection Literal Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CollectionLiteralPartImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCollectionLiteralPart()
		 * @generated
		 */
		EClass COLLECTION_LITERAL_PART = eINSTANCE.getCollectionLiteralPart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.EnumValueImpl <em>Enum Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.EnumValueImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnumValue()
		 * @generated
		 */
		EClass ENUM_VALUE = eINSTANCE.getEnumValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_VALUE__VALUE = eINSTANCE.getEnumValue_Value();

		/**
		 * The meta object literal for the '<em><b>Literal Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_VALUE__LITERAL_DEFINITION = eINSTANCE.getEnumValue_LiteralDefinition();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.BehaviorImpl <em>Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.BehaviorImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getBehavior()
		 * @generated
		 */
		EClass BEHAVIOR = eINSTANCE.getBehavior();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.StateMachineImpl <em>State Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.StateMachineImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getStateMachine()
		 * @generated
		 */
		EClass STATE_MACHINE = eINSTANCE.getStateMachine();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__NODE = eINSTANCE.getStateMachine_Node();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TRANSITION = eINSTANCE.getStateMachine_Transition();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.NewExpressionImpl <em>New Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.NewExpressionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNewExpression()
		 * @generated
		 */
		EClass NEW_EXPRESSION = eINSTANCE.getNewExpression();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_EXPRESSION__CLASS = eINSTANCE.getNewExpression_Class();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_EXPRESSION__ARGUMENT = eINSTANCE.getNewExpression_Argument();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ValueContainerImpl <em>Value Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ValueContainerImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getValueContainer()
		 * @generated
		 */
		EClass VALUE_CONTAINER = eINSTANCE.getValueContainer();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DialogImpl <em>Dialog</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DialogImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialog()
		 * @generated
		 */
		EClass DIALOG = eINSTANCE.getDialog();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__NODE = eINSTANCE.getDialog_Node();

		/**
		 * The meta object literal for the '<em><b>Accessed Functionality</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__ACCESSED_FUNCTIONALITY = eINSTANCE.getDialog_AccessedFunctionality();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__TRANSITION = eINSTANCE.getDialog_Transition();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__MESSAGE = eINSTANCE.getDialog_Message();

		/**
		 * The meta object literal for the '<em><b>Concept</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__CONCEPT = eINSTANCE.getDialog_Concept();

		/**
		 * The meta object literal for the '<em><b>Message Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__MESSAGE_PART = eINSTANCE.getDialog_MessagePart();

		/**
		 * The meta object literal for the '<em><b>External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__EXTERNAL_EVENT = eINSTANCE.getDialog_ExternalEvent();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__CONDITION = eINSTANCE.getDialog_Condition();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__VARIABLE = eINSTANCE.getDialog_Variable();

		/**
		 * The meta object literal for the '<em><b>Sub Dialog</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__SUB_DIALOG = eINSTANCE.getDialog_SubDialog();

		/**
		 * The meta object literal for the '<em><b>Global Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__GLOBAL_VARIABLE = eINSTANCE.getDialog_GlobalVariable();

		/**
		 * The meta object literal for the '<em><b>Referenced Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__REFERENCED_VARIABLE = eINSTANCE.getDialog_ReferencedVariable();

		/**
		 * The meta object literal for the '<em><b>Referenced Message</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__REFERENCED_MESSAGE = eINSTANCE.getDialog_ReferencedMessage();

		/**
		 * The meta object literal for the '<em><b>Referenced Input Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__REFERENCED_INPUT_EVENT = eINSTANCE.getDialog_ReferencedInputEvent();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__OPERATION = eINSTANCE.getDialog_Operation();

		/**
		 * The meta object literal for the '<em><b>Owned Grammar</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG__OWNED_GRAMMAR = eINSTANCE.getDialog_OwnedGrammar();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.SubDialogStateImpl <em>Sub Dialog State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.SubDialogStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSubDialogState()
		 * @generated
		 */
		EClass SUB_DIALOG_STATE = eINSTANCE.getSubDialogState();

		/**
		 * The meta object literal for the '<em><b>Called</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_DIALOG_STATE__CALLED = eINSTANCE.getSubDialogState_Called();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_DIALOG_STATE__ARGUMENT = eINSTANCE.getSubDialogState_Argument();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DialogStateImpl <em>Dialog State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DialogStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialogState()
		 * @generated
		 */
		EClass DIALOG_STATE = eINSTANCE.getDialogState();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG_STATE__ENTRY = eINSTANCE.getDialogState_Entry();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DialogNodeImpl <em>Dialog Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DialogNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDialogNode()
		 * @generated
		 */
		EClass DIALOG_NODE = eINSTANCE.getDialogNode();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG_NODE__OWNER = eINSTANCE.getDialogNode_Owner();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG_NODE__OUTGOING = eINSTANCE.getDialogNode_Outgoing();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIALOG_NODE__INCOMING = eINSTANCE.getDialogNode_Incoming();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TransitionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Is Else</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__IS_ELSE = eINSTANCE.getTransition_IsElse();

		/**
		 * The meta object literal for the '<em><b>Effect</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EFFECT = eINSTANCE.getTransition_Effect();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRIGGER = eINSTANCE.getTransition_Trigger();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__GUARD = eINSTANCE.getTransition_Guard();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__OWNER = eINSTANCE.getTransition_Owner();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ORIGIN = eINSTANCE.getTransition_Origin();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TriggerImpl <em>Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TriggerImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTrigger()
		 * @generated
		 */
		EClass TRIGGER = eINSTANCE.getTrigger();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIGGER__EVENT = eINSTANCE.getTrigger_Event();

		/**
		 * The meta object literal for the '<em><b>Arg Handle</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIGGER__ARG_HANDLE = eINSTANCE.getTrigger_ArgHandle();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.TransientNodeImpl <em>Transient Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.TransientNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getTransientNode()
		 * @generated
		 */
		EClass TRANSIENT_NODE = eINSTANCE.getTransientNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.WaitStateImpl <em>Wait State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.WaitStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getWaitState()
		 * @generated
		 */
		EClass WAIT_STATE = eINSTANCE.getWaitState();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WAIT_STATE__DELAY = eINSTANCE.getWaitState_Delay();

		/**
		 * The meta object literal for the '<em><b>Grammar</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT_STATE__GRAMMAR = eINSTANCE.getWaitState_Grammar();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.AnyStateImpl <em>Any State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.AnyStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyState()
		 * @generated
		 */
		EClass ANY_STATE = eINSTANCE.getAnyState();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ListStateImpl <em>List State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ListStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getListState()
		 * @generated
		 */
		EClass LIST_STATE = eINSTANCE.getListState();

		/**
		 * The meta object literal for the '<em><b>Excluded</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIST_STATE__EXCLUDED = eINSTANCE.getListState_Excluded();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_STATE__STATE = eINSTANCE.getListState_State();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DecisionNodeImpl <em>Decision Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DecisionNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDecisionNode()
		 * @generated
		 */
		EClass DECISION_NODE = eINSTANCE.getDecisionNode();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION_NODE__CONDITION = eINSTANCE.getDecisionNode_Condition();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.InitialNodeImpl <em>Initial Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.InitialNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInitialNode()
		 * @generated
		 */
		EClass INITIAL_NODE = eINSTANCE.getInitialNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.JunctionNodeImpl <em>Junction Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.JunctionNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getJunctionNode()
		 * @generated
		 */
		EClass JUNCTION_NODE = eINSTANCE.getJunctionNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DiversionNodeImpl <em>Diversion Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DiversionNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDiversionNode()
		 * @generated
		 */
		EClass DIVERSION_NODE = eINSTANCE.getDiversionNode();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIVERSION_NODE__TARGET = eINSTANCE.getDiversionNode_Target();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIVERSION_NODE__ARGUMENT = eINSTANCE.getDiversionNode_Argument();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.HistoryStateImpl <em>History State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.HistoryStateImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getHistoryState()
		 * @generated
		 */
		EClass HISTORY_STATE = eINSTANCE.getHistoryState();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.FinalNodeImpl <em>Final Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.FinalNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFinalNode()
		 * @generated
		 */
		EClass FINAL_NODE = eINSTANCE.getFinalNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.NextNodeImpl <em>Next Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.NextNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getNextNode()
		 * @generated
		 */
		EClass NEXT_NODE = eINSTANCE.getNextNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.StopNodeImpl <em>Stop Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.StopNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getStopNode()
		 * @generated
		 */
		EClass STOP_NODE = eINSTANCE.getStopNode();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ReturnNodeImpl <em>Return Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ReturnNodeImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReturnNode()
		 * @generated
		 */
		EClass RETURN_NODE = eINSTANCE.getReturnNode();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_NODE__EXPRESSION = eINSTANCE.getReturnNode_Expression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.AssignmentImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '<em><b>Value Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__VALUE_CONTAINER = eINSTANCE.getAssignment_ValueContainer();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__VALUE = eINSTANCE.getAssignment_Value();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.PlayImpl <em>Play</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.PlayImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getPlay()
		 * @generated
		 */
		EClass PLAY = eINSTANCE.getPlay();

		/**
		 * The meta object literal for the '<em><b>Interruptible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAY__INTERRUPTIBLE = eINSTANCE.getPlay_Interruptible();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLAY__MESSAGE = eINSTANCE.getPlay_Message();

		/**
		 * The meta object literal for the '<em><b>Message Argument</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLAY__MESSAGE_ARGUMENT = eINSTANCE.getPlay_MessageArgument();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.UninterpretedImpl <em>Uninterpreted</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.UninterpretedImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUninterpreted()
		 * @generated
		 */
		EClass UNINTERPRETED = eINSTANCE.getUninterpreted();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNINTERPRETED__BODY = eINSTANCE.getUninterpreted_Body();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.IfThenElseImpl <em>If Then Else</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.IfThenElseImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getIfThenElse()
		 * @generated
		 */
		EClass IF_THEN_ELSE = eINSTANCE.getIfThenElse();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_ELSE__CONDITION = eINSTANCE.getIfThenElse_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_ELSE__THEN_PART = eINSTANCE.getIfThenElse_ThenPart();

		/**
		 * The meta object literal for the '<em><b>Else Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_ELSE__ELSE_PART = eINSTANCE.getIfThenElse_ElsePart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.WhileImpl <em>While</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.WhileImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getWhile()
		 * @generated
		 */
		EClass WHILE = eINSTANCE.getWhile();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE__CONDITION = eINSTANCE.getWhile_Condition();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE__BODY = eINSTANCE.getWhile_Body();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ReturnActionImpl <em>Return Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ReturnActionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReturnAction()
		 * @generated
		 */
		EClass RETURN_ACTION = eINSTANCE.getReturnAction();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_ACTION__EXPRESSION = eINSTANCE.getReturnAction_Expression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.CallActionImpl <em>Call Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.CallActionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getCallAction()
		 * @generated
		 */
		EClass CALL_ACTION = eINSTANCE.getCallAction();

		/**
		 * The meta object literal for the '<em><b>Call Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__CALL_EXPRESSION = eINSTANCE.getCallAction_CallExpression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.InputEventImpl <em>Input Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.InputEventImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInputEvent()
		 * @generated
		 */
		EClass INPUT_EVENT = eINSTANCE.getInputEvent();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INPUT_EVENT__PARAMETER = eINSTANCE.getInputEvent_Parameter();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DTMFImpl <em>DTMF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DTMFImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMF()
		 * @generated
		 */
		EClass DTMF = eINSTANCE.getDTMF();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DTMF__KEY = eINSTANCE.getDTMF_Key();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.RejectImpl <em>Reject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.RejectImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getReject()
		 * @generated
		 */
		EClass REJECT = eINSTANCE.getReject();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.InactivityImpl <em>Inactivity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.InactivityImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getInactivity()
		 * @generated
		 */
		EClass INACTIVITY = eINSTANCE.getInactivity();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ConceptImpl <em>Concept</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ConceptImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConcept()
		 * @generated
		 */
		EClass CONCEPT = eINSTANCE.getConcept();

		/**
		 * The meta object literal for the '<em><b>Grammar</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT__GRAMMAR = eINSTANCE.getConcept_Grammar();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ExternalEventImpl <em>External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ExternalEventImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getExternalEvent()
		 * @generated
		 */
		EClass EXTERNAL_EVENT = eINSTANCE.getExternalEvent();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.AnyDtmfImpl <em>Any Dtmf</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.AnyDtmfImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyDtmf()
		 * @generated
		 */
		EClass ANY_DTMF = eINSTANCE.getAnyDtmf();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.AnyDigitImpl <em>Any Digit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.AnyDigitImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAnyDigit()
		 * @generated
		 */
		EClass ANY_DIGIT = eINSTANCE.getAnyDigit();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.RecordingImpl <em>Recording</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.RecordingImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getRecording()
		 * @generated
		 */
		EClass RECORDING = eINSTANCE.getRecording();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.AbstractDTMFImpl <em>Abstract DTMF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.AbstractDTMFImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getAbstractDTMF()
		 * @generated
		 */
		EClass ABSTRACT_DTMF = eINSTANCE.getAbstractDTMF();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.MessageEndImpl <em>Message End</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.MessageEndImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageEnd()
		 * @generated
		 */
		EClass MESSAGE_END = eINSTANCE.getMessageEnd();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.DtmfInputImpl <em>Dtmf Input</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.DtmfInputImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDtmfInput()
		 * @generated
		 */
		EClass DTMF_INPUT = eINSTANCE.getDtmfInput();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.SystemInputImpl <em>System Input</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.SystemInputImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSystemInput()
		 * @generated
		 */
		EClass SYSTEM_INPUT = eINSTANCE.getSystemInput();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.MessageImpl <em>Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.MessageImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessage()
		 * @generated
		 */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__VISIBILITY = eINSTANCE.getMessage_Visibility();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__BODY = eINSTANCE.getMessage_Body();

		/**
		 * The meta object literal for the '<em><b>Message Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__MESSAGE_ELEMENT = eINSTANCE.getMessage_MessageElement();

		/**
		 * The meta object literal for the '<em><b>Action Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__ACTION_SPECIFICATION = eINSTANCE.getMessage_ActionSpecification();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.MessagePartImpl <em>Message Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.MessagePartImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessagePart()
		 * @generated
		 */
		EClass MESSAGE_PART = eINSTANCE.getMessagePart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.FixPartImpl <em>Fix Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.FixPartImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFixPart()
		 * @generated
		 */
		EClass FIX_PART = eINSTANCE.getFixPart();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIX_PART__CONTENT = eINSTANCE.getFixPart_Content();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIX_PART__FORMAT = eINSTANCE.getFixPart_Format();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.VariablePartImpl <em>Variable Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.VariablePartImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVariablePart()
		 * @generated
		 */
		EClass VARIABLE_PART = eINSTANCE.getVariablePart();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_PART__FORMAT = eINSTANCE.getVariablePart_Format();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_PART__VISIBILITY = eINSTANCE.getVariablePart_Visibility();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_PART__VALUE = eINSTANCE.getVariablePart_Value();

		/**
		 * The meta object literal for the '<em><b>Action Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_PART__ACTION_SPECIFICATION = eINSTANCE.getVariablePart_ActionSpecification();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ConditionalElementImpl <em>Conditional Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ConditionalElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getConditionalElement()
		 * @generated
		 */
		EClass CONDITIONAL_ELEMENT = eINSTANCE.getConditionalElement();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_ELEMENT__CONDITION = eINSTANCE.getConditionalElement_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_ELEMENT__THEN_PART = eINSTANCE.getConditionalElement_ThenPart();

		/**
		 * The meta object literal for the '<em><b>Else Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_ELEMENT__ELSE_PART = eINSTANCE.getConditionalElement_ElsePart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.SilencePartImpl <em>Silence Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.SilencePartImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getSilencePart()
		 * @generated
		 */
		EClass SILENCE_PART = eINSTANCE.getSilencePart();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SILENCE_PART__DURATION = eINSTANCE.getSilencePart_Duration();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.MessageElementImpl <em>Message Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.MessageElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageElement()
		 * @generated
		 */
		EClass MESSAGE_ELEMENT = eINSTANCE.getMessageElement();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.UseElementImpl <em>Use Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.UseElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getUseElement()
		 * @generated
		 */
		EClass USE_ELEMENT = eINSTANCE.getUseElement();

		/**
		 * The meta object literal for the '<em><b>Used Message Part</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_ELEMENT__USED_MESSAGE_PART = eINSTANCE.getUseElement_UsedMessagePart();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.MessageElementConditionImpl <em>Message Element Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.MessageElementConditionImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getMessageElementCondition()
		 * @generated
		 */
		EClass MESSAGE_ELEMENT_CONDITION = eINSTANCE.getMessageElementCondition();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_ELEMENT_CONDITION__VISIBILITY = eINSTANCE.getMessageElementCondition_Visibility();

		/**
		 * The meta object literal for the '<em><b>Condition Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_ELEMENT_CONDITION__CONDITION_EXPRESSION = eINSTANCE.getMessageElementCondition_ConditionExpression();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ForEachElementImpl <em>For Each Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ForEachElementImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getForEachElement()
		 * @generated
		 */
		EClass FOR_EACH_ELEMENT = eINSTANCE.getForEachElement();

		/**
		 * The meta object literal for the '<em><b>Iterator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_EACH_ELEMENT__ITERATOR = eINSTANCE.getForEachElement_Iterator();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_EACH_ELEMENT__SOURCE = eINSTANCE.getForEachElement_Source();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_EACH_ELEMENT__BODY = eINSTANCE.getForEachElement_Body();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.GrammarImpl <em>Grammar</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.GrammarImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getGrammar()
		 * @generated
		 */
		EClass GRAMMAR = eINSTANCE.getGrammar();

		/**
		 * The meta object literal for the '<em><b>Is Computed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAR__IS_COMPUTED = eINSTANCE.getGrammar_IsComputed();

		/**
		 * The meta object literal for the '<em><b>Formalism</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAR__FORMALISM = eINSTANCE.getGrammar_Formalism();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAR__LANGUAGE = eINSTANCE.getGrammar_Language();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAR__CONTENT = eINSTANCE.getGrammar_Content();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAR__LOCATION = eINSTANCE.getGrammar_Location();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAMMAR__ENVIRONMENT = eINSTANCE.getGrammar_Environment();

		/**
		 * The meta object literal for the '<em><b>Service</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAMMAR__SERVICE = eINSTANCE.getGrammar_Service();

		/**
		 * The meta object literal for the '<em><b>Dialog</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAMMAR__DIALOG = eINSTANCE.getGrammar_Dialog();

		/**
		 * The meta object literal for the '<em><b>Dynamic Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAMMAR__DYNAMIC_DEFINITION = eINSTANCE.getGrammar_DynamicDefinition();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.VoiceServiceImpl <em>Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.VoiceServiceImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVoiceService()
		 * @generated
		 */
		EClass VOICE_SERVICE = eINSTANCE.getVoiceService();

		/**
		 * The meta object literal for the '<em><b>Main Dialog</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VOICE_SERVICE__MAIN_DIALOG = eINSTANCE.getVoiceService_MainDialog();

		/**
		 * The meta object literal for the '<em><b>Extra Dialog</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VOICE_SERVICE__EXTRA_DIALOG = eINSTANCE.getVoiceService_ExtraDialog();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ServiceImpl <em>Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ServiceImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getService()
		 * @generated
		 */
		EClass SERVICE = eINSTANCE.getService();

		/**
		 * The meta object literal for the '<em><b>Owned Grammar</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__OWNED_GRAMMAR = eINSTANCE.getService_OwnedGrammar();

		/**
		 * The meta object literal for the '<em><b>Sub Service</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__SUB_SERVICE = eINSTANCE.getService_SubService();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__PARENT = eINSTANCE.getService_Parent();

		/**
		 * The meta object literal for the '<em><b>Offered Functionality</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__OFFERED_FUNCTIONALITY = eINSTANCE.getService_OfferedFunctionality();

		/**
		 * The meta object literal for the '<em><b>Entity Package</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__ENTITY_PACKAGE = eINSTANCE.getService_EntityPackage();

		/**
		 * The meta object literal for the '<em><b>Functionality Package</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__FUNCTIONALITY_PACKAGE = eINSTANCE.getService_FunctionalityPackage();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.FunctionalityImpl <em>Functionality</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.FunctionalityImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getFunctionality()
		 * @generated
		 */
		EClass FUNCTIONALITY = eINSTANCE.getFunctionality();

		/**
		 * The meta object literal for the '<em><b>Access Dialog</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONALITY__ACCESS_DIALOG = eINSTANCE.getFunctionality_AccessDialog();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONALITY__PARTICIPANT = eINSTANCE.getFunctionality_Participant();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.ActorImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Functionality</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTOR__FUNCTIONALITY = eINSTANCE.getActor_Functionality();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.EntityImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.impl.EnvironmentImpl <em>Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.impl.EnvironmentImpl
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getEnvironment()
		 * @generated
		 */
		EClass ENVIRONMENT = eINSTANCE.getEnvironment();

		/**
		 * The meta object literal for the '<em><b>Owned Grammar</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__OWNED_GRAMMAR = eINSTANCE.getEnvironment_OwnedGrammar();

		/**
		 * The meta object literal for the '<em><b>Defined Service</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__DEFINED_SERVICE = eINSTANCE.getEnvironment_DefinedService();

		/**
		 * The meta object literal for the '<em><b>Referenced Service</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__REFERENCED_SERVICE = eINSTANCE.getEnvironment_ReferencedService();

		/**
		 * The meta object literal for the '<em><b>Predefined Type</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__PREDEFINED_TYPE = eINSTANCE.getEnvironment_PredefinedType();

		/**
		 * The meta object literal for the '<em><b>Predefined Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__PREDEFINED_EVENT = eINSTANCE.getEnvironment_PredefinedEvent();

		/**
		 * The meta object literal for the '<em><b>Predefined Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__PREDEFINED_OPERATION = eINSTANCE.getEnvironment_PredefinedOperation();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.ParameterDirectionKind <em>Parameter Direction Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.ParameterDirectionKind
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getParameterDirectionKind()
		 * @generated
		 */
		EEnum PARAMETER_DIRECTION_KIND = eINSTANCE.getParameterDirectionKind();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.VisibilityKind <em>Visibility Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.VisibilityKind
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getVisibilityKind()
		 * @generated
		 */
		EEnum VISIBILITY_KIND = eINSTANCE.getVisibilityKind();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.DTMFKind <em>DTMF Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.DTMFKind
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDTMFKind()
		 * @generated
		 */
		EEnum DTMF_KIND = eINSTANCE.getDTMFKind();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.DigitKind <em>Digit Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.DigitKind
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDigitKind()
		 * @generated
		 */
		EEnum DIGIT_KIND = eINSTANCE.getDigitKind();

		/**
		 * The meta object literal for the '{@link com.orange_ftgroup.voice.DiffusionType <em>Diffusion Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.orange_ftgroup.voice.DiffusionType
		 * @see com.orange_ftgroup.voice.impl.VoicePackageImpl#getDiffusionType()
		 * @generated
		 */
		EEnum DIFFUSION_TYPE = eINSTANCE.getDiffusionType();

	}

} //VoicePackage
