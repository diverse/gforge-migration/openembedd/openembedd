/**
 * <copyright>
 * </copyright>
 *
 * $Id: Type.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getType()
 * @model abstract="true"
 * @generated
 */
public interface Type extends PackageableElement {
} // Type