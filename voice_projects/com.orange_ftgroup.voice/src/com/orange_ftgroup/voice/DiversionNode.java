/**
 * <copyright>
 * </copyright>
 *
 * $Id: DiversionNode.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diversion Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.DiversionNode#getTarget <em>Target</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.DiversionNode#getArgument <em>Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDiversionNode()
 * @model
 * @generated
 */
public interface DiversionNode extends FinalNode {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDiversionNode_Target()
	 * @model
	 * @generated
	 */
	Dialog getTarget();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.DiversionNode#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Dialog value);

	/**
	 * Returns the value of the '<em><b>Argument</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDiversionNode_Argument()
	 * @model type="com.orange_ftgroup.voice.Expression" containment="true"
	 * @generated
	 */
	EList getArgument();

} // DiversionNode