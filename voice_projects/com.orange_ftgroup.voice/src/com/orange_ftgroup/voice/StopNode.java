/**
 * <copyright>
 * </copyright>
 *
 * $Id: StopNode.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stop Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getStopNode()
 * @model
 * @generated
 */
public interface StopNode extends FinalNode {
} // StopNode