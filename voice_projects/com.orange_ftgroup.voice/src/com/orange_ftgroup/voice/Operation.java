/**
 * <copyright>
 * </copyright>
 *
 * $Id: Operation.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Operation#getVariable <em>Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Operation#getBehavior <em>Behavior</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends Signature, TypedElement {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getOperation_Variable()
	 * @model type="com.orange_ftgroup.voice.Variable" containment="true"
	 * @generated
	 */
	EList getVariable();

	/**
	 * Returns the value of the '<em><b>Behavior</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Behavior}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getOperation_Behavior()
	 * @model type="com.orange_ftgroup.voice.Behavior" containment="true"
	 * @generated
	 */
	EList getBehavior();

} // Operation