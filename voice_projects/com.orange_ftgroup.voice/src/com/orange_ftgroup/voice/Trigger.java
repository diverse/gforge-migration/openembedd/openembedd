/**
 * <copyright>
 * </copyright>
 *
 * $Id: Trigger.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Trigger#getEvent <em>Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Trigger#getArgHandle <em>Arg Handle</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getTrigger()
 * @model
 * @generated
 */
public interface Trigger extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(InputEvent)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTrigger_Event()
	 * @model
	 * @generated
	 */
	InputEvent getEvent();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Trigger#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(InputEvent value);

	/**
	 * Returns the value of the '<em><b>Arg Handle</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.ValueContainer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arg Handle</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arg Handle</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getTrigger_ArgHandle()
	 * @model type="com.orange_ftgroup.voice.ValueContainer"
	 * @generated
	 */
	EList getArgHandle();

} // Trigger