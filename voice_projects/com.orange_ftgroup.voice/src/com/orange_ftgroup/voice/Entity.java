/**
 * <copyright>
 * </copyright>
 *
 * $Id: Entity.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getEntity()
 * @model
 * @generated
 */
public interface Entity extends com.orange_ftgroup.voice.Class {
} // Entity