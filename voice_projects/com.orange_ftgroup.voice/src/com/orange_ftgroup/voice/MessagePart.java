/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessagePart.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Part</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getMessagePart()
 * @model abstract="true"
 * @generated
 */
public interface MessagePart extends Signature {
} // MessagePart