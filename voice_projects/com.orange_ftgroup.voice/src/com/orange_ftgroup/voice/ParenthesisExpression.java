/**
 * <copyright>
 * </copyright>
 *
 * $Id: ParenthesisExpression.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parenthesis Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ParenthesisExpression#getInnerExpression <em>Inner Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getParenthesisExpression()
 * @model
 * @generated
 */
public interface ParenthesisExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Inner Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inner Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Expression</em>' containment reference.
	 * @see #setInnerExpression(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getParenthesisExpression_InnerExpression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInnerExpression();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ParenthesisExpression#getInnerExpression <em>Inner Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Expression</em>' containment reference.
	 * @see #getInnerExpression()
	 * @generated
	 */
	void setInnerExpression(Expression value);

} // ParenthesisExpression