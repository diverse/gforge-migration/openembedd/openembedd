/**
 * <copyright>
 * </copyright>
 *
 * $Id: NewExpression.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.NewExpression#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.NewExpression#getArgument <em>Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getNewExpression()
 * @model
 * @generated
 */
public interface NewExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #setClass(com.orange_ftgroup.voice.Class)
	 * @see com.orange_ftgroup.voice.VoicePackage#getNewExpression_Class()
	 * @model
	 * @generated
	 */
	com.orange_ftgroup.voice.Class getClass_();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.NewExpression#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(com.orange_ftgroup.voice.Class value);

	/**
	 * Returns the value of the '<em><b>Argument</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getNewExpression_Argument()
	 * @model type="com.orange_ftgroup.voice.Expression" containment="true"
	 * @generated
	 */
	EList getArgument();

} // NewExpression