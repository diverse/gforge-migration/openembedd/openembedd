/**
 * <copyright>
 * </copyright>
 *
 * $Id: Class.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Class#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Class#getOperation <em>Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends Type {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getClass_Attribute()
	 * @model type="com.orange_ftgroup.voice.Property" containment="true"
	 * @generated
	 */
	EList getAttribute();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getClass_Operation()
	 * @model type="com.orange_ftgroup.voice.Operation" containment="true"
	 * @generated
	 */
	EList getOperation();

} // Class