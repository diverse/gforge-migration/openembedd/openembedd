/**
 * <copyright>
 * </copyright>
 *
 * $Id: DigitKind.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Digit Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoicePackage#getDigitKind()
 * @model
 * @generated
 */
public final class DigitKind extends AbstractEnumerator {
	/**
	 * The '<em><b>Zero</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Zero</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZERO_LITERAL
	 * @model name="zero"
	 * @generated
	 * @ordered
	 */
	public static final int ZERO = 0;

	/**
	 * The '<em><b>One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ONE_LITERAL
	 * @model name="one"
	 * @generated
	 * @ordered
	 */
	public static final int ONE = 1;

	/**
	 * The '<em><b>Two</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Two</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TWO_LITERAL
	 * @model name="two"
	 * @generated
	 * @ordered
	 */
	public static final int TWO = 2;

	/**
	 * The '<em><b>Three</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Three</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THREE_LITERAL
	 * @model name="three"
	 * @generated
	 * @ordered
	 */
	public static final int THREE = 3;

	/**
	 * The '<em><b>Four</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Four</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FOUR_LITERAL
	 * @model name="four"
	 * @generated
	 * @ordered
	 */
	public static final int FOUR = 4;

	/**
	 * The '<em><b>Five</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Five</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIVE_LITERAL
	 * @model name="five"
	 * @generated
	 * @ordered
	 */
	public static final int FIVE = 5;

	/**
	 * The '<em><b>Six</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Six</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIX_LITERAL
	 * @model name="six"
	 * @generated
	 * @ordered
	 */
	public static final int SIX = 6;

	/**
	 * The '<em><b>Seven</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Seven</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SEVEN_LITERAL
	 * @model name="seven"
	 * @generated
	 * @ordered
	 */
	public static final int SEVEN = 7;

	/**
	 * The '<em><b>Eight</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Eight</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EIGHT_LITERAL
	 * @model name="eight"
	 * @generated
	 * @ordered
	 */
	public static final int EIGHT = 8;

	/**
	 * The '<em><b>Nine</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nine</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NINE_LITERAL
	 * @model name="nine"
	 * @generated
	 * @ordered
	 */
	public static final int NINE = 9;

	/**
	 * The '<em><b>Zero</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ZERO
	 * @generated
	 * @ordered
	 */
	public static final DigitKind ZERO_LITERAL = new DigitKind(ZERO, "zero", "zero");

	/**
	 * The '<em><b>One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ONE
	 * @generated
	 * @ordered
	 */
	public static final DigitKind ONE_LITERAL = new DigitKind(ONE, "one", "one");

	/**
	 * The '<em><b>Two</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TWO
	 * @generated
	 * @ordered
	 */
	public static final DigitKind TWO_LITERAL = new DigitKind(TWO, "two", "two");

	/**
	 * The '<em><b>Three</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THREE
	 * @generated
	 * @ordered
	 */
	public static final DigitKind THREE_LITERAL = new DigitKind(THREE, "three", "three");

	/**
	 * The '<em><b>Four</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FOUR
	 * @generated
	 * @ordered
	 */
	public static final DigitKind FOUR_LITERAL = new DigitKind(FOUR, "four", "four");

	/**
	 * The '<em><b>Five</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIVE
	 * @generated
	 * @ordered
	 */
	public static final DigitKind FIVE_LITERAL = new DigitKind(FIVE, "five", "five");

	/**
	 * The '<em><b>Six</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIX
	 * @generated
	 * @ordered
	 */
	public static final DigitKind SIX_LITERAL = new DigitKind(SIX, "six", "six");

	/**
	 * The '<em><b>Seven</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SEVEN
	 * @generated
	 * @ordered
	 */
	public static final DigitKind SEVEN_LITERAL = new DigitKind(SEVEN, "seven", "seven");

	/**
	 * The '<em><b>Eight</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EIGHT
	 * @generated
	 * @ordered
	 */
	public static final DigitKind EIGHT_LITERAL = new DigitKind(EIGHT, "eight", "eight");

	/**
	 * The '<em><b>Nine</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NINE
	 * @generated
	 * @ordered
	 */
	public static final DigitKind NINE_LITERAL = new DigitKind(NINE, "nine", "nine");

	/**
	 * An array of all the '<em><b>Digit Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DigitKind[] VALUES_ARRAY =
		new DigitKind[] {
			ZERO_LITERAL,
			ONE_LITERAL,
			TWO_LITERAL,
			THREE_LITERAL,
			FOUR_LITERAL,
			FIVE_LITERAL,
			SIX_LITERAL,
			SEVEN_LITERAL,
			EIGHT_LITERAL,
			NINE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Digit Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Digit Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DigitKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DigitKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Digit Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DigitKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DigitKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Digit Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DigitKind get(int value) {
		switch (value) {
			case ZERO: return ZERO_LITERAL;
			case ONE: return ONE_LITERAL;
			case TWO: return TWO_LITERAL;
			case THREE: return THREE_LITERAL;
			case FOUR: return FOUR_LITERAL;
			case FIVE: return FIVE_LITERAL;
			case SIX: return SIX_LITERAL;
			case SEVEN: return SEVEN_LITERAL;
			case EIGHT: return EIGHT_LITERAL;
			case NINE: return NINE_LITERAL;
		}
		return null;	
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DigitKind(int value, String name, String literal) {
		super(value, name, literal);
	}

} //DigitKind
