/**
 * <copyright>
 * </copyright>
 *
 * $Id: ChoiceType.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ChoiceType#getAttribute <em>Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getChoiceType()
 * @model
 * @generated
 */
public interface ChoiceType extends DataType {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getChoiceType_Attribute()
	 * @model type="com.orange_ftgroup.voice.Property" containment="true"
	 * @generated
	 */
	EList getAttribute();

} // ChoiceType