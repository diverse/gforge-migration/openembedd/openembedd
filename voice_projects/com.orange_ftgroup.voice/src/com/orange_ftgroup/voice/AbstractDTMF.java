/**
 * <copyright>
 * </copyright>
 *
 * $Id: AbstractDTMF.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract DTMF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getAbstractDTMF()
 * @model
 * @generated
 */
public interface AbstractDTMF extends DtmfInput {
} // AbstractDTMF