/**
 * <copyright>
 * </copyright>
 *
 * $Id: Dialog.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dialog</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getNode <em>Node</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getAccessedFunctionality <em>Accessed Functionality</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getTransition <em>Transition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getMessage <em>Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getConcept <em>Concept</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getMessagePart <em>Message Part</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getExternalEvent <em>External Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getVariable <em>Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getSubDialog <em>Sub Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getGlobalVariable <em>Global Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getReferencedVariable <em>Referenced Variable</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getReferencedMessage <em>Referenced Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getReferencedInputEvent <em>Referenced Input Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getOperation <em>Operation</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Dialog#getOwnedGrammar <em>Owned Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDialog()
 * @model
 * @generated
 */
public interface Dialog extends Signature, PackageableElement {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.DialogNode}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.DialogNode#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Node()
	 * @see com.orange_ftgroup.voice.DialogNode#getOwner
	 * @model type="com.orange_ftgroup.voice.DialogNode" opposite="owner" containment="true"
	 * @generated
	 */
	EList getNode();

	/**
	 * Returns the value of the '<em><b>Accessed Functionality</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Functionality#getAccessDialog <em>Access Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accessed Functionality</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessed Functionality</em>' reference.
	 * @see #setAccessedFunctionality(Functionality)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_AccessedFunctionality()
	 * @see com.orange_ftgroup.voice.Functionality#getAccessDialog
	 * @model opposite="accessDialog"
	 * @generated
	 */
	Functionality getAccessedFunctionality();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Dialog#getAccessedFunctionality <em>Accessed Functionality</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessed Functionality</em>' reference.
	 * @see #getAccessedFunctionality()
	 * @generated
	 */
	void setAccessedFunctionality(Functionality value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Transition}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Transition#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Transition()
	 * @see com.orange_ftgroup.voice.Transition#getOwner
	 * @model type="com.orange_ftgroup.voice.Transition" opposite="owner" containment="true"
	 * @generated
	 */
	EList getTransition();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Message()
	 * @model type="com.orange_ftgroup.voice.Message" containment="true"
	 * @generated
	 */
	EList getMessage();

	/**
	 * Returns the value of the '<em><b>Concept</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Concept}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concept</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Concept()
	 * @model type="com.orange_ftgroup.voice.Concept" containment="true"
	 * @generated
	 */
	EList getConcept();

	/**
	 * Returns the value of the '<em><b>Message Part</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessagePart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Part</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_MessagePart()
	 * @model type="com.orange_ftgroup.voice.MessagePart" containment="true"
	 * @generated
	 */
	EList getMessagePart();

	/**
	 * Returns the value of the '<em><b>External Event</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.ExternalEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Event</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_ExternalEvent()
	 * @model type="com.orange_ftgroup.voice.ExternalEvent" containment="true"
	 * @generated
	 */
	EList getExternalEvent();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessageElementCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Condition()
	 * @model type="com.orange_ftgroup.voice.MessageElementCondition" containment="true"
	 * @generated
	 */
	EList getCondition();

	/**
	 * Returns the value of the '<em><b>Variable</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Variable()
	 * @model type="com.orange_ftgroup.voice.Variable" containment="true"
	 * @generated
	 */
	EList getVariable();

	/**
	 * Returns the value of the '<em><b>Sub Dialog</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Dialog}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Dialog</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Dialog</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_SubDialog()
	 * @model type="com.orange_ftgroup.voice.Dialog" containment="true"
	 * @generated
	 */
	EList getSubDialog();

	/**
	 * Returns the value of the '<em><b>Global Variable</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Variable</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_GlobalVariable()
	 * @model type="com.orange_ftgroup.voice.Variable" containment="true"
	 * @generated
	 */
	EList getGlobalVariable();

	/**
	 * Returns the value of the '<em><b>Referenced Variable</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Variable</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_ReferencedVariable()
	 * @model type="com.orange_ftgroup.voice.Variable" containment="true"
	 * @generated
	 */
	EList getReferencedVariable();

	/**
	 * Returns the value of the '<em><b>Referenced Message</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Message}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Message</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Message</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_ReferencedMessage()
	 * @model type="com.orange_ftgroup.voice.Message" containment="true"
	 * @generated
	 */
	EList getReferencedMessage();

	/**
	 * Returns the value of the '<em><b>Referenced Input Event</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.InputEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Input Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Input Event</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_ReferencedInputEvent()
	 * @model type="com.orange_ftgroup.voice.InputEvent" containment="true"
	 * @generated
	 */
	EList getReferencedInputEvent();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_Operation()
	 * @model type="com.orange_ftgroup.voice.Operation" containment="true"
	 * @generated
	 */
	EList getOperation();

	/**
	 * Returns the value of the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Grammar}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Grammar#getDialog <em>Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Grammar</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Grammar</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getDialog_OwnedGrammar()
	 * @see com.orange_ftgroup.voice.Grammar#getDialog
	 * @model type="com.orange_ftgroup.voice.Grammar" opposite="Dialog" containment="true"
	 * @generated
	 */
	EList getOwnedGrammar();

} // Dialog