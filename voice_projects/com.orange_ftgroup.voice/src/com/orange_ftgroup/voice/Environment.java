/**
 * <copyright>
 * </copyright>
 *
 * $Id: Environment.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getOwnedGrammar <em>Owned Grammar</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getDefinedService <em>Defined Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getReferencedService <em>Referenced Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getPredefinedType <em>Predefined Type</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getPredefinedEvent <em>Predefined Event</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Environment#getPredefinedOperation <em>Predefined Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment()
 * @model
 * @generated
 */
public interface Environment extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Grammar}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Grammar#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Grammar</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Grammar</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_OwnedGrammar()
	 * @see com.orange_ftgroup.voice.Grammar#getEnvironment
	 * @model type="com.orange_ftgroup.voice.Grammar" opposite="Environment" containment="true"
	 * @generated
	 */
	EList getOwnedGrammar();

	/**
	 * Returns the value of the '<em><b>Defined Service</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Service}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defined Service</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defined Service</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_DefinedService()
	 * @model type="com.orange_ftgroup.voice.Service" containment="true"
	 * @generated
	 */
	EList getDefinedService();

	/**
	 * Returns the value of the '<em><b>Referenced Service</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Service}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Service</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Service</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_ReferencedService()
	 * @model type="com.orange_ftgroup.voice.Service" containment="true"
	 * @generated
	 */
	EList getReferencedService();

	/**
	 * Returns the value of the '<em><b>Predefined Type</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predefined Type</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predefined Type</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_PredefinedType()
	 * @model type="com.orange_ftgroup.voice.Type" containment="true"
	 * @generated
	 */
	EList getPredefinedType();

	/**
	 * Returns the value of the '<em><b>Predefined Event</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.InputEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predefined Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predefined Event</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_PredefinedEvent()
	 * @model type="com.orange_ftgroup.voice.InputEvent" containment="true"
	 * @generated
	 */
	EList getPredefinedEvent();

	/**
	 * Returns the value of the '<em><b>Predefined Operation</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predefined Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predefined Operation</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getEnvironment_PredefinedOperation()
	 * @model type="com.orange_ftgroup.voice.Operation" containment="true"
	 * @generated
	 */
	EList getPredefinedOperation();

} // Environment