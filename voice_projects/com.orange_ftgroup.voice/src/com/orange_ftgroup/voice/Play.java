/**
 * <copyright>
 * </copyright>
 *
 * $Id: Play.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Play</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Play#isInterruptible <em>Interruptible</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Play#getMessage <em>Message</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Play#getMessageArgument <em>Message Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getPlay()
 * @model
 * @generated
 */
public interface Play extends Action {
	/**
	 * Returns the value of the '<em><b>Interruptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interruptible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interruptible</em>' attribute.
	 * @see #setInterruptible(boolean)
	 * @see com.orange_ftgroup.voice.VoicePackage#getPlay_Interruptible()
	 * @model
	 * @generated
	 */
	boolean isInterruptible();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Play#isInterruptible <em>Interruptible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interruptible</em>' attribute.
	 * @see #isInterruptible()
	 * @generated
	 */
	void setInterruptible(boolean value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference.
	 * @see #setMessage(Message)
	 * @see com.orange_ftgroup.voice.VoicePackage#getPlay_Message()
	 * @model
	 * @generated
	 */
	Message getMessage();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Play#getMessage <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Message Argument</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Argument</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getPlay_MessageArgument()
	 * @model type="com.orange_ftgroup.voice.Expression" containment="true"
	 * @generated
	 */
	EList getMessageArgument();

} // Play