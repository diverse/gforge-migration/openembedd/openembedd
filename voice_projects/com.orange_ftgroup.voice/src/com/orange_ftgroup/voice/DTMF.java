/**
 * <copyright>
 * </copyright>
 *
 * $Id: DTMF.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTMF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.DTMF#getKey <em>Key</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDTMF()
 * @model
 * @generated
 */
public interface DTMF extends DtmfInput {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.DTMFKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see com.orange_ftgroup.voice.DTMFKind
	 * @see #setKey(DTMFKind)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDTMF_Key()
	 * @model
	 * @generated
	 */
	DTMFKind getKey();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.DTMF#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see com.orange_ftgroup.voice.DTMFKind
	 * @see #getKey()
	 * @generated
	 */
	void setKey(DTMFKind value);

} // DTMF