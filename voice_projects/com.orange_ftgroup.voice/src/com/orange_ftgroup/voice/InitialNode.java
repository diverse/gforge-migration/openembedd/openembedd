/**
 * <copyright>
 * </copyright>
 *
 * $Id: InitialNode.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getInitialNode()
 * @model
 * @generated
 */
public interface InitialNode extends TransientNode {
} // InitialNode