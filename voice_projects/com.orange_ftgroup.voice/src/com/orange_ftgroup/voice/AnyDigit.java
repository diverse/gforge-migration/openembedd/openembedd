/**
 * <copyright>
 * </copyright>
 *
 * $Id: AnyDigit.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Any Digit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getAnyDigit()
 * @model
 * @generated
 */
public interface AnyDigit extends DtmfInput {
} // AnyDigit