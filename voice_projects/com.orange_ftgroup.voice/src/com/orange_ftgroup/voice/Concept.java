/**
 * <copyright>
 * </copyright>
 *
 * $Id: Concept.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Concept#getGrammar <em>Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getConcept()
 * @model
 * @generated
 */
public interface Concept extends InputEvent {
	/**
	 * Returns the value of the '<em><b>Grammar</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Grammar}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grammar</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grammar</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getConcept_Grammar()
	 * @model type="com.orange_ftgroup.voice.Grammar"
	 * @generated
	 */
	EList getGrammar();

} // Concept