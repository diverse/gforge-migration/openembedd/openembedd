/**
 * <copyright>
 * </copyright>
 *
 * $Id: ValueExpression.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getValueExpression()
 * @model abstract="true"
 * @generated
 */
public interface ValueExpression extends Expression {
} // ValueExpression