/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceService.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.VoiceService#getMainDialog <em>Main Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.VoiceService#getExtraDialog <em>Extra Dialog</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getVoiceService()
 * @model
 * @generated
 */
public interface VoiceService extends Service {
	/**
	 * Returns the value of the '<em><b>Main Dialog</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Dialog</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Dialog</em>' containment reference.
	 * @see #setMainDialog(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVoiceService_MainDialog()
	 * @model containment="true"
	 * @generated
	 */
	Dialog getMainDialog();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.VoiceService#getMainDialog <em>Main Dialog</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Dialog</em>' containment reference.
	 * @see #getMainDialog()
	 * @generated
	 */
	void setMainDialog(Dialog value);

	/**
	 * Returns the value of the '<em><b>Extra Dialog</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Dialog}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extra Dialog</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra Dialog</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getVoiceService_ExtraDialog()
	 * @model type="com.orange_ftgroup.voice.Dialog" containment="true"
	 * @generated
	 */
	EList getExtraDialog();

} // VoiceService