/**
 * <copyright>
 * </copyright>
 *
 * $Id: DigitValue.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Digit Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.DigitValue#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getDigitValue()
 * @model
 * @generated
 */
public interface DigitValue extends ValueExpression {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.DigitKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see com.orange_ftgroup.voice.DigitKind
	 * @see #setValue(DigitKind)
	 * @see com.orange_ftgroup.voice.VoicePackage#getDigitValue_Value()
	 * @model
	 * @generated
	 */
	DigitKind getValue();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.DigitValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see com.orange_ftgroup.voice.DigitKind
	 * @see #getValue()
	 * @generated
	 */
	void setValue(DigitKind value);

} // DigitValue