/**
 * <copyright>
 * </copyright>
 *
 * $Id: Transition.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Transition#isIsElse <em>Is Else</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getEffect <em>Effect</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getGuard <em>Guard</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getOwner <em>Owner</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getOrigin <em>Origin</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Transition#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Is Else</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Else</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Else</em>' attribute.
	 * @see #setIsElse(boolean)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_IsElse()
	 * @model
	 * @generated
	 */
	boolean isIsElse();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#isIsElse <em>Is Else</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Else</em>' attribute.
	 * @see #isIsElse()
	 * @generated
	 */
	void setIsElse(boolean value);

	/**
	 * Returns the value of the '<em><b>Effect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effect</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effect</em>' containment reference.
	 * @see #setEffect(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Effect()
	 * @model containment="true"
	 * @generated
	 */
	ActionSequence getEffect();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#getEffect <em>Effect</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Effect</em>' containment reference.
	 * @see #getEffect()
	 * @generated
	 */
	void setEffect(ActionSequence value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Trigger}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Trigger()
	 * @model type="com.orange_ftgroup.voice.Trigger" containment="true"
	 * @generated
	 */
	EList getTrigger();

	/**
	 * Returns the value of the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' containment reference.
	 * @see #setGuard(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Guard()
	 * @model containment="true"
	 * @generated
	 */
	Expression getGuard();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#getGuard <em>Guard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' containment reference.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(Expression value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Dialog#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Owner()
	 * @see com.orange_ftgroup.voice.Dialog#getTransition
	 * @model opposite="transition"
	 * @generated
	 */
	Dialog getOwner();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Dialog value);

	/**
	 * Returns the value of the '<em><b>Origin</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.DialogNode#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin</em>' reference.
	 * @see #setOrigin(DialogNode)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Origin()
	 * @see com.orange_ftgroup.voice.DialogNode#getOutgoing
	 * @model opposite="outgoing"
	 * @generated
	 */
	DialogNode getOrigin();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#getOrigin <em>Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' reference.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(DialogNode value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.DialogNode#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(DialogNode)
	 * @see com.orange_ftgroup.voice.VoicePackage#getTransition_Target()
	 * @see com.orange_ftgroup.voice.DialogNode#getIncoming
	 * @model opposite="incoming"
	 * @generated
	 */
	DialogNode getTarget();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Transition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(DialogNode value);

} // Transition