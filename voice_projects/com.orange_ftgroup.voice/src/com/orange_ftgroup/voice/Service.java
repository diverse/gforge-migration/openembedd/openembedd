/**
 * <copyright>
 * </copyright>
 *
 * $Id: Service.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Service#getOwnedGrammar <em>Owned Grammar</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Service#getSubService <em>Sub Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Service#getParent <em>Parent</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Service#getOfferedFunctionality <em>Offered Functionality</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Service#getEntityPackage <em>Entity Package</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Service#getFunctionalityPackage <em>Functionality Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getService()
 * @model
 * @generated
 */
public interface Service extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Owned Grammar</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Grammar}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Grammar#getService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Grammar</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Grammar</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_OwnedGrammar()
	 * @see com.orange_ftgroup.voice.Grammar#getService
	 * @model type="com.orange_ftgroup.voice.Grammar" opposite="Service" containment="true"
	 * @generated
	 */
	EList getOwnedGrammar();

	/**
	 * Returns the value of the '<em><b>Sub Service</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Service}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Service#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Service</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Service</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_SubService()
	 * @see com.orange_ftgroup.voice.Service#getParent
	 * @model type="com.orange_ftgroup.voice.Service" opposite="parent"
	 * @generated
	 */
	EList getSubService();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Service}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Service#getSubService <em>Sub Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_Parent()
	 * @see com.orange_ftgroup.voice.Service#getSubService
	 * @model type="com.orange_ftgroup.voice.Service" opposite="subService"
	 * @generated
	 */
	EList getParent();

	/**
	 * Returns the value of the '<em><b>Offered Functionality</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Functionality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offered Functionality</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offered Functionality</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_OfferedFunctionality()
	 * @model type="com.orange_ftgroup.voice.Functionality"
	 * @generated
	 */
	EList getOfferedFunctionality();

	/**
	 * Returns the value of the '<em><b>Entity Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Package</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Package</em>' containment reference.
	 * @see #setEntityPackage(com.orange_ftgroup.voice.Package)
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_EntityPackage()
	 * @model containment="true"
	 * @generated
	 */
	com.orange_ftgroup.voice.Package getEntityPackage();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Service#getEntityPackage <em>Entity Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Package</em>' containment reference.
	 * @see #getEntityPackage()
	 * @generated
	 */
	void setEntityPackage(com.orange_ftgroup.voice.Package value);

	/**
	 * Returns the value of the '<em><b>Functionality Package</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functionality Package</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functionality Package</em>' containment reference.
	 * @see #setFunctionalityPackage(com.orange_ftgroup.voice.Package)
	 * @see com.orange_ftgroup.voice.VoicePackage#getService_FunctionalityPackage()
	 * @model containment="true"
	 * @generated
	 */
	com.orange_ftgroup.voice.Package getFunctionalityPackage();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Service#getFunctionalityPackage <em>Functionality Package</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Functionality Package</em>' containment reference.
	 * @see #getFunctionalityPackage()
	 * @generated
	 */
	void setFunctionalityPackage(com.orange_ftgroup.voice.Package value);

} // Service