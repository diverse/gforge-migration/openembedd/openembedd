/**
 * <copyright>
 * </copyright>
 *
 * $Id: FixPart.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fix Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.FixPart#getContent <em>Content</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.FixPart#getFormat <em>Format</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getFixPart()
 * @model
 * @generated
 */
public interface FixPart extends MessagePart {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getFixPart_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.FixPart#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.DiffusionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @see #setFormat(DiffusionType)
	 * @see com.orange_ftgroup.voice.VoicePackage#getFixPart_Format()
	 * @model
	 * @generated
	 */
	DiffusionType getFormat();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.FixPart#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(DiffusionType value);

} // FixPart