/**
 * <copyright>
 * </copyright>
 *
 * $Id: ForEachElement.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Each Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ForEachElement#getIterator <em>Iterator</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ForEachElement#getSource <em>Source</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ForEachElement#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getForEachElement()
 * @model
 * @generated
 */
public interface ForEachElement extends MessageElement {
	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' containment reference.
	 * @see #setIterator(Variable)
	 * @see com.orange_ftgroup.voice.VoicePackage#getForEachElement_Iterator()
	 * @model containment="true"
	 * @generated
	 */
	Variable getIterator();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ForEachElement#getIterator <em>Iterator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' containment reference.
	 * @see #getIterator()
	 * @generated
	 */
	void setIterator(Variable value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getForEachElement_Source()
	 * @model containment="true"
	 * @generated
	 */
	Expression getSource();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ForEachElement#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Expression value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.MessageElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getForEachElement_Body()
	 * @model type="com.orange_ftgroup.voice.MessageElement" containment="true"
	 * @generated
	 */
	EList getBody();

} // ForEachElement