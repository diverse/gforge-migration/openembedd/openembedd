/**
 * <copyright>
 * </copyright>
 *
 * $Id: Grammar.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grammar</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#isIsComputed <em>Is Computed</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getFormalism <em>Formalism</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getLanguage <em>Language</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getContent <em>Content</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getLocation <em>Location</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getService <em>Service</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getDialog <em>Dialog</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.Grammar#getDynamicDefinition <em>Dynamic Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar()
 * @model
 * @generated
 */
public interface Grammar extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Is Computed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Computed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Computed</em>' attribute.
	 * @see #setIsComputed(boolean)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_IsComputed()
	 * @model
	 * @generated
	 */
	boolean isIsComputed();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#isIsComputed <em>Is Computed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Computed</em>' attribute.
	 * @see #isIsComputed()
	 * @generated
	 */
	void setIsComputed(boolean value);

	/**
	 * Returns the value of the '<em><b>Formalism</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formalism</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formalism</em>' attribute.
	 * @see #setFormalism(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Formalism()
	 * @model
	 * @generated
	 */
	String getFormalism();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getFormalism <em>Formalism</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formalism</em>' attribute.
	 * @see #getFormalism()
	 * @generated
	 */
	void setFormalism(String value);

	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Language()
	 * @model
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Environment#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' container reference.
	 * @see #setEnvironment(Environment)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Environment()
	 * @see com.orange_ftgroup.voice.Environment#getOwnedGrammar
	 * @model opposite="ownedGrammar"
	 * @generated
	 */
	Environment getEnvironment();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getEnvironment <em>Environment</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' container reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(Environment value);

	/**
	 * Returns the value of the '<em><b>Service</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Service#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service</em>' container reference.
	 * @see #setService(Service)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Service()
	 * @see com.orange_ftgroup.voice.Service#getOwnedGrammar
	 * @model opposite="ownedGrammar"
	 * @generated
	 */
	Service getService();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getService <em>Service</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service</em>' container reference.
	 * @see #getService()
	 * @generated
	 */
	void setService(Service value);

	/**
	 * Returns the value of the '<em><b>Dialog</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Dialog#getOwnedGrammar <em>Owned Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dialog</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dialog</em>' container reference.
	 * @see #setDialog(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_Dialog()
	 * @see com.orange_ftgroup.voice.Dialog#getOwnedGrammar
	 * @model opposite="ownedGrammar"
	 * @generated
	 */
	Dialog getDialog();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getDialog <em>Dialog</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dialog</em>' container reference.
	 * @see #getDialog()
	 * @generated
	 */
	void setDialog(Dialog value);

	/**
	 * Returns the value of the '<em><b>Dynamic Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Definition</em>' reference.
	 * @see #setDynamicDefinition(Operation)
	 * @see com.orange_ftgroup.voice.VoicePackage#getGrammar_DynamicDefinition()
	 * @model
	 * @generated
	 */
	Operation getDynamicDefinition();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.Grammar#getDynamicDefinition <em>Dynamic Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Definition</em>' reference.
	 * @see #getDynamicDefinition()
	 * @generated
	 */
	void setDynamicDefinition(Operation value);

} // Grammar