/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubDialogState.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Dialog State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.SubDialogState#getCalled <em>Called</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.SubDialogState#getArgument <em>Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getSubDialogState()
 * @model
 * @generated
 */
public interface SubDialogState extends DialogState {
	/**
	 * Returns the value of the '<em><b>Called</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Called</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Called</em>' reference.
	 * @see #setCalled(Dialog)
	 * @see com.orange_ftgroup.voice.VoicePackage#getSubDialogState_Called()
	 * @model
	 * @generated
	 */
	Dialog getCalled();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.SubDialogState#getCalled <em>Called</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Called</em>' reference.
	 * @see #getCalled()
	 * @generated
	 */
	void setCalled(Dialog value);

	/**
	 * Returns the value of the '<em><b>Argument</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getSubDialogState_Argument()
	 * @model type="com.orange_ftgroup.voice.Expression" containment="true"
	 * @generated
	 */
	EList getArgument();

} // SubDialogState