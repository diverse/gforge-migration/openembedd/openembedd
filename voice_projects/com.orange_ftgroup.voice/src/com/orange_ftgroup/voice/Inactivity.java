/**
 * <copyright>
 * </copyright>
 *
 * $Id: Inactivity.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inactivity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getInactivity()
 * @model
 * @generated
 */
public interface Inactivity extends SystemInput {
} // Inactivity