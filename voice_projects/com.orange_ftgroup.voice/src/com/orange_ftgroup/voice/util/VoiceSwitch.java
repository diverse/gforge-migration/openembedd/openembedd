/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceSwitch.java,v 1.1 2008-04-10 15:45:40 vmahe Exp $
 */
package com.orange_ftgroup.voice.util;

import com.orange_ftgroup.voice.AbstractDTMF;
import com.orange_ftgroup.voice.Action;
import com.orange_ftgroup.voice.ActionSequence;
import com.orange_ftgroup.voice.Actor;
import com.orange_ftgroup.voice.AnyDigit;
import com.orange_ftgroup.voice.AnyDtmf;
import com.orange_ftgroup.voice.AnyState;
import com.orange_ftgroup.voice.Assignment;
import com.orange_ftgroup.voice.Behavior;
import com.orange_ftgroup.voice.BinaryExpression;
import com.orange_ftgroup.voice.BooleanValue;
import com.orange_ftgroup.voice.CallAction;
import com.orange_ftgroup.voice.CallExpression;
import com.orange_ftgroup.voice.CharacterValue;
import com.orange_ftgroup.voice.CharstringValue;
import com.orange_ftgroup.voice.ChoiceType;
import com.orange_ftgroup.voice.CollectionItem;
import com.orange_ftgroup.voice.CollectionLiteralExpression;
import com.orange_ftgroup.voice.CollectionLiteralPart;
import com.orange_ftgroup.voice.CollectionRange;
import com.orange_ftgroup.voice.Concept;
import com.orange_ftgroup.voice.ConditionalElement;
import com.orange_ftgroup.voice.ConditionalExpression;
import com.orange_ftgroup.voice.Constraint;
import com.orange_ftgroup.voice.DTMF;
import com.orange_ftgroup.voice.DTMFValue;
import com.orange_ftgroup.voice.DataType;
import com.orange_ftgroup.voice.DecisionNode;
import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.DialogNode;
import com.orange_ftgroup.voice.DialogState;
import com.orange_ftgroup.voice.DigitValue;
import com.orange_ftgroup.voice.DiversionNode;
import com.orange_ftgroup.voice.DtmfInput;
import com.orange_ftgroup.voice.Entity;
import com.orange_ftgroup.voice.EnumValue;
import com.orange_ftgroup.voice.EnumerationItem;
import com.orange_ftgroup.voice.EnumerationType;
import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.Expression;
import com.orange_ftgroup.voice.ExternalEvent;
import com.orange_ftgroup.voice.FeatureExpression;
import com.orange_ftgroup.voice.FinalNode;
import com.orange_ftgroup.voice.FixPart;
import com.orange_ftgroup.voice.FloatValue;
import com.orange_ftgroup.voice.ForEachElement;
import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.HistoryState;
import com.orange_ftgroup.voice.Ident;
import com.orange_ftgroup.voice.IfThenElse;
import com.orange_ftgroup.voice.Inactivity;
import com.orange_ftgroup.voice.InformalExpression;
import com.orange_ftgroup.voice.InitialNode;
import com.orange_ftgroup.voice.InputEvent;
import com.orange_ftgroup.voice.IntegerValue;
import com.orange_ftgroup.voice.JunctionNode;
import com.orange_ftgroup.voice.ListExpression;
import com.orange_ftgroup.voice.ListState;
import com.orange_ftgroup.voice.ListType;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.MessageElement;
import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.MessageEnd;
import com.orange_ftgroup.voice.MessagePart;
import com.orange_ftgroup.voice.ModelElement;
import com.orange_ftgroup.voice.NamedElement;
import com.orange_ftgroup.voice.NewExpression;
import com.orange_ftgroup.voice.NextNode;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.PackageableElement;
import com.orange_ftgroup.voice.Parameter;
import com.orange_ftgroup.voice.ParenthesisExpression;
import com.orange_ftgroup.voice.Play;
import com.orange_ftgroup.voice.Property;
import com.orange_ftgroup.voice.PropertyExpression;
import com.orange_ftgroup.voice.Recording;
import com.orange_ftgroup.voice.Reject;
import com.orange_ftgroup.voice.ReturnAction;
import com.orange_ftgroup.voice.ReturnNode;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.Signature;
import com.orange_ftgroup.voice.SilencePart;
import com.orange_ftgroup.voice.StateMachine;
import com.orange_ftgroup.voice.StopNode;
import com.orange_ftgroup.voice.SubDialogState;
import com.orange_ftgroup.voice.SystemInput;
import com.orange_ftgroup.voice.Tag;
import com.orange_ftgroup.voice.TransientNode;
import com.orange_ftgroup.voice.Transition;
import com.orange_ftgroup.voice.Trigger;
import com.orange_ftgroup.voice.Type;
import com.orange_ftgroup.voice.TypedElement;
import com.orange_ftgroup.voice.UnaryExpression;
import com.orange_ftgroup.voice.Uninterpreted;
import com.orange_ftgroup.voice.UseElement;
import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.ValueExpression;
import com.orange_ftgroup.voice.Variable;
import com.orange_ftgroup.voice.VariablePart;
import com.orange_ftgroup.voice.VoicePackage;
import com.orange_ftgroup.voice.VoiceService;
import com.orange_ftgroup.voice.WaitState;
import com.orange_ftgroup.voice.While;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoicePackage
 * @generated
 */
public class VoiceSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VoicePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoiceSwitch() {
		if (modelPackage == null) {
			modelPackage = VoicePackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VoicePackage.MODEL_ELEMENT: {
				ModelElement modelElement = (ModelElement)theEObject;
				Object result = caseModelElement(modelElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				Object result = caseNamedElement(namedElement);
				if (result == null) result = caseModelElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.EXPRESSION: {
				Expression expression = (Expression)theEObject;
				Object result = caseExpression(expression);
				if (result == null) result = caseModelElement(expression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TYPED_ELEMENT: {
				TypedElement typedElement = (TypedElement)theEObject;
				Object result = caseTypedElement(typedElement);
				if (result == null) result = caseNamedElement(typedElement);
				if (result == null) result = caseModelElement(typedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TYPE: {
				Type type = (Type)theEObject;
				Object result = caseType(type);
				if (result == null) result = casePackageableElement(type);
				if (result == null) result = caseNamedElement(type);
				if (result == null) result = caseModelElement(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				Object result = caseParameter(parameter);
				if (result == null) result = caseValueContainer(parameter);
				if (result == null) result = caseTypedElement(parameter);
				if (result == null) result = caseNamedElement(parameter);
				if (result == null) result = caseModelElement(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.OPERATION: {
				Operation operation = (Operation)theEObject;
				Object result = caseOperation(operation);
				if (result == null) result = caseSignature(operation);
				if (result == null) result = caseTypedElement(operation);
				if (result == null) result = caseNamedElement(operation);
				if (result == null) result = caseModelElement(operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CONSTRAINT: {
				Constraint constraint = (Constraint)theEObject;
				Object result = caseConstraint(constraint);
				if (result == null) result = casePackageableElement(constraint);
				if (result == null) result = caseNamedElement(constraint);
				if (result == null) result = caseModelElement(constraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PACKAGE: {
				com.orange_ftgroup.voice.Package package_ = (com.orange_ftgroup.voice.Package)theEObject;
				Object result = casePackage(package_);
				if (result == null) result = casePackageableElement(package_);
				if (result == null) result = caseNamedElement(package_);
				if (result == null) result = caseModelElement(package_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DATA_TYPE: {
				DataType dataType = (DataType)theEObject;
				Object result = caseDataType(dataType);
				if (result == null) result = caseType(dataType);
				if (result == null) result = casePackageableElement(dataType);
				if (result == null) result = caseNamedElement(dataType);
				if (result == null) result = caseModelElement(dataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TAG: {
				Tag tag = (Tag)theEObject;
				Object result = caseTag(tag);
				if (result == null) result = caseNamedElement(tag);
				if (result == null) result = caseModelElement(tag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PROPERTY: {
				Property property = (Property)theEObject;
				Object result = caseProperty(property);
				if (result == null) result = caseValueContainer(property);
				if (result == null) result = caseTypedElement(property);
				if (result == null) result = caseNamedElement(property);
				if (result == null) result = caseModelElement(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CLASS: {
				com.orange_ftgroup.voice.Class class_ = (com.orange_ftgroup.voice.Class)theEObject;
				Object result = caseClass(class_);
				if (result == null) result = caseType(class_);
				if (result == null) result = casePackageableElement(class_);
				if (result == null) result = caseNamedElement(class_);
				if (result == null) result = caseModelElement(class_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.BINARY_EXPRESSION: {
				BinaryExpression binaryExpression = (BinaryExpression)theEObject;
				Object result = caseBinaryExpression(binaryExpression);
				if (result == null) result = caseExpression(binaryExpression);
				if (result == null) result = caseModelElement(binaryExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.UNARY_EXPRESSION: {
				UnaryExpression unaryExpression = (UnaryExpression)theEObject;
				Object result = caseUnaryExpression(unaryExpression);
				if (result == null) result = caseExpression(unaryExpression);
				if (result == null) result = caseModelElement(unaryExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.VALUE_EXPRESSION: {
				ValueExpression valueExpression = (ValueExpression)theEObject;
				Object result = caseValueExpression(valueExpression);
				if (result == null) result = caseExpression(valueExpression);
				if (result == null) result = caseModelElement(valueExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CHARACTER_VALUE: {
				CharacterValue characterValue = (CharacterValue)theEObject;
				Object result = caseCharacterValue(characterValue);
				if (result == null) result = caseValueExpression(characterValue);
				if (result == null) result = caseExpression(characterValue);
				if (result == null) result = caseModelElement(characterValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.INTEGER_VALUE: {
				IntegerValue integerValue = (IntegerValue)theEObject;
				Object result = caseIntegerValue(integerValue);
				if (result == null) result = caseValueExpression(integerValue);
				if (result == null) result = caseExpression(integerValue);
				if (result == null) result = caseModelElement(integerValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FLOAT_VALUE: {
				FloatValue floatValue = (FloatValue)theEObject;
				Object result = caseFloatValue(floatValue);
				if (result == null) result = caseValueExpression(floatValue);
				if (result == null) result = caseExpression(floatValue);
				if (result == null) result = caseModelElement(floatValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.BOOLEAN_VALUE: {
				BooleanValue booleanValue = (BooleanValue)theEObject;
				Object result = caseBooleanValue(booleanValue);
				if (result == null) result = caseValueExpression(booleanValue);
				if (result == null) result = caseExpression(booleanValue);
				if (result == null) result = caseModelElement(booleanValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CHARSTRING_VALUE: {
				CharstringValue charstringValue = (CharstringValue)theEObject;
				Object result = caseCharstringValue(charstringValue);
				if (result == null) result = caseValueExpression(charstringValue);
				if (result == null) result = caseExpression(charstringValue);
				if (result == null) result = caseModelElement(charstringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.IDENT: {
				Ident ident = (Ident)theEObject;
				Object result = caseIdent(ident);
				if (result == null) result = caseValueExpression(ident);
				if (result == null) result = caseExpression(ident);
				if (result == null) result = caseModelElement(ident);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PARENTHESIS_EXPRESSION: {
				ParenthesisExpression parenthesisExpression = (ParenthesisExpression)theEObject;
				Object result = caseParenthesisExpression(parenthesisExpression);
				if (result == null) result = caseExpression(parenthesisExpression);
				if (result == null) result = caseModelElement(parenthesisExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CONDITIONAL_EXPRESSION: {
				ConditionalExpression conditionalExpression = (ConditionalExpression)theEObject;
				Object result = caseConditionalExpression(conditionalExpression);
				if (result == null) result = caseExpression(conditionalExpression);
				if (result == null) result = caseModelElement(conditionalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.LIST_EXPRESSION: {
				ListExpression listExpression = (ListExpression)theEObject;
				Object result = caseListExpression(listExpression);
				if (result == null) result = caseExpression(listExpression);
				if (result == null) result = caseModelElement(listExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CALL_EXPRESSION: {
				CallExpression callExpression = (CallExpression)theEObject;
				Object result = caseCallExpression(callExpression);
				if (result == null) result = caseFeatureExpression(callExpression);
				if (result == null) result = caseExpression(callExpression);
				if (result == null) result = caseModelElement(callExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ENUMERATION_TYPE: {
				EnumerationType enumerationType = (EnumerationType)theEObject;
				Object result = caseEnumerationType(enumerationType);
				if (result == null) result = caseDataType(enumerationType);
				if (result == null) result = caseType(enumerationType);
				if (result == null) result = casePackageableElement(enumerationType);
				if (result == null) result = caseNamedElement(enumerationType);
				if (result == null) result = caseModelElement(enumerationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ENUMERATION_ITEM: {
				EnumerationItem enumerationItem = (EnumerationItem)theEObject;
				Object result = caseEnumerationItem(enumerationItem);
				if (result == null) result = caseNamedElement(enumerationItem);
				if (result == null) result = caseModelElement(enumerationItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CHOICE_TYPE: {
				ChoiceType choiceType = (ChoiceType)theEObject;
				Object result = caseChoiceType(choiceType);
				if (result == null) result = caseDataType(choiceType);
				if (result == null) result = caseType(choiceType);
				if (result == null) result = casePackageableElement(choiceType);
				if (result == null) result = caseNamedElement(choiceType);
				if (result == null) result = caseModelElement(choiceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.LIST_TYPE: {
				ListType listType = (ListType)theEObject;
				Object result = caseListType(listType);
				if (result == null) result = caseDataType(listType);
				if (result == null) result = caseType(listType);
				if (result == null) result = casePackageableElement(listType);
				if (result == null) result = caseNamedElement(listType);
				if (result == null) result = caseModelElement(listType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.VARIABLE: {
				Variable variable = (Variable)theEObject;
				Object result = caseVariable(variable);
				if (result == null) result = caseTypedElement(variable);
				if (result == null) result = caseValueContainer(variable);
				if (result == null) result = caseNamedElement(variable);
				if (result == null) result = caseModelElement(variable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.SIGNATURE: {
				Signature signature = (Signature)theEObject;
				Object result = caseSignature(signature);
				if (result == null) result = caseTypedElement(signature);
				if (result == null) result = caseNamedElement(signature);
				if (result == null) result = caseModelElement(signature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.INFORMAL_EXPRESSION: {
				InformalExpression informalExpression = (InformalExpression)theEObject;
				Object result = caseInformalExpression(informalExpression);
				if (result == null) result = caseValueExpression(informalExpression);
				if (result == null) result = caseExpression(informalExpression);
				if (result == null) result = caseModelElement(informalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PACKAGEABLE_ELEMENT: {
				PackageableElement packageableElement = (PackageableElement)theEObject;
				Object result = casePackageableElement(packageableElement);
				if (result == null) result = caseNamedElement(packageableElement);
				if (result == null) result = caseModelElement(packageableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ACTION_SEQUENCE: {
				ActionSequence actionSequence = (ActionSequence)theEObject;
				Object result = caseActionSequence(actionSequence);
				if (result == null) result = caseModelElement(actionSequence);
				if (result == null) result = caseBehavior(actionSequence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ACTION: {
				Action action = (Action)theEObject;
				Object result = caseAction(action);
				if (result == null) result = caseModelElement(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DIGIT_VALUE: {
				DigitValue digitValue = (DigitValue)theEObject;
				Object result = caseDigitValue(digitValue);
				if (result == null) result = caseValueExpression(digitValue);
				if (result == null) result = caseExpression(digitValue);
				if (result == null) result = caseModelElement(digitValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DTMF_VALUE: {
				DTMFValue dtmfValue = (DTMFValue)theEObject;
				Object result = caseDTMFValue(dtmfValue);
				if (result == null) result = caseValueExpression(dtmfValue);
				if (result == null) result = caseExpression(dtmfValue);
				if (result == null) result = caseModelElement(dtmfValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FEATURE_EXPRESSION: {
				FeatureExpression featureExpression = (FeatureExpression)theEObject;
				Object result = caseFeatureExpression(featureExpression);
				if (result == null) result = caseExpression(featureExpression);
				if (result == null) result = caseModelElement(featureExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PROPERTY_EXPRESSION: {
				PropertyExpression propertyExpression = (PropertyExpression)theEObject;
				Object result = casePropertyExpression(propertyExpression);
				if (result == null) result = caseFeatureExpression(propertyExpression);
				if (result == null) result = caseExpression(propertyExpression);
				if (result == null) result = caseModelElement(propertyExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.COLLECTION_RANGE: {
				CollectionRange collectionRange = (CollectionRange)theEObject;
				Object result = caseCollectionRange(collectionRange);
				if (result == null) result = caseCollectionLiteralPart(collectionRange);
				if (result == null) result = caseTypedElement(collectionRange);
				if (result == null) result = caseNamedElement(collectionRange);
				if (result == null) result = caseModelElement(collectionRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.COLLECTION_LITERAL_EXPRESSION: {
				CollectionLiteralExpression collectionLiteralExpression = (CollectionLiteralExpression)theEObject;
				Object result = caseCollectionLiteralExpression(collectionLiteralExpression);
				if (result == null) result = caseValueExpression(collectionLiteralExpression);
				if (result == null) result = caseExpression(collectionLiteralExpression);
				if (result == null) result = caseModelElement(collectionLiteralExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.COLLECTION_ITEM: {
				CollectionItem collectionItem = (CollectionItem)theEObject;
				Object result = caseCollectionItem(collectionItem);
				if (result == null) result = caseCollectionLiteralPart(collectionItem);
				if (result == null) result = caseTypedElement(collectionItem);
				if (result == null) result = caseNamedElement(collectionItem);
				if (result == null) result = caseModelElement(collectionItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.COLLECTION_LITERAL_PART: {
				CollectionLiteralPart collectionLiteralPart = (CollectionLiteralPart)theEObject;
				Object result = caseCollectionLiteralPart(collectionLiteralPart);
				if (result == null) result = caseTypedElement(collectionLiteralPart);
				if (result == null) result = caseNamedElement(collectionLiteralPart);
				if (result == null) result = caseModelElement(collectionLiteralPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ENUM_VALUE: {
				EnumValue enumValue = (EnumValue)theEObject;
				Object result = caseEnumValue(enumValue);
				if (result == null) result = caseValueExpression(enumValue);
				if (result == null) result = caseExpression(enumValue);
				if (result == null) result = caseModelElement(enumValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.BEHAVIOR: {
				Behavior behavior = (Behavior)theEObject;
				Object result = caseBehavior(behavior);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.STATE_MACHINE: {
				StateMachine stateMachine = (StateMachine)theEObject;
				Object result = caseStateMachine(stateMachine);
				if (result == null) result = caseBehavior(stateMachine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.NEW_EXPRESSION: {
				NewExpression newExpression = (NewExpression)theEObject;
				Object result = caseNewExpression(newExpression);
				if (result == null) result = caseExpression(newExpression);
				if (result == null) result = caseModelElement(newExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.VALUE_CONTAINER: {
				ValueContainer valueContainer = (ValueContainer)theEObject;
				Object result = caseValueContainer(valueContainer);
				if (result == null) result = caseTypedElement(valueContainer);
				if (result == null) result = caseNamedElement(valueContainer);
				if (result == null) result = caseModelElement(valueContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DIALOG: {
				Dialog dialog = (Dialog)theEObject;
				Object result = caseDialog(dialog);
				if (result == null) result = caseSignature(dialog);
				if (result == null) result = casePackageableElement(dialog);
				if (result == null) result = caseTypedElement(dialog);
				if (result == null) result = caseNamedElement(dialog);
				if (result == null) result = caseModelElement(dialog);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.SUB_DIALOG_STATE: {
				SubDialogState subDialogState = (SubDialogState)theEObject;
				Object result = caseSubDialogState(subDialogState);
				if (result == null) result = caseDialogState(subDialogState);
				if (result == null) result = caseDialogNode(subDialogState);
				if (result == null) result = caseNamedElement(subDialogState);
				if (result == null) result = caseModelElement(subDialogState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DIALOG_STATE: {
				DialogState dialogState = (DialogState)theEObject;
				Object result = caseDialogState(dialogState);
				if (result == null) result = caseDialogNode(dialogState);
				if (result == null) result = caseNamedElement(dialogState);
				if (result == null) result = caseModelElement(dialogState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DIALOG_NODE: {
				DialogNode dialogNode = (DialogNode)theEObject;
				Object result = caseDialogNode(dialogNode);
				if (result == null) result = caseNamedElement(dialogNode);
				if (result == null) result = caseModelElement(dialogNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TRANSITION: {
				Transition transition = (Transition)theEObject;
				Object result = caseTransition(transition);
				if (result == null) result = caseNamedElement(transition);
				if (result == null) result = caseModelElement(transition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TRIGGER: {
				Trigger trigger = (Trigger)theEObject;
				Object result = caseTrigger(trigger);
				if (result == null) result = caseModelElement(trigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.TRANSIENT_NODE: {
				TransientNode transientNode = (TransientNode)theEObject;
				Object result = caseTransientNode(transientNode);
				if (result == null) result = caseDialogNode(transientNode);
				if (result == null) result = caseNamedElement(transientNode);
				if (result == null) result = caseModelElement(transientNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.WAIT_STATE: {
				WaitState waitState = (WaitState)theEObject;
				Object result = caseWaitState(waitState);
				if (result == null) result = caseDialogState(waitState);
				if (result == null) result = caseNamedElement(waitState);
				if (result == null) result = caseDialogNode(waitState);
				if (result == null) result = caseModelElement(waitState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ANY_STATE: {
				AnyState anyState = (AnyState)theEObject;
				Object result = caseAnyState(anyState);
				if (result == null) result = caseDialogState(anyState);
				if (result == null) result = caseDialogNode(anyState);
				if (result == null) result = caseNamedElement(anyState);
				if (result == null) result = caseModelElement(anyState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.LIST_STATE: {
				ListState listState = (ListState)theEObject;
				Object result = caseListState(listState);
				if (result == null) result = caseDialogState(listState);
				if (result == null) result = caseDialogNode(listState);
				if (result == null) result = caseNamedElement(listState);
				if (result == null) result = caseModelElement(listState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DECISION_NODE: {
				DecisionNode decisionNode = (DecisionNode)theEObject;
				Object result = caseDecisionNode(decisionNode);
				if (result == null) result = caseTransientNode(decisionNode);
				if (result == null) result = caseDialogNode(decisionNode);
				if (result == null) result = caseNamedElement(decisionNode);
				if (result == null) result = caseModelElement(decisionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.INITIAL_NODE: {
				InitialNode initialNode = (InitialNode)theEObject;
				Object result = caseInitialNode(initialNode);
				if (result == null) result = caseTransientNode(initialNode);
				if (result == null) result = caseDialogNode(initialNode);
				if (result == null) result = caseNamedElement(initialNode);
				if (result == null) result = caseModelElement(initialNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.JUNCTION_NODE: {
				JunctionNode junctionNode = (JunctionNode)theEObject;
				Object result = caseJunctionNode(junctionNode);
				if (result == null) result = caseTransientNode(junctionNode);
				if (result == null) result = caseNamedElement(junctionNode);
				if (result == null) result = caseDialogNode(junctionNode);
				if (result == null) result = caseModelElement(junctionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DIVERSION_NODE: {
				DiversionNode diversionNode = (DiversionNode)theEObject;
				Object result = caseDiversionNode(diversionNode);
				if (result == null) result = caseFinalNode(diversionNode);
				if (result == null) result = caseTransientNode(diversionNode);
				if (result == null) result = caseDialogNode(diversionNode);
				if (result == null) result = caseNamedElement(diversionNode);
				if (result == null) result = caseModelElement(diversionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.HISTORY_STATE: {
				HistoryState historyState = (HistoryState)theEObject;
				Object result = caseHistoryState(historyState);
				if (result == null) result = caseDialogState(historyState);
				if (result == null) result = caseDialogNode(historyState);
				if (result == null) result = caseNamedElement(historyState);
				if (result == null) result = caseModelElement(historyState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FINAL_NODE: {
				FinalNode finalNode = (FinalNode)theEObject;
				Object result = caseFinalNode(finalNode);
				if (result == null) result = caseTransientNode(finalNode);
				if (result == null) result = caseDialogNode(finalNode);
				if (result == null) result = caseNamedElement(finalNode);
				if (result == null) result = caseModelElement(finalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.NEXT_NODE: {
				NextNode nextNode = (NextNode)theEObject;
				Object result = caseNextNode(nextNode);
				if (result == null) result = caseFinalNode(nextNode);
				if (result == null) result = caseTransientNode(nextNode);
				if (result == null) result = caseDialogNode(nextNode);
				if (result == null) result = caseNamedElement(nextNode);
				if (result == null) result = caseModelElement(nextNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.STOP_NODE: {
				StopNode stopNode = (StopNode)theEObject;
				Object result = caseStopNode(stopNode);
				if (result == null) result = caseFinalNode(stopNode);
				if (result == null) result = caseTransientNode(stopNode);
				if (result == null) result = caseDialogNode(stopNode);
				if (result == null) result = caseNamedElement(stopNode);
				if (result == null) result = caseModelElement(stopNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.RETURN_NODE: {
				ReturnNode returnNode = (ReturnNode)theEObject;
				Object result = caseReturnNode(returnNode);
				if (result == null) result = caseFinalNode(returnNode);
				if (result == null) result = caseTransientNode(returnNode);
				if (result == null) result = caseDialogNode(returnNode);
				if (result == null) result = caseNamedElement(returnNode);
				if (result == null) result = caseModelElement(returnNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ASSIGNMENT: {
				Assignment assignment = (Assignment)theEObject;
				Object result = caseAssignment(assignment);
				if (result == null) result = caseAction(assignment);
				if (result == null) result = caseModelElement(assignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.PLAY: {
				Play play = (Play)theEObject;
				Object result = casePlay(play);
				if (result == null) result = caseAction(play);
				if (result == null) result = caseModelElement(play);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.UNINTERPRETED: {
				Uninterpreted uninterpreted = (Uninterpreted)theEObject;
				Object result = caseUninterpreted(uninterpreted);
				if (result == null) result = caseAction(uninterpreted);
				if (result == null) result = caseModelElement(uninterpreted);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.IF_THEN_ELSE: {
				IfThenElse ifThenElse = (IfThenElse)theEObject;
				Object result = caseIfThenElse(ifThenElse);
				if (result == null) result = caseAction(ifThenElse);
				if (result == null) result = caseModelElement(ifThenElse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.WHILE: {
				While while_ = (While)theEObject;
				Object result = caseWhile(while_);
				if (result == null) result = caseAction(while_);
				if (result == null) result = caseModelElement(while_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.RETURN_ACTION: {
				ReturnAction returnAction = (ReturnAction)theEObject;
				Object result = caseReturnAction(returnAction);
				if (result == null) result = caseAction(returnAction);
				if (result == null) result = caseModelElement(returnAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CALL_ACTION: {
				CallAction callAction = (CallAction)theEObject;
				Object result = caseCallAction(callAction);
				if (result == null) result = caseAction(callAction);
				if (result == null) result = caseModelElement(callAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.INPUT_EVENT: {
				InputEvent inputEvent = (InputEvent)theEObject;
				Object result = caseInputEvent(inputEvent);
				if (result == null) result = caseNamedElement(inputEvent);
				if (result == null) result = caseModelElement(inputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DTMF: {
				DTMF dtmf = (DTMF)theEObject;
				Object result = caseDTMF(dtmf);
				if (result == null) result = caseDtmfInput(dtmf);
				if (result == null) result = caseInputEvent(dtmf);
				if (result == null) result = caseNamedElement(dtmf);
				if (result == null) result = caseModelElement(dtmf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.REJECT: {
				Reject reject = (Reject)theEObject;
				Object result = caseReject(reject);
				if (result == null) result = caseSystemInput(reject);
				if (result == null) result = caseInputEvent(reject);
				if (result == null) result = caseNamedElement(reject);
				if (result == null) result = caseModelElement(reject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.INACTIVITY: {
				Inactivity inactivity = (Inactivity)theEObject;
				Object result = caseInactivity(inactivity);
				if (result == null) result = caseSystemInput(inactivity);
				if (result == null) result = caseInputEvent(inactivity);
				if (result == null) result = caseNamedElement(inactivity);
				if (result == null) result = caseModelElement(inactivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CONCEPT: {
				Concept concept = (Concept)theEObject;
				Object result = caseConcept(concept);
				if (result == null) result = caseInputEvent(concept);
				if (result == null) result = caseNamedElement(concept);
				if (result == null) result = caseModelElement(concept);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.EXTERNAL_EVENT: {
				ExternalEvent externalEvent = (ExternalEvent)theEObject;
				Object result = caseExternalEvent(externalEvent);
				if (result == null) result = caseInputEvent(externalEvent);
				if (result == null) result = caseNamedElement(externalEvent);
				if (result == null) result = caseModelElement(externalEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ANY_DTMF: {
				AnyDtmf anyDtmf = (AnyDtmf)theEObject;
				Object result = caseAnyDtmf(anyDtmf);
				if (result == null) result = caseDtmfInput(anyDtmf);
				if (result == null) result = caseInputEvent(anyDtmf);
				if (result == null) result = caseNamedElement(anyDtmf);
				if (result == null) result = caseModelElement(anyDtmf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ANY_DIGIT: {
				AnyDigit anyDigit = (AnyDigit)theEObject;
				Object result = caseAnyDigit(anyDigit);
				if (result == null) result = caseDtmfInput(anyDigit);
				if (result == null) result = caseInputEvent(anyDigit);
				if (result == null) result = caseNamedElement(anyDigit);
				if (result == null) result = caseModelElement(anyDigit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.RECORDING: {
				Recording recording = (Recording)theEObject;
				Object result = caseRecording(recording);
				if (result == null) result = caseSystemInput(recording);
				if (result == null) result = caseInputEvent(recording);
				if (result == null) result = caseNamedElement(recording);
				if (result == null) result = caseModelElement(recording);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ABSTRACT_DTMF: {
				AbstractDTMF abstractDTMF = (AbstractDTMF)theEObject;
				Object result = caseAbstractDTMF(abstractDTMF);
				if (result == null) result = caseDtmfInput(abstractDTMF);
				if (result == null) result = caseInputEvent(abstractDTMF);
				if (result == null) result = caseNamedElement(abstractDTMF);
				if (result == null) result = caseModelElement(abstractDTMF);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.MESSAGE_END: {
				MessageEnd messageEnd = (MessageEnd)theEObject;
				Object result = caseMessageEnd(messageEnd);
				if (result == null) result = caseSystemInput(messageEnd);
				if (result == null) result = caseInputEvent(messageEnd);
				if (result == null) result = caseNamedElement(messageEnd);
				if (result == null) result = caseModelElement(messageEnd);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.DTMF_INPUT: {
				DtmfInput dtmfInput = (DtmfInput)theEObject;
				Object result = caseDtmfInput(dtmfInput);
				if (result == null) result = caseInputEvent(dtmfInput);
				if (result == null) result = caseNamedElement(dtmfInput);
				if (result == null) result = caseModelElement(dtmfInput);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.SYSTEM_INPUT: {
				SystemInput systemInput = (SystemInput)theEObject;
				Object result = caseSystemInput(systemInput);
				if (result == null) result = caseInputEvent(systemInput);
				if (result == null) result = caseNamedElement(systemInput);
				if (result == null) result = caseModelElement(systemInput);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.MESSAGE: {
				Message message = (Message)theEObject;
				Object result = caseMessage(message);
				if (result == null) result = caseSignature(message);
				if (result == null) result = caseTypedElement(message);
				if (result == null) result = caseNamedElement(message);
				if (result == null) result = caseModelElement(message);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.MESSAGE_PART: {
				MessagePart messagePart = (MessagePart)theEObject;
				Object result = caseMessagePart(messagePart);
				if (result == null) result = caseSignature(messagePart);
				if (result == null) result = caseTypedElement(messagePart);
				if (result == null) result = caseNamedElement(messagePart);
				if (result == null) result = caseModelElement(messagePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FIX_PART: {
				FixPart fixPart = (FixPart)theEObject;
				Object result = caseFixPart(fixPart);
				if (result == null) result = caseMessagePart(fixPart);
				if (result == null) result = caseSignature(fixPart);
				if (result == null) result = caseTypedElement(fixPart);
				if (result == null) result = caseNamedElement(fixPart);
				if (result == null) result = caseModelElement(fixPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.VARIABLE_PART: {
				VariablePart variablePart = (VariablePart)theEObject;
				Object result = caseVariablePart(variablePart);
				if (result == null) result = caseMessagePart(variablePart);
				if (result == null) result = caseSignature(variablePart);
				if (result == null) result = caseTypedElement(variablePart);
				if (result == null) result = caseNamedElement(variablePart);
				if (result == null) result = caseModelElement(variablePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.CONDITIONAL_ELEMENT: {
				ConditionalElement conditionalElement = (ConditionalElement)theEObject;
				Object result = caseConditionalElement(conditionalElement);
				if (result == null) result = caseMessageElement(conditionalElement);
				if (result == null) result = caseModelElement(conditionalElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.SILENCE_PART: {
				SilencePart silencePart = (SilencePart)theEObject;
				Object result = caseSilencePart(silencePart);
				if (result == null) result = caseMessagePart(silencePart);
				if (result == null) result = caseSignature(silencePart);
				if (result == null) result = caseTypedElement(silencePart);
				if (result == null) result = caseNamedElement(silencePart);
				if (result == null) result = caseModelElement(silencePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.MESSAGE_ELEMENT: {
				MessageElement messageElement = (MessageElement)theEObject;
				Object result = caseMessageElement(messageElement);
				if (result == null) result = caseModelElement(messageElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.USE_ELEMENT: {
				UseElement useElement = (UseElement)theEObject;
				Object result = caseUseElement(useElement);
				if (result == null) result = caseMessageElement(useElement);
				if (result == null) result = caseModelElement(useElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.MESSAGE_ELEMENT_CONDITION: {
				MessageElementCondition messageElementCondition = (MessageElementCondition)theEObject;
				Object result = caseMessageElementCondition(messageElementCondition);
				if (result == null) result = caseModelElement(messageElementCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FOR_EACH_ELEMENT: {
				ForEachElement forEachElement = (ForEachElement)theEObject;
				Object result = caseForEachElement(forEachElement);
				if (result == null) result = caseMessageElement(forEachElement);
				if (result == null) result = caseModelElement(forEachElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.GRAMMAR: {
				Grammar grammar = (Grammar)theEObject;
				Object result = caseGrammar(grammar);
				if (result == null) result = caseNamedElement(grammar);
				if (result == null) result = caseModelElement(grammar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.VOICE_SERVICE: {
				VoiceService voiceService = (VoiceService)theEObject;
				Object result = caseVoiceService(voiceService);
				if (result == null) result = caseService(voiceService);
				if (result == null) result = caseNamedElement(voiceService);
				if (result == null) result = caseModelElement(voiceService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.SERVICE: {
				Service service = (Service)theEObject;
				Object result = caseService(service);
				if (result == null) result = caseNamedElement(service);
				if (result == null) result = caseModelElement(service);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.FUNCTIONALITY: {
				Functionality functionality = (Functionality)theEObject;
				Object result = caseFunctionality(functionality);
				if (result == null) result = casePackageableElement(functionality);
				if (result == null) result = caseNamedElement(functionality);
				if (result == null) result = caseModelElement(functionality);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ACTOR: {
				Actor actor = (Actor)theEObject;
				Object result = caseActor(actor);
				if (result == null) result = casePackageableElement(actor);
				if (result == null) result = caseNamedElement(actor);
				if (result == null) result = caseModelElement(actor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ENTITY: {
				Entity entity = (Entity)theEObject;
				Object result = caseEntity(entity);
				if (result == null) result = caseClass(entity);
				if (result == null) result = caseType(entity);
				if (result == null) result = casePackageableElement(entity);
				if (result == null) result = caseNamedElement(entity);
				if (result == null) result = caseModelElement(entity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VoicePackage.ENVIRONMENT: {
				Environment environment = (Environment)theEObject;
				Object result = caseEnvironment(environment);
				if (result == null) result = caseNamedElement(environment);
				if (result == null) result = caseModelElement(environment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseModelElement(ModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTypedElement(TypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseOperation(Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConstraint(Constraint object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePackage(com.orange_ftgroup.voice.Package object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDataType(DataType object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTag(Tag object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseClass(com.orange_ftgroup.voice.Class object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBinaryExpression(BinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseUnaryExpression(UnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Value Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Value Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseValueExpression(ValueExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Character Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Character Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCharacterValue(CharacterValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Integer Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Integer Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIntegerValue(IntegerValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Float Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFloatValue(FloatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Boolean Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBooleanValue(BooleanValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Charstring Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Charstring Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCharstringValue(CharstringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Ident</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Ident</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIdent(Ident object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Parenthesis Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Parenthesis Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseParenthesisExpression(ParenthesisExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Conditional Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Conditional Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConditionalExpression(ConditionalExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>List Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>List Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseListExpression(ListExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCallExpression(CallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Enumeration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEnumerationType(EnumerationType object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Enumeration Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Enumeration Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEnumerationItem(EnumerationItem object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Choice Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Choice Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseChoiceType(ChoiceType object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>List Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>List Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseListType(ListType object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSignature(Signature object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Informal Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Informal Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseInformalExpression(InformalExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Packageable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Packageable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePackageableElement(PackageableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Action Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Action Sequence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseActionSequence(ActionSequence object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Digit Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Digit Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDigitValue(DigitValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>DTMF Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>DTMF Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDTMFValue(DTMFValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Feature Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Feature Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFeatureExpression(FeatureExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Property Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Property Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePropertyExpression(PropertyExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Collection Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Collection Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCollectionRange(CollectionRange object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Collection Literal Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Collection Literal Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCollectionLiteralExpression(CollectionLiteralExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Collection Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Collection Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCollectionItem(CollectionItem object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Collection Literal Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Collection Literal Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCollectionLiteralPart(CollectionLiteralPart object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Enum Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEnumValue(EnumValue object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Behavior</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBehavior(Behavior object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>State Machine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseStateMachine(StateMachine object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>New Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>New Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNewExpression(NewExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Value Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Value Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseValueContainer(ValueContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Dialog</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Dialog</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDialog(Dialog object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Sub Dialog State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Sub Dialog State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSubDialogState(SubDialogState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Dialog State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Dialog State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDialogState(DialogState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Dialog Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Dialog Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDialogNode(DialogNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTrigger(Trigger object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Transient Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Transient Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTransientNode(TransientNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Wait State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Wait State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseWaitState(WaitState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Any State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Any State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAnyState(AnyState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>List State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>List State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseListState(ListState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Decision Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Decision Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDecisionNode(DecisionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Initial Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Initial Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseInitialNode(InitialNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Junction Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Junction Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseJunctionNode(JunctionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Diversion Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Diversion Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDiversionNode(DiversionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>History State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>History State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseHistoryState(HistoryState object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Final Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Final Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFinalNode(FinalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Next Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Next Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNextNode(NextNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Stop Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Stop Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseStopNode(StopNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Return Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Return Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseReturnNode(ReturnNode object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAssignment(Assignment object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Play</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Play</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePlay(Play object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Uninterpreted</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Uninterpreted</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseUninterpreted(Uninterpreted object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>If Then Else</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>If Then Else</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIfThenElse(IfThenElse object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>While</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>While</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseWhile(While object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Return Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Return Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseReturnAction(ReturnAction object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Call Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseCallAction(CallAction object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Input Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseInputEvent(InputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>DTMF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>DTMF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDTMF(DTMF object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Reject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Reject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseReject(Reject object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Inactivity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Inactivity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseInactivity(Inactivity object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Concept</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Concept</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConcept(Concept object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseExternalEvent(ExternalEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Any Dtmf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Any Dtmf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAnyDtmf(AnyDtmf object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Any Digit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Any Digit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAnyDigit(AnyDigit object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Recording</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Recording</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRecording(Recording object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Abstract DTMF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Abstract DTMF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAbstractDTMF(AbstractDTMF object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Message End</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Message End</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseMessageEnd(MessageEnd object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Dtmf Input</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Dtmf Input</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDtmfInput(DtmfInput object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>System Input</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>System Input</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSystemInput(SystemInput object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseMessage(Message object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Message Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Message Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseMessagePart(MessagePart object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Fix Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Fix Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFixPart(FixPart object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Variable Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Variable Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseVariablePart(VariablePart object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Conditional Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Conditional Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConditionalElement(ConditionalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Silence Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Silence Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSilencePart(SilencePart object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Message Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Message Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseMessageElement(MessageElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Use Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Use Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseUseElement(UseElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Message Element Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Message Element Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseMessageElementCondition(MessageElementCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>For Each Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>For Each Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseForEachElement(ForEachElement object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Grammar</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Grammar</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseGrammar(Grammar object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseVoiceService(VoiceService object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseService(Service object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Functionality</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Functionality</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFunctionality(Functionality object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseActor(Actor object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>Environment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>Environment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEnvironment(Environment object) {
		return null;
	}

	/**
	 * Returns the result of interpretting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpretting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //VoiceSwitch
