/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceAdapterFactory.java,v 1.1 2008-04-10 15:45:40 vmahe Exp $
 */
package com.orange_ftgroup.voice.util;

import com.orange_ftgroup.voice.AbstractDTMF;
import com.orange_ftgroup.voice.Action;
import com.orange_ftgroup.voice.ActionSequence;
import com.orange_ftgroup.voice.Actor;
import com.orange_ftgroup.voice.AnyDigit;
import com.orange_ftgroup.voice.AnyDtmf;
import com.orange_ftgroup.voice.AnyState;
import com.orange_ftgroup.voice.Assignment;
import com.orange_ftgroup.voice.Behavior;
import com.orange_ftgroup.voice.BinaryExpression;
import com.orange_ftgroup.voice.BooleanValue;
import com.orange_ftgroup.voice.CallAction;
import com.orange_ftgroup.voice.CallExpression;
import com.orange_ftgroup.voice.CharacterValue;
import com.orange_ftgroup.voice.CharstringValue;
import com.orange_ftgroup.voice.ChoiceType;
import com.orange_ftgroup.voice.CollectionItem;
import com.orange_ftgroup.voice.CollectionLiteralExpression;
import com.orange_ftgroup.voice.CollectionLiteralPart;
import com.orange_ftgroup.voice.CollectionRange;
import com.orange_ftgroup.voice.Concept;
import com.orange_ftgroup.voice.ConditionalElement;
import com.orange_ftgroup.voice.ConditionalExpression;
import com.orange_ftgroup.voice.Constraint;
import com.orange_ftgroup.voice.DTMF;
import com.orange_ftgroup.voice.DTMFValue;
import com.orange_ftgroup.voice.DataType;
import com.orange_ftgroup.voice.DecisionNode;
import com.orange_ftgroup.voice.Dialog;
import com.orange_ftgroup.voice.DialogNode;
import com.orange_ftgroup.voice.DialogState;
import com.orange_ftgroup.voice.DigitValue;
import com.orange_ftgroup.voice.DiversionNode;
import com.orange_ftgroup.voice.DtmfInput;
import com.orange_ftgroup.voice.Entity;
import com.orange_ftgroup.voice.EnumValue;
import com.orange_ftgroup.voice.EnumerationItem;
import com.orange_ftgroup.voice.EnumerationType;
import com.orange_ftgroup.voice.Environment;
import com.orange_ftgroup.voice.Expression;
import com.orange_ftgroup.voice.ExternalEvent;
import com.orange_ftgroup.voice.FeatureExpression;
import com.orange_ftgroup.voice.FinalNode;
import com.orange_ftgroup.voice.FixPart;
import com.orange_ftgroup.voice.FloatValue;
import com.orange_ftgroup.voice.ForEachElement;
import com.orange_ftgroup.voice.Functionality;
import com.orange_ftgroup.voice.Grammar;
import com.orange_ftgroup.voice.HistoryState;
import com.orange_ftgroup.voice.Ident;
import com.orange_ftgroup.voice.IfThenElse;
import com.orange_ftgroup.voice.Inactivity;
import com.orange_ftgroup.voice.InformalExpression;
import com.orange_ftgroup.voice.InitialNode;
import com.orange_ftgroup.voice.InputEvent;
import com.orange_ftgroup.voice.IntegerValue;
import com.orange_ftgroup.voice.JunctionNode;
import com.orange_ftgroup.voice.ListExpression;
import com.orange_ftgroup.voice.ListState;
import com.orange_ftgroup.voice.ListType;
import com.orange_ftgroup.voice.Message;
import com.orange_ftgroup.voice.MessageElement;
import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.MessageEnd;
import com.orange_ftgroup.voice.MessagePart;
import com.orange_ftgroup.voice.ModelElement;
import com.orange_ftgroup.voice.NamedElement;
import com.orange_ftgroup.voice.NewExpression;
import com.orange_ftgroup.voice.NextNode;
import com.orange_ftgroup.voice.Operation;
import com.orange_ftgroup.voice.PackageableElement;
import com.orange_ftgroup.voice.Parameter;
import com.orange_ftgroup.voice.ParenthesisExpression;
import com.orange_ftgroup.voice.Play;
import com.orange_ftgroup.voice.Property;
import com.orange_ftgroup.voice.PropertyExpression;
import com.orange_ftgroup.voice.Recording;
import com.orange_ftgroup.voice.Reject;
import com.orange_ftgroup.voice.ReturnAction;
import com.orange_ftgroup.voice.ReturnNode;
import com.orange_ftgroup.voice.Service;
import com.orange_ftgroup.voice.Signature;
import com.orange_ftgroup.voice.SilencePart;
import com.orange_ftgroup.voice.StateMachine;
import com.orange_ftgroup.voice.StopNode;
import com.orange_ftgroup.voice.SubDialogState;
import com.orange_ftgroup.voice.SystemInput;
import com.orange_ftgroup.voice.Tag;
import com.orange_ftgroup.voice.TransientNode;
import com.orange_ftgroup.voice.Transition;
import com.orange_ftgroup.voice.Trigger;
import com.orange_ftgroup.voice.Type;
import com.orange_ftgroup.voice.TypedElement;
import com.orange_ftgroup.voice.UnaryExpression;
import com.orange_ftgroup.voice.Uninterpreted;
import com.orange_ftgroup.voice.UseElement;
import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.ValueExpression;
import com.orange_ftgroup.voice.Variable;
import com.orange_ftgroup.voice.VariablePart;
import com.orange_ftgroup.voice.VoicePackage;
import com.orange_ftgroup.voice.VoiceService;
import com.orange_ftgroup.voice.WaitState;
import com.orange_ftgroup.voice.While;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoicePackage
 * @generated
 */
public class VoiceAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VoicePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoiceAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = VoicePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch the delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VoiceSwitch modelSwitch =
		new VoiceSwitch() {
			public Object caseModelElement(ModelElement object) {
				return createModelElementAdapter();
			}
			public Object caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			public Object caseExpression(Expression object) {
				return createExpressionAdapter();
			}
			public Object caseTypedElement(TypedElement object) {
				return createTypedElementAdapter();
			}
			public Object caseType(Type object) {
				return createTypeAdapter();
			}
			public Object caseParameter(Parameter object) {
				return createParameterAdapter();
			}
			public Object caseOperation(Operation object) {
				return createOperationAdapter();
			}
			public Object caseConstraint(Constraint object) {
				return createConstraintAdapter();
			}
			public Object casePackage(com.orange_ftgroup.voice.Package object) {
				return createPackageAdapter();
			}
			public Object caseDataType(DataType object) {
				return createDataTypeAdapter();
			}
			public Object caseTag(Tag object) {
				return createTagAdapter();
			}
			public Object caseProperty(Property object) {
				return createPropertyAdapter();
			}
			public Object caseClass(com.orange_ftgroup.voice.Class object) {
				return createClassAdapter();
			}
			public Object caseBinaryExpression(BinaryExpression object) {
				return createBinaryExpressionAdapter();
			}
			public Object caseUnaryExpression(UnaryExpression object) {
				return createUnaryExpressionAdapter();
			}
			public Object caseValueExpression(ValueExpression object) {
				return createValueExpressionAdapter();
			}
			public Object caseCharacterValue(CharacterValue object) {
				return createCharacterValueAdapter();
			}
			public Object caseIntegerValue(IntegerValue object) {
				return createIntegerValueAdapter();
			}
			public Object caseFloatValue(FloatValue object) {
				return createFloatValueAdapter();
			}
			public Object caseBooleanValue(BooleanValue object) {
				return createBooleanValueAdapter();
			}
			public Object caseCharstringValue(CharstringValue object) {
				return createCharstringValueAdapter();
			}
			public Object caseIdent(Ident object) {
				return createIdentAdapter();
			}
			public Object caseParenthesisExpression(ParenthesisExpression object) {
				return createParenthesisExpressionAdapter();
			}
			public Object caseConditionalExpression(ConditionalExpression object) {
				return createConditionalExpressionAdapter();
			}
			public Object caseListExpression(ListExpression object) {
				return createListExpressionAdapter();
			}
			public Object caseCallExpression(CallExpression object) {
				return createCallExpressionAdapter();
			}
			public Object caseEnumerationType(EnumerationType object) {
				return createEnumerationTypeAdapter();
			}
			public Object caseEnumerationItem(EnumerationItem object) {
				return createEnumerationItemAdapter();
			}
			public Object caseChoiceType(ChoiceType object) {
				return createChoiceTypeAdapter();
			}
			public Object caseListType(ListType object) {
				return createListTypeAdapter();
			}
			public Object caseVariable(Variable object) {
				return createVariableAdapter();
			}
			public Object caseSignature(Signature object) {
				return createSignatureAdapter();
			}
			public Object caseInformalExpression(InformalExpression object) {
				return createInformalExpressionAdapter();
			}
			public Object casePackageableElement(PackageableElement object) {
				return createPackageableElementAdapter();
			}
			public Object caseActionSequence(ActionSequence object) {
				return createActionSequenceAdapter();
			}
			public Object caseAction(Action object) {
				return createActionAdapter();
			}
			public Object caseDigitValue(DigitValue object) {
				return createDigitValueAdapter();
			}
			public Object caseDTMFValue(DTMFValue object) {
				return createDTMFValueAdapter();
			}
			public Object caseFeatureExpression(FeatureExpression object) {
				return createFeatureExpressionAdapter();
			}
			public Object casePropertyExpression(PropertyExpression object) {
				return createPropertyExpressionAdapter();
			}
			public Object caseCollectionRange(CollectionRange object) {
				return createCollectionRangeAdapter();
			}
			public Object caseCollectionLiteralExpression(CollectionLiteralExpression object) {
				return createCollectionLiteralExpressionAdapter();
			}
			public Object caseCollectionItem(CollectionItem object) {
				return createCollectionItemAdapter();
			}
			public Object caseCollectionLiteralPart(CollectionLiteralPart object) {
				return createCollectionLiteralPartAdapter();
			}
			public Object caseEnumValue(EnumValue object) {
				return createEnumValueAdapter();
			}
			public Object caseBehavior(Behavior object) {
				return createBehaviorAdapter();
			}
			public Object caseStateMachine(StateMachine object) {
				return createStateMachineAdapter();
			}
			public Object caseNewExpression(NewExpression object) {
				return createNewExpressionAdapter();
			}
			public Object caseValueContainer(ValueContainer object) {
				return createValueContainerAdapter();
			}
			public Object caseDialog(Dialog object) {
				return createDialogAdapter();
			}
			public Object caseSubDialogState(SubDialogState object) {
				return createSubDialogStateAdapter();
			}
			public Object caseDialogState(DialogState object) {
				return createDialogStateAdapter();
			}
			public Object caseDialogNode(DialogNode object) {
				return createDialogNodeAdapter();
			}
			public Object caseTransition(Transition object) {
				return createTransitionAdapter();
			}
			public Object caseTrigger(Trigger object) {
				return createTriggerAdapter();
			}
			public Object caseTransientNode(TransientNode object) {
				return createTransientNodeAdapter();
			}
			public Object caseWaitState(WaitState object) {
				return createWaitStateAdapter();
			}
			public Object caseAnyState(AnyState object) {
				return createAnyStateAdapter();
			}
			public Object caseListState(ListState object) {
				return createListStateAdapter();
			}
			public Object caseDecisionNode(DecisionNode object) {
				return createDecisionNodeAdapter();
			}
			public Object caseInitialNode(InitialNode object) {
				return createInitialNodeAdapter();
			}
			public Object caseJunctionNode(JunctionNode object) {
				return createJunctionNodeAdapter();
			}
			public Object caseDiversionNode(DiversionNode object) {
				return createDiversionNodeAdapter();
			}
			public Object caseHistoryState(HistoryState object) {
				return createHistoryStateAdapter();
			}
			public Object caseFinalNode(FinalNode object) {
				return createFinalNodeAdapter();
			}
			public Object caseNextNode(NextNode object) {
				return createNextNodeAdapter();
			}
			public Object caseStopNode(StopNode object) {
				return createStopNodeAdapter();
			}
			public Object caseReturnNode(ReturnNode object) {
				return createReturnNodeAdapter();
			}
			public Object caseAssignment(Assignment object) {
				return createAssignmentAdapter();
			}
			public Object casePlay(Play object) {
				return createPlayAdapter();
			}
			public Object caseUninterpreted(Uninterpreted object) {
				return createUninterpretedAdapter();
			}
			public Object caseIfThenElse(IfThenElse object) {
				return createIfThenElseAdapter();
			}
			public Object caseWhile(While object) {
				return createWhileAdapter();
			}
			public Object caseReturnAction(ReturnAction object) {
				return createReturnActionAdapter();
			}
			public Object caseCallAction(CallAction object) {
				return createCallActionAdapter();
			}
			public Object caseInputEvent(InputEvent object) {
				return createInputEventAdapter();
			}
			public Object caseDTMF(DTMF object) {
				return createDTMFAdapter();
			}
			public Object caseReject(Reject object) {
				return createRejectAdapter();
			}
			public Object caseInactivity(Inactivity object) {
				return createInactivityAdapter();
			}
			public Object caseConcept(Concept object) {
				return createConceptAdapter();
			}
			public Object caseExternalEvent(ExternalEvent object) {
				return createExternalEventAdapter();
			}
			public Object caseAnyDtmf(AnyDtmf object) {
				return createAnyDtmfAdapter();
			}
			public Object caseAnyDigit(AnyDigit object) {
				return createAnyDigitAdapter();
			}
			public Object caseRecording(Recording object) {
				return createRecordingAdapter();
			}
			public Object caseAbstractDTMF(AbstractDTMF object) {
				return createAbstractDTMFAdapter();
			}
			public Object caseMessageEnd(MessageEnd object) {
				return createMessageEndAdapter();
			}
			public Object caseDtmfInput(DtmfInput object) {
				return createDtmfInputAdapter();
			}
			public Object caseSystemInput(SystemInput object) {
				return createSystemInputAdapter();
			}
			public Object caseMessage(Message object) {
				return createMessageAdapter();
			}
			public Object caseMessagePart(MessagePart object) {
				return createMessagePartAdapter();
			}
			public Object caseFixPart(FixPart object) {
				return createFixPartAdapter();
			}
			public Object caseVariablePart(VariablePart object) {
				return createVariablePartAdapter();
			}
			public Object caseConditionalElement(ConditionalElement object) {
				return createConditionalElementAdapter();
			}
			public Object caseSilencePart(SilencePart object) {
				return createSilencePartAdapter();
			}
			public Object caseMessageElement(MessageElement object) {
				return createMessageElementAdapter();
			}
			public Object caseUseElement(UseElement object) {
				return createUseElementAdapter();
			}
			public Object caseMessageElementCondition(MessageElementCondition object) {
				return createMessageElementConditionAdapter();
			}
			public Object caseForEachElement(ForEachElement object) {
				return createForEachElementAdapter();
			}
			public Object caseGrammar(Grammar object) {
				return createGrammarAdapter();
			}
			public Object caseVoiceService(VoiceService object) {
				return createVoiceServiceAdapter();
			}
			public Object caseService(Service object) {
				return createServiceAdapter();
			}
			public Object caseFunctionality(Functionality object) {
				return createFunctionalityAdapter();
			}
			public Object caseActor(Actor object) {
				return createActorAdapter();
			}
			public Object caseEntity(Entity object) {
				return createEntityAdapter();
			}
			public Object caseEnvironment(Environment object) {
				return createEnvironmentAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ModelElement
	 * @generated
	 */
	public Adapter createModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.TypedElement
	 * @generated
	 */
	public Adapter createTypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Type
	 * @generated
	 */
	public Adapter createTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Constraint
	 * @generated
	 */
	public Adapter createConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Package <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Package
	 * @generated
	 */
	public Adapter createPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DataType
	 * @generated
	 */
	public Adapter createDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Tag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Tag
	 * @generated
	 */
	public Adapter createTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Property
	 * @generated
	 */
	public Adapter createPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Class
	 * @generated
	 */
	public Adapter createClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.BinaryExpression
	 * @generated
	 */
	public Adapter createBinaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.UnaryExpression
	 * @generated
	 */
	public Adapter createUnaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ValueExpression <em>Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ValueExpression
	 * @generated
	 */
	public Adapter createValueExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CharacterValue <em>Character Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CharacterValue
	 * @generated
	 */
	public Adapter createCharacterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.IntegerValue
	 * @generated
	 */
	public Adapter createIntegerValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.FloatValue
	 * @generated
	 */
	public Adapter createFloatValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.BooleanValue
	 * @generated
	 */
	public Adapter createBooleanValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CharstringValue <em>Charstring Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CharstringValue
	 * @generated
	 */
	public Adapter createCharstringValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Ident <em>Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Ident
	 * @generated
	 */
	public Adapter createIdentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ParenthesisExpression <em>Parenthesis Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ParenthesisExpression
	 * @generated
	 */
	public Adapter createParenthesisExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ConditionalExpression <em>Conditional Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ConditionalExpression
	 * @generated
	 */
	public Adapter createConditionalExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ListExpression <em>List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ListExpression
	 * @generated
	 */
	public Adapter createListExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CallExpression <em>Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CallExpression
	 * @generated
	 */
	public Adapter createCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.EnumerationType <em>Enumeration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.EnumerationType
	 * @generated
	 */
	public Adapter createEnumerationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.EnumerationItem <em>Enumeration Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.EnumerationItem
	 * @generated
	 */
	public Adapter createEnumerationItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ChoiceType <em>Choice Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ChoiceType
	 * @generated
	 */
	public Adapter createChoiceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ListType <em>List Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ListType
	 * @generated
	 */
	public Adapter createListTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Signature
	 * @generated
	 */
	public Adapter createSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.InformalExpression <em>Informal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.InformalExpression
	 * @generated
	 */
	public Adapter createInformalExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.PackageableElement <em>Packageable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.PackageableElement
	 * @generated
	 */
	public Adapter createPackageableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ActionSequence <em>Action Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ActionSequence
	 * @generated
	 */
	public Adapter createActionSequenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DigitValue <em>Digit Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DigitValue
	 * @generated
	 */
	public Adapter createDigitValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DTMFValue <em>DTMF Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DTMFValue
	 * @generated
	 */
	public Adapter createDTMFValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.FeatureExpression <em>Feature Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.FeatureExpression
	 * @generated
	 */
	public Adapter createFeatureExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.PropertyExpression <em>Property Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.PropertyExpression
	 * @generated
	 */
	public Adapter createPropertyExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CollectionRange <em>Collection Range</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CollectionRange
	 * @generated
	 */
	public Adapter createCollectionRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CollectionLiteralExpression <em>Collection Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CollectionLiteralExpression
	 * @generated
	 */
	public Adapter createCollectionLiteralExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CollectionItem <em>Collection Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CollectionItem
	 * @generated
	 */
	public Adapter createCollectionItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CollectionLiteralPart <em>Collection Literal Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CollectionLiteralPart
	 * @generated
	 */
	public Adapter createCollectionLiteralPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.EnumValue
	 * @generated
	 */
	public Adapter createEnumValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Behavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Behavior
	 * @generated
	 */
	public Adapter createBehaviorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.StateMachine
	 * @generated
	 */
	public Adapter createStateMachineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.NewExpression <em>New Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.NewExpression
	 * @generated
	 */
	public Adapter createNewExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ValueContainer <em>Value Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ValueContainer
	 * @generated
	 */
	public Adapter createValueContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Dialog <em>Dialog</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Dialog
	 * @generated
	 */
	public Adapter createDialogAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.SubDialogState <em>Sub Dialog State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.SubDialogState
	 * @generated
	 */
	public Adapter createSubDialogStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DialogState <em>Dialog State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DialogState
	 * @generated
	 */
	public Adapter createDialogStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DialogNode <em>Dialog Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DialogNode
	 * @generated
	 */
	public Adapter createDialogNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Transition
	 * @generated
	 */
	public Adapter createTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Trigger
	 * @generated
	 */
	public Adapter createTriggerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.TransientNode <em>Transient Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.TransientNode
	 * @generated
	 */
	public Adapter createTransientNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.WaitState <em>Wait State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.WaitState
	 * @generated
	 */
	public Adapter createWaitStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.AnyState <em>Any State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.AnyState
	 * @generated
	 */
	public Adapter createAnyStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ListState <em>List State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ListState
	 * @generated
	 */
	public Adapter createListStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DecisionNode <em>Decision Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DecisionNode
	 * @generated
	 */
	public Adapter createDecisionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.InitialNode <em>Initial Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.InitialNode
	 * @generated
	 */
	public Adapter createInitialNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.JunctionNode <em>Junction Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.JunctionNode
	 * @generated
	 */
	public Adapter createJunctionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DiversionNode <em>Diversion Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DiversionNode
	 * @generated
	 */
	public Adapter createDiversionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.HistoryState <em>History State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.HistoryState
	 * @generated
	 */
	public Adapter createHistoryStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.FinalNode <em>Final Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.FinalNode
	 * @generated
	 */
	public Adapter createFinalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.NextNode <em>Next Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.NextNode
	 * @generated
	 */
	public Adapter createNextNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.StopNode <em>Stop Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.StopNode
	 * @generated
	 */
	public Adapter createStopNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ReturnNode <em>Return Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ReturnNode
	 * @generated
	 */
	public Adapter createReturnNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Assignment
	 * @generated
	 */
	public Adapter createAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Play <em>Play</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Play
	 * @generated
	 */
	public Adapter createPlayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Uninterpreted <em>Uninterpreted</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Uninterpreted
	 * @generated
	 */
	public Adapter createUninterpretedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.IfThenElse <em>If Then Else</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.IfThenElse
	 * @generated
	 */
	public Adapter createIfThenElseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.While <em>While</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.While
	 * @generated
	 */
	public Adapter createWhileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ReturnAction <em>Return Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ReturnAction
	 * @generated
	 */
	public Adapter createReturnActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.CallAction
	 * @generated
	 */
	public Adapter createCallActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.InputEvent <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.InputEvent
	 * @generated
	 */
	public Adapter createInputEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DTMF <em>DTMF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DTMF
	 * @generated
	 */
	public Adapter createDTMFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Reject <em>Reject</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Reject
	 * @generated
	 */
	public Adapter createRejectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Inactivity <em>Inactivity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Inactivity
	 * @generated
	 */
	public Adapter createInactivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Concept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Concept
	 * @generated
	 */
	public Adapter createConceptAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ExternalEvent <em>External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ExternalEvent
	 * @generated
	 */
	public Adapter createExternalEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.AnyDtmf <em>Any Dtmf</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.AnyDtmf
	 * @generated
	 */
	public Adapter createAnyDtmfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.AnyDigit <em>Any Digit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.AnyDigit
	 * @generated
	 */
	public Adapter createAnyDigitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Recording <em>Recording</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Recording
	 * @generated
	 */
	public Adapter createRecordingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.AbstractDTMF <em>Abstract DTMF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.AbstractDTMF
	 * @generated
	 */
	public Adapter createAbstractDTMFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.MessageEnd <em>Message End</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.MessageEnd
	 * @generated
	 */
	public Adapter createMessageEndAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.DtmfInput <em>Dtmf Input</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.DtmfInput
	 * @generated
	 */
	public Adapter createDtmfInputAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.SystemInput <em>System Input</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.SystemInput
	 * @generated
	 */
	public Adapter createSystemInputAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Message
	 * @generated
	 */
	public Adapter createMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.MessagePart <em>Message Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.MessagePart
	 * @generated
	 */
	public Adapter createMessagePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.FixPart <em>Fix Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.FixPart
	 * @generated
	 */
	public Adapter createFixPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.VariablePart <em>Variable Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.VariablePart
	 * @generated
	 */
	public Adapter createVariablePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ConditionalElement <em>Conditional Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ConditionalElement
	 * @generated
	 */
	public Adapter createConditionalElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.SilencePart <em>Silence Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.SilencePart
	 * @generated
	 */
	public Adapter createSilencePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.MessageElement <em>Message Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.MessageElement
	 * @generated
	 */
	public Adapter createMessageElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.UseElement <em>Use Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.UseElement
	 * @generated
	 */
	public Adapter createUseElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.MessageElementCondition <em>Message Element Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.MessageElementCondition
	 * @generated
	 */
	public Adapter createMessageElementConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.ForEachElement <em>For Each Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.ForEachElement
	 * @generated
	 */
	public Adapter createForEachElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Grammar <em>Grammar</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Grammar
	 * @generated
	 */
	public Adapter createGrammarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.VoiceService <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.VoiceService
	 * @generated
	 */
	public Adapter createVoiceServiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Service <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Service
	 * @generated
	 */
	public Adapter createServiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Functionality <em>Functionality</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Functionality
	 * @generated
	 */
	public Adapter createFunctionalityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Actor
	 * @generated
	 */
	public Adapter createActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Entity
	 * @generated
	 */
	public Adapter createEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.orange_ftgroup.voice.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.orange_ftgroup.voice.Environment
	 * @generated
	 */
	public Adapter createEnvironmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //VoiceAdapterFactory
