/**
 * <copyright>
 * </copyright>
 *
 * $Id: TransientNode.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transient Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getTransientNode()
 * @model abstract="true"
 * @generated
 */
public interface TransientNode extends DialogNode {
} // TransientNode