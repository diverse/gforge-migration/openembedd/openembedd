/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionSequence.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.ActionSequence#getAction <em>Action</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.ActionSequence#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getActionSequence()
 * @model
 * @generated
 */
public interface ActionSequence extends ModelElement, Behavior {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Action}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Action#getActionSequence <em>Action Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getActionSequence_Action()
	 * @see com.orange_ftgroup.voice.Action#getActionSequence
	 * @model type="com.orange_ftgroup.voice.Action" opposite="ActionSequence" containment="true"
	 * @generated
	 */
	EList getAction();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Message#getActionSpecification <em>Action Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' container reference.
	 * @see #setMessage(Message)
	 * @see com.orange_ftgroup.voice.VoicePackage#getActionSequence_Message()
	 * @see com.orange_ftgroup.voice.Message#getActionSpecification
	 * @model opposite="actionSpecification"
	 * @generated
	 */
	Message getMessage();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.ActionSequence#getMessage <em>Message</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' container reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(Message value);

} // ActionSequence