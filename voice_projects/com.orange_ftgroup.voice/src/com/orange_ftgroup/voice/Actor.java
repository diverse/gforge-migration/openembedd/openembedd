/**
 * <copyright>
 * </copyright>
 *
 * $Id: Actor.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.Actor#getFunctionality <em>Functionality</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends PackageableElement {
	/**
	 * Returns the value of the '<em><b>Functionality</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Functionality}.
	 * It is bidirectional and its opposite is '{@link com.orange_ftgroup.voice.Functionality#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functionality</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functionality</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getActor_Functionality()
	 * @see com.orange_ftgroup.voice.Functionality#getParticipant
	 * @model type="com.orange_ftgroup.voice.Functionality" opposite="participant"
	 * @generated
	 */
	EList getFunctionality();

} // Actor