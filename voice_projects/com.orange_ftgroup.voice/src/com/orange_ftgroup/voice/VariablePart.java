/**
 * <copyright>
 * </copyright>
 *
 * $Id: VariablePart.java,v 1.1 2008-04-10 15:45:38 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.VariablePart#getFormat <em>Format</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.VariablePart#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.VariablePart#getValue <em>Value</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.VariablePart#getActionSpecification <em>Action Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getVariablePart()
 * @model
 * @generated
 */
public interface VariablePart extends MessagePart {
	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.DiffusionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @see #setFormat(DiffusionType)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVariablePart_Format()
	 * @model
	 * @generated
	 */
	DiffusionType getFormat();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.VariablePart#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see com.orange_ftgroup.voice.DiffusionType
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(DiffusionType value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link com.orange_ftgroup.voice.VisibilityKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @see #setVisibility(VisibilityKind)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVariablePart_Visibility()
	 * @model
	 * @generated
	 */
	VisibilityKind getVisibility();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.VariablePart#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see com.orange_ftgroup.voice.VisibilityKind
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(VisibilityKind value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Expression)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVariablePart_Value()
	 * @model containment="true"
	 * @generated
	 */
	Expression getValue();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.VariablePart#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Action Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Specification</em>' containment reference.
	 * @see #setActionSpecification(ActionSequence)
	 * @see com.orange_ftgroup.voice.VoicePackage#getVariablePart_ActionSpecification()
	 * @model containment="true"
	 * @generated
	 */
	ActionSequence getActionSpecification();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.VariablePart#getActionSpecification <em>Action Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Specification</em>' containment reference.
	 * @see #getActionSpecification()
	 * @generated
	 */
	void setActionSpecification(ActionSequence value);

} // VariablePart