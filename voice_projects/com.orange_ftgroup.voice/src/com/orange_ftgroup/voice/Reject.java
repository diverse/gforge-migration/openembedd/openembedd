/**
 * <copyright>
 * </copyright>
 *
 * $Id: Reject.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reject</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getReject()
 * @model
 * @generated
 */
public interface Reject extends SystemInput {
} // Reject