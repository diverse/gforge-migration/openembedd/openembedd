/**
 * <copyright>
 * </copyright>
 *
 * $Id: DiffusionType.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Diffusion Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.orange_ftgroup.voice.VoicePackage#getDiffusionType()
 * @model
 * @generated
 */
public final class DiffusionType extends AbstractEnumerator {
	/**
	 * The '<em><b>Digits</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Digits</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIGITS_LITERAL
	 * @model name="digits"
	 * @generated
	 * @ordered
	 */
	public static final int DIGITS = 0;

	/**
	 * The '<em><b>Number</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Number</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NUMBER_LITERAL
	 * @model name="number"
	 * @generated
	 * @ordered
	 */
	public static final int NUMBER = 1;

	/**
	 * The '<em><b>Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COUNT_LITERAL
	 * @model name="count"
	 * @generated
	 * @ordered
	 */
	public static final int COUNT = 2;

	/**
	 * The '<em><b>Phone</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Phone</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHONE_LITERAL
	 * @model name="phone"
	 * @generated
	 * @ordered
	 */
	public static final int PHONE = 3;

	/**
	 * The '<em><b>Spelling</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Spelling</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPELLING_LITERAL
	 * @model name="spelling"
	 * @generated
	 * @ordered
	 */
	public static final int SPELLING = 4;

	/**
	 * The '<em><b>Time</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIME_LITERAL
	 * @model name="time"
	 * @generated
	 * @ordered
	 */
	public static final int TIME = 5;

	/**
	 * The '<em><b>Date</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_LITERAL
	 * @model name="date"
	 * @generated
	 * @ordered
	 */
	public static final int DATE = 6;

	/**
	 * The '<em><b>Currency</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Currency</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CURRENCY_LITERAL
	 * @model name="currency"
	 * @generated
	 * @ordered
	 */
	public static final int CURRENCY = 7;

	/**
	 * The '<em><b>Digits</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIGITS
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType DIGITS_LITERAL = new DiffusionType(DIGITS, "digits", "digits");

	/**
	 * The '<em><b>Number</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NUMBER
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType NUMBER_LITERAL = new DiffusionType(NUMBER, "number", "number");

	/**
	 * The '<em><b>Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COUNT
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType COUNT_LITERAL = new DiffusionType(COUNT, "count", "count");

	/**
	 * The '<em><b>Phone</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PHONE
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType PHONE_LITERAL = new DiffusionType(PHONE, "phone", "phone");

	/**
	 * The '<em><b>Spelling</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPELLING
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType SPELLING_LITERAL = new DiffusionType(SPELLING, "spelling", "spelling");

	/**
	 * The '<em><b>Time</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIME
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType TIME_LITERAL = new DiffusionType(TIME, "time", "time");

	/**
	 * The '<em><b>Date</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType DATE_LITERAL = new DiffusionType(DATE, "date", "date");

	/**
	 * The '<em><b>Currency</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CURRENCY
	 * @generated
	 * @ordered
	 */
	public static final DiffusionType CURRENCY_LITERAL = new DiffusionType(CURRENCY, "currency", "currency");

	/**
	 * An array of all the '<em><b>Diffusion Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DiffusionType[] VALUES_ARRAY =
		new DiffusionType[] {
			DIGITS_LITERAL,
			NUMBER_LITERAL,
			COUNT_LITERAL,
			PHONE_LITERAL,
			SPELLING_LITERAL,
			TIME_LITERAL,
			DATE_LITERAL,
			CURRENCY_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Diffusion Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Diffusion Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DiffusionType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DiffusionType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Diffusion Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DiffusionType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DiffusionType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Diffusion Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DiffusionType get(int value) {
		switch (value) {
			case DIGITS: return DIGITS_LITERAL;
			case NUMBER: return NUMBER_LITERAL;
			case COUNT: return COUNT_LITERAL;
			case PHONE: return PHONE_LITERAL;
			case SPELLING: return SPELLING_LITERAL;
			case TIME: return TIME_LITERAL;
			case DATE: return DATE_LITERAL;
			case CURRENCY: return CURRENCY_LITERAL;
		}
		return null;	
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DiffusionType(int value, String name, String literal) {
		super(value, name, literal);
	}

} //DiffusionType
