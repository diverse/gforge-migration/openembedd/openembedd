/**
 * <copyright>
 * </copyright>
 *
 * $Id: WaitState.java,v 1.1 2008-04-10 15:45:39 vmahe Exp $
 */
package com.orange_ftgroup.voice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wait State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.orange_ftgroup.voice.WaitState#getDelay <em>Delay</em>}</li>
 *   <li>{@link com.orange_ftgroup.voice.WaitState#getGrammar <em>Grammar</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.orange_ftgroup.voice.VoicePackage#getWaitState()
 * @model
 * @generated
 */
public interface WaitState extends DialogState, NamedElement {
	/**
	 * Returns the value of the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' attribute.
	 * @see #setDelay(String)
	 * @see com.orange_ftgroup.voice.VoicePackage#getWaitState_Delay()
	 * @model
	 * @generated
	 */
	String getDelay();

	/**
	 * Sets the value of the '{@link com.orange_ftgroup.voice.WaitState#getDelay <em>Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' attribute.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(String value);

	/**
	 * Returns the value of the '<em><b>Grammar</b></em>' reference list.
	 * The list contents are of type {@link com.orange_ftgroup.voice.Grammar}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grammar</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grammar</em>' reference list.
	 * @see com.orange_ftgroup.voice.VoicePackage#getWaitState_Grammar()
	 * @model type="com.orange_ftgroup.voice.Grammar"
	 * @generated
	 */
	EList getGrammar();

} // WaitState