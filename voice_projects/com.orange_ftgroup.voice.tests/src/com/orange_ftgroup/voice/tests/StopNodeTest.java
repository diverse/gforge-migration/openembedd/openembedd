/**
 * <copyright>
 * </copyright>
 *
 * $Id: StopNodeTest.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.StopNode;
import com.orange_ftgroup.voice.VoiceFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Stop Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StopNodeTest extends FinalNodeTest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StopNodeTest.class);
	}

	/**
	 * Constructs a new Stop Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StopNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Stop Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private StopNode getFixture() {
		return (StopNode)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(VoiceFactory.eINSTANCE.createStopNode());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StopNodeTest
