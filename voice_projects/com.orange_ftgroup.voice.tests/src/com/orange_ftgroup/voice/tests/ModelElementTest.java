/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElementTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.ModelElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ModelElementTest extends TestCase {
	/**
	 * The fixture for this Model Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelElement fixture = null;

	/**
	 * Constructs a new Model Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Model Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ModelElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Model Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ModelElement getFixture() {
		return fixture;
	}

} //ModelElementTest
