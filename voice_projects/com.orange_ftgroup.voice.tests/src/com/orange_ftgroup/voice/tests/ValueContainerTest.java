/**
 * <copyright>
 * </copyright>
 *
 * $Id: ValueContainerTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.ValueContainer;
import com.orange_ftgroup.voice.VoiceFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Value Container</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ValueContainerTest extends TypedElementTest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ValueContainerTest.class);
	}

	/**
	 * Constructs a new Value Container test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueContainerTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Value Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ValueContainer getFixture() {
		return (ValueContainer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(VoiceFactory.eINSTANCE.createValueContainer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ValueContainerTest
