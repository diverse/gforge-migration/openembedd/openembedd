/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessagePartTest.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.MessagePart;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Message Part</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MessagePartTest extends SignatureTest {
	/**
	 * Constructs a new Message Part test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessagePartTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Message Part test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MessagePart getFixture() {
		return (MessagePart)fixture;
	}

} //MessagePartTest
