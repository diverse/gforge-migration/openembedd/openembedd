/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessageEndTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.MessageEnd;
import com.orange_ftgroup.voice.VoiceFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Message End</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MessageEndTest extends SystemInputTest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MessageEndTest.class);
	}

	/**
	 * Constructs a new Message End test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEndTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Message End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MessageEnd getFixture() {
		return (MessageEnd)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(VoiceFactory.eINSTANCE.createMessageEnd());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MessageEndTest
