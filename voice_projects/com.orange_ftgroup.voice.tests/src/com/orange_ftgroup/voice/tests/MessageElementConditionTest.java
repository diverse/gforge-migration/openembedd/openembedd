/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessageElementConditionTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.MessageElementCondition;
import com.orange_ftgroup.voice.VoiceFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Message Element Condition</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MessageElementConditionTest extends ModelElementTest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MessageElementConditionTest.class);
	}

	/**
	 * Constructs a new Message Element Condition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageElementConditionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Message Element Condition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MessageElementCondition getFixture() {
		return (MessageElementCondition)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(VoiceFactory.eINSTANCE.createMessageElementCondition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MessageElementConditionTest
