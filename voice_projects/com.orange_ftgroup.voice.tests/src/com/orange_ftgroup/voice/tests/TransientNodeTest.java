/**
 * <copyright>
 * </copyright>
 *
 * $Id: TransientNodeTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.TransientNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Transient Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TransientNodeTest extends DialogNodeTest {
	/**
	 * Constructs a new Transient Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransientNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Transient Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TransientNode getFixture() {
		return (TransientNode)fixture;
	}

} //TransientNodeTest
