/**
 * <copyright>
 * </copyright>
 *
 * $Id: WaitStateTest.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.VoiceFactory;
import com.orange_ftgroup.voice.WaitState;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Wait State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class WaitStateTest extends DialogStateTest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(WaitStateTest.class);
	}

	/**
	 * Constructs a new Wait State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Wait State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private WaitState getFixture() {
		return (WaitState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	protected void setUp() throws Exception {
		setFixture(VoiceFactory.eINSTANCE.createWaitState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //WaitStateTest
