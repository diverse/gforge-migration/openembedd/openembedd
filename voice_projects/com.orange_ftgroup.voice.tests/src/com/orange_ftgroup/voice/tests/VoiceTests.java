/**
 * <copyright>
 * </copyright>
 *
 * $Id: VoiceTests.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>voice</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class VoiceTests extends TestSuite {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new VoiceTests("voice Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoiceTests(String name) {
		super(name);
	}

} //VoiceTests
