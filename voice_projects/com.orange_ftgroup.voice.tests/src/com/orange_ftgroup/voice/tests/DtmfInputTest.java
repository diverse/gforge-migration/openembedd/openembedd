/**
 * <copyright>
 * </copyright>
 *
 * $Id: DtmfInputTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.DtmfInput;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dtmf Input</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DtmfInputTest extends InputEventTest {
	/**
	 * Constructs a new Dtmf Input test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DtmfInputTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Dtmf Input test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DtmfInput getFixture() {
		return (DtmfInput)fixture;
	}

} //DtmfInputTest
