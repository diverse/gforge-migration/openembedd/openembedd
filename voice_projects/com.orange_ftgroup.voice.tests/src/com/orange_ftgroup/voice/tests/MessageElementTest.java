/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessageElementTest.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.MessageElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Message Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MessageElementTest extends ModelElementTest {
	/**
	 * Constructs a new Message Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Message Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MessageElement getFixture() {
		return (MessageElement)fixture;
	}

} //MessageElementTest
