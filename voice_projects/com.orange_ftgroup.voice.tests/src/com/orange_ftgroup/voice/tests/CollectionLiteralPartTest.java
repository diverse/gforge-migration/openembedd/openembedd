/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollectionLiteralPartTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.CollectionLiteralPart;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Collection Literal Part</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CollectionLiteralPartTest extends TypedElementTest {
	/**
	 * Constructs a new Collection Literal Part test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionLiteralPartTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Collection Literal Part test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CollectionLiteralPart getFixture() {
		return (CollectionLiteralPart)fixture;
	}

} //CollectionLiteralPartTest
