/**
 * <copyright>
 * </copyright>
 *
 * $Id: FinalNodeTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.FinalNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Final Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FinalNodeTest extends TransientNodeTest {
	/**
	 * Constructs a new Final Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FinalNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Final Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FinalNode getFixture() {
		return (FinalNode)fixture;
	}

} //FinalNodeTest
