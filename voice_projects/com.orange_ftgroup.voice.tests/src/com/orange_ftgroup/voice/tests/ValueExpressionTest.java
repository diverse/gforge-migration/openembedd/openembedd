/**
 * <copyright>
 * </copyright>
 *
 * $Id: ValueExpressionTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.ValueExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Value Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ValueExpressionTest extends ExpressionTest {
	/**
	 * Constructs a new Value Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Value Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ValueExpression getFixture() {
		return (ValueExpression)fixture;
	}

} //ValueExpressionTest
