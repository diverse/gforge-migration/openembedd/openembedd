/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogStateTest.java,v 1.1 2008-04-10 15:50:41 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.DialogState;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dialog State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DialogStateTest extends DialogNodeTest {
	/**
	 * Constructs a new Dialog State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DialogStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Dialog State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DialogState getFixture() {
		return (DialogState)fixture;
	}

} //DialogStateTest
