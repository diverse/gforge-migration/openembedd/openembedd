/**
 * <copyright>
 * </copyright>
 *
 * $Id: DialogNodeTest.java,v 1.1 2008-04-10 15:50:42 vmahe Exp $
 */
package com.orange_ftgroup.voice.tests;

import com.orange_ftgroup.voice.DialogNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dialog Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DialogNodeTest extends NamedElementTest {
	/**
	 * Constructs a new Dialog Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DialogNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Dialog Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DialogNode getFixture() {
		return (DialogNode)fixture;
	}

} //DialogNodeTest
