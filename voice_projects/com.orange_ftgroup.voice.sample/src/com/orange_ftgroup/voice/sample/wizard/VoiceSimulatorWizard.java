package com.orange_ftgroup.voice.sample.wizard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openembedd.wizards.AbstractNewExampleWizard;

public class VoiceSimulatorWizard  extends AbstractNewExampleWizard
{

	protected Collection<ProjectDescriptor> getProjectDescriptors()
	{
		// We need the statements example to be unzipped
		// REM: EMF library example model, edit and editor projects have been installed in Eclipse
		List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>(1);
		projects.add(new ProjectDescriptor("com.orange_ftgroup.voice.sample", "zip/com.orange_ftgroup.voice.simulator.zip",
			"com.orange_ftgroup.voice.simulator"));
		return projects;
	}
}
