package com.orange_ftgroup.voice.sample;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openembedd.wizards.AbstractDemoPlugin;

public class VoiceSamplePlugin extends AbstractDemoPlugin {
	public ImageDescriptor getImageDescriptor(String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin("com.orange_ftgroup.voice.sample", path);
	}
}
