PROCESS

This text describes the steps of the process
as it has been designed by Verimag (we hope).

 1 - WB2Dot
 	Generate multiple .dot files from a WB model.
 	
 	ATL transformation:
 		WB2Dot_Query.asm
 		in :	model "IN" : *.wbmm
 				metamodel "WB" : WBMM.ecore 
 		out :	????????
 			need ATL source code to see how to integrate it
 	
 	ATL lib:
 		WB2Dot.asm
 		in :	????????
 			need ATL source code to see how to integrate it
 		out :	????????
 			need ATL source code to see how to integrate it
 
 2 - DotAll
 	Add a layout to each .dot file.
 	
 	GraphViz dot tool:
 		for each *.dot { j |
 			dot -Tdot -Grankdir=LR -Granksep="0.4" -Gnodesep="1.5" %%j -o tmp/dotout/%%~nxj
 		}
 
 3 - WBlayoutUpdater
 	Integrate layout data into the origin WB model.
 	
 	Verimag layout tool:
 		org.ujf.verimag.ma2wb.transformations.layout.WBLayoutUpdater tmp/dotout %WBMODEL%
 
 4 - WB2XML
 	Transform the enriched WB model into an XML file.
 	It's an intermediate step before Workbench file.
 	
 	ATL transformation:
 		WB2XML.asm
 		in :	model "IN" : *.wbmm 
 				metamodel "WB" : WBMM.ecore 
 		out :	model "OUT" : *.xml
 				metamodel "XMLMM" : XMLMM.ecore
 
 5 - XML2Text
 	Generate a Workbench syntax file from the XML file.
 	
 	ATL transformation:
 		XML2Text_Query.asm
 		in :	model "IN" : *.xml 
 				metamodel "XMLMM" : XMLMM.ecore 
 		out :	????????
 			need ATL source code to see how to integrate it
 	
 	ATL lib:
 		XMLMM2Text.asm
 		in :	????????
 			need ATL source code to see how to integrate it
 		out :	????????
 			need ATL source code to see how to integrate it
 	