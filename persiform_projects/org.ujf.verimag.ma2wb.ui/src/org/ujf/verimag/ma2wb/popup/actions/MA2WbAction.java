package org.ujf.verimag.ma2wb.popup.actions;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class MA2WbAction implements IObjectActionDelegate {

	private StructuredSelection	selection;
	
	/**
	 * Constructor for Action1.
	 */
	public MA2WbAction() {
		super();
		this.selection = null;
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		for (Iterator<?> it = selection.iterator(); it.hasNext();)
		{
			Object obj = (Object) it.next();
			if (obj instanceof IFile)
			{
				IFile file = (IFile) obj;

				
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection)
		{
			this.selection = (StructuredSelection) selection;
		}
	}

}
