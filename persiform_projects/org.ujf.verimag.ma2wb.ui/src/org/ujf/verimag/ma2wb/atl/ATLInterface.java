package org.ujf.verimag.ma2wb.atl;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.ujf.verimag.ma2wb.Constants;

/**
 * Class that calls the transformation onto the ATL Interface on the selected file
 * 
 * You must call first initTransfo(),
 *  second initMetamodels()
 *  and then transform()
 * in order to complete the execution of your ATL transformation
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 *         Vincent Mahe - OpenEmbeDD integration team
 */
public class ATLInterface implements Constants
{
	/**
	 * Instance of the current class
	 */
	private static ATLInterface	instance	= null;
	/**
	 * Name of the ATL compiled transformation
	 */
	private String				atlTransfo;
	/**
	 * The model handler used for the transformation
	 */
	private AtlEMFModelHandler	modelHandler;
	/**
	 * The URL to the transformation resource file
	 */
	private URL					transfoResource;
	/**
	 * The map of input/output models/metamodels
	 */
	private Map<String, Object> models;
	/**
	 * The metamodel of the input model
	 */
	private ASMEMFModel 		inMM;
	/**
	 * The metamodel of the output model
	 */
	private ASMEMFModel 		outMM;

	/**
	 * This method returns the instance of the ATL interface (there is only one instance of this object)
	 * 
	 * @return the instance of the ATLInterface
	 */
	public static ATLInterface getInstance()
	{
		if (instance == null)
			instance = new ATLInterface();
		return instance;
	}

	/**
	 * Initialize the transformation resources
	 * 
	 * @param transfo
	 *        the path to the ATL transformation file
	 */
	protected void initTransfo(String transfo)
	{
		modelHandler = (AtlEMFModelHandler) AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);
		transfoResource = ATLInterface.class.getResource(atlTransfo);
	}

	/**
	 * Initialize the metamodels of the transformation
	 * 
	 * @param models
	 *        the map that will be updated with resource elements
	 */
	protected void initMetamodels(String inMMname, String inMMUri, String outMMname, String outMMUri)
	{
		models = new HashMap<String, Object>();

		inMM = (ASMEMFModel) modelHandler
				.loadModel(inMMname, modelHandler.getMof(), inMMUri);
		outMM = (ASMEMFModel) modelHandler
				.loadModel(outMMname, modelHandler.getMof(), outMMUri);

		models.put(inMMname, inMM);
		models.put(outMMname, outMM);
	}

	/**
	 * Call the transformation onto the <code>inFilePath</code> and store the result in the
	 * <code>outFilePath</code> file.
	 * 
	 * @param inFileName
	 *        The ATL ID of the input model (usually 'IN')
	 * @param inFilePath
	 *        The path to the input model file
	 * @param outFilePath
	 *        The ATL ID of the output model (usually 'OUT'
	 * @param outFilePath
	 *        The path to the output model file
	 */
	public void transform(String inFileName, String inFilePath, String outFileName, String outFilePath)
	{
		try
		{
			// get/create models
			ASMEMFModel inModel = (ASMEMFModel) modelHandler.loadModel("IN", inMM, URI.createFileURI(inFilePath));
			ASMEMFModel outModel = (ASMEMFModel) modelHandler.newModel("OUT", URI.createFileURI(outFilePath)
					.toFileString(), outMM);
			// load models
			models.put("IN", inModel);
			models.put("OUT", outModel);
			// add the continue after errors options
			Map<String, String> options = new HashMap<String, String>();
			options.put("continueAfterError", "true");
			// launch
			AtlLauncher.getDefault().launch(transfoResource, Collections.EMPTY_MAP, models, Collections.EMPTY_MAP,
				Collections.EMPTY_LIST, options);

			modelHandler.saveModel(outModel, outFilePath, false);
			dispose(models);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Dispose all objects loaded in the parameter
	 * 
	 * @param models
	 *        the map that contains resource elements
	 */
	private void dispose(Map<String, Object> models)
	{
		for (Object model : models.values())
			((ASMEMFModel) model).dispose();
	}
}
