<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm name="0">
	<cp>
		<constant value="XMLMM2Text"/>
		<constant value="concatAllWith"/>
		<constant value="A"/>
		<constant value="1"/>
		<constant value="QS"/>
		<constant value="2"/>
		<constant value="S"/>
		<constant value=""/>
		<constant value="3"/>
		<constant value="4"/>
		<constant value="J.=(J):J"/>
		<constant value="12"/>
		<constant value="13"/>
		<constant value="J.+(J):J"/>
		<constant value="7:32-7:34"/>
		<constant value="7:17-7:34"/>
		<constant value="7:2-7:4"/>
		<constant value="8:3-8:6"/>
		<constant value="9:7-9:10"/>
		<constant value="9:13-9:15"/>
		<constant value="9:7-9:15"/>
		<constant value="9:30-9:39"/>
		<constant value="9:22-9:24"/>
		<constant value="9:3-9:45"/>
		<constant value="8:3-9:45"/>
		<constant value="10:3-10:4"/>
		<constant value="8:3-10:4"/>
		<constant value="7:2-10:5"/>
		<constant value="s"/>
		<constant value="acc"/>
		<constant value="self"/>
		<constant value="ls"/>
		<constant value="separator"/>
		<constant value="concatAll"/>
		<constant value="J.concatAllWith(JJ):J"/>
		<constant value="13:2-13:12"/>
		<constant value="13:27-13:29"/>
		<constant value="13:31-13:33"/>
		<constant value="13:2-13:34"/>
		<constant value="toDotGraph"/>
		<constant value="MWB!Module;"/>
		<constant value="digraph "/>
		<constant value="0"/>
		<constant value="name"/>
		<constant value=" {"/>
		<constant value="declarationZone"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="14"/>
		<constant value="J.toDot():J"/>
		<constant value="15"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="submodel"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.concatAll(J):J"/>
		<constant value="allocate"/>
		<constant value="service"/>
		<constant value="resourcePool"/>
		<constant value="block"/>
		<constant value="&#10;}&#10;"/>
		<constant value="19:2-19:12"/>
		<constant value="19:15-19:19"/>
		<constant value="19:15-19:24"/>
		<constant value="19:2-19:24"/>
		<constant value="19:27-19:31"/>
		<constant value="19:2-19:31"/>
		<constant value="20:6-20:10"/>
		<constant value="20:6-20:26"/>
		<constant value="20:6-20:43"/>
		<constant value="20:58-20:62"/>
		<constant value="20:58-20:78"/>
		<constant value="20:58-20:86"/>
		<constant value="20:50-20:52"/>
		<constant value="20:2-20:92"/>
		<constant value="19:2-20:92"/>
		<constant value="21:2-21:12"/>
		<constant value="21:23-21:27"/>
		<constant value="21:23-21:36"/>
		<constant value="21:50-21:51"/>
		<constant value="21:50-21:59"/>
		<constant value="21:23-21:60"/>
		<constant value="21:2-21:61"/>
		<constant value="19:2-21:61"/>
		<constant value="22:2-22:12"/>
		<constant value="22:23-22:27"/>
		<constant value="22:23-22:36"/>
		<constant value="22:50-22:51"/>
		<constant value="22:50-22:59"/>
		<constant value="22:23-22:60"/>
		<constant value="22:2-22:61"/>
		<constant value="19:2-22:61"/>
		<constant value="23:2-23:12"/>
		<constant value="23:23-23:27"/>
		<constant value="23:23-23:35"/>
		<constant value="23:49-23:50"/>
		<constant value="23:49-23:58"/>
		<constant value="23:23-23:59"/>
		<constant value="23:2-23:60"/>
		<constant value="19:2-23:60"/>
		<constant value="24:2-24:12"/>
		<constant value="24:23-24:27"/>
		<constant value="24:23-24:40"/>
		<constant value="24:54-24:55"/>
		<constant value="24:54-24:63"/>
		<constant value="24:23-24:64"/>
		<constant value="24:2-24:65"/>
		<constant value="19:2-24:65"/>
		<constant value="25:2-25:12"/>
		<constant value="25:23-25:27"/>
		<constant value="25:23-25:33"/>
		<constant value="25:47-25:48"/>
		<constant value="25:47-25:56"/>
		<constant value="25:23-25:57"/>
		<constant value="25:2-25:58"/>
		<constant value="19:2-25:58"/>
		<constant value="26:2-26:9"/>
		<constant value="19:2-26:9"/>
		<constant value="n"/>
		<constant value="a"/>
		<constant value="MWB!Submodel;"/>
		<constant value="node"/>
		<constant value="arc"/>
		<constant value="29:2-29:12"/>
		<constant value="29:15-29:19"/>
		<constant value="29:15-29:24"/>
		<constant value="29:2-29:24"/>
		<constant value="29:27-29:31"/>
		<constant value="29:2-29:31"/>
		<constant value="30:6-30:10"/>
		<constant value="30:6-30:26"/>
		<constant value="30:6-30:43"/>
		<constant value="30:58-30:62"/>
		<constant value="30:58-30:78"/>
		<constant value="30:58-30:86"/>
		<constant value="30:50-30:52"/>
		<constant value="30:2-30:92"/>
		<constant value="29:2-30:92"/>
		<constant value="31:2-31:12"/>
		<constant value="31:23-31:27"/>
		<constant value="31:23-31:32"/>
		<constant value="31:46-31:47"/>
		<constant value="31:46-31:55"/>
		<constant value="31:23-31:56"/>
		<constant value="31:2-31:57"/>
		<constant value="29:2-31:57"/>
		<constant value="32:2-32:12"/>
		<constant value="32:23-32:27"/>
		<constant value="32:23-32:31"/>
		<constant value="32:45-32:46"/>
		<constant value="32:45-32:54"/>
		<constant value="32:23-32:55"/>
		<constant value="32:2-32:56"/>
		<constant value="29:2-32:56"/>
		<constant value="33:2-33:9"/>
		<constant value="29:2-33:9"/>
		<constant value="toDot"/>
		<constant value="MWB!GraphicalNode;"/>
		<constant value="&#10;&#9;"/>
		<constant value=";"/>
		<constant value="39:2-39:8"/>
		<constant value="39:11-39:15"/>
		<constant value="39:11-39:20"/>
		<constant value="39:2-39:20"/>
		<constant value="39:23-39:26"/>
		<constant value="39:2-39:26"/>
		<constant value="MWB!Arc;"/>
		<constant value="fromNode"/>
		<constant value=" -&gt; "/>
		<constant value="toNode"/>
		<constant value="42:2-42:8"/>
		<constant value="42:11-42:15"/>
		<constant value="42:11-42:24"/>
		<constant value="42:11-42:29"/>
		<constant value="42:2-42:29"/>
		<constant value="42:32-42:38"/>
		<constant value="42:2-42:38"/>
		<constant value="42:41-42:45"/>
		<constant value="42:41-42:52"/>
		<constant value="42:41-42:57"/>
		<constant value="42:2-42:57"/>
		<constant value="42:60-42:63"/>
		<constant value="42:2-42:63"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
			<parameter name="3" type="4"/>
			<parameter name="5" type="6"/>
		</parameters>
		<code>
			<push arg="7"/>
			<store arg="8"/>
			<load arg="3"/>
			<iterate/>
			<store arg="9"/>
			<load arg="8"/>
			<load arg="8"/>
			<push arg="7"/>
			<call arg="10"/>
			<if arg="11"/>
			<load arg="5"/>
			<goto arg="12"/>
			<push arg="7"/>
			<call arg="13"/>
			<load arg="9"/>
			<call arg="13"/>
			<store arg="8"/>
			<enditerate/>
			<load arg="8"/>
		</code>
		<linenumbertable>
			<lne id="14" begin="0" end="0"/>
			<lne id="15" begin="0" end="0"/>
			<lne id="16" begin="2" end="2"/>
			<lne id="17" begin="5" end="5"/>
			<lne id="18" begin="6" end="6"/>
			<lne id="19" begin="7" end="7"/>
			<lne id="20" begin="6" end="8"/>
			<lne id="21" begin="10" end="10"/>
			<lne id="22" begin="12" end="12"/>
			<lne id="23" begin="6" end="12"/>
			<lne id="24" begin="5" end="13"/>
			<lne id="25" begin="14" end="14"/>
			<lne id="26" begin="5" end="15"/>
			<lne id="27" begin="0" end="18"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="4" name="28" begin="4" end="16"/>
			<lve slot="3" name="29" begin="1" end="18"/>
			<lve slot="0" name="30" begin="0" end="18"/>
			<lve slot="1" name="31" begin="0" end="18"/>
			<lve slot="2" name="32" begin="0" end="18"/>
		</localvariabletable>
	</operation>
	<operation name="33">
		<context type="2"/>
		<parameters>
			<parameter name="3" type="4"/>
		</parameters>
		<code>
			<getasm/>
			<load arg="3"/>
			<push arg="7"/>
			<call arg="34"/>
		</code>
		<linenumbertable>
			<lne id="35" begin="0" end="0"/>
			<lne id="36" begin="1" end="1"/>
			<lne id="37" begin="2" end="2"/>
			<lne id="38" begin="0" end="3"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="30" begin="0" end="3"/>
			<lve slot="1" name="31" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="40"/>
		<parameters>
		</parameters>
		<code>
			<push arg="41"/>
			<load arg="42"/>
			<get arg="43"/>
			<call arg="13"/>
			<push arg="44"/>
			<call arg="13"/>
			<load arg="42"/>
			<get arg="45"/>
			<call arg="46"/>
			<if arg="47"/>
			<load arg="42"/>
			<get arg="45"/>
			<call arg="48"/>
			<goto arg="49"/>
			<push arg="7"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="52"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="55"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="56"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="57"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="58"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<push arg="59"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="60" begin="0" end="0"/>
			<lne id="61" begin="1" end="1"/>
			<lne id="62" begin="1" end="2"/>
			<lne id="63" begin="0" end="3"/>
			<lne id="64" begin="4" end="4"/>
			<lne id="65" begin="0" end="5"/>
			<lne id="66" begin="6" end="6"/>
			<lne id="67" begin="6" end="7"/>
			<lne id="68" begin="6" end="8"/>
			<lne id="69" begin="10" end="10"/>
			<lne id="70" begin="10" end="11"/>
			<lne id="71" begin="10" end="12"/>
			<lne id="72" begin="14" end="14"/>
			<lne id="73" begin="6" end="14"/>
			<lne id="74" begin="0" end="15"/>
			<lne id="75" begin="16" end="16"/>
			<lne id="76" begin="20" end="20"/>
			<lne id="77" begin="20" end="21"/>
			<lne id="78" begin="24" end="24"/>
			<lne id="79" begin="24" end="25"/>
			<lne id="80" begin="17" end="27"/>
			<lne id="81" begin="16" end="28"/>
			<lne id="82" begin="0" end="29"/>
			<lne id="83" begin="30" end="30"/>
			<lne id="84" begin="34" end="34"/>
			<lne id="85" begin="34" end="35"/>
			<lne id="86" begin="38" end="38"/>
			<lne id="87" begin="38" end="39"/>
			<lne id="88" begin="31" end="41"/>
			<lne id="89" begin="30" end="42"/>
			<lne id="90" begin="0" end="43"/>
			<lne id="91" begin="44" end="44"/>
			<lne id="92" begin="48" end="48"/>
			<lne id="93" begin="48" end="49"/>
			<lne id="94" begin="52" end="52"/>
			<lne id="95" begin="52" end="53"/>
			<lne id="96" begin="45" end="55"/>
			<lne id="97" begin="44" end="56"/>
			<lne id="98" begin="0" end="57"/>
			<lne id="99" begin="58" end="58"/>
			<lne id="100" begin="62" end="62"/>
			<lne id="101" begin="62" end="63"/>
			<lne id="102" begin="66" end="66"/>
			<lne id="103" begin="66" end="67"/>
			<lne id="104" begin="59" end="69"/>
			<lne id="105" begin="58" end="70"/>
			<lne id="106" begin="0" end="71"/>
			<lne id="107" begin="72" end="72"/>
			<lne id="108" begin="76" end="76"/>
			<lne id="109" begin="76" end="77"/>
			<lne id="110" begin="80" end="80"/>
			<lne id="111" begin="80" end="81"/>
			<lne id="112" begin="73" end="83"/>
			<lne id="113" begin="72" end="84"/>
			<lne id="114" begin="0" end="85"/>
			<lne id="115" begin="86" end="86"/>
			<lne id="116" begin="0" end="87"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="117" begin="23" end="26"/>
			<lve slot="1" name="118" begin="37" end="40"/>
			<lve slot="1" name="118" begin="51" end="54"/>
			<lve slot="1" name="118" begin="65" end="68"/>
			<lve slot="1" name="118" begin="79" end="82"/>
			<lve slot="0" name="30" begin="0" end="87"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="119"/>
		<parameters>
		</parameters>
		<code>
			<push arg="41"/>
			<load arg="42"/>
			<get arg="43"/>
			<call arg="13"/>
			<push arg="44"/>
			<call arg="13"/>
			<load arg="42"/>
			<get arg="45"/>
			<call arg="46"/>
			<if arg="47"/>
			<load arg="42"/>
			<get arg="45"/>
			<call arg="48"/>
			<goto arg="49"/>
			<push arg="7"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="120"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<getasm/>
			<push arg="50"/>
			<push arg="51"/>
			<new/>
			<load arg="42"/>
			<get arg="121"/>
			<iterate/>
			<store arg="3"/>
			<load arg="3"/>
			<call arg="48"/>
			<call arg="53"/>
			<enditerate/>
			<call arg="54"/>
			<call arg="13"/>
			<push arg="59"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="122" begin="0" end="0"/>
			<lne id="123" begin="1" end="1"/>
			<lne id="124" begin="1" end="2"/>
			<lne id="125" begin="0" end="3"/>
			<lne id="126" begin="4" end="4"/>
			<lne id="127" begin="0" end="5"/>
			<lne id="128" begin="6" end="6"/>
			<lne id="129" begin="6" end="7"/>
			<lne id="130" begin="6" end="8"/>
			<lne id="131" begin="10" end="10"/>
			<lne id="132" begin="10" end="11"/>
			<lne id="133" begin="10" end="12"/>
			<lne id="134" begin="14" end="14"/>
			<lne id="135" begin="6" end="14"/>
			<lne id="136" begin="0" end="15"/>
			<lne id="137" begin="16" end="16"/>
			<lne id="138" begin="20" end="20"/>
			<lne id="139" begin="20" end="21"/>
			<lne id="140" begin="24" end="24"/>
			<lne id="141" begin="24" end="25"/>
			<lne id="142" begin="17" end="27"/>
			<lne id="143" begin="16" end="28"/>
			<lne id="144" begin="0" end="29"/>
			<lne id="145" begin="30" end="30"/>
			<lne id="146" begin="34" end="34"/>
			<lne id="147" begin="34" end="35"/>
			<lne id="148" begin="38" end="38"/>
			<lne id="149" begin="38" end="39"/>
			<lne id="150" begin="31" end="41"/>
			<lne id="151" begin="30" end="42"/>
			<lne id="152" begin="0" end="43"/>
			<lne id="153" begin="44" end="44"/>
			<lne id="154" begin="0" end="45"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="117" begin="23" end="26"/>
			<lve slot="1" name="118" begin="37" end="40"/>
			<lve slot="0" name="30" begin="0" end="45"/>
		</localvariabletable>
	</operation>
	<operation name="155">
		<context type="156"/>
		<parameters>
		</parameters>
		<code>
			<push arg="157"/>
			<load arg="42"/>
			<get arg="43"/>
			<call arg="13"/>
			<push arg="158"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="159" begin="0" end="0"/>
			<lne id="160" begin="1" end="1"/>
			<lne id="161" begin="1" end="2"/>
			<lne id="162" begin="0" end="3"/>
			<lne id="163" begin="4" end="4"/>
			<lne id="164" begin="0" end="5"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="30" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="155">
		<context type="165"/>
		<parameters>
		</parameters>
		<code>
			<push arg="157"/>
			<load arg="42"/>
			<get arg="166"/>
			<get arg="43"/>
			<call arg="13"/>
			<push arg="167"/>
			<call arg="13"/>
			<load arg="42"/>
			<get arg="168"/>
			<get arg="43"/>
			<call arg="13"/>
			<push arg="158"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="169" begin="0" end="0"/>
			<lne id="170" begin="1" end="1"/>
			<lne id="171" begin="1" end="2"/>
			<lne id="172" begin="1" end="3"/>
			<lne id="173" begin="0" end="4"/>
			<lne id="174" begin="5" end="5"/>
			<lne id="175" begin="0" end="6"/>
			<lne id="176" begin="7" end="7"/>
			<lne id="177" begin="7" end="8"/>
			<lne id="178" begin="7" end="9"/>
			<lne id="179" begin="0" end="10"/>
			<lne id="180" begin="11" end="11"/>
			<lne id="181" begin="0" end="12"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="30" begin="0" end="12"/>
		</localvariabletable>
	</operation>
</asm>
