package org.ujf.verimag.ma2wb;

public interface Constants {

	/**
	 * Uri of the Worbench metamodel
	 */
	public final static String	WB_MM_URI		= "uri:http:///wbmm.ecore";

	/**
	 * Uri of the ARINC metamodel
	 */
	public final static String	ARINC_MM_URI	= "uri:http:///ARINC.ecore";

	/**
	 * Uri of the XML metamodel
	 */
	public final static String	XML_MM_URI	= "uri:http:///xmlmm.ecore";

	/**
	 * Name of the Workbench metamodel in the ATL transformation file
	 */
	public final static String	WB_MM_NAME		= "XXXXXXXXXXX";

	/**
	 * Name of the ARINC metamodel in the ATL transformation file
	 */
	public final static String	ARINC_MM_NAME	= "XXXXXXXXXXXX";

	/**
	 * Name of the XML metamodel in the ATL transformation file
	 */
	public final static String	XML_MM_NAME		= "XXXXXXXXXX";

	/**
	 * Extension of Workbench file
	 */
	public final static String	WB_EXT			= "wbmm";

	/**
	 * Extension of Fiacre file
	 */
	public final static String	ARINC_EXT		= "mamm";

	/**
	 * Extension of XML file
	 */
	public final static String	XML_EXT			= "xml";
}
