/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.AperiodicPhysicalComponent;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.MajorFrame;
import org.ujf.verimag.ARINC.NamedElement;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.PhysicalComponent;
import org.ujf.verimag.ARINC.Task;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides an adapter <code>createXXX</code>
 * method for each class of the model. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage
 * @generated
 */
public class ARINCAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static ARINCPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ARINCAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = ARINCPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object. <!-- begin-user-doc --> This
	 * implementation returns <code>true</code> if the object is either the model's package or is an instance object of
	 * the model. <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ARINCSwitch<Adapter>	modelSwitch	= new ARINCSwitch<Adapter>()
												{
													@Override
													public Adapter caseMIF(MIF object)
													{
														return createMIFAdapter();
													}
													@Override
													public Adapter caseNamedElement(NamedElement object)
													{
														return createNamedElementAdapter();
													}
													@Override
													public Adapter caseWindow(Window object)
													{
														return createWindowAdapter();
													}
													@Override
													public Adapter casePartition(Partition object)
													{
														return createPartitionAdapter();
													}
													@Override
													public Adapter casePhysicalComponent(PhysicalComponent object)
													{
														return createPhysicalComponentAdapter();
													}
													@Override
													public Adapter caseLogicalComponent(LogicalComponent object)
													{
														return createLogicalComponentAdapter();
													}
													@Override
													public Adapter caseArincSystem(ArincSystem object)
													{
														return createArincSystemAdapter();
													}
													@Override
													public Adapter caseMajorFrame(MajorFrame object)
													{
														return createMajorFrameAdapter();
													}
													@Override
													public Adapter casePeriodicPhysicalComponent(
															PeriodicPhysicalComponent object)
													{
														return createPeriodicPhysicalComponentAdapter();
													}
													@Override
													public Adapter caseTask(Task object)
													{
														return createTaskAdapter();
													}
													@Override
													public Adapter caseAperiodicPhysicalComponent(
															AperiodicPhysicalComponent object)
													{
														return createAperiodicPhysicalComponentAdapter();
													}
													@Override
													public Adapter defaultCase(EObject object)
													{
														return createEObjectAdapter();
													}
												};

	/**
	 * Creates an adapter for the <code>target</code>. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param target
	 *        the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.MIF <em>MIF</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.MIF
	 * @generated
	 */
	public Adapter createMIFAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.Window <em>Window</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.Window
	 * @generated
	 */
	public Adapter createWindowAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.Partition <em>Partition</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.Partition
	 * @generated
	 */
	public Adapter createPartitionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.PhysicalComponent
	 * <em>Physical Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.PhysicalComponent
	 * @generated
	 */
	public Adapter createPhysicalComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.LogicalComponent
	 * <em>Logical Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.LogicalComponent
	 * @generated
	 */
	public Adapter createLogicalComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.ArincSystem <em>Arinc System</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.ArincSystem
	 * @generated
	 */
	public Adapter createArincSystemAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.MajorFrame <em>Major Frame</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.MajorFrame
	 * @generated
	 */
	public Adapter createMajorFrameAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent
	 * <em>Periodic Physical Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent
	 * @generated
	 */
	public Adapter createPeriodicPhysicalComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.Task <em>Task</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.Task
	 * @generated
	 */
	public Adapter createTaskAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.ARINC.AperiodicPhysicalComponent
	 * <em>Aperiodic Physical Component</em>}'. <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.ARINC.AperiodicPhysicalComponent
	 * @generated
	 */
	public Adapter createAperiodicPhysicalComponentAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case. <!-- begin-user-doc --> This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} // ARINCAdapterFactory
