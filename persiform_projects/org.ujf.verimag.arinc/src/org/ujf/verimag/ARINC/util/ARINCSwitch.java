/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.AperiodicPhysicalComponent;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.MajorFrame;
import org.ujf.verimag.ARINC.NamedElement;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.PhysicalComponent;
import org.ujf.verimag.ARINC.Task;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance hierarchy. It supports the call
 * {@link #doSwitch(EObject) doSwitch(object)} to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the inheritance hierarchy until a non-null result is
 * returned, which is the result of the switch. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage
 * @generated
 */
public class ARINCSwitch<T>
{
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static ARINCPackage	modelPackage;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ARINCSwitch()
	{
		if (modelPackage == null)
		{
			modelPackage = ARINCPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject)
	{
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject)
	{
		if (theEClass.eContainer() == modelPackage)
		{
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else
		{
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject)
	{
		switch (classifierID)
		{
		case ARINCPackage.MIF:
		{
			MIF mif = (MIF) theEObject;
			T result = caseMIF(mif);
			if (result == null)
				result = caseNamedElement(mif);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.NAMED_ELEMENT:
		{
			NamedElement namedElement = (NamedElement) theEObject;
			T result = caseNamedElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.WINDOW:
		{
			Window window = (Window) theEObject;
			T result = caseWindow(window);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.PARTITION:
		{
			Partition partition = (Partition) theEObject;
			T result = casePartition(partition);
			if (result == null)
				result = caseNamedElement(partition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.PHYSICAL_COMPONENT:
		{
			PhysicalComponent physicalComponent = (PhysicalComponent) theEObject;
			T result = casePhysicalComponent(physicalComponent);
			if (result == null)
				result = caseNamedElement(physicalComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.LOGICAL_COMPONENT:
		{
			LogicalComponent logicalComponent = (LogicalComponent) theEObject;
			T result = caseLogicalComponent(logicalComponent);
			if (result == null)
				result = caseNamedElement(logicalComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.ARINC_SYSTEM:
		{
			ArincSystem arincSystem = (ArincSystem) theEObject;
			T result = caseArincSystem(arincSystem);
			if (result == null)
				result = caseNamedElement(arincSystem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.MAJOR_FRAME:
		{
			MajorFrame majorFrame = (MajorFrame) theEObject;
			T result = caseMajorFrame(majorFrame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT:
		{
			PeriodicPhysicalComponent periodicPhysicalComponent = (PeriodicPhysicalComponent) theEObject;
			T result = casePeriodicPhysicalComponent(periodicPhysicalComponent);
			if (result == null)
				result = casePhysicalComponent(periodicPhysicalComponent);
			if (result == null)
				result = caseNamedElement(periodicPhysicalComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.TASK:
		{
			Task task = (Task) theEObject;
			T result = caseTask(task);
			if (result == null)
				result = caseNamedElement(task);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ARINCPackage.APERIODIC_PHYSICAL_COMPONENT:
		{
			AperiodicPhysicalComponent aperiodicPhysicalComponent = (AperiodicPhysicalComponent) theEObject;
			T result = caseAperiodicPhysicalComponent(aperiodicPhysicalComponent);
			if (result == null)
				result = casePhysicalComponent(aperiodicPhysicalComponent);
			if (result == null)
				result = caseNamedElement(aperiodicPhysicalComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MIF</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MIF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMIF(MIF object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Window</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Window</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWindow(Window object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Partition</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Partition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePartition(Partition object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalComponent(PhysicalComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Logical Component</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Logical Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogicalComponent(LogicalComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arinc System</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arinc System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArincSystem(ArincSystem object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Major Frame</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Major Frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMajorFrame(MajorFrame object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Periodic Physical Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Periodic Physical Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePeriodicPhysicalComponent(PeriodicPhysicalComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTask(Task object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aperiodic Physical Component</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aperiodic Physical Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAperiodicPhysicalComponent(AperiodicPhysicalComponent object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch, but this is the last case
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object)
	{
		return null;
	}

} // ARINCSwitch
