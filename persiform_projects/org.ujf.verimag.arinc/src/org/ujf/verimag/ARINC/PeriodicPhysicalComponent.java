/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Periodic Physical Component</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getNMifPeriod <em>NMif Period</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getDeadline <em>Deadline</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getFunctionalChain <em>Functional Chain</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getPeriodicPhysicalComponent()
 * @model
 * @generated
 */
public interface PeriodicPhysicalComponent extends PhysicalComponent
{
	/**
	 * Returns the value of the '<em><b>NMif Period</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NMif Period</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>NMif Period</em>' attribute.
	 * @see #setNMifPeriod(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPeriodicPhysicalComponent_NMifPeriod()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getNMifPeriod();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getNMifPeriod <em>NMif Period</em>}
	 * ' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>NMif Period</em>' attribute.
	 * @see #getNMifPeriod()
	 * @generated
	 */
	void setNMifPeriod(int value);

	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPeriodicPhysicalComponent_Deadline()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getDeadline();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getDeadline <em>Deadline</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(int value);

	/**
	 * Returns the value of the '<em><b>Functional Chain</b></em>' containment reference list. The list contents are of
	 * type {@link org.ujf.verimag.ARINC.Task}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent <em>Periodic Physical Component</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Chain</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Functional Chain</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPeriodicPhysicalComponent_FunctionalChain()
	 * @see org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent
	 * @model opposite="periodicPhysicalComponent" containment="true" required="true"
	 * @generated
	 */
	EList<Task> getFunctionalChain();

} // PeriodicPhysicalComponent
