/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Major Frame</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.MajorFrame#getMifDuration <em>Mif Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.MajorFrame#getMif <em>Mif</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.MajorFrame#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getMajorFrame()
 * @model
 * @generated
 */
public interface MajorFrame extends EObject
{
	/**
	 * Returns the value of the '<em><b>Mif Duration</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mif Duration</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mif Duration</em>' attribute.
	 * @see #setMifDuration(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getMajorFrame_MifDuration()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getMifDuration();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.MajorFrame#getMifDuration <em>Mif Duration</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Mif Duration</em>' attribute.
	 * @see #getMifDuration()
	 * @generated
	 */
	void setMifDuration(int value);

	/**
	 * Returns the value of the '<em><b>Mif</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.ARINC.MIF}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mif</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mif</em>' reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getMajorFrame_Mif()
	 * @model required="true"
	 * @generated
	 */
	EList<MIF> getMif();

	/**
	 * Returns the value of the '<em><b>System</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.ArincSystem#getMajorFrame <em>Major Frame</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>System</em>' container reference.
	 * @see #setSystem(ArincSystem)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getMajorFrame_System()
	 * @see org.ujf.verimag.ARINC.ArincSystem#getMajorFrame
	 * @model opposite="majorFrame" required="true" transient="false" ordered="false"
	 * @generated
	 */
	ArincSystem getSystem();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.MajorFrame#getSystem <em>System</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>System</em>' container reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(ArincSystem value);

} // MajorFrame
