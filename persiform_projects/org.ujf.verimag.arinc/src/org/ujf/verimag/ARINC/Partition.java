/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Partition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.Partition#getPhysicalComponent <em>Physical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.Partition#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getPartition()
 * @model
 * @generated
 */
public interface Partition extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Physical Component</b></em>' containment reference list. The list contents are
	 * of type {@link org.ujf.verimag.ARINC.PhysicalComponent}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.PhysicalComponent#getPartition <em>Partition</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Component</em>' containment reference list isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Physical Component</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPartition_PhysicalComponent()
	 * @see org.ujf.verimag.ARINC.PhysicalComponent#getPartition
	 * @model opposite="partition" containment="true" ordered="false"
	 * @generated
	 */
	EList<PhysicalComponent> getPhysicalComponent();

	/**
	 * Returns the value of the '<em><b>System</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.ArincSystem#getPartition <em>Partition</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>System</em>' container reference.
	 * @see #setSystem(ArincSystem)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPartition_System()
	 * @see org.ujf.verimag.ARINC.ArincSystem#getPartition
	 * @model opposite="partition" required="true" transient="false" ordered="false"
	 * @generated
	 */
	ArincSystem getSystem();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Partition#getSystem <em>System</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>System</em>' container reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(ArincSystem value);

} // Partition
