/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PhysicalComponent;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Physical Component</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.PhysicalComponentImpl#getPriority <em>Priority</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.PhysicalComponentImpl#getLogicalComponent <em>Logical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.PhysicalComponentImpl#getPartition <em>Partition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class PhysicalComponentImpl extends NamedElementImpl implements PhysicalComponent
{
	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int			PRIORITY_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected int						priority			= PRIORITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLogicalComponent() <em>Logical Component</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLogicalComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<LogicalComponent>	logicalComponent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PhysicalComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.PHYSICAL_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getPriority()
	{
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPriority(int newPriority)
	{
		int oldPriority = priority;
		priority = newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.PHYSICAL_COMPONENT__PRIORITY,
				oldPriority, priority));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<LogicalComponent> getLogicalComponent()
	{
		if (logicalComponent == null)
		{
			logicalComponent = new EObjectContainmentWithInverseEList<LogicalComponent>(LogicalComponent.class, this,
				ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT, ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT);
		}
		return logicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Partition getPartition()
	{
		if (eContainerFeatureID != ARINCPackage.PHYSICAL_COMPONENT__PARTITION)
			return null;
		return (Partition) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetPartition(Partition newPartition, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newPartition, ARINCPackage.PHYSICAL_COMPONENT__PARTITION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPartition(Partition newPartition)
	{
		if (newPartition != eInternalContainer()
				|| (eContainerFeatureID != ARINCPackage.PHYSICAL_COMPONENT__PARTITION && newPartition != null))
		{
			if (EcoreUtil.isAncestor(this, newPartition))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPartition != null)
				msgs = ((InternalEObject) newPartition).eInverseAdd(this, ARINCPackage.PARTITION__PHYSICAL_COMPONENT,
					Partition.class, msgs);
			msgs = basicSetPartition(newPartition, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.PHYSICAL_COMPONENT__PARTITION,
				newPartition, newPartition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getLogicalComponent()).basicAdd(otherEnd, msgs);
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPartition((Partition) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			return ((InternalEList<?>) getLogicalComponent()).basicRemove(otherEnd, msgs);
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			return basicSetPartition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.PARTITION__PHYSICAL_COMPONENT,
				Partition.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__PRIORITY:
			return new Integer(getPriority());
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			return getLogicalComponent();
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			return getPartition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__PRIORITY:
			setPriority(((Integer) newValue).intValue());
			return;
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			getLogicalComponent().clear();
			getLogicalComponent().addAll((Collection<? extends LogicalComponent>) newValue);
			return;
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			setPartition((Partition) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__PRIORITY:
			setPriority(PRIORITY_EDEFAULT);
			return;
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			getLogicalComponent().clear();
			return;
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			setPartition((Partition) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PHYSICAL_COMPONENT__PRIORITY:
			return priority != PRIORITY_EDEFAULT;
		case ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT:
			return logicalComponent != null && !logicalComponent.isEmpty();
		case ARINCPackage.PHYSICAL_COMPONENT__PARTITION:
			return getPartition() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (priority: ");
		result.append(priority);
		result.append(')');
		return result.toString();
	}

} // PhysicalComponentImpl
