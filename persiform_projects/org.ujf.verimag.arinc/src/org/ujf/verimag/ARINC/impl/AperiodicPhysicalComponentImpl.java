/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.ecore.EClass;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.AperiodicPhysicalComponent;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Aperiodic Physical Component</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class AperiodicPhysicalComponentImpl extends PhysicalComponentImpl implements AperiodicPhysicalComponent
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AperiodicPhysicalComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.APERIODIC_PHYSICAL_COMPONENT;
	}

} // AperiodicPhysicalComponentImpl
