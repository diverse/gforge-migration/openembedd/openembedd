/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.MajorFrame;
import org.ujf.verimag.ARINC.Partition;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Arinc System</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.ArincSystemImpl#getMajorFrame <em>Major Frame</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.ArincSystemImpl#getMif <em>Mif</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.ArincSystemImpl#getPartition <em>Partition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ArincSystemImpl extends NamedElementImpl implements ArincSystem
{
	/**
	 * The cached value of the '{@link #getMajorFrame() <em>Major Frame</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getMajorFrame()
	 * @generated
	 * @ordered
	 */
	protected MajorFrame		majorFrame;

	/**
	 * The cached value of the '{@link #getMif() <em>Mif</em>}' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getMif()
	 * @generated
	 * @ordered
	 */
	protected EList<MIF>		mif;

	/**
	 * The cached value of the '{@link #getPartition() <em>Partition</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPartition()
	 * @generated
	 * @ordered
	 */
	protected EList<Partition>	partition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ArincSystemImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.ARINC_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MajorFrame getMajorFrame()
	{
		return majorFrame;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetMajorFrame(MajorFrame newMajorFrame, NotificationChain msgs)
	{
		MajorFrame oldMajorFrame = majorFrame;
		majorFrame = newMajorFrame;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME, oldMajorFrame, newMajorFrame);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMajorFrame(MajorFrame newMajorFrame)
	{
		if (newMajorFrame != majorFrame)
		{
			NotificationChain msgs = null;
			if (majorFrame != null)
				msgs = ((InternalEObject) majorFrame).eInverseRemove(this, ARINCPackage.MAJOR_FRAME__SYSTEM,
					MajorFrame.class, msgs);
			if (newMajorFrame != null)
				msgs = ((InternalEObject) newMajorFrame).eInverseAdd(this, ARINCPackage.MAJOR_FRAME__SYSTEM,
					MajorFrame.class, msgs);
			msgs = basicSetMajorFrame(newMajorFrame, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME,
				newMajorFrame, newMajorFrame));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<MIF> getMif()
	{
		if (mif == null)
		{
			mif = new EObjectContainmentWithInverseEList<MIF>(MIF.class, this, ARINCPackage.ARINC_SYSTEM__MIF,
				ARINCPackage.MIF__SYSTEM);
		}
		return mif;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Partition> getPartition()
	{
		if (partition == null)
		{
			partition = new EObjectContainmentWithInverseEList<Partition>(Partition.class, this,
				ARINCPackage.ARINC_SYSTEM__PARTITION, ARINCPackage.PARTITION__SYSTEM);
		}
		return partition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			if (majorFrame != null)
				msgs = ((InternalEObject) majorFrame).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME, null, msgs);
			return basicSetMajorFrame((MajorFrame) otherEnd, msgs);
		case ARINCPackage.ARINC_SYSTEM__MIF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getMif()).basicAdd(otherEnd, msgs);
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPartition()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			return basicSetMajorFrame(null, msgs);
		case ARINCPackage.ARINC_SYSTEM__MIF:
			return ((InternalEList<?>) getMif()).basicRemove(otherEnd, msgs);
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			return ((InternalEList<?>) getPartition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			return getMajorFrame();
		case ARINCPackage.ARINC_SYSTEM__MIF:
			return getMif();
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			return getPartition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			setMajorFrame((MajorFrame) newValue);
			return;
		case ARINCPackage.ARINC_SYSTEM__MIF:
			getMif().clear();
			getMif().addAll((Collection<? extends MIF>) newValue);
			return;
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			getPartition().clear();
			getPartition().addAll((Collection<? extends Partition>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			setMajorFrame((MajorFrame) null);
			return;
		case ARINCPackage.ARINC_SYSTEM__MIF:
			getMif().clear();
			return;
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			getPartition().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME:
			return majorFrame != null;
		case ARINCPackage.ARINC_SYSTEM__MIF:
			return mif != null && !mif.isEmpty();
		case ARINCPackage.ARINC_SYSTEM__PARTITION:
			return partition != null && !partition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ArincSystemImpl
