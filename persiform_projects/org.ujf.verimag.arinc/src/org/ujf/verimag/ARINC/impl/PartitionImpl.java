/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PhysicalComponent;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Partition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.PartitionImpl#getPhysicalComponent <em>Physical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.PartitionImpl#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class PartitionImpl extends NamedElementImpl implements Partition
{
	/**
	 * The cached value of the '{@link #getPhysicalComponent() <em>Physical Component</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPhysicalComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalComponent>	physicalComponent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PartitionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.PARTITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<PhysicalComponent> getPhysicalComponent()
	{
		if (physicalComponent == null)
		{
			physicalComponent = new EObjectContainmentWithInverseEList<PhysicalComponent>(PhysicalComponent.class,
				this, ARINCPackage.PARTITION__PHYSICAL_COMPONENT, ARINCPackage.PHYSICAL_COMPONENT__PARTITION);
		}
		return physicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ArincSystem getSystem()
	{
		if (eContainerFeatureID != ARINCPackage.PARTITION__SYSTEM)
			return null;
		return (ArincSystem) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSystem(ArincSystem newSystem, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSystem, ARINCPackage.PARTITION__SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSystem(ArincSystem newSystem)
	{
		if (newSystem != eInternalContainer()
				|| (eContainerFeatureID != ARINCPackage.PARTITION__SYSTEM && newSystem != null))
		{
			if (EcoreUtil.isAncestor(this, newSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSystem != null)
				msgs = ((InternalEObject) newSystem).eInverseAdd(this, ARINCPackage.ARINC_SYSTEM__PARTITION,
					ArincSystem.class, msgs);
			msgs = basicSetSystem(newSystem, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.PARTITION__SYSTEM, newSystem, newSystem));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPhysicalComponent())
					.basicAdd(otherEnd, msgs);
		case ARINCPackage.PARTITION__SYSTEM:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSystem((ArincSystem) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			return ((InternalEList<?>) getPhysicalComponent()).basicRemove(otherEnd, msgs);
		case ARINCPackage.PARTITION__SYSTEM:
			return basicSetSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.PARTITION__SYSTEM:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.ARINC_SYSTEM__PARTITION, ArincSystem.class,
				msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			return getPhysicalComponent();
		case ARINCPackage.PARTITION__SYSTEM:
			return getSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			getPhysicalComponent().clear();
			getPhysicalComponent().addAll((Collection<? extends PhysicalComponent>) newValue);
			return;
		case ARINCPackage.PARTITION__SYSTEM:
			setSystem((ArincSystem) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			getPhysicalComponent().clear();
			return;
		case ARINCPackage.PARTITION__SYSTEM:
			setSystem((ArincSystem) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PARTITION__PHYSICAL_COMPONENT:
			return physicalComponent != null && !physicalComponent.isEmpty();
		case ARINCPackage.PARTITION__SYSTEM:
			return getSystem() != null;
		}
		return super.eIsSet(featureID);
	}

} // PartitionImpl
