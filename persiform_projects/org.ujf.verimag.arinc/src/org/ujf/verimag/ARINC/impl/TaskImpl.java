/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.Task;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Task</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.TaskImpl#getDuration <em>Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.TaskImpl#getLogicalComponent <em>Logical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.TaskImpl#getPeriodicPhysicalComponent <em>Periodic Physical Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TaskImpl extends NamedElementImpl implements Task
{
	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final int	DURATION_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected int				duration			= DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLogicalComponent() <em>Logical Component</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getLogicalComponent()
	 * @generated
	 * @ordered
	 */
	protected LogicalComponent	logicalComponent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TaskImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getDuration()
	{
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDuration(int newDuration)
	{
		int oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.TASK__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LogicalComponent getLogicalComponent()
	{
		if (logicalComponent != null && logicalComponent.eIsProxy())
		{
			InternalEObject oldLogicalComponent = (InternalEObject) logicalComponent;
			logicalComponent = (LogicalComponent) eResolveProxy(oldLogicalComponent);
			if (logicalComponent != oldLogicalComponent)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ARINCPackage.TASK__LOGICAL_COMPONENT,
						oldLogicalComponent, logicalComponent));
			}
		}
		return logicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LogicalComponent basicGetLogicalComponent()
	{
		return logicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLogicalComponent(LogicalComponent newLogicalComponent)
	{
		LogicalComponent oldLogicalComponent = logicalComponent;
		logicalComponent = newLogicalComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.TASK__LOGICAL_COMPONENT,
				oldLogicalComponent, logicalComponent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PeriodicPhysicalComponent getPeriodicPhysicalComponent()
	{
		if (eContainerFeatureID != ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT)
			return null;
		return (PeriodicPhysicalComponent) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetPeriodicPhysicalComponent(PeriodicPhysicalComponent newPeriodicPhysicalComponent,
			NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newPeriodicPhysicalComponent,
			ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPeriodicPhysicalComponent(PeriodicPhysicalComponent newPeriodicPhysicalComponent)
	{
		if (newPeriodicPhysicalComponent != eInternalContainer()
				|| (eContainerFeatureID != ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT && newPeriodicPhysicalComponent != null))
		{
			if (EcoreUtil.isAncestor(this, newPeriodicPhysicalComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPeriodicPhysicalComponent != null)
				msgs = ((InternalEObject) newPeriodicPhysicalComponent).eInverseAdd(this,
					ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN, PeriodicPhysicalComponent.class, msgs);
			msgs = basicSetPeriodicPhysicalComponent(newPeriodicPhysicalComponent, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT,
				newPeriodicPhysicalComponent, newPeriodicPhysicalComponent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPeriodicPhysicalComponent((PeriodicPhysicalComponent) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			return basicSetPeriodicPhysicalComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			return eInternalContainer().eInverseRemove(this,
				ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN, PeriodicPhysicalComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__DURATION:
			return new Integer(getDuration());
		case ARINCPackage.TASK__LOGICAL_COMPONENT:
			if (resolve)
				return getLogicalComponent();
			return basicGetLogicalComponent();
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			return getPeriodicPhysicalComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__DURATION:
			setDuration(((Integer) newValue).intValue());
			return;
		case ARINCPackage.TASK__LOGICAL_COMPONENT:
			setLogicalComponent((LogicalComponent) newValue);
			return;
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			setPeriodicPhysicalComponent((PeriodicPhysicalComponent) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case ARINCPackage.TASK__LOGICAL_COMPONENT:
			setLogicalComponent((LogicalComponent) null);
			return;
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			setPeriodicPhysicalComponent((PeriodicPhysicalComponent) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.TASK__DURATION:
			return duration != DURATION_EDEFAULT;
		case ARINCPackage.TASK__LOGICAL_COMPONENT:
			return logicalComponent != null;
		case ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT:
			return getPeriodicPhysicalComponent() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} // TaskImpl
