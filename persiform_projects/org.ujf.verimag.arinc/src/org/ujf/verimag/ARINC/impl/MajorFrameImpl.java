/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.MajorFrame;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Major Frame</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.MajorFrameImpl#getMifDuration <em>Mif Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.MajorFrameImpl#getMif <em>Mif</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.MajorFrameImpl#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MajorFrameImpl extends EObjectImpl implements MajorFrame
{
	/**
	 * The default value of the '{@link #getMifDuration() <em>Mif Duration</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getMifDuration()
	 * @generated
	 * @ordered
	 */
	protected static final int	MIF_DURATION_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getMifDuration() <em>Mif Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getMifDuration()
	 * @generated
	 * @ordered
	 */
	protected int				mifDuration				= MIF_DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMif() <em>Mif</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getMif()
	 * @generated
	 * @ordered
	 */
	protected EList<MIF>		mif;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected MajorFrameImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.MAJOR_FRAME;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getMifDuration()
	{
		return mifDuration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMifDuration(int newMifDuration)
	{
		int oldMifDuration = mifDuration;
		mifDuration = newMifDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.MAJOR_FRAME__MIF_DURATION,
				oldMifDuration, mifDuration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<MIF> getMif()
	{
		if (mif == null)
		{
			mif = new EObjectResolvingEList<MIF>(MIF.class, this, ARINCPackage.MAJOR_FRAME__MIF);
		}
		return mif;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ArincSystem getSystem()
	{
		if (eContainerFeatureID != ARINCPackage.MAJOR_FRAME__SYSTEM)
			return null;
		return (ArincSystem) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSystem(ArincSystem newSystem, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSystem, ARINCPackage.MAJOR_FRAME__SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSystem(ArincSystem newSystem)
	{
		if (newSystem != eInternalContainer()
				|| (eContainerFeatureID != ARINCPackage.MAJOR_FRAME__SYSTEM && newSystem != null))
		{
			if (EcoreUtil.isAncestor(this, newSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSystem != null)
				msgs = ((InternalEObject) newSystem).eInverseAdd(this, ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME,
					ArincSystem.class, msgs);
			msgs = basicSetSystem(newSystem, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.MAJOR_FRAME__SYSTEM, newSystem,
				newSystem));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSystem((ArincSystem) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			return basicSetSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.ARINC_SYSTEM__MAJOR_FRAME, ArincSystem.class,
				msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__MIF_DURATION:
			return new Integer(getMifDuration());
		case ARINCPackage.MAJOR_FRAME__MIF:
			return getMif();
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			return getSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__MIF_DURATION:
			setMifDuration(((Integer) newValue).intValue());
			return;
		case ARINCPackage.MAJOR_FRAME__MIF:
			getMif().clear();
			getMif().addAll((Collection<? extends MIF>) newValue);
			return;
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			setSystem((ArincSystem) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__MIF_DURATION:
			setMifDuration(MIF_DURATION_EDEFAULT);
			return;
		case ARINCPackage.MAJOR_FRAME__MIF:
			getMif().clear();
			return;
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			setSystem((ArincSystem) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.MAJOR_FRAME__MIF_DURATION:
			return mifDuration != MIF_DURATION_EDEFAULT;
		case ARINCPackage.MAJOR_FRAME__MIF:
			return mif != null && !mif.isEmpty();
		case ARINCPackage.MAJOR_FRAME__SYSTEM:
			return getSystem() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mifDuration: ");
		result.append(mifDuration);
		result.append(')');
		return result.toString();
	}

} // MajorFrameImpl
