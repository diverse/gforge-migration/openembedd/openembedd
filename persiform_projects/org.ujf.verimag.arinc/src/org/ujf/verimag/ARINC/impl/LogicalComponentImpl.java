/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.PhysicalComponent;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Logical Component</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.LogicalComponentImpl#getPhysicalComponent <em>Physical Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class LogicalComponentImpl extends NamedElementImpl implements LogicalComponent
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LogicalComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.LOGICAL_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PhysicalComponent getPhysicalComponent()
	{
		if (eContainerFeatureID != ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT)
			return null;
		return (PhysicalComponent) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetPhysicalComponent(PhysicalComponent newPhysicalComponent, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newPhysicalComponent,
			ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPhysicalComponent(PhysicalComponent newPhysicalComponent)
	{
		if (newPhysicalComponent != eInternalContainer()
				|| (eContainerFeatureID != ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT && newPhysicalComponent != null))
		{
			if (EcoreUtil.isAncestor(this, newPhysicalComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPhysicalComponent != null)
				msgs = ((InternalEObject) newPhysicalComponent).eInverseAdd(this,
					ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT, PhysicalComponent.class, msgs);
			msgs = basicSetPhysicalComponent(newPhysicalComponent, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT,
				newPhysicalComponent, newPhysicalComponent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPhysicalComponent((PhysicalComponent) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			return basicSetPhysicalComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.PHYSICAL_COMPONENT__LOGICAL_COMPONENT,
				PhysicalComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			return getPhysicalComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			setPhysicalComponent((PhysicalComponent) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			setPhysicalComponent((PhysicalComponent) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.LOGICAL_COMPONENT__PHYSICAL_COMPONENT:
			return getPhysicalComponent() != null;
		}
		return super.eIsSet(featureID);
	}

} // LogicalComponentImpl
