/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.ujf.verimag.ARINC.ARINCFactory;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.AperiodicPhysicalComponent;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.MajorFrame;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.Task;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class ARINCFactoryImpl extends EFactoryImpl implements ARINCFactory
{
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static ARINCFactory init()
	{
		try
		{
			ARINCFactory theARINCFactory = (ARINCFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://verimag.ujf.org/ARINC");
			if (theARINCFactory != null)
			{
				return theARINCFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ARINCFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ARINCFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
		case ARINCPackage.MIF:
			return createMIF();
		case ARINCPackage.WINDOW:
			return createWindow();
		case ARINCPackage.PARTITION:
			return createPartition();
		case ARINCPackage.LOGICAL_COMPONENT:
			return createLogicalComponent();
		case ARINCPackage.ARINC_SYSTEM:
			return createArincSystem();
		case ARINCPackage.MAJOR_FRAME:
			return createMajorFrame();
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT:
			return createPeriodicPhysicalComponent();
		case ARINCPackage.TASK:
			return createTask();
		case ARINCPackage.APERIODIC_PHYSICAL_COMPONENT:
			return createAperiodicPhysicalComponent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MIF createMIF()
	{
		MIFImpl mif = new MIFImpl();
		return mif;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Window createWindow()
	{
		WindowImpl window = new WindowImpl();
		return window;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Partition createPartition()
	{
		PartitionImpl partition = new PartitionImpl();
		return partition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LogicalComponent createLogicalComponent()
	{
		LogicalComponentImpl logicalComponent = new LogicalComponentImpl();
		return logicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ArincSystem createArincSystem()
	{
		ArincSystemImpl arincSystem = new ArincSystemImpl();
		return arincSystem;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MajorFrame createMajorFrame()
	{
		MajorFrameImpl majorFrame = new MajorFrameImpl();
		return majorFrame;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PeriodicPhysicalComponent createPeriodicPhysicalComponent()
	{
		PeriodicPhysicalComponentImpl periodicPhysicalComponent = new PeriodicPhysicalComponentImpl();
		return periodicPhysicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Task createTask()
	{
		TaskImpl task = new TaskImpl();
		return task;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AperiodicPhysicalComponent createAperiodicPhysicalComponent()
	{
		AperiodicPhysicalComponentImpl aperiodicPhysicalComponent = new AperiodicPhysicalComponentImpl();
		return aperiodicPhysicalComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ARINCPackage getARINCPackage()
	{
		return (ARINCPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ARINCPackage getPackage()
	{
		return ARINCPackage.eINSTANCE;
	}

} // ARINCFactoryImpl
