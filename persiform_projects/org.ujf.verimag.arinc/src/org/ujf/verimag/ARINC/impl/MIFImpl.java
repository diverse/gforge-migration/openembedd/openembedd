/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>MIF</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.MIFImpl#getWindow <em>Window</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.MIFImpl#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class MIFImpl extends NamedElementImpl implements MIF
{
	/**
	 * The cached value of the '{@link #getWindow() <em>Window</em>}' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getWindow()
	 * @generated
	 * @ordered
	 */
	protected EList<Window>	window;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected MIFImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.MIF;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Window> getWindow()
	{
		if (window == null)
		{
			window = new EObjectContainmentWithInverseEList<Window>(Window.class, this, ARINCPackage.MIF__WINDOW,
				ARINCPackage.WINDOW__MIF);
		}
		return window;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ArincSystem getSystem()
	{
		if (eContainerFeatureID != ARINCPackage.MIF__SYSTEM)
			return null;
		return (ArincSystem) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSystem(ArincSystem newSystem, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSystem, ARINCPackage.MIF__SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSystem(ArincSystem newSystem)
	{
		if (newSystem != eInternalContainer() || (eContainerFeatureID != ARINCPackage.MIF__SYSTEM && newSystem != null))
		{
			if (EcoreUtil.isAncestor(this, newSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSystem != null)
				msgs = ((InternalEObject) newSystem).eInverseAdd(this, ARINCPackage.ARINC_SYSTEM__MIF,
					ArincSystem.class, msgs);
			msgs = basicSetSystem(newSystem, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.MIF__SYSTEM, newSystem, newSystem));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getWindow()).basicAdd(otherEnd, msgs);
		case ARINCPackage.MIF__SYSTEM:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSystem((ArincSystem) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			return ((InternalEList<?>) getWindow()).basicRemove(otherEnd, msgs);
		case ARINCPackage.MIF__SYSTEM:
			return basicSetSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.MIF__SYSTEM:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.ARINC_SYSTEM__MIF, ArincSystem.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			return getWindow();
		case ARINCPackage.MIF__SYSTEM:
			return getSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			getWindow().clear();
			getWindow().addAll((Collection<? extends Window>) newValue);
			return;
		case ARINCPackage.MIF__SYSTEM:
			setSystem((ArincSystem) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			getWindow().clear();
			return;
		case ARINCPackage.MIF__SYSTEM:
			setSystem((ArincSystem) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.MIF__WINDOW:
			return window != null && !window.isEmpty();
		case ARINCPackage.MIF__SYSTEM:
			return getSystem() != null;
		}
		return super.eIsSet(featureID);
	}

} // MIFImpl
