/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.ujf.verimag.ARINC.ARINCFactory;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.AperiodicPhysicalComponent;
import org.ujf.verimag.ARINC.ArincSystem;
import org.ujf.verimag.ARINC.LogicalComponent;
import org.ujf.verimag.ARINC.MajorFrame;
import org.ujf.verimag.ARINC.NamedElement;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.PhysicalComponent;
import org.ujf.verimag.ARINC.Task;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class ARINCPackageImpl extends EPackageImpl implements ARINCPackage
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	mifEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	namedElementEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	windowEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	partitionEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	physicalComponentEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	logicalComponentEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	arincSystemEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	majorFrameEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	periodicPhysicalComponentEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	taskEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	aperiodicPhysicalComponentEClass	= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with {@link org.eclipse.emf.ecore.EPackage.Registry
	 * EPackage.Registry} by the package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory method {@link #init init()}, which also
	 * performs initialization of the package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.ujf.verimag.ARINC.ARINCPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ARINCPackageImpl()
	{
		super(eNS_URI, ARINCFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * Simple dependencies are satisfied by calling this method on all dependent packages before doing anything else.
	 * This method drives initialization for interdependent packages directly, in parallel with this package, itself.
	 * <p>
	 * Of this package and its interdependencies, all packages which have not yet been registered by their URI values
	 * are first created and registered. The packages are then initialized in two steps: meta-model objects for all of
	 * the packages are created before any are initialized, since one package's meta-model objects may refer to those of
	 * another.
	 * <p>
	 * Invocation of this method will not affect any packages that have already been initialized. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ARINCPackage init()
	{
		if (isInited)
			return (ARINCPackage) EPackage.Registry.INSTANCE.getEPackage(ARINCPackage.eNS_URI);

		// Obtain or create and register package
		ARINCPackageImpl theARINCPackage = (ARINCPackageImpl) (EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof ARINCPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(eNS_URI)
				: new ARINCPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theARINCPackage.createPackageContents();

		// Initialize created meta-data
		theARINCPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theARINCPackage.freeze();

		return theARINCPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getMIF()
	{
		return mifEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMIF_Window()
	{
		return (EReference) mifEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMIF_System()
	{
		return (EReference) mifEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getNamedElement()
	{
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNamedElement_Name()
	{
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getWindow()
	{
		return windowEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getWindow_Partition()
	{
		return (EReference) windowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getWindow_Duration()
	{
		return (EAttribute) windowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getWindow_Mif()
	{
		return (EReference) windowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getPartition()
	{
		return partitionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPartition_PhysicalComponent()
	{
		return (EReference) partitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPartition_System()
	{
		return (EReference) partitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getPhysicalComponent()
	{
		return physicalComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getPhysicalComponent_Priority()
	{
		return (EAttribute) physicalComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPhysicalComponent_LogicalComponent()
	{
		return (EReference) physicalComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPhysicalComponent_Partition()
	{
		return (EReference) physicalComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getLogicalComponent()
	{
		return logicalComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLogicalComponent_PhysicalComponent()
	{
		return (EReference) logicalComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getArincSystem()
	{
		return arincSystemEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArincSystem_MajorFrame()
	{
		return (EReference) arincSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArincSystem_Mif()
	{
		return (EReference) arincSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArincSystem_Partition()
	{
		return (EReference) arincSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getMajorFrame()
	{
		return majorFrameEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getMajorFrame_MifDuration()
	{
		return (EAttribute) majorFrameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMajorFrame_Mif()
	{
		return (EReference) majorFrameEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getMajorFrame_System()
	{
		return (EReference) majorFrameEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getPeriodicPhysicalComponent()
	{
		return periodicPhysicalComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getPeriodicPhysicalComponent_NMifPeriod()
	{
		return (EAttribute) periodicPhysicalComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getPeriodicPhysicalComponent_Deadline()
	{
		return (EAttribute) periodicPhysicalComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getPeriodicPhysicalComponent_FunctionalChain()
	{
		return (EReference) periodicPhysicalComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getTask()
	{
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getTask_Duration()
	{
		return (EAttribute) taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTask_LogicalComponent()
	{
		return (EReference) taskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTask_PeriodicPhysicalComponent()
	{
		return (EReference) taskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAperiodicPhysicalComponent()
	{
		return aperiodicPhysicalComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ARINCFactory getARINCFactory()
	{
		return (ARINCFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package. This method is guarded to have no affect on any invocation but
	 * its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents()
	{
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mifEClass = createEClass(MIF);
		createEReference(mifEClass, MIF__WINDOW);
		createEReference(mifEClass, MIF__SYSTEM);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		windowEClass = createEClass(WINDOW);
		createEReference(windowEClass, WINDOW__PARTITION);
		createEAttribute(windowEClass, WINDOW__DURATION);
		createEReference(windowEClass, WINDOW__MIF);

		partitionEClass = createEClass(PARTITION);
		createEReference(partitionEClass, PARTITION__PHYSICAL_COMPONENT);
		createEReference(partitionEClass, PARTITION__SYSTEM);

		physicalComponentEClass = createEClass(PHYSICAL_COMPONENT);
		createEAttribute(physicalComponentEClass, PHYSICAL_COMPONENT__PRIORITY);
		createEReference(physicalComponentEClass, PHYSICAL_COMPONENT__LOGICAL_COMPONENT);
		createEReference(physicalComponentEClass, PHYSICAL_COMPONENT__PARTITION);

		logicalComponentEClass = createEClass(LOGICAL_COMPONENT);
		createEReference(logicalComponentEClass, LOGICAL_COMPONENT__PHYSICAL_COMPONENT);

		arincSystemEClass = createEClass(ARINC_SYSTEM);
		createEReference(arincSystemEClass, ARINC_SYSTEM__MAJOR_FRAME);
		createEReference(arincSystemEClass, ARINC_SYSTEM__MIF);
		createEReference(arincSystemEClass, ARINC_SYSTEM__PARTITION);

		majorFrameEClass = createEClass(MAJOR_FRAME);
		createEAttribute(majorFrameEClass, MAJOR_FRAME__MIF_DURATION);
		createEReference(majorFrameEClass, MAJOR_FRAME__MIF);
		createEReference(majorFrameEClass, MAJOR_FRAME__SYSTEM);

		periodicPhysicalComponentEClass = createEClass(PERIODIC_PHYSICAL_COMPONENT);
		createEAttribute(periodicPhysicalComponentEClass, PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD);
		createEAttribute(periodicPhysicalComponentEClass, PERIODIC_PHYSICAL_COMPONENT__DEADLINE);
		createEReference(periodicPhysicalComponentEClass, PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN);

		taskEClass = createEClass(TASK);
		createEAttribute(taskEClass, TASK__DURATION);
		createEReference(taskEClass, TASK__LOGICAL_COMPONENT);
		createEReference(taskEClass, TASK__PERIODIC_PHYSICAL_COMPONENT);

		aperiodicPhysicalComponentEClass = createEClass(APERIODIC_PHYSICAL_COMPONENT);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model. This method is guarded to have no affect on any
	 * invocation but its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents()
	{
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mifEClass.getESuperTypes().add(this.getNamedElement());
		partitionEClass.getESuperTypes().add(this.getNamedElement());
		physicalComponentEClass.getESuperTypes().add(this.getNamedElement());
		logicalComponentEClass.getESuperTypes().add(this.getNamedElement());
		arincSystemEClass.getESuperTypes().add(this.getNamedElement());
		periodicPhysicalComponentEClass.getESuperTypes().add(this.getPhysicalComponent());
		taskEClass.getESuperTypes().add(this.getNamedElement());
		aperiodicPhysicalComponentEClass.getESuperTypes().add(this.getPhysicalComponent());

		// Initialize classes and features; add operations and parameters
		initEClass(mifEClass, org.ujf.verimag.ARINC.MIF.class, "MIF", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMIF_Window(), this.getWindow(), this.getWindow_Mif(), "window", null, 1, -1,
			org.ujf.verimag.ARINC.MIF.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMIF_System(), this.getArincSystem(), this.getArincSystem_Mif(), "system", null, 1, 1,
			org.ujf.verimag.ARINC.MIF.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(windowEClass, Window.class, "Window", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWindow_Partition(), this.getPartition(), null, "partition", null, 0, 1, Window.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);
		initEAttribute(getWindow_Duration(), ecorePackage.getEInt(), "duration", null, 1, 1, Window.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getWindow_Mif(), this.getMIF(), this.getMIF_Window(), "mif", null, 1, 1, Window.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);

		initEClass(partitionEClass, Partition.class, "Partition", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPartition_PhysicalComponent(), this.getPhysicalComponent(), this
				.getPhysicalComponent_Partition(), "physicalComponent", null, 0, -1, Partition.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED,
			!IS_ORDERED);
		initEReference(getPartition_System(), this.getArincSystem(), this.getArincSystem_Partition(), "system", null,
			1, 1, Partition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(physicalComponentEClass, PhysicalComponent.class, "PhysicalComponent", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhysicalComponent_Priority(), ecorePackage.getEInt(), "priority", null, 1, 1,
			PhysicalComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);
		initEReference(getPhysicalComponent_LogicalComponent(), this.getLogicalComponent(), this
				.getLogicalComponent_PhysicalComponent(), "logicalComponent", null, 0, -1, PhysicalComponent.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalComponent_Partition(), this.getPartition(), this.getPartition_PhysicalComponent(),
			"partition", null, 1, 1, PhysicalComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(logicalComponentEClass, LogicalComponent.class, "LogicalComponent", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLogicalComponent_PhysicalComponent(), this.getPhysicalComponent(), this
				.getPhysicalComponent_LogicalComponent(), "physicalComponent", null, 1, 1, LogicalComponent.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);

		initEClass(arincSystemEClass, ArincSystem.class, "ArincSystem", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArincSystem_MajorFrame(), this.getMajorFrame(), this.getMajorFrame_System(), "majorFrame",
			null, 1, 1, ArincSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getArincSystem_Mif(), this.getMIF(), this.getMIF_System(), "mif", null, 1, -1,
			ArincSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getArincSystem_Partition(), this.getPartition(), this.getPartition_System(), "partition", null,
			1, -1, ArincSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(majorFrameEClass, MajorFrame.class, "MajorFrame", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMajorFrame_MifDuration(), ecorePackage.getEInt(), "mifDuration", null, 1, 1,
			MajorFrame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);
		initEReference(getMajorFrame_Mif(), this.getMIF(), null, "mif", null, 1, -1, MajorFrame.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getMajorFrame_System(), this.getArincSystem(), this.getArincSystem_MajorFrame(), "system", null,
			1, 1, MajorFrame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(periodicPhysicalComponentEClass, PeriodicPhysicalComponent.class, "PeriodicPhysicalComponent",
			!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPeriodicPhysicalComponent_NMifPeriod(), ecorePackage.getEInt(), "nMifPeriod", null, 1, 1,
			PeriodicPhysicalComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getPeriodicPhysicalComponent_Deadline(), ecorePackage.getEInt(), "deadline", null, 1, 1,
			PeriodicPhysicalComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getPeriodicPhysicalComponent_FunctionalChain(), this.getTask(), this
				.getTask_PeriodicPhysicalComponent(), "functionalChain", null, 1, -1, PeriodicPhysicalComponent.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTask_Duration(), ecorePackage.getEInt(), "duration", null, 1, 1, Task.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTask_LogicalComponent(), this.getLogicalComponent(), null, "logicalComponent", null, 1, 1,
			Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
			!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTask_PeriodicPhysicalComponent(), this.getPeriodicPhysicalComponent(), this
				.getPeriodicPhysicalComponent_FunctionalChain(), "periodicPhysicalComponent", null, 1, 1, Task.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE,
			!IS_DERIVED, !IS_ORDERED);

		initEClass(aperiodicPhysicalComponentEClass, AperiodicPhysicalComponent.class, "AperiodicPhysicalComponent",
			!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} // ARINCPackageImpl
