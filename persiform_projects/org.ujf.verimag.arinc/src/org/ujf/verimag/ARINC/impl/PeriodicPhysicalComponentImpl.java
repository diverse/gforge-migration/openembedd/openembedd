/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.PeriodicPhysicalComponent;
import org.ujf.verimag.ARINC.Task;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Periodic Physical Component</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl#getNMifPeriod <em>NMif Period</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl#getDeadline <em>Deadline</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl#getFunctionalChain <em>Functional Chain</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class PeriodicPhysicalComponentImpl extends PhysicalComponentImpl implements PeriodicPhysicalComponent
{
	/**
	 * The default value of the '{@link #getNMifPeriod() <em>NMif Period</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNMifPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final int	NMIF_PERIOD_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getNMifPeriod() <em>NMif Period</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNMifPeriod()
	 * @generated
	 * @ordered
	 */
	protected int				nMifPeriod				= NMIF_PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeadline() <em>Deadline</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected static final int	DEADLINE_EDEFAULT		= 0;

	/**
	 * The cached value of the '{@link #getDeadline() <em>Deadline</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected int				deadline				= DEADLINE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFunctionalChain() <em>Functional Chain</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFunctionalChain()
	 * @generated
	 * @ordered
	 */
	protected EList<Task>		functionalChain;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PeriodicPhysicalComponentImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.PERIODIC_PHYSICAL_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getNMifPeriod()
	{
		return nMifPeriod;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNMifPeriod(int newNMifPeriod)
	{
		int oldNMifPeriod = nMifPeriod;
		nMifPeriod = newNMifPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
				ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD, oldNMifPeriod, nMifPeriod));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getDeadline()
	{
		return deadline;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeadline(int newDeadline)
	{
		int oldDeadline = deadline;
		deadline = newDeadline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__DEADLINE,
				oldDeadline, deadline));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Task> getFunctionalChain()
	{
		if (functionalChain == null)
		{
			functionalChain = new EObjectContainmentWithInverseEList<Task>(Task.class, this,
				ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN,
				ARINCPackage.TASK__PERIODIC_PHYSICAL_COMPONENT);
		}
		return functionalChain;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getFunctionalChain()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			return ((InternalEList<?>) getFunctionalChain()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD:
			return new Integer(getNMifPeriod());
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__DEADLINE:
			return new Integer(getDeadline());
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			return getFunctionalChain();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD:
			setNMifPeriod(((Integer) newValue).intValue());
			return;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__DEADLINE:
			setDeadline(((Integer) newValue).intValue());
			return;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			getFunctionalChain().clear();
			getFunctionalChain().addAll((Collection<? extends Task>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD:
			setNMifPeriod(NMIF_PERIOD_EDEFAULT);
			return;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__DEADLINE:
			setDeadline(DEADLINE_EDEFAULT);
			return;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			getFunctionalChain().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD:
			return nMifPeriod != NMIF_PERIOD_EDEFAULT;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__DEADLINE:
			return deadline != DEADLINE_EDEFAULT;
		case ARINCPackage.PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN:
			return functionalChain != null && !functionalChain.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nMifPeriod: ");
		result.append(nMifPeriod);
		result.append(", deadline: ");
		result.append(deadline);
		result.append(')');
		return result.toString();
	}

} // PeriodicPhysicalComponentImpl
