/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.ARINC.ARINCPackage;
import org.ujf.verimag.ARINC.MIF;
import org.ujf.verimag.ARINC.Partition;
import org.ujf.verimag.ARINC.Window;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Window</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.impl.WindowImpl#getPartition <em>Partition</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.WindowImpl#getDuration <em>Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.impl.WindowImpl#getMif <em>Mif</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class WindowImpl extends EObjectImpl implements Window
{
	/**
	 * The cached value of the '{@link #getPartition() <em>Partition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPartition()
	 * @generated
	 * @ordered
	 */
	protected Partition			partition;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final int	DURATION_EDEFAULT	= 0;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected int				duration			= DURATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected WindowImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ARINCPackage.Literals.WINDOW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Partition getPartition()
	{
		if (partition != null && partition.eIsProxy())
		{
			InternalEObject oldPartition = (InternalEObject) partition;
			partition = (Partition) eResolveProxy(oldPartition);
			if (partition != oldPartition)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ARINCPackage.WINDOW__PARTITION,
						oldPartition, partition));
			}
		}
		return partition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Partition basicGetPartition()
	{
		return partition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPartition(Partition newPartition)
	{
		Partition oldPartition = partition;
		partition = newPartition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.WINDOW__PARTITION, oldPartition,
				partition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getDuration()
	{
		return duration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDuration(int newDuration)
	{
		int oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.WINDOW__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MIF getMif()
	{
		if (eContainerFeatureID != ARINCPackage.WINDOW__MIF)
			return null;
		return (MIF) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetMif(MIF newMif, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newMif, ARINCPackage.WINDOW__MIF, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setMif(MIF newMif)
	{
		if (newMif != eInternalContainer() || (eContainerFeatureID != ARINCPackage.WINDOW__MIF && newMif != null))
		{
			if (EcoreUtil.isAncestor(this, newMif))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMif != null)
				msgs = ((InternalEObject) newMif).eInverseAdd(this, ARINCPackage.MIF__WINDOW, MIF.class, msgs);
			msgs = basicSetMif(newMif, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ARINCPackage.WINDOW__MIF, newMif, newMif));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__MIF:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetMif((MIF) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__MIF:
			return basicSetMif(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case ARINCPackage.WINDOW__MIF:
			return eInternalContainer().eInverseRemove(this, ARINCPackage.MIF__WINDOW, MIF.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__PARTITION:
			if (resolve)
				return getPartition();
			return basicGetPartition();
		case ARINCPackage.WINDOW__DURATION:
			return new Integer(getDuration());
		case ARINCPackage.WINDOW__MIF:
			return getMif();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__PARTITION:
			setPartition((Partition) newValue);
			return;
		case ARINCPackage.WINDOW__DURATION:
			setDuration(((Integer) newValue).intValue());
			return;
		case ARINCPackage.WINDOW__MIF:
			setMif((MIF) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__PARTITION:
			setPartition((Partition) null);
			return;
		case ARINCPackage.WINDOW__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case ARINCPackage.WINDOW__MIF:
			setMif((MIF) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case ARINCPackage.WINDOW__PARTITION:
			return partition != null;
		case ARINCPackage.WINDOW__DURATION:
			return duration != DURATION_EDEFAULT;
		case ARINCPackage.WINDOW__MIF:
			return getMif() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} // WindowImpl
