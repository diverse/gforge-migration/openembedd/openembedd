/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.ARINC.ARINCFactory
 * @model kind="package"
 * @generated
 */
public interface ARINCPackage extends EPackage
{
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNAME											= "ARINC";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_URI											= "http://verimag.ujf.org/ARINC";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String			eNS_PREFIX										= "ARINC";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	ARINCPackage	eINSTANCE										= org.ujf.verimag.ARINC.impl.ARINCPackageImpl
																			.init();

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.NamedElementImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getNamedElement()
	 * @generated
	 */
	int				NAMED_ELEMENT									= 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				NAMED_ELEMENT__NAME								= 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				NAMED_ELEMENT_FEATURE_COUNT						= 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.MIFImpl <em>MIF</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.MIFImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getMIF()
	 * @generated
	 */
	int				MIF												= 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MIF__NAME										= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Window</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MIF__WINDOW										= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>System</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MIF__SYSTEM										= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>MIF</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MIF_FEATURE_COUNT								= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.WindowImpl <em>Window</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.WindowImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getWindow()
	 * @generated
	 */
	int				WINDOW											= 2;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				WINDOW__PARTITION								= 0;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				WINDOW__DURATION								= 1;

	/**
	 * The feature id for the '<em><b>Mif</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				WINDOW__MIF										= 2;

	/**
	 * The number of structural features of the '<em>Window</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				WINDOW_FEATURE_COUNT							= 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.PartitionImpl <em>Partition</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.PartitionImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPartition()
	 * @generated
	 */
	int				PARTITION										= 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARTITION__NAME									= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Physical Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARTITION__PHYSICAL_COMPONENT					= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>System</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARTITION__SYSTEM								= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Partition</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PARTITION_FEATURE_COUNT							= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.PhysicalComponentImpl <em>Physical Component</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.PhysicalComponentImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPhysicalComponent()
	 * @generated
	 */
	int				PHYSICAL_COMPONENT								= 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PHYSICAL_COMPONENT__NAME						= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PHYSICAL_COMPONENT__PRIORITY					= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Logical Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PHYSICAL_COMPONENT__LOGICAL_COMPONENT			= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PHYSICAL_COMPONENT__PARTITION					= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Physical Component</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PHYSICAL_COMPONENT_FEATURE_COUNT				= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.LogicalComponentImpl <em>Logical Component</em>}'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.LogicalComponentImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getLogicalComponent()
	 * @generated
	 */
	int				LOGICAL_COMPONENT								= 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				LOGICAL_COMPONENT__NAME							= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Physical Component</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				LOGICAL_COMPONENT__PHYSICAL_COMPONENT			= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Logical Component</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				LOGICAL_COMPONENT_FEATURE_COUNT					= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.ArincSystemImpl <em>Arinc System</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.ArincSystemImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getArincSystem()
	 * @generated
	 */
	int				ARINC_SYSTEM									= 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARINC_SYSTEM__NAME								= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Major Frame</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARINC_SYSTEM__MAJOR_FRAME						= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mif</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARINC_SYSTEM__MIF								= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARINC_SYSTEM__PARTITION							= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Arinc System</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				ARINC_SYSTEM_FEATURE_COUNT						= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.MajorFrameImpl <em>Major Frame</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.MajorFrameImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getMajorFrame()
	 * @generated
	 */
	int				MAJOR_FRAME										= 7;

	/**
	 * The feature id for the '<em><b>Mif Duration</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MAJOR_FRAME__MIF_DURATION						= 0;

	/**
	 * The feature id for the '<em><b>Mif</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MAJOR_FRAME__MIF								= 1;

	/**
	 * The feature id for the '<em><b>System</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MAJOR_FRAME__SYSTEM								= 2;

	/**
	 * The number of structural features of the '<em>Major Frame</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				MAJOR_FRAME_FEATURE_COUNT						= 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl
	 * <em>Periodic Physical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPeriodicPhysicalComponent()
	 * @generated
	 */
	int				PERIODIC_PHYSICAL_COMPONENT						= 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__NAME				= PHYSICAL_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__PRIORITY			= PHYSICAL_COMPONENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Logical Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__LOGICAL_COMPONENT	= PHYSICAL_COMPONENT__LOGICAL_COMPONENT;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__PARTITION			= PHYSICAL_COMPONENT__PARTITION;

	/**
	 * The feature id for the '<em><b>NMif Period</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD		= PHYSICAL_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__DEADLINE			= PHYSICAL_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Functional Chain</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN	= PHYSICAL_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Periodic Physical Component</em>' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				PERIODIC_PHYSICAL_COMPONENT_FEATURE_COUNT		= PHYSICAL_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.TaskImpl <em>Task</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.TaskImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getTask()
	 * @generated
	 */
	int				TASK											= 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				TASK__NAME										= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				TASK__DURATION									= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Logical Component</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				TASK__LOGICAL_COMPONENT							= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Periodic Physical Component</b></em>' container reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				TASK__PERIODIC_PHYSICAL_COMPONENT				= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Task</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				TASK_FEATURE_COUNT								= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.ARINC.impl.AperiodicPhysicalComponentImpl
	 * <em>Aperiodic Physical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.ARINC.impl.AperiodicPhysicalComponentImpl
	 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getAperiodicPhysicalComponent()
	 * @generated
	 */
	int				APERIODIC_PHYSICAL_COMPONENT					= 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APERIODIC_PHYSICAL_COMPONENT__NAME				= PHYSICAL_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APERIODIC_PHYSICAL_COMPONENT__PRIORITY			= PHYSICAL_COMPONENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Logical Component</b></em>' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APERIODIC_PHYSICAL_COMPONENT__LOGICAL_COMPONENT	= PHYSICAL_COMPONENT__LOGICAL_COMPONENT;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APERIODIC_PHYSICAL_COMPONENT__PARTITION			= PHYSICAL_COMPONENT__PARTITION;

	/**
	 * The number of structural features of the '<em>Aperiodic Physical Component</em>' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int				APERIODIC_PHYSICAL_COMPONENT_FEATURE_COUNT		= PHYSICAL_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.MIF <em>MIF</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>MIF</em>'.
	 * @see org.ujf.verimag.ARINC.MIF
	 * @generated
	 */
	EClass getMIF();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.ARINC.MIF#getWindow
	 * <em>Window</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Window</em>'.
	 * @see org.ujf.verimag.ARINC.MIF#getWindow()
	 * @see #getMIF()
	 * @generated
	 */
	EReference getMIF_Window();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.ARINC.MIF#getSystem <em>System</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>System</em>'.
	 * @see org.ujf.verimag.ARINC.MIF#getSystem()
	 * @see #getMIF()
	 * @generated
	 */
	EReference getMIF_System();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.NamedElement <em>Named Element</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see org.ujf.verimag.ARINC.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ujf.verimag.ARINC.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.Window <em>Window</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Window</em>'.
	 * @see org.ujf.verimag.ARINC.Window
	 * @generated
	 */
	EClass getWindow();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.ARINC.Window#getPartition <em>Partition</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Partition</em>'.
	 * @see org.ujf.verimag.ARINC.Window#getPartition()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_Partition();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.Window#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.ujf.verimag.ARINC.Window#getDuration()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_Duration();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.ARINC.Window#getMif <em>Mif</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Mif</em>'.
	 * @see org.ujf.verimag.ARINC.Window#getMif()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_Mif();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.Partition <em>Partition</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Partition</em>'.
	 * @see org.ujf.verimag.ARINC.Partition
	 * @generated
	 */
	EClass getPartition();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.ARINC.Partition#getPhysicalComponent <em>Physical Component</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.Partition#getPhysicalComponent()
	 * @see #getPartition()
	 * @generated
	 */
	EReference getPartition_PhysicalComponent();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.ARINC.Partition#getSystem
	 * <em>System</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>System</em>'.
	 * @see org.ujf.verimag.ARINC.Partition#getSystem()
	 * @see #getPartition()
	 * @generated
	 */
	EReference getPartition_System();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.PhysicalComponent <em>Physical Component</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.PhysicalComponent
	 * @generated
	 */
	EClass getPhysicalComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.PhysicalComponent#getPriority
	 * <em>Priority</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see org.ujf.verimag.ARINC.PhysicalComponent#getPriority()
	 * @see #getPhysicalComponent()
	 * @generated
	 */
	EAttribute getPhysicalComponent_Priority();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.ARINC.PhysicalComponent#getLogicalComponent <em>Logical Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Logical Component</em>'.
	 * @see org.ujf.verimag.ARINC.PhysicalComponent#getLogicalComponent()
	 * @see #getPhysicalComponent()
	 * @generated
	 */
	EReference getPhysicalComponent_LogicalComponent();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.ARINC.PhysicalComponent#getPartition
	 * <em>Partition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Partition</em>'.
	 * @see org.ujf.verimag.ARINC.PhysicalComponent#getPartition()
	 * @see #getPhysicalComponent()
	 * @generated
	 */
	EReference getPhysicalComponent_Partition();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.LogicalComponent <em>Logical Component</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Logical Component</em>'.
	 * @see org.ujf.verimag.ARINC.LogicalComponent
	 * @generated
	 */
	EClass getLogicalComponent();

	/**
	 * Returns the meta object for the container reference '
	 * {@link org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent <em>Physical Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent()
	 * @see #getLogicalComponent()
	 * @generated
	 */
	EReference getLogicalComponent_PhysicalComponent();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.ArincSystem <em>Arinc System</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Arinc System</em>'.
	 * @see org.ujf.verimag.ARINC.ArincSystem
	 * @generated
	 */
	EClass getArincSystem();

	/**
	 * Returns the meta object for the containment reference '{@link org.ujf.verimag.ARINC.ArincSystem#getMajorFrame
	 * <em>Major Frame</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Major Frame</em>'.
	 * @see org.ujf.verimag.ARINC.ArincSystem#getMajorFrame()
	 * @see #getArincSystem()
	 * @generated
	 */
	EReference getArincSystem_MajorFrame();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.ARINC.ArincSystem#getMif
	 * <em>Mif</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Mif</em>'.
	 * @see org.ujf.verimag.ARINC.ArincSystem#getMif()
	 * @see #getArincSystem()
	 * @generated
	 */
	EReference getArincSystem_Mif();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.ARINC.ArincSystem#getPartition <em>Partition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Partition</em>'.
	 * @see org.ujf.verimag.ARINC.ArincSystem#getPartition()
	 * @see #getArincSystem()
	 * @generated
	 */
	EReference getArincSystem_Partition();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.MajorFrame <em>Major Frame</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Major Frame</em>'.
	 * @see org.ujf.verimag.ARINC.MajorFrame
	 * @generated
	 */
	EClass getMajorFrame();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.MajorFrame#getMifDuration
	 * <em>Mif Duration</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Mif Duration</em>'.
	 * @see org.ujf.verimag.ARINC.MajorFrame#getMifDuration()
	 * @see #getMajorFrame()
	 * @generated
	 */
	EAttribute getMajorFrame_MifDuration();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.ARINC.MajorFrame#getMif <em>Mif</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Mif</em>'.
	 * @see org.ujf.verimag.ARINC.MajorFrame#getMif()
	 * @see #getMajorFrame()
	 * @generated
	 */
	EReference getMajorFrame_Mif();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.ARINC.MajorFrame#getSystem
	 * <em>System</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>System</em>'.
	 * @see org.ujf.verimag.ARINC.MajorFrame#getSystem()
	 * @see #getMajorFrame()
	 * @generated
	 */
	EReference getMajorFrame_System();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent
	 * <em>Periodic Physical Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Periodic Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent
	 * @generated
	 */
	EClass getPeriodicPhysicalComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getNMifPeriod
	 * <em>NMif Period</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>NMif Period</em>'.
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getNMifPeriod()
	 * @see #getPeriodicPhysicalComponent()
	 * @generated
	 */
	EAttribute getPeriodicPhysicalComponent_NMifPeriod();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getDeadline
	 * <em>Deadline</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getDeadline()
	 * @see #getPeriodicPhysicalComponent()
	 * @generated
	 */
	EAttribute getPeriodicPhysicalComponent_Deadline();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getFunctionalChain <em>Functional Chain</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Functional Chain</em>'.
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getFunctionalChain()
	 * @see #getPeriodicPhysicalComponent()
	 * @generated
	 */
	EReference getPeriodicPhysicalComponent_FunctionalChain();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.Task <em>Task</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Task</em>'.
	 * @see org.ujf.verimag.ARINC.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.ARINC.Task#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.ujf.verimag.ARINC.Task#getDuration()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Duration();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.ARINC.Task#getLogicalComponent
	 * <em>Logical Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Logical Component</em>'.
	 * @see org.ujf.verimag.ARINC.Task#getLogicalComponent()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_LogicalComponent();

	/**
	 * Returns the meta object for the container reference '
	 * {@link org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent <em>Periodic Physical Component</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Periodic Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_PeriodicPhysicalComponent();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.ARINC.AperiodicPhysicalComponent
	 * <em>Aperiodic Physical Component</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Aperiodic Physical Component</em>'.
	 * @see org.ujf.verimag.ARINC.AperiodicPhysicalComponent
	 * @generated
	 */
	EClass getAperiodicPhysicalComponent();

	/**
	 * Returns the factory that creates the instances of the model. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ARINCFactory getARINCFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.MIFImpl <em>MIF</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.MIFImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getMIF()
		 * @generated
		 */
		EClass		MIF												= eINSTANCE.getMIF();

		/**
		 * The meta object literal for the '<em><b>Window</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MIF__WINDOW										= eINSTANCE.getMIF_Window();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MIF__SYSTEM										= eINSTANCE.getMIF_System();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.NamedElementImpl <em>Named Element</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.NamedElementImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass		NAMED_ELEMENT									= eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NAMED_ELEMENT__NAME								= eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.WindowImpl <em>Window</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.WindowImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getWindow()
		 * @generated
		 */
		EClass		WINDOW											= eINSTANCE.getWindow();

		/**
		 * The meta object literal for the '<em><b>Partition</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	WINDOW__PARTITION								= eINSTANCE.getWindow_Partition();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	WINDOW__DURATION								= eINSTANCE.getWindow_Duration();

		/**
		 * The meta object literal for the '<em><b>Mif</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	WINDOW__MIF										= eINSTANCE.getWindow_Mif();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.PartitionImpl <em>Partition</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.PartitionImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPartition()
		 * @generated
		 */
		EClass		PARTITION										= eINSTANCE.getPartition();

		/**
		 * The meta object literal for the '<em><b>Physical Component</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PARTITION__PHYSICAL_COMPONENT					= eINSTANCE.getPartition_PhysicalComponent();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PARTITION__SYSTEM								= eINSTANCE.getPartition_System();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.PhysicalComponentImpl
		 * <em>Physical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.PhysicalComponentImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPhysicalComponent()
		 * @generated
		 */
		EClass		PHYSICAL_COMPONENT								= eINSTANCE.getPhysicalComponent();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	PHYSICAL_COMPONENT__PRIORITY					= eINSTANCE.getPhysicalComponent_Priority();

		/**
		 * The meta object literal for the '<em><b>Logical Component</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PHYSICAL_COMPONENT__LOGICAL_COMPONENT			= eINSTANCE.getPhysicalComponent_LogicalComponent();

		/**
		 * The meta object literal for the '<em><b>Partition</b></em>' container reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PHYSICAL_COMPONENT__PARTITION					= eINSTANCE.getPhysicalComponent_Partition();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.LogicalComponentImpl
		 * <em>Logical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.LogicalComponentImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getLogicalComponent()
		 * @generated
		 */
		EClass		LOGICAL_COMPONENT								= eINSTANCE.getLogicalComponent();

		/**
		 * The meta object literal for the '<em><b>Physical Component</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LOGICAL_COMPONENT__PHYSICAL_COMPONENT			= eINSTANCE.getLogicalComponent_PhysicalComponent();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.ArincSystemImpl <em>Arinc System</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.ArincSystemImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getArincSystem()
		 * @generated
		 */
		EClass		ARINC_SYSTEM									= eINSTANCE.getArincSystem();

		/**
		 * The meta object literal for the '<em><b>Major Frame</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARINC_SYSTEM__MAJOR_FRAME						= eINSTANCE.getArincSystem_MajorFrame();

		/**
		 * The meta object literal for the '<em><b>Mif</b></em>' containment reference list feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARINC_SYSTEM__MIF								= eINSTANCE.getArincSystem_Mif();

		/**
		 * The meta object literal for the '<em><b>Partition</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARINC_SYSTEM__PARTITION							= eINSTANCE.getArincSystem_Partition();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.MajorFrameImpl <em>Major Frame</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.MajorFrameImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getMajorFrame()
		 * @generated
		 */
		EClass		MAJOR_FRAME										= eINSTANCE.getMajorFrame();

		/**
		 * The meta object literal for the '<em><b>Mif Duration</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MAJOR_FRAME__MIF_DURATION						= eINSTANCE.getMajorFrame_MifDuration();

		/**
		 * The meta object literal for the '<em><b>Mif</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MAJOR_FRAME__MIF								= eINSTANCE.getMajorFrame_Mif();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MAJOR_FRAME__SYSTEM								= eINSTANCE.getMajorFrame_System();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl
		 * <em>Periodic Physical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.PeriodicPhysicalComponentImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getPeriodicPhysicalComponent()
		 * @generated
		 */
		EClass		PERIODIC_PHYSICAL_COMPONENT						= eINSTANCE.getPeriodicPhysicalComponent();

		/**
		 * The meta object literal for the '<em><b>NMif Period</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	PERIODIC_PHYSICAL_COMPONENT__NMIF_PERIOD		= eINSTANCE
																			.getPeriodicPhysicalComponent_NMifPeriod();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	PERIODIC_PHYSICAL_COMPONENT__DEADLINE			= eINSTANCE.getPeriodicPhysicalComponent_Deadline();

		/**
		 * The meta object literal for the '<em><b>Functional Chain</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	PERIODIC_PHYSICAL_COMPONENT__FUNCTIONAL_CHAIN	= eINSTANCE
																			.getPeriodicPhysicalComponent_FunctionalChain();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.TaskImpl <em>Task</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.TaskImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getTask()
		 * @generated
		 */
		EClass		TASK											= eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	TASK__DURATION									= eINSTANCE.getTask_Duration();

		/**
		 * The meta object literal for the '<em><b>Logical Component</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TASK__LOGICAL_COMPONENT							= eINSTANCE.getTask_LogicalComponent();

		/**
		 * The meta object literal for the '<em><b>Periodic Physical Component</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TASK__PERIODIC_PHYSICAL_COMPONENT				= eINSTANCE.getTask_PeriodicPhysicalComponent();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.ARINC.impl.AperiodicPhysicalComponentImpl
		 * <em>Aperiodic Physical Component</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.ARINC.impl.AperiodicPhysicalComponentImpl
		 * @see org.ujf.verimag.ARINC.impl.ARINCPackageImpl#getAperiodicPhysicalComponent()
		 * @generated
		 */
		EClass		APERIODIC_PHYSICAL_COMPONENT					= eINSTANCE.getAperiodicPhysicalComponent();

	}

} // ARINCPackage
