/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Logical Component</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent <em>Physical Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getLogicalComponent()
 * @model
 * @generated
 */
public interface LogicalComponent extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Physical Component</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.ARINC.PhysicalComponent#getLogicalComponent <em>Logical Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Component</em>' container reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Physical Component</em>' container reference.
	 * @see #setPhysicalComponent(PhysicalComponent)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getLogicalComponent_PhysicalComponent()
	 * @see org.ujf.verimag.ARINC.PhysicalComponent#getLogicalComponent
	 * @model opposite="logicalComponent" required="true" transient="false" ordered="false"
	 * @generated
	 */
	PhysicalComponent getPhysicalComponent();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent
	 * <em>Physical Component</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Physical Component</em>' container reference.
	 * @see #getPhysicalComponent()
	 * @generated
	 */
	void setPhysicalComponent(PhysicalComponent value);

} // LogicalComponent
