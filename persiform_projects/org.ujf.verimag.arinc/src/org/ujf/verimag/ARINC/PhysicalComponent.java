/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Physical Component</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.PhysicalComponent#getPriority <em>Priority</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.PhysicalComponent#getLogicalComponent <em>Logical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.PhysicalComponent#getPartition <em>Partition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getPhysicalComponent()
 * @model abstract="true"
 * @generated
 */
public interface PhysicalComponent extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see #setPriority(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPhysicalComponent_Priority()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getPriority();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.PhysicalComponent#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Priority</em>' attribute.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(int value);

	/**
	 * Returns the value of the '<em><b>Logical Component</b></em>' containment reference list. The list contents are of
	 * type {@link org.ujf.verimag.ARINC.LogicalComponent}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent <em>Physical Component</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Component</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Logical Component</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPhysicalComponent_LogicalComponent()
	 * @see org.ujf.verimag.ARINC.LogicalComponent#getPhysicalComponent
	 * @model opposite="physicalComponent" containment="true"
	 * @generated
	 */
	EList<LogicalComponent> getLogicalComponent();

	/**
	 * Returns the value of the '<em><b>Partition</b></em>' container reference. It is bidirectional and its opposite is
	 * '{@link org.ujf.verimag.ARINC.Partition#getPhysicalComponent <em>Physical Component</em>}'. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Partition</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Partition</em>' container reference.
	 * @see #setPartition(Partition)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getPhysicalComponent_Partition()
	 * @see org.ujf.verimag.ARINC.Partition#getPhysicalComponent
	 * @model opposite="physicalComponent" required="true" transient="false" ordered="false"
	 * @generated
	 */
	Partition getPartition();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.PhysicalComponent#getPartition <em>Partition</em>}' container
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Partition</em>' container reference.
	 * @see #getPartition()
	 * @generated
	 */
	void setPartition(Partition value);

} // PhysicalComponent
