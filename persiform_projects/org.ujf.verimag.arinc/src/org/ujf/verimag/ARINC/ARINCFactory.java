/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a create method for each non-abstract class of
 * the model. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage
 * @generated
 */
public interface ARINCFactory extends EFactory
{
	/**
	 * The singleton instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	ARINCFactory	eINSTANCE	= org.ujf.verimag.ARINC.impl.ARINCFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MIF</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MIF</em>'.
	 * @generated
	 */
	MIF createMIF();

	/**
	 * Returns a new object of class '<em>Window</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Window</em>'.
	 * @generated
	 */
	Window createWindow();

	/**
	 * Returns a new object of class '<em>Partition</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Partition</em>'.
	 * @generated
	 */
	Partition createPartition();

	/**
	 * Returns a new object of class '<em>Logical Component</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Logical Component</em>'.
	 * @generated
	 */
	LogicalComponent createLogicalComponent();

	/**
	 * Returns a new object of class '<em>Arinc System</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Arinc System</em>'.
	 * @generated
	 */
	ArincSystem createArincSystem();

	/**
	 * Returns a new object of class '<em>Major Frame</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Major Frame</em>'.
	 * @generated
	 */
	MajorFrame createMajorFrame();

	/**
	 * Returns a new object of class '<em>Periodic Physical Component</em>'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return a new object of class '<em>Periodic Physical Component</em>'.
	 * @generated
	 */
	PeriodicPhysicalComponent createPeriodicPhysicalComponent();

	/**
	 * Returns a new object of class '<em>Task</em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Task</em>'.
	 * @generated
	 */
	Task createTask();

	/**
	 * Returns a new object of class '<em>Aperiodic Physical Component</em>'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return a new object of class '<em>Aperiodic Physical Component</em>'.
	 * @generated
	 */
	AperiodicPhysicalComponent createAperiodicPhysicalComponent();

	/**
	 * Returns the package supported by this factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	ARINCPackage getARINCPackage();

} // ARINCFactory
