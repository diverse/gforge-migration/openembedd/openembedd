/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>MIF</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> self.window.duration->sum() = self.majorFrame.mifDuration <!-- end-model-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.MIF#getWindow <em>Window</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.MIF#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getMIF()
 * @model
 * @generated
 */
public interface MIF extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Window</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.ARINC.Window}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.Window#getMif <em>Mif</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Window</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Window</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getMIF_Window()
	 * @see org.ujf.verimag.ARINC.Window#getMif
	 * @model opposite="mif" containment="true" required="true"
	 * @generated
	 */
	EList<Window> getWindow();

	/**
	 * Returns the value of the '<em><b>System</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.ArincSystem#getMif <em>Mif</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>System</em>' container reference.
	 * @see #setSystem(ArincSystem)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getMIF_System()
	 * @see org.ujf.verimag.ARINC.ArincSystem#getMif
	 * @model opposite="mif" required="true" transient="false" ordered="false"
	 * @generated
	 */
	ArincSystem getSystem();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.MIF#getSystem <em>System</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>System</em>' container reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(ArincSystem value);

} // MIF
