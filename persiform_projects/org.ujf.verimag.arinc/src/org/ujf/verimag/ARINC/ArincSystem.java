/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Arinc System</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.ArincSystem#getMajorFrame <em>Major Frame</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.ArincSystem#getMif <em>Mif</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.ArincSystem#getPartition <em>Partition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getArincSystem()
 * @model
 * @generated
 */
public interface ArincSystem extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Major Frame</b></em>' containment reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.ARINC.MajorFrame#getSystem <em>System</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Major Frame</em>' containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Major Frame</em>' containment reference.
	 * @see #setMajorFrame(MajorFrame)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getArincSystem_MajorFrame()
	 * @see org.ujf.verimag.ARINC.MajorFrame#getSystem
	 * @model opposite="system" containment="true" required="true" ordered="false"
	 * @generated
	 */
	MajorFrame getMajorFrame();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.ArincSystem#getMajorFrame <em>Major Frame</em>}' containment
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Major Frame</em>' containment reference.
	 * @see #getMajorFrame()
	 * @generated
	 */
	void setMajorFrame(MajorFrame value);

	/**
	 * Returns the value of the '<em><b>Mif</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.ARINC.MIF}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.MIF#getSystem <em>System</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mif</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mif</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getArincSystem_Mif()
	 * @see org.ujf.verimag.ARINC.MIF#getSystem
	 * @model opposite="system" containment="true" required="true" ordered="false"
	 * @generated
	 */
	EList<MIF> getMif();

	/**
	 * Returns the value of the '<em><b>Partition</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.ARINC.Partition}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.Partition#getSystem <em>System</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition</em>' containment reference list isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Partition</em>' containment reference list.
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getArincSystem_Partition()
	 * @see org.ujf.verimag.ARINC.Partition#getSystem
	 * @model opposite="system" containment="true" required="true" ordered="false"
	 * @generated
	 */
	EList<Partition> getPartition();

} // ArincSystem
