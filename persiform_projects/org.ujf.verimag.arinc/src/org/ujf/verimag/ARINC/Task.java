/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Task</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.Task#getDuration <em>Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.Task#getLogicalComponent <em>Logical Component</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent <em>Periodic Physical Component</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getTask_Duration()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Task#getDuration <em>Duration</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Logical Component</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Component</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Logical Component</em>' reference.
	 * @see #setLogicalComponent(LogicalComponent)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getTask_LogicalComponent()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	LogicalComponent getLogicalComponent();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Task#getLogicalComponent <em>Logical Component</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Logical Component</em>' reference.
	 * @see #getLogicalComponent()
	 * @generated
	 */
	void setLogicalComponent(LogicalComponent value);

	/**
	 * Returns the value of the '<em><b>Periodic Physical Component</b></em>' container reference. It is bidirectional
	 * and its opposite is '{@link org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getFunctionalChain
	 * <em>Functional Chain</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Periodic Physical Component</em>' container reference isn't clear, there really should
	 * be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Periodic Physical Component</em>' container reference.
	 * @see #setPeriodicPhysicalComponent(PeriodicPhysicalComponent)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getTask_PeriodicPhysicalComponent()
	 * @see org.ujf.verimag.ARINC.PeriodicPhysicalComponent#getFunctionalChain
	 * @model opposite="functionalChain" required="true" transient="false" ordered="false"
	 * @generated
	 */
	PeriodicPhysicalComponent getPeriodicPhysicalComponent();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Task#getPeriodicPhysicalComponent
	 * <em>Periodic Physical Component</em>}' container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Periodic Physical Component</em>' container reference.
	 * @see #getPeriodicPhysicalComponent()
	 * @generated
	 */
	void setPeriodicPhysicalComponent(PeriodicPhysicalComponent value);

} // Task
