/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.ARINC;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Window</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.ARINC.Window#getPartition <em>Partition</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.Window#getDuration <em>Duration</em>}</li>
 * <li>{@link org.ujf.verimag.ARINC.Window#getMif <em>Mif</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.ARINC.ARINCPackage#getWindow()
 * @model
 * @generated
 */
public interface Window extends EObject
{
	/**
	 * Returns the value of the '<em><b>Partition</b></em>' reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Partition</em>' reference.
	 * @see #setPartition(Partition)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getWindow_Partition()
	 * @model ordered="false"
	 * @generated
	 */
	Partition getPartition();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Window#getPartition <em>Partition</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Partition</em>' reference.
	 * @see #getPartition()
	 * @generated
	 */
	void setPartition(Partition value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getWindow_Duration()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Window#getDuration <em>Duration</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Mif</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.ARINC.MIF#getWindow <em>Window</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mif</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Mif</em>' container reference.
	 * @see #setMif(MIF)
	 * @see org.ujf.verimag.ARINC.ARINCPackage#getWindow_Mif()
	 * @see org.ujf.verimag.ARINC.MIF#getWindow
	 * @model opposite="window" required="true" transient="false" ordered="false"
	 * @generated
	 */
	MIF getMif();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.ARINC.Window#getMif <em>Mif</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Mif</em>' container reference.
	 * @see #getMif()
	 * @generated
	 */
	void setMif(MIF value);

} // Window
