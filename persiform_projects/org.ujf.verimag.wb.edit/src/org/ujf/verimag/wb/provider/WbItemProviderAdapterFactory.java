/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.ujf.verimag.wb.util.WbAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers. The adapters generated by this
 * factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}. The adapters
 * also support Eclipse property sheets. Note that most of the adapters are shared among multiple instances. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class WbItemProviderAdapterFactory extends WbAdapterFactory implements ComposeableAdapterFactory,
		IChangeNotifier, IDisposable
{
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected ComposedAdapterFactory	parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected IChangeNotifier			changeNotifier	= new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected Collection<Object>		supportedTypes	= new ArrayList<Object>();

	/**
	 * This constructs an instance. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbItemProviderAdapterFactory()
	{
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Module} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModuleItemProvider	moduleItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Module}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createModuleAdapter()
	{
		if (moduleItemProvider == null)
		{
			moduleItemProvider = new ModuleItemProvider(this);
		}

		return moduleItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.DeclarationZone} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DeclarationZoneItemProvider	declarationZoneItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.DeclarationZone}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createDeclarationZoneAdapter()
	{
		if (declarationZoneItemProvider == null)
		{
			declarationZoneItemProvider = new DeclarationZoneItemProvider(this);
		}

		return declarationZoneItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Submodel} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SubmodelItemProvider	submodelItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Submodel}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSubmodelAdapter()
	{
		if (submodelItemProvider == null)
		{
			submodelItemProvider = new SubmodelItemProvider(this);
		}

		return submodelItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Allocate} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AllocateItemProvider	allocateItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Allocate}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createAllocateAdapter()
	{
		if (allocateItemProvider == null)
		{
			allocateItemProvider = new AllocateItemProvider(this);
		}

		return allocateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Service} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ServiceItemProvider	serviceItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Service}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createServiceAdapter()
	{
		if (serviceItemProvider == null)
		{
			serviceItemProvider = new ServiceItemProvider(this);
		}

		return serviceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.ResourcePool} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ResourcePoolItemProvider	resourcePoolItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.ResourcePool}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createResourcePoolAdapter()
	{
		if (resourcePoolItemProvider == null)
		{
			resourcePoolItemProvider = new ResourcePoolItemProvider(this);
		}

		return resourcePoolItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Arc} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ArcItemProvider	arcItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Arc}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createArcAdapter()
	{
		if (arcItemProvider == null)
		{
			arcItemProvider = new ArcItemProvider(this);
		}

		return arcItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.ResponseArc} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ResponseArcItemProvider	responseArcItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.ResponseArc}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createResponseArcAdapter()
	{
		if (responseArcItemProvider == null)
		{
			responseArcItemProvider = new ResponseArcItemProvider(this);
		}

		return responseArcItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.CategoryID} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CategoryIDItemProvider	categoryIDItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.CategoryID}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createCategoryIDAdapter()
	{
		if (categoryIDItemProvider == null)
		{
			categoryIDItemProvider = new CategoryIDItemProvider(this);
		}

		return categoryIDItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Variable} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected VariableItemProvider	variableItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Variable}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createVariableAdapter()
	{
		if (variableItemProvider == null)
		{
			variableItemProvider = new VariableItemProvider(this);
		}

		return variableItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.SubmodelRef} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SubmodelRefItemProvider	submodelRefItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.SubmodelRef}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSubmodelRefAdapter()
	{
		if (submodelRefItemProvider == null)
		{
			submodelRefItemProvider = new SubmodelRefItemProvider(this);
		}

		return submodelRefItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.ServiceRef} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ServiceRefItemProvider	serviceRefItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.ServiceRef}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createServiceRefAdapter()
	{
		if (serviceRefItemProvider == null)
		{
			serviceRefItemProvider = new ServiceRefItemProvider(this);
		}

		return serviceRefItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.AllocateRef} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AllocateRefItemProvider	allocateRefItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.AllocateRef}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createAllocateRefAdapter()
	{
		if (allocateRefItemProvider == null)
		{
			allocateRefItemProvider = new AllocateRefItemProvider(this);
		}

		return allocateRefItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Block} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected BlockItemProvider	blockItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Block}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createBlockAdapter()
	{
		if (blockItemProvider == null)
		{
			blockItemProvider = new BlockItemProvider(this);
		}

		return blockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Source} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SourceItemProvider	sourceItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Source}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSourceAdapter()
	{
		if (sourceItemProvider == null)
		{
			sourceItemProvider = new SourceItemProvider(this);
		}

		return sourceItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Sink} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SinkItemProvider	sinkItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Sink}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSinkAdapter()
	{
		if (sinkItemProvider == null)
		{
			sinkItemProvider = new SinkItemProvider(this);
		}

		return sinkItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Enter} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EnterItemProvider	enterItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Enter}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createEnterAdapter()
	{
		if (enterItemProvider == null)
		{
			enterItemProvider = new EnterItemProvider(this);
		}

		return enterItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Return} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ReturnItemProvider	returnItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Return}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createReturnAdapter()
	{
		if (returnItemProvider == null)
		{
			returnItemProvider = new ReturnItemProvider(this);
		}

		return returnItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Split} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SplitItemProvider	splitItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Split}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSplitAdapter()
	{
		if (splitItemProvider == null)
		{
			splitItemProvider = new SplitItemProvider(this);
		}

		return splitItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Fork} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ForkItemProvider	forkItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Fork}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createForkAdapter()
	{
		if (forkItemProvider == null)
		{
			forkItemProvider = new ForkItemProvider(this);
		}

		return forkItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Join} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected JoinItemProvider	joinItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Join}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createJoinAdapter()
	{
		if (joinItemProvider == null)
		{
			joinItemProvider = new JoinItemProvider(this);
		}

		return joinItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.User} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected UserItemProvider	userItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.User}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createUserAdapter()
	{
		if (userItemProvider == null)
		{
			userItemProvider = new UserItemProvider(this);
		}

		return userItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Delay} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DelayItemProvider	delayItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Delay}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createDelayAdapter()
	{
		if (delayItemProvider == null)
		{
			delayItemProvider = new DelayItemProvider(this);
		}

		return delayItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Release} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ReleaseItemProvider	releaseItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Release}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createReleaseAdapter()
	{
		if (releaseItemProvider == null)
		{
			releaseItemProvider = new ReleaseItemProvider(this);
		}

		return releaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Interrupt} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InterruptItemProvider	interruptItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Interrupt}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createInterruptAdapter()
	{
		if (interruptItemProvider == null)
		{
			interruptItemProvider = new InterruptItemProvider(this);
		}

		return interruptItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.ForLoop} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ForLoopItemProvider	forLoopItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.ForLoop}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createForLoopAdapter()
	{
		if (forLoopItemProvider == null)
		{
			forLoopItemProvider = new ForLoopItemProvider(this);
		}

		return forLoopItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.ConditionalLoop} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConditionalLoopItemProvider	conditionalLoopItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.ConditionalLoop}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createConditionalLoopAdapter()
	{
		if (conditionalLoopItemProvider == null)
		{
			conditionalLoopItemProvider = new ConditionalLoopItemProvider(this);
		}

		return conditionalLoopItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.NodeStatistics} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected NodeStatisticsItemProvider	nodeStatisticsItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.NodeStatistics}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createNodeStatisticsAdapter()
	{
		if (nodeStatisticsItemProvider == null)
		{
			nodeStatisticsItemProvider = new NodeStatisticsItemProvider(this);
		}

		return nodeStatisticsItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Super} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SuperItemProvider	superItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Super}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createSuperAdapter()
	{
		if (superItemProvider == null)
		{
			superItemProvider = new SuperItemProvider(this);
		}

		return superItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Resume} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ResumeItemProvider	resumeItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Resume}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createResumeAdapter()
	{
		if (resumeItemProvider == null)
		{
			resumeItemProvider = new ResumeItemProvider(this);
		}

		return resumeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.Branch} instances. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected BranchItemProvider	branchItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.Branch}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createBranchAdapter()
	{
		if (branchItemProvider == null)
		{
			branchItemProvider = new BranchItemProvider(this);
		}

		return branchItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.ujf.verimag.wb.BlockRef} instances. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected BlockRefItemProvider	blockRefItemProvider;

	/**
	 * This creates an adapter for a {@link org.ujf.verimag.wb.BlockRef}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter createBlockRefAdapter()
	{
		if (blockRefItemProvider == null)
		{
			blockRefItemProvider = new BlockRefItemProvider(this);
		}

		return blockRefItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory()
	{
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory)
	{
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type)
	{
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type)
	{
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type)
	{
		if (isFactoryForType(type))
		{
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class) || (((Class<?>) type).isInstance(adapter)))
			{
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener)
	{
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener)
	{
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification)
	{
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null)
		{
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void dispose()
	{
		if (moduleItemProvider != null)
			moduleItemProvider.dispose();
		if (declarationZoneItemProvider != null)
			declarationZoneItemProvider.dispose();
		if (submodelItemProvider != null)
			submodelItemProvider.dispose();
		if (allocateItemProvider != null)
			allocateItemProvider.dispose();
		if (serviceItemProvider != null)
			serviceItemProvider.dispose();
		if (resourcePoolItemProvider != null)
			resourcePoolItemProvider.dispose();
		if (arcItemProvider != null)
			arcItemProvider.dispose();
		if (responseArcItemProvider != null)
			responseArcItemProvider.dispose();
		if (categoryIDItemProvider != null)
			categoryIDItemProvider.dispose();
		if (variableItemProvider != null)
			variableItemProvider.dispose();
		if (submodelRefItemProvider != null)
			submodelRefItemProvider.dispose();
		if (serviceRefItemProvider != null)
			serviceRefItemProvider.dispose();
		if (allocateRefItemProvider != null)
			allocateRefItemProvider.dispose();
		if (blockItemProvider != null)
			blockItemProvider.dispose();
		if (sourceItemProvider != null)
			sourceItemProvider.dispose();
		if (sinkItemProvider != null)
			sinkItemProvider.dispose();
		if (enterItemProvider != null)
			enterItemProvider.dispose();
		if (returnItemProvider != null)
			returnItemProvider.dispose();
		if (splitItemProvider != null)
			splitItemProvider.dispose();
		if (forkItemProvider != null)
			forkItemProvider.dispose();
		if (joinItemProvider != null)
			joinItemProvider.dispose();
		if (userItemProvider != null)
			userItemProvider.dispose();
		if (delayItemProvider != null)
			delayItemProvider.dispose();
		if (releaseItemProvider != null)
			releaseItemProvider.dispose();
		if (interruptItemProvider != null)
			interruptItemProvider.dispose();
		if (forLoopItemProvider != null)
			forLoopItemProvider.dispose();
		if (conditionalLoopItemProvider != null)
			conditionalLoopItemProvider.dispose();
		if (nodeStatisticsItemProvider != null)
			nodeStatisticsItemProvider.dispose();
		if (superItemProvider != null)
			superItemProvider.dispose();
		if (resumeItemProvider != null)
			resumeItemProvider.dispose();
		if (branchItemProvider != null)
			branchItemProvider.dispose();
		if (blockRefItemProvider != null)
			blockRefItemProvider.dispose();
	}

}
