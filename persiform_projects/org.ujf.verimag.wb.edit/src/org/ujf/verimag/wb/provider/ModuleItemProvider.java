/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.WbFactory;
import org.ujf.verimag.wb.WbPackage;

/**
 * This is the item provider adapter for a {@link org.ujf.verimag.wb.Module} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ModuleItemProvider extends NamedElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModuleItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addTimeUnitPropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Time Unit feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addTimeUnitPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_TimedElement_timeUnit_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_TimedElement_timeUnit_feature",
				"_UI_TimedElement_type"), WbPackage.Literals.TIMED_ELEMENT__TIME_UNIT, true, false, false,
			ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Version feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Module_version_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Module_version_feature", "_UI_Module_type"),
			WbPackage.Literals.MODULE__VERSION, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
			null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(WbPackage.Literals.MODULE__DECLARATION_ZONE);
			childrenFeatures.add(WbPackage.Literals.MODULE__SUBMODEL);
			childrenFeatures.add(WbPackage.Literals.MODULE__ALLOCATE);
			childrenFeatures.add(WbPackage.Literals.MODULE__SERVICE);
			childrenFeatures.add(WbPackage.Literals.MODULE__RESOURCE_POOL);
			childrenFeatures.add(WbPackage.Literals.MODULE__BLOCK);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Module.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Module"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Module) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Module_type") : getString("_UI_Module_type") + " "
				+ label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Module.class))
		{
		case WbPackage.MODULE__TIME_UNIT:
		case WbPackage.MODULE__VERSION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case WbPackage.MODULE__DECLARATION_ZONE:
		case WbPackage.MODULE__SUBMODEL:
		case WbPackage.MODULE__ALLOCATE:
		case WbPackage.MODULE__SERVICE:
		case WbPackage.MODULE__RESOURCE_POOL:
		case WbPackage.MODULE__BLOCK:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__DECLARATION_ZONE, WbFactory.eINSTANCE
				.createDeclarationZone()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__SUBMODEL, WbFactory.eINSTANCE
				.createSubmodel()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__ALLOCATE, WbFactory.eINSTANCE
				.createAllocate()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__SERVICE, WbFactory.eINSTANCE
				.createService()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__RESOURCE_POOL, WbFactory.eINSTANCE
				.createResourcePool()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.MODULE__BLOCK, WbFactory.eINSTANCE
				.createBlock()));
	}

}
