/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.WbPackage;

/**
 * This is the item provider adapter for a {@link org.ujf.verimag.wb.Allocate} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class AllocateItemProvider extends NodeWithQueueItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AllocateItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addQuantityPropertyDescriptor(object);
			addGenerateCharacteristicPropertyDescriptor(object);
			addResourceInstanceSpecPropertyDescriptor(object);
			addRefPropertyDescriptor(object);
			addResourceInstancePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Quantity feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addQuantityPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Allocate_quantity_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Allocate_quantity_feature", "_UI_Allocate_type"),
			WbPackage.Literals.ALLOCATE__QUANTITY, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Generate Characteristic feature. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addGenerateCharacteristicPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(),
			getString("_UI_Allocate_generateCharacteristic_feature"), getString("_UI_PropertyDescriptor_description",
				"_UI_Allocate_generateCharacteristic_feature", "_UI_Allocate_type"),
			WbPackage.Literals.ALLOCATE__GENERATE_CHARACTERISTIC, true, false, false,
			ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Resource Instance Spec feature. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	protected void addResourceInstanceSpecPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Allocate_resourceInstanceSpec_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Allocate_resourceInstanceSpec_feature",
				"_UI_Allocate_type"), WbPackage.Literals.ALLOCATE__RESOURCE_INSTANCE_SPEC, true, false, false,
			ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Ref feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRefPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Allocate_ref_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Allocate_ref_feature", "_UI_Allocate_type"),
			WbPackage.Literals.ALLOCATE__REF, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Resource Instance feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addResourceInstancePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Allocate_resourceInstance_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Allocate_resourceInstance_feature",
				"_UI_Allocate_type"), WbPackage.Literals.ALLOCATE__RESOURCE_INSTANCE, true, false, true, null, null,
			null));
	}

	/**
	 * This returns Allocate.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Allocate"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Allocate) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Allocate_type") : getString("_UI_Allocate_type")
				+ " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Allocate.class))
		{
		case WbPackage.ALLOCATE__QUANTITY:
		case WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC:
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
