/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.WbPackage;

/**
 * This is the item provider adapter for a {@link org.ujf.verimag.wb.Arc} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ArcItemProvider extends NamedElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ArcItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addSelectConditionPropertyDescriptor(object);
			addSelectProbabilityPropertyDescriptor(object);
			addSelectPhasePropertyDescriptor(object);
			addSelectParentPropertyDescriptor(object);
			addDestNodeIndexPropertyDescriptor(object);
			addChangePortPropertyDescriptor(object);
			addChangePhasePropertyDescriptor(object);
			addSelectCategoryPropertyDescriptor(object);
			addFromNodePropertyDescriptor(object);
			addToNodePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Select Condition feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSelectConditionPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_selectCondition_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Arc_selectCondition_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__SELECT_CONDITION, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Select Probability feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSelectProbabilityPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_selectProbability_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Arc_selectProbability_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__SELECT_PROBABILITY, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Select Phase feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSelectPhasePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_selectPhase_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_selectPhase_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__SELECT_PHASE, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
			null));
	}

	/**
	 * This adds a property descriptor for the Select Parent feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSelectParentPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_selectParent_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_selectParent_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__SELECT_PARENT, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Dest Node Index feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDestNodeIndexPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_destNodeIndex_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_destNodeIndex_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__DEST_NODE_INDEX, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Change Port feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addChangePortPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_changePort_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_changePort_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__CHANGE_PORT, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
			null));
	}

	/**
	 * This adds a property descriptor for the Change Phase feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addChangePhasePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_changePhase_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_changePhase_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__CHANGE_PHASE, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
			null));
	}

	/**
	 * This adds a property descriptor for the Select Category feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSelectCategoryPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_selectCategory_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_selectCategory_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__SELECT_CATEGORY, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the From Node feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFromNodePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_fromNode_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_fromNode_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__FROM_NODE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the To Node feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addToNodePropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Arc_toNode_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Arc_toNode_feature", "_UI_Arc_type"),
			WbPackage.Literals.ARC__TO_NODE, true, false, true, null, null, null));
	}

	/**
	 * This returns Arc.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Arc"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Arc) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Arc_type") : getString("_UI_Arc_type") + " "
				+ label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Arc.class))
		{
		case WbPackage.ARC__SELECT_CONDITION:
		case WbPackage.ARC__SELECT_PROBABILITY:
		case WbPackage.ARC__SELECT_PHASE:
		case WbPackage.ARC__SELECT_PARENT:
		case WbPackage.ARC__DEST_NODE_INDEX:
		case WbPackage.ARC__CHANGE_PORT:
		case WbPackage.ARC__CHANGE_PHASE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
