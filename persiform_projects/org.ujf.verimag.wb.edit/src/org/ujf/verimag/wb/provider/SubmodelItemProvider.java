/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.WbFactory;
import org.ujf.verimag.wb.WbPackage;

/**
 * This is the item provider adapter for a {@link org.ujf.verimag.wb.Submodel} object. <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class SubmodelItemProvider extends GraphicalNodeItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
	/**
	 * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SubmodelItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addFormalParametersPropertyDescriptor(object);
			addDimensionPropertyDescriptor(object);
			addRefPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Formal Parameters feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFormalParametersPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Submodel_formalParameters_feature"),
			getString("_UI_PropertyDescriptor_description", "_UI_Submodel_formalParameters_feature",
				"_UI_Submodel_type"), WbPackage.Literals.SUBMODEL__FORMAL_PARAMETERS, true, false, false,
			ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Dimension feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDimensionPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Submodel_dimension_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Submodel_dimension_feature", "_UI_Submodel_type"),
			WbPackage.Literals.SUBMODEL__DIMENSION, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
			null, null));
	}

	/**
	 * This adds a property descriptor for the Ref feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRefPropertyDescriptor(Object object)
	{
		itemPropertyDescriptors.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory)
				.getRootAdapterFactory(), getResourceLocator(), getString("_UI_Submodel_ref_feature"), getString(
			"_UI_PropertyDescriptor_description", "_UI_Submodel_ref_feature", "_UI_Submodel_type"),
			WbPackage.Literals.SUBMODEL__REF, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(WbPackage.Literals.SUBMODEL__DECLARATION_ZONE);
			childrenFeatures.add(WbPackage.Literals.SUBMODEL__RESPONSE_ARC);
			childrenFeatures.add(WbPackage.Literals.SUBMODEL__ARC);
			childrenFeatures.add(WbPackage.Literals.SUBMODEL__NODE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Submodel.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Submodel"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object)
	{
		String label = ((Submodel) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Submodel_type") : getString("_UI_Submodel_type")
				+ " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached children and by creating
	 * a viewer notification, which it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Submodel.class))
		{
		case WbPackage.SUBMODEL__FORMAL_PARAMETERS:
		case WbPackage.SUBMODEL__DIMENSION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
		case WbPackage.SUBMODEL__RESPONSE_ARC:
		case WbPackage.SUBMODEL__ARC:
		case WbPackage.SUBMODEL__NODE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children that can be created
	 * under this object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__DECLARATION_ZONE, WbFactory.eINSTANCE
				.createDeclarationZone()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__RESPONSE_ARC, WbFactory.eINSTANCE
				.createResponseArc()));

		newChildDescriptors
				.add(createChildParameter(WbPackage.Literals.SUBMODEL__ARC, WbFactory.eINSTANCE.createArc()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createAllocate()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createService()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createSubmodelRef()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createServiceRef()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createAllocateRef()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createBlock()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createSource()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createSink()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createEnter()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createReturn()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createSplit()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createFork()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createJoin()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createUser()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createDelay()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createRelease()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createInterrupt()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createForLoop()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createConditionalLoop()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createSuper()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createResume()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createBranch()));

		newChildDescriptors.add(createChildParameter(WbPackage.Literals.SUBMODEL__NODE, WbFactory.eINSTANCE
				.createBlockRef()));
	}

}
