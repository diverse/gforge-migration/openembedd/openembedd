/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Timed Element</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.TimedElement#getTimeUnit <em>Time Unit</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getTimedElement()
 * @model abstract="true"
 * @generated
 */
public interface TimedElement extends EObject
{
	/**
	 * Returns the value of the '<em><b>Time Unit</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.TimeUnit}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Unit</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Time Unit</em>' attribute.
	 * @see org.ujf.verimag.wb.TimeUnit
	 * @see #setTimeUnit(TimeUnit)
	 * @see org.ujf.verimag.wb.WbPackage#getTimedElement_TimeUnit()
	 * @model
	 * @generated
	 */
	TimeUnit getTimeUnit();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.TimedElement#getTimeUnit <em>Time Unit</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Time Unit</em>' attribute.
	 * @see org.ujf.verimag.wb.TimeUnit
	 * @see #getTimeUnit()
	 * @generated
	 */
	void setTimeUnit(TimeUnit value);

} // TimedElement
