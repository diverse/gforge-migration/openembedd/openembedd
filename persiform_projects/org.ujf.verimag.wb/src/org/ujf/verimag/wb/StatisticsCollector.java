/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Statistics Collector</b></em>'. <!--
 * end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.StatisticsCollector#getBreakdownList <em>Breakdown List</em>}</li>
 * <li>{@link org.ujf.verimag.wb.StatisticsCollector#getHistogramBuckets <em>Histogram Buckets</em>}</li>
 * <li>{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateChange <em>Update Change</em>}</li>
 * <li>{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateDelay <em>Update Delay</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getStatisticsCollector()
 * @model abstract="true"
 * @generated
 */
public interface StatisticsCollector extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Breakdown List</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Breakdown List</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Breakdown List</em>' attribute.
	 * @see #setBreakdownList(String)
	 * @see org.ujf.verimag.wb.WbPackage#getStatisticsCollector_BreakdownList()
	 * @model
	 * @generated
	 */
	String getBreakdownList();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.StatisticsCollector#getBreakdownList <em>Breakdown List</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Breakdown List</em>' attribute.
	 * @see #getBreakdownList()
	 * @generated
	 */
	void setBreakdownList(String value);

	/**
	 * Returns the value of the '<em><b>Histogram Buckets</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Histogram Buckets</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Histogram Buckets</em>' attribute.
	 * @see #setHistogramBuckets(String)
	 * @see org.ujf.verimag.wb.WbPackage#getStatisticsCollector_HistogramBuckets()
	 * @model
	 * @generated
	 */
	String getHistogramBuckets();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.StatisticsCollector#getHistogramBuckets
	 * <em>Histogram Buckets</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Histogram Buckets</em>' attribute.
	 * @see #getHistogramBuckets()
	 * @generated
	 */
	void setHistogramBuckets(String value);

	/**
	 * Returns the value of the '<em><b>Update Change</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update Change</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Update Change</em>' attribute.
	 * @see #setUpdateChange(String)
	 * @see org.ujf.verimag.wb.WbPackage#getStatisticsCollector_UpdateChange()
	 * @model
	 * @generated
	 */
	String getUpdateChange();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateChange <em>Update Change</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Update Change</em>' attribute.
	 * @see #getUpdateChange()
	 * @generated
	 */
	void setUpdateChange(String value);

	/**
	 * Returns the value of the '<em><b>Update Delay</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update Delay</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Update Delay</em>' attribute.
	 * @see #setUpdateDelay(String)
	 * @see org.ujf.verimag.wb.WbPackage#getStatisticsCollector_UpdateDelay()
	 * @model
	 * @generated
	 */
	String getUpdateDelay();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateDelay <em>Update Delay</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Update Delay</em>' attribute.
	 * @see #getUpdateDelay()
	 * @generated
	 */
	void setUpdateDelay(String value);

} // StatisticsCollector
