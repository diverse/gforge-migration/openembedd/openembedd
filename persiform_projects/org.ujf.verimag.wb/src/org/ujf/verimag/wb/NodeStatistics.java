/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Node Statistics</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.NodeStatistics#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.NodeStatistics#getKind <em>Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.NodeStatistics#getDirectNode <em>Direct Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getNodeStatistics()
 * @model
 * @generated
 */
public interface NodeStatistics extends StatisticsCollector
{
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. The default value is <code>"1"</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeStatistics_Dimension()
	 * @model default="1"
	 * @generated
	 */
	Integer getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeStatistics#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(Integer value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.StatClassKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.StatClassKind
	 * @see #setKind(StatClassKind)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeStatistics_Kind()
	 * @model
	 * @generated
	 */
	StatClassKind getKind();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeStatistics#getKind <em>Kind</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.StatClassKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(StatClassKind value);

	/**
	 * Returns the value of the '<em><b>Direct Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.DirectNode#getNodeStatistics <em>Node Statistics</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Direct Node</em>' reference.
	 * @see #setDirectNode(DirectNode)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeStatistics_DirectNode()
	 * @see org.ujf.verimag.wb.DirectNode#getNodeStatistics
	 * @model opposite="nodeStatistics" required="true"
	 * @generated
	 */
	DirectNode getDirectNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeStatistics#getDirectNode <em>Direct Node</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Direct Node</em>' reference.
	 * @see #getDirectNode()
	 * @generated
	 */
	void setDirectNode(DirectNode value);

} // NodeStatistics
