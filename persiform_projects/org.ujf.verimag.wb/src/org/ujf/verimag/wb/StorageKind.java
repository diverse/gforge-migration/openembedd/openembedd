/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '<em><b>Storage Kind</b></em>', and
 * utility methods for working with them. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.WbPackage#getStorageKind()
 * @model
 * @generated
 */
public enum StorageKind implements Enumerator
{
	/**
	 * The '<em><b>Unshared</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #UNSHARED_VALUE
	 * @generated
	 * @ordered
	 */
	UNSHARED(0, "unshared", "unshared"),

	/**
	 * The '<em><b>Parameter</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	PARAMETER(1, "parameter", "parameter"),

	/**
	 * The '<em><b>Const</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONST_VALUE
	 * @generated
	 * @ordered
	 */
	CONST(2, "const", "const"),

	/**
	 * The '<em><b>Extern C</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #EXTERN_C_VALUE
	 * @generated
	 * @ordered
	 */
	EXTERN_C(3, "externC", "externC"),

	/**
	 * The '<em><b>Parameter Const</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER_CONST_VALUE
	 * @generated
	 * @ordered
	 */
	PARAMETER_CONST(4, "parameterConst", "parameterConst"),

	/**
	 * The '<em><b>Parameter Reeval</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER_REEVAL_VALUE
	 * @generated
	 * @ordered
	 */
	PARAMETER_REEVAL(5, "parameterReeval", "parameterReeval"),

	/**
	 * The '<em><b>Shared</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #SHARED_VALUE
	 * @generated
	 * @ordered
	 */
	SHARED(6, "shared", "shared");

	/**
	 * The '<em><b>Unshared</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unshared</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #UNSHARED
	 * @model name="unshared"
	 * @generated
	 * @ordered
	 */
	public static final int					UNSHARED_VALUE			= 0;

	/**
	 * The '<em><b>Parameter</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Parameter</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER
	 * @model name="parameter"
	 * @generated
	 * @ordered
	 */
	public static final int					PARAMETER_VALUE			= 1;

	/**
	 * The '<em><b>Const</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Const</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONST
	 * @model name="const"
	 * @generated
	 * @ordered
	 */
	public static final int					CONST_VALUE				= 2;

	/**
	 * The '<em><b>Extern C</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Extern C</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #EXTERN_C
	 * @model name="externC"
	 * @generated
	 * @ordered
	 */
	public static final int					EXTERN_C_VALUE			= 3;

	/**
	 * The '<em><b>Parameter Const</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Parameter Const</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER_CONST
	 * @model name="parameterConst"
	 * @generated
	 * @ordered
	 */
	public static final int					PARAMETER_CONST_VALUE	= 4;

	/**
	 * The '<em><b>Parameter Reeval</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Parameter Reeval</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #PARAMETER_REEVAL
	 * @model name="parameterReeval"
	 * @generated
	 * @ordered
	 */
	public static final int					PARAMETER_REEVAL_VALUE	= 5;

	/**
	 * The '<em><b>Shared</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Shared</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SHARED
	 * @model name="shared"
	 * @generated
	 * @ordered
	 */
	public static final int					SHARED_VALUE			= 6;

	/**
	 * An array of all the '<em><b>Storage Kind</b></em>' enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final StorageKind[]		VALUES_ARRAY			= new StorageKind[] { UNSHARED, PARAMETER, CONST,
			EXTERN_C, PARAMETER_CONST, PARAMETER_REEVAL, SHARED,	};

	/**
	 * A public read-only list of all the '<em><b>Storage Kind</b></em>' enumerators. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<StorageKind>	VALUES					= Collections.unmodifiableList(Arrays
																			.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Storage Kind</b></em>' literal with the specified literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static StorageKind get(String literal)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			StorageKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Storage Kind</b></em>' literal with the specified name. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static StorageKind getByName(String name)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			StorageKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Storage Kind</b></em>' literal with the specified integer value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static StorageKind get(int value)
	{
		switch (value)
		{
		case UNSHARED_VALUE:
			return UNSHARED;
		case PARAMETER_VALUE:
			return PARAMETER;
		case CONST_VALUE:
			return CONST;
		case EXTERN_C_VALUE:
			return EXTERN_C;
		case PARAMETER_CONST_VALUE:
			return PARAMETER_CONST;
		case PARAMETER_REEVAL_VALUE:
			return PARAMETER_REEVAL;
		case SHARED_VALUE:
			return SHARED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int		value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private StorageKind(int value, String name, String literal)
	{
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue()
	{
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral()
	{
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		return literal;
	}

} // StorageKind
