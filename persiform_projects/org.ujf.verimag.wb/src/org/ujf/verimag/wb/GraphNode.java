/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Graph Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getOutArc <em>Out Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getInArc <em>In Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getInterrupt <em>Interrupt</em>}</li>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getOutResponseArc <em>Out Response Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.GraphNode#getInResponseArc <em>In Response Arc</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getGraphNode()
 * @model abstract="true"
 * @generated
 */
public interface GraphNode extends GraphicalNode
{
	/**
	 * Returns the value of the '<em><b>Out Arc</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Arc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Arc#getFromNode <em>From Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Arc</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Out Arc</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_OutArc()
	 * @see org.ujf.verimag.wb.Arc#getFromNode
	 * @model opposite="fromNode"
	 * @generated
	 */
	EList<Arc> getOutArc();

	/**
	 * Returns the value of the '<em><b>In Arc</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Arc}. It is bidirectional and its opposite is '{@link org.ujf.verimag.wb.Arc#getToNode
	 * <em>To Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Arc</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>In Arc</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_InArc()
	 * @see org.ujf.verimag.wb.Arc#getToNode
	 * @model opposite="toNode"
	 * @generated
	 */
	EList<Arc> getInArc();

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' container reference. It is bidirectional and its opposite is
	 * '{@link org.ujf.verimag.wb.Submodel#getNode <em>Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Submodel</em>' container reference.
	 * @see #setSubmodel(Submodel)
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_Submodel()
	 * @see org.ujf.verimag.wb.Submodel#getNode
	 * @model opposite="node"
	 * @generated
	 */
	Submodel getSubmodel();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.GraphNode#getSubmodel <em>Submodel</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Submodel</em>' container reference.
	 * @see #getSubmodel()
	 * @generated
	 */
	void setSubmodel(Submodel value);

	/**
	 * Returns the value of the '<em><b>Interrupt</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Interrupt}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Interrupt#getSelectNode <em>Select Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Interrupt</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_Interrupt()
	 * @see org.ujf.verimag.wb.Interrupt#getSelectNode
	 * @model opposite="selectNode"
	 * @generated
	 */
	EList<Interrupt> getInterrupt();

	/**
	 * Returns the value of the '<em><b>Out Response Arc</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.ResponseArc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ResponseArc#getFromNode <em>From Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Response Arc</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Out Response Arc</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_OutResponseArc()
	 * @see org.ujf.verimag.wb.ResponseArc#getFromNode
	 * @model opposite="fromNode"
	 * @generated
	 */
	EList<ResponseArc> getOutResponseArc();

	/**
	 * Returns the value of the '<em><b>In Response Arc</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.ResponseArc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ResponseArc#getToNode <em>To Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Response Arc</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>In Response Arc</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getGraphNode_InResponseArc()
	 * @see org.ujf.verimag.wb.ResponseArc#getToNode
	 * @model opposite="toNode"
	 * @generated
	 */
	EList<ResponseArc> getInResponseArc();

} // GraphNode
