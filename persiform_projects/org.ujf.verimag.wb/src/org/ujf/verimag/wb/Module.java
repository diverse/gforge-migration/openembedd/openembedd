/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Module</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Module#getVersion <em>Version</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getAllocate <em>Allocate</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getService <em>Service</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getResourcePool <em>Resource Pool</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Module#getBlock <em>Block</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getModule()
 * @model
 * @generated
 */
public interface Module extends NamedElement, TimedElement
{
	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.ujf.verimag.wb.WbPackage#getModule_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Module#getVersion <em>Version</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Declaration Zone</b></em>' containment reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.wb.DeclarationZone#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration Zone</em>' containment reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Declaration Zone</em>' containment reference.
	 * @see #setDeclarationZone(DeclarationZone)
	 * @see org.ujf.verimag.wb.WbPackage#getModule_DeclarationZone()
	 * @see org.ujf.verimag.wb.DeclarationZone#get_module
	 * @model opposite="_module" containment="true"
	 * @generated
	 */
	DeclarationZone getDeclarationZone();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Module#getDeclarationZone <em>Declaration Zone</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Declaration Zone</em>' containment reference.
	 * @see #getDeclarationZone()
	 * @generated
	 */
	void setDeclarationZone(DeclarationZone value);

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Submodel}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Submodel#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' containment reference list isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Submodel</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getModule_Submodel()
	 * @see org.ujf.verimag.wb.Submodel#get_module
	 * @model opposite="_module" containment="true" required="true"
	 * @generated
	 */
	EList<Submodel> getSubmodel();

	/**
	 * Returns the value of the '<em><b>Allocate</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Allocate}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Allocate#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocate</em>' containment reference list isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Allocate</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getModule_Allocate()
	 * @see org.ujf.verimag.wb.Allocate#get_module
	 * @model opposite="_module" containment="true"
	 * @generated
	 */
	EList<Allocate> getAllocate();

	/**
	 * Returns the value of the '<em><b>Service</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Service}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Service#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Service</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getModule_Service()
	 * @see org.ujf.verimag.wb.Service#get_module
	 * @model opposite="_module" containment="true"
	 * @generated
	 */
	EList<Service> getService();

	/**
	 * Returns the value of the '<em><b>Resource Pool</b></em>' containment reference list. The list contents are of
	 * type {@link org.ujf.verimag.wb.ResourcePool}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ResourcePool#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Pool</em>' containment reference list isn't clear, there really should be
	 * more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Resource Pool</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getModule_ResourcePool()
	 * @see org.ujf.verimag.wb.ResourcePool#get_module
	 * @model opposite="_module" containment="true"
	 * @generated
	 */
	EList<ResourcePool> getResourcePool();

	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Block}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Block#get_module <em>module</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Block</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getModule_Block()
	 * @see org.ujf.verimag.wb.Block#get_module
	 * @model opposite="_module" containment="true"
	 * @generated
	 */
	EList<Block> getBlock();

} // Module
