/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Submodel</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Submodel#getFormalParameters <em>Formal Parameters</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getResponseArc <em>Response Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getArc <em>Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getRef <em>Ref</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Submodel#getNode <em>Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getSubmodel()
 * @model
 * @generated
 */
public interface Submodel extends GraphicalNode
{
	/**
	 * Returns the value of the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Parameters</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Formal Parameters</em>' attribute.
	 * @see #setFormalParameters(String)
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_FormalParameters()
	 * @model
	 * @generated
	 */
	String getFormalParameters();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Submodel#getFormalParameters <em>Formal Parameters</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Formal Parameters</em>' attribute.
	 * @see #getFormalParameters()
	 * @generated
	 */
	void setFormalParameters(String value);

	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(String)
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_Dimension()
	 * @model
	 * @generated
	 */
	String getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Submodel#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(String value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getSubmodel <em>Submodel</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel__module()
	 * @see org.ujf.verimag.wb.Module#getSubmodel
	 * @model opposite="submodel" required="true"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Submodel#get_module <em>module</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Declaration Zone</b></em>' containment reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.wb.DeclarationZone#getSubmodel <em>Submodel</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration Zone</em>' containment reference isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Declaration Zone</em>' containment reference.
	 * @see #setDeclarationZone(DeclarationZone)
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_DeclarationZone()
	 * @see org.ujf.verimag.wb.DeclarationZone#getSubmodel
	 * @model opposite="submodel" containment="true"
	 * @generated
	 */
	DeclarationZone getDeclarationZone();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Submodel#getDeclarationZone <em>Declaration Zone</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Declaration Zone</em>' containment reference.
	 * @see #getDeclarationZone()
	 * @generated
	 */
	void setDeclarationZone(DeclarationZone value);

	/**
	 * Returns the value of the '<em><b>Response Arc</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.ResponseArc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ResponseArc#getSubmodel <em>Submodel</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Arc</em>' containment reference list isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Response Arc</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_ResponseArc()
	 * @see org.ujf.verimag.wb.ResponseArc#getSubmodel
	 * @model opposite="submodel" containment="true"
	 * @generated
	 */
	EList<ResponseArc> getResponseArc();

	/**
	 * Returns the value of the '<em><b>Arc</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Arc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Arc#getSubmodel <em>Submodel</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Arc</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_Arc()
	 * @see org.ujf.verimag.wb.Arc#getSubmodel
	 * @model opposite="submodel" containment="true" required="true"
	 * @generated
	 */
	EList<Arc> getArc();

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.SubmodelRef}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.SubmodelRef#getTarget <em>Target</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ref</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_Ref()
	 * @see org.ujf.verimag.wb.SubmodelRef#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<SubmodelRef> getRef();

	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.GraphNode}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getSubmodel <em>Submodel</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodel_Node()
	 * @see org.ujf.verimag.wb.GraphNode#getSubmodel
	 * @model opposite="submodel" containment="true" required="true"
	 * @generated
	 */
	EList<GraphNode> getNode();

} // Submodel
