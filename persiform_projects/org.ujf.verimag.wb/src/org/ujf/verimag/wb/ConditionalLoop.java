/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Conditional Loop</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.ConditionalLoop#getIsWhile <em>Is While</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ConditionalLoop#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getConditionalLoop()
 * @model
 * @generated
 */
public interface ConditionalLoop extends Loop
{
	/**
	 * Returns the value of the '<em><b>Is While</b></em>' attribute. The default value is <code>"true"</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is While</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Is While</em>' attribute.
	 * @see #setIsWhile(Boolean)
	 * @see org.ujf.verimag.wb.WbPackage#getConditionalLoop_IsWhile()
	 * @model default="true"
	 * @generated
	 */
	Boolean getIsWhile();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ConditionalLoop#getIsWhile <em>Is While</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Is While</em>' attribute.
	 * @see #getIsWhile()
	 * @generated
	 */
	void setIsWhile(Boolean value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see #setCondition(String)
	 * @see org.ujf.verimag.wb.WbPackage#getConditionalLoop_Condition()
	 * @model
	 * @generated
	 */
	String getCondition();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ConditionalLoop#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Condition</em>' attribute.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(String value);

} // ConditionalLoop
