/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Resource Pool</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#getResourceKind <em>Resource Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#getTokenQuantity <em>Token Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#getAllocate <em>Allocate</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResourcePool#getRelease <em>Release</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getResourcePool()
 * @model
 * @generated
 */
public interface ResourcePool extends GraphicalNode
{
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(String)
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool_Dimension()
	 * @model
	 * @generated
	 */
	String getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResourcePool#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(String value);

	/**
	 * Returns the value of the '<em><b>Resource Kind</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.ResourceKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Kind</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Resource Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.ResourceKind
	 * @see #setResourceKind(ResourceKind)
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool_ResourceKind()
	 * @model
	 * @generated
	 */
	ResourceKind getResourceKind();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResourcePool#getResourceKind <em>Resource Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Resource Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.ResourceKind
	 * @see #getResourceKind()
	 * @generated
	 */
	void setResourceKind(ResourceKind value);

	/**
	 * Returns the value of the '<em><b>Token Quantity</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Quantity</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Token Quantity</em>' attribute.
	 * @see #setTokenQuantity(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool_TokenQuantity()
	 * @model
	 * @generated
	 */
	Integer getTokenQuantity();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResourcePool#getTokenQuantity <em>Token Quantity</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Token Quantity</em>' attribute.
	 * @see #getTokenQuantity()
	 * @generated
	 */
	void setTokenQuantity(Integer value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getResourcePool <em>Resource Pool</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool__module()
	 * @see org.ujf.verimag.wb.Module#getResourcePool
	 * @model opposite="resourcePool"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResourcePool#get_module <em>module</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Allocate</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Allocate}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Allocate#getResourceInstance <em>Resource Instance</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocate</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Allocate</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool_Allocate()
	 * @see org.ujf.verimag.wb.Allocate#getResourceInstance
	 * @model opposite="resourceInstance"
	 * @generated
	 */
	EList<Allocate> getAllocate();

	/**
	 * Returns the value of the '<em><b>Release</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Release}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Release#getResourceInstance <em>Resource Instance</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Release</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Release</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getResourcePool_Release()
	 * @see org.ujf.verimag.wb.Release#getResourceInstance
	 * @model opposite="resourceInstance"
	 * @generated
	 */
	EList<Release> getRelease();

} // ResourcePool
