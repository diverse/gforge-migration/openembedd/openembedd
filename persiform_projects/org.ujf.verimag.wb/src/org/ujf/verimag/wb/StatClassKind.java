/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '<em><b>Stat Class Kind</b></em>', and
 * utility methods for working with them. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.WbPackage#getStatClassKind()
 * @model
 * @generated
 */
public enum StatClassKind implements Enumerator
{
	/**
	 * The '<em><b>Continuous</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #CONTINUOUS_VALUE
	 * @generated
	 * @ordered
	 */
	CONTINUOUS(0, "continuous", "continuous"),

	/**
	 * The '<em><b>Discrete</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #DISCRETE_VALUE
	 * @generated
	 * @ordered
	 */
	DISCRETE(1, "discrete", "discrete"),

	/**
	 * The '<em><b>Interarrival</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #INTERARRIVAL_VALUE
	 * @generated
	 * @ordered
	 */
	INTERARRIVAL(2, "interarrival", "interarrival"),

	/**
	 * The '<em><b>Lifetime</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #LIFETIME_VALUE
	 * @generated
	 * @ordered
	 */
	LIFETIME(3, "lifetime", "lifetime"),

	/**
	 * The '<em><b>Population</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #POPULATION_VALUE
	 * @generated
	 * @ordered
	 */
	POPULATION(4, "population", "population"),

	/**
	 * The '<em><b>Response</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #RESPONSE_VALUE
	 * @generated
	 * @ordered
	 */
	RESPONSE(5, "response", "response"),

	/**
	 * The '<em><b>Utilization</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #UTILIZATION_VALUE
	 * @generated
	 * @ordered
	 */
	UTILIZATION(6, "utilization", "utilization"),

	/**
	 * The '<em><b>Qpopulation</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #QPOPULATION_VALUE
	 * @generated
	 * @ordered
	 */
	QPOPULATION(7, "qpopulation", "qpopulation");

	/**
	 * The '<em><b>Continuous</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Continuous</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #CONTINUOUS
	 * @model name="continuous"
	 * @generated
	 * @ordered
	 */
	public static final int					CONTINUOUS_VALUE	= 0;

	/**
	 * The '<em><b>Discrete</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Discrete</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DISCRETE
	 * @model name="discrete"
	 * @generated
	 * @ordered
	 */
	public static final int					DISCRETE_VALUE		= 1;

	/**
	 * The '<em><b>Interarrival</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Interarrival</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTERARRIVAL
	 * @model name="interarrival"
	 * @generated
	 * @ordered
	 */
	public static final int					INTERARRIVAL_VALUE	= 2;

	/**
	 * The '<em><b>Lifetime</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Lifetime</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #LIFETIME
	 * @model name="lifetime"
	 * @generated
	 * @ordered
	 */
	public static final int					LIFETIME_VALUE		= 3;

	/**
	 * The '<em><b>Population</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Population</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #POPULATION
	 * @model name="population"
	 * @generated
	 * @ordered
	 */
	public static final int					POPULATION_VALUE	= 4;

	/**
	 * The '<em><b>Response</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Response</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #RESPONSE
	 * @model name="response"
	 * @generated
	 * @ordered
	 */
	public static final int					RESPONSE_VALUE		= 5;

	/**
	 * The '<em><b>Utilization</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Utilization</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #UTILIZATION
	 * @model name="utilization"
	 * @generated
	 * @ordered
	 */
	public static final int					UTILIZATION_VALUE	= 6;

	/**
	 * The '<em><b>Qpopulation</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Qpopulation</b></em>' literal object isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #QPOPULATION
	 * @model name="qpopulation"
	 * @generated
	 * @ordered
	 */
	public static final int					QPOPULATION_VALUE	= 7;

	/**
	 * An array of all the '<em><b>Stat Class Kind</b></em>' enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final StatClassKind[]	VALUES_ARRAY		= new StatClassKind[] { CONTINUOUS, DISCRETE,
			INTERARRIVAL, LIFETIME, POPULATION, RESPONSE, UTILIZATION, QPOPULATION, };

	/**
	 * A public read-only list of all the '<em><b>Stat Class Kind</b></em>' enumerators. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<StatClassKind>	VALUES				= Collections.unmodifiableList(Arrays
																		.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Stat Class Kind</b></em>' literal with the specified literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static StatClassKind get(String literal)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			StatClassKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stat Class Kind</b></em>' literal with the specified name. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static StatClassKind getByName(String name)
	{
		for (int i = 0; i < VALUES_ARRAY.length; ++i)
		{
			StatClassKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stat Class Kind</b></em>' literal with the specified integer value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static StatClassKind get(int value)
	{
		switch (value)
		{
		case CONTINUOUS_VALUE:
			return CONTINUOUS;
		case DISCRETE_VALUE:
			return DISCRETE;
		case INTERARRIVAL_VALUE:
			return INTERARRIVAL;
		case LIFETIME_VALUE:
			return LIFETIME;
		case POPULATION_VALUE:
			return POPULATION;
		case RESPONSE_VALUE:
			return RESPONSE;
		case UTILIZATION_VALUE:
			return UTILIZATION;
		case QPOPULATION_VALUE:
			return QPOPULATION;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int		value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String	literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private StatClassKind(int value, String name, String literal)
	{
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue()
	{
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral()
	{
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		return literal;
	}

} // StatClassKind
