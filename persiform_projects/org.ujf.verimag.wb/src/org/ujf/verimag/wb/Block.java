/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Block</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Block#getBlockUntil <em>Block Until</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Block#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Block#getRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getBlock()
 * @model
 * @generated
 */
public interface Block extends NodeWithQueue
{
	/**
	 * Returns the value of the '<em><b>Block Until</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Until</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Block Until</em>' attribute.
	 * @see #setBlockUntil(String)
	 * @see org.ujf.verimag.wb.WbPackage#getBlock_BlockUntil()
	 * @model
	 * @generated
	 */
	String getBlockUntil();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Block#getBlockUntil <em>Block Until</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Block Until</em>' attribute.
	 * @see #getBlockUntil()
	 * @generated
	 */
	void setBlockUntil(String value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getBlock <em>Block</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getBlock__module()
	 * @see org.ujf.verimag.wb.Module#getBlock
	 * @model opposite="block"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Block#get_module <em>module</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.BlockRef}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.BlockRef#getTarget <em>Target</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ref</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getBlock_Ref()
	 * @see org.ujf.verimag.wb.BlockRef#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<BlockRef> getRef();

} // Block
