/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Allocate</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Allocate#getQuantity <em>Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Allocate#getGenerateCharacteristic <em>Generate Characteristic</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Allocate#getResourceInstanceSpec <em>Resource Instance Spec</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Allocate#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Allocate#getRef <em>Ref</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Allocate#getResourceInstance <em>Resource Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getAllocate()
 * @model
 * @generated
 */
public interface Allocate extends NodeWithQueue
{
	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(String)
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate_Quantity()
	 * @model
	 * @generated
	 */
	String getQuantity();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Allocate#getQuantity <em>Quantity</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(String value);

	/**
	 * Returns the value of the '<em><b>Generate Characteristic</b></em>' attribute. The default value is
	 * <code>"false"</code>. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Characteristic</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Generate Characteristic</em>' attribute.
	 * @see #setGenerateCharacteristic(Boolean)
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate_GenerateCharacteristic()
	 * @model default="false"
	 * @generated
	 */
	Boolean getGenerateCharacteristic();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Allocate#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Generate Characteristic</em>' attribute.
	 * @see #getGenerateCharacteristic()
	 * @generated
	 */
	void setGenerateCharacteristic(Boolean value);

	/**
	 * Returns the value of the '<em><b>Resource Instance Spec</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Instance Spec</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Resource Instance Spec</em>' attribute.
	 * @see #setResourceInstanceSpec(String)
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate_ResourceInstanceSpec()
	 * @model
	 * @generated
	 */
	String getResourceInstanceSpec();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Allocate#getResourceInstanceSpec
	 * <em>Resource Instance Spec</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Resource Instance Spec</em>' attribute.
	 * @see #getResourceInstanceSpec()
	 * @generated
	 */
	void setResourceInstanceSpec(String value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getAllocate <em>Allocate</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate__module()
	 * @see org.ujf.verimag.wb.Module#getAllocate
	 * @model opposite="allocate"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Allocate#get_module <em>module</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.AllocateRef}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.AllocateRef#getTarget <em>Target</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ref</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate_Ref()
	 * @see org.ujf.verimag.wb.AllocateRef#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<AllocateRef> getRef();

	/**
	 * Returns the value of the '<em><b>Resource Instance</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ResourcePool#getAllocate <em>Allocate</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Instance</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Resource Instance</em>' reference.
	 * @see #setResourceInstance(ResourcePool)
	 * @see org.ujf.verimag.wb.WbPackage#getAllocate_ResourceInstance()
	 * @see org.ujf.verimag.wb.ResourcePool#getAllocate
	 * @model opposite="allocate"
	 * @generated
	 */
	ResourcePool getResourceInstance();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Allocate#getResourceInstance <em>Resource Instance</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Resource Instance</em>' reference.
	 * @see #getResourceInstance()
	 * @generated
	 */
	void setResourceInstance(ResourcePool value);

} // Allocate
