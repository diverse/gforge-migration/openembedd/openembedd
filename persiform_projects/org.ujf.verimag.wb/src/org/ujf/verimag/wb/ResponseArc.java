/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Response Arc</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.ResponseArc#getSourceNodeIndex <em>Source Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResponseArc#getDestinationNodeIndex <em>Destination Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResponseArc#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResponseArc#getFromNode <em>From Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.ResponseArc#getToNode <em>To Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getResponseArc()
 * @model
 * @generated
 */
public interface ResponseArc extends StatisticsCollector, NamedElement
{
	/**
	 * Returns the value of the '<em><b>Source Node Index</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Node Index</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Source Node Index</em>' attribute.
	 * @see #setSourceNodeIndex(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getResponseArc_SourceNodeIndex()
	 * @model
	 * @generated
	 */
	Integer getSourceNodeIndex();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResponseArc#getSourceNodeIndex <em>Source Node Index</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Source Node Index</em>' attribute.
	 * @see #getSourceNodeIndex()
	 * @generated
	 */
	void setSourceNodeIndex(Integer value);

	/**
	 * Returns the value of the '<em><b>Destination Node Index</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination Node Index</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Destination Node Index</em>' attribute.
	 * @see #setDestinationNodeIndex(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getResponseArc_DestinationNodeIndex()
	 * @model
	 * @generated
	 */
	Integer getDestinationNodeIndex();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResponseArc#getDestinationNodeIndex
	 * <em>Destination Node Index</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Destination Node Index</em>' attribute.
	 * @see #getDestinationNodeIndex()
	 * @generated
	 */
	void setDestinationNodeIndex(Integer value);

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' container reference. It is bidirectional and its opposite is
	 * '{@link org.ujf.verimag.wb.Submodel#getResponseArc <em>Response Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Submodel</em>' container reference.
	 * @see #setSubmodel(Submodel)
	 * @see org.ujf.verimag.wb.WbPackage#getResponseArc_Submodel()
	 * @see org.ujf.verimag.wb.Submodel#getResponseArc
	 * @model opposite="responseArc" required="true"
	 * @generated
	 */
	Submodel getSubmodel();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResponseArc#getSubmodel <em>Submodel</em>}' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Submodel</em>' container reference.
	 * @see #getSubmodel()
	 * @generated
	 */
	void setSubmodel(Submodel value);

	/**
	 * Returns the value of the '<em><b>From Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getOutResponseArc <em>Out Response Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>From Node</em>' reference.
	 * @see #setFromNode(GraphNode)
	 * @see org.ujf.verimag.wb.WbPackage#getResponseArc_FromNode()
	 * @see org.ujf.verimag.wb.GraphNode#getOutResponseArc
	 * @model opposite="outResponseArc" required="true"
	 * @generated
	 */
	GraphNode getFromNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResponseArc#getFromNode <em>From Node</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>From Node</em>' reference.
	 * @see #getFromNode()
	 * @generated
	 */
	void setFromNode(GraphNode value);

	/**
	 * Returns the value of the '<em><b>To Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getInResponseArc <em>In Response Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>To Node</em>' reference.
	 * @see #setToNode(GraphNode)
	 * @see org.ujf.verimag.wb.WbPackage#getResponseArc_ToNode()
	 * @see org.ujf.verimag.wb.GraphNode#getInResponseArc
	 * @model opposite="inResponseArc" required="true"
	 * @generated
	 */
	GraphNode getToNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.ResponseArc#getToNode <em>To Node</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>To Node</em>' reference.
	 * @see #getToNode()
	 * @generated
	 */
	void setToNode(GraphNode value);

} // ResponseArc
