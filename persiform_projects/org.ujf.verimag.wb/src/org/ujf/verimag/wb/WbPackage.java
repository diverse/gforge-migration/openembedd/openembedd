/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.WbFactory
 * @model kind="package"
 * @generated
 */
public interface WbPackage extends EPackage
{
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String		eNAME									= "wb";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String		eNS_URI									= "http://verimag.ujf.org/workbench/WB";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String		eNS_PREFIX								= "wb";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	WbPackage	eINSTANCE								= org.ujf.verimag.wb.impl.WbPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.NamedElementImpl <em>Named Element</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.NamedElementImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNamedElement()
	 * @generated
	 */
	int			NAMED_ELEMENT							= 34;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NAMED_ELEMENT__NAME						= 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NAMED_ELEMENT__DESCRIPTION				= 1;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NAMED_ELEMENT_FEATURE_COUNT				= 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ModuleImpl <em>Module</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ModuleImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getModule()
	 * @generated
	 */
	int			MODULE									= 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__NAME							= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__DESCRIPTION						= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__TIME_UNIT						= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__VERSION							= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Declaration Zone</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__DECLARATION_ZONE				= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__SUBMODEL						= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Allocate</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__ALLOCATE						= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Service</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__SERVICE							= NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Resource Pool</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__RESOURCE_POOL					= NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE__BLOCK							= NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Module</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			MODULE_FEATURE_COUNT					= NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.GraphicalNodeImpl <em>Graphical Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.GraphicalNodeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getGraphicalNode()
	 * @generated
	 */
	int			GRAPHICAL_NODE							= 41;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPHICAL_NODE__NAME					= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPHICAL_NODE__DESCRIPTION				= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPHICAL_NODE__X						= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPHICAL_NODE__Y						= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Graphical Node</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPHICAL_NODE_FEATURE_COUNT			= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl <em>Declaration Zone</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.DeclarationZoneImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDeclarationZone()
	 * @generated
	 */
	int			DECLARATION_ZONE						= 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__NAME					= GRAPHICAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__DESCRIPTION			= GRAPHICAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__X						= GRAPHICAL_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__Y						= GRAPHICAL_NODE__Y;

	/**
	 * The feature id for the '<em><b>Raw Content</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__RAW_CONTENT			= GRAPHICAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__MODULE				= GRAPHICAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__SUBMODEL				= GRAPHICAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Category ID</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__CATEGORY_ID			= GRAPHICAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE__VARIABLE				= GRAPHICAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Declaration Zone</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DECLARATION_ZONE_FEATURE_COUNT			= GRAPHICAL_NODE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SubmodelImpl <em>Submodel</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SubmodelImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSubmodel()
	 * @generated
	 */
	int			SUBMODEL								= 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__NAME							= GRAPHICAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__DESCRIPTION					= GRAPHICAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__X								= GRAPHICAL_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__Y								= GRAPHICAL_NODE__Y;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__FORMAL_PARAMETERS				= GRAPHICAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__DIMENSION						= GRAPHICAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__MODULE						= GRAPHICAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Declaration Zone</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__DECLARATION_ZONE				= GRAPHICAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Response Arc</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__RESPONSE_ARC					= GRAPHICAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Arc</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__ARC							= GRAPHICAL_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__REF							= GRAPHICAL_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL__NODE							= GRAPHICAL_NODE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Submodel</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_FEATURE_COUNT					= GRAPHICAL_NODE_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.GraphNodeImpl <em>Graph Node</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.GraphNodeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getGraphNode()
	 * @generated
	 */
	int			GRAPH_NODE								= 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__NAME						= GRAPHICAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__DESCRIPTION					= GRAPHICAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__X							= GRAPHICAL_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__Y							= GRAPHICAL_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__OUT_ARC						= GRAPHICAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__IN_ARC						= GRAPHICAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__SUBMODEL					= GRAPHICAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__INTERRUPT					= GRAPHICAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__OUT_RESPONSE_ARC			= GRAPHICAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE__IN_RESPONSE_ARC				= GRAPHICAL_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Graph Node</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			GRAPH_NODE_FEATURE_COUNT				= GRAPHICAL_NODE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.DirectNodeImpl <em>Direct Node</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.DirectNodeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDirectNode()
	 * @generated
	 */
	int			DIRECT_NODE								= 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__NAME						= GRAPH_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__DESCRIPTION				= GRAPH_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__X							= GRAPH_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__Y							= GRAPH_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__OUT_ARC					= GRAPH_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__IN_ARC						= GRAPH_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__SUBMODEL					= GRAPH_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__INTERRUPT					= GRAPH_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__OUT_RESPONSE_ARC			= GRAPH_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__IN_RESPONSE_ARC			= GRAPH_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__DIMENSION					= GRAPH_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__CODE						= GRAPH_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__FORMAL_PARAMETERS			= GRAPH_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__DECLARATIONS				= GRAPH_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE__NODE_STATISTICS			= GRAPH_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Direct Node</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DIRECT_NODE_FEATURE_COUNT				= GRAPH_NODE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.NodeWithQueueImpl <em>Node With Queue</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.NodeWithQueueImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNodeWithQueue()
	 * @generated
	 */
	int			NODE_WITH_QUEUE							= 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__NAME					= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__DESCRIPTION			= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__X						= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__Y						= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__OUT_ARC				= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__IN_ARC					= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__SUBMODEL				= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__INTERRUPT				= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__OUT_RESPONSE_ARC		= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__IN_RESPONSE_ARC		= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__DIMENSION				= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__CODE					= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__FORMAL_PARAMETERS		= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__DECLARATIONS			= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__NODE_STATISTICS		= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__PRIORITY				= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Priority Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__PRIORITY_RULE			= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Time Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE__TIME_RULE				= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Node With Queue</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_WITH_QUEUE_FEATURE_COUNT			= DIRECT_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.AllocateImpl <em>Allocate</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.AllocateImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getAllocate()
	 * @generated
	 */
	int			ALLOCATE								= 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__NAME							= NODE_WITH_QUEUE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__DESCRIPTION					= NODE_WITH_QUEUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__X								= NODE_WITH_QUEUE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__Y								= NODE_WITH_QUEUE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__OUT_ARC						= NODE_WITH_QUEUE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__IN_ARC						= NODE_WITH_QUEUE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__SUBMODEL						= NODE_WITH_QUEUE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__INTERRUPT						= NODE_WITH_QUEUE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__OUT_RESPONSE_ARC				= NODE_WITH_QUEUE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__IN_RESPONSE_ARC				= NODE_WITH_QUEUE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__DIMENSION						= NODE_WITH_QUEUE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__CODE							= NODE_WITH_QUEUE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__FORMAL_PARAMETERS				= NODE_WITH_QUEUE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__DECLARATIONS					= NODE_WITH_QUEUE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__NODE_STATISTICS				= NODE_WITH_QUEUE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__PRIORITY						= NODE_WITH_QUEUE__PRIORITY;

	/**
	 * The feature id for the '<em><b>Priority Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__PRIORITY_RULE					= NODE_WITH_QUEUE__PRIORITY_RULE;

	/**
	 * The feature id for the '<em><b>Time Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__TIME_RULE						= NODE_WITH_QUEUE__TIME_RULE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__QUANTITY						= NODE_WITH_QUEUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Generate Characteristic</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__GENERATE_CHARACTERISTIC		= NODE_WITH_QUEUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource Instance Spec</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__RESOURCE_INSTANCE_SPEC		= NODE_WITH_QUEUE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__MODULE						= NODE_WITH_QUEUE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__REF							= NODE_WITH_QUEUE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Resource Instance</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE__RESOURCE_INSTANCE				= NODE_WITH_QUEUE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Allocate</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_FEATURE_COUNT					= NODE_WITH_QUEUE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ServiceImpl <em>Service</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ServiceImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getService()
	 * @generated
	 */
	int			SERVICE									= 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__NAME							= NODE_WITH_QUEUE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__DESCRIPTION					= NODE_WITH_QUEUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__X								= NODE_WITH_QUEUE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__Y								= NODE_WITH_QUEUE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__OUT_ARC						= NODE_WITH_QUEUE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__IN_ARC							= NODE_WITH_QUEUE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__SUBMODEL						= NODE_WITH_QUEUE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__INTERRUPT						= NODE_WITH_QUEUE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__OUT_RESPONSE_ARC				= NODE_WITH_QUEUE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__IN_RESPONSE_ARC				= NODE_WITH_QUEUE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__DIMENSION						= NODE_WITH_QUEUE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__CODE							= NODE_WITH_QUEUE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__FORMAL_PARAMETERS				= NODE_WITH_QUEUE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__DECLARATIONS					= NODE_WITH_QUEUE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__NODE_STATISTICS				= NODE_WITH_QUEUE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__PRIORITY						= NODE_WITH_QUEUE__PRIORITY;

	/**
	 * The feature id for the '<em><b>Priority Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__PRIORITY_RULE					= NODE_WITH_QUEUE__PRIORITY_RULE;

	/**
	 * The feature id for the '<em><b>Time Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__TIME_RULE						= NODE_WITH_QUEUE__TIME_RULE;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__TIME_UNIT						= NODE_WITH_QUEUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Service Time</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__SERVICE_TIME					= NODE_WITH_QUEUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Server Quantity</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__SERVER_QUANTITY				= NODE_WITH_QUEUE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__MODULE							= NODE_WITH_QUEUE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE__REF							= NODE_WITH_QUEUE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Service</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_FEATURE_COUNT					= NODE_WITH_QUEUE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ResourcePoolImpl <em>Resource Pool</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ResourcePoolImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResourcePool()
	 * @generated
	 */
	int			RESOURCE_POOL							= 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__NAME						= GRAPHICAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__DESCRIPTION				= GRAPHICAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__X						= GRAPHICAL_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__Y						= GRAPHICAL_NODE__Y;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__DIMENSION				= GRAPHICAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource Kind</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__RESOURCE_KIND			= GRAPHICAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Token Quantity</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__TOKEN_QUANTITY			= GRAPHICAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__MODULE					= GRAPHICAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Allocate</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__ALLOCATE					= GRAPHICAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Release</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL__RELEASE					= GRAPHICAL_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Resource Pool</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESOURCE_POOL_FEATURE_COUNT				= GRAPHICAL_NODE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ArcImpl <em>Arc</em>}' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ArcImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getArc()
	 * @generated
	 */
	int			ARC										= 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__NAME								= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__DESCRIPTION						= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Select Condition</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SELECT_CONDITION					= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Select Probability</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SELECT_PROBABILITY					= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Select Phase</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SELECT_PHASE						= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Select Parent</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SELECT_PARENT						= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dest Node Index</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__DEST_NODE_INDEX					= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Change Port</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__CHANGE_PORT						= NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Change Phase</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__CHANGE_PHASE						= NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Select Category</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SELECT_CATEGORY					= NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__SUBMODEL							= NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>From Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__FROM_NODE							= NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>To Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC__TO_NODE							= NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Arc</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ARC_FEATURE_COUNT						= NAMED_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl <em>Statistics Collector</em>}
	 * ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.StatisticsCollectorImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStatisticsCollector()
	 * @generated
	 */
	int			STATISTICS_COLLECTOR					= 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__NAME				= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__DESCRIPTION		= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Breakdown List</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__BREAKDOWN_LIST	= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Histogram Buckets</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS	= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Update Change</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__UPDATE_CHANGE		= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Update Delay</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR__UPDATE_DELAY		= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Statistics Collector</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			STATISTICS_COLLECTOR_FEATURE_COUNT		= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ResponseArcImpl <em>Response Arc</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ResponseArcImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResponseArc()
	 * @generated
	 */
	int			RESPONSE_ARC							= 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__NAME						= STATISTICS_COLLECTOR__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__DESCRIPTION				= STATISTICS_COLLECTOR__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Breakdown List</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__BREAKDOWN_LIST			= STATISTICS_COLLECTOR__BREAKDOWN_LIST;

	/**
	 * The feature id for the '<em><b>Histogram Buckets</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__HISTOGRAM_BUCKETS			= STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS;

	/**
	 * The feature id for the '<em><b>Update Change</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__UPDATE_CHANGE				= STATISTICS_COLLECTOR__UPDATE_CHANGE;

	/**
	 * The feature id for the '<em><b>Update Delay</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__UPDATE_DELAY				= STATISTICS_COLLECTOR__UPDATE_DELAY;

	/**
	 * The feature id for the '<em><b>Source Node Index</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__SOURCE_NODE_INDEX			= STATISTICS_COLLECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Destination Node Index</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__DESTINATION_NODE_INDEX	= STATISTICS_COLLECTOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__SUBMODEL					= STATISTICS_COLLECTOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__FROM_NODE					= STATISTICS_COLLECTOR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC__TO_NODE					= STATISTICS_COLLECTOR_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Response Arc</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESPONSE_ARC_FEATURE_COUNT				= STATISTICS_COLLECTOR_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.CategoryIDImpl <em>Category ID</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.CategoryIDImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getCategoryID()
	 * @generated
	 */
	int			CATEGORY_ID								= 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__NAME						= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__DESCRIPTION				= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__DIMENSION					= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__ARC						= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Declaration Zone</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__DECLARATION_ZONE			= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID__INTERRUPT					= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Category ID</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CATEGORY_ID_FEATURE_COUNT				= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.VariableImpl <em>Variable</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.VariableImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getVariable()
	 * @generated
	 */
	int			VARIABLE								= 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__NAME							= NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__DESCRIPTION					= NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__DIMENSION						= NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Storage Kind</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__STORAGE_KIND					= NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__DATATYPE						= NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__VALUE							= NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Declaration Zone</b></em>' container reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE__DECLARATION_ZONE				= NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Variable</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			VARIABLE_FEATURE_COUNT					= NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.RefNodeImpl <em>Ref Node</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.RefNodeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getRefNode()
	 * @generated
	 */
	int			REF_NODE								= 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__NAME							= GRAPH_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__DESCRIPTION					= GRAPH_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__X								= GRAPH_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__Y								= GRAPH_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__OUT_ARC						= GRAPH_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__IN_ARC						= GRAPH_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__SUBMODEL						= GRAPH_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__INTERRUPT						= GRAPH_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__OUT_RESPONSE_ARC				= GRAPH_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__IN_RESPONSE_ARC				= GRAPH_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE__ACTUAL_PARAMETERS				= GRAPH_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ref Node</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			REF_NODE_FEATURE_COUNT					= GRAPH_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SubmodelRefImpl <em>Submodel Ref</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SubmodelRefImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSubmodelRef()
	 * @generated
	 */
	int			SUBMODEL_REF							= 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__NAME						= REF_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__DESCRIPTION				= REF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__X							= REF_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__Y							= REF_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__OUT_ARC					= REF_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__IN_ARC					= REF_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__SUBMODEL					= REF_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__INTERRUPT					= REF_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__OUT_RESPONSE_ARC			= REF_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__IN_RESPONSE_ARC			= REF_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__ACTUAL_PARAMETERS			= REF_NODE__ACTUAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF__TARGET					= REF_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Submodel Ref</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUBMODEL_REF_FEATURE_COUNT				= REF_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ServiceRefImpl <em>Service Ref</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ServiceRefImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getServiceRef()
	 * @generated
	 */
	int			SERVICE_REF								= 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__NAME						= REF_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__DESCRIPTION				= REF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__X							= REF_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__Y							= REF_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__OUT_ARC					= REF_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__IN_ARC						= REF_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__SUBMODEL					= REF_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__INTERRUPT					= REF_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__OUT_RESPONSE_ARC			= REF_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__IN_RESPONSE_ARC			= REF_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__ACTUAL_PARAMETERS			= REF_NODE__ACTUAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF__TARGET						= REF_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Service Ref</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SERVICE_REF_FEATURE_COUNT				= REF_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.AllocateRefImpl <em>Allocate Ref</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.AllocateRefImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getAllocateRef()
	 * @generated
	 */
	int			ALLOCATE_REF							= 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__NAME						= REF_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__DESCRIPTION				= REF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__X							= REF_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__Y							= REF_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__OUT_ARC					= REF_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__IN_ARC					= REF_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__SUBMODEL					= REF_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__INTERRUPT					= REF_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__OUT_RESPONSE_ARC			= REF_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__IN_RESPONSE_ARC			= REF_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__ACTUAL_PARAMETERS			= REF_NODE__ACTUAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF__TARGET					= REF_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Allocate Ref</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ALLOCATE_REF_FEATURE_COUNT				= REF_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.BlockImpl <em>Block</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.BlockImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBlock()
	 * @generated
	 */
	int			BLOCK									= 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__NAME								= NODE_WITH_QUEUE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__DESCRIPTION						= NODE_WITH_QUEUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__X								= NODE_WITH_QUEUE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__Y								= NODE_WITH_QUEUE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__OUT_ARC							= NODE_WITH_QUEUE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__IN_ARC							= NODE_WITH_QUEUE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__SUBMODEL							= NODE_WITH_QUEUE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__INTERRUPT						= NODE_WITH_QUEUE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__OUT_RESPONSE_ARC					= NODE_WITH_QUEUE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__IN_RESPONSE_ARC					= NODE_WITH_QUEUE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__DIMENSION						= NODE_WITH_QUEUE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__CODE								= NODE_WITH_QUEUE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__FORMAL_PARAMETERS				= NODE_WITH_QUEUE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__DECLARATIONS						= NODE_WITH_QUEUE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__NODE_STATISTICS					= NODE_WITH_QUEUE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__PRIORITY							= NODE_WITH_QUEUE__PRIORITY;

	/**
	 * The feature id for the '<em><b>Priority Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__PRIORITY_RULE					= NODE_WITH_QUEUE__PRIORITY_RULE;

	/**
	 * The feature id for the '<em><b>Time Rule</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__TIME_RULE						= NODE_WITH_QUEUE__TIME_RULE;

	/**
	 * The feature id for the '<em><b>Block Until</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__BLOCK_UNTIL						= NODE_WITH_QUEUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>module</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__MODULE							= NODE_WITH_QUEUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK__REF								= NODE_WITH_QUEUE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Block</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_FEATURE_COUNT						= NODE_WITH_QUEUE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SourceImpl <em>Source</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SourceImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSource()
	 * @generated
	 */
	int			SOURCE									= 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Generate Characteristic</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE__GENERATE_CHARACTERISTIC			= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Source</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SOURCE_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SinkImpl <em>Sink</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SinkImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSink()
	 * @generated
	 */
	int			SINK									= 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__X									= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__Y									= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__INTERRUPT							= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__DIMENSION							= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__FORMAL_PARAMETERS					= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Sink</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SINK_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.EnterImpl <em>Enter</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.EnterImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getEnter()
	 * @generated
	 */
	int			ENTER									= 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Enter</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			ENTER_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ReturnImpl <em>Return</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ReturnImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getReturn()
	 * @generated
	 */
	int			RETURN									= 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Return</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RETURN_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.DuplicationNodeImpl <em>Duplication Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.DuplicationNodeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDuplicationNode()
	 * @generated
	 */
	int			DUPLICATION_NODE						= 39;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__NAME					= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__DESCRIPTION			= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__X						= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__Y						= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__OUT_ARC				= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__IN_ARC				= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__SUBMODEL				= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__INTERRUPT				= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__OUT_RESPONSE_ARC		= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__IN_RESPONSE_ARC		= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__DIMENSION				= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__CODE					= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__FORMAL_PARAMETERS		= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__DECLARATIONS			= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__NODE_STATISTICS		= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Inherits Calls</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__INHERITS_CALLS		= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Inherits Unshared</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE__INHERITS_UNSHARED		= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Duplication Node</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DUPLICATION_NODE_FEATURE_COUNT			= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SplitImpl <em>Split</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SplitImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSplit()
	 * @generated
	 */
	int			SPLIT									= 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__NAME								= DUPLICATION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__DESCRIPTION						= DUPLICATION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__X								= DUPLICATION_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__Y								= DUPLICATION_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__OUT_ARC							= DUPLICATION_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__IN_ARC							= DUPLICATION_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__SUBMODEL							= DUPLICATION_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__INTERRUPT						= DUPLICATION_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__OUT_RESPONSE_ARC					= DUPLICATION_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__IN_RESPONSE_ARC					= DUPLICATION_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__DIMENSION						= DUPLICATION_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__CODE								= DUPLICATION_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__FORMAL_PARAMETERS				= DUPLICATION_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__DECLARATIONS						= DUPLICATION_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__NODE_STATISTICS					= DUPLICATION_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Inherits Calls</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__INHERITS_CALLS					= DUPLICATION_NODE__INHERITS_CALLS;

	/**
	 * The feature id for the '<em><b>Inherits Unshared</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT__INHERITS_UNSHARED				= DUPLICATION_NODE__INHERITS_UNSHARED;

	/**
	 * The number of structural features of the '<em>Split</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SPLIT_FEATURE_COUNT						= DUPLICATION_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ForkImpl <em>Fork</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ForkImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getFork()
	 * @generated
	 */
	int			FORK									= 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__NAME								= DUPLICATION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__DESCRIPTION						= DUPLICATION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__X									= DUPLICATION_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__Y									= DUPLICATION_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__OUT_ARC							= DUPLICATION_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__IN_ARC							= DUPLICATION_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__SUBMODEL							= DUPLICATION_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__INTERRUPT							= DUPLICATION_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__OUT_RESPONSE_ARC					= DUPLICATION_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__IN_RESPONSE_ARC					= DUPLICATION_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__DIMENSION							= DUPLICATION_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__CODE								= DUPLICATION_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__FORMAL_PARAMETERS					= DUPLICATION_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__DECLARATIONS						= DUPLICATION_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__NODE_STATISTICS					= DUPLICATION_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Inherits Calls</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__INHERITS_CALLS					= DUPLICATION_NODE__INHERITS_CALLS;

	/**
	 * The feature id for the '<em><b>Inherits Unshared</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK__INHERITS_UNSHARED					= DUPLICATION_NODE__INHERITS_UNSHARED;

	/**
	 * The number of structural features of the '<em>Fork</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FORK_FEATURE_COUNT						= DUPLICATION_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.JoinImpl <em>Join</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.JoinImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getJoin()
	 * @generated
	 */
	int			JOIN									= 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__X									= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__Y									= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__INTERRUPT							= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__DIMENSION							= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__FORMAL_PARAMETERS					= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Join</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			JOIN_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.UserImpl <em>User</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.UserImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getUser()
	 * @generated
	 */
	int			USER									= 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__X									= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__Y									= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__INTERRUPT							= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__DIMENSION							= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__FORMAL_PARAMETERS					= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>User</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			USER_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.DelayImpl <em>Delay</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.DelayImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDelay()
	 * @generated
	 */
	int			DELAY									= 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__TIME_UNIT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Delay Time</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__DELAY_TIME						= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Generate Characteristic</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY__GENERATE_CHARACTERISTIC			= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Delay</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			DELAY_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ReleaseImpl <em>Release</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ReleaseImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getRelease()
	 * @generated
	 */
	int			RELEASE									= 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__DESCRIPTION					= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__OUT_ARC						= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__IN_RESPONSE_ARC				= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__NODE_STATISTICS				= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Generate Characteristic</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__GENERATE_CHARACTERISTIC		= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__QUANTITY						= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource Instance Spec</b></em>' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__RESOURCE_INSTANCE_SPEC			= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Resource Instance</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE__RESOURCE_INSTANCE				= DIRECT_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Release</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RELEASE_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.InterruptImpl <em>Interrupt</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.InterruptImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getInterrupt()
	 * @generated
	 */
	int			INTERRUPT								= 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__DESCRIPTION					= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__X							= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__Y							= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__OUT_ARC						= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__IN_ARC						= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__INTERRUPT					= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__IN_RESPONSE_ARC				= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__DIMENSION					= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__FORMAL_PARAMETERS			= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__NODE_STATISTICS				= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Select Condition</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__SELECT_CONDITION				= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Select Quantity</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__SELECT_QUANTITY				= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Select Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__SELECT_NODE					= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Select Category</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT__SELECT_CATEGORY				= DIRECT_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Interrupt</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			INTERRUPT_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.LoopImpl <em>Loop</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.LoopImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getLoop()
	 * @generated
	 */
	int			LOOP									= 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__X									= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__Y									= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__INTERRUPT							= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__DIMENSION							= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__FORMAL_PARAMETERS					= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Loop Start</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__LOOP_START						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop End</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP__LOOP_END							= DIRECT_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Loop</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			LOOP_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ForLoopImpl <em>For Loop</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ForLoopImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getForLoop()
	 * @generated
	 */
	int			FOR_LOOP								= 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__NAME							= LOOP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__DESCRIPTION					= LOOP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__X								= LOOP__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__Y								= LOOP__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__OUT_ARC						= LOOP__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__IN_ARC						= LOOP__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__SUBMODEL						= LOOP__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__INTERRUPT						= LOOP__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__OUT_RESPONSE_ARC				= LOOP__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__IN_RESPONSE_ARC				= LOOP__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__DIMENSION						= LOOP__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__CODE							= LOOP__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__FORMAL_PARAMETERS				= LOOP__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__DECLARATIONS					= LOOP__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__NODE_STATISTICS				= LOOP__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Loop Start</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__LOOP_START					= LOOP__LOOP_START;

	/**
	 * The feature id for the '<em><b>Loop End</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__LOOP_END						= LOOP__LOOP_END;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP__COUNT							= LOOP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>For Loop</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			FOR_LOOP_FEATURE_COUNT					= LOOP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ConditionalLoopImpl <em>Conditional Loop</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ConditionalLoopImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getConditionalLoop()
	 * @generated
	 */
	int			CONDITIONAL_LOOP						= 31;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__NAME					= LOOP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__DESCRIPTION			= LOOP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__X						= LOOP__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__Y						= LOOP__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__OUT_ARC				= LOOP__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__IN_ARC				= LOOP__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__SUBMODEL				= LOOP__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__INTERRUPT				= LOOP__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__OUT_RESPONSE_ARC		= LOOP__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__IN_RESPONSE_ARC		= LOOP__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__DIMENSION				= LOOP__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__CODE					= LOOP__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__FORMAL_PARAMETERS		= LOOP__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__DECLARATIONS			= LOOP__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__NODE_STATISTICS		= LOOP__NODE_STATISTICS;

	/**
	 * The feature id for the '<em><b>Loop Start</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__LOOP_START			= LOOP__LOOP_START;

	/**
	 * The feature id for the '<em><b>Loop End</b></em>' containment reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__LOOP_END				= LOOP__LOOP_END;

	/**
	 * The feature id for the '<em><b>Is While</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__IS_WHILE				= LOOP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP__CONDITION				= LOOP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conditional Loop</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			CONDITIONAL_LOOP_FEATURE_COUNT			= LOOP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.NodeStatisticsImpl <em>Node Statistics</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.NodeStatisticsImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNodeStatistics()
	 * @generated
	 */
	int			NODE_STATISTICS							= 33;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__NAME					= STATISTICS_COLLECTOR__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__DESCRIPTION			= STATISTICS_COLLECTOR__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Breakdown List</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__BREAKDOWN_LIST			= STATISTICS_COLLECTOR__BREAKDOWN_LIST;

	/**
	 * The feature id for the '<em><b>Histogram Buckets</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__HISTOGRAM_BUCKETS		= STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS;

	/**
	 * The feature id for the '<em><b>Update Change</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__UPDATE_CHANGE			= STATISTICS_COLLECTOR__UPDATE_CHANGE;

	/**
	 * The feature id for the '<em><b>Update Delay</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__UPDATE_DELAY			= STATISTICS_COLLECTOR__UPDATE_DELAY;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__DIMENSION				= STATISTICS_COLLECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__KIND					= STATISTICS_COLLECTOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direct Node</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS__DIRECT_NODE			= STATISTICS_COLLECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Node Statistics</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			NODE_STATISTICS_FEATURE_COUNT			= STATISTICS_COLLECTOR_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.TimedElementImpl <em>Timed Element</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.TimedElementImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimedElement()
	 * @generated
	 */
	int			TIMED_ELEMENT							= 35;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			TIMED_ELEMENT__TIME_UNIT				= 0;

	/**
	 * The number of structural features of the '<em>Timed Element</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			TIMED_ELEMENT_FEATURE_COUNT				= 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.SuperImpl <em>Super</em>}' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.SuperImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSuper()
	 * @generated
	 */
	int			SUPER									= 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__NAME								= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__SUBMODEL							= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__OUT_RESPONSE_ARC					= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__CODE								= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__DECLARATIONS						= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Super</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			SUPER_FEATURE_COUNT						= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.ResumeImpl <em>Resume</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.ResumeImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResume()
	 * @generated
	 */
	int			RESUME									= 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Resume</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			RESUME_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.BranchImpl <em>Branch</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.BranchImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBranch()
	 * @generated
	 */
	int			BRANCH									= 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__NAME							= DIRECT_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__DESCRIPTION						= DIRECT_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__X								= DIRECT_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__Y								= DIRECT_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__OUT_ARC							= DIRECT_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__IN_ARC							= DIRECT_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__SUBMODEL						= DIRECT_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__INTERRUPT						= DIRECT_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__OUT_RESPONSE_ARC				= DIRECT_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__IN_RESPONSE_ARC					= DIRECT_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__DIMENSION						= DIRECT_NODE__DIMENSION;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__CODE							= DIRECT_NODE__CODE;

	/**
	 * The feature id for the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__FORMAL_PARAMETERS				= DIRECT_NODE__FORMAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__DECLARATIONS					= DIRECT_NODE__DECLARATIONS;

	/**
	 * The feature id for the '<em><b>Node Statistics</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH__NODE_STATISTICS					= DIRECT_NODE__NODE_STATISTICS;

	/**
	 * The number of structural features of the '<em>Branch</em>' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BRANCH_FEATURE_COUNT					= DIRECT_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.impl.BlockRefImpl <em>Block Ref</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.impl.BlockRefImpl
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBlockRef()
	 * @generated
	 */
	int			BLOCK_REF								= 40;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__NAME							= REF_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__DESCRIPTION					= REF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__X							= REF_NODE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__Y							= REF_NODE__Y;

	/**
	 * The feature id for the '<em><b>Out Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__OUT_ARC						= REF_NODE__OUT_ARC;

	/**
	 * The feature id for the '<em><b>In Arc</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__IN_ARC						= REF_NODE__IN_ARC;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' container reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__SUBMODEL						= REF_NODE__SUBMODEL;

	/**
	 * The feature id for the '<em><b>Interrupt</b></em>' reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__INTERRUPT					= REF_NODE__INTERRUPT;

	/**
	 * The feature id for the '<em><b>Out Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__OUT_RESPONSE_ARC				= REF_NODE__OUT_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>In Response Arc</b></em>' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__IN_RESPONSE_ARC				= REF_NODE__IN_RESPONSE_ARC;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__ACTUAL_PARAMETERS			= REF_NODE__ACTUAL_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF__TARGET						= REF_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Block Ref</em>' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 * @ordered
	 */
	int			BLOCK_REF_FEATURE_COUNT					= REF_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.StorageKind <em>Storage Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.StorageKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStorageKind()
	 * @generated
	 */
	int			STORAGE_KIND							= 42;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.DataTypeKind <em>Data Type Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.DataTypeKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDataTypeKind()
	 * @generated
	 */
	int			DATA_TYPE_KIND							= 43;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.PriorityRuleKind <em>Priority Rule Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.PriorityRuleKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getPriorityRuleKind()
	 * @generated
	 */
	int			PRIORITY_RULE_KIND						= 44;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.TimeRuleKind <em>Time Rule Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.TimeRuleKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimeRuleKind()
	 * @generated
	 */
	int			TIME_RULE_KIND							= 45;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.ResourceKind <em>Resource Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.ResourceKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResourceKind()
	 * @generated
	 */
	int			RESOURCE_KIND							= 46;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.StatClassKind <em>Stat Class Kind</em>}' enum. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.StatClassKind
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStatClassKind()
	 * @generated
	 */
	int			STAT_CLASS_KIND							= 47;

	/**
	 * The meta object id for the '{@link org.ujf.verimag.wb.TimeUnit <em>Time Unit</em>}' enum. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see org.ujf.verimag.wb.TimeUnit
	 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimeUnit()
	 * @generated
	 */
	int			TIME_UNIT								= 48;

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Module <em>Module</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Module</em>'.
	 * @see org.ujf.verimag.wb.Module
	 * @generated
	 */
	EClass getModule();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Module#getVersion <em>Version</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.ujf.verimag.wb.Module#getVersion()
	 * @see #getModule()
	 * @generated
	 */
	EAttribute getModule_Version();

	/**
	 * Returns the meta object for the containment reference '{@link org.ujf.verimag.wb.Module#getDeclarationZone
	 * <em>Declaration Zone</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Declaration Zone</em>'.
	 * @see org.ujf.verimag.wb.Module#getDeclarationZone()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_DeclarationZone();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Module#getSubmodel
	 * <em>Submodel</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.Module#getSubmodel()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_Submodel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Module#getAllocate
	 * <em>Allocate</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Allocate</em>'.
	 * @see org.ujf.verimag.wb.Module#getAllocate()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_Allocate();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Module#getService
	 * <em>Service</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Service</em>'.
	 * @see org.ujf.verimag.wb.Module#getService()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_Service();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Module#getResourcePool
	 * <em>Resource Pool</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Resource Pool</em>'.
	 * @see org.ujf.verimag.wb.Module#getResourcePool()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_ResourcePool();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Module#getBlock
	 * <em>Block</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Block</em>'.
	 * @see org.ujf.verimag.wb.Module#getBlock()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_Block();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.DeclarationZone <em>Declaration Zone</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Declaration Zone</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone
	 * @generated
	 */
	EClass getDeclarationZone();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DeclarationZone#getRawContent
	 * <em>Raw Content</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Raw Content</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone#getRawContent()
	 * @see #getDeclarationZone()
	 * @generated
	 */
	EAttribute getDeclarationZone_RawContent();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.DeclarationZone#get_module
	 * <em>module</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone#get_module()
	 * @see #getDeclarationZone()
	 * @generated
	 */
	EReference getDeclarationZone__module();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.DeclarationZone#getSubmodel
	 * <em>Submodel</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone#getSubmodel()
	 * @see #getDeclarationZone()
	 * @generated
	 */
	EReference getDeclarationZone_Submodel();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.wb.DeclarationZone#getCategoryID <em>Category ID</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Category ID</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone#getCategoryID()
	 * @see #getDeclarationZone()
	 * @generated
	 */
	EReference getDeclarationZone_CategoryID();

	/**
	 * Returns the meta object for the containment reference list '
	 * {@link org.ujf.verimag.wb.DeclarationZone#getVariable <em>Variable</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Variable</em>'.
	 * @see org.ujf.verimag.wb.DeclarationZone#getVariable()
	 * @see #getDeclarationZone()
	 * @generated
	 */
	EReference getDeclarationZone_Variable();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Submodel <em>Submodel</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.Submodel
	 * @generated
	 */
	EClass getSubmodel();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Submodel#getFormalParameters
	 * <em>Formal Parameters</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Formal Parameters</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getFormalParameters()
	 * @see #getSubmodel()
	 * @generated
	 */
	EAttribute getSubmodel_FormalParameters();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Submodel#getDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getDimension()
	 * @see #getSubmodel()
	 * @generated
	 */
	EAttribute getSubmodel_Dimension();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Submodel#get_module
	 * <em>module</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.Submodel#get_module()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel__module();

	/**
	 * Returns the meta object for the containment reference '{@link org.ujf.verimag.wb.Submodel#getDeclarationZone
	 * <em>Declaration Zone</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Declaration Zone</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getDeclarationZone()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_DeclarationZone();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Submodel#getResponseArc
	 * <em>Response Arc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Response Arc</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getResponseArc()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_ResponseArc();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Submodel#getArc
	 * <em>Arc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Arc</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getArc()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_Arc();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.Submodel#getRef <em>Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Ref</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getRef()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_Ref();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ujf.verimag.wb.Submodel#getNode
	 * <em>Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference list '<em>Node</em>'.
	 * @see org.ujf.verimag.wb.Submodel#getNode()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_Node();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Allocate <em>Allocate</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Allocate</em>'.
	 * @see org.ujf.verimag.wb.Allocate
	 * @generated
	 */
	EClass getAllocate();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Allocate#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see org.ujf.verimag.wb.Allocate#getQuantity()
	 * @see #getAllocate()
	 * @generated
	 */
	EAttribute getAllocate_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Allocate#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Generate Characteristic</em>'.
	 * @see org.ujf.verimag.wb.Allocate#getGenerateCharacteristic()
	 * @see #getAllocate()
	 * @generated
	 */
	EAttribute getAllocate_GenerateCharacteristic();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Allocate#getResourceInstanceSpec
	 * <em>Resource Instance Spec</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Resource Instance Spec</em>'.
	 * @see org.ujf.verimag.wb.Allocate#getResourceInstanceSpec()
	 * @see #getAllocate()
	 * @generated
	 */
	EAttribute getAllocate_ResourceInstanceSpec();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Allocate#get_module
	 * <em>module</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.Allocate#get_module()
	 * @see #getAllocate()
	 * @generated
	 */
	EReference getAllocate__module();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.Allocate#getRef <em>Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Ref</em>'.
	 * @see org.ujf.verimag.wb.Allocate#getRef()
	 * @see #getAllocate()
	 * @generated
	 */
	EReference getAllocate_Ref();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Allocate#getResourceInstance
	 * <em>Resource Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Resource Instance</em>'.
	 * @see org.ujf.verimag.wb.Allocate#getResourceInstance()
	 * @see #getAllocate()
	 * @generated
	 */
	EReference getAllocate_ResourceInstance();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Service <em>Service</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Service</em>'.
	 * @see org.ujf.verimag.wb.Service
	 * @generated
	 */
	EClass getService();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Service#getServiceTime
	 * <em>Service Time</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Service Time</em>'.
	 * @see org.ujf.verimag.wb.Service#getServiceTime()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_ServiceTime();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Service#getServerQuantity
	 * <em>Server Quantity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Server Quantity</em>'.
	 * @see org.ujf.verimag.wb.Service#getServerQuantity()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_ServerQuantity();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Service#get_module
	 * <em>module</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.Service#get_module()
	 * @see #getService()
	 * @generated
	 */
	EReference getService__module();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.Service#getRef <em>Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Ref</em>'.
	 * @see org.ujf.verimag.wb.Service#getRef()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Ref();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.ResourcePool <em>Resource Pool</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Resource Pool</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool
	 * @generated
	 */
	EClass getResourcePool();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ResourcePool#getDimension
	 * <em>Dimension</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#getDimension()
	 * @see #getResourcePool()
	 * @generated
	 */
	EAttribute getResourcePool_Dimension();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ResourcePool#getResourceKind
	 * <em>Resource Kind</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Resource Kind</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#getResourceKind()
	 * @see #getResourcePool()
	 * @generated
	 */
	EAttribute getResourcePool_ResourceKind();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ResourcePool#getTokenQuantity
	 * <em>Token Quantity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Token Quantity</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#getTokenQuantity()
	 * @see #getResourcePool()
	 * @generated
	 */
	EAttribute getResourcePool_TokenQuantity();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.ResourcePool#get_module
	 * <em>module</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#get_module()
	 * @see #getResourcePool()
	 * @generated
	 */
	EReference getResourcePool__module();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.ResourcePool#getAllocate
	 * <em>Allocate</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Allocate</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#getAllocate()
	 * @see #getResourcePool()
	 * @generated
	 */
	EReference getResourcePool_Allocate();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.ResourcePool#getRelease
	 * <em>Release</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Release</em>'.
	 * @see org.ujf.verimag.wb.ResourcePool#getRelease()
	 * @see #getResourcePool()
	 * @generated
	 */
	EReference getResourcePool_Release();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.GraphNode <em>Graph Node</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Graph Node</em>'.
	 * @see org.ujf.verimag.wb.GraphNode
	 * @generated
	 */
	EClass getGraphNode();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.GraphNode#getOutArc <em>Out Arc</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Out Arc</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getOutArc()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_OutArc();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.GraphNode#getInArc <em>In Arc</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>In Arc</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getInArc()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_InArc();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.GraphNode#getSubmodel
	 * <em>Submodel</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getSubmodel()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_Submodel();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.GraphNode#getInterrupt
	 * <em>Interrupt</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Interrupt</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getInterrupt()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_Interrupt();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.GraphNode#getOutResponseArc
	 * <em>Out Response Arc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Out Response Arc</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getOutResponseArc()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_OutResponseArc();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.GraphNode#getInResponseArc
	 * <em>In Response Arc</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>In Response Arc</em>'.
	 * @see org.ujf.verimag.wb.GraphNode#getInResponseArc()
	 * @see #getGraphNode()
	 * @generated
	 */
	EReference getGraphNode_InResponseArc();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Arc <em>Arc</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Arc</em>'.
	 * @see org.ujf.verimag.wb.Arc
	 * @generated
	 */
	EClass getArc();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getSelectCondition
	 * <em>Select Condition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Condition</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSelectCondition()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_SelectCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getSelectProbability
	 * <em>Select Probability</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Probability</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSelectProbability()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_SelectProbability();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getSelectPhase <em>Select Phase</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Phase</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSelectPhase()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_SelectPhase();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getSelectParent <em>Select Parent</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Parent</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSelectParent()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_SelectParent();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getDestNodeIndex
	 * <em>Dest Node Index</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dest Node Index</em>'.
	 * @see org.ujf.verimag.wb.Arc#getDestNodeIndex()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_DestNodeIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getChangePort <em>Change Port</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Change Port</em>'.
	 * @see org.ujf.verimag.wb.Arc#getChangePort()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_ChangePort();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Arc#getChangePhase <em>Change Phase</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Change Phase</em>'.
	 * @see org.ujf.verimag.wb.Arc#getChangePhase()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_ChangePhase();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Arc#getSelectCategory
	 * <em>Select Category</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Select Category</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSelectCategory()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_SelectCategory();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Arc#getSubmodel <em>Submodel</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.Arc#getSubmodel()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_Submodel();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Arc#getFromNode <em>From Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>From Node</em>'.
	 * @see org.ujf.verimag.wb.Arc#getFromNode()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_FromNode();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Arc#getToNode <em>To Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>To Node</em>'.
	 * @see org.ujf.verimag.wb.Arc#getToNode()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_ToNode();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.ResponseArc <em>Response Arc</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Response Arc</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc
	 * @generated
	 */
	EClass getResponseArc();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ResponseArc#getSourceNodeIndex
	 * <em>Source Node Index</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Source Node Index</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc#getSourceNodeIndex()
	 * @see #getResponseArc()
	 * @generated
	 */
	EAttribute getResponseArc_SourceNodeIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ResponseArc#getDestinationNodeIndex
	 * <em>Destination Node Index</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Destination Node Index</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc#getDestinationNodeIndex()
	 * @see #getResponseArc()
	 * @generated
	 */
	EAttribute getResponseArc_DestinationNodeIndex();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.ResponseArc#getSubmodel
	 * <em>Submodel</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Submodel</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc#getSubmodel()
	 * @see #getResponseArc()
	 * @generated
	 */
	EReference getResponseArc_Submodel();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.ResponseArc#getFromNode <em>From Node</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>From Node</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc#getFromNode()
	 * @see #getResponseArc()
	 * @generated
	 */
	EReference getResponseArc_FromNode();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.ResponseArc#getToNode <em>To Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>To Node</em>'.
	 * @see org.ujf.verimag.wb.ResponseArc#getToNode()
	 * @see #getResponseArc()
	 * @generated
	 */
	EReference getResponseArc_ToNode();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.CategoryID <em>Category ID</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Category ID</em>'.
	 * @see org.ujf.verimag.wb.CategoryID
	 * @generated
	 */
	EClass getCategoryID();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.CategoryID#getDimension <em>Dimension</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.CategoryID#getDimension()
	 * @see #getCategoryID()
	 * @generated
	 */
	EAttribute getCategoryID_Dimension();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.CategoryID#getArc <em>Arc</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Arc</em>'.
	 * @see org.ujf.verimag.wb.CategoryID#getArc()
	 * @see #getCategoryID()
	 * @generated
	 */
	EReference getCategoryID_Arc();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.CategoryID#getDeclarationZone
	 * <em>Declaration Zone</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Declaration Zone</em>'.
	 * @see org.ujf.verimag.wb.CategoryID#getDeclarationZone()
	 * @see #getCategoryID()
	 * @generated
	 */
	EReference getCategoryID_DeclarationZone();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.CategoryID#getInterrupt
	 * <em>Interrupt</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Interrupt</em>'.
	 * @see org.ujf.verimag.wb.CategoryID#getInterrupt()
	 * @see #getCategoryID()
	 * @generated
	 */
	EReference getCategoryID_Interrupt();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Variable <em>Variable</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.ujf.verimag.wb.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Variable#getDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.Variable#getDimension()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Dimension();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Variable#getStorageKind
	 * <em>Storage Kind</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Storage Kind</em>'.
	 * @see org.ujf.verimag.wb.Variable#getStorageKind()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_StorageKind();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Variable#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Datatype</em>'.
	 * @see org.ujf.verimag.wb.Variable#getDatatype()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Datatype();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Variable#getValue <em>Value</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.ujf.verimag.wb.Variable#getValue()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Value();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Variable#getDeclarationZone
	 * <em>Declaration Zone</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>Declaration Zone</em>'.
	 * @see org.ujf.verimag.wb.Variable#getDeclarationZone()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_DeclarationZone();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.DirectNode <em>Direct Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Direct Node</em>'.
	 * @see org.ujf.verimag.wb.DirectNode
	 * @generated
	 */
	EClass getDirectNode();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DirectNode#getDimension <em>Dimension</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.DirectNode#getDimension()
	 * @see #getDirectNode()
	 * @generated
	 */
	EAttribute getDirectNode_Dimension();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DirectNode#getCode <em>Code</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see org.ujf.verimag.wb.DirectNode#getCode()
	 * @see #getDirectNode()
	 * @generated
	 */
	EAttribute getDirectNode_Code();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DirectNode#getFormalParameters
	 * <em>Formal Parameters</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Formal Parameters</em>'.
	 * @see org.ujf.verimag.wb.DirectNode#getFormalParameters()
	 * @see #getDirectNode()
	 * @generated
	 */
	EAttribute getDirectNode_FormalParameters();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DirectNode#getDeclarations
	 * <em>Declarations</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Declarations</em>'.
	 * @see org.ujf.verimag.wb.DirectNode#getDeclarations()
	 * @see #getDirectNode()
	 * @generated
	 */
	EAttribute getDirectNode_Declarations();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.DirectNode#getNodeStatistics
	 * <em>Node Statistics</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Node Statistics</em>'.
	 * @see org.ujf.verimag.wb.DirectNode#getNodeStatistics()
	 * @see #getDirectNode()
	 * @generated
	 */
	EReference getDirectNode_NodeStatistics();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.RefNode <em>Ref Node</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Ref Node</em>'.
	 * @see org.ujf.verimag.wb.RefNode
	 * @generated
	 */
	EClass getRefNode();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.RefNode#getActualParameters
	 * <em>Actual Parameters</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Actual Parameters</em>'.
	 * @see org.ujf.verimag.wb.RefNode#getActualParameters()
	 * @see #getRefNode()
	 * @generated
	 */
	EAttribute getRefNode_ActualParameters();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.SubmodelRef <em>Submodel Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Submodel Ref</em>'.
	 * @see org.ujf.verimag.wb.SubmodelRef
	 * @generated
	 */
	EClass getSubmodelRef();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.SubmodelRef#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.ujf.verimag.wb.SubmodelRef#getTarget()
	 * @see #getSubmodelRef()
	 * @generated
	 */
	EReference getSubmodelRef_Target();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.ServiceRef <em>Service Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Service Ref</em>'.
	 * @see org.ujf.verimag.wb.ServiceRef
	 * @generated
	 */
	EClass getServiceRef();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.ServiceRef#getTarget <em>Target</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.ujf.verimag.wb.ServiceRef#getTarget()
	 * @see #getServiceRef()
	 * @generated
	 */
	EReference getServiceRef_Target();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.AllocateRef <em>Allocate Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Allocate Ref</em>'.
	 * @see org.ujf.verimag.wb.AllocateRef
	 * @generated
	 */
	EClass getAllocateRef();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.AllocateRef#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.ujf.verimag.wb.AllocateRef#getTarget()
	 * @see #getAllocateRef()
	 * @generated
	 */
	EReference getAllocateRef_Target();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.NodeWithQueue <em>Node With Queue</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Node With Queue</em>'.
	 * @see org.ujf.verimag.wb.NodeWithQueue
	 * @generated
	 */
	EClass getNodeWithQueue();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NodeWithQueue#getPriority <em>Priority</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see org.ujf.verimag.wb.NodeWithQueue#getPriority()
	 * @see #getNodeWithQueue()
	 * @generated
	 */
	EAttribute getNodeWithQueue_Priority();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NodeWithQueue#getPriorityRule
	 * <em>Priority Rule</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Priority Rule</em>'.
	 * @see org.ujf.verimag.wb.NodeWithQueue#getPriorityRule()
	 * @see #getNodeWithQueue()
	 * @generated
	 */
	EAttribute getNodeWithQueue_PriorityRule();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NodeWithQueue#getTimeRule
	 * <em>Time Rule</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Time Rule</em>'.
	 * @see org.ujf.verimag.wb.NodeWithQueue#getTimeRule()
	 * @see #getNodeWithQueue()
	 * @generated
	 */
	EAttribute getNodeWithQueue_TimeRule();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Block <em>Block</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Block</em>'.
	 * @see org.ujf.verimag.wb.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Block#getBlockUntil <em>Block Until</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Block Until</em>'.
	 * @see org.ujf.verimag.wb.Block#getBlockUntil()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_BlockUntil();

	/**
	 * Returns the meta object for the container reference '{@link org.ujf.verimag.wb.Block#get_module <em>module</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the container reference '<em>module</em>'.
	 * @see org.ujf.verimag.wb.Block#get_module()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock__module();

	/**
	 * Returns the meta object for the reference list '{@link org.ujf.verimag.wb.Block#getRef <em>Ref</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Ref</em>'.
	 * @see org.ujf.verimag.wb.Block#getRef()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Ref();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Source <em>Source</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Source</em>'.
	 * @see org.ujf.verimag.wb.Source
	 * @generated
	 */
	EClass getSource();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Source#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Generate Characteristic</em>'.
	 * @see org.ujf.verimag.wb.Source#getGenerateCharacteristic()
	 * @see #getSource()
	 * @generated
	 */
	EAttribute getSource_GenerateCharacteristic();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Sink <em>Sink</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Sink</em>'.
	 * @see org.ujf.verimag.wb.Sink
	 * @generated
	 */
	EClass getSink();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Enter <em>Enter</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Enter</em>'.
	 * @see org.ujf.verimag.wb.Enter
	 * @generated
	 */
	EClass getEnter();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Return <em>Return</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Return</em>'.
	 * @see org.ujf.verimag.wb.Return
	 * @generated
	 */
	EClass getReturn();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Split <em>Split</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Split</em>'.
	 * @see org.ujf.verimag.wb.Split
	 * @generated
	 */
	EClass getSplit();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Fork <em>Fork</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Fork</em>'.
	 * @see org.ujf.verimag.wb.Fork
	 * @generated
	 */
	EClass getFork();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Join <em>Join</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Join</em>'.
	 * @see org.ujf.verimag.wb.Join
	 * @generated
	 */
	EClass getJoin();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.User <em>User</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>User</em>'.
	 * @see org.ujf.verimag.wb.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Delay <em>Delay</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Delay</em>'.
	 * @see org.ujf.verimag.wb.Delay
	 * @generated
	 */
	EClass getDelay();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Delay#getDelayTime <em>Delay Time</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Delay Time</em>'.
	 * @see org.ujf.verimag.wb.Delay#getDelayTime()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_DelayTime();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Delay#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Generate Characteristic</em>'.
	 * @see org.ujf.verimag.wb.Delay#getGenerateCharacteristic()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_GenerateCharacteristic();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Release <em>Release</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Release</em>'.
	 * @see org.ujf.verimag.wb.Release
	 * @generated
	 */
	EClass getRelease();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Release#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Generate Characteristic</em>'.
	 * @see org.ujf.verimag.wb.Release#getGenerateCharacteristic()
	 * @see #getRelease()
	 * @generated
	 */
	EAttribute getRelease_GenerateCharacteristic();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Release#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see org.ujf.verimag.wb.Release#getQuantity()
	 * @see #getRelease()
	 * @generated
	 */
	EAttribute getRelease_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Release#getResourceInstanceSpec
	 * <em>Resource Instance Spec</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Resource Instance Spec</em>'.
	 * @see org.ujf.verimag.wb.Release#getResourceInstanceSpec()
	 * @see #getRelease()
	 * @generated
	 */
	EAttribute getRelease_ResourceInstanceSpec();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Release#getResourceInstance
	 * <em>Resource Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Resource Instance</em>'.
	 * @see org.ujf.verimag.wb.Release#getResourceInstance()
	 * @see #getRelease()
	 * @generated
	 */
	EReference getRelease_ResourceInstance();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Interrupt <em>Interrupt</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Interrupt</em>'.
	 * @see org.ujf.verimag.wb.Interrupt
	 * @generated
	 */
	EClass getInterrupt();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Interrupt#getSelectCondition
	 * <em>Select Condition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Condition</em>'.
	 * @see org.ujf.verimag.wb.Interrupt#getSelectCondition()
	 * @see #getInterrupt()
	 * @generated
	 */
	EAttribute getInterrupt_SelectCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.Interrupt#getSelectQuantity
	 * <em>Select Quantity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Select Quantity</em>'.
	 * @see org.ujf.verimag.wb.Interrupt#getSelectQuantity()
	 * @see #getInterrupt()
	 * @generated
	 */
	EAttribute getInterrupt_SelectQuantity();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Interrupt#getSelectNode
	 * <em>Select Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Select Node</em>'.
	 * @see org.ujf.verimag.wb.Interrupt#getSelectNode()
	 * @see #getInterrupt()
	 * @generated
	 */
	EReference getInterrupt_SelectNode();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.Interrupt#getSelectCategory
	 * <em>Select Category</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Select Category</em>'.
	 * @see org.ujf.verimag.wb.Interrupt#getSelectCategory()
	 * @see #getInterrupt()
	 * @generated
	 */
	EReference getInterrupt_SelectCategory();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Loop <em>Loop</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Loop</em>'.
	 * @see org.ujf.verimag.wb.Loop
	 * @generated
	 */
	EClass getLoop();

	/**
	 * Returns the meta object for the containment reference '{@link org.ujf.verimag.wb.Loop#getLoopStart
	 * <em>Loop Start</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Loop Start</em>'.
	 * @see org.ujf.verimag.wb.Loop#getLoopStart()
	 * @see #getLoop()
	 * @generated
	 */
	EReference getLoop_LoopStart();

	/**
	 * Returns the meta object for the containment reference '{@link org.ujf.verimag.wb.Loop#getLoopEnd
	 * <em>Loop End</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the containment reference '<em>Loop End</em>'.
	 * @see org.ujf.verimag.wb.Loop#getLoopEnd()
	 * @see #getLoop()
	 * @generated
	 */
	EReference getLoop_LoopEnd();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.ForLoop <em>For Loop</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>For Loop</em>'.
	 * @see org.ujf.verimag.wb.ForLoop
	 * @generated
	 */
	EClass getForLoop();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ForLoop#getCount <em>Count</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see org.ujf.verimag.wb.ForLoop#getCount()
	 * @see #getForLoop()
	 * @generated
	 */
	EAttribute getForLoop_Count();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.ConditionalLoop <em>Conditional Loop</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Conditional Loop</em>'.
	 * @see org.ujf.verimag.wb.ConditionalLoop
	 * @generated
	 */
	EClass getConditionalLoop();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ConditionalLoop#getIsWhile
	 * <em>Is While</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Is While</em>'.
	 * @see org.ujf.verimag.wb.ConditionalLoop#getIsWhile()
	 * @see #getConditionalLoop()
	 * @generated
	 */
	EAttribute getConditionalLoop_IsWhile();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.ConditionalLoop#getCondition
	 * <em>Condition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see org.ujf.verimag.wb.ConditionalLoop#getCondition()
	 * @see #getConditionalLoop()
	 * @generated
	 */
	EAttribute getConditionalLoop_Condition();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.StatisticsCollector <em>Statistics Collector</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Statistics Collector</em>'.
	 * @see org.ujf.verimag.wb.StatisticsCollector
	 * @generated
	 */
	EClass getStatisticsCollector();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.StatisticsCollector#getBreakdownList
	 * <em>Breakdown List</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Breakdown List</em>'.
	 * @see org.ujf.verimag.wb.StatisticsCollector#getBreakdownList()
	 * @see #getStatisticsCollector()
	 * @generated
	 */
	EAttribute getStatisticsCollector_BreakdownList();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.StatisticsCollector#getHistogramBuckets
	 * <em>Histogram Buckets</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Histogram Buckets</em>'.
	 * @see org.ujf.verimag.wb.StatisticsCollector#getHistogramBuckets()
	 * @see #getStatisticsCollector()
	 * @generated
	 */
	EAttribute getStatisticsCollector_HistogramBuckets();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateChange
	 * <em>Update Change</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Update Change</em>'.
	 * @see org.ujf.verimag.wb.StatisticsCollector#getUpdateChange()
	 * @see #getStatisticsCollector()
	 * @generated
	 */
	EAttribute getStatisticsCollector_UpdateChange();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.StatisticsCollector#getUpdateDelay
	 * <em>Update Delay</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Update Delay</em>'.
	 * @see org.ujf.verimag.wb.StatisticsCollector#getUpdateDelay()
	 * @see #getStatisticsCollector()
	 * @generated
	 */
	EAttribute getStatisticsCollector_UpdateDelay();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.NodeStatistics <em>Node Statistics</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Node Statistics</em>'.
	 * @see org.ujf.verimag.wb.NodeStatistics
	 * @generated
	 */
	EClass getNodeStatistics();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NodeStatistics#getDimension
	 * <em>Dimension</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Dimension</em>'.
	 * @see org.ujf.verimag.wb.NodeStatistics#getDimension()
	 * @see #getNodeStatistics()
	 * @generated
	 */
	EAttribute getNodeStatistics_Dimension();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NodeStatistics#getKind <em>Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.ujf.verimag.wb.NodeStatistics#getKind()
	 * @see #getNodeStatistics()
	 * @generated
	 */
	EAttribute getNodeStatistics_Kind();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.NodeStatistics#getDirectNode
	 * <em>Direct Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Direct Node</em>'.
	 * @see org.ujf.verimag.wb.NodeStatistics#getDirectNode()
	 * @see #getNodeStatistics()
	 * @generated
	 */
	EReference getNodeStatistics_DirectNode();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.NamedElement <em>Named Element</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see org.ujf.verimag.wb.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NamedElement#getName <em>Name</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ujf.verimag.wb.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.NamedElement#getDescription
	 * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.ujf.verimag.wb.NamedElement#getDescription()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Description();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.TimedElement <em>Timed Element</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Timed Element</em>'.
	 * @see org.ujf.verimag.wb.TimedElement
	 * @generated
	 */
	EClass getTimedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.TimedElement#getTimeUnit <em>Time Unit</em>}
	 * '. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Time Unit</em>'.
	 * @see org.ujf.verimag.wb.TimedElement#getTimeUnit()
	 * @see #getTimedElement()
	 * @generated
	 */
	EAttribute getTimedElement_TimeUnit();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Super <em>Super</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Super</em>'.
	 * @see org.ujf.verimag.wb.Super
	 * @generated
	 */
	EClass getSuper();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Resume <em>Resume</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Resume</em>'.
	 * @see org.ujf.verimag.wb.Resume
	 * @generated
	 */
	EClass getResume();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.Branch <em>Branch</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Branch</em>'.
	 * @see org.ujf.verimag.wb.Branch
	 * @generated
	 */
	EClass getBranch();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.DuplicationNode <em>Duplication Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Duplication Node</em>'.
	 * @see org.ujf.verimag.wb.DuplicationNode
	 * @generated
	 */
	EClass getDuplicationNode();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DuplicationNode#getInheritsCalls
	 * <em>Inherits Calls</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Inherits Calls</em>'.
	 * @see org.ujf.verimag.wb.DuplicationNode#getInheritsCalls()
	 * @see #getDuplicationNode()
	 * @generated
	 */
	EAttribute getDuplicationNode_InheritsCalls();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.DuplicationNode#getInheritsUnshared
	 * <em>Inherits Unshared</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Inherits Unshared</em>'.
	 * @see org.ujf.verimag.wb.DuplicationNode#getInheritsUnshared()
	 * @see #getDuplicationNode()
	 * @generated
	 */
	EAttribute getDuplicationNode_InheritsUnshared();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.BlockRef <em>Block Ref</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Block Ref</em>'.
	 * @see org.ujf.verimag.wb.BlockRef
	 * @generated
	 */
	EClass getBlockRef();

	/**
	 * Returns the meta object for the reference '{@link org.ujf.verimag.wb.BlockRef#getTarget <em>Target</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.ujf.verimag.wb.BlockRef#getTarget()
	 * @see #getBlockRef()
	 * @generated
	 */
	EReference getBlockRef_Target();

	/**
	 * Returns the meta object for class '{@link org.ujf.verimag.wb.GraphicalNode <em>Graphical Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Graphical Node</em>'.
	 * @see org.ujf.verimag.wb.GraphicalNode
	 * @generated
	 */
	EClass getGraphicalNode();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.GraphicalNode#getX <em>X</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see org.ujf.verimag.wb.GraphicalNode#getX()
	 * @see #getGraphicalNode()
	 * @generated
	 */
	EAttribute getGraphicalNode_X();

	/**
	 * Returns the meta object for the attribute '{@link org.ujf.verimag.wb.GraphicalNode#getY <em>Y</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see org.ujf.verimag.wb.GraphicalNode#getY()
	 * @see #getGraphicalNode()
	 * @generated
	 */
	EAttribute getGraphicalNode_Y();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.StorageKind <em>Storage Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Storage Kind</em>'.
	 * @see org.ujf.verimag.wb.StorageKind
	 * @generated
	 */
	EEnum getStorageKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.DataTypeKind <em>Data Type Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Data Type Kind</em>'.
	 * @see org.ujf.verimag.wb.DataTypeKind
	 * @generated
	 */
	EEnum getDataTypeKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.PriorityRuleKind <em>Priority Rule Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Priority Rule Kind</em>'.
	 * @see org.ujf.verimag.wb.PriorityRuleKind
	 * @generated
	 */
	EEnum getPriorityRuleKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.TimeRuleKind <em>Time Rule Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Time Rule Kind</em>'.
	 * @see org.ujf.verimag.wb.TimeRuleKind
	 * @generated
	 */
	EEnum getTimeRuleKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.ResourceKind <em>Resource Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Resource Kind</em>'.
	 * @see org.ujf.verimag.wb.ResourceKind
	 * @generated
	 */
	EEnum getResourceKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.StatClassKind <em>Stat Class Kind</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Stat Class Kind</em>'.
	 * @see org.ujf.verimag.wb.StatClassKind
	 * @generated
	 */
	EEnum getStatClassKind();

	/**
	 * Returns the meta object for enum '{@link org.ujf.verimag.wb.TimeUnit <em>Time Unit</em>}'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for enum '<em>Time Unit</em>'.
	 * @see org.ujf.verimag.wb.TimeUnit
	 * @generated
	 */
	EEnum getTimeUnit();

	/**
	 * Returns the factory that creates the instances of the model. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WbFactory getWbFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ModuleImpl <em>Module</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ModuleImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getModule()
		 * @generated
		 */
		EClass		MODULE									= eINSTANCE.getModule();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MODULE__VERSION							= eINSTANCE.getModule_Version();

		/**
		 * The meta object literal for the '<em><b>Declaration Zone</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__DECLARATION_ZONE				= eINSTANCE.getModule_DeclarationZone();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__SUBMODEL						= eINSTANCE.getModule_Submodel();

		/**
		 * The meta object literal for the '<em><b>Allocate</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__ALLOCATE						= eINSTANCE.getModule_Allocate();

		/**
		 * The meta object literal for the '<em><b>Service</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__SERVICE							= eINSTANCE.getModule_Service();

		/**
		 * The meta object literal for the '<em><b>Resource Pool</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__RESOURCE_POOL					= eINSTANCE.getModule_ResourcePool();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	MODULE__BLOCK							= eINSTANCE.getModule_Block();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl
		 * <em>Declaration Zone</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.DeclarationZoneImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDeclarationZone()
		 * @generated
		 */
		EClass		DECLARATION_ZONE						= eINSTANCE.getDeclarationZone();

		/**
		 * The meta object literal for the '<em><b>Raw Content</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DECLARATION_ZONE__RAW_CONTENT			= eINSTANCE.getDeclarationZone_RawContent();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DECLARATION_ZONE__MODULE				= eINSTANCE.getDeclarationZone__module();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' container reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DECLARATION_ZONE__SUBMODEL				= eINSTANCE.getDeclarationZone_Submodel();

		/**
		 * The meta object literal for the '<em><b>Category ID</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DECLARATION_ZONE__CATEGORY_ID			= eINSTANCE.getDeclarationZone_CategoryID();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DECLARATION_ZONE__VARIABLE				= eINSTANCE.getDeclarationZone_Variable();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SubmodelImpl <em>Submodel</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SubmodelImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSubmodel()
		 * @generated
		 */
		EClass		SUBMODEL								= eINSTANCE.getSubmodel();

		/**
		 * The meta object literal for the '<em><b>Formal Parameters</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	SUBMODEL__FORMAL_PARAMETERS				= eINSTANCE.getSubmodel_FormalParameters();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	SUBMODEL__DIMENSION						= eINSTANCE.getSubmodel_Dimension();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__MODULE						= eINSTANCE.getSubmodel__module();

		/**
		 * The meta object literal for the '<em><b>Declaration Zone</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__DECLARATION_ZONE				= eINSTANCE.getSubmodel_DeclarationZone();

		/**
		 * The meta object literal for the '<em><b>Response Arc</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__RESPONSE_ARC					= eINSTANCE.getSubmodel_ResponseArc();

		/**
		 * The meta object literal for the '<em><b>Arc</b></em>' containment reference list feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__ARC							= eINSTANCE.getSubmodel_Arc();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__REF							= eINSTANCE.getSubmodel_Ref();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL__NODE							= eINSTANCE.getSubmodel_Node();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.AllocateImpl <em>Allocate</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.AllocateImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getAllocate()
		 * @generated
		 */
		EClass		ALLOCATE								= eINSTANCE.getAllocate();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ALLOCATE__QUANTITY						= eINSTANCE.getAllocate_Quantity();

		/**
		 * The meta object literal for the '<em><b>Generate Characteristic</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ALLOCATE__GENERATE_CHARACTERISTIC		= eINSTANCE.getAllocate_GenerateCharacteristic();

		/**
		 * The meta object literal for the '<em><b>Resource Instance Spec</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ALLOCATE__RESOURCE_INSTANCE_SPEC		= eINSTANCE.getAllocate_ResourceInstanceSpec();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ALLOCATE__MODULE						= eINSTANCE.getAllocate__module();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ALLOCATE__REF							= eINSTANCE.getAllocate_Ref();

		/**
		 * The meta object literal for the '<em><b>Resource Instance</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ALLOCATE__RESOURCE_INSTANCE				= eINSTANCE.getAllocate_ResourceInstance();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ServiceImpl <em>Service</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ServiceImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getService()
		 * @generated
		 */
		EClass		SERVICE									= eINSTANCE.getService();

		/**
		 * The meta object literal for the '<em><b>Service Time</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	SERVICE__SERVICE_TIME					= eINSTANCE.getService_ServiceTime();

		/**
		 * The meta object literal for the '<em><b>Server Quantity</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	SERVICE__SERVER_QUANTITY				= eINSTANCE.getService_ServerQuantity();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SERVICE__MODULE							= eINSTANCE.getService__module();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SERVICE__REF							= eINSTANCE.getService_Ref();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ResourcePoolImpl <em>Resource Pool</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ResourcePoolImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResourcePool()
		 * @generated
		 */
		EClass		RESOURCE_POOL							= eINSTANCE.getResourcePool();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RESOURCE_POOL__DIMENSION				= eINSTANCE.getResourcePool_Dimension();

		/**
		 * The meta object literal for the '<em><b>Resource Kind</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RESOURCE_POOL__RESOURCE_KIND			= eINSTANCE.getResourcePool_ResourceKind();

		/**
		 * The meta object literal for the '<em><b>Token Quantity</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RESOURCE_POOL__TOKEN_QUANTITY			= eINSTANCE.getResourcePool_TokenQuantity();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESOURCE_POOL__MODULE					= eINSTANCE.getResourcePool__module();

		/**
		 * The meta object literal for the '<em><b>Allocate</b></em>' reference list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESOURCE_POOL__ALLOCATE					= eINSTANCE.getResourcePool_Allocate();

		/**
		 * The meta object literal for the '<em><b>Release</b></em>' reference list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESOURCE_POOL__RELEASE					= eINSTANCE.getResourcePool_Release();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.GraphNodeImpl <em>Graph Node</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.GraphNodeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getGraphNode()
		 * @generated
		 */
		EClass		GRAPH_NODE								= eINSTANCE.getGraphNode();

		/**
		 * The meta object literal for the '<em><b>Out Arc</b></em>' reference list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__OUT_ARC						= eINSTANCE.getGraphNode_OutArc();

		/**
		 * The meta object literal for the '<em><b>In Arc</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__IN_ARC						= eINSTANCE.getGraphNode_InArc();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' container reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__SUBMODEL					= eINSTANCE.getGraphNode_Submodel();

		/**
		 * The meta object literal for the '<em><b>Interrupt</b></em>' reference list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__INTERRUPT					= eINSTANCE.getGraphNode_Interrupt();

		/**
		 * The meta object literal for the '<em><b>Out Response Arc</b></em>' reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__OUT_RESPONSE_ARC			= eINSTANCE.getGraphNode_OutResponseArc();

		/**
		 * The meta object literal for the '<em><b>In Response Arc</b></em>' reference list feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	GRAPH_NODE__IN_RESPONSE_ARC				= eINSTANCE.getGraphNode_InResponseArc();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ArcImpl <em>Arc</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ArcImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getArc()
		 * @generated
		 */
		EClass		ARC										= eINSTANCE.getArc();

		/**
		 * The meta object literal for the '<em><b>Select Condition</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__SELECT_CONDITION					= eINSTANCE.getArc_SelectCondition();

		/**
		 * The meta object literal for the '<em><b>Select Probability</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__SELECT_PROBABILITY					= eINSTANCE.getArc_SelectProbability();

		/**
		 * The meta object literal for the '<em><b>Select Phase</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__SELECT_PHASE						= eINSTANCE.getArc_SelectPhase();

		/**
		 * The meta object literal for the '<em><b>Select Parent</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__SELECT_PARENT						= eINSTANCE.getArc_SelectParent();

		/**
		 * The meta object literal for the '<em><b>Dest Node Index</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__DEST_NODE_INDEX					= eINSTANCE.getArc_DestNodeIndex();

		/**
		 * The meta object literal for the '<em><b>Change Port</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__CHANGE_PORT						= eINSTANCE.getArc_ChangePort();

		/**
		 * The meta object literal for the '<em><b>Change Phase</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	ARC__CHANGE_PHASE						= eINSTANCE.getArc_ChangePhase();

		/**
		 * The meta object literal for the '<em><b>Select Category</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARC__SELECT_CATEGORY					= eINSTANCE.getArc_SelectCategory();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' container reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARC__SUBMODEL							= eINSTANCE.getArc_Submodel();

		/**
		 * The meta object literal for the '<em><b>From Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARC__FROM_NODE							= eINSTANCE.getArc_FromNode();

		/**
		 * The meta object literal for the '<em><b>To Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ARC__TO_NODE							= eINSTANCE.getArc_ToNode();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ResponseArcImpl <em>Response Arc</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ResponseArcImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResponseArc()
		 * @generated
		 */
		EClass		RESPONSE_ARC							= eINSTANCE.getResponseArc();

		/**
		 * The meta object literal for the '<em><b>Source Node Index</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RESPONSE_ARC__SOURCE_NODE_INDEX			= eINSTANCE.getResponseArc_SourceNodeIndex();

		/**
		 * The meta object literal for the '<em><b>Destination Node Index</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RESPONSE_ARC__DESTINATION_NODE_INDEX	= eINSTANCE.getResponseArc_DestinationNodeIndex();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' container reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESPONSE_ARC__SUBMODEL					= eINSTANCE.getResponseArc_Submodel();

		/**
		 * The meta object literal for the '<em><b>From Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESPONSE_ARC__FROM_NODE					= eINSTANCE.getResponseArc_FromNode();

		/**
		 * The meta object literal for the '<em><b>To Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RESPONSE_ARC__TO_NODE					= eINSTANCE.getResponseArc_ToNode();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.CategoryIDImpl <em>Category ID</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.CategoryIDImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getCategoryID()
		 * @generated
		 */
		EClass		CATEGORY_ID								= eINSTANCE.getCategoryID();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CATEGORY_ID__DIMENSION					= eINSTANCE.getCategoryID_Dimension();

		/**
		 * The meta object literal for the '<em><b>Arc</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CATEGORY_ID__ARC						= eINSTANCE.getCategoryID_Arc();

		/**
		 * The meta object literal for the '<em><b>Declaration Zone</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CATEGORY_ID__DECLARATION_ZONE			= eINSTANCE.getCategoryID_DeclarationZone();

		/**
		 * The meta object literal for the '<em><b>Interrupt</b></em>' reference list feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	CATEGORY_ID__INTERRUPT					= eINSTANCE.getCategoryID_Interrupt();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.VariableImpl <em>Variable</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.VariableImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getVariable()
		 * @generated
		 */
		EClass		VARIABLE								= eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	VARIABLE__DIMENSION						= eINSTANCE.getVariable_Dimension();

		/**
		 * The meta object literal for the '<em><b>Storage Kind</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	VARIABLE__STORAGE_KIND					= eINSTANCE.getVariable_StorageKind();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	VARIABLE__DATATYPE						= eINSTANCE.getVariable_Datatype();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	VARIABLE__VALUE							= eINSTANCE.getVariable_Value();

		/**
		 * The meta object literal for the '<em><b>Declaration Zone</b></em>' container reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	VARIABLE__DECLARATION_ZONE				= eINSTANCE.getVariable_DeclarationZone();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.DirectNodeImpl <em>Direct Node</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.DirectNodeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDirectNode()
		 * @generated
		 */
		EClass		DIRECT_NODE								= eINSTANCE.getDirectNode();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DIRECT_NODE__DIMENSION					= eINSTANCE.getDirectNode_Dimension();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DIRECT_NODE__CODE						= eINSTANCE.getDirectNode_Code();

		/**
		 * The meta object literal for the '<em><b>Formal Parameters</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DIRECT_NODE__FORMAL_PARAMETERS			= eINSTANCE.getDirectNode_FormalParameters();

		/**
		 * The meta object literal for the '<em><b>Declarations</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DIRECT_NODE__DECLARATIONS				= eINSTANCE.getDirectNode_Declarations();

		/**
		 * The meta object literal for the '<em><b>Node Statistics</b></em>' reference list feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	DIRECT_NODE__NODE_STATISTICS			= eINSTANCE.getDirectNode_NodeStatistics();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.RefNodeImpl <em>Ref Node</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.RefNodeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getRefNode()
		 * @generated
		 */
		EClass		REF_NODE								= eINSTANCE.getRefNode();

		/**
		 * The meta object literal for the '<em><b>Actual Parameters</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	REF_NODE__ACTUAL_PARAMETERS				= eINSTANCE.getRefNode_ActualParameters();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SubmodelRefImpl <em>Submodel Ref</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SubmodelRefImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSubmodelRef()
		 * @generated
		 */
		EClass		SUBMODEL_REF							= eINSTANCE.getSubmodelRef();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SUBMODEL_REF__TARGET					= eINSTANCE.getSubmodelRef_Target();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ServiceRefImpl <em>Service Ref</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ServiceRefImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getServiceRef()
		 * @generated
		 */
		EClass		SERVICE_REF								= eINSTANCE.getServiceRef();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	SERVICE_REF__TARGET						= eINSTANCE.getServiceRef_Target();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.AllocateRefImpl <em>Allocate Ref</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.AllocateRefImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getAllocateRef()
		 * @generated
		 */
		EClass		ALLOCATE_REF							= eINSTANCE.getAllocateRef();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	ALLOCATE_REF__TARGET					= eINSTANCE.getAllocateRef_Target();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.NodeWithQueueImpl <em>Node With Queue</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.NodeWithQueueImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNodeWithQueue()
		 * @generated
		 */
		EClass		NODE_WITH_QUEUE							= eINSTANCE.getNodeWithQueue();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NODE_WITH_QUEUE__PRIORITY				= eINSTANCE.getNodeWithQueue_Priority();

		/**
		 * The meta object literal for the '<em><b>Priority Rule</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NODE_WITH_QUEUE__PRIORITY_RULE			= eINSTANCE.getNodeWithQueue_PriorityRule();

		/**
		 * The meta object literal for the '<em><b>Time Rule</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NODE_WITH_QUEUE__TIME_RULE				= eINSTANCE.getNodeWithQueue_TimeRule();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.BlockImpl <em>Block</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.BlockImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBlock()
		 * @generated
		 */
		EClass		BLOCK									= eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Block Until</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	BLOCK__BLOCK_UNTIL						= eINSTANCE.getBlock_BlockUntil();

		/**
		 * The meta object literal for the '<em><b>module</b></em>' container reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	BLOCK__MODULE							= eINSTANCE.getBlock__module();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference list feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	BLOCK__REF								= eINSTANCE.getBlock_Ref();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SourceImpl <em>Source</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SourceImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSource()
		 * @generated
		 */
		EClass		SOURCE									= eINSTANCE.getSource();

		/**
		 * The meta object literal for the '<em><b>Generate Characteristic</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	SOURCE__GENERATE_CHARACTERISTIC			= eINSTANCE.getSource_GenerateCharacteristic();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SinkImpl <em>Sink</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SinkImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSink()
		 * @generated
		 */
		EClass		SINK									= eINSTANCE.getSink();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.EnterImpl <em>Enter</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.EnterImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getEnter()
		 * @generated
		 */
		EClass		ENTER									= eINSTANCE.getEnter();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ReturnImpl <em>Return</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ReturnImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getReturn()
		 * @generated
		 */
		EClass		RETURN									= eINSTANCE.getReturn();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SplitImpl <em>Split</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SplitImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSplit()
		 * @generated
		 */
		EClass		SPLIT									= eINSTANCE.getSplit();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ForkImpl <em>Fork</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ForkImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getFork()
		 * @generated
		 */
		EClass		FORK									= eINSTANCE.getFork();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.JoinImpl <em>Join</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.JoinImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getJoin()
		 * @generated
		 */
		EClass		JOIN									= eINSTANCE.getJoin();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.UserImpl <em>User</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.UserImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getUser()
		 * @generated
		 */
		EClass		USER									= eINSTANCE.getUser();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.DelayImpl <em>Delay</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.DelayImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDelay()
		 * @generated
		 */
		EClass		DELAY									= eINSTANCE.getDelay();

		/**
		 * The meta object literal for the '<em><b>Delay Time</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DELAY__DELAY_TIME						= eINSTANCE.getDelay_DelayTime();

		/**
		 * The meta object literal for the '<em><b>Generate Characteristic</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DELAY__GENERATE_CHARACTERISTIC			= eINSTANCE.getDelay_GenerateCharacteristic();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ReleaseImpl <em>Release</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ReleaseImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getRelease()
		 * @generated
		 */
		EClass		RELEASE									= eINSTANCE.getRelease();

		/**
		 * The meta object literal for the '<em><b>Generate Characteristic</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RELEASE__GENERATE_CHARACTERISTIC		= eINSTANCE.getRelease_GenerateCharacteristic();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RELEASE__QUANTITY						= eINSTANCE.getRelease_Quantity();

		/**
		 * The meta object literal for the '<em><b>Resource Instance Spec</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	RELEASE__RESOURCE_INSTANCE_SPEC			= eINSTANCE.getRelease_ResourceInstanceSpec();

		/**
		 * The meta object literal for the '<em><b>Resource Instance</b></em>' reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	RELEASE__RESOURCE_INSTANCE				= eINSTANCE.getRelease_ResourceInstance();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.InterruptImpl <em>Interrupt</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.InterruptImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getInterrupt()
		 * @generated
		 */
		EClass		INTERRUPT								= eINSTANCE.getInterrupt();

		/**
		 * The meta object literal for the '<em><b>Select Condition</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	INTERRUPT__SELECT_CONDITION				= eINSTANCE.getInterrupt_SelectCondition();

		/**
		 * The meta object literal for the '<em><b>Select Quantity</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	INTERRUPT__SELECT_QUANTITY				= eINSTANCE.getInterrupt_SelectQuantity();

		/**
		 * The meta object literal for the '<em><b>Select Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERRUPT__SELECT_NODE					= eINSTANCE.getInterrupt_SelectNode();

		/**
		 * The meta object literal for the '<em><b>Select Category</b></em>' reference feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	INTERRUPT__SELECT_CATEGORY				= eINSTANCE.getInterrupt_SelectCategory();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.LoopImpl <em>Loop</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.LoopImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getLoop()
		 * @generated
		 */
		EClass		LOOP									= eINSTANCE.getLoop();

		/**
		 * The meta object literal for the '<em><b>Loop Start</b></em>' containment reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LOOP__LOOP_START						= eINSTANCE.getLoop_LoopStart();

		/**
		 * The meta object literal for the '<em><b>Loop End</b></em>' containment reference feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LOOP__LOOP_END							= eINSTANCE.getLoop_LoopEnd();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ForLoopImpl <em>For Loop</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ForLoopImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getForLoop()
		 * @generated
		 */
		EClass		FOR_LOOP								= eINSTANCE.getForLoop();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	FOR_LOOP__COUNT							= eINSTANCE.getForLoop_Count();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ConditionalLoopImpl
		 * <em>Conditional Loop</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ConditionalLoopImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getConditionalLoop()
		 * @generated
		 */
		EClass		CONDITIONAL_LOOP						= eINSTANCE.getConditionalLoop();

		/**
		 * The meta object literal for the '<em><b>Is While</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CONDITIONAL_LOOP__IS_WHILE				= eINSTANCE.getConditionalLoop_IsWhile();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	CONDITIONAL_LOOP__CONDITION				= eINSTANCE.getConditionalLoop_Condition();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl
		 * <em>Statistics Collector</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.StatisticsCollectorImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStatisticsCollector()
		 * @generated
		 */
		EClass		STATISTICS_COLLECTOR					= eINSTANCE.getStatisticsCollector();

		/**
		 * The meta object literal for the '<em><b>Breakdown List</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATISTICS_COLLECTOR__BREAKDOWN_LIST	= eINSTANCE.getStatisticsCollector_BreakdownList();

		/**
		 * The meta object literal for the '<em><b>Histogram Buckets</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS	= eINSTANCE.getStatisticsCollector_HistogramBuckets();

		/**
		 * The meta object literal for the '<em><b>Update Change</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATISTICS_COLLECTOR__UPDATE_CHANGE		= eINSTANCE.getStatisticsCollector_UpdateChange();

		/**
		 * The meta object literal for the '<em><b>Update Delay</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATISTICS_COLLECTOR__UPDATE_DELAY		= eINSTANCE.getStatisticsCollector_UpdateDelay();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.NodeStatisticsImpl <em>Node Statistics</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.NodeStatisticsImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNodeStatistics()
		 * @generated
		 */
		EClass		NODE_STATISTICS							= eINSTANCE.getNodeStatistics();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NODE_STATISTICS__DIMENSION				= eINSTANCE.getNodeStatistics_Dimension();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NODE_STATISTICS__KIND					= eINSTANCE.getNodeStatistics_Kind();

		/**
		 * The meta object literal for the '<em><b>Direct Node</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	NODE_STATISTICS__DIRECT_NODE			= eINSTANCE.getNodeStatistics_DirectNode();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.NamedElementImpl <em>Named Element</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.NamedElementImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass		NAMED_ELEMENT							= eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NAMED_ELEMENT__NAME						= eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	NAMED_ELEMENT__DESCRIPTION				= eINSTANCE.getNamedElement_Description();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.TimedElementImpl <em>Timed Element</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.TimedElementImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimedElement()
		 * @generated
		 */
		EClass		TIMED_ELEMENT							= eINSTANCE.getTimedElement();

		/**
		 * The meta object literal for the '<em><b>Time Unit</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	TIMED_ELEMENT__TIME_UNIT				= eINSTANCE.getTimedElement_TimeUnit();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.SuperImpl <em>Super</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.SuperImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getSuper()
		 * @generated
		 */
		EClass		SUPER									= eINSTANCE.getSuper();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.ResumeImpl <em>Resume</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.ResumeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResume()
		 * @generated
		 */
		EClass		RESUME									= eINSTANCE.getResume();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.BranchImpl <em>Branch</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.BranchImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBranch()
		 * @generated
		 */
		EClass		BRANCH									= eINSTANCE.getBranch();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.DuplicationNodeImpl
		 * <em>Duplication Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.DuplicationNodeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDuplicationNode()
		 * @generated
		 */
		EClass		DUPLICATION_NODE						= eINSTANCE.getDuplicationNode();

		/**
		 * The meta object literal for the '<em><b>Inherits Calls</b></em>' attribute feature. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DUPLICATION_NODE__INHERITS_CALLS		= eINSTANCE.getDuplicationNode_InheritsCalls();

		/**
		 * The meta object literal for the '<em><b>Inherits Unshared</b></em>' attribute feature. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	DUPLICATION_NODE__INHERITS_UNSHARED		= eINSTANCE.getDuplicationNode_InheritsUnshared();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.BlockRefImpl <em>Block Ref</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.BlockRefImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getBlockRef()
		 * @generated
		 */
		EClass		BLOCK_REF								= eINSTANCE.getBlockRef();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	BLOCK_REF__TARGET						= eINSTANCE.getBlockRef_Target();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.impl.GraphicalNodeImpl <em>Graphical Node</em>}'
		 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.impl.GraphicalNodeImpl
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getGraphicalNode()
		 * @generated
		 */
		EClass		GRAPHICAL_NODE							= eINSTANCE.getGraphicalNode();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	GRAPHICAL_NODE__X						= eINSTANCE.getGraphicalNode_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	GRAPHICAL_NODE__Y						= eINSTANCE.getGraphicalNode_Y();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.StorageKind <em>Storage Kind</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.StorageKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStorageKind()
		 * @generated
		 */
		EEnum		STORAGE_KIND							= eINSTANCE.getStorageKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.DataTypeKind <em>Data Type Kind</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.DataTypeKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getDataTypeKind()
		 * @generated
		 */
		EEnum		DATA_TYPE_KIND							= eINSTANCE.getDataTypeKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.PriorityRuleKind <em>Priority Rule Kind</em>}'
		 * enum. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.PriorityRuleKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getPriorityRuleKind()
		 * @generated
		 */
		EEnum		PRIORITY_RULE_KIND						= eINSTANCE.getPriorityRuleKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.TimeRuleKind <em>Time Rule Kind</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.TimeRuleKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimeRuleKind()
		 * @generated
		 */
		EEnum		TIME_RULE_KIND							= eINSTANCE.getTimeRuleKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.ResourceKind <em>Resource Kind</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.ResourceKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getResourceKind()
		 * @generated
		 */
		EEnum		RESOURCE_KIND							= eINSTANCE.getResourceKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.StatClassKind <em>Stat Class Kind</em>}' enum.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.StatClassKind
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getStatClassKind()
		 * @generated
		 */
		EEnum		STAT_CLASS_KIND							= eINSTANCE.getStatClassKind();

		/**
		 * The meta object literal for the '{@link org.ujf.verimag.wb.TimeUnit <em>Time Unit</em>}' enum. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see org.ujf.verimag.wb.TimeUnit
		 * @see org.ujf.verimag.wb.impl.WbPackageImpl#getTimeUnit()
		 * @generated
		 */
		EEnum		TIME_UNIT								= eINSTANCE.getTimeUnit();

	}

} // WbPackage
