/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Duplication Node</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.DuplicationNode#getInheritsCalls <em>Inherits Calls</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DuplicationNode#getInheritsUnshared <em>Inherits Unshared</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getDuplicationNode()
 * @model abstract="true"
 * @generated
 */
public interface DuplicationNode extends DirectNode
{
	/**
	 * Returns the value of the '<em><b>Inherits Calls</b></em>' attribute. The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inherits Calls</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Inherits Calls</em>' attribute.
	 * @see #setInheritsCalls(Boolean)
	 * @see org.ujf.verimag.wb.WbPackage#getDuplicationNode_InheritsCalls()
	 * @model default="true"
	 * @generated
	 */
	Boolean getInheritsCalls();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DuplicationNode#getInheritsCalls <em>Inherits Calls</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Inherits Calls</em>' attribute.
	 * @see #getInheritsCalls()
	 * @generated
	 */
	void setInheritsCalls(Boolean value);

	/**
	 * Returns the value of the '<em><b>Inherits Unshared</b></em>' attribute. The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inherits Unshared</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Inherits Unshared</em>' attribute.
	 * @see #setInheritsUnshared(Boolean)
	 * @see org.ujf.verimag.wb.WbPackage#getDuplicationNode_InheritsUnshared()
	 * @model default="true"
	 * @generated
	 */
	Boolean getInheritsUnshared();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DuplicationNode#getInheritsUnshared <em>Inherits Unshared</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Inherits Unshared</em>' attribute.
	 * @see #getInheritsUnshared()
	 * @generated
	 */
	void setInheritsUnshared(Boolean value);

} // DuplicationNode
