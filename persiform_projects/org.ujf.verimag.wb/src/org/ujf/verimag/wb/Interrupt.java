/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Interrupt</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Interrupt#getSelectCondition <em>Select Condition</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Interrupt#getSelectQuantity <em>Select Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Interrupt#getSelectNode <em>Select Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Interrupt#getSelectCategory <em>Select Category</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getInterrupt()
 * @model
 * @generated
 */
public interface Interrupt extends DirectNode
{
	/**
	 * Returns the value of the '<em><b>Select Condition</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Condition</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Condition</em>' attribute.
	 * @see #setSelectCondition(String)
	 * @see org.ujf.verimag.wb.WbPackage#getInterrupt_SelectCondition()
	 * @model
	 * @generated
	 */
	String getSelectCondition();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Interrupt#getSelectCondition <em>Select Condition</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Condition</em>' attribute.
	 * @see #getSelectCondition()
	 * @generated
	 */
	void setSelectCondition(String value);

	/**
	 * Returns the value of the '<em><b>Select Quantity</b></em>' attribute. The default value is <code>"1"</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Quantity</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Quantity</em>' attribute.
	 * @see #setSelectQuantity(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getInterrupt_SelectQuantity()
	 * @model default="1"
	 * @generated
	 */
	Integer getSelectQuantity();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Interrupt#getSelectQuantity <em>Select Quantity</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Quantity</em>' attribute.
	 * @see #getSelectQuantity()
	 * @generated
	 */
	void setSelectQuantity(Integer value);

	/**
	 * Returns the value of the '<em><b>Select Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getInterrupt <em>Interrupt</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Node</em>' reference.
	 * @see #setSelectNode(GraphNode)
	 * @see org.ujf.verimag.wb.WbPackage#getInterrupt_SelectNode()
	 * @see org.ujf.verimag.wb.GraphNode#getInterrupt
	 * @model opposite="interrupt"
	 * @generated
	 */
	GraphNode getSelectNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Interrupt#getSelectNode <em>Select Node</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Node</em>' reference.
	 * @see #getSelectNode()
	 * @generated
	 */
	void setSelectNode(GraphNode value);

	/**
	 * Returns the value of the '<em><b>Select Category</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.CategoryID#getInterrupt <em>Interrupt</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Category</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Category</em>' reference.
	 * @see #setSelectCategory(CategoryID)
	 * @see org.ujf.verimag.wb.WbPackage#getInterrupt_SelectCategory()
	 * @see org.ujf.verimag.wb.CategoryID#getInterrupt
	 * @model opposite="interrupt"
	 * @generated
	 */
	CategoryID getSelectCategory();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Interrupt#getSelectCategory <em>Select Category</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Category</em>' reference.
	 * @see #getSelectCategory()
	 * @generated
	 */
	void setSelectCategory(CategoryID value);

} // Interrupt
