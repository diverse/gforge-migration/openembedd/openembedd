/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Node With Queue</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.NodeWithQueue#getPriority <em>Priority</em>}</li>
 * <li>{@link org.ujf.verimag.wb.NodeWithQueue#getPriorityRule <em>Priority Rule</em>}</li>
 * <li>{@link org.ujf.verimag.wb.NodeWithQueue#getTimeRule <em>Time Rule</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getNodeWithQueue()
 * @model abstract="true"
 * @generated
 */
public interface NodeWithQueue extends DirectNode
{
	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see #setPriority(String)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeWithQueue_Priority()
	 * @model
	 * @generated
	 */
	String getPriority();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeWithQueue#getPriority <em>Priority</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Priority</em>' attribute.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(String value);

	/**
	 * Returns the value of the '<em><b>Priority Rule</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.PriorityRuleKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Rule</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Priority Rule</em>' attribute.
	 * @see org.ujf.verimag.wb.PriorityRuleKind
	 * @see #setPriorityRule(PriorityRuleKind)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeWithQueue_PriorityRule()
	 * @model
	 * @generated
	 */
	PriorityRuleKind getPriorityRule();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeWithQueue#getPriorityRule <em>Priority Rule</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Priority Rule</em>' attribute.
	 * @see org.ujf.verimag.wb.PriorityRuleKind
	 * @see #getPriorityRule()
	 * @generated
	 */
	void setPriorityRule(PriorityRuleKind value);

	/**
	 * Returns the value of the '<em><b>Time Rule</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.TimeRuleKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Rule</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Time Rule</em>' attribute.
	 * @see org.ujf.verimag.wb.TimeRuleKind
	 * @see #setTimeRule(TimeRuleKind)
	 * @see org.ujf.verimag.wb.WbPackage#getNodeWithQueue_TimeRule()
	 * @model
	 * @generated
	 */
	TimeRuleKind getTimeRule();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.NodeWithQueue#getTimeRule <em>Time Rule</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Time Rule</em>' attribute.
	 * @see org.ujf.verimag.wb.TimeRuleKind
	 * @see #getTimeRule()
	 * @generated
	 */
	void setTimeRule(TimeRuleKind value);

} // NodeWithQueue
