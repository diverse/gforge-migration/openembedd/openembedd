/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Declaration Zone</b></em>'. <!-- end-user-doc
 * -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.DeclarationZone#getRawContent <em>Raw Content</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DeclarationZone#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DeclarationZone#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DeclarationZone#getCategoryID <em>Category ID</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DeclarationZone#getVariable <em>Variable</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone()
 * @model
 * @generated
 */
public interface DeclarationZone extends GraphicalNode
{
	/**
	 * Returns the value of the '<em><b>Raw Content</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Content</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Raw Content</em>' attribute.
	 * @see #setRawContent(String)
	 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone_RawContent()
	 * @model
	 * @generated
	 */
	String getRawContent();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DeclarationZone#getRawContent <em>Raw Content</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Raw Content</em>' attribute.
	 * @see #getRawContent()
	 * @generated
	 */
	void setRawContent(String value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getDeclarationZone <em>Declaration Zone</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone__module()
	 * @see org.ujf.verimag.wb.Module#getDeclarationZone
	 * @model opposite="declarationZone"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DeclarationZone#get_module <em>module</em>}' container
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' container reference. It is bidirectional and its opposite is
	 * '{@link org.ujf.verimag.wb.Submodel#getDeclarationZone <em>Declaration Zone</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Submodel</em>' container reference.
	 * @see #setSubmodel(Submodel)
	 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone_Submodel()
	 * @see org.ujf.verimag.wb.Submodel#getDeclarationZone
	 * @model opposite="declarationZone"
	 * @generated
	 */
	Submodel getSubmodel();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DeclarationZone#getSubmodel <em>Submodel</em>}' container
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Submodel</em>' container reference.
	 * @see #getSubmodel()
	 * @generated
	 */
	void setSubmodel(Submodel value);

	/**
	 * Returns the value of the '<em><b>Category ID</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.CategoryID}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.CategoryID#getDeclarationZone <em>Declaration Zone</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category ID</em>' containment reference list isn't clear, there really should be more
	 * of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Category ID</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone_CategoryID()
	 * @see org.ujf.verimag.wb.CategoryID#getDeclarationZone
	 * @model opposite="declarationZone" containment="true"
	 * @generated
	 */
	EList<CategoryID> getCategoryID();

	/**
	 * Returns the value of the '<em><b>Variable</b></em>' containment reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Variable}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Variable#getDeclarationZone <em>Declaration Zone</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' containment reference list isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Variable</em>' containment reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getDeclarationZone_Variable()
	 * @see org.ujf.verimag.wb.Variable#getDeclarationZone
	 * @model opposite="declarationZone" containment="true"
	 * @generated
	 */
	EList<Variable> getVariable();

} // DeclarationZone
