/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Return</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see org.ujf.verimag.wb.WbPackage#getReturn()
 * @model
 * @generated
 */
public interface Return extends DirectNode
{} // Return
