/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Arc</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Arc#getSelectCondition <em>Select Condition</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getSelectProbability <em>Select Probability</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getSelectPhase <em>Select Phase</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getSelectParent <em>Select Parent</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getDestNodeIndex <em>Dest Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getChangePort <em>Change Port</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getChangePhase <em>Change Phase</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getSelectCategory <em>Select Category</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getFromNode <em>From Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Arc#getToNode <em>To Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getArc()
 * @model
 * @generated
 */
public interface Arc extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Select Condition</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Condition</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Condition</em>' attribute.
	 * @see #setSelectCondition(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_SelectCondition()
	 * @model
	 * @generated
	 */
	String getSelectCondition();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSelectCondition <em>Select Condition</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Condition</em>' attribute.
	 * @see #getSelectCondition()
	 * @generated
	 */
	void setSelectCondition(String value);

	/**
	 * Returns the value of the '<em><b>Select Probability</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Probability</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Probability</em>' attribute.
	 * @see #setSelectProbability(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_SelectProbability()
	 * @model
	 * @generated
	 */
	String getSelectProbability();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSelectProbability <em>Select Probability</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Probability</em>' attribute.
	 * @see #getSelectProbability()
	 * @generated
	 */
	void setSelectProbability(String value);

	/**
	 * Returns the value of the '<em><b>Select Phase</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Phase</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Phase</em>' attribute.
	 * @see #setSelectPhase(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_SelectPhase()
	 * @model
	 * @generated
	 */
	String getSelectPhase();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSelectPhase <em>Select Phase</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Phase</em>' attribute.
	 * @see #getSelectPhase()
	 * @generated
	 */
	void setSelectPhase(String value);

	/**
	 * Returns the value of the '<em><b>Select Parent</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Parent</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Parent</em>' attribute.
	 * @see #setSelectParent(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_SelectParent()
	 * @model
	 * @generated
	 */
	String getSelectParent();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSelectParent <em>Select Parent</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Parent</em>' attribute.
	 * @see #getSelectParent()
	 * @generated
	 */
	void setSelectParent(String value);

	/**
	 * Returns the value of the '<em><b>Dest Node Index</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest Node Index</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dest Node Index</em>' attribute.
	 * @see #setDestNodeIndex(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_DestNodeIndex()
	 * @model
	 * @generated
	 */
	String getDestNodeIndex();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getDestNodeIndex <em>Dest Node Index</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dest Node Index</em>' attribute.
	 * @see #getDestNodeIndex()
	 * @generated
	 */
	void setDestNodeIndex(String value);

	/**
	 * Returns the value of the '<em><b>Change Port</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Port</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Change Port</em>' attribute.
	 * @see #setChangePort(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_ChangePort()
	 * @model
	 * @generated
	 */
	String getChangePort();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getChangePort <em>Change Port</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Change Port</em>' attribute.
	 * @see #getChangePort()
	 * @generated
	 */
	void setChangePort(String value);

	/**
	 * Returns the value of the '<em><b>Change Phase</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Phase</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Change Phase</em>' attribute.
	 * @see #setChangePhase(String)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_ChangePhase()
	 * @model
	 * @generated
	 */
	String getChangePhase();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getChangePhase <em>Change Phase</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Change Phase</em>' attribute.
	 * @see #getChangePhase()
	 * @generated
	 */
	void setChangePhase(String value);

	/**
	 * Returns the value of the '<em><b>Select Category</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.CategoryID#getArc <em>Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Category</em>' reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Select Category</em>' reference.
	 * @see #setSelectCategory(CategoryID)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_SelectCategory()
	 * @see org.ujf.verimag.wb.CategoryID#getArc
	 * @model opposite="arc"
	 * @generated
	 */
	CategoryID getSelectCategory();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSelectCategory <em>Select Category</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Select Category</em>' reference.
	 * @see #getSelectCategory()
	 * @generated
	 */
	void setSelectCategory(CategoryID value);

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' container reference. It is bidirectional and its opposite is
	 * '{@link org.ujf.verimag.wb.Submodel#getArc <em>Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Submodel</em>' container reference.
	 * @see #setSubmodel(Submodel)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_Submodel()
	 * @see org.ujf.verimag.wb.Submodel#getArc
	 * @model opposite="arc" required="true"
	 * @generated
	 */
	Submodel getSubmodel();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getSubmodel <em>Submodel</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Submodel</em>' container reference.
	 * @see #getSubmodel()
	 * @generated
	 */
	void setSubmodel(Submodel value);

	/**
	 * Returns the value of the '<em><b>From Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getOutArc <em>Out Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>From Node</em>' reference.
	 * @see #setFromNode(GraphNode)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_FromNode()
	 * @see org.ujf.verimag.wb.GraphNode#getOutArc
	 * @model opposite="outArc" required="true"
	 * @generated
	 */
	GraphNode getFromNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getFromNode <em>From Node</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>From Node</em>' reference.
	 * @see #getFromNode()
	 * @generated
	 */
	void setFromNode(GraphNode value);

	/**
	 * Returns the value of the '<em><b>To Node</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.GraphNode#getInArc <em>In Arc</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Node</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>To Node</em>' reference.
	 * @see #setToNode(GraphNode)
	 * @see org.ujf.verimag.wb.WbPackage#getArc_ToNode()
	 * @see org.ujf.verimag.wb.GraphNode#getInArc
	 * @model opposite="inArc" required="true"
	 * @generated
	 */
	GraphNode getToNode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Arc#getToNode <em>To Node</em>}' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>To Node</em>' reference.
	 * @see #getToNode()
	 * @generated
	 */
	void setToNode(GraphNode value);

} // Arc
