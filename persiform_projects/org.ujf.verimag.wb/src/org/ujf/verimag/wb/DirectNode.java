/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Direct Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.DirectNode#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DirectNode#getCode <em>Code</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DirectNode#getFormalParameters <em>Formal Parameters</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DirectNode#getDeclarations <em>Declarations</em>}</li>
 * <li>{@link org.ujf.verimag.wb.DirectNode#getNodeStatistics <em>Node Statistics</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getDirectNode()
 * @model abstract="true"
 * @generated
 */
public interface DirectNode extends GraphNode
{
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(String)
	 * @see org.ujf.verimag.wb.WbPackage#getDirectNode_Dimension()
	 * @model
	 * @generated
	 */
	String getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DirectNode#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see org.ujf.verimag.wb.WbPackage#getDirectNode_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DirectNode#getCode <em>Code</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Formal Parameters</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Parameters</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Formal Parameters</em>' attribute.
	 * @see #setFormalParameters(String)
	 * @see org.ujf.verimag.wb.WbPackage#getDirectNode_FormalParameters()
	 * @model
	 * @generated
	 */
	String getFormalParameters();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DirectNode#getFormalParameters <em>Formal Parameters</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Formal Parameters</em>' attribute.
	 * @see #getFormalParameters()
	 * @generated
	 */
	void setFormalParameters(String value);

	/**
	 * Returns the value of the '<em><b>Declarations</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declarations</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Declarations</em>' attribute.
	 * @see #setDeclarations(String)
	 * @see org.ujf.verimag.wb.WbPackage#getDirectNode_Declarations()
	 * @model
	 * @generated
	 */
	String getDeclarations();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.DirectNode#getDeclarations <em>Declarations</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Declarations</em>' attribute.
	 * @see #getDeclarations()
	 * @generated
	 */
	void setDeclarations(String value);

	/**
	 * Returns the value of the '<em><b>Node Statistics</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.NodeStatistics}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.NodeStatistics#getDirectNode <em>Direct Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Statistics</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Node Statistics</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getDirectNode_NodeStatistics()
	 * @see org.ujf.verimag.wb.NodeStatistics#getDirectNode
	 * @model opposite="directNode"
	 * @generated
	 */
	EList<NodeStatistics> getNodeStatistics();

} // DirectNode
