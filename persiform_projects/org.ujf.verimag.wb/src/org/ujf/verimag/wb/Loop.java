/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Loop</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Loop#getLoopStart <em>Loop Start</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Loop#getLoopEnd <em>Loop End</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getLoop()
 * @model abstract="true"
 * @generated
 */
public interface Loop extends DirectNode
{
	/**
	 * Returns the value of the '<em><b>Loop Start</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Start</em>' containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Loop Start</em>' containment reference.
	 * @see #setLoopStart(Arc)
	 * @see org.ujf.verimag.wb.WbPackage#getLoop_LoopStart()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Arc getLoopStart();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Loop#getLoopStart <em>Loop Start</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Loop Start</em>' containment reference.
	 * @see #getLoopStart()
	 * @generated
	 */
	void setLoopStart(Arc value);

	/**
	 * Returns the value of the '<em><b>Loop End</b></em>' containment reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop End</em>' containment reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Loop End</em>' containment reference.
	 * @see #setLoopEnd(Arc)
	 * @see org.ujf.verimag.wb.WbPackage#getLoop_LoopEnd()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Arc getLoopEnd();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Loop#getLoopEnd <em>Loop End</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Loop End</em>' containment reference.
	 * @see #getLoopEnd()
	 * @generated
	 */
	void setLoopEnd(Arc value);

} // Loop
