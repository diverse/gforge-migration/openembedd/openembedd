/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Source</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Source#getGenerateCharacteristic <em>Generate Characteristic</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getSource()
 * @model
 * @generated
 */
public interface Source extends DirectNode
{
	/**
	 * Returns the value of the '<em><b>Generate Characteristic</b></em>' attribute. The default value is
	 * <code>"false"</code>. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Characteristic</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Generate Characteristic</em>' attribute.
	 * @see #setGenerateCharacteristic(Boolean)
	 * @see org.ujf.verimag.wb.WbPackage#getSource_GenerateCharacteristic()
	 * @model default="false"
	 * @generated
	 */
	Boolean getGenerateCharacteristic();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Source#getGenerateCharacteristic
	 * <em>Generate Characteristic</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Generate Characteristic</em>' attribute.
	 * @see #getGenerateCharacteristic()
	 * @generated
	 */
	void setGenerateCharacteristic(Boolean value);

} // Source
