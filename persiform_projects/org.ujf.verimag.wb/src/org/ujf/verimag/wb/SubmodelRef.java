/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Submodel Ref</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.SubmodelRef#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getSubmodelRef()
 * @model
 * @generated
 */
public interface SubmodelRef extends RefNode
{
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Submodel#getRef <em>Ref</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Submodel)
	 * @see org.ujf.verimag.wb.WbPackage#getSubmodelRef_Target()
	 * @see org.ujf.verimag.wb.Submodel#getRef
	 * @model opposite="ref" required="true"
	 * @generated
	 */
	Submodel getTarget();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.SubmodelRef#getTarget <em>Target</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Submodel value);

} // SubmodelRef
