/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Variable</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Variable#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Variable#getStorageKind <em>Storage Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Variable#getDatatype <em>Datatype</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Variable#getValue <em>Value</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Variable#getDeclarationZone <em>Declaration Zone</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. The default value is <code>"1"</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getVariable_Dimension()
	 * @model default="1"
	 * @generated
	 */
	Integer getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Variable#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(Integer value);

	/**
	 * Returns the value of the '<em><b>Storage Kind</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.StorageKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Kind</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Storage Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.StorageKind
	 * @see #setStorageKind(StorageKind)
	 * @see org.ujf.verimag.wb.WbPackage#getVariable_StorageKind()
	 * @model
	 * @generated
	 */
	StorageKind getStorageKind();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Variable#getStorageKind <em>Storage Kind</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Storage Kind</em>' attribute.
	 * @see org.ujf.verimag.wb.StorageKind
	 * @see #getStorageKind()
	 * @generated
	 */
	void setStorageKind(StorageKind value);

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' attribute. The literals are from the enumeration
	 * {@link org.ujf.verimag.wb.DataTypeKind}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Datatype</em>' attribute.
	 * @see org.ujf.verimag.wb.DataTypeKind
	 * @see #setDatatype(DataTypeKind)
	 * @see org.ujf.verimag.wb.WbPackage#getVariable_Datatype()
	 * @model
	 * @generated
	 */
	DataTypeKind getDatatype();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Variable#getDatatype <em>Datatype</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Datatype</em>' attribute.
	 * @see org.ujf.verimag.wb.DataTypeKind
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(DataTypeKind value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.ujf.verimag.wb.WbPackage#getVariable_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Variable#getValue <em>Value</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Declaration Zone</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.wb.DeclarationZone#getVariable <em>Variable</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration Zone</em>' container reference isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Declaration Zone</em>' container reference.
	 * @see #setDeclarationZone(DeclarationZone)
	 * @see org.ujf.verimag.wb.WbPackage#getVariable_DeclarationZone()
	 * @see org.ujf.verimag.wb.DeclarationZone#getVariable
	 * @model opposite="variable" required="true"
	 * @generated
	 */
	DeclarationZone getDeclarationZone();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Variable#getDeclarationZone <em>Declaration Zone</em>}'
	 * container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Declaration Zone</em>' container reference.
	 * @see #getDeclarationZone()
	 * @generated
	 */
	void setDeclarationZone(DeclarationZone value);

} // Variable
