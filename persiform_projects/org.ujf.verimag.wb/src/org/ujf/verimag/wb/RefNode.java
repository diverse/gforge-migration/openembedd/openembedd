/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Ref Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.RefNode#getActualParameters <em>Actual Parameters</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getRefNode()
 * @model abstract="true"
 * @generated
 */
public interface RefNode extends GraphNode
{
	/**
	 * Returns the value of the '<em><b>Actual Parameters</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Parameters</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Actual Parameters</em>' attribute.
	 * @see #setActualParameters(String)
	 * @see org.ujf.verimag.wb.WbPackage#getRefNode_ActualParameters()
	 * @model
	 * @generated
	 */
	String getActualParameters();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.RefNode#getActualParameters <em>Actual Parameters</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Actual Parameters</em>' attribute.
	 * @see #getActualParameters()
	 * @generated
	 */
	void setActualParameters(String value);

} // RefNode
