/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Service</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.Service#getServiceTime <em>Service Time</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Service#getServerQuantity <em>Server Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Service#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.Service#getRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getService()
 * @model
 * @generated
 */
public interface Service extends NodeWithQueue, TimedElement
{
	/**
	 * Returns the value of the '<em><b>Service Time</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Time</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Service Time</em>' attribute.
	 * @see #setServiceTime(String)
	 * @see org.ujf.verimag.wb.WbPackage#getService_ServiceTime()
	 * @model
	 * @generated
	 */
	String getServiceTime();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Service#getServiceTime <em>Service Time</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Service Time</em>' attribute.
	 * @see #getServiceTime()
	 * @generated
	 */
	void setServiceTime(String value);

	/**
	 * Returns the value of the '<em><b>Server Quantity</b></em>' attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server Quantity</em>' attribute isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Server Quantity</em>' attribute.
	 * @see #setServerQuantity(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getService_ServerQuantity()
	 * @model
	 * @generated
	 */
	Integer getServerQuantity();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Service#getServerQuantity <em>Server Quantity</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Server Quantity</em>' attribute.
	 * @see #getServerQuantity()
	 * @generated
	 */
	void setServerQuantity(Integer value);

	/**
	 * Returns the value of the '<em><b>module</b></em>' container reference. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Module#getService <em>Service</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>module</em>' container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>module</em>' container reference.
	 * @see #set_module(Module)
	 * @see org.ujf.verimag.wb.WbPackage#getService__module()
	 * @see org.ujf.verimag.wb.Module#getService
	 * @model opposite="service"
	 * @generated
	 */
	Module get_module();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.Service#get_module <em>module</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>module</em>' container reference.
	 * @see #get_module()
	 * @generated
	 */
	void set_module(Module value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.ServiceRef}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.ServiceRef#getTarget <em>Target</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ref</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getService_Ref()
	 * @see org.ujf.verimag.wb.ServiceRef#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<ServiceRef> getRef();

} // Service
