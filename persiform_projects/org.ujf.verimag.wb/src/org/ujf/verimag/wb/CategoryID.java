/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Category ID</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.CategoryID#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.CategoryID#getArc <em>Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.CategoryID#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.CategoryID#getInterrupt <em>Interrupt</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.ujf.verimag.wb.WbPackage#getCategoryID()
 * @model
 * @generated
 */
public interface CategoryID extends NamedElement
{
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' attribute. The default value is <code>"1"</code>. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' attribute isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Dimension</em>' attribute.
	 * @see #setDimension(Integer)
	 * @see org.ujf.verimag.wb.WbPackage#getCategoryID_Dimension()
	 * @model default="1"
	 * @generated
	 */
	Integer getDimension();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.CategoryID#getDimension <em>Dimension</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Dimension</em>' attribute.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(Integer value);

	/**
	 * Returns the value of the '<em><b>Arc</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Arc}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Arc#getSelectCategory <em>Select Category</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc</em>' reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Arc</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getCategoryID_Arc()
	 * @see org.ujf.verimag.wb.Arc#getSelectCategory
	 * @model opposite="selectCategory"
	 * @generated
	 */
	EList<Arc> getArc();

	/**
	 * Returns the value of the '<em><b>Declaration Zone</b></em>' container reference. It is bidirectional and its
	 * opposite is '{@link org.ujf.verimag.wb.DeclarationZone#getCategoryID <em>Category ID</em>}'. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Declaration Zone</em>' container reference isn't clear, there really should be more of
	 * a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Declaration Zone</em>' container reference.
	 * @see #setDeclarationZone(DeclarationZone)
	 * @see org.ujf.verimag.wb.WbPackage#getCategoryID_DeclarationZone()
	 * @see org.ujf.verimag.wb.DeclarationZone#getCategoryID
	 * @model opposite="categoryID" required="true"
	 * @generated
	 */
	DeclarationZone getDeclarationZone();

	/**
	 * Sets the value of the '{@link org.ujf.verimag.wb.CategoryID#getDeclarationZone <em>Declaration Zone</em>}'
	 * container reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *        the new value of the '<em>Declaration Zone</em>' container reference.
	 * @see #getDeclarationZone()
	 * @generated
	 */
	void setDeclarationZone(DeclarationZone value);

	/**
	 * Returns the value of the '<em><b>Interrupt</b></em>' reference list. The list contents are of type
	 * {@link org.ujf.verimag.wb.Interrupt}. It is bidirectional and its opposite is '
	 * {@link org.ujf.verimag.wb.Interrupt#getSelectCategory <em>Select Category</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt</em>' reference list isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Interrupt</em>' reference list.
	 * @see org.ujf.verimag.wb.WbPackage#getCategoryID_Interrupt()
	 * @see org.ujf.verimag.wb.Interrupt#getSelectCategory
	 * @model opposite="selectCategory"
	 * @generated
	 */
	EList<Interrupt> getInterrupt();

} // CategoryID
