/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc --> The <b>Resource </b> associated with the package. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.util.WbResourceFactoryImpl
 * @generated
 */
public class WbResourceImpl extends XMIResourceImpl
{
	protected int	currentId	= 0;

	/**
	 * Creates an instance of the resource. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param uri
	 *        the URI of the new resource.
	 * @generated
	 */
	public WbResourceImpl(URI uri)
	{
		super(uri);
	}

	@Override
	public String getID(EObject object)
	{
		String id = super.getID(object);
		if (id == null)
		{
			do
			{
				// Search for an available slot:
				id = "i" + currentId;
				currentId++;
			}
			while (getEObjectByID(id) != null);
			setID(object, id);
		}
		return id;
	}
} // WbResourceImpl
