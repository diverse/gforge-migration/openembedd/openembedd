/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.AllocateRef;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.BlockRef;
import org.ujf.verimag.wb.Branch;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.ConditionalLoop;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Delay;
import org.ujf.verimag.wb.DirectNode;
import org.ujf.verimag.wb.DuplicationNode;
import org.ujf.verimag.wb.Enter;
import org.ujf.verimag.wb.ForLoop;
import org.ujf.verimag.wb.Fork;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.GraphicalNode;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.Join;
import org.ujf.verimag.wb.Loop;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.NamedElement;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.NodeWithQueue;
import org.ujf.verimag.wb.RefNode;
import org.ujf.verimag.wb.Release;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Resume;
import org.ujf.verimag.wb.Return;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.ServiceRef;
import org.ujf.verimag.wb.Sink;
import org.ujf.verimag.wb.Source;
import org.ujf.verimag.wb.Split;
import org.ujf.verimag.wb.StatisticsCollector;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.SubmodelRef;
import org.ujf.verimag.wb.Super;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.User;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance hierarchy. It supports the call
 * {@link #doSwitch(EObject) doSwitch(object)} to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the inheritance hierarchy until a non-null result is
 * returned, which is the result of the switch. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.WbPackage
 * @generated
 */
public class WbSwitch<T>
{
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static WbPackage	modelPackage;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbSwitch()
	{
		if (modelPackage == null)
		{
			modelPackage = WbPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject)
	{
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject)
	{
		if (theEClass.eContainer() == modelPackage)
		{
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else
		{
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
	 * result. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject)
	{
		switch (classifierID)
		{
		case WbPackage.MODULE:
		{
			Module module = (Module) theEObject;
			T result = caseModule(module);
			if (result == null)
				result = caseNamedElement(module);
			if (result == null)
				result = caseTimedElement(module);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.DECLARATION_ZONE:
		{
			DeclarationZone declarationZone = (DeclarationZone) theEObject;
			T result = caseDeclarationZone(declarationZone);
			if (result == null)
				result = caseGraphicalNode(declarationZone);
			if (result == null)
				result = caseNamedElement(declarationZone);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SUBMODEL:
		{
			Submodel submodel = (Submodel) theEObject;
			T result = caseSubmodel(submodel);
			if (result == null)
				result = caseGraphicalNode(submodel);
			if (result == null)
				result = caseNamedElement(submodel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.ALLOCATE:
		{
			Allocate allocate = (Allocate) theEObject;
			T result = caseAllocate(allocate);
			if (result == null)
				result = caseNodeWithQueue(allocate);
			if (result == null)
				result = caseDirectNode(allocate);
			if (result == null)
				result = caseGraphNode(allocate);
			if (result == null)
				result = caseGraphicalNode(allocate);
			if (result == null)
				result = caseNamedElement(allocate);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SERVICE:
		{
			Service service = (Service) theEObject;
			T result = caseService(service);
			if (result == null)
				result = caseNodeWithQueue(service);
			if (result == null)
				result = caseTimedElement(service);
			if (result == null)
				result = caseDirectNode(service);
			if (result == null)
				result = caseGraphNode(service);
			if (result == null)
				result = caseGraphicalNode(service);
			if (result == null)
				result = caseNamedElement(service);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.RESOURCE_POOL:
		{
			ResourcePool resourcePool = (ResourcePool) theEObject;
			T result = caseResourcePool(resourcePool);
			if (result == null)
				result = caseGraphicalNode(resourcePool);
			if (result == null)
				result = caseNamedElement(resourcePool);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.GRAPH_NODE:
		{
			GraphNode graphNode = (GraphNode) theEObject;
			T result = caseGraphNode(graphNode);
			if (result == null)
				result = caseGraphicalNode(graphNode);
			if (result == null)
				result = caseNamedElement(graphNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.ARC:
		{
			Arc arc = (Arc) theEObject;
			T result = caseArc(arc);
			if (result == null)
				result = caseNamedElement(arc);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.RESPONSE_ARC:
		{
			ResponseArc responseArc = (ResponseArc) theEObject;
			T result = caseResponseArc(responseArc);
			if (result == null)
				result = caseStatisticsCollector(responseArc);
			if (result == null)
				result = caseNamedElement(responseArc);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.CATEGORY_ID:
		{
			CategoryID categoryID = (CategoryID) theEObject;
			T result = caseCategoryID(categoryID);
			if (result == null)
				result = caseNamedElement(categoryID);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.VARIABLE:
		{
			Variable variable = (Variable) theEObject;
			T result = caseVariable(variable);
			if (result == null)
				result = caseNamedElement(variable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.DIRECT_NODE:
		{
			DirectNode directNode = (DirectNode) theEObject;
			T result = caseDirectNode(directNode);
			if (result == null)
				result = caseGraphNode(directNode);
			if (result == null)
				result = caseGraphicalNode(directNode);
			if (result == null)
				result = caseNamedElement(directNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.REF_NODE:
		{
			RefNode refNode = (RefNode) theEObject;
			T result = caseRefNode(refNode);
			if (result == null)
				result = caseGraphNode(refNode);
			if (result == null)
				result = caseGraphicalNode(refNode);
			if (result == null)
				result = caseNamedElement(refNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SUBMODEL_REF:
		{
			SubmodelRef submodelRef = (SubmodelRef) theEObject;
			T result = caseSubmodelRef(submodelRef);
			if (result == null)
				result = caseRefNode(submodelRef);
			if (result == null)
				result = caseGraphNode(submodelRef);
			if (result == null)
				result = caseGraphicalNode(submodelRef);
			if (result == null)
				result = caseNamedElement(submodelRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SERVICE_REF:
		{
			ServiceRef serviceRef = (ServiceRef) theEObject;
			T result = caseServiceRef(serviceRef);
			if (result == null)
				result = caseRefNode(serviceRef);
			if (result == null)
				result = caseGraphNode(serviceRef);
			if (result == null)
				result = caseGraphicalNode(serviceRef);
			if (result == null)
				result = caseNamedElement(serviceRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.ALLOCATE_REF:
		{
			AllocateRef allocateRef = (AllocateRef) theEObject;
			T result = caseAllocateRef(allocateRef);
			if (result == null)
				result = caseRefNode(allocateRef);
			if (result == null)
				result = caseGraphNode(allocateRef);
			if (result == null)
				result = caseGraphicalNode(allocateRef);
			if (result == null)
				result = caseNamedElement(allocateRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.NODE_WITH_QUEUE:
		{
			NodeWithQueue nodeWithQueue = (NodeWithQueue) theEObject;
			T result = caseNodeWithQueue(nodeWithQueue);
			if (result == null)
				result = caseDirectNode(nodeWithQueue);
			if (result == null)
				result = caseGraphNode(nodeWithQueue);
			if (result == null)
				result = caseGraphicalNode(nodeWithQueue);
			if (result == null)
				result = caseNamedElement(nodeWithQueue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.BLOCK:
		{
			Block block = (Block) theEObject;
			T result = caseBlock(block);
			if (result == null)
				result = caseNodeWithQueue(block);
			if (result == null)
				result = caseDirectNode(block);
			if (result == null)
				result = caseGraphNode(block);
			if (result == null)
				result = caseGraphicalNode(block);
			if (result == null)
				result = caseNamedElement(block);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SOURCE:
		{
			Source source = (Source) theEObject;
			T result = caseSource(source);
			if (result == null)
				result = caseDirectNode(source);
			if (result == null)
				result = caseGraphNode(source);
			if (result == null)
				result = caseGraphicalNode(source);
			if (result == null)
				result = caseNamedElement(source);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SINK:
		{
			Sink sink = (Sink) theEObject;
			T result = caseSink(sink);
			if (result == null)
				result = caseDirectNode(sink);
			if (result == null)
				result = caseGraphNode(sink);
			if (result == null)
				result = caseGraphicalNode(sink);
			if (result == null)
				result = caseNamedElement(sink);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.ENTER:
		{
			Enter enter = (Enter) theEObject;
			T result = caseEnter(enter);
			if (result == null)
				result = caseDirectNode(enter);
			if (result == null)
				result = caseGraphNode(enter);
			if (result == null)
				result = caseGraphicalNode(enter);
			if (result == null)
				result = caseNamedElement(enter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.RETURN:
		{
			Return return_ = (Return) theEObject;
			T result = caseReturn(return_);
			if (result == null)
				result = caseDirectNode(return_);
			if (result == null)
				result = caseGraphNode(return_);
			if (result == null)
				result = caseGraphicalNode(return_);
			if (result == null)
				result = caseNamedElement(return_);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SPLIT:
		{
			Split split = (Split) theEObject;
			T result = caseSplit(split);
			if (result == null)
				result = caseDuplicationNode(split);
			if (result == null)
				result = caseDirectNode(split);
			if (result == null)
				result = caseGraphNode(split);
			if (result == null)
				result = caseGraphicalNode(split);
			if (result == null)
				result = caseNamedElement(split);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.FORK:
		{
			Fork fork = (Fork) theEObject;
			T result = caseFork(fork);
			if (result == null)
				result = caseDuplicationNode(fork);
			if (result == null)
				result = caseDirectNode(fork);
			if (result == null)
				result = caseGraphNode(fork);
			if (result == null)
				result = caseGraphicalNode(fork);
			if (result == null)
				result = caseNamedElement(fork);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.JOIN:
		{
			Join join = (Join) theEObject;
			T result = caseJoin(join);
			if (result == null)
				result = caseDirectNode(join);
			if (result == null)
				result = caseGraphNode(join);
			if (result == null)
				result = caseGraphicalNode(join);
			if (result == null)
				result = caseNamedElement(join);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.USER:
		{
			User user = (User) theEObject;
			T result = caseUser(user);
			if (result == null)
				result = caseDirectNode(user);
			if (result == null)
				result = caseGraphNode(user);
			if (result == null)
				result = caseGraphicalNode(user);
			if (result == null)
				result = caseNamedElement(user);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.DELAY:
		{
			Delay delay = (Delay) theEObject;
			T result = caseDelay(delay);
			if (result == null)
				result = caseDirectNode(delay);
			if (result == null)
				result = caseTimedElement(delay);
			if (result == null)
				result = caseGraphNode(delay);
			if (result == null)
				result = caseGraphicalNode(delay);
			if (result == null)
				result = caseNamedElement(delay);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.RELEASE:
		{
			Release release = (Release) theEObject;
			T result = caseRelease(release);
			if (result == null)
				result = caseDirectNode(release);
			if (result == null)
				result = caseGraphNode(release);
			if (result == null)
				result = caseGraphicalNode(release);
			if (result == null)
				result = caseNamedElement(release);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.INTERRUPT:
		{
			Interrupt interrupt = (Interrupt) theEObject;
			T result = caseInterrupt(interrupt);
			if (result == null)
				result = caseDirectNode(interrupt);
			if (result == null)
				result = caseGraphNode(interrupt);
			if (result == null)
				result = caseGraphicalNode(interrupt);
			if (result == null)
				result = caseNamedElement(interrupt);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.LOOP:
		{
			Loop loop = (Loop) theEObject;
			T result = caseLoop(loop);
			if (result == null)
				result = caseDirectNode(loop);
			if (result == null)
				result = caseGraphNode(loop);
			if (result == null)
				result = caseGraphicalNode(loop);
			if (result == null)
				result = caseNamedElement(loop);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.FOR_LOOP:
		{
			ForLoop forLoop = (ForLoop) theEObject;
			T result = caseForLoop(forLoop);
			if (result == null)
				result = caseLoop(forLoop);
			if (result == null)
				result = caseDirectNode(forLoop);
			if (result == null)
				result = caseGraphNode(forLoop);
			if (result == null)
				result = caseGraphicalNode(forLoop);
			if (result == null)
				result = caseNamedElement(forLoop);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.CONDITIONAL_LOOP:
		{
			ConditionalLoop conditionalLoop = (ConditionalLoop) theEObject;
			T result = caseConditionalLoop(conditionalLoop);
			if (result == null)
				result = caseLoop(conditionalLoop);
			if (result == null)
				result = caseDirectNode(conditionalLoop);
			if (result == null)
				result = caseGraphNode(conditionalLoop);
			if (result == null)
				result = caseGraphicalNode(conditionalLoop);
			if (result == null)
				result = caseNamedElement(conditionalLoop);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.STATISTICS_COLLECTOR:
		{
			StatisticsCollector statisticsCollector = (StatisticsCollector) theEObject;
			T result = caseStatisticsCollector(statisticsCollector);
			if (result == null)
				result = caseNamedElement(statisticsCollector);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.NODE_STATISTICS:
		{
			NodeStatistics nodeStatistics = (NodeStatistics) theEObject;
			T result = caseNodeStatistics(nodeStatistics);
			if (result == null)
				result = caseStatisticsCollector(nodeStatistics);
			if (result == null)
				result = caseNamedElement(nodeStatistics);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.NAMED_ELEMENT:
		{
			NamedElement namedElement = (NamedElement) theEObject;
			T result = caseNamedElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.TIMED_ELEMENT:
		{
			TimedElement timedElement = (TimedElement) theEObject;
			T result = caseTimedElement(timedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.SUPER:
		{
			Super super_ = (Super) theEObject;
			T result = caseSuper(super_);
			if (result == null)
				result = caseDirectNode(super_);
			if (result == null)
				result = caseGraphNode(super_);
			if (result == null)
				result = caseGraphicalNode(super_);
			if (result == null)
				result = caseNamedElement(super_);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.RESUME:
		{
			Resume resume = (Resume) theEObject;
			T result = caseResume(resume);
			if (result == null)
				result = caseDirectNode(resume);
			if (result == null)
				result = caseGraphNode(resume);
			if (result == null)
				result = caseGraphicalNode(resume);
			if (result == null)
				result = caseNamedElement(resume);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.BRANCH:
		{
			Branch branch = (Branch) theEObject;
			T result = caseBranch(branch);
			if (result == null)
				result = caseDirectNode(branch);
			if (result == null)
				result = caseGraphNode(branch);
			if (result == null)
				result = caseGraphicalNode(branch);
			if (result == null)
				result = caseNamedElement(branch);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.DUPLICATION_NODE:
		{
			DuplicationNode duplicationNode = (DuplicationNode) theEObject;
			T result = caseDuplicationNode(duplicationNode);
			if (result == null)
				result = caseDirectNode(duplicationNode);
			if (result == null)
				result = caseGraphNode(duplicationNode);
			if (result == null)
				result = caseGraphicalNode(duplicationNode);
			if (result == null)
				result = caseNamedElement(duplicationNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.BLOCK_REF:
		{
			BlockRef blockRef = (BlockRef) theEObject;
			T result = caseBlockRef(blockRef);
			if (result == null)
				result = caseRefNode(blockRef);
			if (result == null)
				result = caseGraphNode(blockRef);
			if (result == null)
				result = caseGraphicalNode(blockRef);
			if (result == null)
				result = caseNamedElement(blockRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case WbPackage.GRAPHICAL_NODE:
		{
			GraphicalNode graphicalNode = (GraphicalNode) theEObject;
			T result = caseGraphicalNode(graphicalNode);
			if (result == null)
				result = caseNamedElement(graphicalNode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Module</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModule(Module object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration Zone</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration Zone</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclarationZone(DeclarationZone object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodel(Submodel object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocate</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocate(Allocate object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseService(Service object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Pool</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Pool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourcePool(ResourcePool object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph Node</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphNode(GraphNode object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arc</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arc</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArc(Arc object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Arc</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Arc</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseArc(ResponseArc object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Category ID</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Category ID</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCategoryID(CategoryID object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Direct Node</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Direct Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDirectNode(DirectNode object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ref Node</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ref Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRefNode(RefNode object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Ref</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelRef(SubmodelRef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Ref</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceRef(ServiceRef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocate Ref</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocate Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocateRef(AllocateRef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node With Queue</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node With Queue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeWithQueue(NodeWithQueue object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSource(Source object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sink</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sink</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSink(Sink object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enter</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnter(Enter object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturn(Return object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Split</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Split</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSplit(Split object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fork</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fork</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFork(Fork object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Join</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Join</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJoin(Join object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUser(User object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelay(Delay object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Release</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Release</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelease(Release object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interrupt</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interrupt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterrupt(Interrupt object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoop(Loop object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Loop</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForLoop(ForLoop object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Loop</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalLoop(ConditionalLoop object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statistics Collector</em>'. <!--
	 * begin-user-doc --> This implementation returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statistics Collector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatisticsCollector(StatisticsCollector object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Statistics</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Statistics</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeStatistics(NodeStatistics object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Element</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedElement(TimedElement object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Super</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Super</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSuper(Super object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resume</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resume</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResume(Resume object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Branch</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Branch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBranch(Branch object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duplication Node</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duplication Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDuplicationNode(DuplicationNode object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Ref</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockRef(BlockRef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graphical Node</em>'. <!-- begin-user-doc
	 * --> This implementation returns null; returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graphical Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGraphicalNode(GraphicalNode object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate the switch, but this is the last case
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @param object
	 *        the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object)
	{
		return null;
	}

} // WbSwitch
