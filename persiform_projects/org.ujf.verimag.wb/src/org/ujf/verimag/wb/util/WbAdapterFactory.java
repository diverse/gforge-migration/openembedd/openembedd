/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.AllocateRef;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.BlockRef;
import org.ujf.verimag.wb.Branch;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.ConditionalLoop;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Delay;
import org.ujf.verimag.wb.DirectNode;
import org.ujf.verimag.wb.DuplicationNode;
import org.ujf.verimag.wb.Enter;
import org.ujf.verimag.wb.ForLoop;
import org.ujf.verimag.wb.Fork;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.GraphicalNode;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.Join;
import org.ujf.verimag.wb.Loop;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.NamedElement;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.NodeWithQueue;
import org.ujf.verimag.wb.RefNode;
import org.ujf.verimag.wb.Release;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Resume;
import org.ujf.verimag.wb.Return;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.ServiceRef;
import org.ujf.verimag.wb.Sink;
import org.ujf.verimag.wb.Source;
import org.ujf.verimag.wb.Split;
import org.ujf.verimag.wb.StatisticsCollector;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.SubmodelRef;
import org.ujf.verimag.wb.Super;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.User;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides an adapter <code>createXXX</code>
 * method for each class of the model. <!-- end-user-doc -->
 * 
 * @see org.ujf.verimag.wb.WbPackage
 * @generated
 */
public class WbAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static WbPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = WbPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object. <!-- begin-user-doc --> This
	 * implementation returns <code>true</code> if the object is either the model's package or is an instance object of
	 * the model. <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected WbSwitch<Adapter>	modelSwitch	= new WbSwitch<Adapter>()
											{
												@Override
												public Adapter caseModule(Module object)
												{
													return createModuleAdapter();
												}
												@Override
												public Adapter caseDeclarationZone(DeclarationZone object)
												{
													return createDeclarationZoneAdapter();
												}
												@Override
												public Adapter caseSubmodel(Submodel object)
												{
													return createSubmodelAdapter();
												}
												@Override
												public Adapter caseAllocate(Allocate object)
												{
													return createAllocateAdapter();
												}
												@Override
												public Adapter caseService(Service object)
												{
													return createServiceAdapter();
												}
												@Override
												public Adapter caseResourcePool(ResourcePool object)
												{
													return createResourcePoolAdapter();
												}
												@Override
												public Adapter caseGraphNode(GraphNode object)
												{
													return createGraphNodeAdapter();
												}
												@Override
												public Adapter caseArc(Arc object)
												{
													return createArcAdapter();
												}
												@Override
												public Adapter caseResponseArc(ResponseArc object)
												{
													return createResponseArcAdapter();
												}
												@Override
												public Adapter caseCategoryID(CategoryID object)
												{
													return createCategoryIDAdapter();
												}
												@Override
												public Adapter caseVariable(Variable object)
												{
													return createVariableAdapter();
												}
												@Override
												public Adapter caseDirectNode(DirectNode object)
												{
													return createDirectNodeAdapter();
												}
												@Override
												public Adapter caseRefNode(RefNode object)
												{
													return createRefNodeAdapter();
												}
												@Override
												public Adapter caseSubmodelRef(SubmodelRef object)
												{
													return createSubmodelRefAdapter();
												}
												@Override
												public Adapter caseServiceRef(ServiceRef object)
												{
													return createServiceRefAdapter();
												}
												@Override
												public Adapter caseAllocateRef(AllocateRef object)
												{
													return createAllocateRefAdapter();
												}
												@Override
												public Adapter caseNodeWithQueue(NodeWithQueue object)
												{
													return createNodeWithQueueAdapter();
												}
												@Override
												public Adapter caseBlock(Block object)
												{
													return createBlockAdapter();
												}
												@Override
												public Adapter caseSource(Source object)
												{
													return createSourceAdapter();
												}
												@Override
												public Adapter caseSink(Sink object)
												{
													return createSinkAdapter();
												}
												@Override
												public Adapter caseEnter(Enter object)
												{
													return createEnterAdapter();
												}
												@Override
												public Adapter caseReturn(Return object)
												{
													return createReturnAdapter();
												}
												@Override
												public Adapter caseSplit(Split object)
												{
													return createSplitAdapter();
												}
												@Override
												public Adapter caseFork(Fork object)
												{
													return createForkAdapter();
												}
												@Override
												public Adapter caseJoin(Join object)
												{
													return createJoinAdapter();
												}
												@Override
												public Adapter caseUser(User object)
												{
													return createUserAdapter();
												}
												@Override
												public Adapter caseDelay(Delay object)
												{
													return createDelayAdapter();
												}
												@Override
												public Adapter caseRelease(Release object)
												{
													return createReleaseAdapter();
												}
												@Override
												public Adapter caseInterrupt(Interrupt object)
												{
													return createInterruptAdapter();
												}
												@Override
												public Adapter caseLoop(Loop object)
												{
													return createLoopAdapter();
												}
												@Override
												public Adapter caseForLoop(ForLoop object)
												{
													return createForLoopAdapter();
												}
												@Override
												public Adapter caseConditionalLoop(ConditionalLoop object)
												{
													return createConditionalLoopAdapter();
												}
												@Override
												public Adapter caseStatisticsCollector(StatisticsCollector object)
												{
													return createStatisticsCollectorAdapter();
												}
												@Override
												public Adapter caseNodeStatistics(NodeStatistics object)
												{
													return createNodeStatisticsAdapter();
												}
												@Override
												public Adapter caseNamedElement(NamedElement object)
												{
													return createNamedElementAdapter();
												}
												@Override
												public Adapter caseTimedElement(TimedElement object)
												{
													return createTimedElementAdapter();
												}
												@Override
												public Adapter caseSuper(Super object)
												{
													return createSuperAdapter();
												}
												@Override
												public Adapter caseResume(Resume object)
												{
													return createResumeAdapter();
												}
												@Override
												public Adapter caseBranch(Branch object)
												{
													return createBranchAdapter();
												}
												@Override
												public Adapter caseDuplicationNode(DuplicationNode object)
												{
													return createDuplicationNodeAdapter();
												}
												@Override
												public Adapter caseBlockRef(BlockRef object)
												{
													return createBlockRefAdapter();
												}
												@Override
												public Adapter caseGraphicalNode(GraphicalNode object)
												{
													return createGraphicalNodeAdapter();
												}
												@Override
												public Adapter defaultCase(EObject object)
												{
													return createEObjectAdapter();
												}
											};

	/**
	 * Creates an adapter for the <code>target</code>. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param target
	 *        the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Module <em>Module</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Module
	 * @generated
	 */
	public Adapter createModuleAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.DeclarationZone
	 * <em>Declaration Zone</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.DeclarationZone
	 * @generated
	 */
	public Adapter createDeclarationZoneAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Submodel <em>Submodel</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Submodel
	 * @generated
	 */
	public Adapter createSubmodelAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Allocate <em>Allocate</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Allocate
	 * @generated
	 */
	public Adapter createAllocateAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Service <em>Service</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Service
	 * @generated
	 */
	public Adapter createServiceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.ResourcePool <em>Resource Pool</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.ResourcePool
	 * @generated
	 */
	public Adapter createResourcePoolAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.GraphNode <em>Graph Node</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.GraphNode
	 * @generated
	 */
	public Adapter createGraphNodeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Arc <em>Arc</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Arc
	 * @generated
	 */
	public Adapter createArcAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.ResponseArc <em>Response Arc</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.ResponseArc
	 * @generated
	 */
	public Adapter createResponseArcAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.CategoryID <em>Category ID</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.CategoryID
	 * @generated
	 */
	public Adapter createCategoryIDAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Variable <em>Variable</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.DirectNode <em>Direct Node</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.DirectNode
	 * @generated
	 */
	public Adapter createDirectNodeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.RefNode <em>Ref Node</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.RefNode
	 * @generated
	 */
	public Adapter createRefNodeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.SubmodelRef <em>Submodel Ref</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.SubmodelRef
	 * @generated
	 */
	public Adapter createSubmodelRefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.ServiceRef <em>Service Ref</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.ServiceRef
	 * @generated
	 */
	public Adapter createServiceRefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.AllocateRef <em>Allocate Ref</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.AllocateRef
	 * @generated
	 */
	public Adapter createAllocateRefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.NodeWithQueue <em>Node With Queue</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.NodeWithQueue
	 * @generated
	 */
	public Adapter createNodeWithQueueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Block <em>Block</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Block
	 * @generated
	 */
	public Adapter createBlockAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Source <em>Source</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Source
	 * @generated
	 */
	public Adapter createSourceAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Sink <em>Sink</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Sink
	 * @generated
	 */
	public Adapter createSinkAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Enter <em>Enter</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Enter
	 * @generated
	 */
	public Adapter createEnterAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Return <em>Return</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Return
	 * @generated
	 */
	public Adapter createReturnAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Split <em>Split</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Split
	 * @generated
	 */
	public Adapter createSplitAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Fork <em>Fork</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Fork
	 * @generated
	 */
	public Adapter createForkAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Join <em>Join</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Join
	 * @generated
	 */
	public Adapter createJoinAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.User <em>User</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.User
	 * @generated
	 */
	public Adapter createUserAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Delay <em>Delay</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Delay
	 * @generated
	 */
	public Adapter createDelayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Release <em>Release</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Release
	 * @generated
	 */
	public Adapter createReleaseAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Interrupt <em>Interrupt</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Interrupt
	 * @generated
	 */
	public Adapter createInterruptAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Loop <em>Loop</em>}'. <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore cases; it's useful to ignore a case
	 * when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Loop
	 * @generated
	 */
	public Adapter createLoopAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.ForLoop <em>For Loop</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.ForLoop
	 * @generated
	 */
	public Adapter createForLoopAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.ConditionalLoop
	 * <em>Conditional Loop</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.ConditionalLoop
	 * @generated
	 */
	public Adapter createConditionalLoopAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.StatisticsCollector
	 * <em>Statistics Collector</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.StatisticsCollector
	 * @generated
	 */
	public Adapter createStatisticsCollectorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.NodeStatistics <em>Node Statistics</em>}
	 * '. <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.NodeStatistics
	 * @generated
	 */
	public Adapter createNodeStatisticsAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.TimedElement <em>Timed Element</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.TimedElement
	 * @generated
	 */
	public Adapter createTimedElementAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Super <em>Super</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Super
	 * @generated
	 */
	public Adapter createSuperAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Resume <em>Resume</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Resume
	 * @generated
	 */
	public Adapter createResumeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.Branch <em>Branch</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.Branch
	 * @generated
	 */
	public Adapter createBranchAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.DuplicationNode
	 * <em>Duplication Node</em>}'. <!-- begin-user-doc --> This default implementation returns null so that we can
	 * easily ignore cases; it's useful to ignore a case when inheritance will catch all the cases anyway. <!--
	 * end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.DuplicationNode
	 * @generated
	 */
	public Adapter createDuplicationNodeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.BlockRef <em>Block Ref</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful to
	 * ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.BlockRef
	 * @generated
	 */
	public Adapter createBlockRefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.ujf.verimag.wb.GraphicalNode <em>Graphical Node</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that we can easily ignore cases; it's useful
	 * to ignore a case when inheritance will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.ujf.verimag.wb.GraphicalNode
	 * @generated
	 */
	public Adapter createGraphicalNodeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case. <!-- begin-user-doc --> This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} // WbAdapterFactory
