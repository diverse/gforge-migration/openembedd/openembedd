/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Interrupt</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.InterruptImpl#getSelectCondition <em>Select Condition</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.InterruptImpl#getSelectQuantity <em>Select Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.InterruptImpl#getSelectNode <em>Select Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.InterruptImpl#getSelectCategory <em>Select Category</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class InterruptImpl extends DirectNodeImpl implements Interrupt
{
	/**
	 * The default value of the '{@link #getSelectCondition() <em>Select Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String	SELECT_CONDITION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getSelectCondition() <em>Select Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCondition()
	 * @generated
	 * @ordered
	 */
	protected String				selectCondition				= SELECT_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSelectQuantity() <em>Select Quantity</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final Integer	SELECT_QUANTITY_EDEFAULT	= new Integer(1);

	/**
	 * The cached value of the '{@link #getSelectQuantity() <em>Select Quantity</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectQuantity()
	 * @generated
	 * @ordered
	 */
	protected Integer				selectQuantity				= SELECT_QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSelectNode() <em>Select Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSelectNode()
	 * @generated
	 * @ordered
	 */
	protected GraphNode				selectNode;

	/**
	 * The cached value of the '{@link #getSelectCategory() <em>Select Category</em>}' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCategory()
	 * @generated
	 * @ordered
	 */
	protected CategoryID			selectCategory;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected InterruptImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.INTERRUPT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSelectCondition()
	{
		return selectCondition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectCondition(String newSelectCondition)
	{
		String oldSelectCondition = selectCondition;
		selectCondition = newSelectCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.INTERRUPT__SELECT_CONDITION,
				oldSelectCondition, selectCondition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getSelectQuantity()
	{
		return selectQuantity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectQuantity(Integer newSelectQuantity)
	{
		Integer oldSelectQuantity = selectQuantity;
		selectQuantity = newSelectQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.INTERRUPT__SELECT_QUANTITY,
				oldSelectQuantity, selectQuantity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode getSelectNode()
	{
		if (selectNode != null && selectNode.eIsProxy())
		{
			InternalEObject oldSelectNode = (InternalEObject) selectNode;
			selectNode = (GraphNode) eResolveProxy(oldSelectNode);
			if (selectNode != oldSelectNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.INTERRUPT__SELECT_NODE,
						oldSelectNode, selectNode));
			}
		}
		return selectNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode basicGetSelectNode()
	{
		return selectNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSelectNode(GraphNode newSelectNode, NotificationChain msgs)
	{
		GraphNode oldSelectNode = selectNode;
		selectNode = newSelectNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.INTERRUPT__SELECT_NODE, oldSelectNode, newSelectNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectNode(GraphNode newSelectNode)
	{
		if (newSelectNode != selectNode)
		{
			NotificationChain msgs = null;
			if (selectNode != null)
				msgs = ((InternalEObject) selectNode).eInverseRemove(this, WbPackage.GRAPH_NODE__INTERRUPT,
					GraphNode.class, msgs);
			if (newSelectNode != null)
				msgs = ((InternalEObject) newSelectNode).eInverseAdd(this, WbPackage.GRAPH_NODE__INTERRUPT,
					GraphNode.class, msgs);
			msgs = basicSetSelectNode(newSelectNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.INTERRUPT__SELECT_NODE, newSelectNode,
				newSelectNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CategoryID getSelectCategory()
	{
		if (selectCategory != null && selectCategory.eIsProxy())
		{
			InternalEObject oldSelectCategory = (InternalEObject) selectCategory;
			selectCategory = (CategoryID) eResolveProxy(oldSelectCategory);
			if (selectCategory != oldSelectCategory)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.INTERRUPT__SELECT_CATEGORY,
						oldSelectCategory, selectCategory));
			}
		}
		return selectCategory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CategoryID basicGetSelectCategory()
	{
		return selectCategory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSelectCategory(CategoryID newSelectCategory, NotificationChain msgs)
	{
		CategoryID oldSelectCategory = selectCategory;
		selectCategory = newSelectCategory;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.INTERRUPT__SELECT_CATEGORY, oldSelectCategory, newSelectCategory);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectCategory(CategoryID newSelectCategory)
	{
		if (newSelectCategory != selectCategory)
		{
			NotificationChain msgs = null;
			if (selectCategory != null)
				msgs = ((InternalEObject) selectCategory).eInverseRemove(this, WbPackage.CATEGORY_ID__INTERRUPT,
					CategoryID.class, msgs);
			if (newSelectCategory != null)
				msgs = ((InternalEObject) newSelectCategory).eInverseAdd(this, WbPackage.CATEGORY_ID__INTERRUPT,
					CategoryID.class, msgs);
			msgs = basicSetSelectCategory(newSelectCategory, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.INTERRUPT__SELECT_CATEGORY,
				newSelectCategory, newSelectCategory));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_NODE:
			if (selectNode != null)
				msgs = ((InternalEObject) selectNode).eInverseRemove(this, WbPackage.GRAPH_NODE__INTERRUPT,
					GraphNode.class, msgs);
			return basicSetSelectNode((GraphNode) otherEnd, msgs);
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			if (selectCategory != null)
				msgs = ((InternalEObject) selectCategory).eInverseRemove(this, WbPackage.CATEGORY_ID__INTERRUPT,
					CategoryID.class, msgs);
			return basicSetSelectCategory((CategoryID) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_NODE:
			return basicSetSelectNode(null, msgs);
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			return basicSetSelectCategory(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_CONDITION:
			return getSelectCondition();
		case WbPackage.INTERRUPT__SELECT_QUANTITY:
			return getSelectQuantity();
		case WbPackage.INTERRUPT__SELECT_NODE:
			if (resolve)
				return getSelectNode();
			return basicGetSelectNode();
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			if (resolve)
				return getSelectCategory();
			return basicGetSelectCategory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_CONDITION:
			setSelectCondition((String) newValue);
			return;
		case WbPackage.INTERRUPT__SELECT_QUANTITY:
			setSelectQuantity((Integer) newValue);
			return;
		case WbPackage.INTERRUPT__SELECT_NODE:
			setSelectNode((GraphNode) newValue);
			return;
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			setSelectCategory((CategoryID) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_CONDITION:
			setSelectCondition(SELECT_CONDITION_EDEFAULT);
			return;
		case WbPackage.INTERRUPT__SELECT_QUANTITY:
			setSelectQuantity(SELECT_QUANTITY_EDEFAULT);
			return;
		case WbPackage.INTERRUPT__SELECT_NODE:
			setSelectNode((GraphNode) null);
			return;
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			setSelectCategory((CategoryID) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.INTERRUPT__SELECT_CONDITION:
			return SELECT_CONDITION_EDEFAULT == null ? selectCondition != null : !SELECT_CONDITION_EDEFAULT
					.equals(selectCondition);
		case WbPackage.INTERRUPT__SELECT_QUANTITY:
			return SELECT_QUANTITY_EDEFAULT == null ? selectQuantity != null : !SELECT_QUANTITY_EDEFAULT
					.equals(selectQuantity);
		case WbPackage.INTERRUPT__SELECT_NODE:
			return selectNode != null;
		case WbPackage.INTERRUPT__SELECT_CATEGORY:
			return selectCategory != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selectCondition: ");
		result.append(selectCondition);
		result.append(", selectQuantity: ");
		result.append(selectQuantity);
		result.append(')');
		return result.toString();
	}

} // InterruptImpl
