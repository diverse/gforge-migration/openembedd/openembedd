/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.AllocateRef;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Allocate</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#getQuantity <em>Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#getGenerateCharacteristic <em>Generate Characteristic</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#getResourceInstanceSpec <em>Resource Instance Spec</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#getRef <em>Ref</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.AllocateImpl#getResourceInstance <em>Resource Instance</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class AllocateImpl extends NodeWithQueueImpl implements Allocate
{
	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final String	QUANTITY_EDEFAULT					= null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected String				quantity							= QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	GENERATE_CHARACTERISTIC_EDEFAULT	= Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected Boolean				generateCharacteristic				= GENERATE_CHARACTERISTIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getResourceInstanceSpec() <em>Resource Instance Spec</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getResourceInstanceSpec()
	 * @generated
	 * @ordered
	 */
	protected static final String	RESOURCE_INSTANCE_SPEC_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getResourceInstanceSpec() <em>Resource Instance Spec</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getResourceInstanceSpec()
	 * @generated
	 * @ordered
	 */
	protected String				resourceInstanceSpec				= RESOURCE_INSTANCE_SPEC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected EList<AllocateRef>	ref;

	/**
	 * The cached value of the '{@link #getResourceInstance() <em>Resource Instance</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getResourceInstance()
	 * @generated
	 * @ordered
	 */
	protected ResourcePool			resourceInstance;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected AllocateImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.ALLOCATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setQuantity(String newQuantity)
	{
		String oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ALLOCATE__QUANTITY, oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getGenerateCharacteristic()
	{
		return generateCharacteristic;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setGenerateCharacteristic(Boolean newGenerateCharacteristic)
	{
		Boolean oldGenerateCharacteristic = generateCharacteristic;
		generateCharacteristic = newGenerateCharacteristic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC,
				oldGenerateCharacteristic, generateCharacteristic));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getResourceInstanceSpec()
	{
		return resourceInstanceSpec;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setResourceInstanceSpec(String newResourceInstanceSpec)
	{
		String oldResourceInstanceSpec = resourceInstanceSpec;
		resourceInstanceSpec = newResourceInstanceSpec;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC,
				oldResourceInstanceSpec, resourceInstanceSpec));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.ALLOCATE__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.ALLOCATE__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.ALLOCATE__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__ALLOCATE, Module.class, msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ALLOCATE__MODULE, new_module, new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<AllocateRef> getRef()
	{
		if (ref == null)
		{
			ref = new EObjectWithInverseResolvingEList<AllocateRef>(AllocateRef.class, this, WbPackage.ALLOCATE__REF,
				WbPackage.ALLOCATE_REF__TARGET);
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResourcePool getResourceInstance()
	{
		if (resourceInstance != null && resourceInstance.eIsProxy())
		{
			InternalEObject oldResourceInstance = (InternalEObject) resourceInstance;
			resourceInstance = (ResourcePool) eResolveProxy(oldResourceInstance);
			if (resourceInstance != oldResourceInstance)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.ALLOCATE__RESOURCE_INSTANCE,
						oldResourceInstance, resourceInstance));
			}
		}
		return resourceInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResourcePool basicGetResourceInstance()
	{
		return resourceInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetResourceInstance(ResourcePool newResourceInstance, NotificationChain msgs)
	{
		ResourcePool oldResourceInstance = resourceInstance;
		resourceInstance = newResourceInstance;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.ALLOCATE__RESOURCE_INSTANCE, oldResourceInstance, newResourceInstance);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setResourceInstance(ResourcePool newResourceInstance)
	{
		if (newResourceInstance != resourceInstance)
		{
			NotificationChain msgs = null;
			if (resourceInstance != null)
				msgs = ((InternalEObject) resourceInstance).eInverseRemove(this, WbPackage.RESOURCE_POOL__ALLOCATE,
					ResourcePool.class, msgs);
			if (newResourceInstance != null)
				msgs = ((InternalEObject) newResourceInstance).eInverseAdd(this, WbPackage.RESOURCE_POOL__ALLOCATE,
					ResourcePool.class, msgs);
			msgs = basicSetResourceInstance(newResourceInstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ALLOCATE__RESOURCE_INSTANCE,
				newResourceInstance, newResourceInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.ALLOCATE__REF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRef()).basicAdd(otherEnd, msgs);
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			if (resourceInstance != null)
				msgs = ((InternalEObject) resourceInstance).eInverseRemove(this, WbPackage.RESOURCE_POOL__ALLOCATE,
					ResourcePool.class, msgs);
			return basicSetResourceInstance((ResourcePool) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.ALLOCATE__REF:
			return ((InternalEList<?>) getRef()).basicRemove(otherEnd, msgs);
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			return basicSetResourceInstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.ALLOCATE__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__ALLOCATE, Module.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__QUANTITY:
			return getQuantity();
		case WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC:
			return getGenerateCharacteristic();
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC:
			return getResourceInstanceSpec();
		case WbPackage.ALLOCATE__MODULE:
			return get_module();
		case WbPackage.ALLOCATE__REF:
			return getRef();
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			if (resolve)
				return getResourceInstance();
			return basicGetResourceInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__QUANTITY:
			setQuantity((String) newValue);
			return;
		case WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic((Boolean) newValue);
			return;
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC:
			setResourceInstanceSpec((String) newValue);
			return;
		case WbPackage.ALLOCATE__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.ALLOCATE__REF:
			getRef().clear();
			getRef().addAll((Collection<? extends AllocateRef>) newValue);
			return;
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			setResourceInstance((ResourcePool) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic(GENERATE_CHARACTERISTIC_EDEFAULT);
			return;
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC:
			setResourceInstanceSpec(RESOURCE_INSTANCE_SPEC_EDEFAULT);
			return;
		case WbPackage.ALLOCATE__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.ALLOCATE__REF:
			getRef().clear();
			return;
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			setResourceInstance((ResourcePool) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.ALLOCATE__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case WbPackage.ALLOCATE__GENERATE_CHARACTERISTIC:
			return GENERATE_CHARACTERISTIC_EDEFAULT == null ? generateCharacteristic != null
					: !GENERATE_CHARACTERISTIC_EDEFAULT.equals(generateCharacteristic);
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE_SPEC:
			return RESOURCE_INSTANCE_SPEC_EDEFAULT == null ? resourceInstanceSpec != null
					: !RESOURCE_INSTANCE_SPEC_EDEFAULT.equals(resourceInstanceSpec);
		case WbPackage.ALLOCATE__MODULE:
			return get_module() != null;
		case WbPackage.ALLOCATE__REF:
			return ref != null && !ref.isEmpty();
		case WbPackage.ALLOCATE__RESOURCE_INSTANCE:
			return resourceInstance != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quantity: ");
		result.append(quantity);
		result.append(", generateCharacteristic: ");
		result.append(generateCharacteristic);
		result.append(", resourceInstanceSpec: ");
		result.append(resourceInstanceSpec);
		result.append(')');
		return result.toString();
	}

} // AllocateImpl
