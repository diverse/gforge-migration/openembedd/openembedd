/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.ConditionalLoop;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Conditional Loop</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ConditionalLoopImpl#getIsWhile <em>Is While</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ConditionalLoopImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ConditionalLoopImpl extends LoopImpl implements ConditionalLoop
{
	/**
	 * The default value of the '{@link #getIsWhile() <em>Is While</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getIsWhile()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	IS_WHILE_EDEFAULT	= Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getIsWhile() <em>Is While</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getIsWhile()
	 * @generated
	 * @ordered
	 */
	protected Boolean				isWhile				= IS_WHILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCondition() <em>Condition</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String	CONDITION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected String				condition			= CONDITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConditionalLoopImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.CONDITIONAL_LOOP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getIsWhile()
	{
		return isWhile;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setIsWhile(Boolean newIsWhile)
	{
		Boolean oldIsWhile = isWhile;
		isWhile = newIsWhile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.CONDITIONAL_LOOP__IS_WHILE, oldIsWhile,
				isWhile));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getCondition()
	{
		return condition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCondition(String newCondition)
	{
		String oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.CONDITIONAL_LOOP__CONDITION, oldCondition,
				condition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.CONDITIONAL_LOOP__IS_WHILE:
			return getIsWhile();
		case WbPackage.CONDITIONAL_LOOP__CONDITION:
			return getCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.CONDITIONAL_LOOP__IS_WHILE:
			setIsWhile((Boolean) newValue);
			return;
		case WbPackage.CONDITIONAL_LOOP__CONDITION:
			setCondition((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.CONDITIONAL_LOOP__IS_WHILE:
			setIsWhile(IS_WHILE_EDEFAULT);
			return;
		case WbPackage.CONDITIONAL_LOOP__CONDITION:
			setCondition(CONDITION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.CONDITIONAL_LOOP__IS_WHILE:
			return IS_WHILE_EDEFAULT == null ? isWhile != null : !IS_WHILE_EDEFAULT.equals(isWhile);
		case WbPackage.CONDITIONAL_LOOP__CONDITION:
			return CONDITION_EDEFAULT == null ? condition != null : !CONDITION_EDEFAULT.equals(condition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isWhile: ");
		result.append(isWhile);
		result.append(", condition: ");
		result.append(condition);
		result.append(')');
		return result.toString();
	}

} // ConditionalLoopImpl
