/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.DirectNode;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.StatClassKind;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Node Statistics</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.NodeStatisticsImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.NodeStatisticsImpl#getKind <em>Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.NodeStatisticsImpl#getDirectNode <em>Direct Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class NodeStatisticsImpl extends StatisticsCollectorImpl implements NodeStatistics
{
	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final Integer			DIMENSION_EDEFAULT	= new Integer(1);

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected Integer						dimension			= DIMENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final StatClassKind	KIND_EDEFAULT		= StatClassKind.CONTINUOUS;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected StatClassKind					kind				= KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDirectNode() <em>Direct Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDirectNode()
	 * @generated
	 * @ordered
	 */
	protected DirectNode					directNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected NodeStatisticsImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.NODE_STATISTICS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(Integer newDimension)
	{
		Integer oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_STATISTICS__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatClassKind getKind()
	{
		return kind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setKind(StatClassKind newKind)
	{
		StatClassKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_STATISTICS__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DirectNode getDirectNode()
	{
		if (directNode != null && directNode.eIsProxy())
		{
			InternalEObject oldDirectNode = (InternalEObject) directNode;
			directNode = (DirectNode) eResolveProxy(oldDirectNode);
			if (directNode != oldDirectNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.NODE_STATISTICS__DIRECT_NODE,
						oldDirectNode, directNode));
			}
		}
		return directNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DirectNode basicGetDirectNode()
	{
		return directNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetDirectNode(DirectNode newDirectNode, NotificationChain msgs)
	{
		DirectNode oldDirectNode = directNode;
		directNode = newDirectNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.NODE_STATISTICS__DIRECT_NODE, oldDirectNode, newDirectNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDirectNode(DirectNode newDirectNode)
	{
		if (newDirectNode != directNode)
		{
			NotificationChain msgs = null;
			if (directNode != null)
				msgs = ((InternalEObject) directNode).eInverseRemove(this, WbPackage.DIRECT_NODE__NODE_STATISTICS,
					DirectNode.class, msgs);
			if (newDirectNode != null)
				msgs = ((InternalEObject) newDirectNode).eInverseAdd(this, WbPackage.DIRECT_NODE__NODE_STATISTICS,
					DirectNode.class, msgs);
			msgs = basicSetDirectNode(newDirectNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_STATISTICS__DIRECT_NODE,
				newDirectNode, newDirectNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			if (directNode != null)
				msgs = ((InternalEObject) directNode).eInverseRemove(this, WbPackage.DIRECT_NODE__NODE_STATISTICS,
					DirectNode.class, msgs);
			return basicSetDirectNode((DirectNode) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			return basicSetDirectNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIMENSION:
			return getDimension();
		case WbPackage.NODE_STATISTICS__KIND:
			return getKind();
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			if (resolve)
				return getDirectNode();
			return basicGetDirectNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIMENSION:
			setDimension((Integer) newValue);
			return;
		case WbPackage.NODE_STATISTICS__KIND:
			setKind((StatClassKind) newValue);
			return;
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			setDirectNode((DirectNode) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.NODE_STATISTICS__KIND:
			setKind(KIND_EDEFAULT);
			return;
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			setDirectNode((DirectNode) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.NODE_STATISTICS__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.NODE_STATISTICS__KIND:
			return kind != KIND_EDEFAULT;
		case WbPackage.NODE_STATISTICS__DIRECT_NODE:
			return directNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(", kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} // NodeStatisticsImpl
