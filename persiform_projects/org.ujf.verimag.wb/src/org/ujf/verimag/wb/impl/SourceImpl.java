/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.Source;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Source</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.SourceImpl#getGenerateCharacteristic <em>Generate Characteristic</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class SourceImpl extends DirectNodeImpl implements Source
{
	/**
	 * The default value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	GENERATE_CHARACTERISTIC_EDEFAULT	= Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected Boolean				generateCharacteristic				= GENERATE_CHARACTERISTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SourceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.SOURCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getGenerateCharacteristic()
	{
		return generateCharacteristic;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setGenerateCharacteristic(Boolean newGenerateCharacteristic)
	{
		Boolean oldGenerateCharacteristic = generateCharacteristic;
		generateCharacteristic = newGenerateCharacteristic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SOURCE__GENERATE_CHARACTERISTIC,
				oldGenerateCharacteristic, generateCharacteristic));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.SOURCE__GENERATE_CHARACTERISTIC:
			return getGenerateCharacteristic();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.SOURCE__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SOURCE__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic(GENERATE_CHARACTERISTIC_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SOURCE__GENERATE_CHARACTERISTIC:
			return GENERATE_CHARACTERISTIC_EDEFAULT == null ? generateCharacteristic != null
					: !GENERATE_CHARACTERISTIC_EDEFAULT.equals(generateCharacteristic);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (generateCharacteristic: ");
		result.append(generateCharacteristic);
		result.append(')');
		return result.toString();
	}

} // SourceImpl
