/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Graph Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getOutArc <em>Out Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getInArc <em>In Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getInterrupt <em>Interrupt</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getOutResponseArc <em>Out Response Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.GraphNodeImpl#getInResponseArc <em>In Response Arc</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class GraphNodeImpl extends GraphicalNodeImpl implements GraphNode
{
	/**
	 * The cached value of the '{@link #getOutArc() <em>Out Arc</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOutArc()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc>			outArc;

	/**
	 * The cached value of the '{@link #getInArc() <em>In Arc</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInArc()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc>			inArc;

	/**
	 * The cached value of the '{@link #getInterrupt() <em>Interrupt</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInterrupt()
	 * @generated
	 * @ordered
	 */
	protected EList<Interrupt>		interrupt;

	/**
	 * The cached value of the '{@link #getOutResponseArc() <em>Out Response Arc</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getOutResponseArc()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseArc>	outResponseArc;

	/**
	 * The cached value of the '{@link #getInResponseArc() <em>In Response Arc</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInResponseArc()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseArc>	inResponseArc;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected GraphNodeImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.GRAPH_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Arc> getOutArc()
	{
		if (outArc == null)
		{
			outArc = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, WbPackage.GRAPH_NODE__OUT_ARC,
				WbPackage.ARC__FROM_NODE);
		}
		return outArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Arc> getInArc()
	{
		if (inArc == null)
		{
			inArc = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, WbPackage.GRAPH_NODE__IN_ARC,
				WbPackage.ARC__TO_NODE);
		}
		return inArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Submodel getSubmodel()
	{
		if (eContainerFeatureID != WbPackage.GRAPH_NODE__SUBMODEL)
			return null;
		return (Submodel) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSubmodel(Submodel newSubmodel, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSubmodel, WbPackage.GRAPH_NODE__SUBMODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSubmodel(Submodel newSubmodel)
	{
		if (newSubmodel != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.GRAPH_NODE__SUBMODEL && newSubmodel != null))
		{
			if (EcoreUtil.isAncestor(this, newSubmodel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubmodel != null)
				msgs = ((InternalEObject) newSubmodel)
						.eInverseAdd(this, WbPackage.SUBMODEL__NODE, Submodel.class, msgs);
			msgs = basicSetSubmodel(newSubmodel, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.GRAPH_NODE__SUBMODEL, newSubmodel,
				newSubmodel));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Interrupt> getInterrupt()
	{
		if (interrupt == null)
		{
			interrupt = new EObjectWithInverseResolvingEList<Interrupt>(Interrupt.class, this,
				WbPackage.GRAPH_NODE__INTERRUPT, WbPackage.INTERRUPT__SELECT_NODE);
		}
		return interrupt;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ResponseArc> getOutResponseArc()
	{
		if (outResponseArc == null)
		{
			outResponseArc = new EObjectWithInverseResolvingEList<ResponseArc>(ResponseArc.class, this,
				WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC, WbPackage.RESPONSE_ARC__FROM_NODE);
		}
		return outResponseArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ResponseArc> getInResponseArc()
	{
		if (inResponseArc == null)
		{
			inResponseArc = new EObjectWithInverseResolvingEList<ResponseArc>(ResponseArc.class, this,
				WbPackage.GRAPH_NODE__IN_RESPONSE_ARC, WbPackage.RESPONSE_ARC__TO_NODE);
		}
		return inResponseArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutArc()).basicAdd(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__IN_ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getInArc()).basicAdd(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__SUBMODEL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSubmodel((Submodel) otherEnd, msgs);
		case WbPackage.GRAPH_NODE__INTERRUPT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getInterrupt()).basicAdd(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutResponseArc()).basicAdd(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getInResponseArc()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			return ((InternalEList<?>) getOutArc()).basicRemove(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__IN_ARC:
			return ((InternalEList<?>) getInArc()).basicRemove(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__SUBMODEL:
			return basicSetSubmodel(null, msgs);
		case WbPackage.GRAPH_NODE__INTERRUPT:
			return ((InternalEList<?>) getInterrupt()).basicRemove(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			return ((InternalEList<?>) getOutResponseArc()).basicRemove(otherEnd, msgs);
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			return ((InternalEList<?>) getInResponseArc()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.GRAPH_NODE__SUBMODEL:
			return eInternalContainer().eInverseRemove(this, WbPackage.SUBMODEL__NODE, Submodel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			return getOutArc();
		case WbPackage.GRAPH_NODE__IN_ARC:
			return getInArc();
		case WbPackage.GRAPH_NODE__SUBMODEL:
			return getSubmodel();
		case WbPackage.GRAPH_NODE__INTERRUPT:
			return getInterrupt();
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			return getOutResponseArc();
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			return getInResponseArc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			getOutArc().clear();
			getOutArc().addAll((Collection<? extends Arc>) newValue);
			return;
		case WbPackage.GRAPH_NODE__IN_ARC:
			getInArc().clear();
			getInArc().addAll((Collection<? extends Arc>) newValue);
			return;
		case WbPackage.GRAPH_NODE__SUBMODEL:
			setSubmodel((Submodel) newValue);
			return;
		case WbPackage.GRAPH_NODE__INTERRUPT:
			getInterrupt().clear();
			getInterrupt().addAll((Collection<? extends Interrupt>) newValue);
			return;
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			getOutResponseArc().clear();
			getOutResponseArc().addAll((Collection<? extends ResponseArc>) newValue);
			return;
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			getInResponseArc().clear();
			getInResponseArc().addAll((Collection<? extends ResponseArc>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			getOutArc().clear();
			return;
		case WbPackage.GRAPH_NODE__IN_ARC:
			getInArc().clear();
			return;
		case WbPackage.GRAPH_NODE__SUBMODEL:
			setSubmodel((Submodel) null);
			return;
		case WbPackage.GRAPH_NODE__INTERRUPT:
			getInterrupt().clear();
			return;
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			getOutResponseArc().clear();
			return;
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			getInResponseArc().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.GRAPH_NODE__OUT_ARC:
			return outArc != null && !outArc.isEmpty();
		case WbPackage.GRAPH_NODE__IN_ARC:
			return inArc != null && !inArc.isEmpty();
		case WbPackage.GRAPH_NODE__SUBMODEL:
			return getSubmodel() != null;
		case WbPackage.GRAPH_NODE__INTERRUPT:
			return interrupt != null && !interrupt.isEmpty();
		case WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC:
			return outResponseArc != null && !outResponseArc.isEmpty();
		case WbPackage.GRAPH_NODE__IN_RESPONSE_ARC:
			return inResponseArc != null && !inResponseArc.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // GraphNodeImpl
