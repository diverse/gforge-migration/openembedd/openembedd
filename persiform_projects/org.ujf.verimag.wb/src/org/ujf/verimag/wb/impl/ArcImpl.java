/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Arc</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSelectCondition <em>Select Condition</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSelectProbability <em>Select Probability</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSelectPhase <em>Select Phase</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSelectParent <em>Select Parent</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getDestNodeIndex <em>Dest Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getChangePort <em>Change Port</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getChangePhase <em>Change Phase</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSelectCategory <em>Select Category</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getFromNode <em>From Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ArcImpl#getToNode <em>To Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ArcImpl extends NamedElementImpl implements Arc
{
	/**
	 * The default value of the '{@link #getSelectCondition() <em>Select Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String	SELECT_CONDITION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getSelectCondition() <em>Select Condition</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCondition()
	 * @generated
	 * @ordered
	 */
	protected String				selectCondition				= SELECT_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSelectProbability() <em>Select Probability</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectProbability()
	 * @generated
	 * @ordered
	 */
	protected static final String	SELECT_PROBABILITY_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getSelectProbability() <em>Select Probability</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectProbability()
	 * @generated
	 * @ordered
	 */
	protected String				selectProbability			= SELECT_PROBABILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getSelectPhase() <em>Select Phase</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSelectPhase()
	 * @generated
	 * @ordered
	 */
	protected static final String	SELECT_PHASE_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getSelectPhase() <em>Select Phase</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSelectPhase()
	 * @generated
	 * @ordered
	 */
	protected String				selectPhase					= SELECT_PHASE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSelectParent() <em>Select Parent</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSelectParent()
	 * @generated
	 * @ordered
	 */
	protected static final String	SELECT_PARENT_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getSelectParent() <em>Select Parent</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getSelectParent()
	 * @generated
	 * @ordered
	 */
	protected String				selectParent				= SELECT_PARENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestNodeIndex() <em>Dest Node Index</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getDestNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected static final String	DEST_NODE_INDEX_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getDestNodeIndex() <em>Dest Node Index</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getDestNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected String				destNodeIndex				= DEST_NODE_INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getChangePort() <em>Change Port</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getChangePort()
	 * @generated
	 * @ordered
	 */
	protected static final String	CHANGE_PORT_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getChangePort() <em>Change Port</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getChangePort()
	 * @generated
	 * @ordered
	 */
	protected String				changePort					= CHANGE_PORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getChangePhase() <em>Change Phase</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getChangePhase()
	 * @generated
	 * @ordered
	 */
	protected static final String	CHANGE_PHASE_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getChangePhase() <em>Change Phase</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getChangePhase()
	 * @generated
	 * @ordered
	 */
	protected String				changePhase					= CHANGE_PHASE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSelectCategory() <em>Select Category</em>}' reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSelectCategory()
	 * @generated
	 * @ordered
	 */
	protected CategoryID			selectCategory;

	/**
	 * The cached value of the '{@link #getFromNode() <em>From Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getFromNode()
	 * @generated
	 * @ordered
	 */
	protected GraphNode				fromNode;

	/**
	 * The cached value of the '{@link #getToNode() <em>To Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getToNode()
	 * @generated
	 * @ordered
	 */
	protected GraphNode				toNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ArcImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.ARC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSelectCondition()
	{
		return selectCondition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectCondition(String newSelectCondition)
	{
		String oldSelectCondition = selectCondition;
		selectCondition = newSelectCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SELECT_CONDITION, oldSelectCondition,
				selectCondition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSelectProbability()
	{
		return selectProbability;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectProbability(String newSelectProbability)
	{
		String oldSelectProbability = selectProbability;
		selectProbability = newSelectProbability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SELECT_PROBABILITY,
				oldSelectProbability, selectProbability));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSelectPhase()
	{
		return selectPhase;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectPhase(String newSelectPhase)
	{
		String oldSelectPhase = selectPhase;
		selectPhase = newSelectPhase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SELECT_PHASE, oldSelectPhase,
				selectPhase));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getSelectParent()
	{
		return selectParent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectParent(String newSelectParent)
	{
		String oldSelectParent = selectParent;
		selectParent = newSelectParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SELECT_PARENT, oldSelectParent,
				selectParent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDestNodeIndex()
	{
		return destNodeIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDestNodeIndex(String newDestNodeIndex)
	{
		String oldDestNodeIndex = destNodeIndex;
		destNodeIndex = newDestNodeIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__DEST_NODE_INDEX, oldDestNodeIndex,
				destNodeIndex));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getChangePort()
	{
		return changePort;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setChangePort(String newChangePort)
	{
		String oldChangePort = changePort;
		changePort = newChangePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__CHANGE_PORT, oldChangePort, changePort));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getChangePhase()
	{
		return changePhase;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setChangePhase(String newChangePhase)
	{
		String oldChangePhase = changePhase;
		changePhase = newChangePhase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__CHANGE_PHASE, oldChangePhase,
				changePhase));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CategoryID getSelectCategory()
	{
		if (selectCategory != null && selectCategory.eIsProxy())
		{
			InternalEObject oldSelectCategory = (InternalEObject) selectCategory;
			selectCategory = (CategoryID) eResolveProxy(oldSelectCategory);
			if (selectCategory != oldSelectCategory)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.ARC__SELECT_CATEGORY,
						oldSelectCategory, selectCategory));
			}
		}
		return selectCategory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CategoryID basicGetSelectCategory()
	{
		return selectCategory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSelectCategory(CategoryID newSelectCategory, NotificationChain msgs)
	{
		CategoryID oldSelectCategory = selectCategory;
		selectCategory = newSelectCategory;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.ARC__SELECT_CATEGORY, oldSelectCategory, newSelectCategory);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSelectCategory(CategoryID newSelectCategory)
	{
		if (newSelectCategory != selectCategory)
		{
			NotificationChain msgs = null;
			if (selectCategory != null)
				msgs = ((InternalEObject) selectCategory).eInverseRemove(this, WbPackage.CATEGORY_ID__ARC,
					CategoryID.class, msgs);
			if (newSelectCategory != null)
				msgs = ((InternalEObject) newSelectCategory).eInverseAdd(this, WbPackage.CATEGORY_ID__ARC,
					CategoryID.class, msgs);
			msgs = basicSetSelectCategory(newSelectCategory, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SELECT_CATEGORY, newSelectCategory,
				newSelectCategory));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Submodel getSubmodel()
	{
		if (eContainerFeatureID != WbPackage.ARC__SUBMODEL)
			return null;
		return (Submodel) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSubmodel(Submodel newSubmodel, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSubmodel, WbPackage.ARC__SUBMODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSubmodel(Submodel newSubmodel)
	{
		if (newSubmodel != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.ARC__SUBMODEL && newSubmodel != null))
		{
			if (EcoreUtil.isAncestor(this, newSubmodel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubmodel != null)
				msgs = ((InternalEObject) newSubmodel).eInverseAdd(this, WbPackage.SUBMODEL__ARC, Submodel.class, msgs);
			msgs = basicSetSubmodel(newSubmodel, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__SUBMODEL, newSubmodel, newSubmodel));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode getFromNode()
	{
		if (fromNode != null && fromNode.eIsProxy())
		{
			InternalEObject oldFromNode = (InternalEObject) fromNode;
			fromNode = (GraphNode) eResolveProxy(oldFromNode);
			if (fromNode != oldFromNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.ARC__FROM_NODE, oldFromNode,
						fromNode));
			}
		}
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode basicGetFromNode()
	{
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetFromNode(GraphNode newFromNode, NotificationChain msgs)
	{
		GraphNode oldFromNode = fromNode;
		fromNode = newFromNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WbPackage.ARC__FROM_NODE,
				oldFromNode, newFromNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setFromNode(GraphNode newFromNode)
	{
		if (newFromNode != fromNode)
		{
			NotificationChain msgs = null;
			if (fromNode != null)
				msgs = ((InternalEObject) fromNode).eInverseRemove(this, WbPackage.GRAPH_NODE__OUT_ARC,
					GraphNode.class, msgs);
			if (newFromNode != null)
				msgs = ((InternalEObject) newFromNode).eInverseAdd(this, WbPackage.GRAPH_NODE__OUT_ARC,
					GraphNode.class, msgs);
			msgs = basicSetFromNode(newFromNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__FROM_NODE, newFromNode, newFromNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode getToNode()
	{
		if (toNode != null && toNode.eIsProxy())
		{
			InternalEObject oldToNode = (InternalEObject) toNode;
			toNode = (GraphNode) eResolveProxy(oldToNode);
			if (toNode != oldToNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.ARC__TO_NODE, oldToNode, toNode));
			}
		}
		return toNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode basicGetToNode()
	{
		return toNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetToNode(GraphNode newToNode, NotificationChain msgs)
	{
		GraphNode oldToNode = toNode;
		toNode = newToNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WbPackage.ARC__TO_NODE,
				oldToNode, newToNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setToNode(GraphNode newToNode)
	{
		if (newToNode != toNode)
		{
			NotificationChain msgs = null;
			if (toNode != null)
				msgs = ((InternalEObject) toNode).eInverseRemove(this, WbPackage.GRAPH_NODE__IN_ARC, GraphNode.class,
					msgs);
			if (newToNode != null)
				msgs = ((InternalEObject) newToNode).eInverseAdd(this, WbPackage.GRAPH_NODE__IN_ARC, GraphNode.class,
					msgs);
			msgs = basicSetToNode(newToNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.ARC__TO_NODE, newToNode, newToNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CATEGORY:
			if (selectCategory != null)
				msgs = ((InternalEObject) selectCategory).eInverseRemove(this, WbPackage.CATEGORY_ID__ARC,
					CategoryID.class, msgs);
			return basicSetSelectCategory((CategoryID) otherEnd, msgs);
		case WbPackage.ARC__SUBMODEL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSubmodel((Submodel) otherEnd, msgs);
		case WbPackage.ARC__FROM_NODE:
			if (fromNode != null)
				msgs = ((InternalEObject) fromNode).eInverseRemove(this, WbPackage.GRAPH_NODE__OUT_ARC,
					GraphNode.class, msgs);
			return basicSetFromNode((GraphNode) otherEnd, msgs);
		case WbPackage.ARC__TO_NODE:
			if (toNode != null)
				msgs = ((InternalEObject) toNode).eInverseRemove(this, WbPackage.GRAPH_NODE__IN_ARC, GraphNode.class,
					msgs);
			return basicSetToNode((GraphNode) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CATEGORY:
			return basicSetSelectCategory(null, msgs);
		case WbPackage.ARC__SUBMODEL:
			return basicSetSubmodel(null, msgs);
		case WbPackage.ARC__FROM_NODE:
			return basicSetFromNode(null, msgs);
		case WbPackage.ARC__TO_NODE:
			return basicSetToNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.ARC__SUBMODEL:
			return eInternalContainer().eInverseRemove(this, WbPackage.SUBMODEL__ARC, Submodel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CONDITION:
			return getSelectCondition();
		case WbPackage.ARC__SELECT_PROBABILITY:
			return getSelectProbability();
		case WbPackage.ARC__SELECT_PHASE:
			return getSelectPhase();
		case WbPackage.ARC__SELECT_PARENT:
			return getSelectParent();
		case WbPackage.ARC__DEST_NODE_INDEX:
			return getDestNodeIndex();
		case WbPackage.ARC__CHANGE_PORT:
			return getChangePort();
		case WbPackage.ARC__CHANGE_PHASE:
			return getChangePhase();
		case WbPackage.ARC__SELECT_CATEGORY:
			if (resolve)
				return getSelectCategory();
			return basicGetSelectCategory();
		case WbPackage.ARC__SUBMODEL:
			return getSubmodel();
		case WbPackage.ARC__FROM_NODE:
			if (resolve)
				return getFromNode();
			return basicGetFromNode();
		case WbPackage.ARC__TO_NODE:
			if (resolve)
				return getToNode();
			return basicGetToNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CONDITION:
			setSelectCondition((String) newValue);
			return;
		case WbPackage.ARC__SELECT_PROBABILITY:
			setSelectProbability((String) newValue);
			return;
		case WbPackage.ARC__SELECT_PHASE:
			setSelectPhase((String) newValue);
			return;
		case WbPackage.ARC__SELECT_PARENT:
			setSelectParent((String) newValue);
			return;
		case WbPackage.ARC__DEST_NODE_INDEX:
			setDestNodeIndex((String) newValue);
			return;
		case WbPackage.ARC__CHANGE_PORT:
			setChangePort((String) newValue);
			return;
		case WbPackage.ARC__CHANGE_PHASE:
			setChangePhase((String) newValue);
			return;
		case WbPackage.ARC__SELECT_CATEGORY:
			setSelectCategory((CategoryID) newValue);
			return;
		case WbPackage.ARC__SUBMODEL:
			setSubmodel((Submodel) newValue);
			return;
		case WbPackage.ARC__FROM_NODE:
			setFromNode((GraphNode) newValue);
			return;
		case WbPackage.ARC__TO_NODE:
			setToNode((GraphNode) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CONDITION:
			setSelectCondition(SELECT_CONDITION_EDEFAULT);
			return;
		case WbPackage.ARC__SELECT_PROBABILITY:
			setSelectProbability(SELECT_PROBABILITY_EDEFAULT);
			return;
		case WbPackage.ARC__SELECT_PHASE:
			setSelectPhase(SELECT_PHASE_EDEFAULT);
			return;
		case WbPackage.ARC__SELECT_PARENT:
			setSelectParent(SELECT_PARENT_EDEFAULT);
			return;
		case WbPackage.ARC__DEST_NODE_INDEX:
			setDestNodeIndex(DEST_NODE_INDEX_EDEFAULT);
			return;
		case WbPackage.ARC__CHANGE_PORT:
			setChangePort(CHANGE_PORT_EDEFAULT);
			return;
		case WbPackage.ARC__CHANGE_PHASE:
			setChangePhase(CHANGE_PHASE_EDEFAULT);
			return;
		case WbPackage.ARC__SELECT_CATEGORY:
			setSelectCategory((CategoryID) null);
			return;
		case WbPackage.ARC__SUBMODEL:
			setSubmodel((Submodel) null);
			return;
		case WbPackage.ARC__FROM_NODE:
			setFromNode((GraphNode) null);
			return;
		case WbPackage.ARC__TO_NODE:
			setToNode((GraphNode) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.ARC__SELECT_CONDITION:
			return SELECT_CONDITION_EDEFAULT == null ? selectCondition != null : !SELECT_CONDITION_EDEFAULT
					.equals(selectCondition);
		case WbPackage.ARC__SELECT_PROBABILITY:
			return SELECT_PROBABILITY_EDEFAULT == null ? selectProbability != null : !SELECT_PROBABILITY_EDEFAULT
					.equals(selectProbability);
		case WbPackage.ARC__SELECT_PHASE:
			return SELECT_PHASE_EDEFAULT == null ? selectPhase != null : !SELECT_PHASE_EDEFAULT.equals(selectPhase);
		case WbPackage.ARC__SELECT_PARENT:
			return SELECT_PARENT_EDEFAULT == null ? selectParent != null : !SELECT_PARENT_EDEFAULT.equals(selectParent);
		case WbPackage.ARC__DEST_NODE_INDEX:
			return DEST_NODE_INDEX_EDEFAULT == null ? destNodeIndex != null : !DEST_NODE_INDEX_EDEFAULT
					.equals(destNodeIndex);
		case WbPackage.ARC__CHANGE_PORT:
			return CHANGE_PORT_EDEFAULT == null ? changePort != null : !CHANGE_PORT_EDEFAULT.equals(changePort);
		case WbPackage.ARC__CHANGE_PHASE:
			return CHANGE_PHASE_EDEFAULT == null ? changePhase != null : !CHANGE_PHASE_EDEFAULT.equals(changePhase);
		case WbPackage.ARC__SELECT_CATEGORY:
			return selectCategory != null;
		case WbPackage.ARC__SUBMODEL:
			return getSubmodel() != null;
		case WbPackage.ARC__FROM_NODE:
			return fromNode != null;
		case WbPackage.ARC__TO_NODE:
			return toNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selectCondition: ");
		result.append(selectCondition);
		result.append(", selectProbability: ");
		result.append(selectProbability);
		result.append(", selectPhase: ");
		result.append(selectPhase);
		result.append(", selectParent: ");
		result.append(selectParent);
		result.append(", destNodeIndex: ");
		result.append(destNodeIndex);
		result.append(", changePort: ");
		result.append(changePort);
		result.append(", changePhase: ");
		result.append(changePhase);
		result.append(')');
		return result.toString();
	}

} // ArcImpl
