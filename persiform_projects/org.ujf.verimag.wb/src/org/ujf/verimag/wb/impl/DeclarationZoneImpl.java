/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Declaration Zone</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl#getRawContent <em>Raw Content</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl#getCategoryID <em>Category ID</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DeclarationZoneImpl#getVariable <em>Variable</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class DeclarationZoneImpl extends GraphicalNodeImpl implements DeclarationZone
{
	/**
	 * The default value of the '{@link #getRawContent() <em>Raw Content</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawContent()
	 * @generated
	 * @ordered
	 */
	protected static final String	RAW_CONTENT_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getRawContent() <em>Raw Content</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawContent()
	 * @generated
	 * @ordered
	 */
	protected String				rawContent				= RAW_CONTENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCategoryID() <em>Category ID</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCategoryID()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryID>		categoryID;

	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable>		variable;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DeclarationZoneImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.DECLARATION_ZONE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getRawContent()
	{
		return rawContent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRawContent(String newRawContent)
	{
		String oldRawContent = rawContent;
		rawContent = newRawContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DECLARATION_ZONE__RAW_CONTENT,
				oldRawContent, rawContent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.DECLARATION_ZONE__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.DECLARATION_ZONE__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.DECLARATION_ZONE__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__DECLARATION_ZONE,
					Module.class, msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DECLARATION_ZONE__MODULE, new_module,
				new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Submodel getSubmodel()
	{
		if (eContainerFeatureID != WbPackage.DECLARATION_ZONE__SUBMODEL)
			return null;
		return (Submodel) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSubmodel(Submodel newSubmodel, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSubmodel, WbPackage.DECLARATION_ZONE__SUBMODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSubmodel(Submodel newSubmodel)
	{
		if (newSubmodel != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.DECLARATION_ZONE__SUBMODEL && newSubmodel != null))
		{
			if (EcoreUtil.isAncestor(this, newSubmodel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubmodel != null)
				msgs = ((InternalEObject) newSubmodel).eInverseAdd(this, WbPackage.SUBMODEL__DECLARATION_ZONE,
					Submodel.class, msgs);
			msgs = basicSetSubmodel(newSubmodel, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DECLARATION_ZONE__SUBMODEL, newSubmodel,
				newSubmodel));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<CategoryID> getCategoryID()
	{
		if (categoryID == null)
		{
			categoryID = new EObjectContainmentWithInverseEList<CategoryID>(CategoryID.class, this,
				WbPackage.DECLARATION_ZONE__CATEGORY_ID, WbPackage.CATEGORY_ID__DECLARATION_ZONE);
		}
		return categoryID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Variable> getVariable()
	{
		if (variable == null)
		{
			variable = new EObjectContainmentWithInverseEList<Variable>(Variable.class, this,
				WbPackage.DECLARATION_ZONE__VARIABLE, WbPackage.VARIABLE__DECLARATION_ZONE);
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSubmodel((Submodel) otherEnd, msgs);
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCategoryID()).basicAdd(otherEnd, msgs);
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getVariable()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			return basicSetSubmodel(null, msgs);
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			return ((InternalEList<?>) getCategoryID()).basicRemove(otherEnd, msgs);
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			return ((InternalEList<?>) getVariable()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.DECLARATION_ZONE__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__DECLARATION_ZONE, Module.class, msgs);
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			return eInternalContainer()
					.eInverseRemove(this, WbPackage.SUBMODEL__DECLARATION_ZONE, Submodel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__RAW_CONTENT:
			return getRawContent();
		case WbPackage.DECLARATION_ZONE__MODULE:
			return get_module();
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			return getSubmodel();
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			return getCategoryID();
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			return getVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__RAW_CONTENT:
			setRawContent((String) newValue);
			return;
		case WbPackage.DECLARATION_ZONE__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			setSubmodel((Submodel) newValue);
			return;
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			getCategoryID().clear();
			getCategoryID().addAll((Collection<? extends CategoryID>) newValue);
			return;
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			getVariable().clear();
			getVariable().addAll((Collection<? extends Variable>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__RAW_CONTENT:
			setRawContent(RAW_CONTENT_EDEFAULT);
			return;
		case WbPackage.DECLARATION_ZONE__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			setSubmodel((Submodel) null);
			return;
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			getCategoryID().clear();
			return;
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			getVariable().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DECLARATION_ZONE__RAW_CONTENT:
			return RAW_CONTENT_EDEFAULT == null ? rawContent != null : !RAW_CONTENT_EDEFAULT.equals(rawContent);
		case WbPackage.DECLARATION_ZONE__MODULE:
			return get_module() != null;
		case WbPackage.DECLARATION_ZONE__SUBMODEL:
			return getSubmodel() != null;
		case WbPackage.DECLARATION_ZONE__CATEGORY_ID:
			return categoryID != null && !categoryID.isEmpty();
		case WbPackage.DECLARATION_ZONE__VARIABLE:
			return variable != null && !variable.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (rawContent: ");
		result.append(rawContent);
		result.append(')');
		return result.toString();
	}

} // DeclarationZoneImpl
