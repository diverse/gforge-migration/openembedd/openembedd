/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.NodeWithQueue;
import org.ujf.verimag.wb.PriorityRuleKind;
import org.ujf.verimag.wb.TimeRuleKind;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Node With Queue</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.NodeWithQueueImpl#getPriority <em>Priority</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.NodeWithQueueImpl#getPriorityRule <em>Priority Rule</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.NodeWithQueueImpl#getTimeRule <em>Time Rule</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class NodeWithQueueImpl extends DirectNodeImpl implements NodeWithQueue
{
	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final String			PRIORITY_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected String						priority				= PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPriorityRule() <em>Priority Rule</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getPriorityRule()
	 * @generated
	 * @ordered
	 */
	protected static final PriorityRuleKind	PRIORITY_RULE_EDEFAULT	= PriorityRuleKind.NOPRIORITY;

	/**
	 * The cached value of the '{@link #getPriorityRule() <em>Priority Rule</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getPriorityRule()
	 * @generated
	 * @ordered
	 */
	protected PriorityRuleKind				priorityRule			= PRIORITY_RULE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeRule() <em>Time Rule</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeRule()
	 * @generated
	 * @ordered
	 */
	protected static final TimeRuleKind		TIME_RULE_EDEFAULT		= TimeRuleKind.FCFS;

	/**
	 * The cached value of the '{@link #getTimeRule() <em>Time Rule</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeRule()
	 * @generated
	 * @ordered
	 */
	protected TimeRuleKind					timeRule				= TIME_RULE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected NodeWithQueueImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.NODE_WITH_QUEUE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getPriority()
	{
		return priority;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPriority(String newPriority)
	{
		String oldPriority = priority;
		priority = newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_WITH_QUEUE__PRIORITY, oldPriority,
				priority));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PriorityRuleKind getPriorityRule()
	{
		return priorityRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPriorityRule(PriorityRuleKind newPriorityRule)
	{
		PriorityRuleKind oldPriorityRule = priorityRule;
		priorityRule = newPriorityRule == null ? PRIORITY_RULE_EDEFAULT : newPriorityRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_WITH_QUEUE__PRIORITY_RULE,
				oldPriorityRule, priorityRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeRuleKind getTimeRule()
	{
		return timeRule;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTimeRule(TimeRuleKind newTimeRule)
	{
		TimeRuleKind oldTimeRule = timeRule;
		timeRule = newTimeRule == null ? TIME_RULE_EDEFAULT : newTimeRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.NODE_WITH_QUEUE__TIME_RULE, oldTimeRule,
				timeRule));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.NODE_WITH_QUEUE__PRIORITY:
			return getPriority();
		case WbPackage.NODE_WITH_QUEUE__PRIORITY_RULE:
			return getPriorityRule();
		case WbPackage.NODE_WITH_QUEUE__TIME_RULE:
			return getTimeRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.NODE_WITH_QUEUE__PRIORITY:
			setPriority((String) newValue);
			return;
		case WbPackage.NODE_WITH_QUEUE__PRIORITY_RULE:
			setPriorityRule((PriorityRuleKind) newValue);
			return;
		case WbPackage.NODE_WITH_QUEUE__TIME_RULE:
			setTimeRule((TimeRuleKind) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.NODE_WITH_QUEUE__PRIORITY:
			setPriority(PRIORITY_EDEFAULT);
			return;
		case WbPackage.NODE_WITH_QUEUE__PRIORITY_RULE:
			setPriorityRule(PRIORITY_RULE_EDEFAULT);
			return;
		case WbPackage.NODE_WITH_QUEUE__TIME_RULE:
			setTimeRule(TIME_RULE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.NODE_WITH_QUEUE__PRIORITY:
			return PRIORITY_EDEFAULT == null ? priority != null : !PRIORITY_EDEFAULT.equals(priority);
		case WbPackage.NODE_WITH_QUEUE__PRIORITY_RULE:
			return priorityRule != PRIORITY_RULE_EDEFAULT;
		case WbPackage.NODE_WITH_QUEUE__TIME_RULE:
			return timeRule != TIME_RULE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (priority: ");
		result.append(priority);
		result.append(", priorityRule: ");
		result.append(priorityRule);
		result.append(", timeRule: ");
		result.append(timeRule);
		result.append(')');
		return result.toString();
	}

} // NodeWithQueueImpl
