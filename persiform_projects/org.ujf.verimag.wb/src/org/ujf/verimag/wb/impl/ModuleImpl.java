/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.TimeUnit;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Module</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getTimeUnit <em>Time Unit</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getVersion <em>Version</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getAllocate <em>Allocate</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getService <em>Service</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getResourcePool <em>Resource Pool</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ModuleImpl#getBlock <em>Block</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ModuleImpl extends NamedElementImpl implements Module
{
	/**
	 * The default value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnit	TIME_UNIT_EDEFAULT	= TimeUnit.PICOSECOND;

	/**
	 * The cached value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected TimeUnit				timeUnit			= TIME_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String	VERSION_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String				version				= VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDeclarationZone() <em>Declaration Zone</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDeclarationZone()
	 * @generated
	 * @ordered
	 */
	protected DeclarationZone		declarationZone;

	/**
	 * The cached value of the '{@link #getSubmodel() <em>Submodel</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSubmodel()
	 * @generated
	 * @ordered
	 */
	protected EList<Submodel>		submodel;

	/**
	 * The cached value of the '{@link #getAllocate() <em>Allocate</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAllocate()
	 * @generated
	 * @ordered
	 */
	protected EList<Allocate>		allocate;

	/**
	 * The cached value of the '{@link #getService() <em>Service</em>}' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getService()
	 * @generated
	 * @ordered
	 */
	protected EList<Service>		service;

	/**
	 * The cached value of the '{@link #getResourcePool() <em>Resource Pool</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getResourcePool()
	 * @generated
	 * @ordered
	 */
	protected EList<ResourcePool>	resourcePool;

	/**
	 * The cached value of the '{@link #getBlock() <em>Block</em>}' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected EList<Block>			block;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModuleImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.MODULE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeUnit getTimeUnit()
	{
		return timeUnit;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTimeUnit(TimeUnit newTimeUnit)
	{
		TimeUnit oldTimeUnit = timeUnit;
		timeUnit = newTimeUnit == null ? TIME_UNIT_EDEFAULT : newTimeUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.MODULE__TIME_UNIT, oldTimeUnit, timeUnit));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setVersion(String newVersion)
	{
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.MODULE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DeclarationZone getDeclarationZone()
	{
		return declarationZone;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetDeclarationZone(DeclarationZone newDeclarationZone, NotificationChain msgs)
	{
		DeclarationZone oldDeclarationZone = declarationZone;
		declarationZone = newDeclarationZone;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.MODULE__DECLARATION_ZONE, oldDeclarationZone, newDeclarationZone);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeclarationZone(DeclarationZone newDeclarationZone)
	{
		if (newDeclarationZone != declarationZone)
		{
			NotificationChain msgs = null;
			if (declarationZone != null)
				msgs = ((InternalEObject) declarationZone).eInverseRemove(this, WbPackage.DECLARATION_ZONE__MODULE,
					DeclarationZone.class, msgs);
			if (newDeclarationZone != null)
				msgs = ((InternalEObject) newDeclarationZone).eInverseAdd(this, WbPackage.DECLARATION_ZONE__MODULE,
					DeclarationZone.class, msgs);
			msgs = basicSetDeclarationZone(newDeclarationZone, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.MODULE__DECLARATION_ZONE,
				newDeclarationZone, newDeclarationZone));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Submodel> getSubmodel()
	{
		if (submodel == null)
		{
			submodel = new EObjectContainmentWithInverseEList<Submodel>(Submodel.class, this,
				WbPackage.MODULE__SUBMODEL, WbPackage.SUBMODEL__MODULE);
		}
		return submodel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Allocate> getAllocate()
	{
		if (allocate == null)
		{
			allocate = new EObjectContainmentWithInverseEList<Allocate>(Allocate.class, this,
				WbPackage.MODULE__ALLOCATE, WbPackage.ALLOCATE__MODULE);
		}
		return allocate;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Service> getService()
	{
		if (service == null)
		{
			service = new EObjectContainmentWithInverseEList<Service>(Service.class, this, WbPackage.MODULE__SERVICE,
				WbPackage.SERVICE__MODULE);
		}
		return service;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ResourcePool> getResourcePool()
	{
		if (resourcePool == null)
		{
			resourcePool = new EObjectContainmentWithInverseEList<ResourcePool>(ResourcePool.class, this,
				WbPackage.MODULE__RESOURCE_POOL, WbPackage.RESOURCE_POOL__MODULE);
		}
		return resourcePool;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Block> getBlock()
	{
		if (block == null)
		{
			block = new EObjectContainmentWithInverseEList<Block>(Block.class, this, WbPackage.MODULE__BLOCK,
				WbPackage.BLOCK__MODULE);
		}
		return block;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__DECLARATION_ZONE:
			if (declarationZone != null)
				msgs = ((InternalEObject) declarationZone).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.MODULE__DECLARATION_ZONE, null, msgs);
			return basicSetDeclarationZone((DeclarationZone) otherEnd, msgs);
		case WbPackage.MODULE__SUBMODEL:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSubmodel()).basicAdd(otherEnd, msgs);
		case WbPackage.MODULE__ALLOCATE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAllocate()).basicAdd(otherEnd, msgs);
		case WbPackage.MODULE__SERVICE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getService()).basicAdd(otherEnd, msgs);
		case WbPackage.MODULE__RESOURCE_POOL:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getResourcePool()).basicAdd(otherEnd, msgs);
		case WbPackage.MODULE__BLOCK:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getBlock()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__DECLARATION_ZONE:
			return basicSetDeclarationZone(null, msgs);
		case WbPackage.MODULE__SUBMODEL:
			return ((InternalEList<?>) getSubmodel()).basicRemove(otherEnd, msgs);
		case WbPackage.MODULE__ALLOCATE:
			return ((InternalEList<?>) getAllocate()).basicRemove(otherEnd, msgs);
		case WbPackage.MODULE__SERVICE:
			return ((InternalEList<?>) getService()).basicRemove(otherEnd, msgs);
		case WbPackage.MODULE__RESOURCE_POOL:
			return ((InternalEList<?>) getResourcePool()).basicRemove(otherEnd, msgs);
		case WbPackage.MODULE__BLOCK:
			return ((InternalEList<?>) getBlock()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__TIME_UNIT:
			return getTimeUnit();
		case WbPackage.MODULE__VERSION:
			return getVersion();
		case WbPackage.MODULE__DECLARATION_ZONE:
			return getDeclarationZone();
		case WbPackage.MODULE__SUBMODEL:
			return getSubmodel();
		case WbPackage.MODULE__ALLOCATE:
			return getAllocate();
		case WbPackage.MODULE__SERVICE:
			return getService();
		case WbPackage.MODULE__RESOURCE_POOL:
			return getResourcePool();
		case WbPackage.MODULE__BLOCK:
			return getBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__TIME_UNIT:
			setTimeUnit((TimeUnit) newValue);
			return;
		case WbPackage.MODULE__VERSION:
			setVersion((String) newValue);
			return;
		case WbPackage.MODULE__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) newValue);
			return;
		case WbPackage.MODULE__SUBMODEL:
			getSubmodel().clear();
			getSubmodel().addAll((Collection<? extends Submodel>) newValue);
			return;
		case WbPackage.MODULE__ALLOCATE:
			getAllocate().clear();
			getAllocate().addAll((Collection<? extends Allocate>) newValue);
			return;
		case WbPackage.MODULE__SERVICE:
			getService().clear();
			getService().addAll((Collection<? extends Service>) newValue);
			return;
		case WbPackage.MODULE__RESOURCE_POOL:
			getResourcePool().clear();
			getResourcePool().addAll((Collection<? extends ResourcePool>) newValue);
			return;
		case WbPackage.MODULE__BLOCK:
			getBlock().clear();
			getBlock().addAll((Collection<? extends Block>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__TIME_UNIT:
			setTimeUnit(TIME_UNIT_EDEFAULT);
			return;
		case WbPackage.MODULE__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case WbPackage.MODULE__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) null);
			return;
		case WbPackage.MODULE__SUBMODEL:
			getSubmodel().clear();
			return;
		case WbPackage.MODULE__ALLOCATE:
			getAllocate().clear();
			return;
		case WbPackage.MODULE__SERVICE:
			getService().clear();
			return;
		case WbPackage.MODULE__RESOURCE_POOL:
			getResourcePool().clear();
			return;
		case WbPackage.MODULE__BLOCK:
			getBlock().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.MODULE__TIME_UNIT:
			return timeUnit != TIME_UNIT_EDEFAULT;
		case WbPackage.MODULE__VERSION:
			return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		case WbPackage.MODULE__DECLARATION_ZONE:
			return declarationZone != null;
		case WbPackage.MODULE__SUBMODEL:
			return submodel != null && !submodel.isEmpty();
		case WbPackage.MODULE__ALLOCATE:
			return allocate != null && !allocate.isEmpty();
		case WbPackage.MODULE__SERVICE:
			return service != null && !service.isEmpty();
		case WbPackage.MODULE__RESOURCE_POOL:
			return resourcePool != null && !resourcePool.isEmpty();
		case WbPackage.MODULE__BLOCK:
			return block != null && !block.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (derivedFeatureID)
			{
			case WbPackage.MODULE__TIME_UNIT:
				return WbPackage.TIMED_ELEMENT__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (baseFeatureID)
			{
			case WbPackage.TIMED_ELEMENT__TIME_UNIT:
				return WbPackage.MODULE__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeUnit: ");
		result.append(timeUnit);
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} // ModuleImpl
