/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.DuplicationNode;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Duplication Node</b></em>'. <!-- end-user-doc
 * -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.DuplicationNodeImpl#getInheritsCalls <em>Inherits Calls</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DuplicationNodeImpl#getInheritsUnshared <em>Inherits Unshared</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class DuplicationNodeImpl extends DirectNodeImpl implements DuplicationNode
{
	/**
	 * The default value of the '{@link #getInheritsCalls() <em>Inherits Calls</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getInheritsCalls()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	INHERITS_CALLS_EDEFAULT		= Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getInheritsCalls() <em>Inherits Calls</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getInheritsCalls()
	 * @generated
	 * @ordered
	 */
	protected Boolean				inheritsCalls				= INHERITS_CALLS_EDEFAULT;

	/**
	 * The default value of the '{@link #getInheritsUnshared() <em>Inherits Unshared</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInheritsUnshared()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	INHERITS_UNSHARED_EDEFAULT	= Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getInheritsUnshared() <em>Inherits Unshared</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInheritsUnshared()
	 * @generated
	 * @ordered
	 */
	protected Boolean				inheritsUnshared			= INHERITS_UNSHARED_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DuplicationNodeImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.DUPLICATION_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getInheritsCalls()
	{
		return inheritsCalls;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setInheritsCalls(Boolean newInheritsCalls)
	{
		Boolean oldInheritsCalls = inheritsCalls;
		inheritsCalls = newInheritsCalls;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DUPLICATION_NODE__INHERITS_CALLS,
				oldInheritsCalls, inheritsCalls));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getInheritsUnshared()
	{
		return inheritsUnshared;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setInheritsUnshared(Boolean newInheritsUnshared)
	{
		Boolean oldInheritsUnshared = inheritsUnshared;
		inheritsUnshared = newInheritsUnshared;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DUPLICATION_NODE__INHERITS_UNSHARED,
				oldInheritsUnshared, inheritsUnshared));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.DUPLICATION_NODE__INHERITS_CALLS:
			return getInheritsCalls();
		case WbPackage.DUPLICATION_NODE__INHERITS_UNSHARED:
			return getInheritsUnshared();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.DUPLICATION_NODE__INHERITS_CALLS:
			setInheritsCalls((Boolean) newValue);
			return;
		case WbPackage.DUPLICATION_NODE__INHERITS_UNSHARED:
			setInheritsUnshared((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DUPLICATION_NODE__INHERITS_CALLS:
			setInheritsCalls(INHERITS_CALLS_EDEFAULT);
			return;
		case WbPackage.DUPLICATION_NODE__INHERITS_UNSHARED:
			setInheritsUnshared(INHERITS_UNSHARED_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DUPLICATION_NODE__INHERITS_CALLS:
			return INHERITS_CALLS_EDEFAULT == null ? inheritsCalls != null : !INHERITS_CALLS_EDEFAULT
					.equals(inheritsCalls);
		case WbPackage.DUPLICATION_NODE__INHERITS_UNSHARED:
			return INHERITS_UNSHARED_EDEFAULT == null ? inheritsUnshared != null : !INHERITS_UNSHARED_EDEFAULT
					.equals(inheritsUnshared);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inheritsCalls: ");
		result.append(inheritsCalls);
		result.append(", inheritsUnshared: ");
		result.append(inheritsUnshared);
		result.append(')');
		return result.toString();
	}

} // DuplicationNodeImpl
