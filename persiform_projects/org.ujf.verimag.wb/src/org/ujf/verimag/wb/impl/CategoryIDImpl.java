/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Category ID</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.CategoryIDImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.CategoryIDImpl#getArc <em>Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.CategoryIDImpl#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.CategoryIDImpl#getInterrupt <em>Interrupt</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class CategoryIDImpl extends NamedElementImpl implements CategoryID
{
	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final Integer	DIMENSION_EDEFAULT	= new Integer(1);

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected Integer				dimension			= DIMENSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArc() <em>Arc</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getArc()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc>			arc;

	/**
	 * The cached value of the '{@link #getInterrupt() <em>Interrupt</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInterrupt()
	 * @generated
	 * @ordered
	 */
	protected EList<Interrupt>		interrupt;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected CategoryIDImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.CATEGORY_ID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(Integer newDimension)
	{
		Integer oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.CATEGORY_ID__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Arc> getArc()
	{
		if (arc == null)
		{
			arc = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, WbPackage.CATEGORY_ID__ARC,
				WbPackage.ARC__SELECT_CATEGORY);
		}
		return arc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DeclarationZone getDeclarationZone()
	{
		if (eContainerFeatureID != WbPackage.CATEGORY_ID__DECLARATION_ZONE)
			return null;
		return (DeclarationZone) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetDeclarationZone(DeclarationZone newDeclarationZone, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newDeclarationZone, WbPackage.CATEGORY_ID__DECLARATION_ZONE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeclarationZone(DeclarationZone newDeclarationZone)
	{
		if (newDeclarationZone != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.CATEGORY_ID__DECLARATION_ZONE && newDeclarationZone != null))
		{
			if (EcoreUtil.isAncestor(this, newDeclarationZone))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDeclarationZone != null)
				msgs = ((InternalEObject) newDeclarationZone).eInverseAdd(this,
					WbPackage.DECLARATION_ZONE__CATEGORY_ID, DeclarationZone.class, msgs);
			msgs = basicSetDeclarationZone(newDeclarationZone, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.CATEGORY_ID__DECLARATION_ZONE,
				newDeclarationZone, newDeclarationZone));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Interrupt> getInterrupt()
	{
		if (interrupt == null)
		{
			interrupt = new EObjectWithInverseResolvingEList<Interrupt>(Interrupt.class, this,
				WbPackage.CATEGORY_ID__INTERRUPT, WbPackage.INTERRUPT__SELECT_CATEGORY);
		}
		return interrupt;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getArc()).basicAdd(otherEnd, msgs);
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetDeclarationZone((DeclarationZone) otherEnd, msgs);
		case WbPackage.CATEGORY_ID__INTERRUPT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getInterrupt()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__ARC:
			return ((InternalEList<?>) getArc()).basicRemove(otherEnd, msgs);
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			return basicSetDeclarationZone(null, msgs);
		case WbPackage.CATEGORY_ID__INTERRUPT:
			return ((InternalEList<?>) getInterrupt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			return eInternalContainer().eInverseRemove(this, WbPackage.DECLARATION_ZONE__CATEGORY_ID,
				DeclarationZone.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__DIMENSION:
			return getDimension();
		case WbPackage.CATEGORY_ID__ARC:
			return getArc();
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			return getDeclarationZone();
		case WbPackage.CATEGORY_ID__INTERRUPT:
			return getInterrupt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__DIMENSION:
			setDimension((Integer) newValue);
			return;
		case WbPackage.CATEGORY_ID__ARC:
			getArc().clear();
			getArc().addAll((Collection<? extends Arc>) newValue);
			return;
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) newValue);
			return;
		case WbPackage.CATEGORY_ID__INTERRUPT:
			getInterrupt().clear();
			getInterrupt().addAll((Collection<? extends Interrupt>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.CATEGORY_ID__ARC:
			getArc().clear();
			return;
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) null);
			return;
		case WbPackage.CATEGORY_ID__INTERRUPT:
			getInterrupt().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.CATEGORY_ID__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.CATEGORY_ID__ARC:
			return arc != null && !arc.isEmpty();
		case WbPackage.CATEGORY_ID__DECLARATION_ZONE:
			return getDeclarationZone() != null;
		case WbPackage.CATEGORY_ID__INTERRUPT:
			return interrupt != null && !interrupt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(')');
		return result.toString();
	}

} // CategoryIDImpl
