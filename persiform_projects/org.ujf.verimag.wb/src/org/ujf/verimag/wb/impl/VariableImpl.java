/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.wb.DataTypeKind;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.StorageKind;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Variable</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.VariableImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.VariableImpl#getStorageKind <em>Storage Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.VariableImpl#getDatatype <em>Datatype</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.VariableImpl#getValue <em>Value</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.VariableImpl#getDeclarationZone <em>Declaration Zone</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class VariableImpl extends NamedElementImpl implements Variable
{
	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final Integer		DIMENSION_EDEFAULT		= new Integer(1);

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected Integer					dimension				= DIMENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStorageKind() <em>Storage Kind</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getStorageKind()
	 * @generated
	 * @ordered
	 */
	protected static final StorageKind	STORAGE_KIND_EDEFAULT	= StorageKind.UNSHARED;

	/**
	 * The cached value of the '{@link #getStorageKind() <em>Storage Kind</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getStorageKind()
	 * @generated
	 * @ordered
	 */
	protected StorageKind				storageKind				= STORAGE_KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getDatatype() <em>Datatype</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDatatype()
	 * @generated
	 * @ordered
	 */
	protected static final DataTypeKind	DATATYPE_EDEFAULT		= DataTypeKind.INT;

	/**
	 * The cached value of the '{@link #getDatatype() <em>Datatype</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDatatype()
	 * @generated
	 * @ordered
	 */
	protected DataTypeKind				datatype				= DATATYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String		VALUE_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String					value					= VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected VariableImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.VARIABLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(Integer newDimension)
	{
		Integer oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.VARIABLE__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StorageKind getStorageKind()
	{
		return storageKind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStorageKind(StorageKind newStorageKind)
	{
		StorageKind oldStorageKind = storageKind;
		storageKind = newStorageKind == null ? STORAGE_KIND_EDEFAULT : newStorageKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.VARIABLE__STORAGE_KIND, oldStorageKind,
				storageKind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DataTypeKind getDatatype()
	{
		return datatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDatatype(DataTypeKind newDatatype)
	{
		DataTypeKind oldDatatype = datatype;
		datatype = newDatatype == null ? DATATYPE_EDEFAULT : newDatatype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.VARIABLE__DATATYPE, oldDatatype, datatype));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setValue(String newValue)
	{
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.VARIABLE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DeclarationZone getDeclarationZone()
	{
		if (eContainerFeatureID != WbPackage.VARIABLE__DECLARATION_ZONE)
			return null;
		return (DeclarationZone) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetDeclarationZone(DeclarationZone newDeclarationZone, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newDeclarationZone, WbPackage.VARIABLE__DECLARATION_ZONE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeclarationZone(DeclarationZone newDeclarationZone)
	{
		if (newDeclarationZone != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.VARIABLE__DECLARATION_ZONE && newDeclarationZone != null))
		{
			if (EcoreUtil.isAncestor(this, newDeclarationZone))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDeclarationZone != null)
				msgs = ((InternalEObject) newDeclarationZone).eInverseAdd(this, WbPackage.DECLARATION_ZONE__VARIABLE,
					DeclarationZone.class, msgs);
			msgs = basicSetDeclarationZone(newDeclarationZone, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.VARIABLE__DECLARATION_ZONE,
				newDeclarationZone, newDeclarationZone));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetDeclarationZone((DeclarationZone) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			return basicSetDeclarationZone(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			return eInternalContainer().eInverseRemove(this, WbPackage.DECLARATION_ZONE__VARIABLE,
				DeclarationZone.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DIMENSION:
			return getDimension();
		case WbPackage.VARIABLE__STORAGE_KIND:
			return getStorageKind();
		case WbPackage.VARIABLE__DATATYPE:
			return getDatatype();
		case WbPackage.VARIABLE__VALUE:
			return getValue();
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			return getDeclarationZone();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DIMENSION:
			setDimension((Integer) newValue);
			return;
		case WbPackage.VARIABLE__STORAGE_KIND:
			setStorageKind((StorageKind) newValue);
			return;
		case WbPackage.VARIABLE__DATATYPE:
			setDatatype((DataTypeKind) newValue);
			return;
		case WbPackage.VARIABLE__VALUE:
			setValue((String) newValue);
			return;
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.VARIABLE__STORAGE_KIND:
			setStorageKind(STORAGE_KIND_EDEFAULT);
			return;
		case WbPackage.VARIABLE__DATATYPE:
			setDatatype(DATATYPE_EDEFAULT);
			return;
		case WbPackage.VARIABLE__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.VARIABLE__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.VARIABLE__STORAGE_KIND:
			return storageKind != STORAGE_KIND_EDEFAULT;
		case WbPackage.VARIABLE__DATATYPE:
			return datatype != DATATYPE_EDEFAULT;
		case WbPackage.VARIABLE__VALUE:
			return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
		case WbPackage.VARIABLE__DECLARATION_ZONE:
			return getDeclarationZone() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(", storageKind: ");
		result.append(storageKind);
		result.append(", datatype: ");
		result.append(datatype);
		result.append(", value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} // VariableImpl
