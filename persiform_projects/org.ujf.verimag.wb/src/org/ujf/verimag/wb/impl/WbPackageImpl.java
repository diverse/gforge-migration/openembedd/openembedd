/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.AllocateRef;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.BlockRef;
import org.ujf.verimag.wb.Branch;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.ConditionalLoop;
import org.ujf.verimag.wb.DataTypeKind;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Delay;
import org.ujf.verimag.wb.DirectNode;
import org.ujf.verimag.wb.DuplicationNode;
import org.ujf.verimag.wb.Enter;
import org.ujf.verimag.wb.ForLoop;
import org.ujf.verimag.wb.Fork;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.GraphicalNode;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.Join;
import org.ujf.verimag.wb.Loop;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.NamedElement;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.NodeWithQueue;
import org.ujf.verimag.wb.PriorityRuleKind;
import org.ujf.verimag.wb.RefNode;
import org.ujf.verimag.wb.Release;
import org.ujf.verimag.wb.ResourceKind;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Resume;
import org.ujf.verimag.wb.Return;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.ServiceRef;
import org.ujf.verimag.wb.Sink;
import org.ujf.verimag.wb.Source;
import org.ujf.verimag.wb.Split;
import org.ujf.verimag.wb.StatClassKind;
import org.ujf.verimag.wb.StatisticsCollector;
import org.ujf.verimag.wb.StorageKind;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.SubmodelRef;
import org.ujf.verimag.wb.Super;
import org.ujf.verimag.wb.TimeRuleKind;
import org.ujf.verimag.wb.TimeUnit;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.User;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbFactory;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class WbPackageImpl extends EPackageImpl implements WbPackage
{
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	moduleEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	declarationZoneEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	submodelEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	allocateEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	serviceEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	resourcePoolEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	graphNodeEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	arcEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	responseArcEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	categoryIDEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	variableEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	directNodeEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	refNodeEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	submodelRefEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	serviceRefEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	allocateRefEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	nodeWithQueueEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	blockEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	sourceEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	sinkEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	enterEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	returnEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	splitEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	forkEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	joinEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	userEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	delayEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	releaseEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	interruptEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	loopEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	forLoopEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	conditionalLoopEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statisticsCollectorEClass	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	nodeStatisticsEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	namedElementEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	timedElementEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	superEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	resumeEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	branchEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	duplicationNodeEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	blockRefEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	graphicalNodeEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	storageKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	dataTypeKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	priorityRuleKindEEnum		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	timeRuleKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	resourceKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	statClassKindEEnum			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	timeUnitEEnum				= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with {@link org.eclipse.emf.ecore.EPackage.Registry
	 * EPackage.Registry} by the package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory method {@link #init init()}, which also
	 * performs initialization of the package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.ujf.verimag.wb.WbPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WbPackageImpl()
	{
		super(eNS_URI, WbFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * Simple dependencies are satisfied by calling this method on all dependent packages before doing anything else.
	 * This method drives initialization for interdependent packages directly, in parallel with this package, itself.
	 * <p>
	 * Of this package and its interdependencies, all packages which have not yet been registered by their URI values
	 * are first created and registered. The packages are then initialized in two steps: meta-model objects for all of
	 * the packages are created before any are initialized, since one package's meta-model objects may refer to those of
	 * another.
	 * <p>
	 * Invocation of this method will not affect any packages that have already been initialized. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WbPackage init()
	{
		if (isInited)
			return (WbPackage) EPackage.Registry.INSTANCE.getEPackage(WbPackage.eNS_URI);

		// Obtain or create and register package
		WbPackageImpl theWbPackage = (WbPackageImpl) (EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof WbPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(eNS_URI)
				: new WbPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theWbPackage.createPackageContents();

		// Initialize created meta-data
		theWbPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWbPackage.freeze();

		return theWbPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getModule()
	{
		return moduleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getModule_Version()
	{
		return (EAttribute) moduleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_DeclarationZone()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_Submodel()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_Allocate()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_Service()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_ResourcePool()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getModule_Block()
	{
		return (EReference) moduleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDeclarationZone()
	{
		return declarationZoneEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDeclarationZone_RawContent()
	{
		return (EAttribute) declarationZoneEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDeclarationZone__module()
	{
		return (EReference) declarationZoneEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDeclarationZone_Submodel()
	{
		return (EReference) declarationZoneEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDeclarationZone_CategoryID()
	{
		return (EReference) declarationZoneEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDeclarationZone_Variable()
	{
		return (EReference) declarationZoneEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSubmodel()
	{
		return submodelEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getSubmodel_FormalParameters()
	{
		return (EAttribute) submodelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getSubmodel_Dimension()
	{
		return (EAttribute) submodelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel__module()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel_DeclarationZone()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel_ResponseArc()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel_Arc()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel_Ref()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodel_Node()
	{
		return (EReference) submodelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAllocate()
	{
		return allocateEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getAllocate_Quantity()
	{
		return (EAttribute) allocateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getAllocate_GenerateCharacteristic()
	{
		return (EAttribute) allocateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getAllocate_ResourceInstanceSpec()
	{
		return (EAttribute) allocateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAllocate__module()
	{
		return (EReference) allocateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAllocate_Ref()
	{
		return (EReference) allocateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAllocate_ResourceInstance()
	{
		return (EReference) allocateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getService()
	{
		return serviceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getService_ServiceTime()
	{
		return (EAttribute) serviceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getService_ServerQuantity()
	{
		return (EAttribute) serviceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getService__module()
	{
		return (EReference) serviceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getService_Ref()
	{
		return (EReference) serviceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getResourcePool()
	{
		return resourcePoolEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getResourcePool_Dimension()
	{
		return (EAttribute) resourcePoolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getResourcePool_ResourceKind()
	{
		return (EAttribute) resourcePoolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getResourcePool_TokenQuantity()
	{
		return (EAttribute) resourcePoolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResourcePool__module()
	{
		return (EReference) resourcePoolEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResourcePool_Allocate()
	{
		return (EReference) resourcePoolEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResourcePool_Release()
	{
		return (EReference) resourcePoolEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getGraphNode()
	{
		return graphNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_OutArc()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_InArc()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_Submodel()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_Interrupt()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_OutResponseArc()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getGraphNode_InResponseArc()
	{
		return (EReference) graphNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getArc()
	{
		return arcEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_SelectCondition()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_SelectProbability()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_SelectPhase()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_SelectParent()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_DestNodeIndex()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_ChangePort()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getArc_ChangePhase()
	{
		return (EAttribute) arcEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArc_SelectCategory()
	{
		return (EReference) arcEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArc_Submodel()
	{
		return (EReference) arcEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArc_FromNode()
	{
		return (EReference) arcEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getArc_ToNode()
	{
		return (EReference) arcEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getResponseArc()
	{
		return responseArcEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getResponseArc_SourceNodeIndex()
	{
		return (EAttribute) responseArcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getResponseArc_DestinationNodeIndex()
	{
		return (EAttribute) responseArcEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResponseArc_Submodel()
	{
		return (EReference) responseArcEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResponseArc_FromNode()
	{
		return (EReference) responseArcEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getResponseArc_ToNode()
	{
		return (EReference) responseArcEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getCategoryID()
	{
		return categoryIDEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getCategoryID_Dimension()
	{
		return (EAttribute) categoryIDEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCategoryID_Arc()
	{
		return (EReference) categoryIDEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCategoryID_DeclarationZone()
	{
		return (EReference) categoryIDEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getCategoryID_Interrupt()
	{
		return (EReference) categoryIDEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getVariable()
	{
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getVariable_Dimension()
	{
		return (EAttribute) variableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getVariable_StorageKind()
	{
		return (EAttribute) variableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getVariable_Datatype()
	{
		return (EAttribute) variableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getVariable_Value()
	{
		return (EAttribute) variableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getVariable_DeclarationZone()
	{
		return (EReference) variableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDirectNode()
	{
		return directNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDirectNode_Dimension()
	{
		return (EAttribute) directNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDirectNode_Code()
	{
		return (EAttribute) directNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDirectNode_FormalParameters()
	{
		return (EAttribute) directNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDirectNode_Declarations()
	{
		return (EAttribute) directNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getDirectNode_NodeStatistics()
	{
		return (EReference) directNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getRefNode()
	{
		return refNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getRefNode_ActualParameters()
	{
		return (EAttribute) refNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSubmodelRef()
	{
		return submodelRefEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getSubmodelRef_Target()
	{
		return (EReference) submodelRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getServiceRef()
	{
		return serviceRefEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getServiceRef_Target()
	{
		return (EReference) serviceRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getAllocateRef()
	{
		return allocateRefEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getAllocateRef_Target()
	{
		return (EReference) allocateRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getNodeWithQueue()
	{
		return nodeWithQueueEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNodeWithQueue_Priority()
	{
		return (EAttribute) nodeWithQueueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNodeWithQueue_PriorityRule()
	{
		return (EAttribute) nodeWithQueueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNodeWithQueue_TimeRule()
	{
		return (EAttribute) nodeWithQueueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getBlock()
	{
		return blockEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getBlock_BlockUntil()
	{
		return (EAttribute) blockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getBlock__module()
	{
		return (EReference) blockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getBlock_Ref()
	{
		return (EReference) blockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSource()
	{
		return sourceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getSource_GenerateCharacteristic()
	{
		return (EAttribute) sourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSink()
	{
		return sinkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getEnter()
	{
		return enterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getReturn()
	{
		return returnEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSplit()
	{
		return splitEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getFork()
	{
		return forkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getJoin()
	{
		return joinEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getUser()
	{
		return userEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDelay()
	{
		return delayEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDelay_DelayTime()
	{
		return (EAttribute) delayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDelay_GenerateCharacteristic()
	{
		return (EAttribute) delayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getRelease()
	{
		return releaseEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getRelease_GenerateCharacteristic()
	{
		return (EAttribute) releaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getRelease_Quantity()
	{
		return (EAttribute) releaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getRelease_ResourceInstanceSpec()
	{
		return (EAttribute) releaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getRelease_ResourceInstance()
	{
		return (EReference) releaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getInterrupt()
	{
		return interruptEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getInterrupt_SelectCondition()
	{
		return (EAttribute) interruptEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getInterrupt_SelectQuantity()
	{
		return (EAttribute) interruptEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInterrupt_SelectNode()
	{
		return (EReference) interruptEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getInterrupt_SelectCategory()
	{
		return (EReference) interruptEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getLoop()
	{
		return loopEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLoop_LoopStart()
	{
		return (EReference) loopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLoop_LoopEnd()
	{
		return (EReference) loopEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getForLoop()
	{
		return forLoopEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getForLoop_Count()
	{
		return (EAttribute) forLoopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getConditionalLoop()
	{
		return conditionalLoopEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getConditionalLoop_IsWhile()
	{
		return (EAttribute) conditionalLoopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getConditionalLoop_Condition()
	{
		return (EAttribute) conditionalLoopEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatisticsCollector()
	{
		return statisticsCollectorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStatisticsCollector_BreakdownList()
	{
		return (EAttribute) statisticsCollectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStatisticsCollector_HistogramBuckets()
	{
		return (EAttribute) statisticsCollectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStatisticsCollector_UpdateChange()
	{
		return (EAttribute) statisticsCollectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStatisticsCollector_UpdateDelay()
	{
		return (EAttribute) statisticsCollectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getNodeStatistics()
	{
		return nodeStatisticsEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNodeStatistics_Dimension()
	{
		return (EAttribute) nodeStatisticsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNodeStatistics_Kind()
	{
		return (EAttribute) nodeStatisticsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getNodeStatistics_DirectNode()
	{
		return (EReference) nodeStatisticsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getNamedElement()
	{
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNamedElement_Name()
	{
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getNamedElement_Description()
	{
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getTimedElement()
	{
		return timedElementEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getTimedElement_TimeUnit()
	{
		return (EAttribute) timedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getSuper()
	{
		return superEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getResume()
	{
		return resumeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getBranch()
	{
		return branchEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getDuplicationNode()
	{
		return duplicationNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDuplicationNode_InheritsCalls()
	{
		return (EAttribute) duplicationNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getDuplicationNode_InheritsUnshared()
	{
		return (EAttribute) duplicationNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getBlockRef()
	{
		return blockRefEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getBlockRef_Target()
	{
		return (EReference) blockRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getGraphicalNode()
	{
		return graphicalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getGraphicalNode_X()
	{
		return (EAttribute) graphicalNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getGraphicalNode_Y()
	{
		return (EAttribute) graphicalNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getStorageKind()
	{
		return storageKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getDataTypeKind()
	{
		return dataTypeKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getPriorityRuleKind()
	{
		return priorityRuleKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getTimeRuleKind()
	{
		return timeRuleKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getResourceKind()
	{
		return resourceKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getStatClassKind()
	{
		return statClassKindEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getTimeUnit()
	{
		return timeUnitEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbFactory getWbFactory()
	{
		return (WbFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package. This method is guarded to have no affect on any invocation but
	 * its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents()
	{
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		moduleEClass = createEClass(MODULE);
		createEAttribute(moduleEClass, MODULE__VERSION);
		createEReference(moduleEClass, MODULE__DECLARATION_ZONE);
		createEReference(moduleEClass, MODULE__SUBMODEL);
		createEReference(moduleEClass, MODULE__ALLOCATE);
		createEReference(moduleEClass, MODULE__SERVICE);
		createEReference(moduleEClass, MODULE__RESOURCE_POOL);
		createEReference(moduleEClass, MODULE__BLOCK);

		declarationZoneEClass = createEClass(DECLARATION_ZONE);
		createEAttribute(declarationZoneEClass, DECLARATION_ZONE__RAW_CONTENT);
		createEReference(declarationZoneEClass, DECLARATION_ZONE__MODULE);
		createEReference(declarationZoneEClass, DECLARATION_ZONE__SUBMODEL);
		createEReference(declarationZoneEClass, DECLARATION_ZONE__CATEGORY_ID);
		createEReference(declarationZoneEClass, DECLARATION_ZONE__VARIABLE);

		submodelEClass = createEClass(SUBMODEL);
		createEAttribute(submodelEClass, SUBMODEL__FORMAL_PARAMETERS);
		createEAttribute(submodelEClass, SUBMODEL__DIMENSION);
		createEReference(submodelEClass, SUBMODEL__MODULE);
		createEReference(submodelEClass, SUBMODEL__DECLARATION_ZONE);
		createEReference(submodelEClass, SUBMODEL__RESPONSE_ARC);
		createEReference(submodelEClass, SUBMODEL__ARC);
		createEReference(submodelEClass, SUBMODEL__REF);
		createEReference(submodelEClass, SUBMODEL__NODE);

		allocateEClass = createEClass(ALLOCATE);
		createEAttribute(allocateEClass, ALLOCATE__QUANTITY);
		createEAttribute(allocateEClass, ALLOCATE__GENERATE_CHARACTERISTIC);
		createEAttribute(allocateEClass, ALLOCATE__RESOURCE_INSTANCE_SPEC);
		createEReference(allocateEClass, ALLOCATE__MODULE);
		createEReference(allocateEClass, ALLOCATE__REF);
		createEReference(allocateEClass, ALLOCATE__RESOURCE_INSTANCE);

		serviceEClass = createEClass(SERVICE);
		createEAttribute(serviceEClass, SERVICE__SERVICE_TIME);
		createEAttribute(serviceEClass, SERVICE__SERVER_QUANTITY);
		createEReference(serviceEClass, SERVICE__MODULE);
		createEReference(serviceEClass, SERVICE__REF);

		resourcePoolEClass = createEClass(RESOURCE_POOL);
		createEAttribute(resourcePoolEClass, RESOURCE_POOL__DIMENSION);
		createEAttribute(resourcePoolEClass, RESOURCE_POOL__RESOURCE_KIND);
		createEAttribute(resourcePoolEClass, RESOURCE_POOL__TOKEN_QUANTITY);
		createEReference(resourcePoolEClass, RESOURCE_POOL__MODULE);
		createEReference(resourcePoolEClass, RESOURCE_POOL__ALLOCATE);
		createEReference(resourcePoolEClass, RESOURCE_POOL__RELEASE);

		graphNodeEClass = createEClass(GRAPH_NODE);
		createEReference(graphNodeEClass, GRAPH_NODE__OUT_ARC);
		createEReference(graphNodeEClass, GRAPH_NODE__IN_ARC);
		createEReference(graphNodeEClass, GRAPH_NODE__SUBMODEL);
		createEReference(graphNodeEClass, GRAPH_NODE__INTERRUPT);
		createEReference(graphNodeEClass, GRAPH_NODE__OUT_RESPONSE_ARC);
		createEReference(graphNodeEClass, GRAPH_NODE__IN_RESPONSE_ARC);

		arcEClass = createEClass(ARC);
		createEAttribute(arcEClass, ARC__SELECT_CONDITION);
		createEAttribute(arcEClass, ARC__SELECT_PROBABILITY);
		createEAttribute(arcEClass, ARC__SELECT_PHASE);
		createEAttribute(arcEClass, ARC__SELECT_PARENT);
		createEAttribute(arcEClass, ARC__DEST_NODE_INDEX);
		createEAttribute(arcEClass, ARC__CHANGE_PORT);
		createEAttribute(arcEClass, ARC__CHANGE_PHASE);
		createEReference(arcEClass, ARC__SELECT_CATEGORY);
		createEReference(arcEClass, ARC__SUBMODEL);
		createEReference(arcEClass, ARC__FROM_NODE);
		createEReference(arcEClass, ARC__TO_NODE);

		responseArcEClass = createEClass(RESPONSE_ARC);
		createEAttribute(responseArcEClass, RESPONSE_ARC__SOURCE_NODE_INDEX);
		createEAttribute(responseArcEClass, RESPONSE_ARC__DESTINATION_NODE_INDEX);
		createEReference(responseArcEClass, RESPONSE_ARC__SUBMODEL);
		createEReference(responseArcEClass, RESPONSE_ARC__FROM_NODE);
		createEReference(responseArcEClass, RESPONSE_ARC__TO_NODE);

		categoryIDEClass = createEClass(CATEGORY_ID);
		createEAttribute(categoryIDEClass, CATEGORY_ID__DIMENSION);
		createEReference(categoryIDEClass, CATEGORY_ID__ARC);
		createEReference(categoryIDEClass, CATEGORY_ID__DECLARATION_ZONE);
		createEReference(categoryIDEClass, CATEGORY_ID__INTERRUPT);

		variableEClass = createEClass(VARIABLE);
		createEAttribute(variableEClass, VARIABLE__DIMENSION);
		createEAttribute(variableEClass, VARIABLE__STORAGE_KIND);
		createEAttribute(variableEClass, VARIABLE__DATATYPE);
		createEAttribute(variableEClass, VARIABLE__VALUE);
		createEReference(variableEClass, VARIABLE__DECLARATION_ZONE);

		directNodeEClass = createEClass(DIRECT_NODE);
		createEAttribute(directNodeEClass, DIRECT_NODE__DIMENSION);
		createEAttribute(directNodeEClass, DIRECT_NODE__CODE);
		createEAttribute(directNodeEClass, DIRECT_NODE__FORMAL_PARAMETERS);
		createEAttribute(directNodeEClass, DIRECT_NODE__DECLARATIONS);
		createEReference(directNodeEClass, DIRECT_NODE__NODE_STATISTICS);

		refNodeEClass = createEClass(REF_NODE);
		createEAttribute(refNodeEClass, REF_NODE__ACTUAL_PARAMETERS);

		submodelRefEClass = createEClass(SUBMODEL_REF);
		createEReference(submodelRefEClass, SUBMODEL_REF__TARGET);

		serviceRefEClass = createEClass(SERVICE_REF);
		createEReference(serviceRefEClass, SERVICE_REF__TARGET);

		allocateRefEClass = createEClass(ALLOCATE_REF);
		createEReference(allocateRefEClass, ALLOCATE_REF__TARGET);

		nodeWithQueueEClass = createEClass(NODE_WITH_QUEUE);
		createEAttribute(nodeWithQueueEClass, NODE_WITH_QUEUE__PRIORITY);
		createEAttribute(nodeWithQueueEClass, NODE_WITH_QUEUE__PRIORITY_RULE);
		createEAttribute(nodeWithQueueEClass, NODE_WITH_QUEUE__TIME_RULE);

		blockEClass = createEClass(BLOCK);
		createEAttribute(blockEClass, BLOCK__BLOCK_UNTIL);
		createEReference(blockEClass, BLOCK__MODULE);
		createEReference(blockEClass, BLOCK__REF);

		sourceEClass = createEClass(SOURCE);
		createEAttribute(sourceEClass, SOURCE__GENERATE_CHARACTERISTIC);

		sinkEClass = createEClass(SINK);

		enterEClass = createEClass(ENTER);

		returnEClass = createEClass(RETURN);

		splitEClass = createEClass(SPLIT);

		forkEClass = createEClass(FORK);

		joinEClass = createEClass(JOIN);

		userEClass = createEClass(USER);

		delayEClass = createEClass(DELAY);
		createEAttribute(delayEClass, DELAY__DELAY_TIME);
		createEAttribute(delayEClass, DELAY__GENERATE_CHARACTERISTIC);

		releaseEClass = createEClass(RELEASE);
		createEAttribute(releaseEClass, RELEASE__GENERATE_CHARACTERISTIC);
		createEAttribute(releaseEClass, RELEASE__QUANTITY);
		createEAttribute(releaseEClass, RELEASE__RESOURCE_INSTANCE_SPEC);
		createEReference(releaseEClass, RELEASE__RESOURCE_INSTANCE);

		interruptEClass = createEClass(INTERRUPT);
		createEAttribute(interruptEClass, INTERRUPT__SELECT_CONDITION);
		createEAttribute(interruptEClass, INTERRUPT__SELECT_QUANTITY);
		createEReference(interruptEClass, INTERRUPT__SELECT_NODE);
		createEReference(interruptEClass, INTERRUPT__SELECT_CATEGORY);

		loopEClass = createEClass(LOOP);
		createEReference(loopEClass, LOOP__LOOP_START);
		createEReference(loopEClass, LOOP__LOOP_END);

		forLoopEClass = createEClass(FOR_LOOP);
		createEAttribute(forLoopEClass, FOR_LOOP__COUNT);

		conditionalLoopEClass = createEClass(CONDITIONAL_LOOP);
		createEAttribute(conditionalLoopEClass, CONDITIONAL_LOOP__IS_WHILE);
		createEAttribute(conditionalLoopEClass, CONDITIONAL_LOOP__CONDITION);

		statisticsCollectorEClass = createEClass(STATISTICS_COLLECTOR);
		createEAttribute(statisticsCollectorEClass, STATISTICS_COLLECTOR__BREAKDOWN_LIST);
		createEAttribute(statisticsCollectorEClass, STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS);
		createEAttribute(statisticsCollectorEClass, STATISTICS_COLLECTOR__UPDATE_CHANGE);
		createEAttribute(statisticsCollectorEClass, STATISTICS_COLLECTOR__UPDATE_DELAY);

		nodeStatisticsEClass = createEClass(NODE_STATISTICS);
		createEAttribute(nodeStatisticsEClass, NODE_STATISTICS__DIMENSION);
		createEAttribute(nodeStatisticsEClass, NODE_STATISTICS__KIND);
		createEReference(nodeStatisticsEClass, NODE_STATISTICS__DIRECT_NODE);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__DESCRIPTION);

		timedElementEClass = createEClass(TIMED_ELEMENT);
		createEAttribute(timedElementEClass, TIMED_ELEMENT__TIME_UNIT);

		superEClass = createEClass(SUPER);

		resumeEClass = createEClass(RESUME);

		branchEClass = createEClass(BRANCH);

		duplicationNodeEClass = createEClass(DUPLICATION_NODE);
		createEAttribute(duplicationNodeEClass, DUPLICATION_NODE__INHERITS_CALLS);
		createEAttribute(duplicationNodeEClass, DUPLICATION_NODE__INHERITS_UNSHARED);

		blockRefEClass = createEClass(BLOCK_REF);
		createEReference(blockRefEClass, BLOCK_REF__TARGET);

		graphicalNodeEClass = createEClass(GRAPHICAL_NODE);
		createEAttribute(graphicalNodeEClass, GRAPHICAL_NODE__X);
		createEAttribute(graphicalNodeEClass, GRAPHICAL_NODE__Y);

		// Create enums
		storageKindEEnum = createEEnum(STORAGE_KIND);
		dataTypeKindEEnum = createEEnum(DATA_TYPE_KIND);
		priorityRuleKindEEnum = createEEnum(PRIORITY_RULE_KIND);
		timeRuleKindEEnum = createEEnum(TIME_RULE_KIND);
		resourceKindEEnum = createEEnum(RESOURCE_KIND);
		statClassKindEEnum = createEEnum(STAT_CLASS_KIND);
		timeUnitEEnum = createEEnum(TIME_UNIT);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model. This method is guarded to have no affect on any
	 * invocation but its first. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents()
	{
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		moduleEClass.getESuperTypes().add(this.getNamedElement());
		moduleEClass.getESuperTypes().add(this.getTimedElement());
		declarationZoneEClass.getESuperTypes().add(this.getGraphicalNode());
		submodelEClass.getESuperTypes().add(this.getGraphicalNode());
		allocateEClass.getESuperTypes().add(this.getNodeWithQueue());
		serviceEClass.getESuperTypes().add(this.getNodeWithQueue());
		serviceEClass.getESuperTypes().add(this.getTimedElement());
		resourcePoolEClass.getESuperTypes().add(this.getGraphicalNode());
		graphNodeEClass.getESuperTypes().add(this.getGraphicalNode());
		arcEClass.getESuperTypes().add(this.getNamedElement());
		responseArcEClass.getESuperTypes().add(this.getStatisticsCollector());
		responseArcEClass.getESuperTypes().add(this.getNamedElement());
		categoryIDEClass.getESuperTypes().add(this.getNamedElement());
		variableEClass.getESuperTypes().add(this.getNamedElement());
		directNodeEClass.getESuperTypes().add(this.getGraphNode());
		refNodeEClass.getESuperTypes().add(this.getGraphNode());
		submodelRefEClass.getESuperTypes().add(this.getRefNode());
		serviceRefEClass.getESuperTypes().add(this.getRefNode());
		allocateRefEClass.getESuperTypes().add(this.getRefNode());
		nodeWithQueueEClass.getESuperTypes().add(this.getDirectNode());
		blockEClass.getESuperTypes().add(this.getNodeWithQueue());
		sourceEClass.getESuperTypes().add(this.getDirectNode());
		sinkEClass.getESuperTypes().add(this.getDirectNode());
		enterEClass.getESuperTypes().add(this.getDirectNode());
		returnEClass.getESuperTypes().add(this.getDirectNode());
		splitEClass.getESuperTypes().add(this.getDuplicationNode());
		forkEClass.getESuperTypes().add(this.getDuplicationNode());
		joinEClass.getESuperTypes().add(this.getDirectNode());
		userEClass.getESuperTypes().add(this.getDirectNode());
		delayEClass.getESuperTypes().add(this.getDirectNode());
		delayEClass.getESuperTypes().add(this.getTimedElement());
		releaseEClass.getESuperTypes().add(this.getDirectNode());
		interruptEClass.getESuperTypes().add(this.getDirectNode());
		loopEClass.getESuperTypes().add(this.getDirectNode());
		forLoopEClass.getESuperTypes().add(this.getLoop());
		conditionalLoopEClass.getESuperTypes().add(this.getLoop());
		statisticsCollectorEClass.getESuperTypes().add(this.getNamedElement());
		nodeStatisticsEClass.getESuperTypes().add(this.getStatisticsCollector());
		superEClass.getESuperTypes().add(this.getDirectNode());
		resumeEClass.getESuperTypes().add(this.getDirectNode());
		branchEClass.getESuperTypes().add(this.getDirectNode());
		duplicationNodeEClass.getESuperTypes().add(this.getDirectNode());
		blockRefEClass.getESuperTypes().add(this.getRefNode());
		graphicalNodeEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(moduleEClass, Module.class, "Module", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModule_Version(), ecorePackage.getEString(), "version", null, 0, 1, Module.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_DeclarationZone(), this.getDeclarationZone(), this.getDeclarationZone__module(),
			"declarationZone", null, 0, 1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_Submodel(), this.getSubmodel(), this.getSubmodel__module(), "submodel", null, 1, -1,
			Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_Allocate(), this.getAllocate(), this.getAllocate__module(), "allocate", null, 0, -1,
			Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_Service(), this.getService(), this.getService__module(), "service", null, 0, -1,
			Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_ResourcePool(), this.getResourcePool(), this.getResourcePool__module(),
			"resourcePool", null, 0, -1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_Block(), this.getBlock(), this.getBlock__module(), "block", null, 0, -1, Module.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(declarationZoneEClass, DeclarationZone.class, "DeclarationZone", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeclarationZone_RawContent(), ecorePackage.getEString(), "rawContent", null, 0, 1,
			DeclarationZone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getDeclarationZone__module(), this.getModule(), this.getModule_DeclarationZone(), "_module",
			null, 0, 1, DeclarationZone.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeclarationZone_Submodel(), this.getSubmodel(), this.getSubmodel_DeclarationZone(),
			"submodel", null, 0, 1, DeclarationZone.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeclarationZone_CategoryID(), this.getCategoryID(), this.getCategoryID_DeclarationZone(),
			"categoryID", null, 0, -1, DeclarationZone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeclarationZone_Variable(), this.getVariable(), this.getVariable_DeclarationZone(),
			"variable", null, 0, -1, DeclarationZone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelEClass, Submodel.class, "Submodel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubmodel_FormalParameters(), ecorePackage.getEString(), "formalParameters", null, 0, 1,
			Submodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEAttribute(getSubmodel_Dimension(), ecorePackage.getEString(), "dimension", null, 0, 1, Submodel.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel__module(), this.getModule(), this.getModule_Submodel(), "_module", null, 1, 1,
			Submodel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel_DeclarationZone(), this.getDeclarationZone(), this.getDeclarationZone_Submodel(),
			"declarationZone", null, 0, 1, Submodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel_ResponseArc(), this.getResponseArc(), this.getResponseArc_Submodel(), "responseArc",
			null, 0, -1, Submodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel_Arc(), this.getArc(), this.getArc_Submodel(), "arc", null, 1, -1, Submodel.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel_Ref(), this.getSubmodelRef(), this.getSubmodelRef_Target(), "ref", null, 0, -1,
			Submodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodel_Node(), this.getGraphNode(), this.getGraphNode_Submodel(), "node", null, 1, -1,
			Submodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(allocateEClass, Allocate.class, "Allocate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAllocate_Quantity(), ecorePackage.getEString(), "quantity", null, 0, 1, Allocate.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAllocate_GenerateCharacteristic(), ecorePackage.getEBooleanObject(),
			"generateCharacteristic", "false", 0, 1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
			!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAllocate_ResourceInstanceSpec(), ecorePackage.getEString(), "resourceInstanceSpec", null, 0,
			1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getAllocate__module(), this.getModule(), this.getModule_Allocate(), "_module", null, 0, 1,
			Allocate.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocate_Ref(), this.getAllocateRef(), this.getAllocateRef_Target(), "ref", null, 0, -1,
			Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAllocate_ResourceInstance(), this.getResourcePool(), this.getResourcePool_Allocate(),
			"resourceInstance", null, 0, 1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceEClass, Service.class, "Service", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getService_ServiceTime(), ecorePackage.getEString(), "serviceTime", null, 0, 1, Service.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getService_ServerQuantity(), ecorePackage.getEIntegerObject(), "serverQuantity", null, 0, 1,
			Service.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getService__module(), this.getModule(), this.getModule_Service(), "_module", null, 0, 1,
			Service.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getService_Ref(), this.getServiceRef(), this.getServiceRef_Target(), "ref", null, 0, -1,
			Service.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourcePoolEClass, ResourcePool.class, "ResourcePool", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResourcePool_Dimension(), ecorePackage.getEString(), "dimension", null, 0, 1,
			ResourcePool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourcePool_ResourceKind(), this.getResourceKind(), "resourceKind", null, 0, 1,
			ResourcePool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourcePool_TokenQuantity(), ecorePackage.getEIntegerObject(), "tokenQuantity", null, 0, 1,
			ResourcePool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getResourcePool__module(), this.getModule(), this.getModule_ResourcePool(), "_module", null, 0,
			1, ResourcePool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourcePool_Allocate(), this.getAllocate(), this.getAllocate_ResourceInstance(), "allocate",
			null, 0, -1, ResourcePool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourcePool_Release(), this.getRelease(), this.getRelease_ResourceInstance(), "release",
			null, 0, -1, ResourcePool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(graphNodeEClass, GraphNode.class, "GraphNode", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraphNode_OutArc(), this.getArc(), this.getArc_FromNode(), "outArc", null, 0, -1,
			GraphNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphNode_InArc(), this.getArc(), this.getArc_ToNode(), "inArc", null, 0, -1,
			GraphNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphNode_Submodel(), this.getSubmodel(), this.getSubmodel_Node(), "submodel", null, 0, 1,
			GraphNode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphNode_Interrupt(), this.getInterrupt(), this.getInterrupt_SelectNode(), "interrupt",
			null, 0, -1, GraphNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphNode_OutResponseArc(), this.getResponseArc(), this.getResponseArc_FromNode(),
			"outResponseArc", null, 0, -1, GraphNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphNode_InResponseArc(), this.getResponseArc(), this.getResponseArc_ToNode(),
			"inResponseArc", null, 0, -1, GraphNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(arcEClass, Arc.class, "Arc", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArc_SelectCondition(), ecorePackage.getEString(), "selectCondition", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArc_SelectProbability(), ecorePackage.getEString(), "selectProbability", null, 0, 1,
			Arc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEAttribute(getArc_SelectPhase(), ecorePackage.getEString(), "selectPhase", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArc_SelectParent(), ecorePackage.getEString(), "selectParent", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArc_DestNodeIndex(), ecorePackage.getEString(), "destNodeIndex", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArc_ChangePort(), ecorePackage.getEString(), "changePort", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArc_ChangePhase(), ecorePackage.getEString(), "changePhase", null, 0, 1, Arc.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArc_SelectCategory(), this.getCategoryID(), this.getCategoryID_Arc(), "selectCategory", null,
			0, 1, Arc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArc_Submodel(), this.getSubmodel(), this.getSubmodel_Arc(), "submodel", null, 1, 1,
			Arc.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArc_FromNode(), this.getGraphNode(), this.getGraphNode_OutArc(), "fromNode", null, 1, 1,
			Arc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArc_ToNode(), this.getGraphNode(), this.getGraphNode_InArc(), "toNode", null, 1, 1,
			Arc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(responseArcEClass, ResponseArc.class, "ResponseArc", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResponseArc_SourceNodeIndex(), ecorePackage.getEIntegerObject(), "sourceNodeIndex", null, 0,
			1, ResponseArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getResponseArc_DestinationNodeIndex(), ecorePackage.getEIntegerObject(), "destinationNodeIndex",
			null, 0, 1, ResponseArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResponseArc_Submodel(), this.getSubmodel(), this.getSubmodel_ResponseArc(), "submodel", null,
			1, 1, ResponseArc.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResponseArc_FromNode(), this.getGraphNode(), this.getGraphNode_OutResponseArc(), "fromNode",
			null, 1, 1, ResponseArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResponseArc_ToNode(), this.getGraphNode(), this.getGraphNode_InResponseArc(), "toNode", null,
			1, 1, ResponseArc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryIDEClass, CategoryID.class, "CategoryID", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryID_Dimension(), ecorePackage.getEIntegerObject(), "dimension", "1", 0, 1,
			CategoryID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryID_Arc(), this.getArc(), this.getArc_SelectCategory(), "arc", null, 0, -1,
			CategoryID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategoryID_DeclarationZone(), this.getDeclarationZone(),
			this.getDeclarationZone_CategoryID(), "declarationZone", null, 1, 1, CategoryID.class, IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getCategoryID_Interrupt(), this.getInterrupt(), this.getInterrupt_SelectCategory(), "interrupt",
			null, 0, -1, CategoryID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableEClass, Variable.class, "Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariable_Dimension(), ecorePackage.getEIntegerObject(), "dimension", "1", 0, 1,
			Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEAttribute(getVariable_StorageKind(), this.getStorageKind(), "storageKind", null, 0, 1, Variable.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_Datatype(), this.getDataTypeKind(), "datatype", null, 0, 1, Variable.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_Value(), ecorePackage.getEString(), "value", null, 0, 1, Variable.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariable_DeclarationZone(), this.getDeclarationZone(), this.getDeclarationZone_Variable(),
			"declarationZone", null, 1, 1, Variable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(directNodeEClass, DirectNode.class, "DirectNode", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDirectNode_Dimension(), ecorePackage.getEString(), "dimension", null, 0, 1, DirectNode.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDirectNode_Code(), ecorePackage.getEString(), "code", null, 0, 1, DirectNode.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDirectNode_FormalParameters(), ecorePackage.getEString(), "formalParameters", null, 0, 1,
			DirectNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDirectNode_Declarations(), ecorePackage.getEString(), "declarations", null, 0, 1,
			DirectNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getDirectNode_NodeStatistics(), this.getNodeStatistics(), this.getNodeStatistics_DirectNode(),
			"nodeStatistics", null, 0, -1, DirectNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refNodeEClass, RefNode.class, "RefNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefNode_ActualParameters(), ecorePackage.getEString(), "actualParameters", null, 0, 1,
			RefNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);

		initEClass(submodelRefEClass, SubmodelRef.class, "SubmodelRef", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelRef_Target(), this.getSubmodel(), this.getSubmodel_Ref(), "target", null, 1, 1,
			SubmodelRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceRefEClass, ServiceRef.class, "ServiceRef", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceRef_Target(), this.getService(), this.getService_Ref(), "target", null, 1, 1,
			ServiceRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(allocateRefEClass, AllocateRef.class, "AllocateRef", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAllocateRef_Target(), this.getAllocate(), this.getAllocate_Ref(), "target", null, 1, 1,
			AllocateRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeWithQueueEClass, NodeWithQueue.class, "NodeWithQueue", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodeWithQueue_Priority(), ecorePackage.getEString(), "priority", null, 0, 1,
			NodeWithQueue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getNodeWithQueue_PriorityRule(), this.getPriorityRuleKind(), "priorityRule", null, 0, 1,
			NodeWithQueue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getNodeWithQueue_TimeRule(), this.getTimeRuleKind(), "timeRule", null, 0, 1,
			NodeWithQueue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(blockEClass, Block.class, "Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlock_BlockUntil(), ecorePackage.getEString(), "blockUntil", null, 0, 1, Block.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock__module(), this.getModule(), this.getModule_Block(), "_module", null, 0, 1,
			Block.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Ref(), this.getBlockRef(), this.getBlockRef_Target(), "ref", null, 0, -1, Block.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(sourceEClass, Source.class, "Source", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSource_GenerateCharacteristic(), ecorePackage.getEBooleanObject(), "generateCharacteristic",
			"false", 0, 1, Source.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(sinkEClass, Sink.class, "Sink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(enterEClass, Enter.class, "Enter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(returnEClass, Return.class, "Return", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(splitEClass, Split.class, "Split", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(forkEClass, Fork.class, "Fork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(joinEClass, Join.class, "Join", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(userEClass, User.class, "User", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(delayEClass, Delay.class, "Delay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDelay_DelayTime(), ecorePackage.getEString(), "delayTime", null, 0, 1, Delay.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_GenerateCharacteristic(), ecorePackage.getEBooleanObject(), "generateCharacteristic",
			null, 0, 1, Delay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(releaseEClass, Release.class, "Release", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRelease_GenerateCharacteristic(), ecorePackage.getEBooleanObject(), "generateCharacteristic",
			"false", 0, 1, Release.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRelease_Quantity(), ecorePackage.getEString(), "quantity", null, 0, 1, Release.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRelease_ResourceInstanceSpec(), ecorePackage.getEString(), "resourceInstanceSpec", null, 0,
			1, Release.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getRelease_ResourceInstance(), this.getResourcePool(), this.getResourcePool_Release(),
			"resourceInstance", null, 0, 1, Release.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interruptEClass, Interrupt.class, "Interrupt", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInterrupt_SelectCondition(), ecorePackage.getEString(), "selectCondition", null, 0, 1,
			Interrupt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterrupt_SelectQuantity(), ecorePackage.getEIntegerObject(), "selectQuantity", "1", 0, 1,
			Interrupt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEReference(getInterrupt_SelectNode(), this.getGraphNode(), this.getGraphNode_Interrupt(), "selectNode",
			null, 0, 1, Interrupt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterrupt_SelectCategory(), this.getCategoryID(), this.getCategoryID_Interrupt(),
			"selectCategory", null, 0, 1, Interrupt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loopEClass, Loop.class, "Loop", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLoop_LoopStart(), this.getArc(), null, "loopStart", null, 1, 1, Loop.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);
		initEReference(getLoop_LoopEnd(), this.getArc(), null, "loopEnd", null, 1, 1, Loop.class, !IS_TRANSIENT,
			!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
			IS_ORDERED);

		initEClass(forLoopEClass, ForLoop.class, "ForLoop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getForLoop_Count(), ecorePackage.getEString(), "count", null, 0, 1, ForLoop.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionalLoopEClass, ConditionalLoop.class, "ConditionalLoop", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionalLoop_IsWhile(), ecorePackage.getEBooleanObject(), "isWhile", "true", 0, 1,
			ConditionalLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getConditionalLoop_Condition(), ecorePackage.getEString(), "condition", null, 0, 1,
			ConditionalLoop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(statisticsCollectorEClass, StatisticsCollector.class, "StatisticsCollector", IS_ABSTRACT,
			!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStatisticsCollector_BreakdownList(), ecorePackage.getEString(), "breakdownList", null, 0, 1,
			StatisticsCollector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getStatisticsCollector_HistogramBuckets(), ecorePackage.getEString(), "histogramBuckets", null,
			0, 1, StatisticsCollector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStatisticsCollector_UpdateChange(), ecorePackage.getEString(), "updateChange", null, 0, 1,
			StatisticsCollector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getStatisticsCollector_UpdateDelay(), ecorePackage.getEString(), "updateDelay", null, 0, 1,
			StatisticsCollector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(nodeStatisticsEClass, NodeStatistics.class, "NodeStatistics", !IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodeStatistics_Dimension(), ecorePackage.getEIntegerObject(), "dimension", "1", 0, 1,
			NodeStatistics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getNodeStatistics_Kind(), this.getStatClassKind(), "kind", null, 0, 1, NodeStatistics.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNodeStatistics_DirectNode(), this.getDirectNode(), this.getDirectNode_NodeStatistics(),
			"directNode", null, 1, 1, NodeStatistics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
			IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedElement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
			NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);

		initEClass(timedElementEClass, TimedElement.class, "TimedElement", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimedElement_TimeUnit(), this.getTimeUnit(), "timeUnit", null, 0, 1, TimedElement.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(superEClass, Super.class, "Super", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(resumeEClass, Resume.class, "Resume", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(branchEClass, Branch.class, "Branch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(duplicationNodeEClass, DuplicationNode.class, "DuplicationNode", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDuplicationNode_InheritsCalls(), ecorePackage.getEBooleanObject(), "inheritsCalls", "true",
			0, 1, DuplicationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
			!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuplicationNode_InheritsUnshared(), ecorePackage.getEBooleanObject(), "inheritsUnshared",
			"true", 0, 1, DuplicationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
			IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blockRefEClass, BlockRef.class, "BlockRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlockRef_Target(), this.getBlock(), this.getBlock_Ref(), "target", null, 1, 1,
			BlockRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
			!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(graphicalNodeEClass, GraphicalNode.class, "GraphicalNode", IS_ABSTRACT, !IS_INTERFACE,
			IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGraphicalNode_X(), ecorePackage.getEIntegerObject(), "x", "0", 0, 1, GraphicalNode.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphicalNode_Y(), ecorePackage.getEIntegerObject(), "y", "0", 0, 1, GraphicalNode.class,
			!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(storageKindEEnum, StorageKind.class, "StorageKind");
		addEEnumLiteral(storageKindEEnum, StorageKind.UNSHARED);
		addEEnumLiteral(storageKindEEnum, StorageKind.PARAMETER);
		addEEnumLiteral(storageKindEEnum, StorageKind.CONST);
		addEEnumLiteral(storageKindEEnum, StorageKind.EXTERN_C);
		addEEnumLiteral(storageKindEEnum, StorageKind.PARAMETER_CONST);
		addEEnumLiteral(storageKindEEnum, StorageKind.PARAMETER_REEVAL);
		addEEnumLiteral(storageKindEEnum, StorageKind.SHARED);

		initEEnum(dataTypeKindEEnum, DataTypeKind.class, "DataTypeKind");
		addEEnumLiteral(dataTypeKindEEnum, DataTypeKind.INT);
		addEEnumLiteral(dataTypeKindEEnum, DataTypeKind.CHARSTRING);
		addEEnumLiteral(dataTypeKindEEnum, DataTypeKind.DOUBLE);
		addEEnumLiteral(dataTypeKindEEnum, DataTypeKind.LONG);
		addEEnumLiteral(dataTypeKindEEnum, DataTypeKind.TTIME);

		initEEnum(priorityRuleKindEEnum, PriorityRuleKind.class, "PriorityRuleKind");
		addEEnumLiteral(priorityRuleKindEEnum, PriorityRuleKind.NOPRIORITY);
		addEEnumLiteral(priorityRuleKindEEnum, PriorityRuleKind.POLLING);
		addEEnumLiteral(priorityRuleKindEEnum, PriorityRuleKind.PREEMPTIVE);
		addEEnumLiteral(priorityRuleKindEEnum, PriorityRuleKind.NONPREEMPTIVE);

		initEEnum(timeRuleKindEEnum, TimeRuleKind.class, "TimeRuleKind");
		addEEnumLiteral(timeRuleKindEEnum, TimeRuleKind.FCFS);
		addEEnumLiteral(timeRuleKindEEnum, TimeRuleKind.LCFSPR);
		addEEnumLiteral(timeRuleKindEEnum, TimeRuleKind.PS);
		addEEnumLiteral(timeRuleKindEEnum, TimeRuleKind.RR);

		initEEnum(resourceKindEEnum, ResourceKind.class, "ResourceKind");
		addEEnumLiteral(resourceKindEEnum, ResourceKind.TOKEN);
		addEEnumLiteral(resourceKindEEnum, ResourceKind.BESTFIT);
		addEEnumLiteral(resourceKindEEnum, ResourceKind.FIRSTFIT);
		addEEnumLiteral(resourceKindEEnum, ResourceKind.CONSUMED);
		addEEnumLiteral(resourceKindEEnum, ResourceKind.DATA);

		initEEnum(statClassKindEEnum, StatClassKind.class, "StatClassKind");
		addEEnumLiteral(statClassKindEEnum, StatClassKind.CONTINUOUS);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.DISCRETE);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.INTERARRIVAL);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.LIFETIME);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.POPULATION);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.RESPONSE);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.UTILIZATION);
		addEEnumLiteral(statClassKindEEnum, StatClassKind.QPOPULATION);

		initEEnum(timeUnitEEnum, TimeUnit.class, "TimeUnit");
		addEEnumLiteral(timeUnitEEnum, TimeUnit.PICOSECOND);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.NANOSECOND);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.MICROSECOND);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.MILLISECOND);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.SECOND);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.MINUTE);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.HOUR);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.MODULE_UNIT);

		// Create resource
		createResource(eNS_URI);
	}

} // WbPackageImpl
