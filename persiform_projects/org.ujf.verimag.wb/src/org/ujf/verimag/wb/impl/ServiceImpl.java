/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.ServiceRef;
import org.ujf.verimag.wb.TimeUnit;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Service</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ServiceImpl#getTimeUnit <em>Time Unit</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ServiceImpl#getServiceTime <em>Service Time</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ServiceImpl#getServerQuantity <em>Server Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ServiceImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ServiceImpl#getRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ServiceImpl extends NodeWithQueueImpl implements Service
{
	/**
	 * The default value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnit	TIME_UNIT_EDEFAULT			= TimeUnit.PICOSECOND;

	/**
	 * The cached value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected TimeUnit				timeUnit					= TIME_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getServiceTime() <em>Service Time</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getServiceTime()
	 * @generated
	 * @ordered
	 */
	protected static final String	SERVICE_TIME_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getServiceTime() <em>Service Time</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getServiceTime()
	 * @generated
	 * @ordered
	 */
	protected String				serviceTime					= SERVICE_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getServerQuantity() <em>Server Quantity</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getServerQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final Integer	SERVER_QUANTITY_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getServerQuantity() <em>Server Quantity</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getServerQuantity()
	 * @generated
	 * @ordered
	 */
	protected Integer				serverQuantity				= SERVER_QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceRef>		ref;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ServiceImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.SERVICE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeUnit getTimeUnit()
	{
		return timeUnit;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTimeUnit(TimeUnit newTimeUnit)
	{
		TimeUnit oldTimeUnit = timeUnit;
		timeUnit = newTimeUnit == null ? TIME_UNIT_EDEFAULT : newTimeUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SERVICE__TIME_UNIT, oldTimeUnit, timeUnit));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getServiceTime()
	{
		return serviceTime;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setServiceTime(String newServiceTime)
	{
		String oldServiceTime = serviceTime;
		serviceTime = newServiceTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SERVICE__SERVICE_TIME, oldServiceTime,
				serviceTime));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getServerQuantity()
	{
		return serverQuantity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setServerQuantity(Integer newServerQuantity)
	{
		Integer oldServerQuantity = serverQuantity;
		serverQuantity = newServerQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SERVICE__SERVER_QUANTITY,
				oldServerQuantity, serverQuantity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.SERVICE__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.SERVICE__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.SERVICE__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__SERVICE, Module.class, msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SERVICE__MODULE, new_module, new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ServiceRef> getRef()
	{
		if (ref == null)
		{
			ref = new EObjectWithInverseResolvingEList<ServiceRef>(ServiceRef.class, this, WbPackage.SERVICE__REF,
				WbPackage.SERVICE_REF__TARGET);
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.SERVICE__REF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRef()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.SERVICE__REF:
			return ((InternalEList<?>) getRef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.SERVICE__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__SERVICE, Module.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__TIME_UNIT:
			return getTimeUnit();
		case WbPackage.SERVICE__SERVICE_TIME:
			return getServiceTime();
		case WbPackage.SERVICE__SERVER_QUANTITY:
			return getServerQuantity();
		case WbPackage.SERVICE__MODULE:
			return get_module();
		case WbPackage.SERVICE__REF:
			return getRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__TIME_UNIT:
			setTimeUnit((TimeUnit) newValue);
			return;
		case WbPackage.SERVICE__SERVICE_TIME:
			setServiceTime((String) newValue);
			return;
		case WbPackage.SERVICE__SERVER_QUANTITY:
			setServerQuantity((Integer) newValue);
			return;
		case WbPackage.SERVICE__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.SERVICE__REF:
			getRef().clear();
			getRef().addAll((Collection<? extends ServiceRef>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__TIME_UNIT:
			setTimeUnit(TIME_UNIT_EDEFAULT);
			return;
		case WbPackage.SERVICE__SERVICE_TIME:
			setServiceTime(SERVICE_TIME_EDEFAULT);
			return;
		case WbPackage.SERVICE__SERVER_QUANTITY:
			setServerQuantity(SERVER_QUANTITY_EDEFAULT);
			return;
		case WbPackage.SERVICE__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.SERVICE__REF:
			getRef().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SERVICE__TIME_UNIT:
			return timeUnit != TIME_UNIT_EDEFAULT;
		case WbPackage.SERVICE__SERVICE_TIME:
			return SERVICE_TIME_EDEFAULT == null ? serviceTime != null : !SERVICE_TIME_EDEFAULT.equals(serviceTime);
		case WbPackage.SERVICE__SERVER_QUANTITY:
			return SERVER_QUANTITY_EDEFAULT == null ? serverQuantity != null : !SERVER_QUANTITY_EDEFAULT
					.equals(serverQuantity);
		case WbPackage.SERVICE__MODULE:
			return get_module() != null;
		case WbPackage.SERVICE__REF:
			return ref != null && !ref.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (derivedFeatureID)
			{
			case WbPackage.SERVICE__TIME_UNIT:
				return WbPackage.TIMED_ELEMENT__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (baseFeatureID)
			{
			case WbPackage.TIMED_ELEMENT__TIME_UNIT:
				return WbPackage.SERVICE__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeUnit: ");
		result.append(timeUnit);
		result.append(", serviceTime: ");
		result.append(serviceTime);
		result.append(", serverQuantity: ");
		result.append(serverQuantity);
		result.append(')');
		return result.toString();
	}

} // ServiceImpl
