/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.StatisticsCollector;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Statistics Collector</b></em>'. <!--
 * end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl#getBreakdownList <em>Breakdown List</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl#getHistogramBuckets <em>Histogram Buckets</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl#getUpdateChange <em>Update Change</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.StatisticsCollectorImpl#getUpdateDelay <em>Update Delay</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class StatisticsCollectorImpl extends NamedElementImpl implements StatisticsCollector
{
	/**
	 * The default value of the '{@link #getBreakdownList() <em>Breakdown List</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBreakdownList()
	 * @generated
	 * @ordered
	 */
	protected static final String	BREAKDOWN_LIST_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getBreakdownList() <em>Breakdown List</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBreakdownList()
	 * @generated
	 * @ordered
	 */
	protected String				breakdownList				= BREAKDOWN_LIST_EDEFAULT;

	/**
	 * The default value of the '{@link #getHistogramBuckets() <em>Histogram Buckets</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHistogramBuckets()
	 * @generated
	 * @ordered
	 */
	protected static final String	HISTOGRAM_BUCKETS_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getHistogramBuckets() <em>Histogram Buckets</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHistogramBuckets()
	 * @generated
	 * @ordered
	 */
	protected String				histogramBuckets			= HISTOGRAM_BUCKETS_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpdateChange() <em>Update Change</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getUpdateChange()
	 * @generated
	 * @ordered
	 */
	protected static final String	UPDATE_CHANGE_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getUpdateChange() <em>Update Change</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getUpdateChange()
	 * @generated
	 * @ordered
	 */
	protected String				updateChange				= UPDATE_CHANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpdateDelay() <em>Update Delay</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getUpdateDelay()
	 * @generated
	 * @ordered
	 */
	protected static final String	UPDATE_DELAY_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getUpdateDelay() <em>Update Delay</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getUpdateDelay()
	 * @generated
	 * @ordered
	 */
	protected String				updateDelay					= UPDATE_DELAY_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatisticsCollectorImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.STATISTICS_COLLECTOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getBreakdownList()
	{
		return breakdownList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBreakdownList(String newBreakdownList)
	{
		String oldBreakdownList = breakdownList;
		breakdownList = newBreakdownList;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.STATISTICS_COLLECTOR__BREAKDOWN_LIST,
				oldBreakdownList, breakdownList));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getHistogramBuckets()
	{
		return histogramBuckets;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setHistogramBuckets(String newHistogramBuckets)
	{
		String oldHistogramBuckets = histogramBuckets;
		histogramBuckets = newHistogramBuckets;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS,
				oldHistogramBuckets, histogramBuckets));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getUpdateChange()
	{
		return updateChange;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setUpdateChange(String newUpdateChange)
	{
		String oldUpdateChange = updateChange;
		updateChange = newUpdateChange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.STATISTICS_COLLECTOR__UPDATE_CHANGE,
				oldUpdateChange, updateChange));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getUpdateDelay()
	{
		return updateDelay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setUpdateDelay(String newUpdateDelay)
	{
		String oldUpdateDelay = updateDelay;
		updateDelay = newUpdateDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.STATISTICS_COLLECTOR__UPDATE_DELAY,
				oldUpdateDelay, updateDelay));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.STATISTICS_COLLECTOR__BREAKDOWN_LIST:
			return getBreakdownList();
		case WbPackage.STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS:
			return getHistogramBuckets();
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_CHANGE:
			return getUpdateChange();
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_DELAY:
			return getUpdateDelay();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.STATISTICS_COLLECTOR__BREAKDOWN_LIST:
			setBreakdownList((String) newValue);
			return;
		case WbPackage.STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS:
			setHistogramBuckets((String) newValue);
			return;
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_CHANGE:
			setUpdateChange((String) newValue);
			return;
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_DELAY:
			setUpdateDelay((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.STATISTICS_COLLECTOR__BREAKDOWN_LIST:
			setBreakdownList(BREAKDOWN_LIST_EDEFAULT);
			return;
		case WbPackage.STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS:
			setHistogramBuckets(HISTOGRAM_BUCKETS_EDEFAULT);
			return;
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_CHANGE:
			setUpdateChange(UPDATE_CHANGE_EDEFAULT);
			return;
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_DELAY:
			setUpdateDelay(UPDATE_DELAY_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.STATISTICS_COLLECTOR__BREAKDOWN_LIST:
			return BREAKDOWN_LIST_EDEFAULT == null ? breakdownList != null : !BREAKDOWN_LIST_EDEFAULT
					.equals(breakdownList);
		case WbPackage.STATISTICS_COLLECTOR__HISTOGRAM_BUCKETS:
			return HISTOGRAM_BUCKETS_EDEFAULT == null ? histogramBuckets != null : !HISTOGRAM_BUCKETS_EDEFAULT
					.equals(histogramBuckets);
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_CHANGE:
			return UPDATE_CHANGE_EDEFAULT == null ? updateChange != null : !UPDATE_CHANGE_EDEFAULT.equals(updateChange);
		case WbPackage.STATISTICS_COLLECTOR__UPDATE_DELAY:
			return UPDATE_DELAY_EDEFAULT == null ? updateDelay != null : !UPDATE_DELAY_EDEFAULT.equals(updateDelay);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (breakdownList: ");
		result.append(breakdownList);
		result.append(", histogramBuckets: ");
		result.append(histogramBuckets);
		result.append(", updateChange: ");
		result.append(updateChange);
		result.append(", updateDelay: ");
		result.append(updateDelay);
		result.append(')');
		return result.toString();
	}

} // StatisticsCollectorImpl
