/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.Release;
import org.ujf.verimag.wb.ResourceKind;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Resource Pool</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#getResourceKind <em>Resource Kind</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#getTokenQuantity <em>Token Quantity</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#getAllocate <em>Allocate</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResourcePoolImpl#getRelease <em>Release</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ResourcePoolImpl extends GraphicalNodeImpl implements ResourcePool
{
	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final String		DIMENSION_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected String					dimension				= DIMENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getResourceKind() <em>Resource Kind</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getResourceKind()
	 * @generated
	 * @ordered
	 */
	protected static final ResourceKind	RESOURCE_KIND_EDEFAULT	= ResourceKind.TOKEN;

	/**
	 * The cached value of the '{@link #getResourceKind() <em>Resource Kind</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getResourceKind()
	 * @generated
	 * @ordered
	 */
	protected ResourceKind				resourceKind			= RESOURCE_KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getTokenQuantity() <em>Token Quantity</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getTokenQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final Integer		TOKEN_QUANTITY_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getTokenQuantity() <em>Token Quantity</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getTokenQuantity()
	 * @generated
	 * @ordered
	 */
	protected Integer					tokenQuantity			= TOKEN_QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAllocate() <em>Allocate</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getAllocate()
	 * @generated
	 * @ordered
	 */
	protected EList<Allocate>			allocate;

	/**
	 * The cached value of the '{@link #getRelease() <em>Release</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRelease()
	 * @generated
	 * @ordered
	 */
	protected EList<Release>			release;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ResourcePoolImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.RESOURCE_POOL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(String newDimension)
	{
		String oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESOURCE_POOL__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResourceKind getResourceKind()
	{
		return resourceKind;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setResourceKind(ResourceKind newResourceKind)
	{
		ResourceKind oldResourceKind = resourceKind;
		resourceKind = newResourceKind == null ? RESOURCE_KIND_EDEFAULT : newResourceKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESOURCE_POOL__RESOURCE_KIND,
				oldResourceKind, resourceKind));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getTokenQuantity()
	{
		return tokenQuantity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTokenQuantity(Integer newTokenQuantity)
	{
		Integer oldTokenQuantity = tokenQuantity;
		tokenQuantity = newTokenQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESOURCE_POOL__TOKEN_QUANTITY,
				oldTokenQuantity, tokenQuantity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.RESOURCE_POOL__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.RESOURCE_POOL__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.RESOURCE_POOL__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__RESOURCE_POOL, Module.class,
					msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESOURCE_POOL__MODULE, new_module,
				new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Allocate> getAllocate()
	{
		if (allocate == null)
		{
			allocate = new EObjectWithInverseResolvingEList<Allocate>(Allocate.class, this,
				WbPackage.RESOURCE_POOL__ALLOCATE, WbPackage.ALLOCATE__RESOURCE_INSTANCE);
		}
		return allocate;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Release> getRelease()
	{
		if (release == null)
		{
			release = new EObjectWithInverseResolvingEList<Release>(Release.class, this,
				WbPackage.RESOURCE_POOL__RELEASE, WbPackage.RELEASE__RESOURCE_INSTANCE);
		}
		return release;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAllocate()).basicAdd(otherEnd, msgs);
		case WbPackage.RESOURCE_POOL__RELEASE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRelease()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			return ((InternalEList<?>) getAllocate()).basicRemove(otherEnd, msgs);
		case WbPackage.RESOURCE_POOL__RELEASE:
			return ((InternalEList<?>) getRelease()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.RESOURCE_POOL__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__RESOURCE_POOL, Module.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__DIMENSION:
			return getDimension();
		case WbPackage.RESOURCE_POOL__RESOURCE_KIND:
			return getResourceKind();
		case WbPackage.RESOURCE_POOL__TOKEN_QUANTITY:
			return getTokenQuantity();
		case WbPackage.RESOURCE_POOL__MODULE:
			return get_module();
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			return getAllocate();
		case WbPackage.RESOURCE_POOL__RELEASE:
			return getRelease();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__DIMENSION:
			setDimension((String) newValue);
			return;
		case WbPackage.RESOURCE_POOL__RESOURCE_KIND:
			setResourceKind((ResourceKind) newValue);
			return;
		case WbPackage.RESOURCE_POOL__TOKEN_QUANTITY:
			setTokenQuantity((Integer) newValue);
			return;
		case WbPackage.RESOURCE_POOL__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			getAllocate().clear();
			getAllocate().addAll((Collection<? extends Allocate>) newValue);
			return;
		case WbPackage.RESOURCE_POOL__RELEASE:
			getRelease().clear();
			getRelease().addAll((Collection<? extends Release>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.RESOURCE_POOL__RESOURCE_KIND:
			setResourceKind(RESOURCE_KIND_EDEFAULT);
			return;
		case WbPackage.RESOURCE_POOL__TOKEN_QUANTITY:
			setTokenQuantity(TOKEN_QUANTITY_EDEFAULT);
			return;
		case WbPackage.RESOURCE_POOL__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			getAllocate().clear();
			return;
		case WbPackage.RESOURCE_POOL__RELEASE:
			getRelease().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.RESOURCE_POOL__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.RESOURCE_POOL__RESOURCE_KIND:
			return resourceKind != RESOURCE_KIND_EDEFAULT;
		case WbPackage.RESOURCE_POOL__TOKEN_QUANTITY:
			return TOKEN_QUANTITY_EDEFAULT == null ? tokenQuantity != null : !TOKEN_QUANTITY_EDEFAULT
					.equals(tokenQuantity);
		case WbPackage.RESOURCE_POOL__MODULE:
			return get_module() != null;
		case WbPackage.RESOURCE_POOL__ALLOCATE:
			return allocate != null && !allocate.isEmpty();
		case WbPackage.RESOURCE_POOL__RELEASE:
			return release != null && !release.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(", resourceKind: ");
		result.append(resourceKind);
		result.append(", tokenQuantity: ");
		result.append(tokenQuantity);
		result.append(')');
		return result.toString();
	}

} // ResourcePoolImpl
