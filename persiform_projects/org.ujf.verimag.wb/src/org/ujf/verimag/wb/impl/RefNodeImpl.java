/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.RefNode;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Ref Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.RefNodeImpl#getActualParameters <em>Actual Parameters</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class RefNodeImpl extends GraphNodeImpl implements RefNode
{
	/**
	 * The default value of the '{@link #getActualParameters() <em>Actual Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActualParameters()
	 * @generated
	 * @ordered
	 */
	protected static final String	ACTUAL_PARAMETERS_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getActualParameters() <em>Actual Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getActualParameters()
	 * @generated
	 * @ordered
	 */
	protected String				actualParameters			= ACTUAL_PARAMETERS_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected RefNodeImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.REF_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getActualParameters()
	{
		return actualParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setActualParameters(String newActualParameters)
	{
		String oldActualParameters = actualParameters;
		actualParameters = newActualParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.REF_NODE__ACTUAL_PARAMETERS,
				oldActualParameters, actualParameters));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.REF_NODE__ACTUAL_PARAMETERS:
			return getActualParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.REF_NODE__ACTUAL_PARAMETERS:
			setActualParameters((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.REF_NODE__ACTUAL_PARAMETERS:
			setActualParameters(ACTUAL_PARAMETERS_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.REF_NODE__ACTUAL_PARAMETERS:
			return ACTUAL_PARAMETERS_EDEFAULT == null ? actualParameters != null : !ACTUAL_PARAMETERS_EDEFAULT
					.equals(actualParameters);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (actualParameters: ");
		result.append(actualParameters);
		result.append(')');
		return result.toString();
	}

} // RefNodeImpl
