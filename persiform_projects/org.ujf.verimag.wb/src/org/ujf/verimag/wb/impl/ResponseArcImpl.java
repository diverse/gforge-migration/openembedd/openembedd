/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Response Arc</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.ResponseArcImpl#getSourceNodeIndex <em>Source Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResponseArcImpl#getDestinationNodeIndex <em>Destination Node Index</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResponseArcImpl#getSubmodel <em>Submodel</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResponseArcImpl#getFromNode <em>From Node</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.ResponseArcImpl#getToNode <em>To Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ResponseArcImpl extends StatisticsCollectorImpl implements ResponseArc
{
	/**
	 * The default value of the '{@link #getSourceNodeIndex() <em>Source Node Index</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected static final Integer	SOURCE_NODE_INDEX_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getSourceNodeIndex() <em>Source Node Index</em>}' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSourceNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected Integer				sourceNodeIndex					= SOURCE_NODE_INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestinationNodeIndex() <em>Destination Node Index</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDestinationNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected static final Integer	DESTINATION_NODE_INDEX_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getDestinationNodeIndex() <em>Destination Node Index</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDestinationNodeIndex()
	 * @generated
	 * @ordered
	 */
	protected Integer				destinationNodeIndex			= DESTINATION_NODE_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFromNode() <em>From Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getFromNode()
	 * @generated
	 * @ordered
	 */
	protected GraphNode				fromNode;

	/**
	 * The cached value of the '{@link #getToNode() <em>To Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getToNode()
	 * @generated
	 * @ordered
	 */
	protected GraphNode				toNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ResponseArcImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.RESPONSE_ARC;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getSourceNodeIndex()
	{
		return sourceNodeIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSourceNodeIndex(Integer newSourceNodeIndex)
	{
		Integer oldSourceNodeIndex = sourceNodeIndex;
		sourceNodeIndex = newSourceNodeIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESPONSE_ARC__SOURCE_NODE_INDEX,
				oldSourceNodeIndex, sourceNodeIndex));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getDestinationNodeIndex()
	{
		return destinationNodeIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDestinationNodeIndex(Integer newDestinationNodeIndex)
	{
		Integer oldDestinationNodeIndex = destinationNodeIndex;
		destinationNodeIndex = newDestinationNodeIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESPONSE_ARC__DESTINATION_NODE_INDEX,
				oldDestinationNodeIndex, destinationNodeIndex));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Submodel getSubmodel()
	{
		if (eContainerFeatureID != WbPackage.RESPONSE_ARC__SUBMODEL)
			return null;
		return (Submodel) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSubmodel(Submodel newSubmodel, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) newSubmodel, WbPackage.RESPONSE_ARC__SUBMODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSubmodel(Submodel newSubmodel)
	{
		if (newSubmodel != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.RESPONSE_ARC__SUBMODEL && newSubmodel != null))
		{
			if (EcoreUtil.isAncestor(this, newSubmodel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubmodel != null)
				msgs = ((InternalEObject) newSubmodel).eInverseAdd(this, WbPackage.SUBMODEL__RESPONSE_ARC,
					Submodel.class, msgs);
			msgs = basicSetSubmodel(newSubmodel, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESPONSE_ARC__SUBMODEL, newSubmodel,
				newSubmodel));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode getFromNode()
	{
		if (fromNode != null && fromNode.eIsProxy())
		{
			InternalEObject oldFromNode = (InternalEObject) fromNode;
			fromNode = (GraphNode) eResolveProxy(oldFromNode);
			if (fromNode != oldFromNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.RESPONSE_ARC__FROM_NODE,
						oldFromNode, fromNode));
			}
		}
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode basicGetFromNode()
	{
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetFromNode(GraphNode newFromNode, NotificationChain msgs)
	{
		GraphNode oldFromNode = fromNode;
		fromNode = newFromNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.RESPONSE_ARC__FROM_NODE, oldFromNode, newFromNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setFromNode(GraphNode newFromNode)
	{
		if (newFromNode != fromNode)
		{
			NotificationChain msgs = null;
			if (fromNode != null)
				msgs = ((InternalEObject) fromNode).eInverseRemove(this, WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC,
					GraphNode.class, msgs);
			if (newFromNode != null)
				msgs = ((InternalEObject) newFromNode).eInverseAdd(this, WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC,
					GraphNode.class, msgs);
			msgs = basicSetFromNode(newFromNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESPONSE_ARC__FROM_NODE, newFromNode,
				newFromNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode getToNode()
	{
		if (toNode != null && toNode.eIsProxy())
		{
			InternalEObject oldToNode = (InternalEObject) toNode;
			toNode = (GraphNode) eResolveProxy(oldToNode);
			if (toNode != oldToNode)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WbPackage.RESPONSE_ARC__TO_NODE,
						oldToNode, toNode));
			}
		}
		return toNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GraphNode basicGetToNode()
	{
		return toNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetToNode(GraphNode newToNode, NotificationChain msgs)
	{
		GraphNode oldToNode = toNode;
		toNode = newToNode;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.RESPONSE_ARC__TO_NODE, oldToNode, newToNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setToNode(GraphNode newToNode)
	{
		if (newToNode != toNode)
		{
			NotificationChain msgs = null;
			if (toNode != null)
				msgs = ((InternalEObject) toNode).eInverseRemove(this, WbPackage.GRAPH_NODE__IN_RESPONSE_ARC,
					GraphNode.class, msgs);
			if (newToNode != null)
				msgs = ((InternalEObject) newToNode).eInverseAdd(this, WbPackage.GRAPH_NODE__IN_RESPONSE_ARC,
					GraphNode.class, msgs);
			msgs = basicSetToNode(newToNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.RESPONSE_ARC__TO_NODE, newToNode, newToNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSubmodel((Submodel) otherEnd, msgs);
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			if (fromNode != null)
				msgs = ((InternalEObject) fromNode).eInverseRemove(this, WbPackage.GRAPH_NODE__OUT_RESPONSE_ARC,
					GraphNode.class, msgs);
			return basicSetFromNode((GraphNode) otherEnd, msgs);
		case WbPackage.RESPONSE_ARC__TO_NODE:
			if (toNode != null)
				msgs = ((InternalEObject) toNode).eInverseRemove(this, WbPackage.GRAPH_NODE__IN_RESPONSE_ARC,
					GraphNode.class, msgs);
			return basicSetToNode((GraphNode) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			return basicSetSubmodel(null, msgs);
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			return basicSetFromNode(null, msgs);
		case WbPackage.RESPONSE_ARC__TO_NODE:
			return basicSetToNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			return eInternalContainer().eInverseRemove(this, WbPackage.SUBMODEL__RESPONSE_ARC, Submodel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SOURCE_NODE_INDEX:
			return getSourceNodeIndex();
		case WbPackage.RESPONSE_ARC__DESTINATION_NODE_INDEX:
			return getDestinationNodeIndex();
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			return getSubmodel();
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			if (resolve)
				return getFromNode();
			return basicGetFromNode();
		case WbPackage.RESPONSE_ARC__TO_NODE:
			if (resolve)
				return getToNode();
			return basicGetToNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SOURCE_NODE_INDEX:
			setSourceNodeIndex((Integer) newValue);
			return;
		case WbPackage.RESPONSE_ARC__DESTINATION_NODE_INDEX:
			setDestinationNodeIndex((Integer) newValue);
			return;
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			setSubmodel((Submodel) newValue);
			return;
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			setFromNode((GraphNode) newValue);
			return;
		case WbPackage.RESPONSE_ARC__TO_NODE:
			setToNode((GraphNode) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SOURCE_NODE_INDEX:
			setSourceNodeIndex(SOURCE_NODE_INDEX_EDEFAULT);
			return;
		case WbPackage.RESPONSE_ARC__DESTINATION_NODE_INDEX:
			setDestinationNodeIndex(DESTINATION_NODE_INDEX_EDEFAULT);
			return;
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			setSubmodel((Submodel) null);
			return;
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			setFromNode((GraphNode) null);
			return;
		case WbPackage.RESPONSE_ARC__TO_NODE:
			setToNode((GraphNode) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.RESPONSE_ARC__SOURCE_NODE_INDEX:
			return SOURCE_NODE_INDEX_EDEFAULT == null ? sourceNodeIndex != null : !SOURCE_NODE_INDEX_EDEFAULT
					.equals(sourceNodeIndex);
		case WbPackage.RESPONSE_ARC__DESTINATION_NODE_INDEX:
			return DESTINATION_NODE_INDEX_EDEFAULT == null ? destinationNodeIndex != null
					: !DESTINATION_NODE_INDEX_EDEFAULT.equals(destinationNodeIndex);
		case WbPackage.RESPONSE_ARC__SUBMODEL:
			return getSubmodel() != null;
		case WbPackage.RESPONSE_ARC__FROM_NODE:
			return fromNode != null;
		case WbPackage.RESPONSE_ARC__TO_NODE:
			return toNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourceNodeIndex: ");
		result.append(sourceNodeIndex);
		result.append(", destinationNodeIndex: ");
		result.append(destinationNodeIndex);
		result.append(')');
		return result.toString();
	}

} // ResponseArcImpl
