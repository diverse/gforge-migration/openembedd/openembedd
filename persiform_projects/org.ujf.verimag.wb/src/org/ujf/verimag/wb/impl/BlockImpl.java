/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.BlockRef;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Block</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.BlockImpl#getBlockUntil <em>Block Until</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.BlockImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.BlockImpl#getRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class BlockImpl extends NodeWithQueueImpl implements Block
{
	/**
	 * The default value of the '{@link #getBlockUntil() <em>Block Until</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getBlockUntil()
	 * @generated
	 * @ordered
	 */
	protected static final String	BLOCK_UNTIL_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getBlockUntil() <em>Block Until</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getBlockUntil()
	 * @generated
	 * @ordered
	 */
	protected String				blockUntil				= BLOCK_UNTIL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected EList<BlockRef>		ref;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected BlockImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.BLOCK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getBlockUntil()
	{
		return blockUntil;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBlockUntil(String newBlockUntil)
	{
		String oldBlockUntil = blockUntil;
		blockUntil = newBlockUntil;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.BLOCK__BLOCK_UNTIL, oldBlockUntil,
				blockUntil));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.BLOCK__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.BLOCK__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.BLOCK__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__BLOCK, Module.class, msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.BLOCK__MODULE, new_module, new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<BlockRef> getRef()
	{
		if (ref == null)
		{
			ref = new EObjectWithInverseResolvingEList<BlockRef>(BlockRef.class, this, WbPackage.BLOCK__REF,
				WbPackage.BLOCK_REF__TARGET);
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.BLOCK__REF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRef()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.BLOCK__REF:
			return ((InternalEList<?>) getRef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.BLOCK__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__BLOCK, Module.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__BLOCK_UNTIL:
			return getBlockUntil();
		case WbPackage.BLOCK__MODULE:
			return get_module();
		case WbPackage.BLOCK__REF:
			return getRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__BLOCK_UNTIL:
			setBlockUntil((String) newValue);
			return;
		case WbPackage.BLOCK__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.BLOCK__REF:
			getRef().clear();
			getRef().addAll((Collection<? extends BlockRef>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__BLOCK_UNTIL:
			setBlockUntil(BLOCK_UNTIL_EDEFAULT);
			return;
		case WbPackage.BLOCK__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.BLOCK__REF:
			getRef().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.BLOCK__BLOCK_UNTIL:
			return BLOCK_UNTIL_EDEFAULT == null ? blockUntil != null : !BLOCK_UNTIL_EDEFAULT.equals(blockUntil);
		case WbPackage.BLOCK__MODULE:
			return get_module() != null;
		case WbPackage.BLOCK__REF:
			return ref != null && !ref.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (blockUntil: ");
		result.append(blockUntil);
		result.append(')');
		return result.toString();
	}

} // BlockImpl
