/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.Delay;
import org.ujf.verimag.wb.TimeUnit;
import org.ujf.verimag.wb.TimedElement;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Delay</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.DelayImpl#getTimeUnit <em>Time Unit</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DelayImpl#getDelayTime <em>Delay Time</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DelayImpl#getGenerateCharacteristic <em>Generate Characteristic</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class DelayImpl extends DirectNodeImpl implements Delay
{
	/**
	 * The default value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnit	TIME_UNIT_EDEFAULT					= TimeUnit.PICOSECOND;

	/**
	 * The cached value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected TimeUnit				timeUnit							= TIME_UNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDelayTime() <em>Delay Time</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDelayTime()
	 * @generated
	 * @ordered
	 */
	protected static final String	DELAY_TIME_EDEFAULT					= null;

	/**
	 * The cached value of the '{@link #getDelayTime() <em>Delay Time</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDelayTime()
	 * @generated
	 * @ordered
	 */
	protected String				delayTime							= DELAY_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean	GENERATE_CHARACTERISTIC_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getGenerateCharacteristic() <em>Generate Characteristic</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGenerateCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected Boolean				generateCharacteristic				= GENERATE_CHARACTERISTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DelayImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.DELAY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeUnit getTimeUnit()
	{
		return timeUnit;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTimeUnit(TimeUnit newTimeUnit)
	{
		TimeUnit oldTimeUnit = timeUnit;
		timeUnit = newTimeUnit == null ? TIME_UNIT_EDEFAULT : newTimeUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DELAY__TIME_UNIT, oldTimeUnit, timeUnit));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDelayTime()
	{
		return delayTime;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDelayTime(String newDelayTime)
	{
		String oldDelayTime = delayTime;
		delayTime = newDelayTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DELAY__DELAY_TIME, oldDelayTime, delayTime));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Boolean getGenerateCharacteristic()
	{
		return generateCharacteristic;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setGenerateCharacteristic(Boolean newGenerateCharacteristic)
	{
		Boolean oldGenerateCharacteristic = generateCharacteristic;
		generateCharacteristic = newGenerateCharacteristic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DELAY__GENERATE_CHARACTERISTIC,
				oldGenerateCharacteristic, generateCharacteristic));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.DELAY__TIME_UNIT:
			return getTimeUnit();
		case WbPackage.DELAY__DELAY_TIME:
			return getDelayTime();
		case WbPackage.DELAY__GENERATE_CHARACTERISTIC:
			return getGenerateCharacteristic();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.DELAY__TIME_UNIT:
			setTimeUnit((TimeUnit) newValue);
			return;
		case WbPackage.DELAY__DELAY_TIME:
			setDelayTime((String) newValue);
			return;
		case WbPackage.DELAY__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DELAY__TIME_UNIT:
			setTimeUnit(TIME_UNIT_EDEFAULT);
			return;
		case WbPackage.DELAY__DELAY_TIME:
			setDelayTime(DELAY_TIME_EDEFAULT);
			return;
		case WbPackage.DELAY__GENERATE_CHARACTERISTIC:
			setGenerateCharacteristic(GENERATE_CHARACTERISTIC_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DELAY__TIME_UNIT:
			return timeUnit != TIME_UNIT_EDEFAULT;
		case WbPackage.DELAY__DELAY_TIME:
			return DELAY_TIME_EDEFAULT == null ? delayTime != null : !DELAY_TIME_EDEFAULT.equals(delayTime);
		case WbPackage.DELAY__GENERATE_CHARACTERISTIC:
			return GENERATE_CHARACTERISTIC_EDEFAULT == null ? generateCharacteristic != null
					: !GENERATE_CHARACTERISTIC_EDEFAULT.equals(generateCharacteristic);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (derivedFeatureID)
			{
			case WbPackage.DELAY__TIME_UNIT:
				return WbPackage.TIMED_ELEMENT__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
	{
		if (baseClass == TimedElement.class)
		{
			switch (baseFeatureID)
			{
			case WbPackage.TIMED_ELEMENT__TIME_UNIT:
				return WbPackage.DELAY__TIME_UNIT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeUnit: ");
		result.append(timeUnit);
		result.append(", delayTime: ");
		result.append(delayTime);
		result.append(", generateCharacteristic: ");
		result.append(generateCharacteristic);
		result.append(')');
		return result.toString();
	}

} // DelayImpl
