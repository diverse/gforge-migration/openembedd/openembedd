/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.ujf.verimag.wb.Allocate;
import org.ujf.verimag.wb.AllocateRef;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.Block;
import org.ujf.verimag.wb.BlockRef;
import org.ujf.verimag.wb.Branch;
import org.ujf.verimag.wb.CategoryID;
import org.ujf.verimag.wb.ConditionalLoop;
import org.ujf.verimag.wb.DataTypeKind;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.Delay;
import org.ujf.verimag.wb.Enter;
import org.ujf.verimag.wb.ForLoop;
import org.ujf.verimag.wb.Fork;
import org.ujf.verimag.wb.Interrupt;
import org.ujf.verimag.wb.Join;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.PriorityRuleKind;
import org.ujf.verimag.wb.Release;
import org.ujf.verimag.wb.ResourceKind;
import org.ujf.verimag.wb.ResourcePool;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Resume;
import org.ujf.verimag.wb.Return;
import org.ujf.verimag.wb.Service;
import org.ujf.verimag.wb.ServiceRef;
import org.ujf.verimag.wb.Sink;
import org.ujf.verimag.wb.Source;
import org.ujf.verimag.wb.Split;
import org.ujf.verimag.wb.StatClassKind;
import org.ujf.verimag.wb.StorageKind;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.SubmodelRef;
import org.ujf.verimag.wb.Super;
import org.ujf.verimag.wb.TimeRuleKind;
import org.ujf.verimag.wb.TimeUnit;
import org.ujf.verimag.wb.User;
import org.ujf.verimag.wb.Variable;
import org.ujf.verimag.wb.WbFactory;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!-- end-user-doc -->
 * 
 * @generated
 */
public class WbFactoryImpl extends EFactoryImpl implements WbFactory
{
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static WbFactory init()
	{
		try
		{
			WbFactory theWbFactory = (WbFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://verimag.ujf.org/workbench/WB");
			if (theWbFactory != null)
			{
				return theWbFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WbFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
		case WbPackage.MODULE:
			return createModule();
		case WbPackage.DECLARATION_ZONE:
			return createDeclarationZone();
		case WbPackage.SUBMODEL:
			return createSubmodel();
		case WbPackage.ALLOCATE:
			return createAllocate();
		case WbPackage.SERVICE:
			return createService();
		case WbPackage.RESOURCE_POOL:
			return createResourcePool();
		case WbPackage.ARC:
			return createArc();
		case WbPackage.RESPONSE_ARC:
			return createResponseArc();
		case WbPackage.CATEGORY_ID:
			return createCategoryID();
		case WbPackage.VARIABLE:
			return createVariable();
		case WbPackage.SUBMODEL_REF:
			return createSubmodelRef();
		case WbPackage.SERVICE_REF:
			return createServiceRef();
		case WbPackage.ALLOCATE_REF:
			return createAllocateRef();
		case WbPackage.BLOCK:
			return createBlock();
		case WbPackage.SOURCE:
			return createSource();
		case WbPackage.SINK:
			return createSink();
		case WbPackage.ENTER:
			return createEnter();
		case WbPackage.RETURN:
			return createReturn();
		case WbPackage.SPLIT:
			return createSplit();
		case WbPackage.FORK:
			return createFork();
		case WbPackage.JOIN:
			return createJoin();
		case WbPackage.USER:
			return createUser();
		case WbPackage.DELAY:
			return createDelay();
		case WbPackage.RELEASE:
			return createRelease();
		case WbPackage.INTERRUPT:
			return createInterrupt();
		case WbPackage.FOR_LOOP:
			return createForLoop();
		case WbPackage.CONDITIONAL_LOOP:
			return createConditionalLoop();
		case WbPackage.NODE_STATISTICS:
			return createNodeStatistics();
		case WbPackage.SUPER:
			return createSuper();
		case WbPackage.RESUME:
			return createResume();
		case WbPackage.BRANCH:
			return createBranch();
		case WbPackage.BLOCK_REF:
			return createBlockRef();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue)
	{
		switch (eDataType.getClassifierID())
		{
		case WbPackage.STORAGE_KIND:
			return createStorageKindFromString(eDataType, initialValue);
		case WbPackage.DATA_TYPE_KIND:
			return createDataTypeKindFromString(eDataType, initialValue);
		case WbPackage.PRIORITY_RULE_KIND:
			return createPriorityRuleKindFromString(eDataType, initialValue);
		case WbPackage.TIME_RULE_KIND:
			return createTimeRuleKindFromString(eDataType, initialValue);
		case WbPackage.RESOURCE_KIND:
			return createResourceKindFromString(eDataType, initialValue);
		case WbPackage.STAT_CLASS_KIND:
			return createStatClassKindFromString(eDataType, initialValue);
		case WbPackage.TIME_UNIT:
			return createTimeUnitFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue)
	{
		switch (eDataType.getClassifierID())
		{
		case WbPackage.STORAGE_KIND:
			return convertStorageKindToString(eDataType, instanceValue);
		case WbPackage.DATA_TYPE_KIND:
			return convertDataTypeKindToString(eDataType, instanceValue);
		case WbPackage.PRIORITY_RULE_KIND:
			return convertPriorityRuleKindToString(eDataType, instanceValue);
		case WbPackage.TIME_RULE_KIND:
			return convertTimeRuleKindToString(eDataType, instanceValue);
		case WbPackage.RESOURCE_KIND:
			return convertResourceKindToString(eDataType, instanceValue);
		case WbPackage.STAT_CLASS_KIND:
			return convertStatClassKindToString(eDataType, instanceValue);
		case WbPackage.TIME_UNIT:
			return convertTimeUnitToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module createModule()
	{
		ModuleImpl module = new ModuleImpl();
		return module;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DeclarationZone createDeclarationZone()
	{
		DeclarationZoneImpl declarationZone = new DeclarationZoneImpl();
		return declarationZone;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Submodel createSubmodel()
	{
		SubmodelImpl submodel = new SubmodelImpl();
		return submodel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Allocate createAllocate()
	{
		AllocateImpl allocate = new AllocateImpl();
		return allocate;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Service createService()
	{
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResourcePool createResourcePool()
	{
		ResourcePoolImpl resourcePool = new ResourcePoolImpl();
		return resourcePool;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Arc createArc()
	{
		ArcImpl arc = new ArcImpl();
		return arc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResponseArc createResponseArc()
	{
		ResponseArcImpl responseArc = new ResponseArcImpl();
		return responseArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public CategoryID createCategoryID()
	{
		CategoryIDImpl categoryID = new CategoryIDImpl();
		return categoryID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Variable createVariable()
	{
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SubmodelRef createSubmodelRef()
	{
		SubmodelRefImpl submodelRef = new SubmodelRefImpl();
		return submodelRef;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ServiceRef createServiceRef()
	{
		ServiceRefImpl serviceRef = new ServiceRefImpl();
		return serviceRef;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public AllocateRef createAllocateRef()
	{
		AllocateRefImpl allocateRef = new AllocateRefImpl();
		return allocateRef;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Block createBlock()
	{
		BlockImpl block = new BlockImpl();
		return block;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Source createSource()
	{
		SourceImpl source = new SourceImpl();
		return source;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Sink createSink()
	{
		SinkImpl sink = new SinkImpl();
		return sink;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Enter createEnter()
	{
		EnterImpl enter = new EnterImpl();
		return enter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Return createReturn()
	{
		ReturnImpl return_ = new ReturnImpl();
		return return_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Split createSplit()
	{
		SplitImpl split = new SplitImpl();
		return split;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Fork createFork()
	{
		ForkImpl fork = new ForkImpl();
		return fork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Join createJoin()
	{
		JoinImpl join = new JoinImpl();
		return join;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public User createUser()
	{
		UserImpl user = new UserImpl();
		return user;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Delay createDelay()
	{
		DelayImpl delay = new DelayImpl();
		return delay;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Release createRelease()
	{
		ReleaseImpl release = new ReleaseImpl();
		return release;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Interrupt createInterrupt()
	{
		InterruptImpl interrupt = new InterruptImpl();
		return interrupt;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ForLoop createForLoop()
	{
		ForLoopImpl forLoop = new ForLoopImpl();
		return forLoop;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ConditionalLoop createConditionalLoop()
	{
		ConditionalLoopImpl conditionalLoop = new ConditionalLoopImpl();
		return conditionalLoop;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NodeStatistics createNodeStatistics()
	{
		NodeStatisticsImpl nodeStatistics = new NodeStatisticsImpl();
		return nodeStatistics;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Super createSuper()
	{
		SuperImpl super_ = new SuperImpl();
		return super_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Resume createResume()
	{
		ResumeImpl resume = new ResumeImpl();
		return resume;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Branch createBranch()
	{
		BranchImpl branch = new BranchImpl();
		return branch;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public BlockRef createBlockRef()
	{
		BlockRefImpl blockRef = new BlockRefImpl();
		return blockRef;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StorageKind createStorageKindFromString(EDataType eDataType, String initialValue)
	{
		StorageKind result = StorageKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertStorageKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DataTypeKind createDataTypeKindFromString(EDataType eDataType, String initialValue)
	{
		DataTypeKind result = DataTypeKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertDataTypeKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PriorityRuleKind createPriorityRuleKindFromString(EDataType eDataType, String initialValue)
	{
		PriorityRuleKind result = PriorityRuleKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertPriorityRuleKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeRuleKind createTimeRuleKindFromString(EDataType eDataType, String initialValue)
	{
		TimeRuleKind result = TimeRuleKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertTimeRuleKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ResourceKind createResourceKindFromString(EDataType eDataType, String initialValue)
	{
		ResourceKind result = ResourceKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertResourceKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatClassKind createStatClassKindFromString(EDataType eDataType, String initialValue)
	{
		StatClassKind result = StatClassKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertStatClassKindToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TimeUnit createTimeUnitFromString(EDataType eDataType, String initialValue)
	{
		TimeUnit result = TimeUnit.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '"
					+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertTimeUnitToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public WbPackage getWbPackage()
	{
		return (WbPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WbPackage getPackage()
	{
		return WbPackage.eINSTANCE;
	}

} // WbFactoryImpl
