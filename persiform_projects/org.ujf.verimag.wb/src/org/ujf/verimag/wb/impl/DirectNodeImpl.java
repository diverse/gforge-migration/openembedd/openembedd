/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.DirectNode;
import org.ujf.verimag.wb.NodeStatistics;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Direct Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.DirectNodeImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DirectNodeImpl#getCode <em>Code</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DirectNodeImpl#getFormalParameters <em>Formal Parameters</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DirectNodeImpl#getDeclarations <em>Declarations</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.DirectNodeImpl#getNodeStatistics <em>Node Statistics</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class DirectNodeImpl extends GraphNodeImpl implements DirectNode
{
	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final String	DIMENSION_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected String				dimension					= DIMENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String	CODE_EDEFAULT				= null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String				code						= CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalParameters() <em>Formal Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFormalParameters()
	 * @generated
	 * @ordered
	 */
	protected static final String	FORMAL_PARAMETERS_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getFormalParameters() <em>Formal Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFormalParameters()
	 * @generated
	 * @ordered
	 */
	protected String				formalParameters			= FORMAL_PARAMETERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeclarations() <em>Declarations</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getDeclarations()
	 * @generated
	 * @ordered
	 */
	protected static final String	DECLARATIONS_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' attribute. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getDeclarations()
	 * @generated
	 * @ordered
	 */
	protected String				declarations				= DECLARATIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNodeStatistics() <em>Node Statistics</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNodeStatistics()
	 * @generated
	 * @ordered
	 */
	protected EList<NodeStatistics>	nodeStatistics;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DirectNodeImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.DIRECT_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(String newDimension)
	{
		String oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DIRECT_NODE__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCode(String newCode)
	{
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DIRECT_NODE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getFormalParameters()
	{
		return formalParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setFormalParameters(String newFormalParameters)
	{
		String oldFormalParameters = formalParameters;
		formalParameters = newFormalParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DIRECT_NODE__FORMAL_PARAMETERS,
				oldFormalParameters, formalParameters));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDeclarations()
	{
		return declarations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeclarations(String newDeclarations)
	{
		String oldDeclarations = declarations;
		declarations = newDeclarations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.DIRECT_NODE__DECLARATIONS, oldDeclarations,
				declarations));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<NodeStatistics> getNodeStatistics()
	{
		if (nodeStatistics == null)
		{
			nodeStatistics = new EObjectWithInverseResolvingEList<NodeStatistics>(NodeStatistics.class, this,
				WbPackage.DIRECT_NODE__NODE_STATISTICS, WbPackage.NODE_STATISTICS__DIRECT_NODE);
		}
		return nodeStatistics;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getNodeStatistics()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			return ((InternalEList<?>) getNodeStatistics()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__DIMENSION:
			return getDimension();
		case WbPackage.DIRECT_NODE__CODE:
			return getCode();
		case WbPackage.DIRECT_NODE__FORMAL_PARAMETERS:
			return getFormalParameters();
		case WbPackage.DIRECT_NODE__DECLARATIONS:
			return getDeclarations();
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			return getNodeStatistics();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__DIMENSION:
			setDimension((String) newValue);
			return;
		case WbPackage.DIRECT_NODE__CODE:
			setCode((String) newValue);
			return;
		case WbPackage.DIRECT_NODE__FORMAL_PARAMETERS:
			setFormalParameters((String) newValue);
			return;
		case WbPackage.DIRECT_NODE__DECLARATIONS:
			setDeclarations((String) newValue);
			return;
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			getNodeStatistics().clear();
			getNodeStatistics().addAll((Collection<? extends NodeStatistics>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.DIRECT_NODE__CODE:
			setCode(CODE_EDEFAULT);
			return;
		case WbPackage.DIRECT_NODE__FORMAL_PARAMETERS:
			setFormalParameters(FORMAL_PARAMETERS_EDEFAULT);
			return;
		case WbPackage.DIRECT_NODE__DECLARATIONS:
			setDeclarations(DECLARATIONS_EDEFAULT);
			return;
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			getNodeStatistics().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.DIRECT_NODE__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.DIRECT_NODE__CODE:
			return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
		case WbPackage.DIRECT_NODE__FORMAL_PARAMETERS:
			return FORMAL_PARAMETERS_EDEFAULT == null ? formalParameters != null : !FORMAL_PARAMETERS_EDEFAULT
					.equals(formalParameters);
		case WbPackage.DIRECT_NODE__DECLARATIONS:
			return DECLARATIONS_EDEFAULT == null ? declarations != null : !DECLARATIONS_EDEFAULT.equals(declarations);
		case WbPackage.DIRECT_NODE__NODE_STATISTICS:
			return nodeStatistics != null && !nodeStatistics.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dimension: ");
		result.append(dimension);
		result.append(", code: ");
		result.append(code);
		result.append(", formalParameters: ");
		result.append(formalParameters);
		result.append(", declarations: ");
		result.append(declarations);
		result.append(')');
		return result.toString();
	}

} // DirectNodeImpl
