/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.DeclarationZone;
import org.ujf.verimag.wb.GraphNode;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.ResponseArc;
import org.ujf.verimag.wb.Submodel;
import org.ujf.verimag.wb.SubmodelRef;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Submodel</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getFormalParameters <em>Formal Parameters</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getDimension <em>Dimension</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#get_module <em>module</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getDeclarationZone <em>Declaration Zone</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getResponseArc <em>Response Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getArc <em>Arc</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getRef <em>Ref</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.SubmodelImpl#getNode <em>Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class SubmodelImpl extends GraphicalNodeImpl implements Submodel
{
	/**
	 * The default value of the '{@link #getFormalParameters() <em>Formal Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFormalParameters()
	 * @generated
	 * @ordered
	 */
	protected static final String	FORMAL_PARAMETERS_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getFormalParameters() <em>Formal Parameters</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFormalParameters()
	 * @generated
	 * @ordered
	 */
	protected String				formalParameters			= FORMAL_PARAMETERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected static final String	DIMENSION_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getDimension() <em>Dimension</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getDimension()
	 * @generated
	 * @ordered
	 */
	protected String				dimension					= DIMENSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDeclarationZone() <em>Declaration Zone</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDeclarationZone()
	 * @generated
	 * @ordered
	 */
	protected DeclarationZone		declarationZone;

	/**
	 * The cached value of the '{@link #getResponseArc() <em>Response Arc</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getResponseArc()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseArc>	responseArc;

	/**
	 * The cached value of the '{@link #getArc() <em>Arc</em>}' containment reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getArc()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc>			arc;

	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected EList<SubmodelRef>	ref;

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' containment reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected EList<GraphNode>		node;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SubmodelImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.SUBMODEL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getFormalParameters()
	{
		return formalParameters;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setFormalParameters(String newFormalParameters)
	{
		String oldFormalParameters = formalParameters;
		formalParameters = newFormalParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SUBMODEL__FORMAL_PARAMETERS,
				oldFormalParameters, formalParameters));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDimension()
	{
		return dimension;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDimension(String newDimension)
	{
		String oldDimension = dimension;
		dimension = newDimension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SUBMODEL__DIMENSION, oldDimension,
				dimension));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Module get_module()
	{
		if (eContainerFeatureID != WbPackage.SUBMODEL__MODULE)
			return null;
		return (Module) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSet_module(Module new_module, NotificationChain msgs)
	{
		msgs = eBasicSetContainer((InternalEObject) new_module, WbPackage.SUBMODEL__MODULE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void set_module(Module new_module)
	{
		if (new_module != eInternalContainer()
				|| (eContainerFeatureID != WbPackage.SUBMODEL__MODULE && new_module != null))
		{
			if (EcoreUtil.isAncestor(this, new_module))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (new_module != null)
				msgs = ((InternalEObject) new_module).eInverseAdd(this, WbPackage.MODULE__SUBMODEL, Module.class, msgs);
			msgs = basicSet_module(new_module, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SUBMODEL__MODULE, new_module, new_module));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DeclarationZone getDeclarationZone()
	{
		return declarationZone;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetDeclarationZone(DeclarationZone newDeclarationZone, NotificationChain msgs)
	{
		DeclarationZone oldDeclarationZone = declarationZone;
		declarationZone = newDeclarationZone;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
				WbPackage.SUBMODEL__DECLARATION_ZONE, oldDeclarationZone, newDeclarationZone);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDeclarationZone(DeclarationZone newDeclarationZone)
	{
		if (newDeclarationZone != declarationZone)
		{
			NotificationChain msgs = null;
			if (declarationZone != null)
				msgs = ((InternalEObject) declarationZone).eInverseRemove(this, WbPackage.DECLARATION_ZONE__SUBMODEL,
					DeclarationZone.class, msgs);
			if (newDeclarationZone != null)
				msgs = ((InternalEObject) newDeclarationZone).eInverseAdd(this, WbPackage.DECLARATION_ZONE__SUBMODEL,
					DeclarationZone.class, msgs);
			msgs = basicSetDeclarationZone(newDeclarationZone, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.SUBMODEL__DECLARATION_ZONE,
				newDeclarationZone, newDeclarationZone));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ResponseArc> getResponseArc()
	{
		if (responseArc == null)
		{
			responseArc = new EObjectContainmentWithInverseEList<ResponseArc>(ResponseArc.class, this,
				WbPackage.SUBMODEL__RESPONSE_ARC, WbPackage.RESPONSE_ARC__SUBMODEL);
		}
		return responseArc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Arc> getArc()
	{
		if (arc == null)
		{
			arc = new EObjectContainmentWithInverseEList<Arc>(Arc.class, this, WbPackage.SUBMODEL__ARC,
				WbPackage.ARC__SUBMODEL);
		}
		return arc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<SubmodelRef> getRef()
	{
		if (ref == null)
		{
			ref = new EObjectWithInverseResolvingEList<SubmodelRef>(SubmodelRef.class, this, WbPackage.SUBMODEL__REF,
				WbPackage.SUBMODEL_REF__TARGET);
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<GraphNode> getNode()
	{
		if (node == null)
		{
			node = new EObjectContainmentWithInverseEList<GraphNode>(GraphNode.class, this, WbPackage.SUBMODEL__NODE,
				WbPackage.GRAPH_NODE__SUBMODEL);
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__MODULE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSet_module((Module) otherEnd, msgs);
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			if (declarationZone != null)
				msgs = ((InternalEObject) declarationZone).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.SUBMODEL__DECLARATION_ZONE, null, msgs);
			return basicSetDeclarationZone((DeclarationZone) otherEnd, msgs);
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getResponseArc()).basicAdd(otherEnd, msgs);
		case WbPackage.SUBMODEL__ARC:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getArc()).basicAdd(otherEnd, msgs);
		case WbPackage.SUBMODEL__REF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRef()).basicAdd(otherEnd, msgs);
		case WbPackage.SUBMODEL__NODE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getNode()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__MODULE:
			return basicSet_module(null, msgs);
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			return basicSetDeclarationZone(null, msgs);
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			return ((InternalEList<?>) getResponseArc()).basicRemove(otherEnd, msgs);
		case WbPackage.SUBMODEL__ARC:
			return ((InternalEList<?>) getArc()).basicRemove(otherEnd, msgs);
		case WbPackage.SUBMODEL__REF:
			return ((InternalEList<?>) getRef()).basicRemove(otherEnd, msgs);
		case WbPackage.SUBMODEL__NODE:
			return ((InternalEList<?>) getNode()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
	{
		switch (eContainerFeatureID)
		{
		case WbPackage.SUBMODEL__MODULE:
			return eInternalContainer().eInverseRemove(this, WbPackage.MODULE__SUBMODEL, Module.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__FORMAL_PARAMETERS:
			return getFormalParameters();
		case WbPackage.SUBMODEL__DIMENSION:
			return getDimension();
		case WbPackage.SUBMODEL__MODULE:
			return get_module();
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			return getDeclarationZone();
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			return getResponseArc();
		case WbPackage.SUBMODEL__ARC:
			return getArc();
		case WbPackage.SUBMODEL__REF:
			return getRef();
		case WbPackage.SUBMODEL__NODE:
			return getNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__FORMAL_PARAMETERS:
			setFormalParameters((String) newValue);
			return;
		case WbPackage.SUBMODEL__DIMENSION:
			setDimension((String) newValue);
			return;
		case WbPackage.SUBMODEL__MODULE:
			set_module((Module) newValue);
			return;
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) newValue);
			return;
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			getResponseArc().clear();
			getResponseArc().addAll((Collection<? extends ResponseArc>) newValue);
			return;
		case WbPackage.SUBMODEL__ARC:
			getArc().clear();
			getArc().addAll((Collection<? extends Arc>) newValue);
			return;
		case WbPackage.SUBMODEL__REF:
			getRef().clear();
			getRef().addAll((Collection<? extends SubmodelRef>) newValue);
			return;
		case WbPackage.SUBMODEL__NODE:
			getNode().clear();
			getNode().addAll((Collection<? extends GraphNode>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__FORMAL_PARAMETERS:
			setFormalParameters(FORMAL_PARAMETERS_EDEFAULT);
			return;
		case WbPackage.SUBMODEL__DIMENSION:
			setDimension(DIMENSION_EDEFAULT);
			return;
		case WbPackage.SUBMODEL__MODULE:
			set_module((Module) null);
			return;
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			setDeclarationZone((DeclarationZone) null);
			return;
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			getResponseArc().clear();
			return;
		case WbPackage.SUBMODEL__ARC:
			getArc().clear();
			return;
		case WbPackage.SUBMODEL__REF:
			getRef().clear();
			return;
		case WbPackage.SUBMODEL__NODE:
			getNode().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.SUBMODEL__FORMAL_PARAMETERS:
			return FORMAL_PARAMETERS_EDEFAULT == null ? formalParameters != null : !FORMAL_PARAMETERS_EDEFAULT
					.equals(formalParameters);
		case WbPackage.SUBMODEL__DIMENSION:
			return DIMENSION_EDEFAULT == null ? dimension != null : !DIMENSION_EDEFAULT.equals(dimension);
		case WbPackage.SUBMODEL__MODULE:
			return get_module() != null;
		case WbPackage.SUBMODEL__DECLARATION_ZONE:
			return declarationZone != null;
		case WbPackage.SUBMODEL__RESPONSE_ARC:
			return responseArc != null && !responseArc.isEmpty();
		case WbPackage.SUBMODEL__ARC:
			return arc != null && !arc.isEmpty();
		case WbPackage.SUBMODEL__REF:
			return ref != null && !ref.isEmpty();
		case WbPackage.SUBMODEL__NODE:
			return node != null && !node.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (formalParameters: ");
		result.append(formalParameters);
		result.append(", dimension: ");
		result.append(dimension);
		result.append(')');
		return result.toString();
	}

} // SubmodelImpl
