/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Christian Brunette - OpenEmbeDD integration team
 * 
 * 
 *
 * $Id$
 */
package org.ujf.verimag.wb.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ujf.verimag.wb.Arc;
import org.ujf.verimag.wb.Loop;
import org.ujf.verimag.wb.WbPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Loop</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link org.ujf.verimag.wb.impl.LoopImpl#getLoopStart <em>Loop Start</em>}</li>
 * <li>{@link org.ujf.verimag.wb.impl.LoopImpl#getLoopEnd <em>Loop End</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class LoopImpl extends DirectNodeImpl implements Loop
{
	/**
	 * The cached value of the '{@link #getLoopStart() <em>Loop Start</em>}' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getLoopStart()
	 * @generated
	 * @ordered
	 */
	protected Arc	loopStart;

	/**
	 * The cached value of the '{@link #getLoopEnd() <em>Loop End</em>}' containment reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getLoopEnd()
	 * @generated
	 * @ordered
	 */
	protected Arc	loopEnd;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LoopImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return WbPackage.Literals.LOOP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Arc getLoopStart()
	{
		return loopStart;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetLoopStart(Arc newLoopStart, NotificationChain msgs)
	{
		Arc oldLoopStart = loopStart;
		loopStart = newLoopStart;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WbPackage.LOOP__LOOP_START,
				oldLoopStart, newLoopStart);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLoopStart(Arc newLoopStart)
	{
		if (newLoopStart != loopStart)
		{
			NotificationChain msgs = null;
			if (loopStart != null)
				msgs = ((InternalEObject) loopStart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.LOOP__LOOP_START, null, msgs);
			if (newLoopStart != null)
				msgs = ((InternalEObject) newLoopStart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.LOOP__LOOP_START, null, msgs);
			msgs = basicSetLoopStart(newLoopStart, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.LOOP__LOOP_START, newLoopStart,
				newLoopStart));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Arc getLoopEnd()
	{
		return loopEnd;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetLoopEnd(Arc newLoopEnd, NotificationChain msgs)
	{
		Arc oldLoopEnd = loopEnd;
		loopEnd = newLoopEnd;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WbPackage.LOOP__LOOP_END,
				oldLoopEnd, newLoopEnd);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setLoopEnd(Arc newLoopEnd)
	{
		if (newLoopEnd != loopEnd)
		{
			NotificationChain msgs = null;
			if (loopEnd != null)
				msgs = ((InternalEObject) loopEnd).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.LOOP__LOOP_END, null, msgs);
			if (newLoopEnd != null)
				msgs = ((InternalEObject) newLoopEnd).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- WbPackage.LOOP__LOOP_END, null, msgs);
			msgs = basicSetLoopEnd(newLoopEnd, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WbPackage.LOOP__LOOP_END, newLoopEnd, newLoopEnd));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
	{
		switch (featureID)
		{
		case WbPackage.LOOP__LOOP_START:
			return basicSetLoopStart(null, msgs);
		case WbPackage.LOOP__LOOP_END:
			return basicSetLoopEnd(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
		case WbPackage.LOOP__LOOP_START:
			return getLoopStart();
		case WbPackage.LOOP__LOOP_END:
			return getLoopEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
		case WbPackage.LOOP__LOOP_START:
			setLoopStart((Arc) newValue);
			return;
		case WbPackage.LOOP__LOOP_END:
			setLoopEnd((Arc) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.LOOP__LOOP_START:
			setLoopStart((Arc) null);
			return;
		case WbPackage.LOOP__LOOP_END:
			setLoopEnd((Arc) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
		case WbPackage.LOOP__LOOP_START:
			return loopStart != null;
		case WbPackage.LOOP__LOOP_END:
			return loopEnd != null;
		}
		return super.eIsSet(featureID);
	}

} // LoopImpl
