/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Olivier Constant - Verimag
 */
package org.ujf.verimag.ma2wb.transformations.layout;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DotOutputHandler
{
	private File				dotFile;
	private Map<String, Point>	content;

	public DotOutputHandler(File file)
	{
		dotFile = file;
		content = null;
	}

	private void initialize() throws IOException
	{
		content = new HashMap<String, Point>();
		BufferedReader input = new BufferedReader(new FileReader(dotFile));

		Pattern pattern = Pattern.compile("\t(\\S+)\\s\\[pos=\"(\\d+),(\\d+)\"");
		Matcher matcher;
		String line = null;
		String nodename, xs, ys;
		int x, y;
		Point p;
		while ((line = input.readLine()) != null)
		{
			matcher = pattern.matcher(line);
			while (matcher.find())
			{
				nodename = matcher.group(1);
				xs = matcher.group(2);
				ys = matcher.group(3);
				x = Integer.parseInt(xs);
				y = Integer.parseInt(ys);
				p = new Point(x, y);
				content.put(nodename, p);
			}
		}
		input.close();
	}

	public Point positionOf(String nodename) throws IOException
	{
		if (content == null)
			initialize();
		return content.get(nodename);
	}

	/* Args: 0-Dot file to be read ; 1-Name of node */
	public static void main(String[] args)
	{
		if (args.length != 2)
		{
			System.out.println("Usage: <file name> <node name>");
			return;
		}
		String filename = args[0];
		String nodename = args[1];
		try
		{
			File file = new File(filename);
			DotOutputHandler handler = new DotOutputHandler(file);
			Point p = handler.positionOf(nodename);
			System.out.println("Position of '" + nodename + "': " + (p == null ? "unknown" : p));

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
