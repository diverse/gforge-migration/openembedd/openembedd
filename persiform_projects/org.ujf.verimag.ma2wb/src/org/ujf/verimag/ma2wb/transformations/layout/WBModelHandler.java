/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Olivier Constant - Verimag
 */
package org.ujf.verimag.ma2wb.transformations.layout;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.ujf.verimag.wb.WbFactory;
import org.ujf.verimag.wb.WbPackage;
import org.ujf.verimag.wb.util.WbResourceFactoryImpl;

/**
 * Utility class for managing access to WB models
 */
public class WBModelHandler
{
	/* Default print stream for messages */
	private static final PrintStream	OUT	= java.lang.System.out;

	/* The model file */
	private File						modelFile;

	/* The EMF resource corresponding to the model file */
	private Resource					resource;

	/* The factory for manipulating models */
	private WbFactory					factory;

	public WBModelHandler(File file)
	{
		modelFile = file;
		/* Create a resource set to hold the resources. */
		ResourceSet resourceSet = new ResourceSetImpl();
		/*
		 * Register the appropriate resource factory to handle all file extensions.
		 */
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new WbResourceFactoryImpl());
		/* Register the package to ensure it is available during loading. */
		resourceSet.getPackageRegistry().put(WbPackage.eNS_URI, WbPackage.eINSTANCE);
		/* Create the corresponding EMF resource */
		URI uri = URI.createFileURI(modelFile.getAbsolutePath());
		resource = resourceSet.getResource(uri, true);
		factory = WbFactory.eINSTANCE;
	}

	public WbFactory getFactory()
	{
		return factory;
	}

	public void clearModel()
	{
		resource.getContents().clear();
	}

	public File getFile()
	{
		return modelFile;
	}

	public EList<EObject> loadModel()
	{
		return resource.getContents(); // Load on demand
	}

	public void saveModel()
	{
		saveModel(new BasicEList<EObject>());
	}

	public void saveModel(EList<EObject> elts)
	{
		for (EObject e : elts)
			resource.getContents().add(e);
		try
		{
			resource.save(new HashMap<Object, Object>());
			OUT.println("Model successfully saved in " + modelFile.getAbsolutePath());
		}
		catch (IOException e)
		{
			OUT.println("Error: Cannot save model in " + modelFile.getAbsolutePath() + ". Reason: " + e);
		}
	}

}
