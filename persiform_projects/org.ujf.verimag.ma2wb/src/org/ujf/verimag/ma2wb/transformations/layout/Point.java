/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Olivier Constant - Verimag
 */
package org.ujf.verimag.ma2wb.transformations.layout;

public class Point
{
	public int	x;
	public int	y;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	public String toString()
	{
		return "(" + x + "," + y + ")";
	}
}
