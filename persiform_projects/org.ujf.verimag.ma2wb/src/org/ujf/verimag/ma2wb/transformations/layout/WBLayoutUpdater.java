/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author Olivier Constant - Verimag
 */
package org.ujf.verimag.ma2wb.transformations.layout;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.ujf.verimag.wb.GraphicalNode;
import org.ujf.verimag.wb.Module;
import org.ujf.verimag.wb.Submodel;

public class WBLayoutUpdater
{
	private File			dotDataFolder;
	private WBModelHandler	wbmh;
	private Module			module;

	/* dot: folder for dot outputted files, wb: WB model file */
	public WBLayoutUpdater(File dot, File wb)
	{
		dotDataFolder = dot;
		wbmh = new WBModelHandler(wb);
		module = null;
	}

	private void initialize() throws RuntimeException, IOException
	{
		if (!dotDataFolder.isDirectory() || !dotDataFolder.exists())
			throw new IOException("Invalid folder for dot files: " + dotDataFolder);
		File wb = wbmh.getFile();
		if (!wb.exists())
			throw new IOException("Cannot find WB model file: " + wbmh.getFile());
		if (!wbmh.getFile().isFile())
			throw new IOException("WB model file " + wbmh.getFile() + " is not an actual file");
		EList<EObject> os = wbmh.loadModel();
		if (os == null || os.size() != 1 || !(os.get(0) instanceof Module))
			throw new RuntimeException("Invalid WB model: single root of type Module expected");
		module = (Module) os.get(0);
	}

	public void updateLayout() throws RuntimeException, IOException
	{
		if (module == null)
			initialize();
		Map<String, File> map = dotFiles();
		// Module
		File dotFile = map.get(module.getName());
		if (dotFile == null || !dotFile.exists() || !dotFile.isFile())
			System.err.println("Warning: cannot find proper dot file for module " + module.getName());
		else
			updateContentsLayout(module, dotFile);
		// Submodels
		for (Submodel sm : module.getSubmodel())
		{
			dotFile = map.get(sm.getName());
			if (dotFile == null || !dotFile.exists() || !dotFile.isFile())
			{
				System.err.println("Warning: cannot find proper dot file for submodel " + sm.getName());
				continue;
			}
			updateContentsLayout(sm, dotFile);
		}
		wbmh.saveModel();
	}

	private Map<String, File> dotFiles()
	{
		Map<String, File> map = new HashMap<String, File>();
		FileFilter filter = new FileFilter()
		{
			public boolean accept(File pathname)
			{
				return pathname.getName().endsWith(".dot");
			}
		};
		File[] files = dotDataFolder.listFiles(filter);
		String nonExtName;
		int length;
		for (File f : files)
		{
			length = f.getName().length();
			nonExtName = f.getName().substring(0, length - 4); // remove ".dot"
			map.put(nonExtName, f);
		}
		return map;
	}

	private void updateContentsLayout(EObject container, File dotFile) throws IOException
	{
		DotOutputHandler handler = new DotOutputHandler(dotFile);
		String nodename;
		Point p;
		GraphicalNode node;
		for (EObject child : container.eContents())
		{
			if (!(child instanceof GraphicalNode))
				continue;
			node = (GraphicalNode) child;
			nodename = node.getName();
			p = handler.positionOf(nodename);
			if (p == null)
			{
				System.err.println("Warning: cannot find position of " + node + " in submodel " + container);
				continue;
			}
			node.setX(p.x);
			node.setY(p.y);
		}
	}

	/* Args: 0-Folder for dot output files; 1-File for WB model */
	public static void main(String[] args)
	{
		if (args.length != 2)
		{
			System.out.println("Usage: <dot folder name> <WB file name>");
			return;
		}
		String foldername = args[0];
		String filename = args[1];
		try
		{
			File folder = new File(foldername);
			File file = new File(filename);
			WBLayoutUpdater updater = new WBLayoutUpdater(folder, file);
			updater.updateLayout();
			System.out.println("Done.");

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
