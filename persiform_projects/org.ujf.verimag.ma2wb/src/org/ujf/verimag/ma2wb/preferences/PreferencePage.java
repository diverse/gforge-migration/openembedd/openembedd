/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.preferences;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents a preference page that is contributed to the Preferences dialog. By subclassing
 * <samp>FieldEditorPreferencePage</samp>, we can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main
 * plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{
	public PreferencePage()
	{
		super(GRID);
		setPreferenceStore(org.ujf.verimag.ma2wb.Ma2WbPlugin.getDefault().getPreferenceStore());
		setDescription("The transformation from UML/Marte model to HyPerformix Workbench files needs the GraphViz tool." +
			"This tool must be installed on your computer and you have to indicate the path to its bin directory." +
			"GraphViz can be downloaded at URL http://www.graphviz.org/");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks needed to manipulate various
	 * types of preferences. Each field editor knows how to save and restore itself.
	 */
	public void createFieldEditors()
	{
		// addField(new BooleanFieldEditor(PreferenceConstants.INC_WBPROJ, "&Generate Workbench project",
		// getFieldEditorParent()));

		addField(new DirectoryFieldEditor(PreferenceConstants.GRAPHVIZ_PATH, "&Graphviz bin directory:",
			getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{}

}
