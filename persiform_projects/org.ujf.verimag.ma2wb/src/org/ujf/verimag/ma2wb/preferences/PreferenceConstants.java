/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants
{
	// public static final String INC_WBLIBS = "INC_WBLIBS";
	// public static final String INC_WBPROJ = "INC_WBPROJ";
	public static final String	GRAPHVIZ_PATH	= "GRAPHVIZ_PATH";
}
