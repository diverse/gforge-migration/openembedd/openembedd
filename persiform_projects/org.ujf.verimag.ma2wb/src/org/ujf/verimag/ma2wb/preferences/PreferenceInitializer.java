/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences()
	{
	// IPreferenceStore store = org.ujf.verimag.ma2wb.Ma2WbPlugin.getDefault().getPreferenceStore();
	// store.setDefault(PreferenceConstants.INC_WBPROJ, true);
	// store.setDefault(PreferenceConstants.INC_WBLIBS, false);
	}
}
