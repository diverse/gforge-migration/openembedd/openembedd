/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import org.eclipse.core.runtime.IPath;
import org.ujf.verimag.ma2wb.Constants;

public class UML2ARINCLauncher implements Constants
{
	public static IPath transform(IPath inputFile)
	{
		ATLInterface atl = new ATLInterface();
		atl.initTransfo(UML2ARINC_TRANSFO_PATH);
		atl.addMetamodel(UML_MM_ID, UML_MM_URI);
		atl.addMetamodel(ARINC_MM_ID, ARINC_MM_URI);

		atl.addModel(IN_MODEL_ID, inputFile, UML_MM_ID);

		IPath outputPath = inputFile.removeFileExtension().addFileExtension(ARINC_EXT);

		atl.addNewModel(OUT_MODEL_ID, outputPath, ARINC_MM_ID);

		atl.transform();

		atl.saveModel(OUT_MODEL_ID, outputPath.toOSString());

		return outputPath;
	}
}
