<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm name="0">
	<cp>
		<constant value="XMLMM2Text_Query"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="0"/>
		<constant value="A.__initindentLength():V"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="XMLDocument"/>
		<constant value="XMLMM"/>
		<constant value="J.allInstances():J"/>
		<constant value="1"/>
		<constant value="J.toText():J"/>
		<constant value="./tmp/"/>
		<constant value="Output.grf"/>
		<constant value="J.+(J):J"/>
		<constant value="Persiform XML2Text - Writing XML file"/>
		<constant value="J.debug(J):J"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="2:26-2:43"/>
		<constant value="2:26-2:58"/>
		<constant value="3:16-3:19"/>
		<constant value="3:16-3:28"/>
		<constant value="4:4-4:12"/>
		<constant value="6:3-6:15"/>
		<constant value="4:4-6:15"/>
		<constant value="6:23-6:62"/>
		<constant value="4:3-6:63"/>
		<constant value="3:16-7:3"/>
		<constant value="2:26-7:4"/>
		<constant value="doc"/>
		<constant value="self"/>
		<constant value="__initindentLength"/>
		<constant value="3"/>
		<constant value="indentLength"/>
		<constant value="12:38-12:39"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<load arg="3"/>
			<call arg="4"/>
			<push arg="5"/>
			<push arg="6"/>
			<new/>
			<push arg="7"/>
			<push arg="8"/>
			<findme/>
			<call arg="9"/>
			<iterate/>
			<store arg="10"/>
			<load arg="10"/>
			<call arg="11"/>
			<push arg="12"/>
			<push arg="13"/>
			<call arg="14"/>
			<push arg="15"/>
			<call arg="16"/>
			<call arg="17"/>
			<call arg="18"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="19" begin="5" end="7"/>
			<lne id="20" begin="5" end="8"/>
			<lne id="21" begin="11" end="11"/>
			<lne id="22" begin="11" end="12"/>
			<lne id="23" begin="13" end="13"/>
			<lne id="24" begin="14" end="14"/>
			<lne id="25" begin="13" end="15"/>
			<lne id="26" begin="16" end="16"/>
			<lne id="27" begin="13" end="17"/>
			<lne id="28" begin="11" end="18"/>
			<lne id="29" begin="2" end="20"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="30" begin="10" end="19"/>
			<lve slot="0" name="31" begin="0" end="20"/>
		</localvariabletable>
	</operation>
	<operation name="32">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<load arg="3"/>
			<pushi arg="33"/>
			<set arg="34"/>
		</code>
		<linenumbertable>
			<lne id="35" begin="1" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="31" begin="0" end="2"/>
		</localvariabletable>
	</operation>
</asm>
