/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IPath;
import org.ujf.verimag.ma2wb.Constants;
import org.ujf.verimag.ma2wb.transformations.layout.WBLayoutUpdater;

public class WBLayoutUpdateLauncher implements Constants
{
	public static void transform(IPath inputFile) throws RuntimeException, IOException
	{
		File javaIoWbFile = new File(inputFile.toOSString());
		File dotOutFolder = new File(DOTOUT_FOLDER_PATH);

		WBLayoutUpdater updater = new WBLayoutUpdater(dotOutFolder, javaIoWbFile);
		updater.updateLayout();
	}
}
