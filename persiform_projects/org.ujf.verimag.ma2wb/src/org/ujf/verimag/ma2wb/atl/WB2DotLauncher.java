/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import java.net.MalformedURLException;

import org.eclipse.core.runtime.IPath;
import org.ujf.verimag.ma2wb.Constants;

public class WB2DotLauncher implements Constants
{
	public static void transform(IPath inputFile) throws MalformedURLException
	{
		ATLInterface atl = new ATLInterface();
		atl.initTransfo(WB2DOT_QUERY_PATH);
		atl.addMetamodel(WB_MM_ID, WB_MM_URI);
		atl.addModel(IN_MODEL_ID, inputFile, WB_MM_ID);
		atl.addLibrary(WB2DOT_LIB_PATH);

		atl.transform();
	}
}
