/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import org.eclipse.core.runtime.IPath;
import org.ujf.verimag.ma2wb.Constants;

public class WB2XMLLauncher implements Constants
{
	public static IPath transform(IPath inputFile)
	{
		// the XML intermediate file
		IPath outputPath = inputFile.removeFileExtension().addFileExtension(XML_EXT);

		// WB2XML step
		ATLInterface atl = new ATLInterface();
		atl.initTransfo(WB2XML_TRANSFO_PATH);

		atl.addMetamodel(WB_MM_ID, WB_MM_URI);
		atl.addMetamodel(XML_MM_ID, XML_MM_URI);
		atl.addModel(IN_MODEL_ID, inputFile, WB_MM_ID);
		atl.addNewModel(OUT_MODEL_ID, outputPath, XML_MM_ID);

		atl.transform();

		atl.saveModel(OUT_MODEL_ID, outputPath.toOSString());

		return outputPath;
	}
}
