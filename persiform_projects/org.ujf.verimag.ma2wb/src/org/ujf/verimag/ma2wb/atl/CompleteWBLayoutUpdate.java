/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.openembedd.launchexec.Console;
import org.openembedd.launchexec.ExecCommand;
import org.openembedd.launchexec.Plugin;
import org.ujf.verimag.ma2wb.Constants;
import org.ujf.verimag.ma2wb.Ma2WbPlugin;
import org.ujf.verimag.ma2wb.preferences.PreferenceConstants;

public class CompleteWBLayoutUpdate implements Constants
{
	public static void transform(IPath inputFile)
	{
		try
		{
			// Localize the GraphViz Dot executable on the file system
			String graphvizBinPath = org.ujf.verimag.ma2wb.Ma2WbPlugin.getDefault().getPreferenceStore().getString(
				PreferenceConstants.GRAPHVIZ_PATH);

			// For Windows platform, the executable will be suffixed by .exe
			// For Linux & MacOS X, it stays dot
			String execName = "/dot";
			if (Platform.getOS().equals(Platform.OS_WIN32))
				execName += ".exe";

			File entry = new File(graphvizBinPath + execName);
			if (entry.exists())
			{
				// ////////// WB2Dot step //////////////

				// create Dot input folder (for WB2Dot output)
				File dotInFolder = new File(DOTIN_FOLDER_PATH);
				if (dotInFolder.exists())
				{
					for (int i = 0; i < dotInFolder.listFiles().length; i++)
						dotInFolder.listFiles()[i].delete();
				}
				else
					dotInFolder.mkdir();

				WB2DotLauncher.transform(inputFile);

				// ////////// DotAll step //////////////////

				String[] dotArgs = new String[8];
				Console console = new Console("GraphViz Dot tool console");

				// create output folder
				File dotOutFolder = new File(DOTOUT_FOLDER_PATH);
				if (dotOutFolder.exists())
				{
					for (int i = 0; i < dotOutFolder.listFiles().length; i++)
						dotOutFolder.listFiles()[i].delete();
				}
				else
					dotOutFolder.mkdir();

				// Get the absolute path of the executable
				String dotPath = entry.getAbsolutePath();

				dotArgs[0] = dotPath;
				dotArgs[1] = "-Tdot";
				dotArgs[2] = "-Grankdir=LR";
				dotArgs[3] = "-Granksep='0.4'";
				dotArgs[4] = "-Gnodesep='1.5'";

				// we process all the *.dot files generated at previous step
				for (int i = 0; i < dotInFolder.listFiles().length; i++)
				{
					File dotFile = dotInFolder.listFiles()[i];
					String dotFileName = dotFile.getName();

					dotArgs[5] = dotFile.getAbsolutePath();
					dotArgs[6] = "-o";
					dotArgs[7] = DOTOUT_FOLDER_PATH + "/" + dotFileName; // output file (temporary folder)

					// execute the GraphViz Dot tool
					ExecCommand command = new ExecCommand(console, dotArgs);
					command.execute();
				}

				// ////////// Layout update step ////////////////

				WBLayoutUpdateLauncher.transform(inputFile);
			}
			else
				// if Dot is not reachable, do nothing at all
				Plugin.getDefault().getLog().log(
					new Status(IStatus.ERROR, Plugin.PLUGIN_ID, "Cannot find the GraphViz Dot utility."));

		}
		catch (MalformedURLException mue)
		{
			Ma2WbPlugin.getDefault().getLog()
					.log(
						new Status(IStatus.ERROR, Ma2WbPlugin.PLUGIN_ID, 0,
							"Problem during the WB to Dot transformation.", mue));
		}
		catch (RuntimeException re)
		{
			Ma2WbPlugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, Ma2WbPlugin.PLUGIN_ID, 0, "Problem during the WB Layout update.", re));
		}
		catch (IOException ioe)
		{
			Ma2WbPlugin.getDefault().getLog().log(
				new Status(IStatus.ERROR, Ma2WbPlugin.PLUGIN_ID, 0, "Problem during the WB Layout update.", ioe));
		}
	}
}
