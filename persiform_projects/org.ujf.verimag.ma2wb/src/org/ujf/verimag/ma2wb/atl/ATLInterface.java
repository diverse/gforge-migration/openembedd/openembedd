/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.ujf.verimag.ma2wb.Constants;

/**
 * Class that calls the transformation onto the ATL Interface on the selected file
 * 
 * You must call first initTransfo(), second initMetamodels() and then transform() in order to complete the execution of
 * your ATL transformation
 */
public class ATLInterface implements Constants
{
	/**
	 * The model handler used for the transformation
	 */
	private AtlEMFModelHandler	modelHandler;

	/**
	 * The URL to the transformation resource file
	 */
	private URL					transfoResource;

	/**
	 * The map of input/output models/metamodels
	 */
	private Map<String, Object>	models		= new HashMap<String, Object>();

	/**
	 * The map of transformation libraries
	 */
	private Map<String, Object>	libraries	= new HashMap<String, Object>();

	/**
	 * Initialize the transformation resources
	 * 
	 * @param transfo
	 *        the path to the ATL transformation file
	 */
	public void initTransfo(String atlTransfo)
	{
		modelHandler = (AtlEMFModelHandler) AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		// compute the path to ATL transformations (.asm files)
		transfoResource = ATLInterface.class.getResource(atlTransfo);
	}

	/**
	 * add a metamodel needed by the transformation
	 * 
	 * @param metamodelId
	 *        the ATL Id of the metamodel
	 * @param metamodelUri
	 *        the URI (full path) of the metamodel file
	 */
	public void addMetamodel(String metamodelId, String metamodelUri)
	{
		ASMEMFModel metamodel = (ASMEMFModel) modelHandler.loadModel(metamodelId, modelHandler.getMof(), metamodelUri);

		models.put(metamodelId, metamodel);
	}

	/**
	 * add a library needed by the transformation
	 * 
	 * @param libraryUri
	 *        the URI (full path) of the library file
	 */
	public void addLibrary(String path) throws MalformedURLException
	{
		// We must had the plug-in path
		URL libURL = new URL(Platform.getBundle(PLUGIN_NAME).getEntry(ATL_PATH) + path);
		libraries.put(path, libURL);
	}

	/**
	 * @return
	 */
	private Map<String, Object> getLibraries()
	{
		if (libraries.size() == 0)
			libraries = Collections.emptyMap();
		return libraries;
	}

	/**
	 * add a model to be created by the transformation
	 * 
	 * @param modelId
	 *        The ATL ID of the model
	 * @param modelPath
	 *        The full path of the model file
	 * @param metamodelId
	 *        The ATL ID of the model metamodel
	 */
	public void addNewModel(String modelId, IPath modelPath, String metamodelId)
	{
		// get/create model
		ASMEMFModel metamodel = (ASMEMFModel) models.get(metamodelId);
		ASMEMFModel model = (ASMEMFModel) modelHandler.newModel(modelId, URI.createFileURI(modelPath.toOSString())
				.toFileString(), metamodel);
		// load models
		models.put(modelId, model);
	}

	/**
	 * add a model to the transformation models map
	 * 
	 * @param modelId
	 *        The ATL ID of the model
	 * @param modelPath
	 *        The full path of the model file
	 * @param metamodelId
	 *        The ATL ID of the model metamodel
	 */
	public void addModel(String modelId, IPath modelPath, String metamodelId)
	{
		// get/create model
		ASMEMFModel metamodel = (ASMEMFModel) models.get(metamodelId);
		ASMEMFModel model = (ASMEMFModel) modelHandler.loadModel(modelId, metamodel, URI.createFileURI(modelPath
				.toOSString()));
		// load models
		models.put(modelId, model);
	}

	/**
	 * Call the transformation
	 */
	public void transform()
	{
		try
		{
			// add the continue after errors options
			Map<String, String> options = new HashMap<String, String>();
			options.put("continueAfterError", "true");

			// launch
			AtlLauncher.getDefault().launch(transfoResource, getLibraries(), models, Collections.EMPTY_MAP,
				Collections.EMPTY_LIST, options);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Call the transformation
	 */
	public void saveModel(String outModelId, String modelFullPath)
	{
		ASMEMFModel outModel = (ASMEMFModel) models.get(outModelId);
		modelHandler.saveModel(outModel, modelFullPath, false);
	}
}
