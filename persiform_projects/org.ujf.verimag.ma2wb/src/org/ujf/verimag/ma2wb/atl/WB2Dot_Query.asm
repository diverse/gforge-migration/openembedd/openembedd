<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm name="0">
	<cp>
		<constant value="WB2Dot_Query"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="Module"/>
		<constant value="WB"/>
		<constant value="J.allInstances():J"/>
		<constant value="1"/>
		<constant value="submodel"/>
		<constant value="J.including(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.flatten():J"/>
		<constant value="2"/>
		<constant value="J.toDotGraph():J"/>
		<constant value="J.toDotFile():J"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="4:3-4:12"/>
		<constant value="4:3-4:27"/>
		<constant value="4:41-4:42"/>
		<constant value="4:41-4:51"/>
		<constant value="4:63-4:64"/>
		<constant value="4:41-4:65"/>
		<constant value="4:3-4:66"/>
		<constant value="4:3-4:77"/>
		<constant value="3:6-4:77"/>
		<constant value="5:2-5:8"/>
		<constant value="5:22-5:23"/>
		<constant value="5:22-5:36"/>
		<constant value="5:45-5:46"/>
		<constant value="5:45-5:58"/>
		<constant value="5:22-5:59"/>
		<constant value="5:2-5:60"/>
		<constant value="3:2-5:60"/>
		<constant value="m"/>
		<constant value="g"/>
		<constant value="graphs"/>
		<constant value="self"/>
		<constant value="toDotFile"/>
		<constant value="MWB!NamedElement;"/>
		<constant value="./tmp/dotin/"/>
		<constant value="0"/>
		<constant value="name"/>
		<constant value=".dot"/>
		<constant value="J.+(J):J"/>
		<constant value="WB2Dot - Writing dot file"/>
		<constant value="J.debug(J):J"/>
		<constant value="11:2-11:16"/>
		<constant value="12:3-12:7"/>
		<constant value="12:3-12:12"/>
		<constant value="12:15-12:21"/>
		<constant value="12:3-12:21"/>
		<constant value="12:29-12:56"/>
		<constant value="12:2-12:57"/>
		<constant value="11:2-12:57"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="3"/>
			<push arg="4"/>
			<new/>
			<push arg="5"/>
			<push arg="6"/>
			<findme/>
			<call arg="7"/>
			<iterate/>
			<store arg="8"/>
			<load arg="8"/>
			<get arg="9"/>
			<load arg="8"/>
			<call arg="10"/>
			<call arg="11"/>
			<enditerate/>
			<call arg="12"/>
			<store arg="8"/>
			<push arg="3"/>
			<push arg="4"/>
			<new/>
			<load arg="8"/>
			<iterate/>
			<store arg="13"/>
			<load arg="13"/>
			<call arg="14"/>
			<load arg="13"/>
			<call arg="15"/>
			<call arg="16"/>
			<call arg="11"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="17" begin="3" end="5"/>
			<lne id="18" begin="3" end="6"/>
			<lne id="19" begin="9" end="9"/>
			<lne id="20" begin="9" end="10"/>
			<lne id="21" begin="11" end="11"/>
			<lne id="22" begin="9" end="12"/>
			<lne id="23" begin="0" end="14"/>
			<lne id="24" begin="0" end="15"/>
			<lne id="25" begin="0" end="15"/>
			<lne id="26" begin="20" end="20"/>
			<lne id="27" begin="23" end="23"/>
			<lne id="28" begin="23" end="24"/>
			<lne id="29" begin="25" end="25"/>
			<lne id="30" begin="25" end="26"/>
			<lne id="31" begin="23" end="27"/>
			<lne id="32" begin="17" end="29"/>
			<lne id="33" begin="0" end="29"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="34" begin="8" end="13"/>
			<lve slot="2" name="35" begin="22" end="28"/>
			<lve slot="1" name="36" begin="16" end="29"/>
			<lve slot="0" name="37" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="38">
		<context type="39"/>
		<parameters>
		</parameters>
		<code>
			<push arg="40"/>
			<load arg="41"/>
			<get arg="42"/>
			<push arg="43"/>
			<call arg="44"/>
			<push arg="45"/>
			<call arg="46"/>
			<call arg="44"/>
		</code>
		<linenumbertable>
			<lne id="47" begin="0" end="0"/>
			<lne id="48" begin="1" end="1"/>
			<lne id="49" begin="1" end="2"/>
			<lne id="50" begin="3" end="3"/>
			<lne id="51" begin="1" end="4"/>
			<lne id="52" begin="5" end="5"/>
			<lne id="53" begin="1" end="6"/>
			<lne id="54" begin="0" end="7"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="37" begin="0" end="7"/>
		</localvariabletable>
	</operation>
</asm>
