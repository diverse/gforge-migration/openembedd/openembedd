/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.atl;

import org.eclipse.core.runtime.IPath;
import org.ujf.verimag.ma2wb.Constants;

public class ARINC2WBLauncher implements Constants
{
	public static IPath transform(IPath inputFile)
	{
		ATLInterface atl = new ATLInterface();
		atl.initTransfo(ARINC2WB_TRANSFO_PATH);
		atl.addMetamodel(ARINC_MM_ID, ARINC_MM_URI);
		atl.addMetamodel(WB_MM_ID, WB_MM_URI);

		atl.addModel(IN_MODEL_ID, inputFile, ARINC_MM_ID);

		IPath outputPath = inputFile.removeFileExtension().addFileExtension(WB_EXT);
		atl.addNewModel(OUT_MODEL_ID, outputPath, WB_MM_ID);

		atl.transform();

		atl.saveModel(OUT_MODEL_ID, outputPath.toOSString());

		return outputPath;
	}
}
