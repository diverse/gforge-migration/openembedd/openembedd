/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.popup.actions;

import java.net.MalformedURLException;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.ujf.verimag.ma2wb.Constants;
import org.ujf.verimag.ma2wb.Ma2WbPlugin;
import org.ujf.verimag.ma2wb.atl.XML2TextLauncher;

public class XML2TextAction implements IObjectActionDelegate, Constants
{
	private StructuredSelection	selection;

	/**
	 * Constructor
	 */
	public XML2TextAction()
	{
		super();
		this.selection = null;
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action)
	{
		for (Iterator<?> it = selection.iterator(); it.hasNext();)
		{
			Object obj = (Object) it.next();
			if (obj instanceof IFile)
			{
				try
				{
					IFile inputFile = (IFile) obj;
					XML2TextLauncher.transform(inputFile.getLocation());
					try
					{
						// will be useful when we know how pass output file name to the query
						inputFile.getParent().refreshLocal(2, null);
					}
					catch (CoreException ce)
					{
						Ma2WbPlugin.getDefault().getLog().log(
							new Status(IStatus.ERROR, Ma2WbPlugin.PLUGIN_ID, 0, "Refreshing error.", ce));
					}
				}
				catch (MalformedURLException mue)
				{
					Ma2WbPlugin.getDefault().getLog().log(
						new Status(IStatus.ERROR, Ma2WbPlugin.PLUGIN_ID, 0,
							"Problem during the XML to Text transformation.", mue));
				}
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		if (selection instanceof StructuredSelection)
		{
			this.selection = (StructuredSelection) selection;
		}
	}
}
