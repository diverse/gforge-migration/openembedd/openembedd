/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb;

public interface Constants
{
	/**
	 * name of this plug-in
	 */
	public final static String	PLUGIN_NAME				= "org.ujf.verimag.ma2wb";

	/**
	 * path to ATL transformations
	 */
	public final static String	ATL_PATH				= "/src/org/ujf/verimag/ma2wb/atl/";

	/**
	 * Uri of the Worbench metamodel
	 */
	public final static String	WB_MM_URI				= "uri:http://verimag.ujf.org/workbench/WB";

	/**
	 * Uri of the UML2 metamodel
	 */
	public final static String	UML_MM_URI				= "uri:http://www.eclipse.org/uml2/2.1.0/UML";

	/**
	 * Uri of the ARINC metamodel
	 */
	public final static String	ARINC_MM_URI			= "uri:http://verimag.ujf.org/ARINC";

	/**
	 * Uri of the XML metamodel
	 */
	public final static String	XML_MM_URI				= "uri:http://verimag.ujf.org/xml/XML";

	/**
	 * Id of the Workbench metamodel in the ATL transformation file
	 */
	public final static String	WB_MM_ID				= "WB";

	/**
	 * Id of the UML2 metamodel in the ATL transformation file
	 */
	public final static String	UML_MM_ID				= "UML";

	/**
	 * Id of the Marte/ARINC metamodel in the ATL transformation file
	 */
	public final static String	ARINC_MM_ID				= "ARINC";

	/**
	 * Id of the XML metamodel in the ATL transformation file
	 */
	public final static String	XML_MM_ID				= "XML";

	/**
	 * Id of the input model in ATL transformations
	 */
	public final static String	IN_MODEL_ID				= "IN";

	/**
	 * Id of the output model in ATL transformations
	 */
	public final static String	OUT_MODEL_ID			= "OUT";

	/**
	 * Extension of Workbench file
	 */
	public final static String	WB_EXT					= "wb";

	/**
	 * Extension of UML2 file
	 */
	public final static String	UML_EXT					= "uml";

	/**
	 * Extension of Fiacre file
	 */
	public final static String	ARINC_EXT				= "arinc";

	/**
	 * Extension of XML file
	 */
	public final static String	XML_EXT					= "xml";

	/**
	 * Path of WB2Dot query
	 */
	public final static String	WB2DOT_QUERY_PATH		= "WB2Dot_Query.asm";

	/**
	 * Path of WB2Dot library
	 */
	public final static String	WB2DOT_LIB_PATH			= "WB2Dot.asm";

	/**
	 * Path of temporary Dot input folder
	 * 
	 * CARE !!! If you modify this path, you have to modify also the path in WB2Dot_Query.atl and XML2Text_Query.atl
	 */
	public final static String	DOTIN_FOLDER_PATH		= "/tmp/dotin";

	/**
	 * Path of temporary Dot output folder
	 * 
	 * CARE !!! If you modify this path, you have to modify also the path in WB2Dot_Query.atl and XML2Text_Query.atl
	 */
	public final static String	DOTOUT_FOLDER_PATH		= "/tmp/dotout";

	/**
	 * Path of WB2XML transformation
	 */
	public final static String	WB2XML_TRANSFO_PATH		= "WB2XML.asm";

	/**
	 * Path of UML2ARINC transformation
	 */
	public final static String	UML2ARINC_TRANSFO_PATH	= "UML2ARINC.asm";

	/**
	 * Path of ARINC2WB transformation
	 */
	public final static String	ARINC2WB_TRANSFO_PATH	= "ARINC2WB.asm";

	/**
	 * Path of XML2Text query
	 */
	public final static String	XML2TEXT_QUERY_PATH		= "XML2Text_Query.asm";

	/**
	 * Path of XML2Text library
	 */
	public final static String	XML2TEXT_LIB_PATH		= "XML2Text.asm";
}
