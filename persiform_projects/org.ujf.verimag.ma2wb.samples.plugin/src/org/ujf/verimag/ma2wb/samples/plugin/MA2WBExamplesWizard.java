/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.samples.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openembedd.wizards.AbstractNewExampleWizard;

public class MA2WBExamplesWizard extends AbstractNewExampleWizard
{
	protected Collection<ProjectDescriptor> getProjectDescriptors()
	{
		// We need the statements example to be unzipped along with the
		// EMF library example model, edit and editor examples
		List<ProjectDescriptor> projects = new ArrayList<ProjectDescriptor>(1);
		projects.add(new ProjectDescriptor("org.ujf.verimag.ma2wb.samples.plugin",
			"zip/org.ujf.verimag.ma2wb.samples.zip", "org.ujf.verimag.ma2wb.samples"));
		return projects;
	}
}
