/**
 * Copyright : UJF-VERIMAG 
 * This plug-in is under the terms of the EPL License. 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * @author 	Vincent Mah� - OpenEmbeDD integration team
 * 			Christian Brunette - OpenEmbeDD integration team
 */
package org.ujf.verimag.ma2wb.samples.plugin;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openembedd.wizards.AbstractDemoPlugin;

/**
 * The main plugin class to be used in the desktop.
 */
public class MA2WBExamplesPlugin extends AbstractDemoPlugin
{
	public ImageDescriptor getImageDescriptor(String path)
	{
		return AbstractUIPlugin.imageDescriptorFromPlugin("org.ujf.verimag.ma2wb.samples", path);
	}
}
